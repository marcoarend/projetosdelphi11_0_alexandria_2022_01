object FmSAC_01_STC: TFmSAC_01_STC
  Tag = 1
  Left = 258
  Top = 135
  Caption = 'ATD-TELEF-005 :: Terreno'
  ClientHeight = 438
  ClientWidth = 1000
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GBConfirma: TGroupBox
    Left = 0
    Top = 375
    Width = 1000
    Height = 63
    Align = alBottom
    TabOrder = 0
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 12
      Top = 17
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel4: TPanel
      Left = 890
      Top = 15
      Width = 108
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 7
        Top = 2
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 1000
    Height = 375
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 1000
      Height = 375
      Align = alClient
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 2
        Top = 15
        Width = 996
        Height = 66
        Align = alTop
        Caption = ' Dados Gerais: '
        TabOrder = 0
        object Label1: TLabel
          Left = 540
          Top = 20
          Width = 38
          Height = 13
          Caption = 'Cliente: '
          Enabled = False
        end
        object Label2: TLabel
          Left = 12
          Top = 20
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          Enabled = False
        end
        object Label3: TLabel
          Left = 80
          Top = 20
          Width = 114
          Height = 13
          Caption = 'Descri'#231#227'o do endere'#231'o:'
          FocusControl = EdNome
        end
        object EdCliente: TdmkEditCB
          Left = 540
          Top = 36
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cliente'
          UpdCampo = 'Cliente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 596
          Top = 36
          Width = 345
          Height = 21
          Enabled = False
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsCunsCad
          TabOrder = 3
          dmkEditCB = EdCliente
          QryCampo = 'Cliente'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCodigo: TdmkEdit
          Left = 12
          Top = 36
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome: TdmkEdit
          Left = 80
          Top = 36
          Width = 453
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object GB_R: TGroupBox
          Left = 946
          Top = 15
          Width = 48
          Height = 49
          Align = alRight
          TabOrder = 4
          object ImgTipo: TdmkImage
            Left = 8
            Top = 11
            Width = 32
            Height = 32
            Transparent = True
            SQLType = stNil
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 2
        Top = 81
        Width = 996
        Height = 292
        Align = alClient
        Caption = ' Dados do endere'#231'o: '
        TabOrder = 1
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 992
          Height = 275
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label12: TLabel
            Left = 124
            Top = 8
            Width = 24
            Height = 13
            Caption = 'CEP:'
          end
          object Label97: TLabel
            Left = 8
            Top = 32
            Width = 77
            Height = 13
            Caption = 'Tipo logradouro:'
          end
          object Label31: TLabel
            Left = 268
            Top = 32
            Width = 99
            Height = 13
            Caption = 'Nome do logradouro:'
            FocusControl = EdSRua
          end
          object Label32: TLabel
            Left = 716
            Top = 32
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
            FocusControl = EdSNumero
          end
          object Label34: TLabel
            Left = 8
            Top = 76
            Width = 30
            Height = 13
            Caption = 'Bairro:'
            FocusControl = EdSBairro
          end
          object Label35: TLabel
            Left = 324
            Top = 76
            Width = 36
            Height = 13
            Caption = 'Cidade:'
            FocusControl = EdSCidade
          end
          object Label38: TLabel
            Left = 640
            Top = 76
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label15: TLabel
            Left = 672
            Top = 76
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
          end
          object Label16: TLabel
            Left = 808
            Top = 32
            Width = 67
            Height = 13
            Caption = 'Complemento:'
            FocusControl = EdSCompl
          end
          object Label17: TLabel
            Left = 8
            Top = 116
            Width = 55
            Height = 13
            Caption = 'Refer'#234'ncia:'
            FocusControl = EdSEndeRef
          end
          object Label106: TLabel
            Left = 8
            Top = 156
            Width = 36
            Height = 13
            Caption = 'Cidade:'
          end
          object Label109: TLabel
            Left = 336
            Top = 156
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
          end
          object Label112: TLabel
            Left = 868
            Top = 116
            Width = 54
            Height = 13
            Caption = 'Telefone 1:'
          end
          object Label4: TLabel
            Left = 648
            Top = 156
            Width = 55
            Height = 13
            Caption = 'Constru'#237'da:'
          end
          object Label6: TLabel
            Left = 732
            Top = 156
            Width = 77
            Height = 13
            Caption = 'N'#227'o constru'#237'da:'
          end
          object Label10: TLabel
            Left = 816
            Top = 156
            Width = 40
            Height = 13
            Caption = 'Terreno:'
          end
          object Label11: TLabel
            Left = 900
            Top = 156
            Width = 27
            Height = 13
            Caption = 'Total:'
            Enabled = False
          end
          object BtCEP_S: TBitBtn
            Left = 232
            Top = 4
            Width = 21
            Height = 21
            TabOrder = 1
            TabStop = False
            OnClick = BtCEP_SClick
          end
          object EdSCEP: TdmkEdit
            Left = 156
            Top = 4
            Width = 72
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtCEP
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SCEP'
            UpdCampo = 'SCEP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnEnter = EdSCEPEnter
            OnExit = EdSCEPExit
          end
          object CBSLograd: TdmkDBLookupComboBox
            Left = 64
            Top = 48
            Width = 200
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsListaLograd
            TabOrder = 3
            dmkEditCB = EdSLograd
            QryCampo = 'SLograd'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdSRua: TdmkEdit
            Left = 268
            Top = 48
            Width = 444
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SRua'
            UpdCampo = 'SRua'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSNumero: TdmkEdit
            Left = 716
            Top = 48
            Width = 88
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SNumero'
            UpdCampo = 'SNumero'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSBairro: TdmkEdit
            Left = 8
            Top = 92
            Width = 312
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SBairro'
            UpdCampo = 'SBairro'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSCidade: TdmkEdit
            Left = 324
            Top = 92
            Width = 312
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SCidade'
            UpdCampo = 'SCidade'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSUF: TdmkEdit
            Left = 640
            Top = 92
            Width = 28
            Height = 21
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SUF'
            UpdCampo = 'SUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSPais: TdmkEdit
            Left = 672
            Top = 92
            Width = 308
            Height = 21
            TabOrder = 10
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SPais'
            UpdCampo = 'SPais'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSLograd: TdmkEditCB
            Left = 8
            Top = 48
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'SLograd'
            UpdCampo = 'SLograd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBSLograd
            IgnoraDBLookupComboBox = False
          end
          object EdSCompl: TdmkEdit
            Left = 808
            Top = 48
            Width = 172
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SCompl'
            UpdCampo = 'SCompl'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSEndeRef: TdmkEdit
            Left = 8
            Top = 132
            Width = 857
            Height = 21
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SEndeRef'
            UpdCampo = 'SEndeRef'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSCodMunici: TdmkEditCB
            Left = 8
            Top = 172
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 13
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'SCodMunici'
            UpdCampo = 'SCodMunici'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBSCodMunici
            IgnoraDBLookupComboBox = False
          end
          object CBSCodMunici: TdmkDBLookupComboBox
            Left = 64
            Top = 172
            Width = 269
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsMunici
            TabOrder = 14
            dmkEditCB = EdSCodMunici
            QryCampo = 'SCodMunici'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdSCodiPais: TdmkEditCB
            Left = 336
            Top = 172
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 15
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'SCodiPais'
            UpdCampo = 'SCodiPais'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBSCodiPais
            IgnoraDBLookupComboBox = False
          end
          object CBSCodiPais: TdmkDBLookupComboBox
            Left = 392
            Top = 172
            Width = 253
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsBacen_Pais
            TabOrder = 16
            dmkEditCB = EdSCodiPais
            QryCampo = 'SCodiPais'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdSTe1: TdmkEdit
            Left = 868
            Top = 132
            Width = 112
            Height = 21
            TabOrder = 12
            FormatType = dmktfString
            MskType = fmtTelLongo
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'STe1'
            UpdCampo = 'STe1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdM2Constru: TdmkEdit
            Left = 648
            Top = 172
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 17
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'M2Constru'
            UpdCampo = 'M2Constru'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdM2ConstruChange
          end
          object EdM2NaoBuild: TdmkEdit
            Left = 732
            Top = 172
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 18
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'M2NaoBuild'
            UpdCampo = 'M2NaoBuild'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdM2NaoBuildChange
          end
          object EdM2Terreno: TdmkEdit
            Left = 816
            Top = 172
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 19
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'M2Terreno'
            UpdCampo = 'M2Terreno'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdM2Total: TdmkEdit
            Left = 900
            Top = 172
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 20
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'M2Total'
            UpdCampo = 'M2Total'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object BrCopia: TBitBtn
            Left = 8
            Top = 4
            Width = 113
            Height = 21
            Caption = 'Copia da entidade'
            TabOrder = 21
            TabStop = False
            OnClick = BrCopiaClick
          end
        end
      end
    end
  end
  object PMCEP: TPopupMenu
    Left = 344
    Top = 12
    object Descobrir1: TMenuItem
      Caption = '&Descobrir (BD)'
      OnClick = Descobrir1Click
    end
    object Verificar1: TMenuItem
      Caption = '&Verificar (BD)'
      OnClick = Verificar1Click
    end
    object VerificarInternet1: TMenuItem
      Caption = 'Verificar (&Internet)'
      OnClick = VerificarInternet1Click
    end
  end
  object QrBacen_Pais: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bacen_pais'
      'ORDER BY Nome'
      '')
    Left = 372
    Top = 12
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBacen_Pais: TDataSource
    DataSet = QrBacen_Pais
    Left = 400
    Top = 12
  end
  object QrMunici: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 428
    Top = 12
    object QrMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMunici: TDataSource
    DataSet = QrMunici
    Left = 456
    Top = 12
  end
  object QrCunsCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'WHERE Codigo IN ('
      '     SELECT Codigo '
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 484
    Top = 12
    object QrCunsCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCunsCadNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsCunsCad: TDataSource
    DataSet = QrCunsCad
    Left = 512
    Top = 12
  end
  object QrListaLograd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM listalograd'
      'ORDER BY Nome')
    Left = 540
    Top = 12
    object QrListaLogradCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaLogradNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 10
    end
  end
  object DsListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 568
    Top = 12
  end
  object CsQry: TdmkCompoStore
    Left = 216
    Top = 4
  end
  object CsEd: TdmkCompoStore
    Left = 244
    Top = 4
  end
  object CsCB: TdmkCompoStore
    Left = 272
    Top = 4
  end
end
