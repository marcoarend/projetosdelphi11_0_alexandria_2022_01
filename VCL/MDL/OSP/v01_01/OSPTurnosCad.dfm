object FmOSPTurnosCad: TFmOSPTurnosCad
  Left = 368
  Top = 194
  Caption = 'OSP-CADAS-001 :: Turnos'
  ClientHeight = 499
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        MaxLength = 60
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 340
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 796
        Top = 16
        Width = 83
        Height = 13
        Caption = 'Tempo (h:mm:ss):'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 888
        Top = 16
        Width = 73
        Height = 13
        Caption = 'Total (Minutos):'
        FocusControl = DBEdit2
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOSPTurnosCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 717
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOSPTurnosCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 796
        Top = 32
        Width = 89
        Height = 21
        DataField = 'Txt_TotHoras'
        DataSource = DsOSPTurnosCad
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 888
        Top = 32
        Width = 101
        Height = 21
        DataField = 'TotMinut'
        DataSource = DsOSPTurnosCad
        TabOrder = 3
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 339
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Turno'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Dia'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtSub: TBitBtn
          Tag = 110
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Horas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtSubClick
        end
      end
    end
    object DBGDds: TDBGrid
      Left = 0
      Top = 65
      Width = 625
      Height = 274
      Align = alLeft
      DataSource = DsOSPTurnosDds
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 126
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Dom'
          Title.Alignment = taCenter
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Seg'
          Title.Alignment = taCenter
          Width = 26
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Ter'
          Title.Alignment = taCenter
          Width = 26
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qua'
          Width = 26
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qui'
          Width = 26
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sex'
          Width = 26
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sab'
          Width = 26
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdDdSem'
          Title.Caption = 'Qtd.Dd'
          Width = 38
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'Txt_UniHoras'
          Title.Caption = 'Un. horas'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UniMinut'
          Title.Caption = 'Un. Minut'
          Width = 40
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'Txt_SumHoras'
          Title.Caption = 'Horas sem.'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SumMinut'
          Title.Caption = 'Minut.Sem.'
          Width = 40
          Visible = True
        end>
    end
    object DBGIts: TDBGrid
      Left = 656
      Top = 65
      Width = 352
      Height = 274
      Align = alRight
      DataSource = DsOSPTurnosIts
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Conta'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Parte turno'
          Width = 92
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Txt_HrIni'
          Title.Alignment = taCenter
          Title.Caption = 'In'#237'cio'
          Width = 52
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Txt_HrFim'
          Title.Alignment = taCenter
          Title.Caption = 'Final'
          Width = 52
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'TXT_SubHoras'
          Title.Alignment = taCenter
          Title.Caption = 'Horas'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SubMinut'
          Title.Caption = 'Minutos'
          Width = 41
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 83
        Height = 32
        Caption = 'Turnos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 83
        Height = 32
        Caption = 'Turnos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 83
        Height = 32
        Caption = 'Turnos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrOSPTurnosCad: TMySQLQuery
    Database = FmTempDB.DBTeste
    BeforeOpen = QrOSPTurnosCadBeforeOpen
    AfterOpen = QrOSPTurnosCadAfterOpen
    BeforeClose = QrOSPTurnosCadBeforeClose
    AfterScroll = QrOSPTurnosCadAfterScroll
    SQL.Strings = (
      'SELECT CONCAT('
      '  FLOOR(TotMinut / 60), ":",'
      '  LPAD(FLOOR(MOD(TotMinut, 60)),2, "0"), ":",'
      '  LPAD(FLOOR(MOD(TotMinut, FLOOR(TotMinut))*60), 2, "0")'
      ') '
      'Txt_TotHoras, otc.*'
      'FROM ospturnoscad otc')
    Left = 92
    Top = 233
    object QrOSPTurnosCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOSPTurnosCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOSPTurnosCadTotMinut: TFloatField
      FieldName = 'TotMinut'
      Required = True
    end
    object QrOSPTurnosCadTxt_TotHoras: TWideStringField
      DisplayWidth = 23
      FieldName = 'Txt_TotHoras'
      Size = 23
    end
  end
  object DsOSPTurnosCad: TDataSource
    DataSet = QrOSPTurnosCad
    Left = 92
    Top = 277
  end
  object QrOSPTurnosIts: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT '
      'DATE_FORMAT(oti.HrIni, "%H:%i:%s") Txt_HrIni, '
      'DATE_FORMAT(oti.HrFim, "%H:%i:%s") Txt_HrFim, '
      ''
      'CONCAT( '
      '  FLOOR(SubMinut / 60), ":", '
      '  LPAD(FLOOR(MOD(SubMinut, 60)),2, "0"), ":", '
      '  LPAD(FLOOR(MOD(SubMinut, FLOOR(SubMinut))*60), 2, "0") '
      ')  TXT_SubHoras,'
      ''
      'oti.* '
      'FROM ospturnosits oti')
    Left = 296
    Top = 229
    object QrOSPTurnosItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOSPTurnosItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOSPTurnosItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrOSPTurnosItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrOSPTurnosItsHrIni: TTimeField
      FieldName = 'HrIni'
      Required = True
    end
    object QrOSPTurnosItsHrFim: TTimeField
      FieldName = 'HrFim'
      Required = True
    end
    object QrOSPTurnosItsSubMinut: TFloatField
      FieldName = 'SubMinut'
      Required = True
    end
    object QrOSPTurnosItsTxt_HrIni: TWideStringField
      FieldName = 'Txt_HrIni'
      Size = 13
    end
    object QrOSPTurnosItsTxt_HrFim: TWideStringField
      FieldName = 'Txt_HrFim'
      Size = 13
    end
    object QrOSPTurnosItsTXT_SubHoras: TWideStringField
      FieldName = 'TXT_SubHoras'
      Size = 23
    end
  end
  object DsOSPTurnosIts: TDataSource
    DataSet = QrOSPTurnosIts
    Left = 296
    Top = 273
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Inclui Dia(s)'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Altera Dia(s)'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Exclui Dia(s)'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrSum: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT  '
      'SUM(IF(TIME_TO_SEC(HrFim) > TIME_TO_SEC(HrIni), '
      'TIME_TO_SEC(HrFim) - TIME_TO_SEC(HrIni), '
      'TIME_TO_SEC(HrFim) - TIME_TO_SEC(HrIni) + 86400)) Segundos'
      'FROM ospturnosits oti '
      'WHERE oti.Codigo=2  '
      ' ')
    Left = 536
    Top = 236
    object QrSumSumMinut: TFloatField
      FieldName = 'SumMinut'
    end
  end
  object PMSub: TPopupMenu
    OnPopup = PMSubPopup
    Left = 556
    Top = 376
    object IncluiHoras1: TMenuItem
      Caption = '&Inclui Horas'
      Enabled = False
      OnClick = IncluiHoras1Click
    end
    object AlteraHoras1: TMenuItem
      Caption = '&Altera Horas'
      Enabled = False
      OnClick = AlteraHoras1Click
    end
    object ExcluiHoras1: TMenuItem
      Caption = '&Exclui Horas'
      Enabled = False
      OnClick = ExcluiHoras1Click
    end
  end
  object QrOSPTurnosDds: TMySQLQuery
    Database = FmTempDB.DBTeste
    BeforeClose = QrOSPTurnosDdsBeforeClose
    AfterScroll = QrOSPTurnosDdsAfterScroll
    SQL.Strings = (
      'SELECT  '
      'CONCAT( '
      '  FLOOR(UniMinut / 60), ":", '
      '  LPAD(FLOOR(MOD(UniMinut, 60)),2, "0"), ":", '
      '  LPAD(FLOOR(MOD(UniMinut, FLOOR(UniMinut))*60), 2, "0") '
      ')  '
      'Txt_UniHoras,  '
      'CONCAT( '
      '  FLOOR(SumMinut / 60), ":", '
      '  LPAD(FLOOR(MOD(SumMinut, 60)),2, "0"), ":", '
      '  LPAD(FLOOR(MOD(SumMinut, FLOOR(SumMinut))*60), 2, "0") '
      ')  '
      'Txt_SumHoras, otd.* '
      'FROM ospturnosdds otd ')
    Left = 188
    Top = 237
    object QrOSPTurnosDdsUniMinut: TFloatField
      FieldName = 'UniMinut'
      Required = True
    end
    object QrOSPTurnosDdsSumMinut: TFloatField
      FieldName = 'SumMinut'
      Required = True
    end
    object QrOSPTurnosDdsDom: TWideStringField
      FieldName = 'Dom'
      Required = True
      Size = 3
    end
    object QrOSPTurnosDdsSeg: TWideStringField
      FieldName = 'Seg'
      Required = True
      Size = 3
    end
    object QrOSPTurnosDdsTer: TWideStringField
      FieldName = 'Ter'
      Required = True
      Size = 3
    end
    object QrOSPTurnosDdsQua: TWideStringField
      FieldName = 'Qua'
      Required = True
      Size = 3
    end
    object QrOSPTurnosDdsQui: TWideStringField
      FieldName = 'Qui'
      Required = True
      Size = 3
    end
    object QrOSPTurnosDdsSex: TWideStringField
      FieldName = 'Sex'
      Required = True
      Size = 3
    end
    object QrOSPTurnosDdsSab: TWideStringField
      FieldName = 'Sab'
      Required = True
      Size = 3
    end
    object QrOSPTurnosDdsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOSPTurnosDdsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOSPTurnosDdsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOSPTurnosDdsQuaisDdSem: TIntegerField
      FieldName = 'QuaisDdSem'
      Required = True
    end
    object QrOSPTurnosDdsQtdDdSem: TIntegerField
      FieldName = 'QtdDdSem'
      Required = True
    end
    object QrOSPTurnosDdsTxt_SumHoras: TWideStringField
      FieldName = 'Txt_SumHoras'
    end
    object QrOSPTurnosDdsTxt_UniHoras: TWideStringField
      FieldName = 'Txt_UniHoras'
    end
  end
  object DsOSPTurnosDds: TDataSource
    DataSet = QrOSPTurnosDds
    Left = 188
    Top = 281
  end
  object QrDds: TMySQLQuery
    Database = FmTempDB.DBTeste
    BeforeClose = QrOSPTurnosDdsBeforeClose
    AfterScroll = QrOSPTurnosDdsAfterScroll
    SQL.Strings = (
      'SELECT '
      '  IF(1 & QuaisDdSem = 1, "Dom", "") Dom, '
      '  IF(2 & QuaisDdSem = 2, "Seg", "") Seg, '
      '  IF(4 & QuaisDdSem = 4, "Ter", "") Ter, '
      '  IF(8 & QuaisDdSem = 8, "Qua", "") Qua, '
      '  IF(16 & QuaisDdSem = 16, "Qui", "") Qui, '
      '  IF(32 & QuaisDdSem = 32, "Sex", "") Sex, '
      '  IF(64 & QuaisDdSem = 64, "Sab", "") Sab, '
      'DATE_FORMAT(otd.UniHoras, "%H:%i:%s") Txt_UniHoras,'
      'DATE_FORMAT(otd.SumHoras, "%H:%i:%s") Txt_SumHoras,'
      'otd.* '
      'FROM ospturnosdds otd ')
    Left = 188
    Top = 329
    object QrDdsQtdDdSem: TIntegerField
      FieldName = 'QtdDdSem'
      Required = True
    end
  end
end
