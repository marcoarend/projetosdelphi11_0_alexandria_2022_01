unit OSPFluxosStg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmOSPFluxosStg = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBEstagio: TdmkDBLookupComboBox;
    EdEstagio: TdmkEditCB;
    Label1: TLabel;
    SBEstagio: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrOSPEstagiosCad: TMySQLQuery;
    DsOSPEstagiosCad: TDataSource;
    QrOSPEstagiosCadCodigo: TIntegerField;
    QrOSPEstagiosCadNome: TWideStringField;
    EdOrdem: TdmkEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBEstagioClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOSPFluxosStg: TFmOSPFluxosStg;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnOSP_Jan, OSPFluxosCad;

{$R *.DFM}

procedure TFmOSPFluxosStg.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Estagio, Ordem: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Estagio        := EdEstagio.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  //
  if MyObjects.FIC(Estagio = 0, EdEstagio, 'Informe o est�gio!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('ospfluxosstg', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ospfluxosstg', False, [
  'Codigo', 'Estagio', 'Ordem'], [
  'Controle'], [
  Codigo, Estagio, Ordem], [
  Controle], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    FmOSPFluxosCad.ReordenaItens(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdEstagio.ValueVariant     := 0;
      CBEstagio.KeyValue         := Null;
      EdOrdem.ValueVariant       := EdOrdem.ValueVariant + 1;
      //
      EdEstagio.SetFocus;
    end else Close;
  end;
end;

procedure TFmOSPFluxosStg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPFluxosStg.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmOSPFluxosStg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrOSPEstagiosCad, Dmod.MyDB);
end;

procedure TFmOSPFluxosStg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPFluxosStg.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmOSPFluxosStg.SBEstagioClick(Sender: TObject);
begin
{
  VAR_CADASTRO := 0;
  OSP_Jan.MostraFormOSP????);
  UMyMod.SetaCodigoPesquisado(EdEstagio, CBEstagio, QrOSPEstagiosCad, VAR_CADASTRO);
}
end;

end.
