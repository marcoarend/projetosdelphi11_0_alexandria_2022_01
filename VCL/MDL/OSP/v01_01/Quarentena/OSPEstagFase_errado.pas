unit OSPEstagFase_errado;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmOSPEstagFase_errado = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBMaqEqu: TdmkDBLookupComboBox;
    EdMaqEqu: TdmkEditCB;
    Label1: TLabel;
    SbMaqEqu: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrOSPMaqEquCad: TMySQLQuery;
    DsOSPMaqEquCad: TDataSource;
    EdNome: TdmkEdit;
    Label7: TLabel;
    EdQtdOperadores: TdmkEdit;
    Label2: TLabel;
    EdTurno: TdmkEditCB;
    CBTurno: TdmkDBLookupComboBox;
    Label4: TLabel;
    SbTurno: TSpeedButton;
    QrOSPTurnosCad: TMySQLQuery;
    DsOSPTurnosCad: TDataSource;
    Label8: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    SbFornecedor: TSpeedButton;
    QrEntidades: TMySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    DsEntidades: TDataSource;
    EdUtiCapInst: TdmkEdit;
    Label9: TLabel;
    RGUndCtrole: TRadioGroup;
    Label10: TLabel;
    EdFatCtrole: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbMaqEquClick(Sender: TObject);
    procedure SbTurnoClick(Sender: TObject);
    procedure SbFornecedorClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOSPEstagFase_errado: TFmOSPEstagFase_errado;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnAppEnums, UnOSP_Jan, UnEntities;

{$R *.DFM}

procedure TFmOSPEstagFase_errado.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, MaqEqu, QtdOperadores, Turno, Fornecedor, UndCtrole: Integer;
  UtiCapInst, FatCtrole: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  MaqEqu         := EdMaqEqu.ValueVariant;
  Nome           := EdNome.ValueVariant;
  QtdOperadores  := EdQtdOperadores.ValueVariant;
  Turno          := EdTurno.ValueVariant;
  UtiCapInst     := EdUtiCapInst.ValueVariant;
  Fornecedor     := EdFornecedor.ValueVariant;
  UndCtrole      := RGUndCtrole.ItemIndex;
  FatCtrole      := EdFatCtrole.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  if MyObjects.FIC(MaqEqu = 0, EdMaqEqu, 'Informe a m�quina / equipamento!') then
    Exit;
  if MyObjects.FIC(QtdOperadores = 0, EdQtdOperadores, 'Informe a quantidade de operadores!') then
    Exit;
  if MyObjects.FIC(Turno = 0, EdTurno, 'Informe o turno!') then
    Exit;
  if MyObjects.FIC(UtiCapInst = 0, EdUtiCapInst, 'Informe o percentual da Utiliza��o da Capacidade Instalada!') then
    Exit;
  if MyObjects.FIC(UndCtrole = 0, RGUndCtrole, 'Informe a unidade de controle!') then
    Exit;
  if UndCtrole > 1 then
    if MyObjects.FIC(FatCtrole = 0, EdFatCtrole, 'Informe o Fator(qtde) de controle!') then
      Exit;
  //
  Controle := UMyMod.BPGS1I32('ospestagfase', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ospestagfase', False, [
  'Codigo', 'MaqEqu', 'Nome',
  'QtdOperadores', 'Turno', 'UtiCapInst',
  'Fornecedor', 'UndCtrole', 'FatCtrole'], [
  'Controle'], [
  Codigo, MaqEqu, Nome,
  QtdOperadores, Turno, UtiCapInst,
  Fornecedor, UndCtrole, FatCtrole], [
  Controle], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType               := stIns;
      EdControle.ValueVariant       := 0;
      EdMaqEqu.ValueVariant         := 0;
      CBMaqEqu.KeyValue             := Null;
      EdTurno.ValueVariant          := 0;
      CBTurno.KeyValue              := Null;
      EdFornecedor.ValueVariant     := 0;
      CBFornecedor.KeyValue         := Null;
      EdNome.ValueVariant           := '';
      EdQtdOperadores.ValueVariant  := 0;
      EdUtiCapInst.ValueVariant     := 0;
      RGUndCtrole.ItemIndex         := 0;
      EdFatCtrole.ValueVariant      := 0;
      //
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmOSPEstagFase_errado.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPEstagFase_errado.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmOSPEstagFase_errado.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrOSPMaqEquCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOSPTurnosCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  //
  MyObjects.ConfiguraRadioGroup(RGUndCtrole, sUndCtrleProd, (*Colunas*)5, (*Default*)0);
end;

procedure TFmOSPEstagFase_errado.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPEstagFase_errado.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmOSPEstagFase_errado.SbFornecedorClick(Sender: TObject);
begin
  Entities.CadastroESelecaoDeEntidade2(QrEntidades, EdFornecedor, CBFornecedor);
end;

procedure TFmOSPEstagFase_errado.SbMaqEquClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OSP_Jan.MostraFormOSPMaqEquCad();
  UMyMod.SetaCodigoPesquisado(EdMaqEqu, CBMaqEqu, QrOSPMaqEquCad, VAR_CADASTRO);
end;

procedure TFmOSPEstagFase_errado.SbTurnoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OSP_Jan.MostraFormOSPTurnosCad(EdTurno.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdTurno, CBTurno, QrOSPTurnosCad, VAR_CADASTRO);
end;

end.
