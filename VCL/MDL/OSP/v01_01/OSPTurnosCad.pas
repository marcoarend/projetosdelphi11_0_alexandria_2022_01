unit OSPTurnosCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmOSPTurnosCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOSPTurnosCad: TMySQLQuery;
    DsOSPTurnosCad: TDataSource;
    QrOSPTurnosIts: TMySQLQuery;
    DsOSPTurnosIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DBGDds: TDBGrid;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    QrSum: TMySQLQuery;
    BtSub: TBitBtn;
    PMSub: TPopupMenu;
    DBGIts: TDBGrid;
    IncluiHoras1: TMenuItem;
    AlteraHoras1: TMenuItem;
    ExcluiHoras1: TMenuItem;
    QrOSPTurnosDds: TMySQLQuery;
    DsOSPTurnosDds: TDataSource;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    QrDds: TMySQLQuery;
    QrDdsQtdDdSem: TIntegerField;
    QrOSPTurnosCadCodigo: TIntegerField;
    QrOSPTurnosCadNome: TWideStringField;
    QrOSPTurnosCadTotMinut: TFloatField;
    QrOSPTurnosDdsUniMinut: TFloatField;
    QrOSPTurnosDdsSumMinut: TFloatField;
    QrOSPTurnosDdsDom: TWideStringField;
    QrOSPTurnosDdsSeg: TWideStringField;
    QrOSPTurnosDdsTer: TWideStringField;
    QrOSPTurnosDdsQua: TWideStringField;
    QrOSPTurnosDdsQui: TWideStringField;
    QrOSPTurnosDdsSex: TWideStringField;
    QrOSPTurnosDdsSab: TWideStringField;
    QrOSPTurnosDdsCodigo: TIntegerField;
    QrOSPTurnosDdsControle: TIntegerField;
    QrOSPTurnosDdsNome: TWideStringField;
    QrOSPTurnosDdsQuaisDdSem: TIntegerField;
    QrOSPTurnosDdsQtdDdSem: TIntegerField;
    QrOSPTurnosItsCodigo: TIntegerField;
    QrOSPTurnosItsControle: TIntegerField;
    QrOSPTurnosItsConta: TIntegerField;
    QrOSPTurnosItsNome: TWideStringField;
    QrOSPTurnosItsHrIni: TTimeField;
    QrOSPTurnosItsHrFim: TTimeField;
    QrOSPTurnosItsSubMinut: TFloatField;
    QrOSPTurnosItsTxt_HrIni: TWideStringField;
    QrOSPTurnosItsTxt_HrFim: TWideStringField;
    QrSumSumMinut: TFloatField;
    QrOSPTurnosCadTxt_TotHoras: TWideStringField;
    QrOSPTurnosDdsTxt_SumHoras: TWideStringField;
    QrOSPTurnosDdsTxt_UniHoras: TWideStringField;
    QrOSPTurnosItsTXT_SubHoras: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOSPTurnosCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOSPTurnosCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOSPTurnosCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrOSPTurnosCadBeforeClose(DataSet: TDataSet);
    procedure BtSubClick(Sender: TObject);
    procedure IncluiHoras1Click(Sender: TObject);
    procedure AlteraHoras1Click(Sender: TObject);
    procedure ExcluiHoras1Click(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure QrOSPTurnosDdsAfterScroll(DataSet: TDataSet);
    procedure QrOSPTurnosDdsBeforeClose(DataSet: TDataSet);
    procedure PMSubPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraOSPTurnosDds(SQLType: TSQLType);
    procedure MostraOSPTurnosIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure DefineTotalTempoTurno(Codigo, Controle: Integer);
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenOSPTurnosDds(Controle: Integer);
    procedure ReopenOSPTurnosIts(Conta: Integer);

  end;

var
  FmOSPTurnosCad: TFmOSPTurnosCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, OSPTurnosDds, OSPTurnosIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOSPTurnosCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOSPTurnosCad.MostraOSPTurnosDds(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSPTurnosDds, FmOSPTurnosDds, afmoNegarComAviso) then
  begin
    FmOSPTurnosDds.ImgTipo.SQLType := SQLType;
    FmOSPTurnosDds.FQrCab := QrOSPTurnosCad;
    FmOSPTurnosDds.FDsCab := DsOSPTurnosCad;
    FmOSPTurnosDds.FQrIts := QrOSPTurnosDds;
    if SQLType = stIns then

    else
    begin
      FmOSPTurnosDds.EdControle.ValueVariant := QrOSPTurnosDdsControle.Value;
      //
      FmOSPTurnosDds.EdNome.Text          := QrOSPTurnosDdsNome.Value;
      FmOSPTurnosDds.CGQuaisDdSem.Value   := QrOSPTurnosDdsQuaisDdSem.Value;
    end;
    FmOSPTurnosDds.ShowModal;
    FmOSPTurnosDds.Destroy;
  end;
end;

procedure TFmOSPTurnosCad.MostraOSPTurnosIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSPTurnosIts, FmOSPTurnosIts, afmoNegarComAviso) then
  begin
    FmOSPTurnosIts.ImgTipo.SQLType := SQLType;
    FmOSPTurnosIts.FQrCab := QrOSPTurnosCad;
    FmOSPTurnosIts.FDsCab := DsOSPTurnosCad;
    FmOSPTurnosIts.FQrDds := QrOSPTurnosDds;
    FmOSPTurnosIts.FDsDds := DsOSPTurnosDds;
    FmOSPTurnosIts.FQrIts := QrOSPTurnosIts;
    if SQLType = stIns then

    else
    begin
      FmOSPTurnosIts.EdConta.ValueVariant := QrOSPTurnosItsConta.Value;
      //
      FmOSPTurnosIts.EdNome.Text          := QrOSPTurnosItsNome.Value;
      FmOSPTurnosIts.EdHrIni.ValueVariant := QrOSPTurnosItsHrIni.Value;
      FmOSPTurnosIts.EdHrFim.ValueVariant := QrOSPTurnosItsHrFim.Value;
    end;
    FmOSPTurnosIts.ShowModal;
    FmOSPTurnosIts.Destroy;
  end;
end;

procedure TFmOSPTurnosCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOSPTurnosCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOSPTurnosCad, QrOSPTurnosIts);
end;

procedure TFmOSPTurnosCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOSPTurnosCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOSPTurnosDds);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrOSPTurnosDds);
end;

procedure TFmOSPTurnosCad.PMSubPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiHoras1, QrOSPTurnosDds);
  MyObjects.HabilitaMenuItemItsUpd(AlteraHoras1, QrOSPTurnosIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiHoras1, QrOSPTurnosIts);
end;

procedure TFmOSPTurnosCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOSPTurnosCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOSPTurnosCad.DefParams;
begin
  VAR_GOTOTABELA := 'ospturnoscad';
  VAR_GOTOMYSQLTABLE := QrOSPTurnosCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  //VAR_SQLx.Add('SELECT DATE_FORMAT(otc.TotHoras, "%H:%i:%s") TotHoras_TXT, otc.*');
  VAR_SQLx.Add('SELECT CONCAT(');
  VAR_SQLx.Add('  FLOOR(TotMinut / 60), ":",');
  VAR_SQLx.Add('  LPAD(FLOOR(MOD(TotMinut, 60)),2, "0"), ":",');
  VAR_SQLx.Add('  LPAD(FLOOR((MOD(TotMinut, FLOOR(TotMinut))+0.001)*60), 2, "0")');
  VAR_SQLx.Add(') Txt_TotHoras, otc.*');
  VAR_SQLx.Add('FROM ospturnoscad otc');
  VAR_SQLx.Add('WHERE otc.Codigo > 0');

  //
  VAR_SQL1.Add('AND otc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND otc.Nome Like :P0');
  //
end;

procedure TFmOSPTurnosCad.ExcluiHoras1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'OSPTurnosIts', 'Conta', QrOSPTurnosItsConta.Value, Dmod.MyDB) = ID_YES then
  begin
    DefineTotalTempoTurno(QrOSPTurnosCadCodigo.Value, QrOSPTurnosDdsControle.Value);
    Conta := GOTOy.LocalizaPriorNextIntQr(QrOSPTurnosIts,
      QrOSPTurnosItsConta, QrOSPTurnosItsConta.Value);
    ReopenOSPTurnosIts(Conta);
  end;
end;

procedure TFmOSPTurnosCad.IncluiHoras1Click(Sender: TObject);
begin
  MostraOSPTurnosIts(stIns);
end;

procedure TFmOSPTurnosCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOSPTurnosCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOSPTurnosCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOSPTurnosCad.ItsAltera1Click(Sender: TObject);
begin
  MostraOSPTurnosDds(stUpd);
end;

procedure TFmOSPTurnosCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
(*
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'OSPTurnosIts', 'Controle', QrOSPTurnosItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    DefineTotalTempoTurno(QrOSPTurnosCadCodigo.Value, QrOSPTurnosDdsControle.Value);
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOSPTurnosIts,
      QrOSPTurnosItsControle, QrOSPTurnosItsControle.Value);
    ReopenOSPTurnosIts(Controle);
  end;
*)
end;

procedure TFmOSPTurnosCad.ItsInclui1Click(Sender: TObject);
begin
  MostraOSPTurnosDds(stIns);
end;

procedure TFmOSPTurnosCad.ReopenOSPTurnosDds(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPTurnosDds, Dmod.MyDB, [
  'SELECT ',
  '  IF(1 & QuaisDdSem = 1, "Dom", "") Dom, ',
  '  IF(2 & QuaisDdSem = 2, "Seg", "") Seg, ',
  '  IF(4 & QuaisDdSem = 4, "Ter", "") Ter, ',
  '  IF(8 & QuaisDdSem = 8, "Qua", "") Qua, ',
  '  IF(16 & QuaisDdSem = 16, "Qui", "") Qui, ',
  '  IF(32 & QuaisDdSem = 32, "Sex", "") Sex, ',
  '  IF(64 & QuaisDdSem = 64, "Sab", "") Sab, ',
  //
  'CONCAT( ',
  '  FLOOR(UniMinut / 60), ":", ',
  '  LPAD(FLOOR(MOD(UniMinut, 60)),2, "0"), ":", ',
  '  LPAD(FLOOR((MOD(UniMinut, FLOOR(UniMinut))+0.001)*60), 2, "0") ',
  ')  ',
  'Txt_UniHoras,  ',
  //
  'CONCAT( ',
  '  FLOOR(SumMinut / 60), ":", ',
  '  LPAD(FLOOR(MOD(SumMinut, 60)),2, "0"), ":", ',
  '  LPAD(FLOOR((MOD(SumMinut, FLOOR(SumMinut))+0.001)*60), 2, "0") ',
  ')  ',
  'Txt_SumHoras, ',
  //
  'otd.* ',
  'FROM ospturnosdds otd ',
  'WHERE otd.Codigo=' + Geral.FF0(QrOSPTurnosCadCodigo.Value),
  '']);
  //
  QrOSPTurnosDds.Locate('Controle', Controle, []);
end;

procedure TFmOSPTurnosCad.ReopenOSPTurnosIts(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPTurnosIts, Dmod.MyDB, [
  'SELECT ',
  'DATE_FORMAT(oti.HrIni, "%H:%i:%s") Txt_HrIni, ',
  'DATE_FORMAT(oti.HrFim, "%H:%i:%s") Txt_HrFim, ',
  '',
  'CONCAT( ',
  '  FLOOR(SubMinut / 60), ":", ',
  '  LPAD(FLOOR(MOD(SubMinut, 60)),2, "0"), ":", ',
  '  LPAD(FLOOR((MOD(SubMinut, FLOOR(SubMinut))+0.001)*60), 2, "0") ',
  ')  TXT_SubHoras,',
  '',
  'oti.* ',
  'FROM ospturnosits oti',
  'WHERE oti.Controle=' + Geral.FF0(QrOSPTurnosDdsControle.Value),
  '']);
  //
  QrOSPTurnosIts.Locate('Conta', Conta, []);
end;


procedure TFmOSPTurnosCad.DefineONomeDoForm;
begin
end;

procedure TFmOSPTurnosCad.DefineTotalTempoTurno(Codigo, Controle: Integer);
var
  //TotHoras, UniHoras, SumHoras: String;
  TotMinut, UniMinut, SumMinut: Double;
  Total: TDateTime;
  QtdDdSem: Integer;
begin
  // Soma do tempo do Controle (Dias da semana)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(SubMinut) SumMinut ',
  'FROM ospturnosits oti ',
  'WHERE Controle=' + Geral.FF0(Controle),
  EmptyStr]);
  TotMinut := QrSumSumMinut.Value;
  //TotHoras := Geral.FDT(QrSumSegundos.Value / 86400, 109);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDds, Dmod.MyDB, [
  'SELECT  ',
  'QtdDdSem',
  'FROM ospturnosdds oti ',
  'WHERE Controle=' + Geral.FF0(Controle),
  EmptyStr]);
  QtdDdSem := QrDdsQtdDdSem.Value;
  UniMinut := TotMinut;
  SumMinut := UniMinut * QtdDdSem;
  if QrSumSumMinut.Value > 0 then
  begin
    //UniHoras := Geral.FDT(QrSumSegundos.Value / 86400, 109);
    //SumHoras := Geral.FDT(QrSumSegundos.Value * QtdDdSem / 86400, 109);
  end else
  begin
    //UniHoras := '00:00:00';
    //SumHoras := '00:00:00';
  end;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ospturnosdds', False, [
  'UniMinut', 'SumMinut'], [
  'Controle'], [
  UniMinut, SumMinut], [
  Controle], True);
  //

  // Soma do tempo do Codigo (Todo cadastro do turno)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT  ',
  'SUM(SumMinut) SumMinut ',
  'FROM ospturnosdds oti ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  EmptyStr]);
  TotMinut := QrSumSumMinut.Value;
(*
  if TotMinut = 0 then
    TotHoras := '00:00:00'
  else
    TotHoras := Geral.FDT(QrSumSegundos.Value / 86400, 109);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ospturnoscad', False, [
  'TotHoras', 'TotMinut'], ['Codigo'], [
  TotHoras, TotMinut], [Codigo], True) then
    LocCod(Codigo, Codigo);
*)
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ospturnoscad', False, [
  'TotMinut'], ['Codigo'], [
  TotMinut], [Codigo], True) then
    LocCod(Codigo, Codigo);

end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOSPTurnosCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOSPTurnosCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOSPTurnosCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOSPTurnosCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOSPTurnosCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPTurnosCad.BtSubClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSub, BtSub);
end;

procedure TFmOSPTurnosCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOSPTurnosCadCodigo.Value;
  Close;
end;

procedure TFmOSPTurnosCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOSPTurnosCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ospturnoscad');
end;

procedure TFmOSPTurnosCad.BtConfirmaClick(Sender: TObject);
var
  Nome(*, TotHoras, TotMinut*): String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //Tempo          := ;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ospturnoscad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ospturnoscad', False, [
  'Nome'(*, 'TotHoras', 'TotMinut'*)], [
  'Codigo'], [
  Nome(*, TotHoras, TotMinut*)], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOSPTurnosCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ospturnoscad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ospturnoscad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmOSPTurnosCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOSPTurnosCad.AlteraHoras1Click(Sender: TObject);
begin
  MostraOSPTurnosIts(stUpd);
end;

procedure TFmOSPTurnosCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOSPTurnosCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  DBGIts.Align    := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmOSPTurnosCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOSPTurnosCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOSPTurnosCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOSPTurnosCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOSPTurnosCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOSPTurnosCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOSPTurnosCad.QrOSPTurnosCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOSPTurnosCad.QrOSPTurnosCadAfterScroll(DataSet: TDataSet);
begin
  ReopenOSPTurnosDds(0);
end;

procedure TFmOSPTurnosCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOSPTurnosCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOSPTurnosCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOSPTurnosCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ospturnoscad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOSPTurnosCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPTurnosCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOSPTurnosCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ospturnoscad');
end;

procedure TFmOSPTurnosCad.QrOSPTurnosCadBeforeClose(
  DataSet: TDataSet);
begin
  QrOSPTurnosDds.Close;
end;

procedure TFmOSPTurnosCad.QrOSPTurnosCadBeforeOpen(DataSet: TDataSet);
begin
  QrOSPTurnosCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmOSPTurnosCad.QrOSPTurnosDdsAfterScroll(DataSet: TDataSet);
begin
  ReopenOSPTurnosIts(0);
end;

procedure TFmOSPTurnosCad.QrOSPTurnosDdsBeforeClose(DataSet: TDataSet);
begin
  QrOSPTurnosIts.Close;
end;

{

SELECT BIT_COUNT(59) Itens,
CONCAT(
  IF(1 & 59 = 1, "1,", ""),
  IF(2 & 59 = 2, "2,", ""),
  IF(4 & 59 = 4, "4,", ""),
  IF(8 & 59 = 8, "8,", ""),
  IF(16 & 59 = 16, "16,", ""),
  IF(32 & 59 = 32, "32,", ""),
  IF(64 & 59 = 64, "64,", "")
) Contidos

}

end.

