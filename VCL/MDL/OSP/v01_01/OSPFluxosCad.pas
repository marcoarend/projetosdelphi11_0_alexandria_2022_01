unit OSPFluxosCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmOSPFluxosCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOSPFluxosCad: TMySQLQuery;
    DsOSPFluxosCad: TDataSource;
    QrOSPFluxosFse: TMySQLQuery;
    DsOSPFluxosFse: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrOSPFluxosCadCodigo: TIntegerField;
    QrOSPFluxosCadNome: TWideStringField;
    QrOSPFluxosFseNO_Fase: TWideStringField;
    QrOSPFluxosFseCodigo: TIntegerField;
    QrOSPFluxosFseControle: TIntegerField;
    QrOSPFluxosFseFase: TIntegerField;
    QrOSPFluxosFseOrdem: TIntegerField;
    BitBtn1: TBitBtn;
    QrOSPFluxosFseNO_ESTAGIO: TStringField;
    QrOSPFluxosFseLk: TIntegerField;
    QrOSPFluxosFseDataCad: TDateField;
    QrOSPFluxosFseDataAlt: TDateField;
    QrOSPFluxosFseUserCad: TIntegerField;
    QrOSPFluxosFseUserAlt: TIntegerField;
    QrOSPFluxosFseAlterWeb: TSmallintField;
    QrOSPFluxosFseAWServerID: TIntegerField;
    QrOSPFluxosFseAWStatSinc: TSmallintField;
    QrOSPFluxosFseAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOSPFluxosCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOSPFluxosCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOSPFluxosCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrOSPFluxosCadBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraOSPFluxosFse(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenOSPFluxosFse(Controle: Integer);
    procedure ReordenaItens(Controle: Integer);


  end;

var
  FmOSPFluxosCad: TFmOSPFluxosCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, OSPFluxosFse;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOSPFluxosCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOSPFluxosCad.MostraOSPFluxosFse(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSPFluxosFse, FmOSPFluxosFse, afmoNegarComAviso) then
  begin
    FmOSPFluxosFse.ImgTipo.SQLType := SQLType;
    FmOSPFluxosFse.FQrCab := QrOSPFluxosCad;
    FmOSPFluxosFse.FDsCab := DsOSPFluxosCad;
    FmOSPFluxosFse.FQrIts := QrOSPFluxosFse;
    if SQLType = stIns then
    begin
      FmOSPFluxosFse.EdOrdem.ValueVariant   := QrOSPFluxosFse.RecNo;
    end else
    begin
      FmOSPFluxosFse.EdControle.ValueVariant := QrOSPFluxosFseControle.Value;
      //
      FmOSPFluxosFse.EdFase.ValueVariant    := QrOSPFluxosFseFase.Value;
      FmOSPFluxosFse.CBFase.KeyValue        := QrOSPFluxosFseFase.Value;
      FmOSPFluxosFse.EdOrdem.ValueVariant   := QrOSPFluxosFseOrdem.Value;
    end;
    FmOSPFluxosFse.ShowModal;
    FmOSPFluxosFse.Destroy;
  end;
end;

procedure TFmOSPFluxosCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOSPFluxosCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOSPFluxosCad, QrOSPFluxosFse);
end;

procedure TFmOSPFluxosCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOSPFluxosCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOSPFluxosFse);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrOSPFluxosFse);
end;

procedure TFmOSPFluxosCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOSPFluxosCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOSPFluxosCad.DefParams;
begin
  VAR_GOTOTABELA := 'ospfluxoscad';
  VAR_GOTOMYSQLTABLE := QrOSPFluxosCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM ospfluxoscad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmOSPFluxosCad.ItsAltera1Click(Sender: TObject);
begin
  MostraOSPFluxosFse(stUpd);
end;

procedure TFmOSPFluxosCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOSPFluxosCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOSPFluxosCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOSPFluxosCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'OSPFluxosFse', 'Controle', QrOSPFluxosFseControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOSPFluxosFse,
      QrOSPFluxosFseControle, QrOSPFluxosFseControle.Value);
    ReopenOSPFluxosFse(Controle);
  end;
}
end;

procedure TFmOSPFluxosCad.ReopenOSPFluxosFse(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPFluxosFse, Dmod.MyDB, [
  'SELECT est.Nome NO_ESTAGIO,  ',
  'oef.Nome NO_Fase, fse.*  ',
  'FROM ospfluxosfse fse ',
  'WHERE fse.Codigo=' + Geral.FF0(QrOSPFluxosCadCodigo.Value),
  'LEFT JOIN ospestagfase oef ON oef.Codigo=fse.Fase ',
  'LEFT JOIN ospestagstor est ON oef.Estagio=est.Codigo ',
  'ORDER BY fse.Ordem, fse.Controle ']);
(*
  'SELECT oef.Nome NO_Fase, fse.*  ',
  'FROM ospfluxosfse fse ',
  'LEFT JOIN ospestagfase oef ON oef.Codigo=fse.Fase ',
  'WHERE fse.Codigo=' + Geral.FF0(QrOSPFluxosCadCodigo.Value),
  'ORDER BY Ordem, Controle']);
*)
  //
  QrOSPFluxosFse.Locate('Controle', Controle, []);
end;


procedure TFmOSPFluxosCad.ReordenaItens(Controle: Integer);
begin
  Screen.Cursor := crHourGlass;
  try
    QrOSPFluxosFse.DisableControls;
    QrOSPFluxosFse.First;
    while not QrOSPFluxosFse.Eof do
    begin
      Dmod.MyDB.Execute('UPDATE ospfluxosfse SET Ordem=' +
      Geral.FF0(QrOSPFluxosFse.RecNo) + ' WHERE Controle=' +
      Geral.FF0(QrOSPFluxosFseControle.Value));
      //
      QrOSPFluxosFse.Next;
    end;
    ReopenOSPFluxosFse(Controle);
  finally
    QrOSPFluxosFse.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSPFluxosCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOSPFluxosCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOSPFluxosCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOSPFluxosCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOSPFluxosCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOSPFluxosCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPFluxosCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOSPFluxosCadCodigo.Value;
  Close;
end;

procedure TFmOSPFluxosCad.ItsInclui1Click(Sender: TObject);
begin
  MostraOSPFluxosFse(stIns);
end;

procedure TFmOSPFluxosCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOSPFluxosCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ospfluxoscad');
end;

procedure TFmOSPFluxosCad.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Codigo := UMyMod.BPGS1I32('ospfluxoscad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ospfluxoscad', False, [
  'Nome'], [
  'Codigo'], [
  Nome], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOSPFluxosCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ospfluxoscad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ospfluxoscad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmOSPFluxosCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOSPFluxosCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOSPFluxosCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmOSPFluxosCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOSPFluxosCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOSPFluxosCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOSPFluxosCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOSPFluxosCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOSPFluxosCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOSPFluxosCad.QrOSPFluxosCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOSPFluxosCad.QrOSPFluxosCadAfterScroll(DataSet: TDataSet);
begin
  ReopenOSPFluxosFse(0);
end;

procedure TFmOSPFluxosCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOSPFluxosCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOSPFluxosCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOSPFluxosCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ospfluxoscad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOSPFluxosCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPFluxosCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOSPFluxosCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ospfluxoscad');
end;

procedure TFmOSPFluxosCad.QrOSPFluxosCadBeforeClose(
  DataSet: TDataSet);
begin
  QrOSPFluxosFse.Close;
end;

procedure TFmOSPFluxosCad.QrOSPFluxosCadBeforeOpen(DataSet: TDataSet);
begin
  QrOSPFluxosCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

