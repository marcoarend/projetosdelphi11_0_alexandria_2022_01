object FmOSPEstagFase: TFmOSPEstagFase
  Left = 368
  Top = 194
  Caption = 'OSP-CADAS-007 :: Cadastro de Fase em Est'#225'gio de Produ'#231#227'o'
  ClientHeight = 527
  ClientWidth = 786
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 786
    Height = 431
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object GBDados: TGroupBox
      Left = 0
      Top = 101
      Width = 786
      Height = 188
      Align = alTop
      Caption = ' Dados do item: '
      TabOrder = 2
      object Label14: TLabel
        Left = 12
        Top = 16
        Width = 116
        Height = 13
        Caption = 'M'#225'quina / equipamento:'
      end
      object Label15: TLabel
        Left = 588
        Top = 16
        Width = 161
        Height = 13
        Caption = 'Nome da m'#225'quina / equipamento:'
      end
      object Label16: TLabel
        Left = 12
        Top = 56
        Width = 94
        Height = 13
        Caption = 'Qtd. de operadores:'
      end
      object Label17: TLabel
        Left = 112
        Top = 56
        Width = 31
        Height = 13
        Caption = 'Turno:'
      end
      object Label18: TLabel
        Left = 12
        Top = 96
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label19: TLabel
        Left = 720
        Top = 56
        Width = 36
        Height = 13
        Caption = '% *UCI:'
      end
      object Label20: TLabel
        Left = 708
        Top = 140
        Width = 45
        Height = 13
        Caption = 'Fator UC:'
      end
      object DBEdit1: TDBEdit
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        DataField = 'MaqEqu'
        DataSource = DsOSPEstagFase
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 68
        Top = 32
        Width = 517
        Height = 21
        DataField = 'NO_MaqEqu'
        DataSource = DsOSPEstagFase
        TabOrder = 1
      end
      object DBEdit4: TDBEdit
        Left = 12
        Top = 72
        Width = 97
        Height = 21
        DataField = 'QtdOperadores'
        DataSource = DsOSPEstagFase
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 112
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Turno'
        DataSource = DsOSPEstagFase
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 720
        Top = 72
        Width = 53
        Height = 21
        DataField = 'UtiCapInst'
        DataSource = DsOSPEstagFase
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 172
        Top = 72
        Width = 545
        Height = 21
        DataField = 'NO_Turno'
        DataSource = DsOSPEstagFase
        TabOrder = 5
      end
      object DBEdit8: TDBEdit
        Left = 12
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Fornecedor'
        DataSource = DsOSPEstagFase
        TabOrder = 7
      end
      object DBEdit9: TDBEdit
        Left = 68
        Top = 112
        Width = 705
        Height = 21
        DataField = 'NO_Fornecedor'
        DataSource = DsOSPEstagFase
        TabOrder = 8
      end
      object DBRGUndCtrole: TDBRadioGroup
        Left = 12
        Top = 136
        Width = 689
        Height = 49
        Caption = ' Unidade de Controle: '
        DataField = 'UndCtrole'
        DataSource = DsOSPEstagFase
        Items.Strings = (
          'UnAppEnums.sUndCtrleProd....')
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 708
        Top = 156
        Width = 65
        Height = 21
        DataField = 'FatCtrole'
        DataSource = DsOSPEstagFase
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 588
        Top = 32
        Width = 185
        Height = 21
        DataField = 'MqEqEtiqt'
        DataSource = DsOSPEstagFase
        TabOrder = 2
      end
    end
    object GBTitulo: TGroupBox
      Left = 0
      Top = 0
      Width = 786
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label6: TLabel
        Left = 76
        Top = 16
        Width = 89
        Height = 13
        Caption = 'Descri'#231#227'o da fase:'
        FocusControl = DBEdCodigo
      end
      object Label13: TLabel
        Left = 16
        Top = 56
        Width = 38
        Height = 13
        Caption = 'Est'#225'gio:'
        FocusControl = dmkDBEdit1
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOSPEstagFase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit3: TDBEdit
        Left = 76
        Top = 32
        Width = 701
        Height = 21
        DataField = 'Nome'
        DataSource = DsOSPEstagFase
        TabOrder = 1
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Estagio'
        DataSource = DsOSPEstagFase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit12: TDBEdit
        Left = 76
        Top = 72
        Width = 701
        Height = 21
        DataField = 'NO_Estagio'
        DataSource = DsOSPEstagFase
        TabOrder = 3
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 367
      Width = 786
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 90
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 264
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 786
    Height = 431
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object GBTutul2: TGroupBox
      Left = 0
      Top = 0
      Width = 786
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 89
        Height = 13
        Caption = 'Descri'#231#227'o da fase:'
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 38
        Height = 13
        Caption = 'Est'#225'gio:'
      end
      object SbEstagio: TSpeedButton
        Left = 756
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbEstagioClick
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 697
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEstagio: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Estagio'
        UpdCampo = 'Estagio'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEstagio
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEstagio: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 681
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOSPEstagStor
        TabOrder = 3
        dmkEditCB = EdEstagio
        QryCampo = 'Estagio'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 368
      Width = 786
      Height = 63
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 647
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 101
      Width = 786
      Height = 193
      Align = alTop
      Caption = ' Dados do item: '
      TabOrder = 1
      object Label3: TLabel
        Left = 12
        Top = 16
        Width = 116
        Height = 13
        Caption = 'M'#225'quina / equipamento:'
      end
      object Label4: TLabel
        Left = 568
        Top = 16
        Width = 194
        Height = 13
        Caption = 'Identifica'#231#227'o da m'#225'quina / equipamento:'
      end
      object Label5: TLabel
        Left = 12
        Top = 56
        Width = 94
        Height = 13
        Caption = 'Qtd. de operadores:'
      end
      object Label8: TLabel
        Left = 112
        Top = 56
        Width = 31
        Height = 13
        Caption = 'Turno:'
      end
      object SbTurno: TSpeedButton
        Left = 696
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbTurnoClick
      end
      object Label10: TLabel
        Left = 12
        Top = 96
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object SbFornecedor: TSpeedButton
        Left = 752
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbFornecedorClick
      end
      object Label11: TLabel
        Left = 720
        Top = 56
        Width = 36
        Height = 13
        Caption = '% *UCI:'
      end
      object Label12: TLabel
        Left = 708
        Top = 140
        Width = 45
        Height = 13
        Caption = 'Fator UC:'
      end
      object SpeedButton8: TSpeedButton
        Left = 544
        Top = 32
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbMaqEquClick
      end
      object EdQtdOperadores: TdmkEdit
        Left = 12
        Top = 72
        Width = 97
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'QtdOperadores'
        UpdCampo = 'QtdOperadores'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdTurno: TdmkEditCB
        Left = 112
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Turno'
        UpdCampo = 'Turno'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTurno
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTurno: TdmkDBLookupComboBox
        Left = 168
        Top = 72
        Width = 525
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOSPTurnosCad
        TabOrder = 5
        dmkEditCB = EdTurno
        QryCampo = 'Turno'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFornecedor: TdmkEditCB
        Left = 12
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fornecedor'
        UpdCampo = 'Fornecedor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecedor: TdmkDBLookupComboBox
        Left = 68
        Top = 112
        Width = 681
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsEntidades
        TabOrder = 8
        dmkEditCB = EdFornecedor
        QryCampo = 'Fornecedor'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdUtiCapInst: TdmkEdit
        Left = 720
        Top = 72
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'UtiCapInst'
        UpdCampo = 'UtiCapInst'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object RGUndCtrole: TdmkRadioGroup
        Left = 12
        Top = 136
        Width = 689
        Height = 49
        Caption = ' Unidade de Controle: '
        Items.Strings = (
          'UnAppEnums.sUndCtrleProd....')
        TabOrder = 9
        QryCampo = 'UndCtrole'
        UpdCampo = 'UndCtrole'
        UpdType = utYes
        OldValor = 0
      end
      object EdFatCtrole: TdmkEdit
        Left = 708
        Top = 156
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'FatCtrole'
        UpdCampo = 'FatCtrole'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMaqEqu: TdmkEditCB
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MaqEqu'
        UpdCampo = 'MaqEqu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMaqEqu
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMaqEqu: TdmkDBLookupComboBox
        Left = 68
        Top = 32
        Width = 473
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOSPMaqEquCad
        TabOrder = 1
        dmkEditCB = EdMaqEqu
        QryCampo = 'MaqEqu'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdMqEqEtiqt: TdmkEdit
        Left = 568
        Top = 32
        Width = 205
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'MqEqEtiqt'
        UpdCampo = 'MqEqEtiqt'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 786
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 738
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 522
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 523
        Height = 32
        Caption = 'Cadastro de Fase em Est'#225'gio de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 523
        Height = 32
        Caption = 'Cadastro de Fase em Est'#225'gio de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 523
        Height = 32
        Caption = 'Cadastro de Fase em Est'#225'gio de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 786
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 782
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 394
        Height = 16
        Caption = 
          '% *UCI: Percentual esperado de utiliza'#231#227'o da capacidade instalad' +
          'a.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 394
        Height = 16
        Caption = 
          '% *UCI: Percentual esperado de utiliza'#231#227'o da capacidade instalad' +
          'a.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrOSPEstagFase: TMySQLQuery
    Database = FmTempDB.DBTeste
    BeforeOpen = QrOSPEstagFaseBeforeOpen
    AfterOpen = QrOSPEstagFaseAfterOpen
    SQL.Strings = (
      'SELECT mec.Nome NO_MaqEqu, otc.Nome NO_Turno, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_Fornecedor,'
      'oes.Nome NO_Estagio, oef.*'
      'FROM ospestagfase oef'
      'LEFT JOIN ospmaqequcad mec ON mec.Codigo=oef.MaqEqu'
      'LEFT JOIN ospturnoscad otc ON otc.Codigo=oef.Turno'
      'LEFT JOIN entidades    frn ON frn.Codigo=oef.Fornecedor'
      'LEFT JOIN ospestagstor oes ON oes.Codigo=oef.Estagio'
      'WHERE oef.Codigo > 0')
    Left = 64
    Top = 64
    object QrOSPEstagFaseCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOSPEstagFaseMaqEqu: TIntegerField
      FieldName = 'MaqEqu'
      Required = True
    end
    object QrOSPEstagFaseNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOSPEstagFaseQtdOperadores: TIntegerField
      FieldName = 'QtdOperadores'
      Required = True
    end
    object QrOSPEstagFaseTurno: TIntegerField
      FieldName = 'Turno'
      Required = True
    end
    object QrOSPEstagFaseUtiCapInst: TFloatField
      FieldName = 'UtiCapInst'
      Required = True
    end
    object QrOSPEstagFaseFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Required = True
    end
    object QrOSPEstagFaseUndCtrole: TSmallintField
      FieldName = 'UndCtrole'
      Required = True
    end
    object QrOSPEstagFaseFatCtrole: TFloatField
      FieldName = 'FatCtrole'
      Required = True
    end
    object QrOSPEstagFaseLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOSPEstagFaseDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSPEstagFaseDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSPEstagFaseUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOSPEstagFaseUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOSPEstagFaseAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOSPEstagFaseAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOSPEstagFaseAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOSPEstagFaseAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOSPEstagFaseNO_MaqEqu: TWideStringField
      FieldName = 'NO_MaqEqu'
      Size = 60
    end
    object QrOSPEstagFaseNO_Turno: TWideStringField
      FieldName = 'NO_Turno'
      Size = 60
    end
    object QrOSPEstagFaseNO_Fornecedor: TWideStringField
      FieldName = 'NO_Fornecedor'
      Size = 100
    end
    object QrOSPEstagFaseMqEqEtiqt: TWideStringField
      FieldName = 'MqEqEtiqt'
      Size = 60
    end
    object QrOSPEstagFaseEstagio: TIntegerField
      FieldName = 'Estagio'
    end
    object QrOSPEstagFaseNO_Estagio: TWideStringField
      FieldName = 'NO_Estagio'
      Size = 60
    end
  end
  object DsOSPEstagFase: TDataSource
    DataSet = QrOSPEstagFase
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrOSPMaqEquCad: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ospmaqequcad'
      'ORDER BY Nome')
    Left = 544
    Top = 56
    object QrOSPMaqEquCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOSPMaqEquCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsOSPMaqEquCad: TDataSource
    DataSet = QrOSPMaqEquCad
    Left = 544
    Top = 104
  end
  object QrOSPTurnosCad: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ospturnoscad'
      'ORDER BY Nome')
    Left = 648
    Top = 56
    object QrOSPTurnosCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOSPTurnosCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOSPTurnosCad: TDataSource
    DataSet = QrOSPTurnosCad
    Left = 648
    Top = 104
  end
  object QrEntidades: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT Codigo, IF(tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 736
    Top = 56
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 736
    Top = 104
  end
  object QrOSPEstagStor: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ospestagstor')
    Left = 448
    Top = 56
    object QrOSPEstagStorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPEstagStorNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOSPEstagStor: TDataSource
    DataSet = QrOSPEstagStor
    Left = 448
    Top = 104
  end
end
