unit OSPEstagFasPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables;

type
  TFmOSPEstagFasPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    Label1: TLabel;
    EdTxt: TEdit;
    DGDados: TDBGrid;
    QrOSPEstagFase: TMySQLQuery;
    QrOSPEstagFaseCodigo: TIntegerField;
    QrOSPEstagFaseControle: TIntegerField;
    QrOSPEstagFaseMaqEqu: TIntegerField;
    QrOSPEstagFaseNome: TWideStringField;
    QrOSPEstagFaseQtdOperadores: TIntegerField;
    QrOSPEstagFaseTurno: TIntegerField;
    QrOSPEstagFaseUtiCapInst: TFloatField;
    QrOSPEstagFaseFornecedor: TIntegerField;
    QrOSPEstagFaseUndCtrole: TSmallintField;
    QrOSPEstagFaseFatCtrole: TFloatField;
    QrOSPEstagFaseNO_MaqEqu: TWideStringField;
    QrOSPEstagFaseNO_Turno: TWideStringField;
    QrOSPEstagFaseNO_Fornecedor: TWideStringField;
    QrOSPEstagFaseNO_UndCtrole: TWideStringField;
    DsOSPEstagFase: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdTxtChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure SelecionaItem();
  public
    { Public declarations }
    FCodigo, FControle: Integer;
  end;

  var
  FmOSPEstagFasPsq: TFmOSPEstagFasPsq;

implementation

uses UnMyObjects, UnDmkProcFunc, UnAppEnums, DmkDAC_PF, Module;

{$R *.DFM}

procedure TFmOSPEstagFasPsq.BtOKClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmOSPEstagFasPsq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPEstagFasPsq.DGDadosDblClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmOSPEstagFasPsq.EdTxtChange(Sender: TObject);
var
  ATT_UndCtrole, sTexto: String;
begin
  ATT_UndCtrole := dmkPF.ArrayToTexto('oef.UndCtrole', 'NO_UndCtrole', pvPos, True,
    sUndCtrleProd);
  if Length(EdTxt.Text) > 1 then
  begin
    sTexto := EdTxt.Text;
    UnDmkDAC_PF.AbreMySQLQuery0(QrOSPEstagFase, Dmod.MyDB, [
    'SELECT oef.*, ',
    ATT_UndCtrole,
    'omc.Nome NO_MaqEqu, otc.Nome NO_Turno, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Fornecedor ',
    //'ELT(oef.UndCtrole, "?") NO_UndCtrole ',
    'FROM ospestagfase oef ',
    'LEFT JOIN ospmaqequcad omc ON omc.Codigo=oef.MaqEqu ',
    'LEFT JOIN ospturnoscad otc ON otc.Codigo=oef.Turno ',
    'LEFT JOIN entidades ent ON ent.Codigo=oef.Fornecedor ',
    'WHERE omc.Nome LIKE "%' + sTexto + '%"',
    'OR oef.Nome LIKE "%' + sTexto + '%"',
    '']);
  end;
end;

procedure TFmOSPEstagFasPsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSPEstagFasPsq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo   := 0;
  FControle := 0;
end;

procedure TFmOSPEstagFasPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPEstagFasPsq.SelecionaItem();
begin
  if QrOSPEstagFase.RecordCount > 0 then
  begin
    FCodigo   := QrOSPEstagFaseCodigo.Value;
    FControle := QrOSPEstagFaseControle.Value;
    //
    Close;
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

end.
