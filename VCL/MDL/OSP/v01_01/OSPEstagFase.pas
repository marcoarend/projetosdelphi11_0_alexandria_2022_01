unit OSPEstagFase;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkEditCB, dmkDBLookupComboBox;

type
  TFmOSPEstagFase = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBTutul2: TGroupBox;
    GBTitulo: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    QrOSPEstagFase: TMySQLQuery;
    DsOSPEstagFase: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrOSPMaqEquCad: TMySQLQuery;
    DsOSPMaqEquCad: TDataSource;
    QrOSPTurnosCad: TMySQLQuery;
    DsOSPTurnosCad: TDataSource;
    QrEntidades: TMySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    DsEntidades: TDataSource;
    GBEdita: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    SbTurno: TSpeedButton;
    Label10: TLabel;
    SbFornecedor: TSpeedButton;
    Label11: TLabel;
    Label12: TLabel;
    EdQtdOperadores: TdmkEdit;
    EdTurno: TdmkEditCB;
    CBTurno: TdmkDBLookupComboBox;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    EdUtiCapInst: TdmkEdit;
    RGUndCtrole: TdmkRadioGroup;
    EdFatCtrole: TdmkEdit;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBDados: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    QrOSPEstagFaseCodigo: TIntegerField;
    QrOSPEstagFaseMaqEqu: TIntegerField;
    QrOSPEstagFaseNome: TWideStringField;
    QrOSPEstagFaseQtdOperadores: TIntegerField;
    QrOSPEstagFaseTurno: TIntegerField;
    QrOSPEstagFaseUtiCapInst: TFloatField;
    QrOSPEstagFaseFornecedor: TIntegerField;
    QrOSPEstagFaseUndCtrole: TSmallintField;
    QrOSPEstagFaseFatCtrole: TFloatField;
    QrOSPEstagFaseLk: TIntegerField;
    QrOSPEstagFaseDataCad: TDateField;
    QrOSPEstagFaseDataAlt: TDateField;
    QrOSPEstagFaseUserCad: TIntegerField;
    QrOSPEstagFaseUserAlt: TIntegerField;
    QrOSPEstagFaseAlterWeb: TSmallintField;
    QrOSPEstagFaseAWServerID: TIntegerField;
    QrOSPEstagFaseAWStatSinc: TSmallintField;
    QrOSPEstagFaseAtivo: TSmallintField;
    QrOSPEstagFaseNO_MaqEqu: TWideStringField;
    QrOSPMaqEquCadCodigo: TIntegerField;
    QrOSPMaqEquCadNome: TWideStringField;
    QrOSPTurnosCadCodigo: TIntegerField;
    QrOSPTurnosCadNome: TWideStringField;
    EdMaqEqu: TdmkEditCB;
    CBMaqEqu: TdmkDBLookupComboBox;
    SpeedButton8: TSpeedButton;
    EdMqEqEtiqt: TdmkEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrOSPEstagFaseNO_Turno: TWideStringField;
    QrOSPEstagFaseNO_Fornecedor: TWideStringField;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBRGUndCtrole: TDBRadioGroup;
    DBEdit10: TDBEdit;
    EdNome: TdmkEdit;
    Label2: TLabel;
    QrOSPEstagFaseMqEqEtiqt: TWideStringField;
    DBEdit3: TDBEdit;
    DBEdit11: TDBEdit;
    Label6: TLabel;
    QrOSPEstagFaseEstagio: TIntegerField;
    QrOSPEstagFaseNO_Estagio: TWideStringField;
    Label9: TLabel;
    EdEstagio: TdmkEditCB;
    CBEstagio: TdmkDBLookupComboBox;
    SbEstagio: TSpeedButton;
    QrOSPEstagStor: TMySQLQuery;
    QrOSPEstagStorCodigo: TIntegerField;
    QrOSPEstagStorNome: TWideStringField;
    DsOSPEstagStor: TDataSource;
    Label13: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    DBEdit12: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOSPEstagFaseAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOSPEstagFaseBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbMaqEquClick(Sender: TObject);
    procedure SbTurnoClick(Sender: TObject);
    procedure SbFornecedorClick(Sender: TObject);
    procedure SbEstagioClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmOSPEstagFase: TFmOSPEstagFase;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnAppEnums, UnEntities, UnOSP_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOSPEstagFase.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOSPEstagFase.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOSPEstagFaseCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOSPEstagFase.DefParams;
begin
  VAR_GOTOTABELA := 'ospestagfase';
  VAR_GOTOMYSQLTABLE := QrOSPEstagFase;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT mec.Nome NO_MaqEqu, otc.Nome NO_Turno, ');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_Fornecedor,');
  VAR_SQLx.Add('oes.Nome NO_Estagio, oef.*');
  VAR_SQLx.Add('FROM ospestagfase oef');
  VAR_SQLx.Add('LEFT JOIN ospmaqequcad mec ON mec.Codigo=oef.MaqEqu');
  VAR_SQLx.Add('LEFT JOIN ospturnoscad otc ON otc.Codigo=oef.Turno');
  VAR_SQLx.Add('LEFT JOIN entidades    frn ON frn.Codigo=oef.Fornecedor');
  VAR_SQLx.Add('LEFT JOIN ospestagstor oes ON oes.Codigo=oef.Estagio');
  VAR_SQLx.Add('WHERE oef.Codigo > 0');
  //
  VAR_SQL1.Add('AND oef.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND oef.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND oef.Nome Like :P0');
  //
end;

procedure TFmOSPEstagFase.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOSPEstagFase.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOSPEstagFase.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOSPEstagFase.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOSPEstagFase.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOSPEstagFase.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOSPEstagFase.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOSPEstagFase.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPEstagFase.BtAlteraClick(Sender: TObject);
begin
  if (QrOSPEstagFase.State <> dsInactive) and (QrOSPEstagFase.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOSPEstagFase, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'ospestagfase');
  end;
end;

procedure TFmOSPEstagFase.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOSPEstagFaseCodigo.Value;
  Close;
end;

procedure TFmOSPEstagFase.BtConfirmaClick(Sender: TObject);
var
  Nome, MqEqEtiqt: String;
  Codigo, Controle, MaqEqu, QtdOperadores, Turno, Fornecedor, UndCtrole,
  Estagio: Integer;
  UtiCapInst, FatCtrole: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  //Controle       := ;
  MaqEqu         := EdMaqEqu.ValueVariant;
  Nome           := EdNome.ValueVariant;
  MqEqEtiqt      := EdMqEqEtiqt.ValueVariant;
  QtdOperadores  := EdQtdOperadores.ValueVariant;
  Turno          := EdTurno.ValueVariant;
  UtiCapInst     := EdUtiCapInst.ValueVariant;
  Fornecedor     := EdFornecedor.ValueVariant;
  UndCtrole      := RGUndCtrole.ItemIndex;
  FatCtrole      := EdFatCtrole.ValueVariant;
  Estagio        := EdEstagio.ValueVariant;
  //
  if MyObjects.FIC(Nome = EmptyStr, EdNome, 'Informe a descri��o da fase!') then Exit;
  if MyObjects.FIC(Estagio = 0, EdEstagio, 'Informe o est�gio!') then Exit;

  //Codigo := UMyMod.BuscaEmLivreY_Def('ospestagfase', 'Controle', SQLType, Codigo);
  Codigo := UMyMod.BPGS1I32('ospestagfase', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ospestagfase', False, [
  'MaqEqu', 'Nome', 'MqEqEtiqt',
  'QtdOperadores', 'Turno', 'UtiCapInst',
  'Fornecedor', 'UndCtrole', 'FatCtrole',
  'Estagio'], [
  'Codigo'], [
  MaqEqu, Nome, MqEqEtiqt,
  QtdOperadores, Turno, UtiCapInst,
  Fornecedor, UndCtrole, FatCtrole,
  Estagio], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;

{
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ospestagfase', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrOSPEstagFaseCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'ospestagfase', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
*)
    LocCod(Codigo, Codigo);
  end;
}
end;

procedure TFmOSPEstagFase.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ospestagfase', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmOSPEstagFase.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOSPEstagFase, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ospestagfase');
end;

procedure TFmOSPEstagFase.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  //
  UnDmkDAC_PF.AbreQuery(QrOSPMaqEquCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOSPTurnosCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOSPEstagStor, Dmod.MyDB);
  //
  MyObjects.ConfiguraRadioGroup(RGUndCtrole, sUndCtrleProd, (*Colunas*)5, (*Default*)0);
  MyObjects.ConfiguraDBRadioGroup(DBRGUndCtrole, sUndCtrleProd, (*Colunas*)5);
end;

procedure TFmOSPEstagFase.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOSPEstagFaseCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOSPEstagFase.SbEstagioClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OSP_Jan.MostraFormOSPEstagStor(EdEstagio.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdEstagio, CBEstagio, QrOSPMaqEquCad, VAR_CADASTRO);
end;

procedure TFmOSPEstagFase.SbFornecedorClick(Sender: TObject);
begin
  Entities.CadastroESelecaoDeEntidade2(QrEntidades, EdFornecedor, CBFornecedor);
end;

procedure TFmOSPEstagFase.SbMaqEquClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OSP_Jan.MostraFormOSPMaqEquCad();
  UMyMod.SetaCodigoPesquisado(EdMaqEqu, CBMaqEqu, QrOSPMaqEquCad, VAR_CADASTRO);
end;

procedure TFmOSPEstagFase.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOSPEstagFase.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOSPEstagFaseCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOSPEstagFase.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOSPEstagFase.QrOSPEstagFaseAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOSPEstagFase.FormActivate(Sender: TObject);
begin
  //DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmOSPEstagFase.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOSPEstagFaseCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ospestagfase', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOSPEstagFase.SbTurnoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OSP_Jan.MostraFormOSPTurnosCad(EdTurno.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdTurno, CBTurno, QrOSPTurnosCad, VAR_CADASTRO);
end;

procedure TFmOSPEstagFase.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPEstagFase.QrOSPEstagFaseBeforeOpen(DataSet: TDataSet);
begin
  QrOSPEstagFaseCodigo.DisplayFormat := FFormatFloat;
end;

end.

