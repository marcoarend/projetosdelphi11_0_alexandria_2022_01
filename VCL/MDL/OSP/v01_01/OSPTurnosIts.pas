unit OSPTurnosIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmOSPTurnosIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNom_Cod: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesCodUsu: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    EdHrIni: TdmkEdit;
    LaHora: TLabel;
    EdHrFim: TdmkEdit;
    Label2: TLabel;
    Label1: TLabel;
    DBEdControle: TdmkDBEdit;
    Label4: TLabel;
    DBEdNom_Ctr: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrDds, FQrIts: TmySQLQuery;
    FDsCab, FDsDds: TDataSource;
  end;

  var
  FmOSPTurnosIts: TFmOSPTurnosIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  OSPTurnosCad;

{$R *.DFM}

procedure TFmOSPTurnosIts.BtOKClick(Sender: TObject);
var
  Nome, HrIni, HrFim: String;
  Codigo, Controle, Conta: Integer;
  SubMinut: Double;
  Periodo: TDateTime;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := Geral.IMV(DBEdControle.Text);
  Conta          := EdConta.ValueVariant;
  Nome           := EdNome.ValueVariant;
  HrIni          := Geral.FDT(EdHrIni.ValueVariant, 100);
  HrFim          := Geral.FDT(EdHrFim.ValueVariant, 100);
  Periodo       := EdHrFim.ValueVariant - EdHrIni.ValueVariant;
  if Periodo < 0 then
    Periodo := Periodo + 1;
  SubMinut       := Periodo * 1440;
  //
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  //
  Conta := UMyMod.BPGS1I32('ospturnosits', 'Conta', '', '', tsPos, SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ospturnosits', False, [
  'Codigo', 'Controle', 'Nome',
  'HrIni', 'HrFim',
  'SubMinut'], [
  'Conta'], [
  Codigo, Controle, Nome,
  HrIni, HrFim,
  SubMinut], [
  Conta], True) then
  begin
    FmOSPTurnosCad.DefineTotalTempoTurno(Codigo, Controle);
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdConta.ValueVariant     := 0;
      EdHrIni.ValueVariant     := 0;
      EdHrFim.ValueVariant     := 0;
      EdNome.ValueVariant      := '';
      //
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmOSPTurnosIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPTurnosIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNom_Cod.DataSource := FDsCab;
  DBEdControle.DataSource := FDsDds;
  DBEdNom_Ctr.DataSource := FDsDds;
  MyObjects.CorIniComponente();
end;

procedure TFmOSPTurnosIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSPTurnosIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPTurnosIts.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
