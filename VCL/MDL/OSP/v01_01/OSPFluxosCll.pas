unit OSPFluxosCll;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmOSPFluxosCll = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CB_Sel_: TdmkDBLookupComboBox;
    Ed_Sel_: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrOSPMaqEquCad: TMySQLQuery;
    QrOSPMaqEquCadCodigo: TIntegerField;
    QrOSPMaqEquCadNome: TWideStringField;
    DsOSPMaqEquCad: TDataSource;
    EdCodigo: TdmkEdit;
    EdNO_Fluxo: TdmkEdit;
    Label2: TLabel;
    EdControle: TdmkEdit;
    Label4: TLabel;
    EdNO_Estagio: TdmkEdit;
    QrOSPTurnosCad: TMySQLQuery;
    DsOSPTurnosCad: TDataSource;
    MySQLQuery2: TMySQLQuery;
    DataSource2: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOSPFluxosCll: TFmOSPFluxosCll;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmOSPFluxosCll.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, Conta, MaqEqu, QtdOperadores, Turno, Fornecedor, UndCtrole: Integer;
  UtiCapInst, FatCtrole: Double;
  SQLType: TSQLType;
begin
(*
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Conta          := EdConta.ValueVariant;
  MaqEqu         := EdMaqEqu.ValueVariant;
  Nome           := EdNome.ValueVariant;
  QtdOperadores  := EdQtdOperadores.ValueVariant;
  Turno          := EdTurno.ValueVariant;
  UtiCapInst     := EdUtiCapInst.ValueVariant;
  Fornecedor     := EdFornecedor.ValueVariant;
  UndCtrole      := EdUndCtrole.ValueVariant;
  FatCtrole      := EdFatCtrole.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  if MyObjects.FIC(MaqEqu = 0, EdMaqEqu, 'Informe a m�quina / equipamento!') then
    Exit;
  if MyObjects.FIC(QtdOperadores = 0, EdQtdOperadores, 'Informe a quantidade de operadores!') then
    Exit;
  if MyObjects.FIC(Turno = 0, EdTurno, 'Informe o turno!') then
    Exit;
  if MyObjects.FIC(UtiCapInst = 0, EdUtiCapInst, 'Informe o percentual da Utiliza��o da Capacidade Instalada!') then
    Exit;
  if MyObjects.FIC(UndCtrole = 0, EdUndCtrole, 'Informe a unidade de controle!') then
    Exit;
  if MyObjects.FIC(FatCtrole = 0, EdFatCtrole, 'Informe o Fator(qtde) de controle!') then
    Exit;
  //
  Conta := UMyMod.BPGS1I32('ospfluxosmem', 'Conta', '', '', tsPos, SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ospfluxosmem', False, [
  'Codigo', 'Controle', 'MaqEqu',
  'Nome', 'QtdOperadores', 'Turno',
  'UtiCapInst', 'Fornecedor', 'UndCtrole',
  'FatCtrole'], [
  'Conta'], [
  Codigo, Controle, MaqEqu,
  Nome, QtdOperadores, Turno,
  UtiCapInst, Fornecedor, UndCtrole,
  FatCtrole], [
  Conta], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType               := stIns;
      EdConta.ValueVariant          := 0;
      EdMaqEqu.ValueVariant         := 0;
      CBMaqEqu.KeyValue             := Null;
      EdTurno.ValueVariant          := 0;
      CBTurno.KeyValue              := Null;
      EdFornecedor.ValueVariant     := 0;
      CBFornecedor.KeyValue         := Null;
      EdNome.ValueVariant           := '';
      EdQtdOperadores.ValueVariant  := 0;
      EdUtiCapInst.ValueVariant     := 0;
      RGUndCtrole.ItemIndex         := 0;
      EdFatCtrole.ValueVariant      := 0;
      //
      EdNome.SetFocus;
    end else Close;
  end;
*)
end;

procedure TFmOSPFluxosCll.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPFluxosCll.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSPFluxosCll.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
//  UnDmkDAC_PF.AbreQuery(Qr_Sel_, Dmod.MyDB?);
end;

procedure TFmOSPFluxosCll.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPFluxosCll.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
(*
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
*)
end;

procedure TFmOSPFluxosCll.SpeedButton1Click(Sender: TObject);
begin
(*
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFm_CadSel_, Fm_CadSel_, afmoNegarComAviso) then
  begin
    Fm_CadSel_.ShowModal;
    Fm_CadSel_.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO);
*)
end;

end.
