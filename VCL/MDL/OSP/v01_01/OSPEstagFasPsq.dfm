object FmOSPEstagFasPsq: TFmOSPEstagFasPsq
  Left = 339
  Top = 185
  Caption = 'OSP-CADAS-009 :: Pesquisa de Fase em Est'#225'gio de Produ'#231#227'o'
  ClientHeight = 609
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 531
        Height = 32
        Caption = ' Pesquisa de Fase em Est'#225'gio de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 531
        Height = 32
        Caption = ' Pesquisa de Fase em Est'#225'gio de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 531
        Height = 32
        Caption = ' Pesquisa de Fase em Est'#225'gio de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 447
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 447
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 447
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 4
        ExplicitWidth = 812
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 42
          Align = alTop
          TabOrder = 0
          ExplicitWidth = 808
          object Label1: TLabel
            Left = 12
            Top = 1
            Width = 139
            Height = 13
            Caption = 'Digite parte do nome da fase:'
          end
          object EdTxt: TEdit
            Left = 12
            Top = 17
            Width = 445
            Height = 21
            TabOrder = 0
            OnChange = EdTxtChange
          end
        end
        object DGDados: TDBGrid
          Left = 2
          Top = 57
          Width = 1004
          Height = 388
          Align = alClient
          DataSource = DsOSPEstagFase
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DGDadosDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MaqEqu'
              Title.Caption = 'Maq.'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MaqEqu'
              Title.Caption = 'Descri'#231#227'o da m'#225'quina / equipamento'
              Width = 156
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Nome da m'#225'quina / equipamento'
              Width = 162
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdOperadores'
              Title.Caption = 'Q. Operad.'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Turno'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Turno'
              Title.Caption = 'Descri'#231#227'o do turno'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UtiCapInst'
              Title.Caption = '% UCI'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fornecedor'
              Width = 35
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Fornecedor'
              Title.Caption = 'Descri'#231#227'o do fornecedor'
              Width = 188
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_UndCtrole'
              Title.Caption = 'Und.Controle'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatCtrole'
              Title.Caption = 'Fator UC'
              Width = 52
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 495
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 539
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrOSPEstagFase: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT oef.*, omc.Nome NO_MaqEqu, otc.Nome NO_Turno,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Fornecedor,'
      'ELT(oef.UndCtrole, "?") NO_UndCtrole '
      'FROM ospestagfase oef'
      'LEFT JOIN ospmaqequcad omc ON omc.Codigo=oef.MaqEqu'
      'LEFT JOIN ospturnoscad otc ON otc.Codigo=oef.Turno'
      'LEFT JOIN entidades ent ON ent.Codigo=oef.Fornecedor')
    Left = 188
    Top = 233
    object QrOSPEstagFaseCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOSPEstagFaseControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOSPEstagFaseMaqEqu: TIntegerField
      FieldName = 'MaqEqu'
      Required = True
    end
    object QrOSPEstagFaseNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOSPEstagFaseQtdOperadores: TIntegerField
      FieldName = 'QtdOperadores'
      Required = True
    end
    object QrOSPEstagFaseTurno: TIntegerField
      FieldName = 'Turno'
      Required = True
    end
    object QrOSPEstagFaseUtiCapInst: TFloatField
      FieldName = 'UtiCapInst'
      Required = True
    end
    object QrOSPEstagFaseFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Required = True
    end
    object QrOSPEstagFaseUndCtrole: TSmallintField
      FieldName = 'UndCtrole'
      Required = True
    end
    object QrOSPEstagFaseFatCtrole: TFloatField
      FieldName = 'FatCtrole'
      Required = True
      DisplayFormat = '#,###,###.###.###;-#,###,###.###.###; '
    end
    object QrOSPEstagFaseNO_MaqEqu: TWideStringField
      FieldName = 'NO_MaqEqu'
      Size = 60
    end
    object QrOSPEstagFaseNO_Turno: TWideStringField
      FieldName = 'NO_Turno'
      Size = 60
    end
    object QrOSPEstagFaseNO_Fornecedor: TWideStringField
      FieldName = 'NO_Fornecedor'
      Size = 100
    end
    object QrOSPEstagFaseNO_UndCtrole: TWideStringField
      DisplayWidth = 30
      FieldName = 'NO_UndCtrole'
      Size = 30
    end
  end
  object DsOSPEstagFase: TDataSource
    DataSet = QrOSPEstagFase
    Left = 188
    Top = 277
  end
end
