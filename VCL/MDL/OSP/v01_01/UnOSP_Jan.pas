unit UnOSP_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, mySQLDbTables, DmkGeral, DmkDAC_PF,
  UnInternalConsts, UnDmkProcFunc, frxClass, frxDBSet, Menus, Variants,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums, ModProd, UnGrl_Vars,
  mySQLDirectQuery;

type
  TUnOSP_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormOSPTurnosCad(Codigo: Integer);
    procedure MostraFormOSPPeriodosCad(Codigo: Integer);
    procedure MostraFormOSPMaqEquCad();
    //procedure MostraFormOSPEstagiosCad();
    procedure MostraFormOSPEstagStor(Codigo: Integer);
    procedure MostraFormOSPEstagFase(Codigo: Integer);
    procedure MostraFormOSPFluxosCad(Codigo: Integer);

  end;

var
  OSP_Jan: TUnOSP_Jan;

implementation

uses MyDBCheck, ModuleGeral, UnMyObjects, Module, CfgCadLista, UMySQLDB,
  OSPTurnosCad, OSPPeriodosCad, OSPEstagStor, OSPFluxosCad, OSPEstagFase;

{ TUnOSP_Jan }

{
procedure TUnOSP_Jan.MostraFormOSPEstagiosCad();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'OSPEstagiosCad', 60, ncControle,
  'Est�gios de produ��o',
  [], False, Null, [], [], False);
end;
}

procedure TUnOSP_Jan.MostraFormOSPEstagFase(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOSPEstagFase, FmOSPEstagFase, afmoNegarComAviso) then
  begin
    FmOSPEstagFase.ShowModal;
    if Codigo <> 0 then
      FmOSPEstagFase.LocCod(Codigo, Codigo);
    FmOSPEstagFase.Destroy;
  end;
end;

procedure TUnOSP_Jan.MostraFormOSPEstagStor(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOSPEstagStor, FmOSPEstagStor, afmoNegarComAviso) then
  begin
    FmOSPEstagStor.ShowModal;
    if Codigo <> 0 then
      FmOSPEstagStor.LocCod(Codigo, Codigo);
    FmOSPEstagStor.Destroy;
  end;
end;

procedure TUnOSP_Jan.MostraFormOSPFluxosCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOSPFluxosCad, FmOSPFluxosCad, afmoNegarComAviso) then
  begin
    FmOSPFluxosCad.ShowModal;
    if Codigo <> 0 then
      FmOSPFluxosCad.LocCod(Codigo, Codigo);
    FmOSPFluxosCad.Destroy;
  end;
end;

procedure TUnOSP_Jan.MostraFormOSPMaqEquCad();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'OSPMaqEquCad', 60, ncControle,
  'M�quinas e Equipamentos',
  [], False, Null, [], [], False);
end;

procedure TUnOSP_Jan.MostraFormOSPPeriodosCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOSPPeriodosCad, FmOSPPeriodosCad, afmoNegarComAviso) then
  begin
    FmOSPPeriodosCad.ShowModal;
    if Codigo <> 0 then
      FmOSPPeriodosCad.LocCod(Codigo, Codigo);
    FmOSPPeriodosCad.Destroy;
  end;
end;

procedure TUnOSP_Jan.MostraFormOSPTurnosCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOSPTurnosCad, FmOSPTurnosCad, afmoNegarComAviso) then
  begin
    FmOSPTurnosCad.ShowModal;
    if Codigo <> 0 then
      FmOSPTurnosCad.LocCod(Codigo, Codigo);
    FmOSPTurnosCad.Destroy;
  end;
end;

end.


