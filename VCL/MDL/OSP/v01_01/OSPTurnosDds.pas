unit OSPTurnosDds;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkCheckGroup, Math;

type
  TFmOSPTurnosDds = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesCodUsu: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    CGQuaisDdSem: TdmkCheckGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOSPTurnosDds: TFmOSPTurnosDds;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  OSPTurnosCad;

{$R *.DFM}

procedure TFmOSPTurnosDds.BtOKClick(Sender: TObject);
var
  Nome(*, UniHoras, SumHoras*): String;
  I, N,Codigo, Controle, QuaisDdSem, QtdDdSem: Integer;
  //UniMinut, SumMinut: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Nome           := EdNome.ValueVariant;
  QuaisDdSem     := CGQuaisDdSem.Value;
  QtdDdSem       := 0;
  for I := 0 to CGQuaisDdSem.Items.Count -1  do
  begin
    N := Trunc(IntPower(2, I));
    if Geral.IntInConjunto(N, QuaisDdSem) then
      QtdDdSem := QtdDdSem + 1;
  end;
{
  UniHoras       := ;
  UniMinut       := ;
  SumHoras       := ;
  SumMinut       := ;
}
  //
if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('ospturnosdds', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ospturnosdds', False, [
  'Codigo', 'Nome', 'QuaisDdSem',
  'QtdDdSem'(*, 'UniHoras', 'UniMinut',
  'SumHoras', 'SumMinut'*)], [
  'Controle'], [
  Codigo, Nome, QuaisDdSem,
  QtdDdSem(*, UniHoras, UniMinut,
  SumHoras, SumMinut*)], [
  Controle], True) then
  begin
    FmOSPTurnosCad.DefineTotalTempoTurno(Codigo, Controle);
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      CGQuaisDdSem.Value       := 0;
      EdNome.ValueVariant      := '';
      //
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmOSPTurnosDds.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPTurnosDds.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmOSPTurnosDds.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSPTurnosDds.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPTurnosDds.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
