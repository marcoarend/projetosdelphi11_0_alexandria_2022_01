unit EntiDocs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  frxClass, frxPreview, mySQLDbTables, frxDBSet, frxOLE, System.TypInfo,
  Soap.WebServExp, Soap.WSDLBind, Xml.XMLSchema,
  // ini Ver Delphi Alexandria
  //cefvcl,
  // fim Ver Delphi Alexandria
  Soap.WSDLPub;

type
  TFmEntiDocs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel32: TPanel;
    Panel33: TPanel;
    QrEntiDocs: TMySQLQuery;
    DsEntiDocs: TDataSource;
    QrEntiDocsCodigo: TIntegerField;
    QrEntiDocsNome: TWideStringField;
    QrEntiDocsCodEnti: TIntegerField;
    QrEntiDocsTipImg: TSmallintField;
    QrEntiDocsLk: TIntegerField;
    QrEntiDocsDataCad: TDateField;
    QrEntiDocsDataAlt: TDateField;
    QrEntiDocsUserCad: TIntegerField;
    QrEntiDocsUserAlt: TIntegerField;
    QrEntiDocsAlterWeb: TSmallintField;
    QrEntiDocsAWServerID: TIntegerField;
    QrEntiDocsAWStatSinc: TSmallintField;
    QrEntiDocsAtivo: TSmallintField;
    QrEntiDocsTIPO: TWideStringField;
    QrEntiDocsCHAVE: TIntegerField;
    QrEntiDocsSEQUENCIA: TIntegerField;
    QrEntiDocsDESCRICAO: TWideStringField;
    QrEntiDocsTIPOARQUIVO: TWideStringField;
    QrEntiDocsDATAATUALIZACAO: TDateTimeField;
    Splitter1: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    // ini Ver Delphi Alexandria
    //Chromium1: TChromium;
    // fim Ver Delphi Alexandria
    Timer1: TTimer;
    DBGrid1: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrEntiDocsBeforeScroll(DataSet: TDataSet);
    procedure QrEntiDocsAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
  private
    { Private declarations }
    FCriando: Boolean;
  public
    { Public declarations }
    FTipo: String;
    FChave: Integer;
    FCaminhoExiste: Boolean;
    //
    procedure ReopenEntiDocs();
  end;

  var
  FmEntiDocs: TFmEntiDocs;

implementation

uses UnMyObjects, DmkDAC_PF, Module, ModuleGeral;

{$R *.DFM}

procedure TFmEntiDocs.BtOKClick(Sender: TObject);
var
  DtHrAbert, DtHrFecha, ZtatusDtH: String;
  Codigo, Local, NrOP, SeqGrupo, NrReduzidoOP, OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed, ZtatusIsp, ZtatusMot: Integer;
  SQLType: TSQLType;
begin
  QrEntiDocs.Prior;
{
  SQLType := ImgTipo.SQLType?;
  Codigo         := ;
  Local          := ;
  NrOP           := ;
  SeqGrupo       := ;
  NrReduzidoOP   := ;
  DtHrAbert      := ;
  DtHrFecha      := ;
  OVcYnsMed      := ;
  OVcYnsChk      := ;
  LimiteChk      := ;
  LimiteMed      := ;
  ZtatusIsp      := ;
  ZtatusDtH      := ;
  ZtatusMot      := ;

  //
? := UMyMod.BuscaEmLivreY_Def('ovgispgercab', 'Codigo', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('ovgispgercab', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovgispgercab', auto_increment?[
'Local', 'NrOP', 'SeqGrupo',
'NrReduzidoOP', 'DtHrAbert', 'DtHrFecha',
'OVcYnsMed', 'OVcYnsChk', 'LimiteChk',
'LimiteMed', 'ZtatusIsp', 'ZtatusDtH',
'ZtatusMot'], [
'Codigo'], [
Local, NrOP, SeqGrupo,
NrReduzidoOP, DtHrAbert, DtHrFecha,
OVcYnsMed, OVcYnsChk, LimiteChk,
LimiteMed, ZtatusIsp, ZtatusDtH,
ZtatusMot], [
Codigo], UserDataAlterweb?, IGNORE?
}
end;

procedure TFmEntiDocs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiDocs.DBGrid1CellClick(Column: TColumn);
begin
{
      Chromium1.Load('file:///' + 'C:/Dermatek/Tiso lin/ImagensGerenciaFacil/pessoa/' +
        QrEntiDocsNome.Value);
}
end;

procedure TFmEntiDocs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiDocs.FormCreate(Sender: TObject);
begin
  FCriando := True;
  ImgTipo.SQLType := stLok;
  FCaminhoExiste := DirectoryExists(DModG.QrOpcoesGerl.FieldByName('DirDocsEnti').AsString);
end;

procedure TFmEntiDocs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiDocs.FormShow(Sender: TObject);
begin
  FCriando := False;
end;

procedure TFmEntiDocs.QrEntiDocsAfterScroll(DataSet: TDataSet);
begin
{
  if not FCriando then
  begin
    //Timer1.Enabled := False;
    try
      Chromium1.Load('file:///' + 'C:/Dermatek/Tiso lin/ImagensGerenciaFacil/pessoa/' +
        QrEntiDocsNome.Value);
    finally
      QrEntiDocs.EnableControls;
    end;
  end;
}
end;

procedure TFmEntiDocs.QrEntiDocsBeforeScroll(DataSet: TDataSet);
begin
  //Chromium1.Load('about:blank') ;
end;

procedure TFmEntiDocs.ReopenEntiDocs();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiDocs, Dmod.MyDB, [
  'SELECT * ',
  'FROM entiimgs ',
  'WHERE Tipo="' + FTIPO + '" ',
  'AND Chave=' + Geral.FF0(FChave),
  'ORDER BY SEQUENCIA, Codigo  ',
  '']);
end;

end.
