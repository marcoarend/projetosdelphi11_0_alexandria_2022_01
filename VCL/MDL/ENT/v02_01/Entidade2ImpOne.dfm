object FmEntidade2ImpOne: TFmEntidade2ImpOne
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-022 :: Impress'#227'o de Dados de Entidade '#218'nica'
  ClientHeight = 335
  ClientWidth = 754
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 754
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 695
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 636
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 567
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Dados de Entidade '#218'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 567
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Dados de Entidade '#218'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 567
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Dados de Entidade '#218'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 754
    Height = 135
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 754
      Height = 135
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 754
        Height = 135
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object LaCliente: TLabel
          Left = 10
          Top = 15
          Width = 57
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Entidade:'
        end
        object Label1: TLabel
          Left = 10
          Top = 69
          Width = 508
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Linhas de cadastros de contatos + telefones + emails em branco p' +
            'ara preenchimento:'
        end
        object Label2: TLabel
          Left = 10
          Top = 98
          Width = 367
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Linhas em branco para escrever coment'#225'rios e observa'#231#245'es:'
        end
        object EdEntidade: TdmkEditCB
          Left = 10
          Top = 34
          Width = 80
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEntidade
          IgnoraDBLookupComboBox = False
        end
        object CBEntidade: TdmkDBLookupComboBox
          Left = 89
          Top = 34
          Width = 648
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsEntidades
          TabOrder = 1
          dmkEditCB = EdEntidade
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdContatos: TdmkEdit
          Left = 517
          Top = 64
          Width = 98
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '5'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 5
          ValWarn = False
        end
        object EdComent: TdmkEdit
          Left = 517
          Top = 94
          Width = 98
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '20'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 20
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 194
    Width = 754
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 750
      Height = 35
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 249
    Width = 754
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 575
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 573
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntidade: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntidadeCalcFields
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, "CNPJ", "CPF") NOME_CNPJ_CPF,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,'
      'IF(ent.Tipo=0, "I.E.", "R.G.") NOME_IE_RG,'
      'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,'
      ''
      'IF(ent.Tipo=0, ent.Fantasia   , ent.Apelido) NO_2_ENT,'
      'IF(ent.Tipo=0, ent.ERua       , ent.PRua   ) RUA,'
      'IF(ent.Tipo=0, ent.ECompl     , ent.PCompl ) COMPL,'
      'IF(ent.Tipo=0, ent.EBairro    , ent.PBairro) BAIRRO,'
      'IF(ent.Tipo=0, ent.ECidade    , ent.PCidade) CIDADE,'
      'IF(ent.Tipo=0, lle.Nome       , llp.Nome  ) NOMELOGRAD,'
      'IF(ent.Tipo=0, ufe.Nome       , ufp.Nome  ) NOMEUF,'
      'IF(ent.Tipo=0, ent.EPais      , ent.PPais  ) Pais,'
      ' '
      'IF(ent.Tipo=0, ent.ETe1       , ent.PTe1   ) TE1,'
      'IF(ent.Tipo=0, lle.Trato      , llp.Trato ) TRATO,'
      'IF(ent.Tipo=0, ent.EEndeRef   , ent.PEndeRef  ) ENDEREF,'
      ''
      'IF(ent.Tipo=0, ent.ELograd    , ent.PLograd) + 0.000 Lograd,'
      'IF(ent.Tipo=0, ent.ECEP       , ent.PCEP   ) + 0.000 CEP,'
      'IF(ent.Tipo=0, ent.ENumero    , ent.PNumero) + 0.000 NUMERO,'
      
        'IF(ent.Tipo=0, ent.ECodMunici , ent.PCodMunici) + 0.000 CODMUNIC' +
        'I,'
      'IF(ent.Tipo=0, ent.ECodiPais  , ent.PCodiPais ) + 0.000 CODPAIS,'
      ''
      
        'IF(ent.Tipo=0, "Inscri'#231#227'o Municipal", "SSP / Data RG" ) SSP_NIRE' +
        '_X,'
      'IF(ent.Tipo=0, ent.NIRE, CONCAT(ent.SSP, " / ",'
      'IF(ent.DataRG < "1900-01-01", "", '
      '  DATE_FORMAT(ent.DataRG, "%d/%m/%Y")))) SSP_NIRE_V'
      ''
      'FROM entidades ent'
      'LEFT JOIN ufs ufe ON ufe.Codigo=ent.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=ent.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=ent.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=ent.PLograd'
      ''
      ''
      'WHERE ent.Codigo=:P0')
    Left = 328
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntidadeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadeNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrEntidadeNOME_CNPJ_CPF: TWideStringField
      FieldName = 'NOME_CNPJ_CPF'
      Required = True
      Size = 4
    end
    object QrEntidadeCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntidadeNOME_IE_RG: TWideStringField
      FieldName = 'NOME_IE_RG'
      Required = True
      Size = 4
    end
    object QrEntidadeIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEntidadeNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrEntidadeRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrEntidadeCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrEntidadeBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrEntidadeCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 60
    end
    object QrEntidadeNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrEntidadeNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEntidadePais: TWideStringField
      FieldName = 'Pais'
      Size = 60
    end
    object QrEntidadeTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntidadeTRATO: TWideStringField
      FieldName = 'TRATO'
      Size = 10
    end
    object QrEntidadeENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntidadeSSP_NIRE_X: TWideStringField
      FieldName = 'SSP_NIRE_X'
      Required = True
      Size = 19
    end
    object QrEntidadeSSP_NIRE_V: TWideStringField
      FieldName = 'SSP_NIRE_V'
      Size = 23
    end
    object QrEntidadeTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntidadeCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadeNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadeTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 50
      Calculated = True
    end
    object C: TFloatField
      FieldName = 'Lograd'
    end
    object QrEntidadeCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEntidadeNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrEntidadeCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
    end
    object QrEntidadeCODPAIS: TFloatField
      FieldName = 'CODPAIS'
    end
  end
  object frxDsEntidade: TfrxDBDataset
    UserName = 'frxDsEntidade'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NO_ENT=NO_ENT'
      'NOME_CNPJ_CPF=NOME_CNPJ_CPF'
      'CNPJ_CPF=CNPJ_CPF'
      'NOME_IE_RG=NOME_IE_RG'
      'IE_RG=IE_RG'
      'NO_2_ENT=NO_2_ENT'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'TE1=TE1'
      'TRATO=TRATO'
      'ENDEREF=ENDEREF'
      'Lograd=Lograd'
      'CEP=CEP'
      'NUMERO=NUMERO'
      'CODMUNICI=CODMUNICI'
      'CODPAIS=CODPAIS'
      'SSP_NIRE_X=SSP_NIRE_X'
      'SSP_NIRE_V=SSP_NIRE_V'
      'Tipo=Tipo'
      'CEP_TXT=CEP_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'TE1_TXT=TE1_TXT')
    DataSet = QrEntidade
    BCDToCurrency = False
    Left = 356
    Top = 56
  end
  object frxEntidade: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxEntidadeGetValue
    Left = 300
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEntidade
        DataSetName = 'frxDsEntidade'
      end
      item
        DataSet = frxDsEntiEma
        DataSetName = 'frxDsEntiEma'
      end
      item
        DataSet = frxDsEntiTel
        DataSetName = 'frxDsEntiTel'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 132.283550000000000000
          Top = 18.897650000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'DADOS CADASTRAIS [VARF_CODIGO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 124.724426540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 124.724426540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntidade."NO_ENT"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 744.567410000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 136.063080000000000000
        Top = 143.622140000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEntidade
        DataSetName = 'frxDsEntidade'
        RowCount = 0
        object Memo4: TfrxMemoView
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CADASTRO]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 211.653680000000000000
          Width = 192.756030000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_REGISTRO]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 404.409710000000000000
          Width = 275.905690000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_IM]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Top = 18.897650000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome fantasia')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 98.267780000000000000
          Top = 18.897650000000000000
          Width = 582.047620000000000000
          Height = 18.897650000000000000
          DataField = 'NO_2_ENT'
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidade."NO_2_ENT"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Top = 37.795300000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 41.574830000000000000
          Top = 37.795300000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidade."CEP_TXT"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 151.181200000000000000
          Top = 37.795300000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Logradouro')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 241.889920000000000000
          Top = 37.795300000000000000
          Width = 438.425480000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_LOGRADOURO]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Top = 56.692950000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 41.574830000000000000
          Top = 56.692950000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidade."NUMERO_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 151.181200000000000000
          Top = 56.692950000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Complemento')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 241.889920000000000000
          Top = 56.692950000000000000
          Width = 438.425480000000000000
          Height = 18.897650000000000000
          DataField = 'COMPL'
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidade."COMPL"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Top = 75.590600000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Bairro')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 49.133890000000000000
          Top = 75.590600000000000000
          Width = 253.228510000000000000
          Height = 18.897650000000000000
          DataField = 'BAIRRO'
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidade."BAIRRO"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Top = 94.488250000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cidade')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 49.133890000000000000
          Top = 94.488250000000000000
          Width = 253.228510000000000000
          Height = 18.897650000000000000
          DataField = 'CIDADE'
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidade."CIDADE"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 241.889920000000000000
          Top = 113.385900000000000000
          Width = 30.236240000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 272.126160000000000000
          Top = 113.385900000000000000
          Width = 30.236240000000000000
          Height = 18.897650000000000000
          DataField = 'NOMEUF'
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidade."NOMEUF"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Top = 113.385900000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fone 1')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 49.133890000000000000
          Top = 113.385900000000000000
          Width = 192.756030000000000000
          Height = 18.897650000000000000
          DataField = 'TE1_TXT'
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidade."TE1_TXT"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 302.362400000000000000
          Top = 75.590600000000000000
          Width = 377.953000000000000000
          Height = 56.692950000000000000
          DataSet = frxDsEntidade
          DataSetName = 'frxDsEntidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ponto refer'#234'ncia: [frxDsEntidade."ENDEREF"]')
          ParentFont = False
        end
      end
      object Header14: TfrxHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 302.362400000000000000
        Width = 680.315400000000000000
        object Memo42: TfrxMemoView
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Contato')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 113.385838980000000000
          Width = 158.740198980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Cargo / Grau parent.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          Left = 272.126160000000000000
          Width = 124.724428980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Tipo de telefone')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          Left = 396.850588980000000000
          Width = 132.283488980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'N'#250'mero telefone')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          Left = 529.134080390000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Ramal')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Nascimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer14: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 340.157700000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEntiTel
        DataSetName = 'frxDsEntiTel'
        RowCount = 0
        object Memo67: TfrxMemoView
          Left = 272.126160000000000000
          Width = 124.724428980000000000
          Height = 15.118110240000000000
          DataField = 'NOMEETC'
          DataSet = frxDsEntiTel
          DataSetName = 'frxDsEntiTel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsEntiTel."NOMEETC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 396.850588980000000000
          Width = 132.283488980000000000
          Height = 15.118110240000000000
          DataField = 'TEL_TXT'
          DataSet = frxDsEntiTel
          DataSetName = 'frxDsEntiTel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsEntiTel."TEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          Left = 529.134080390000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataField = 'Ramal'
          DataSet = frxDsEntiTel
          DataSetName = 'frxDsEntiTel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsEntiTel."Ramal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'NO_CONTATO'
          DataSet = frxDsEntiTel
          DataSetName = 'frxDsEntiTel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsEntiTel."NO_CONTATO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          Left = 113.385838980000000000
          Width = 158.740198980000000000
          Height = 15.118110240000000000
          DataField = 'NO_CARGO'
          DataSet = frxDsEntiTel
          DataSetName = 'frxDsEntiTel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsEntiTel."NO_CARGO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataField = 'NATAL_TXT'
          DataSet = frxDsEntiTel
          DataSetName = 'frxDsEntiTel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsEntiTel."NATAL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header15: TfrxHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 404.409710000000000000
        Width = 680.315400000000000000
        object Memo105: TfrxMemoView
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Contato')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          Left = 113.385838980000000000
          Width = 158.740198980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Cargo / Grau parent.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          Left = 272.126160000000000000
          Width = 124.724428980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Tipo de email')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Left = 396.850588980000000000
          Width = 207.874088980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Email')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Nascimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer15: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 480.000310000000000000
        Width = 680.315400000000000000
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 442.205010000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEntiEma
        DataSetName = 'frxDsEntiEma'
        RowCount = 0
        object Memo111: TfrxMemoView
          Left = 272.126160000000000000
          Width = 124.724428980000000000
          Height = 15.118110240000000000
          DataField = 'NOMEETC'
          DataSet = frxDsEntiEma
          DataSetName = 'frxDsEntiEma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsEntiEma."NOMEETC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          Left = 396.850588980000000000
          Width = 207.874088980000000000
          Height = 15.118110240000000000
          DataField = 'EMail'
          DataSet = frxDsEntiEma
          DataSetName = 'frxDsEntiEma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsEntiEma."EMail"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'NO_CONTATO'
          DataSet = frxDsEntiEma
          DataSetName = 'frxDsEntiEma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsEntiEma."NO_CONTATO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          Left = 113.385838980000000000
          Width = 158.740198980000000000
          Height = 15.118110240000000000
          DataField = 'NO_CARGO'
          DataSet = frxDsEntiEma
          DataSetName = 'frxDsEntiEma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsEntiEma."NO_CARGO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataField = 'NATAL_TXT'
          DataSet = frxDsEntiEma
          DataSetName = 'frxDsEntiEma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsEntiEma."NATAL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header16: TfrxHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 506.457020000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Contato')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 113.385838980000000000
          Width = 158.740198980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Cargo / Grau parent.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 272.126160000000000000
          Width = 124.724428980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Tipo de email')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 396.850588980000000000
          Width = 207.874088980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Email ou Telefone + Ramal')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Nascimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DD_Contat_Empty: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 544.252320000000000000
        Width = 680.315400000000000000
        RowCount = 5
        object Memo109: TfrxMemoView
          Left = 272.126160000000000000
          Width = 124.724428980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          Left = 396.850588980000000000
          Width = 207.874088980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo118: TfrxMemoView
          Left = 113.385838980000000000
          Width = 158.740198980000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer16: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 582.047620000000000000
        Width = 680.315400000000000000
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 608.504330000000000000
        Width = 680.315400000000000000
        object Memo22: TfrxMemoView
          Width = 680.315326770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DD_Observacoes: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 646.299630000000000000
        Width = 680.315400000000000000
        RowCount = 20
        object Memo35: TfrxMemoView
          Width = 680.315326770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 684.094930000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades '
      'ORDER BY NOMEENTIDADE')
    Left = 208
    Top = 50
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 236
    Top = 50
  end
  object QrET: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrETCalcFields
    SQL.Strings = (
      'SELECT con.Codigo, con.Nome NO_CONTATO, '
      'crg.Nome NO_CARGO, con.DtaNatal, '
      'etc.Nome NOMEETC, emt.Telefone, emt.Ramal'
      'FROM enticontat con '
      'LEFT JOIN enticargos crg ON crg.Codigo=con.Cargo'
      'LEFT JOIN entitel emt ON emt.Controle=con.Controle'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto '
      'WHERE con.Codigo=:P0'
      'AND con.Ativo=1')
    Left = 328
    Top = 84
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrETNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrETCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrETTelefone: TWideStringField
      FieldName = 'Telefone'
    end
    object QrETRamal: TWideStringField
      FieldName = 'Ramal'
    end
    object QrETTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrETNO_CONTATO: TWideStringField
      FieldName = 'NO_CONTATO'
      Size = 30
    end
    object QrETNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrETDtaNatal: TDateField
      FieldName = 'DtaNatal'
    end
    object QrETNO_CARGO: TWideStringField
      FieldName = 'NO_CARGO'
      Size = 30
    end
  end
  object QrEM: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEMCalcFields
    SQL.Strings = (
      'SELECT con.Codigo, con.Nome NO_CONTATO, '
      'crg.Nome NO_CARGO, con.DtaNatal, '
      'etc.Nome NOMEETC, emt.EMail'
      'FROM enticontat con '
      'LEFT JOIN enticargos crg ON crg.Codigo=con.Cargo'
      'LEFT JOIN entimail emt ON emt.Controle=con.Controle'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto '
      'WHERE con.Codigo=:P0'
      'AND con.Ativo=1')
    Left = 328
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEMCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEMNO_CONTATO: TWideStringField
      FieldName = 'NO_CONTATO'
      Size = 30
    end
    object QrEMNO_CARGO: TWideStringField
      FieldName = 'NO_CARGO'
      Size = 30
    end
    object QrEMDtaNatal: TDateField
      FieldName = 'DtaNatal'
    end
    object QrEMNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrEMEMail: TWideStringField
      FieldName = 'EMail'
      Size = 255
    end
    object QrEMNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 10
      Calculated = True
    end
  end
  object frxDsEntiTel: TfrxDBDataset
    UserName = 'frxDsEntiTel'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEETC=NOMEETC'
      'Codigo=Codigo'
      'Telefone=Telefone'
      'Ramal=Ramal'
      'TEL_TXT=TEL_TXT'
      'NO_CONTATO=NO_CONTATO'
      'NATAL_TXT=NATAL_TXT'
      'DtaNatal=DtaNatal'
      'NO_CARGO=NO_CARGO')
    DataSet = QrET
    BCDToCurrency = False
    Left = 356
    Top = 84
  end
  object frxDsEntiEma: TfrxDBDataset
    UserName = 'frxDsEntiEma'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NO_CONTATO=NO_CONTATO'
      'NO_CARGO=NO_CARGO'
      'DtaNatal=DtaNatal'
      'NOMEETC=NOMEETC'
      'EMail=EMail'
      'NATAL_TXT=NATAL_TXT')
    DataSet = QrEM
    BCDToCurrency = False
    Left = 356
    Top = 112
  end
end
