unit EntiTel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral, Variants, dmkValUsu,
  dmkImage, UnDmkEnums;

type
  TFmEntiTel = class(TForm)
    Panel1: TPanel;
    QrEntiTipCto: TmySQLQuery;
    DsEntiTipCto: TDataSource;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdCodUsu: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    dmkValUsu1: TdmkValUsu;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    SpeedButton2: TSpeedButton;
    EdConta: TdmkEdit;
    EdTelefone: TdmkEdit;
    CBEntiTipCto: TdmkDBLookupComboBox;
    EdEntiTipCto: TdmkEditCB;
    DBEdContatoCod: TdmkDBEdit;
    Label1: TLabel;
    DBEdContatoNom: TDBEdit;
    Label7: TLabel;
    DBEdContatoCrg: TDBEdit;
    Label6: TLabel;
    QrEntiTipCtoCodigo: TIntegerField;
    QrEntiTipCtoCodUsu: TIntegerField;
    QrEntiTipCtoNome: TWideStringField;
    EdRamal: TdmkEdit;
    Label10: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEntiTel(Conta: Integer);
  public
    { Public declarations }
    FQrEntiTel, FQrEntidades, FQrEntiContat: TmySQLQuery;
    FDsEntiContat, FDsEntidades: TDataSource;
  end;

  var
  FmEntiTel: TFmEntiTel;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, Principal, EntiContat,
  MyDBCheck, DmkDAC_PF, UnEntities;

{$R *.DFM}

procedure TFmEntiTel.BtOKClick(Sender: TObject);
var
  Conta: Integer;
begin
  if DBEdCodigo.Text = '' then
    DBEdCodigo.UpdCampo := '';
  //
  if Trim(EdTelefone.Text) = '' then
  begin
    Geral.MB_Aviso('Informe o telefone do contato!');
    EdTelefone.SetFocus;
    Exit;
  end;
  Conta := UMyMod.BuscaEmLivreY_Def('entitel', 'Conta', ImgTipo.SQLType,
  EdConta.ValueVariant);
  //
  if UMyMod.ExecSQLInsUpdFm(FmEntiTel, ImgTipo.SQLType, 'entitel',
  Conta, Dmod.QrUpd) then
  begin
    DBEdCodigo.UpdCampo := 'Codigo';
    ReopenEntiTel(Conta);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdConta.ValueVariant      := 0;
      EdEntiTipCto.ValueVariant := 0;
      CBEntiTipCto.KeyValue     := Null;
      EdRamal.ValueVariant      := '';
      EdTelefone.ValueVariant   := '';
      EdTelefone.SetFocus;
    end else Close;
  end;
end;

procedure TFmEntiTel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiTel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiTel.FormCreate(Sender: TObject);
begin
  Entities.ReopenTipCto(QrEntiTipCto, etcTel);
end;

procedure TFmEntiTel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiTel.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := True;
  end else
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
  end;  
end;

procedure TFmEntiTel.ReopenEntiTel(Conta: Integer);
begin
  if (FQrEntiTel <> nil) and (FQrEntiContat <> nil) then
  begin
    FQrEntiTel.Close;
    if FQrEntiTel.Params.Count > 0 then
      FQrEntiTel.Params[0].AsInteger :=
        FQrEntiContat.FieldByName('Controle').AsInteger;
    UnDmkDAC_PF.AbreQuery(FQrEntiTel, Dmod.MyDB);
    //
    if Conta <> 0 then
      FQrEntiTel.Locate('Conta', Conta, []);
  end;
end;

procedure TFmEntiTel.SpeedButton2Click(Sender: TObject);
begin
  Entities.MostraFormMostraEntiTipCto(dmkValUsu1.ValueVariant, EdEntiTipCto,
    CBEntiTipCto, QrEntiTipCto);
end;

end.
