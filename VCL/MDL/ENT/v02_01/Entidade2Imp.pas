unit Entidade2Imp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkCheckGroup, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkCheckBox,
  Variants, dmkGeral, ComCtrls, Menus, frxClass, frxDBSet, dmkDBGrid, dmkImage,
  dmkEditDateTimePicker, UnDmkProcFunc, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmEntidade2Imp = class(TForm)
    Panel1: TPanel;
    PnFiltros: TPanel;
    BGFiltros: TGroupBox;
    CkUF: TdmkCheckBox;
    CBUF: TdmkDBLookupComboBox;
    CBCidade: TdmkDBLookupComboBox;
    CkCidade: TdmkCheckBox;
    CBPais: TdmkDBLookupComboBox;
    CkPais: TdmkCheckBox;
    CkBairro: TdmkCheckBox;
    CBBairro: TdmkDBLookupComboBox;
    CkLograd: TdmkCheckBox;
    CBLograd: TdmkDBLookupComboBox;
    CkCEP: TdmkCheckBox;
    EdCepIni: TdmkEdit;
    Label1: TLabel;
    EdCepFim: TdmkEdit;
    EdCNPJ: TdmkEdit;
    CkCNPJ: TdmkCheckBox;
    EdCPF: TdmkEdit;
    CkCPF: TdmkCheckBox;
    EdTelefone: TdmkEdit;
    CkRazao: TdmkCheckBox;
    CkTelefone: TdmkCheckBox;
    EdRazao: TdmkEdit;
    EdApelido: TdmkEdit;
    CkApelido: TdmkCheckBox;
    EdFantasia: TdmkEdit;
    CkFantasia: TdmkCheckBox;
    RGOrigem: TRadioGroup;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    DsUF: TDataSource;
    QrUF: TmySQLQuery;
    QrUFCodigo: TIntegerField;
    QrUFNome: TWideStringField;
    QrUFLk: TIntegerField;
    QrUFDataCad: TDateField;
    QrUFDataAlt: TDateField;
    QrUFUserCad: TIntegerField;
    QrUFUserAlt: TIntegerField;
    QrUFICMS_C: TFloatField;
    QrUFICMS_V: TFloatField;
    DsBacen_Pais: TDataSource;
    QrBacen_Pais: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsMunici: TDataSource;
    QrMunici: TmySQLQuery;
    QrMuniciCodigo: TIntegerField;
    QrMuniciNome: TWideStringField;
    DsBairros: TDataSource;
    QrBairros: TmySQLQuery;
    QrBairrosBAIRRO: TWideStringField;
    QrRuas: TmySQLQuery;
    QrRuasRua: TWideStringField;
    DsRuas: TDataSource;
    GroupBox4: TGroupBox;
    CkCliente1_0: TdmkCheckBox;
    CkFornece1_0: TdmkCheckBox;
    CkFornece2_0: TdmkCheckBox;
    CkFornece3_0: TdmkCheckBox;
    CkFornece4_0: TdmkCheckBox;
    CkTerceiro_0: TdmkCheckBox;
    CkCliente2_0: TdmkCheckBox;
    CkFornece5_0: TdmkCheckBox;
    CkFornece6_0: TdmkCheckBox;
    CkCliente4_0: TdmkCheckBox;
    CkCliente3_0: TdmkCheckBox;
    CkFornece8_0: TdmkCheckBox;
    CkFornece7_0: TdmkCheckBox;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesCNPJ_CPF: TWideStringField;
    QrEntidadesIE_RG: TWideStringField;
    QrEntidadesRUA: TWideStringField;
    QrEntidadesCompl: TWideStringField;
    QrEntidadesBairro: TWideStringField;
    QrEntidadesPTe1: TWideStringField;
    QrEntidadesETe1: TWideStringField;
    QrEntidadesNUM_TXT: TWideStringField;
    QrEntidadesETE1_TXT: TWideStringField;
    QrEntidadesCNPJ_CPF_TXT: TWideStringField;
    QrEntidadesPTE1_TXT: TWideStringField;
    QrEntidadesCEP_TXT: TWideStringField;
    QrEntidadesNOMEUF: TWideStringField;
    QrEntidadesRUA_NUM_COMPL: TWideStringField;
    QrEntidadesBAIRRO_CIDADE: TWideStringField;
    QrEntidadesCEP_UF_PAIS: TWideStringField;
    QrEntidadesNOMEFANTASIA: TWideStringField;
    QrEntidadesLimiCred: TFloatField;
    QrEntidadesCODIGO_NOME: TWideStringField;
    QrEntidadesNumero: TFloatField;
    DsEntidades: TDataSource;
    CkCliInt: TCheckBox;
    QrEntidadesDTB_MUNICI: TWideStringField;
    QrEntidadesBACEN_PAIS: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    DBGEntiMail: TDBGrid;
    DBGEntiTel: TDBGrid;
    DBGEntiContat: TDBGrid;
    DBGEntiCtas: TDBGrid;
    QrEntiCtas: TmySQLQuery;
    QrEntiCtasCodigo: TIntegerField;
    QrEntiCtasControle: TIntegerField;
    QrEntiCtasOrdem: TIntegerField;
    QrEntiCtasBanco: TIntegerField;
    QrEntiCtasAgencia: TIntegerField;
    QrEntiCtasContaCor: TWideStringField;
    QrEntiCtasContaTip: TWideStringField;
    QrEntiCtasDAC_A: TWideStringField;
    QrEntiCtasDAC_C: TWideStringField;
    QrEntiCtasDAC_AC: TWideStringField;
    QrEntiCtasLk: TIntegerField;
    QrEntiCtasDataCad: TDateField;
    QrEntiCtasDataAlt: TDateField;
    QrEntiCtasUserCad: TIntegerField;
    QrEntiCtasUserAlt: TIntegerField;
    QrEntiCtasAlterWeb: TSmallintField;
    QrEntiCtasAtivo: TSmallintField;
    QrEntiCtasNOMEBANCO: TWideStringField;
    DsEntiCtas: TDataSource;
    QrEntiContat: TmySQLQuery;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    QrEntiContatCargo: TIntegerField;
    QrEntiContatNOME_CARGO: TWideStringField;
    DsEntiContat: TDataSource;
    QrEntiTel: TmySQLQuery;
    QrEntiTelNOMEETC: TWideStringField;
    QrEntiTelConta: TIntegerField;
    QrEntiTelTelefone: TWideStringField;
    QrEntiTelEntiTipCto: TIntegerField;
    QrEntiTelTEL_TXT: TWideStringField;
    QrEntiTelRamal: TWideStringField;
    DsEntiTel: TDataSource;
    QrEntiMail: TmySQLQuery;
    QrEntiMailNOMEETC: TWideStringField;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailEntiTipCto: TIntegerField;
    QrEntiMailOrdem: TIntegerField;
    DsEntiMail: TDataSource;
    PMImprime: TPopupMenu;
    ListaTelefnica1: TMenuItem;
    Endereos1: TMenuItem;
    EMails1: TMenuItem;
    frxENT_GEREN_016_001: TfrxReport;
    frxDsLista: TfrxDBDataset;
    frsDsEntiContat: TfrxDBDataset;
    frxDsEntiTel: TfrxDBDataset;
    frxENT_GEREN_016_002: TfrxReport;
    frxDsEntiMail: TfrxDBDataset;
    DadosBancrios1: TMenuItem;
    frxDsEntiCtas: TfrxDBDataset;
    frxENT_GEREN_016_003: TfrxReport;
    dmkDBGrid1: TdmkDBGrid;
    frxENT_GEREN_016_004: TfrxReport;
    EdCodigo: TdmkEdit;
    CkCodigo: TdmkCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSair: TBitBtn;
    Panel6: TPanel;
    BtPesquisa: TBitBtn;
    BtImprime: TBitBtn;
    QrEntidadesCEP: TFloatField;
    QrEntidadesUF: TFloatField;
    CkAtivo: TCheckBox;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    QrEntiContatDtaNatal: TDateField;
    frxENT_GEREN_016_005: TfrxReport;
    ListadeAniversrios1: TMenuItem;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    TPDataFim: TDateTimePicker;
    TPDataIni: TDateTimePicker;
    Label3: TLabel;
    CkAniversario: TCheckBox;
    QrEntiContatDESCRI_TXT: TWideStringField;
    QrEntiContatNOMECARGO_TXT: TWideStringField;
    QrEntidadesNATAL_TXT: TWideStringField;
    QrEntiContatDTANATAL_TXT: TWideStringField;
    QrEntidadesENatal: TDateField;
    QrEntidadesPNatal: TDateField;
    QrEntidadesTipo: TIntegerField;
    QrEntidadesIDADE_TXT: TWideStringField;
    QrEntidadesAtivo: TSmallintField;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrEntidadesAfterClose(DataSet: TDataSet);
    procedure QrEntidadesAfterOpen(DataSet: TDataSet);
    procedure QrEntidadesCalcFields(DataSet: TDataSet);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrEntidadesAfterScroll(DataSet: TDataSet);
    procedure QrEntidadesBeforeClose(DataSet: TDataSet);
    procedure QrEntiContatAfterScroll(DataSet: TDataSet);
    procedure QrEntiContatBeforeClose(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure ListaTelefnica1Click(Sender: TObject);
    procedure frxENT_GEREN_016_001GetValue(const VarName: string;
      var Value: Variant);
    procedure QrEntiTelCalcFields(DataSet: TDataSet);
    procedure EMails1Click(Sender: TObject);
    procedure frxENT_GEREN_016_002GetValue(const VarName: string;
      var Value: Variant);
    procedure DadosBancrios1Click(Sender: TObject);
    procedure Endereos1Click(Sender: TObject);
    procedure frxENT_GEREN_016_003GetValue(const VarName: string;
      var Value: Variant);
    procedure frxENT_GEREN_016_004GetValue(const VarName: string;
      var Value: Variant);
    procedure BtSairClick(Sender: TObject);
    procedure frxENT_GEREN_016_005GetValue(const VarName: string;
      var Value: Variant);
    procedure ListadeAniversrios1Click(Sender: TObject);
    procedure QrEntiContatCalcFields(DataSet: TDataSet);
    procedure BtTudoClick(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure dmkDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure PesquisaEntidades(Codigo: Integer);
    procedure ReopenEntiCtas;
    procedure ReopenEntiContat(Controle: Integer);
    procedure ReopenEntiTel(Conta: Integer);
    procedure ReopenEntiMail(Conta: Integer);
    procedure MarcarTodos(Todos: Boolean);
    function CalculaIdade(const DtaNatal: TDateTime; var Sufixo: String): Integer;
  public
    { Public declarations }
  end;

  var
  FmEntidade2Imp: TFmEntidade2Imp;

implementation

uses UnMyObjects, UnInternalConsts, UnGOTOy, MyDBCheck, UMySQLModule, Module,
  ModuleGeral, DmkDAC_PF, UnEntities, UMySQLDB;

{$R *.DFM}

procedure TFmEntidade2Imp.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmEntidade2Imp.BtNenhumClick(Sender: TObject);
begin
  MarcarTodos(False);
end;

function TFmEntidade2Imp.CalculaIdade(const DtaNatal: TDateTime;
  var Sufixo: String): Integer;
var
  DiaN, MesN, AnoN, DiaA, MesA, AnoA: Word;
  Idade: Integer;
  NewDate: TDateTime;
begin
  DecodeDate(Date, AnoA, MesA, DiaA);
  DecodeDate(DtaNatal, AnoN, MesN, DiaN);
  //
  NewDate := EncodeDate(AnoA, MesN, DiaN);
  if AnoN > 1899 then
  begin
    if Date < NewDate then
      Idade := AnoA - AnoN - 1
    else
      Idade := AnoA - AnoN;
  end else
    Idade := 0;
  //
  if Idade = 1 then
    Sufixo := 'ano'
  else
    Sufixo := 'anos';
  //
  Result := Idade;
end;

procedure TFmEntidade2Imp.PesquisaEntidades(Codigo: Integer);
var
  Tels, Liga, Tipos, Ordem, OrdemNome, CEPi, CEPf: String;
  UF: Integer;
  DtaNatalI, DtaNatalF: TDateTime;
  DiaI, MesI, AnoI, DiaF, MesF, AnoF: Word;
begin
  if CBUF.KeyValue <> Null then
    UF := CBUF.KeyValue
  else
    UF := 0;
  //
  if  (CkCliente1_0.Checked = False)
  and (CkCliente2_0.Checked = False)
  and (CkCliente3_0.Checked = False)
  and (CkCliente4_0.Checked = False)
  and (CkFornece1_0.Checked = False)
  and (CkFornece2_0.Checked = False)
  and (CkFornece3_0.Checked = False)
  and (CkFornece4_0.Checked = False)
  and (CkFornece5_0.Checked = False)
  and (CkFornece6_0.Checked = False)
  and (CkFornece7_0.Checked = False)
  and (CkFornece8_0.Checked = False)
  and (CkTerceiro_0.Checked = False) then
  begin
    Geral.MB_Aviso('Nenhum tipo de cadastro foi definido!');
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    CEPi := Geral.SoNumero_TT(EdCEPIni.Text);
    CEPf := Geral.SoNumero_TT(EdCEPFim.Text);
    //
    Liga  := '';
    Tipos := '';
    //
    if CkCliente1_0.Checked then
    begin
      Tipos := 'Cliente1 = "V"';
      Liga  := ' OR ';
    end;
    if CkCliente2_0.Checked then
    begin
     Tipos := Tipos + Liga + 'Cliente2 = "V"';
     Liga  := ' OR ';
    end;
    if CkCliente3_0.Checked then
    begin
      Tipos := Tipos + Liga + 'Cliente3 = "V"';
      Liga  := ' OR ';
    end;
    if CkCliente4_0.Checked then
    begin
      Tipos := Tipos + Liga + 'Cliente4 = "V"';
      Liga  := ' OR ';
    end;
    if CkFornece1_0.Checked then
    begin
      Tipos := Tipos + Liga + 'Fornece1 = "V"';
      Liga  := ' OR ';
    end;
    if CkFornece2_0.Checked then
    begin
      Tipos := Tipos + Liga + 'Fornece2 = "V"';
      Liga  := ' OR ';
    end;
    if CkFornece3_0.Checked then
    begin
      Tipos := Tipos + Liga + 'Fornece3 = "V"';
      Liga  := ' OR ';
    end;
    if CkFornece4_0.Checked then
    begin
      Tipos := Tipos + Liga + 'Fornece4 = "V"';
      Liga  := ' OR ';
    end;
    if CkFornece5_0.Checked then
    begin
      Tipos := Tipos + Liga + 'Fornece5 = "V"';
      Liga  := ' OR ';
    end;
    if CkFornece6_0.Checked then
    begin
      Tipos := Tipos + Liga + 'Fornece6 = "V"';
      Liga  := ' OR ';
    end;
    if CkFornece7_0.Checked then
    begin
      Tipos := Tipos + Liga + 'Fornece7 = "V"';
      Liga  := ' OR ';
    end;
    if CkFornece8_0.Checked then
    begin
      Tipos := Tipos + Liga + 'Fornece8 = "V"';
      Liga  := ' OR ';
    end;
    if CkTerceiro_0.Checked then
    begin
      Tipos := Tipos + Liga + 'Terceiro = "V"';
    end;
    //
    case RGOrigem.ItemIndex of
      1:   OrdemNome := 'en.Nome';
      2:   OrdemNome := 'en.RazaoSocial';
      else OrdemNome := 'IF (en.Tipo=0, en.RazaoSocial, en.Nome)';
    end;
    //
    QrEntidades.Close;
    QrEntidades.SQL.Clear;
    QrEntidades.SQL.Add('SELECT en.Codigo, en.LimiCred, ');
    //
    case RGOrigem.ItemIndex of
      1:   QrEntidades.SQL.Add('en.Nome        NOMEENTIDADE,');
      2:   QrEntidades.SQL.Add('en.RazaoSocial NOMEENTIDADE,');
      else QrEntidades.SQL.Add('IF (en.Tipo=0, en.RazaoSocial, en.Nome) NOMEENTIDADE,');
    end;
    //
    case RGOrigem.ItemIndex of
      1:   QrEntidades.SQL.Add('en.Apelido     NOMEFANTASIA,');
      2:   QrEntidades.SQL.Add('en.Fantasia    NOMEFANTASIA,');
      else QrEntidades.SQL.Add('IF (en.Tipo=0, en.Fantasia, en.Apelido) NOMEFANTASIA,');
    end;
    //
    case RGOrigem.ItemIndex of
      1:   QrEntidades.SQL.Add('en.CPF  CNPJ_CPF,');
      2:   QrEntidades.SQL.Add('en.CNPJ CNPJ_CPF,');
      else QrEntidades.SQL.Add('IF (en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF,');
    end;
    //
    case RGOrigem.ItemIndex of
      1:   QrEntidades.SQL.Add('en.RG  IE_RG,');
      2:   QrEntidades.SQL.Add('en.IE IE_RG,');
      else QrEntidades.SQL.Add('IF (en.Tipo=0, en.IE, en.RG) IE_RG,');
    end;
    //
    case RGOrigem.ItemIndex of
      0:
      begin
        QrEntidades.SQL.Add('IF (Tipo=0, en.ERua,    en.PRua    ) RUA,');
        QrEntidades.SQL.Add('IF (Tipo=0, en.ENumero+0.000, en.PNumero+0.000 ) Numero,');
        QrEntidades.SQL.Add('IF (Tipo=0, en.ECompl,  en.PCompl  ) Compl      ,');
        QrEntidades.SQL.Add('IF (Tipo=0, en.EBairro, en.PBairro ) Bairro     ,');
        QrEntidades.SQL.Add('IF (Tipo=0, mue.Nome, mup.Nome     ) DTB_MUNICI ,');
        QrEntidades.SQL.Add('IF (Tipo=0, en.EUF+0.000, en.PUF+0.000) UF      ,');
        QrEntidades.SQL.Add('IF (Tipo=0, en.ECEP+0.000, en.PCEP+0.000) CEP   ,');
        QrEntidades.SQL.Add('IF (Tipo=0, pae.Nome,   pap.Nome   ) BACEN_PAIS ,');
      end;
      1:
      begin
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.PNumero+0.000, en.PNumero+0.000 ) Numero,');
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.PUF+0.000, en.PUF+0.000 ) UF,');
        QrEntidades.SQL.Add('en.PRua RUA, en.PCompl COMPL, en.PBairro BAIRRO,');
        QrEntidades.SQL.Add('mup.Nome DTB_MUNICI, pap.Nome BACEN_PAIS, ');
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.PCEP+0.000, en.PCEP+0.000) CEP,');
      end;
      2:
      begin
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.ENumero+0.000, en.ENumero+0.000 ) Numero,');
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.EUF+0.000, en.EUF+0.000) UF,');
        QrEntidades.SQL.Add('en.ERua RUA, en.ECompl COMPL, en.EBairro BAIRRO,');
        QrEntidades.SQL.Add('mue.Nome DTB_MUNICI, pae.Nome BACEN_PAIS, ');
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.ECEP+0.000, en.ECEP+0.000) CEP   ,');
      end;
      3:
      begin
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.CNumero+0.000, en.CNumero+0.000 ) Numero,');
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.CUF+0.000, en.CUF+0.000) UF,');
        QrEntidades.SQL.Add('en.CRua RUA, en.CCompl COMPL, en.CBairro BAIRRO,');
        QrEntidades.SQL.Add('muc.Nome DTB_MUNICI,');
        QrEntidades.SQL.Add('IF (Tipo=0, pae.Nome,   pap.Nome   ) BACEN_PAIS,');
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.CCEP+0.000, en.CCEP+0.000) CEP   ,');
      end;
      4:
      begin
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.LNumero+0.000, en.LNumero+0.000 ) Numero,');
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.LUF+0.000, en.LUF+0.000) UF,');
        QrEntidades.SQL.Add('en.LRua RUA, en.LCompl COMPL, en.LBairro BAIRRO,');
        QrEntidades.SQL.Add('mul.Nome DTB_MUNICI,');
        QrEntidades.SQL.Add('IF (en.Tipo=0, pae.Nome,   pap.Nome   ) BACEN_PAIS,');
        QrEntidades.SQL.Add('IF (en.Tipo=0, en.LCEP+0.000,    en.LCEP+0.000) CEP   ,');
      end;
    end;
    //
    QrEntidades.SQL.Add('en.Tipo, en.ENatal, en.PNatal,');
    QrEntidades.SQL.Add('IF (en.Tipo=0, ufe.Nome,   ufp.Nome   ) NOMEUF, en.PTe1, en.ETe1, en.Ativo');
    //
    QrEntidades.SQL.Add('FROM entidades en');
    QrEntidades.SQL.Add('LEFT JOIN ufs ufe ON ufe.Codigo = en.EUF');
    QrEntidades.SQL.Add('LEFT JOIN ufs ufp ON ufp.Codigo = en.PUF');
    QrEntidades.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pae ON pae.Codigo=en.ECodiPais');
    QrEntidades.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pap ON pap.Codigo=en.PCodiPais');
    QrEntidades.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mue ON mue.Codigo=en.ECodMunici');
    QrEntidades.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mup ON mup.Codigo=en.PCodMunici');
    QrEntidades.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici muc ON muc.Codigo=en.CCodMunici');
    QrEntidades.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mul ON mul.Codigo=en.LCodMunici');
    //
    if CkAniversario.Checked then
      QrEntidades.SQL.Add('LEFT JOIN enticontat con ON con.Codigo = en.Codigo ');
    //
    QrEntidades.SQL.Add('WHERE en.Codigo>0');
    QrEntidades.SQL.Add('AND ('+Tipos+')');
    //
    if CkUF.Checked then
    begin
      case RGOrigem.ItemIndex of
        0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.EUF, en.PUF))='+IntToStr(UF));
        1: QrEntidades.SQL.Add('AND en.PUF='+IntToStr(UF));
        2: QrEntidades.SQL.Add('AND en.EUF='+IntToStr(UF));
        3: QrEntidades.SQL.Add('AND en.CUF='+IntToStr(UF));
        4: QrEntidades.SQL.Add('AND en.LUF='+IntToStr(UF));
      end;
    end;
    //
    if CkCidade.Checked then
    begin
      case RGOrigem.ItemIndex of
        0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.ECodMunici, en.PCodMunici))="'+ FormatFloat('0', CBCidade.KeyValue) +'"');
        1: QrEntidades.SQL.Add('AND en.PCodMunici="'+ FormatFloat('0', CBCidade.KeyValue) +'"');
        2: QrEntidades.SQL.Add('AND en.ECodMunici="'+ FormatFloat('0', CBCidade.KeyValue) +'"');
        3: QrEntidades.SQL.Add('AND en.CCodMunici="'+ FormatFloat('0', CBCidade.KeyValue) +'"');
        4: QrEntidades.SQL.Add('AND en.LCodMunici="'+ FormatFloat('0', CBCidade.KeyValue) +'"');
      end;
    end;
    if CkPais.Checked then
    begin
      case RGOrigem.ItemIndex of
        0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.ECodiPais, en.PCodiPais))="'+ FormatFloat('0', CBPais.KeyValue) +'"');
        1: QrEntidades.SQL.Add('AND en.PCodiPais="'+ FormatFloat('0', CBPais.KeyValue) +'"');
        2: QrEntidades.SQL.Add('AND en.ECodiPais="'+ FormatFloat('0', CBPais.KeyValue) +'"');
        3: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.ECodiPais, en.PCodiPais))="'+ FormatFloat('0', CBPais.KeyValue) +'"');
        4: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.ECodiPais, en.PCodiPais))="'+ FormatFloat('0', CBPais.KeyValue) +'"');
      end;
    end;
    if CkBairro.Checked then
    begin
      case RGOrigem.ItemIndex of
        0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.EBairro, en.PBairro))="'+CBBairro.Text+'"');
        1: QrEntidades.SQL.Add('AND en.PBairro="'+CBBairro.Text+'"');
        2: QrEntidades.SQL.Add('AND en.EBairro="'+CBBairro.Text+'"');
        3: QrEntidades.SQL.Add('AND en.CBairro="'+CBBairro.Text+'"');
        4: QrEntidades.SQL.Add('AND en.LBairro="'+CBBairro.Text+'"');
      end;
    end;
    if CkLograd.Checked then
    begin
      case RGOrigem.ItemIndex of
        0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.ERua, en.PRua))="'+CBLograd.Text+'"');
        1: QrEntidades.SQL.Add('AND en.PRua="'+CBLograd.Text+'"');
        2: QrEntidades.SQL.Add('AND en.ERua="'+CBLograd.Text+'"');
        3: QrEntidades.SQL.Add('AND en.CRua="'+CBLograd.Text+'"');
        4: QrEntidades.SQL.Add('AND en.LRua="'+CBLograd.Text+'"');
      end;
    end;
    if CkTelefone.Checked then
    begin
      Tels := Geral.SoNumeroELetra_TT(EdTelefone.ValueVariant);
      case RGOrigem.ItemIndex of
        0, 3, 4: QrEntidades.SQL.Add('AND (PTe1 LIKE "%'+Tels+'%" OR ETe1 LIKE "%'+Tels+'%")');
        1:       QrEntidades.SQL.Add('AND (PTe1 LIKE "%'+Tels+'%"');
        2:       QrEntidades.SQL.Add('AND (ETe1 LIKE "%'+Tels+'%"');
      end;
    end;
    if CkCEP.Checked then
    begin
      case RGOrigem.ItemIndex of
        0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.ECEP, en.PCEP)) BETWEEN "'+CEPi+'" AND "'+CEPf+'"');
        1: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.PCEP, en.PCEP)) BETWEEN "'+CEPi+'" AND "'+CEPf+'"');
        2: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.ECEP, en.ECEP)) BETWEEN "'+CEPi+'" AND "'+CEPf+'"');
        3: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.CCEP, en.CCEP)) BETWEEN "'+CEPi+'" AND "'+CEPf+'"');
        4: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.LCEP, en.LCEP)) BETWEEN "'+CEPi+'" AND "'+CEPf+'"');
      end;
    end;
    case RGOrigem.ItemIndex of
      1: QrEntidades.SQL.Add('AND en.Tipo=1');
      2: QrEntidades.SQL.Add('AND en.Tipo=0');
    end;
    //
    if CkCPF.Checked then
      QrEntidades.SQL.Add('AND en.CPF="'+ Geral.FormataCNPJ_TFT(EdCPF.Text) +'"');
    if CkCNPJ.Checked then
      QrEntidades.SQL.Add('AND en.CNPJ="'+ Geral.FormataCNPJ_TFT(EdCNPJ.Text) +'"');
    //
    if CkApelido.Checked then
      QrEntidades.SQL.Add('AND en.Apelido LIKE "%'+EdApelido.Text+'%"');
    if CkFantasia.Checked then
      QrEntidades.SQL.Add('AND en.Fantasia LIKE "%'+EdFantasia.Text+'%"');
    if CkRazao.Checked then
      QrEntidades.SQL.Add('AND IF(en.Tipo=0, en.RazaoSocial, en.Nome) LIKE "%'+EdRazao.Text+'%"');
    if CkCliInt.Checked then
      QrEntidades.SQL.Add('AND en.CliInt <> 0');
    if CkAtivo.Checked then
      QrEntidades.SQL.Add('AND en.Ativo = 1');
    if CkCodigo.Checked then
      QrEntidades.SQL.Add('AND en.Codigo = ' + FormatFloat('0', EdCodigo.ValueVariant));
    //
    if CkAniversario.Checked then
    begin
      DtaNatalI := TPDataIni.Date;
      DecodeDate(DtaNatalI, AnoI, MesI, DiaI);
      DtaNatalF := TPDataFim.Date;
      DecodeDate(DtaNatalF, AnoF, MesF, DiaF);
      //
      DtaNatalI := TPDataIni.Date;
      DecodeDate(DtaNatalI, AnoI, MesI, DiaI);
      DtaNatalF := TPDataFim.Date;
      DecodeDate(DtaNatalF, AnoF, MesF, DiaF);
      //
      QrEntidades.SQL.Add('AND ');
      QrEntidades.SQL.Add('( ');
      QrEntidades.SQL.Add('  con.DtaNatal > "1900-01-01" ');
      QrEntidades.SQL.Add('  OR ');
      QrEntidades.SQL.Add('  IF(en.Tipo=0, en.ENatal, en.PNatal) > "1900-01-01" ');
      QrEntidades.SQL.Add(') ');
      QrEntidades.SQL.Add('AND ( ');
      QrEntidades.SQL.Add('  (MONTH(con.DtaNatal) >= ' + Geral.FF0(MesI) + ') AND (DAY(con.DtaNatal) >= '+ Geral.FF0(DiaI) +') ');
      QrEntidades.SQL.Add('  OR ');
      QrEntidades.SQL.Add('  ( ');
      QrEntidades.SQL.Add('  IF(en.Tipo=0, MONTH(en.ENatal), MONTH(en.PNatal)) >= '+ Geral.FF0(MesI));
      QrEntidades.SQL.Add('  AND ');
      QrEntidades.SQL.Add('  IF(en.Tipo=0, DAY(en.ENatal), DAY(en.PNatal)) >= '+ Geral.FF0(DiaI));
      QrEntidades.SQL.Add('  ) ');
      QrEntidades.SQL.Add(') ');
      QrEntidades.SQL.Add('AND ( ');
      QrEntidades.SQL.Add('  (MONTH(con.DtaNatal) <= '+ Geral.FF0(MesF) +') AND (DAY(con.DtaNatal) <= '+ Geral.FF0(DiaF) +') ');
      QrEntidades.SQL.Add('  OR ');
      QrEntidades.SQL.Add('  ( ');
      QrEntidades.SQL.Add('  IF(en.Tipo=0, MONTH(en.ENatal), MONTH(en.PNatal)) <= '+ Geral.FF0(MesF));
      QrEntidades.SQL.Add('  AND ');
      QrEntidades.SQL.Add('  IF(en.Tipo=0, DAY(en.ENatal), DAY(en.PNatal)) <= '+ Geral.FF0(MesF));
      QrEntidades.SQL.Add('  ) ');
      QrEntidades.SQL.Add(') ');
      QrEntidades.SQL.Add('GROUP BY en.Codigo');
    end;
    //////////////////////////////////////////////////////////////////////////////
    Ordem := OrdemNome;
    QrEntidades.SQL.Add('ORDER BY ' + Ordem);
    //
    UMyMod.AbreQuery(QrEntidades, Dmod.MyDB, ' > TFmEntidade2Imp.BtPesquisaClick()');
    //
    if Codigo <> 0 then
      QrEntidades.Locate('Codigo', Codigo, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEntidade2Imp.BtPesquisaClick(Sender: TObject);
begin
  PesquisaEntidades(0);
end;

procedure TFmEntidade2Imp.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntidade2Imp.BtTudoClick(Sender: TObject);
begin
  MarcarTodos(True);
end;

procedure TFmEntidade2Imp.DadosBancrios1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxENT_GEREN_016_003, 'Dados banc�rios');
end;

procedure TFmEntidade2Imp.DBGrid1DblClick(Sender: TObject);
begin
  VAR_ENTIDADE := QrEntidadesCodigo.Value;
  Close;
end;

procedure TFmEntidade2Imp.dmkDBGrid1CellClick(Column: TColumn);
var
  Codigo, Ativo: Integer;
begin
  if (Column.FieldName = 'Ativo') and (VAR_SENHA = CO_MASTER) then
  begin
    Ativo  := QrEntidadesAtivo.Value;
    Codigo := QrEntidadesCodigo.Value;
    //
    USQLDB.AtualizaCampoAtivo_Unico(Dmod.QrUpd, 'entidades', 'Ativo', 'Codigo', Ativo,
      Codigo, scaInverte);
    //
    PesquisaEntidades(Codigo);
  end;
end;

procedure TFmEntidade2Imp.dmkDBGrid1DblClick(Sender: TObject);
var
  Campo, Codigo: String;
begin
  if (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0) then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if (LowerCase(Campo) = 'codigo') then
    begin
      if InputQuery('C�digo da entidade', 'Defina o c�digo da entidade', Codigo) then
      begin
        if Codigo <> '' then
        begin
          if not QrEntidades.Locate('Codigo', Codigo, []) then
            Geral.MB_Aviso('O c�digo informado n�o foi localizado!');
        end;
      end;
    end;
  end;
end;

procedure TFmEntidade2Imp.EMails1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxENT_GEREN_016_002, 'Lista de E-mails');
end;

procedure TFmEntidade2Imp.Endereos1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxENT_GEREN_016_004, 'Lista de Endere�os');
end;

procedure TFmEntidade2Imp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntidade2Imp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  PageControl1.ActivePageIndex := 0;
  //
  Entities.ConfiguraTipoDeCadastro(CkCliente1_0, CkCliente2_0, CkCliente3_0,
    CkCliente4_0, CkFornece1_0, CkFornece2_0, CkFornece3_0, CkFornece4_0,
    CkFornece5_0, CkFornece6_0, CkFornece7_0, CkFornece8_0, CkTerceiro_0);
  //
  MarcarTodos(True);
  //
  UnDmkDAC_PF.AbreQuery(QrBacen_Pais, DmodG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrUF, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMunici, DmodG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrBairros, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrRuas, Dmod.MyDB);
end;

procedure TFmEntidade2Imp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidade2Imp.frxENT_GEREN_016_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'NOMEREL' then
    Value := 'LISTA TELEF�NICA';
end;

procedure TFmEntidade2Imp.frxENT_GEREN_016_002GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'NOMEREL' then
    Value := 'LISTA DE E-MAILS';
end;

procedure TFmEntidade2Imp.frxENT_GEREN_016_003GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'NOMEREL' then
    Value := 'LISTA DE DADOS BANC�RIOS';
end;

procedure TFmEntidade2Imp.frxENT_GEREN_016_004GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'NOMEREL' then
    Value := 'LISTA DE ENDERE�OS';
end;

procedure TFmEntidade2Imp.frxENT_GEREN_016_005GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'NOMEREL' then
    Value := 'LISTA DE ANIVERS�RIOS';
end;

procedure TFmEntidade2Imp.ListadeAniversrios1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxENT_GEREN_016_005, 'Lista de Anivers�rios');
end;

procedure TFmEntidade2Imp.ListaTelefnica1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxENT_GEREN_016_001, 'Lista Telef�nica');
end;

procedure TFmEntidade2Imp.MarcarTodos(Todos: Boolean);
begin
  if CkCliente1_0.Visible then CkCliente1_0.Checked := Todos;
  if CkCliente2_0.Visible then CkCliente2_0.Checked := Todos;
  if CkCliente3_0.Visible then CkCliente3_0.Checked := Todos;
  if CkCliente4_0.Visible then CkCliente4_0.Checked := Todos;
  if CkFornece1_0.Visible then CkFornece1_0.Checked := Todos;
  if CkFornece2_0.Visible then CkFornece2_0.Checked := Todos;
  if CkFornece3_0.Visible then CkFornece3_0.Checked := Todos;
  if CkFornece4_0.Visible then CkFornece4_0.Checked := Todos;
  if CkFornece5_0.Visible then CkFornece5_0.Checked := Todos;
  if CkFornece6_0.Visible then CkFornece6_0.Checked := Todos;
  if CkFornece7_0.Visible then CkFornece7_0.Checked := Todos;
  if CkFornece8_0.Visible then CkFornece8_0.Checked := Todos;
  if CkTerceiro_0.Visible then CkTerceiro_0.Checked := Todos;
end;

procedure TFmEntidade2Imp.QrEntiContatAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiMail(0);
  ReopenEntiTel(0);
end;

procedure TFmEntidade2Imp.QrEntiContatBeforeClose(DataSet: TDataSet);
begin
  QrEntiMail.Close;
  QrEntiTel.Close;
end;

procedure TFmEntidade2Imp.QrEntiContatCalcFields(DataSet: TDataSet);
var
  Texto, NomeCargo, Sufixo: String;
  Idade: Integer;
begin
  Texto := QrEntiContatNome.Value;
  if QrEntiContatCargo.Value <> 0 then
    Texto := Texto + ' (' + QrEntiContatNOME_CARGO.Value + ')';
  Idade := CalculaIdade(QrEntiContatDtaNatal.Value, Sufixo);
  //
  if Idade <> 0 then
  begin
    Texto := Texto + ' ' + Geral.FF0(Idade) + ' ' + Sufixo;
  end;
  NomeCargo := QrEntiContatNome.Value;
  if QrEntiContatCargo.Value <> 0 then
    NomeCargo := NomeCargo + ' (' + QrEntiContatNOME_CARGO.Value + ')';
  //
  QrEntiContatDESCRI_TXT.Value    := Texto;
  QrEntiContatNOMECARGO_TXT.Value := NomeCargo;
  QrEntiContatDTANATAL_TXT.Value  := dmkPF.FDT_NULO(QrEntiContatDtaNatal.Value, 12);
end;

procedure TFmEntidade2Imp.QrEntidadesAfterClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmEntidade2Imp.QrEntidadesAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := True;
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Registros encontrados: ' + IntToStr(QrEntidades.RecordCount));
end;

procedure TFmEntidade2Imp.QrEntidadesAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiContat(0);
  ReopenEntiCtas;
end;

procedure TFmEntidade2Imp.QrEntidadesBeforeClose(DataSet: TDataSet);
begin
  QrEntiContat.Close;
  QrEntiCtas.Close;
end;

procedure TFmEntidade2Imp.QrEntidadesCalcFields(DataSet: TDataSet);
var
  Idade: Integer;
  Sufixo: String;
begin
  if QrEntidadesNumero.Value = 0 then
     QrEntidadesNUM_TXT.Value := 'S/N'
  else
     QrEntidadesNUM_TXT.Value := FloatToStr(QrEntidadesNumero.Value);
  //
  QrEntidadesETE1_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe1.Value);
  QrEntidadesCNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCNPJ_CPF.Value);
  //
  QrEntidadesPTE1_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe1.Value);
  //
  QrEntidadesCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntidadesCEP.Value);
  ////
  QrEntidadesCODIGO_NOME.Value := FormatFloat('000000', QrEntidadesCodigo.Value) +
    QrEntidadesNOMEENTIDADE.Value;
  QrEntidadesRUA_NUM_COMPL.Value := QrEntidadesRUA.Value + ', ' +
    QrEntidadesNUM_TXT.Value + '   '+ QrEntidadesCompl.Value;
  QrEntidadesBAIRRO_CIDADE.Value := 'Bairro '+QrEntidadesBairro.Value + ' - ' +
    QrEntidadesDTB_MUNICI.Value;
  QrEntidadesCEP_UF_PAIS.Value := QrEntidadesCEP_TXT.Value + '    ' +
    QrEntidadesNOMEUF.Value + '   '+ QrEntidadesBACEN_PAIS.Value;
  //
  if QrEntidadesTipo.Value = 0 then
  begin
    QrEntidadesNATAL_TXT.Value := dmkPF.FDT_NULO(QrEntidadesENatal.Value, 12);
    //
    Idade := CalculaIdade(QrEntidadesENatal.Value, Sufixo);
  end else
  begin
    QrEntidadesNATAL_TXT.Value := dmkPF.FDT_NULO(QrEntidadesPNatal.Value, 12);
    //
    Idade := CalculaIdade(QrEntidadesPNatal.Value, Sufixo);
  end;
  //
  if Idade <> 0 then
    QrEntidadesIDADE_TXT.Value := Geral.FF0(Idade) + ' ' + Sufixo
  else
    QrEntidadesIDADE_TXT.Value := '';
end;

procedure TFmEntidade2Imp.QrEntiTelCalcFields(DataSet: TDataSet);
begin
  QrEntiTelTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntiTelTelefone.Value);
end;

procedure TFmEntidade2Imp.ReopenEntiContat(Controle: Integer);
var
  DtaNatalI, DtaNatalF: TDateTime;
  DiaI, MesI, AnoI, DiaF, MesF, AnoF: Word;
begin
  QrEntiContat.Close;
  QrEntiContat.SQL.Clear;
  QrEntiContat.SQL.Add('SELECT eco.Controle, eco.Nome, eco.Cargo,');
  QrEntiContat.SQL.Add('eca.Nome NOME_CARGO, eco.DtaNatal,');
  QrEntiContat.SQL.Add('IF(eco.DtaNatal = 0, "", DATE_FORMAT(eco.DtaNatal, "%d/%m")) DTANATAL_TXT');
  QrEntiContat.SQL.Add('FROM enticontat eco');
  QrEntiContat.SQL.Add('LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo');
  QrEntiContat.SQL.Add('WHERE eco.Codigo=:P0');
  //
  if CkAniversario.Checked then
  begin
    DtaNatalI := TPDataIni.Date;
    DecodeDate(DtaNatalI, AnoI, MesI, DiaI);
    DtaNatalF := TPDataFim.Date;
    DecodeDate(DtaNatalF, AnoF, MesF, DiaF);
    //
    QrEntiContat.SQL.Add('AND eco.DtaNatal > "1900-01-01" ');
    QrEntiContat.SQL.Add('AND ( ');
    QrEntiContat.SQL.Add('  (MONTH(eco.DtaNatal) >= '+ Geral.FF0(MesI) +') AND (DAY(eco.DtaNatal) >= '+ Geral.FF0(DiaI) +') ');
    QrEntiContat.SQL.Add(') ');
    QrEntiContat.SQL.Add('AND ( ');
    QrEntiContat.SQL.Add('  (MONTH(eco.DtaNatal) <= '+ Geral.FF0(MesF) +') AND (DAY(eco.DtaNatal) <= '+ Geral.FF0(DiaF) +') ');
    QrEntiContat.SQL.Add(') ');
    QrEntiContat.SQL.Add('ORDER BY eco.DtaNatal');
  end else
    QrEntiContat.SQL.Add('ORDER BY eco.Nome');
  QrEntiContat.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrEntiContat, Dmod.MyDB, ' > TFmEntidade2Imo.ReopenEntiContat()');
  //
  if Controle <> 0 then
    QrEntiContat.Locate('Controle', Controle, []);
end;

procedure TFmEntidade2Imp.ReopenEntiTel(Conta: Integer);
begin
  QrEntiTel.Close;
  QrEntiTel.Params[0].AsInteger := QrEntiContatControle.Value;
  UnDmkDAC_PF.AbreQuery(QrEntiTel, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiTel.Locate('Conta', Conta, []);
end;

procedure TFmEntidade2Imp.ReopenEntiCtas;
begin
  QrEntiCtas.Close;
  QrEntiCtas.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrEntiCtas, Dmod.MyDB);
end;

procedure TFmEntidade2Imp.ReopenEntiMail(Conta: Integer);
begin
  QrEntiMail.Close;
  QrEntiMail.Params[0].AsInteger := QrEntiContatControle.Value;
  UnDmkDAC_PF.AbreQuery(QrEntiMail, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiMail.Locate('Conta', Conta, []);
end;

end.
