// ini Delphi 28 Alexandria
//{$I dmk.inc}
// ini Delphi 28 Alexandria

unit EntiImagens;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, DBCGrids, jpeg, Menus, ActnPopup, DmkDAC_PF, UnDmkProcFunc,
  ExtDlgs, UnDmkEnums;

type
  TFmEntiImagens = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrImagens: TmySQLQuery;
    DsImagens: TDataSource;
    QrEntidadesAtivo: TSmallintField;
    QrImagensCodigo: TIntegerField;
    QrImagensNome: TWideStringField;
    QrImagensTipImg: TFloatField;
    QrImagensNOMETIP_TXT: TWideStringField;
    PMImagem: TPopupMenu;
    irarfoto1: TMenuItem;
    Carregarimagem1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Splitter1: TSplitter;
    DBCtrlGrid1: TDBCtrlGrid;
    N1: TMenuItem;
    VisualizarImagem1: TMenuItem;
    Image2: TImage;
    Panel5: TPanel;
    DBText4: TDBText;
    DBText3: TDBText;
    QrLoc: TmySQLQuery;
    QrImagensNomeImg: TWideStringField;
    QrImagensCodImg: TIntegerField;
    Image1: TImage;
    OpenPictureDialog1: TOpenPictureDialog;
    SbDiretorio: TBitBtn;
    Panel6: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    EdPesquisa: TdmkEdit;
    BtImagem: TBitBtn;
    N2: TMenuItem;
    Removerimagem1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrImagensCalcFields(DataSet: TDataSet);
    procedure QrEntidadesAfterScroll(DataSet: TDataSet);
    procedure QrEntidadesBeforeClose(DataSet: TDataSet);
    procedure VisualizarImagem1Click(Sender: TObject);
    procedure Image1DblClick(Sender: TObject);
    procedure irarfoto1Click(Sender: TObject);
    procedure DBCtrlGrid1PaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
    procedure Image2Click(Sender: TObject);
    procedure Panel5DblClick(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Carregarimagem1Click(Sender: TObject);
    procedure SbDiretorioClick(Sender: TObject);
    procedure EdPesquisaChange(Sender: TObject);
    procedure DBCtrlGrid1Click(Sender: TObject);
    procedure EdPesquisaEnter(Sender: TObject);
    procedure BtImagemClick(Sender: TObject);
    procedure Removerimagem1Click(Sender: TObject);
  private
    { Private declarations }
    FImage, FCamAvatar: String;
    procedure SalvaDadosBD(NomeImg: String; CodEnti, Tipo: Integer);
    procedure CarregaFotoAvatarURL();
  public
    { Public declarations }
    FCodigo, FCodEnti, FTipImg: Integer;
    FMostraImagem: Boolean;
    //
    procedure ReopenEntidades(Codigo: Integer; Texto: String);
    procedure ReopenEntiImgs(Codigo, CodEnti, TipImg: Integer);
    procedure VisualizaImagem();
  end;

  var
    FmEntiImagens: TFmEntiImagens;

  const
    FComplCaminho = 'Imagens\Entidades\';

implementation

uses UnMyObjects, Module, UMySQLModule, MyGlyfs, MyDBCheck,
{$IfDef VER_BEFORE_BERLIN}
  ObtemFoto4,
{$Else}
  // TODO Berlin
  ObtemFoto4,
{$EndIf}


  ModuleGeral, UnDmkWeb;

{$R *.DFM}

const
  FAvatar = 'C:\Dermatek\Imagens\avatar.jpg';

procedure TFmEntiImagens.BtImagemClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImagem, BtImagem);
end;

procedure TFmEntiImagens.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiImagens.CarregaFotoAvatarURL();
var
  Versao: Int64;
  Arquivo: String;
begin
  if DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_AVATAR, 'Avatar',
    Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), 0, 0, DModG.ObtemAgora(), nil,
    dtImag, Versao, Arquivo, False) = True then
  begin
    if not DirectoryExists(ExtractFilePath(FCamAvatar)) then
      ForceDirectories(ExtractFilePath(FCamAvatar));
    //
    CopyFile(PChar(FAvatar), PChar(FCamAvatar), True);
  end;
end;

procedure TFmEntiImagens.Carregarimagem1Click(Sender: TObject);
var
  Diretorio, NomeArq, Caminho, Extensao: String;
begin
  if OpenPictureDialog1.Execute then
  begin
    Diretorio := DModG.QrCtrlGeralDmkNetPath.Value;
    if Length(Diretorio) = 0 then
    begin
       Geral.MB_Info('Diret�rio n�o definido!' + sLineBreak +
        'Defina o diret�rio nas op��es do aplicativo e tente novamente.');
       Exit;
    end else
      Diretorio := Diretorio + FComplCaminho;
    //
    Caminho  := OpenPictureDialog1.FileName;
    Extensao := ExtractFileExt(Caminho);
    NomeArq  := FormatDateTime('yymmdd_hhnnss_zzz', Now) + Extensao; //Para n�o duplicar o nome
    //
    Image2.Picture.LoadFromFile(Caminho);
    Image2.Picture.SaveToFile(Diretorio + NomeArq);
    //
    SalvaDadosBD(NomeArq, QrImagensCodigo.Value, Trunc(QrImagensTipImg.Value));
  end;
end;

procedure TFmEntiImagens.DBCtrlGrid1Click(Sender: TObject);
begin
  DBCtrlGrid1.SetFocus;
end;

procedure TFmEntiImagens.DBCtrlGrid1PaintPanel(DBCtrlGrid: TDBCtrlGrid;
  Index: Integer);
var
  CamImagem: String;
  //ImgBmp: TBitmap;
begin
  CamImagem := DModG.QrCtrlGeralDmkNetPath.Value + FComplCaminho + QrImagensNomeImg.Value;
  //
  if FileExists(CamImagem) then
    CamImagem := CamImagem
  else if FileExists(FCamAvatar) then
    CamImagem := FCamAvatar
  else
    CamImagem := '';
  //
  if FImage <> CamImagem then
  begin
    Image1.Picture.LoadFromFile(CamImagem);
    {
    //
    ImgBmp := TBitmap.Create;
    ImgBmp.Height := 48;
    ImgBmp.Width  := 48;
    ImgBmp.Assign(Image1.Picture.Graphic);
    dmkPF.ResizeImage_Bitmap_Novo(ImgBmp, 48, 48);
    //
    DBCtrlGrid1.Canvas.Draw(8, 8, ImgBmp);
    //
    }
    FImage := CamImagem;
  end;
end;

procedure TFmEntiImagens.EdPesquisaChange(Sender: TObject);
begin
  ReopenEntidades(0, EdPesquisa.ValueVariant);
end;

procedure TFmEntiImagens.EdPesquisaEnter(Sender: TObject);
begin
  EdPesquisa.ValueVariant := '';
end;

procedure TFmEntiImagens.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiImagens.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(DModG.QrCtrlGeral, DMod.MyDB);
  //
  if (Length(DModG.QrCtrlGeralDmkNetPath.Value) > 0) and (DirectoryExists(DModG.QrCtrlGeralDmkNetPath.Value)) then
  begin
    PageControl1.Visible := True;
    TabSheet2.TabVisible := False;
    FCamAvatar           := DModG.QrCtrlGeralDmkNetPath.Value + FComplCaminho + 'avatar.jpg';
    FImage               := '';
    //
    if not FileExists(FCamAvatar) then
      CarregaFotoAvatarURL();
  end else
  begin
    PageControl1.Visible := False;
    Geral.MB_Info('Diret�rio padr�o n�o foi definido nas op��es do aplicativo ou o diret�rio padr�o n�o est� dispon�vel!');
  end;
end;

procedure TFmEntiImagens.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiImagens.Image1Click(Sender: TObject);
begin
  VisualizaImagem();
end;

procedure TFmEntiImagens.Image1DblClick(Sender: TObject);
begin
  Image2.Picture.LoadFromFile(FCamAvatar);
  TabSheet2.TabVisible := True;
  PageControl1.ActivePageIndex := 1;
end;

procedure TFmEntiImagens.Image2Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  TabSheet2.TabVisible := False;
  TabSheet1.TabVisible := True;
  //
  ReopenEntidades(QrEntidadesCodigo.Value, '');
end;

procedure TFmEntiImagens.irarfoto1Click(Sender: TObject);
var
  Diretorio, NomeImg: String;
  Foto: TJPEGImage;

  function SalvaImgEmDisco(Caminho: String): String;
  var
    Nome: String;
  begin
    Result := '';
    try
      if not DirectoryExists(Caminho) then
      begin
        if DirectoryExists(DModG.QrCtrlGeralDmkNetPath.Value) then
        begin
          try
            ForceDirectories(Caminho);
          except
            Geral.MB_Info('O diret�rio "' + Caminho + '" n�o foi localizado!' +
              sLineBreak + 'Verifique se ele existe ou se est� acess�vel e tente novamente!');
            Exit;
          end;
        end else
        begin
          Geral.MB_Info('O diret�rio "' + DModG.QrCtrlGeralDmkNetPath.Value +
            '" n�o foi localizado!' + sLineBreak +
            'Verifique se ele existe ou se est� acess�vel e tente novamente!');
          Exit;
        end;
      end;
      Nome := FormatDateTime('yymmdd_hhnnss_zzz', Now) + '.jpg'; //Para n�o duplicar o nome
      Foto.SaveToFile(Caminho + Nome);
      Result := Nome;
    finally
      Foto.Free;
    end;
  end;
begin
  NomeImg   := '';
  Diretorio := DModG.QrCtrlGeralDmkNetPath.Value;
  if Length(Diretorio) = 0 then
  begin
     Geral.MB_Info('Diret�rio n�o definido!' + sLineBreak +
      'Defina o diret�rio nas op��es do aplicativo e tente novamente.');
     Exit;
  end else
    Diretorio := Diretorio + FComplCaminho;
  //
  Foto := TJPEGImage.Create;
  //
//{$IfDef VER_BEFORE_BERLIN}
  if DBCheck.CriaFm(TFmObtemFoto4, FmObtemFoto4, afmoNegarComAviso) then
  begin
    FmObtemFoto4.ShowModal;
    //
    if FmObtemFoto4.FJPGImage <> nil then
    begin
      Foto.Assign(FmObtemFoto4.FJPGImage);
      //
      NomeImg := SalvaImgEmDisco(Diretorio);
    end;
    FmObtemFoto4.Destroy;
    //
    if Length(NomeImg) > 0 then
    begin
      SalvaDadosBD(NomeImg, QrImagensCodigo.Value, Trunc(QrImagensTipImg.Value));
      //SalvaDadosBD(NomeImg, FCodigo, FTipImg);
      //ReopenEntiImgs(FCodigo, FCodEnti, FTipImg);
      //VisualizaImagem();
    end;
  end;
//{$Else}
  //... c�digo novo aqui   // TODO Berlin
//{$EndIf}
end;

procedure TFmEntiImagens.Panel5DblClick(Sender: TObject);
begin
  VisualizaImagem();
end;

procedure TFmEntiImagens.QrEntidadesAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiImgs(QrEntidadesCodigo.Value, 0, -1);
end;

procedure TFmEntiImagens.QrEntidadesBeforeClose(DataSet: TDataSet);
begin
  QrImagens.Close;
end;

procedure TFmEntiImagens.QrImagensCalcFields(DataSet: TDataSet);
var
  TipImg: String;
begin
  case Trunc(QrImagensTipImg.Value) of
    0: TipImg := 'Entidade';
    1: TipImg := 'Contato';
    2: TipImg := 'Respons�vel';
    else
  end;
  QrImagensNOMETIP_TXT.Value := TipImg;
end;

procedure TFmEntiImagens.Removerimagem1Click(Sender: TObject);
var
  Codigo, CodEnti: Integer;
  CamImagem: String;
begin
  if (QrImagens.State <> dsInactive) or (QrImagens.RecordCount > 0) then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then
      Exit;
    //
    Codigo    := QrImagensCodImg.Value;
    CodEnti   := QrImagensCodigo.Value;
    CamImagem := DModG.QrCtrlGeralDmkNetPath.Value + FComplCaminho + QrImagensNomeImg.Value;
    //
    if Codigo <> 0 then
    begin
      UMyMod.ExcluiRegistroInt1('Confirma a excluis�o da imagem?', 'entiimgs', 'Codigo', Codigo, Dmod.MyDB);
      //
      if FileExists(CamImagem) then
      begin
        if not DeleteFile(CamImagem) then
        begin
          Geral.MB_Aviso('Arquivo removido do banco de dados. Mas houve falha ao remover arquivo!' +
            sLineBreak + 'O arquivo dever� ser removido manualmente!');
        end else
          ReopenEntiImgs(CodEnti, 0, -1);
      end;
    end;
  end;
end;

procedure TFmEntiImagens.ReopenEntidades(Codigo: Integer; Texto: String);
var
  SQL, Numeros: String;
begin
  if Length(Texto) > 0 then
  begin
    Numeros := Geral.SoNumero_TT(Texto);
    // 
    if Length(Numeros) = 0 then
      SQL := 'AND IF(Tipo=0, RazaoSocial, Nome) LIKE "%' + Texto + '%"'
    else
      SQL := 'AND Codigo = ' + Texto;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB, [
  'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE, Ativo ',
  'FROM entidades ',
  'WHERE Codigo <> 0 ',
  SQL,
  'ORDER BY NOMEENTIDADE ',
  '']);
  //
  if Codigo <> 0 then
    QrEntidades.Locate('Codigo', Codigo, []);
end;

procedure TFmEntiImagens.ReopenEntiImgs(Codigo, CodEnti, TipImg: Integer);
var
  SQL: String;
begin
  if CodEnti <> 0 then
    SQL := 'AND img.CodEnti=' + Geral.FF0(CodEnti)
  else
    SQL := '';
  //
  case TipImg of
    0://Entidade
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrImagens, Dmod.MyDB, [
      'SELECT ent.Codigo, 0 + 0.000 TipImg, ',
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END Nome, ',
      'img.Nome NomeImg, img.Codigo CodImg ',
      'FROM entidades ent ',
      'LEFT JOIN entiimgs img ON ',
      '( ',
      'img.TipImg = 0 AND img.CodEnti = ent.Codigo ',
      ') ',
      'WHERE ent.Codigo=' + Geral.FF0(Codigo),
      SQL,
      'AND img.TipImg=0 ',
      'ORDER BY TipImg, Nome ',
      '']);
    end;
    1://Enti Contato
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrImagens, Dmod.MyDB, [
      'SELECT con.Controle Codigo, 1 + 0.000 TipImg, con.Nome, ',
      'img.Nome NomeImg, img.Codigo CodImg ',
      'FROM enticontat con ',
      'LEFT JOIN entiimgs img ON ',
      '( ',
      'img.TipImg = 1 AND img.CodEnti = con.Controle ',
      ') ',
      'WHERE con.Codigo=' + Geral.FF0(Codigo),
      SQL,
      'AND img.TipImg=1 ',
      'ORDER BY TipImg, Nome ',
      '']);
    end;
    2://Enti Respons
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrImagens, Dmod.MyDB, [
      'SELECT res.Controle Codigo, 2 + 0.000 TipImg, res.Nome, ',
      'img.Nome NomeImg, img.Codigo CodImg ',
      'FROM entirespon res ',
      'LEFT JOIN entiimgs img ON ',
      '( ',
      'img.TipImg = 2 AND img.CodEnti = res.Controle ',
      ') ',
      'WHERE res.Codigo=' + Geral.FF0(Codigo),
      SQL,
      'AND img.TipImg=2 ',
      'ORDER BY TipImg, Nome ',
      '']);
    end
    else //Todos
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrImagens, Dmod.MyDB, [
      'SELECT ent.Codigo, 0 + 0.000 TipImg, ',
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END Nome, ',
      'img.Nome NomeImg, img.Codigo CodImg ',
      'FROM entidades ent ',
      'LEFT JOIN entiimgs img ON ',
      '( ',
      'img.TipImg = 0 AND img.CodEnti = ent.Codigo ',
      ') ',
      'WHERE ent.Codigo=' + Geral.FF0(Codigo),
      SQL,
      ' ',
      'UNION ',
      ' ',
      'SELECT con.Controle Codigo, 1 + 0.000 TipImg, con.Nome, ',
      'img.Nome NomeImg, img.Codigo CodImg ',
      'FROM enticontat con ',
      'LEFT JOIN entiimgs img ON ',
      '( ',
      'img.TipImg = 1 AND img.CodEnti = con.Controle ',
      ') ',
      'WHERE con.Codigo=' + Geral.FF0(Codigo),
      SQL,
      ' ',
      'UNION ',
      ' ',
      'SELECT res.Controle Codigo, 2 + 0.000 TipImg, res.Nome, ',
      'img.Nome NomeImg, img.Codigo CodImg ',
      'FROM entirespon res ',
      'LEFT JOIN entiimgs img ON ',
      '( ',
      'img.TipImg = 2 AND img.CodEnti = res.Controle ',
      ') ',
      'WHERE res.Codigo=' + Geral.FF0(Codigo),
      SQL,
      ' ',
      'ORDER BY TipImg, Nome ',
      '']);
    end;
  end;
end;

procedure TFmEntiImagens.SalvaDadosBD(NomeImg: String; CodEnti, Tipo: Integer);
  function LocalizaImg(CodEn, Tip: Integer): Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM entiimgs ',
    'WHERE TipImg=' + Geral.FF0(Tip),
    'AND CodEnti=' + Geral.FF0(CodEn),
    '']);
    if QrLoc.RecordCount > 0 then
      Result := QrLoc.FieldByName('Codigo').AsInteger
    else
      Result := 0;
  end;
var
  Codigo: Integer;
  SQLType: TSQLType;
begin
  Codigo := LocalizaImg(CodEnti, Tipo);
  if Codigo = 0 then
  begin
    SQLType := stIns;
    Codigo  := UMyMod.BuscaEmLivreY_Def('entiimgs', 'Codigo', stIns, Codigo, nil);
  end else
    SQLType := stUpd;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entiimgs', False, ['Nome',
    'TipImg', 'CodEnti'], ['Codigo'], [NomeImg, Tipo, CodEnti], [Codigo], True)
  then
  begin
    ReopenEntiImgs(QrEntidadesCodigo.Value, 0, -1);
    VisualizaImagem();
  end;
end;

procedure TFmEntiImagens.SbDiretorioClick(Sender: TObject);
begin
  Geral.MB_Info('Diret�rio onde as imagens ser�o salvas:' + sLineBreak +
    DModG.QrCtrlGeralDmkNetPath.Value + FComplCaminho);
end;

procedure TFmEntiImagens.VisualizaImagem;
var
  CamImagem: String;
begin
  CamImagem := DModG.QrCtrlGeralDmkNetPath.Value + FComplCaminho + QrImagensNomeImg.Value;
  //
  if FileExists(CamImagem) then
    CamImagem := CamImagem
  else if FileExists(FCamAvatar) then
    CamImagem := FCamAvatar
  else
    CamImagem := '';
  //
  Image2.Picture.LoadFromFile(CamImagem);
  TabSheet2.TabVisible := True;
  TabSheet1.TabVisible := False;
  PageControl1.ActivePageIndex := 1;
end;

procedure TFmEntiImagens.VisualizarImagem1Click(Sender: TObject);
begin
  VisualizaImagem();
end;

end.
