unit EntiSrvPro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, ComCtrls, dmkEditDateTimePicker, UnDmkEnums;

type
  TFmEntiSrvPro = class(TForm)
    Panel1: TPanel;
    QrEntiTipPro: TmySQLQuery;
    DsEntiTipPro: TDataSource;
    CkContinuar_: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdCodUso: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdResultado: TdmkEdit;
    Label7: TLabel;
    CBOrgao: TdmkDBLookupComboBox;
    EdOrgao: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    LaData: TLabel;
    LaHora: TLabel;
    Label2: TLabel;
    EdNumero: TdmkEdit;
    QrEntiTipProCodigo: TIntegerField;
    QrEntiTipProNome: TWideStringField;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEntiSrvPro(Controle: Integer);
  public
    { Public declarations }
    FQrEntidades, FQrEntiSrvPro: TmySQLQuery;
    FDsEntidades: TDataSource;
  end;

  var
  FmEntiSrvPro: TFmEntiSrvPro;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, Principal, MyDBCheck,
  CfgCadLista, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmEntiSrvPro.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  if Trim(EdResultado.Text) = '' then
  begin
    Geral.MB_Aviso('Informe o resultado da consulta!');
    EdResultado.SetFocus;
    Exit;
  end;
  Controle := UMyMod.BuscaEmLivreY_Def('EntiSrvPro', 'Controle', ImgTipo.SQLType,
  EdControle.ValueVariant);
  //
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'entisrvpro',
  Controle, Dmod.QrUpd) then
  begin
    ReopenEntiSrvPro(Controle);
    (*
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCargo.ValueVariant     := 0;
      CBCargo.KeyValue         := Null;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else*) Close;
  end;
end;

procedure TFmEntiSrvPro.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiSrvPro.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsEntidades;
  DBEdCodUso.DataSource := FDsEntidades;
  DBEdNome.DataSource := FDsEntidades;
  MyObjects.CorIniComponente();
end;

procedure TFmEntiSrvPro.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEntiTipPro, Dmod.MyDB);
end;

procedure TFmEntiSrvPro.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiSrvPro.ReopenEntiSrvPro(Controle: Integer);
begin
  if FQrEntiSrvPro <> nil then
  begin
    FQrEntiSrvPro.Close;
    if FQrEntiSrvPro.ParamCount > 0 then
      FQrEntiSrvPro.Params[0].AsInteger :=
      FQrEntidades.FieldByName('Codigo').AsInteger;
    UnDmkDAC_PF.AbreQuery(FQrEntiSrvPro, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrEntiSrvPro.Locate('Controle', Controle, []);
  end;
end;

procedure TFmEntiSrvPro.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'entitippro', 60, ncControle,
  'Cadastro de �rg�o de Prote��o ao Cr�dito',
  [], False, Null, [], [], False);
  UMyMod.SetaCodigoPesquisado(EdOrgao, CBOrgao, QrEntiTipPro, VAR_CADASTRO);
end;

end.
