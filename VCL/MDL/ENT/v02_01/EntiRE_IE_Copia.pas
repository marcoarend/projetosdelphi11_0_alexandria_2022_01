unit EntiRE_IE_Copia;

interface
//{$I cef.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, OleCtrls, SHDocVw, ActiveX, JPEG, ShellAPI, MSHTML, Variants,

  // Componente TChromium :  https://code.google.com/p/delphichromiumembedded/
  cefvcl, ceflib,
  // Fim componente
  ValEdit, WiniNet, UnDmkEnums, UnitWin, StrUtils;

type
  TFmEntiRE_IE = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    ProgressBar1: TProgressBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    BtLeDados: TBitBtn;
    Panel_Cadastro: TPanel;
    LaRazaoSocial: TLabel;
    LaCNPJ: TLabel;
    LaLogradouro: TLabel;
    LaCEP: TLabel;
    LaNumero: TLabel;
    LaComplemento: TLabel;
    LaBairro: TLabel;
    LaMunicipio: TLabel;
    LaUF: TLabel;
    EdRazaoSocial: TEdit;
    EdCNPJ: TEdit;
    EdLogradouro: TEdit;
    EdNumero: TEdit;
    EdCompl: TEdit;
    EdCEP: TEdit;
    EdBairro: TEdit;
    EdCidade: TEdit;
    EdUF: TEdit;
    RGOpcoes: TRadioGroup;
    EdCNAE: TEdit;
    LaCNAE: TLabel;
    LaIE: TLabel;
    EdIE: TEdit;
    LaTelefone: TLabel;
    EdTelefone: TEdit;
    LaEmail: TLabel;
    EdEmail: TEdit;
    LaFantasia: TLabel;
    EdFantasia: TEdit;
    MS: TTabSheet;
    GradeMS: TStringGrid;
    Panel5: TPanel;
    EdURL_A: TEdit;
    EdURL_B: TEdit;
    EdSUFRAMA: TEdit;
    LaSUFRAMA: TLabel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    MeDados: TMemo;
    MeCodigo: TMemo;
    Panel6: TPanel;
    RGNavegador: TRadioGroup;
    TabSheet5: TTabSheet;
    MeChrome: TMemo;
    Panel7: TPanel;
    WB_IE: TWebBrowser;
    WB_Ch: TChromium;
    TabSheet7: TTabSheet;
    MeTxtSite: TMemo;
    BtImporta: TBitBtn;
    MeMS: TMemo;
    Splitter1: TSplitter;
    Panel8: TPanel;
    SbNavega: TSpeedButton;
    LaRE: TLabel;
    CBRE: TComboBox;
    SpeedButton1: TSpeedButton;
    PnURL: TPanel;
    EdURL_WB: TdmkEdit;
    CkCCC: TCheckBox;
    TabSheet6: TTabSheet;
    Memo1: TMemo;
    Panel9: TPanel;
    Memo2: TMemo;
    Memo3: TMemo;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    EdENatal: TEdit;
    EdCodMunici: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure WB_IEProgressChange(ASender: TObject; Progress,
      ProgressMax: Integer);
    procedure FormCreate(Sender: TObject);
    procedure BtLeDadosClick(Sender: TObject);
    procedure WB_IEBeforeNavigate2(ASender: TObject;
      const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);
    procedure Button1Click(Sender: TObject);
    procedure WB_IEDownloadComplete(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure WB_IENavigateComplete2(ASender: TObject;
      const pDisp: IDispatch; var URL: OleVariant);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure SbNavegaClick(Sender: TObject);
    procedure CBREChange(Sender: TObject);
    procedure CBREClick(Sender: TObject);
    procedure RGNavegadorClick(Sender: TObject);
    procedure MeDadosChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WB_ChLoadEnd(Sender: TObject; const browser: ICefBrowser;
      const frame: ICefFrame; httpStatusCode: Integer; out Result: Boolean);
    procedure WB_ChLoadStart(Sender: TObject; const browser: ICefBrowser;
      const frame: ICefFrame);
    procedure WB_IEDocumentComplete(ASender: TObject; const pDisp: IDispatch;
      const URL: OleVariant);
    procedure SpeedButton1Click(Sender: TObject);
    procedure MeTxtSiteChange(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure EdRazaoSocialChange(Sender: TObject);
    procedure PnURLResize(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FCriouForm: Boolean;
    //HTMLDoc : IHTMLDocument2;
    vHtml: Variant;
    FCh_Source: String;
    //FstrLocalFile: string;
    //
    //Chrome
    //FLoading: Boolean;
    //
    function  EditdeCaptionDeLabel_XX(Linha: String): TEdit;
    function  EditdeCaptionDeLabel_GO(const Linha: String; var Texto: String): TEdit;
    procedure ListaDados_XX();
    procedure ListaDados_MS();
    procedure MudouUF();
    procedure ConfiguraNavegador(Navegador: Integer);
    procedure ConfiguraAbas(Mostra: Boolean);
    //function  GetCachedFileFromURL(strUL: string; var strLocalFile: string): boolean;
    function RemoveOutros(Linha, OutroIni, OutroFim: String): String;
    function ObtemTextoEntre(Texto, TagIni, TagFim: String): String;
    function ObtemTextoAposLinha(SLTexto: TStrings; TagIni: String;
             LinAfter: Integer): String;
    procedure ObtemDadosEspecificosDeUF_RJ(Txt: String);
    procedure ObtemDadosEspecificosDeUF_AP(Txt: String);
    procedure ObtemDadosEspecificosDeUF_RN(Txt: String);
    function  PoeQuebraDeLinha(Texto: String): String;

    procedure GetTextOfSite();
    //
    procedure DefineVariaveis();
    //
    //CCC
    function WebFormNames(const document: IHTMLDocument2): TStringList;
    procedure PreencheDadosAPesquisar_IEDocument_CCC();
  public
    { Public declarations }
    FImporta: Boolean;
    FTipoLograd, FTipoImport: Integer;
    FCNPJCampo, FCNPJ, FRazaoSocial, FFantasia, FRua, FLogradouro, FNumero,
    FCompl, FCEP, FBairro, FCidade, FUF, FCNAE, FIE, FTelefone, FEmail,
    FSUFRAMA: String;
    FENatal: TDateTime;
    FUFConsulta: String;
    FCodUF_IBGE, FCodMunici: Integer;
  end;

  var
    FmEntiRE_IE: TFmEntiRE_IE;

implementation

uses UnMyObjects, Module, ModuleGeral, UnDmkProcFunc, UnConsultasWeb;

{$R *.DFM}

function GetBrowserForFrame(Doc: IHTMLDocument2; nFrame: Integer): IWebBrowser2;
  //Thanks to Rik Barker
  //returns an interface to the frame's browser
var
  pContainer: IOLEContainer;
  enumerator: ActiveX.IEnumUnknown;
  nFetched: PLongInt;
  unkFrame: IUnknown;
  hr: HRESULT;
begin
  Result := nil;
  nFetched := nil;
  // Cast the page as an OLE container
  pContainer := Doc as IOleContainer;
  // Get an enumerator for the frames on the page
  hr := pContainer.EnumObjects(OLECONTF_EMBEDDINGS or OLECONTF_OTHERS, enumerator);
  if hr <> S_OK then
  begin
    pContainer._Release;
    Exit;
  end;
  // Now skip to the frame we're interested in
  enumerator.Skip(nFrame);
  // and get the frame as IUnknown
  enumerator.Next(1,unkFrame, nFetched);
  // Now QI the frame for a WebBrowser Interface - I'm not  entirely
  // sure this is necessary, but COM never ceases to surprise me
  unkframe.QueryInterface(IID_IWebBrowser2, Result);
end;

function GetFrameSource(WebDoc: iHTMLDocument2): string;
  //returns frame HTML and scripts as a text string
var
  re: integer;
  HTMLel: iHTMLElement;
  HTMLcol: iHTMLElementCollection;
  HTMLlen: Integer;
  //ScriptEL: IHTMLScriptElement;
begin
  Result := '';
  if Assigned(WebDoc) then
  begin
    HTMLcol := WebDoc.Get_all;
    HTMLlen := HTMLcol.Length;
    for re := 0 to HTMLlen - 1 do
    begin
      HTMLel := HTMLcol.Item(re, 0) as iHTMLElement;
      if HTMLEl.tagName = 'HTML' then
        Result := Result + HTMLEl.outerHTML;
    end;
  end;
end;

procedure WB_SaveFrameToFile(HTMLDocument: IHTMLDocument2;
  const FileName: TFileName);
// Save IHTMLDocument2 to a file
var
  PersistFile: IPersistFile;
begin
  PersistFile := HTMLDocument as IPersistFile;
  PersistFile.Save(StringToOleStr(FileName), System.True);
end;


function SaveWBFrames(WebBrowser1: TWebBrowser): string;
// return the source for all frames in the browser
var
  Webdoc, HTMLDoc: ihtmldocument2;
  framesCol: iHTMLFramesCollection2;
  FramesLen: integer;
  pickFrame: olevariant;
  p: integer;
begin
  try
    WebDoc := WebBrowser1.Document as IHTMLDocument2;
    Result := GetFrameSource(WebDoc);

    // ��� Hier kann Result in eine Datei gespeichert werden ����  oder  mit
    WB_SaveFrameToFile(WebDoc,'c:\MainPage.html');

    //Handle multiple or single frames
    FramesCol := WebDoc.Get_frames;
    FramesLen := FramesCol.Get_length;
    if FramesLen > 0 then
      for p := 0 to FramesLen - 1 do
      begin
        pickframe := p;
        HTMLDoc   := WebBrowser1.Document as iHTMLDocument2;

        WebDoc := GetBrowserForFrame(HTMLDoc, pickframe).document as iHTMLDocument2;
        if WebDoc <> nil then
        begin
          Result := GetFrameSource(WebDoc);
          WB_SaveFrameToFile(WebDoc, 'c:\Frame' + IntToStr(p) + '.html');
          // ShowMessage(HTMLDoc.Get_parentWindow.Get_name);
          // ShowMessage(HTMLDoc.Get_parentWindow.Parent.Get_document.nameProp);

        end;
      end;
  except
    Result := 'No Source Available';
  end;
end;

procedure TFmEntiRE_IE.BitBtn1Click(Sender: TObject);
{
function TFmEntiRE_IE.WebFormNames(const document: IHTMLDocument2): TStringList;
var
  forms : IHTMLElementCollection;
  form : IHTMLFormElement;
  idx : integer;
begin
  forms := document.Forms as IHTMLElementCollection;
  result := TStringList.Create;
  for idx := 0 to -1 + forms.length do
  begin
    form := forms.item(idx,0) as IHTMLFormElement;
    result.Add(form.name);
  end;
end;
}

{
  function WebFormFieldValue(const document: IHTMLDocument2; const formNumber:
  Integer; const fieldName : string): string;
  var
    form : IHTMLFormElement;
    field: IHTMLElement;
  begin
    form := WebFormGet(formNumber, WebBrowser1.Document AS IHTMLDocument2) ;
    field := form.Item(fieldName,'') as IHTMLElement;
    if field = nil then Exit;
    if field.tagName = 'INPUT' then
      result := (field as IHTMLInputElement).value;
    if field.tagName = 'SELECT' then
      result := (field as IHTMLSelectElement).value;
    if field.tagName = 'TEXTAREA' then
      result := (field as IHTMLTextAreaElement).value;
  end;
}


    {
    function WebFormGet(const formNumber: integer; const document: IHTMLDocument2): IHTMLFormElement;
    var
      forms : IHTMLElementCollection;
    begin
      forms := document.Forms as IHTMLElementCollection;
      result := forms.Item(formNumber,'') as IHTMLFormElement;
    end;
    }


{
  function WebFormFields(const document: IHTMLDocument2; const formName : string): TStringList;
  var
    form : IHTMLFormElement;
    field : IHTMLElement;
    fName : string;
    idx : integer;
  begin
    form := WebFormGet(0, WebBrowser1.Document AS IHTMLDocument2) ;
    result := TStringList.Create;
    for idx := 0 to -1 + form.length do
    begin
      field := form.item(idx, '') as IHTMLElement;
      if field = nil then Continue;
      fName := field.id;
      if field.tagName = 'INPUT' then
        fName := (field as IHTMLInputElement).name;
      if field.tagName = 'SELECT' then
        fName := (field as IHTMLSelectElement).name;
      if field.tagName = 'TEXTAREA' then
        fName := (field as IHTMLTextAreaElement).name;
      result.Add(fName) ;
    end;
  end;
}

{
  procedure WebFormSetFieldValue(const document: IHTMLDocument2; const
  formNumber: integer; const fieldName, newValue: string) ;
  var
    form : IHTMLFormElement;
    field: IHTMLElement;
  begin
    form := WebFormGet(formNumber, WebBrowser1.Document AS IHTMLDocument2) ;
    field := form.Item(fieldName,'') as IHTMLElement;
    if field = nil then Exit;
    if field.tagName = 'INPUT' then
      (field as IHTMLInputElement).value := newValue;
    if field.tagName = 'SELECT' then
      (field as IHTMLSelectElement) := newValue;
    if field.tagName = 'TEXTAREA' then
      (field as IHTMLTextAreaElement) := newValue;
    end;
}

{
var
  forms : TStringList;
}
//begin
{
  forms := WebFormNames(WB_IE.Document AS IHTMLDocument2) ;
  try
    memo1.Lines.Assign(forms) ;
    //
  finally
    forms.Free;
  end;
}
//const
  //FIELDNAME = 'url';
var
  document: IHTMLDocument2;
  forms : IHTMLElementCollection;
  form : IHTMLFormElement;
  idx : integer;
  //
var
  doc :IHTMLDocument2;
  fieldValue : string;
var
  field: IHTMLElement;
  FieldName, UF_E_Cod, sCNPJ, sIE: String;
begin
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  Memo3.Lines.Clear;
  document := WB_IE.Document AS IHTMLDocument2;
  forms := document.Forms as IHTMLElementCollection;
  //result := TStringList.Create;
  for idx := 0 to -1 + forms.length do
  begin
    form := forms.item(idx,0) as IHTMLFormElement;
    //result.Add(form.name);
    Memo1.Lines.Add('Form ' + IntToStr(idx+1) + ': ' + form.name);
    {
    function WebFormGet(const formNumber: integer; const document: IHTMLDocument2): IHTMLFormElement;
    var
      forms : IHTMLElementCollection;
    begin
      forms := document.Forms as IHTMLElementCollection;
      result := forms.Item(formNumber,'') as IHTMLFormElement;
    end;
    }

    //
(*
<div class="row">
  <input data-val="true" data-val-required="The Origem field is required." id="Origem" name="Origem" type="hidden" value="PortalDfe">
  <input data-val="true" data-val-required="The TipoPesquisa field is required." id="TipoPesquisa" name="TipoPesquisa" type="hidden" value="">

  <div class="form-group col-sm-4">
      <label for="codUf">UF</label>
      <select class="form-control " data-val="true" data-val-number="The field UF must be a number." data-val-required="Informe a UF do contribuinte" id="CodUf" name="CodUf"><option value=""></option>
<option value="12">AC - 12</option>
<option value="27">AL - 27</option>
<option value="13">AM - 13</option>
<option value="16">AP - 16</option>
<option value="29">BA - 29</option>
<option value="23">CE - 23</option>
<option value="53">DF - 53</option>
<option value="32">ES - 32</option>
<option value="52">GO - 52</option>
<option value="21">MA - 21</option>
<option value="31">MG - 31</option>
<option value="50">MS - 50</option>
<option value="51">MT - 51</option>
<option value="15">PA - 15</option>
<option value="25">PB - 25</option>
<option value="26">PE - 26</option>
<option value="22">PI - 22</option>
<option value="41">PR - 41</option>
<option value="33">RJ - 33</option>
<option value="24">RN - 24</option>
<option value="11">RO - 11</option>
<option value="14">RR - 14</option>
<option value="43">RS - 43</option>
<option value="42">SC - 42</option>
<option value="28">SE - 28</option>
<option value="35">SP - 35</option>
<option value="17">TO - 17</option>
</select>
                                <span class="field-validation-valid text-danger" data-valmsg-for="CodUf" data-valmsg-replace="true"></span>
                            </div>
                            <div class="form-group col-sm-8">
                                <label for="ambiente">Ambiente de Processamento</label>
                                <select class="form-control " data-val="true" data-val-range="Ambiente inv�lido" data-val-range-max="2" data-val-range-min="1" data-val-required="Ambiente n�o informado" id="Ambiente" name="Ambiente"><option selected="selected" value="1">Produ��o</option>
<option value="2">Homologa��o</option>
</select>
                                <span class="field-validation-valid text-danger" data-valmsg-for="Ambiente" data-valmsg-replace="true"></span>
                            </div>
                        </div>
     *)
                        //
    //begin
    //doc := WebBrowser1.Document AS IHTMLDocument2;
    //fieldValue := WebFormFieldValue(doc, 0, FIELDNAME) ;
    fieldName := 'codUf';
    field := form.Item(fieldName,'') as IHTMLElement;
    if field <> nil then
    begin
      UF_E_Cod := '';
      if field.tagName = 'INPUT' then
       UF_E_Cod := (field as IHTMLInputElement).value;
      if field.tagName = 'SELECT' then
       UF_E_Cod := (field as IHTMLSelectElement).value;
      if field.tagName = 'TEXTAREA' then
       UF_E_Cod := (field as IHTMLTextAreaElement).value;
      if UF_E_Cod <> EmptyStr then
      begin
       Geral.MB_Info('UF_E_Cod: ' + UF_E_Cod);
       //memo2.Lines.Add('Field : "' + fieldName + '", value:' + fieldValue) ;
       fieldValue := UF_E_Cod;
       memo2.Lines.Add(
         'Form ' + IntToStr(idx+1) + ': "' + form.name + '" -> ' +
         'Field : "' + fieldName + '", value: "' + fieldValue + '"') ;
      end;
    end;



(*
<input class="form-control  apenasNumeros" data-val="true" data-val-number="The field CodInscrMf must be a number." id="CodInscrMf" name="CodInscrMf" type="text" value="">
*)



(*
<span class="field-validation-valid text-danger" data-valmsg-for="CodInscrMf" data-valmsg-replace="true"></span>
*)
    fieldName := 'CodInscrMf';
    field := form.Item(fieldName,'') as IHTMLElement;
    if field <> nil then
    begin
      sCNPJ := '';
      if field.tagName = 'INPUT' then
      sCNPJ := (field as IHTMLInputElement).value;
      if field.tagName = 'SELECT' then
      sCNPJ := (field as IHTMLSelectElement).value;
      if field.tagName = 'TEXTAREA' then
      sCNPJ := (field as IHTMLTextAreaElement).value;
      if sCNPJ <> EmptyStr then
      begin
        Geral.MB_Info('sCNPJ: ' + sCNPJ);
        //memo2.Lines.Add('Field : "' + fieldName + '", value:' + fieldValue) ;
        fieldValue := sCNPJ;
        memo2.Lines.Add(
         'Form ' + IntToStr(idx+1) + ': "' + form.name + '" -> ' +
         'Field : "' + fieldName + '", value: "' + fieldValue + '"') ;
      end;
    end;


{
<div class="row" style="margin-left: 7px">
                            <div class="form-group col-sm-8">
                                <label for="CodIe">Inscri��o Estadual</label>
                                <input class="form-control  apenasNumeros" data-val="true" data-val-number="The field CodIe must be a number." id="CodIe" name="CodIe" type="text" value="">
                                <span class="field-validation-valid text-danger" data-valmsg-for="CodIe" data-valmsg-replace="true"></span>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">&nbsp;</label>
                                <button type="button" class="form-control btn btn-default" onclick="pesquisa('Ie')">Pesquisar por IE</button>
                            </div>
                        </div>

                             end;
    //
    //
}
  end;
end;

procedure TFmEntiRE_IE.BtImportaClick(Sender: TObject);
begin
  if FUFConsulta = 'RJ' then
  begin
    if Geral.MB_Pergunta(
    'A UF do RJ precisa de revis�o dos dados, principalmente separa��o do Complemento do Bairro!'
    + sLineBreak +
    'Os dados j� est�o revisados e corretos?') <> ID_YES then Exit;
  end else
  begin
    if Geral.MB_Pergunta(
    'O m�todo de obten��o dos dados n�o � preciso!' + sLineBreak +
    'Por isso � necess�rio revisar dos dados!' + sLineBreak +
    'Os dados j� foram revisados e est�o corretos?') <> ID_YES then Exit;
  end;
  //
  DefineVariaveis();
  //
  FImporta := True;
  Close;
end;

procedure TFmEntiRE_IE.BtLeDadosClick(Sender: TObject);
  procedure PreencheEdits();
  var
    I, P: Integer;
    Tudo, Texto, Txt2, Txt: String;
    Edit: TEdit;
    J: Integer;
  begin
    MeDados.Lines.Clear;
    //
{
    if FUFConsulta = 'RO'  then
    begin
      Tudo := MeCodigo.Text;
      P := Pos('ENDERE�O DE CORRESPOND�NCIA', Tudo);
      if P > 0 then
        MeCodigo.Text := Copy(Tudo, 1, P);
    end;
    if FUFConsulta = 'MS' then
      ListaDados_MS()
    else
}
      ListaDados_XX();
    //
{
    // 'BA':
    MeDados.Text := Geral.Substitui(MeDados.Text,
    'if isnull (rsCadastro("des_bairro_distrito")) then' + sLineBreak +
    'Response.Write (rsCadastro("des_bairro"))' + sLineBreak +
    'else' + sLineBreak +
    'Response.Write(rsCadastro("des_bairro_distrito"))' + sLineBreak +
    'end if %--', '');
    // fim BA
    //MeDados.Text := Geral.Substitui(MeDados.Text, 'align="right"'
}
    for I := 0 to MeDados.Lines.Count -2 do
    begin
      Txt := MeDados.Lines[I];
      Txt := RemoveOutros(Txt, 'align="', '"');
      Txt := RemoveOutros(Txt, 'class="', '"');
      Txt := RemoveOutros(Txt, 'style="', '"');
      Txt := RemoveOutros(Txt, 'colspan="', '"');
      Txt := AnsiString(StringReplace(Txt, '10%', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '20%', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '30%', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '40%', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '50%', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '60%', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '70%', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '80%', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '90%', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '100%', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, ';', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '"', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '`', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '�', '', [rfReplaceAll]));
      Txt := AnsiString(StringReplace(Txt, '''', '', [rfReplaceAll]));
      MeDados.Lines[I] := Txt;
    end;
    //Geral.MB_Info(MeDados.Text);
(*
    if (FUFConsulta = 'SP') and (RGNavegador.ItemIndex = 0) then
    begin
      Txt := MeDados.Text;
      //
      EdUF.Text := 'SP';
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'IE:', 'CNPJ:');
      p := pos('"', EdIE.Text);
      if P > 0 then
        EdIE.Text := Copy(EdIE.Text, p+1);
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ:', 'Nome Empresarial:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Nome Empresarial:', 'Nome Fantasia:');
      EdFantasia.Text := ObtemTextoEntre(Txt,'Nome Fantasia:', 'Natureza Jur�dica:');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'CEP:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
    end else
*)
(*
    if (FUFConsulta = 'GO') and (MeTxtSite.Lines.Count > 10) then
    begin
      MeTxtSiteChange(self)
    end else
*)

    if CkCCC.Checked or (

    //if (FUFConsulta = 'AC')
    //or (FUFConsulta = 'AL')
    //if (FUFConsulta = 'AM')
       (FUFConsulta = 'AP')
    or (FUFConsulta = 'BA')
    or (FUFConsulta = 'CE')
    or (FUFConsulta = 'DF')
    or (FUFConsulta = 'GO')
    or (FUFConsulta = 'MG')
    or (FUFConsulta = 'MS')
    or (FUFConsulta = 'RN')
    or (FUFConsulta = 'RR')
    or (FUFConsulta = 'SE')
    or (FUFConsulta = 'RS')
    or (FUFConsulta = 'SP')
    //or (FUFConsulta = 'TO')
    or (FUFConsulta = 'SU'))
    then
    begin
      if MeTxtSite.Text = EmptyStr then
        GetTextOfSite();
      MeTxtSiteChange(self)
    end else
    begin
      for I := 0 to MeDados.Lines.Count -2 do
      begin
        Edit := EditdeCaptionDeLabel_XX(MeDados.Lines[I]);
        if Edit <> nil then
        begin
          Edit.Text := MeDados.Lines[I+1];
          // MG - In�cio
          if Edit = EdLogradouro then
          begin
            if pos('name=enderecoEstabelecimento.nomeLogradouro value=', MeDados.Lines[I+2]) > 0 then
            begin
              Txt2 := MeDados.Lines[I+2];
              Txt2 := Geral.Substitui(Txt2, 'name=enderecoEstabelecimento.nomeLogradouro value=', ' ');
              P := pos('type=hidden', Txt2);
              if P > 0 then
                Txt2 := Copy(Txt2, 1, P-1);
              Txt2 := Geral.Substitui(Txt2, '"', ' ');
              Edit.Text := Edit.Text + ' ' + Trim(Txt2);
            end;
          end;
          // MG Final
        end;
      end;
{
   end else
    if (FUFConsulta <> 'GO') and (RGNavegador.ItemIndex = 1)
     begin
      for I := 0 to MeDados.Lines.Count -2 do
      begin
        Edit := EditdeCaptionDeLabel_GO(MeDados.Lines[I], Texto);
        if Edit <> nil then
          Edit.Text := Texto;
      end;
}
    end;
 // fim 2021-11-24
  end;
(*
var
  CNPJ, RazaoSocial, Fantasia, Rua, Numero, Compl, CEP, Bairro, Cidade, UF,
  Txt1, Txt2, Txt3, CNAE, IE, Telefone, Email, SUFRAMA: String;
  P: Integer;
*)
  //Url, S: String;
  //Doc: Variant;
begin
  Screen.Cursor := crHourGlass;
  try
    PageControl1.ActivePageIndex := 1;
    PreencheEdits();
    //
(*
    CNPJ        := EdCNPJ.Text;
    RazaoSocial := EdRazaoSocial.Text;
    Fantasia    := EdFantasia.Text;
    IE          := EdIE.Text;
    Telefone    := EdTelefone.Text;
    Email       := EdEmail.Text;
    CEP         := EdCEP.Text;
    SUFRAMA     := EdSUFRAMA.Text;
    UF          := EdUF.Text; //Colocar aqui por causa do "EdIE.LinkMsk"
    case RGOpcoes.ItemIndex of
      0:
      begin
        Rua         := '';
        Numero      := '';
        Compl       := '';
        Bairro      := '';
        Cidade      := '';
        CNAE        := '';
        FTipoImport := 0;
      end;
      1:
      begin
        Rua         := EdLogradouro.Text;
        Numero      := EdNumero.Text;
        Compl       := EdCompl.Text;
        Bairro      := EdBairro.Text;
        Cidade      := EdCidade.Text;
        CNAE        := Geral.SoNumero_TT(EdCNAE.Text);
        FTipoImport := 1;
        //
        if FUFConsulta = 'MT' then
        begin
          P := pos('- MT', Cidade);
          if P > 0 then
          begin
            Cidade := Trim(Copy(Cidade, 1, P-1));
            UF := 'MT';
          end;
        end;
      end
      else
      begin
        Geral.MB_Aviso('Tipo de importa��o n�o definido!');
        Exit;
      end;
    end;
    if Length(CNPJ) > 0 then
      FCNPJ := Geral.SoNumero_TT(Trim(CNPJ))
    else
      FCNPJ := '';
    //
    if Length(RazaoSocial) > 0 then
      FRazaoSocial := Trim(RazaoSocial)
    else
      FRazaoSocial := '';
    //
    if Length(Fantasia) > 0 then
      FFantasia := Trim(Fantasia)
    else
      FFantasia := '';
    //
    if Length(Rua) > 0 then
    begin
      FRua := Trim(Rua);
      //
      Geral.SeparaLogradouro(0, FRua, Txt1, Txt2, Txt3, nil, nil);
      //
      FLogradouro := Txt3;
      FTipoLograd := Geral.IMV(Txt2);
    end else
    begin
      FRua        := '';
      FLogradouro := '';
      FTipoLograd := 0;
    end;
    //
    if Length(Numero) > 0 then
      FNumero := Geral.SoNumero_TT(Numero)
    else
      FNumero := '';
    //
    if Length(Compl) > 0 then
      FCompl := Trim(Compl)
    else
      FCompl := '';
    if (AnsiUppercase(Trim(FCompl)) = 'NUMERO:')
    or (AnsiUppercase(Trim(FCompl)) = 'N�MERO')
    or (AnsiUppercase(Trim(FCompl)) = 'N�MERO:')
    or (AnsiUppercase(Trim(FCompl)) = 'NUMERO')
    or (AnsiUppercase(Trim(FCompl)) = 'BAIRRO')
    or (AnsiUppercase(Trim(FCompl)) = 'BAIRRO:')
    or (AnsiUppercase(Trim(FCompl)) = 'TELEFONE')
    or (AnsiUppercase(Trim(FCompl)) = 'TELEFONE:') then
      FCompl := '';
    //
    if Length(CEP) > 0 then
      FCEP := Trim(CEP)
    else
      FCEP := '';
    //
    if Length(Bairro) > 0 then
      FBairro := Trim(Bairro)
    else
      FBairro := '';
    //
    if Length(Cidade) > 0 then
      FCidade := Trim(Cidade)
    else
      FCidade := '';
    //
    if Length(UF) > 2 then
      FUF := Geral.GetSiglaDaUFdoEstado(UF)
    else
    if Length(UF) > 0 then
      FUF := Trim(UF)
    else
      FUF := '';
    //
    if Length(CNAE) > 0 then
      FCNAE := Trim(CNAE)
    else
      FCNAE := '';
    //
    if Length(IE) > 0 then
      //FIE := Geral.SoNumero_TT(IE)
      FIE := Geral.SoNumeroELetra_TT(IE)
    else
      FIE := '';
    //
    if Length(Telefone) > 0 then
      FTelefone := Geral.SoNumero_TT(Telefone)
    else
      FTelefone := '';
    //
    if Length(Email) > 0 then
      FEmail := Trim(Email)
    else
      FEmail := '';
    //
    if Length(SUFRAMA) > 0 then
      FSUFRAMA := Trim(SUFRAMA)
    else
      FSUFRAMA := '';
    //
{
    if VAR_USUARIO <> -1 then
      Close
    else
      Geral.MB_Info('Mensagem exclusiva para usu�rio Master:' + sLineBreak +
      'Dados definidos. Feche a janela!');
}
*)
    DefineVariaveis();
    //
    BtImporta.Enabled := True;
    Geral.MB_Info('Dados definidos!' + sLineBreak +
      'Clique no bot�o "Importa" para os dados serem copiados ao cadastro!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEntiRE_IE.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiRE_IE.Button1Click(Sender: TObject);
begin
{
  MeDados.Lines.Text := WebBrowser.OleObject.Document.documentElement.innerText;
  MeCodigo.Lines.Text := Webbrowser.OleObject.Document.documentElement.innerHTML;
}
end;

procedure TFmEntiRE_IE.Button2Click(Sender: TObject);
begin
//  WebBrowser.Navigate('view-source:' + EdURL_B.Text);
end;

procedure TFmEntiRE_IE.Button3Click(Sender: TObject);
begin
{
  Memo6.Lines.Add('A: ' + WebBrowser.LocationName);
  Memo6.Lines.Add('B: ' + WebBrowser.LocationURL);
  if GetCachedFileFromURL(EdURL_B.Text, FstrLocalFile) then
    Memo6.Lines.Add('3:  ' + FstrLocalFile);
  if GetCachedFileFromURL(EdURL_B.Text, FstrLocalFile) then
    Memo6.Lines.Add('4:  ' + WebBrowser.LocationURL);
}
end;

procedure TFmEntiRE_IE.Button4Click(Sender: TObject);
{
var
  Flags, Headers, TargetFrameName, PostData: OLEVariant;
  Url, Ref: string;
  IEApp: OLEVariant;
}
begin
{
  Flags := '1';
  TargetFrameName := '';
  PostData := '';
  Url := 'http://www.sefaz.pe.gov.br/';//'http://www.dach.de/weiterempfehlen.php';
  Ref := EdURL_B.Text; //'http://www.dach.de/';
  // u cannot navigate to the url above without this referer
  Headers := 'Referer: ' + Ref + sLineBreak;
  Webbrowser.Navigate(Url, Flags, TargetFrameName, PostData, Headers);
}
end;

procedure TFmEntiRE_IE.Button5Click(Sender: TObject);
begin
//  SaveWBFrames(WebBrowser);
end;

procedure TFmEntiRE_IE.CBREChange(Sender: TObject);
begin
  MudouUF();
end;

procedure TFmEntiRE_IE.CBREClick(Sender: TObject);
begin
  MudouUF();
end;

function TFmEntiRE_IE.EditdeCaptionDeLabel_XX(Linha: String): TEdit;
var
  x: String;

  function SoSeVazio(Edit: TEdit): TEdit;
  begin
    if Trim(Edit.Text) = '' then
      Result := Edit
    else
      Result := nil;
  end;

  function VerificaTextoLabel(xLabel: TLabel): Boolean;
  var
    Y: String;
    //I: Integer;
  begin
    Result := LowerCase(x) = LowerCase(xLabel.Caption);
    //
    if not Result then
    begin
      y := xLabel.Caption;
      //
      if pos(' ', Y) > 0 then
      begin
        while pos(' ', Y) > 0 do
          y := Copy(Y, pos(' ', Y) + 1);
        //
        Result := y = x;
      end;
    end;
  end;

begin
  Result := nil;
  x := Trim(Linha);
  if X = '' then
    Exit;
  //
  if VerificaTextoLabel(LaCNPJ) then
    Result := SoSeVazio(EdCNPJ)
  else
  if VerificaTextoLabel(LaIE) then
    Result := SoSeVazio(EdIE)
  else
  if VerificaTextoLabel(LaRazaoSocial) then
    Result := SoSeVazio(EdRazaoSocial)
  else
  if VerificaTextoLabel(LaLogradouro) then
    Result := SoSeVazio(EdLogradouro)
  else
  if VerificaTextoLabel(LaNumero) then
    Result := SoSeVazio(EdNumero)
  else
  if VerificaTextoLabel(LaComplemento) then
    Result := SoSeVazio(EdCompl)
  else
  if VerificaTextoLabel(LaBairro) then
    Result := SoSeVazio(EdBairro)
  else
  if VerificaTextoLabel(LaMunicipio) then
    Result := SoSeVazio(EdCidade)
  else
  if VerificaTextoLabel(LaCEP) then
    Result := SoSeVazio(EdCEP)
  else
  if VerificaTextoLabel(LaUF) then
    Result := SoSeVazio(EdUF)
  else
  if VerificaTextoLabel(LaTelefone) then
    Result := SoSeVazio(EdTelefone)
  else
  if VerificaTextoLabel(LaEMail) then
    Result := SoSeVazio(EdEmail)
  else
  if VerificaTextoLabel(LaCNAE) then
    Result := SoSeVazio(EdCNAE)
  else
  if VerificaTextoLabel(LaSUFRAMA) then
    Result := SoSeVazio(EdSUFRAMA)
end;

procedure TFmEntiRE_IE.EdRazaoSocialChange(Sender: TObject);
{
var
  I: Integer;
  s, OrdChar: String;
}
begin
{
  s := EdRazaoSocial.Text;
  OrdChar := '';
  for I := 0 to Length(s) - 1 do
    OrdChar := OrdChar + IntToStr(I) + ': ' + IntToStr(Ord(s[I])) + ' = ' + s[I];
  Geral.MB_Teste(OrdChar);
}
end;

function TFmEntiRE_IE.EditdeCaptionDeLabel_GO(const Linha: String; var Texto: String): TEdit;
begin
  Result := nil;
  if Trim(Linha) = '' then
    Exit;
  //
  if pos(LaCNPJ.Caption, Linha) > 0  then
  begin
    Result := EdCNPJ;
    Texto := Copy(Linha, 1 + pos(LaCNPJ.Caption, Linha) + Length(LaCNPJ.Caption));
  end else
  if pos(LaIE.Caption, Linha) > 0  then
  begin
    Result := EdIE;
    Texto := Copy(Linha, 1 + pos(LaIE.Caption, Linha) + Length(LaIE.Caption));
  end else
  if pos(LaRazaoSocial.Caption, Linha) > 0  then
  begin
    Result := EdRazaoSocial;
    Texto := Copy(Linha, 1 + pos(LaRazaoSocial.Caption, Linha) + Length(LaRazaoSocial.Caption));
  end else
  if pos(LaLogradouro.Caption, Linha) > 0  then
  begin
    Result := EdLogradouro;
    Texto := Copy(Linha, 1 + pos(LaLogradouro.Caption, Linha) + Length(LaLogradouro.Caption));
  end else
  if pos(LaNumero.Caption, Linha) > 0  then
  begin
    Result := EdNumero;
    Texto := Copy(Linha, 1 + pos(LaNumero.Caption, Linha) + Length(LaNumero.Caption));
  end else
  if pos(LaComplemento.Caption, Linha) > 0  then
  begin
    Result := EdCompl;
    Texto := Copy(Linha, 1 + pos(LaComplemento.Caption, Linha) + Length(LaComplemento.Caption));
  end else
  if pos(LaBairro.Caption, Linha) > 0  then
  begin
    Result := EdBairro;
    Texto := Copy(Linha, 1 + pos(LaBairro.Caption, Linha) + Length(LaBairro.Caption));
  end else
  if pos(LaMunicipio.Caption, Linha) > 0  then
  begin
    Result := EdCidade;
    Texto := Copy(Linha, 1 + pos(LaMunicipio.Caption, Linha) + Length(LaMunicipio.Caption));
  end else
  if pos(LaCEP.Caption, Linha) > 0  then
  begin
    Result := EdCEP;
    Texto := Copy(Linha, 1 + pos(LaCNPJ.Caption, Linha) + Length(LaCNPJ.Caption));
  end else
  if pos(LaUF.Caption, Linha) > 0  then
  begin
    Result := EdUF;
    Texto := Copy(Linha, 1 + pos(LaUF.Caption, Linha) + Length(LaUF.Caption));
  end else
  if pos(LaTelefone.Caption, Linha) > 0  then
  begin
    Result := EdTelefone;
    Texto := Copy(Linha, 1 + pos(LaTelefone.Caption, Linha) + Length(LaTelefone.Caption));
  end else
  if pos(LaEmail.Caption, Linha) > 0  then
  begin
    Result := EdEmail;
    Texto := Copy(Linha, 1 + pos(LaEmail.Caption, Linha) + Length(LaEmail.Caption));
  end else
  if pos(LaCNAE.Caption, Linha) > 0  then
  begin
    Result := EdCNAE;
    Texto := Copy(Linha, 1 + pos(LaCNAE.Caption, Linha) + Length(LaCNAE.Caption));
  end else
  if pos(LaSUFRAMA.Caption, Linha) > 0  then
  begin
    Result := EdSUFRAMA;
    Texto := Copy(Linha, 1 + pos(LaSUFRAMA.Caption, Linha) + Length(LaSUFRAMA.Caption));
  end else
  ;
end;

procedure TFmEntiRE_IE.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
  FCriouForm := True;
end;

procedure TFmEntiRE_IE.FormCreate(Sender: TObject);
begin
  FImporta    := False;
  FCriouForm  := False;
  FTipoImport := -1;
  FCodUF_IBGE := 0;
  FENatal := 0;
  FCodMunici := 0;
  //
  PageControl1.ActivePageIndex := 0;
  RGNavegador.ItemIndex        := 1;
  //
  WB_IE.Visible := False;
  WB_IE.Align   := alClient;
  //
  WB_Ch.Visible := False;
  WB_Ch.Align   := alClient;
  //
  ConfiguraAbas(False);
  // 2021-11-24
  RGNavegador.ItemIndex        := 0;
  ConfiguraNavegador(RGNavegador.ItemIndex);
  WB_IE.Visible := True;
  WB_IE.Align   := alClient;
  //
  WB_Ch.Visible := False;
  WB_Ch.Align   := alClient;
end;

procedure TFmEntiRE_IE.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiRE_IE.FormShow(Sender: TObject);
begin
  FCriouForm := True;
  //
  //if Uppercase(CBRE.Text) = 'SP' then
    //RGNavegador.ItemIndex := 0;
  ConfiguraNavegador(0);
  MudouUF();
end;

procedure TFmEntiRE_IE.GetTextOfSite();
const
  sProcName = 'GetTextOfSite()';
  //
  procedure GetTextOfSite_IE();
  //function GetTextOfSite(WebBrowser : TWebBrowser) : WideString;
  var
    Doc: IHTMLDocument2;
  begin
     Doc := WB_IE.Document as IHTMLDocument2;
     repeat
        Application.ProcessMessages;
     until Assigned(Doc.body);
     //Result := Doc.Body.InnerText;
     MeTxtSite.Text := Doc.Body.InnerText;
  end;
  //
  procedure GetTextOfSite_Ch();
  var
    Txt: String;
  begin
    if FUFConsulta = 'RN' then
    begin
      Txt := WB_Ch.Browser.MainFrame.Text;
      Txt := PoeQuebraDeLinha(Txt);
      MeTxtSite.Text := Txt;
    end else
    begin
      MeTxtSite.Text := FCh_Source;
    end;
  end;
begin
  case RGNavegador.ItemIndex of
    0: GetTextOfSite_IE();
    1: GetTextOfSite_Ch();
    else
      Geral.MB_Erro('Navegador n�o definido em ' + sProcName);
  end;
end;

{
function TFmEntiRE_IE.GetCachedFileFromURL(strUL: string;
  var strLocalFile: string): boolean;
var
  lpEntryInfo: PInternetCacheEntryInfo;
  hCacheDir: LongWord;
  dwEntrySize: LongWord;
  dwLastError: LongWord;
begin
  Result := False;
  dwEntrySize := 0;
  // Begin the enumeration of the Internet cache.
  FindFirstUrlCacheEntry(nil, TInternetCacheEntryInfo(nil^), dwEntrySize);
  GetMem(lpEntryInfo, dwEntrySize);
  hCacheDir := FindFirstUrlCacheEntry(nil, lpEntryInfo^, dwEntrySize);
  if (hCacheDir <> 0) and (strUL = lpEntryInfo^.lpszSourceUrlName) then
  begin
    strLocalFile := lpEntryInfo^.lpszLocalFileName;
    Result := True;
  end;
  FreeMem(lpEntryInfo);
  if Result = False then
    repeat
      dwEntrySize := 0;
      // Retrieves the next cache group in a cache group enumeration
      FindNextUrlCacheEntry(hCacheDir, TInternetCacheEntryInfo(nil^), dwEntrySize);
      dwLastError := GetLastError();
      if (GetLastError = ERROR_INSUFFICIENT_BUFFER) then
      begin
        GetMem(lpEntryInfo, dwEntrySize);
        if (FindNextUrlCacheEntry(hCacheDir, lpEntryInfo^, dwEntrySize)) then
        begin
          if strUL = lpEntryInfo^.lpszSourceUrlName then
          begin
            strLocalFile := lpEntryInfo^.lpszLocalFileName;
            Result := True;
            Break;
          end;
        end;
        FreeMem(lpEntryInfo);
      end;
    until (dwLastError = ERROR_NO_MORE_ITEMS);
end;
}

procedure TFmEntiRE_IE.ListaDados_MS();
var
  A, B, I: Integer;
  Texto: String;
  Memo: TMemo;
begin
  MeDados.Lines.Clear;
  A := 0;
  B := 0;
  case RGNavegador.ItemIndex of
    0: Memo := MeCodigo;
    1: Memo := MeMS;
    else Exit;
  end;
  for I := 0 to Memo.Lines.Count - 1 do
  begin
    if (pos('class=caption_esquerda', Memo.Lines[I]) > 0)
    or (pos('class="caption_esquerda"', Memo.Lines[I]) > 0) then
    begin
      A := A + 1;
      Texto := Geral.RemoveTagsHTML(Memo.Lines[I]);
      Texto := Geral.Substitui(Texto, '&nbsp;', ' ');
      Texto := Trim(Texto);
      GradeMS.Cells[1, A] := Texto;
    end;
    if (pos('class=valor_esquerda', Memo.Lines[I]) > 0) 
    or (pos('class="valor_esquerda"', Memo.Lines[I]) > 0) then
    begin
      B := B + 1;
      Texto := Geral.RemoveTagsHTML(Memo.Lines[I]);
      Texto := Geral.Substitui(Texto, '&nbsp;', ' ');
      Texto := Trim(Texto);
      GradeMS.Cells[2, B] := Texto;
    end;
  end;
  //
  for I := 1 to GradeMS.RowCount - 1 do
  begin
    MeDados.Lines.Add(dmkPF.ParseText(GradeMS.Cells[1, I], True, False));
    MeDados.Lines.Add(dmkPF.ParseText(GradeMS.Cells[2, I], True, False));
  end;
end;

procedure TFmEntiRE_IE.ListaDados_XX();
var
  I: Integer;
  Texto: String;
begin
  MeDados.Lines.Clear;
  for I := 0 to MeCodigo.Lines.Count - 1 do
  begin
    Texto := Geral.RemoveTagsHTML(Mecodigo.Lines[I]);
    Texto := Geral.Substitui(Texto, '&nbsp;', ' ');
    Texto := Trim(Texto);
    if Texto <> '' then
      MeDados.Lines.Add(dmkPF.ParseText(Texto, True, False));
  end;
end;

procedure TFmEntiRE_IE.MeDadosChange(Sender: TObject);
{
var
  I: Integer;
}
begin
{
  Memo1.Lines.Clear;
  for I := 1 to Length(Medados.Text) do
  begin
    Memo1.Lines.Add(Medados.Text[I] + ' - ' + Geral.FFn(Ord(Medados.Text[I]), 3));
  end;
}
end;

procedure TFmEntiRE_IE.MeTxtSiteChange(Sender: TObject);
var
  Txt, Cidade_E_IBGE, sCEP: String;
  p: Integer;
begin
//ini 2021-11-24
  EdCNPJ.Text := '';
  EdIE.Text := '';
  EdRazaoSocial.Text := '';
  EdFantasia.Text := '';
  EdLogradouro.Text := '';
  EdNumero.Text := '';
  EdCompl.Text := '';
  EdBairro.Text := '';
  EdCidade.Text := '';
  EdCEP.Text := '';
  EdUF.Text := FUFConsulta;
  EdCNAE.Text := '';
  EdTelefone.Text := '';
  EdEmail.Text := '';
  EdSUFRAMA.Text := '';
  EdENatal.Text := '';
  EdCodMunici.Text := '';
  //
  if Length(MeTxtSite.Text) > 10 then
  begin
    if CkCCC.Checked then
    begin
      Txt := MeTxtSite.Text;
      p := Pos('Identifica��o do Contribuinte', Txt);
      if p > 0 then
        Txt := Copy(Txt, p);
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual (IE):', 'Situa��o IE:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ:', 'Situa��o CNPJ:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Nome da Empresa:', 'UF:');
      EdFantasia.Text := ObtemTextoEntre(Txt, 'Nome Fantasia:', 'Data In�cio Atividade:');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'Nro:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'Nro:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'CEP:');
      //EdCidade.Text := ObtemTextoEntre(Txt, '', '');
      //EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Voltar');
      EdCNAE.Text := ObtemTextoEntre(Txt, 'CNAE Principal', 'Data Situa��o na UF:');
      EdTelefone.Text := ObtemTextoEntre(Txt, '', '');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
      EdENatal.Text := ObtemTextoEntre(Txt, 'Data In�cio Atividade:', 'Data Fim Atividade:');
      //
      Cidade_E_IBGE := ObtemTextoEntre(Txt, 'Munic�pio IBGE:', 'UF de Localiza��o:');
      p := Pos('-', Cidade_E_IBGE);
      if p > 0 then
      begin
        EdCidade.Text := Trim(Copy(Cidade_E_IBGE, p+1));
        EdCodMunici.Text := Geral.SoNumero_TT(Copy(Cidade_E_IBGE, 1, p-1));
      end else
        EdCidade.Text := Cidade_E_IBGE;
      //
      sCEP := Geral.SONumero_TT(ObtemTextoEntre(Txt, 'CEP:', 'Voltar'));
      EdCEP.Text := Copy(sCEP, 1, 8);
    end else
    if (FUFConsulta = 'AC') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Raz�o Social:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social:', 'ENDERE�O');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'INFORMA��ES COMPLEMENTARES');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, '', '');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'AL') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'In�cio da Atividade:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social:', 'Nome de Fantasia:');
      EdFantasia.Text := ObtemTextoEntre(Txt, 'Nome de Fantasia:', 'Logradouro:');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'CEP:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Munic�pio:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'Atividade(s) Econ�mica(s):');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'AM') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Raz�o Social:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'C.N.P.J:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social:', 'Tipo de Unidade Auxiliar:');
      if EdRazaoSocial.Text = '' then
        EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social:', 'ENDERE�O');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Telefone:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'INFORMA��ES COMPLEMENTARES');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'AP') then
    begin
      Txt := MeTxtSite.Text;
      //
      ObtemDadosEspecificosDeUF_AP(Txt);
    end else
    if (FUFConsulta = 'BA') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'UF:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social:', 'ENDERE�O');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'UF:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'CEP:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Endere�o Eletr�nico:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'INFORMA��ES COMPLEMENTARES');
      EdEmail.Text := ObtemTextoEntre(Txt, 'Endere�o Eletr�nico:', 'Telefone:');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'CE') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Raz�o');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ/CPF:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Social:', 'ENDERE�O');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Telefone:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'INFORMA��ES COMPLEMENTARES');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'DF') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'CF/DF', 'RAZ�O SOCIAL');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ/CPF', 'CF/DF');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'RAZ�O SOCIAL', 'NOME FANTASIA');
      EdFantasia.Text := ObtemTextoEntre(Txt, 'NOME FANTASIA', 'ENDERE�O');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'LOGRADOURO', 'N�MERO');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�MERO', 'Complemento');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento', 'BAIRRO');
      EdBairro.Text := ObtemTextoEntre(Txt, 'BAIRRO', 'MUNIC�PIO');
      EdCidade.Text := ObtemTextoEntre(Txt, 'MUNIC�PIO', 'UF');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP', 'Telefone');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone', 'INFORMA��ES COMPLEMENTARES');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'ES') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Raz�o Social :');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social :', 'ENDERE�O');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Telefone:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'INFORMA��ES COMPLEMENTARES');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'GO') then
    begin
      //
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'INSCRI��O ESTADUAL - CCE :', 'NOME EMPRESARIAL:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ:', 'INSCRI��O ESTADUAL - CCE :');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'NOME EMPRESARIAL:', 'CONTRIBUINTE?');
      EdFantasia.Text := ObtemTextoEntre(Txt,'NOME FANTASIA:', 'ENDERE�O ESTABELECIMENTO');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'LOGRADOURO:', 'N�MERO:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�MERO:', 'QUADRA:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'COMPLEMENTO:', 'BAIRRO:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'INFORMA��ES COMPLEMENTARES');
      EdBairro.Text := ObtemTextoEntre(Txt, 'BAIRRO:', 'MUNIC�PIO:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'MUNIC�PIO:', 'UF:');
    end else
    if (FUFConsulta = 'MG') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'UF:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Nome Empresarial:', 'Informa��es Complementares');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Telefone:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'UF:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Logradouro:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'Distrito/Povoado:');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'function');
      //
    end else
    if (FUFConsulta = 'MS') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Incri��o Estadual', 'Data de Inclus�o do Contribuinte');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CPF/CNPJ', 'Raz�o Social/Nome');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social/Nome', 'Descri��o da Atividade');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro', 'Numero');
      EdNumero.Text := ObtemTextoEntre(Txt, 'Numero', 'Complemento');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento', 'CEP');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro', 'Munic�pio');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio', 'UF');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP', 'Bairro');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, '', '');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'MT') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'N�mero de Inscri��o Estadual', 'CNPJ');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ', 'Data In�cio Atividade - SEFAZ');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'NOME EMPRESARIAL', 'T�TULO DO ESTABELECIMENTO(NOME FANTASIA)');
      EdFantasia.Text := ObtemTextoEntre(Txt, 'T�TULO DO ESTABELECIMENTO(NOME FANTASIA)', 'C�DIGO E DESCRI��O DA ATIVIDADE ECON�MICA PRINCIPAL');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'LOGRADOURO', 'N�MERO');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�MERO', 'COMPLEMENTO');
      EdCompl.Text := ObtemTextoEntre(Txt, 'COMPLEMENTO', 'CEP');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP', 'BAIRRO');
      EdBairro.Text := ObtemTextoEntre(Txt, 'BAIRRO', 'MUNIC�PIO');
      EdCidade.Text := ObtemTextoEntre(Txt, 'MUNIC�PIO', 'UF');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'TELEFONE', 'SITUA��O CADASTRAL');
      EdEmail.Text := ObtemTextoEntre(Txt, 'ENDERE�O ELETR�NICO', 'TELEFONE');
    end else
    if (FUFConsulta = 'PA') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Raz�o Social:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social:', 'ENDERE�O');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'UF:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'CEP:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Endere�o Eletr�nico:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'INFORMA��ES COMPLEMENTARES');
      EdEmail.Text := ObtemTextoEntre(Txt, 'Endere�o Eletr�nico:', 'Telefone:');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'PB') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Raz�o Social:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social:', 'Logradouro:');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Telefone:');
      EdCNAE.Text := ObtemTextoEntre(Txt, 'Atividade Econ�mica:', 'Regime de Pagamento:');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'Atividade Econ�mica:');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'PE') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual do Contribuinte:', 'CPF/CNPJ:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CPF/CNPJ:', 'Raz�o Social:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social:', 'Nome Fantasia:');
      EdFantasia.Text := ObtemTextoEntre(Txt, 'Nome Fantasia:', 'Endere�o');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Rua:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Municipio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Municipio:', 'Municipio IBGE:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Rua:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone :', 'Email:');
      EdEmail.Text := ObtemTextoEntre(Txt, 'Email:', 'Informa��es Complementares');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'PI') then
    begin
      Geral.MB_Info('O estado do Piau� (PI) n�o possui mais consulta pelo SINTEGRA!');
{
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, '', '');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, '', '');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, '', '');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, '', '');
      EdNumero.Text := ObtemTextoEntre(Txt, '', '');
      EdCompl.Text := ObtemTextoEntre(Txt, '', '');
      EdBairro.Text := ObtemTextoEntre(Txt, '', '');
      EdCidade.Text := ObtemTextoEntre(Txt, '', '');
      EdCEP.Text := ObtemTextoEntre(Txt, '', '');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, '', '');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
}
    end else
    if (FUFConsulta = 'PR') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Nome Empresarial:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Nome Empresarial:', 'ENDERE�O');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Telefone:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'E-mail:');
      EdEmail.Text := ObtemTextoEntre(Txt, 'E-mail:', 'INFORMA��ES COMPLEMENTARES');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'RJ') then
    begin
      Txt := MeTxtSite.Text;
      //
      ObtemDadosEspecificosDeUF_RJ(Txt);
    end else
    if (FUFConsulta = 'RN') then
    begin
      Txt := MeTxtSite.Text;
      //
      ObtemDadosEspecificosDeUF_RN(Txt);
    end else
    if (FUFConsulta = 'RO') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Nire:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'C.P.F/C.N.P.J:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social:', 'Nome Fantasia:');
      EdFantasia.Text := ObtemTextoEntre(Txt, 'Nome Fantasia:', 'Utiliza��o do Estabelecimento:');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Endere�o:', 'Complemento:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Munic�pio:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'N�mero:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'CEP:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'UF:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'UF:');
      EdEmail.Text := ObtemTextoEntre(Txt, 'E-mail:', 'INFORMA��ES COMPLEMENTARES');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'RR') then
    begin
      Txt := MeTxtSite.Text;
      p := pos('Resultados da Consulta', Txt);
      if p > 0 then
        Txt := Copy(Txt, p);
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Raz�o Social:');
      //
      EdCNPJ.Text := Geral.FormataCNPJ_TT(Geral.SoNumero_TT(ObtemTextoEntre(Txt, 'CNPJ:', 'Inscri��o Estadual:')));
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social:', 'Endere�o');
      EdFantasia.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdLogradouro.Text := ObtemTextoEntre(Txt, '', '');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Telefone:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'Informa��es Complementares');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'RS') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'CAD ICMS', 'CNPJ');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ', 'Raz�o Social');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social', 'Nome Fantasia');
      EdFantasia.Text := ObtemTextoEntre(Txt, 'Nome Fantasia', 'Endere�o');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro', 'N�mero');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero', 'Complemento');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento', 'Bairro/Distrito');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro/Distrito', 'Munic�pio');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio', 'U.F.');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP', 'Telefone');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, '', '');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'SC') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Nome/Raz�o Estadual:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CPF/CNPJ:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Nome/Raz�o Estadual:', '');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', '');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'UF:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'CEP:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Endere�o Eletr�nico:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'INFORMA��ES COMPLEMENTARES');
      EdEmail.Text := ObtemTextoEntre(Txt, 'Endere�o Eletr�nico:', 'Telefone:');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'SE') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Raz�o Social:');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CGC:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social:', 'ENDERE�O');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Telefone:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'INFORMA��ES COMPLEMENTARES');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'SP') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'IE:', 'CNPJ:');
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ:', 'Nome Empresarial:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Nome Empresarial:', 'Nome Fantasia:');
      EdFantasia.Text := ObtemTextoEntre(Txt,'Nome Fantasia:', 'Natureza Jur�dica:');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�:', 'Complemento:');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'CEP:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
    end else
    if (FUFConsulta = 'TO') then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual:', 'Raz�o');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ/CPF:', 'Inscri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Social:', 'ENDERE�O');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', 'Bairro:');
      EdCompl.Text := ObtemTextoEntre(Txt, '', '');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Telefone:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'INFORMA��ES COMPLEMENTARES');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
    end else
    if (FUFConsulta = 'SU') then
    begin
      EdUF.Text := 'AM';
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, 'Incri��o Estadual:', 'Incri��o Estadual2');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, 'CGC:', 'Incri��o Estadual:');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Social:', 'ENDERE�O');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, 'Logradouro:', 'N�mero:');
      EdNumero.Text := ObtemTextoEntre(Txt, 'N�mero:', '');
      EdCompl.Text := ObtemTextoEntre(Txt, 'Complemento:', 'Bairro:');
      EdBairro.Text := ObtemTextoEntre(Txt, 'Bairro:', 'Munic�pio:');
      EdCidade.Text := ObtemTextoEntre(Txt, 'Munic�pio:', 'UF:');
      EdCEP.Text := ObtemTextoEntre(Txt, 'CEP:', 'Telefone:');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, 'Telefone:', 'INFORMA��ES COMPLEMENTARES');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, 'Inscri��o Suframa :', 'CGC:');
  (*

SINTEGRA

Sintegra

Consulta P�blica ao Cadastro da

SUFRAMA


Cadastro atualizado at�: 01/12/2021 as 09:53:42

IDENTIFICA��O

Inscri��o Suframa :	200306014
CGC:	02.075.387/0001-70
Incri��o Estadual:	0000000041354451	Incri��o Estadual2:	0
Raz�o
Social:	B&M DA AMAZONIA INDUSTRIAL LTDA.
ENDERE�O
Logradouro:	AVENIDA AYRAO 1316
N�mero:		Complemento:
Bairro:	PRACA 14 DE JAN
Munic�pio:	Manaus	UF:	AM
CEP:	69020410	Telefone:	922320269
INFORMA��ES COMPLEMENTARES
Atividade
Econ�mica:
METALURGIA DE OUTROS METAIS NAO-FERROSOS E SUAS LIGAS

Data da Inscri��o
SUFRAMA	31/10/1997
Situa��o Cadastral:	INATIVA
Tipo(s) de Incentivo(s) para a regi�o de domic�lio da empresa :	ICMS (Conv�nio 65/88), IPI (Decreto no. 4544/02) e PIS/COFINS (Lei no. 10996/04-Zona Franca e Lei no. 11945/09-ALC's)
Isenta de TSA :	N�O
OBSERVA��O: Os dados acima s�o baseados em informa��es fornecidas pelo contribuinte, estando sujeitos a posterior confirma��o pela SUFRAMA
 Data da Consulta	01/12/2021 as 09:53:42
*)







(*
    end else
    if (FUFConsulta = ??) then
    begin
      Txt := MeTxtSite.Text;
      //
      EdIE.Text := ObtemTextoEntre(Txt, '', '');
      //
      EdCNPJ.Text := ObtemTextoEntre(Txt, '', '');
      EdRazaoSocial.Text := ObtemTextoEntre(Txt, '', '');
      EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
      EdLogradouro.Text := ObtemTextoEntre(Txt, '', '');
      EdNumero.Text := ObtemTextoEntre(Txt, '', '');
      EdCompl.Text := ObtemTextoEntre(Txt, '', '');
      EdBairro.Text := ObtemTextoEntre(Txt, '', '');
      EdCidade.Text := ObtemTextoEntre(Txt, '', '');
      EdCEP.Text := ObtemTextoEntre(Txt, '', '');
      EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
      EdTelefone.Text := ObtemTextoEntre(Txt, '', '');
      EdEmail.Text := ObtemTextoEntre(Txt, '', '');
      EdSUframa.Text := ObtemTextoEntre(Txt, '', '');

*)
    end;
  end;
//fim 2021-11-24
end;

procedure TFmEntiRE_IE.MudouUF();
var
  InfoCopiar: Boolean;
begin
  if CkCCC.Checked then
  begin
    EdURL_WB.Text := UConsultasWeb.ObtemURLdoSINTEGRA(FUFConsulta, CkCCC.Checked);
    SbNavegaClick(Self);
    Exit;
  end;
  InfoCopiar := True;
  //
  if FCriouForm then
  begin
    FUFConsulta := Uppercase(CBRE.Text);
    if Length(FUFConsulta) = 2 then
    begin
      if CBRE.Items.IndexOf(FUFConsulta) > -1 then
      begin
        UConsultasWeb.ObtemTagSINTEGRA(FUFConsulta, LaCNPJ, LaIE,
        LaRazaoSocial, LaLogradouro, LaNumero,
        LaComplemento, LaBairro, LaMunicipio,
        LaCEP, LaUF, LaTelefone,
        LaEMail, LaCNAE, LaFantasia,
        LaSUFRAMA);
        //
        EdURL_WB.Text := UConsultasWeb.ObtemURLdoSINTEGRA(FUFConsulta, CkCCC.Checked);
        //
        if FUFConsulta = 'AC' then
          RGNavegador.ItemIndex := 1
        else
        if FUFConsulta = 'GO' then
          RGNavegador.ItemIndex := 1
        else
        if FUFConsulta = 'MT' then
          RGNavegador.ItemIndex := 1
        else
        if FUFConsulta = 'RN' then
          RGNavegador.ItemIndex := 1
        else
        if FUFConsulta = 'SP' then
          RGNavegador.ItemIndex := 0
        else
        if FUFConsulta = 'SU' then
          RGNavegador.ItemIndex := 1
        else
        try
          SbNavegaClick(Self);
        except
          // nada
        end;
      end else
        EdURL_WB.Text := '';
    end else
    begin
      EdURL_WB.Text := '';
    end;
  end;
  if (FUFConsulta = 'AC')
  or (FUFConsulta = 'AL')
  or (FUFConsulta = 'MT') then
    Geral.MB_Info('ATEN��O: Ap�s consulta copie o texto da janela popup na guia "Texto do site pesquisado" e clique no bot�o "Importa Dados"!')
  else
  if (FUFConsulta = 'AM')
  or (FUFConsulta = 'RO')
  or (FUFConsulta = 'TO') then
    Geral.MB_Info('ATEN��O: Ap�s consulta (talvez seja necess�rio fazer no navegador fora deste ERP) copie o texto da janela popup na guia "Texto do site pesquisado" e clique no bot�o "Importa Dados"!')
  else
  if FUFConsulta = 'RJ' then
    Geral.MB_Info('ATEN��O: O RJ gera um PDF com estrutura de informa��o truncada tornando a procura pelas informa��es n�o confi�vel o suficiente!')
  else
    InfoCopiar := False;
  //
  if InfoCopiar then
    Geral.MB_Info('ATEN��O: Para copiar texto a ser extraido copie usando os comandos Ctrl+ "A" e depois Ctrl + "C" para que todo texto HTML ou PDF seja copiado')
end;

procedure TFmEntiRE_IE.ObtemDadosEspecificosDeUF_AP(Txt: String);
var
  sCNPJ, sEnder, sRevertEnder, sNum, sRua, sMuniCEP, sRevertMuniCEP, sMunicipio,
  sCEP: String;
  I, p: Integer;
  EhNum, EhCEP: Boolean;
begin
  EdIE.Text := ObtemTextoEntre(Txt, 'N�MERO DE INSCRI��O', 'FIRMA OU RAZ�O SOCIAL');
  //
  sCNPJ := ObtemTextoEntre(Txt, 'CNPJ/CPF', 'LOGRADOURO');
  sCNPJ := Geral.SONumero_TT(sCNPJ);
  sCNPJ := Copy(sCNPJ, 1, 14);
  //
  EdCNPJ.Text := sCNPJ;
  EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'FIRMA OU RAZ�O SOCIAL', 'NOME FANTASIA');
  EdFantasia.Text := ObtemTextoEntre(Txt, 'NOME FANTASIA', 'CNPJ/CPF');
  //
  sEnder := Trim(ObtemTextoEntre(Txt, 'LOGRADOURO N�MERO', 'COMPLEMENTO BAIRRO'));
  sRevertEnder := ReverseString(sEnder);
  p := pos(' ', sRevertEnder);
  sNum := Copy(sRevertEnder, 1, p-1);
  sNum := Trim(sNum);
  EhNum := (Length(sNum) > 0) and (sNum = Geral.SoNumero_TT(sNum));
  if (p > 0) and (EhNum) then
  begin
    sNum := ReverseString(sNum);
    sRua := Copy(sRevertEnder, p+1);
    sRua := Trim(sRua);
    sRua := ReverseString(sRua);
    //
    EdNumero.Text := sNum;
    EdLogradouro.Text := sRua;
    //
  end else
  begin
    EdNumero.Text := '';
    EdLogradouro.Text := sEnder;
  end;
  //
  EdCompl.Text := ObtemTextoEntre(Txt, 'COMPLEMENTO BAIRRO', 'MUNIC�PIO CEP');
  EdBairro.Text := EdCompl.Text;
  //
  sMuniCEP :=  ObtemTextoEntre(Txt, 'MUNIC�PIO CEP', 'SITUA��O');
  sRevertMuniCEP := ReverseString(sMuniCEP);
  p := pos(' ', sRevertMuniCEP);
  sCEP := Copy(sRevertMuniCEP, 1, p-1);
  sCEP := Trim(sCEP);
  EhCEP := (Length(Geral.SoNumero_TT(sCEP)) > 0) and (Geral.FormataCEP_TT(sCEP) = Geral.FormataCEP_TT(Geral.SoNumero_TT(sCEP)));
  if (p > 0) and (EhCEP) then
  begin
    sCEP := ReverseString(sCEP);
    sMunicipio := Copy(sRevertMuniCEP, p+1);
    sMunicipio := Trim(sMunicipio);
    sMunicipio := ReverseString(sMunicipio);
    //
    EdCEP.Text := sCEP;
    EdCidade.Text := sMunicipio;
    //
  end else
  begin
    EdCEP.Text := '';
    EdCidade.Text := sMunicipio;
  end;
  //
  EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
  EdTelefone.Text := ObtemTextoEntre(Txt, '', '');
  EdEmail.Text := ObtemTextoEntre(Txt, '', '');
  EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
end;

procedure TFmEntiRE_IE.ObtemDadosEspecificosDeUF_RJ(Txt: String);
var
  sIE_Data, sSohIE, sCNPJ, sTemp1, sTemp2, sEnder: String;
  sRestEnder, sLogradouro, sNumero, sComplEBairro, SComplemento, sBairro,
  sCidade, sCEP: String;
  p: Integer;
begin
  sCNPJ := ObtemTextoAposLinha(MeTxtSite.Lines, 'Habilitada', +1);
  sIE_Data := ObtemTextoEntre(Txt, 'Comprovante de Inscri��o e de Situa��o Cadastral', 'CNPJ/CPF');;
  p := pos(' ', sIE_Data);
  if p > 1 then
    sSohIE := Trim(Copy(sIE_Data, 1, p))
  else
    sSohIE := sIE_Data;
  //
  EdIE.Text := sSohIE;

  //
  EdCNPJ.Text := sCNPJ;
  //
  EdRazaoSocial.Text := ObtemTextoAposLinha(MeTxtSite.Lines, 'Regime normal de tributa��o - Confronto d�bito e cr�dito', +1);
  //
  EdFantasia.Text := ObtemTextoEntre(Txt, '', '');
  sTemp1 := ObtemTextoAposLinha(MeTxtSite.Lines, sCNPJ, +1);
  sTemp2 := ObtemTextoAposLinha(MeTxtSite.Lines, sCNPJ, +2);
  //
  if pos(' RJ ', sTemp1) > 0 then
    sEnder := Trim(sTemp1)
  else
    sEnder := Trim(sTemp1) + ' ' + Trim(sTemp2);
  // Extrai Endere�o:
  // RUA Da Ajuda, 00035 SAL 2801 CENTRO - RIO DE JANEIRO RJ 20.040-915

  p := pos(',', sEnder);
  sLogradouro := Trim(Copy(sEnder, 1, p-1));
  EdLogradouro.Text := sLogradouro;
  sRestEnder := Trim(Copy(sEnder, p+1));

  p := pos(' ', sRestEnder);
  sNumero := Trim(Copy(sRestEnder, 1, p-1));
  EdNumero.Text := sNumero;
  sRestEnder := Trim(Copy(sRestEnder, p+1));

  p := pos('-', sRestEnder);
  sComplEBairro := Copy(sRestEnder, 1, p-1);
  EdCompl.Text := sComplEBairro;
  EdBairro.Text := '';
  sRestEnder := Trim(Copy(sRestEnder, p+1));

  p := pos(FUFConsulta, sRestEnder);
  sCidade := Trim(Copy(sRestEnder, 1, p-1));
  sCEP := Geral.SoNumero_TT(Copy(sRestEnder, p+3));
  EdCidade.Text := sCidade;
  EdCEP.Text := sCEP;

  EdCNAE.Text := '';
  EdTelefone.Text := '';
  EdEmail.Text := '';
  EdSUframa.Text := '';
end;

procedure TFmEntiRE_IE.ObtemDadosEspecificosDeUF_RN(Txt: String);
var
  sEnd, sLograd, sNum, sCEP, sMunicipio, Tag, sComplBairro: String;
  p: Integer;
begin
  Txt := MeTxtSite.Text;
  //
  EdIE.Text := ObtemTextoEntre(Txt, 'Inscri��o Estadual', 'CNPJ/CPF');
  //
  EdCNPJ.Text := ObtemTextoEntre(Txt, 'CNPJ/CPF', 'Raz�o Social');
  EdRazaoSocial.Text := ObtemTextoEntre(Txt, 'Raz�o Social', 'Nome Fantasia');
  EdFantasia.Text := ObtemTextoEntre(Txt, 'Nome Fantasia', 'Natureza Jur�dica');
  sEnd := ObtemTextoAposLinha(MeTxtSite.Lines, 'Endere�o do Estabelecimento', +1);
  //AV AMINTAS BARROS, 2879 - DIX-SEPT ROSADO - CEP: 59062255 - NATAL/RN
  p := Pos(',', sEnd);
  sLograd := Copy(sEnd, 1, p-1);
  EdLogradouro.Text := Trim(sLograd);
  if p > 0 then
    sEnd := Copy(sEnd, p+1);
  //
  p := Pos('-', sEnd);
  sNum := Copy(sEnd, 1, p-1);
  EdNumero.Text := Trim(sNum);
  if p > 0 then
    sEnd := Trim(Copy(sEnd, p+1));
  //
  Tag := 'CEP:';
  p := Pos(Tag, sEnd);
  sComplBairro := Copy(sEnd, 1, p-Length(Tag));
  EdCompl.Text := Trim(sComplBairro);
  EdBairro.Text := Trim(sComplBairro);
  if p > 0 then
    sEnd := Copy(sEnd, p + Length(Tag));
  //
  Tag := ' - ';
  p := Pos(Tag, sEnd);
  sCEP := Copy(sEnd, 1, p);
  EdCEP.Text := Geral.SONUmero_TT(sCEP);
  if p > 0 then
    sEnd := Trim(Copy(sEnd, p+Length(Tag)));
  //
  //
  p := Pos('/RN', sEnd);
  sMunicipio := Copy(sEnd, 1, p-1);
  EdCidade.Text := Trim(sMunicipio);
  //
  EdCompl.Text := ObtemTextoEntre(Txt, '', '');
  EdBairro.Text := ObtemTextoEntre(Txt, '', '');
  EdCNAE.Text := ObtemTextoEntre(Txt, '', '');
  EdTelefone.Text := ObtemTextoEntre(Txt, '', '');
  EdEmail.Text := ObtemTextoEntre(Txt, '', '');
  EdSUframa.Text := ObtemTextoEntre(Txt, '', '');
{
Inscri��o Estadual
20.099.678-9

CNPJ/CPF
07.189.181/0001-95

Raz�o Social
CAVALCANTI & ALVES LTDA

Nome Fantasia
MULEKADA BUFFET INFANTIL

Natureza Jur�dica
206-2 - SOCIEDADE EMPRESARIA LIMITADA

Tipo Contribuinte
SIMPLES NACIONAL

Regional
1 URT

Produtor Rural de Pequeno Porte
N�O

Contato
Endere�o do Estabelecimento
AV AMINTAS BARROS, 2879 - DIX-SEPT ROSADO - CEP: 59062255 - NATAL/RN

Endere�o para Correspond�ncia
R AGNALDO GURGEL JUNIOR, 1915 - LAGOA NOVA - CEP: 59066030 - NATAL/RN

Telefone
()

Informa��es Complementares
Atividade Econ�mica (CNAE) Principal
( ** Gerador de ICMS **) 5620-1/02 - Servicos de alimenta��o para eventos e recep��es - bufe

Situa��o Cadastral
ATIVO

Situa��o Cadastral Atualizada em
22/11/2010 20:19:21

Cadastro Atualizado em
22/02/2018 13:33:37

Observa��o
Sem Informa��o

Detalhes da Inscri��o
Sem Informa��o

Regime de Pagamento
SIMPLIFICADO

In�cio de Atividade Comercial
05/07/2005

Credenciamento (Data de In�cio)
Credenciamento para ICMS antecipado (07/04/2020)

Emiss�o de NF-e (01/01/2017)

Habilita��o para NFC-e - mod. 65

PAF-ECF Cadastrado
Sem PAF-ECF cadastrado.

Obrigado ao envio de arquivos de EFD
SIM -

Contador
Nome:EMANUEL OLIVEIRA DO NASCIMENTO
Representante(s)
Nome:MARIA DAS GRACAS CAVALCANTI TEIXEIRA
Qualifica��o
ADMINISTRADOR
SOCIO
Nome:SIMONE CAVALCANTI TEXEIRA ALVES
Qualifica��o
ADMINISTRADOR
SOCIO
Governo do Estado do Rio Grande do Norte - Secretaria de Estado da Tributa��o
Centro Administrativo do Estado - Av. Senador Salgado Filho - S/N - Lagoa Nova - CEP: 59064-901 - Natal, RN
Telefone: (84) 3209-7880 ou Entre em contato - Consulte nossas Unidades de Atendimento
}

end;

function TFmEntiRE_IE.ObtemTextoAposLinha(SLTexto: TStrings; TagIni: String;
  LinAfter: Integer): String;
var
  I, Linhas: Integer;
  //s, rTxt: String;
begin
  Result := '';
  if SLTexto.Count > 0 then
  begin
    for I := 0 to SLTexto.Count - 1 do
    begin
      if Uppercase(SLTexto[I]) = Uppercase(TagIni) then
      begin
        Result := SLTexto[I + LinAfter]
      end;
    end;
  end;
end;

function TFmEntiRE_IE.ObtemTextoEntre(Texto, TagIni, TagFim: String): String;
var
  pIni, pFim, Tam: Integer;
  s, rTxt: String;
begin
  Result := '';
  pIni := pos(TagIni, Texto);
  pIni := pIni + Length(TagIni);
  rTxt := Copy(Texto, pIni);
  Tam := pos(TagFim, rTxt) - 1;
  if (pIni > 0) and (tam >0) then
  begin
    //Tam := PFim - pIni;
    s := Copy(Texto, pIni, Tam);
    s := AnsiString(StringReplace(s, #13#10, ' ', [rfReplaceAll]));
    s := AnsiString(StringReplace(s, #13, ' ', [rfReplaceAll]));
    s := AnsiString(StringReplace(s, #10, ' ', [rfReplaceAll]));
    s := AnsiString(StringReplace(s, '  ', ' ', [rfReplaceAll]));
    s := AnsiString(StringReplace(s, #0, '', [rfReplaceAll]));
    s := AnsiString(StringReplace(s, #160, '', [rfReplaceAll]));
    //
    Result := Trim(s);
  end;
end;

procedure TFmEntiRE_IE.PnURLResize(Sender: TObject);
begin
  EdURL_WB.Width := PnURL.Width - 1;
end;

function TFmEntiRE_IE.PoeQuebraDeLinha(Texto: String): String;
begin
  Result := AnsiString(StringReplace(Texto, #10, #13#10, [rfReplaceAll]));
  Result := AnsiString(StringReplace(Result, #13#13, #13, [rfReplaceAll]));
end;

procedure TFmEntiRE_IE.PreencheDadosAPesquisar_IEDocument_CCC();
var
  document: IHTMLDocument2;
  forms : IHTMLElementCollection;
  form : IHTMLFormElement;
  idx : integer;
  //
var
  doc :IHTMLDocument2;
  fieldValue : string;
var
  field: IHTMLElement;
  FieldName, UF_E_Cod, sCNPJ, sIE: String;
begin
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  Memo3.Lines.Clear;
  //
  document := WB_IE.Document AS IHTMLDocument2;
  forms := document.Forms as IHTMLElementCollection;
  //result := TStringList.Create;
  for idx := 0 to -1 + forms.length do
  begin
    form := forms.item(idx,0) as IHTMLFormElement;
    //result.Add(form.name);
    Memo1.Lines.Add('Form ' + IntToStr(idx+1) + ': ' + form.name);
    {
    function WebFormGet(const formNumber: integer; const document: IHTMLDocument2): IHTMLFormElement;
    var
      forms : IHTMLElementCollection;
    begin
      forms := document.Forms as IHTMLElementCollection;
      result := forms.Item(formNumber,'') as IHTMLFormElement;
    end;
    }

    //
(*
<div class="row">
  <input data-val="true" data-val-required="The Origem field is required." id="Origem" name="Origem" type="hidden" value="PortalDfe">
  <input data-val="true" data-val-required="The TipoPesquisa field is required." id="TipoPesquisa" name="TipoPesquisa" type="hidden" value="">

  <div class="form-group col-sm-4">
      <label for="codUf">UF</label>
      <select class="form-control " data-val="true" data-val-number="The field UF must be a number." data-val-required="Informe a UF do contribuinte" id="CodUf" name="CodUf"><option value=""></option>
<option value="12">AC - 12</option>
<option value="27">AL - 27</option>
<option value="13">AM - 13</option>
<option value="16">AP - 16</option>
<option value="29">BA - 29</option>
<option value="23">CE - 23</option>
<option value="53">DF - 53</option>
<option value="32">ES - 32</option>
<option value="52">GO - 52</option>
<option value="21">MA - 21</option>
<option value="31">MG - 31</option>
<option value="50">MS - 50</option>
<option value="51">MT - 51</option>
<option value="15">PA - 15</option>
<option value="25">PB - 25</option>
<option value="26">PE - 26</option>
<option value="22">PI - 22</option>
<option value="41">PR - 41</option>
<option value="33">RJ - 33</option>
<option value="24">RN - 24</option>
<option value="11">RO - 11</option>
<option value="14">RR - 14</option>
<option value="43">RS - 43</option>
<option value="42">SC - 42</option>
<option value="28">SE - 28</option>
<option value="35">SP - 35</option>
<option value="17">TO - 17</option>
</select>
                                <span class="field-validation-valid text-danger" data-valmsg-for="CodUf" data-valmsg-replace="true"></span>
                            </div>
                            <div class="form-group col-sm-8">
                                <label for="ambiente">Ambiente de Processamento</label>
                                <select class="form-control " data-val="true" data-val-range="Ambiente inv�lido" data-val-range-max="2" data-val-range-min="1" data-val-required="Ambiente n�o informado" id="Ambiente" name="Ambiente"><option selected="selected" value="1">Produ��o</option>
<option value="2">Homologa��o</option>
</select>
                                <span class="field-validation-valid text-danger" data-valmsg-for="Ambiente" data-valmsg-replace="true"></span>
                            </div>
                        </div>
     *)
                        //
    //doc := WebBrowser1.Document AS IHTMLDocument2;
    //fieldValue := WebFormFieldValue(doc, 0, FIELDNAME) ;
    fieldName := 'codUf';
    field := form.Item(fieldName,'') as IHTMLElement;
    if field <> nil then
    begin
      UF_E_Cod := '';
      if field.tagName = 'INPUT' then
      begin
        UF_E_Cod := (field as IHTMLInputElement).value;
        //if UF_E_Cod = EmptyStr then
          //(field as IHTMLInputElement).value := Geral.FF0(FCodUF_IBGE);
      end;
      if field.tagName = 'SELECT' then
      begin
        UF_E_Cod := (field as IHTMLSelectElement).value;
        if UF_E_Cod = EmptyStr then
          (field as IHTMLSelectElement).value := Geral.FF0(FCodUF_IBGE);
      end;
      if field.tagName = 'TEXTAREA' then
      begin
        UF_E_Cod := (field as IHTMLTextAreaElement).value;
        if UF_E_Cod = EmptyStr then
          (field as IHTMLTextAreaElement).value := Geral.FF0(FCodUF_IBGE);
      end;
      if UF_E_Cod <> EmptyStr then
      begin
        fieldValue := UF_E_Cod;
        memo2.Lines.Add(
         'Form ' + IntToStr(idx+1) + ': "' + form.name + '" -> ' +
         'Field : "' + fieldName + '", value: "' + fieldValue + '"') ;
      end;
    end;



(*
<input class="form-control  apenasNumeros" data-val="true" data-val-number="The field CodInscrMf must be a number." id="CodInscrMf" name="CodInscrMf" type="text" value="">
*)



(*
<span class="field-validation-valid text-danger" data-valmsg-for="CodInscrMf" data-valmsg-replace="true"></span>
*)
    fieldName := 'CodInscrMf';
    field := form.Item(fieldName,'') as IHTMLElement;
    if field <> nil then
    begin
      sIE := '';
      if field.tagName = 'INPUT' then
      begin
        sCNPJ := (field as IHTMLInputElement).value;
        if (sCNPJ = EmptyStr) and (FCNPJ <> EmptyStr) then
          (field as IHTMLInputElement).value := FCNPJ;
      end;
      if field.tagName = 'SELECT' then
        sCNPJ := (field as IHTMLSelectElement).value;
      if field.tagName = 'TEXTAREA' then
        sCNPJ := (field as IHTMLTextAreaElement).value;
      if sCNPJ <> EmptyStr then
      begin
        fieldValue := sCNPJ;
        memo2.Lines.Add(
        'Form ' + IntToStr(idx+1) + ': "' + form.name + '" -> ' +
        'Field : "' + fieldName + '", value: "' + fieldValue + '"') ;
      end;
    end;



(*
<div class="row" style="margin-left: 7px">
                            <div class="form-group col-sm-8">
                                <label for="CodIe">Inscri��o Estadual</label>
                                <input class="form-control  apenasNumeros" data-val="true" data-val-number="The field CodIe must be a number." id="CodIe" name="CodIe" type="text" value="">
                                <span class="field-validation-valid text-danger" data-valmsg-for="CodIe" data-valmsg-replace="true"></span>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">&nbsp;</label>
                                <button type="button" class="form-control btn btn-default" onclick="pesquisa('Ie')">Pesquisar por IE</button>
                            </div>
                        </div>

                             end;
*)
    fieldName := 'CodIe';
    field := form.Item(fieldName,'') as IHTMLElement;
    if field <> nil then
    begin
      sIE := '';
      if field.tagName = 'INPUT' then
      begin
        sIE := (field as IHTMLInputElement).value;
        if (sIE = EmptyStr) and (FIE <> EmptyStr) and (FCNPJ = EmptyStr) then
          (field as IHTMLInputElement).value := FIE;
      end;
      if field.tagName = 'SELECT' then
        sIE := (field as IHTMLSelectElement).value;
      if field.tagName = 'TEXTAREA' then
        sIE := (field as IHTMLTextAreaElement).value;
      if sIE <> EmptyStr then
      begin
        fieldValue := sIE;
        memo2.Lines.Add(
        'Form ' + IntToStr(idx+1) + ': "' + form.name + '" -> ' +
        'Field : "' + fieldName + '", value: "' + fieldValue + '"') ;
      end;
    end;

  end;
end;

procedure TFmEntiRE_IE.ConfiguraAbas(Mostra: Boolean);
begin
  TabSheet2.TabVisible := Mostra; //Dados separados
  MS.TabVisible        := Mostra; //MS
  TabSheet3.TabVisible := Mostra; //C�digo
  TabSheet4.TabVisible := Mostra; //Texto Plano
  TabSheet5.TabVisible := Mostra; //C�digo Chrome
end;

procedure TFmEntiRE_IE.ConfiguraNavegador(Navegador: Integer);
begin
  Screen.Cursor := crHourglass;
  try
    Panel7.Visible := False;
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Se o resultado da pesquisa aparecer em janela popup e os dados n�o forem lidos ap�s clicar em "L� Dados", copie o texto da janela popup e clique em "L� Dados novamente"');
    //
    WB_IE.Visible := Navegador = 0;
    WB_Ch.Visible := Navegador = 1;
    //
    if (Length(FUFConsulta) = 2) and (CBRE.Items.IndexOf(FUFConsulta) > -1) and
      (EdURL_WB.Text <> '')
    then
      SbNavegaClick(Self);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEntiRE_IE.DefineVariaveis;
var
  CNPJ, RazaoSocial, Fantasia, Rua, Numero, Compl, CEP, Bairro, Cidade, UF,
  Txt1, Txt2, Txt3, CNAE, IE, Telefone, Email, SUFRAMA, ENatal, CodMunici: String;
  P: Integer;
  //Url, S: String;
  //Doc: Variant;
begin
  //Screen.Cursor := crHourGlass;
  //try
    //PageControl1.ActivePageIndex := 1;
    //PreencheEdits();
    //
    CNPJ        := EdCNPJ.Text;
    RazaoSocial := EdRazaoSocial.Text;
    Fantasia    := EdFantasia.Text;
    IE          := EdIE.Text;
    Telefone    := EdTelefone.Text;
    Email       := EdEmail.Text;
    CEP         := EdCEP.Text;
    SUFRAMA     := EdSUFRAMA.Text;
    UF          := EdUF.Text; //Colocar aqui por causa do "EdIE.LinkMsk"
    ENatal      := EdENatal.Text;
    CodMunici   := EdCodMunici.Text;
    case RGOpcoes.ItemIndex of
      0:
      begin
        Rua         := '';
        Numero      := '';
        Compl       := '';
        Bairro      := '';
        Cidade      := '';
        CNAE        := '';
        FTipoImport := 0;
      end;
      1:
      begin
        Rua         := EdLogradouro.Text;
        Numero      := EdNumero.Text;
        Compl       := EdCompl.Text;
        Bairro      := EdBairro.Text;
        Cidade      := EdCidade.Text;
        CNAE        := Geral.SoNumero_TT(EdCNAE.Text);
        FTipoImport := 1;
        //
        if FUFConsulta = 'MT' then
        begin
          P := pos('- MT', Cidade);
          if P > 0 then
          begin
            Cidade := Trim(Copy(Cidade, 1, P-1));
            UF := 'MT';
          end;
        end;
      end
      else
      begin
        Geral.MB_Aviso('Tipo de importa��o n�o definido!');
        Exit;
      end;
    end;
    if Length(CNPJ) > 0 then
      FCNPJ := Geral.SoNumero_TT(Trim(CNPJ))
    else
      FCNPJ := '';
    //
    if Length(RazaoSocial) > 0 then
      FRazaoSocial := Trim(RazaoSocial)
    else
      FRazaoSocial := '';
    //
    if Length(Fantasia) > 0 then
      FFantasia := Trim(Fantasia)
    else
      FFantasia := '';
    //
    if Length(Rua) > 0 then
    begin
      FRua := Trim(Rua);
      //
      Geral.SeparaLogradouro(0, FRua, Txt1, Txt2, Txt3, nil, nil);
      //
      FLogradouro := Txt3;
      FTipoLograd := Geral.IMV(Txt2);
    end else
    begin
      FRua        := '';
      FLogradouro := '';
      FTipoLograd := 0;
    end;
    //
    if Length(Numero) > 0 then
      FNumero := Geral.SoNumero_TT(Numero)
    else
      FNumero := '';
    //
    if Length(Compl) > 0 then
      FCompl := Trim(Compl)
    else
      FCompl := '';
    if (AnsiUppercase(Trim(FCompl)) = 'NUMERO:')
    or (AnsiUppercase(Trim(FCompl)) = 'N�MERO')
    or (AnsiUppercase(Trim(FCompl)) = 'N�MERO:')
    or (AnsiUppercase(Trim(FCompl)) = 'NUMERO')
    or (AnsiUppercase(Trim(FCompl)) = 'BAIRRO')
    or (AnsiUppercase(Trim(FCompl)) = 'BAIRRO:')
    or (AnsiUppercase(Trim(FCompl)) = 'TELEFONE')
    or (AnsiUppercase(Trim(FCompl)) = 'TELEFONE:') then
      FCompl := '';
    //
    if Length(CEP) > 0 then
      FCEP := Trim(CEP)
    else
      FCEP := '';
    //
    if Length(Bairro) > 0 then
      FBairro := Trim(Bairro)
    else
      FBairro := '';
    //
    if Length(Cidade) > 0 then
      FCidade := Trim(Cidade)
    else
      FCidade := '';
    //
    if Length(UF) > 2 then
      FUF := Geral.GetSiglaDaUFdoEstado(UF)
    else
    if Length(UF) > 0 then
      FUF := Trim(UF)
    else
      FUF := '';
    //
    if Length(CNAE) > 0 then
      FCNAE := Trim(CNAE)
    else
      FCNAE := '';
    //
    if Length(IE) > 0 then
      //FIE := Geral.SoNumero_TT(IE)
      FIE := Geral.SoNumeroELetra_TT(IE)
    else
      FIE := '';
    //
    if Length(Telefone) > 0 then
      FTelefone := Geral.SoNumero_TT(Telefone)
    else
      FTelefone := '';
    //
    if Length(Email) > 0 then
      FEmail := Trim(Email)
    else
      FEmail := '';
    //
    if Length(SUFRAMA) > 0 then
      FSUFRAMA := Trim(SUFRAMA)
    else
      FSUFRAMA := '';
    //
    if Length(ENatal) > 0 then
      FENatal := Geral.ValidaDataBR(ENatal, (*PermiteZero*) True,
      (*ForceNextYear*) False, (*MostraMsg*)True)
    else
      FENatal := 0;
    //
    if Length(CodMunici) > 0 then
      FCodMunici := Geral.IMV(CodMunici)
    else
      FCodMunici := 0;
    //end;
  //end;
end;

function TFmEntiRE_IE.RemoveOutros(Linha, OutroIni, OutroFim: String): String;
var
  pIni, pFim: Integer;
  s, r: String;
begin
  Result := Linha;
  pIni := pos(OutroIni, Linha);
  if (pIni > 0) then
  begin
    //Geral.MB_Teste(Linha + sLineBreak + OutroIni + sLineBreak + OutroFim);
    s := Copy(Linha, 1, pIni -1);
    r := Copy(Linha, Length(OutroIni) + 1);
    pFim := pos(OutroFim, r);
    s := s + Copy(r, pFim + 1);
    //
    Result := Trim(s);
  end;
end;

procedure TFmEntiRE_IE.RGNavegadorClick(Sender: TObject);
(*
var
  II: Integer;
*)
begin
  (*
  Screen.cursor := crHourglass;
  Panel7.Visible := False;
  II := RGNavegador.ItemIndex;
  WB_IE.Visible := II = 0;
  WB_Ch.Visible := II = 1;
  //
  if (Length(FUFConsulta) = 2) and (CBRE.Items.IndexOf(FUFConsulta) > -1) and
  (EdURL_WB.Text <> '') then
    SbNavegaClick(Self);
  *)
  ConfiguraNavegador(RGNavegador.ItemIndex);
end;

procedure TFmEntiRE_IE.SbNavegaClick(Sender: TObject);
begin
  try
  case RGNavegador.ItemIndex of
    0:
    begin
      WB_IE.Stop;
      WB_IE.Navigate(EdURL_WB.Text);
    end;
    1: if WB_Ch.Browser <> nil then
    begin
       WB_Ch.Browser.MainFrame.LoadUrl(EdURL_WB.Text);
    end else
    begin
      WB_IE.Stop;
      WB_IE.Navigate('about:blank');
      //
      if WB_Ch.Browser <> nil then
      begin
        WB_Ch.Browser.StopLoad;
        WB_Ch.Browser.MainFrame.LoadUrl('about:blank');
      end;
      //
      Geral.MB_Aviso('Selecione o Navegador!');
    end;
  end;
  except
    on E: Exception do
    begin
      if VAR_USUARIO = -1 then
        Geral.MB_Erro(E.Message);
    end;
  end;
end;

procedure TFmEntiRE_IE.SpeedButton1Click(Sender: TObject);
begin
{
  WB_IE.Visible := True;
  WB_IE.Align := alClient;
  WB_IE.BringToFront;
  WB_IE.Refresh;
  WB_IE.Invalidate;
}
  EdURL_WB.Text := 'http://www.sintegra.gov.br/';
  SbNavegaClick(Self);
end;

procedure TFmEntiRE_IE.WB_ChLoadEnd(Sender: TObject; const browser: ICefBrowser;
  const frame: ICefFrame; httpStatusCode: Integer; out Result: Boolean);

  procedure GetHTML();
  var
    Source: ustring;
    I: Integer;
    Txt: String;
  begin
    Source := frame.Source;
    FCh_Source := frame.Text;
    MeChrome.Text := frame.Text;
    //
    //Geral.MB_Teste(FCh_Source);
    //Geral.MB_Teste(frame.Text);
    Source := AnsiString(StringReplace(String(Source), '</p>', '</p>' + sLineBreak, [rfReplaceAll]));
    Source := AnsiString(StringReplace(String(Source), '</td>', '</td>' + sLineBreak, [rfReplaceAll]));
    Source := AnsiString(StringReplace(String(Source), '</tr>', '</tr>' + sLineBreak, [rfReplaceAll]));
    Source := AnsiString(StringReplace(String(Source), '</div>', '</div>' + sLineBreak, [rfReplaceAll]));
    Source := AnsiString(StringReplace(String(Source), '</span>', '</span>' + sLineBreak, [rfReplaceAll]));
    MeMS.Text := Source;
    Source := Geral.Substitui(Source, '<br>', '');
    Source := dmkPF.ParseText(Source, True, False);
    Source := Geral.Substitui(Source, sLineBreak, #9);
    Source := Geral.Substitui(Source, #10, #9);
    Source := Geral.Substitui(Source, #9, sLineBreak);
    Source := Geral.Substitui(Source, #160, '');
    Source := AnsiString(StringReplace(String(Source), '&nbsp;', ' ', [rfReplaceAll]));
    MeDados.Text := Geral.RemoveTagsHTML(Source);
    MeCodigo.Lines.Clear;
    for I := 1 to MeDados.Lines.Count do
    begin
      Txt := Trim(MeDados.Lines[I]);
      if Txt <> '' then
        MeCodigo.Lines.Add(Txt);
    end;
    MeDados.Lines.Clear;
  end;
  procedure GetText();
  var
    //frame: ICefFrame;
    source: ustring;
    I: Integer;
  begin
    (*
    if crm.Browser = nil then Exit;
    frame := crm.Browser.MainFrame;
    *)
    source := frame.Text;
    source := frame.Text;
    Source := Geral.Substitui(Source, sLineBreak, #9);
    Source := Geral.Substitui(Source, #10, #9);
    Source := Geral.Substitui(Source, #9, sLineBreak);
    Source := Geral.Substitui(Source, #160, '');
    MeCodigo.Text := Source;
    for I := 1 to MeCodigo.Lines.Count do
      MeCodigo.Lines[I] := Trim(MeCodigo.Lines[I]);
    (*
    source := StringReplace(source, '<', '&lt;', [rfReplaceAll]);
    source := StringReplace(source, '>', '&gt;', [rfReplaceAll]);
    source := '<html><body>Text:<pre>' + source + '</pre></body></html>';
    frame.LoadString(source, 'http://tests/gettext');
    *)
  end;
begin
  GetHTML();
  //GetText();
  BtLeDados.Enabled := True;
  Screen.Cursor := crDefault;
end;

procedure TFmEntiRE_IE.WB_ChLoadStart(Sender: TObject;
  const browser: ICefBrowser; const frame: ICefFrame);
begin
  Screen.Cursor := crHourGlass;
  BtLeDados.Enabled := False;
end;

procedure TFmEntiRE_IE.WB_IEBeforeNavigate2(ASender: TObject;
  const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData,
  Headers: OleVariant; var Cancel: WordBool);
begin
  EdURL_A.Text := URL;
  BtLeDados.Enabled := False;
end;

procedure TFmEntiRE_IE.WB_IEDocumentComplete(ASender: TObject;
  const pDisp: IDispatch; const URL: OleVariant);

  // ini CCC
  function WebFormGet(const formNumber: integer; const document: IHTMLDocument2): IHTMLFormElement;
  var
    forms : IHTMLElementCollection;
  begin
    forms := document.Forms as IHTMLElementCollection;
    result := forms.Item(formNumber,'') as IHTMLFormElement;
  end;

{
  function WebFormFieldValue(const document: IHTMLDocument2; const formNumber:
  Integer; const fieldName : string): string;
  var
    form : IHTMLFormElement;
    field: IHTMLElement;
  begin
    form := WebFormGet(formNumber, WebBrowser1.Document AS IHTMLDocument2) ;
    field := form.Item(fieldName,'') as IHTMLElement;
    if field = nil then Exit;
    if field.tagName = 'INPUT' then
      result := (field as IHTMLInputElement).value;
    if field.tagName = 'SELECT' then
      result := (field as IHTMLSelectElement).value;
    if field.tagName = 'TEXTAREA' then
      result := (field as IHTMLTextAreaElement).value;
  end;
}


  function WB_GetHTMLCode(WebBrowser: TWebBrowser; ACode: TStrings): Boolean;
  var
    ps: IPersistStreamInit;
    ss: TStringStream;
    sa: IStream;
    s: string;
  begin
    ps := WebBrowser.Document as IPersistStreamInit;
    s := '';
    ss := TStringStream.Create(s);
    try
      sa := TStreamAdapter.Create(ss, soReference) as IStream;
      Result := Succeeded(ps.Save(sa, True));
      if Result then
         ACode.Add(ss.Datastring);
    finally
      ss.Free;
    end;
  end;

var
  Html : IHTMLElement;
  //Document: IHtmlDocument2;
  //S: String;
{
  //CCC
  forms : TStringList;
}
begin
  if Assigned(WB_IE.Document) then
  begin
    (* Ver o que fazer est� dando erro
    if (WB_IE.Visible = True) and (FCNPJCampo <> '') and (FCNPJ <> '') then
      WB_IE.OleObject.Document.all.Item(FCNPJCampo, 0).value := FCNPJ;
    *)
    Html := (WB_IE.Document as IHTMLDocument2).body;
    while Html.parentElement <> Nil do
      Html := Html.parentElement;
    MeCodigo.Text := Html.outerHTML;
    //Geral.MB_Teste(MeCodigo.Text);
{   CCC
    //////////////////////////
    forms := WebFormNames(WB_IE.Document AS IHTMLDocument2) ;
    try
      memo1.Lines.Assign(forms) ;
      //
    finally
      forms.Free;
    end;
    //////////////////////////
}
    PreencheDadosAPesquisar_IEDocument_CCC();
  end;
  //
  EdURL_B.Text:=url;
  BtLeDados.Enabled := True;

  //CCC
(*
  Html := (WB_IE.Document as IHTMLDocument2).body;
  while Html.parentElement <> Nil do
    Html := Html.parentElement;
*)
  //WebFormNames(const document: IHTMLDocument2): TStringList; var   forms : IHTMLElementCollection;   form : IHTMLFormElement;   idx : integer; begin   forms := document.Forms as IHTMLElementCollection;   result := TStringList.Create;   for idx := 0 to -1 + forms.length do   begin     form := forms.item(idx,0) as IHTMLFormElement;     result.Add(form.name) ;   end; end;
end;

procedure TFmEntiRE_IE.WB_IEDownloadComplete(Sender: TObject);
begin
  vHTML:= Variant(WB_IE.Document).ParentWindow;
end;

procedure TFmEntiRE_IE.WB_IENavigateComplete2(ASender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
begin
{
  if GetCachedFileFromURL(URL, FstrLocalFile) then
    Memo6.Lines.Add('2:  ' + FstrLocalFile);
}
  BtLeDados.Enabled := True;
end;

procedure TFmEntiRE_IE.WB_IEProgressChange(ASender: TObject; Progress,
  ProgressMax: Integer);
begin
  if (Progress>=1) and (ProgressMax>1) then
  begin
    //Ele tira uma valor percentual para colocar no Progressbar
    ProgressBar1.Position := Round((Progress * 100) div ProgressMax);
    ProgressBar1.Visible  := True;
  end else
  begin
    ProgressBar1.Position := 1;
    ProgressBar1.Visible  := False;
  end;
end;

function TFmEntiRE_IE.WebFormNames(const document: IHTMLDocument2): TStringList;
var
  forms : IHTMLElementCollection;
  form : IHTMLFormElement;
  idx : integer;
begin
  forms := document.Forms as IHTMLElementCollection;
  result := TStringList.Create;
  for idx := 0 to -1 + forms.length do
  begin
    form := forms.item(idx,0) as IHTMLFormElement;
    result.Add(form.name);
  end;
end;

end.
