object FmEntiImagens: TFmEntiImagens
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-029 :: Cadastros de Entidades - Imagens'
  ClientHeight = 562
  ClientWidth = 821
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 821
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 967
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbDiretorio: TBitBtn
        Tag = 38
        Left = 5
        Top = 6
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbDiretorioClick
      end
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 908
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 495
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastros de Entidades - Imagens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 495
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastros de Entidades - Imagens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 495
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastros de Entidades - Imagens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 821
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1026
      Height = 503
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1026
        Height = 503
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 450
    Width = 821
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1022
      Height = 35
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 494
    Width = 821
    Height = 68
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 847
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 845
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImagem: TBitBtn
        Tag = 102
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Imagem'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtImagemClick
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 47
    Width = 821
    Height = 403
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 4
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Imagens'
      object Splitter1: TSplitter
        Left = 492
        Top = 0
        Width = 13
        Height = 472
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ExplicitHeight = 469
      end
      object DBCtrlGrid1: TDBCtrlGrid
        AlignWithMargins = True
        Left = 509
        Top = 4
        Width = 505
        Height = 462
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsImagens
        PanelBorder = gbNone
        PanelHeight = 77
        PanelWidth = 488
        ParentShowHint = False
        TabOrder = 0
        RowCount = 6
        SelectedColor = clGradientInactiveCaption
        ShowHint = False
        OnClick = DBCtrlGrid1Click
        OnPaintPanel = DBCtrlGrid1PaintPanel
        ExplicitHeight = 464
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 488
          Height = 77
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          PopupMenu = PMImagem
          TabOrder = 0
          OnDblClick = Panel5DblClick
          object DBText4: TDBText
            Left = 79
            Top = 38
            Width = 394
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMETIP_TXT'
            DataSource = DsImagens
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object DBText3: TDBText
            Left = 79
            Top = 10
            Width = 394
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Nome'
            DataSource = DsImagens
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Image1: TImage
            Left = 10
            Top = 10
            Width = 59
            Height = 59
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Proportional = True
            Stretch = True
            OnClick = Image1Click
          end
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 492
        Height = 472
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 26
          Width = 492
          Height = 446
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENTIDADE'
              Title.Caption = 'Entidade'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Width = 30
              Visible = True
            end>
          Color = clWindow
          DataSource = DsEntidades
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENTIDADE'
              Title.Caption = 'Entidade'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Width = 30
              Visible = True
            end>
        end
        object EdPesquisa: TdmkEdit
          Left = 0
          Top = 0
          Width = 492
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = 'Pesquisa'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 'Pesquisa'
          ValWarn = False
          OnChange = EdPesquisaChange
          OnEnter = EdPesquisaEnter
        end
      end
    end
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Visualiza'#231#227'o'
      ImageIndex = 1
      object Image2: TImage
        Left = 0
        Top = 0
        Width = 1018
        Height = 472
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Center = True
        Proportional = True
        OnClick = Image2Click
        ExplicitWidth = 1017
        ExplicitHeight = 469
      end
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntidadesBeforeClose
    AfterScroll = QrEntidadesAfterScroll
    SQL.Strings = (
      
        'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE, Ativo' +
        ' '
      'FROM entidades '
      'ORDER BY NOMEENTIDADE ')
    Left = 128
    Top = 152
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrEntidadesAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 156
    Top = 152
  end
  object QrImagens: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrImagensCalcFields
    SQL.Strings = (
      'SELECT img.CodEnti, ent.Codigo, 0 + 0.000 TipImg, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END Nome' +
        ','
      'img.Nome NomeImg, img.Codigo CodImg'
      'FROM entidades ent'
      'LEFT JOIN entiimgs img ON '
      '('
      'img.TipImg = 0 AND img.CodEnti = ent.Codigo'
      ')'
      'WHERE ent.Codigo=:P0'
      ''
      'UNION'
      ''
      
        'SELECT img.CodEnti, con.Controle Codigo, 1 + 0.000 TipImg, con.N' +
        'ome,'
      'img.Nome NomeImg, img.Codigo CodImg'
      'FROM enticontat con'
      'LEFT JOIN entiimgs img ON '
      '('
      'img.TipImg = 1 AND img.CodEnti = con.Codigo'
      ')'
      'WHERE con.Codigo=:P0'
      ''
      'UNION'
      ''
      
        'SELECT  img.CodEnti, res.Controle Codigo, 2 + 0.000 TipImg, res.' +
        'Nome,'
      'img.Nome NomeImg, img.Codigo CodImg'
      'FROM entirespon res'
      'LEFT JOIN entiimgs img ON '
      '('
      'img.TipImg = 2 AND img.CodEnti = res.Codigo'
      ')'
      'WHERE res.Codigo=:P0'
      ''
      'ORDER BY TipImg, Nome')
    Left = 128
    Top = 180
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrImagensCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrImagensNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrImagensTipImg: TFloatField
      FieldName = 'TipImg'
      Required = True
    end
    object QrImagensNOMETIP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIP_TXT'
      Size = 100
      Calculated = True
    end
    object QrImagensNomeImg: TWideStringField
      FieldName = 'NomeImg'
      Size = 30
    end
    object QrImagensCodImg: TIntegerField
      FieldName = 'CodImg'
    end
  end
  object DsImagens: TDataSource
    DataSet = QrImagens
    Left = 156
    Top = 180
  end
  object PMImagem: TPopupMenu
    Left = 488
    Top = 192
    object irarfoto1: TMenuItem
      Caption = '&Tirar foto'
      OnClick = irarfoto1Click
    end
    object Carregarimagem1: TMenuItem
      Caption = '&Carregar imagem'
      OnClick = Carregarimagem1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object VisualizarImagem1: TMenuItem
      Caption = '&Visualizar Imagem'
      OnClick = VisualizarImagem1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Removerimagem1: TMenuItem
      Caption = '&Remover imagem'
      OnClick = Removerimagem1Click
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrImagensCalcFields
    Left = 144
    Top = 260
  end
  object OpenPictureDialog1: TOpenPictureDialog
    DefaultExt = 'bmp'
    Filter = 
      'JPEG (*.jpg;*.jpeg;*.jpe;*.jfif)|*.jpg;*.jpeg;*.jpe;*jfif|Bitmap' +
      's (*.bmp)|*.bmp|Icons (*.ico)|*.ico'
    Left = 249
    Top = 173
  end
end
