object FmEntiSrvPro: TFmEntiSrvPro
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-019 :: Consulta a Org'#227'o de Prote'#231#227'o ao Cr'#233'dito'
  ClientHeight = 357
  ClientWidth = 617
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 215
    Width = 617
    Height = 28
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar_: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 0
      Visible = False
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 617
    Height = 64
    Align = alTop
    Caption = ' Dados da entidade: '
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label4: TLabel
      Left = 72
      Top = 20
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdCodUso
    end
    object Label3: TLabel
      Left = 156
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdCodUso: TDBEdit
      Left = 72
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'CodUsu'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DBEdNome: TDBEdit
      Left = 156
      Top = 36
      Width = 449
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'NOMEENTIDADE'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 617
    Height = 103
    Align = alTop
    Caption = ' Dados do contato: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label7: TLabel
      Left = 12
      Top = 56
      Width = 51
      Height = 13
      Caption = 'Resultado:'
    end
    object Label1: TLabel
      Left = 96
      Top = 16
      Width = 32
      Height = 13
      Caption = 'Org'#227'o:'
    end
    object SpeedButton1: TSpeedButton
      Left = 272
      Top = 32
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object LaData: TLabel
      Left = 297
      Top = 16
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object LaHora: TLabel
      Left = 413
      Top = 16
      Width = 26
      Height = 13
      Caption = 'Hora:'
    end
    object Label2: TLabel
      Left = 472
      Top = 16
      Width = 98
      Height = 13
      Caption = 'N'#250'mero da consulta:'
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdResultado: TdmkEdit
      Left = 12
      Top = 72
      Width = 592
      Height = 21
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Resultado'
      UpdCampo = 'Resultado'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CBOrgao: TdmkDBLookupComboBox
      Left = 132
      Top = 32
      Width = 140
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsEntiTipPro
      TabOrder = 2
      dmkEditCB = EdOrgao
      QryCampo = 'Orgao'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdOrgao: TdmkEditCB
      Left = 96
      Top = 32
      Width = 36
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Orgao'
      UpdCampo = 'Orgao'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBOrgao
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdNumero: TdmkEdit
      Left = 472
      Top = 32
      Width = 132
      Height = 21
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Numero'
      UpdCampo = 'Numero'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object TPData: TdmkEditDateTimePicker
      Left = 296
      Top = 32
      Width = 112
      Height = 21
      Date = 40724.768997777780000000
      Time = 40724.768997777780000000
      TabOrder = 3
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'Data'
      UpdCampo = 'Data'
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdHora: TdmkEdit
      Left = 412
      Top = 32
      Width = 57
      Height = 21
      TabOrder = 4
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfLong
      HoraFormat = dmkhfLong
      Texto = '00:00:00'
      QryCampo = 'Hora'
      UpdCampo = 'Hora'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 617
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 569
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 521
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 502
        Height = 32
        Caption = 'Consulta a Org'#227'o de Prote'#231#227'o ao Cr'#233'dito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 502
        Height = 32
        Caption = 'Consulta a Org'#227'o de Prote'#231#227'o ao Cr'#233'dito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 502
        Height = 32
        Caption = 'Consulta a Org'#227'o de Prote'#231#227'o ao Cr'#233'dito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 243
    Width = 617
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 613
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 287
    Width = 617
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 471
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 469
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntiTipPro: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM entitippro'
      'ORDER BY Nome')
    Left = 8
    Top = 12
    object QrEntiTipProCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiTipProNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEntiTipPro: TDataSource
    DataSet = QrEntiTipPro
    Left = 36
    Top = 12
  end
end
