unit EntidadesSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB, DmkDAC_PF,
  dmkDBLookupComboBox, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmEntidadesSel = class(TForm)
    PainelDados: TPanel;
    LaPrompt: TLabel;
    CBSel: TdmkDBLookupComboBox;
    DsSel: TDataSource;
    EdSel: TdmkEditCB;
    QrSel: TmySQLQuery;
    QrSelNO_ENT: TWideStringField;
    QrSelCodigo: TIntegerField;
    QrSelCNPJ_CPF: TWideStringField;
    QrSelCNPJ_CPF_TXT: TWideStringField;
    Panel2: TPanel;
    PnDados: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    QrSelCOD_PART: TWideStringField;
    QrSelCodiPais: TLargeintField;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrSelCalcFields(DataSet: TDataSet);
    procedure EdSelChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmEntidadesSel: TFmEntidadesSel;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmEntidadesSel.BtDesisteClick(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  Close;
end;

procedure TFmEntidadesSel.EdSelChange(Sender: TObject);
begin
  PnDados.Visible := EdSel.ValueVariant <> 0;
end;

procedure TFmEntidadesSel.BtConfirmaClick(Sender: TObject);
begin
  VAR_ENTIDADE := EdSel.ValueVariant;
  if VAR_ENTIDADE = 0 then
  begin
    Application.MessageBox('Nenhuma entidade foi selecionada!', 'Erro',
      MB_OK+MB_ICONERROR);
    Exit;
  end;
  Close;
end;

procedure TFmEntidadesSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  VAR_ENTIDADE := 0;
  UnDmkDAC_PF.AbreQuery(QrSel, Dmod.MyDB);
end;

procedure TFmEntidadesSel.FormResize(Sender: TObject);
begin
   MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidadesSel.QrSelCalcFields(DataSet: TDataSet);
begin
  QrSelCNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrSelCNPJ_CPF.Value);
end;

procedure TFmEntidadesSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
