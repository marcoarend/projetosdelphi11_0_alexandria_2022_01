object FmEntiLoad01: TFmEntiLoad01
  Left = 339
  Top = 185
  Caption = 'ENT-LOAD_-001 :: Importa'#231#227'o de entidades'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Label2: TLabel
      Left = 216
      Top = 4
      Width = 50
      Height = 13
      Caption = 'Progresso:'
    end
    object Label3: TLabel
      Left = 456
      Top = 4
      Width = 45
      Height = 13
      Caption = 'Inseridos:'
    end
    object Label4: TLabel
      Left = 532
      Top = 4
      Width = 57
      Height = 13
      Caption = 'Atualizados:'
    end
    object BtOK: TBitBtn
      Tag = 39
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Carrega'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtCadastra: TBitBtn
      Tag = 14
      Left = 116
      Top = 4
      Width = 90
      Height = 40
      Caption = 'C&adastra'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtCadastraClick
    end
    object PB1: TProgressBar
      Left = 216
      Top = 24
      Width = 237
      Height = 17
      TabOrder = 3
    end
    object EdInser: TdmkEdit
      Left = 456
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdUpdat: TdmkEdit
      Left = 532
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Importa'#231#227'o de entidades'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 398
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 72
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 585
        Height = 72
        Align = alLeft
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 109
          Height = 13
          Caption = 'Arquivo de importa'#231#227'o:'
        end
        object SpeedButton1: TSpeedButton
          Left = 556
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object EdArq: TEdit
          Left = 8
          Top = 20
          Width = 545
          Height = 21
          TabOrder = 0
        end
        object CkMaiusculas: TCheckBox
          Left = 12
          Top = 48
          Width = 97
          Height = 17
          Caption = 'Letra mai'#250'scula.'
          TabOrder = 1
          OnClick = CkMaiusculasClick
        end
      end
      object MeTitulo: TMemo
        Left = 585
        Top = 0
        Width = 421
        Height = 72
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
    end
    object Grade: TStringGrid
      Left = 1
      Top = 73
      Width = 831
      Height = 324
      Align = alClient
      ColCount = 16
      DefaultColWidth = 32
      DefaultRowHeight = 18
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
      TabOrder = 1
      ExplicitWidth = 1006
      ColWidths = (
        32
        191
        73
        95
        81
        37
        171
        54
        25
        161
        48
        121
        71
        69
        100
        32)
    end
    object MeLgr: TdmkMemo
      Left = 832
      Top = 73
      Width = 175
      Height = 324
      Align = alRight
      TabOrder = 2
      WordWrap = False
      UpdType = utYes
    end
  end
  object QrDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu'
      'FROM entidades'
      'WHERE Tipo=:P0'
      'AND CNPJ =:P1'
      'AND CPF =:P1')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDuplCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
end
