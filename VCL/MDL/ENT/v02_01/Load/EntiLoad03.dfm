object FmEntiLoad03: TFmEntiLoad03
  Left = 339
  Top = 185
  Caption = 'ENT-LOAD_-003 :: Importa'#231#227'o de entidades'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 0
    object Label2: TLabel
      Left = 216
      Top = 4
      Width = 50
      Height = 13
      Caption = 'Progresso:'
    end
    object Label3: TLabel
      Left = 456
      Top = 4
      Width = 45
      Height = 13
      Caption = 'Inseridos:'
    end
    object Label4: TLabel
      Left = 532
      Top = 4
      Width = 57
      Height = 13
      Caption = 'Atualizados:'
      Visible = False
    end
    object Label5: TLabel
      Left = 608
      Top = 4
      Width = 59
      Height = 13
      Caption = 'N'#227'o atualiz.:'
      Visible = False
    end
    object BtOK: TBitBtn
      Tag = 39
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Carrega'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtCadastra: TBitBtn
      Tag = 14
      Left = 116
      Top = 4
      Width = 90
      Height = 40
      Caption = 'C&adastra'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtCadastraClick
    end
    object PB1: TProgressBar
      Left = 216
      Top = 24
      Width = 237
      Height = 17
      TabOrder = 3
    end
    object EdInser: TdmkEdit
      Left = 456
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdUpdat: TdmkEdit
      Left = 532
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 5
      Visible = False
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNaoAtz: TdmkEdit
      Left = 608
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 6
      Visible = False
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Importa'#231#227'o de entidades'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 1008
    Height = 398
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = ' Dados '
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 370
        Align = alClient
        ParentBackground = False
        TabOrder = 0
        object Panel3: TPanel
          Left = 1
          Top = 1
          Width = 998
          Height = 88
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 585
            Height = 88
            Align = alLeft
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 4
              Width = 109
              Height = 13
              Caption = 'Arquivo de importa'#231#227'o:'
            end
            object SpeedButton1: TSpeedButton
              Left = 556
              Top = 20
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton1Click
            end
            object Label6: TLabel
              Left = 8
              Top = 44
              Width = 60
              Height = 13
              Caption = 'Condom'#237'nio:'
            end
            object SpeedButton2: TSpeedButton
              Left = 556
              Top = 60
              Width = 21
              Height = 21
              Caption = '...'
              Visible = False
              OnClick = SpeedButton1Click
            end
            object EdArq: TEdit
              Left = 8
              Top = 20
              Width = 545
              Height = 21
              TabOrder = 0
            end
            object EdCondCod: TdmkEdit
              Left = 8
              Top = 60
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdCondNom: TEdit
              Left = 64
              Top = 60
              Width = 489
              Height = 21
              TabOrder = 2
            end
          end
          object Panel5: TPanel
            Left = 585
            Top = 0
            Width = 413
            Height = 88
            Align = alClient
            TabOrder = 1
            object CkMaiusculas: TCheckBox
              Left = 12
              Top = 8
              Width = 97
              Height = 17
              Caption = 'Letra mai'#250'scula.'
              TabOrder = 0
              OnClick = CkMaiusculasClick
            end
            object CkNaoAtz_: TCheckBox
              Left = 12
              Top = 28
              Width = 137
              Height = 17
              Caption = 'N'#227'o atualizar repetidos.'
              Checked = True
              State = cbChecked
              TabOrder = 1
              Visible = False
              OnClick = CkMaiusculasClick
            end
          end
        end
        object DBGrid1: TDBGrid
          Left = 1
          Top = 89
          Width = 998
          Height = 280
          Align = alClient
          DataSource = DsEntiLoad03
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Categor'
              Title.Caption = 'Cond'
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 227
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fantasia'
              Width = 134
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJ'
              Width = 103
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPF'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Endereco'
              Width = 257
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nLogr'
              Width = 39
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'xLogr'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Rua'
              Width = 118
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Numero'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Compl'
              Width = 93
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Bairro'
              Width = 134
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CidUF'
              Width = 154
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cidade'
              Width = 134
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UF'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CEP'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fax'
              Width = 81
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Telex'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tel1'
              Width = 83
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tel2'
              Width = 86
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Contato'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ContaCtb'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IM'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_A_'
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_B_'
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_C_'
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_D_'
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_E_'
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_F_'
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_G_'
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_H_'
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_I_'
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_J_'
              Visible = True
            end>
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = ' Arquivo carregado '
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 1000
        Height = 370
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object QrDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu'
      'FROM entidades'
      'WHERE Tipo=:P0'
      'AND CNPJ =:P1'
      'AND CPF =:P1')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDuplCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrEntiLoad03: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntiLoad03AfterOpen
    BeforeClose = QrEntiLoad03BeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM entiload03')
    Left = 220
    Top = 156
    object QrEntiLoad03Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entiload03.Codigo'
    end
    object QrEntiLoad03_A_: TWideStringField
      FieldName = '_A_'
      Origin = 'entiload03._A_'
      Size = 5
    end
    object QrEntiLoad03Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'entiload03.Nome'
      Size = 100
    end
    object QrEntiLoad03Fantasia: TWideStringField
      FieldName = 'Fantasia'
      Origin = 'entiload03.Fantasia'
      Size = 50
    end
    object QrEntiLoad03_B_: TWideStringField
      FieldName = '_B_'
      Origin = 'entiload03._B_'
      Size = 5
    end
    object QrEntiLoad03_C_: TWideStringField
      FieldName = '_C_'
      Origin = 'entiload03._C_'
      Size = 5
    end
    object QrEntiLoad03CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'entiload03.CNPJ'
    end
    object QrEntiLoad03_D_: TWideStringField
      FieldName = '_D_'
      Origin = 'entiload03._D_'
      Size = 5
    end
    object QrEntiLoad03Endereco: TWideStringField
      FieldName = 'Endereco'
      Origin = 'entiload03.Endereco'
      Size = 100
    end
    object QrEntiLoad03Rua: TWideStringField
      FieldName = 'Rua'
      Origin = 'entiload03.Rua'
      Size = 30
    end
    object QrEntiLoad03Numero: TIntegerField
      FieldName = 'Numero'
      Origin = 'entiload03.Numero'
    end
    object QrEntiLoad03Compl: TWideStringField
      FieldName = 'Compl'
      Origin = 'entiload03.Compl'
      Size = 30
    end
    object QrEntiLoad03Contato: TWideStringField
      FieldName = 'Contato'
      Origin = 'entiload03.Contato'
      Size = 30
    end
    object QrEntiLoad03ContaCtb: TWideStringField
      FieldName = 'ContaCtb'
      Origin = 'entiload03.ContaCtb'
    end
    object QrEntiLoad03CPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'entiload03.CPF'
      Size = 18
    end
    object QrEntiLoad03_E_: TWideStringField
      FieldName = '_E_'
      Origin = 'entiload03._E_'
      Size = 5
    end
    object QrEntiLoad03Bairro: TWideStringField
      FieldName = 'Bairro'
      Origin = 'entiload03.Bairro'
      Size = 30
    end
    object QrEntiLoad03Fax: TWideStringField
      FieldName = 'Fax'
      Origin = 'entiload03.Fax'
    end
    object QrEntiLoad03Telex: TWideStringField
      FieldName = 'Telex'
      Origin = 'entiload03.Telex'
    end
    object QrEntiLoad03IM: TWideStringField
      FieldName = 'IM'
      Origin = 'entiload03.IM'
    end
    object QrEntiLoad03CEP: TWideStringField
      FieldName = 'CEP'
      Origin = 'entiload03.CEP'
      Size = 12
    end
    object QrEntiLoad03_F_: TWideStringField
      FieldName = '_F_'
      Origin = 'entiload03._F_'
      Size = 5
    end
    object QrEntiLoad03CidUF: TWideStringField
      FieldName = 'CidUF'
      Origin = 'entiload03.CidUF'
      Size = 50
    end
    object QrEntiLoad03Tel1: TWideStringField
      FieldName = 'Tel1'
      Origin = 'entiload03.Tel1'
    end
    object QrEntiLoad03_G_: TWideStringField
      FieldName = '_G_'
      Origin = 'entiload03._G_'
      Size = 5
    end
    object QrEntiLoad03Tel2: TWideStringField
      FieldName = 'Tel2'
      Origin = 'entiload03.Tel2'
    end
    object QrEntiLoad03_H_: TWideStringField
      FieldName = '_H_'
      Origin = 'entiload03._H_'
      Size = 5
    end
    object QrEntiLoad03_I_: TWideStringField
      FieldName = '_I_'
      Origin = 'entiload03._I_'
      Size = 5
    end
    object QrEntiLoad03_J_: TWideStringField
      FieldName = '_J_'
      Origin = 'entiload03._J_'
      Size = 5
    end
    object QrEntiLoad03Categor: TWideStringField
      FieldName = 'Categor'
      Origin = 'entiload03.Categor'
      Size = 11
    end
    object QrEntiLoad03nLogr: TIntegerField
      FieldName = 'nLogr'
    end
    object QrEntiLoad03xLogr: TWideStringField
      FieldName = 'xLogr'
    end
    object QrEntiLoad03Cidade: TWideStringField
      FieldName = 'Cidade'
      Size = 30
    end
    object QrEntiLoad03UF: TIntegerField
      FieldName = 'UF'
    end
  end
  object DsEntiLoad03: TDataSource
    DataSet = QrEntiLoad03
    Left = 248
    Top = 156
  end
end
