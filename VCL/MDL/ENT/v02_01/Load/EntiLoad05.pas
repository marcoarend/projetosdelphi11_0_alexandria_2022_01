unit EntiLoad05;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, Mask, dmkEdit, mySQLDbTables, UnDmkEnums;

type
  TFmEntiLoad05 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    LaAviso: TLabel;
    EdArq: TdmkEdit;
    PB1: TProgressBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Grade1: TStringGrid;
    TabSheet2: TTabSheet;
    BtAbre: TBitBtn;
    BtCarrega: TBitBtn;
    StringGrid1: TStringGrid;
    Memo1: TMemo;
    QrBloco: TmySQLQuery;
    QrBlocoCodigo: TIntegerField;
    QrBlocoControle: TIntegerField;
    QrBlocoDescri: TWideStringField;
    QrImov: TmySQLQuery;
    QrImovCodigo: TIntegerField;
    QrImovControle: TIntegerField;
    QrImovConta: TIntegerField;
    QrImovUnidade: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCod_Cond: Integer;
  end;

  var
  FmEntiLoad05: TFmEntiLoad05;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmEntiLoad05.BtAbreClick(Sender: TObject);
begin
  MyObjects.Xls_To_StringGrid(Grade1, EdArq.Text, PB1, LaAviso, nil);
  BtCarrega.Enabled := True;
end;

procedure TFmEntiLoad05.BtCarregaClick(Sender: TObject);
const
  x = 'Examinando/cadastrando dados da linha ';
  //
  A1_a = 'QUADRA';     Col_NO_BLOC = 1;
  B1_a = 'LOTE';       Col_NO_U_H_ = 2;
  C1_a = 'NOME';       Col_NO_ENTI = 3;
  D1_a = 'CGC_CIC';    Col_DOCUM_1 = 4;
  E1_a = 'RG';         Col_DOCUM_2 = 5;
  F1_a = 'CONSTR';     Col_CONSTRU = 6;
  G1_a = 'FONE';       Col_NO_xTe1 = 7;
  H1_a = 'CELULAR';    Col_NO_xCel = 8;
  I1_a = 'CELULAR_1';  Col_NO_xTe2 = 9;
  J1_a = 'ENDERECO';   Col_NO_Ende = 10;
  K1_a = 'CIDADE';     Col_xCidade = 11;
  L1_a = 'ESTADO';     Col_xEstado = 12;
  M1_a = 'CEP';        Col_xCEP___ = 13;
  N1_a = 'E_MAIL';     Col_E_Mail_ = 14;
  //O1_a = '';           Col_O1_a = 15;
var
  Erros: String;
  procedure IncErr(Coluna: Integer; Campo: String);
  //
  begin
    Erros := Erros + sLineBreak + 'Coluna ' + Char(Coluna + 64) + ': ' + Campo;
  end;
  //
  var
    I, Codigo, CodUsu, Controle, Conta, nUF_, nCEP, Registros: Integer;
    y, DOC1, DOC2, CNPJ, CPF, RG, IE, NO_E, xTe1, xCel, xTe2, xCid, xUF_,
    Nume, Copl, Descri, _U_H_, Unidade,
    RazaoSocial, Nome, Ende, Bair, xLog, nLog, xRua, Mail: String;
    ETe1, ECel, ETe2, ERua, ECidade, EEmail, ECompl: String;
    PTe1, PCel, PTe2, PRua, PCidade, PEmail, PCompl: String;
    ELograd, ENumero, EUF, ECEP: Integer;
    PLograd, PNumero, PUF, PCEP, Tipo: Integer;
    //
    Propriet: Integer;
begin
  Propriet := 0;
  Codigo   := FCod_Cond;
  Registros := 0;
  //ver se condominio est� vazio!
  //
  Erros := '';
  if Grade1.Cells[Col_NO_BLOC, 00] <> A1_a then IncErr(1, A1_a);
  if Grade1.Cells[Col_NO_U_H_, 00] <> B1_a then IncErr(1, B1_a);
  if Grade1.Cells[Col_NO_ENTI, 00] <> C1_a then IncErr(1, C1_a);
  if Grade1.Cells[Col_DOCUM_1, 00] <> D1_a then IncErr(1, D1_a);
  if Grade1.Cells[Col_DOCUM_2, 00] <> E1_a then IncErr(1, E1_a);
  if Grade1.Cells[Col_CONSTRU, 00] <> F1_a then IncErr(1, F1_a);
  if Grade1.Cells[Col_NO_xTe1, 00] <> G1_a then IncErr(1, G1_a);
  if Grade1.Cells[Col_NO_xCel, 00] <> H1_a then IncErr(1, H1_a);
  if Grade1.Cells[Col_NO_xTe2, 00] <> I1_a then IncErr(1, I1_a);
  if Grade1.Cells[Col_NO_Ende, 00] <> J1_a then IncErr(1, J1_a);
  if Grade1.Cells[Col_xCidade, 00] <> K1_a then IncErr(1, K1_a);
  if Grade1.Cells[Col_xEstado, 00] <> L1_a then IncErr(1, L1_a);
  if Grade1.Cells[Col_xCEP___, 00] <> M1_a then IncErr(1, M1_a);
  if Grade1.Cells[Col_E_Mail_, 00] <> N1_a then IncErr(1, N1_a);
  //
  if Erros <> '' then
  begin
    Geral.MB_Aviso('Colunas esperadas e n�o encontradas: ' + Erros);
    Exit;
  end;
  //
  PB1.Position := 0;
  PB1.Max := Grade1.RowCount;
  //
  for I := 1 to Grade1.RowCount - 1 do
  begin
    y := Geral.FF0(I) + '. ';
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, x + y );
    //
    Descri  := Trim(Grade1.Cells[Col_NO_BLOC, I]);
    _U_H_   := Trim(Grade1.Cells[Col_NO_U_H_, I]);
    //
    DOC1 := Trim(Geral.SoNumero_TT(Grade1.Cells[Col_DOCUM_1, I]));
    DOC2 := Trim(Geral.SoNumero_TT(Grade1.Cells[Col_DOCUM_2, I]));
    NO_E := Trim(Grade1.Cells[Col_NO_ENTI, I]);
    xTe1 := Trim(Grade1.Cells[Col_NO_xTe1, I]);
    xCel := Trim(Grade1.Cells[Col_NO_xCel, I]);
    xTe2 := Trim(Grade1.Cells[Col_NO_xTe2, I]);
    Ende := Trim(Grade1.Cells[Col_NO_Ende, I]);
    Geral.SeparaEndereco(I, Ende, Nume, Copl, Bair, xLog, nLog, xRua,
      False, Memo1, StringGrid1);
    xCid := Trim(Grade1.Cells[Col_xCidade, I]);
    xUF_ := Trim(Grade1.Cells[Col_xEstado, I]);
    nUF_ := Geral.GetCodigoUF_da_SiglaUF(xUF_);
    Mail := Trim(Grade1.Cells[Col_E_Mail_, I]);
    nCEP := Geral.IMV(Trim(Geral.SoNumero_TT(Grade1.Cells[Col_xCEP___, I])));
    //
    Geral.ArrumaTelefoneBR_SemZero(xTe1, xTe1);
    Geral.ArrumaTelefoneBR_SemZero(xCel, xCel);
    Geral.ArrumaTelefoneBR_SemZero(xTe2, xTe2);
    //
    if (Descri <> '') or (_U_H_ <> '') then
    begin
      if (NO_E <> '') or (DOC1 <> '') then
      begin
        if not DModG.ObtemEntidadeDeCNPJCFP(DOC1, Propriet) then
        if not DModG.ObtemEntidadeDeNO_ENT(NO_E, Propriet) then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, x + y +
            'Cadastrando entidade ' + NO_E);
          //
          Tipo        := 1;
          CNPJ        := '';
          CPF         := '';
          RG          := '';
          IE          := '';
          RazaoSocial := '';
          Nome        := '';
          ETe1        := '';
          ECel        := '';
          ETe2        := '';
          ERua        := '';
          ECompl      := '';
          ECidade     := '';
          EEmail      := '';
          PTe1        := '';
          PCel        := '';
          PTe2        := '';
          PRua        := '';
          PCompl      := '';
          PCidade     := '';
          PEmail      := '';
          ELograd     := 0;
          ENumero     := 0;
          EUF         := 0;
          ECEP        := 0;
          PLograd     := 0;
          PNumero     := 0;
          PUF         := 0;
          PCEP        := 0;
          //

          if Length(DOC1) >= 14 then
          begin
            Tipo        := 0;
            CNPJ        := DOC1;
            IE          := DOC2;
            RazaoSocial := NO_E;
            ETe1        := xTe1;
            ECel        := xCel;
            ETe2        := xTe2;
            ERua        := xRua;
            ECompl      := Copl;
            ECidade     := xCid;
            EEmail      := Mail;
            ELograd     := Geral.IMV(nLog);
            ENumero     := Geral.IMV(Nume);
            EUF         := nUF_;
            ECEP        := nCEP;
          end else
          begin
            Tipo        := 1;
            CPF         := DOC1;
            RG          := DOC2;
            Nome        := NO_E;
            PTe1        := xTe1;
            PCel        := xCel;
            PTe2        := xTe2;
            PRua        := xRua;
            PCompl      := Copl;
            PCidade     := xCid;
            PEmail      := Mail;
            PLograd     := Geral.IMV(nLog);
            PNumero     := Geral.IMV(Nume);
            PUF         := nUF_;
            PCEP        := nCEP;
          end;

          Codigo := UMyMod.BuscaEmLivreY_Def('Entidades', 'Codigo', stIns, 0);
          CodUsu := Codigo;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
          'Tipo', 'CNPJ', 'CPF',
          'RG', 'IE', 'RazaoSocial',
          'Nome', 'ETe1', 'ECel',
          'ETe2', 'ERua', 'ECidade',
          'EEmail', 'PTe1', 'PCel',
          'PTe2', 'PRua', 'PCidade',
          'PEmail', 'ELograd', 'ENumero',
          'EUF', 'ECEP', 'PLograd',
          'PNumero', 'PUF', 'PCEP',
          'ECompl', 'PCompl', 'CodUsu'
          ], ['Codigo'
          ], [
          Tipo, CNPJ, CPF,
          RG, IE, RazaoSocial,
          Nome, ETe1, ECel,
          ETe2, ERua, ECidade,
          EEmail, PTe1, PCel,
          PTe2, PRua, PCidade,
          PEmail, ELograd, ENumero,
          EUF, ECEP, PLograd,
          PNumero, PUF, PCEP,
          ECompl, PCompl, CodUsu
          ], [Codigo], True) then
          begin
            Propriet := Codigo;
            Registros := Registros + 1;
          end;
        end;
      end;

      Codigo := FCod_Cond;

      // TABELA: CondBloco
      UnDmkDAC_PF.AbreMySQLQuery0(QrBloco, Dmod.MyDB, [
      'SELECT Codigo, Controle, Descri ',
      'FROM condbloco ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Descri="' + Descri + '" ',
      '']);
      //
      Controle := QrBlocoControle.Value;
      if Controle = 0 then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, x + y +
          'Cadastrando bloco ' + Descri);
        Controle := UMyMod.BuscaEmLivreY_Def('condbloco', 'Controle', stIns, 0);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'condbloco', False, [
        'Codigo', 'Descri'(*, 'Ordem',
        'SomaFracao', 'PrefixoUH', 'IDExporta'*)], [
        'Controle'], [
        Codigo, Descri(*, Ordem,
        SomaFracao, PrefixoUH, IDExporta*)], [
        Controle], True) then
          Registros := Registros + 1;
        //
      end;

      // TABELA: CondImov
      Unidade := 'Q' + Descri + '-L' + _U_H_;
      UnDmkDAC_PF.AbreMySQLQuery0(QrImov, Dmod.MyDB, [
      'SELECT Codigo, Controle, Conta, Unidade ',
      'FROM condimov ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Controle=' + Geral.FF0(Controle),
      'AND Unidade="' + Unidade + '" ',
      '']);
      //
      Conta := QrImovConta.Value;
      if Conta = 0 then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, x + y +
          'Cadastrando unidade ' + Unidade);
        Conta := UMyMod.BuscaEmLivreY_Def('condimov', 'Conta', stIns, 0);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'condimov', False, [
        'Codigo', 'Controle', 'Propriet',
        (*'Conjuge', 'Andar',*) 'Unidade'(*,
        'QtdGaragem', 'Status', 'ENome1',
        'EHoraSSIni1', 'EHoraSSSai1', 'EHoraSDIni1',
        'EHoraSDSai1', 'ESegunda1', 'ETerca1',
        'EQuarta1', 'EQuinta1', 'ESexta1',
        'ESabado1', 'EDomingo1', 'EVeic1',
        'EFilho1', 'ENome2', 'EHoraSSIni2',
        'EHoraSSSai2', 'EHoraSDIni2', 'EHoraSDSai2',
        'ESegunda2', 'ETerca2', 'EQuarta2',
        'EQuinta2', 'ESexta2', 'ESabado2',
        'EDomingo2', 'EVeic2', 'EFilho2',
        'ENome3', 'EHoraSSIni3', 'EHoraSSSai3',
        'EHoraSDIni3', 'EHoraSDSai3', 'ESegunda3',
        'ETerca3', 'EQuarta3', 'EQuinta3',
        'ESexta3', 'ESabado3', 'EDomingo3',
        'EVeic3', 'EFilho3', 'EmNome1',
        'EmTel1', 'EmNome2', 'EmTel2',
        'Observ', 'SitImv', 'Imobiliaria',
        'Contato', 'ContTel', 'Usuario',
        'WebLogin', 'WebPwd', 'WebNivel',
        'WebLogID', 'WebLastLog', 'Protocolo',
        'Moradores', 'FracaoIdeal', 'ModelBloq',
        'ConfigBol', 'BloqEndTip', 'BloqEndEnt',
        'EnderLin1', 'EnderLin2', 'EnderNome',
        'Procurador', 'ddVctEsp', 'IDExporta',
        'Juridico', 'InadCEMCad', 'SMSCelTipo',
        'SMSCelNumr', 'SMSCelNome', 'SMSCelEnti'*)], [
        'Conta'], [
        Codigo, Controle, Propriet,
        (*Conjuge, Andar,*) Unidade(*,
        QtdGaragem, Status, ENome1,
        EHoraSSIni1, EHoraSSSai1, EHoraSDIni1,
        EHoraSDSai1, ESegunda1, ETerca1,
        EQuarta1, EQuinta1, ESexta1,
        ESabado1, EDomingo1, EVeic1,
        EFilho1, ENome2, EHoraSSIni2,
        EHoraSSSai2, EHoraSDIni2, EHoraSDSai2,
        ESegunda2, ETerca2, EQuarta2,
        EQuinta2, ESexta2, ESabado2,
        EDomingo2, EVeic2, EFilho2,
        ENome3, EHoraSSIni3, EHoraSSSai3,
        EHoraSDIni3, EHoraSDSai3, ESegunda3,
        ETerca3, EQuarta3, EQuinta3,
        ESexta3, ESabado3, EDomingo3,
        EVeic3, EFilho3, EmNome1,
        EmTel1, EmNome2, EmTel2,
        Observ, SitImv, Imobiliaria,
        Contato, ContTel, Usuario,
        WebLogin, WebPwd, WebNivel,
        WebLogID, WebLastLog, Protocolo,
        Moradores, FracaoIdeal, ModelBloq,
        ConfigBol, BloqEndTip, BloqEndEnt,
        EnderLin1, EnderLin2, EnderNome,
        Procurador, ddVctEsp, IDExporta,
        Juridico, InadCEMCad, SMSCelTipo,
        SMSCelNumr, SMSCelNome, SMSCelEnti*)], [
        Conta], True) then
          Registros := Registros + 1;
        //
      end;
    end;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
  'Importa��o finalizada! Registros inclu�dos: ' + Geral.FF0(Registros));
end;

procedure TFmEntiLoad05.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiLoad05.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiLoad05.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
  EdArq.ValueVariant           := '';
end;

procedure TFmEntiLoad05.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiLoad05.SpeedButton8Click(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Sele��o de arquivo', '', [], Arquivo) then
    EdArq.ValueVariant := Arquivo
  else
    EdArq.ValueVariant := '';
end;

end.
