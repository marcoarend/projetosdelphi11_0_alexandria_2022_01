unit EntiLoad01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DB,
  mySQLDbTables, dmkEdit, ComCtrls, dmkMemo;

type
  TFmEntiLoad01 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Grade: TStringGrid;
    Panel4: TPanel;
    Label1: TLabel;
    EdArq: TEdit;
    SpeedButton1: TSpeedButton;
    MeTitulo: TMemo;
    BtCadastra: TBitBtn;
    QrDupl: TmySQLQuery;
    QrDuplCodigo: TIntegerField;
    QrDuplCodUsu: TIntegerField;
    PB1: TProgressBar;
    Label2: TLabel;
    EdInser: TdmkEdit;
    Label3: TLabel;
    EdUpdat: TdmkEdit;
    Label4: TLabel;
    CkMaiusculas: TCheckBox;
    MeLgr: TdmkMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCadastraClick(Sender: TObject);
    procedure CkMaiusculasClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEntiLoad01: TFmEntiLoad01;

implementation

uses UnMyObjects, dmkGeral, UMySQLModule, Module;

{$R *.DFM}

procedure TFmEntiLoad01.BtCadastraClick(Sender: TObject);
  procedure InsereAtual(Linha: Integer);
  var
    SQLType: TSQLType;
    EUF, PUF, Codigo, CodUsu, PNumero, ENumero, Tipo: Integer;
    Doc1: String;
    RazaoSocial, Fantasia, CNPJ, IE,
    Nome, Apelido, CPF, RG, SSP,

    ELograd, ERua,
    ECompl, EBairro, ECidade,
    ECEP, EPais, ETe1,

    PLograd, PRua,
    PCompl, PBairro, PCidade,
    PCEP, PPais, PTe1: String;

  begin
    Doc1 := Geral.SoNumero_TT(Grade.Cells[03,Linha]);
    if Length(Doc1) < 14 then
    begin
      Tipo        := 1;
      CPF         := Doc1;
      Nome        := Grade.Cells[01,Linha];  //Nome';
      PTe1        := Grade.Cells[02,Linha];  //Telefone';
      RG          := Grade.Cells[04,Linha];  //RG';
      PLograd     := Grade.Cells[08,Linha];  //C�d.';
      PRua        := Grade.Cells[09,Linha];  //Nome';
      PNumero     := Geral.IMV(Grade.Cells[10,Linha]);  //Num.';
      PCompl      := Grade.Cells[11,Linha];  //Compl.';
      PBairro     := Grade.Cells[12,Linha];  //Bairro';
      PCEP        := Grade.Cells[13,Linha];  //CEP';
      PCidade     := Grade.Cells[14,Linha];  //Cidade';
      PUF         := Geral.GetCodigoUF_da_SiglaUF(Grade.Cells[15,Linha]);  //UF';
      //
      CNPJ        := '';
      RazaoSocial := '';
      ETe1        := '';
      IE          := '';
      ELograd     := '';
      ERua        := '';
      ENUmero     := 0;
      ECompl      := '';
      EBairro     := '';
      ECEP        := '';
      ECidade     := '';
      EUF         := 0;
    end else begin
      Tipo        := 0;
      CNPJ        := Doc1;
      RazaoSocial := Grade.Cells[01,Linha];
      ETe1        := Grade.Cells[02,Linha];
      IE          := Grade.Cells[04,Linha];
      ELograd     := Grade.Cells[08,Linha];
      ERua        := Grade.Cells[09,Linha];
      ENUmero     := Geral.IMV(Grade.Cells[10,Linha]);
      ECompl      := Grade.Cells[11,Linha];
      EBairro     := Grade.Cells[12,Linha];
      ECEP        := Grade.Cells[13,Linha];
      ECidade     := Grade.Cells[14,Linha];
      EUF         := Geral.GetCodigoUF_da_SiglaUF(Grade.Cells[15,Linha]);
      //
      CPF         := '';
      Nome        := '';
      PTe1        := '';
      RG          := '';
      PLograd     := '';
      PRua        := '';
      PNumero     := 0;
      PCompl      := '';
      PBairro     := '';
      PCEP        := '';
      PCidade     := '';
      PUF         := 0;
    end;
    SSP   := Grade.Cells[05,Linha];  //SSP';
           //Grade.Cells[06,Linha];  //Logradouro'; Completo
           //Grade.Cells[07,Linha];  //Tipo'; Abrev. tipo lograd.
    //
    Apelido     := Nome;
    Fantasia    := RazaoSocial;
    //
    QrDupl.Close;
    QrDupl.SQL.Clear;
    QrDupl.SQL.Add('SELECT Codigo, CodUsu');
    QrDupl.SQL.Add('FROM entidades');
    QrDupl.SQL.Add('WHERE Tipo=' + IntToStr(Tipo));
    if Tipo = 0 then
    begin
      if CNPJ <> '' then
        QrDupl.SQL.Add('AND CNPJ = "' + CNPJ + '"')
      else
        QrDupl.SQL.Add('AND RazaoSocial = "' + RazaoSocial + '"')
    end else begin
      if CPF <> '' then
        QrDupl.SQL.Add('AND CPF = "' + CPF + '"')
      else
        QrDupl.SQL.Add('AND Nome = "' + Nome + '"')
    end;
    QrDupl.Open;
    if QrDupl.RecordCount > 0 then
      SQLType := stUpd
    else begin
      SQLType := stIns;
    end;
    if SQLType = stIns then
    begin
      Codigo := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', SQLType, 0);
      CodUsu := Codigo;
    end else begin
      Codigo := QrDuplCodigo.Value;
      CodUsu := QrDuplCodUsu.Value;
    end;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entidades', False, [
    'RazaoSocial', 'Fantasia', 'CNPJ', 'IE',
    'Nome', 'Apelido', 'CPF', 'RG', 'SSP',

    'ELograd', 'ERua', 'ENumero',
    'ECompl', 'EBairro', 'ECidade',
    'EUF', 'ECEP', 'EPais',

    'PLograd', 'PRua', 'PNumero',
    'PCompl', 'PBairro', 'PCidade',
    'PUF', 'PCEP', 'PPais',

    'ETe1', 'PTe1', 'Cliente2',

    'Tipo', 'CodUsu'], ['Codigo'], [
    RazaoSocial, Fantasia, CNPJ, IE,
    Nome, Apelido, CPF, RG, SSP,

    ELograd, ERua, ENumero,
    ECompl, EBairro, ECidade,
    EUF, ECEP, EPais,

    PLograd, PRua, PNumero,
    PCompl, PBairro, PCidade,
    PUF, PCEP, PPais,

    ETe1, PTe1, 'V',

    Tipo, CodUsu], [Codigo], True) then
    begin
      PB1.Position := PB1.Position + 1;
      if SQLType = stIns then
        EdInser.ValueVariant := EdInser.ValueVariant + 1
      else
        EdUpdat.ValueVariant := EdUpdat.ValueVariant + 1;
    end;
  end;
var
  I: Integer;
begin
  // 2349 entidades antes do teste
  PB1.Position := 0;
  PB1.Max      := Grade.RowCount - 1;
  EdInser.ValueVariant := 0;
  EdUpdat.ValueVariant := 0;
  //
  for I := 1 to Grade.RowCount - 1 do
    InsereAtual(i);
  Application.MessageBox('Cadastramento concluido!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmEntiLoad01.BtOKClick(Sender: TObject);
const
  TagNome = 'COMPRADOR:';
  TagTel  = 'TEL:';
  TagDc1  = 'CPF:';
  TagDc0  = 'CNPJ:';
  TagRG   = 'RG:';
  TagEnd  = 'END:';
  TagCEP  = 'CEP:';
  TagCid  = 'CIDADE:';

var
  Lista: TStringList;
  i, n, p1, p2, ini, Fim: Integer;
  TagX, Linha, Nome, Tel, CPF, RG, RG2, SSP, Logr, Num, Compl, Bairro, Txt,
  Sep1, Sep2, Sep3, CEP, Cid, UF: String;
  Achou: Boolean;
  X: Char;
begin
  MyObjects.LimpaGrade(Grade, 1, 1, True);
  MeTitulo.Lines.Clear;
  if not FileExists(EdArq.Text) then
  begin
    Application.MessageBox(PChar('Arquivo n�o localizado: ' + EdArq.Text),
      'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  //
  Lista := TStringList.Create;
  try
    Lista.LoadFromFile(EdArq.Text);
    n := 0;
    for i := 0 to Lista.Count -1 do
    begin
      Achou := False;
      Linha := Lista[i];

      // Nome, Tel
      p1 := pos(TagNome, Linha);
      if p1 > 0 then
      begin
        Achou := True;
        ini := 1 + p1 + Length(TagNome);
        n := n + 1;
        Grade.RowCount := n + 1;
        Grade.Cells[00,n] := IntToStr(n);
        //
        p2 := pos(TagTel, Linha);
        if p2 > 0 then
        begin
          Nome := Trim(Copy(Linha, Ini, p2 - Ini));
          Tel := Geral.SoNumero_TT(Trim(Copy(Linha, p2 + Length(TagTel))));
          while Length(Tel) > 10 do
            Tel := Copy(Tel, 2);
        end else begin
          Nome := Trim(Copy(Linha, Ini));
          Tel  := '';
        end;
        Grade.Cells[01,n] := Nome;
        Grade.Cells[02,n] := Tel;
      end;

      // CPF, RG e SSP
      TagX := tagDc1;
      p1 := pos(tagX, Linha);
      if p1 = 0 then
      begin
        TagX := tagDc0;
        p1 := pos(TagX, Linha);
      end;
      if p1 > 0 then
      begin
        Achou := True;
        ini := 1 + p1 + Length(TagX);
        p2  := pos(TagRG, Linha);
        if p2 > 0 then
        begin
          CPF := Trim(Copy(Linha, Ini, p2 - Ini));
          RG  := Uppercase(Trim(Copy(Linha, p2 + Length(TagRG))));
          //
          SSP := '';
          //Tam := 0;
          if Length(RG) > 0 then
          begin
            for Fim := Length(RG) downto 1 do
            begin
              if RG[Fim] in (['A'..'Z']) then
              begin
                SSP := RG[Fim] + SSP;
              end else
              if RG[Fim] in (['0'..'9']) then
                Break;
            end;
            RG2 := Copy(RG, 1, Length(RG) - Length(SSP));
            while (Length(RG2) > 0) and (not (RG2[Length(RG2)] in (['0'..'9']))) do
              RG2 := Copy(RG2, 1, Length(RG2) - 1);
          end else RG2 := RG;
        end else begin
          CPF := Trim(Copy(Linha, Ini));
          RG  := '';
          RG2 := '';
          SSP := '';
        end;
        Grade.Cells[03,n] := CPF;
        Grade.Cells[04,n] := RG2;
        Grade.Cells[05,n] := SSP;
      end;

      // Endere�o:
      Logr := '';
      Num := '';

      p1 := pos(TagEnd, Linha);
      if p1 > 0 then
      begin
        Achou := True;
        ini := 1 + p1 + Length(TagEnd);
        p2  := pos(',', Linha);
        if p2 > 0 then
        begin
          Logr := Trim(Copy(Linha, Ini, p2 - Ini));
          Linha := Trim(Copy(Linha, p2+1));

          // Numero
          if Length(Linha) > 0 then
          begin
            X := Linha[1];
            if X in (['0'..'9']) then
            begin
              Num := '';
              while X in (['0'..'9','.']) do
              begin
                Num := Num + X;
                Linha := Copy(Linha, 2);
                if Length(Linha) > 0 then
                  X := Linha[1]
                else X := ' ';
              end;
            end else Num := '';
          end;

          // Limpar texto apartamento/bairro
          if Length(Linha) > 0 then
          begin
            X := Linha[1];
            if not (X in (['A'..'Z'])) then
            begin
              while (not (X in (['A'..'Z']))) and (Length(Linha) > 0) do
              begin
                Linha := Copy(Linha, 2);
                if Length(Linha) > 0 then
                  X := Linha[1]
                else X := 'A';
              end;
            end;
          end;

          // Apto = Bairro
          Compl := '';
          Bairro := '';
          p1 := pos(#150, Linha);
          if p1 = 0 then p1 := pos('-', Linha);
          if p1 > 0 then
          begin
            Compl  := Trim(Copy(Linha, 1, p1 - 1));
            Bairro := Trim(Copy(Linha, p1 + 1));
            //ShowMessage(Compl + ' # ' + Bairro);
          end else begin
            Txt := Geral.Maiusculas(Linha, False);
            if pos('APTO', Txt) > 0 then
              Compl := Linha
            else
            if pos('AP.', Txt) > 0 then
              Compl := Linha
            else
            if pos('APT.', Txt) > 0 then
              Compl := Linha
            else
            if pos('SALA', Txt) > 0 then
              Compl := Linha
            else
            if pos('LOJA', Txt) > 0 then
              Compl := Linha
            else
            if pos('CASA', Txt) > 0 then
              Compl := Linha
            else
            if pos('COMERCIO', Txt) > 0 then
              Compl := Linha
            else
            if pos('ZONA', Txt) > 0 then
              Bairro := Linha
            else
            if pos('BAIRRO', Txt) > 0 then
              Bairro := Linha
            else
            if pos('JD', Txt) > 0 then
              Bairro := Linha
            else
            if pos('CJ', Txt) > 0 then
              Bairro := Linha
            else
            if pos('PQ', Txt) > 0 then
              Bairro := Linha
            else
            if pos('V.', Txt) > 0 then
              Bairro := Linha
            else
            if pos('VILA', Txt) > 0 then
              Bairro := Linha
            else
            if pos('CENTRO', Txt) > 0 then
              Bairro := Linha
            else
            if pos('CH�CARA', Txt) > 0 then
              Bairro := Linha
            else
            if pos('INDUS', Txt) > 0 then
              Bairro := Linha
            else
            if pos('RES', Txt) > 0 then
              Bairro := Linha
            {
            else
            if pos('CJ', Txt) then
              Compl := Linha
            }
            else begin
              if Trim(Linha) <> '' then
                if Application.MessageBox(PChar('"' + Linha + '" � um bairro?'),
                'Pergunta', MB_YESNO+MB_ICONQUESTION) = ID_YES then
                  Bairro := Linha
                else
                  Compl := Linha;
            end;
          end;
          // Parei aqui Fazer
          //Linha := Trim(Linha);
        end else Linha := '';
        //
{
procedure TGeral.SeparaLogradouro(const Linha: Integer; const Logr: String; var Sep1, Sep2,
  Sep3: String; Memo: TMemo; Grade: TStringGrid);
}

        Geral.SeparaLogradouro(0, Logr, Sep1, Sep2, Sep3, TMemo(MeLgr), Grade);

        // Parei aqui
        Grade.Cells[06,n] := Logr;
        Grade.Cells[07,n] := Trim(Sep1);
        Grade.Cells[08,n] := Trim(Sep2);
        Grade.Cells[09,n] := Trim(Sep3);
        Grade.Cells[10,n] := Geral.SoNumero_TT(Num);
        Grade.Cells[11,n] := Compl;
        Grade.Cells[12,n] := Bairro;
      end;

      // CEP, Cidade UF
      p1 := pos(TagCEP, Linha);
      if p1 > 0 then
      begin
        Achou := True;
        ini := 1 + p1 + Length(TagCEP);
        p2  := pos(TagCid, Linha);
        if p2 > 0 then
        begin
          CEP := Trim(Copy(Linha, Ini, p2 - Ini));
          Cid := Uppercase(Trim(Copy(Linha, p2 + Length(TagCid))));
          //
          UF  := '';
          //Tam := 0;
          if Length(Cid) > 0 then
          begin
            p1 := pos('-', Cid);
            if p1 > 0 then
            begin
              UF  := Trim(Copy(Cid, p1 + 1));
              Cid := Trim(Copy(Cid, 1, p1 -1));
            end;
          end;
        end else begin
          Cid := Trim(Copy(Linha, Ini));
          CEP := '';
          UF  := '';
        end;
        Grade.Cells[13,n] := Geral.SoNumero_TT(CEP);
        Grade.Cells[14,n] := Cid;
        Grade.Cells[15,n] := UF;
      end;

      // LOTE
      p1 := pos('LOTE:', Linha);
      if p1 > 0 then
      begin
        Achou := True;
        // nada
      end;

      // -----
      p1 := pos('-----', Linha);
      if p1 > 0 then
      begin
        Achou := True;
        // nada
      end;


      if not Achou then
        MeTitulo.Lines.Add(Linha);
    end;
    BtCadastra.Enabled := Grade.RowCount > 2;
    CkMaiusculas.Enabled := True;
    CkMaiusculas.Checked := False;
    Application.MessageBox(PChar('Arquivo carregado! Clique no bot�o ' +
    '"Cadastra" para cadastrar/alterar as entidades carregadas.'+ sLineBreak+
    'Antes de cadastrar � poss�vel editar as informa��es carregadas!'),
    'Mensagem', MB_OK+MB_ICONINFORMATION);
  finally
    Screen.Cursor := crDefault;
    Lista.Free;
  end;
end;

procedure TFmEntiLoad01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiLoad01.CkMaiusculasClick(Sender: TObject);
var
  i, j: Integer;
begin
  Screen.Cursor := crHourGlass;
  CkMaiusculas.Enabled := False;
  for i := 1 to Grade.ColCount do
    for j := 1 to Grade.RowCount do
      Grade.Cells[i,j] := Geral.Maiusculas(Grade.Cells[i,j], False);
  Screen.Cursor := crDefault;
end;

procedure TFmEntiLoad01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiLoad01.FormCreate(Sender: TObject);
begin
  Grade.Cells[00,00] := 'Seq';
  Grade.Cells[01,00] := 'Nome';
  Grade.Cells[02,00] := 'Telefone';
  Grade.Cells[03,00] := 'CPF';
  Grade.Cells[04,00] := 'RG';
  Grade.Cells[05,00] := 'SSP';
  Grade.Cells[06,00] := 'Logradouro';
  Grade.Cells[07,00] := 'Tipo';
  Grade.Cells[08,00] := 'C�d.';
  Grade.Cells[09,00] := 'Nome';
  Grade.Cells[10,00] := 'Num.';
  Grade.Cells[11,00] := 'Compl.';
  Grade.Cells[12,00] := 'Bairro';
  Grade.Cells[13,00] := 'CEP';
  Grade.Cells[14,00] := 'Cidade';
  Grade.Cells[15,00] := 'UF';
end;

procedure TFmEntiLoad01.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEntiLoad01.SpeedButton1Click(Sender: TObject);
var
  Arq: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'CdADM Administrador de Im�veis',
    '', [], Arq) then
  begin
    EdArq.Text := Arq;
    CkMaiusculas.Enabled := True;
    CkMaiusculas.Checked := False;
  end;
end;

end.
