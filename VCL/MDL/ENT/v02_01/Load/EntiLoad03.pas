unit EntiLoad03;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, mySQLDbTables, dmkEdit,
  ComCtrls, dmkMemo, DBGrids, UnDmkEnums;

type
  TFmEntiLoad03 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCadastra: TBitBtn;
    QrDupl: TmySQLQuery;
    QrDuplCodigo: TIntegerField;
    QrDuplCodUsu: TIntegerField;
    PB1: TProgressBar;
    Label2: TLabel;
    EdInser: TdmkEdit;
    Label3: TLabel;
    EdUpdat: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdNaoAtz: TdmkEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    EdArq: TEdit;
    TabSheet2: TTabSheet;
    Memo1: TMemo;
    QrEntiLoad03: TmySQLQuery;
    DsEntiLoad03: TDataSource;
    DBGrid1: TDBGrid;
    QrEntiLoad03Codigo: TIntegerField;
    QrEntiLoad03_A_: TWideStringField;
    QrEntiLoad03Nome: TWideStringField;
    QrEntiLoad03Fantasia: TWideStringField;
    QrEntiLoad03_B_: TWideStringField;
    QrEntiLoad03_C_: TWideStringField;
    QrEntiLoad03CNPJ: TWideStringField;
    QrEntiLoad03_D_: TWideStringField;
    QrEntiLoad03Endereco: TWideStringField;
    QrEntiLoad03Contato: TWideStringField;
    QrEntiLoad03ContaCtb: TWideStringField;
    QrEntiLoad03CPF: TWideStringField;
    QrEntiLoad03_E_: TWideStringField;
    QrEntiLoad03Bairro: TWideStringField;
    QrEntiLoad03Fax: TWideStringField;
    QrEntiLoad03Telex: TWideStringField;
    QrEntiLoad03IM: TWideStringField;
    QrEntiLoad03CEP: TWideStringField;
    QrEntiLoad03_F_: TWideStringField;
    QrEntiLoad03CidUF: TWideStringField;
    QrEntiLoad03Tel1: TWideStringField;
    QrEntiLoad03_G_: TWideStringField;
    QrEntiLoad03Tel2: TWideStringField;
    QrEntiLoad03_H_: TWideStringField;
    QrEntiLoad03_I_: TWideStringField;
    QrEntiLoad03_J_: TWideStringField;
    QrEntiLoad03Categor: TWideStringField;
    QrEntiLoad03Rua: TWideStringField;
    QrEntiLoad03Numero: TIntegerField;
    QrEntiLoad03Compl: TWideStringField;
    QrEntiLoad03nLogr: TIntegerField;
    QrEntiLoad03xLogr: TWideStringField;
    QrEntiLoad03Cidade: TWideStringField;
    QrEntiLoad03UF: TIntegerField;
    Panel5: TPanel;
    CkMaiusculas: TCheckBox;
    CkNaoAtz_: TCheckBox;
    Label6: TLabel;
    SpeedButton2: TSpeedButton;
    EdCondCod: TdmkEdit;
    EdCondNom: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCadastraClick(Sender: TObject);
    procedure CkMaiusculasClick(Sender: TObject);
    procedure QrEntiLoad03BeforeClose(DataSet: TDataSet);
    procedure QrEntiLoad03AfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FEntiLoad03: String;
    //
    function LeArquivo(Arq: String): Boolean;
  public
    { Public declarations }
  end;

  var
  FmEntiLoad03: TFmEntiLoad03;

implementation

uses UnMyObjects, dmkGeral, UMySQLModule, Module, ModuleGeral, UCreate;

{$R *.DFM}

procedure TFmEntiLoad03.BtCadastraClick(Sender: TObject);
  procedure InsereAtual(Cond, Bloco: Integer);
  var
    SQLType: TSQLType;
    EUF, PUF, Codigo, CodUsu, PNumero, ENumero, Tipo, PLograd, ELograd,
    PCEP, ECEP: Integer;
    RazaoSocial, Fantasia, CNPJ, IE,
    Nome, Apelido, CPF, RG, SSP,

    ERua,
    ECompl, EBairro, ECidade,
    EPais, ETe1, ETe2, ETe3, EFax,

    PRua,
    PCompl, PBairro, PCidade,
    PPais, PTe1, PTe2, PTe3, PFax, Antigo: String;
    //
    UH: String;
    Conta, SitImv: Integer;
    InsEnti: Boolean;
  begin
    if Cond <> Geral.IMV(QrEntiLoad03Categor.Value) then
    begin
      Geral.MensagemBox('A categoria '+ QrEntiLoad03Categor.Value +
      ' diverge do condom�nio '+IntToStr(Cond)+' !', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    CNPJ := Trim(Geral.SoNumero_TT(QrEntiLoad03CNPJ.Value));
    CPF  := Trim(Geral.SoNumero_TT(QrEntiLoad03CPF.Value));
    QrEntiLoad03CidUF.Value;
    Codigo        := QrEntiLoad03Codigo.Value;
    Antigo        := FormatFloat('0', Codigo);
    if CNPJ = '' then
    begin
      Tipo        := 1;
      Nome        := QrEntiLoad03Nome.Value;
      Apelido     := QrEntiLoad03Fantasia.Value;
      PTe1        := QrEntiLoad03Tel1.Value;
      PTe2        := QrEntiLoad03Tel2.Value;
      PTe3        := QrEntiLoad03Telex.Value;
      PFax        := QrEntiLoad03Fax.Value;
      RG          := '';
      PLograd     := QrEntiLoad03nLogr.Value;
      PRua        := QrEntiLoad03Rua.Value;
      PNumero     := QrEntiLoad03Numero.Value;
      PCompl      := QrEntiLoad03Compl.Value;
      PBairro     := QrEntiLoad03Bairro.Value;
      PCEP        := Geral.IMV(Geral.SoNumero_TT(QrEntiLoad03CEP.Value));
      PCidade     := QrEntiLoad03Cidade.Value;
      PUF         := QrEntiLoad03UF.Value;
      //
      CNPJ        := '';
      RazaoSocial := '';
      ETe1        := '';
      IE          := '';
      ELograd     := 0;
      ERua        := '';
      ENUmero     := 0;
      ECompl      := '';
      EBairro     := '';
      ECEP        := 0;
      ECidade     := '';
      EUF         := 0;
    end else begin
      Tipo        := 0;
      RazaoSocial := QrEntiLoad03Nome.Value;
      Fantasia    := QrEntiLoad03Fantasia.Value;
      ETe1        := QrEntiLoad03Tel1.Value;
      ETe2        := QrEntiLoad03Tel2.Value;
      ETe3        := QrEntiLoad03Telex.Value;
      EFax        := QrEntiLoad03Fax.Value;
      IE          := '';
      ELograd     := QrEntiLoad03nLogr.Value;
      ERua        := QrEntiLoad03Rua.Value;
      ENumero     := QrEntiLoad03Numero.Value;
      ECompl      := QrEntiLoad03Compl.Value;
      EBairro     := QrEntiLoad03Bairro.Value;
      ECEP        := Geral.IMV(Geral.SoNumero_TT(QrEntiLoad03CEP.Value));
      ECidade     := QrEntiLoad03Cidade.Value;
      EUF         := QrEntiLoad03UF.Value;
      //
      CPF         := '';
      Nome        := '';
      PTe1        := '';
      RG          := '';
      PLograd     := 0;
      PRua        := '';
      PNumero     := 0;
      PCompl      := '';
      PBairro     := '';
      PCEP        := 0;
      PCidade     := '';
      PUF         := 0;
    end;
    SSP   := '';
    //
    QrDupl.Close;
    QrDupl.SQL.Clear;
    QrDupl.SQL.Add('SELECT Codigo, CodUsu');
    QrDupl.SQL.Add('FROM entidades');
    QrDupl.SQL.Add('WHERE Codigo=' + FormatFloat('0', Codigo));
    QrDupl.Open;
    if (QrDupl.RecordCount > 0) (*and (CkNaoAtz.Checked = True)*) then
    begin
      PB1.Position := PB1.Position + 1;
      EdNaoAtz.ValueVariant := EdNaoAtz.ValueVariant + 1;
      InsEnti := False;
    end else InsEnti := True;

    (*
    if QrDupl.RecordCount > 0 then
      SQLType := stUpd
    else begin
      SQLType := stIns;
    end;
    if SQLType = stIns then
    begin
      Codigo := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', SQLType, 0);
      CodUsu := Codigo;
    end else begin
      Codigo := QrDuplCodigo.Value;
      CodUsu := QrDuplCodUsu.Value;
    end;
    *)
    if InsEnti then
    begin
      CodUsu := Codigo;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entidades', False, [
      'RazaoSocial', 'Fantasia', 'CNPJ', 'IE',
      'Nome', 'Apelido', 'CPF', 'RG', 'SSP',

      'ELograd', 'ERua', 'ENumero',
      'ECompl', 'EBairro', 'ECidade',
      'EUF', 'ECEP', 'EPais',

      'PLograd', 'PRua', 'PNumero',
      'PCompl', 'PBairro', 'PCidade',
      'PUF', 'PCEP', 'PPais',

      'ETe1', 'PTe1', 'Cliente2',

      'Tipo', 'CodUsu', 'Antigo'], ['Codigo'], [
      RazaoSocial, Fantasia, CNPJ, IE,
      Nome, Apelido, CPF, RG, SSP,

      ELograd, ERua, ENumero,
      ECompl, EBairro, ECidade,
      EUF, ECEP, EPais,

      PLograd, PRua, PNumero,
      PCompl, PBairro, PCidade,
      PUF, PCEP, PPais,

      ETe1, PTe1, 'V',

      Tipo, CodUsu, Antigo], [Codigo], True) then
      begin
        PB1.Position := PB1.Position + 1;
        if SQLType = stIns then
          EdInser.ValueVariant := EdInser.ValueVariant + 1
        else
          EdUpdat.ValueVariant := EdUpdat.ValueVariant + 1;
      end;
    end;
    //
    // Cadastro do im�vel
    //
    UH := Geral.SoNumero_TT(QrEntiLoad03Fantasia.Value);
    //
    QrDupl.Close;
    QrDupl.SQL.Clear;
    QrDupl.SQL.Add('SELECT Codigo, Conta CodUsu');
    QrDupl.SQL.Add('FROM condimov');
    QrDupl.SQL.Add('WHERE Codigo=' + FormatFloat('0', Cond));
    QrDupl.SQL.Add('AND Controle=' + FormatFloat('0', Bloco));
    QrDupl.SQL.Add('AND Unidade="' + UH + '"');
    QrDupl.Open;
    if QrDupl.RecordCount = 0 then
    begin
      Conta := UMyMod.BuscaEmLivreY_Def('condimov', 'Conta', stIns, 0);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'condimov', False, [
      'Codigo', 'Controle', 'Propriet',
      'Usuario', 'Andar', 'Unidade',
      'Status', 'SitImv'], [
      'Conta'], [
      Cond, Bloco, Codigo,
      0, 0, UH,
      1, 1], [
      Conta], True);
    end;
    //
  end;
var
  Codigo, Bloco: Integer;
begin
  Codigo := EdCondCod.ValueVariant;
  //
  if MyObjects.FIC(Codigo=0, EdCondCod, 'Informe o c�digo do condom�nio') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    //
    QrDupl.Close;
    QrDupl.SQL.Clear;
    QrDupl.SQL.Add('SELECT Codigo, CodUsu');
    QrDupl.SQL.Add('FROM entidades');
    QrDupl.SQL.Add('WHERE Codigo=' + FormatFloat('0', Codigo));
    QrDupl.Open;
    //
    if QrDupl.RecordCount = 0 then
    begin
      DMod.QrUpd.SQL.Clear;
      DMod.QrUpd.SQL.Add('INSERT INTO entidades SET Tipo=0, Cliente1="V", ');
      DMod.QrUpd.SQL.Add('Codigo=:P0, CodUsu=:P1, CliInt=:P2, RazaoSocial=:P3');
      DMod.QrUpd.Params[00].AsInteger := Codigo;
      DMod.QrUpd.Params[01].AsInteger := Codigo;
      DMod.QrUpd.Params[02].AsInteger := Codigo;
      DMod.QrUpd.Params[03].AsString  := EdCondNom.Text;
      DMod.QrUpd.ExecSQL;
    end;
    //
    QrDupl.Close;
    QrDupl.SQL.Clear;
    QrDupl.SQL.Add('SELECT Codigo, Cliente CodUsu');
    QrDupl.SQL.Add('FROM cond');
    QrDupl.SQL.Add('WHERE Codigo=' + FormatFloat('0', Codigo));
    QrDupl.Open;
    if QrDupl.RecordCount = 0 then
    begin
      DMod.QrUpd.SQL.Clear;
      DMod.QrUpd.SQL.Add('INSERT INTO cond SET ');
      DMod.QrUpd.SQL.Add('Codigo=:P0, Cliente=:P1');
      DMod.QrUpd.Params[00].AsInteger := Codigo;
      DMod.QrUpd.Params[01].AsInteger := Codigo;
      DMod.QrUpd.ExecSQL;
    end;
    //
    QrDupl.Close;
    QrDupl.SQL.Clear;
    QrDupl.SQL.Add('SELECT Codigo, Controle CodUsu');
    QrDupl.SQL.Add('FROM condbloco');
    QrDupl.SQL.Add('WHERE Codigo=' + FormatFloat('0', Codigo));
    QrDupl.Open;
    if QrDupl.RecordCount = 0 then
    begin
      Bloco := UMyMod.BuscaEmLivreY_Def('condbloco', 'Controle', stIns, 0);
      DMod.QrUpd.SQL.Clear;
      DMod.QrUpd.SQL.Add('INSERT INTO condbloco SET Nome="UNICO",');
      DMod.QrUpd.SQL.Add('Codigo=:P0, Controle=:P1');
      DMod.QrUpd.Params[00].AsInteger := Codigo;
      DMod.QrUpd.Params[01].AsInteger := Bloco;
      DMod.QrUpd.ExecSQL;
    end else Bloco := QrDuplCodUsu.Value;
    //
    PB1.Position := 0;
    PB1.Max      := QrEntiLoad03.RecordCount;
    EdInser.ValueVariant := 0;
    EdUpdat.ValueVariant := 0;
    QrEntiLoad03.First;
    while not QrEntiLoad03.Eof do
    begin
      InsereAtual(Codigo, Bloco);
      QrEntiLoad03.Next;
    end;
    Application.MessageBox('Cadastramento concluido!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEntiLoad03.BtOKClick(Sender: TObject);
{
var
  Lista: TStringList;
  i, n: Integer;
  Linha, Sep1, Sep2, Sep3: String;
  //
  pCod, pCNP, pNom, pLgr, pNum, pUF_, pIE_, pMun: Integer;
  vCod, vCNP, vNom, vLgr, vNum, vUF_, vIE_, vMun: String;
}
  function replace(S: string; const Find: string; const Replace: string): string;
    function Head(S: string; const Subs: string; var Tail: string): string;
    var
      I: Integer;
    begin
      I:= ansipos(Subs, S);
      if  I = 0 then
      begin
        Result := S;
        Tail := '';
      end
      else
      begin
        Tail := S;
        delete(Tail, 1, I + length(Subs) - 1);
        delete(S, I, length(S));
        Result := S;
      end;
    end;
  begin
    Result := '';
    while ansipos(Find, S) > 0 do
    begin
      Result := Result + Head(S, Find, S) + Replace
    end;
    Result := Result + S;
  end;
var
  Txt: String;
  I, K, N: Integer;
  //
  Numero, Compl, Bairro, xLogr, nLogr, xRua, Cidade, xUF: String;
  cUF: Integer;
begin
    if LeArquivo(EdArq.Text) then
    begin
      FEntiLoad03 := UCriar.RecriaTempTable('entiload03', DModG.QrUpdPID1, False);
      K := Memo1.Lines.Count div 5;
      for I := 1 to K do
      begin
        N := (I-1) * 5;
        Txt := Memo1.Lines[N] + ',' + Memo1.Lines[N+1] + ',' + Memo1.Lines[N+2] + ',' + Memo1.Lines[N+3];
        //Geral.MensagemBox(Txt, 'Cadastro ' + IntToStr(I), MB_OK+MB_ICONINFORMATION);
        //Txt := Replace(Txt, '"', #39);
        Txt := Replace(Txt, #13, '');
        Txt := Replace(Txt, #10, '''');
        Txt := Txt + ',0,"","",0,"","",0';
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add('INSERT INTO `'+FEntiLoad03+'` VALUES ('+Txt+')');
        UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
      end;
      QrEntiLoad03.Close;
      QrEntiLoad03.Open;
      QrEntiLoad03.First;
      while not QrEntiLoad03.Eof do
      begin
        //
        Geral.SeparaEndereco(0, QrEntiLoad03Endereco.Value, Numero, Compl, Bairro,
          xLogr, nLogr, xRua, False, nil); // n�o tem bairro!
        //
        Compl := Trim(Compl)(* + ' ' + Bairro*);
        xRua  := Trim(xRua);
        //

        Cidade := Trim(QrEntiLoad03CidUF.Value);
        K := Length(Cidade);
        if K > 1  then
        begin
          xUF    := Cidade[K-1]+Cidade[K];
          cUF    := Geral.GetCodigoUF_da_SiglaUF(xUF);
          if cUF <> 0 then
            Cidade := Trim(Copy(Cidade, 1, K-2));
        end else begin
          xUF := '';
          cUF := 0;
        end;
        //
        UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stupd, 'entiload03', False, [
        'nLogr', 'xLogr', 'Rua', 'Numero', 'Compl', 'Cidade', 'UF'], ['Codigo'
        ], [nLogr, xlogr, xRua, Numero, Compl, Cidade, cUF
        ], [QrEntiLoad03Codigo.Value
        ], False);
        //
        QrEntiLoad03.Next;
      end;
      QrEntiLoad03.Close;
      QrEntiLoad03.Open;
    end;
{
  MeLgr.Lines.Clear;
  MLAGeral.LimpaGrade(Grade, 1, 1, True);
  MeTitulo.Lines.Clear;
  if not FileExists(EdArq.Text) then
  begin
    Application.MessageBox(PChar('Arquivo n�o localizado: ' + EdArq.Text),
      'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  //
  Lista := TStringList.Create;
  try
    Lista.LoadFromFile(EdArq.Text);
    n := 0;
    if pos('RELAT�RIO DE TERCEIROS', Lista[0]) = 0 then
    begin
      Geral.MensagemBox('Arquivo n�o � de entidades ProSoft. o t�tulo "' +
      'RELAT�RIO DE TERCEIROS" n�o foi encontrado na primeira linha!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
    pCod := pos('C�digo', Lista[3]);
    pCNP := pos('CNPJ/CPF/Livre', Lista[3]);
    pNom := pos('Raz�o Social/Nome', Lista[3]);

    pUF_ := pos('UF', Lista[5]);
    pLgr := pos('Logradouro', Lista[5]);
    pNum := pos('N�mer', Lista[5]);
    pIE_ := pos('Inscri��o', Lista[5]);
    pMun := pos('Munici', Lista[5]);

    if (pCod = 0) or (pCNP = 0) or (pNom = 0) or (pLgr = 0) or (pNum = 0)
    or (pUF_ = 0) or (pIE_ = 0) or (pMun = 0) then
    begin
      Geral.MensagemBox('Arquivo mal formatado. !', 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;

    i := 4;
    while i < Lista.Count -1 do
    //for i := 0 to Lista.Count -1 do
    begin
      n := n + 1;
      Grade.RowCount := n + 1;
      //
      Linha := Lista[i];
      vCod := Copy(Linha, pCod, pCNP - pCod);
      vCNP := Copy(Linha, pCNP, pNom - pCNP);
      vNom := Copy(Linha, pNom);

      Linha := Lista[i + 2];
      vUF_ := Copy(Linha, pUF_, pLgr - pUF_);
      vLgr := Copy(Linha, pLgr, pNum - pLgr);
      vNum := Copy(Linha, pNum, pIE_ - pNum);
      vIE_ := Copy(Linha, pIE_, pMun - pIE_);
      vMun := Copy(Linha, pMun);

      Geral.SeparaLogradouro(vLgr, Sep1, Sep2, Sep3, TMemo(MeLgr));

      Grade.Cells[00,n] := IntToStr(n);
      Grade.Cells[01,n] := vNom;
      Grade.Cells[02,n] := '';//Tel;
      Grade.Cells[03,n] := vCNP;
      Grade.Cells[04,n] := '';//RG2;
      Grade.Cells[05,n] := '';//SSP;
      Grade.Cells[06,n] := vLgr;
      Grade.Cells[07,n] := Trim(Sep1);
      Grade.Cells[08,n] := Trim(Sep2);
      Grade.Cells[09,n] := Trim(Sep3);
      Grade.Cells[10,n] := Geral.SoNumero_TT(vNum);
      Grade.Cells[11,n] := '';//Compl;
      Grade.Cells[12,n] := '';//Bairro;
      Grade.Cells[13,n] := '';//Geral.SoNumero_TT(CEP);
      Grade.Cells[14,n] := vMun;
      Grade.Cells[15,n] := vUF_;
      Grade.Cells[16,n] := vCod;
      //
      i := i + 4;
      (*

      // Nome, Tel
      p1 := pos(TagNome, Linha);
      if p1 > 0 then
      begin
        Achou := True;
        ini := 1 + p1 + Length(TagNome);
        n := n + 1;
        Grade.RowCount := n + 1;
        Grade.Cells[00,n] := IntToStr(n);
        //
        p2 := pos(TagTel, Linha);
        if p2 > 0 then
        begin
          Nome := Trim(Copy(Linha, Ini, p2 - Ini));
          Tel := Geral.SoNumero_TT(Trim(Copy(Linha, p2 + Length(TagTel))));
          while Length(Tel) > 10 do
            Tel := Copy(Tel, 2);
        end else begin
          Nome := Trim(Copy(Linha, Ini));
          Tel  := '';
        end;
        Grade.Cells[01,n] := Nome;
        Grade.Cells[02,n] := Tel;
      end;

      // CPF, RG e SSP
      TagX := tagDc1;
      p1 := pos(tagX, Linha);
      if p1 = 0 then
      begin
        TagX := tagDc0;
        p1 := pos(TagX, Linha);
      end;
      if p1 > 0 then
      begin
        Achou := True;
        ini := 1 + p1 + Length(TagX);
        p2  := pos(TagRG, Linha);
        if p2 > 0 then
        begin
          CPF := Trim(Copy(Linha, Ini, p2 - Ini));
          RG  := Uppercase(Trim(Copy(Linha, p2 + Length(TagRG))));
          //
          SSP := '';
          //Tam := 0;
          if Length(RG) > 0 then
          begin
            for Fim := Length(RG) downto 1 do
            begin
              if RG[Fim] in (['A'..'Z']) then
              begin
                SSP := RG[Fim] + SSP;
              end else
              if RG[Fim] in (['0'..'9']) then
                Break;
            end;
            RG2 := Copy(RG, 1, Length(RG) - Length(SSP));
            while (Length(RG2) > 0) and (not (RG2[Length(RG2)] in (['0'..'9']))) do
              RG2 := Copy(RG2, 1, Length(RG2) - 1);
          end else RG2 := RG;
        end else begin
          CPF := Trim(Copy(Linha, Ini));
          RG  := '';
          RG2 := '';
          SSP := '';
        end;
        Grade.Cells[03,n] := CPF;
        Grade.Cells[04,n] := RG2;
        Grade.Cells[05,n] := SSP;
      end;

      // Endere�o:
      Logr := '';
      Num := '';

      p1 := pos(TagEnd, Linha);
      if p1 > 0 then
      begin
        Achou := True;
        ini := 1 + p1 + Length(TagEnd);
        p2  := pos(',', Linha);
        if p2 > 0 then
        begin
          Logr := Trim(Copy(Linha, Ini, p2 - Ini));
          Linha := Trim(Copy(Linha, p2+1));

          // Numero
          if Length(Linha) > 0 then
          begin
            X := Linha[1];
            if X in (['0'..'9']) then
            begin
              Num := '';
              while X in (['0'..'9','.']) do
              begin
                Num := Num + X;
                Linha := Copy(Linha, 2);
                if Length(Linha) > 0 then
                  X := Linha[1]
                else X := ' ';
              end;
            end else Num := '';
          end;

          // Limpar texto apartamento/bairro
          if Length(Linha) > 0 then
          begin
            X := Linha[1];
            if not (X in (['A'..'Z'])) then
            begin
              while (not (X in (['A'..'Z']))) and (Length(Linha) > 0) do
              begin
                Linha := Copy(Linha, 2);
                if Length(Linha) > 0 then
                  X := Linha[1]
                else X := 'A';
              end;
            end;
          end;

          // Apto = Bairro
          Compl := '';
          Bairro := '';
          p1 := pos(#150, Linha);
          if p1 = 0 then p1 := pos('-', Linha);
          if p1 > 0 then
          begin
            Compl  := Trim(Copy(Linha, 1, p1 - 1));
            Bairro := Trim(Copy(Linha, p1 + 1));
            //ShowMessage(Compl + ' # ' + Bairro);
          end else begin
            Txt := Geral.Maiusculas(Linha, False);
            if pos('APTO', Txt) > 0 then
              Compl := Linha
            else
            if pos('AP.', Txt) > 0 then
              Compl := Linha
            else
            if pos('APT.', Txt) > 0 then
              Compl := Linha
            else
            if pos('SALA', Txt) > 0 then
              Compl := Linha
            else
            if pos('LOJA', Txt) > 0 then
              Compl := Linha
            else
            if pos('CASA', Txt) > 0 then
              Compl := Linha
            else
            if pos('COMERCIO', Txt) > 0 then
              Compl := Linha
            else
            if pos('ZONA', Txt) > 0 then
              Bairro := Linha
            else
            if pos('BAIRRO', Txt) > 0 then
              Bairro := Linha
            else
            if pos('JD', Txt) > 0 then
              Bairro := Linha
            else
            if pos('CJ', Txt) > 0 then
              Bairro := Linha
            else
            if pos('PQ', Txt) > 0 then
              Bairro := Linha
            else
            if pos('V.', Txt) > 0 then
              Bairro := Linha
            else
            if pos('VILA', Txt) > 0 then
              Bairro := Linha
            else
            if pos('CENTRO', Txt) > 0 then
              Bairro := Linha
            else
            if pos('CH�CARA', Txt) > 0 then
              Bairro := Linha
            else
            if pos('INDUS', Txt) > 0 then
              Bairro := Linha
            else
            if pos('RES', Txt) > 0 then
              Bairro := Linha

            //else
            //if pos('CJ', Txt) then
            //  Compl := Linha
            else begin
              if Trim(Linha) <> '' then
                if Application.MessageBox(PChar('"' + Linha + '" � um bairro?'),
                'Pergunta', MB_YESNO+MB_ICONQUESTION) = ID_YES then
                  Bairro := Linha
                else
                  Compl := Linha;
            end;
          end;
          // Parei aqui Fazer
          //Linha := Trim(Linha);
        end else Linha := '';
        //
        Geral.SeparaLogradouro(Logr, Sep1, Sep2, Sep3);

        // Parei aqui
        Grade.Cells[06,n] := Logr;
        Grade.Cells[07,n] := Trim(Sep1);
        Grade.Cells[08,n] := Trim(Sep2);
        Grade.Cells[09,n] := Trim(Sep3);
        Grade.Cells[10,n] := Geral.SoNumero_TT(Num);
        Grade.Cells[11,n] := Compl;
        Grade.Cells[12,n] := Bairro;
      end;

      // CEP, Cidade UF
      p1 := pos(TagCEP, Linha);
      if p1 > 0 then
      begin
        Achou := True;
        ini := 1 + p1 + Length(TagCEP);
        p2  := pos(TagCid, Linha);
        if p2 > 0 then
        begin
          CEP := Trim(Copy(Linha, Ini, p2 - Ini));
          Cid := Uppercase(Trim(Copy(Linha, p2 + Length(TagCid))));
          //
          UF  := '';
          //Tam := 0;
          if Length(Cid) > 0 then
          begin
            p1 := pos('-', Cid);
            if p1 > 0 then
            begin
              UF  := Trim(Copy(Cid, p1 + 1));
              Cid := Trim(Copy(Cid, 1, p1 -1));
            end;
          end;
        end else begin
          Cid := Trim(Copy(Linha, Ini));
          CEP := '';
          UF  := '';
        end;
        Grade.Cells[13,n] := Geral.SoNumero_TT(CEP);
        Grade.Cells[14,n] := Cid;
        Grade.Cells[15,n] := UF;
      end;

      // LOTE
      p1 := pos('LOTE:', Linha);
      if p1 > 0 then
      begin
        Achou := True;
        // nada
      end;

      // -----
      p1 := pos('-----', Linha);
      if p1 > 0 then
      begin
        Achou := True;
        // nada
      end;


      if not Achou then
        MeTitulo.Lines.Add(Linha);
      *)
    end;
    BtCadastra.Enabled := Grade.RowCount > 2;
    CkMaiusculas.Enabled := True;
    CkMaiusculas.Checked := False;
    Application.MessageBox(PChar('Arquivo carregado! Clique no bot�o ' +
    '"Cadastra" para cadastrar/alterar as entidades carregadas.'+ sLineBreak+
    'Antes de cadastrar � poss�vel editar as informa��es carregadas!'),
    'Mensagem', MB_OK+MB_ICONINFORMATION);
  finally
    Screen.Cursor := crDefault;
    Lista.Free;
  end;
}
end;

procedure TFmEntiLoad03.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiLoad03.CkMaiusculasClick(Sender: TObject);
{
var
  i, j: Integer;
}
begin
{
  Screen.Cursor := crHourGlass;
  CkMaiusculas.Enabled := False;
  for i := 1 to Grade.ColCount do
    for j := 1 to Grade.RowCount do
      Grade.Cells[i,j] := Geral.Maiusculas(Grade.Cells[i,j], False);
  Screen.Cursor := crDefault;
}
end;

procedure TFmEntiLoad03.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiLoad03.FormCreate(Sender: TObject);
begin
{
  Grade.Cells[00,00] := 'Seq';
  Grade.Cells[01,00] := 'Nome';
  Grade.Cells[02,00] := 'Telefone';
  Grade.Cells[03,00] := 'CPF';
  Grade.Cells[04,00] := 'RG';
  Grade.Cells[05,00] := 'SSP';
  Grade.Cells[06,00] := 'Logradouro';
  Grade.Cells[07,00] := 'Tipo';
  Grade.Cells[08,00] := 'C�d.';
  Grade.Cells[09,00] := 'Nome';
  Grade.Cells[10,00] := 'Num.';
  Grade.Cells[11,00] := 'Compl.';
  Grade.Cells[12,00] := 'Bairro';
  Grade.Cells[13,00] := 'CEP';
  Grade.Cells[14,00] := 'Cidade';
  Grade.Cells[15,00] := 'UF';
  Grade.Cells[16,00] := 'C�digo';
}
  QrEntiLoad03.Database := DModG.MyPID_DB;
end;

procedure TFmEntiLoad03.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmEntiLoad03.LeArquivo(Arq: String): Boolean;
var
  F: TextFile;
  S: String;
  //
  EhArq: Byte;
begin
  Memo1.Lines.Clear;
  EhArq := 0;
  if FileExists(Arq) then
  begin
    AssignFile(F, Arq);
    Reset(F);
    while not Eof(F) do
    begin
      Readln(F, S);
      //
      if pos('"M�ximo - Sistema Financeiro"', S) > 0 then
      begin
        if EhArq = 0 then
          EhArq := 1;
      end else
      if pos('"RELA��O DE CLIENTES EM ORDEM DE NOME FANTASIA"', S) > 0 then
      begin
        if EhArq < 2 then
          EhArq := 2;
      end else
      if S = '' then
      begin
        // nada
      end else
      if S = '""' then
      begin
        // nada
      end else
      if pos('"C�digo"', S) > 0 then
      begin
        // nada
      end else
      if pos('"Endere�o"', S) > 0 then
      begin
        // nada
      end else
      if pos('"Inscri��o Municipal"', S) > 0 then
      begin
        // nada
      end else
      if pos('"Banco/Ag�ncia"', S) > 0 then
      begin
        // nada
      end else
      begin
        Memo1.Lines.Add(S);
      end;
    end;
    CloseFile(F);
  end;
  if EhArq <> 2 then Geral.MensagemBox(
  'O arquivo selecionado n�o � do tipo esperado!', 'Aviso', MB_OK+MB_ICONWARNING);
  Result := EhArq = 2;
end;

procedure TFmEntiLoad03.QrEntiLoad03AfterOpen(DataSet: TDataSet);
begin
  BtCadastra.Enabled := QrEntiLoad03.RecordCount > 0;
end;

procedure TFmEntiLoad03.QrEntiLoad03BeforeClose(DataSet: TDataSet);
begin
  BtCadastra.Enabled := False;
end;

procedure TFmEntiLoad03.SpeedButton1Click(Sender: TObject);
var
  Cod, Nom, Arq: String;
  N, I: Integer;
  Continua: Boolean;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'M�XIMO - RELA��O DE CLIENTES EM ORDEM DE NOME FANTASIA',
    '', [], Arq) then
  begin
    EdArq.Text := Arq;
    CkMaiusculas.Enabled := True;
    CkMaiusculas.Checked := False;
    //
    QrEntiLoad03.Close;
    //
    Cod := '';
    Nom := '';
    Continua := True;
    Arq := ExtractFileName(Arq);
    N := Length(Arq);
    I := 0;
    while Continua = True do
    begin
      I := I + 1;
      if I <= N then
      begin
        if Arq[I] in (['0'..'9']) then
          Cod  := Cod + Arq[I]
        else
          Continua := False;
      end else Continua := False;
    end;
    EdCondCod.ValueVariant := Geral.IMV(Cod);
    Nom := Trim(Copy(Arq, Length(Cod)+1));
    N := pos('.', Nom);
    if N > 0 then 
      Nom := Copy(Nom, 1, N-1);
    EdCondNom.Text := Nom;
  end;
end;

end.
