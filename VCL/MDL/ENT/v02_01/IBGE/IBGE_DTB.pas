unit IBGE_DTB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComObj, Variants, Grids, ComCtrls, DB, ABSMain,
  DBGrids, mySQLDbTables, (*DBTables,*) dmkGeral, dmkDBGrid, dmkImage, UnDmkEnums,
  UnGrl_Consts;

type
  TFmIBGE_DTB = class(TForm)
    Panel1: TPanel;
    EdArq: TEdit;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    Query: TABSQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Grade1: TStringGrid;
    TabSheet2: TTabSheet;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    Panel4: TPanel;
    BitBtn2: TBitBtn;
    QrDTB1: TmySQLQuery;
    BtCompara: TBitBtn;
    Label2: TLabel;
    QrDTB1SIGLA: TWideStringField;
    QrDTB1MUNICI: TWideStringField;
    QrDTB1DISTRI: TWideStringField;
    QrDNE: TQuery;
    DNCEP: TDatabase;
    QrDNELOC_NU_SEQUENCIAL: TIntegerField;
    QrDNELOC_NOSUB: TWideStringField;
    QrDNELOC_NO: TWideStringField;
    QrDNEUFE_SG: TWideStringField;
    DsDTB2: TDataSource;
    QrDNEMunici: TWideStringField;
    QrDTB2: TmySQLQuery;
    QrDTB1COD_DIS: TIntegerField;
    QrDTB1COD_MUN: TIntegerField;
    QrDNELOC_NU_SEQUENCIAL_SUB: TIntegerField;
    QrDTB2SIGLA: TWideStringField;
    QrDTB2MUNICI: TWideStringField;
    QrDTB2COD_MUN: TIntegerField;
    QrDNELOC_IN_TIPO_LOCALIDADE: TWideStringField;
    BitBtn3: TBitBtn;
    QrMunici: TmySQLQuery;
    QrMuniciSigla: TWideStringField;
    QrMuniciCodigo: TIntegerField;
    QrMuniciNome: TWideStringField;
    QrDTB2NomeSCE: TWideStringField;
    TabSheet3: TTabSheet;
    Panel5: TPanel;
    mySQLQuery1: TmySQLQuery;
    DataSource2: TDataSource;
    BtOK: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    Grade2: TStringGrid;
    TabSheet4: TTabSheet;
    Panel6: TPanel;
    QrDTB_UFs: TmySQLQuery;
    DsDTB_UFs: TDataSource;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    Grade3: TStringGrid;
    TabSheet5: TTabSheet;
    DBGrid2: TDBGrid;
    DsDNE: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn8: TBitBtn;
    Panel8: TPanel;
    PB1: TProgressBar;
    TabSheet6: TTabSheet;
    Memo1: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtComparaClick(Sender: TObject);
    procedure QrDNECalcFields(DataSet: TDataSet);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmIBGE_DTB: TFmIBGE_DTB;

implementation

uses UnMyObjects, ModuleGeral, VerifiDB, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmIBGE_DTB.BitBtn1Click(Sender: TObject);
var
  UF_C, i: Integer;
  Codi_UF, Codi_Meso, Codi_Micro, Codi_Munici, Codi_Distri, Codi_SubDis,
  Nome_UF, Nome_Meso, Nome_Micro, Nome_Munici, Nome_Distri, Nome_SubDis,
  CodU_Munici, UF_T, SQL: String;
begin
  Screen.Cursor := crHourGlass;
  //
  DModG.AllID_DB.Execute(DELETE_FROM + ' dtb_ufs');
  DModG.AllID_DB.Execute(DELETE_FROM + ' dtb_meso');
  DModG.AllID_DB.Execute(DELETE_FROM + ' dtb_micro');
  DModG.AllID_DB.Execute(DELETE_FROM + ' dtb_munici');
  DModG.AllID_DB.Execute(DELETE_FROM + ' dtb_distri');
  DModG.AllID_DB.Execute(DELETE_FROM + ' dtb_subdis');
  //
  PB1.Position := 0;
  PB1.Max := Grade1.RowCount - 1;
  for i := 1 to Grade1.RowCount - 1 do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;
    // UFs
    Codi_UF     := Trim(Grade1.Cells[01, i]);
    Nome_UF     := Trim(Grade1.Cells[02, i]);
    UF_C := Geral.IMV(Codi_UF);
    UF_T := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(UF_C);
    if UF_C > 0 then
    begin
      // Mesorregi�o
      Codi_Meso   := Trim(Grade1.Cells[03, i]);
      Nome_Meso   := Geral.Substitui(Trim(Grade1.Cells[04, i]), '"', '''');
      // Microrregi�o
      Codi_Micro  := Trim(Grade1.Cells[05, i]);
      Nome_Micro  := Geral.Substitui(Trim(Grade1.Cells[06, i]), '"', '''');
      // Munic�pio
      CodU_Munici := Trim(Grade1.Cells[07, i]);
      Nome_Munici := Geral.Substitui(Trim(Grade1.Cells[08, i]), '"', '''');
      Codi_Munici := Trim(Grade1.Cells[01, i]) + Trim(Grade1.Cells[07, i]);
      // Distrito
      Codi_Distri := Trim(Grade1.Cells[09, i]);
      Nome_Distri := Geral.Substitui(Trim(Grade1.Cells[10, i]), '"', '''');
      // Sub-distrito
      Codi_SubDis := Trim(Grade1.Cells[11, i]);
      Nome_SubDis := Geral.Substitui(Trim(Grade1.Cells[12, i]), '"', '''');
      //
      //
      SQL := 'INSERT INTO dtb_ufs (Codigo, Nome, Sigla) VALUES (' +
      Codi_UF + ',"' + Nome_UF + '","'+ UF_T + '") ' +
      'ON DUPLICATE KEY UPDATE itens=itens+1;';
      try
        DModG.AllID_DB.Execute(SQL);
      except
        ShowMessage(SQL);
      end;
      //
      SQL := 'INSERT INTO dtb_meso (Codigo, Nome, DTB_UF) VALUES (' +
      Codi_Meso + ',"' + Nome_Meso + '",'+ Codi_UF + ') ' +
      'ON DUPLICATE KEY UPDATE itens=itens+1;';
      try
        DModG.AllID_DB.Execute(SQL);
      except
        ShowMessage(SQL);
      end;
      //
      SQL := 'INSERT INTO dtb_micro (Codigo, Nome, DTB_UF, DTB_Meso) VALUES (' +
      Codi_Micro + ',"' + Nome_Micro + '",'+ Codi_UF + ',' + Codi_Meso +') ' +
      'ON DUPLICATE KEY UPDATE itens=itens+1;';
      try
        DModG.AllID_DB.Execute(SQL);
      except
        ShowMessage(SQL);
      end;
      //
      SQL := 'INSERT INTO dtb_munici (Codigo, CodUsu, Nome, DTB_UF, DTB_Meso, DTB_Micro) VALUES (' +
      Codi_Munici + ',' + CodU_Munici + ',"' + Nome_Munici + '",'+ Codi_UF + ',' + Codi_Meso+ ',' + Codi_Micro +') ' +
      'ON DUPLICATE KEY UPDATE itens=itens+1;';
      try
        DModG.AllID_DB.Execute(SQL);
      except
        ShowMessage(SQL);
      end;
      //
      if Codi_Distri <> '' then
      begin
        SQL := 'INSERT INTO dtb_distri (Codigo, Nome, DTB_UF, DTB_Meso, DTB_Micro, DTB_Munici) VALUES (' +
        Codi_Distri + ',"' + Nome_Distri + '",'+ Codi_UF + ',' + Codi_Meso + ',' + Codi_Micro  + ',' + Codi_Munici +') ' +
        'ON DUPLICATE KEY UPDATE itens=itens+1;';
        try
          DModG.AllID_DB.Execute(SQL);
        except
          ShowMessage(SQL);
        end;
        //
        if Codi_SubDis <> '' then
        begin
          SQL := 'INSERT INTO dtb_subdis (Codigo, Nome, DTB_UF, DTB_Meso, DTB_Micro, DTB_Munici, DTB_Distri) VALUES (' +
          Codi_SubDis + ',"' + Nome_SubDis + '",'+ Codi_UF + ',' + Codi_Meso + ',' + Codi_Micro  + ',' + Codi_Munici + ',' + Codi_Distri +') ' +
          'ON DUPLICATE KEY UPDATE itens=itens+1;';
          try
            DModG.AllID_DB.Execute(SQL);
          except
            ShowMessage(SQL);
          end;
        end;
      end;
    end;
  end;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmIBGE_DTB.BtComparaClick(Sender: TObject);
var
  NomeSCE: String;
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  Memo1.Text := '';
  PB1.Position := 0;
  DModG.AllID_DB.Execute(DELETE_FROM + ' dissemdtb');
  DModG.AllID_DB.Execute(DELETE_FROM + ' munsemdtb');
  DModG.AllID_DB.Execute('UPDATE dtb_distri SET DNE=0');
  DModG.AllID_DB.Execute('UPDATE dtb_munici SET DNE=0, ObtencaoDNE=0');
  //
  //Exit;
  //
  UnDmkDAC_PF.AbreQuery(QrDTB1, DModG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrDTB2, DModG.AllID_DB);
  UMyMod.AbreBDEQuery(QrDNE);
  PB1.Max := QrDNE.RecordCount;
  QrDNE.First;
  while not QrDNE.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;

    //  CIDADE

    if QrDNELOC_IN_TIPO_LOCALIDADE.Value = 'M' then
    begin
      if QrDTB2.Locate('SIGLA;MUNICI', VarArrayOf([
      QrDNEUFE_SG.Value, QrDNEMunici.Value]),[]) then
        DModG.AllID_DB.Execute('UPDATE dtb_munici SET ObtencaoDNE=1, DNE=' +
        FormatFloat('0', QrDNELOC_NU_SEQUENCIAL.Value) + ' WHERE Codigo=' +
        FormatFloat('0', QrDTB2COD_MUN.Value))
      else
      begin
        NomeSCE := Geral.SemAcento(QrDNEMunici.Value);
        NomeSCE := Geral.Maiusculas(NomeSCE, False);
        //
        if QrDTB2.Locate('SIGLA;NomeSCE', VarArrayOf([
        QrDNEUFE_SG.Value, NomeSCE]),[]) then
          DModG.AllID_DB.Execute('UPDATE dtb_munici SET ObtencaoDNE=2, DNE=' +
          FormatFloat('0', QrDNELOC_NU_SEQUENCIAL.Value) + ' WHERE Codigo=' +
          FormatFloat('0', QrDTB2COD_MUN.Value))



        // Em �ltimo caso, manual...
        else begin
          Codigo := 0;
          case QrDNELOC_NU_SEQUENCIAL.Value of
            // Lista atual:  DNE de 2008
     //2004   18: Codigo := 120043; // AC DNE=Santa Rosa DTB=Santa Rosa dos Purus
             281: Codigo := 160015; // AP DNE=Amapari DTB=Pedra Branca do Amapari
             405: Codigo := 290330; // BA DNE=Governador Lomato Junior DTB=Barro Preto
             750: Codigo := 291820; // BA DNE=Jequiri�a DTB= Liquiri�a
             998: Codigo := 292850; // BA DNE=Santa Terezinha DTB=Santa Teresinha
            1419: Codigo := 230630; // CE DNE=Itapaj� DTB=Itapag�
            2285: Codigo := 521760; // GO DNE=Planaltina de Goi�s DTB=Planaltina
            2419: Codigo := 210235; // MA DNE=Buritirama DTB=Buritirana
            2462: Codigo := 210455; // MA DNE=Governador Edson Lob�o DTB=Governador Edison Lob�o
            2537: Codigo := 210850; // MA DNE=Pindar� Mirim DTB=Pindar�-mirim
            2568: Codigo := 211027; // MA DNE=Santo Amaro DTB=Santo Amaro do Maranh�o
            2599: Codigo := 211176; // MA DNE=Senador La Roque DTB=Senador La Rocque
            2673: Codigo := 310250; // MG DNE=Amparo da Serra DTB=Amparo do Serra
            3059: Codigo := 312290; // MG DNE=Dona Euz�bia DTB=Dona Eus�bia
            3168: Codigo := 312760; // MG DNE=Gouvea DTB=Gouveia
            3497: Codigo := 314545; // MG DNE=Olhos D��gua DTB=Olhos-d��gua
            3544: Codigo := 314780; // MG DNE=Passa Vinte DTB=Passa-Vinte
            3673: Codigo := 315380; // MG DNE=Queluzita DTB=Queluzito
            3761: Codigo := 315940; // MG DNE=Santa Rita do Ibitipoca DTB=Santa Rita de Ibitipoca
            3951: Codigo := 316556; // MG DNE=Sem Peixe DTB=Sem-Peixe
     //2004 4125: Codigo := 500200; // MS DNE=Bataipor� DTB=Bataypor�
            4448: Codigo := 510700; // MT DNE=Poxor�u DTB=Poxor�o
            5058: Codigo := 251315; // PB DNE=Santa Cec�lia do Umbuzeiro DTB=Santa Cec�lia
            5076: Codigo := 251396; // PB DNE=S�o Domingos de Pombal DTB=S�o Domingos
            5103: Codigo := 251540; // PB DNE=S�o Vicente do Srid� DTB=Serid�
    // 2004 5834: Codigo := 410275; // PR DNE=Bela Vista do Caroba DTB=Bela Vista da Caroba
            6838: Codigo := 412065; // PR DNE=4� Centen�rio DTB=Quarto Centen�rio
            7104: Codigo := 330590; // RJ DNE=Trajano de Morais DTB=Trajano de Moraes
            7135: Codigo := 240120; // RN DNE=Arez DTB=Ar�s
            7142: Codigo := 240530; // RN DNE=Boa Sa�de DTB=Janu�rio Cicco
            7149: Codigo := 240130; // RN DNE=Campo Grande DTB= Augusto Severo
            7275: Codigo := 241255; // RN DNE=S�o Miguel de Touros DTB=S�o Miguel do Gostoso
            7283: Codigo := 241030; // RN DNE=Serra Caiada DTB= Presidente Juscelino
    // 2004 7312: Codigo := 110001; // RO DNE=Alta Floresta do Oeste DTB=Alta Floresta d�Oeste
            7330: Codigo := 110009; // RO DNE=Espig�o do Oeste DTB=Espig�o d�Oeste
            8101: Codigo := 431710; // RS DNE=Santana do Livramento DTB=Sant�Ana do Livramento
    // 2004 8607: Codigo := 421280; // SC DNE=Pi�arras DTB=Balne�rio Pi�arras
            8627: Codigo := 421390; // SC DNE=Presidente Castelo Branco DTB=Presidente Castello Branco
            8793: Codigo := 280260; // SE DNE=Graccho Cardoso DTB=Gracho Cardoso
            9371: Codigo := 353080; // SP DNE=Mogi Mirim DTB=Moji Mirim
            9663: Codigo := 355000; // SP DNE=S�o Luiz do Paraitinga DTB=S�o Luis do Paraitinga
            9842: Codigo := 170600; // TO DNE=Couto de Magalh�es DTB=Couto Magalh�es
            // DTB 2012 x DNE 2008
            9113: Codigo := 351500;//4 // SP DNE=Embu DTB=Embu das Artes
            7004: Codigo := 330380;//7 // RJ DNE=Paraty DTB=Parati
            5322: Codigo := 260850;//3 // PE DNE=Lagoa do Itaenga DTB=Lagoa de Itaenga
            9944: Codigo := 172049;//9 // TO DNE=S�o Val�rio da Natividade DTB=S�o Val�rio
            5117: Codigo := 251640;//9 // PB DNE=Campo de Santana DTB=Tacima
            5072: Codigo := 251365;//3 // PB DNE=Santar�m DTB=Joca Claudino
            else
            begin
              Memo1.Lines.Add(Geral.FF0(QrDNELOC_NU_SEQUENCIAL.Value) + '  -  ' +
              QrDNELOC_NO.Value);
            end;
          end;
          if Codigo > 0 then
          begin
            Codigo := Codigo * 10 + Geral.IMV(Geral.Modulo10_2e1_X_Back(
              FormatFloat('000000', Codigo), 0));
            DModG.AllID_DB.Execute('UPDATE dtb_munici SET ObtencaoDNE=9, DNE=' +
            FormatFloat('0', QrDNELOC_NU_SEQUENCIAL.Value) + ' WHERE Codigo=' +
            FormatFloat('0', Codigo))
          end else
            // caso contr�rio coloca em tabela de n�o achados
            DModG.AllID_DB.Execute('INSERT INTO munsemdtb SET Codigo=' +
            FormatFloat('0', QrDNELOC_NU_SEQUENCIAL.Value) +
            ', UF= "' + QrDNEUFE_SG.Value + '", Munici="' + QrDNEMunici.Value + '"');
        end;
      end;
    end;
    //  DISTRITO
    if QrDTB1.Locate('SIGLA;MUNICI;DISTRI', VarArrayOf([
    QrDNEUFE_SG.Value, QrDNEMunici.Value, QrDNELOC_NOSUB.Value]),[]) then
      DModG.AllID_DB.Execute('UPDATE dtb_distri SET DNE=' +
      FormatFloat('0', QrDNELOC_NU_SEQUENCIAL.Value) + ' WHERE Codigo=' +
      FormatFloat('0', QrDTB1COD_DIS.Value) + ' AND DTB_Munici=' +
      FormatFloat('0', QrDTB1COD_MUN.Value))
    else
      DModG.AllID_DB.Execute('INSERT INTO dissemdtb SET Codigo=' +
      FormatFloat('0', QrDNELOC_NU_SEQUENCIAL.Value) +
      ', UF= "' + QrDNEUFE_SG.Value + '", Munici="' + QrDNEMunici.Value +
      '", Distri="' + QrDNELOC_NOSUB.Value + '"');
    //
    QrDNE.Next;
  end;
  if Memo1.Text <> '' then
    PageControl1.ActivePageIndex := 5;

  Screen.Cursor := crDefault;
end;

procedure TFmIBGE_DTB.BtOKClick(Sender: TObject);
{
const
  xlCellTypeLastCell = $0000000B;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r: Integer;
  Arquivo: String;
}
begin
  MyObjects.Xls_To_StringGrid(Grade1, EdArq.Text, PB1, LaAviso1, LaAviso2);

{
  Screen.Cursor := crHourGlass;
  //Result := False;
  Arquivo := EdArq.Text;
  XLApp := CreateOleObject('Excel.Application');
  try
    XLApp.Visible := False;
    XLApp.Workbooks.Open(Arquivo);
    Sheet := XLApp.Workbooks[ExtractFileName(Arquivo)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    Grade1.RowCount := x;
    Grade1.ColCount := y + 1;
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;
    PB1.Position := 0;
    PB1.Max := x;
    k := 1; // 2?
    repeat
      PB1.Position := PB1.Position + 1;
      Update;
      Application.ProcessMessages;
      Grade1.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
        Grade1.Cells[r, (k - 1)] := RangeMatrix[K, R];
      Inc(k, 1);
      Grade1.RowCount := k;
    until k > x;
    RangeMatrix := Unassigned;
    Geral.MB_(PChar('Os dados foram importados com sucesso do '+
      'arquivo: '+sLineBreak+Arquivo), 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
  finally
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      //Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmIBGE_DTB.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmIBGE_DTB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmIBGE_DTB.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
end;

procedure TFmIBGE_DTB.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmIBGE_DTB.QrDNECalcFields(DataSet: TDataSet);
var
  k, t: Integer;
begin
  if Trim(QrDNELOC_NOSUB.Value) <> Trim(QrDNELOC_NO.Value) then
  begin
    k := pos('(', QrDNELOC_NO.Value) + 1;
    t := pos(')', QrDNELOC_NO.Value) - k;
    QrDNEMunici.Value := Copy(QrDNELOC_NO.Value, k, t);
  end else QrDNEMunici.Value := QrDNELOC_NO.Value;
end;

procedure TFmIBGE_DTB.SpeedButton1Click(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, ''(*IniDir*), ''(*Arquivo*), 'Arquivo IBGE-DTB',
  ''(*Filtro*), [](*Opcoes*), Arquivo) then
  begin
    EdArq.Text := Arquivo;
  end;
end;

procedure TFmIBGE_DTB.BitBtn2Click(Sender: TObject);
{const
  xlCellTypeLastCell = $0000000B;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r: Integer;
  Arquivo, SQL: String;
}
begin
{
  Screen.Cursor := crHourGlass;
  //Result := False;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE IBGE_DTB; ');
  Query.SQL.Add('CREATE TABLE IBGE_DTB (');
  Query.SQL.Add('  Codi_Estado integer      ,');
  Query.SQL.Add('  Nome_Estado varchar(20)  ,');
  Query.SQL.Add('  Codi_Meso   integer      ,');
  Query.SQL.Add('  Nome_Meso   varchar(100) ,');
  Query.SQL.Add('  Codi_Micro  integer      ,');
  Query.SQL.Add('  Nome_Micro  varchar(100) ,');
  Query.SQL.Add('  Codi_Munici integer      ,');
  Query.SQL.Add('  Nome_Munici varchar(100) ,');
  Query.SQL.Add('  Codi_Distri integer      ,');
  Query.SQL.Add('  Nome_Distri varchar(100) ,');
  Query.SQL.Add('  Codi_SubDis integer      ,');
  Query.SQL.Add('  Nome_SubDis varchar(100)  ');
  Query.SQL.Add(');');
  //




  Arquivo := EdArq.Text;
  XLApp := CreateOleObject('Excel.Application');
  try
    XLApp.Visible := False;
    XLApp.Workbooks.Open(Arquivo);
    Sheet := XLApp.Workbooks[ExtractFileName(Arquivo)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;

    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;
    PB1.Position := 0;
    PB1.Max := x;
    k := 2; // 1 = t�tulo
    repeat
      PB1.Position := PB1.Position + 1;
      Update;
      Application.ProcessMessages;
      //
      if Geral.IMV(RangeMatrix[k, 1]) > 0 then
      begin
        SQL := 'INSERT INTO ibge_dtb (' +
          'Codi_Estado, Nome_Estado, Codi_Meso, Nome_Meso, ' +
          'Codi_Micro, Nome_Micro, Codi_Munici, Nome_Munici, ' +
          'Codi_Distri, Nome_Distri, Codi_SubDis, Nome_SubDis) VALUES (';
        for r := 1 to y do
        begin
          SQL := SQL + '"' + Trim(RangeMatrix[k, r]) + '"';
          if r < y then
            SQL := SQL + ','
          else
            SQL := SQL + ');';
        end;
        Inc(k, 1);
        //
        //ShowMessage(SQL);
        Query.SQL.Add(SQL);
      end;
    until k > x;
    RangeMatrix := Unassigned;


    Query.SQL.Add('SELECT * FROM ibge_dtb;');
    UnDmkDAC_PF.AbreQuery(Query.


    Geral.MB_(PChar('Os dados foram importados com sucesso do '+
      'arquivo: '+sLineBreak+Arquivo), 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
  finally
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      //Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmIBGE_DTB.BitBtn3Click(Sender: TObject);
var
  NomeSCE: String;
begin
  Screen.Cursor := crHourGlass;
  DModG.AllID_DB.Execute('UPDATE dtb_munici SET NomeSCE=""');
  PB1.Position := 0;
  UnDmkDAC_PF.AbreQuery(QrMunici, DModG.AllID_DB);
  PB1.Max := QrMunici.RecordCount;
  while not QrMunici.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    NomeSCE := Geral.SemAcento(QrMuniciNome.Value);
    NomeSCE := Geral.Maiusculas(NomeSce, False);
    DModG.AllID_DB.Execute('UPDATE dtb_munici SET NomeSCE="' + NomeSCE +
    '" WHERE Codigo=' + FormatFloat('0', QrMuniciCodigo.Value));
    //
    QrMunici.Next;
  end;
  PB1.Position := 0;
  Screen.Cursor := crDefault;
end;

procedure TFmIBGE_DTB.BitBtn4Click(Sender: TObject);
const
  xlCellTypeLastCell = $0000000B;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r: Integer;
  Arquivo: String;
begin
  Screen.Cursor := crHourGlass;
  //Result := False;
  Arquivo := EdArq.Text;
  XLApp := CreateOleObject('Excel.Application');
  try
    XLApp.Visible := False;
    XLApp.Workbooks.Open(Arquivo);
    Sheet := XLApp.Workbooks[ExtractFileName(Arquivo)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    Grade2.RowCount := x;
    Grade2.ColCount := y + 1;
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;
    PB1.Position := 0;
    PB1.Max := x;
    k := 1; // 2?
    repeat
      PB1.Position := PB1.Position + 1;
      Update;
      Application.ProcessMessages;
      Grade2.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
        Grade2.Cells[r, (k - 1)] := RangeMatrix[K, R];
      Inc(k, 1);
      Grade2.RowCount := k;
    until k > x;
    RangeMatrix := Unassigned;
    Geral.MB_Info('Os dados foram importados com sucesso do arquivo: ' +
      sLineBreak + Arquivo);
  finally
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      //Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmIBGE_DTB.BitBtn5Click(Sender: TObject);
var
  LinIni, Nome, Local, Popu, SQL: String;
  ini, fim, k, i: Integer;
begin
  LinIni := '3';
  if not InputQuery('Importa��o de Pa�ses',
  'Informe a linha inicial (cfe col 0):', LinIni) then Exit;
  //
  ini := Geral.IMV(LinIni);
  fim := Grade2.RowCount - 1;
  //
  Screen.Cursor := crHourGlass;
  DModG.AllID_DB.Execute(DELETE_FROM + ' dtb_paises');
  PB1.Position := 0;
  PB1.Max := fim;
  k       := 0;
  for i := Ini to fim do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    Nome  := Grade2.Cells[01, i];
    Local := Grade2.Cells[03, i];
    try
      Popu  := FormatFloat('0', Geral.IMV(Grade2.Cells[02, i]));
    except
      Popu := '0';
      Geral.MB_Aviso('N�o foi poss�vel obter a popula��o de "' +
      Nome + '  -  ' + Local + '". Ser� considerado como popula��o nula!');
    end;
    if Trim(Nome) <> '' then
    begin
      SQL := 'INSERT INTO dtb_paises (Nome, Local, Populacao) VALUES ("' +
      Nome + '","' + Local + '",'+ Popu + ') ';// + 'ON DUPLICATE KEY UPDATE itens=itens+1;';
      try
        DModG.AllID_DB.Execute(SQL);
        k := k + 1;
      except
        ShowMessage(SQL);
      end;
    end;
  end;
  PB1.Position := 0;
  Screen.Cursor := crDefault;
  Geral.MB_Info('Foram importados ' + IntToStr(k) + ' pa�ses!');
end;

procedure TFmIBGE_DTB.BitBtn6Click(Sender: TObject);
const
  xlCellTypeLastCell = $0000000B;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r: Integer;
  Arquivo: String;
begin
  Screen.Cursor := crHourGlass;
  //Result := False;
  Arquivo := EdArq.Text;
  XLApp := CreateOleObject('Excel.Application');
  try
    XLApp.Visible := False;
    XLApp.Workbooks.Open(Arquivo);
    Sheet := XLApp.Workbooks[ExtractFileName(Arquivo)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    Grade3.RowCount := x;
    Grade3.ColCount := y + 1;
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;
    PB1.Position := 0;
    PB1.Max := x;
    k := 1; // 2?
    repeat
      PB1.Position := PB1.Position + 1;
      Update;
      Application.ProcessMessages;
      Grade3.Cells[0, (k - 1)] := FormatFloat('0000', (k - 1));
      for r := 1 to y do
        Grade3.Cells[r, (k - 1)] := RangeMatrix[K, R];
      Inc(k, 1);
      Grade3.RowCount := k;
    until k > x;
    RangeMatrix := Unassigned;
    Geral.MB_Info('Os dados foram importados com sucesso do arquivo: ' +
    sLineBreak + Arquivo);
  finally
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      //Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmIBGE_DTB.BitBtn7Click(Sender: TObject);
var
  LinIni, Codigo, Nome, SQL: String;
  ini, fim, k, i: Integer;
begin
  LinIni := '4';
  if not InputQuery('Importa��o de Pa�ses',
  'Informe a linha inicial (cfe col 0):', LinIni) then Exit;
  //
  ini := Geral.IMV(LinIni);
  fim := Grade3.RowCount - 1;
  //
  Screen.Cursor := crHourGlass;
  DModG.AllID_DB.Execute(DELETE_FROM + ' bacen_pais');
  PB1.Position := 0;
  PB1.Max := fim;
  k       := 0;
  for i := Ini to fim do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    Codigo  := Grade3.Cells[01, i];
    //Codigo  := FormatFloat('0', Geral.IMV(Codigo) div 10);
    Nome    := Grade3.Cells[02, i];
    if Trim(Nome) <> '' then
    begin
      SQL := 'INSERT INTO bacen_pais (Codigo, Nome) VALUES ("' +
      Codigo + '","' + Nome + '") ';// + 'ON DUPLICATE KEY UPDATE itens=itens+1;';
      try
        DModG.AllID_DB.Execute(SQL);
        k := k + 1;
      except
        ShowMessage(SQL);
      end;
    end;
  end;
  PB1.Position := 0;
  Screen.Cursor := crDefault;
  Geral.MB_Info('Foram importados ' + IntToStr(k) + ' pa�ses!');
end;

end.

