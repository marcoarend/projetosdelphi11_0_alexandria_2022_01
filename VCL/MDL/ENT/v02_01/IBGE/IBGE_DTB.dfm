object FmIBGE_DTB: TFmIBGE_DTB
  Left = 339
  Top = 185
  Caption = 'DTB-INPOR-001 :: Distribui'#231#227'o Territorial Brasileira'
  ClientHeight = 478
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 304
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 303
        Height = 13
        Caption = 'Arquivo excel de origem para importa'#231#227'o dos dados atualizados:'
      end
      object SpeedButton1: TSpeedButton
        Left = 748
        Top = 24
        Width = 21
        Height = 21
        OnClick = SpeedButton1Click
      end
      object EdArq: TEdit
        Left = 8
        Top = 24
        Width = 741
        Height = 21
        TabOrder = 0
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 52
      Width = 784
      Height = 252
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Carrega'
        object Grade1: TStringGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 176
          Align = alClient
          ColCount = 13
          DefaultColWidth = 56
          DefaultRowHeight = 18
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 0
        end
        object Panel3: TPanel
          Left = 0
          Top = 176
          Width = 776
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn1: TBitBtn
            Left = 104
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Carrega'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BitBtn1Click
          end
          object BtOK: TBitBtn
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Abre arq. '
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtOKClick
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Compara c'#243'digos cidades (CEP x DTB)'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 176
          Align = alClient
          DataSource = DsDTB2
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object Panel4: TPanel
          Left = 0
          Top = 176
          Width = 776
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object Label2: TLabel
            Left = 528
            Top = 16
            Width = 9
            Height = 13
            Caption = '...'
          end
          object BitBtn2: TBitBtn
            Left = 676
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Abre arq. 2'
            NumGlyphs = 2
            TabOrder = 0
            Visible = False
            OnClick = BitBtn2Click
          end
          object BtCompara: TBitBtn
            Left = 200
            Top = 4
            Width = 173
            Height = 40
            Caption = 'Compara distritos e cidades'
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtComparaClick
          end
          object BitBtn3: TBitBtn
            Left = 24
            Top = 4
            Width = 173
            Height = 40
            Caption = 'Ajusta nomes de cidades'
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BitBtn3Click
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Pa'#237'ses'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel5: TPanel
          Left = 0
          Top = 176
          Width = 776
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object BitBtn4: TBitBtn
            Left = 208
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Abre arq. '
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BitBtn4Click
          end
          object BitBtn5: TBitBtn
            Left = 308
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Carrega'
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BitBtn5Click
          end
        end
        object Grade2: TStringGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 176
          Align = alClient
          ColCount = 4
          DefaultColWidth = 28
          DefaultRowHeight = 18
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 1
          ColWidths = (
            28
            361
            88
            270)
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Pa'#237'ses BACEN '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel6: TPanel
          Left = 0
          Top = 176
          Width = 776
          Height = 48
          Align = alBottom
          ParentBackground = False
          TabOrder = 0
          object BitBtn6: TBitBtn
            Left = 208
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Abre arq. '
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BitBtn6Click
          end
          object BitBtn7: TBitBtn
            Left = 308
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Carrega'
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BitBtn7Click
          end
        end
        object Grade3: TStringGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 176
          Align = alClient
          ColCount = 3
          DefaultColWidth = 28
          DefaultRowHeight = 18
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 1
          ColWidths = (
            28
            74
            639)
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'DNE ? '
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 224
          Align = alClient
          DataSource = DsDNE
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'DTB sem DNE'
        ImageIndex = 5
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 776
          Height = 224
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 383
        Height = 32
        Caption = 'Distribui'#231#227'o Territorial Brasileira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 383
        Height = 32
        Caption = 'Distribui'#231#227'o Territorial Brasileira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 383
        Height = 32
        Caption = 'Distribui'#231#227'o Territorial Brasileira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 352
    Width = 784
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 408
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BitBtn8: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object DataSource1: TDataSource
    Left = 36
    Top = 12
  end
  object QrDTB1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ufs.SIGLA, mun.Nome MUNICI, dis.Nome DISTRI,'
      'dis.Codigo COD_DIS, mun.Codigo COD_MUN'#13
      'FROM dtb_distri dis'
      'LEFT JOIN dtb_munici mun ON mun.Codigo=dis.DTB_Munici'
      'LEFT JOIN dtb_ufs ufs ON ufs.Codigo=dis.DTB_UF'#10
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 92
    Top = 240
    object QrDTB1SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 2
    end
    object QrDTB1MUNICI: TWideStringField
      FieldName = 'MUNICI'
      Size = 100
    end
    object QrDTB1DISTRI: TWideStringField
      FieldName = 'DISTRI'
      Required = True
      Size = 100
    end
    object QrDTB1COD_DIS: TIntegerField
      FieldName = 'COD_DIS'
      Required = True
    end
    object QrDTB1COD_MUN: TIntegerField
      FieldName = 'COD_MUN'
      Required = True
    end
  end
  object DsDTB2: TDataSource
    Left = 148
    Top = 240
  end
  object QrDTB2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ufs.SIGLA, mun.Nome MUNICI, '
      'mun.Codigo COD_MUN, mun.NomeSCE'#13
      'FROM dtb_munici mun '#10
      ''
      ''
      'LEFT JOIN dtb_ufs ufs ON ufs.Codigo=mun.DTB_UF'#10
      ''
      '')
    Left = 120
    Top = 240
    object QrDTB2SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 2
    end
    object QrDTB2MUNICI: TWideStringField
      FieldName = 'MUNICI'
      Required = True
      Size = 100
    end
    object QrDTB2COD_MUN: TIntegerField
      FieldName = 'COD_MUN'
      Required = True
    end
    object QrDTB2NomeSCE: TWideStringField
      FieldName = 'NomeSCE'
      Required = True
      Size = 100
    end
  end
  object QrMunici: TMySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT ufs.Sigla, mun.Codigo, mun.Nome '
      'FROM dtb_munici mun'
      'LEFT JOIN dtb_ufs ufs ON ufs.Codigo=mun.dtb_uf')
    Left = 92
    Top = 208
    object QrMuniciSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 2
    end
    object QrMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMuniciNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object mySQLQuery1: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 92
    Top = 296
  end
  object DataSource2: TDataSource
    Left = 120
    Top = 296
  end
  object QrDTB_UFs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM dtb_ufs')
    Left = 340
    Top = 168
  end
  object DsDTB_UFs: TDataSource
    DataSet = QrDTB_UFs
    Left = 368
    Top = 168
  end
  object DsDNE: TDataSource
    Left = 148
    Top = 268
  end
end
