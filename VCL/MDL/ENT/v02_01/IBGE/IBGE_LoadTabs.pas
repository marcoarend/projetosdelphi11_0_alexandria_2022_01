unit IBGE_LoadTabs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  UnDmkProcFunc, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmIBGE_LoadTabs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label39: TLabel;
    EdBACEN_Pais: TdmkEdit;
    Label1: TLabel;
    EdDTB_Munici: TdmkEdit;
    SbBacen_Pais: TSpeedButton;
    SbDTB_Munici: TSpeedButton;
    BtDTB_Munici: TButton;
    BtBacen_Pais: TButton;
    MeTextoArq: TMemo;
    PB: TProgressBar;
    QrDest: TmySQLQuery;
    Button1: TButton;
    Button2: TButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbBacen_PaisClick(Sender: TObject);
    procedure SbDTB_MuniciClick(Sender: TObject);
    procedure BtBacen_PaisClick(Sender: TObject);
    procedure BtDTB_MuniciClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    FItens: Integer;
    procedure CarregaArquivo(tpNome, Arquivo, Tabela: String);
    procedure CarregaArquivo_Novo(tpNome, Arquivo, Tabela: String);
    procedure InsereRegistroCodigoNome(tpNome, Tabela, CodTxt, Nome: String);
    procedure ObtemCodigoNome(const Linha: String; var CodTxt, Nome,
              DataIni, DataFim: String);
    function  ConfirmaArquivo(Texto: String): Boolean;
  public
    { Public declarations }
  end;

  var
  FmIBGE_LoadTabs: TFmIBGE_LoadTabs;

implementation

uses UnMyObjects, ModuleGeral, UMySQLModule, UnDmkWeb, Module;

const
  FURLBacen      = 'http://www.dermatek.com.br/Tabelas_Publicas/tb6.txt';
  FURLDTB        = 'http://www.dermatek.com.br/Tabelas_Publicas/tb4.txt';
  FDestFileBacen = 'C:\Dermatek\Tabelas\BACEN\tb6.txt';
  FDestFileDTB   = 'C:\Dermatek\Tabelas\DTB\tb4.txt';


{$R *.DFM}

procedure TFmIBGE_LoadTabs.BtBacen_PaisClick(Sender: TObject);
var
  Fonte: String;
begin
  if Lowercase(Copy(EdBACEN_Pais.Text, 1, 4)) = 'http' then
  begin
    Fonte := EdBACEN_Pais.Text;
    if FileExists(FDestFileBacen) then DeleteFile(FDestFileBacen);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Aguarde... Baixando arquivo!');
    Application.ProcessMessages;
    //
    if not DirectoryExists(ExtractFileDir(FDestFileBacen)) then
      ForceDirectories(ExtractFileDir(FDestFileBacen));
    //
    if dmkWeb.DownloadFile(Fonte, FDestFileBacen) then
      CarregaArquivo('Pa�s', FDestFileBacen, 'bacen_pais');
  end else
    CarregaArquivo('Pa�s', EdBacen_Pais.Text, 'bacen_pais');
end;

procedure TFmIBGE_LoadTabs.BtDTB_MuniciClick(Sender: TObject);
var
  Fonte: String;
begin
  if Lowercase(Copy(EdDTB_Munici.Text, 1, 4)) = 'http' then
  begin
    Fonte := EdDTB_Munici.Text;
    if FileExists(FDestFileDTB) then DeleteFile(FDestFileDTB);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Aguarde... Baixando arquivo!');
    Application.ProcessMessages;
    //
    if not DirectoryExists(ExtractFileDir(FDestFileDTB)) then
      ForceDirectories(ExtractFileDir(FDestFileDTB));
    //
    if dmkWeb.DownloadFile(Fonte, FDestFileDTB) then
      CarregaArquivo_Novo('Munic�pio', FDestFileDTB, 'dtb_munici');
  end else
    CarregaArquivo_Novo('Munic�pio', EdDTB_Munici.Text, 'dtb_munici');
end;

procedure TFmIBGE_LoadTabs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmIBGE_LoadTabs.Button2Click(Sender: TObject);
var
  Fonte: String;
begin
  if Lowercase(Copy(EdBACEN_Pais.Text, 1, 4)) = 'http' then
  begin
    Fonte := EdBACEN_Pais.Text;
    if FileExists(FDestFileBacen) then DeleteFile(FDestFileBacen);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Aguarde... Baixando arquivo!');
    Application.ProcessMessages;
    //
    if not DirectoryExists(ExtractFileDir(FDestFileBacen)) then
      ForceDirectories(ExtractFileDir(FDestFileBacen));
    //
    if dmkWeb.DownloadFile(Fonte, FDestFileBacen) then
      CarregaArquivo_Novo('Pa�s', FDestFileBacen, 'bacen_pais');
  end else
    CarregaArquivo_Novo('Pa�s', EdBacen_Pais.Text, 'bacen_pais');
end;

procedure TFmIBGE_LoadTabs.CarregaArquivo(tpNome, Arquivo, Tabela: String);
var
  I: Integer;
  Linha, CodTxt, Nome, DataIni, DataFim: String;
  lstArq: TStringList;
begin
  FItens := 0;
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
    lstArq := TStringList.Create;
    try
      lstArq.LoadFromFile(Arquivo);
      if lstArq.Count > 0 then
      begin
        PB.Position := 0;
        PB.Max := lstArq.Count;
        for I := 1 to lstArq.Count - 1 do
        begin
          PB.Position := PB.Position + 1;
          //
          Linha := lstArq[I];
          //
          ObtemCodigoNome(Linha, CodTxt, Nome, DataIni, DataFim);
          //
          InsereRegistroCodigoNome(tpNome, Tabela, CodTxt, Nome);
        end;
      end;
      //
      Geral.MB_Info('Carregamento finalizado!');
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      PB.Position := 0;
    finally
      if lstArq <> nil then
        lstArq.Free;
    end;
  end;
end;

procedure TFmIBGE_LoadTabs.CarregaArquivo_Novo(tpNome, Arquivo, Tabela: String);
const
  MaxStat = 100;
var
  I, StatCount: Integer;
  Linha, CodTxt, Nome, DataIni, DataFim, SQLStat: String;
  lstArq: TStringList;
  //
  SQL_FIELDS, SQL_VALUES, Tab: String;
begin
  Tab := Lowercase(Tabela);
  if Geral.MB_Pergunta(
  'TODOS cadastros da tabela "' + tab + '" ser�o exclu�dos antes da inclus�o!' +
  sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  //
  DModG.AllID_DB.Execute('DELETE FROM ' + Tab);
  //
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + Tab + ' ( ',
    'Codigo, Nome) VALUES ']);
  SQL_VALUES := '';
  //

  FItens := 0;
  StatCount := 0;
  //
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
    lstArq := TStringList.Create;
    try
      lstArq.LoadFromFile(Arquivo);
      if lstArq.Count > 0 then
      begin
        PB.Position := 0;
        PB.Max := lstArq.Count;
        for I := 1 to lstArq.Count - 1 do
        begin
          PB.Position := PB.Position + 1;
          //
          Linha := lstArq[I];
          //
          ObtemCodigoNome(Linha, CodTxt, Nome, DataIni, DataFim);
          //
          SQLStat := '(' + CodTxt + ', "' + Nome + '")';
          SQL_VALUES := SQL_VALUES + SQLStat;
          StatCount := StatCount + 1;
          //
          if (StatCount >= MaxStat) or (I = lstArq.Count - 1) then
          begin
            DModG.AllID_DB.Execute(SQL_FIELDS + SQL_VALUES);
            SQL_VALUES := '';
            StatCount := 0;
          end else
          begin
            if SQL_VALUES <> EmptyStr then
              SQL_VALUES := SQL_VALUES + ', ';
          end;
        end;
      end;
      //
      Geral.MB_Info('Carregamento finalizado!');
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      PB.Position := 0;
    finally
      if lstArq <> nil then
        lstArq.Free;
    end;
  end;
end;

function TFmIBGE_LoadTabs.ConfirmaArquivo(Texto: String): Boolean;
var
  I, N: Integer;
  K: Boolean;
  Linha: String;
begin
  K := False;
  for I := 0 to MeTextoArq.Lines.Count - 1 do
  begin
    Linha := MeTextoArq.Lines[I];
    N := pos(Texto, Linha);
    if N = 1 then
    begin
      K := True;
      Break;
    end;
  end;
  Result := K;
  if not K then
  begin
    Result := Geral.MB_Pergunta('A string "' + Texto +
    '" n�o foi localizado no texto carregado!' + sLineBreak +
    'Deseja desabilitar o bot�o "Carrega" correspondente assim mesmo?') = ID_YES;
  end;
end;

procedure TFmIBGE_LoadTabs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
  //
  EdBACEN_Pais.CharCase     := ecNormal;
  EdDTB_Munici.CharCase     := ecNormal;
  EdBACEN_Pais.ValueVariant := FURLBacen;
  EdDTB_Munici.ValueVariant := FURLDTB;
end;

procedure TFmIBGE_LoadTabs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmIBGE_LoadTabs.InsereRegistroCodigoNome(tpNome, Tabela, CodTxt, Nome: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDest, DModG.AllID_DB, [
  'SELECT Codigo, Nome ',
  'FROM ' + Tabela,
  'WHERE Codigo=' + CodTxt,
  '']);
  //
  if QrDest.RecordCount = 0 then
  begin
    FItens := FItens + 1;
    if UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, Tabela, False, [
    'Nome'], ['Codigo'], [Nome], [CodTxt], False) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, tpNome + ' inclu�dos: ' +
      Geral.FF0(FItens) + '  ::  �ltimo incluido: ' + CodTxt + ' > "' + Nome + '"');
    end;
  end;
end;

procedure TFmIBGE_LoadTabs.ObtemCodigoNome(const Linha: String; var CodTxt, Nome,
  DataIni, DataFim: String);
var
  lstLin: TStringList;
  P: Integer;
begin
  CodTxt  := '';
  Nome    := '';
  DataIni := '0000000000';
  DataFim := '0000000000';
  //
  P := pos('|', Linha);
  if P > 0 then
  begin
    lstLin := TStringList.Create;
    try
      Geral.MyExtractStrings(['|'], [' '], PChar(Linha + '|'), lstLin);
      CodTxt  := Copy(Linha, 1, P-1);
      if lstLin.Count > 0 then
        Nome    := lstLin[00];
      if lstLin.Count > 1 then
        DataIni := lstLin[01];
      if lstLin.Count > 2 then
        DataFim := lstLin[02];
    finally
      if lstLin <> nil then
        lstLin.Free;
    end;
  end;
end;

procedure TFmIBGE_LoadTabs.SbBacen_PaisClick(Sender: TObject);
var
  Arq, IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(FDestFileBacen);
  Arquivo := ExtractFileName(FDestFileBacen);
  //
  MyObjects.DefineArquivo2(Self, EdBACEN_Pais, IniDir, Arquivo, ppAtual_Edit);
  Arq := EdBACEN_Pais.Text;
  if FileExists(Arq) then
  begin
    MeTextoArq.Text := dmkPF.LoadFileToText(Arq);
    BtBacen_Pais.Enabled := ConfirmaArquivo('1058|BRASIL|');
  end;
end;

procedure TFmIBGE_LoadTabs.SbDTB_MuniciClick(Sender: TObject);
var
  Arq, IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(FDestFileDTB);
  Arquivo := ExtractFileName(FDestFileDTB);
  //
  MyObjects.DefineArquivo2(Self, EdDTB_Munici, IniDir, Arquivo, ppAtual_Edit);
  Arq := EdDTB_Munici.Text;
  if FileExists(Arq) then
  begin
    MeTextoArq.Text := dmkPF.LoadFileToText(Arq);
    BtDTB_Munici.Enabled := ConfirmaArquivo('5300108|Bras�lia|');

  end;
end;

end.
