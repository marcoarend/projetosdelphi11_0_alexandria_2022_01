unit EntiCtas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkGeral, dmkLabel, dmkImage, UnDmkEnums;

type
  TFmEntiCtas = class(TForm)
    QrBancos: TmySQLQuery;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    DsBancos: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    PainelDados: TPanel;
    Label3: TLabel;
    Label5: TLabel;
    Label11: TLabel;
    Label7: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    EdCedAgencia: TdmkEdit;
    EdCedDAC_A: TdmkEdit;
    EdCedConta: TdmkEdit;
    EdCedDAC_C: TdmkEdit;
    EdCedDAC_AC: TdmkEdit;
    EdCedContaTip: TdmkEdit;
    EdCedBanco: TdmkEditCB;
    CBCedBanco: TdmkDBLookupComboBox;
    EdControle: TdmkEdit;
    EdCodigo: TdmkEdit;
    EdOrdem: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmEntiCtas: TFmEntiCtas;

implementation

uses UnMyObjects, Module, Entidades, EntiTransp, DmkDAC_PF;

{$R *.DFM}

procedure TFmEntiCtas.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiCtas.BtConfirmaClick(Sender: TObject);
var
  Controle, Codigo, Banco, Agencia, Ordem: Integer;
  ContaCor, DAC_A, DAC_C, DAC_AC, ContaTip: String;
begin
  Codigo   := Geral.IMV(EdCodigo.Text);
  Banco    := EdCedBanco.ValueVariant;
  Agencia  := EdCedAgencia.ValueVariant;
  ContaCor := EdCedConta.ValueVariant;
  DAC_A    := EdCedDAC_A.ValueVariant;
  DAC_C    := EdCedDAC_C.ValueVariant;
  DAC_AC   := EdCedDAC_AC.ValueVariant;
  ContaTip := EdCedContaTip.ValueVariant;
  Ordem    := Geral.IMV(EdOrdem.Text);

  if Banco = 0 then
  begin
    Geral.MB_Aviso('Informe o banco!');
    EdCedBanco.SetFocus;
    Exit;
  end;
  if Agencia = 0 then
  begin
    Geral.MB_Aviso('Informe a ag�ncia!');
    EdCedAgencia.SetFocus;
    Exit;
  end;
  if ContaCor = '' then
  begin
    Geral.MB_Aviso('Informe a conta!');
    EdCedConta.SetFocus;
    Exit;
  end;
  if ImgTipo.SQLType = stIns then
    Controle := UMyMod.BuscaEmLivreY_Def('entictas', 'Controle', ImgTipo.SQLType, EdControle.ValueVariant)
  else
    Controle := EdControle.ValueVariant;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'entictas', False,
  [
    'Banco', 'Agencia', 'ContaCor', 'DAC_A', 'DAC_C',
    'DAC_AC', 'ContaTip', 'Ordem', 'Codigo'
  ], ['Controle'],
  [
    Banco, Agencia, ContaCor, DAC_A,
    DAC_C, DAC_AC, ContaTip, Ordem, Codigo
  ], [Controle], True) then
  begin
    Close;
  end;
end;

procedure TFmEntiCtas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrBancos, Dmod.MyDB);
end;

procedure TFmEntiCtas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiCtas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
