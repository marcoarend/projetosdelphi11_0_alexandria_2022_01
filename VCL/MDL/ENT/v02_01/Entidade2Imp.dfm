object FmEntidade2Imp: TFmEntidade2Imp
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-016 :: Pesquisa entidades'
  ClientHeight = 646
  ClientWidth = 870
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 870
    Height = 486
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnFiltros: TPanel
      Left = 0
      Top = 0
      Width = 870
      Height = 364
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object BGFiltros: TGroupBox
        Left = 0
        Top = 0
        Width = 870
        Height = 266
        Align = alTop
        Caption = 'Filtros'
        TabOrder = 0
        object Label1: TLabel
          Left = 437
          Top = 84
          Width = 15
          Height = 13
          Caption = 'at'#233
        end
        object CkUF: TdmkCheckBox
          Left = 222
          Top = 18
          Width = 44
          Height = 16
          Caption = 'UF'
          TabOrder = 2
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object CBUF: TdmkDBLookupComboBox
          Left = 222
          Top = 37
          Width = 44
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsUF
          TabOrder = 3
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBCidade: TdmkDBLookupComboBox
          Left = 272
          Top = 37
          Width = 246
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsMunici
          TabOrder = 5
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CkCidade: TdmkCheckBox
          Left = 272
          Top = 18
          Width = 246
          Height = 16
          Caption = 'Cidade'
          TabOrder = 4
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object CBPais: TdmkDBLookupComboBox
          Left = 18
          Top = 37
          Width = 198
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsBacen_Pais
          TabOrder = 1
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CkPais: TdmkCheckBox
          Left = 18
          Top = 18
          Width = 198
          Height = 16
          Caption = 'Pa'#237's'
          TabOrder = 0
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object CkBairro: TdmkCheckBox
          Left = 524
          Top = 18
          Width = 227
          Height = 16
          Caption = 'Bairro'
          TabOrder = 6
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object CBBairro: TdmkDBLookupComboBox
          Left = 524
          Top = 37
          Width = 227
          Height = 21
          KeyField = 'BAIRRO'
          ListField = 'BAIRRO'
          ListSource = DsBairros
          TabOrder = 7
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CkLograd: TdmkCheckBox
          Left = 18
          Top = 62
          Width = 248
          Height = 16
          Caption = 'Nome do logradouro'
          TabOrder = 8
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object CBLograd: TdmkDBLookupComboBox
          Left = 18
          Top = 81
          Width = 349
          Height = 21
          KeyField = 'Rua'
          ListField = 'Rua'
          ListSource = DsRuas
          TabOrder = 9
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CkCEP: TdmkCheckBox
          Left = 374
          Top = 62
          Width = 143
          Height = 16
          Caption = 'CEP'
          TabOrder = 10
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object EdCepIni: TdmkEdit
          Left = 373
          Top = 81
          Width = 61
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtCEP
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ECEP'
          UpdCampo = 'ECEP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCepFim: TdmkEdit
          Left = 456
          Top = 81
          Width = 61
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtCEP
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ECEP'
          UpdCampo = 'ECEP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCNPJ: TdmkEdit
          Left = 523
          Top = 81
          Width = 111
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
          FormatType = dmktfString
          MskType = fmtCPFJ
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CNPJ'
          UpdCampo = 'CNPJ'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkCNPJ: TdmkCheckBox
          Left = 523
          Top = 62
          Width = 111
          Height = 16
          Caption = 'CNPJ'
          TabOrder = 13
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object EdCPF: TdmkEdit
          Left = 641
          Top = 81
          Width = 110
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 16
          FormatType = dmktfString
          MskType = fmtCPFJ
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CPF'
          UpdCampo = 'CPF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkCPF: TdmkCheckBox
          Left = 641
          Top = 62
          Width = 110
          Height = 16
          Caption = 'CPF'
          TabOrder = 15
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object EdTelefone: TdmkEdit
          Left = 641
          Top = 126
          Width = 110
          Height = 20
          TabOrder = 20
          FormatType = dmktfString
          MskType = fmtTelLongo
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ETe1'
          UpdCampo = 'ETe1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkRazao: TdmkCheckBox
          Left = 18
          Top = 107
          Width = 616
          Height = 17
          Caption = 'Nome, Raz'#227'o Social'
          TabOrder = 17
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object CkTelefone: TdmkCheckBox
          Left = 641
          Top = 107
          Width = 110
          Height = 17
          Caption = 'Telefone principal'
          TabOrder = 19
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object EdRazao: TdmkEdit
          Left = 18
          Top = 126
          Width = 616
          Height = 20
          TabOrder = 18
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdApelido: TdmkEdit
          Left = 18
          Top = 174
          Width = 331
          Height = 20
          TabOrder = 22
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkApelido: TdmkCheckBox
          Left = 18
          Top = 154
          Width = 331
          Height = 17
          Caption = 'Apelido'
          TabOrder = 21
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object EdFantasia: TdmkEdit
          Left = 354
          Top = 174
          Width = 329
          Height = 20
          TabOrder = 24
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkFantasia: TdmkCheckBox
          Left = 354
          Top = 154
          Width = 329
          Height = 17
          Caption = 'Nome fantasia'
          TabOrder = 23
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object EdCodigo: TdmkEdit
          Left = 687
          Top = 174
          Width = 64
          Height = 20
          Alignment = taRightJustify
          TabOrder = 26
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ETe1'
          UpdCampo = 'ETe1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CkCodigo: TdmkCheckBox
          Left = 687
          Top = 154
          Width = 63
          Height = 17
          Caption = 'ID'
          TabOrder = 25
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object CkCliInt: TCheckBox
          Left = 368
          Top = 216
          Width = 151
          Height = 16
          Caption = 'Somente clientes internos.'
          TabOrder = 29
        end
        object CkAtivo: TCheckBox
          Left = 368
          Top = 238
          Width = 110
          Height = 17
          Caption = 'Somente ativos'
          TabOrder = 30
        end
        object GroupBox1: TGroupBox
          Left = 18
          Top = 202
          Width = 345
          Height = 59
          TabOrder = 28
          object Label2: TLabel
            Left = 174
            Top = 16
            Width = 81
            Height = 13
            Caption = 'Final: (Dia / M'#234's)'
            FocusControl = EdFantasia
          end
          object Label3: TLabel
            Left = 11
            Top = 16
            Width = 86
            Height = 13
            Caption = 'Inicial: (Dia / M'#234's)'
            FocusControl = EdFantasia
          end
          object TPDataFim: TDateTimePicker
            Left = 174
            Top = 31
            Width = 158
            Height = 21
            Date = 41198.000000000000000000
            Format = 'dd/MM'
            Time = 0.370597881941648700
            TabOrder = 1
          end
          object TPDataIni: TDateTimePicker
            Left = 11
            Top = 31
            Width = 157
            Height = 21
            Date = 41198.000000000000000000
            Format = 'dd/MM'
            Time = 0.370597881941648700
            TabOrder = 0
          end
        end
        object CkAniversario: TCheckBox
          Left = -37
          Top = 210
          Width = 79
          Height = 16
          Caption = 'Anivers'#225'rios'
          TabOrder = 27
        end
      end
      object RGOrigem: TRadioGroup
        Left = 18
        Top = 274
        Width = 158
        Height = 84
        Caption = ' Origem dos dados: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Entidade'
          'Pessoal'
          'Empresa'
          'Cobran'#231'a'
          'Entrega')
        TabOrder = 1
      end
      object BtTudo: TBitBtn
        Tag = 127
        Left = 726
        Top = 277
        Width = 132
        Height = 39
        Cursor = crHandPoint
        Hint = 'Todos tipos de cadastro'
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtTudoClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 726
        Top = 321
        Width = 132
        Height = 39
        Cursor = crHandPoint
        Hint = 'Todos tipos de cadastro'
        Caption = '&Nenhum'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtNenhumClick
      end
      object GroupBox4: TGroupBox
        Left = 182
        Top = 274
        Width = 536
        Height = 84
        Caption = ' Tipo de cadastro: '
        TabOrder = 4
        object CkCliente1_0: TdmkCheckBox
          Left = 5
          Top = 16
          Width = 128
          Height = 17
          Caption = 'Cliente1'
          TabOrder = 0
          QryCampo = 'Cliente1'
          UpdCampo = 'Cliente1'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkFornece1_0: TdmkCheckBox
          Left = 138
          Top = 16
          Width = 128
          Height = 17
          Caption = 'Fornece 1'
          TabOrder = 4
          QryCampo = 'Fornece1'
          UpdCampo = 'Fornece1'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkFornece2_0: TdmkCheckBox
          Left = 138
          Top = 31
          Width = 128
          Height = 17
          Caption = 'Fornece 2'
          TabOrder = 5
          Visible = False
          QryCampo = 'Fornece2'
          UpdCampo = 'Fornece2'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkFornece3_0: TdmkCheckBox
          Left = 138
          Top = 47
          Width = 128
          Height = 17
          Caption = 'Fornece 3'
          TabOrder = 6
          Visible = False
          QryCampo = 'Fornece3'
          UpdCampo = 'Fornece3'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkFornece4_0: TdmkCheckBox
          Left = 138
          Top = 63
          Width = 128
          Height = 17
          Caption = 'Fornece 4'
          TabOrder = 7
          Visible = False
          QryCampo = 'Fornece4'
          UpdCampo = 'Fornece4'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkTerceiro_0: TdmkCheckBox
          Left = 404
          Top = 16
          Width = 128
          Height = 17
          Caption = 'Terceiro'
          TabOrder = 12
          QryCampo = 'Terceiro'
          UpdCampo = 'Terceiro'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkCliente2_0: TdmkCheckBox
          Left = 5
          Top = 31
          Width = 128
          Height = 17
          Caption = 'Cliente 2'
          TabOrder = 1
          Visible = False
          QryCampo = 'Cliente2'
          UpdCampo = 'Cliente2'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkFornece5_0: TdmkCheckBox
          Left = 270
          Top = 16
          Width = 128
          Height = 17
          Caption = 'Fornece 5'
          TabOrder = 8
          Visible = False
          QryCampo = 'Fornece5'
          UpdCampo = 'Fornece5'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkFornece6_0: TdmkCheckBox
          Left = 270
          Top = 31
          Width = 128
          Height = 17
          Caption = 'Fornece 6'
          TabOrder = 9
          Visible = False
          QryCampo = 'Fornece6'
          UpdCampo = 'Fornece6'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkCliente4_0: TdmkCheckBox
          Left = 5
          Top = 63
          Width = 128
          Height = 17
          Caption = 'Cliente 4'
          TabOrder = 3
          Visible = False
          QryCampo = 'Cliente4'
          UpdCampo = 'Cliente4'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkCliente3_0: TdmkCheckBox
          Left = 5
          Top = 47
          Width = 128
          Height = 17
          Caption = 'Cliente 3'
          TabOrder = 2
          Visible = False
          QryCampo = 'Cliente3'
          UpdCampo = 'Cliente3'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkFornece8_0: TdmkCheckBox
          Left = 270
          Top = 63
          Width = 128
          Height = 17
          Caption = 'Fornece 8'
          TabOrder = 11
          Visible = False
          QryCampo = 'Fornece8'
          UpdCampo = 'Fornece8'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
        object CkFornece7_0: TdmkCheckBox
          Left = 270
          Top = 47
          Width = 128
          Height = 17
          Caption = 'Fornece 7'
          TabOrder = 10
          Visible = False
          QryCampo = 'Fornece7'
          UpdCampo = 'Fornece7'
          UpdType = utYes
          ValCheck = 'V'
          ValUncheck = 'F'
          OldValor = #0
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 364
      Width = 870
      Height = 122
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Entidades'
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 864
          Height = 97
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENTIDADE'
              Title.Caption = 'Nome'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RUA'
              Title.Caption = 'Logradouro'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NUM_TXT'
              Title.Caption = 'N'#250'mero'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Compl'
              Title.Caption = 'Complemento'
              Width = 171
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Bairro'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DTB_MUNICI'
              Title.Caption = 'Cidade'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEUF'
              Title.Caption = 'UF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CEP_TXT'
              Title.Caption = 'CEP'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BACEN_PAIS'
              Title.Caption = 'Pa'#237's'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ETE1_TXT'
              Title.Caption = 'Telefone principal'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJ_CPF_TXT'
              Title.Caption = 'CNPJ / CPF'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IE_RG'
              Title.Caption = 'I.E. / RG'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Width = 59
              Visible = True
            end>
          Color = clWindow
          DataSource = DsEntidades
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dmkDBGrid1CellClick
          OnDblClick = dmkDBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENTIDADE'
              Title.Caption = 'Nome'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RUA'
              Title.Caption = 'Logradouro'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NUM_TXT'
              Title.Caption = 'N'#250'mero'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Compl'
              Title.Caption = 'Complemento'
              Width = 171
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Bairro'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DTB_MUNICI'
              Title.Caption = 'Cidade'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEUF'
              Title.Caption = 'UF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CEP_TXT'
              Title.Caption = 'CEP'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BACEN_PAIS'
              Title.Caption = 'Pa'#237's'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ETE1_TXT'
              Title.Caption = 'Telefone principal'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJ_CPF_TXT'
              Title.Caption = 'CNPJ / CPF'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IE_RG'
              Title.Caption = 'I.E. / RG'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Width = 59
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Contatos'
        ImageIndex = 2
        object Splitter1: TSplitter
          Left = 576
          Top = 0
          Width = 5
          Height = 103
        end
        object Splitter2: TSplitter
          Left = 295
          Top = 0
          Width = 5
          Height = 103
        end
        object DBGEntiMail: TDBGrid
          Left = 581
          Top = 0
          Width = 283
          Height = 103
          Align = alClient
          DataSource = DsEntiMail
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEETC'
              Title.Caption = 'Tipo de e-mail'
              Width = 67
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EMail'
              Title.Caption = 'E-mail'
              Width = 200
              Visible = True
            end>
        end
        object DBGEntiTel: TDBGrid
          Left = 300
          Top = 0
          Width = 276
          Height = 103
          Align = alLeft
          DataSource = DsEntiTel
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMEETC'
              Title.Caption = 'Tipo de telefone'
              Width = 67
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TEL_TXT'
              Title.Caption = 'Telefone'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ramal'
              Width = 30
              Visible = True
            end>
        end
        object DBGEntiContat: TDBGrid
          Left = 0
          Top = 0
          Width = 295
          Height = 103
          Align = alLeft
          DataSource = DsEntiContat
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_CARGO'
              Title.Caption = 'Cargo'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DTANATAL_TXT'
              Title.Caption = 'Anivers'#225'rio'
              Width = 60
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Dados banc'#225'rios'
        ImageIndex = 1
        object DBGEntiCtas: TDBGrid
          Left = 0
          Top = 0
          Width = 864
          Height = 103
          Align = alClient
          DataSource = DsEntiCtas
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBANCO'
              Title.Caption = 'Banco'
              Width = 222
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag'#234'ncia'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DAC_A'
              Title.Caption = '[A]'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ContaCor'
              Title.Caption = 'Conta'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DAC_C'
              Title.Caption = '[C]'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DAC_AC'
              Title.Caption = '[AC]'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ContaTip'
              Title.Caption = 'Tipo'
              Width = 59
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 870
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 823
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 776
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 228
        Height = 31
        Caption = 'Pesquisa Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 228
        Height = 31
        Caption = 'Pesquisa Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 228
        Height = 31
        Caption = 'Pesquisa Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 533
    Width = 870
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 14
      Width = 867
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 577
    Width = 870
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 726
      Top = 14
      Width = 143
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSair: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSairClick
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 14
      Width = 724
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 22
        Left = 12
        Top = 4
        Width = 118
        Height = 39
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 134
        Top = 4
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Hint = 'Imprime'
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object DsUF: TDataSource
    DataSet = QrUF
    Left = 96
    Top = 12
  end
  object QrUF: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ufs'
      'ORDER BY Nome')
    Left = 68
    Top = 12
    object QrUFCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUFNome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
    object QrUFLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrUFDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrUFDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrUFUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrUFUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrUFICMS_C: TFloatField
      FieldName = 'ICMS_C'
    end
    object QrUFICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
  end
  object DsBacen_Pais: TDataSource
    DataSet = QrBacen_Pais
    Left = 40
    Top = 12
  end
  object QrBacen_Pais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bacen_pais'
      'ORDER BY Nome'
      '')
    Left = 12
    Top = 12
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMunici: TDataSource
    DataSet = QrMunici
    Left = 152
    Top = 12
  end
  object QrMunici: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 124
    Top = 12
    object QrMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBairros: TDataSource
    DataSet = QrBairros
    Left = 208
    Top = 12
  end
  object QrBairros: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT'
      'IF (Tipo=0, EBairro, PBairro) BAIRRO'
      'FROM entidades'
      'WHERE IF (Tipo=0, EBairro, PBairro) <>'#39#39
      'ORDER BY Bairro')
    Left = 180
    Top = 12
    object QrBairrosBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
  end
  object QrRuas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT'
      'IF (Tipo=0, ERua, PRua) Rua'
      'FROM entidades'
      'WHERE IF (Tipo=0, ERua, PRua) <>'#39#39
      'ORDER BY Rua')
    Left = 236
    Top = 12
    object QrRuasRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
  end
  object DsRuas: TDataSource
    DataSet = QrRuas
    Left = 264
    Top = 12
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntidadesAfterOpen
    BeforeClose = QrEntidadesBeforeClose
    AfterClose = QrEntidadesAfterClose
    AfterScroll = QrEntidadesAfterScroll
    OnCalcFields = QrEntidadesCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.LimiCred, '
      'IF (en.Tipo=0, en.RazaoSocial, en.Nome) NOMEENTIDADE,'
      'IF (en.Tipo=0, en.Fantasia, en.Apelido) NOMEFANTASIA,'
      'IF (en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF,'
      'IF (en.Tipo=0, en.IE, en.RG) IE_RG,'
      'IF (Tipo=0, en.ERua,    en.PRua    ) RUA,'
      'IF (Tipo=0, en.ENumero+0.000, en.PNumero+0.000 ) Numero,'
      'IF (Tipo=0, en.ECompl,  en.PCompl  ) Compl      ,'
      'IF (Tipo=0, en.EBairro, en.PBairro ) Bairro     ,'
      'IF (Tipo=0, mue.Nome, mup.Nome     ) DTB_MUNICI ,'
      'IF (Tipo=0, en.EUF+0.000,     en.PUF+0.000     )  UF,'
      'IF (Tipo=0, en.ECEP+0.000,    en.PCEP+0.000    ) CEP,'
      'IF (Tipo=0, pae.Nome,   pap.Nome   ) BACEN_PAIS ,'
      'IF (Tipo=0, ufe.Nome,   ufp.Nome   ) NOMEUF,'
      'en.PTe1, en.ETe1, en.ENatal, en.PNatal, en.Tipo'
      'FROM entidades en'
      'LEFT JOIN ufs ufe ON ufe.Codigo = en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo = en.PUF'
      'LEFT JOIN bacen_pais pae ON pae.Codigo = en.ECodiPais'
      'LEFT JOIN bacen_pais pap ON pap.Codigo = en.PCodiPais'
      'LEFT JOIN dtb_munici mue ON mue.Codigo=en.ECodMunici'
      'LEFT JOIN dtb_munici mup ON mup.Codigo=en.PCodMunici'
      'LEFT JOIN dtb_munici muc ON muc.Codigo=en.CCodMunici'
      'LEFT JOIN dtb_munici mul ON mul.Codigo=en.LCodMunici'
      'WHERE en.Codigo>0'
      
        'AND (Cliente1 = "V" OR Fornece1 = "V" OR Fornece2 = "V" OR Forne' +
        'ce3 = "V")'
      'ORDER BY IF (en.Tipo=0, en.RazaoSocial, en.Nome)')
    Left = 552
    Top = 12
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Required = True
      Size = 100
    end
    object QrEntidadesCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntidadesIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEntidadesRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEntidadesCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrEntidadesBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrEntidadesPTe1: TWideStringField
      FieldName = 'PTe1'
      Origin = 'entidades.PTe1'
    end
    object QrEntidadesETe1: TWideStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrEntidadesNUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUM_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesPTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesRUA_NUM_COMPL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'RUA_NUM_COMPL'
      Size = 255
      Calculated = True
    end
    object QrEntidadesBAIRRO_CIDADE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'BAIRRO_CIDADE'
      Size = 255
      Calculated = True
    end
    object QrEntidadesCEP_UF_PAIS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_UF_PAIS'
      Size = 255
      Calculated = True
    end
    object QrEntidadesNOMEFANTASIA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEFANTASIA'
      Required = True
      Size = 60
    end
    object QrEntidadesLimiCred: TFloatField
      FieldName = 'LimiCred'
      Origin = 'entidades.LimiCred'
    end
    object QrEntidadesCODIGO_NOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CODIGO_NOME'
      Size = 200
      Calculated = True
    end
    object QrEntidadesNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEntidadesDTB_MUNICI: TWideStringField
      FieldName = 'DTB_MUNICI'
      Size = 100
    end
    object QrEntidadesBACEN_PAIS: TWideStringField
      FieldName = 'BACEN_PAIS'
      Size = 100
    end
    object QrEntidadesCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEntidadesUF: TFloatField
      FieldName = 'UF'
    end
    object QrEntidadesNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntidadesENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEntidadesPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEntidadesTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrEntidadesIDADE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IDADE_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 580
    Top = 12
  end
  object QrEntiCtas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.*, ban.Nome NOMEBANCO'
      'FROM entictas cta'
      'LEFT JOIN entidades ent ON ent.Codigo=cta.Codigo'
      'LEFT JOIN bancos ban ON ban.Codigo=cta.Banco'
      'WHERE cta.Codigo=:P0'
      'ORDER BY cta.Ordem')
    Left = 608
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCtasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entictas.Codigo'
      Required = True
    end
    object QrEntiCtasControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'entictas.Controle'
      Required = True
    end
    object QrEntiCtasOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'entictas.Ordem'
    end
    object QrEntiCtasBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'entictas.Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrEntiCtasAgencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'entictas.Agencia'
      Required = True
    end
    object QrEntiCtasContaCor: TWideStringField
      FieldName = 'ContaCor'
      Origin = 'entictas.ContaCor'
      Size = 10
    end
    object QrEntiCtasContaTip: TWideStringField
      FieldName = 'ContaTip'
      Origin = 'entictas.ContaTip'
      Size = 10
    end
    object QrEntiCtasDAC_A: TWideStringField
      FieldName = 'DAC_A'
      Origin = 'entictas.DAC_A'
      Size = 1
    end
    object QrEntiCtasDAC_C: TWideStringField
      FieldName = 'DAC_C'
      Origin = 'entictas.DAC_C'
      Size = 1
    end
    object QrEntiCtasDAC_AC: TWideStringField
      FieldName = 'DAC_AC'
      Origin = 'entictas.DAC_AC'
      Size = 1
    end
    object QrEntiCtasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'entictas.Lk'
    end
    object QrEntiCtasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'entictas.DataCad'
    end
    object QrEntiCtasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'entictas.DataAlt'
    end
    object QrEntiCtasUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'entictas.UserCad'
    end
    object QrEntiCtasUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'entictas.UserAlt'
    end
    object QrEntiCtasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'entictas.AlterWeb'
      Required = True
    end
    object QrEntiCtasAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'entictas.Ativo'
      Required = True
    end
    object QrEntiCtasNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'bancos.Nome'
      Size = 100
    end
  end
  object DsEntiCtas: TDataSource
    DataSet = QrEntiCtas
    Left = 636
    Top = 12
  end
  object QrEntiContat: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntiContatBeforeClose
    AfterScroll = QrEntiContatAfterScroll
    OnCalcFields = QrEntiContatCalcFields
    SQL.Strings = (
      'SELECT eco.Controle, eco.Nome, eco.Cargo,'
      'eca.Nome NOME_CARGO, eco.DtaNatal,'
      
        'IF(eco.DtaNatal = 0, "", DATE_FORMAT(eco.DtaNatal, "%d/%m")) DTA' +
        'NATAL_TXT'
      'FROM enticontat eco'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo'
      'WHERE eco.Codigo=:P0')
    Left = 664
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'enticontat.Controle'
      Required = True
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'enticontat.Nome'
      Required = True
      Size = 30
    end
    object QrEntiContatCargo: TIntegerField
      FieldName = 'Cargo'
      Origin = 'enticontat.Cargo'
      Required = True
    end
    object QrEntiContatNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Origin = 'enticargos.Nome'
      Size = 30
    end
    object QrEntiContatDtaNatal: TDateField
      FieldName = 'DtaNatal'
    end
    object QrEntiContatDESCRI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DESCRI_TXT'
      Size = 255
      Calculated = True
    end
    object QrEntiContatNOMECARGO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECARGO_TXT'
      Size = 150
      Calculated = True
    end
    object QrEntiContatDTANATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTANATAL_TXT'
      Size = 10
      Calculated = True
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 692
    Top = 12
  end
  object QrEntiTel: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiTelCalcFields
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, emt.Conta, '
      'emt.Telefone, emt.EntiTipCto, emt.Ramal'
      'FROM entitel emt'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto'#13
      'WHERE emt.Controle=:P0')
    Left = 720
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTelNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Origin = 'entitipcto.Nome'
      Size = 30
    end
    object QrEntiTelConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'entitel.Conta'
      Required = True
    end
    object QrEntiTelTelefone: TWideStringField
      FieldName = 'Telefone'
      Origin = 'entitel.Telefone'
    end
    object QrEntiTelEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'entitel.EntiTipCto'
      Required = True
    end
    object QrEntiTelTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiTelRamal: TWideStringField
      FieldName = 'Ramal'
      Origin = 'entitel.Ramal'
    end
  end
  object DsEntiTel: TDataSource
    DataSet = QrEntiTel
    Left = 748
    Top = 12
  end
  object QrEntiMail: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, ema.Conta, '
      'ema.EMail, ema.EntiTipCto, ema.Ordem'
      'FROM entimail ema'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto'#13
      'WHERE ema.Controle=:P0'
      'ORDER BY ema.Ordem, ema.Conta'#10)
    Left = 776
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiMailNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrEntiMailConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Required = True
      Size = 255
    end
    object QrEntiMailEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
    object QrEntiMailOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsEntiMail: TDataSource
    DataSet = QrEntiMail
    Left = 804
    Top = 12
  end
  object PMImprime: TPopupMenu
    Left = 156
    Top = 520
    object Endereos1: TMenuItem
      Caption = '&Endere'#231'os'
      OnClick = Endereos1Click
    end
    object ListaTelefnica1: TMenuItem
      Caption = '&Lista Telef'#244'nica'
      OnClick = ListaTelefnica1Click
    end
    object EMails1: TMenuItem
      Caption = 'E-&Mails'
      OnClick = EMails1Click
    end
    object DadosBancrios1: TMenuItem
      Caption = '&Dados Banc'#225'rios'
      OnClick = DadosBancrios1Click
    end
    object ListadeAniversrios1: TMenuItem
      Caption = 'Lista de &Anivers'#225'rios'
      OnClick = ListadeAniversrios1Click
    end
  end
  object frxENT_GEREN_016_001: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.595331481500000000
    ReportOptions.LastChange = 40617.449667662000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxENT_GEREN_016_001GetValue
    Left = 272
    Top = 432
    Datasets = <
      item
        DataSet = frsDsEntiContat
        DataSetName = 'frsDsEntiContat'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEntiTel
        DataSetName = 'frxDsEntiTel'
      end
      item
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 2
      ColumnWidth = 90.000000000000000000
      ColumnPositions.Strings = (
        '0'
        '90')
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 124.724490000000000000
        Width = 340.157700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Width = 321.259842520000000000
          Height = 13.228346460000000000
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              '[frxDsLista."Codigo"] - [frxDsLista."NOMEENTIDADE"] [frxDsLista.' +
              '"ETE1_TXT"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 162.519790000000000000
        Width = 340.157700000000000000
        DataSet = frsDsEntiContat
        DataSetName = 'frsDsEntiContat'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 0.000048820000000000
          Width = 321.259927950000000000
          Height = 13.228346460000000000
          DataField = 'NOMECARGO_TXT'
          DataSet = frsDsEntiContat
          DataSetName = 'frsDsEntiContat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsItalic]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frsDsEntiContat."NOMECARGO_TXT"]')
          ParentFont = False
        end
      end
      object SubdetailData1: TfrxSubdetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 200.315090000000000000
        Width = 340.157700000000000000
        Columns = 2
        ColumnWidth = 170.078740157480300000
        DataSet = frxDsEntiTel
        DataSetName = 'frxDsEntiTel'
        RowCount = 0
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Width = 64.251887950000000000
          Height = 13.228346460000000000
          DataField = 'NOMEETC'
          DataSet = frxDsEntiTel
          DataSetName = 'frxDsEntiTel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiTel."NOMEETC"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252107640000000000
          Width = 64.251887950000000000
          Height = 13.228346460000000000
          DataField = 'TEL_TXT'
          DataSet = frxDsEntiTel
          DataSetName = 'frxDsEntiTel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiTel."TEL_TXT"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504117640000000000
          Width = 41.574707950000000000
          Height = 13.228346460000000000
          DataField = 'Ramal'
          DataSet = frxDsEntiTel
          DataSetName = 'frxDsEntiTel'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiTel."Ramal"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsLista: TfrxDBDataset
    UserName = 'frxDsLista'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NOMEENTIDADE=NOMEENTIDADE'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'RUA=RUA'
      'Compl=Compl'
      'Bairro=Bairro'
      'PTe1=PTe1'
      'ETe1=ETe1'
      'NUM_TXT=NUM_TXT'
      'ETE1_TXT=ETE1_TXT'
      'CNPJ_CPF_TXT=CNPJ_CPF_TXT'
      'PTE1_TXT=PTE1_TXT'
      'CEP_TXT=CEP_TXT'
      'NOMEUF=NOMEUF'
      'RUA_NUM_COMPL=RUA_NUM_COMPL'
      'BAIRRO_CIDADE=BAIRRO_CIDADE'
      'CEP_UF_PAIS=CEP_UF_PAIS'
      'NOMEFANTASIA=NOMEFANTASIA'
      'LimiCred=LimiCred'
      'CODIGO_NOME=CODIGO_NOME'
      'Numero=Numero'
      'DTB_MUNICI=DTB_MUNICI'
      'BACEN_PAIS=BACEN_PAIS'
      'CEP=CEP'
      'UF=UF'
      'NATAL_TXT=NATAL_TXT'
      'ENatal=ENatal'
      'PNatal=PNatal'
      'Tipo=Tipo'
      'IDADE_TXT=IDADE_TXT')
    DataSet = QrEntidades
    BCDToCurrency = False
    
    Left = 300
    Top = 432
  end
  object frsDsEntiContat: TfrxDBDataset
    UserName = 'frsDsEntiContat'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'Nome=Nome'
      'Cargo=Cargo'
      'NOME_CARGO=NOME_CARGO'
      'DtaNatal=DtaNatal'
      'DTANATAL_TXT=DTANATAL_TXT'
      'DESCRI_TXT=DESCRI_TXT'
      'NOMECARGO_TXT=NOMECARGO_TXT')
    DataSet = QrEntiContat
    BCDToCurrency = False
    
    Left = 328
    Top = 432
  end
  object frxDsEntiTel: TfrxDBDataset
    UserName = 'frxDsEntiTel'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEETC=NOMEETC'
      'Conta=Conta'
      'Telefone=Telefone'
      'EntiTipCto=EntiTipCto'
      'TEL_TXT=TEL_TXT'
      'Ramal=Ramal')
    DataSet = QrEntiTel
    BCDToCurrency = False
    
    Left = 356
    Top = 432
  end
  object frxENT_GEREN_016_002: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.595331481500000000
    ReportOptions.LastChange = 40617.701239976900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxENT_GEREN_016_002GetValue
    Left = 244
    Top = 432
    Datasets = <
      item
        DataSet = frsDsEntiContat
        DataSetName = 'frsDsEntiContat'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEntiMail
        DataSetName = 'frxDsEntiMail'
      end
      item
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 2
      ColumnWidth = 90.000000000000000000
      ColumnPositions.Strings = (
        '0'
        '90')
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 124.724490000000000000
        Width = 340.157700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Width = 321.259842520000000000
          Height = 13.228346460000000000
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."Codigo"] - [frxDsLista."NOMEENTIDADE"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 162.519790000000000000
        Width = 340.157700000000000000
        DataSet = frsDsEntiContat
        DataSetName = 'frsDsEntiContat'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 0.000048820000000000
          Width = 321.259927950000000000
          Height = 13.228346460000000000
          DataField = 'NOMECARGO_TXT'
          DataSet = frsDsEntiContat
          DataSetName = 'frsDsEntiContat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsItalic]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frsDsEntiContat."NOMECARGO_TXT"]')
          ParentFont = False
        end
      end
      object SubdetailData1: TfrxSubdetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 200.315090000000000000
        Width = 340.157700000000000000
        DataSet = frxDsEntiMail
        DataSetName = 'frxDsEntiMail'
        RowCount = 0
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Width = 94.488188980000000000
          Height = 13.228346460000000000
          DataField = 'NOMEETC'
          DataSet = frxDsEntiMail
          DataSetName = 'frxDsEntiMail'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiMail."NOMEETC"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488347640000000000
          Width = 226.771677950000000000
          Height = 13.228346460000000000
          DataField = 'EMail'
          DataSet = frxDsEntiMail
          DataSetName = 'frxDsEntiMail'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiMail."EMail"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEntiMail: TfrxDBDataset
    UserName = 'frxDsEntiMail'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEETC=NOMEETC'
      'Conta=Conta'
      'EMail=EMail'
      'EntiTipCto=EntiTipCto'
      'Ordem=Ordem')
    DataSet = QrEntiMail
    BCDToCurrency = False
    
    Left = 384
    Top = 432
  end
  object frxDsEntiCtas: TfrxDBDataset
    UserName = 'frxDsEntiCtas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Ordem=Ordem'
      'Banco=Banco'
      'Agencia=Agencia'
      'ContaCor=ContaCor'
      'ContaTip=ContaTip'
      'DAC_A=DAC_A'
      'DAC_C=DAC_C'
      'DAC_AC=DAC_AC'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NOMEBANCO=NOMEBANCO')
    DataSet = QrEntiCtas
    BCDToCurrency = False
    
    Left = 412
    Top = 432
  end
  object frxENT_GEREN_016_003: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.595331481500000000
    ReportOptions.LastChange = 41158.664422812500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxENT_GEREN_016_003GetValue
    Left = 216
    Top = 432
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEntiCtas
        DataSetName = 'frxDsEntiCtas'
      end
      item
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
        RowCount = 0
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Width = 26.456692913385800000
          Height = 13.228346460000000000
          DataField = 'Codigo'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLista."Codigo"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Width = 143.622037480000000000
          Height = 13.228346460000000000
          DataField = 'NOMEENTIDADE'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."NOMEENTIDADE"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078705980000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          DataField = 'NOMEFANTASIA'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."NOMEFANTASIA"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464781730000000000
          Width = 132.283476770000000000
          Height = 13.228346460000000000
          DataField = 'RUA_NUM_COMPL'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."RUA_NUM_COMPL"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748331730000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          DataField = 'BAIRRO_CIDADE'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."BAIRRO_CIDADE"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134231730000000000
          Width = 83.149574570000000000
          Height = 13.228346460000000000
          DataField = 'CEP_UF_PAIS'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."CEP_UF_PAIS"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 68.031496062992100000
          Height = 13.228346460000000000
          DataField = 'ETE1_TXT'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."ETE1_TXT"]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 86.929190000000000000
        Width = 680.315400000000000000
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456678270000000000
          Width = 143.622047240000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome / Raz'#227'o Social')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Width = 26.456692913385800000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078674250000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Apelido / Fantasia')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Width = 328.818902520000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Endere'#231'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 68.031496062992100000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Telefone')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEntiCtas
        DataSetName = 'frxDsEntiCtas'
        RowCount = 0
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Width = 177.637787950000000000
          Height = 13.228346456692900000
          DataSet = frsDsEntiContat
          DataSetName = 'frsDsEntiContat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiCtas."Banco"] [frxDsEntiCtas."NOMEBANCO"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 313.701038820000000000
          Width = 45.354330710000000000
          Height = 13.228346456692900000
          DataField = 'Agencia'
          DataSet = frxDsEntiCtas
          DataSetName = 'frxDsEntiCtas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiCtas."Agencia"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 30.236117950000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Banco:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Width = 30.236220470000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ag'#234'ncia:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953048820000000000
          Width = 18.897637800000000000
          Height = 13.228346456692900000
          DataField = 'DAC_A'
          DataSet = frxDsEntiCtas
          DataSetName = 'frxDsEntiCtas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiCtas."DAC_A"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 18.897637795275600000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '(A):')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086938820000000000
          Width = 45.354330710000000000
          Height = 13.228346456692900000
          DataField = 'ContaCor'
          DataSet = frxDsEntiCtas
          DataSetName = 'frxDsEntiCtas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiCtas."ContaCor"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 30.236220472440900000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338948820000000000
          Width = 18.897637800000000000
          Height = 13.228346456692900000
          DataField = 'DAC_C'
          DataSet = frxDsEntiCtas
          DataSetName = 'frxDsEntiCtas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiCtas."DAC_C"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 18.897637800000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '(C):')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134248820000000000
          Width = 37.795275590551200000
          Height = 13.228346456692900000
          DataField = 'DAC_AC'
          DataSet = frxDsEntiCtas
          DataSetName = 'frxDsEntiCtas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiCtas."DAC_AC"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 18.897637800000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '(AC):')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165788820000000000
          Width = 83.149630710000000000
          Height = 13.228346456692900000
          DataField = 'ContaTip'
          DataSet = frxDsEntiCtas
          DataSetName = 'frxDsEntiCtas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntiCtas."ContaTip"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 30.236220470000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo:')
          ParentFont = False
        end
      end
    end
  end
  object frxENT_GEREN_016_004: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.595331481500000000
    ReportOptions.LastChange = 41158.664228194440000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxENT_GEREN_016_004GetValue
    Left = 188
    Top = 432
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEntiCtas
        DataSetName = 'frxDsEntiCtas'
      end
      item
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 994.015748030000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 797.480830000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 162.519790000000000000
        Width = 1009.134510000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456678270000000000
          Width = 200.314967950000000000
          Height = 13.228346460000000000
          DataField = 'NOMEENTIDADE'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."NOMEENTIDADE"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Width = 26.456685590000000000
          Height = 13.228346460000000000
          DataField = 'Codigo'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."Codigo"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Width = 185.196850390000000000
          Height = 13.228346460000000000
          DataField = 'RUA'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."RUA"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'NUM_TXT'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."NUM_TXT"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Width = 120.944881890000000000
          Height = 13.228346460000000000
          DataField = 'Compl'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."Compl"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 154.960629920000000000
          Height = 13.228346460000000000
          DataField = 'Bairro'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."Bairro"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Width = 124.724409450000000000
          Height = 13.228346460000000000
          DataField = 'DTB_MUNICI'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."DTB_MUNICI"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Width = 22.677165350000000000
          Height = 13.228346460000000000
          DataField = 'NOMEUF'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."NOMEUF"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 925.984850000000000000
          Width = 83.149625830000000000
          Height = 13.228346456692900000
          DataField = 'BACEN_PAIS'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."BACEN_PAIS"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'CEP_TXT'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLista."CEP_TXT"]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 86.929190000000000000
        Width = 1009.134510000000000000
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456678270000000000
          Width = 200.314967950000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Width = 26.456692910000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Width = 185.196850390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Logradouro')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Width = 120.944881890000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Complemento')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 154.960629920000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Bairro')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 778.583180000000000000
          Width = 124.724409450000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cidade')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Width = 22.677165350000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 925.984850000000000000
          Width = 83.149625830000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pa'#237's')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 238.110390000000000000
        Width = 1009.134510000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxENT_GEREN_016_005: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.595331481500000000
    ReportOptions.LastChange = 41198.471108425900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxENT_GEREN_016_005GetValue
    Left = 160
    Top = 432
    Datasets = <
      item
        DataSet = frsDsEntiContat
        DataSetName = 'frsDsEntiContat'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEntiMail
        DataSetName = 'frxDsEntiMail'
      end
      item
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 2
      ColumnWidth = 90.000000000000000000
      ColumnPositions.Strings = (
        '0'
        '90')
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 162.519790000000000000
        Width = 340.157700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Width = 245.669242520000000000
          Height = 13.228346460000000000
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              '[frxDsLista."Codigo"] - [frxDsLista."NOMEENTIDADE"] [frxDsLista.' +
              '"IDADE_TXT"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 75.590392520000000000
          Height = 13.228346460000000000
          DataField = 'NATAL_TXT'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLista."NATAL_TXT"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 200.315090000000000000
        Width = 340.157700000000000000
        DataSet = frsDsEntiContat
        DataSetName = 'frsDsEntiContat'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 0.000048820000000000
          Width = 245.669327950000000000
          Height = 13.228346460000000000
          DataSet = frsDsEntiContat
          DataSetName = 'frsDsEntiContat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsItalic]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frsDsEntiContat."DESCRI_TXT"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 75.590477950000000000
          Height = 13.228346460000000000
          DataField = 'DTANATAL_TXT'
          DataSet = frsDsEntiContat
          DataSetName = 'frsDsEntiContat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsItalic]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frsDsEntiContat."DTANATAL_TXT"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 124.724490000000000000
        Width = 340.157700000000000000
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Width = 245.669242520000000000
          Height = 13.228346460000000000
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 75.590392520000000000
          Height = 13.228346460000000000
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Anivers'#225'rio')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 238.110390000000000000
        Width = 340.157700000000000000
      end
    end
  end
end
