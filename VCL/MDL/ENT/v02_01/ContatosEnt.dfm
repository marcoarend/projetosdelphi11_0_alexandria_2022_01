object FmContatosEnt: TFmContatosEnt
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-027 :: Gerenciamento de Contatos de Entidades'
  ClientHeight = 629
  ClientWidth = 1014
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1014
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 966
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 918
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 508
        Height = 32
        Caption = 'Gerenciamento de Contatos de Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 508
        Height = 32
        Caption = 'Gerenciamento de Contatos de Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 508
        Height = 32
        Caption = 'Gerenciamento de Contatos de Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1014
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1014
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1014
        Height = 467
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 427
          Height = 450
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 427
            Height = 41
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object LaPesq: TLabel
              Left = 8
              Top = 0
              Width = 46
              Height = 13
              Caption = 'Pesquisa:'
            end
            object Label2: TLabel
              Left = 340
              Top = 0
              Width = 14
              Height = 13
              Caption = 'ID:'
            end
            object EdPesq: TEdit
              Left = 8
              Top = 16
              Width = 209
              Height = 21
              TabOrder = 0
              OnChange = EdPesqChange
            end
            object CkMaskManual: TCheckBox
              Left = 220
              Top = 20
              Width = 105
              Height = 17
              Caption = 'M'#225'scara manual.'
              TabOrder = 1
              OnClick = CkMaskManualClick
            end
            object EdControle: TdmkEdit
              Left = 340
              Top = 16
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdControleChange
            end
          end
          object DBGEntiContat: TdmkDBGrid
            Left = 0
            Top = 41
            Width = 427
            Height = 409
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SEXO_TXT'
                Title.Caption = 'SX'
                Width = 22
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTANATAL_TXT'
                Title.Caption = 'Natal'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DiarioAdd'
                Title.Caption = '1'#186' di'#225'rio'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsEntiContat
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SEXO_TXT'
                Title.Caption = 'SX'
                Width = 22
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTANATAL_TXT'
                Title.Caption = 'Natal'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DiarioAdd'
                Title.Caption = '1'#186' di'#225'rio'
                Visible = True
              end>
          end
        end
        object Panel6: TPanel
          Left = 429
          Top = 15
          Width = 583
          Height = 450
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Splitter1: TSplitter
            Left = 0
            Top = 241
            Width = 583
            Height = 5
            Cursor = crVSplit
            Align = alTop
          end
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 583
            Height = 241
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object DBGEntidades: TDBGrid
              Left = 0
              Top = 0
              Width = 583
              Height = 241
              Align = alClient
              DataSource = DsEntidades
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDrawColumnCell = DBGEntidadesDrawColumnCell
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEENTIDADE'
                  Title.Caption = '[R]az'#227'o Social, [N]ome, [F]antasia e [A]pelido'
                  Visible = True
                end>
            end
          end
          object Panel10: TPanel
            Left = 0
            Top = 246
            Width = 583
            Height = 204
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel10'
            TabOrder = 1
            object Splitter2: TSplitter
              Left = 280
              Top = 0
              Width = 5
              Height = 204
            end
            object DBGEntiMail: TDBGrid
              Left = 285
              Top = 0
              Width = 298
              Height = 204
              Align = alClient
              DataSource = DsEntiMail
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ordem'
                  Width = 39
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEETC'
                  Title.Caption = 'Tipo de e-mail'
                  Width = 84
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EMail'
                  Title.Caption = 'E-mail'
                  Width = 496
                  Visible = True
                end>
            end
            object DBGEntiTel: TDBGrid
              Left = 0
              Top = 0
              Width = 280
              Height = 204
              Align = alLeft
              DataSource = DsEntiTel
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMEETC'
                  Title.Caption = 'Tipo de telefone'
                  Width = 84
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TEL_TXT'
                  Title.Caption = 'Telefone'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ramal'
                  Width = 37
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1014
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1010
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1014
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 868
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 866
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtContato: TBitBtn
        Tag = 10092
        Left = 12
        Top = 8
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Contato'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtContatoClick
      end
      object BtTelefones: TBitBtn
        Tag = 92
        Left = 266
        Top = 8
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Telefones'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtTelefonesClick
      end
      object BtEMails: TBitBtn
        Tag = 66
        Left = 396
        Top = 8
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'E&Mails'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtEMailsClick
      end
      object BtEntidade: TBitBtn
        Tag = 10062
        Left = 140
        Top = 8
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Entidade'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtEntidadeClick
      end
    end
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntiContatAfterOpen
    BeforeClose = QrEntiContatBeforeClose
    AfterScroll = QrEntiContatAfterScroll
    SQL.Strings = (
      'SELECT eco.Codigo, eco.Controle, eco.Nome, eco.Cargo,'
      'eco.DiarioAdd, eca.Nome NOME_CARGO, eco.DtaNatal, eco.Sexo,'
      'ELT(eco.Sexo+1, "NI", "M", "F", "?") SEXO_TXT,'
      
        'IF(eco.DtaNatal = 0, "", DATE_FORMAT(eco.DtaNatal, "%d/%m/%y")) ' +
        'DTANATAL_TXT'
      'FROM enticontat eco'
      'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo'
      'WHERE eco.Nome LIKE "%RENAT%"')
    Left = 152
    Top = 120
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrEntiContatDtaNatal: TDateField
      FieldName = 'DtaNatal'
    end
    object QrEntiContatSexo: TSmallintField
      FieldName = 'Sexo'
    end
    object QrEntiContatSEXO_TXT: TWideStringField
      FieldName = 'SEXO_TXT'
      Size = 2
    end
    object QrEntiContatDTANATAL_TXT: TWideStringField
      FieldName = 'DTANATAL_TXT'
      Size = 10
    end
    object QrEntiContatDiarioAdd: TIntegerField
      FieldName = 'DiarioAdd'
    end
    object QrEntiContatCargo: TIntegerField
      FieldName = 'Cargo'
    end
    object QrEntiContatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiContatNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 180
    Top = 120
  end
  object QrEntiMail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, ema.Conta, '
      'ema.EMail, ema.EntiTipCto, ema.Ordem'
      'FROM entimail ema'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto'#13
      'WHERE ema.Controle=:P0'
      'ORDER BY ema.Ordem, ema.Conta'#10)
    Left = 208
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiMailNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrEntiMailConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Required = True
      Size = 255
    end
    object QrEntiMailEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
    object QrEntiMailOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsEntiMail: TDataSource
    DataSet = QrEntiMail
    Left = 236
    Top = 148
  end
  object QrEntiTel: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiTelCalcFields
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, emt.Conta, '
      'emt.Telefone, emt.EntiTipCto, emt.Ramal'
      'FROM entitel emt'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto'#13
      'WHERE emt.Controle=:P0')
    Left = 264
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTelNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Origin = 'entitipcto.Nome'
      Size = 30
    end
    object QrEntiTelConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'entitel.Conta'
      Required = True
    end
    object QrEntiTelTelefone: TWideStringField
      FieldName = 'Telefone'
      Origin = 'entitel.Telefone'
    end
    object QrEntiTelEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'entitel.EntiTipCto'
      Required = True
    end
    object QrEntiTelTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiTelRamal: TWideStringField
      FieldName = 'Ramal'
      Origin = 'entitel.Ramal'
    end
  end
  object DsEntiTel: TDataSource
    DataSet = QrEntiTel
    Left = 292
    Top = 148
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    AutoCalcFields = False
    OnCalcFields = QrEntidadesCalcFields
    ParamCheck = False
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, CONCAT('
      
        'CASE WHEN ent.RazaoSocial<>"" THEN CONCAT(" [R] ", ent.RazaoSoci' +
        'al) ELSE "" END, '
      
        'CASE WHEN ent.Nome<>"" THEN CONCAT(" [N] ", ent.Nome) ELSE "" EN' +
        'D, '
      
        'CASE WHEN ent.Fantasia<>"" THEN CONCAT(" [F] ", ent.Fantasia) EL' +
        'SE "" END, '
      
        'CASE WHEN ent.Apelido<>"" THEN CONCAT(" [A] ", ent.Apelido) ELSE' +
        ' "" END'
      ') NO_ENT FROM entidades ent'
      'LEFT JOIN enticonent ece ON ece.Codigo=ent.Codigo'
      'WHERE ece.Controle=1200'
      'ORDER BY NOMEENTIDADE'
      '')
    Left = 152
    Top = 148
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEENTIDADE'
      Size = 255
      Calculated = True
    end
    object QrEntidadesRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 100
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEntidadesFantasia: TWideStringField
      FieldName = 'Fantasia'
      Size = 60
    end
    object QrEntidadesApelido: TWideStringField
      FieldName = 'Apelido'
      Size = 60
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 180
    Top = 148
  end
  object PMContatos: TPopupMenu
    OnPopup = PMContatosPopup
    Left = 120
    Top = 424
    object IncluiContato1: TMenuItem
      Caption = '&Inclui novo contato'
      object porIncluso1: TMenuItem
        Caption = 'por &Inclus'#227'o'
        OnClick = porIncluso1Click
      end
    end
    object AlteraContato1: TMenuItem
      Caption = '&Altera Contato atual'
      OnClick = AlteraContato1Click
    end
    object ExcluiContato1: TMenuItem
      Caption = '&Exclui contato(s)'
      OnClick = ExcluiContato1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Mesclacontatos1: TMenuItem
      Caption = '&Mesclar este contato com outro'
      Enabled = False
      OnClick = Mesclacontatos1Click
    end
  end
  object PMTelefones: TPopupMenu
    OnPopup = PMTelefonesPopup
    Left = 176
    Top = 424
    object IncluiTelefone1: TMenuItem
      Caption = '&Inclui novo telefone'
      OnClick = IncluiTelefone1Click
    end
    object AlteraTelefone1: TMenuItem
      Caption = '&Altera telefone atual'
      OnClick = AlteraTelefone1Click
    end
    object ExcluiTelefone1: TMenuItem
      Caption = '&Exclui telefone(s)'
      OnClick = ExcluiTelefone1Click
    end
  end
  object PMEMails: TPopupMenu
    OnPopup = PMEMailsPopup
    Left = 204
    Top = 424
    object IncluiEmail1: TMenuItem
      Caption = '&Novo e-mail'
      OnClick = IncluiEmail1Click
    end
    object AlteraEmail1: TMenuItem
      Caption = '&Altera e-mail selecionado'
      OnClick = AlteraEmail1Click
    end
    object ExcluiEmail1: TMenuItem
      Caption = '&Exclui e-mail selecionado'
      OnClick = ExcluiEmail1Click
    end
  end
  object PMEntidades: TPopupMenu
    OnPopup = PMEntidadesPopup
    Left = 148
    Top = 424
    object IncluiEntidade1: TMenuItem
      Caption = '&Adiciona Entidade'
      OnClick = IncluiEntidade1Click
    end
    object ExcluiEntidade1: TMenuItem
      Caption = '&Retira Entidade'
      OnClick = ExcluiEntidade1Click
    end
  end
  object QrPsq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens'
      'FROM enticonent'
      'WHERE Codigo=1'
      'AND Controle=1')
    Left = 212
    Top = 232
    object QrPsqItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
end
