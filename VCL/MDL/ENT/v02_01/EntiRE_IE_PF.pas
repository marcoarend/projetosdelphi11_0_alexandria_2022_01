unit EntiRE_IE_PF;

interface

uses Vcl.OleCtrls, Vcl.ComCtrls, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Graphics,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, MSHTML,
  WBFuncs, SHDocVw, IdHTTP, IdMultipartFormData, IdSSLOpenSSL, UnDmkEnums,
  System.NetEncoding, ActiveX;

type
  TUnEntiRE_IE_PF = class(TObject)
  private
    { Private declarations }
    function  GetBrowserForFrame(Doc: IHTMLDocument2; nFrame: Integer): IWebBrowser2;
    function  GetFrameSource(WebDoc: iHTMLDocument2): string;
    procedure WB_SaveFrameToFile(HTMLDocument: IHTMLDocument2; const FileName: TFileName);

  public
    { Public declarations }
    function SaveWBFrames(WebBrowser1: TWebBrowser): string;
  end;

var
  UnEntiRE_IE_PF: TUnEntiRE_IE_PF;

implementation

uses dmkGeral;

{ TUnEntiRE_IE_PF }

function TUnEntiRE_IE_PF.GetBrowserForFrame(Doc: IHTMLDocument2;
  nFrame: Integer): IWebBrowser2;
  //Thanks to Rik Barker
  //returns an interface to the frame's browser
var
  pContainer: IOLEContainer;
  enumerator: ActiveX.IEnumUnknown;
  nFetched: PLongInt;
  unkFrame: IUnknown;
  hr: HRESULT;
begin
  Result := nil;
  nFetched := nil;
  // Cast the page as an OLE container
  pContainer := Doc as IOleContainer;
  // Get an enumerator for the frames on the page
  hr := pContainer.EnumObjects(OLECONTF_EMBEDDINGS or OLECONTF_OTHERS, enumerator);
  if hr <> S_OK then
  begin
    pContainer._Release;
    Exit;
  end;
  // Now skip to the frame we're interested in
  enumerator.Skip(nFrame);
  // and get the frame as IUnknown
  enumerator.Next(1,unkFrame, nFetched);
  // Now QI the frame for a WebBrowser Interface - I'm not  entirely
  // sure this is necessary, but COM never ceases to surprise me
  unkframe.QueryInterface(IID_IWebBrowser2, Result);
end;

function TUnEntiRE_IE_PF.GetFrameSource(WebDoc: iHTMLDocument2): string;
  //returns frame HTML and scripts as a text string
var
  re: integer;
  HTMLel: iHTMLElement;
  HTMLcol: iHTMLElementCollection;
  HTMLlen: Integer;
  //ScriptEL: IHTMLScriptElement;
begin
  Result := '';
  if Assigned(WebDoc) then
  begin
    HTMLcol := WebDoc.Get_all;
    HTMLlen := HTMLcol.Length;
    for re := 0 to HTMLlen - 1 do
    begin
      HTMLel := HTMLcol.Item(re, 0) as iHTMLElement;
      if HTMLEl.tagName = 'HTML' then
        Result := Result + HTMLEl.outerHTML;
    end;
  end;
end;

function TUnEntiRE_IE_PF.SaveWBFrames(WebBrowser1: TWebBrowser): string;
// return the source for all frames in the browser
var
  Webdoc, HTMLDoc: ihtmldocument2;
  framesCol: iHTMLFramesCollection2;
  FramesLen: integer;
  pickFrame: olevariant;
  p: integer;
begin
  try
    WebDoc := WebBrowser1.Document as IHTMLDocument2;
    Result := GetFrameSource(WebDoc);

    // ��� Hier kann Result in eine Datei gespeichert werden ����  oder  mit
    WB_SaveFrameToFile(WebDoc,'c:\MainPage.html');

    //Handle multiple or single frames
    FramesCol := WebDoc.Get_frames;
    FramesLen := FramesCol.Get_length;
    if FramesLen > 0 then
      for p := 0 to FramesLen - 1 do
      begin
        pickframe := p;
        HTMLDoc   := WebBrowser1.Document as iHTMLDocument2;

        WebDoc := GetBrowserForFrame(HTMLDoc, pickframe).document as iHTMLDocument2;
        if WebDoc <> nil then
        begin
          Result := GetFrameSource(WebDoc);
          WB_SaveFrameToFile(WebDoc, 'c:\Frame' + IntToStr(p) + '.html');
          // ShowMessage(HTMLDoc.Get_parentWindow.Get_name);
          // ShowMessage(HTMLDoc.Get_parentWindow.Parent.Get_document.nameProp);

        end;
      end;
  except
    Result := 'No Source Available';
  end;
end;

procedure TUnEntiRE_IE_PF.WB_SaveFrameToFile(HTMLDocument: IHTMLDocument2;
  const FileName: TFileName);
var
  PersistFile: IPersistFile;
begin
  PersistFile := HTMLDocument as IPersistFile;
  PersistFile.Save(StringToOleStr(FileName), System.True);
end;

end.
