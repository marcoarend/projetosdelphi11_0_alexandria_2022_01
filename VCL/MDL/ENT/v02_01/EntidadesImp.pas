unit EntidadesImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, (*DBTables,*) DBCtrls, dmkGeral,
  UnInternalConsts, Grids, DBGrids, Menus, ResIntStrings,
  mySQLDbTables, UnGOTOy, frxClass, frxDBSet, Variants, dmkEdit,
  dmkImage, UnDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox;

type
  TFmEntidadesImp = class(TForm)
    DsUF: TDataSource;
    DsEntidades: TDataSource;
    DsCidades: TDataSource;
    DsBairros: TDataSource;
    DsRuas: TDataSource;
    PMImprime: TPopupMenu;
    ListaTelefnica1: TMenuItem;
    Endereos1: TMenuItem;
    DsProfiss: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    RGOrigem: TRadioGroup;
    GroupBox1: TGroupBox;
    CkCliente1: TCheckBox;
    CkCliente2: TCheckBox;
    CkFornece1: TCheckBox;
    CkFornece2: TCheckBox;
    CkFornece3: TCheckBox;
    CkFornece4: TCheckBox;
    CkTerceiro: TCheckBox;
    GBFiltro: TGroupBox;
    Label1: TLabel;
    CBUF: TdmkDBLookupComboBox;
    CBCidade: TdmkDBLookupComboBox;
    EdCEPIni: TdmkEdit;
    CkUF: TCheckBox;
    CkCidade: TCheckBox;
    CkBairro: TCheckBox;
    CkRua: TCheckBox;
    CkCEP: TCheckBox;
    CBBairro: TdmkDBLookupComboBox;
    CBRua: TdmkDBLookupComboBox;
    EdCEPFim: TdmkEdit;
    CkCPF: TCheckBox;
    EdCPF: TdmkEdit;
    EdCNPJ: TdmkEdit;
    CkCNPJ: TCheckBox;
    CkProfiss: TCheckBox;
    CBProfiss: TdmkDBLookupComboBox;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    CkApelido: TCheckBox;
    EdApelido: TdmkEdit;
    CkFantasia: TCheckBox;
    EdFantasia: TdmkEdit;
    QrProfiss: TmySQLQuery;
    QrProfissProfissao: TWideStringField;
    QrEntidades: TmySQLQuery;
    QrUF: TmySQLQuery;
    QrCidades: TmySQLQuery;
    QrCidadesCIDADE: TWideStringField;
    QrBairros: TmySQLQuery;
    QrBairrosBAIRRO: TWideStringField;
    QrRuas: TmySQLQuery;
    QrRuasRua: TWideStringField;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesCNPJ_CPF: TWideStringField;
    QrEntidadesIE_RG: TWideStringField;
    QrEntidadesCONTATO: TWideStringField;
    QrEntidadesRUA: TWideStringField;
    QrEntidadesCompl: TWideStringField;
    QrEntidadesBairro: TWideStringField;
    QrEntidadesCidade: TWideStringField;
    QrEntidadesUF: TFloatField;
    QrEntidadesCEP: TFloatField;
    QrEntidadesPais: TWideStringField;
    QrEntidadesPTe1: TWideStringField;
    QrEntidadesPTe2: TWideStringField;
    QrEntidadesPTe3: TWideStringField;
    QrEntidadesETe1: TWideStringField;
    QrEntidadesETe2: TWideStringField;
    QrEntidadesETe3: TWideStringField;
    QrEntidadesPFax: TWideStringField;
    QrEntidadesEFax: TWideStringField;
    QrEntidadesPCel: TWideStringField;
    QrEntidadesECel: TWideStringField;
    QrEntidadesCTel: TWideStringField;
    QrEntidadesCCel: TWideStringField;
    QrEntidadesCFax: TWideStringField;
    QrEntidadesLTel: TWideStringField;
    QrEntidadesLCel: TWideStringField;
    QrEntidadesLFax: TWideStringField;
    QrEntidadesNUM_TXT: TWideStringField;
    QrEntidadesETE1_TXT: TWideStringField;
    QrEntidadesETE2_TXT: TWideStringField;
    QrEntidadesETE3_TXT: TWideStringField;
    QrEntidadesEFAX_TXT: TWideStringField;
    QrEntidadesECEL_TXT: TWideStringField;
    QrEntidadesCNPJ_CPF_TXT: TWideStringField;
    QrEntidadesPTE1_TXT: TWideStringField;
    QrEntidadesPTE2_TXT: TWideStringField;
    QrEntidadesPTE3_TXT: TWideStringField;
    QrEntidadesPFAX_TXT: TWideStringField;
    QrEntidadesPCEL_TXT: TWideStringField;
    QrEntidadesCCEL_TXT: TWideStringField;
    QrEntidadesCFAX_TXT: TWideStringField;
    QrEntidadesCTEL_TXT: TWideStringField;
    QrEntidadesLTEL_TXT: TWideStringField;
    QrEntidadesLCEL_TXT: TWideStringField;
    QrEntidadesLFAX_TXT: TWideStringField;
    QrEntidadesCEP_TXT: TWideStringField;
    QrEntidadesNOMEUF: TWideStringField;
    QrEntidadesRUA_NUM_COMPL: TWideStringField;
    QrEntidadesBAIRRO_CIDADE: TWideStringField;
    QrEntidadesCEP_UF_PAIS: TWideStringField;
    CkGrupo: TCheckBox;
    CBGrupo: TdmkDBLookupComboBox;
    BtTudo: TBitBtn;
    QrEntiGrupos: TmySQLQuery;
    DsEntiGrupos: TDataSource;
    CkAccount: TCheckBox;
    CBAccount: TdmkDBLookupComboBox;
    QrAccounts: TmySQLQuery;
    DsAccounts: TDataSource;
    QrAccountsCodigo: TIntegerField;
    QrAccountsNOMEENTIDADE: TWideStringField;
    QrEntidadesNOMEFANTASIA: TWideStringField;
    EMails1: TMenuItem;
    QrEntidadesEMail: TWideStringField;
    RGEMail: TRadioGroup;
    LimitedeCrdito1: TMenuItem;
    QrEntidadesLimiCred: TFloatField;
    QrEntidadesCODIGO_NOME: TWideStringField;
    QrUFCodigo: TIntegerField;
    QrUFNome: TWideStringField;
    QrUFLk: TIntegerField;
    QrUFDataCad: TDateField;
    QrUFDataAlt: TDateField;
    QrUFUserCad: TIntegerField;
    QrUFUserAlt: TIntegerField;
    QrUFICMS_C: TFloatField;
    QrUFICMS_V: TFloatField;
    CkPais: TCheckBox;
    CBPais: TdmkDBLookupComboBox;
    QrPais: TmySQLQuery;
    DsPais: TDataSource;
    QrPaisPAIS: TWideStringField;
    frxDsLista: TfrxDBDataset;
    frxLista: TfrxReport;
    frxEndereco: TfrxReport;
    frxDsEndereco: TfrxDBDataset;
    QrEntidadesNumero: TFloatField;
    frxEmail: TfrxReport;
    frxDsEntidades: TfrxDBDataset;
    frxLimiCred: TfrxReport;
    DataSource1: TDataSource;
    EdTelefones: TdmkEdit;
    CkTelefones: TCheckBox;
    CkNome: TCheckBox;
    EdNome: TdmkEdit;
    CkCliInt: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSair: TBitBtn;
    Panel6: TPanel;
    BtLocaliza1: TBitBtn;
    BtImprime: TBitBtn;
    BtSelecionar: TBitBtn;
    GroupBox2: TGroupBox;
    EdLimiCredMax: TdmkEdit;
    Label3: TLabel;
    EdLimiCredMin: TdmkEdit;
    Label2: TLabel;
    CkLimiCred: TCheckBox;
    Modelo11: TMenuItem;
    Modelo21: TMenuItem;
    frxLista2: TfrxReport;
    QrEntidadesTe1: TWideStringField;
    QrEntidadesTe2: TWideStringField;
    QrEntidadesTe3: TWideStringField;
    QrEntidadesCel: TWideStringField;
    QrEntidadesFax: TWideStringField;
    QrEntidadesTE1_TXT: TWideStringField;
    QrEntidadesTE2_TXT: TWideStringField;
    QrEntidadesTE3_TXT: TWideStringField;
    QrEntidadesCEL_TXT: TWideStringField;
    QrEntidadesFAX_TXT: TWideStringField;
    CkAtivo: TCheckBox;
    N1: TMenuItem;
    ExportaparaExcel1: TMenuItem;
    procedure BtSairClick(Sender: TObject);
    procedure BtLocaliza1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGOrigemClick(Sender: TObject);
    procedure CBUFClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CkCEPClick(Sender: TObject);
    procedure EdCEPIniExit(Sender: TObject);
    procedure EdCEPFimExit(Sender: TObject);
    procedure CkUFClick(Sender: TObject);
    procedure CkCidadeClick(Sender: TObject);
    procedure CkBairroClick(Sender: TObject);
    procedure CkRuaClick(Sender: TObject);
    procedure CkCliente1Click(Sender: TObject);
    procedure CkCliente2Click(Sender: TObject);
    procedure CkFornece1Click(Sender: TObject);
    procedure CkFornece2Click(Sender: TObject);
    procedure CkFornece3Click(Sender: TObject);
    procedure CkFornece4Click(Sender: TObject);
    procedure CkTerceiroClick(Sender: TObject);
    procedure Endereos1Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure EdCNPJExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrEntidadesAfterClose(DataSet: TDataSet);
    procedure QrEntidadesAfterOpen(DataSet: TDataSet);
    procedure QrEntidadesCalcFields(DataSet: TDataSet);
    procedure BtTudoClick(Sender: TObject);
    procedure EMails1Click(Sender: TObject);
    procedure RGEMailClick(Sender: TObject);
    procedure CkLimiCredClick(Sender: TObject);
    procedure EdLimiCredMinChange(Sender: TObject);
    procedure EdLimiCredMaxChange(Sender: TObject);
    procedure LimitedeCrdito1Click(Sender: TObject);
    procedure EdTelefonesExit(Sender: TObject);
    procedure frxLimiCredGetValue(const VarName: String;
      var Value: Variant);
    procedure BtSelecionarClick(Sender: TObject);
    procedure Modelo11Click(Sender: TObject);
    procedure Modelo21Click(Sender: TObject);
    procedure ExportaparaExcel1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCidade;
    procedure ReopenBairro;
    procedure ReopenRua;
    procedure ReopenPais;
    procedure NovaPesquisa;
  public
    { Public declarations }
    FCadastrar: Boolean;
    FLiberaSelecionar: Boolean;
  end;

var
  FmEntidadesImp: TFmEntidadesImp;

implementation

uses UnMyObjects, Module, Entidades, UMySQLModule, MyDBCheck, EntiSel,
  DmkDAC_PF (*&, UnDmkABS_PF*);

{$R *.DFM}

procedure TFmEntidadesImp.BtSairClick(Sender: TObject);
begin
  VAR_ENTIDADE := QrEntidadesCodigo.Value;
  Close;
end;

procedure TFmEntidadesImp.BtSelecionarClick(Sender: TObject);
begin
(*&
  if (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0) then
  begin
    if DBCheck.CriaFm(TFmEntiSel, FmEntiSel, afmoNegarComAviso) then
    begin
      Screen.Cursor := crHourGlass;
      try
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('DROP TABLE entisel; ');
        Query.SQL.Add('CREATE TABLE entisel  (');
        Query.SQL.Add('  Nome varchar(30)     ,');
        Query.SQL.Add('  Codigo integer       ,');
        Query.SQL.Add('  CNPJCPF varchar(20) ,');
        Query.SQL.Add('  Ativo smallint        ');
        Query.SQL.Add(');');
        //
        QrEntidades.Close;
        UnDmkDAC_PF.AbreQueryApenas(QrEntidades);
        while not QrEntidades.Eof do
        begin
          Query.SQL.Add(
            'INSERT INTO entisel (Nome, Codigo, CNPJCPF, Ativo) VALUES (' +
            '"' + QrEntidadesNOMEENTIDADE.Value + '", ' +
            FormatFloat('0', QrEntidadesCodigo.Value) + ',' +
            '"' + QrEntidadesCNPJ_CPF_TXT.Value + '",1);');
          QrEntidades.Next;
        end;
        //
        Query.SQL.Add('SELECT * FROM entisel;');
        DmkABS_PF.AbreQuery(Query);
        with FmEntiSel do
        begin
          ShowModal;
          Destroy;
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
    if FCadastrar then Close;
  end;
*)
end;

procedure TFmEntidadesImp.FormCreate(Sender: TObject);
begin
  FLiberaSelecionar := False;
  UnDmkDAC_PF.AbreQuery(QrUF, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProfiss, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntiGrupos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrAccounts, Dmod.MyDB);
  if VAR_TERCEIR2 <> CO_VAZIO then CkTerceiro.Visible := True;
  if VAR_CLIENTE1 <> CO_VAZIO then CkCliente1.Visible := True;
  if VAR_CLIENTE2 <> CO_VAZIO then CkCliente2.Visible := True;
  if VAR_FORNECE1 <> CO_VAZIO then CkFornece1.Visible := True;
  if VAR_FORNECE2 <> CO_VAZIO then CkFornece2.Visible := True;
  if VAR_FORNECE3 <> CO_VAZIO then CkFornece3.Visible := True;
  if VAR_FORNECE4 <> CO_VAZIO then CkFornece4.Visible := True;
  //
  CkTerceiro.Caption := VAR_TERCEIR2;
  CkCliente1.Caption := VAR_CLIENTE1;
  CkCliente2.Caption := VAR_CLIENTE2;
  CkFornece1.Caption := VAR_FORNECE1;
  CkFornece2.Caption := VAR_FORNECE2;
  CkFornece3.Caption := VAR_FORNECE3;
  CkFornece4.Caption := VAR_FORNECE4;
end;

procedure TFmEntidadesImp.BtLocaliza1Click(Sender: TObject);
var
  Tels, Liga, Tipos, Ordem, OrdemNome, CEPi, CEPf: String;
  UF, Grupo, Account: Integer;
begin
  // N�o Prefcisa
  //if Trim(CBCidade.Text) = '' then CBCidade.KeyValue := Null;
  if CBUF.KeyValue     <> Null then UF := CBUF.KeyValue else UF := 0;
  if  (CkCliente1.Checked = False)
  and (CkCliente2.Checked = False)
  and (CkFornece1.Checked = False)
  and (CkFornece2.Checked = False)
  and (CkFornece3.Checked = False)
  and (CkFornece4.Checked = False)
  and (CkTerceiro.Checked = False) then
  begin
    Geral.MB_Aviso('Nenhum tipo de cadastro foi definido');
    Exit;
  end;
  Grupo := 0;
  if (CkGrupo.Checked) then
  begin
    if (CBGrupo.KeyValue=Null) then
    begin
      Geral.MB_Aviso('Defina um Grupo!');
      Exit;
    end;
    Grupo := CBGrupo.KeyValue;
  end;
  Account := 0;
  if (CkAccount.Checked) then
  begin
    if (CBAccount.KeyValue=Null) then
    begin
      Geral.MB_Aviso('Defina um Representante!');
      Exit;
    end;
    Account := CBAccount.KeyValue;
  end;
  CEPi := Geral.SoNumero_TT(EdCEPIni.Text);
  CEPf := Geral.SoNumero_TT(EdCEPFim.Text);
  Liga  := '';
  Tipos := '';
  if CkCliente1.Checked then
  begin
   Tipos := 'Cliente1 = "V"';
   Liga  := ' OR ';
  end;
  if CkCliente2.Checked then
  begin
   Tipos := Tipos + Liga + 'Cliente2 = "V"';
   Liga  := ' OR ';
  end;
  if CkFornece1.Checked then
  begin
   Tipos := Tipos + Liga + 'Fornece1 = "V"';
   Liga  := ' OR ';
  end;
  if CkFornece2.Checked then
  begin
   Tipos := Tipos + Liga + 'Fornece2 = "V"';
   Liga  := ' OR ';
  end;
  if CkFornece3.Checked then
  begin
   Tipos := Tipos + Liga + 'Fornece3 = "V"';
   Liga  := ' OR ';
  end;
  if CkFornece4.Checked then
  begin
   Tipos := Tipos + Liga + 'Fornece4 = "V"';
   Liga  := ' OR ';
  end;
  if CkTerceiro.Checked then Tipos := Tipos + Liga + 'Terceiro = "V"';
  //
  case RGOrigem.ItemIndex of
    1: OrdemNome := 'en.Nome';
    2: OrdemNome := 'en.RazaoSocial';
    else OrdemNome := 'IF (en.Tipo=0, en.RazaoSocial, en.Nome)';
  end;
  //
  QrEntidades.Close;
  QrEntidades.SQL.Clear;
  QrEntidades.SQL.Add('SELECT en.Codigo, en.LimiCred, ');
  case RGOrigem.ItemIndex of
    1: QrEntidades.SQL.Add('en.Nome        NOMEENTIDADE,');
    2: QrEntidades.SQL.Add('en.RazaoSocial NOMEENTIDADE,');
    else QrEntidades.SQL.Add('IF (en.Tipo=0, en.RazaoSocial, en.Nome) NOMEENTIDADE,');
  end;
  //
  case RGOrigem.ItemIndex of
    1: QrEntidades.SQL.Add('en.Apelido     NOMEFANTASIA,');
    2: QrEntidades.SQL.Add('en.Fantasia    NOMEFANTASIA,');
    else QrEntidades.SQL.Add('IF (en.Tipo=0, en.Fantasia, en.Apelido) NOMEFANTASIA,');
  end;
  //
  case RGOrigem.ItemIndex of
    1: QrEntidades.SQL.Add('en.PEMail      EMAIL,');
    2: QrEntidades.SQL.Add('en.EEMAIL      EMAIL,');
    else QrEntidades.SQL.Add('IF (en.Tipo=0, en.EEMail, en.PEMail) EMail,');
  end;
  //
  case RGOrigem.ItemIndex of
    1: QrEntidades.SQL.Add('en.CPF  CNPJ_CPF,');
    2: QrEntidades.SQL.Add('en.CNPJ CNPJ_CPF,');
    else QrEntidades.SQL.Add('IF (en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF,');
  end;
  //
  case RGOrigem.ItemIndex of
    1: QrEntidades.SQL.Add('en.RG  IE_RG,');
    2: QrEntidades.SQL.Add('en.IE IE_RG,');
    else QrEntidades.SQL.Add('IF (en.Tipo=0, en.IE, en.RG) IE_RG,');
  end;
  //
  case RGOrigem.ItemIndex of
    1: QrEntidades.SQL.Add('en.PContato CONTATO,');
    2: QrEntidades.SQL.Add('en.EContato CONTATO,');
    else QrEntidades.SQL.Add('IF (en.Tipo=0, en.EContato, en.PContato) CONTATO,');
  end;
  //
  case RGOrigem.ItemIndex of
    0:
    begin
      QrEntidades.SQL.Add('IF (Tipo=0, en.ERua,    en.PRua    ) RUA,');
      QrEntidades.SQL.Add('IF (Tipo=0, en.ENumero+0.000, en.PNumero+0.000 ) Numero,');
      QrEntidades.SQL.Add('IF (Tipo=0, en.ECompl,  en.PCompl  ) Compl ,');
      QrEntidades.SQL.Add('IF (Tipo=0, en.EBairro, en.PBairro ) Bairro,');
      QrEntidades.SQL.Add('IF (Tipo=0, en.ECidade, en.PCidade ) Cidade,');
      QrEntidades.SQL.Add('IF (Tipo=0, en.EUF,     en.PUF     ) + 0.000 UF,');
      QrEntidades.SQL.Add('IF (Tipo=0, en.ECEP,    en.PCEP    ) + 0.000 CEP,');
      QrEntidades.SQL.Add('IF (Tipo=0, en.EPais,   en.PPais   ) Pais  ,');
      //
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.ETe1,   en.PTe1   ) Te1 ,');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.ETe2,   en.PTe2   ) Te2 ,');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.ETe3,   en.PTe3   ) Te3 ,');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.EFax,   en.PFax   ) Fax ,');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.ECel,   en.PCel   ) Cel ,');
    end;
    1:
    begin
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.PNumero+0.000, en.PNumero+0.000 ) Numero,');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.PUF, en.PUF ) + 0.000 UF,');
      QrEntidades.SQL.Add('en.PRua RUA, en.PCompl COMPL, en.PBairro BAIRRO,');
      QrEntidades.SQL.Add('en.PCidade CIDADE, en.PPais PAIS, ');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.PCEP,    en.PCEP    ) + 0.000 CEP   ,');
      //
      QrEntidades.SQL.Add('en.PTe1 Te1 ,');
      QrEntidades.SQL.Add('en.PTe2 Te2 ,');
      QrEntidades.SQL.Add('en.ETe3 Te3 ,');
      QrEntidades.SQL.Add('en.PFax  Fax ,');
      QrEntidades.SQL.Add('en.PCel  Cel ,');
    end;
    2:
    begin
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.ENumero+0.000, en.ENumero+0.000 ) Numero,');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.EUF, en.EUF ) + 0.000 UF,');
      QrEntidades.SQL.Add('en.ERua RUA, en.ECompl COMPL, en.EBairro BAIRRO,');
      QrEntidades.SQL.Add('en.ECidade CIDADE, en.EPais PAIS, ');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.ECEP,    en.ECEP    ) + 0.000 CEP   ,');
      //
      QrEntidades.SQL.Add('en.ETe1 Te1 ,');
      QrEntidades.SQL.Add('en.ETe2 Te2 ,');
      QrEntidades.SQL.Add('en.ETe3 Te3 ,');
      QrEntidades.SQL.Add('en.EFax  Fax ,');
      QrEntidades.SQL.Add('en.ECel  Cel ,');
    end;
    3:
    begin
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.CNumero+0.000, en.CNumero+0.000 ) Numero,');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.CUF, en.CUF ) + 0.000 UF,');
      QrEntidades.SQL.Add('en.CRua RUA, en.CCompl COMPL, en.CBairro BAIRRO,');
      QrEntidades.SQL.Add('en.CCidade CIDADE, en.CPais PAIS,');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.CCEP,    en.CCEP    ) + 0.000 CEP   ,');
      //
      QrEntidades.SQL.Add('en.CTel Te1 ,');
      QrEntidades.SQL.Add('""      Te2 ,');
      QrEntidades.SQL.Add('""      Te3 ,');
      QrEntidades.SQL.Add('en.CFax Fax ,');
      QrEntidades.SQL.Add('en.CCel Cel ,');
    end;
    4:
    begin
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.LNumero+0.000, en.LNumero+0.000 ) Numero,');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.LUF, en.LUF ) + 0.000 UF,');
      QrEntidades.SQL.Add('en.LRua RUA, en.LCompl COMPL, en.LBairro BAIRRO,');
      QrEntidades.SQL.Add('en.LCidade CIDADE, en.LPais PAIS,');
      QrEntidades.SQL.Add('IF (en.Tipo=0, en.LCEP,    en.LCEP    ) + 0.000 CEP   ,');
      //
      QrEntidades.SQL.Add('en.LTel Te1 ,');
      QrEntidades.SQL.Add('""      Te2 ,');
      QrEntidades.SQL.Add('""      Te3 ,');
      QrEntidades.SQL.Add('en.LFax Fax ,');
      QrEntidades.SQL.Add('en.LCel Cel ,');
    end;
  end;
  //
  QrEntidades.SQL.Add('en.PTe1, en.PTe2, en.PTe3, en.ETe1, en.ETe2, en.ETe3, ');
  QrEntidades.SQL.Add('en.PFax, en.EFax, en.PCel, en.ECel, uf.Nome NOMEUF,');
  QrEntidades.SQL.Add('en.CTel, en.CCel, en.CFax, en.LTel, en.LCel, en.LFax');
  //
  QrEntidades.SQL.Add('FROM entidades en, ufs uf');
  QrEntidades.SQL.Add('WHERE uf.Codigo=(CASE WHEN en.Tipo=0 THEN en.EUF else en.PUF END)');
  //
  case RGEMail.ItemIndex of
    1:
    case RGOrigem.ItemIndex of
      1: QrEntidades.SQL.Add('AND en.PEMail <> ""');
      2: QrEntidades.SQL.Add('AND en.EEMail <> ""');
      else QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.EEMail, en.PEMail)) <> ""');
    end;
    2:
    case RGOrigem.ItemIndex of
      1: QrEntidades.SQL.Add('AND en.PEMail = ""');
      2: QrEntidades.SQL.Add('AND en.EEMail = ""');
      else QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.EEMail, en.PEMail)) = ""');
    end;
  end;
  //
  QrEntidades.SQL.Add('AND en.Codigo>0');
  QrEntidades.SQL.Add('AND ('+Tipos+')');
  //
  if CkUF.Checked then
  begin
    case RGOrigem.ItemIndex of
      0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.EUF, en.PUF))='+IntToStr(UF));
      1: QrEntidades.SQL.Add('AND en.PUF='+IntToStr(UF));
      2: QrEntidades.SQL.Add('AND en.EUF='+IntToStr(UF));
      3: QrEntidades.SQL.Add('AND en.CUF='+IntToStr(UF));
      4: QrEntidades.SQL.Add('AND en.LUF='+IntToStr(UF));
    end;
  end;
  //
  if CkCidade.Checked then
  begin
    case RGOrigem.ItemIndex of
      0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.ECidade, en.PCidade))="'+CBCidade.Text+'"');
      1: QrEntidades.SQL.Add('AND en.PCidade="'+CBCidade.Text+'"');
      2: QrEntidades.SQL.Add('AND en.ECidade="'+CBCidade.Text+'"');
      3: QrEntidades.SQL.Add('AND en.CCidade="'+CBCidade.Text+'"');
      4: QrEntidades.SQL.Add('AND en.LCidade="'+CBCidade.Text+'"');
    end;
  end;
  if CkPais.Checked then
  begin
    case RGOrigem.ItemIndex of
      0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.EPais, en.PPais))="'+CBPais.Text+'"');
      1: QrEntidades.SQL.Add('AND en.PPais="'+CBPais.Text+'"');
      2: QrEntidades.SQL.Add('AND en.EPais="'+CBPais.Text+'"');
      3: QrEntidades.SQL.Add('AND en.CPais="'+CBPais.Text+'"');
      4: QrEntidades.SQL.Add('AND en.LPais="'+CBPais.Text+'"');
    end;
  end;
  if CkBairro.Checked then
  begin
    case RGOrigem.ItemIndex of
      0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.EBairro, en.PBairro))="'+CBBairro.Text+'"');
      1: QrEntidades.SQL.Add('AND en.PBairro="'+CBBairro.Text+'"');
      2: QrEntidades.SQL.Add('AND en.EBairro="'+CBBairro.Text+'"');
      3: QrEntidades.SQL.Add('AND en.CBairro="'+CBBairro.Text+'"');
      4: QrEntidades.SQL.Add('AND en.LBairro="'+CBBairro.Text+'"');
    end;
  end;
  if CkRua.Checked then
  begin
    case RGOrigem.ItemIndex of
      0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.ERua, en.PRua))="'+CBRua.Text+'"');
      1: QrEntidades.SQL.Add('AND en.PRua="'+CBRua.Text+'"');
      2: QrEntidades.SQL.Add('AND en.ERua="'+CBRua.Text+'"');
      3: QrEntidades.SQL.Add('AND en.CRua="'+CBRua.Text+'"');
      4: QrEntidades.SQL.Add('AND en.LRua="'+CBRua.Text+'"');
    end;
  end;
  if CkTelefones.Checked then
  begin
    Tels := Geral.SoNumero_TT(EdTelefones.Text);
    case RGOrigem.ItemIndex of
      0: QrEntidades.SQL.Add('AND ('+
          '   PTe1 LIKE "%'+Tels+'%" OR PTe2 LIKE "%'+Tels+'%" '+
          'OR PTe3 LIKE "%'+Tels+'%" OR PCel LIKE "%'+Tels+'%" '+
          'OR PFax LIKE "%'+Tels+'%" OR ETe1 LIKE "%'+Tels+'%" '+
          'OR ETe2 LIKE "%'+Tels+'%" OR ETe3 LIKE "%'+Tels+'%" '+
          'OR ECel LIKE "%'+Tels+'%" OR EFax LIKE "%'+Tels+'%" )');
      1: QrEntidades.SQL.Add('AND ('+
          '   PTe1 LIKE "%'+Tels+'%" OR PTe2 LIKE "%'+Tels+'%" '+
          'OR PTe3 LIKE "%'+Tels+'%" OR PCel LIKE "%'+Tels+'%" '+
          'OR PFax LIKE "%'+Tels+'%")');
      2: QrEntidades.SQL.Add('AND ('+
          '   ETe1 LIKE "%'+Tels+'%" OR ETe2 LIKE "%'+Tels+'%" '+
          'OR ETe3 LIKE "%'+Tels+'%" OR ECel LIKE "%'+Tels+'%" '+
          'OR EFax LIKE "%'+Tels+'%")');
      3: QrEntidades.SQL.Add('AND ('+
          '   CTel LIKE "%'+Tels+'%" OR CCel LIKE "%'+Tels+'%" '+
          'OR CFax LIKE "%'+Tels+'%")');
      4: QrEntidades.SQL.Add('AND ('+
          '   LTel LIKE "%'+Tels+'%" OR LCel LIKE "%'+Tels+'%" '+
          'OR LFax LIKE "%'+Tels+'%")');
    end;
  end;
  if CkCEP.Checked then
  begin
    case RGOrigem.ItemIndex of
      0: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.ECEP, en.PCEP)) BETWEEN "'+CEPi+'" AND "'+CEPf+'"');
      1: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.PCEP, en.PCEP)) BETWEEN "'+CEPi+'" AND "'+CEPf+'"');
      2: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.ECEP, en.ECEP)) BETWEEN "'+CEPi+'" AND "'+CEPf+'"');
      3: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.CCEP, en.CCEP)) BETWEEN "'+CEPi+'" AND "'+CEPf+'"');
      4: QrEntidades.SQL.Add('AND (IF (en.Tipo=0, en.LCEP, en.LCEP)) BETWEEN "'+CEPi+'" AND "'+CEPf+'"');
    end;
  end;
  case RGOrigem.ItemIndex of
    1: QrEntidades.SQL.Add('AND en.Tipo=1');
    2: QrEntidades.SQL.Add('AND en.Tipo=0');
  end;
  //
  if CkCPF.Checked then QrEntidades.SQL.Add('AND en.CPF="'+
  Geral.FormataCNPJ_TFT(EdCPF.Text)+'"');
  if CkCNPJ.Checked then QrEntidades.SQL.Add('AND en.CNPJ="'+
  Geral.FormataCNPJ_TFT(EdCNPJ.Text)+'"');
  //
  {
  if CkApelido.Checked then
    QrEntidades.SQL.Add('AND en.Apelido LIKE "%'+EdApelido.Text+'%"');
  if CkFantasia.Checked then
    QrEntidades.SQL.Add('AND en.Fantasia LIKE "%'+EdFantasia.Text+'%"');
  if CkNome.Checked then
    QrEntidades.SQL.Add('AND IF(en.Tipo=0, en.RazaoSocial, en.Nome) LIKE "%'+EdNome.Text+'%"');
  }
  if CkNome.Checked then
    QrEntidades.SQL.Add('AND (en.RazaoSocial LIKE "%'+EdNome.Text+'%"' +
    ' OR en.Nome LIKE "%'+EdNome.Text+'%"' +
    ' OR en.Fantasia LIKE "%'+EdNome.Text+'%"' +
    ' OR en.Apelido LIKE "%'+EdNome.Text+'%")');
  if CkProfiss.Checked then
    QrEntidades.SQL.Add('AND en.Profissao="'+CBProfiss.Text+'"');
  if CkGrupo.Checked then
    QrEntidades.SQL.Add('AND en.Grupo="'+IntToStr(Grupo)+'"');
  if CkAccount.Checked then
    QrEntidades.SQL.Add('AND en.Account="'+IntToStr(Account)+'"');
  if CkLimiCred.Checked then
  begin
    QrEntidades.SQL.Add('AND en.LimiCred BETWEEN :CredMin AND :CredMax');
    QrEntidades.ParamByName('CredMin').AsFloat := Geral.DMV(EdLimiCredMin.Text);
    QrEntidades.ParamByName('CredMax').AsFloat := Geral.DMV(EdLimiCredMax.Text);
  end;
  if CkAtivo.Checked then
    QrEntidades.SQL.Add('AND en.Ativo = 1');  
  if CkCliInt.Checked then
    QrEntidades.SQL.Add('AND en.CliInt <> 0');
  //QrEntidades.SQL.Add('');
  //////////////////////////////////////////////////////////////////////////////
  Ordem := OrdemNome;
  QrEntidades.SQL.Add('ORDER BY ' + Ordem);
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmEntidadesImp.RGOrigemClick(Sender: TObject);
begin
  ReopenCidade;
  ReopenBairro;
  ReopenRua;
  ReopenPais;
  NovaPesquisa;
end;

procedure TFmEntidadesImp.ReopenCidade;
begin
  QrCidades.Close;
  QrCidades.SQL.Clear;
  QrCidades.SQL.Add('SELECT DISTINCT');
  case RGOrigem.ItemIndex of
    0: QrCidades.SQL.Add('IF (Tipo=0, ECidade, PCidade) CIDADE');
    1: QrCidades.SQL.Add('PCidade CIDADE');
    2: QrCidades.SQL.Add('ECidade CIDADE');
    3: QrCidades.SQL.Add('CCidade CIDADE');
    4: QrCidades.SQL.Add('LCidade CIDADE');
  end;
  QrCidades.SQL.Add('FROM entidades');
  case RGOrigem.ItemIndex of
    0: QrCidades.SQL.Add('WHERE IF (Tipo=0, ECidade, PCidade) <> ""');
    1: QrCidades.SQL.Add('WHERE PCidade <> ""');
    2: QrCidades.SQL.Add('WHERE ECidade <> ""');
    3: QrCidades.SQL.Add('WHERE CCidade <> ""');
    4: QrCidades.SQL.Add('WHERE LCidade <> ""');
  end;
  QrCidades.SQL.Add('ORDER BY CIDADE');
  UnDmkDAC_PF.AbreQuery(QrCidades, Dmod.MyDB);
end;

procedure TFmEntidadesImp.ReopenPais;
begin
  QrPais.Close;
  QrPais.SQL.Clear;
  QrPais.SQL.Add('SELECT DISTINCT');
  case RGOrigem.ItemIndex of
    0: QrPais.SQL.Add('IF (Tipo=0, EPais, PPais) PAIS');
    1: QrPais.SQL.Add('PPais PAIS');
    2: QrPais.SQL.Add('EPais PAIS');
    3: QrPais.SQL.Add('CPais PAIS');
    4: QrPais.SQL.Add('LPais PAIS');
  end;
  QrPais.SQL.Add('FROM entidades');
  case RGOrigem.ItemIndex of
    0: QrPais.SQL.Add('WHERE IF (Tipo=0, EPais, PPais) <> ""');
    1: QrPais.SQL.Add('WHERE PPais <> ""');
    2: QrPais.SQL.Add('WHERE EPais <> ""');
    3: QrPais.SQL.Add('WHERE CPais <> ""');
    4: QrPais.SQL.Add('WHERE LPais <> ""');
  end;
  QrPais.SQL.Add('ORDER BY PAIS');
  UnDmkDAC_PF.AbreQuery(QrPais, Dmod.MyDB);
end;

procedure TFmEntidadesImp.ReopenBairro;
begin
  QrBairros.Close;
  QrBairros.SQL.Clear;
  QrBairros.SQL.Add('SELECT DISTINCT');
  case RGOrigem.ItemIndex of
    0: QrBairros.SQL.Add('IF (Tipo=0, EBairro, PBairro) Bairro');
    1: QrBairros.SQL.Add('PBairro Bairro');
    2: QrBairros.SQL.Add('EBairro Bairro');
    3: QrBairros.SQL.Add('CBairro Bairro');
    4: QrBairros.SQL.Add('LBairro Bairro');
  end;
  QrBairros.SQL.Add('FROM entidades');
  case RGOrigem.ItemIndex of
    0: QrBairros.SQL.Add('WHERE IF (Tipo=0, EBairro, PBairro) <> ""');
    1: QrBairros.SQL.Add('WHERE PBairro <> ""');
    2: QrBairros.SQL.Add('WHERE EBairro <> ""');
    3: QrBairros.SQL.Add('WHERE CBairro <> ""');
    4: QrBairros.SQL.Add('WHERE LBairro <> ""');
  end;
  QrBairros.SQL.Add('ORDER BY Bairro');
  UnDmkDAC_PF.AbreQuery(QrBairros, Dmod.MyDB);
end;

procedure TFmEntidadesImp.ReopenRua;
begin
  QrRuas.Close;
  QrRuas.SQL.Clear;
  QrRuas.SQL.Add('SELECT DISTINCT');
  case RGOrigem.ItemIndex of
    0: QrRuas.SQL.Add('IF (Tipo=0, ERua, PRua) Rua');
    1: QrRuas.SQL.Add('PRua Rua');
    2: QrRuas.SQL.Add('ERua Rua');
    3: QrRuas.SQL.Add('CRua Rua');
    4: QrRuas.SQL.Add('LRua Rua');
  end;
  QrRuas.SQL.Add('FROM entidades');
  case RGOrigem.ItemIndex of
    0: QrRuas.SQL.Add('WHERE IF (Tipo=0, ERua, PRua) <> ""');
    1: QrRuas.SQL.Add('WHERE PRua <> ""');
    2: QrRuas.SQL.Add('WHERE ERua <> ""');
    3: QrRuas.SQL.Add('WHERE CRua <> ""');
    4: QrRuas.SQL.Add('WHERE LRua <> ""');
  end;
  QrRuas.SQL.Add('ORDER BY Rua');
  UnDmkDAC_PF.AbreQuery(QrRuas, Dmod.MyDB);
end;

procedure TFmEntidadesImp.CBUFClick(Sender: TObject);
begin
  ReopenCidade;
  ReopenBairro;
  ReopenRua;
end;

procedure TFmEntidadesImp.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  MyObjects.CorIniComponente();
  ReopenCidade;
  ReopenBairro;
  ReopenRua;
  ReopenPais;
end;

procedure TFmEntidadesImp.CkCEPClick(Sender: TObject);
begin
  if CkCEP.Checked then
  begin
    CkUF.Checked := False;
    CkCidade.Checked := False;
    CkBairro.Checked := False;
    CkRua.Checked := False;
  end;
  NovaPesquisa;
end;

procedure TFmEntidadesImp.EdCEPIniExit(Sender: TObject);
begin
  EdCEPIni.Text := Geral.FormataCEP_TT(EdCEPIni.Text);
end;

procedure TFmEntidadesImp.EdCEPFimExit(Sender: TObject);
begin
  EdCEPFim.Text := Geral.FormataCEP_TT(EdCEPFim.Text);
end;

procedure TFmEntidadesImp.NovaPesquisa;
begin
  QrEntidades.Close;
end;

procedure TFmEntidadesImp.CkUFClick(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.CkCidadeClick(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.CkBairroClick(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.CkRuaClick(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.CkCliente1Click(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.CkCliente2Click(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.CkFornece1Click(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.CkFornece2Click(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.CkFornece3Click(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.CkFornece4Click(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.CkTerceiroClick(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.Endereos1Click(Sender: TObject);
begin
  QrEntidades.DisableControls;
  MyObjects.frxMostra(frxEndereco, 'Lista de endere�os');
  QrEntidades.EnableControls;
end;

procedure TFmEntidadesImp.ExportaparaExcel1Click(Sender: TObject);
const
  ArqOSWin = 'C:\Dermatek\Celulares.csv';
  ArqMySQL = 'C://Dermatek//Celulares.csv';
begin
  if FileExists(ArqOSWin) then
    DeleteFile(ArqOSWin);
  if UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'SELECT TRIM(SUBSTRING(TRIM(ent.Nome), 1, ',
  'IF(LOCATE(" ", TRIM(ent.Nome)) > 0, ',
  'LOCATE(" ", ent.Nome), 100))) PrimoName, ',
  'ent.PCel, ent.PNatal, ent.Sexo, ',
  'IF(ent.EstCivil=0, "", lec.Nome) NO_EstCivil, ent.PCidade,',
  'ufs.Nome NO_PUF, ent.Codigo ',
  'FROM Entidades ent',
  'LEFT JOIN ListaECIvil lec ON lec.Codigo=ent.EstCivil',
  'LEFT JOIN ufs ufs ON ufs.Codigo=ent.PUF',
  'WHERE Tipo=1 ',
  'AND PCel <> "" ',
  'ORDER BY PrimoName ',
  'INTO OUTFILE "' + ArqMySQL + '" ',
  'FIELDS TERMINATED BY ";" ',
  '']) then
    Geral.MB_Info('Dados salvos no arquivo: ' + ArqOSWin);
end;

procedure TFmEntidadesImp.DBGrid1DblClick(Sender: TObject);
begin
  VAR_ENTIDADE := QrEntidadesCodigo.Value;
  Close;
{
  if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
  begin
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
  end;
}
end;

procedure TFmEntidadesImp.EdCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := Geral.CalculaCNPJCPF(CPF);
    if Geral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MB_Aviso(SMLA_NUMEROINVALIDO2);
      EdCPF.SetFocus;
    end else EdCPF.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF.Text := CO_VAZIO;
end;

procedure TFmEntidadesImp.EdCNPJExit(Sender: TObject);
var
  Num : String;
  CNPJ : String;
begin
  CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
  if CNPJ <> CO_VAZIO then
    begin
    Num := Geral.CalculaCNPJCPF(CNPJ);
    if Geral.FormataCNPJ_TFT(CNPJ) <> Num then
    begin
      Geral.MB_Aviso(SMLA_NUMEROINVALIDO2);
      EdCNPJ.SetFocus;
    end else EdCNPJ.Text := Geral.FormataCNPJ_TT(CNPJ);
  end else EdCNPJ.Text := CO_VAZIO;
end;

procedure TFmEntidadesImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidadesImp.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmEntidadesImp.QrEntidadesAfterClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
  BtSelecionar.Enabled := False;
end;

procedure TFmEntidadesImp.QrEntidadesAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := True;
  BtSelecionar.Enabled := FLiberaSelecionar;
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Registros encontrados: ' + IntToStr(QrEntidades.RecordCount));
end;

procedure TFmEntidadesImp.QrEntidadesCalcFields(DataSet: TDataSet);
begin
  if QrEntidadesNumero.Value = 0 then
     QrEntidadesNUM_TXT.Value := 'S/N' else
     QrEntidadesNUM_TXT.Value :=
     FloatToStr(QrEntidadesNumero.Value);
  //
  QrEntidadesETE1_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe1.Value);
  QrEntidadesETE2_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe2.Value);
  QrEntidadesETE3_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe3.Value);
  QrEntidadesEFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesEFax.Value);
  QrEntidadesECEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesECel.Value);
  QrEntidadesCNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCNPJ_CPF.Value);
  //
  QrEntidadesPTE1_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe1.Value);
  QrEntidadesPTE2_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe2.Value);
  QrEntidadesPTE3_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe3.Value);
  QrEntidadesPFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPFax.Value);
  QrEntidadesPCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPCel.Value);
  //
  QrEntidadesCTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCTel.Value);
  QrEntidadesCFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCFax.Value);
  QrEntidadesCCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCCel.Value);
  //
  QrEntidadesLTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLTel.Value);
  QrEntidadesLFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLFax.Value);
  QrEntidadesLCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLCel.Value);
  //
  QrEntidadesCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntidadesCEP.Value);
  ////
  QrEntidadesCODIGO_NOME.Value := FormatFloat('000000', QrEntidadesCodigo.Value) +
  QrEntidadesNOMEENTIDADE.Value;
  QrEntidadesRUA_NUM_COMPL.Value := QrEntidadesRUA.Value + ', ' +
  QrEntidadesNUM_TXT.Value + '   '+ QrEntidadesCompl.Value;
  QrEntidadesBAIRRO_CIDADE.Value := 'Bairro '+QrEntidadesBairro.Value + ' - ' +
  QrEntidadesCidade.Value;
  QrEntidadesCEP_UF_PAIS.Value := QrEntidadesCEP_TXT.Value + '    ' +
  QrEntidadesNOMEUF.Value + '   '+ QrEntidadesPais.Value;
  //
  QrEntidadesTE1_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesTe1.Value);
  QrEntidadesTE2_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesTe2.Value);
  QrEntidadesTE3_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesTe3.Value);
  QrEntidadesCEL_TXT.Value  := Geral.FormataTelefone_TT(QrEntidadesCel.Value);
  QrEntidadesFAX_TXT.Value  := Geral.FormataTelefone_TT(QrEntidadesFax.Value);
end;

procedure TFmEntidadesImp.BtTudoClick(Sender: TObject);
begin
  if CkCliente1.Visible then CkCliente1.Checked := True;
  if CkFornece2.Visible then CkFornece2.Checked := True;
  if CkFornece1.Visible then CkFornece1.Checked := True;
  if CkFornece2.Visible then CkFornece2.Checked := True;
  if CkFornece3.Visible then CkFornece3.Checked := True;
  if CkFornece4.Visible then CkFornece4.Checked := True;
  if CkTerceiro.Visible then CkTerceiro.Checked := True;
end;

procedure TFmEntidadesImp.EMails1Click(Sender: TObject);
begin
  QrEntidades.DisableControls;
  MyObjects.frxMostra(frxEmail, 'Lista de Emeios');
  QrEntidades.EnableControls;
end;

procedure TFmEntidadesImp.RGEMailClick(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.CkLimiCredClick(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.EdLimiCredMinChange(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.EdLimiCredMaxChange(Sender: TObject);
begin
  NovaPesquisa;
end;

procedure TFmEntidadesImp.LimitedeCrdito1Click(Sender: TObject);
begin
  QrEntidades.DisableControls;
  MyObjects.frxMostra(frxLimiCred, 'Lista de entidades com limite de cr�dito');
  QrEntidades.EnableControls;
end;

procedure TFmEntidadesImp.Modelo11Click(Sender: TObject);
begin
  QrEntidades.DisableControls;
  MyObjects.frxMostra(frxLista, 'Lista telef�nica');
  QrEntidades.EnableControls;
end;

procedure TFmEntidadesImp.Modelo21Click(Sender: TObject);
begin
  QrEntidades.DisableControls;
  MyObjects.frxMostra(frxLista2, 'Lista telef�nica');
  QrEntidades.EnableControls;
end;

procedure TFmEntidadesImp.EdTelefonesExit(Sender: TObject);
begin
  EdTelefones.Text := Geral.FormataTelefone_TT_Curto(EdTelefones.Text);
end;

procedure TFmEntidadesImp.frxLimiCredGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName='MIN_MAX' then Value := EdLimiCredMin.Text+' e '+
    EdLimiCredMax.Text;
end;

end.

