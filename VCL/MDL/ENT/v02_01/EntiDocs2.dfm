object FmEntiDocs2: TFmEntiDocs2
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-033 :: Documentos de Entidades'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnMouseActivate = FormMouseActivate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 331
    Width = 812
    Height = 5
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 333
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 320
        Height = 32
        Caption = 'Documentos de Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 320
        Height = 32
        Caption = 'Documentos de Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 320
        Height = 32
        Caption = 'Documentos de Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 336
    Width = 812
    Height = 223
    Align = alBottom
    Caption = ' Arquivos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 206
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object DBGEntiDocs: TdmkDBGridZTO
        Left = 0
        Top = 0
        Width = 808
        Height = 206
        Align = alClient
        DataSource = DsEntiDocs
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        OnDblClick = DBGEntiDocsDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'ID'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SEQUENCIA'
            Title.Caption = 'Seq.'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'Descri'#231#227'o'
            Width = 500
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATAATUALIZACAO'
            Title.Caption = 'Atualizado em'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIPOARQUIVO'
            Title.Caption = 'Tipo'
            Width = 48
            Visible = True
          end>
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 172
        Height = 53
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 181
        Top = 4
        Width = 111
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 294
        Top = 4
        Width = 111
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 407
        Top = 4
        Width = 111
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtExcluiClick
      end
      object BtArquivo: TBitBtn
        Tag = 100148
        Left = 520
        Top = 4
        Width = 111
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Mostrar'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtArquivoClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 283
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrEntiDocs: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntiDocsAfterOpen
    BeforeClose = QrEntiDocsBeforeClose
    AfterScroll = QrEntiDocsAfterScroll
    Left = 384
    Top = 284
    object QrEntiDocsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiDocsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiDocsCodEnti: TIntegerField
      FieldName = 'CodEnti'
      Required = True
    end
    object QrEntiDocsTipImg: TSmallintField
      FieldName = 'TipImg'
      Required = True
    end
    object QrEntiDocsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrEntiDocsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEntiDocsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntiDocsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrEntiDocsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrEntiDocsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEntiDocsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrEntiDocsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrEntiDocsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEntiDocsTIPO: TWideStringField
      FieldName = 'TIPO'
      Required = True
      Size = 1
    end
    object QrEntiDocsCHAVE: TIntegerField
      FieldName = 'CHAVE'
      Required = True
    end
    object QrEntiDocsSEQUENCIA: TIntegerField
      FieldName = 'SEQUENCIA'
      Required = True
    end
    object QrEntiDocsDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object QrEntiDocsTIPOARQUIVO: TWideStringField
      FieldName = 'TIPOARQUIVO'
    end
    object QrEntiDocsDATAATUALIZACAO: TDateTimeField
      FieldName = 'DATAATUALIZACAO'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
  end
  object DsEntiDocs: TDataSource
    DataSet = QrEntiDocs
    Left = 384
    Top = 340
  end
  object PMInclui: TPopupMenu
    Left = 256
    Top = 524
    object CarregaArquivoIns1: TMenuItem
      Caption = 'Carrega arquivo'
      OnClick = CarregaArquivoIns1Click
    end
    object WebCamIns1: TMenuItem
      Caption = 'WebCam'
      OnClick = WebCamIns1Click
    end
    object Scanner1: TMenuItem
      Caption = 'Scanner'
      object AbrirScanner1: TMenuItem
        Caption = 'Abrir Scanner'
        OnClick = AbrirScanner1Click
      end
      object Carregararquivo1: TMenuItem
        Caption = 'Carregar arquivo'
        OnClick = Carregararquivo1Click
      end
    end
  end
  object PMAltera: TPopupMenu
    Left = 370
    Top = 527
    object CarregaArquivoAlt1: TMenuItem
      Caption = 'Carrega arquivo'
      OnClick = CarregaArquivoAlt1Click
    end
    object WebCamAlt1: TMenuItem
      Caption = 'WebCam'
      OnClick = WebCamAlt1Click
    end
    object Scanner2: TMenuItem
      Caption = 'Scanner'
      object AbrirScanner2: TMenuItem
        Caption = 'Abrir Scanner'
        OnClick = AbrirScanner2Click
      end
      object Carregararquivo2: TMenuItem
        Caption = 'Carregar arquivo'
        OnClick = Carregararquivo2Click
      end
    end
    object Descrio1: TMenuItem
      Caption = 'Descri'#231#227'o'
      OnClick = Descrio1Click
    end
  end
  object PMArquivo: TPopupMenu
    Left = 626
    Top = 475
    object NoApporiginalseinstalado1: TMenuItem
      Caption = '&No app padr'#227'o (se instalado)'
      OnClick = NoApporiginalseinstalado1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AquiNavegadorembutido1: TMenuItem
      Caption = '&Aqui (Navegador embutido)'
      OnClick = AquiNavegadorembutido1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Mostarnapasta1: TMenuItem
      Caption = 'Mostar na &Pasta'
      OnClick = Mostarnapasta1Click
    end
  end
end
