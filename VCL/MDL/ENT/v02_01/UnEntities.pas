unit UnEntities;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, dmkImage, Forms,
  Controls, Windows, DmkDAC_PF, ComCtrls, StdCtrls, ExtCtrls, Messages,
  SysUtils, Classes, Graphics, Dialogs, Variants, MaskUtils, Registry, ShellAPI,
  UnMsgInt, DB,(* DbTables,*) mySQLExceptions, UnInternalConsts, UnDmkEnums,
  dmkEditCB, dmkDBLookupCombobox, UnGrl_Vars;


type
  TTipCto = (etcTel=1, etcEmail=2);
  TUnEntities = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    (* 2017-10-14 => Movido para UnGrlEnt
    function  ObtemListaSexo(): TStringList;
    *)
    function  ObtemCodBacenPais(Pais: String; AllDataBase: TmySQLDatabase): Integer;
    function  ObtemCodMunicipio(Cidade: String; AllDataBase: TmySQLDatabase): Integer;
    function  CriaContatoGenerico(Codigo: Integer; QueryUpd: Tmysqlquery): Integer;
    function  IncluiEntiMail(Codigo, Controle, EntiTipCto, Ordem,
                DiarioAdd: Integer; EMail: String; QueryUpd: Tmysqlquery): Integer;
    function  CNPJCPFDuplicado(Codigo, Tipo: Integer; CNPJ, CPF: String; SQLType: TSQLType): Boolean;
    function  CadastroDeEntidade(Entidade: Integer; TipoForm,
              Padrao: TFormCadEnti; EditingByMatriz: Boolean = False;
              AbrirEmAba: Boolean = False; InOwner: TWincontrol = nil;
              Pager: TWinControl = nil): Boolean;
    procedure CadastroESelecaoDeEntidade2(QrEntidades: TmySQLQuery;
              EdEntidade: TdmkEditCB; CBEntidade: TdmkDBLookupCombobox);
    procedure MostraFormEntidadesImp();
    procedure MostraFormMostraEntiTipCto(Codigo: Integer; EditCB: TdmkEditCB = nil;
              ComboBox: TdmkDBLookupComboBox = nil; Query: TmySQLQuery = nil);
//    procedure MostraFormEntiImagens(Codigo, CodEnti, TipImg: Integer; MostraImagem: Boolean);
    procedure ConfiguraTipoDeCadastro(CkCliente1, CkCliente2, CkCliente3,
              CkCliente4, CkFornece1, CkFornece2, CkFornece3, CkFornece4,
              CkFornece5, CkFornece6, CkFornece7, CkFornece8, CkTerceiro: TCheckBox);
    function  TipoDeCadastroDefinido(CkCliente1, CkCliente2, CkCliente3,
              CkCliente4, CkFornece1, CkFornece2, CkFornece3, CkFornece4, CkFornece5,
              CkFornece6, CkFornece7, CkFornece8, CkTerceiro: TCheckBox;
              PageControl: TPageControl): Boolean;
    procedure MostraFormOpcoes();
    procedure MostraFormEntiGrupos(Codigo: Integer);
    procedure MostraFormEntiCargos(Codigo: Integer);
    function  MontaEndereco(NomeLograd, Rua: String; Numero: Double; Compl,
              Bairro, Cidade, UF, Pais: String; CEP: Double;
              UmaLinha: Boolean = False): String;
    procedure MostraEntiImagens(Codigo, CodEnti, TipImg: Integer; MostraImagem: Boolean);
    procedure MostraFormEntiAssina(Entidade: Integer);
    procedure MostraFormEntiDocs(TIPO: String; CHAVE: Integer; NO_CHAVE: String);
    function  MostraFormEntiLoad(const SQLType: TSQLType; const DirIni: String;
              const TIPO: String; const CHAVE, Codigo, Sequencia: Integer; const
              NO_CHAVE: String; var Caminho: String; var CodRes: Integer): Boolean;
    (*
    2017-06-28 => corre��o j� executada em todos clientes!
    procedure CorrigeContatoGenerico(Progress: TProgressBar; DataBase: TmySQLDatabase);
    *)
    function  ValidaIndicadorDeIEEntidade_1(IE: String; indIEDest, Tipo: Integer;
              PermiteindIEDestZero: Boolean; var Msg: String): Boolean;
    function  ValidaIndicadorDeIEEntidade_2(const IE: String; const indIEDest,
              Tipo, idDest, indFinal: Integer; const PermiteindIEDestZero:
              Boolean; var Msg: String): Boolean;
    function  ObtemindIEDestAuto(IE: String; UF, Tipo: Integer): Integer;
    function  SelecionaEntidade(const MostraDados: Boolean; const SQL: String;
              var Entidade: Integer): Boolean;
    function  ConfiguracoesIniciaisEntidades(DMKID_APP: Integer): Boolean;
    function  WhatsApp_ObtemTelefone(DataBase: TmySQLDatabase; EntiTipCto,
              Id_Contato: Integer): String;
    procedure ReopenTipCto(Query: TmySQLQuery; TipCto: TTipCto);
    procedure AtualizaDadosEntidadeDono(Codigo, Tipo: Integer; Nome,
              RazaoSocial, CPF, CNPJ: String);
  end;

var
  Entities: TUnEntities;

const
  CO_VERSAO_MODULO_ENT = 2.01;
  CONST_TIPO_EntiDocs2_ENTIDADE  = 'C';
  CONST_TIPO_EntiDocs2_LOCACAO   = 'L';
  CONST_TIPO_EntiDocs2_PREGAO    = 'P';

implementation

uses
  (*Entidades,*) Entidade2,
  {$IfNDef NoEntiAux}
    EntiImagens, Entidade2Imp, EntiAssina, EntiGrupos, EntiCargos, EntiTipCto,
  {$EndIf}
  {$IfNDef NoEntiDocs}
  EntiDocs2, EntiDocLoad,
  {$EndIf}
  UnMyObjects,
  MyDBCheck, Opcoes, (*EntidadesSel,*) Module, ModuleGeral, UnDmkWeb;

{ TUnEntities }

//var
  //FEntiToCad: Integer;

procedure TUnEntities.AtualizaDadosEntidadeDono(Codigo, Tipo: Integer; Nome,
  RazaoSocial, CPF, CNPJ: String);
var
  Dono: Integer;
  EntNome, EntDocum: String;
begin
  DModG.ReopenMaster('UnEntities.110');
  //
  Dono := DmodG.QrMasterDono.Value;
  //
  if Tipo = 0 then //Jur�dica
  begin
    EntNome  := RazaoSocial;
    EntDocum := Geral.SoNumero_TT(CNPJ);
  end else
  begin
    EntNome  := Nome;
    EntDocum := Geral.SoNumero_TT(CPF);
  end;
  //
  if (Dono <> 0) and (Codigo <> 0) and (Codigo = Dono) and (EntNome <> '') and
    (EntDocum <> '') then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE master SET ',
      'Em="' + EntNome + '", CNPJ="' + EntDocum + '"',
      '']);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE controle SET ',
      'CNPJ="' + EntDocum + '"',
      '']);
  end;
end;

function TUnEntities.WhatsApp_ObtemTelefone(DataBase: TmySQLDatabase; EntiTipCto,
  Id_Contato: Integer): String;
var
  Qry: TmySQLQuery;
begin
  Result := '';
  Qry    := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
      'SELECT Telefone ',
      'FROM entitel ',
      'WHERE Controle=' + Geral.FF0(Id_Contato),
      'AND EntiTipCto=' + Geral.FF0(EntiTipCto),
      '']);
    if Qry.RecordCount > 0 then
      Result := Qry.FieldByName('Telefone').AsString;
  finally
    Qry.Free;
  end;
end;

function TUnEntities.CadastroDeEntidade(Entidade: Integer; TipoForm,
  Padrao: TFormCadEnti; EditingByMatriz, AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl): Boolean;
var
  Modelo: Integer;
  Form: TForm;
begin
  Result := False;
  //FEntiToCad := Entidade;
  if GERAL_MODELO_FORM_ENTIDADES <> fmcadSelecionar then
    Modelo := Integer(GERAL_MODELO_FORM_ENTIDADES) -1
  else
  begin
    case TipoForm of
      fmcadSelecionar:
      begin
        Modelo := MyObjects.SelRadioGroup('Selecione o modelo', '', ['Modelo A', 'Modelo B'], 2);
        if not Modelo in [0, 1] then
          Exit;
      end;
      //
      fmcadEntidades: Modelo := 0;
      fmcadEntidade2: Modelo := 1;
      else Modelo := -1;
    end;
  end;
  case Modelo of
    0:
    begin
(*
      if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
      begin
        // Deve ser antes
        FmEntidades.FEditingByMatriz := EditingByMatriz;
        // somente depois...
        //if FEntiToCad <> 0 then
          //TFmEntidades(Form).LocCod(FEntiToCad, FEntiToCad);
        if Entidade <> 0 then
          FmEntidades.LocCod(Entidade, Entidade);
        FmEntidades.ShowModal;
        FmEntidades.Destroy;
        Result := True;
      end;
*)
    end;
    1:
    begin
      if AbrirEmAba then
      begin
        if FmEntidade2 = nil then
        begin
          Form := MyObjects.FormTDICria(TFmEntidade2, InOwner, Pager, False, True);
          //
          TFmEntidade2(Form).FEditingByMatriz := EditingByMatriz;
          //
          //if FEntiToCad <> 0 then
            //TFmEntidade2(Form).LocCod(FEntiToCad, FEntiToCad);
          if Entidade <> 0 then
            TFmEntidade2(Form).LocCod(Entidade, Entidade);
        end;
      end else
      begin
        if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
        begin
          // Deve ser antes
          FmEntidade2.FEditingByMatriz := EditingByMatriz;
          // somente depois...
          //if FEntiToCad <> 0 then
            ///FmEntidade2.LocCod(FEntiToCad, FEntiToCad);
          if Entidade <> 0 then
            FmEntidade2.LocCod(Entidade, Entidade);
          FmEntidade2.ShowModal;
          FmEntidade2.Destroy;
          //
          FmEntidade2 := nil;
          //
          Result := True;
        end;
      end;
    end;
    else Geral.MB_Erro('Modelo de cadastro de entidade n�o implementado!');
  end;
end;

procedure TUnEntities.CadastroESelecaoDeEntidade2(QrEntidades: TmySQLQuery;
  EdEntidade: TdmkEditCB; CBEntidade: TdmkDBLookupCombobox);
var
  Entidade: Integer;
begin
  VAR_ENTIDADE := 0;
  //
  Entidade := EdEntidade.ValueVariant;
  //
  CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrEntidades, QrEntidades.Database);

  if VAR_ENTIDADE <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
    if QrEntidades.Locate('Codigo', VAR_ENTIDADE, []) then
    begin
      EdEntidade.ValueVariant := VAR_ENTIDADE;
      CBEntidade.KeyValue     := VAR_ENTIDADE;
      EdEntidade.SetFocus;
    end;
  end;
end;

function TUnEntities.CNPJCPFDuplicado(Codigo, Tipo: Integer; CNPJ, CPF: String;
  SQLType: TSQLType): Boolean;
begin
  Result := False;
  //
  if SQLType <> stUpd then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      DModG.QrAux.Close;
      DModG.QrAux.SQL.Clear;
      if Tipo = 0 then
      begin
        DModG.QrAux.SQL.Add('SELECT Codigo FROM entidades');
        DModG.QrAux.SQL.Add('WHERE CNPJ="' + Geral.SoNumero_TT(CNPJ) + '"');
        DModG.QrAux.SQL.Add('AND Codigo <> ' + Geral.FF0(Codigo));
      end;
      if Tipo = 1 then
      begin
        DModG.QrAux.SQL.Add('SELECT Codigo FROM entidades');
        DModG.QrAux.SQL.Add('WHERE CPF="' + Geral.SoNumero_TT(CPF)+'"');
        DModG.QrAux.SQL.Add('AND Codigo <> ' + Geral.FF0(Codigo));
      end;
      UnDmkDAC_PF.AbreQueryApenas(DModG.QrAux);
    finally
      Screen.Cursor := crDefault;
    end;
    if DModG.QrAux.RecordCount > 0 then
    begin
      Result := True;
      //
      Geral.MB_Aviso('Esta entidade j� foi cadastrada com o c�digo n� ' +
        Geral.FF0(DModG.QrAux.FieldByName('Codigo').AsInteger) + '.');
      DModG.QrAux.Close;
      //
      Exit;
    end;
    DModG.QrAux.Close;
  end;
end;

function TUnEntities.CriaContatoGenerico(Codigo: Integer;
  QueryUpd: Tmysqlquery): Integer;
var
  Controle: Integer;
  Nome: String;
begin
  try
    Controle := UMyMod.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
    Nome     := '[Contato]'; //Nome gen�rico para o contato
    //
    if UMyMod.SQLInsUpd(QueryUpd, stIns, 'enticontat', False,
      ['Nome', 'Codigo'], ['Controle'], [Nome, Codigo], [Controle], True) then
    begin
      DModG.AtualizaEntiConEnt();
      //
      Result := Controle;
    end else
      Result := 0;
  except
    Result := 0;
  end;
end;

function TUnEntities.IncluiEntiMail(Codigo, Controle, EntiTipCto, Ordem,
  DiarioAdd: Integer; EMail: String; QueryUpd: Tmysqlquery): Integer;
var
  Conta: Integer;
begin
  try
    Conta := UMyMod.BuscaEmLivreY_Def('entimail', 'Conta', stIns, 0);
    //
    if UMyMod.SQLInsUpd(QueryUpd, stIns, 'entimail', False,
      ['EMail', 'EntiTipCto', 'Ordem', 'DiarioAdd', 'Codigo', 'Controle'],
      ['Conta'], [EMail, EntiTipCto, Ordem, DiarioAdd, Codigo, Controle],
      [Conta], True)
    then
      Result := Conta
    else
      Result := 0;
  except
    Result := 0;
  end;
end;

{
procedure TUnEntities.MostraFormEntiImagens(Codigo, CodEnti, TipImg: Integer;
  MostraImagem: Boolean);
begin
  if DBCheck.CriaFm(TFmEntiImagens, FmEntiImagens, afmoNegarComAviso) then
  begin
    FmEntiImagens.FCodigo       := Codigo;
    FmEntiImagens.FCodEnti      := CodEnti;
    FmEntiImagens.FTipImg       := TipImg;
    FmEntiImagens.FMostraImagem := MostraImagem;
    //
    FmEntiImagens.ReopenEntidades(Codigo, '');
    if MostraImagem then
    begin
      FmEntiImagens.ReopenEntiImgs(Codigo, CodEnti, TipImg);
      FmEntiImagens.VisualizaImagem();
    end;
    FmEntiImagens.ShowModal;
    FmEntiImagens.Destroy;
  end;
end;
}

procedure TUnEntities.MostraFormOpcoes();
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoNegarComAviso) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

function TUnEntities.MontaEndereco(NomeLograd, Rua: String; Numero: Double;
  Compl, Bairro, Cidade, UF, Pais: String; CEP: Double;
  UmaLinha: Boolean = False): String;
var
  E_ALL, NUMERO_TXT, Logr: String;
  I, LowerC, UpperC: Integer;
begin
  // Primeira linha:
  NUMERO_TXT := Geral.FormataNumeroDeRua(Rua, Numero, False);
  E_ALL      := '';
  //
  E_ALL := E_ALL + Rua;

  if Trim(Rua) <> '' then
    E_ALL := E_ALL + ', ' + NUMERO_TXT;

  if Trim(Compl) <> '' then
    E_ALL := E_ALL + ' ' + Compl;

  if not UmaLinha then
    E_ALL := E_ALL + sLineBreak;
  //
  // Segunda Linha
  if Trim(Bairro) <> '' then
    E_ALL := E_ALL + 'BAIRRO: ' + Bairro;

  if not UmaLinha then
    E_ALL := E_ALL + sLineBreak;
  //
  // terceira Linha
  if CEP > 0 then
    E_ALL := E_ALL + 'CEP ' +Geral.FormataCEP_NT(CEP);

  if Trim(Cidade) <> '' then
    E_ALL := E_ALL + '  ' + Cidade;

  if Trim(UF) <> '' then
    E_ALL := E_ALL + ', ' + UF;

  if Trim(Pais) <> '' then
    E_ALL := E_ALL + ' - ' + Pais;
  //
  Logr := Trim(NomeLograd);

  if Logr <> '' then
  begin
    LowerC := 0;
    UpperC := 0;

    for I := 0 to Length(E_ALL) do
    begin
      if Ord(E_ALL[i]) in [65..90] then
        UpperC := UpperC + 1
      else if Ord(E_ALL[i]) in [97..122] then
        LowerC := LowerC + 1;
    end;

    if UpperC > LowerC then
      Logr := AnsiUpperCase(Logr);

    E_ALL := Logr + ' ' + E_ALL;
  end;

  Result := E_ALL;
end;

procedure TUnEntities.MostraEntiImagens(Codigo, CodEnti, TipImg: Integer;
  MostraImagem: Boolean);
begin
  {$IfNDef NoEntiAux}
  if DBCheck.CriaFm(TFmEntiImagens, FmEntiImagens, afmoNegarComAviso) then
  begin
    FmEntiImagens.FCodigo       := Codigo;
    FmEntiImagens.FCodEnti      := CodEnti;
    FmEntiImagens.FTipImg       := TipImg;
    FmEntiImagens.FMostraImagem := MostraImagem;
    //
    FmEntiImagens.ReopenEntidades(Codigo, '');
    if MostraImagem then
    begin
      FmEntiImagens.ReopenEntiImgs(Codigo, CodEnti, TipImg);
      FmEntiImagens.VisualizaImagem();
    end;
    FmEntiImagens.ShowModal;
    FmEntiImagens.Destroy;
  end;
  {$EndIf}
end;

procedure TUnEntities.MostraFormEntiAssina(Entidade: Integer);
begin
  {$IfNDef NoEntiAux}
  if DBCheck.CriaFm(TFmEntiAssina, FmEntiAssina, afmoNegarComAviso) then
  begin
    FmEntiAssina.FEntidade := Entidade;
    FmEntiAssina.ShowModal;
    FmEntiAssina.Destroy;
  end;
  {$EndIf}
end;

procedure TUnEntities.MostraFormEntiCargos(Codigo: Integer);
begin
  {$IfNDef NoEntiAux}
  if DBCheck.CriaFm(TFmEntiCargos, FmEntiCargos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEntiCargos.LocCod(Codigo, Codigo);
    FmEntiCargos.ShowModal;
    FmEntiCargos.Destroy;
  end;
  {$EndIf}
end;

procedure TUnEntities.MostraFormMostraEntiTipCto(Codigo: Integer;
  EditCB: TdmkEditCB = nil; ComboBox: TdmkDBLookupComboBox = nil;
  Query: TmySQLQuery = nil);
var
  CfgCompo: Boolean;
begin
  {$IfNDef NoEntiAux}
  CfgCompo := (EditCB <> nil) and (ComboBox <> nil) and (Query <> nil);
  //
  if CfgCompo then
    VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmEntiTipCto, FmEntiTipCto, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEntiTipCto.LocCod(Codigo, Codigo);
    FmEntiTipCto.ShowModal;
    FmEntiTipCto.Destroy;
  end;
  //
  if CfgCompo then
  begin
    if VAR_CADASTRO > 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EditCB, ComboBox, Query, VAR_CADASTRO);
      //
      EditCB.SetFocus;
    end;
  end;
  {$EndIf}
end;

procedure TUnEntities.MostraFormEntidadesImp();
begin
  {$IfNDef NoEntiAux}
  if DBCheck.CriaFm(TFmEntidade2Imp, FmEntidade2Imp, afmoNegarComAviso) then
  begin
    FmEntidade2Imp.ShowModal;
    FmEntidade2Imp.Destroy;
  end;
  {$EndIf}
end;

procedure TUnEntities.MostraFormEntiDocs(TIPO: String; CHAVE: Integer; NO_CHAVE: String);
const
  sProcName = 'TUnEntities.MostraFormEntiDocs()';
var
  Caminho: String;
  Continua: Boolean;
begin
  {$IfNDef NoEntiDocs}
  try
    Continua := True;
    //
    if Tipo = CONST_TIPO_EntiDocs2_ENTIDADE then //= 'C';
      Caminho := DModG.QrOpcoesGerl.FieldByName('DirDocsEnti').Value
    else
    if Tipo = CONST_TIPO_EntiDocs2_LOCACAO then //= 'L'; >> Toolrent
      Caminho := DModG.QrOpcoesGerl.FieldByName('DirDocsApOS').Value
    else
    if Tipo = CONST_TIPO_EntiDocs2_PREGAO then //= 'P'; >> BidWon
      Caminho := DModG.QrOpcoesGerl.FieldByName('DirDocsApOS').Value
    else
    begin
      Caminho := '????';
      Continua := False;
    end;
  except
    Continua := False;
    Geral.MB_Erro('N�o foi possivel definir o diret�rio dos arquivos!' +
    sLineBreak + 'AVISE A DERMATEK!' + sLineBreak + sProcName);
  end;
  if Continua then
  begin
    if DBCheck.CriaFm(TFmEntiDocs2, FmEntiDocs2, afmoNegarComAviso) then
    begin
      FmEntiDocs2.ImgTipo.SQLType := stPsq;
      FmEntiDocs2.FCaminho := Caminho;
      FmEntiDocs2.FTIPO := TIPO;
      FmEntiDocs2.FCHAVE := CHAVE;
      FmEntiDocs2.FNO_CHAVE := NO_CHAVE;
      //
      FmEntiDocs2.ReopenEntiDocs();
      //
      FmEntiDocs2.ShowModal;
      FmEntiDocs2.Destroy;
    end;
  end;
  {$EndIf}
end;

procedure TUnEntities.MostraFormEntiGrupos(Codigo: Integer);
begin
  {$IfNDef NoEntiAux}
  if DBCheck.CriaFm(TFmEntiGrupos, FmEntiGrupos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEntiGrupos.LocCod(Codigo, Codigo);
    FmEntiGrupos.ShowModal;
    FmEntiGrupos.Destroy;
  end;
  {$EndIf}
end;

function TUnEntities.MostraFormEntiLoad(const SQLType: TSQLType; const DirIni:
  String; const TIPO: String; const CHAVE, Codigo, Sequencia: Integer; const
  NO_CHAVE: String; var Caminho: String; var CodRes: Integer): Boolean;
begin
  {$IfNDef NoEntiDocs}
  if DBCheck.CriaFm(TFmEntiDocLoad, FmEntiDocLoad, afmoNegarComAviso) then
  begin
    FmEntiDocLoad.ImgTipo.SQLType := SQLType;
    FmEntiDocLoad.FDirIni := DirIni;
    FmEntiDocLoad.FTIPO := TIPO;
    FmEntiDocLoad.FCHAVE := CHAVE;
    FmEntiDocLoad.FCodigo := Codigo;
    FmEntiDocLoad.FSEQUENCIA := SEQUENCIA;
    FmEntiDocLoad.FCaminho := Caminho;
    //
    FmEntiDocLoad.EdCodigo.ValueVariant := Codigo;
    FmEntiDocLoad.EdCHAVE.ValueVariant := CHAVE;
    FmEntiDocLoad.EdNO_CHAVE.ValueVariant := NO_CHAVE;
    FmEntiDocLoad.EdArquivo.ValueVariant := DirIni;
    //
    FmEntiDocLoad.ShowModal;
    Result := FmEntiDocLoad.FAlterou;
    CodRes := FmEntiDocLoad.FCodRes;
    FmEntiDocLoad.Destroy;
  end;
  {$EndIf}
end;

function TUnEntities.ObtemCodBacenPais(Pais: String;
  AllDataBase: TmySQLDatabase): Integer;
var
  QueryPais: TmySQLQuery;
begin
  QueryPais := TmySQLQuery.Create(AllDataBase);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QueryPais, AllDataBase, [
      'SELECT Codigo ',
      'FROM bacen_pais ',
      'WHERE Nome LIKE "' + Pais + '" ',
      '']);
    if QueryPais.RecordCount > 0 then
      Result := QueryPais.FieldByName('Codigo').AsInteger
    else
      Result := 0;
  finally
    QueryPais.Free;
  end;
end;

(*
2017-06-28 => corre��o j� executada em todos clientes!
procedure TUnEntities.CorrigeContatoGenerico(Progress: TProgressBar;
  DataBase: TmySQLDatabase);
var
  Query: TmySQLQuery;
  Controle: Integer;
  CampoExiste: Boolean;
begin
  try
    Query := TmySQLQuery.Create(DataBase);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SHOW COLUMNS ',
      'FROM controle ',
      'LIKE "AtzEntiContat" ',
      '']);
    if Query.RecordCount > 0 then
      CampoExiste := True;
    //
    if CampoExiste then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
        'SELECT con.Controle ',
        'FROM enticontat con ',
        'LEFT JOIN entidades ent ON ent.Codigo = con.Codigo ',
        'LEFT JOIN entitel tel ON tel.Controle = con.Controle ',
        'LEFT JOIN entimail mai ON mai.Controle = con.Controle ',
        'WHERE con.Nome = "[Contato]" ',
        'AND tel.Controle IS NULL ',
        'AND (mai.Controle IS NULL OR mai.Email = IF(ent.Tipo=0, ent.EEmail, ent.PEmail)) ',
        'GROUP BY con.Controle ',
        '']);
      if Query.RecordCount > 0 then
      begin
        Query.First;
        //
        Progress.Visible  := True;
        Progress.Max      := Query.RecordCount;
        Progress.Position := 0;
        //
        while not Query.EOF do
        begin
          Controle := Query.FieldByName('Controle').AsInteger;
          //
          UMyMod.ExcluiRegistroInt1('', 'enticontat', 'Controle', Controle, DataBase);
          //
          Progress.Position := Progress.Position + 1;
          Progress.Update;
          Application.ProcessMessages;
          //
          Query.Next;
        end;
      end;
      UMyMod.SQLInsUpd(Query, stUpd, 'controle', False,
        ['AtzEntiContat'], ['Codigo'], ['1'], [1], False);
      //
      Progress.Visible := False;
    end;
  finally
    Query.Free;
  end;
end;
*)

function TUnEntities.ObtemCodMunicipio(Cidade: String;
  AllDataBase: TmySQLDatabase): Integer;
var
  QueryMuni: TmySQLQuery;
begin
  QueryMuni := TmySQLQuery.Create(AllDataBase);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QueryMuni, AllDataBase, [
      'SELECT Codigo ',
      'FROM dtb_munici ',
      'WHERE Nome LIKE "' + Cidade + '" ',
      '']);
    if QueryMuni.RecordCount > 0 then
      Result := QueryMuni.FieldByName('Codigo').AsInteger
    else
      Result := 0;
  finally
    QueryMuni.Free;
  end;
end;

function TUnEntities.ConfiguracoesIniciaisEntidades(DMKID_APP: Integer): Boolean;
begin
  Result := True;
  //
  case DMKID_APP of
     2: //Bluederm
    begin
      VAR_CLIENTE1 := CO_CLIENTE + ' Produtos';
      VAR_CLIENTE2 := CO_CLIENTE + ' Servi�os';
      VAR_FORNECE1 := CO_FORNECE + ' Mat�ria-prima';
      VAR_FORNECE2 := CO_FORNECE + ' Uso / Consumo';
      VAR_FORNECE3 := CO_TRANSPORTADOR;
      VAR_FORNECE4 := CO_FORNECE + ' Outros';
      VAR_FORNECE5 := 'Funcion�rio';
      VAR_FORNECE6 := 'Vendedor';
      VAR_FORNECE7 := CO_FORNECE + ' M�o de obra';
      VAR_FORNECE8 := CO_FORNECE + ' Servi�os';
      VAR_CLIENTEC := '1';
      VAR_CLIENTEI := '1';
      VAR_FORNECEF := '1';
      VAR_FORNECEV := '3';
      VAR_FORNECET := '3';
      VAR_CXEDIT_ICM := True;
      //
      VAR_FLD_ENT_PRESTADOR := 'Fornece7';
    end;
     3: //Creditor
    begin
      VAR_CLIENTE1 := 'Cliente';
      VAR_CLIENTE2 := '';
      VAR_FORNECE1 := 'Fornecedor';
      VAR_FORNECE2 := 'Coligado';
      VAR_FORNECE3 := '';
      VAR_FORNECE4 := 'Fiador';
      VAR_FORNECE5 := 'S�cio (repres.)';
      VAR_QUANTI1NOME := '?????????';
      VAR_CAMPOTRANSPORTADORA := '?????????';
    end;
     4: //Syndi2
    begin
      VAR_TITULO_FATNUM := 'Bloqueto';
      VAR_CLIENTE1 := 'Condom�nio';
      VAR_CLIENTE2 := 'Dono im�vel';
      VAR_CLIENTE3 := 'Resp.Im�vel';
      VAR_CLIENTE4 := 'Morador';
      VAR_FORNECE1 := CO_FORNECEDOR;
      VAR_FORNECE2 := 'Funci.Condom.';
      VAR_FORNECE3 := 'Empreg.dom.';
      VAR_FORNECE4 := 'Morad.Remun.';
      VAR_FORNECE5 := 'Funci. Adm.';
      VAR_FORNECE6 := '';
      VAR_CLIENTEI := '1';
      VAR_FORNECEI := '1';
      VAR_QUANTI1NOME := '?????????';
      VAR_CAMPOTRANSPORTADORA := '?????????';
      VAR_FP_EMPRESA := 'Cliente1="V"';
      VAR_FP_FUNCION := '(Fornece2="V" OR Fornece4="V")';
    end;
     6: //Planning
    begin
      VAR_TITULO_FATNUM := 'Fatura';
      VAR_CLIENTE1 := CO_CLIENTE + ' Produtos';
      VAR_CLIENTE2 := CO_CLIENTE + ' Servi�os';
      VAR_CLIENTE3 := '';
      VAR_CLIENTE4 := '';
      VAR_FORNECE1 := CO_FORNECEDOR;
      VAR_FORNECE2 := CO_TRANSPORTADOR;
      VAR_FORNECE3 := 'Funcion�rio';
      VAR_FORNECE4 := 'Representante';
      VAR_FORNECE5 := 'Vendedor';
      VAR_FORNECE6 := 'Supervisor';
      VAR_FORNECE7 := 'Guia';
      VAR_FORNECE8 := CO_FORNECE + ' m�o de obra';
      //
      VAR_CLIENTEC := '1';
      VAR_FORNECEF := '1';
      VAR_FORNECEV := '3';
      VAR_FORNECET := '2';
      //
      VAR_FLD_ENT_PRESTADOR := 'Fornece8';
      {
      VAR_CLIENTEI := '1';
      VAR_FORNECEI := '1';
      VAR_QUANTI1NOME := '?????????';
      VAR_CAMPOTRANSPORTADORA := '?????????';
      VAR_FP_EMPRESA := 'Cliente1="V"';
      VAR_FP_FUNCION := '(Fornece2="V" OR Fornece4="V")';
      }
    end;
     7: //LeSew
    begin
      VAR_CLIENTE1 := CO_CLIENTE;
      VAR_CLIENTE2 := 'Aluno';
      VAR_CLIENTE3 := 'Ponto de venda';
      VAR_CLIENTE4 := '';
      VAR_FORNECE1 := CO_FORNECEDOR;
      VAR_FORNECE2 := 'Promotor';
      VAR_FORNECE3 := 'Professor';
      VAR_FORNECE4 := 'Funcion�rio';
      VAR_FORNECE5 := CO_TRANSPORTADOR;
      VAR_FORNECE6 := 'Consultor';
      VAR_FORNECE7 := 'Cons. Gerente';
      VAR_FORNECE8 := '';
      VAR_CLIENTEI := '';
      VAR_FORNECEI := '';
      VAR_QUANTI1NOME := '?????????';
      VAR_CAMPOTRANSPORTADORA := '?????????';
      VAR_FP_EMPRESA := '';
      VAR_FP_FUNCION := '';
    end;
    17: //DControl
    begin
      VAR_CLIENTE1 := 'Cliente';
      VAR_CLIENTE2 := '';
      VAR_CLIENTE3 := '';
      VAR_CLIENTE4 := '';
      VAR_FORNECE1 := 'Fornecedor';
      VAR_FORNECE2 := 'Funcion�rio';
      VAR_FORNECE3 := 'Atendente';
      VAR_FORNECE4 := '';
      VAR_FORNECE5 := '';
      VAR_FORNECE6 := '';
      VAR_FORNECE7 := '';
      VAR_FORNECE8 := '';
      VAR_QUANTI1NOME := '?????????';
      VAR_CAMPOTRANSPORTADORA := '?????????';
    end;
    19: //Academy
    begin
      VAR_CLIENTE1 := 'Aluno';
      VAR_CLIENTE2 := 'Instrutor';
      VAR_FORNECE1 := CO_FORNECEDOR;
      VAR_FORNECE2 := '';
      VAR_FORNECE3 := '';
      VAR_FORNECE4 := '';
      VAR_QUANTI1NOME := '?????????';
      VAR_CAMPOTRANSPORTADORA := '';
    end;
    22: //Credito2
    begin
      VAR_CLIENTE1 := 'Cliente';
      VAR_CLIENTE2 := '';
      VAR_FORNECE1 := 'Fornecedor';
      VAR_FORNECE2 := 'Coligado';
      VAR_FORNECE3 := '';
      VAR_FORNECE4 := 'Fiador';
      VAR_FORNECE5 := 'S�cio (repres.)';
      VAR_QUANTI1NOME := '?????????';
      VAR_CAMPOTRANSPORTADORA := '?????????';
    end;
    24: //Bugstrol
    begin
      VAR_CLIENTE1 := 'Cliente';
      VAR_CLIENTE2 := '';
      VAR_CLIENTE3 := '';
      VAR_CLIENTE4 := '';
      VAR_FORNECE1 := 'Fornecedor';
      VAR_FORNECE2 := 'Funcion�rio';
      VAR_FORNECE3 := 'Vendedor';
      VAR_FORNECE4 := '';
      VAR_FORNECE5 := '';
      VAR_FORNECE6 := '';
      VAR_FORNECE7 := '';
      VAR_FORNECE8 := '';
      VAR_QUANTI1NOME := '?????????';
      VAR_CAMPOTRANSPORTADORA := '?????????';
      //
      VAR_FP_EMPRESA := 'Cliente1="V"';
      VAR_FP_FUNCION := 'Fornece2="V"';
    end;
    28: //Toolrent
    begin
      VAR_CLIENTE1 := 'Cliente';
      VAR_CLIENTE2 := '';
      VAR_CLIENTE3 := '';
      VAR_CLIENTE4 := '';
      VAR_FORNECE1 := 'Fornecedor';
      VAR_FORNECE2 := 'Transportador';
      VAR_FORNECE3 := 'Funcion�rio';
      VAR_FORNECE4 := '';
      VAR_FORNECE5 := '';
      VAR_FORNECE6 := '';
      VAR_FORNECE7 := '';
      VAR_FORNECE8 := '';
      VAR_QUANTI1NOME := '?????????';
      VAR_CAMPOTRANSPORTADORA := '?????????';
      //
      VAR_FP_EMPRESA := 'Cliente1="V"';
      VAR_FP_FUNCION := 'Fornece3="V"';
    end;
    29: //Infrajob
    begin
      VAR_CLIENTE1 := 'Cliente';
      VAR_CLIENTE2 := '';
      VAR_CLIENTE3 := '';
      VAR_CLIENTE4 := '';
      VAR_FORNECE1 := 'Fornecedor';
      VAR_FORNECE2 := 'Transportador';
      VAR_FORNECE3 := 'Vendedor';
      VAR_FORNECE4 := 'Funcion�rio';
      VAR_FORNECE5 := '';
      VAR_FORNECE6 := '';
      VAR_FORNECE7 := '';
      VAR_FORNECE8 := '';
    end;
    42: //Logidek
    begin
      //n�o h� outro al�m de cliente1, fornece1 e terceiro;
    end;
    43: //Syndi3
    begin
      VAR_TITULO_FATNUM := 'Bloqueto';
      VAR_CLIENTE1 := 'Condom�nio';
      VAR_CLIENTE2 := 'Dono im�vel';
      VAR_CLIENTE3 := 'Resp.Im�vel';
      VAR_CLIENTE4 := 'Morador';
      VAR_FORNECE1 := CO_FORNECEDOR;
      VAR_FORNECE2 := 'Funci.Condom.';
      VAR_FORNECE3 := 'Empreg.dom.';
      VAR_FORNECE4 := 'Morad.Remun.';
      VAR_FORNECE5 := 'Funci. Adm.';
      VAR_FORNECE6 := '';
      VAR_CLIENTEI := '1';
      VAR_FORNECEI := '1';
      VAR_QUANTI1NOME := '?????????';
      VAR_CAMPOTRANSPORTADORA := '?????????';
      VAR_FP_EMPRESA := 'Cliente1="V"';
      VAR_FP_FUNCION := '(Fornece2="V" OR Fornece4="V")';
    end;
    else
      Result := False;
  end;
end;

procedure TUnEntities.ConfiguraTipoDeCadastro(CkCliente1, CkCliente2, CkCliente3,
  CkCliente4, CkFornece1, CkFornece2, CkFornece3, CkFornece4, CkFornece5,
  CkFornece6, CkFornece7, CkFornece8, CkTerceiro: TCheckBox);
begin
  CkCliente1.Caption := VAR_CLIENTE1;
  CkCliente2.Caption := VAR_CLIENTE2;
  CkCliente3.Caption := VAR_CLIENTE3;
  CkCliente4.Caption := VAR_CLIENTE4;
  CkFornece1.Caption := VAR_FORNECE1;
  CkFornece2.Caption := VAR_FORNECE2;
  CkFornece3.Caption := VAR_FORNECE3;
  CkFornece4.Caption := VAR_FORNECE4;
  CkFornece5.Caption := VAR_FORNECE5;
  CkFornece6.Caption := VAR_FORNECE6;
  CkFornece7.Caption := VAR_FORNECE7;
  CkFornece8.Caption := VAR_FORNECE8;
  CkTerceiro.Caption := VAR_TERCEIR2;
  //
  CkCliente2.Visible := False;
  CkCliente3.Visible := False;
  CkCliente4.Visible := False;
  CkFornece1.Visible := False;
  CkFornece2.Visible := False;
  CkFornece3.Visible := False;
  CkFornece4.Visible := False;
  CkFornece5.Visible := False;
  CkFornece6.Visible := False;
  CkFornece7.Visible := False;
  CkFornece8.Visible := False;
  CkTerceiro.Visible := False;
  //
  if VAR_CLIENTE2 <> '' then CkCliente2.Visible := True;
  if VAR_CLIENTE3 <> '' then CkCliente3.Visible := True;
  if VAR_CLIENTE4 <> '' then CkCliente4.Visible := True;
  if VAR_FORNECE1 <> '' then CkFornece1.Visible := True;
  if VAR_FORNECE2 <> '' then CkFornece2.Visible := True;
  if VAR_FORNECE3 <> '' then CkFornece3.Visible := True;
  if VAR_FORNECE4 <> '' then CkFornece4.Visible := True;
  if VAR_FORNECE5 <> '' then CkFornece5.Visible := True;
  if VAR_FORNECE6 <> '' then CkFornece6.Visible := True;
  if VAR_FORNECE7 <> '' then CkFornece7.Visible := True;
  if VAR_FORNECE8 <> '' then CkFornece8.Visible := True;
  if VAR_TERCEIR2 <> '' then CkTerceiro.Visible := True;
end;

function TUnEntities.TipoDeCadastroDefinido(CkCliente1, CkCliente2, CkCliente3,
  CkCliente4, CkFornece1, CkFornece2, CkFornece3, CkFornece4, CkFornece5,
  CkFornece6, CkFornece7, CkFornece8, CkTerceiro: TCheckBox;
  PageControl: TPageControl): Boolean;
var
  Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4,
  Fornece5, Fornece6, Fornece7, Fornece8,
  Terceiro: String;
begin
  if CkCliente1.Checked then Cliente1 := 'V' else Cliente1 := 'F';
  if CkCliente2.Checked then Cliente2 := 'V' else Cliente2 := 'F';
  if CkCliente3.Checked then Cliente3 := 'V' else Cliente3 := 'F';
  if CkCliente4.Checked then Cliente4 := 'V' else Cliente4 := 'F';
  if CkFornece1.Checked then Fornece1 := 'V' else Fornece1 := 'F';
  if CkFornece2.Checked then Fornece2 := 'V' else Fornece2 := 'F';
  if CkFornece3.Checked then Fornece3 := 'V' else Fornece3 := 'F';
  if CkFornece4.Checked then Fornece4 := 'V' else Fornece4 := 'F';
  if CkFornece5.Checked then Fornece5 := 'V' else Fornece5 := 'F';
  if CkFornece6.Checked then Fornece6 := 'V' else Fornece6 := 'F';
  if CkFornece7.Checked then Fornece7 := 'V' else Fornece7 := 'F';
  if CkFornece8.Checked then Fornece8 := 'V' else Fornece8 := 'F';
  if CkTerceiro.Checked then Terceiro := 'V' else Terceiro := 'F';
  if
  (Cliente1 = 'F') and (Cliente2 = 'F') and (Cliente3 = 'F') and (Cliente4 = 'F') and
  (Fornece1 = 'F') and (Fornece2 = 'F') and (Fornece3 = 'F') and (Fornece4 = 'F') and
  (Fornece5 = 'F') and (Fornece6 = 'F') and (Fornece7 = 'F') and (Fornece8 = 'F') and
  (Terceiro = 'F') then
  begin
    Result := False;
    Screen.Cursor := crDefault;
    if PageControl <> nil then
      PageControl.ActivePageIndex := 1;
    Geral.MB_Aviso('Defina pelo menos um "tipo" de cadastro');
    //Exit;
  end else Result := True;
end;

function TUnEntities.ObtemindIEDestAuto(IE: String; UF, Tipo: Integer): Integer;
var
  UF_Txt: String;
begin
  UF_Txt := Geral.GetSiglaUF_do_CodigoUF(UF);
  //
  if Tipo = 0 then
  begin
    if (IE <> '') and (UpperCase(IE) <> 'ISENTO') then
      Result := 1
    else if UpperCase(UF_Txt) = 'EX' then
      Result := 9
    else
      Result := 2;
  end else
    Result := 9;
end;

procedure TUnEntities.ReopenTipCto(Query: TmySQLQuery; TipCto: TTipCto);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Codigo, CodUsu, Nome ',
    'FROM entitipcto ',
    'WHERE Ativo = 1 ',
    'AND Tipo IN (0, ' + Geral.FF0(Integer(TipCto)) + ') ',
    'ORDER BY Nome ',
    '']);
end;

function TUnEntities.SelecionaEntidade(const MostraDados: Boolean; const SQL:
  String; var Entidade: Integer): Boolean;
(*
begin
  Result := False;
  VAR_ENTIDADE := 0;
  //
  if DBCheck.CriaFm(TFmEntidadesSel, FmEntidadesSel, afmoLiberado) then
  begin
    FmEntidadesSel.PnDados.Visible := MostraDados;
    FmEntidadesSel.ShowModal;
    if VAR_ENTIDADE <> 0 then
    begin
      Entidade := VAR_ENTIDADE;
      Result := True;
    end;
  end;
*)
const
  Aviso  = '...';
  Titulo = 'Sele��o de entidade';
  Prompt = 'Informe a entidade: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Controle, Codigo: Variant;
  PesqSQL: String;
begin
  if SQL <> '' then
    PesqSQL := SQL
  else
    PesqSQL := Geral.ATS([
    'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) ' + Campo,
    'FROM entidades ',
    'WHERE Codigo <> 0',
    'ORDER BY ' + Campo,
    '']);
  Codigo :=
    DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, Entidade, [
    PesqSQL], Dmod.MyDB, True);
  if Codigo <> Null then
  begin
    Entidade := Codigo;
    Result   := True;
    //
  end;
end;

function TUnEntities.ValidaIndicadorDeIEEntidade_1(IE: String; indIEDest,
  Tipo: Integer; PermiteindIEDestZero: Boolean; var Msg: String): Boolean;
begin
  Msg    := '';
  Result := True;
  //
  if Tipo = 0 then //Apenas CNPJ
  begin
    case indIEDest of
      0:
      begin
        if not PermiteindIEDestZero then
        begin
          Msg    := 'Indicador da I.E. n�o informado no cadastro da entidade!';
          Result := False;
        end;
      end;
      1:
      begin
        if (IE = '') or (UpperCase(IE) = 'ISENTO') then
        begin
          Msg    := 'Para o Indicador da I.E. "1" � necess�rio preencher a I.E. no cadastro da entidade!';
          Result := False;
        end;
      end;
      2:
      begin
        if (IE = '') or (UpperCase(IE) <> 'ISENTO') then
        begin
          Msg    := 'O campo I.E. deve ser preenchido com o valor: "ISENTO" no cadastro da entidade!';
          Result := False;
        end;
      end;
      9:
      begin
        ; //Exporta��o / importa��o n�o faz nada
      end
      else
      begin
        Msg    := 'Indicador da I.E. informado no cadastro da entidade inv�lido!';
        Result := False;
      end;
    end;
  end else
  begin
    if indIEDest <> 9 then
    begin
      Msg    := 'Indicador da I.E. informado no cadastro da entidade inv�lido!';
      Result := False;
    end;
  end;
end;

function TUnEntities.ValidaIndicadorDeIEEntidade_2(const IE: String; const
  indIEDest, Tipo, idDest, indFinal: Integer; const PermiteindIEDestZero:
  Boolean; var Msg: String): Boolean;
begin
  if ValidaIndicadorDeIEEntidade_1(IE, indIEDest, Tipo, PermiteindIEDestZero,
  Msg) = False then Exit;
  //
  //////////////////////////////////////////////////////////////////////////////
  //Rej. 696
  //Rejei��o: Opera��o com n�o contribuinte deve indicar opera��o com consumidor final
  //Informado indicador de IE do Destinat�rio n�o-contribuinte (tag: indIEDest=9)
  //e n�o � opera��o com consumidor final (tag: indFinal<>1) em opera��o de sa�da
  //(tag: tpNF=1) que n�o � com exterior (tag:idDest<>3). (NT 2019.001 v1.00)
  if indIEDest = 9 then
  begin
    if idDest <> 3 then
    begin
      if indFinal<>1 then
      begin
        Result := False;
        Msg := Geral.ATS(['Rej. c�digo 696 ',
        'Rejei��o: Opera��o com n�o contribuinte deve indicar opera��o com consumidor final. ',
        '------------------------------------------------------------------------------------',
        'Informado indicador de IE do Destinat�rio n�o-contribuinte (tag: indIEDest=9) ' +
        'e n�o � opera��o com consumidor final (tag: indFinal<>1) em opera��o de sa�da ' +
        '(tag: tpNF=1) que n�o � com exterior (tag:idDest<>3). (NT 2019.001 v1.00)']);
      end;
    end;
  end;
end;

end.
