object FmEntiAtivo: TFmEntiAtivo
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Dados referentes ao cliente'
  ClientHeight = 619
  ClientWidth = 799
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 799
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 940
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 881
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 390
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Dados referentes ao cliente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 390
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Dados referentes ao cliente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 390
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Dados referentes ao cliente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 799
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 999
      Height = 575
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 999
        Height = 575
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PnOutros: TGridPanel
          Left = 2
          Top = 118
          Width = 995
          Height = 455
          Align = alClient
          ColumnCollection = <
            item
              Value = 50.000000000000000000
            end
            item
              Value = 50.000000000000000000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = GBContratos
              Row = 0
            end
            item
              Column = 1
              Control = GBArrecada
              Row = 0
            end
            item
              Column = 0
              Control = GBUsrWeb
              Row = 1
            end>
          RowCollection = <
            item
              Value = 50.000000000000000000
            end
            item
              Value = 50.000000000000000000
            end>
          TabOrder = 0
          object GBContratos: TGroupBox
            Left = 1
            Top = 1
            Width = 496
            Height = 226
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = ' Contratos:'
            TabOrder = 0
            object Label1: TLabel
              Left = 2
              Top = 18
              Width = 350
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              Caption = 'D'#234' um clique na coluna Status para mudar o status'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBGContratos: TdmkDBGrid
              Left = 2
              Top = 34
              Width = 492
              Height = 190
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Status_TXT'
                  Title.Caption = 'Status'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DContrato_TXT'
                  Title.Caption = 'Data contrato'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtaPrxRenw_TXT'
                  Title.Caption = 'Pr'#243'x. renov;'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtaCntrFim_TXT'
                  Title.Caption = 'Data rescis'#227'o'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 250
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsContratos
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = DBGContratosCellClick
              OnDblClick = DBGContratosDblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Status_TXT'
                  Title.Caption = 'Status'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DContrato_TXT'
                  Title.Caption = 'Data contrato'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtaPrxRenw_TXT'
                  Title.Caption = 'Pr'#243'x. renov;'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtaCntrFim_TXT'
                  Title.Caption = 'Data rescis'#227'o'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 250
                  Visible = True
                end>
            end
          end
          object GBArrecada: TGroupBox
            Left = 497
            Top = 1
            Width = 497
            Height = 226
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = ' Arrecada'#231#245'es:'
            TabOrder = 1
            object Label2: TLabel
              Left = 2
              Top = 18
              Width = 431
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              Caption = 'D'#234' um clique na coluna Ativo para Ativar / Desativar o registro'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBGArrecada: TdmkDBGrid
              Left = 2
              Top = 34
              Width = 493
              Height = 190
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID'
                  Width = 75
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsArrecada
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = DBGArrecadaCellClick
              OnDblClick = DBGArrecadaDblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID'
                  Width = 75
                  Visible = True
                end>
            end
          end
          object GBUsrWeb: TGroupBox
            Left = 1
            Top = 227
            Width = 496
            Height = 227
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = ' Usu'#225'rios WEB:'
            TabOrder = 2
            object Label4: TLabel
              Left = 2
              Top = 18
              Width = 431
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              Caption = 'D'#234' um clique na coluna Ativo para Ativar / Desativar o registro'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBGUsrWeb: TdmkDBGrid
              Left = 2
              Top = 34
              Width = 492
              Height = 191
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Username'
                  Title.Caption = 'Usu'#225'rio'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Width = 75
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsUsrWeb
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = DBGUsrWebCellClick
              OnDblClick = DBGUsrWebDblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Username'
                  Title.Caption = 'Usu'#225'rio'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Width = 75
                  Visible = True
                end>
            end
          end
        end
        object PnEntidade: TPanel
          Left = 2
          Top = 18
          Width = 995
          Height = 100
          Align = alTop
          TabOrder = 1
          object GBEntidades: TGroupBox
            Left = 1
            Top = 1
            Width = 993
            Height = 98
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = ' Entidade:'
            TabOrder = 0
            object Label3: TLabel
              Left = 2
              Top = 18
              Width = 431
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              Caption = 'D'#234' um clique na coluna Ativo para Ativar / Desativar o registro'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBGEntidades: TdmkDBGrid
              Left = 2
              Top = 34
              Width = 989
              Height = 62
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Raz'#227'o social / Nome '
                  Width = 350
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Width = 75
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsEntidades
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = DBGEntidadesCellClick
              OnDblClick = DBGEntidadesDblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Raz'#227'o social / Nome '
                  Width = 350
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Width = 75
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 507
    Width = 799
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 995
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 415
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'D'#234' um duplo clique na coluna ID para localizar o registro'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 415
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'D'#234' um duplo clique na coluna ID para localizar o registro'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 550
    Width = 799
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 820
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 818
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object QrEntidades: TMySQLQuery
    Database = DModG.RV_CEP_DB
    BeforeClose = QrEntidadesBeforeClose
    AfterScroll = QrEntidadesAfterScroll
    Left = 264
    Top = 127
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 292
    Top = 127
  end
  object QrContratos: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrContratosCalcFields
    Left = 152
    Top = 279
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratosNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrContratosStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrContratosDContrato_TXT: TWideStringField
      FieldName = 'DContrato_TXT'
      Size = 10
    end
    object QrContratosDtaPrxRenw_TXT: TWideStringField
      FieldName = 'DtaPrxRenw_TXT'
      Size = 10
    end
    object QrContratosDtaCntrFim_TXT: TWideStringField
      FieldName = 'DtaCntrFim_TXT'
      Size = 10
    end
    object QrContratosStatus_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Status_TXT'
      Size = 15
      Calculated = True
    end
    object QrContratosDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 180
    Top = 279
  end
  object QrArrecada: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 600
    Top = 263
    object QrArrecadaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrArrecadaAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrArrecadaNome: TWideStringField
      FieldName = 'Nome'
      Size = 40
    end
  end
  object DsArrecada: TDataSource
    DataSet = QrArrecada
    Left = 628
    Top = 263
  end
  object QrUsrWeb: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 152
    Top = 519
    object QrUsrWebCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUsrWebAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrUsrWebUsername: TWideStringField
      FieldName = 'Username'
      Size = 100
    end
  end
  object DsUsrWeb: TDataSource
    DataSet = QrUsrWeb
    Left = 180
    Top = 519
  end
end
