unit EntiDocLoad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw;

type
  TFmEntiDocLoad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    EdCHAVE: TdmkEdit;
    Label1: TLabel;
    EdNO_CHAVE: TdmkEdit;
    EdCodigo: TdmkEdit;
    Label2: TLabel;
    EdDescricao: TdmkEdit;
    Label3: TLabel;
    EdArquivo: TdmkEdit;
    Label4: TLabel;
    SbArquivo: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbArquivoClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FTIPO, FCaminho, FDirIni: String;
    FCHAVE, FCodigo, FSEQUENCIA, FCodRes: Integer;
    FAlterou: Boolean;
  end;

  var
  FmEntiDocLoad: TFmEntiDocLoad;

implementation

uses UnMyObjects, ModuleGeral, UMySQLModule, Module;

{$R *.DFM}

procedure TFmEntiDocLoad.BtOKClick(Sender: TObject);
var
  Arquivo,
  Nome, TIPO, DESCRICAO, TIPOARQUIVO, DATAATUALIZACAO, FileExt, NovoArquivo: String;
  Codigo, CodEnti, TipImg, CHAVE, SEQUENCIA: Integer;
  SQLType: TSQLType;
begin
  Arquivo := EdArquivo.ValueVariant;
  if not FileExists(Arquivo) then
  begin
    Geral.MB_Aviso('N�o foi poss�vel encontrar o aqrquivo "' +
    EdArquivo.Text + '"');
    Exit;
  end;
  FileExt         := ExtractFileExt(Arquivo);
  //
  SQLType         := ImgTipo.SQLType;
  Codigo          := EdCodigo.ValueVariant;
  Nome            := Geral.FFN(FCHAVE, 7) + '-' + Geral.FFN(FSEQUENCIA, 2) + FileExt;
  CodEnti         := 0;
  TipImg          := -1;
  TIPO            := FTIPO; // 'C' => Entidade ( T i s o l i n .Cliente)
  CHAVE           := EdCHAVE.ValueVariant;
  SEQUENCIA       := FSEQUENCIA;
  DESCRICAO       := EdDescricao.ValueVariant;
  TIPOARQUIVO     := FileExt;
  DATAATUALIZACAO := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  NovoArquivo     := FCaminho + '\' + Nome;
  if CopyFile(PChar(Arquivo), PChar(NovoArquivo), True) then
  begin
    Codigo := UMyMod.BPGS1I32('entiimgs', 'Codigo', '', '', tsPos, SQLType, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entiimgs', False, [
    'Nome', 'CodEnti', 'TipImg',
    'TIPO', 'CHAVE', 'SEQUENCIA',
    'DESCRICAO', 'TIPOARQUIVO', 'DATAATUALIZACAO'], [
    'Codigo'], [
    Nome, CodEnti, TipImg,
    TIPO, CHAVE, SEQUENCIA,
    DESCRICAO, TIPOARQUIVO, DATAATUALIZACAO], [
    Codigo], True) then
    begin
      FAlterou := True;
      FCodRes  := Codigo;
      //
      Close;
    end;
  end;
end;

procedure TFmEntiDocLoad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiDocLoad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiDocLoad.FormCreate(Sender: TObject);
begin
  FAlterou := False;
  FCodRes  := 0;
  ImgTipo.SQLType := stLok;
end;

procedure TFmEntiDocLoad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiDocLoad.SbArquivoClick(Sender: TObject);
begin
  MyObjects.DefineArquivo1(FmEntiDocLoad, EdArquivo);
end;

end.
