unit EntiCfgRel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, Mask, dmkDBGrid, Menus, mySQLDbTables,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmEntiCfgRel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtRelatorios: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    PageControl2: TPageControl;
    Splitter1: TSplitter;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    Panel5: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    BtRelMtrz: TBitBtn;
    DBGrid4: TDBGrid;
    PMRelatorios: TPopupMenu;
    Balancete1: TMenuItem;
    Incluiitem1: TMenuItem;
    Alteraitem1: TMenuItem;
    Retiraitem1: TMenuItem;
    N1: TMenuItem;
    Itematual1: TMenuItem;
    Texto1: TMenuItem;
    Logo1: TMenuItem;
    PMRelMtrz: TPopupMenu;
    Balancete2: TMenuItem;
    Incluiitem2: TMenuItem;
    Alteraitem2: TMenuItem;
    Retiraitem2: TMenuItem;
    N2: TMenuItem;
    ItemAtual2: TMenuItem;
    exto2: TMenuItem;
    Logo2: TMenuItem;
    QrFiliais: TmySQLQuery;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisNOMEENT: TWideStringField;
    QrFiliaisCNPJCPF: TWideStringField;
    QrFiliaisIERG: TWideStringField;
    DsFiliais: TDataSource;
    QrFiliaisCNPJCPF_TXT: TWideStringField;
    QrEntiCfgRel: TmySQLQuery;
    QrEntiCfgRelRelTipo: TIntegerField;
    QrEntiCfgRelNO_RelTipo: TWideStringField;
    DsEntiCfgRel: TDataSource;
    QrEntiCfgRel_01: TmySQLQuery;
    QrEntiCfgRel_01Codigo: TIntegerField;
    QrEntiCfgRel_01Controle: TIntegerField;
    QrEntiCfgRel_01RelTipo: TIntegerField;
    QrEntiCfgRel_01RelTitu: TWideStringField;
    QrEntiCfgRel_01Ordem: TIntegerField;
    QrEntiCfgRel_01RelPrcr: TWideMemoField;
    QrEntiCfgRel_01Genero: TIntegerField;
    QrEntiCfgRel_01NO_Genero: TWideStringField;
    QrEntiCfgRel_01RelItem: TIntegerField;
    QrEntiCfgRel_01NO_RelItem: TWideStringField;
    QrEntiCfgRel_01LogoPath: TWideStringField;
    QrEntiCfgRel_01FonteTam: TSmallintField;
    QrEntiCfgRel_01CpaLin1FonNom: TWideStringField;
    QrEntiCfgRel_01CpaLin1FonTam: TSmallintField;
    QrEntiCfgRel_01CpaLin1MrgSup: TIntegerField;
    QrEntiCfgRel_01CpaLin1AltLin: TIntegerField;
    QrEntiCfgRel_01CpaLin1AliTex: TSmallintField;
    QrEntiCfgRel_01CpaLin2FonNom: TWideStringField;
    QrEntiCfgRel_01CpaLin2FonTam: TSmallintField;
    QrEntiCfgRel_01CpaLin2MrgSup: TIntegerField;
    QrEntiCfgRel_01CpaLin2AltLin: TIntegerField;
    QrEntiCfgRel_01CpaLin2AliTex: TSmallintField;
    QrEntiCfgRel_01CpaLin3FonNom: TWideStringField;
    QrEntiCfgRel_01CpaLin3FonTam: TSmallintField;
    QrEntiCfgRel_01CpaLin3MrgSup: TIntegerField;
    QrEntiCfgRel_01CpaLin3AltLin: TIntegerField;
    QrEntiCfgRel_01CpaLin3AliTex: TSmallintField;
    QrEntiCfgRel_01CpaImgMrgSup: TIntegerField;
    QrEntiCfgRel_01CpaImgMrgEsq: TIntegerField;
    QrEntiCfgRel_01CpaImgAlt: TIntegerField;
    QrEntiCfgRel_01CpaImgLar: TIntegerField;
    QrEntiCfgRel_01CpaImgStre: TSmallintField;
    QrEntiCfgRel_01CpaImgProp: TSmallintField;
    QrEntiCfgRel_01CpaImgTran: TSmallintField;
    QrEntiCfgRel_01CpaImgTranCol: TIntegerField;
    DsEntiCfgRel_01: TDataSource;
    QrOrdena: TmySQLQuery;
    QrOrdenaControle: TIntegerField;
    QrMtrzCfgRel: TmySQLQuery;
    QrMtrzCfgRelRelTipo: TIntegerField;
    QrMtrzCfgRelNO_RelTipo: TWideStringField;
    DsMtrzCfgRel: TDataSource;
    QrMtrzCfgRel_01: TmySQLQuery;
    QrMtrzCfgRel_01NO_Genero: TWideStringField;
    QrMtrzCfgRel_01Codigo: TIntegerField;
    QrMtrzCfgRel_01Controle: TIntegerField;
    QrMtrzCfgRel_01RelTipo: TIntegerField;
    QrMtrzCfgRel_01RelTitu: TWideStringField;
    QrMtrzCfgRel_01RelPrcr: TWideMemoField;
    QrMtrzCfgRel_01Ordem: TIntegerField;
    QrMtrzCfgRel_01Genero: TIntegerField;
    QrMtrzCfgRel_01Lk: TIntegerField;
    QrMtrzCfgRel_01DataCad: TDateField;
    QrMtrzCfgRel_01DataAlt: TDateField;
    QrMtrzCfgRel_01UserCad: TIntegerField;
    QrMtrzCfgRel_01UserAlt: TIntegerField;
    QrMtrzCfgRel_01AlterWeb: TSmallintField;
    QrMtrzCfgRel_01Ativo: TSmallintField;
    QrMtrzCfgRel_01LogoPath: TWideStringField;
    QrMtrzCfgRel_01FonteTam: TSmallintField;
    QrMtrzCfgRel_01PBB: TSmallintField;
    QrMtrzCfgRel_01NO_RelItem: TWideStringField;
    QrMtrzCfgRel_01CpaLin1FonNom: TWideStringField;
    QrMtrzCfgRel_01CpaLin1FonTam: TSmallintField;
    QrMtrzCfgRel_01CpaLin1MrgSup: TIntegerField;
    QrMtrzCfgRel_01CpaLin1AltLin: TIntegerField;
    QrMtrzCfgRel_01CpaLin1AliTex: TSmallintField;
    QrMtrzCfgRel_01CpaLin2FonNom: TWideStringField;
    QrMtrzCfgRel_01CpaLin2FonTam: TSmallintField;
    QrMtrzCfgRel_01CpaLin2MrgSup: TIntegerField;
    QrMtrzCfgRel_01CpaLin2AltLin: TIntegerField;
    QrMtrzCfgRel_01CpaLin2AliTex: TSmallintField;
    QrMtrzCfgRel_01CpaLin3FonNom: TWideStringField;
    QrMtrzCfgRel_01CpaLin3FonTam: TSmallintField;
    QrMtrzCfgRel_01CpaLin3MrgSup: TIntegerField;
    QrMtrzCfgRel_01CpaLin3AltLin: TIntegerField;
    QrMtrzCfgRel_01CpaLin3AliTex: TSmallintField;
    QrMtrzCfgRel_01CpaImgMrgSup: TIntegerField;
    QrMtrzCfgRel_01CpaImgMrgEsq: TIntegerField;
    QrMtrzCfgRel_01CpaImgAlt: TIntegerField;
    QrMtrzCfgRel_01CpaImgLar: TIntegerField;
    QrMtrzCfgRel_01CpaImgStre: TSmallintField;
    QrMtrzCfgRel_01CpaImgProp: TSmallintField;
    QrMtrzCfgRel_01CpaImgTran: TSmallintField;
    QrMtrzCfgRel_01CpaImgTranCol: TIntegerField;
    DsMtrzCfgRel_01: TDataSource;
    TabSheet5: TTabSheet;
    ImgLogo: TImage;
    PageControl3: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    ImgMtrz: TImage;
    TabSheet8: TTabSheet;
    DBGrid5: TDBGrid;
    QrMtrzCfgRel_01RelItem: TIntegerField;
    Removelogo1: TMenuItem;
    Removelogo2: TMenuItem;
    RETxtParecer: TRichEdit;
    RETxtParecerMtrz: TRichEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtRelatoriosClick(Sender: TObject);
    procedure BtRelMtrzClick(Sender: TObject);
    procedure QrFiliaisCalcFields(DataSet: TDataSet);
    procedure QrFiliaisAfterScroll(DataSet: TDataSet);
    procedure QrFiliaisBeforeClose(DataSet: TDataSet);
    procedure QrEntiCfgRelAfterScroll(DataSet: TDataSet);
    procedure QrEntiCfgRelBeforeClose(DataSet: TDataSet);
    procedure Incluiitem1Click(Sender: TObject);
    procedure Alteraitem1Click(Sender: TObject);
    procedure Retiraitem1Click(Sender: TObject);
    procedure QrMtrzCfgRelAfterScroll(DataSet: TDataSet);
    procedure QrMtrzCfgRelBeforeClose(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure Texto1Click(Sender: TObject);
    procedure Logo1Click(Sender: TObject);
    procedure PMRelatoriosPopup(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure Incluiitem2Click(Sender: TObject);
    procedure Alteraitem2Click(Sender: TObject);
    procedure Retiraitem2Click(Sender: TObject);
    procedure Logo2Click(Sender: TObject);
    procedure exto2Click(Sender: TObject);
    procedure PageControl3Change(Sender: TObject);
    procedure QrEntiCfgRel_01AfterScroll(DataSet: TDataSet);
    procedure QrMtrzCfgRel_01AfterScroll(DataSet: TDataSet);
    procedure PMRelMtrzPopup(Sender: TObject);
    procedure Removelogo1Click(Sender: TObject);
    procedure Removelogo2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QrEntiCfgRel_01BeforeClose(DataSet: TDataSet);
    procedure QrMtrzCfgRel_01BeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FControle: Integer;
    function ReopenFiliais(Codigo: Integer): Boolean;
    procedure ReopenEntiCfgRel(RelTipo, Controle: Integer);
    procedure ReopenEntiCfgRel_xx(Controle: Integer);
    procedure OrdenaItens(Empresa, RelTipo, LocCod: Integer);
    procedure ReopenMtrzCfgRel_xx(Controle: Integer);
    procedure AtualizaImg(Query: TmySQLQuery; PageControle: TPageControl; Img: TImage);
  public
    { Public declarations }
    FCodigo: Integer;
    procedure ItemBalanceteFilial(SQLType: TSQLType);
    procedure ItemBalanceteGeral(SQLType: TSQLType);
  end;

  var
  FmEntiCfgRel: TFmEntiCfgRel;

implementation

uses UnMyObjects,
{$IFNDef NAO_USA_TEXTOS} Textos, {$EndIf}
{$IFNDef SemEntiCfgRel} EntiCfgRel_01, {$EndIf}
Module, UMySQLModule, MyListas, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmEntiCfgRel.Alteraitem1Click(Sender: TObject);
begin
  ItemBalanceteFilial(stUpd);
end;

procedure TFmEntiCfgRel.Alteraitem2Click(Sender: TObject);
begin
  ItemBalanceteGeral(stUpd);
end;

procedure TFmEntiCfgRel.AtualizaImg(Query: TmySQLQuery;
  PageControle: TPageControl; Img: TImage);
begin
  if PageControle.ActivePageIndex = 2 then
  begin
    Img.Picture := nil;
    //
    if (Query.State <> dsInactive) and (Query.RecordCount > 0) and
      (Query.FieldByName('RelItem').AsInteger = 1) then
    begin
      if FileExists(Query.FieldByName('LogoPath').AsString) then
        Img.Picture.LoadFromFile(Query.FieldByName('LogoPath').AsString);      
    end;
  end;
end;

procedure TFmEntiCfgRel.BtRelatoriosClick(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMRelatorios, BtRelatorios);
end;

procedure TFmEntiCfgRel.BtRelMtrzClick(Sender: TObject);
begin
  PageControl3.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMRelMtrz, BtRelMtrz);
end;

procedure TFmEntiCfgRel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiCfgRel.exto2Click(Sender: TObject);
{$IFNDef NAO_USA_TEXTOS}
const
  MaxVars = 5;
  sLista: array[0..MaxVars] of String = (
    '[MES_ANO ] Per�odo (m�s e ano) do balancete',
    '[CIDADE  ] Cidade onde se localiza o condom�nio / empresa',
    '[SIGLA_UF] Estado onde se localiza o condom�nio / empresa',
    '[DATA_ATU] Data atual',
    '[DATA_FIM] �ltimo dia do m�s do per�odo do balancete',
    '');
var
  TxtParecer: AnsiString;
{$EndIf}
begin
{$IFNDef NAO_USA_Textos}
  if QrMtrzCfgRel_01RelItem.Value <> 2 then
  begin
    Geral.MB_Aviso('Item n�o usa texto!');
    Exit;
  end;
  PageControl3.ActivePageIndex := 1;
  FControle                     := QrMtrzCfgRel_01Controle.Value;
  //
  Application.CreateForm(TFmTextos, FmTextos);
  FmTextos.FAtributesSize   := 10;
  FmTextos.FAtributesName   := 'Arial';
  FmTextos.FSaveSit         := 3;
  FmTextos.FRichEdit        := RETxtParecerMtrz;
  FmTextos.LBItensMD.Sorted := False;
  MyObjects.ListaItensListBoxDeLista(FmTextos.LBItensMD, sLista);
  FmTextos.LBItensMD.Sorted := True;
  MyObjects.TextoEntreRichEdits(RETxtParecerMtrz, FmTextos.Editor);
  FmTextos.ShowModal;
  FmTextos.Destroy;
  //
  TxtParecer := MyObjects.ObtemTextoRichEdit(Self, RETxtParecerMtrz);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticfgrel', False,
    ['RelPrcr'], ['Controle'], [TxtParecer], [FControle], False)
  then
    ReopenEntiCfgRel_xx(FControle);
{$EndIf}
end;

procedure TFmEntiCfgRel.Texto1Click(Sender: TObject);
{$IFNDef NAO_USA_TEXTOS}
const
  MaxVars = 5;
  sLista: array[0..MaxVars] of String = (
    '[MES_ANO ] Per�odo (m�s e ano) do balancete',
    '[CIDADE  ] Cidade onde se localiza o condom�nio / empresa',
    '[SIGLA_UF] Estado onde se localiza o condom�nio / empresa',
    '[DATA_ATU] Data atual',
    '[DATA_FIM] �ltimo dia do m�s do per�odo do balancete',
    '');
var
  TxtParecer: AnsiString;
{$EndIf}
begin
{$IFNDef NAO_USA_TEXTOS}
  if QrEntiCfgRel_01RelItem.Value <> 2 then //Texto
  begin
    Geral.MB_Aviso('Item n�o usa texto!');
    Exit;
  end;
  PageControl2.ActivePageIndex := 1;
  FControle                     := QrEntiCfgRel_01Controle.Value;
  //
  Application.CreateForm(TFmTextos, FmTextos);
  FmTextos.FAtributesSize   := 10;
  FmTextos.FAtributesName   := 'Arial';
  FmTextos.FSaveSit         := 3;
  FmTextos.FRichEdit        := RETxtParecer;
  FmTextos.LBItensMD.Sorted := False;
  MyObjects.ListaItensListBoxDeLista(FmTextos.LBItensMD, sLista);
  FmTextos.LBItensMD.Sorted := True;
  MyObjects.TextoEntreRichEdits(RETxtParecer, FmTextos.Editor);
  FmTextos.ShowModal;
  FmTextos.Destroy;
  //
  TxtParecer := MyObjects.ObtemTextoRichEdit(Self, RETxtParecer);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticfgrel', False,
    ['RelPrcr'], ['Controle'], [TxtParecer], [FControle], False)
  then
    ReopenEntiCfgRel_xx(FControle);
{$EndIf}
end;

procedure TFmEntiCfgRel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiCfgRel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  //
  DBGrid3.PopupMenu := PMRelatorios;
  DBGrid5.PopupMenu := PMRelMtrz;
  //
  FCodigo := 0;
end;

procedure TFmEntiCfgRel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiCfgRel.FormShow(Sender: TObject);
begin
  if ReopenFiliais(FCodigo) then
    UMyMod.AbreQuery(QrMtrzCfgRel, DMod.MyDB);
end;

procedure TFmEntiCfgRel.Incluiitem1Click(Sender: TObject);
begin
  ItemBalanceteFilial(stIns);
end;

procedure TFmEntiCfgRel.Incluiitem2Click(Sender: TObject);
begin
  ItemBalanceteGeral(stIns);
end;

procedure TFmEntiCfgRel.ItemBalanceteFilial(SQLType: TSQLType);
begin
{$IFNDef SemEntiCfgRel}
  if UmyMod.FormInsUpd_Cria(TFmEntiCfgRel_01, FmEntiCfgRel_01,
    afmoNegarComAviso, QrEntiCfgRel_01, SQLType) then
  begin
    FmEntiCfgRel_01.FRelPadrao            := False;
    FmEntiCfgRel_01.EdCodigo.ValueVariant := QrFiliaisCodigo.Value;
    FmEntiCfgRel_01.ShowModal;
    OrdenaItens(QrFiliaisCodigo.Value, QrEntiCfgRelRelTipo.Value, FmEntiCfgRel_01.FControle);
    FmEntiCfgRel_01.Destroy;
    //
    UMyMod.AbreQuery(QrEntiCfgRel, DMod.MyDB);
  end;
{$EndIf}
end;

procedure TFmEntiCfgRel.ItemBalanceteGeral(SQLType: TSQLType);
begin
{$IFNDef SemEntiCfgRel}
  if UmyMod.FormInsUpd_Cria(TFmEntiCfgRel_01, FmEntiCfgRel_01,
  afmoNegarComAviso, QrEntiCfgRel_01, SQLType) then
  begin
    FmEntiCfgRel_01.FRelPadrao := True;
    FmEntiCfgRel_01.EdCodigo.ValueVariant := 0;
    FmEntiCfgRel_01.ShowModal;
    OrdenaItens(0, QrMtrzCfgRelRelTipo.Value, FmEntiCfgRel_01.FControle);
    FmEntiCfgRel_01.Destroy;
    //
    UMyMod.AbreQuery(QrMtrzCfgRel, Dmod.MyDB);
  end;
{$EndIf} 

end;

procedure TFmEntiCfgRel.Logo1Click(Sender: TObject);
var
  IniDir, Arquivo, Proporcao: String;
begin
  if QrEntiCfgRel_01RelItem.Value = 1 then //Capa
    Proporcao := '(propor��o: 18 x 18 cm)'
  else begin
    Proporcao := '(propor��o: ???)';
    Geral.MB_Aviso('Item n�o usa logo!');
    Exit;
  end;
  IniDir  := ExtractFileDir(QrEntiCfgRel_01LogoPath.Value);
  Arquivo := ExtractFileName(QrEntiCfgRel_01LogoPath.Value);
  if MyObjects.FileOpenDialog(FmEntiCfgRel, IniDir, Arquivo,
    'Selecione o Arquivo ' + Proporcao, '', [], Arquivo) then
  begin
    FControle := QrEntiCfgRel_01Controle.Value;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE enticfgrel SET AlterWeb=1, ');
    Dmod.QrUpd.SQL.Add('LogoPath=:P0 WHERE Controle=:P1 ');
    Dmod.QrUpd.Params[00].AsString  := Arquivo;
    Dmod.QrUpd.Params[01].AsInteger := FControle;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenEntiCfgRel_xx(FControle);
    PageControl2.ActivePageIndex := 2; 
    AtualizaImg(QrEntiCfgRel_01, PageControl2, ImgLogo);
  end;
end;

procedure TFmEntiCfgRel.Logo2Click(Sender: TObject);
var
  IniDir, Arquivo, Proporcao: String;
begin
  if QrMtrzCfgRel_01RelItem.Value = 1 then
    Proporcao := '(propor��o: 18 x 18 cm)'
  else begin
    Proporcao := '(propor��o: ???)';
    Geral.MB_Aviso('Item n�o usa logo!');
    Exit;
  end;
  IniDir  := ExtractFileDir(QrMtrzCfgRel_01LogoPath.Value);
  Arquivo := ExtractFileName(QrMtrzCfgRel_01LogoPath.Value);
  if MyObjects.FileOpenDialog(FmEntiCfgRel, IniDir, Arquivo,
  'Selecione o Arquivo ' + Proporcao, '', [], Arquivo) then
  begin
    FControle := QrMtrzCfgRel_01Controle.Value;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE enticfgrel SET AlterWeb=1, ');
    Dmod.QrUpd.SQL.Add('LogoPath=:P0 WHERE Controle=:P1 ');
    Dmod.QrUpd.Params[00].AsString  := Arquivo;
    Dmod.QrUpd.Params[01].AsInteger := FControle;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenMtrzCfgRel_xx(FControle);
    PageControl3.ActivePageIndex := 2;
    AtualizaImg(QrMtrzCfgRel_01, PageControl3, ImgMtrz);
  end;
end;

procedure TFmEntiCfgRel.OrdenaItens(Empresa, RelTipo, LocCod: Integer);
begin
  QrOrdena.Close;
  QrOrdena.Params[00].AsInteger := RelTipo;
  QrOrdena.Params[01].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrOrdena, Dmod.MyDB);
  QrOrdena.First;
  while not QrOrdena.Eof do
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE enticfgrel SET Ordem=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[00].AsInteger := QrOrdena.RecNo;
    Dmod.QrUpd.Params[01].AsInteger := QrOrdenaControle.Value;
    Dmod.QrUpd.ExecSQL;
    QrOrdena.Next;
  end;
  ReopenEntiCfgRel(RelTipo, LocCod);
end;

procedure TFmEntiCfgRel.PageControl1Change(Sender: TObject);
begin
  BtRelatorios.Visible := PageControl1.ActivePageIndex = 0;
end;

procedure TFmEntiCfgRel.PageControl2Change(Sender: TObject);
begin
  AtualizaImg(QrEntiCfgRel_01, PageControl2, ImgLogo);
end;

procedure TFmEntiCfgRel.PageControl3Change(Sender: TObject);
begin
  AtualizaImg(QrMtrzCfgRel_01, PageControl3, ImgMtrz);
end;

procedure TFmEntiCfgRel.PMRelatoriosPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrFiliais.State <> dsInactive) and (QrFiliais.RecordCount > 0);
  Enab2 := (QrEntiCfgRel_01.State <> dsInactive) and (QrEntiCfgRel_01.RecordCount > 0);
  //
  Incluiitem1.Enabled := Enab1;
  Alteraitem1.Enabled := Enab1 and Enab2;
  Retiraitem1.Enabled := Enab1 and Enab2;
  Itematual1.Enabled  := Enab1 and Enab2;
end;

procedure TFmEntiCfgRel.PMRelMtrzPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrFiliais.State <> dsInactive) and (QrFiliais.RecordCount > 0);
  Enab2 := (QrMtrzCfgRel_01.State <> dsInactive) and (QrMtrzCfgRel_01.RecordCount > 0);
  //
  Incluiitem2.Enabled := Enab1;
  Alteraitem2.Enabled := Enab1 and Enab2;
  Retiraitem2.Enabled := Enab1 and Enab2;
  ItemAtual2.Enabled  := Enab1 and Enab2; 
end;

procedure TFmEntiCfgRel.QrEntiCfgRelAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiCfgRel_xx(0);
end;

procedure TFmEntiCfgRel.QrEntiCfgRelBeforeClose(DataSet: TDataSet);
begin
  QrEntiCfgRel_01.Close;  
end;

procedure TFmEntiCfgRel.QrEntiCfgRel_01AfterScroll(DataSet: TDataSet);
begin
  //Texto = TabSheet3
  //Capa  = TabSheet5
  case QrEntiCfgRel_01RelItem.Value of
    1:
    begin
      TabSheet3.TabVisible := False;
      TabSheet5.TabVisible := True;
    end;
    2:
    begin
      TabSheet3.TabVisible := True;
      TabSheet5.TabVisible := False;
    end;
    else
    begin
      TabSheet3.TabVisible := False;
      TabSheet5.TabVisible := False;
    end;
  end;
  MyObjects.DefineTextoRichEdit(RETxtParecer, QrEntiCfgRel_01RelPrcr.Value);
end;

procedure TFmEntiCfgRel.QrEntiCfgRel_01BeforeClose(DataSet: TDataSet);
begin
  RETxtParecer.Text := '';
end;

procedure TFmEntiCfgRel.QrFiliaisAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiCfgRel(0, 0);
end;

procedure TFmEntiCfgRel.QrFiliaisBeforeClose(DataSet: TDataSet);
begin
  QrEntiCfgRel.Close;
end;

procedure TFmEntiCfgRel.QrFiliaisCalcFields(DataSet: TDataSet);
begin
  QrFiliaisCNPJCPF_TXT.Value := Geral.FormataCNPJ_TT(QrFiliaisCNPJCPF.Value);
end;

procedure TFmEntiCfgRel.QrMtrzCfgRelAfterScroll(DataSet: TDataSet);
begin
  ReopenMtrzCfgRel_xx(0);
end;

procedure TFmEntiCfgRel.QrMtrzCfgRelBeforeClose(DataSet: TDataSet);
begin
  QrMtrzCfgRel_01.Close;
end;

procedure TFmEntiCfgRel.QrMtrzCfgRel_01AfterScroll(DataSet: TDataSet);
begin
  //Texto = TabSheet6
  //Capa  = TabSheet7
  case QrMtrzCfgRel_01RelItem.Value of
    1:
    begin
      TabSheet6.TabVisible := False;
      TabSheet7.TabVisible := True;
    end;
    2:
    begin
      TabSheet6.TabVisible := True;
      TabSheet7.TabVisible := False;
    end;
    else
    begin
      TabSheet6.TabVisible := False;
      TabSheet7.TabVisible := False;
    end;
  end;
  MyObjects.DefineTextoRichEdit(RETxtParecerMtrz, QrMtrzCfgRel_01RelPrcr.Value);
end;

procedure TFmEntiCfgRel.QrMtrzCfgRel_01BeforeClose(DataSet: TDataSet);
begin
  RETxtParecerMtrz.Text := '';
end;

procedure TFmEntiCfgRel.Removelogo1Click(Sender: TObject);
begin
  if QrMtrzCfgRel_01RelItem.Value <> 1 then
  begin
    Geral.MB_Aviso('Item n�o usa logo!');
    Exit;
  end;
  FControle := QrMtrzCfgRel_01Controle.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticfgrel', False, [
    'LogoPath'], ['Controle'], [''], [FControle], True) then
  begin
    ReopenMtrzCfgRel_xx(FControle);
    PageControl3.ActivePageIndex := 2;
    AtualizaImg(QrMtrzCfgRel_01, PageControl3, ImgMtrz);
  end;
end;

procedure TFmEntiCfgRel.Removelogo2Click(Sender: TObject);
begin
  if QrEntiCfgRel_01RelItem.Value <> 1 then
  begin
    Geral.MB_Aviso('Item n�o usa logo!');
    Exit;
  end;
  FControle := QrEntiCfgRel_01Controle.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticfgrel', False, [
    'LogoPath'], ['Controle'], [''], [FControle], True) then
  begin
    ReopenEntiCfgRel_xx(FControle);
    PageControl2.ActivePageIndex := 2;
    //
    AtualizaImg(QrEntiCfgRel_01, PageControl2, ImgLogo);
  end;
end;

procedure TFmEntiCfgRel.ReopenEntiCfgRel(RelTipo, Controle: Integer);
begin
  QrEntiCfgRel.Close;
  QrEntiCfgRel.Params[0].AsInteger := QrFiliaisCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrEntiCfgRel, Dmod.MyDB);
  QrEntiCfgRel.Locate('RelTipo', RelTipo, []);
  //
  FControle := Controle;
end;

procedure TFmEntiCfgRel.ReopenEntiCfgRel_xx(Controle: Integer);
var
  Lista: TStrings;
  I, Tipo: Integer;
begin
  Tipo := QrEntiCfgRelRelTipo.Value;
  Lista := TStringList.Create;
  try
    case Tipo of
      1: DModG.ListaDeItensDeBalancete(tcrBalanceteLoc, True, Lista);
      2: DModG.ListaDeItensDeBalancete(tcrBalanceteWeb, True, Lista);
    end;
    //
    QrEntiCfgRel_01.Close;
    QrEntiCfgRel_01.SQL.Clear;
    QrEntiCfgRel_01.SQL.Add('SELECT cta.Nome NO_Genero, ecr.*, ELT(ecr.RelItem+1,');
    for I := 0 to Lista.Count - 1 do
      QrEntiCfgRel_01.SQL.Add('"' + Lista[I] + '",' + sLineBreak);
    QrEntiCfgRel_01.SQL.Add('"???") NO_RelItem');
    QrEntiCfgRel_01.SQL.Add('FROM enticfgrel ecr');
    QrEntiCfgRel_01.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=ecr.Genero');
    QrEntiCfgRel_01.SQL.Add('WHERE ecr.RelTipo=' + Geral.FF0(Tipo));
    QrEntiCfgRel_01.SQL.Add('AND ecr.Codigo=' + Geral.FF0(QrFiliaisCodigo.Value));
    QrEntiCfgRel_01.SQL.Add('ORDER BY ecr.Ordem');
    UnDmkDAC_PF.AbreQuery(QrEntiCfgRel_01, Dmod.MyDB);
    //
    QrEntiCfgRel_01.Locate('Controle', FControle, []);
  finally
    Lista.Free;
  end;
end;

function TFmEntiCfgRel.ReopenFiliais(Codigo: Integer): Boolean;
begin
  Result := False;
  try
    QrFiliais.Close;
    QrFiliais.SQL.Clear;
    if VAR_KIND_DEPTO = kdUH then
    begin
      QrFiliais.SQL.Add('SELECT DISTINCT ent.Codigo, ent.CliInt Filial,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.IE, ent.RG) IERG,');
      QrFiliais.SQL.Add('FormaSociet, Simples, Atividade');
      QrFiliais.SQL.Add('FROM entidades ent');
      QrFiliais.SQL.Add('WHERE ent.CliInt <> 0');
    end else begin
      QrFiliais.SQL.Add('SELECT DISTINCT ent.Codigo, ent.Filial,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.IE, ent.RG) IERG,');
      QrFiliais.SQL.Add('FormaSociet, Simples, Atividade');
      QrFiliais.SQL.Add('FROM entidades ent');
      QrFiliais.SQL.Add('WHERE ent.Codigo < -10');
    end;
    UnDmkDAC_PF.AbreQuery(QrFiliais, Dmod.MyDB);
    Result := True;
    QrFiliais.Locate('Codigo', Codigo, []);
  except
    Geral.MB_Aviso('N�o foi poss�vel listar as filiais!');
  end;
end;

procedure TFmEntiCfgRel.ReopenMtrzCfgRel_xx(Controle: Integer);
const
  Qualquer = '0'; // = Qualquer empresa (espec�ficos para cada aplica��o)
var
  Lista: TStrings;
  I, Tipo: Integer;
begin
  Tipo := QrMtrzCfgRelRelTipo.Value;
  Lista := TStringList.Create;
  try
    case Tipo of
      1: DModG.ListaDeItensDeBalancete(tcrBalanceteLoc, True, Lista);
      2: DModG.ListaDeItensDeBalancete(tcrBalanceteWeb, True, Lista);
    end;
    //
    QrMtrzCfgRel_01.Close;
    QrMtrzCfgRel_01.SQL.Clear;
    QrMtrzCfgRel_01.SQL.Add('SELECT cta.Nome NO_Genero, ecr.*, ELT(ecr.RelItem+1,');
    for I := 0 to Lista.Count - 1 do
      QrMtrzCfgRel_01.SQL.Add('"' + Lista[I] + '",'+sLineBreak);
    QrMtrzCfgRel_01.SQL.Add('"???") NO_RelItem');
    QrMtrzCfgRel_01.SQL.Add('FROM enticfgrel ecr');
    QrMtrzCfgRel_01.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=ecr.Genero');
    QrMtrzCfgRel_01.SQL.Add('WHERE ecr.RelTipo=' + Geral.FF0(Tipo));
    QrMtrzCfgRel_01.SQL.Add('AND ecr.Codigo=' + Qualquer);
    QrMtrzCfgRel_01.SQL.Add('ORDER BY ecr.Ordem');
    //QrMtrzCfgRel_01.Params[0].AsInteger := QrFiliaisCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrMtrzCfgRel_01, Dmod.MyDB);
    //
    QrMtrzCfgRel_01.Locate('Controle', FControle, []);
  finally
    Lista.Free;
  end;
end;

procedure TFmEntiCfgRel.Retiraitem1Click(Sender: TObject);
var
  Prox: Integer;
begin
  if Geral.MB_Pergunta('Confirma a retirada do item "' +
    QrEntiCfgRel_01RelTitu.Value + '" ?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add(DELETE_FROM + ' enticfgrel WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrEntiCfgRel_01Controle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Prox := UMyMod.ProximoRegistro(QrEntiCfgRel_01, 'Controle',
        QrEntiCfgRel_01Controle.Value);
      //
      OrdenaItens(QrFiliaisCodigo.Value, QrEntiCfgRelRelTipo.Value, Prox);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmEntiCfgRel.Retiraitem2Click(Sender: TObject);
var
  Prox: Integer;
begin
  if Geral.MB_Pergunta('Confirma a retirada do item "' + QrMtrzCfgRel_01RelTitu.Value +
  '" ?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add(DELETE_FROM + ' enticfgrel WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrMtrzCfgRel_01Controle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Prox := UMyMod.ProximoRegistro(QrMtrzCfgRel_01, 'Controle',
        QrMtrzCfgRel_01Controle.Value);
      //
      OrdenaItens(QrFiliaisCodigo.Value, QrMtrzCfgRelRelTipo.Value, Prox);
      //
      QrMtrzCfgRel.Close;
      UnDmkDAC_PF.AbreQuery(QrMtrzCfgRel, Dmod.MyDB);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
