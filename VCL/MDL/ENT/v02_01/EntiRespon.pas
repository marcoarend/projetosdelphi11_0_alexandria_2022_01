unit EntiRespon;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, dmkRadioGroup, ComCtrls, dmkEditDateTimePicker, dmkImage,
  UnDmkEnums;

type
  TFmEntiRespon = class(TForm)
    Panel1: TPanel;
    QrCargos: TmySQLQuery;
    DsCargos: TDataSource;
    CkContinuar: TCheckBox;
    QrCargosCodigo: TIntegerField;
    QrCargosCodUsu: TIntegerField;
    QrCargosNome: TWideStringField;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBCargo: TdmkDBLookupComboBox;
    EdCargo: TdmkEditCB;
    Label1: TLabel;
    dmkValUsu1: TdmkValUsu;
    SpeedButton1: TSpeedButton;
    Label22: TLabel;
    EdObserv: TdmkEdit;
    Label2: TLabel;
    EdOrdemLista: TdmkEdit;
    RGOrdemLista: TdmkRadioGroup;
    TPMandatoIni: TdmkEditDateTimePicker;
    Label8: TLabel;
    TPMandatoFim: TdmkEditDateTimePicker;
    Label9: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel3: TPanel;
    BtOK: TBitBtn;
    Label7: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    QrEntidades: TmySQLQuery;
    IntegerField1: TIntegerField;
    DsEntidades: TDataSource;
    QrEntidadesNome: TWideStringField;
    Label10: TLabel;
    EdNome: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEntiCargos(Codigo: Integer);
  public
    { Public declarations }
    FForm: TForm;
  end;

  var
  FmEntiRespon: TFmEntiRespon;

implementation

uses UnMyObjects, Module, Entidade2, UMySQLModule, UnInternalConsts, Principal,
  MyDBCheck, EntiCargos, DmkDAC_PF;

{$R *.DFM}

procedure TFmEntiRespon.BtOKClick(Sender: TObject);
var
  Nome: String;
  Controle: Integer;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Informe o nome do responsável!') then Exit;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('entirespon', 'Controle', ImgTipo.SQLType,
                EdControle.ValueVariant);
  //
  if UMyMod.ExecSQLInsUpdFm(FmEntiRespon, ImgTipo.SQLType, 'EntiRespon',
  Controle, Dmod.QrUpd) then
  begin
  if FForm <> nil then
      TFmEntidade2(FForm).ReopenEntiRespon(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdControle.ValueVariant   := 0;
      EdCargo.ValueVariant      := 0;
      CBCargo.KeyValue          := Null;
      EdEntidade.ValueVariant   := 0;
      CBEntidade.KeyValue       := Null;
      EdObserv.ValueVariant     := '';
      EdOrdemLista.ValueVariant := EdOrdemLista.ValueVariant + 1;
      EdEntidade.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmEntiRespon.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiRespon.EdEntidadeChange(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := EdEntidade.ValueVariant;
  //
  if Entidade <> 0 then
    EdNome.ValueVariant := QrEntidadesNome.Value;
end;

procedure TFmEntiRespon.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiRespon.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrCargos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmEntiRespon.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiRespon.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    TPMandatoIni.Date := 0;
    TPMandatoFim.Date := 0;
    //
    CkContinuar.Checked := True;
    CkContinuar.Visible := True;
  end else
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
  end;
end;

procedure TFmEntiRespon.MostraEntiCargos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmEntiCargos, FmEntiCargos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEntiCargos.LocCod(Codigo, Codigo);
    FmEntiCargos.ShowModal;
    FmEntiCargos.Destroy;
  end;
end;

procedure TFmEntiRespon.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  MostraEntiCargos(dmkValUsu1.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdCargo, CBCargo, QrCargos, VAR_CADASTRO);
    //
    EdCargo.SetFocus;
  end;
end;

end.
