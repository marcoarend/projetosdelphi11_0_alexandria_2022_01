unit EntiCfgRel_01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, dmkCheckGroup, dmkRadioGroup, ComCtrls, dmkPopOutFntCBox,
  dmkCheckBox, dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TFmEntiCfgRel_01 = class(TForm)
    Panel1: TPanel;
    EdRelTitu: TdmkEdit;
    Label2: TLabel;
    Panel3: TPanel;
    Label5: TLabel;
    EdRelTipo: TdmkEdit;
    Label1: TLabel;
    EdControle: TdmkEdit;
    Label3: TLabel;
    EdOrdem: TdmkEdit;
    Label4: TLabel;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    QrMaxOrd: TmySQLQuery;
    QrMaxOrdItens: TLargeintField;
    RGRelItem: TdmkRadioGroup;
    Pn00: TPanel;
    Pn03: TPanel;
    DsContas: TDataSource;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    Label7: TLabel;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    RGFonteTam: TdmkRadioGroup;
    QrMax: TmySQLQuery;
    QrMaxItens: TLargeintField;
    Pn0: TPanel;
    PGCapa: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    LaCred: TLabel;
    EdCpaLin1MrgSup: TdmkEdit;
    EdCpaLin1AltLin: TdmkEdit;
    Label8: TLabel;
    RGCpaLin1AliTex: TdmkRadioGroup;
    PFCpaLin1FonNom: TdmkPopOutFntCBox;
    Label9: TLabel;
    CBCpaLin1FonTam: TComboBox;
    Label10: TLabel;
    Label11: TLabel;
    PFCpaLin2FonNom: TdmkPopOutFntCBox;
    CBCpaLin2FonTam: TComboBox;
    Label12: TLabel;
    Label13: TLabel;
    EdCpaLin2MrgSup: TdmkEdit;
    EdCpaLin2AltLin: TdmkEdit;
    Label14: TLabel;
    RGCpaLin2AliTex: TdmkRadioGroup;
    Label15: TLabel;
    PFCpaLin3FonNom: TdmkPopOutFntCBox;
    CBCpaLin3FonTam: TComboBox;
    Label16: TLabel;
    Label17: TLabel;
    EdCpaLin3MrgSup: TdmkEdit;
    EdCpaLin3AltLin: TdmkEdit;
    Label18: TLabel;
    RGCpaLin3AliTex: TdmkRadioGroup;
    Label19: TLabel;
    EdCpaImgMrgSup: TdmkEdit;
    EdCpaImgMrgEsq: TdmkEdit;
    Label20: TLabel;
    EdCpaImgAlt: TdmkEdit;
    Label21: TLabel;
    EdCpaImgLar: TdmkEdit;
    Label22: TLabel;
    CkCpaImgProp: TdmkCheckBox;
    CkCpaImgStre: TdmkCheckBox;
    CkCpaImgTran: TdmkCheckBox;
    Label23: TLabel;
    EdCpaImgTranCol: TdmkEdit;
    SpeedButton6: TSpeedButton;
    ColorDialog1: TColorDialog;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGRelItemClick(Sender: TObject);
    procedure EdRelTituKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGFonteTamClick(Sender: TObject);
    procedure EdOrdemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PFCpaLin1FonNomChange(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure PFCpaLin2FonNomChange(Sender: TObject);
    procedure PFCpaLin3FonNomChange(Sender: TObject);
    procedure CkCpaImgTranClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(SQLType: TSQLType);
  public
    { Public declarations }
    FControle: Integer;
    FRelPadrao: Boolean;
  end;

  var
  FmEntiCfgRel_01: TFmEntiCfgRel_01;

implementation

uses
  //{$IfNDef NO_FINANCEIRO}
  //ModuleFin,
  //{$EndIf}
  UnMyObjects, Module, UMySQLModule, ModuleGeral, EntiCfgRel,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmEntiCfgRel_01.BtOKClick(Sender: TObject);
var
  Codigo, RelTipo, Ordem, RelItem, FonteTam, Genero, CpaLin1FonTam,
  CpaLin1MrgSup, CpaLin1AltLin, CpaLin1AliTex, CpaLin2FonTam, CpaLin2MrgSup,
  CpaLin2AltLin, CpaLin2AliTex, CpaLin3FonTam, CpaLin3MrgSup, CpaLin3AltLin,
  CpaLin3AliTex, CpaImgMrgSup, CpaImgMrgEsq, CpaImgAlt, CpaImgLar, CpaImgStre,
  CpaImgProp, CpaImgTran, CpaImgTranCol, MaxOrd: Integer;
  RelTitu, CpaLin1FonNom, CpaLin2FonNom, CpaLin3FonNom: String;
begin
  if MyObjects.FIC(EdOrdem.ValueVariant <= 0, EdOrdem,
    'Ordem inv�lida! N�mero m�nimo = 1') then Exit;
  if MyObjects.FIC(Trim(EdRelTitu.Text) = '', EdRelTitu,
    'Informe o t�tulo!') then Exit;
  QrMaxOrd.Close;
  QrMaxOrd.Params[00].AsInteger := EdRelTipo.ValueVariant;
  QrMaxOrd.Params[01].AsInteger := EdCodigo.ValueVariant;
  UnDmkDAC_PF.AbreQuery(QrMaxOrd, Dmod.MyDB);
  //
  MaxOrd := QrMaxOrdItens.Value;
  if ImgTipo.SQLType = stIns then MaxOrd := MaxOrd + 1;
  if MyObjects.FIC(EdOrdem.ValueVariant > MaxOrd, EdOrdem,
    'Ordem inv�lida! N�mero m�ximo = ' + IntToStr(
    MaxOrd))
  then
    Exit;
  Codigo   := EdCodigo.ValueVariant;
  RelTipo  := EdRelTipo.ValueVariant;
  Ordem    := EdOrdem.ValueVariant;
  RelItem  := RGRelItem.ItemIndex;
  FonteTam := RGFonteTam.ItemIndex;
  Genero   := EdGenero.ValueVariant;
  RelTitu  := EdRelTitu.ValueVariant;
  //Capa balancete Ini
  CpaLin1FonNom := PFCpaLin1FonNom.FonteNome;
  CpaLin1FonTam := Geral.IMV(CBCpaLin1FonTam.Text);
  CpaLin1MrgSup := EdCpaLin1MrgSup.ValueVariant * 1000;
  CpaLin1AltLin := EdCpaLin1AltLin.ValueVariant * 1000;
  CpaLin1AliTex := RGCpaLin1AliTex.ItemIndex;
  //
  CpaLin2FonNom := PFCpaLin2FonNom.FonteNome;
  CpaLin2FonTam := Geral.IMV(CBCpaLin2FonTam.Text);
  CpaLin2MrgSup := EdCpaLin2MrgSup.ValueVariant * 1000;
  CpaLin2AltLin := EdCpaLin2AltLin.ValueVariant * 1000;
  //CpaLin2AliTex := RGCpaLin2AliTex.ItemIndex;
  //
  CpaLin3FonNom := PFCpaLin3FonNom.FonteNome;
  CpaLin3FonTam := Geral.IMV(CBCpaLin3FonTam.Text);
  CpaLin3MrgSup := EdCpaLin3MrgSup.ValueVariant * 1000;
  CpaLin3AltLin := EdCpaLin3AltLin.ValueVariant * 1000;
  CpaLin3AliTex := RGCpaLin3AliTex.ItemIndex;
  //
  CpaImgMrgSup := EdCpaImgMrgSup.ValueVariant * 1000;
  CpaImgMrgEsq := EdCpaImgMrgEsq.ValueVariant * 1000;
  CpaImgAlt    := EdCpaImgAlt.ValueVariant * 1000;
  CpaImgLar    := EdCpaImgLar.ValueVariant * 1000;
  CpaImgStre   := Geral.BoolToInt(CkCpaImgStre.Checked);
  CpaImgProp   := Geral.BoolToInt(CkCpaImgProp.Checked);
  CpaImgTran   := Geral.BoolToInt(CkCpaImgTran.Checked);
  if CkCpaImgTran.Checked then
    CpaImgTranCol := ColorToRGB(EdCpaImgTranCol.Color)
  else
    CpaImgTranCol := 0;
  //Capa balancete Fim
  //
  FControle := EdControle.ValueVariant;
  FControle := UMyMod.BuscaEmLivreY_Def('enticfgrel', 'Controle', ImgTipo.SQLType,
    FControle, EdControle);
  EdControle.ValueVariant := FControle;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'enticfgrel', False, [
    'RelTipo', 'RelItem', 'RelTitu', 'Ordem', 'Genero', 'FonteTam',
    'CpaLin1FonNom', 'CpaLin1FonTam', 'CpaLin1MrgSup', 'CpaLin1AltLin', 'CpaLin1AliTex',
    'CpaLin2FonNom', 'CpaLin2FonTam', 'CpaLin2MrgSup', 'CpaLin2AltLin', 'CpaLin2AliTex',
    'CpaLin3FonNom', 'CpaLin3FonTam', 'CpaLin3MrgSup', 'CpaLin3AltLin', 'CpaLin3AliTex',
    'CpaImgMrgSup', 'CpaImgMrgEsq', 'CpaImgAlt', 'CpaImgLar', 'CpaImgStre',
    'CpaImgProp', 'CpaImgTran', 'CpaImgTranCol', 'Codigo'
  ], ['Controle'],
  [
    RelTipo, RelItem, RelTitu, Ordem, Genero, FonteTam,
    CpaLin1FonNom, CpaLin1FonTam, CpaLin1MrgSup, CpaLin1AltLin, CpaLin1AliTex,
    CpaLin2FonNom, CpaLin2FonTam, CpaLin2MrgSup, CpaLin2AltLin, CpaLin1AliTex,
    CpaLin3FonNom, CpaLin3FonTam, CpaLin3MrgSup, CpaLin3AltLin, CpaLin3AliTex,
    CpaImgMrgSup, CpaImgMrgEsq, CpaImgAlt, CpaImgLar, CpaImgStre,
    CpaImgProp, CpaImgTran, CpaImgTranCol, Codigo
  ], [FControle], True) then
  begin
    if dmkPF.ContinuarInserindo(ImgTipo.SQLType) then
    begin
      EdControle.Text      := '0';
      EdOrdem.ValueVariant := EdOrdem.ValueVariant + 1;
      EdRelTitu.Text       := '';
      RGRelItem.ItemIndex  := 0;
    end else
      Close;
  end;
end;

procedure TFmEntiCfgRel_01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiCfgRel_01.CkCpaImgTranClick(Sender: TObject);
var
  Visi: Boolean;
begin
  Visi := CkCpaImgTran.Checked;
  //
  Label23.Visible         := Visi;
  EdCpaImgTranCol.Visible := Visi;
  SpeedButton6.Visible    := Visi;
end;

procedure TFmEntiCfgRel_01.PFCpaLin1FonNomChange(Sender: TObject);
begin
  Geral.GetFontSizes(PFCpaLin1FonNom.FonteNome, CBCpaLin1FonTam.Items);
end;

procedure TFmEntiCfgRel_01.PFCpaLin2FonNomChange(Sender: TObject);
begin
  Geral.GetFontSizes(PFCpaLin2FonNom.FonteNome, CBCpaLin2FonTam.Items);
end;

procedure TFmEntiCfgRel_01.PFCpaLin3FonNomChange(Sender: TObject);
begin
  Geral.GetFontSizes(PFCpaLin3FonNom.FonteNome, CBCpaLin3FonTam.Items);
end;

procedure TFmEntiCfgRel_01.EdOrdemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    if EdControle.ValueVariant = 0 then
    begin
      QrMax.Close;
      QrMax.Params[00].AsInteger := EdRelTipo.ValueVariant;
      QrMax.Params[01].AsInteger := EdCodigo.ValueVariant;
      UnDmkDAC_PF.AbreQuery(QrMax, Dmod.MyDB);
      //
      EdOrdem.ValueVariant := QrMaxItens.Value + 1;
    end;
  end
end;

procedure TFmEntiCfgRel_01.EdRelTituKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdRelTitu.Text := RGRelItem.Items[RGRelItem.ItemIndex]
  else
  if Key = VK_F5 then
    EdRelTitu.Text :=
    Geral.Maiusculas(RGRelItem.Items[RGRelItem.ItemIndex], False);
end;

procedure TFmEntiCfgRel_01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiCfgRel_01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DModG.ListaDeItensDeBalancete(tcrBalanceteLoc, True, RGRelitem.Items);
  //
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  //
  PGCapa.ActivePageIndex := 0;
end;

procedure TFmEntiCfgRel_01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiCfgRel_01.FormShow(Sender: TObject);
begin
  MostraEdicao(ImgTipo.SQLType);
end;

procedure TFmEntiCfgRel_01.MostraEdicao(SQLType: TSQLType);
{
var
  i: Integer;
}
begin
  if SQLType = stIns then
  begin
    if FRelPadrao then
      EdCodigo.ValueVariant := 0
    else
      EdCodigo.ValueVariant := FmEntiCfgRel.QrFiliaisCodigo.Value;
  end else
  begin
    if FRelPadrao then
      EdCodigo.ValueVariant := FmEntiCfgRel.QrMtrzCfgRel_01Codigo.Value
    else
      EdCodigo.ValueVariant := FmEntiCfgRel.QrEntiCfgRel_01Codigo.Value;
  end;
  //
  if FRelPadrao then
  begin
    Geral.GetFontSizes(FmEntiCfgRel.QrMtrzCfgRel_01CpaLin1FonNom.Value, CBCpaLin1FonTam.Items);
    Geral.GetFontSizes(FmEntiCfgRel.QrMtrzCfgRel_01CpaLin2FonNom.Value, CBCpaLin2FonTam.Items);
    Geral.GetFontSizes(FmEntiCfgRel.QrMtrzCfgRel_01CpaLin3FonNom.Value, CBCpaLin3FonTam.Items);
    //
    EdRelTipo.ValueVariant       := 1;//Tem que ser 1 para aparecer no balancete FmEntiCfgRel.QrMtrzCfgRel_01RelTipo.Value;
    EdControle.ValueVariant      := FmEntiCfgRel.QrMtrzCfgRel_01Controle.Value;
    EdOrdem.ValueVariant         := FmEntiCfgRel.QrMtrzCfgRel_01Ordem.Value;
    RGRelItem.ItemIndex          := FmEntiCfgRel.QrMtrzCfgRel_01RelItem.Value;
    EdRelTitu.ValueVariant       := FmEntiCfgRel.QrMtrzCfgRel_01RelTitu.Value;
    RGFonteTam.ItemIndex         := FmEntiCfgRel.QrMtrzCfgRel_01FonteTam.Value;
    EdGenero.ValueVariant        := FmEntiCfgRel.QrMtrzCfgRel_01Genero.Value;
    CBGenero.KeyValue            := FmEntiCfgRel.QrMtrzCfgRel_01Genero.Value;
    PFCpaLin1FonNom.Font.Name    := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin1FonNom.Value;
    PFCpaLin1FonNom.FonteNome    := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin1FonNom.Value;
    CBCpaLin1FonTam.Text         := Geral.FF0(FmEntiCfgRel.QrMtrzCfgRel_01CpaLin1FonTam.Value);
    EdCpaLin1MrgSup.ValueVariant := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin1MrgSup.Value / 1000;
    EdCpaLin1AltLin.ValueVariant := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin1AltLin.Value / 1000;
    RGCpaLin1AliTex.ItemIndex    := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin1AliTex.Value;
    PFCpaLin2FonNom.Font.Name    := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin2FonNom.Value;
    PFCpaLin2FonNom.FonteNome    := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin2FonNom.Value;
    CBCpaLin2FonTam.Text         := Geral.FF0(FmEntiCfgRel.QrMtrzCfgRel_01CpaLin2FonTam.Value);
    EdCpaLin2MrgSup.ValueVariant := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin2MrgSup.Value / 1000;
    EdCpaLin2AltLin.ValueVariant := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin2AltLin.Value / 1000;
    RGCpaLin2AliTex.ItemIndex    := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin2AliTex.Value;
    PFCpaLin3FonNom.Font.Name    := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin3FonNom.Value;
    PFCpaLin3FonNom.FonteNome    := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin3FonNom.Value;
    CBCpaLin3FonTam.Text         := Geral.FF0(FmEntiCfgRel.QrMtrzCfgRel_01CpaLin3FonTam.Value);
    EdCpaLin3MrgSup.ValueVariant := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin3MrgSup.Value / 1000;
    EdCpaLin3AltLin.ValueVariant := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin3AltLin.Value / 1000;
    RGCpaLin3AliTex.ItemIndex    := FmEntiCfgRel.QrMtrzCfgRel_01CpaLin3AliTex.Value;
    EdCpaImgMrgSup.ValueVariant  := FmEntiCfgRel.QrMtrzCfgRel_01CpaImgMrgSup.Value / 1000;
    EdCpaImgMrgEsq.ValueVariant  := FmEntiCfgRel.QrMtrzCfgRel_01CpaImgMrgSup.Value / 1000;
    EdCpaImgAlt.ValueVariant     := FmEntiCfgRel.QrMtrzCfgRel_01CpaImgAlt.Value / 1000;
    EdCpaImgLar.ValueVariant     := FmEntiCfgRel.QrMtrzCfgRel_01CpaImgLar.Value / 1000;
    CkCpaImgStre.Checked         := Geral.IntToBool(FmEntiCfgRel.QrMtrzCfgRel_01CpaImgStre.Value);
    CkCpaImgProp.Checked         := Geral.IntToBool(FmEntiCfgRel.QrMtrzCfgRel_01CpaImgProp.Value);
    CkCpaImgTran.Checked         := Geral.IntToBool(FmEntiCfgRel.QrMtrzCfgRel_01CpaImgTran.Value);
    EdCpaImgTranCol.ValueVariant := '';
    EdCpaImgTranCol.Color        := FmEntiCfgRel.QrMtrzCfgRel_01CpaImgTranCol.Value;
  end else
  begin
    Geral.GetFontSizes(FmEntiCfgRel.QrEntiCfgRel_01CpaLin1FonNom.Value, CBCpaLin1FonTam.Items);
    Geral.GetFontSizes(FmEntiCfgRel.QrEntiCfgRel_01CpaLin2FonNom.Value, CBCpaLin2FonTam.Items);
    Geral.GetFontSizes(FmEntiCfgRel.QrEntiCfgRel_01CpaLin3FonNom.Value, CBCpaLin3FonTam.Items);
    //
    EdRelTipo.ValueVariant       := 1;//Tem que ser 1 para aparecer no balancete FmEntiCfgRel.QrEntiCfgRel_01RelTipo.Value;
    EdControle.ValueVariant      := FmEntiCfgRel.QrEntiCfgRel_01Controle.Value;
    EdOrdem.ValueVariant         := FmEntiCfgRel.QrEntiCfgRel_01Ordem.Value;
    RGRelItem.ItemIndex          := FmEntiCfgRel.QrEntiCfgRel_01RelItem.Value;
    EdRelTitu.ValueVariant       := FmEntiCfgRel.QrEntiCfgRel_01RelTitu.Value;
    RGFonteTam.ItemIndex         := FmEntiCfgRel.QrEntiCfgRel_01FonteTam.Value;
    EdGenero.ValueVariant        := FmEntiCfgRel.QrEntiCfgRel_01Genero.Value;
    CBGenero.KeyValue            := FmEntiCfgRel.QrEntiCfgRel_01Genero.Value;
    PFCpaLin1FonNom.Font.Name    := FmEntiCfgRel.QrEntiCfgRel_01CpaLin1FonNom.Value;
    PFCpaLin1FonNom.FonteNome    := FmEntiCfgRel.QrEntiCfgRel_01CpaLin1FonNom.Value;
    CBCpaLin1FonTam.Text         := Geral.FF0(FmEntiCfgRel.QrEntiCfgRel_01CpaLin1FonTam.Value);
    EdCpaLin1MrgSup.ValueVariant := FmEntiCfgRel.QrEntiCfgRel_01CpaLin1MrgSup.Value / 1000;
    EdCpaLin1AltLin.ValueVariant := FmEntiCfgRel.QrEntiCfgRel_01CpaLin1AltLin.Value / 1000;
    RGCpaLin1AliTex.ItemIndex    := FmEntiCfgRel.QrEntiCfgRel_01CpaLin1AliTex.Value;
    PFCpaLin2FonNom.Font.Name    := FmEntiCfgRel.QrEntiCfgRel_01CpaLin2FonNom.Value;
    PFCpaLin2FonNom.FonteNome    := FmEntiCfgRel.QrEntiCfgRel_01CpaLin2FonNom.Value;
    CBCpaLin2FonTam.Text         := Geral.FF0(FmEntiCfgRel.QrEntiCfgRel_01CpaLin2FonTam.Value);
    EdCpaLin2MrgSup.ValueVariant := FmEntiCfgRel.QrEntiCfgRel_01CpaLin2MrgSup.Value / 1000;
    EdCpaLin2AltLin.ValueVariant := FmEntiCfgRel.QrEntiCfgRel_01CpaLin2AltLin.Value / 1000;
    RGCpaLin2AliTex.ItemIndex    := FmEntiCfgRel.QrEntiCfgRel_01CpaLin2AliTex.Value;
    PFCpaLin3FonNom.Font.Name    := FmEntiCfgRel.QrEntiCfgRel_01CpaLin3FonNom.Value;
    PFCpaLin3FonNom.FonteNome    := FmEntiCfgRel.QrEntiCfgRel_01CpaLin3FonNom.Value;
    CBCpaLin3FonTam.Text         := Geral.FF0(FmEntiCfgRel.QrEntiCfgRel_01CpaLin3FonTam.Value);
    EdCpaLin3MrgSup.ValueVariant := FmEntiCfgRel.QrEntiCfgRel_01CpaLin3MrgSup.Value / 1000;
    EdCpaLin3AltLin.ValueVariant := FmEntiCfgRel.QrEntiCfgRel_01CpaLin3AltLin.Value / 1000;
    RGCpaLin3AliTex.ItemIndex    := FmEntiCfgRel.QrEntiCfgRel_01CpaLin3AliTex.Value;
    EdCpaImgMrgSup.ValueVariant  := FmEntiCfgRel.QrEntiCfgRel_01CpaImgMrgSup.Value / 1000;
    EdCpaImgMrgEsq.ValueVariant  := FmEntiCfgRel.QrEntiCfgRel_01CpaImgMrgSup.Value / 1000;
    EdCpaImgAlt.ValueVariant     := FmEntiCfgRel.QrEntiCfgRel_01CpaImgAlt.Value / 1000;
    EdCpaImgLar.ValueVariant     := FmEntiCfgRel.QrEntiCfgRel_01CpaImgLar.Value / 1000;
    CkCpaImgStre.Checked         := Geral.IntToBool(FmEntiCfgRel.QrEntiCfgRel_01CpaImgStre.Value);
    CkCpaImgProp.Checked         := Geral.IntToBool(FmEntiCfgRel.QrEntiCfgRel_01CpaImgProp.Value);
    CkCpaImgTran.Checked         := Geral.IntToBool(FmEntiCfgRel.QrEntiCfgRel_01CpaImgTran.Value);
    EdCpaImgTranCol.ValueVariant := '';
    EdCpaImgTranCol.Color        := FmEntiCfgRel.QrEntiCfgRel_01CpaImgTranCol.Value;
  end;
end;

procedure TFmEntiCfgRel_01.RGFonteTamClick(Sender: TObject);
begin
  if RGFonteTam.ItemIndex < 0 then RGFonteTam.ItemIndex := 1;
end;

procedure TFmEntiCfgRel_01.RGRelItemClick(Sender: TObject);
begin
  Pn00.Visible    := False;
  Pn03.Visible    := False;
  PGCapa.Visible  := RGRelItem.ItemIndex = 1;
  EdRelTitu.Texto := '';
  //
  case RGRelItem.ItemIndex of
    0, 1, 2: Pn00.Visible := True;
       3, 4: Pn03.Visible := True;
  end;
  if RGRelItem.ItemIndex = 12 then
    RGFonteTam.Enabled := True
  else begin
    RGFonteTam.Enabled := False;
    RGFonteTam.ItemIndex := 1;
  end;
end;

procedure TFmEntiCfgRel_01.SpeedButton6Click(Sender: TObject);
begin
  if ColorDialog1.Execute then
    EdCpaImgTranCol.Color := ColorDialog1.Color;
end;

end.
