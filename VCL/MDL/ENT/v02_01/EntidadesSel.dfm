object FmEntidadesSel: TFmEntidadesSel
  Left = 396
  Top = 181
  Caption = 'ENT-GEREN-024 :: Seleciona Entidade'
  ClientHeight = 257
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 624
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object LaPrompt: TLabel
      Left = 12
      Top = 4
      Width = 45
      Height = 13
      Caption = 'Entidade:'
    end
    object CBSel: TdmkDBLookupComboBox
      Left = 68
      Top = 20
      Width = 545
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NO_ENT'
      ListSource = DsSel
      TabOrder = 1
      dmkEditCB = EdSel
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdSel: TdmkEditCB
      Left = 12
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdSelChange
      DBLookupComboBox = CBSel
      IgnoraDBLookupComboBox = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 97
    Width = 624
    Height = 52
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object PnDados: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 52
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 61
        Height = 13
        Caption = 'CNPJ / CPF:'
      end
      object Label2: TLabel
        Left = 128
        Top = 4
        Width = 112
        Height = 13
        Caption = 'C'#243'digo do participante: '
        FocusControl = DBEdit2
      end
      object Label3: TLabel
        Left = 552
        Top = 4
        Width = 61
        Height = 13
        Caption = 'C'#243'digo Pa'#237's:'
        FocusControl = DBEdit3
      end
      object DBEdit1: TDBEdit
        Left = 12
        Top = 20
        Width = 112
        Height = 21
        DataField = 'CNPJ_CPF'
        DataSource = DsSel
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 128
        Top = 20
        Width = 421
        Height = 21
        DataField = 'COD_PART'
        DataSource = DsSel
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 552
        Top = 20
        Width = 60
        Height = 21
        DataField = 'CodiPais'
        DataSource = DsSel
        TabOrder = 2
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 528
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 234
        Height = 32
        Caption = 'Seleciona Entidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 234
        Height = 32
        Caption = 'Seleciona Entidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 234
        Height = 32
        Caption = 'Seleciona Entidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 149
    Width = 624
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 193
    Width = 624
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 476
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 6
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 14
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
    end
  end
  object DsSel: TDataSource
    DataSet = QrSel
    Left = 224
    Top = 170
  end
  object QrSel: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSelCalcFields
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF, COD_PART,'
      'IF(Tipo=0, ECodiPais, PCodiPais) CodiPais'
      'FROM entidades '
      'ORDER BY NO_ENT')
    Left = 196
    Top = 170
    object QrSelNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrSelCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSelCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrSelCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrSelCOD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrSelCodiPais: TLargeintField
      FieldName = 'CodiPais'
      Required = True
    end
  end
end
