object FmEntidade2: TFmEntidade2
  Left = 368
  Top = 194
  Caption = 'ENT-GEREN-000 :: Cadastros de Pessoas F'#237'sicas e Jur'#237'dicas'
  ClientHeight = 721
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 113
    Width = 1008
    Height = 608
    Align = alClient
    BevelOuter = bvNone
    Color = clBackground
    ParentBackground = False
    TabOrder = 0
    Visible = False
    object PainelConfirma: TPanel
      Left = 0
      Top = 560
      Width = 1008
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 873
        Top = 0
        Width = 135
        Height = 48
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PCEditGer: TPageControl
      Left = 0
      Top = 0
      Width = 1008
      Height = 435
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Cadastro '
        object PnEdit2: TPanel
          Left = 0
          Top = 7
          Width = 1000
          Height = 400
          Align = alBottom
          BevelOuter = bvNone
          Color = clHotLight
          ParentBackground = False
          TabOrder = 0
          object PnEdit1: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 44
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label7: TLabel
              Left = 92
              Top = 4
              Width = 14
              Height = 13
              Caption = 'ID:'
            end
            object Label29: TLabel
              Left = 8
              Top = 4
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
            end
            object SpeedButton5: TSpeedButton
              Left = 68
              Top = 19
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton5Click
            end
            object LaindIEDest: TLabel
              Left = 289
              Top = 4
              Width = 102
              Height = 13
              Caption = 'Indicador da I.E.: [F4]'
            end
            object Label182: TLabel
              Left = 204
              Top = 3
              Width = 26
              Height = 13
              Caption = 'Sigla:'
            end
            object EdCodigo: TdmkEdit
              Left = 92
              Top = 19
              Width = 56
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clInactiveCaption
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Codigo'
              UpdCampo = 'Codigo'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object RGTipo: TdmkRadioGroup
              Left = 856
              Top = 4
              Width = 133
              Height = 37
              Caption = '  Pessoa: '
              Columns = 2
              Items.Strings = (
                'Jur'#237'dica'
                'F'#237'sica')
              TabOrder = 7
              OnClick = RGTipoClick
              QryCampo = 'Tipo'
              UpdCampo = 'Tipo'
              UpdType = utYes
              OldValor = 0
            end
            object EdCodUsu: TdmkEdit
              Left = 8
              Top = 19
              Width = 56
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CodUsu'
              UpdCampo = 'CodUsu'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object CkAtivo: TdmkCheckBox
              Left = 153
              Top = 21
              Width = 50
              Height = 17
              Caption = 'Ativo'
              TabOrder = 2
              QryCampo = 'Ativo'
              UpdCampo = 'Ativo'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
            object BtImporta: TBitBtn
              Tag = 39
              Left = 812
              Top = 1
              Width = 40
              Height = 40
              TabOrder = 6
              TabStop = False
              OnClick = BtImportaClick
            end
            object EdindIEDest: TdmkEdit
              Left = 288
              Top = 19
              Width = 32
              Height = 21
              Alignment = taRightJustify
              CharCase = ecUpperCase
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'indIEDest'
              UpdCampo = 'indIEDest'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdindIEDestChange
              OnKeyDown = EdindIEDestKeyDown
            end
            object EdindIEDest_TXT: TEdit
              Left = 320
              Top = 19
              Width = 457
              Height = 21
              ReadOnly = True
              TabOrder = 5
              Text = 'N'#227'o definido.'
            end
            object EdSigla: TdmkEdit
              Left = 204
              Top = 19
              Width = 80
              Height = 21
              MaxLength = 6
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Sigla'
              UpdCampo = 'Sigla'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object PnEdits: TPanel
            Left = 0
            Top = 65
            Width = 1000
            Height = 335
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object PnEdit2PF: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 335
              Align = alClient
              BevelOuter = bvNone
              Color = clSkyBlue
              ParentBackground = False
              TabOrder = 0
              Visible = False
              object Panel6: TPanel
                Left = 0
                Top = 0
                Width = 1000
                Height = 52
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label5: TLabel
                  Left = 8
                  Top = 8
                  Width = 31
                  Height = 13
                  Caption = 'Nome:'
                end
                object Label8: TLabel
                  Left = 840
                  Top = 8
                  Width = 23
                  Height = 13
                  Caption = 'CPF:'
                end
                object Label6: TLabel
                  Left = 8
                  Top = 32
                  Width = 38
                  Height = 13
                  Caption = 'Apelido:'
                end
                object Label9: TLabel
                  Left = 480
                  Top = 32
                  Width = 19
                  Height = 13
                  Caption = 'RG:'
                end
                object Label13: TLabel
                  Left = 619
                  Top = 32
                  Width = 70
                  Height = 13
                  Caption = #211'rg'#227'o emissor:'
                end
                object Label35: TLabel
                  Left = 776
                  Top = 32
                  Width = 82
                  Height = 13
                  Caption = 'Data da emiss'#227'o:'
                end
                object Label162: TLabel
                  Left = 756
                  Top = 8
                  Width = 27
                  Height = 13
                  Caption = 'Sexo:'
                end
                object EdNome: TdmkEdit
                  Left = 84
                  Top = 4
                  Width = 666
                  Height = 21
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Nome'
                  UpdCampo = 'Nome'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCPF: TdmkEdit
                  Left = 872
                  Top = 4
                  Width = 112
                  Height = 21
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtCPFJ
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'CPF'
                  UpdCampo = 'CPF'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnExit = EdCPFExit
                end
                object EdApelido: TdmkEdit
                  Left = 84
                  Top = 28
                  Width = 390
                  Height = 21
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Apelido'
                  UpdCampo = 'Apelido'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdRG: TdmkEdit
                  Left = 504
                  Top = 28
                  Width = 112
                  Height = 21
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'RG'
                  UpdCampo = 'RG'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdSSP: TdmkEdit
                  Left = 699
                  Top = 28
                  Width = 73
                  Height = 21
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  MaxLength = 10
                  ParentFont = False
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'SSP'
                  UpdCampo = 'SSP'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object TPDataRG: TdmkEditDateTimePicker
                  Left = 860
                  Top = 28
                  Width = 124
                  Height = 21
                  Date = 39868.000000000000000000
                  Time = 0.515408865729114000
                  TabOrder = 6
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  QryCampo = 'DataRG'
                  UpdCampo = 'DataRG'
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object CBSexo: TComboBox
                  Left = 788
                  Top = 4
                  Width = 49
                  Height = 21
                  Style = csDropDownList
                  TabOrder = 1
                  Items.Strings = (
                    'M'
                    'F')
                end
              end
              object PageControl2: TPageControl
                Left = 0
                Top = 60
                Width = 1000
                Height = 275
                ActivePage = TabSheet2
                Align = alBottom
                TabOrder = 1
                object TabSheet2: TTabSheet
                  Caption = ' Residencial '
                  object Panel8: TPanel
                    Left = 0
                    Top = 0
                    Width = 992
                    Height = 247
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label12: TLabel
                      Left = 8
                      Top = 8
                      Width = 24
                      Height = 13
                      Caption = 'CEP:'
                    end
                    object Label97: TLabel
                      Left = 8
                      Top = 32
                      Width = 77
                      Height = 13
                      Caption = 'Tipo logradouro:'
                    end
                    object Label31: TLabel
                      Left = 218
                      Top = 32
                      Width = 99
                      Height = 13
                      Caption = 'Nome do logradouro:'
                      FocusControl = EdPRua
                    end
                    object Label32: TLabel
                      Left = 500
                      Top = 32
                      Width = 40
                      Height = 13
                      Caption = 'N'#250'mero:'
                      FocusControl = EdPNumero
                    end
                    object Label34: TLabel
                      Left = 770
                      Top = 32
                      Width = 30
                      Height = 13
                      Caption = 'Bairro:'
                      FocusControl = EdPBairro
                    end
                    object Label16: TLabel
                      Left = 593
                      Top = 32
                      Width = 67
                      Height = 13
                      Caption = 'Complemento:'
                      FocusControl = EdPCompl
                    end
                    object Label17: TLabel
                      Left = 8
                      Top = 116
                      Width = 165
                      Height = 13
                      Caption = 'Ponto de refer'#234'ncia (do endere'#231'o):'
                      FocusControl = EdPEndeRef
                    end
                    object Label106: TLabel
                      Left = 42
                      Top = 76
                      Width = 36
                      Height = 13
                      Caption = 'Cidade:'
                    end
                    object Label109: TLabel
                      Left = 674
                      Top = 76
                      Width = 25
                      Height = 13
                      Caption = 'Pa'#237's:'
                    end
                    object Label112: TLabel
                      Left = 244
                      Top = 159
                      Width = 45
                      Height = 13
                      Caption = 'Telefone:'
                    end
                    object Label143: TLabel
                      Left = 8
                      Top = 159
                      Width = 65
                      Height = 13
                      Caption = 'Tipo telefone:'
                    end
                    object Label144: TLabel
                      Left = 360
                      Top = 159
                      Width = 58
                      Height = 13
                      Caption = 'Tipo celular:'
                    end
                    object Label145: TLabel
                      Left = 598
                      Top = 159
                      Width = 35
                      Height = 13
                      Caption = 'Celular:'
                    end
                    object SpeedButton9: TSpeedButton
                      Left = 216
                      Top = 175
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton9Click
                    end
                    object SpeedButton10: TSpeedButton
                      Left = 570
                      Top = 175
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton10Click
                    end
                    object Label14: TLabel
                      Left = 586
                      Top = 116
                      Width = 59
                      Height = 13
                      Caption = 'Nascimento:'
                    end
                    object Label38: TLabel
                      Left = 8
                      Top = 76
                      Width = 17
                      Height = 13
                      Caption = 'UF:'
                    end
                    object Label81: TLabel
                      Left = 716
                      Top = 159
                      Width = 31
                      Height = 13
                      Caption = 'E-mail:'
                      FocusControl = EdPEmail
                    end
                    object Label123: TLabel
                      Left = 716
                      Top = 116
                      Width = 64
                      Height = 13
                      Caption = 'Site da WEB:'
                      FocusControl = EdPSite
                    end
                    object Label146: TLabel
                      Left = 748
                      Top = 8
                      Width = 57
                      Height = 13
                      Caption = 'Estado civil:'
                    end
                    object Label141: TLabel
                      Left = 557
                      Top = 8
                      Width = 71
                      Height = 13
                      Caption = 'Nacionalidade:'
                      FocusControl = EdNacionalid
                    end
                    object Label154: TLabel
                      Left = 268
                      Top = 8
                      Width = 46
                      Height = 13
                      Caption = 'Profiss'#227'o:'
                      FocusControl = EdProfissao
                    end
                    object Label157: TLabel
                      Left = 427
                      Top = 8
                      Width = 31
                      Height = 13
                      Caption = 'Cargo:'
                      FocusControl = EdCargo
                    end
                    object Label160: TLabel
                      Left = 8
                      Top = 199
                      Width = 44
                      Height = 13
                      Caption = 'Empresa:'
                    end
                    object BtCEP_P: TBitBtn
                      Left = 156
                      Top = 5
                      Width = 21
                      Height = 21
                      TabOrder = 1
                      TabStop = False
                      OnClick = BtCEP_PClick
                    end
                    object EdPCEP: TdmkEdit
                      Left = 80
                      Top = 5
                      Width = 72
                      Height = 21
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtCEP
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'PCEP'
                      UpdCampo = 'PCEP'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnEnter = EdPCEPEnter
                      OnExit = EdPCEPExit
                    end
                    object CBPLograd: TdmkDBLookupComboBox
                      Left = 64
                      Top = 48
                      Width = 150
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsPListaLograd
                      TabOrder = 3
                      dmkEditCB = EdPLograd
                      QryCampo = 'PLograd'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdPRua: TdmkEdit
                      Left = 218
                      Top = 48
                      Width = 276
                      Height = 21
                      TabOrder = 4
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'PRua'
                      UpdCampo = 'PRua'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdPNumero: TdmkEdit
                      Left = 500
                      Top = 48
                      Width = 88
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 5
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'PNumero'
                      UpdCampo = 'PNumero'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object EdPBairro: TdmkEdit
                      Left = 770
                      Top = 48
                      Width = 210
                      Height = 21
                      TabOrder = 7
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'PBairro'
                      UpdCampo = 'PBairro'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdPLograd: TdmkEditCB
                      Left = 8
                      Top = 48
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'PLograd'
                      UpdCampo = 'PLograd'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPLograd
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object EdPCompl: TdmkEdit
                      Left = 593
                      Top = 48
                      Width = 172
                      Height = 21
                      TabOrder = 6
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'PCompl'
                      UpdCampo = 'PCompl'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdPEndeRef: TdmkEdit
                      Left = 8
                      Top = 132
                      Width = 572
                      Height = 21
                      TabOrder = 13
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'PEndeRef'
                      UpdCampo = 'PEndeRef'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdPCodMunici: TdmkEditCB
                      Left = 42
                      Top = 92
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 9
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'PCodMunici'
                      UpdCampo = 'PCodMunici'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPCodMunici
                      IgnoraDBLookupComboBox = True
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPCodMunici: TdmkDBLookupComboBox
                      Left = 98
                      Top = 92
                      Width = 570
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsPMunici
                      TabOrder = 10
                      dmkEditCB = EdPCodMunici
                      QryCampo = 'PCodMunici'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdPCodiPais: TdmkEditCB
                      Left = 674
                      Top = 92
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 11
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'PCodiPais'
                      UpdCampo = 'PCodiPais'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPCodiPais
                      IgnoraDBLookupComboBox = True
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPCodiPais: TdmkDBLookupComboBox
                      Left = 730
                      Top = 92
                      Width = 250
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsPBacen_Pais
                      TabOrder = 12
                      dmkEditCB = EdPCodiPais
                      QryCampo = 'PCodiPais'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdPTe1: TdmkEdit
                      Left = 244
                      Top = 175
                      Width = 112
                      Height = 21
                      TabOrder = 18
                      FormatType = dmktfString
                      MskType = fmtTelCurto
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'PTe1'
                      UpdCampo = 'PTe1'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdPTe1Tip: TdmkEditCB
                      Left = 8
                      Top = 175
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 16
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'PTe1Tip'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPTe1Tip
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPTe1Tip: TdmkDBLookupComboBox
                      Left = 64
                      Top = 175
                      Width = 150
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsPTe1Tip
                      TabOrder = 17
                      dmkEditCB = EdPTe1Tip
                      QryCampo = 'PTe1Tip'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdPCelTip: TdmkEditCB
                      Left = 360
                      Top = 175
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 19
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'PCelTip'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPCelTip
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPCelTip: TdmkDBLookupComboBox
                      Left = 417
                      Top = 175
                      Width = 150
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsPCelTip
                      TabOrder = 20
                      dmkEditCB = EdPCelTip
                      QryCampo = 'PCelTip'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdPCel: TdmkEdit
                      Left = 598
                      Top = 175
                      Width = 112
                      Height = 21
                      TabOrder = 21
                      FormatType = dmktfString
                      MskType = fmtTelCurto
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'PCel'
                      UpdCampo = 'PCel'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object TPPNatal: TdmkEditDateTimePicker
                      Left = 586
                      Top = 132
                      Width = 124
                      Height = 21
                      Date = 41367.000000000000000000
                      Time = 0.706863923609489600
                      TabOrder = 14
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      QryCampo = 'PNatal'
                      UpdCampo = 'PNatal'
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                    object EdPUF: TdmkEdit
                      Left = 8
                      Top = 92
                      Width = 28
                      Height = 21
                      CharCase = ecUpperCase
                      TabOrder = 8
                      FormatType = dmktfString
                      MskType = fmtUF
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'PUF'
                      UpdCampo = 'PUF'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnExit = EdPUFExit
                    end
                    object EdPEmail: TdmkEdit
                      Left = 716
                      Top = 175
                      Width = 264
                      Height = 21
                      CharCase = ecLowerCase
                      TabOrder = 22
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = True
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'PEmail'
                      UpdCampo = 'PEmail'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdPSite: TdmkEdit
                      Left = 716
                      Top = 133
                      Width = 264
                      Height = 21
                      TabOrder = 15
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'PSite'
                      UpdCampo = 'PSite'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdEstCivil: TdmkEditCB
                      Left = 811
                      Top = 4
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 28
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'EstCivil'
                      UpdCampo = 'EstCivil'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBEstCivil
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBEstCivil: TdmkDBLookupComboBox
                      Left = 867
                      Top = 4
                      Width = 113
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsEstCivil
                      TabOrder = 29
                      dmkEditCB = EdEstCivil
                      QryCampo = 'EstCivil'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdNacionalid: TdmkEdit
                      Left = 634
                      Top = 4
                      Width = 105
                      Height = 21
                      TabOrder = 27
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'Nacionalid'
                      UpdCampo = 'Nacionalid'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdProfissao: TdmkEdit
                      Left = 325
                      Top = 4
                      Width = 98
                      Height = 21
                      TabOrder = 25
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'Profissao'
                      UpdCampo = 'Profissao'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdCargo: TdmkEdit
                      Left = 462
                      Top = 4
                      Width = 90
                      Height = 21
                      TabOrder = 26
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'Cargo'
                      UpdCampo = 'Cargo'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object CBPEmpresa: TdmkDBLookupComboBox
                      Left = 64
                      Top = 215
                      Width = 173
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'RazaoSocial'
                      ListSource = DsEmpresas
                      TabOrder = 24
                      dmkEditCB = EdPEmpresa
                      QryCampo = 'Empresa'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdPEmpresa: TdmkEditCB
                      Left = 8
                      Top = 215
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 23
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'Empresa'
                      UpdCampo = 'Empresa'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPEmpresa
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                  end
                end
              end
            end
            object PnEdit2PJ: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 335
              Align = alClient
              BevelOuter = bvNone
              Color = clSkyBlue
              ParentBackground = False
              TabOrder = 1
              Visible = False
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 1000
                Height = 52
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label1: TLabel
                  Left = 8
                  Top = 8
                  Width = 66
                  Height = 13
                  Caption = 'Raz'#227'o Social:'
                end
                object Label4: TLabel
                  Left = 8
                  Top = 32
                  Width = 74
                  Height = 13
                  Caption = 'Nome Fantasia:'
                end
                object Label11: TLabel
                  Left = 840
                  Top = 8
                  Width = 30
                  Height = 13
                  Caption = 'CNPJ:'
                end
                object EdRazaoSocial: TdmkEdit
                  Left = 84
                  Top = 4
                  Width = 753
                  Height = 21
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'RazaoSocial'
                  UpdCampo = 'RazaoSocial'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdFantasia: TdmkEdit
                  Left = 84
                  Top = 28
                  Width = 901
                  Height = 21
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Fantasia'
                  UpdCampo = 'Fantasia'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCNPJ: TdmkEdit
                  Left = 872
                  Top = 4
                  Width = 113
                  Height = 21
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtCPFJ
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'CNPJ'
                  UpdCampo = 'CNPJ'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnRedefinido = EdCNPJRedefinido
                end
              end
              object PageControl3: TPageControl
                Left = 0
                Top = 60
                Width = 1000
                Height = 275
                ActivePage = TabSheet3
                Align = alBottom
                TabOrder = 1
                TabStop = False
                object TabSheet3: TTabSheet
                  Caption = ' Comercial '
                  object Panel9: TPanel
                    Left = 0
                    Top = 0
                    Width = 992
                    Height = 247
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label18: TLabel
                      Left = 8
                      Top = 8
                      Width = 24
                      Height = 13
                      Caption = 'CEP:'
                    end
                    object Label19: TLabel
                      Left = 8
                      Top = 32
                      Width = 77
                      Height = 13
                      Caption = 'Tipo logradouro:'
                    end
                    object Label20: TLabel
                      Left = 268
                      Top = 32
                      Width = 99
                      Height = 13
                      Caption = 'Nome do logradouro:'
                      FocusControl = EdERua
                    end
                    object Label21: TLabel
                      Left = 716
                      Top = 32
                      Width = 40
                      Height = 13
                      Caption = 'N'#250'mero:'
                      FocusControl = EdENumero
                    end
                    object Label22: TLabel
                      Left = 8
                      Top = 76
                      Width = 30
                      Height = 13
                      Caption = 'Bairro:'
                      FocusControl = EdEBairro
                    end
                    object Label26: TLabel
                      Left = 808
                      Top = 32
                      Width = 67
                      Height = 13
                      Caption = 'Complemento:'
                      FocusControl = EdECompl
                    end
                    object Label27: TLabel
                      Left = 360
                      Top = 116
                      Width = 165
                      Height = 13
                      Caption = 'Ponto de refer'#234'ncia (do endere'#231'o):'
                      FocusControl = EdEEndeRef
                    end
                    object Label10: TLabel
                      Left = 8
                      Top = 116
                      Width = 19
                      Height = 13
                      Caption = 'I.E.:'
                    end
                    object Label105: TLabel
                      Left = 360
                      Top = 76
                      Width = 36
                      Height = 13
                      Caption = 'Cidade:'
                    end
                    object Label110: TLabel
                      Left = 727
                      Top = 76
                      Width = 25
                      Height = 13
                      Caption = 'Pa'#237's:'
                    end
                    object Label124: TLabel
                      Left = 8
                      Top = 161
                      Width = 338
                      Height = 13
                      Caption = 
                        'ID [F3] [F4] - CNAE (Classifica'#231#227'o Nacional de Atividades Econ'#244'm' +
                        'icas):'
                    end
                    object Label129: TLabel
                      Left = 124
                      Top = 116
                      Width = 39
                      Height = 13
                      Caption = 'I.E.S.T.:'
                    end
                    object Label130: TLabel
                      Left = 240
                      Top = 116
                      Width = 58
                      Height = 13
                      Caption = 'I.M. / NIRE:'
                    end
                    object Label134: TLabel
                      Left = 431
                      Top = 161
                      Width = 230
                      Height = 13
                      Caption = 'ID [F3] [F4] - CRT (C'#243'digo da Regime Tribut'#225'rio):'
                    end
                    object Label148: TLabel
                      Left = 8
                      Top = 205
                      Width = 65
                      Height = 13
                      Caption = 'Tipo telefone:'
                    end
                    object Label149: TLabel
                      Left = 215
                      Top = 205
                      Width = 45
                      Height = 13
                      Caption = 'Telefone:'
                    end
                    object Label150: TLabel
                      Left = 332
                      Top = 205
                      Width = 58
                      Height = 13
                      Caption = 'Tipo celular:'
                    end
                    object Label151: TLabel
                      Left = 536
                      Top = 205
                      Width = 35
                      Height = 13
                      Caption = 'Celular:'
                    end
                    object SpeedButton7: TSpeedButton
                      Left = 189
                      Top = 221
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton7Click
                    end
                    object SpeedButton8: TSpeedButton
                      Left = 512
                      Top = 221
                      Width = 20
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton8Click
                    end
                    object Label45: TLabel
                      Left = 857
                      Top = 116
                      Width = 51
                      Height = 13
                      Caption = 'Funda'#231#227'o:'
                    end
                    object LaFilial: TLabel
                      Left = 946
                      Top = 161
                      Width = 23
                      Height = 13
                      Caption = 'Filial:'
                      Visible = False
                    end
                    object Label24: TLabel
                      Left = 325
                      Top = 76
                      Width = 17
                      Height = 13
                      Caption = 'UF:'
                    end
                    object Label79: TLabel
                      Left = 680
                      Top = 161
                      Width = 64
                      Height = 13
                      Caption = 'Site da WEB:'
                      FocusControl = EdESite
                    end
                    object Label111: TLabel
                      Left = 772
                      Top = 205
                      Width = 31
                      Height = 13
                      Caption = 'E-mail:'
                      FocusControl = EdEEmail
                    end
                    object Label155: TLabel
                      Left = 654
                      Top = 205
                      Width = 20
                      Height = 13
                      Caption = 'Fax:'
                      FocusControl = EdEFax
                    end
                    object BtCEP_E: TBitBtn
                      Left = 156
                      Top = 4
                      Width = 21
                      Height = 21
                      Caption = '...'
                      TabOrder = 1
                      TabStop = False
                      OnClick = BtCEP_EClick
                    end
                    object EdECep: TdmkEdit
                      Left = 80
                      Top = 4
                      Width = 72
                      Height = 21
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtCEP
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'ECEP'
                      UpdCampo = 'ECEP'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnEnter = EdECepEnter
                      OnExit = EdECepExit
                    end
                    object CBELograd: TdmkDBLookupComboBox
                      Left = 64
                      Top = 48
                      Width = 200
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsEListaLograd
                      TabOrder = 3
                      dmkEditCB = EdELograd
                      QryCampo = 'ELograd'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdERua: TdmkEdit
                      Left = 268
                      Top = 48
                      Width = 444
                      Height = 21
                      TabOrder = 4
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'ERua'
                      UpdCampo = 'ERua'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdENumero: TdmkEdit
                      Left = 716
                      Top = 48
                      Width = 88
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 5
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'ENumero'
                      UpdCampo = 'ENumero'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object EdEBairro: TdmkEdit
                      Left = 8
                      Top = 92
                      Width = 312
                      Height = 21
                      TabOrder = 7
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'EBairro'
                      UpdCampo = 'EBairro'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdECompl: TdmkEdit
                      Left = 809
                      Top = 48
                      Width = 171
                      Height = 21
                      TabOrder = 6
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'ECompl'
                      UpdCampo = 'ECompl'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdEEndeRef: TdmkEdit
                      Left = 360
                      Top = 132
                      Width = 492
                      Height = 21
                      TabOrder = 16
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'EEndeRef'
                      UpdCampo = 'EEndeRef'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdIE: TdmkEdit
                      Left = 8
                      Top = 132
                      Width = 112
                      Height = 21
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 13
                      FormatType = dmktfString
                      MskType = fmtIE
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'IE'
                      UpdCampo = 'IE'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdECodMunici: TdmkEditCB
                      Left = 360
                      Top = 92
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 9
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'ECodMunici'
                      UpdCampo = 'ECodMunici'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBECodMunici
                      IgnoraDBLookupComboBox = True
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBECodMunici: TdmkDBLookupComboBox
                      Left = 417
                      Top = 92
                      Width = 305
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsEMunici
                      TabOrder = 10
                      dmkEditCB = EdECodMunici
                      QryCampo = 'ECodMunici'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdECodiPais: TdmkEditCB
                      Left = 727
                      Top = 92
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 11
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'ECodiPais'
                      UpdCampo = 'ECodiPais'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBECodiPais
                      IgnoraDBLookupComboBox = True
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBECodiPais: TdmkDBLookupComboBox
                      Left = 784
                      Top = 92
                      Width = 197
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsEBacen_Pais
                      TabOrder = 12
                      dmkEditCB = EdECodiPais
                      QryCampo = 'ECodiPais'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdCodCNAE: TdmkEditCB
                      Left = 8
                      Top = 177
                      Width = 56
                      Height = 21
                      TabOrder = 18
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'CNAE'
                      UpdCampo = 'CNAE'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = '0'
                      ValWarn = False
                      OnKeyDown = EdCodCNAEKeyDown
                      DBLookupComboBox = CBCodCNAE
                      IgnoraDBLookupComboBox = True
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBCodCNAE: TdmkDBLookupComboBox
                      Left = 65
                      Top = 177
                      Width = 340
                      Height = 21
                      KeyField = 'CodAlf'
                      ListField = 'Nome'
                      ListSource = DsCNAE21Cad
                      TabOrder = 19
                      dmkEditCB = EdCodCNAE
                      QryCampo = 'CNAE'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object BitBtn1: TBitBtn
                      Left = 408
                      Top = 177
                      Width = 20
                      Height = 21
                      Caption = '...'
                      TabOrder = 20
                      TabStop = False
                      OnClick = BitBtn1Click
                    end
                    object EdELograd: TdmkEditCB
                      Left = 8
                      Top = 48
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'ELograd'
                      UpdCampo = 'ELograd'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBELograd
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object EdIEST: TdmkEdit
                      Left = 124
                      Top = 132
                      Width = 112
                      Height = 21
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 14
                      FormatType = dmktfString
                      MskType = fmtIE
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'IEST'
                      UpdCampo = 'IEST'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdNIRE: TdmkEdit
                      Left = 241
                      Top = 132
                      Width = 112
                      Height = 21
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 15
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'NIRE'
                      UpdCampo = 'NIRE'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object Memo1: TMemo
                      Left = 467
                      Top = 177
                      Width = 207
                      Height = 21
                      ReadOnly = True
                      TabOrder = 22
                    end
                    object EdCRT: TdmkEdit
                      Left = 431
                      Top = 177
                      Width = 35
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 21
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'CRT'
                      UpdCampo = 'CRT'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdCRTChange
                      OnKeyDown = EdCRTKeyDown
                    end
                    object EdETe1Tip: TdmkEditCB
                      Left = 8
                      Top = 221
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 25
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'ETe1Tip'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBETe1Tip
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBETe1Tip: TdmkDBLookupComboBox
                      Left = 64
                      Top = 221
                      Width = 122
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsETe1Tip
                      TabOrder = 26
                      dmkEditCB = EdETe1Tip
                      QryCampo = 'ETe1Tip'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdETe1: TdmkEdit
                      Left = 219
                      Top = 221
                      Width = 112
                      Height = 21
                      TabOrder = 27
                      FormatType = dmktfString
                      MskType = fmtTelCurto
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'ETe1'
                      UpdCampo = 'ETe1'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdECelTip: TdmkEditCB
                      Left = 332
                      Top = 221
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 28
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'ECelTip'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBECelTip
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBECelTip: TdmkDBLookupComboBox
                      Left = 388
                      Top = 221
                      Width = 121
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsECelTip
                      TabOrder = 29
                      dmkEditCB = EdECelTip
                      QryCampo = 'ECelTip'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdECel: TdmkEdit
                      Left = 536
                      Top = 221
                      Width = 112
                      Height = 21
                      TabOrder = 30
                      FormatType = dmktfString
                      MskType = fmtTelCurto
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'ECel'
                      UpdCampo = 'ECel'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object TPENatal: TdmkEditDateTimePicker
                      Left = 857
                      Top = 132
                      Width = 123
                      Height = 21
                      Date = 41367.000000000000000000
                      Time = 0.705797928239917400
                      TabOrder = 17
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      QryCampo = 'ENatal'
                      UpdCampo = 'ENatal'
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                    object EdFilial: TdmkEdit
                      Left = 946
                      Top = 177
                      Width = 34
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 24
                      Visible = False
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'Filial'
                      UpdCampo = 'Filial'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object EdEUF: TdmkEdit
                      Left = 325
                      Top = 92
                      Width = 28
                      Height = 21
                      CharCase = ecUpperCase
                      TabOrder = 8
                      FormatType = dmktfString
                      MskType = fmtUF
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'EUF'
                      UpdCampo = 'EUF'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnChange = EdEUFChange
                      OnExit = EdEUFExit
                    end
                    object EdESite: TdmkEdit
                      Left = 680
                      Top = 177
                      Width = 260
                      Height = 21
                      TabOrder = 23
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'ESite'
                      UpdCampo = 'ESite'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdEEmail: TdmkEdit
                      Left = 772
                      Top = 221
                      Width = 210
                      Height = 21
                      CharCase = ecLowerCase
                      TabOrder = 32
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = True
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'EEmail'
                      UpdCampo = 'EEmail'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdEFax: TdmkEdit
                      Left = 654
                      Top = 221
                      Width = 113
                      Height = 21
                      TabOrder = 31
                      FormatType = dmktfString
                      MskType = fmtTelCurto
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'EFax'
                      UpdCampo = 'EFax'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                  end
                end
              end
            end
          end
        end
      end
      object TabSheet8: TTabSheet
        Caption = ' Tipo '
        ImageIndex = 1
        object Panel21: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 407
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label127: TLabel
            Left = 4
            Top = 95
            Width = 97
            Height = 13
            Caption = 'Carteira preferencial:'
          end
          object Label139: TLabel
            Left = 4
            Top = 136
            Width = 55
            Height = 13
            Caption = 'SUFRAMA:'
            FocusControl = EdSUFRAMA
          end
          object Label137: TLabel
            Left = 180
            Top = 134
            Width = 139
            Height = 13
            Caption = 'C'#243'd. participante SPED EFD:'
            FocusControl = EdCOD_PART
          end
          object GroupBox4: TGroupBox
            Left = 4
            Top = 4
            Width = 636
            Height = 89
            Caption = ' Tipo de cadastro: '
            TabOrder = 0
            object CkCliente1_0: TdmkCheckBox
              Left = 8
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Cliente1'
              TabOrder = 0
              QryCampo = 'Cliente1'
              UpdCampo = 'Cliente1'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece1_0: TdmkCheckBox
              Left = 164
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Fornece 1'
              TabOrder = 4
              QryCampo = 'Fornece1'
              UpdCampo = 'Fornece1'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece2_0: TdmkCheckBox
              Left = 164
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Fornece 2'
              TabOrder = 5
              Visible = False
              QryCampo = 'Fornece2'
              UpdCampo = 'Fornece2'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece3_0: TdmkCheckBox
              Left = 164
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Fornece 3'
              TabOrder = 6
              Visible = False
              QryCampo = 'Fornece3'
              UpdCampo = 'Fornece3'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece4_0: TdmkCheckBox
              Left = 164
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Fornece 4'
              TabOrder = 7
              Visible = False
              QryCampo = 'Fornece4'
              UpdCampo = 'Fornece4'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkTerceiro_0: TdmkCheckBox
              Left = 476
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Terceiro'
              TabOrder = 12
              QryCampo = 'Terceiro'
              UpdCampo = 'Terceiro'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkCliente2_0: TdmkCheckBox
              Left = 8
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Cliente 2'
              TabOrder = 1
              Visible = False
              QryCampo = 'Cliente2'
              UpdCampo = 'Cliente2'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece5_0: TdmkCheckBox
              Left = 320
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Fornece 5'
              TabOrder = 8
              Visible = False
              QryCampo = 'Fornece5'
              UpdCampo = 'Fornece5'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece6_0: TdmkCheckBox
              Left = 320
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Fornece 6'
              TabOrder = 9
              Visible = False
              QryCampo = 'Fornece6'
              UpdCampo = 'Fornece6'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkCliente4_0: TdmkCheckBox
              Left = 8
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Cliente 4'
              TabOrder = 3
              Visible = False
              QryCampo = 'Cliente4'
              UpdCampo = 'Cliente4'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkCliente3_0: TdmkCheckBox
              Left = 8
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Cliente 3'
              TabOrder = 2
              Visible = False
              QryCampo = 'Cliente3'
              UpdCampo = 'Cliente3'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece8_0: TdmkCheckBox
              Left = 320
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Fornece 8'
              TabOrder = 11
              Visible = False
              QryCampo = 'Fornece8'
              UpdCampo = 'Fornece8'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece7_0: TdmkCheckBox
              Left = 320
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Fornece 7'
              TabOrder = 10
              Visible = False
              QryCampo = 'Fornece7'
              UpdCampo = 'Fornece7'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
          end
          object EdCarteira: TdmkEditCB
            Left = 4
            Top = 111
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CartPref'
            UpdCampo = 'CartPref'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCarteira
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCarteira: TdmkDBLookupComboBox
            Left = 62
            Top = 111
            Width = 578
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCarteiras
            TabOrder = 2
            dmkEditCB = EdCarteira
            QryCampo = 'CartPref'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdSUFRAMA: TdmkEdit
            Left = 4
            Top = 150
            Width = 172
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SUFRAMA'
            UpdCampo = 'SUFRAMA'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCOD_PART: TdmkEdit
            Left = 180
            Top = 150
            Width = 172
            Height = 21
            MaxLength = 60
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'COD_PART'
            UpdCampo = 'COD_PART'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object RGMadeBy: TdmkRadioGroup
            Left = 4
            Top = 176
            Width = 637
            Height = 41
            Caption = ' Fornecimento de produtos: '
            Columns = 3
            Items.Strings = (
              'Indefinido'
              'Fabrica'#231#227'o Pr'#243'pria'
              'Revenda ')
            TabOrder = 5
            QryCampo = 'MadeBy'
            UpdCampo = 'MadeBy'
            UpdType = utYes
            OldValor = 0
          end
        end
      end
      object TabSheet13: TTabSheet
        Caption = ' Endere'#231'o de cobran'#231'a '
        ImageIndex = 2
        object Panel17: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 407
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label84: TLabel
            Left = 8
            Top = 8
            Width = 24
            Height = 13
            Caption = 'CEP:'
          end
          object Label85: TLabel
            Left = 8
            Top = 32
            Width = 77
            Height = 13
            Caption = 'Tipo logradouro:'
          end
          object Label86: TLabel
            Left = 268
            Top = 32
            Width = 99
            Height = 13
            Caption = 'Nome do logradouro:'
            FocusControl = EdERua
          end
          object Label87: TLabel
            Left = 716
            Top = 32
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
            FocusControl = EdENumero
          end
          object Label88: TLabel
            Left = 8
            Top = 76
            Width = 30
            Height = 13
            Caption = 'Bairro:'
            FocusControl = EdEBairro
          end
          object Label92: TLabel
            Left = 808
            Top = 32
            Width = 67
            Height = 13
            Caption = 'Complemento:'
            FocusControl = EdECompl
          end
          object Label93: TLabel
            Left = 8
            Top = 116
            Width = 55
            Height = 13
            Caption = 'Refer'#234'ncia:'
            FocusControl = EdEEndeRef
          end
          object Label107: TLabel
            Left = 357
            Top = 76
            Width = 36
            Height = 13
            Caption = 'Cidade:'
          end
          object Label90: TLabel
            Left = 324
            Top = 76
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object BtCEP_C: TBitBtn
            Left = 156
            Top = 4
            Width = 21
            Height = 21
            Caption = '...'
            TabOrder = 11
            TabStop = False
            OnClick = BtCEP_CClick
          end
          object EdCCep: TdmkEdit
            Left = 80
            Top = 4
            Width = 72
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtCEP
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CCEP'
            UpdCampo = 'CCEP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnEnter = EdCCepEnter
            OnExit = EdCCepExit
          end
          object CBCLograd: TdmkDBLookupComboBox
            Left = 64
            Top = 48
            Width = 200
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCListaLograd
            TabOrder = 2
            dmkEditCB = EdCLograd
            QryCampo = 'CLograd'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCRua: TdmkEdit
            Left = 268
            Top = 48
            Width = 444
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CRua'
            UpdCampo = 'CRua'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCNumero: TdmkEdit
            Left = 716
            Top = 48
            Width = 88
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CNumero'
            UpdCampo = 'CNumero'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCBairro: TdmkEdit
            Left = 8
            Top = 92
            Width = 312
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CBairro'
            UpdCampo = 'CBairro'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCLograd: TdmkEditCB
            Left = 8
            Top = 48
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CLograd'
            UpdCampo = 'CLograd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCLograd
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdCCompl: TdmkEdit
            Left = 808
            Top = 48
            Width = 172
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CCompl'
            UpdCampo = 'CCompl'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCEndeRef: TdmkEdit
            Left = 8
            Top = 132
            Width = 972
            Height = 21
            TabOrder = 10
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CEndeRef'
            UpdCampo = 'CEndeRef'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCCodMunici: TdmkEditCB
            Left = 357
            Top = 92
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CCodMunici'
            UpdCampo = 'CCodMunici'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCCodMunici
            IgnoraDBLookupComboBox = True
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCCodMunici: TdmkDBLookupComboBox
            Left = 413
            Top = 92
            Width = 567
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCMunici
            TabOrder = 9
            dmkEditCB = EdCCodMunici
            QryCampo = 'CCodMunici'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCUF: TdmkEdit
            Left = 324
            Top = 92
            Width = 28
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CUF'
            UpdCampo = 'CUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdCUFExit
          end
        end
      end
      object TabSheet14: TTabSheet
        Caption = ' Endere'#231'o de entrega '
        ImageIndex = 3
        object Panel20: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 407
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label94: TLabel
            Left = 8
            Top = 32
            Width = 24
            Height = 13
            Caption = 'CEP:'
          end
          object Label95: TLabel
            Left = 112
            Top = 32
            Width = 77
            Height = 13
            Caption = 'Tipo logradouro:'
          end
          object Label96: TLabel
            Left = 372
            Top = 32
            Width = 99
            Height = 13
            Caption = 'Nome do logradouro:'
            FocusControl = EdERua
          end
          object Label98: TLabel
            Left = 716
            Top = 32
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
            FocusControl = EdLNumero
          end
          object Label99: TLabel
            Left = 8
            Top = 76
            Width = 30
            Height = 13
            Caption = 'Bairro:'
            FocusControl = EdLBairro
          end
          object Label103: TLabel
            Left = 808
            Top = 32
            Width = 67
            Height = 13
            Caption = 'Complemento:'
            FocusControl = EdLCompl
          end
          object Label104: TLabel
            Left = 308
            Top = 116
            Width = 55
            Height = 13
            Caption = 'Refer'#234'ncia:'
            FocusControl = EdLEndeRef
          end
          object Label108: TLabel
            Left = 357
            Top = 76
            Width = 36
            Height = 13
            Caption = 'Cidade:'
          end
          object Label113: TLabel
            Left = 8
            Top = 8
            Width = 30
            Height = 13
            Caption = 'CNPJ:'
          end
          object Label101: TLabel
            Left = 324
            Top = 76
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label164: TLabel
            Left = 7
            Top = 116
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
          end
          object Label165: TLabel
            Left = 164
            Top = 8
            Width = 53
            Height = 13
            Caption = '...ou...CPF:'
          end
          object Label166: TLabel
            Left = 360
            Top = 8
            Width = 112
            Height = 13
            Caption = 'Nome ou Raz'#227'o Social:'
            FocusControl = EdERua
          end
          object Label167: TLabel
            Left = 184
            Top = 156
            Width = 31
            Height = 13
            Caption = 'E-mail:'
            FocusControl = EdLEmail
          end
          object Label168: TLabel
            Left = 680
            Top = 156
            Width = 19
            Height = 13
            Caption = 'I.E.:'
          end
          object Label175: TLabel
            Left = 7
            Top = 156
            Width = 87
            Height = 13
            Caption = 'Telefone principal:'
          end
          object BtCEP_L: TBitBtn
            Left = 84
            Top = 48
            Width = 21
            Height = 21
            Caption = '...'
            TabOrder = 4
            TabStop = False
            OnClick = BtCEP_LClick
          end
          object EdLCep: TdmkEdit
            Left = 8
            Top = 48
            Width = 72
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtCEP
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'LCEP'
            UpdCampo = 'LCEP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnEnter = EdLCepEnter
            OnExit = EdLCepExit
          end
          object CBLLograd: TdmkDBLookupComboBox
            Left = 168
            Top = 48
            Width = 200
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsLListaLograd
            TabOrder = 6
            dmkEditCB = EdLLograd
            QryCampo = 'CLograd'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdLRua: TdmkEdit
            Left = 372
            Top = 48
            Width = 341
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'LRua'
            UpdCampo = 'LRua'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLNumero: TdmkEdit
            Left = 716
            Top = 48
            Width = 88
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'LNumero'
            UpdCampo = 'LNumero'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdLBairro: TdmkEdit
            Left = 8
            Top = 92
            Width = 312
            Height = 21
            TabOrder = 10
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'LBairro'
            UpdCampo = 'LBairro'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLLograd: TdmkEditCB
            Left = 112
            Top = 48
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'LLograd'
            UpdCampo = 'LLograd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBLLograd
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdLCompl: TdmkEdit
            Left = 808
            Top = 48
            Width = 172
            Height = 21
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'LCompl'
            UpdCampo = 'LCompl'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLEndeRef: TdmkEdit
            Left = 308
            Top = 132
            Width = 673
            Height = 21
            TabOrder = 16
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'LEndeRef'
            UpdCampo = 'LEndeRef'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLCodMunici: TdmkEditCB
            Left = 357
            Top = 92
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'LCodMunici'
            UpdCampo = 'LCodMunici'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBLCodMunici
            IgnoraDBLookupComboBox = True
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBLCodMunici: TdmkDBLookupComboBox
            Left = 414
            Top = 92
            Width = 566
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsLMunici
            TabOrder = 13
            dmkEditCB = EdLCodMunici
            QryCampo = 'LCodMunici'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdL_CNPJ: TdmkEdit
            Left = 40
            Top = 4
            Width = 113
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'L_CNPJ'
            UpdCampo = 'L_CNPJ'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnRedefinido = EdL_CNPJRedefinido
          end
          object RGL_Ativo: TdmkRadioGroup
            Left = 831
            Top = 157
            Width = 149
            Height = 37
            Caption = ' Entregar neste endere'#231'o?: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o'
              'Sim')
            TabOrder = 20
            QryCampo = 'L_Ativo'
            UpdCampo = 'L_Ativo'
            UpdType = utYes
            OldValor = 0
          end
          object EdLUF: TdmkEdit
            Left = 324
            Top = 92
            Width = 28
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'LUF'
            UpdCampo = 'LUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdLUFExit
          end
          object EdLCodiPais: TdmkEditCB
            Left = 7
            Top = 132
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 14
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'LCodiPais'
            UpdCampo = 'LCodiPais'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBLCodiPais
            IgnoraDBLookupComboBox = True
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBLCodiPais: TdmkDBLookupComboBox
            Left = 64
            Top = 132
            Width = 237
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsLBacen_Pais
            TabOrder = 15
            dmkEditCB = EdLCodiPais
            QryCampo = 'LCodiPais'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdL_CPF: TdmkEdit
            Left = 232
            Top = 4
            Width = 112
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'L_CPF'
            UpdCampo = 'L_CPF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnRedefinido = EdL_CPFRedefinido
          end
          object EdL_Nome: TdmkEdit
            Left = 488
            Top = 4
            Width = 492
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'L_Nome'
            UpdCampo = 'L_Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLEmail: TdmkEdit
            Left = 184
            Top = 172
            Width = 493
            Height = 21
            CharCase = ecLowerCase
            TabOrder = 18
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = True
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'LEmail'
            UpdCampo = 'LEmail'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdL_IE: TdmkEdit
            Left = 680
            Top = 172
            Width = 141
            Height = 21
            TabOrder = 19
            FormatType = dmktfString
            MskType = fmtIE
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'L_IE'
            UpdCampo = 'L_IE'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLTel: TdmkEdit
            Left = 8
            Top = 172
            Width = 173
            Height = 21
            TabOrder = 17
            FormatType = dmktfString
            MskType = fmtTelCurto
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'LTel'
            UpdCampo = 'LTel'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object TabSheet18: TTabSheet
        Caption = 'Observa'#231#245'es'
        ImageIndex = 4
        object MeObservacoes_Edit: TdmkMemo
          Left = 0
          Top = 29
          Width = 1000
          Height = 378
          Align = alClient
          TabOrder = 0
          UpdCampo = 'Observacoes'
          UpdType = utYes
        end
        object Panel32: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label176: TLabel
            Left = 8
            Top = 8
            Width = 80
            Height = 13
            Caption = 'Limite de cr'#233'dito:'
          end
          object SbLimiCred: TSpeedButton
            Left = 236
            Top = 4
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SbLimiCredClick
          end
          object EdLimiCred: TdmkEdit
            Left = 100
            Top = 4
            Width = 134
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'LimiCred'
            UpdCampo = 'LimiCred'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object TabSheet22: TTabSheet
        Caption = ' Estrangeiro | Cont'#225'bil'
        ImageIndex = 5
        object Panel23: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 407
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object CkEstrangDef: TdmkCheckBox
            Left = 8
            Top = 8
            Width = 97
            Height = 17
            Caption = 'Estrangeiro.'
            TabOrder = 0
            OnClick = CkEstrangDefClick
            QryCampo = 'EstrangDef'
            UpdCampo = 'EstrangDef'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
          object GBEstrangeiro: TGroupBox
            Left = 12
            Top = 28
            Width = 977
            Height = 133
            Caption = '  Dados espec'#237'ficos de estrangeiro: '
            TabOrder = 1
            Visible = False
            object Panel24: TPanel
              Left = 2
              Top = 15
              Width = 973
              Height = 116
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label135: TLabel
                Left = 8
                Top = 68
                Width = 111
                Height = 13
                Caption = 'N'#250'mero do documento:'
              end
              object RGEstrangTip: TdmkRadioGroup
                Left = 8
                Top = 0
                Width = 957
                Height = 65
                Caption = ' Tipo de documento: '
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o definido'
                  'Passaporte')
                TabOrder = 0
                QryCampo = 'EstrangTip'
                UpdCampo = 'EstrangTip'
                UpdType = utYes
                OldValor = 0
              end
              object EdEstrangNum: TdmkEdit
                Left = 8
                Top = 84
                Width = 264
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'EstrangNum'
                UpdCampo = 'EstrangNum'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object Panel35: TPanel
            Left = 16
            Top = 180
            Width = 977
            Height = 101
            TabOrder = 2
            object Label180: TLabel
              Left = 4
              Top = 4
              Width = 72
              Height = 13
              Caption = 'Grupo cont'#225'bil:'
            end
            object Label181: TLabel
              Left = 4
              Top = 44
              Width = 142
              Height = 13
              Caption = 'Reduzido do plano de contas:'
            end
            object SbCtbCadGru: TSpeedButton
              Left = 576
              Top = 20
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbCtbCadGruClick
            end
            object SbCtbPlaCta: TSpeedButton
              Left = 576
              Top = 60
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbCtbPlaCtaClick
            end
            object EdCtbCadGru: TdmkEditCB
              Left = 4
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CtbCadGru'
              UpdCampo = 'CtbCadGru'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCtbCadGru
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCtbCadGru: TdmkDBLookupComboBox
              Left = 60
              Top = 20
              Width = 513
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCtbCadGru
              TabOrder = 1
              dmkEditCB = EdCtbCadGru
              QryCampo = 'CtbCadGru'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdCtbPlaCta: TdmkEditCB
              Left = 4
              Top = 60
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CtbPlaCta'
              UpdCampo = 'CtbPlaCta'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCtbPlaCta
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCtbPlaCta: TdmkDBLookupComboBox
              Left = 60
              Top = 60
              Width = 513
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsPlaAllCad
              TabOrder = 3
              dmkEditCB = EdCtbPlaCta
              QryCampo = 'CtbPlaCta'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 113
    Width = 1008
    Height = 608
    Align = alClient
    BevelOuter = bvNone
    Color = clDefault
    ParentBackground = False
    TabOrder = 1
    object Panel11: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 428
      Align = alTop
      BevelOuter = bvNone
      Color = clHotLight
      ParentBackground = False
      TabOrder = 1
      object Panel12: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 98
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label2: TLabel
          Left = 92
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label3: TLabel
          Left = 8
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object SpeedButton6: TSpeedButton
          Left = 68
          Top = 19
          Width = 21
          Height = 21
          Caption = '...'
        end
        object DBLaindIEDest: TLabel
          Left = 288
          Top = 3
          Width = 102
          Height = 13
          Caption = 'Indicador da I.E.: [F4]'
        end
        object Label183: TLabel
          Left = 200
          Top = 4
          Width = 26
          Height = 13
          Caption = 'Sigla:'
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 92
          Top = 19
          Width = 56
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsEntidades
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaption
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Alignment = taRightJustify
        end
        object dmkRadioGroup1: TDBRadioGroup
          Left = 856
          Top = 4
          Width = 133
          Height = 37
          Caption = '  Pessoa: '
          Columns = 2
          DataField = 'Tipo'
          DataSource = DsEntidades
          Items.Strings = (
            'Jur'#237'dica'
            'F'#237'sica')
          TabOrder = 3
          Values.Strings = (
            '0'
            '1')
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 8
          Top = 19
          Width = 56
          Height = 21
          DataField = 'Codigo'
          DataSource = DsEntidades
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBCheckBox2: TDBCheckBox
          Left = 153
          Top = 21
          Width = 50
          Height = 17
          Caption = 'Ativo'
          DataField = 'Ativo'
          DataSource = DsEntidades
          TabOrder = 2
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBEdindIEDest_TXT: TEdit
          Left = 320
          Top = 20
          Width = 320
          Height = 21
          ReadOnly = True
          TabOrder = 4
          Text = 'N'#227'o definido.'
        end
        object DBEdindIEDest: TDBEdit
          Left = 288
          Top = 20
          Width = 29
          Height = 21
          DataField = 'indIEDest'
          DataSource = DsEntidades
          TabOrder = 5
          OnChange = DBEdindIEDestChange
        end
        object Panel15: TPanel
          Left = 0
          Top = 46
          Width = 1008
          Height = 52
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 6
          object PnShow2Pj2: TPanel
            Left = 0
            Top = 0
            Width = 1008
            Height = 52
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Label50: TLabel
              Left = 8
              Top = 8
              Width = 66
              Height = 13
              Caption = 'Raz'#227'o Social:'
            end
            object Label51: TLabel
              Left = 8
              Top = 32
              Width = 74
              Height = 13
              Caption = 'Nome Fantasia:'
            end
            object Label52: TLabel
              Left = 840
              Top = 8
              Width = 30
              Height = 13
              Caption = 'CNPJ:'
            end
            object Label53: TLabel
              Left = 840
              Top = 32
              Width = 19
              Height = 13
              Caption = 'I.E.:'
            end
            object dmkDBEdit19: TdmkDBEdit
              Left = 84
              Top = 4
              Width = 752
              Height = 21
              DataField = 'RazaoSocial'
              DataSource = DsEntidades
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              QryCampo = 'RazaoSocial'
              UpdCampo = 'RazaoSocial'
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit20: TdmkDBEdit
              Left = 84
              Top = 28
              Width = 752
              Height = 21
              DataField = 'Fantasia'
              DataSource = DsEntidades
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              QryCampo = 'Fantasia'
              UpdCampo = 'Fantasia'
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit21: TdmkDBEdit
              Left = 872
              Top = 4
              Width = 113
              Height = 21
              DataField = 'CNPJ_TXT'
              DataSource = DsEntidades
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              QryCampo = 'CNPJ'
              UpdCampo = 'CNPJ'
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit22: TdmkDBEdit
              Left = 872
              Top = 28
              Width = 113
              Height = 21
              DataField = 'IE_TXT'
              DataSource = DsEntidades
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              QryCampo = 'IE'
              UpdCampo = 'IE'
              UpdType = utYes
              Alignment = taLeftJustify
            end
          end
          object PnShow2PF2: TPanel
            Left = 0
            Top = 0
            Width = 1008
            Height = 52
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label28: TLabel
              Left = 8
              Top = 32
              Width = 38
              Height = 13
              Caption = 'Apelido:'
            end
            object Label30: TLabel
              Left = 8
              Top = 8
              Width = 31
              Height = 13
              Caption = 'Nome:'
            end
            object Label33: TLabel
              Left = 840
              Top = 8
              Width = 23
              Height = 13
              Caption = 'CPF:'
            end
            object Label36: TLabel
              Left = 476
              Top = 32
              Width = 19
              Height = 13
              Caption = 'RG:'
            end
            object Label37: TLabel
              Left = 618
              Top = 32
              Width = 70
              Height = 13
              Caption = #211'rg'#227'o emissor:'
            end
            object Label39: TLabel
              Left = 781
              Top = 32
              Width = 82
              Height = 13
              Caption = 'Data da emiss'#227'o:'
            end
            object Label163: TLabel
              Left = 772
              Top = 8
              Width = 27
              Height = 13
              Caption = 'Sexo:'
              FocusControl = DBEdit48
            end
            object dmkDBEdit3: TdmkDBEdit
              Left = 84
              Top = 27
              Width = 386
              Height = 21
              DataField = 'Apelido'
              DataSource = DsEntidades
              TabOrder = 2
              QryCampo = 'Apelido'
              UpdCampo = 'Apelido'
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit4: TdmkDBEdit
              Left = 84
              Top = 4
              Width = 682
              Height = 21
              DataField = 'Nome'
              DataSource = DsEntidades
              TabOrder = 0
              QryCampo = 'Nome'
              UpdCampo = 'Nome'
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit5: TdmkDBEdit
              Left = 872
              Top = 4
              Width = 112
              Height = 21
              DataField = 'PCPF_TXT'
              DataSource = DsEntidades
              TabOrder = 1
              QryCampo = 'CPF'
              UpdCampo = 'CPF'
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit6: TdmkDBEdit
              Left = 500
              Top = 27
              Width = 112
              Height = 21
              DataField = 'RG'
              DataSource = DsEntidades
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              QryCampo = 'RG'
              UpdCampo = 'RG'
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit7: TdmkDBEdit
              Left = 696
              Top = 27
              Width = 80
              Height = 21
              DataField = 'SSP'
              DataSource = DsEntidades
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
              QryCampo = 'SSP'
              UpdCampo = 'SSP'
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit34: TdmkDBEdit
              Left = 872
              Top = 27
              Width = 112
              Height = 21
              DataField = 'DATARG_TXT'
              DataSource = DsEntidades
              TabOrder = 5
              QryCampo = 'SSP'
              UpdCampo = 'SSP'
              UpdType = utYes
              Alignment = taCenter
            end
            object DBEdit48: TDBEdit
              Left = 804
              Top = 4
              Width = 33
              Height = 21
              DataField = 'Sexo'
              DataSource = DsEntidades
              TabOrder = 6
            end
          end
        end
        object dmkDBEdit51: TdmkDBEdit
          Left = 200
          Top = 19
          Width = 80
          Height = 21
          DataField = 'Sigla'
          DataSource = DsEntidades
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaption
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Alignment = taRightJustify
        end
      end
      object PnShowEnd: TPanel
        Left = 0
        Top = 111
        Width = 1008
        Height = 317
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object PCShowGer: TPageControl
          Left = 0
          Top = 0
          Width = 1008
          Height = 317
          ActivePage = TabSheet4
          Align = alClient
          TabOrder = 0
          OnChange = PCShowGerChange
          object TabSheet4: TTabSheet
            Caption = ' Cadastro '
            object PnShow2PF: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 289
              Align = alClient
              Color = clSkyBlue
              ParentBackground = False
              TabOrder = 1
              Visible = False
              object PCShowRes: TPageControl
                Left = 1
                Top = 17
                Width = 998
                Height = 271
                ActivePage = TabSheet6
                Align = alBottom
                TabOrder = 0
                ExplicitLeft = 61
                ExplicitTop = 33
                object TabSheet6: TTabSheet
                  Caption = ' Residencial '
                  object Panel16: TPanel
                    Left = 0
                    Top = 0
                    Width = 990
                    Height = 243
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label40: TLabel
                      Left = 8
                      Top = 8
                      Width = 24
                      Height = 13
                      Caption = 'CEP:'
                    end
                    object Label41: TLabel
                      Left = 8
                      Top = 32
                      Width = 77
                      Height = 13
                      Caption = 'Tipo logradouro:'
                    end
                    object Label42: TLabel
                      Left = 268
                      Top = 32
                      Width = 99
                      Height = 13
                      Caption = 'Nome do logradouro:'
                      FocusControl = dmkDBEdit10
                    end
                    object Label43: TLabel
                      Left = 716
                      Top = 32
                      Width = 40
                      Height = 13
                      Caption = 'N'#250'mero:'
                      FocusControl = dmkDBEdit11
                    end
                    object Label44: TLabel
                      Left = 8
                      Top = 76
                      Width = 30
                      Height = 13
                      Caption = 'Bairro:'
                      FocusControl = dmkDBEdit12
                    end
                    object Label46: TLabel
                      Left = 693
                      Top = 76
                      Width = 17
                      Height = 13
                      Caption = 'UF:'
                    end
                    object Label48: TLabel
                      Left = 808
                      Top = 32
                      Width = 67
                      Height = 13
                      Caption = 'Complemento:'
                      FocusControl = dmkDBEdit17
                    end
                    object Label49: TLabel
                      Left = 8
                      Top = 116
                      Width = 165
                      Height = 13
                      Caption = 'Ponto de refer'#234'ncia (do endere'#231'o):'
                      FocusControl = dmkDBEdit18
                    end
                    object Label119: TLabel
                      Left = 323
                      Top = 76
                      Width = 36
                      Height = 13
                      Caption = 'Cidade:'
                    end
                    object Label120: TLabel
                      Left = 724
                      Top = 76
                      Width = 25
                      Height = 13
                      Caption = 'Pa'#237's:'
                    end
                    object Label47: TLabel
                      Left = 8
                      Top = 160
                      Width = 80
                      Height = 13
                      Caption = 'Tipo de telefone:'
                    end
                    object Label61: TLabel
                      Left = 126
                      Top = 160
                      Width = 45
                      Height = 13
                      Caption = 'Telefone:'
                    end
                    object Label69: TLabel
                      Left = 244
                      Top = 160
                      Width = 80
                      Height = 13
                      Caption = 'Tipo de telefone:'
                    end
                    object Label71: TLabel
                      Left = 362
                      Top = 160
                      Width = 35
                      Height = 13
                      Caption = 'Celular:'
                    end
                    object Label59: TLabel
                      Left = 479
                      Top = 160
                      Width = 59
                      Height = 13
                      Caption = 'Nascimento:'
                    end
                    object Label91: TLabel
                      Left = 596
                      Top = 160
                      Width = 64
                      Height = 13
                      Caption = 'Site da WEB:'
                      FocusControl = dmkDBEdit28
                    end
                    object SBPSite: TSpeedButton
                      Left = 959
                      Top = 176
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SBPSiteClick
                    end
                    object Label102: TLabel
                      Left = 693
                      Top = 116
                      Width = 31
                      Height = 13
                      Caption = 'E-mail:'
                      FocusControl = dmkDBEdit40
                    end
                    object Label152: TLabel
                      Left = 640
                      Top = 8
                      Width = 71
                      Height = 13
                      Caption = 'Nacionalidade:'
                      FocusControl = DBEdit26
                    end
                    object Label153: TLabel
                      Left = 825
                      Top = 8
                      Width = 57
                      Height = 13
                      Caption = 'Estado civil:'
                      FocusControl = DBEdit27
                    end
                    object Label158: TLabel
                      Left = 353
                      Top = 8
                      Width = 46
                      Height = 13
                      Caption = 'Profiss'#227'o:'
                    end
                    object Label159: TLabel
                      Left = 511
                      Top = 8
                      Width = 31
                      Height = 13
                      Caption = 'Cargo:'
                    end
                    object Label161: TLabel
                      Left = 8
                      Top = 200
                      Width = 44
                      Height = 13
                      Caption = 'Empresa:'
                      FocusControl = dmkDBEdit42
                    end
                    object dmkDBEdit8: TdmkDBEdit
                      Left = 80
                      Top = 5
                      Width = 72
                      Height = 21
                      DataField = 'PCEP_TXT'
                      DataSource = DsEntidades
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      QryCampo = 'PCEP'
                      UpdCampo = 'PCEP'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit9: TdmkDBEdit
                      Left = 64
                      Top = 48
                      Width = 200
                      Height = 21
                      DataField = 'NOMEPLOGRAD'
                      DataSource = DsEntidades
                      TabOrder = 2
                      QryCampo = 'PLograd'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit10: TdmkDBEdit
                      Left = 268
                      Top = 48
                      Width = 444
                      Height = 21
                      DataField = 'PRua'
                      DataSource = DsEntidades
                      TabOrder = 3
                      QryCampo = 'PRua'
                      UpdCampo = 'PRua'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit11: TdmkDBEdit
                      Left = 716
                      Top = 48
                      Width = 88
                      Height = 21
                      DataField = 'PNUMERO_TXT'
                      DataSource = DsEntidades
                      TabOrder = 4
                      QryCampo = 'PNumero'
                      UpdCampo = 'PNumero'
                      UpdType = utYes
                      Alignment = taCenter
                    end
                    object dmkDBEdit12: TdmkDBEdit
                      Left = 8
                      Top = 92
                      Width = 312
                      Height = 21
                      DataField = 'PBairro'
                      DataSource = DsEntidades
                      TabOrder = 6
                      QryCampo = 'PBairro'
                      UpdCampo = 'PBairro'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit14: TdmkDBEdit
                      Left = 693
                      Top = 92
                      Width = 28
                      Height = 21
                      DataField = 'NOMEPUF'
                      DataSource = DsEntidades
                      TabOrder = 9
                      QryCampo = 'PUF'
                      UpdCampo = 'PUF'
                      UpdType = utYes
                      Alignment = taCenter
                    end
                    object dmkDBEdit16: TdmkDBEdit
                      Left = 8
                      Top = 48
                      Width = 56
                      Height = 21
                      DataField = 'PLograd'
                      DataSource = DsEntidades
                      TabOrder = 1
                      QryCampo = 'PLograd'
                      UpdCampo = 'PLograd'
                      UpdType = utYes
                      Alignment = taRightJustify
                    end
                    object dmkDBEdit17: TdmkDBEdit
                      Left = 808
                      Top = 48
                      Width = 172
                      Height = 21
                      DataField = 'PCompl'
                      DataSource = DsEntidades
                      TabOrder = 5
                      QryCampo = 'PCompl'
                      UpdCampo = 'PCompl'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit18: TdmkDBEdit
                      Left = 8
                      Top = 132
                      Width = 681
                      Height = 21
                      DataField = 'PEndeRef'
                      DataSource = DsEntidades
                      TabOrder = 12
                      QryCampo = 'PEndeRef'
                      UpdCampo = 'PEndeRef'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object DBEdit10: TDBEdit
                      Left = 323
                      Top = 92
                      Width = 56
                      Height = 21
                      DataField = 'PCodMunici'
                      DataSource = DsEntidades
                      TabOrder = 7
                    end
                    object DBEdit11: TDBEdit
                      Left = 379
                      Top = 92
                      Width = 310
                      Height = 21
                      DataField = 'NO_DTB_PMUNICI'
                      DataSource = DsEntidades
                      TabOrder = 8
                    end
                    object DBEdit12: TDBEdit
                      Left = 724
                      Top = 92
                      Width = 56
                      Height = 21
                      DataField = 'PCodiPais'
                      DataSource = DsEntidades
                      TabOrder = 10
                    end
                    object DBEdit13: TDBEdit
                      Left = 780
                      Top = 92
                      Width = 200
                      Height = 21
                      DataField = 'NO_BACEN_PPAIS'
                      DataSource = DsEntidades
                      TabOrder = 11
                    end
                    object DBEdit16: TDBEdit
                      Left = 8
                      Top = 176
                      Width = 112
                      Height = 21
                      DataField = 'PTe1Tip_TXT'
                      DataSource = DsEntidades
                      TabOrder = 14
                    end
                    object DBEdit29: TDBEdit
                      Left = 126
                      Top = 176
                      Width = 112
                      Height = 21
                      DataField = 'PTE1_TXT'
                      DataSource = DsEntidades
                      TabOrder = 15
                    end
                    object DBEdit30: TDBEdit
                      Left = 244
                      Top = 176
                      Width = 112
                      Height = 21
                      DataField = 'PCelTip_TXT'
                      DataSource = DsEntidades
                      TabOrder = 16
                    end
                    object DBEdit31: TDBEdit
                      Left = 362
                      Top = 176
                      Width = 112
                      Height = 21
                      DataField = 'PCEL_TXT'
                      DataSource = DsEntidades
                      TabOrder = 17
                    end
                    object dmkDBEdit13: TdmkDBEdit
                      Left = 479
                      Top = 176
                      Width = 112
                      Height = 21
                      DataField = 'PNATAL_TXT'
                      DataSource = DsEntidades
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 18
                      QryCampo = 'SSP'
                      UpdCampo = 'SSP'
                      UpdType = utYes
                      Alignment = taCenter
                    end
                    object dmkDBEdit28: TdmkDBEdit
                      Left = 596
                      Top = 176
                      Width = 360
                      Height = 21
                      DataField = 'PSite'
                      DataSource = DsEntidades
                      TabOrder = 19
                      UpdCampo = 'PCompl'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit40: TdmkDBEdit
                      Left = 693
                      Top = 132
                      Width = 287
                      Height = 21
                      DataField = 'PEMail'
                      DataSource = DsEntidades
                      TabOrder = 13
                      UpdCampo = 'PCompl'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object DBEdit26: TDBEdit
                      Left = 715
                      Top = 5
                      Width = 105
                      Height = 21
                      DataField = 'Nacionalid'
                      DataSource = DsEntidades
                      TabOrder = 22
                    end
                    object DBEdit27: TDBEdit
                      Left = 886
                      Top = 5
                      Width = 93
                      Height = 21
                      DataField = 'NOMEECIVIL'
                      DataSource = DsEntidades
                      TabOrder = 23
                    end
                    object DBEdit32: TDBEdit
                      Left = 410
                      Top = 5
                      Width = 97
                      Height = 21
                      DataField = 'Profissao'
                      DataSource = DsEntidades
                      TabOrder = 20
                    end
                    object DBEdit33: TDBEdit
                      Left = 547
                      Top = 5
                      Width = 89
                      Height = 21
                      DataField = 'Cargo'
                      DataSource = DsEntidades
                      TabOrder = 21
                    end
                    object dmkDBEdit42: TdmkDBEdit
                      Left = 8
                      Top = 216
                      Width = 230
                      Height = 21
                      DataField = 'NOMEEMPRESA'
                      DataSource = DsEntidades
                      TabOrder = 24
                      UpdCampo = 'PCompl'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                  end
                end
              end
            end
            object PnShow2Pj: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 289
              Align = alClient
              BevelOuter = bvNone
              Color = clSkyBlue
              ParentBackground = False
              TabOrder = 0
              Visible = False
              object PCShowCom: TPageControl
                Left = 0
                Top = 18
                Width = 1000
                Height = 271
                ActivePage = TabSheet5
                Align = alBottom
                TabOrder = 0
                object TabSheet5: TTabSheet
                  Caption = ' Comercial '
                  object Panel19: TPanel
                    Left = 0
                    Top = 0
                    Width = 992
                    Height = 243
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label54: TLabel
                      Left = 8
                      Top = 8
                      Width = 24
                      Height = 13
                      Caption = 'CEP:'
                    end
                    object Label55: TLabel
                      Left = 8
                      Top = 32
                      Width = 77
                      Height = 13
                      Caption = 'Tipo logradouro:'
                    end
                    object Label56: TLabel
                      Left = 268
                      Top = 32
                      Width = 99
                      Height = 13
                      Caption = 'Nome do logradouro:'
                      FocusControl = dmkDBEdit25
                    end
                    object Label57: TLabel
                      Left = 716
                      Top = 32
                      Width = 40
                      Height = 13
                      Caption = 'N'#250'mero:'
                      FocusControl = dmkDBEdit26
                    end
                    object Label58: TLabel
                      Left = 8
                      Top = 76
                      Width = 30
                      Height = 13
                      Caption = 'Bairro:'
                      FocusControl = dmkDBEdit27
                    end
                    object Label60: TLabel
                      Left = 694
                      Top = 76
                      Width = 17
                      Height = 13
                      Caption = 'UF:'
                    end
                    object Label62: TLabel
                      Left = 808
                      Top = 32
                      Width = 67
                      Height = 13
                      Caption = 'Complemento:'
                      FocusControl = dmkDBEdit32
                    end
                    object Label63: TLabel
                      Left = 358
                      Top = 116
                      Width = 165
                      Height = 13
                      Caption = 'Ponto de refer'#234'ncia (do endere'#231'o):'
                      FocusControl = dmkDBEdit33
                    end
                    object Label117: TLabel
                      Left = 323
                      Top = 76
                      Width = 36
                      Height = 13
                      Caption = 'Cidade:'
                    end
                    object Label121: TLabel
                      Left = 948
                      Top = 159
                      Width = 23
                      Height = 13
                      Caption = 'Filial:'
                    end
                    object Label126: TLabel
                      Left = 8
                      Top = 159
                      Width = 276
                      Height = 13
                      Caption = 'CNAE (Classifica'#231#227'o Nacional de Atividades Econ'#244'micas):'
                    end
                    object Label131: TLabel
                      Left = 240
                      Top = 116
                      Width = 58
                      Height = 13
                      Caption = 'I.M. / NIRE:'
                    end
                    object Label132: TLabel
                      Left = 124
                      Top = 116
                      Width = 39
                      Height = 13
                      Caption = 'I.E.S.T.:'
                    end
                    object Label133: TLabel
                      Left = 8
                      Top = 116
                      Width = 19
                      Height = 13
                      Caption = 'I.E.:'
                    end
                    object Label136: TLabel
                      Left = 319
                      Top = 159
                      Width = 168
                      Height = 13
                      Caption = 'CRT (C'#243'digo da Regime Tribut'#225'rio):'
                    end
                    object Label118: TLabel
                      Left = 724
                      Top = 76
                      Width = 25
                      Height = 13
                      Caption = 'Pa'#237's:'
                    end
                    object Label122: TLabel
                      Left = 8
                      Top = 200
                      Width = 80
                      Height = 13
                      Caption = 'Tipo de telefone:'
                    end
                    object Label15: TLabel
                      Left = 126
                      Top = 200
                      Width = 45
                      Height = 13
                      Caption = 'Telefone:'
                    end
                    object Label25: TLabel
                      Left = 244
                      Top = 200
                      Width = 80
                      Height = 13
                      Caption = 'Tipo de telefone:'
                    end
                    object Label23: TLabel
                      Left = 362
                      Top = 200
                      Width = 35
                      Height = 13
                      Caption = 'Celular:'
                    end
                    object SBESite: TSpeedButton
                      Left = 920
                      Top = 175
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SBESiteClick
                    end
                    object Label89: TLabel
                      Left = 667
                      Top = 159
                      Width = 64
                      Height = 13
                      Caption = 'Site da WEB:'
                      FocusControl = dmkDBEdit15
                    end
                    object Label100: TLabel
                      Left = 597
                      Top = 200
                      Width = 31
                      Height = 13
                      Caption = 'E-mail:'
                      FocusControl = dmkDBEdit30
                    end
                    object Label156: TLabel
                      Left = 479
                      Top = 200
                      Width = 20
                      Height = 13
                      Caption = 'Fax:'
                      FocusControl = DBEdit28
                    end
                    object Label142: TLabel
                      Left = 864
                      Top = 200
                      Width = 51
                      Height = 13
                      Caption = 'Funda'#231#227'o:'
                    end
                    object dmkDBEdit23: TdmkDBEdit
                      Left = 80
                      Top = 5
                      Width = 72
                      Height = 21
                      DataField = 'ECEP_TXT'
                      DataSource = DsEntidades
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      QryCampo = 'ECEP'
                      UpdCampo = 'ECEP'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit24: TdmkDBEdit
                      Left = 64
                      Top = 48
                      Width = 200
                      Height = 21
                      DataField = 'NOMEELOGRAD'
                      DataSource = DsEntidades
                      TabOrder = 2
                      QryCampo = 'ELograd'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit25: TdmkDBEdit
                      Left = 268
                      Top = 48
                      Width = 444
                      Height = 21
                      DataField = 'ERua'
                      DataSource = DsEntidades
                      TabOrder = 3
                      QryCampo = 'ERua'
                      UpdCampo = 'ERua'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit26: TdmkDBEdit
                      Left = 716
                      Top = 48
                      Width = 88
                      Height = 21
                      DataField = 'ENUMERO_TXT'
                      DataSource = DsEntidades
                      TabOrder = 4
                      QryCampo = 'ENumero'
                      UpdCampo = 'ENumero'
                      UpdType = utYes
                      Alignment = taCenter
                    end
                    object dmkDBEdit27: TdmkDBEdit
                      Left = 8
                      Top = 92
                      Width = 312
                      Height = 21
                      DataField = 'EBairro'
                      DataSource = DsEntidades
                      TabOrder = 6
                      QryCampo = 'EBairro'
                      UpdCampo = 'EBairro'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit29: TdmkDBEdit
                      Left = 694
                      Top = 92
                      Width = 28
                      Height = 21
                      DataField = 'NOMEEUF'
                      DataSource = DsEntidades
                      TabOrder = 9
                      QryCampo = 'EUF'
                      UpdCampo = 'EUF'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit31: TdmkDBEdit
                      Left = 8
                      Top = 48
                      Width = 56
                      Height = 21
                      DataField = 'ELograd'
                      DataSource = DsEntidades
                      TabOrder = 1
                      QryCampo = 'ELograd'
                      UpdCampo = 'ELograd'
                      UpdType = utYes
                      Alignment = taRightJustify
                    end
                    object dmkDBEdit32: TdmkDBEdit
                      Left = 808
                      Top = 48
                      Width = 172
                      Height = 21
                      DataField = 'ECompl'
                      DataSource = DsEntidades
                      TabOrder = 5
                      QryCampo = 'ECompl'
                      UpdCampo = 'ECompl'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit33: TdmkDBEdit
                      Left = 358
                      Top = 132
                      Width = 622
                      Height = 21
                      DataField = 'EEndeRef'
                      DataSource = DsEntidades
                      TabOrder = 15
                      QryCampo = 'EEndeRef'
                      UpdCampo = 'EEndeRef'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object DBEdit6: TDBEdit
                      Left = 379
                      Top = 92
                      Width = 312
                      Height = 21
                      DataField = 'NO_DTB_EMUNICI'
                      DataSource = DsEntidades
                      TabOrder = 8
                    end
                    object DBEdit9: TDBEdit
                      Left = 323
                      Top = 92
                      Width = 56
                      Height = 21
                      DataField = 'ECodMunici'
                      DataSource = DsEntidades
                      TabOrder = 7
                    end
                    object DBEdit14: TDBEdit
                      Left = 948
                      Top = 175
                      Width = 33
                      Height = 21
                      DataField = 'Filial'
                      DataSource = DsEntidades
                      TabOrder = 21
                    end
                    object DBEdit17: TDBEdit
                      Left = 8
                      Top = 175
                      Width = 56
                      Height = 21
                      DataField = 'CNAE'
                      DataSource = DsEntidades
                      TabOrder = 16
                    end
                    object DBEdit18: TDBEdit
                      Left = 64
                      Top = 175
                      Width = 250
                      Height = 21
                      DataField = 'CNAE_Nome'
                      DataSource = DsEntidades
                      TabOrder = 17
                    end
                    object DBEdit19: TDBEdit
                      Left = 124
                      Top = 132
                      Width = 112
                      Height = 21
                      DataField = 'IEST'
                      DataSource = DsEntidades
                      TabOrder = 13
                    end
                    object DBEdit20: TDBEdit
                      Left = 240
                      Top = 132
                      Width = 112
                      Height = 21
                      DataField = 'NIRE'
                      DataSource = DsEntidades
                      TabOrder = 14
                    end
                    object DBEdit21: TDBEdit
                      Left = 8
                      Top = 132
                      Width = 112
                      Height = 21
                      DataField = 'IE_TXT'
                      DataSource = DsEntidades
                      TabOrder = 12
                    end
                    object Memo2: TMemo
                      Left = 355
                      Top = 175
                      Width = 305
                      Height = 21
                      ReadOnly = True
                      TabOrder = 19
                    end
                    object DBEdCRT: TDBEdit
                      Left = 319
                      Top = 175
                      Width = 35
                      Height = 21
                      DataField = 'CRT'
                      DataSource = DsEntidades
                      TabOrder = 18
                      OnChange = DBEdCRTChange
                    end
                    object DBEdit7: TDBEdit
                      Left = 780
                      Top = 92
                      Width = 200
                      Height = 21
                      DataField = 'NO_BACEN_EPAIS'
                      DataSource = DsEntidades
                      TabOrder = 11
                    end
                    object DBEdit8: TDBEdit
                      Left = 724
                      Top = 92
                      Width = 56
                      Height = 21
                      DataField = 'ECodiPais'
                      DataSource = DsEntidades
                      TabOrder = 10
                    end
                    object DBEdit15: TDBEdit
                      Left = 8
                      Top = 215
                      Width = 112
                      Height = 21
                      DataField = 'ETe1Tip_TXT'
                      DataSource = DsEntidades
                      TabOrder = 22
                    end
                    object DBEdit22: TDBEdit
                      Left = 126
                      Top = 216
                      Width = 112
                      Height = 21
                      DataField = 'ETE1_TXT'
                      DataSource = DsEntidades
                      TabOrder = 23
                    end
                    object DBEdit24: TDBEdit
                      Left = 244
                      Top = 216
                      Width = 112
                      Height = 21
                      DataField = 'ECelTip_TXT'
                      DataSource = DsEntidades
                      TabOrder = 24
                    end
                    object DBEdit23: TDBEdit
                      Left = 362
                      Top = 216
                      Width = 112
                      Height = 21
                      DataField = 'ECEL_TXT'
                      DataSource = DsEntidades
                      TabOrder = 25
                    end
                    object dmkDBEdit15: TdmkDBEdit
                      Left = 667
                      Top = 175
                      Width = 250
                      Height = 21
                      DataField = 'ESite'
                      DataSource = DsEntidades
                      TabOrder = 20
                      UpdCampo = 'ERua'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit30: TdmkDBEdit
                      Left = 597
                      Top = 216
                      Width = 262
                      Height = 21
                      DataField = 'EEMail'
                      DataSource = DsEntidades
                      TabOrder = 27
                      UpdCampo = 'ERua'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object DBEdit28: TDBEdit
                      Left = 479
                      Top = 216
                      Width = 113
                      Height = 21
                      DataField = 'EFAX_TXT'
                      DataSource = DsEntidades
                      TabOrder = 26
                    end
                    object dmkDBEdit60: TdmkDBEdit
                      Left = 864
                      Top = 216
                      Width = 116
                      Height = 21
                      DataField = 'ENATAL_TXT'
                      DataSource = DsEntidades
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 28
                      QryCampo = 'SSP'
                      UpdCampo = 'SSP'
                      UpdType = utYes
                      Alignment = taCenter
                    end
                  end
                end
              end
            end
          end
          object TabSheet23: TTabSheet
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Caption = ' Contatos'
            ImageIndex = 7
            object PnContatos: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 289
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Splitter1: TSplitter
                Left = 376
                Top = 0
                Width = 5
                Height = 289
                ExplicitHeight = 292
              end
              object DBGEntiContat: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 376
                Height = 289
                Align = alLeft
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'DTANATAL_TXT'
                    Title.Caption = 'Nascimento'
                    Width = 85
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Width = 180
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOME_CARGO'
                    Title.Caption = 'Cargo'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CNPJ_CPF'
                    Title.Caption = 'CNPJ / CPF'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Entidade'
                    Width = 75
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsEntiContat
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDblClick = DBGEntiContatDblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'DTANATAL_TXT'
                    Title.Caption = 'Nascimento'
                    Width = 85
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ativo'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Width = 180
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOME_CARGO'
                    Title.Caption = 'Cargo'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CNPJ_CPF'
                    Title.Caption = 'CNPJ / CPF'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Entidade'
                    Width = 75
                    Visible = True
                  end>
              end
              object Panel10: TPanel
                Left = 381
                Top = 0
                Width = 619
                Height = 289
                Align = alClient
                BevelOuter = bvNone
                Caption = 'Panel10'
                TabOrder = 1
                object Splitter2: TSplitter
                  Left = 280
                  Top = 0
                  Width = 5
                  Height = 289
                  ExplicitHeight = 292
                end
                object DBGEntiMail: TdmkDBGrid
                  Left = 285
                  Top = 0
                  Width = 334
                  Height = 289
                  Align = alClient
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Ordem'
                      Width = 39
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOMEETC'
                      Title.Caption = 'Tipo de e-mail'
                      Width = 84
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'EMail'
                      Title.Caption = 'E-mail'
                      Width = 496
                      Visible = True
                    end>
                  Color = clWindow
                  DataSource = DsEntiMail
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Ordem'
                      Width = 39
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOMEETC'
                      Title.Caption = 'Tipo de e-mail'
                      Width = 84
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'EMail'
                      Title.Caption = 'E-mail'
                      Width = 496
                      Visible = True
                    end>
                end
                object DBGEntiTel: TdmkDBGrid
                  Left = 0
                  Top = 0
                  Width = 280
                  Height = 289
                  Align = alLeft
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NOMEETC'
                      Title.Caption = 'Tipo de telefone'
                      Width = 84
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'TEL_TXT'
                      Title.Caption = 'Telefone'
                      Width = 120
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Ramal'
                      Width = 37
                      Visible = True
                    end>
                  Color = clWindow
                  DataSource = DsEntiTel
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NOMEETC'
                      Title.Caption = 'Tipo de telefone'
                      Width = 84
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'TEL_TXT'
                      Title.Caption = 'Telefone'
                      Width = 120
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Ramal'
                      Width = 37
                      Visible = True
                    end>
                end
              end
            end
          end
          object TabSheet7: TTabSheet
            Caption = ' Tipo '
            ImageIndex = 1
            object Panel13: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 289
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label128: TLabel
                Left = 4
                Top = 95
                Width = 97
                Height = 13
                Caption = 'Carteira preferencial:'
              end
              object Label138: TLabel
                Left = 4
                Top = 136
                Width = 139
                Height = 13
                Caption = 'C'#243'd. participante SPED EFD:'
              end
              object Label140: TLabel
                Left = 180
                Top = 136
                Width = 55
                Height = 13
                Caption = 'SUFRAMA:'
              end
              object GroupBox1: TGroupBox
                Left = 4
                Top = 4
                Width = 636
                Height = 85
                Caption = ' Tipo de cadastro: '
                TabOrder = 0
                object CkCliente1: TDBCheckBox
                  Left = 8
                  Top = 16
                  Width = 152
                  Height = 17
                  Caption = 'Cliente'
                  DataField = 'Cliente1'
                  DataSource = DsEntidades
                  TabOrder = 0
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                end
                object CkFornece1: TDBCheckBox
                  Left = 164
                  Top = 16
                  Width = 152
                  Height = 17
                  Caption = 'Fornecedor'
                  DataField = 'Fornece1'
                  DataSource = DsEntidades
                  TabOrder = 4
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                end
                object CkFornece2: TDBCheckBox
                  Left = 164
                  Top = 32
                  Width = 152
                  Height = 17
                  Caption = 'Transportador'
                  DataField = 'Fornece2'
                  DataSource = DsEntidades
                  TabOrder = 5
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                end
                object CkFornece3: TDBCheckBox
                  Left = 164
                  Top = 48
                  Width = 152
                  Height = 17
                  Caption = 'Vendedor'
                  DataField = 'Fornece3'
                  DataSource = DsEntidades
                  TabOrder = 6
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                end
                object CkTerceiro: TDBCheckBox
                  Left = 476
                  Top = 16
                  Width = 152
                  Height = 17
                  Caption = 'Terceiro'
                  DataField = 'Terceiro'
                  DataSource = DsEntidades
                  TabOrder = 12
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                end
                object CkFornece4: TDBCheckBox
                  Left = 164
                  Top = 64
                  Width = 152
                  Height = 17
                  Caption = 'Fornece 4'
                  DataField = 'Fornece4'
                  DataSource = DsEntidades
                  TabOrder = 7
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                  Visible = False
                end
                object CkCliente2: TDBCheckBox
                  Left = 8
                  Top = 32
                  Width = 152
                  Height = 17
                  Caption = 'Cliente 2'
                  DataField = 'Cliente2'
                  DataSource = DsEntidades
                  TabOrder = 1
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                  Visible = False
                end
                object CkFornece5: TDBCheckBox
                  Left = 320
                  Top = 16
                  Width = 152
                  Height = 17
                  Caption = 'Fornece 5'
                  DataField = 'Fornece5'
                  DataSource = DsEntidades
                  TabOrder = 8
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                  Visible = False
                end
                object CkFornece6: TDBCheckBox
                  Left = 320
                  Top = 32
                  Width = 152
                  Height = 17
                  Caption = 'Fornece 6'
                  DataField = 'Fornece6'
                  DataSource = DsEntidades
                  TabOrder = 9
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                  Visible = False
                end
                object CkCliente3: TDBCheckBox
                  Left = 8
                  Top = 48
                  Width = 152
                  Height = 17
                  Caption = 'Cliente 3'
                  DataField = 'Cliente3'
                  DataSource = DsEntidades
                  TabOrder = 2
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                  Visible = False
                end
                object CkCliente4: TDBCheckBox
                  Left = 8
                  Top = 64
                  Width = 152
                  Height = 17
                  Caption = 'Cliente 4'
                  DataField = 'Cliente4'
                  DataSource = DsEntidades
                  TabOrder = 3
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                  Visible = False
                end
                object CkFornece8: TDBCheckBox
                  Left = 320
                  Top = 64
                  Width = 152
                  Height = 17
                  Caption = 'Fornece 8'
                  DataField = 'Fornece8'
                  DataSource = DsEntidades
                  TabOrder = 11
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                  Visible = False
                end
                object CkFornece7: TDBCheckBox
                  Left = 320
                  Top = 48
                  Width = 152
                  Height = 17
                  Caption = 'Fornece 7'
                  DataField = 'Fornece7'
                  DataSource = DsEntidades
                  TabOrder = 10
                  ValueChecked = 'V'
                  ValueUnchecked = 'F'
                  Visible = False
                end
              end
              object dmkDBEdit57: TdmkDBEdit
                Left = 4
                Top = 111
                Width = 636
                Height = 21
                DataField = 'NOMECARTPREF'
                DataSource = DsEntidades
                TabOrder = 1
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit58: TdmkDBEdit
                Left = 4
                Top = 152
                Width = 172
                Height = 21
                DataField = 'COD_PART'
                DataSource = DsEntidades
                TabOrder = 2
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit59: TdmkDBEdit
                Left = 180
                Top = 152
                Width = 172
                Height = 21
                DataField = 'SUFRAMA'
                DataSource = DsEntidades
                TabOrder = 3
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object DBRadioGroup3: TDBRadioGroup
                Left = 4
                Top = 176
                Width = 637
                Height = 41
                Caption = ' Fabrica'#231#227'o: '
                Columns = 3
                DataField = 'MadeBy'
                DataSource = DsEntidades
                Items.Strings = (
                  'Indefinido'
                  'Fabrica'#231#227'o Pr'#243'pria'
                  'Revenda ')
                TabOrder = 4
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
            end
          end
          object TabSheet11: TTabSheet
            Caption = ' Endere'#231'o de cobran'#231'a '
            ImageIndex = 2
            object Panel4: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 289
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label64: TLabel
                Left = 8
                Top = 8
                Width = 24
                Height = 13
                Caption = 'CEP:'
              end
              object Label65: TLabel
                Left = 8
                Top = 32
                Width = 77
                Height = 13
                Caption = 'Tipo logradouro:'
              end
              object Label66: TLabel
                Left = 268
                Top = 32
                Width = 99
                Height = 13
                Caption = 'Nome do logradouro:'
                FocusControl = dmkDBEdit37
              end
              object Label67: TLabel
                Left = 716
                Top = 32
                Width = 40
                Height = 13
                Caption = 'N'#250'mero:'
                FocusControl = dmkDBEdit38
              end
              object Label68: TLabel
                Left = 8
                Top = 76
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                FocusControl = dmkDBEdit39
              end
              object Label70: TLabel
                Left = 952
                Top = 76
                Width = 17
                Height = 13
                Caption = 'UF:'
              end
              object Label72: TLabel
                Left = 808
                Top = 32
                Width = 67
                Height = 13
                Caption = 'Complemento:'
                FocusControl = dmkDBEdit44
              end
              object Label73: TLabel
                Left = 8
                Top = 116
                Width = 55
                Height = 13
                Caption = 'Refer'#234'ncia:'
                FocusControl = dmkDBEdit45
              end
              object Label116: TLabel
                Left = 323
                Top = 76
                Width = 36
                Height = 13
                Caption = 'Cidade:'
              end
              object dmkDBEdit35: TdmkDBEdit
                Left = 80
                Top = 5
                Width = 72
                Height = 21
                DataField = 'CCEP_TXT'
                DataSource = DsEntidades
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                QryCampo = 'PCEP'
                UpdCampo = 'PCEP'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit36: TdmkDBEdit
                Left = 64
                Top = 48
                Width = 200
                Height = 21
                DataField = 'NOMECLOGRAD'
                DataSource = DsEntidades
                TabOrder = 2
                QryCampo = 'PLograd'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit37: TdmkDBEdit
                Left = 268
                Top = 48
                Width = 444
                Height = 21
                DataField = 'CRua'
                DataSource = DsEntidades
                TabOrder = 3
                QryCampo = 'PRua'
                UpdCampo = 'PRua'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit38: TdmkDBEdit
                Left = 716
                Top = 48
                Width = 88
                Height = 21
                DataField = 'CNUMERO_TXT'
                DataSource = DsEntidades
                TabOrder = 4
                QryCampo = 'PNumero'
                UpdCampo = 'PNumero'
                UpdType = utYes
                Alignment = taCenter
              end
              object dmkDBEdit39: TdmkDBEdit
                Left = 8
                Top = 92
                Width = 312
                Height = 21
                DataField = 'CBairro'
                DataSource = DsEntidades
                TabOrder = 6
                QryCampo = 'PBairro'
                UpdCampo = 'PBairro'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit41: TdmkDBEdit
                Left = 952
                Top = 92
                Width = 28
                Height = 21
                DataField = 'NOMECUF'
                DataSource = DsEntidades
                TabOrder = 9
                QryCampo = 'PUF'
                UpdCampo = 'PUF'
                UpdType = utYes
                Alignment = taCenter
              end
              object dmkDBEdit43: TdmkDBEdit
                Left = 8
                Top = 48
                Width = 56
                Height = 21
                DataField = 'CLograd'
                DataSource = DsEntidades
                TabOrder = 1
                QryCampo = 'PLograd'
                UpdCampo = 'PLograd'
                UpdType = utYes
                Alignment = taRightJustify
              end
              object dmkDBEdit44: TdmkDBEdit
                Left = 808
                Top = 48
                Width = 172
                Height = 21
                DataField = 'CCompl'
                DataSource = DsEntidades
                TabOrder = 5
                QryCampo = 'PCompl'
                UpdCampo = 'PCompl'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit45: TdmkDBEdit
                Left = 8
                Top = 132
                Width = 972
                Height = 21
                DataField = 'CEndeRef'
                DataSource = DsEntidades
                TabOrder = 10
                QryCampo = 'PEndeRef'
                UpdCampo = 'PEndeRef'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object DBEdit5: TDBEdit
                Left = 381
                Top = 92
                Width = 565
                Height = 21
                DataField = 'NO_DTB_CMUNICI'
                DataSource = DsEntidades
                TabOrder = 8
              end
              object DBEdit4: TDBEdit
                Left = 323
                Top = 92
                Width = 57
                Height = 21
                DataField = 'CCodMunici'
                DataSource = DsEntidades
                TabOrder = 7
              end
            end
          end
          object TabSheet12: TTabSheet
            Caption = ' Endere'#231'o de entrega '
            ImageIndex = 3
            object Panel14: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 289
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label74: TLabel
                Left = 8
                Top = 52
                Width = 24
                Height = 13
                Caption = 'CEP:'
              end
              object Label75: TLabel
                Left = 112
                Top = 32
                Width = 77
                Height = 13
                Caption = 'Tipo logradouro:'
              end
              object Label76: TLabel
                Left = 368
                Top = 32
                Width = 99
                Height = 13
                Caption = 'Nome do logradouro:'
                FocusControl = dmkDBEdit48
              end
              object Label77: TLabel
                Left = 716
                Top = 32
                Width = 40
                Height = 13
                Caption = 'N'#250'mero:'
                FocusControl = dmkDBEdit49
              end
              object Label78: TLabel
                Left = 8
                Top = 76
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                FocusControl = dmkDBEdit50
              end
              object Label80: TLabel
                Left = 952
                Top = 76
                Width = 17
                Height = 13
                Caption = 'UF:'
              end
              object Label82: TLabel
                Left = 808
                Top = 32
                Width = 67
                Height = 13
                Caption = 'Complemento:'
                FocusControl = dmkDBEdit55
              end
              object Label83: TLabel
                Left = 308
                Top = 116
                Width = 55
                Height = 13
                Caption = 'Refer'#234'ncia:'
                FocusControl = dmkDBEdit56
              end
              object Label114: TLabel
                Left = 8
                Top = 8
                Width = 30
                Height = 13
                Caption = 'CNPJ:'
                FocusControl = DBEdit1
              end
              object Label115: TLabel
                Left = 323
                Top = 76
                Width = 36
                Height = 13
                Caption = 'Cidade:'
              end
              object Label169: TLabel
                Left = 164
                Top = 8
                Width = 53
                Height = 13
                Caption = '...ou...CPF:'
              end
              object Label170: TLabel
                Left = 360
                Top = 8
                Width = 112
                Height = 13
                Caption = 'Nome ou Raz'#227'o Social:'
                FocusControl = EdERua
              end
              object Label171: TLabel
                Left = 7
                Top = 116
                Width = 25
                Height = 13
                Caption = 'Pa'#237's:'
              end
              object Label172: TLabel
                Left = 184
                Top = 156
                Width = 31
                Height = 13
                Caption = 'E-mail:'
                FocusControl = EdLEmail
              end
              object Label173: TLabel
                Left = 680
                Top = 156
                Width = 19
                Height = 13
                Caption = 'I.E.:'
              end
              object Label174: TLabel
                Left = 7
                Top = 156
                Width = 87
                Height = 13
                Caption = 'Telefone principal:'
              end
              object dmkDBEdit46: TdmkDBEdit
                Left = 36
                Top = 48
                Width = 72
                Height = 21
                DataField = 'LCEP_TXT'
                DataSource = DsEntidades
                TabOrder = 3
                QryCampo = 'PCEP'
                UpdCampo = 'PCEP'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit47: TdmkDBEdit
                Left = 168
                Top = 48
                Width = 193
                Height = 21
                DataField = 'NOMELLOGRAD'
                DataSource = DsEntidades
                TabOrder = 5
                QryCampo = 'PLograd'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit48: TdmkDBEdit
                Left = 368
                Top = 48
                Width = 345
                Height = 21
                DataField = 'LRua'
                DataSource = DsEntidades
                TabOrder = 6
                QryCampo = 'PRua'
                UpdCampo = 'PRua'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit49: TdmkDBEdit
                Left = 716
                Top = 48
                Width = 88
                Height = 21
                DataField = 'LNUMERO_TXT'
                DataSource = DsEntidades
                TabOrder = 7
                QryCampo = 'PNumero'
                UpdCampo = 'PNumero'
                UpdType = utYes
                Alignment = taCenter
              end
              object dmkDBEdit50: TdmkDBEdit
                Left = 8
                Top = 92
                Width = 312
                Height = 21
                DataField = 'LBairro'
                DataSource = DsEntidades
                TabOrder = 9
                QryCampo = 'PBairro'
                UpdCampo = 'PBairro'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit52: TdmkDBEdit
                Left = 952
                Top = 92
                Width = 28
                Height = 21
                DataField = 'NOMELUF'
                DataSource = DsEntidades
                TabOrder = 12
                QryCampo = 'PUF'
                UpdCampo = 'PUF'
                UpdType = utYes
                Alignment = taCenter
              end
              object dmkDBEdit54: TdmkDBEdit
                Left = 112
                Top = 48
                Width = 56
                Height = 21
                DataField = 'LLograd'
                DataSource = DsEntidades
                TabOrder = 4
                QryCampo = 'PLograd'
                UpdCampo = 'PLograd'
                UpdType = utYes
                Alignment = taRightJustify
              end
              object dmkDBEdit55: TdmkDBEdit
                Left = 808
                Top = 48
                Width = 172
                Height = 21
                DataField = 'LCompl'
                DataSource = DsEntidades
                TabOrder = 8
                QryCampo = 'PCompl'
                UpdCampo = 'PCompl'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object dmkDBEdit56: TdmkDBEdit
                Left = 308
                Top = 132
                Width = 673
                Height = 21
                DataField = 'LEndeRef'
                DataSource = DsEntidades
                TabOrder = 15
                QryCampo = 'PEndeRef'
                UpdCampo = 'PEndeRef'
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object DBEdit1: TDBEdit
                Left = 40
                Top = 4
                Width = 113
                Height = 21
                DataField = 'L_CNPJ_TXT'
                DataSource = DsEntidades
                TabOrder = 0
              end
              object DBRadioGroup1: TDBRadioGroup
                Left = 831
                Top = 157
                Width = 149
                Height = 37
                Caption = ' Entregar neste endere'#231'o?: '
                Columns = 2
                DataField = 'L_Ativo'
                DataSource = DsEntidades
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 19
                Values.Strings = (
                  '0'
                  '1')
              end
              object DBEdit2: TDBEdit
                Left = 323
                Top = 92
                Width = 57
                Height = 21
                DataField = 'LCodMunici'
                DataSource = DsEntidades
                TabOrder = 10
              end
              object DBEdit3: TDBEdit
                Left = 381
                Top = 92
                Width = 565
                Height = 21
                DataField = 'NO_DTB_LMUNICI'
                DataSource = DsEntidades
                TabOrder = 11
              end
              object DBEdit34: TDBEdit
                Left = 680
                Top = 172
                Width = 149
                Height = 21
                DataField = 'L_IE'
                DataSource = DsEntidades
                TabOrder = 18
              end
              object DBEdit35: TDBEdit
                Left = 184
                Top = 172
                Width = 493
                Height = 21
                DataField = 'LEmail'
                DataSource = DsEntidades
                TabOrder = 17
              end
              object DBEdit36: TDBEdit
                Left = 8
                Top = 132
                Width = 69
                Height = 21
                DataField = 'LCodiPais'
                DataSource = DsEntidades
                TabOrder = 13
              end
              object DBEdit37: TDBEdit
                Left = 80
                Top = 132
                Width = 225
                Height = 21
                DataField = 'NO_BACEN_LPAIS'
                DataSource = DsEntidades
                TabOrder = 14
              end
              object DBEdit38: TDBEdit
                Left = 224
                Top = 4
                Width = 133
                Height = 21
                DataField = 'L_CPF_TXT'
                DataSource = DsEntidades
                TabOrder = 1
              end
              object DBEdit39: TDBEdit
                Left = 476
                Top = 4
                Width = 505
                Height = 21
                DataField = 'L_Nome'
                DataSource = DsEntidades
                TabOrder = 2
              end
              object DBEdit40: TDBEdit
                Left = 12
                Top = 172
                Width = 173
                Height = 21
                DataField = 'LTEL_TXT'
                DataSource = DsEntidades
                TabOrder = 16
              end
            end
          end
          object TabSheet19: TTabSheet
            Caption = 'Observa'#231#245'es'
            ImageIndex = 4
            object MeObservacoes_Show: TMemo
              Left = 0
              Top = 29
              Width = 1000
              Height = 260
              Align = alClient
              ReadOnly = True
              TabOrder = 0
            end
            object Panel33: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 29
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Label177: TLabel
                Left = 8
                Top = 8
                Width = 80
                Height = 13
                Caption = 'Limite de cr'#233'dito:'
              end
              object DBEdit41: TDBEdit
                Left = 100
                Top = 4
                Width = 134
                Height = 21
                DataField = 'LimiCred'
                DataSource = DsEntidades
                TabOrder = 0
              end
            end
          end
          object TabSheet21: TTabSheet
            Caption = ' Estrangeiro | Cont'#225'bil'
            ImageIndex = 6
            object Panel25: TPanel
              Left = 0
              Top = 0
              Width = 1000
              Height = 289
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object DBGBEstrangeiro: TGroupBox
                Left = 12
                Top = 28
                Width = 977
                Height = 133
                Caption = '  Dados espec'#237'ficos de estrangeiro: '
                TabOrder = 0
                Visible = False
                object Panel26: TPanel
                  Left = 2
                  Top = 15
                  Width = 973
                  Height = 116
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label147: TLabel
                    Left = 8
                    Top = 68
                    Width = 111
                    Height = 13
                    Caption = 'N'#250'mero do documento:'
                  end
                  object DBRadioGroup2: TDBRadioGroup
                    Left = 8
                    Top = 0
                    Width = 957
                    Height = 65
                    Caption = ' Tipo de documento: '
                    DataField = 'EstrangTip'
                    DataSource = DsEntidades
                    Items.Strings = (
                      'N'#227'o definido'
                      'Passaporte')
                    TabOrder = 0
                    Values.Strings = (
                      '0'
                      '1'
                      '2'
                      '3'
                      '4'
                      '5'
                      '6'
                      '7'
                      '8'
                      '9')
                  end
                  object DBEdit25: TDBEdit
                    Left = 8
                    Top = 84
                    Width = 264
                    Height = 21
                    DataField = 'EstrangNum'
                    DataSource = DsEntidades
                    TabOrder = 1
                  end
                end
              end
              object DBCheckBox1: TDBCheckBox
                Left = 8
                Top = 8
                Width = 97
                Height = 17
                Caption = 'Estrangeiro'
                DataField = 'EstrangDef'
                DataSource = DsEntidades
                TabOrder = 1
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object Panel34: TPanel
                Left = 12
                Top = 176
                Width = 977
                Height = 101
                TabOrder = 2
                object Label178: TLabel
                  Left = 4
                  Top = 4
                  Width = 72
                  Height = 13
                  Caption = 'Grupo cont'#225'bil:'
                end
                object Label179: TLabel
                  Left = 4
                  Top = 44
                  Width = 125
                  Height = 13
                  Caption = 'Conta do plano de contas:'
                end
                object DBEdit42: TDBEdit
                  Left = 4
                  Top = 60
                  Width = 56
                  Height = 21
                  DataField = 'CtbPlaCta'
                  DataSource = DsEntidades
                  TabOrder = 0
                end
                object DBEdit43: TDBEdit
                  Left = 60
                  Top = 60
                  Width = 536
                  Height = 21
                  DataField = 'NO_CtbPlaCta'
                  DataSource = DsEntidades
                  TabOrder = 1
                end
                object DBEdit44: TDBEdit
                  Left = 4
                  Top = 20
                  Width = 56
                  Height = 21
                  DataField = 'CtbCadGru'
                  DataSource = DsEntidades
                  TabOrder = 2
                end
                object DBEdit45: TDBEdit
                  Left = 60
                  Top = 20
                  Width = 536
                  Height = 21
                  DataField = 'NO_CtbCadGru'
                  DataSource = DsEntidades
                  TabOrder = 3
                end
              end
            end
          end
          object TabSheet9: TTabSheet
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Caption = ' Atributos '
            ImageIndex = 8
            object GradeDefAtr: TdmkDBGrid
              Left = 93
              Top = 0
              Width = 907
              Height = 289
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CODUSU_CAD'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_CAD'
                  Title.Caption = 'Atributo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CODUSU_ITS'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_ITS'
                  Title.Caption = 'Descri'#231#227'o do item do atributo'
                  Width = 236
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CTRL_ATR'
                  Title.Caption = 'ID'
                  Visible = True
                end>
              Color = clWindow
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CODUSU_CAD'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_CAD'
                  Title.Caption = 'Atributo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CODUSU_ITS'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_ITS'
                  Title.Caption = 'Descri'#231#227'o do item do atributo'
                  Width = 236
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CTRL_ATR'
                  Title.Caption = 'ID'
                  Visible = True
                end>
            end
            object Panel28: TPanel
              Left = 0
              Top = 0
              Width = 93
              Height = 289
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Align = alLeft
              ParentBackground = False
              TabOrder = 1
              object BtAtributos: TBitBtn
                Tag = 10099
                Left = 4
                Top = 4
                Width = 85
                Height = 40
                Cursor = crHandPoint
                Caption = '&Atributos'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtAtributosClick
              end
            end
          end
          object TabSheet24: TTabSheet
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Caption = ' Respons'#225'veis '
            ImageIndex = 9
            object Label125: TLabel
              Left = 0
              Top = 0
              Width = 525
              Height = 13
              Align = alTop
              Alignment = taCenter
              Caption = 
                'D'#234' um duplo clique na linha correspondente para localizar o cada' +
                'stro da entidade vinculada'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBGEntiRespon: TDBGrid
              Left = 93
              Top = 13
              Width = 907
              Height = 276
              Align = alClient
              DataSource = DsEntiRespon
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBGEntiResponDblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'OrdemLista'
                  Title.Caption = 'Ordem lista'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Entidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_CARGO'
                  Title.Caption = 'Cargo'
                  Width = 112
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ASSINA'
                  Title.Caption = 'Assina'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MandatoIni_TXT'
                  Title.Caption = 'Ini. mandato'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MandatoFim_TXT'
                  Title.Caption = 'Fim mandato'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Entidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Te1_TXT'
                  Title.Caption = 'Telefone'
                  Width = 110
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Cel_TXT'
                  Title.Caption = 'Celular'
                  Width = 110
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Email'
                  Title.Caption = 'E-mail'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observ'
                  Title.Caption = 'Observa'#231#245'es'
                  Width = 350
                  Visible = True
                end>
            end
            object Panel29: TPanel
              Left = 0
              Top = 13
              Width = 93
              Height = 276
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Align = alLeft
              ParentBackground = False
              TabOrder = 1
              object BtRespon: TBitBtn
                Tag = 10093
                Left = 4
                Top = 4
                Width = 85
                Height = 40
                Cursor = crHandPoint
                Caption = '&Respons.'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtResponClick
              end
            end
          end
          object TabSheet25: TTabSheet
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Caption = ' Dados Banc'#225'rios'
            ImageIndex = 10
            object DBGEntiCtas: TDBGrid
              Left = 93
              Top = 0
              Width = 907
              Height = 289
              Align = alClient
              DataSource = DsEntiCtas
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Banco'
                  Width = 53
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEBANCO'
                  Title.Caption = 'Banco'
                  Width = 278
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Agencia'
                  Title.Caption = 'Ag'#234'ncia'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DAC_A'
                  Title.Caption = '[A]'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ContaCor'
                  Title.Caption = 'Conta'
                  Width = 73
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DAC_C'
                  Title.Caption = '[C]'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DAC_AC'
                  Title.Caption = '[AC]'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ContaTip'
                  Title.Caption = 'Tipo'
                  Visible = True
                end>
            end
            object Panel27: TPanel
              Left = 0
              Top = 0
              Width = 93
              Height = 289
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Align = alLeft
              ParentBackground = False
              TabOrder = 1
              object BtDBanc: TBitBtn
                Tag = 426
                Left = 4
                Top = 4
                Width = 85
                Height = 40
                Cursor = crHandPoint
                Caption = '&Dados '#13#10'Bancos'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtDBancClick
              end
            end
          end
          object TabSheet16: TTabSheet
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Caption = 'Transportadoras'
            ImageIndex = 13
            object DBGEntiTransp: TDBGrid
              Left = 93
              Top = 0
              Width = 907
              Height = 289
              Align = alClient
              DataSource = DsEntiTransp
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Transp'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEENTIDADE'
                  Title.Caption = 'Nome Transportadora'
                  Width = 400
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ordem'
                  Width = 75
                  Visible = True
                end>
            end
            object Panel31: TPanel
              Left = 0
              Top = 0
              Width = 93
              Height = 289
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Align = alLeft
              ParentBackground = False
              TabOrder = 1
              object BtEntiTransp: TBitBtn
                Tag = 105
                Left = 4
                Top = 4
                Width = 85
                Height = 40
                Cursor = crHandPoint
                Hint = 'Inclui novo registro'
                Caption = '&Transp.'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                TabStop = False
                OnClick = BtEntiTranspClick
              end
            end
          end
          object TabSheet10: TTabSheet
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Caption = ' Consultas'
            ImageIndex = 11
            object DBGEntiSrvPro: TDBGrid
              Left = 93
              Top = 0
              Width = 907
              Height = 289
              Align = alClient
              DataSource = DsEntiSrvPro
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_ORGAO'
                  Title.Caption = #211'rg'#227'o'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Numero'
                  Title.Caption = 'N'#250'mero da consulta'
                  Width = 112
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Hora'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Resultado'
                  Width = 647
                  Visible = True
                end>
            end
            object Panel30: TPanel
              Left = 0
              Top = 0
              Width = 93
              Height = 289
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Align = alLeft
              ParentBackground = False
              TabOrder = 1
              object BtEntiSrvPro: TBitBtn
                Tag = 328
                Left = 4
                Top = 4
                Width = 85
                Height = 40
                Cursor = crHandPoint
                Caption = 'C&OPC'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtEntiSrvProClick
              end
            end
          end
          object TabSheet15: TTabSheet
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Caption = 'Grupos'
            ImageIndex = 12
            object Panel18: TPanel
              Left = 0
              Top = 0
              Width = 93
              Height = 289
              Align = alLeft
              ParentBackground = False
              TabOrder = 0
              object BtGrupos: TBitBtn
                Tag = 243
                Left = 4
                Top = 4
                Width = 85
                Height = 40
                Cursor = crHandPoint
                Caption = '&Grupo'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtGruposClick
              end
            end
            object DBGrid1: TDBGrid
              Left = 93
              Top = 0
              Width = 907
              Height = 289
              Align = alClient
              DataSource = DsEntiGrupos
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBGrid1DblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'Grupo'
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 250
                  Visible = True
                end>
            end
          end
          object TabSheet20: TTabSheet
            Caption = 'Dados espec'#237'ficos'
            ImageIndex = 5
            object DBGrid3: TDBGrid
              Left = 0
              Top = 0
              Width = 1000
              Height = 289
              Align = alClient
              DataSource = DsCondImov
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Unidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TIPO'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CND'
                  Title.Caption = 'Condom'#237'nio'
                  Width = 486
                  Visible = True
                end>
            end
          end
          object TabSheet17: TTabSheet
            Caption = ' Status '
            ImageIndex = 14
            object DBGEntiStatus: TDBGrid
              Left = 0
              Top = 0
              Width = 1000
              Height = 289
              Align = alClient
              DataSource = DsEntiStatus
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDrawColumnCell = DBGEntiStatusDrawColumnCell
              OnDblClick = DBGEntiStatusDblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SELECI'
                  Title.Caption = 'Sel.'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Status'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_Status'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 860
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object PainelControle: TPanel
      Left = 0
      Top = 560
      Width = 1008
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 172
        Top = 0
        Width = 186
        Height = 48
        Align = alClient
        Caption = '[N]: 0'
        ExplicitWidth = 26
        ExplicitHeight = 13
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 172
        Height = 48
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 124
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 84
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 44
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 4
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 358
        Top = 0
        Width = 650
        Height = 48
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtContatos: TBitBtn
          Tag = 10092
          Left = 102
          Top = 4
          Width = 97
          Height = 40
          Cursor = crHandPoint
          Caption = '&Contatos'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtContatosClick
        end
        object BtEntidade: TBitBtn
          Tag = 10062
          Left = 4
          Top = 4
          Width = 98
          Height = 40
          Cursor = crHandPoint
          Caption = '&Entidade'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtEntidadeClick
        end
        object Panel2: TPanel
          Left = 540
          Top = 0
          Width = 110
          Height = 48
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 4
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 98
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtEMails: TBitBtn
          Tag = 66
          Left = 297
          Top = 4
          Width = 97
          Height = 40
          Cursor = crHandPoint
          Caption = 'E&Mails'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtEMailsClick
        end
        object BtTelefones: TBitBtn
          Tag = 92
          Left = 199
          Top = 4
          Width = 98
          Height = 40
          Cursor = crHandPoint
          Caption = '&Telefones'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtTelefonesClick
        end
        object BtPrazos: TBitBtn
          Tag = 407
          Left = 394
          Top = 4
          Width = 98
          Height = 40
          Cursor = crHandPoint
          Caption = '&Prazos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtPrazosClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 429
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object SbMapa: TBitBtn
        Tag = 471
        Left = 214
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = SbMapaClick
      end
      object SbImagens: TBitBtn
        Tag = 102
        Left = 256
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 6
        OnClick = SbImagensClick
      end
      object SBAssinaturas: TBitBtn
        Tag = 630
        Left = 340
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 7
        OnClick = SBAssinaturasClick
      end
      object SBConfig: TBitBtn
        Tag = 263
        Left = 382
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 8
        OnClick = SBConfigClick
      end
      object SbEntiDocs: TBitBtn
        Tag = 160
        Left = 298
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 9
        OnClick = SbEntiDocsClick
      end
    end
    object GB_M: TGroupBox
      Left = 429
      Top = 0
      Width = 531
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 507
        Height = 32
        Caption = 'Cadastros de Pessoas F'#237'sicas e Jur'#237'dicas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 507
        Height = 32
        Caption = 'Cadastros de Pessoas F'#237'sicas e Jur'#237'dicas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 507
        Height = 32
        Caption = 'Cadastros de Pessoas F'#237'sicas e Jur'#237'dicas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel22: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 96
    Width = 1008
    Height = 17
    Align = alTop
    TabOrder = 4
    Visible = False
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtEntidade
    CanUpd01 = BtContatos
    Left = 448
    Top = 24
  end
  object PMEntidades: TPopupMenu
    OnPopup = PMEntidadesPopup
    Left = 392
    Top = 52
    object Incluinovaentidade1: TMenuItem
      Caption = '&Inclui nova entidade'
      OnClick = Incluinovaentidade1Click
    end
    object Alteraentidadeatual1: TMenuItem
      Caption = '&Altera entidade atual'
      OnClick = Alteraentidadeatual1Click
    end
    object Excluientidadeatual1: TMenuItem
      Caption = '&Exclui entidade atual'
      Enabled = False
      OnClick = Excluientidadeatual1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object esteObservaes1: TMenuItem
      Caption = 'Teste Observa'#231#245'es'
      OnClick = esteObservaes1Click
    end
  end
  object QrEntiMail: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, ema.Conta, '
      'ema.EMail, ema.EntiTipCto, ema.Ordem'
      'FROM entimail ema'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto'#13
      'WHERE ema.Controle=:P0'
      'ORDER BY ema.Ordem, ema.Conta'#10)
    Left = 728
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiMailNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrEntiMailConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Required = True
      Size = 255
    end
    object QrEntiMailEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
    object QrEntiMailOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsEntiMail: TDataSource
    DataSet = QrEntiMail
    Left = 756
    Top = 24
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntidadesAfterOpen
    BeforeClose = QrEntidadesBeforeClose
    AfterScroll = QrEntidadesAfterScroll
    OnCalcFields = QrEntidadesCalcFields
    Left = 392
    Top = 24
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEntidadesCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'entidades.CodUsu'
      Required = True
    end
    object QrEntidadesRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Origin = 'entidades.RazaoSocial'
      Required = True
      Size = 100
    end
    object QrEntidadesFantasia: TWideStringField
      FieldName = 'Fantasia'
      Origin = 'entidades.Fantasia'
      Required = True
      Size = 60
    end
    object QrEntidadesRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrEntidadesRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Required = True
      Size = 60
    end
    object QrEntidadesPai: TWideStringField
      FieldName = 'Pai'
      Origin = 'entidades.Pai'
      Required = True
      Size = 60
    end
    object QrEntidadesMae: TWideStringField
      FieldName = 'Mae'
      Origin = 'entidades.Mae'
      Required = True
      Size = 60
    end
    object QrEntidadesCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'entidades.CNPJ'
      Size = 18
    end
    object QrEntidadesIE: TWideStringField
      FieldName = 'IE'
      Origin = 'entidades.IE'
      Size = 15
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'entidades.Nome'
      Required = True
      Size = 100
    end
    object QrEntidadesApelido: TWideStringField
      FieldName = 'Apelido'
      Origin = 'entidades.Apelido'
      Required = True
      Size = 60
    end
    object QrEntidadesCPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'entidades.CPF'
      Size = 18
    end
    object QrEntidadesRG: TWideStringField
      FieldName = 'RG'
      Origin = 'entidades.RG'
      Size = 15
    end
    object QrEntidadesERua: TWideStringField
      DisplayWidth = 60
      FieldName = 'ERua'
      Origin = 'entidades.ERua'
      Size = 60
    end
    object QrEntidadesECompl: TWideStringField
      DisplayWidth = 60
      FieldName = 'ECompl'
      Origin = 'entidades.ECompl'
      Size = 60
    end
    object QrEntidadesEBairro: TWideStringField
      DisplayWidth = 60
      FieldName = 'EBairro'
      Origin = 'entidades.EBairro'
      Size = 60
    end
    object QrEntidadesECidade: TWideStringField
      DisplayWidth = 60
      FieldName = 'ECidade'
      Origin = 'entidades.ECidade'
      Size = 60
    end
    object QrEntidadesEUF: TSmallintField
      FieldName = 'EUF'
      Origin = 'entidades.EUF'
      Required = True
    end
    object QrEntidadesEPais: TWideStringField
      DisplayWidth = 60
      FieldName = 'EPais'
      Origin = 'entidades.EPais'
      Size = 60
    end
    object QrEntidadesETe1: TWideStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrEntidadesETe2: TWideStringField
      FieldName = 'ETe2'
      Origin = 'entidades.Ete2'
    end
    object QrEntidadesETe3: TWideStringField
      FieldName = 'ETe3'
      Origin = 'entidades.Ete3'
    end
    object QrEntidadesECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrEntidadesEFax: TWideStringField
      FieldName = 'EFax'
      Origin = 'entidades.EFax'
    end
    object QrEntidadesEEMail: TWideStringField
      FieldName = 'EEMail'
      Origin = 'entidades.EEmail'
      Size = 100
    end
    object QrEntidadesEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrEntidadesPRua: TWideStringField
      DisplayWidth = 60
      FieldName = 'PRua'
      Origin = 'entidades.PRua'
      Size = 60
    end
    object QrEntidadesPCompl: TWideStringField
      DisplayWidth = 60
      FieldName = 'PCompl'
      Origin = 'entidades.PCompl'
      Size = 60
    end
    object QrEntidadesPBairro: TWideStringField
      DisplayWidth = 60
      FieldName = 'PBairro'
      Origin = 'entidades.PBairro'
      Size = 60
    end
    object QrEntidadesPCidade: TWideStringField
      DisplayWidth = 60
      FieldName = 'PCidade'
      Origin = 'entidades.PCidade'
      Size = 60
    end
    object QrEntidadesPUF: TSmallintField
      FieldName = 'PUF'
      Origin = 'entidades.PUF'
      Required = True
    end
    object QrEntidadesPPais: TWideStringField
      DisplayWidth = 60
      FieldName = 'PPais'
      Origin = 'entidades.PPais'
      Size = 60
    end
    object QrEntidadesPTe1: TWideStringField
      FieldName = 'PTe1'
      Origin = 'entidades.PTe1'
    end
    object QrEntidadesPTe2: TWideStringField
      FieldName = 'PTe2'
      Origin = 'entidades.Pte2'
    end
    object QrEntidadesPTe3: TWideStringField
      FieldName = 'PTe3'
      Origin = 'entidades.Pte3'
    end
    object QrEntidadesPCel: TWideStringField
      FieldName = 'PCel'
      Origin = 'entidades.PCel'
    end
    object QrEntidadesPFax: TWideStringField
      FieldName = 'PFax'
      Origin = 'entidades.PFax'
    end
    object QrEntidadesPEMail: TWideStringField
      FieldName = 'PEMail'
      Origin = 'entidades.PEmail'
      Size = 100
    end
    object QrEntidadesPContato: TWideStringField
      FieldName = 'PContato'
      Origin = 'entidades.PContato'
      Size = 60
    end
    object QrEntidadesSexo: TWideStringField
      FieldName = 'Sexo'
      Origin = 'entidades.Sexo'
      Required = True
      Size = 1
    end
    object QrEntidadesResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Origin = 'entidades.Responsavel'
      Size = 60
    end
    object QrEntidadesProfissao: TWideStringField
      FieldName = 'Profissao'
      Origin = 'entidades.Profissao'
      Size = 60
    end
    object QrEntidadesCargo: TWideStringField
      FieldName = 'Cargo'
      Origin = 'entidades.Cargo'
      Size = 60
    end
    object QrEntidadesRecibo: TSmallintField
      FieldName = 'Recibo'
      Origin = 'entidades.Recibo'
      Required = True
    end
    object QrEntidadesDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Origin = 'entidades.DiaRecibo'
      Required = True
    end
    object QrEntidadesAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Origin = 'entidades.AjudaEmpV'
      Required = True
    end
    object QrEntidadesAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Origin = 'entidades.AjudaEmpP'
      Required = True
    end
    object QrEntidadesCliente1: TWideStringField
      FieldName = 'Cliente1'
      Origin = 'entidades.Cliente1'
      Size = 1
    end
    object QrEntidadesCliente2: TWideStringField
      FieldName = 'Cliente2'
      Origin = 'entidades.Cliente2'
      Size = 1
    end
    object QrEntidadesCliente3: TWideStringField
      FieldName = 'Cliente3'
      Origin = 'entidades.Cliente3'
      Size = 1
    end
    object QrEntidadesCliente4: TWideStringField
      FieldName = 'Cliente4'
      Origin = 'entidades.Cliente4'
      Size = 1
    end
    object QrEntidadesFornece1: TWideStringField
      FieldName = 'Fornece1'
      Origin = 'entidades.Fornece1'
      Size = 1
    end
    object QrEntidadesFornece2: TWideStringField
      FieldName = 'Fornece2'
      Origin = 'entidades.Fornece2'
      Size = 1
    end
    object QrEntidadesFornece3: TWideStringField
      FieldName = 'Fornece3'
      Origin = 'entidades.Fornece3'
      Size = 1
    end
    object QrEntidadesFornece4: TWideStringField
      FieldName = 'Fornece4'
      Origin = 'entidades.Fornece4'
      Size = 1
    end
    object QrEntidadesFornece5: TWideStringField
      FieldName = 'Fornece5'
      Origin = 'entidades.Fornece5'
      Size = 1
    end
    object QrEntidadesFornece6: TWideStringField
      FieldName = 'Fornece6'
      Origin = 'entidades.Fornece6'
      Size = 1
    end
    object QrEntidadesFornece7: TWideStringField
      FieldName = 'Fornece7'
      Origin = 'entidades.Fornece7'
      Size = 1
    end
    object QrEntidadesFornece8: TWideStringField
      FieldName = 'Fornece8'
      Origin = 'entidades.Fornece8'
      Size = 1
    end
    object QrEntidadesTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Origin = 'entidades.Terceiro'
      Size = 1
    end
    object QrEntidadesCadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrEntidadesInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Origin = 'entidades.Informacoes'
      Size = 255
    end
    object QrEntidadesVeiculo: TIntegerField
      FieldName = 'Veiculo'
      Origin = 'entidades.Veiculo'
    end
    object QrEntidadesMensal: TWideStringField
      FieldName = 'Mensal'
      Origin = 'entidades.Mensal'
      Size = 1
    end
    object QrEntidadesObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
    end
    object QrEntidadesTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrEntidadesLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'entidades.Lk'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrEntidadesNOMEEUF: TWideStringField
      FieldName = 'NOMEEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesPCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE2_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE3_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE2_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE3_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesECEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesEFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesECEP: TIntegerField
      FieldName = 'ECEP'
      Origin = 'entidades.ECEP'
    end
    object QrEntidadesPCEP: TIntegerField
      FieldName = 'PCEP'
      Origin = 'entidades.PCEP'
    end
    object QrEntidadesGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'entidades.Grupo'
    end
    object QrEntidadesDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'entidades.DataAlt'
    end
    object QrEntidadesUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'entidades.UserCad'
    end
    object QrEntidadesUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'entidades.UserAlt'
    end
    object QrEntidadesCRua: TWideStringField
      FieldName = 'CRua'
      Origin = 'entidades.CRua'
      Size = 60
    end
    object QrEntidadesCCompl: TWideStringField
      FieldName = 'CCompl'
      Origin = 'entidades.CCompl'
      Size = 60
    end
    object QrEntidadesCBairro: TWideStringField
      FieldName = 'CBairro'
      Origin = 'entidades.CBairro'
      Size = 60
    end
    object QrEntidadesCCidade: TWideStringField
      FieldName = 'CCidade'
      Origin = 'entidades.CCidade'
      Size = 60
    end
    object QrEntidadesCUF: TSmallintField
      FieldName = 'CUF'
      Origin = 'entidades.CUF'
      Required = True
    end
    object QrEntidadesCCEP: TIntegerField
      FieldName = 'CCEP'
      Origin = 'entidades.CCEP'
    end
    object QrEntidadesCPais: TWideStringField
      FieldName = 'CPais'
      Origin = 'entidades.CPais'
    end
    object QrEntidadesCTel: TWideStringField
      FieldName = 'CTel'
      Origin = 'entidades.CTel'
    end
    object QrEntidadesCFax: TWideStringField
      FieldName = 'CFax'
      Origin = 'entidades.CFax'
    end
    object QrEntidadesCCel: TWideStringField
      FieldName = 'CCel'
      Origin = 'entidades.CCel'
    end
    object QrEntidadesCContato: TWideStringField
      FieldName = 'CContato'
      Origin = 'entidades.CContato'
      Size = 60
    end
    object QrEntidadesLRua: TWideStringField
      FieldName = 'LRua'
      Origin = 'entidades.LRua'
      Size = 60
    end
    object QrEntidadesLCompl: TWideStringField
      FieldName = 'LCompl'
      Origin = 'entidades.LCompl'
      Size = 60
    end
    object QrEntidadesLBairro: TWideStringField
      FieldName = 'LBairro'
      Origin = 'entidades.LBairro'
      Size = 60
    end
    object QrEntidadesLCidade: TWideStringField
      FieldName = 'LCidade'
      Origin = 'entidades.LCidade'
      Size = 60
    end
    object QrEntidadesLUF: TSmallintField
      FieldName = 'LUF'
      Origin = 'entidades.LUF'
      Required = True
    end
    object QrEntidadesLCEP: TIntegerField
      FieldName = 'LCEP'
      Origin = 'entidades.LCEP'
    end
    object QrEntidadesLPais: TWideStringField
      FieldName = 'LPais'
      Origin = 'entidades.LPais'
    end
    object QrEntidadesLTel: TWideStringField
      FieldName = 'LTel'
      Origin = 'entidades.LTel'
    end
    object QrEntidadesLFax: TWideStringField
      FieldName = 'LFax'
      Origin = 'entidades.LFax'
    end
    object QrEntidadesLCel: TWideStringField
      FieldName = 'LCel'
      Origin = 'entidades.LCel'
    end
    object QrEntidadesLContato: TWideStringField
      FieldName = 'LContato'
      Origin = 'entidades.LContato'
      Size = 60
    end
    object QrEntidadesComissao: TFloatField
      FieldName = 'Comissao'
      Origin = 'entidades.Comissao'
      DisplayFormat = '0.000000'
    end
    object QrEntidadesDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'entidades.DataCad'
      Required = True
    end
    object QrEntidadesECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesPCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesCCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesLCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Size = 100
      Calculated = True
    end
    object QrEntidadesNOMEALT2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEALT2'
      Size = 100
      Calculated = True
    end
    object QrEntidadesSituacao: TSmallintField
      FieldName = 'Situacao'
      Origin = 'entidades.Situacao'
    end
    object QrEntidadesNivel: TWideStringField
      FieldName = 'Nivel'
      Origin = 'entidades.Nivel'
      Size = 1
    end
    object QrEntidadesCTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CTEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LTEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesNOMEENTIGRUPO: TWideStringField
      FieldName = 'NOMEENTIGRUPO'
      Origin = 'entigrupos.Nome'
      Size = 100
    end
    object QrEntidadesNOMECUF: TWideStringField
      FieldName = 'NOMECUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMELUF: TWideStringField
      FieldName = 'NOMELUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesAccount: TIntegerField
      FieldName = 'Account'
      Origin = 'entidades.Account'
    end
    object QrEntidadesNOMEACCOUNT: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEACCOUNT'
      Size = 100
    end
    object QrEntidadesELograd: TSmallintField
      FieldName = 'ELograd'
      Origin = 'entidades.ELograd'
      Required = True
    end
    object QrEntidadesPLograd: TSmallintField
      FieldName = 'PLograd'
      Origin = 'entidades.PLograd'
      Required = True
    end
    object QrEntidadesConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Origin = 'entidades.ConjugeNome'
      Size = 35
    end
    object QrEntidadesConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
      Origin = 'entidades.ConjugeNatal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEntidadesNome1: TWideStringField
      FieldName = 'Nome1'
      Origin = 'entidades.Nome1'
      Size = 30
    end
    object QrEntidadesNatal1: TDateField
      FieldName = 'Natal1'
      Origin = 'entidades.Natal1'
    end
    object QrEntidadesNome2: TWideStringField
      FieldName = 'Nome2'
      Origin = 'entidades.Nome2'
      Size = 30
    end
    object QrEntidadesNatal2: TDateField
      FieldName = 'Natal2'
      Origin = 'entidades.Natal2'
    end
    object QrEntidadesNome3: TWideStringField
      FieldName = 'Nome3'
      Origin = 'entidades.Nome3'
      Size = 30
    end
    object QrEntidadesNatal3: TDateField
      FieldName = 'Natal3'
      Origin = 'entidades.Natal3'
    end
    object QrEntidadesCreditosI: TIntegerField
      FieldName = 'CreditosI'
      Origin = 'entidades.CreditosI'
      Required = True
    end
    object QrEntidadesCreditosL: TIntegerField
      FieldName = 'CreditosL'
      Origin = 'entidades.CreditosL'
      Required = True
    end
    object QrEntidadesCreditosD: TDateField
      FieldName = 'CreditosD'
      Origin = 'entidades.CreditosD'
    end
    object QrEntidadesCreditosU: TDateField
      FieldName = 'CreditosU'
      Origin = 'entidades.CreditosU'
    end
    object QrEntidadesCreditosV: TDateField
      FieldName = 'CreditosV'
      Origin = 'entidades.CreditosV'
    end
    object QrEntidadesMotivo: TIntegerField
      FieldName = 'Motivo'
      Origin = 'entidades.Motivo'
      Required = True
    end
    object QrEntidadesQuantI1: TIntegerField
      FieldName = 'QuantI1'
      Origin = 'entidades.QuantI1'
      Required = True
    end
    object QrEntidadesQuantI2: TIntegerField
      FieldName = 'QuantI2'
      Origin = 'entidades.QuantI2'
      Required = True
    end
    object QrEntidadesQuantI3: TIntegerField
      FieldName = 'QuantI3'
      Origin = 'entidades.QuantI3'
      Required = True
    end
    object QrEntidadesQuantI4: TIntegerField
      FieldName = 'QuantI4'
      Origin = 'entidades.QuantI4'
      Required = True
    end
    object QrEntidadesAgenda: TWideStringField
      FieldName = 'Agenda'
      Origin = 'entidades.Agenda'
      Required = True
      Size = 1
    end
    object QrEntidadesSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Origin = 'entidades.SenhaQuer'
      Required = True
      Size = 1
    end
    object QrEntidadesSenha1: TWideStringField
      FieldName = 'Senha1'
      Origin = 'entidades.Senha1'
      Size = 6
    end
    object QrEntidadesNatal4: TDateField
      FieldName = 'Natal4'
      Origin = 'entidades.Natal4'
    end
    object QrEntidadesNome4: TWideStringField
      FieldName = 'Nome4'
      Origin = 'entidades.Nome4'
      Size = 30
    end
    object QrEntidadesNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Origin = 'motivose.Descricao'
      Size = 50
    end
    object QrEntidadesCreditosF2: TFloatField
      FieldName = 'CreditosF2'
      Origin = 'entidades.CreditosF2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesNOMEELOGRAD: TWideStringField
      FieldName = 'NOMEELOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesNOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesCLograd: TSmallintField
      FieldName = 'CLograd'
      Origin = 'entidades.CLograd'
      Required = True
    end
    object QrEntidadesLLograd: TSmallintField
      FieldName = 'LLograd'
      Origin = 'entidades.LLograd'
      Required = True
    end
    object QrEntidadesNOMECLOGRAD: TWideStringField
      FieldName = 'NOMECLOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesNOMELLOGRAD: TWideStringField
      FieldName = 'NOMELLOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesQuantN1: TFloatField
      FieldName = 'QuantN1'
      Origin = 'entidades.QuantN1'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesQuantN2: TFloatField
      FieldName = 'QuantN2'
      Origin = 'entidades.QuantN2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesLimiCred: TFloatField
      FieldName = 'LimiCred'
      Origin = 'entidades.LimiCred'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesDesco: TFloatField
      FieldName = 'Desco'
      Origin = 'entidades.Desco'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrEntidadesCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Origin = 'entidades.CasasApliDesco'
      Required = True
    end
    object QrEntidadesCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Origin = 'entidades.CPF_Pai'
      Size = 18
    end
    object QrEntidadesSSP: TWideStringField
      FieldName = 'SSP'
      Origin = 'entidades.SSP'
      Size = 10
    end
    object QrEntidadesCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Origin = 'entidades.CidadeNatal'
      Size = 30
    end
    object QrEntidadesCPF_PAI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_PAI_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Origin = 'entidades.UFNatal'
      Required = True
    end
    object QrEntidadesNOMENUF: TWideStringField
      FieldName = 'NOMENUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesFatorCompra: TFloatField
      FieldName = 'FatorCompra'
      Origin = 'entidades.FatorCompra'
      DisplayFormat = '#,###,###0.0000'
    end
    object QrEntidadesAdValorem: TFloatField
      FieldName = 'AdValorem'
      Origin = 'entidades.AdValorem'
      DisplayFormat = '#,###,###0.0000'
    end
    object QrEntidadesDMaisC: TIntegerField
      FieldName = 'DMaisC'
      Origin = 'entidades.DMaisC'
      DisplayFormat = '0'
    end
    object QrEntidadesDMaisD: TIntegerField
      FieldName = 'DMaisD'
      Origin = 'entidades.DMaisD'
      DisplayFormat = '0'
    end
    object QrEntidadesDataRG: TDateField
      FieldName = 'DataRG'
      Origin = 'entidades.DataRG'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEntidadesNacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Origin = 'entidades.Nacionalid'
      Size = 15
    end
    object QrEntidadesEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'entidades.Empresa'
    end
    object QrEntidadesNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'entidades.RazaoSocial'
      Size = 100
    end
    object QrEntidadesFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
      Origin = 'entidades.FormaSociet'
    end
    object QrEntidadesSimples: TSmallintField
      FieldName = 'Simples'
      Origin = 'entidades.Simples'
      Required = True
    end
    object QrEntidadesAtividade: TWideStringField
      FieldName = 'Atividade'
      Origin = 'entidades.Atividade'
      Size = 50
    end
    object QrEntidadesEstCivil: TSmallintField
      FieldName = 'EstCivil'
      Origin = 'entidades.EstCivil'
      Required = True
    end
    object QrEntidadesNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Origin = 'listaecivil.Nome'
      Required = True
      Size = 10
    end
    object QrEntidadesCPF_Conjuge: TWideStringField
      FieldName = 'CPF_Conjuge'
      Origin = 'entidades.CPF_Conjuge'
      Size = 18
    end
    object QrEntidadesCBE: TIntegerField
      FieldName = 'CBE'
      Origin = 'entidades.CBE'
    end
    object QrEntidadesSCB: TIntegerField
      FieldName = 'SCB'
      Origin = 'entidades.SCB'
    end
    object QrEntidadesCPF_Resp1: TWideStringField
      FieldName = 'CPF_Resp1'
      Origin = 'entidades.CPF_Resp1'
      Size = 18
    end
    object QrEntidadesCPF_Resp1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_Resp1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesENumero: TIntegerField
      FieldName = 'ENumero'
      Origin = 'entidades.ENumero'
    end
    object QrEntidadesPNumero: TIntegerField
      FieldName = 'PNumero'
      Origin = 'entidades.PNumero'
    end
    object QrEntidadesCNumero: TIntegerField
      FieldName = 'CNumero'
      Origin = 'entidades.CNumero'
    end
    object QrEntidadesLNumero: TIntegerField
      FieldName = 'LNumero'
      Origin = 'entidades.LNumero'
    end
    object QrEntidadesBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'entidades.Banco'
      DisplayFormat = '000'
    end
    object QrEntidadesAgencia: TWideStringField
      DisplayWidth = 4
      FieldName = 'Agencia'
      Origin = 'entidades.Agencia'
      Size = 11
    end
    object QrEntidadesContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'entidades.ContaCorrente'
      Size = 15
    end
    object QrEntidadesCartPref: TIntegerField
      FieldName = 'CartPref'
      Origin = 'entidades.CartPref'
      Required = True
    end
    object QrEntidadesNOMECARTPREF: TWideStringField
      FieldName = 'NOMECARTPREF'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrEntidadesRolComis: TIntegerField
      FieldName = 'RolComis'
      Origin = 'entidades.RolComis'
      Required = True
    end
    object QrEntidadesFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
      Required = True
    end
    object QrEntidadesIEST: TWideStringField
      FieldName = 'IEST'
      Origin = 'entidades.IEST'
    end
    object QrEntidadesQuantN3: TFloatField
      FieldName = 'QuantN3'
      Origin = 'entidades.QuantN3'
    end
    object QrEntidadesTempA: TFloatField
      FieldName = 'TempA'
      Origin = 'entidades.TempA'
    end
    object QrEntidadesTempD: TFloatField
      FieldName = 'TempD'
      Origin = 'entidades.TempD'
    end
    object QrEntidadesPAtividad: TIntegerField
      FieldName = 'PAtividad'
      Origin = 'entidades.PAtividad'
    end
    object QrEntidadesEAtividad: TIntegerField
      FieldName = 'EAtividad'
      Origin = 'entidades.EAtividad'
    end
    object QrEntidadesPCidadeCod: TIntegerField
      FieldName = 'PCidadeCod'
      Origin = 'entidades.PCidadeCod'
    end
    object QrEntidadesECidadeCod: TIntegerField
      FieldName = 'ECidadeCod'
      Origin = 'entidades.ECidadeCod'
    end
    object QrEntidadesPPaisCod: TIntegerField
      FieldName = 'PPaisCod'
      Origin = 'entidades.PPaisCod'
    end
    object QrEntidadesEPaisCod: TIntegerField
      FieldName = 'EPaisCod'
      Origin = 'entidades.EPaisCod'
    end
    object QrEntidadesAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'entidades.Antigo'
    end
    object QrEntidadesCUF2: TWideStringField
      FieldName = 'CUF2'
      Origin = 'entidades.CUF2'
      Size = 2
    end
    object QrEntidadesContab: TWideStringField
      FieldName = 'Contab'
      Origin = 'entidades.Contab'
    end
    object QrEntidadesMSN1: TWideStringField
      FieldName = 'MSN1'
      Origin = 'entidades.MSN1'
      Size = 255
    end
    object QrEntidadesPastaTxtFTP: TWideStringField
      FieldName = 'PastaTxtFTP'
      Origin = 'entidades.PastaTxtFTP'
      Size = 8
    end
    object QrEntidadesPastaPwdFTP: TWideStringField
      FieldName = 'PastaPwdFTP'
      Origin = 'entidades.PastaPwdFTP'
      Size = 100
    end
    object QrEntidadesProtestar: TSmallintField
      FieldName = 'Protestar'
      Origin = 'entidades.Protestar'
      Required = True
    end
    object QrEntidadesMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
      Origin = 'entidades.MultaCodi'
      Required = True
    end
    object QrEntidadesMultaDias: TSmallintField
      FieldName = 'MultaDias'
      Origin = 'entidades.MultaDias'
      Required = True
    end
    object QrEntidadesMultaValr: TFloatField
      FieldName = 'MultaValr'
      Origin = 'entidades.MultaValr'
      Required = True
    end
    object QrEntidadesMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'entidades.MultaPerc'
      Required = True
    end
    object QrEntidadesMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
      Origin = 'entidades.MultaTiVe'
      Required = True
    end
    object QrEntidadesJuroSacado: TFloatField
      FieldName = 'JuroSacado'
      Origin = 'entidades.JuroSacado'
      Required = True
    end
    object QrEntidadesCPMF: TFloatField
      FieldName = 'CPMF'
      Origin = 'entidades.CPMF'
      Required = True
    end
    object QrEntidadesCorrido: TIntegerField
      FieldName = 'Corrido'
      Origin = 'entidades.Corrido'
      Required = True
    end
    object QrEntidadesCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
      Required = True
    end
    object QrEntidadesAltDtPlaCt: TDateField
      FieldName = 'AltDtPlaCt'
      Origin = 'entidades.AltDtPlaCt'
      Required = True
    end
    object QrEntidadesAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'entidades.AlterWeb'
      Required = True
    end
    object QrEntidadesAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'entidades.Ativo'
      Required = True
    end
    object QrEntidadesEEndeRef: TWideStringField
      FieldName = 'EEndeRef'
      Origin = 'entidades.EEndeRef'
      Size = 100
    end
    object QrEntidadesPEndeRef: TWideStringField
      FieldName = 'PEndeRef'
      Origin = 'entidades.PEndeRef'
      Size = 100
    end
    object QrEntidadesCEndeRef: TWideStringField
      FieldName = 'CEndeRef'
      Origin = 'entidades.CEndeRef'
      Size = 100
    end
    object QrEntidadesLEndeRef: TWideStringField
      FieldName = 'LEndeRef'
      Origin = 'entidades.LEndeRef'
      Size = 100
    end
    object QrEntidadesPNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PNUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesENUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesCNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesLNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LNUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesDATARG_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATARG_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntidadesECodMunici: TIntegerField
      FieldName = 'ECodMunici'
      Origin = 'entidades.ECodMunici'
    end
    object QrEntidadesPCodMunici: TIntegerField
      FieldName = 'PCodMunici'
      Origin = 'entidades.PCodMunici'
    end
    object QrEntidadesCCodMunici: TIntegerField
      FieldName = 'CCodMunici'
      Origin = 'entidades.CCodMunici'
    end
    object QrEntidadesLCodMunici: TIntegerField
      FieldName = 'LCodMunici'
      Origin = 'entidades.LCodMunici'
    end
    object QrEntidadesCNAE: TWideStringField
      FieldName = 'CNAE'
      Origin = 'entidades.CNAE'
      Size = 7
    end
    object QrEntidadesSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Origin = 'entidades.SUFRAMA'
      Size = 9
    end
    object QrEntidadesECodiPais: TIntegerField
      FieldName = 'ECodiPais'
      Origin = 'entidades.ECodiPais'
    end
    object QrEntidadesPCodiPais: TIntegerField
      FieldName = 'PCodiPais'
      Origin = 'entidades.PCodiPais'
    end
    object QrEntidadesL_CNPJ: TWideStringField
      FieldName = 'L_CNPJ'
      Origin = 'entidades.L_CNPJ'
      Size = 14
    end
    object QrEntidadesL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
      Origin = 'entidades.L_Ativo'
    end
    object QrEntidadesL_CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'L_CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesNO_DTB_EMUNICI: TWideStringField
      FieldName = 'NO_DTB_EMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_DTB_PMUNICI: TWideStringField
      FieldName = 'NO_DTB_PMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_DTB_CMUNICI: TWideStringField
      FieldName = 'NO_DTB_CMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_DTB_LMUNICI: TWideStringField
      FieldName = 'NO_DTB_LMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_BACEN_EPAIS: TWideStringField
      FieldName = 'NO_BACEN_EPAIS'
      Origin = 'bacen_pais.Nome'
      Size = 100
    end
    object QrEntidadesNO_BACEN_PPAIS: TWideStringField
      FieldName = 'NO_BACEN_PPAIS'
      Origin = 'bacen_pais.Nome'
      Size = 100
    end
    object QrEntidadesNO_BACEN_LPAIS: TWideStringField
      FieldName = 'NO_BACEN_LPAIS'
      Origin = 'bacen_pais.Nome'
      Size = 100
    end
    object QrEntidadesCNAE_Nome: TWideStringField
      FieldName = 'CNAE_Nome'
      Origin = 'cnae20n.Nome'
      Size = 255
    end
    object QrEntidadesNIRE: TWideStringField
      FieldName = 'NIRE'
      Origin = 'entidades.NIRE'
      Size = 15
    end
    object QrEntidadesIEST_TXT: TWideStringField
      DisplayWidth = 30
      FieldKind = fkCalculated
      FieldName = 'IEST_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesURL: TWideStringField
      FieldName = 'URL'
      Origin = 'entidades.URL'
      Size = 60
    end
    object QrEntidadesCRT: TSmallintField
      FieldName = 'CRT'
    end
    object QrEntidadesCOD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEntidadesETe1Tip: TIntegerField
      FieldName = 'ETe1Tip'
    end
    object QrEntidadesECelTip: TIntegerField
      FieldName = 'ECelTip'
    end
    object QrEntidadesPTe1Tip: TIntegerField
      FieldName = 'PTe1Tip'
    end
    object QrEntidadesPCelTip: TIntegerField
      FieldName = 'PCelTip'
    end
    object QrEntidadesPTe1Tip_TXT: TWideStringField
      FieldName = 'PTe1Tip_TXT'
      Size = 30
    end
    object QrEntidadesPCelTip_TXT: TWideStringField
      FieldName = 'PCelTip_TXT'
      Size = 30
    end
    object QrEntidadesETe1Tip_TXT: TWideStringField
      FieldName = 'ETe1Tip_TXT'
      Size = 30
    end
    object QrEntidadesECelTip_TXT: TWideStringField
      FieldName = 'ECelTip_TXT'
      Size = 30
    end
    object QrEntidadesENATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENATAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntidadesPNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PNATAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntidadesENatal: TDateField
      FieldName = 'ENatal'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEntidadesPNatal: TDateField
      FieldName = 'PNatal'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEntidadesESite: TWideStringField
      FieldName = 'ESite'
      Size = 100
    end
    object QrEntidadesPSite: TWideStringField
      FieldName = 'PSite'
      Size = 100
    end
    object QrEntidadesindIEDest: TSmallintField
      FieldName = 'indIEDest'
    end
    object QrEntidadesEstrangDef: TSmallintField
      FieldName = 'EstrangDef'
    end
    object QrEntidadesEstrangTip: TSmallintField
      FieldName = 'EstrangTip'
    end
    object QrEntidadesEstrangNum: TWideStringField
      FieldName = 'EstrangNum'
    end
    object QrEntidadesL_CPF: TWideStringField
      FieldName = 'L_CPF'
      Size = 18
    end
    object QrEntidadesL_Nome: TWideStringField
      FieldName = 'L_Nome'
      Size = 60
    end
    object QrEntidadesLCodiPais: TIntegerField
      FieldName = 'LCodiPais'
    end
    object QrEntidadesLEmail: TWideStringField
      FieldName = 'LEmail'
      Size = 100
    end
    object QrEntidadesL_IE: TWideStringField
      FieldName = 'L_IE'
    end
    object QrEntidadesL_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'L_CPF_TXT'
      Size = 25
      Calculated = True
    end
    object QrEntidadesCtbPlaCta: TIntegerField
      FieldName = 'CtbPlaCta'
    end
    object QrEntidadesNO_CtbPlaCta: TWideStringField
      FieldName = 'NO_CtbPlaCta'
      Size = 60
    end
    object QrEntidadesCtbCadGru: TIntegerField
      FieldName = 'CtbCadGru'
    end
    object QrEntidadesNO_CtbCadGru: TWideStringField
      FieldName = 'NO_CtbCadGru'
      Size = 60
    end
    object QrEntidadesMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrEntidadesSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 420
    Top = 24
  end
  object QrListaLograd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM listalograd'
      'ORDER BY Nome')
    Left = 476
    Top = 24
    object QrListaLogradCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaLogradNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 10
    end
  end
  object DsEListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 504
    Top = 24
  end
  object DsLListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 588
    Top = 24
  end
  object DsCListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 560
    Top = 24
  end
  object DsPListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 532
    Top = 24
  end
  object PMCEP: TPopupMenu
    Left = 420
    Top = 52
    object WebServiceVerificar1: TMenuItem
      Caption = 'WebService - Verificar'
      OnClick = WebServiceVerificar1Click
    end
    object Descobrir1: TMenuItem
      Caption = 'BD - &Descobrir'
      Enabled = False
      OnClick = Descobrir1Click
    end
    object Verificar1: TMenuItem
      Caption = 'BD - &Verificar'
      Enabled = False
      OnClick = Verificar1Click
    end
    object Internet1: TMenuItem
      Caption = 'Navegador - Verificar'
      OnClick = Internet1Click
    end
    object Correios1: TMenuItem
      Caption = 'WS - Correios'
      OnClick = Correios1Click
    end
  end
  object PMContatos: TPopupMenu
    OnPopup = PMContatosPopup
    Left = 448
    Top = 52
    object Incluinovocontato1: TMenuItem
      Caption = '&Inclui &novo contato'
      OnClick = Incluinovocontato1Click
    end
    object Incluinovocontatoporatrelamento1: TMenuItem
      Caption = 'Inclui novo contato por &atrelamento'
      OnClick = Incluinovocontatoporatrelamento1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object AlteraContatoatual1: TMenuItem
      Caption = '&Altera contato atual'
      OnClick = AlteraContatoatual1Click
    end
    object Excluicontatos1: TMenuItem
      Caption = '&Exclui contato(s)'
      OnClick = Excluicontatos1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Gerenciacontatos1: TMenuItem
      Caption = '&Gerencia contatos'
      OnClick = Gerenciacontatos1Click
    end
  end
  object DsEntiTel: TDataSource
    DataSet = QrEntiTel
    Left = 812
    Top = 24
  end
  object QrEntiTel: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiTelCalcFields
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, emt.Conta, '
      'emt.Telefone, emt.EntiTipCto, emt.Ramal'
      'FROM entitel emt'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto'#13
      'WHERE emt.Controle=:P0')
    Left = 784
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTelNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Origin = 'entitipcto.Nome'
      Size = 30
    end
    object QrEntiTelConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'entitel.Conta'
      Required = True
    end
    object QrEntiTelTelefone: TWideStringField
      FieldName = 'Telefone'
      Origin = 'entitel.Telefone'
    end
    object QrEntiTelEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'entitel.EntiTipCto'
      Required = True
    end
    object QrEntiTelTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiTelRamal: TWideStringField
      FieldName = 'Ramal'
      Origin = 'entitel.Ramal'
    end
  end
  object PMEMails: TPopupMenu
    OnPopup = PMEMailsPopup
    Left = 504
    Top = 52
    object NovoEMail1: TMenuItem
      Caption = '&Novo e-mail'
      OnClick = NovoEMail1Click
    end
    object Alteraemailselecionado1: TMenuItem
      Caption = '&Altera e-mail selecionado'
      OnClick = Alteraemailselecionado1Click
    end
    object Excluiemailselecionado1: TMenuItem
      Caption = '&Exclui e-mail selecionado'
      OnClick = Excluiemailselecionado1Click
    end
  end
  object PMTelefones: TPopupMenu
    OnPopup = PMTelefonesPopup
    Left = 532
    Top = 52
    object Incluinovotelefone1: TMenuItem
      Caption = '&Inclui novo telefone'
      OnClick = Incluinovotelefone1Click
    end
    object Alteratelefoneatual1: TMenuItem
      Caption = '&Altera telefone atual'
      OnClick = Alteratelefoneatual1Click
    end
    object Excluitelefones1: TMenuItem
      Caption = '&Exclui telefone(s)'
      OnClick = Excluitelefones1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object DefinircomofonedaNFe1: TMenuItem
      Caption = '&Definir como fone da NFe'
      OnClick = DefinircomofonedaNFe2Click
    end
  end
  object QrDuplic2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM entidades'
      'WHERE CNPJ=:P0'
      'AND Codigo <> :P1')
    Left = 896
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplic2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrNext: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(CodUsu) CodUsu '
      'FROM entidades')
    Left = 280
    Top = 204
    object QrNextCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
  end
  object PMAtributos: TPopupMenu
    OnPopup = PMAtributosPopup
    Left = 560
    Top = 52
    object IncluiAtributo1: TMenuItem
      Caption = '&Inclui um novo atributo'
      OnClick = IncluiAtributo1Click
    end
    object AlteraAtributo1: TMenuItem
      Caption = '&Altera o atributo selecionado'
      OnClick = AlteraAtributo1Click
    end
    object ExcluiAtributo1: TMenuItem
      Caption = '&Exclui o atributo selecionado'
      OnClick = ExcluiAtributo1Click
    end
  end
  object QrEntiRespon: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiResponCalcFields
    SQL.Strings = (
      'SELECT ere.*, eca.Nome NOME_CARGO ,'
      'ELT(ere.Assina+1, "N'#227'o", "Sim", "???") NO_ASSINA,'
      'IF(ent.Tipo=0, ent.ETe1, PTe1) Te1,'
      'IF(ent.Tipo=0, ent.ECel, PCel) Cel,'
      'IF(ent.Tipo=0, ent.EEmail, PEmail) Email'
      'FROM entirespon ere'
      'LEFT JOIN enticargos eca ON eca.Codigo=ere.Cargo'
      'LEFT JOIN entidades ent ON ent.Codigo = ere.Entidade'
      'WHERE ere.Codigo=:P0'
      ''
      ''
      ''
      '')
    Left = 672
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiResponCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiResponControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiResponNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiResponCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrEntiResponAssina: TSmallintField
      FieldName = 'Assina'
      Required = True
    end
    object QrEntiResponOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Required = True
    end
    object QrEntiResponObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrEntiResponNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrEntiResponNO_ASSINA: TWideStringField
      FieldName = 'NO_ASSINA'
      Size = 3
    end
    object QrEntiResponMandatoIni: TDateField
      FieldName = 'MandatoIni'
    end
    object QrEntiResponMandatoFim: TDateField
      FieldName = 'MandatoFim'
    end
    object QrEntiResponMandatoIni_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoIni_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntiResponMandatoFim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoFim_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntiResponTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrEntiResponCel: TWideStringField
      FieldName = 'Cel'
    end
    object QrEntiResponEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrEntiResponEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrEntiResponTe1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiResponCel_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Cel_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsEntiRespon: TDataSource
    DataSet = QrEntiRespon
    Left = 700
    Top = 24
  end
  object PMRespon: TPopupMenu
    OnPopup = PMResponPopup
    Left = 476
    Top = 52
    object Incluinovoresponsvel1: TMenuItem
      Caption = '&Inclui novo respons'#225'vel'
      OnClick = Incluinovoresponsvel1Click
    end
    object Alteraresponsvelatual1: TMenuItem
      Caption = '&Altera respons'#225'vel atual'
      OnClick = Alteraresponsvelatual1Click
    end
    object Excluiresponsveleis1: TMenuItem
      Caption = '&Exclui respons'#225'vel (eis)'
      OnClick = Excluiresponsveleis1Click
    end
  end
  object QrEMunici: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 924
    Top = 24
    object QrEMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEMuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEMunici: TDataSource
    DataSet = QrEMunici
    Left = 952
    Top = 24
  end
  object DsLMunici: TDataSource
    DataSet = QrLMunici
    Left = 952
    Top = 108
  end
  object DsCMunici: TDataSource
    DataSet = QrCMunici
    Left = 952
    Top = 80
  end
  object DsPMunici: TDataSource
    DataSet = QrPMunici
    Left = 952
    Top = 52
  end
  object QrPMunici: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 924
    Top = 52
    object QrPMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPMuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrCMunici: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 924
    Top = 80
    object QrCMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCMuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrLMunici: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 924
    Top = 108
    object QrLMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLMuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrEBacen_Pais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bacen_pais'
      'ORDER BY Nome'
      '')
    Left = 868
    Top = 80
    object QrEBacen_PaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEBacen_PaisNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEBacen_Pais: TDataSource
    DataSet = QrEBacen_Pais
    Left = 896
    Top = 80
  end
  object DsPBacen_Pais: TDataSource
    DataSet = QrPBacen_Pais
    Left = 896
    Top = 108
  end
  object QrPBacen_Pais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bacen_pais'
      'ORDER BY Nome'
      '')
    Left = 868
    Top = 108
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrCNAE21Cad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodAlf, Nome'
      'FROM cnae21cad'
      'ORDER BY Nome')
    Left = 868
    Top = 52
    object QrCNAE21CadCodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
    object QrCNAE21CadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCNAE21Cad: TDataSource
    DataSet = QrCNAE21Cad
    Left = 896
    Top = 52
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 812
    Top = 52
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 840
    Top = 52
  end
  object PMDBanc: TPopupMenu
    OnPopup = PMDBancPopup
    Left = 588
    Top = 52
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
  object QrEntiCtas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.*, ban.Nome NOMEBANCO'
      'FROM entictas cta'
      'LEFT JOIN entidades ent ON ent.Codigo=cta.Codigo'
      'LEFT JOIN bancos ban ON ban.Codigo=cta.Banco'
      'WHERE cta.Codigo=:P0'
      'ORDER BY cta.Ordem')
    Left = 756
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCtasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiCtasControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiCtasOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEntiCtasBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrEntiCtasAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrEntiCtasContaCor: TWideStringField
      FieldName = 'ContaCor'
      Size = 10
    end
    object QrEntiCtasContaTip: TWideStringField
      FieldName = 'ContaTip'
      Size = 10
    end
    object QrEntiCtasDAC_A: TWideStringField
      FieldName = 'DAC_A'
      Size = 1
    end
    object QrEntiCtasDAC_C: TWideStringField
      FieldName = 'DAC_C'
      Size = 1
    end
    object QrEntiCtasDAC_AC: TWideStringField
      FieldName = 'DAC_AC'
      Size = 1
    end
    object QrEntiCtasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEntiCtasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEntiCtasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntiCtasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEntiCtasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEntiCtasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEntiCtasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEntiCtasNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
  end
  object DsEntiCtas: TDataSource
    DataSet = QrEntiCtas
    Left = 784
    Top = 52
  end
  object PMQuery: TPopupMenu
    Left = 180
    Top = 36
    object LocalizarEntidades1: TMenuItem
      Caption = 'Localizar &Entidades por...'
      object Nome1: TMenuItem
        Caption = '&Nome'
        OnClick = Nome1Click
      end
      object Dados2: TMenuItem
        Caption = '&Dados'
        OnClick = Dados2Click
      end
      object CNPJCPF1: TMenuItem
        Caption = '&CNPJ/CPF'
        OnClick = CNPJCPF1Click
      end
      object elefone1: TMenuItem
        Caption = '&Telefone'
        OnClick = elefone1Click
      end
    end
    object LocalizarClienteporNotaFiscal1: TMenuItem
      Caption = 'Localizar &Cliente por ...'
      Enabled = False
      object porNotaFiscal1: TMenuItem
        Caption = '&Nota Fiscal'
      end
      object Produtoquecompra1: TMenuItem
        Caption = '&Produto que compra'
        Enabled = False
      end
    end
    object LocalizarFornecedorporNoitaFiscal1: TMenuItem
      Caption = 'Localizar &Fornecedor por ...'
      Enabled = False
      object porNotaFiscal2: TMenuItem
        Caption = '&Nota Fiscal'
      end
      object Produtoquevende1: TMenuItem
        Caption = '&Produto que vende'
      end
    end
    object N1: TMenuItem
      Caption = '-'
      Enabled = False
    end
    object ConferirCdigosdeusurio1: TMenuItem
      Caption = 'Atribuir c'#243'digos de usu'#225'rio'
      Enabled = False
    end
  end
  object QrEntiContat: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntiContatBeforeClose
    AfterScroll = QrEntiContatAfterScroll
    OnCalcFields = QrEntiContatCalcFields
    SQL.Strings = (
      'SELECT eco.Codigo, eco.Controle, eco.Nome, eco.Cargo,'
      'eca.Nome NOME_CARGO, eco.DtaNatal, eco.Sexo, eco.Entidade,'
      'ELT(eco.Sexo+1, "NI", "M", "F", "?") SEXO_TXT,'
      
        'IF(eco.DtaNatal <= "1899-12-30", "", DATE_FORMAT(eco.DtaNatal, "' +
        '%d/%m/%Y")) DTANATAL_TXT'
      'FROM enticontat eco'
      'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo'
      'WHERE ece.Codigo=:P0')
    Left = 616
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiContatCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrEntiContatNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrEntiContatDtaNatal: TDateField
      FieldName = 'DtaNatal'
    end
    object QrEntiContatDTANATAL_TXT: TWideStringField
      FieldName = 'DTANATAL_TXT'
      Size = 10
    end
    object QrEntiContatSexo: TSmallintField
      FieldName = 'Sexo'
    end
    object QrEntiContatSEXO_TXT: TWideStringField
      FieldName = 'SEXO_TXT'
      Size = 2
    end
    object QrEntiContatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiContatTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrEntiContatCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntiContatCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntiContatAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrEntiContatCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntiContatAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrEntiContatEntidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 644
    Top = 24
  end
  object QrEntiSrvPro: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etp.Nome NO_ORGAO, esp.* '
      'FROM entisrvpro esp'
      'LEFT JOIN entitippro etp ON etp.Codigo=esp.Orgao'
      'WHERE esp.Codigo=:P0'
      'ORDER BY Controle DESC')
    Left = 668
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiSrvProNO_ORGAO: TWideStringField
      FieldName = 'NO_ORGAO'
      Size = 60
    end
    object QrEntiSrvProCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiSrvProControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEntiSrvProOrgao: TIntegerField
      FieldName = 'Orgao'
    end
    object QrEntiSrvProData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEntiSrvProHora: TTimeField
      FieldName = 'Hora'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrEntiSrvProNumero: TWideStringField
      FieldName = 'Numero'
      Size = 60
    end
    object QrEntiSrvProResultado: TWideStringField
      FieldName = 'Resultado'
      Size = 255
    end
  end
  object DsEntiSrvPro: TDataSource
    DataSet = QrEntiSrvPro
    Left = 696
    Top = 52
  end
  object PMEntiSrvPro: TPopupMenu
    OnPopup = PMEntiSrvProPopup
    Left = 616
    Top = 52
    object Inclui4: TMenuItem
      Caption = '&Inclui nova consulta'
      OnClick = Inclui4Click
    end
    object Altera4: TMenuItem
      Caption = '&Altera consulta atual'
      OnClick = Altera4Click
    end
    object Exclui4: TMenuItem
      Caption = '&Exclui consulta atual'
      OnClick = Exclui4Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 24
    Top = 12
    object Pesquisas1: TMenuItem
      Caption = '&Pesquisar antes de imprimir'
      OnClick = Pesquisas1Click
    end
    object Dados1: TMenuItem
      Caption = '&Dados das entidades selecionadas'
      OnClick = Dados1Click
    end
    object Dadoscompactosdaentidadeatual1: TMenuItem
      Caption = 'Dados compactos da entidade &atual'
      OnClick = Dadoscompactosdaentidadeatual1Click
    end
    object Dadosdestaentidade2012111: TMenuItem
      Caption = 'Dados desta entidade'
      OnClick = Dadosdestaentidade2012111Click
    end
  end
  object frxDsCadastro: TfrxDBDataset
    UserName = 'frxDsCadastro'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'CodUsu=CodUsu'
      'RazaoSocial=RazaoSocial'
      'Fantasia=Fantasia'
      'Respons1=Respons1'
      'Respons2=Respons2'
      'Pai=Pai'
      'Mae=Mae'
      'CNPJ=CNPJ'
      'IE=IE'
      'Nome=Nome'
      'Apelido=Apelido'
      'CPF=CPF'
      'RG=RG'
      'ERua=ERua'
      'ECompl=ECompl'
      'EBairro=EBairro'
      'ECidade=ECidade'
      'EUF=EUF'
      'EPais=EPais'
      'ETe1=ETe1'
      'ETe2=ETe2'
      'ETe3=ETe3'
      'ECel=ECel'
      'EFax=EFax'
      'EEMail=EEMail'
      'EContato=EContato'
      'PRua=PRua'
      'PCompl=PCompl'
      'PBairro=PBairro'
      'PCidade=PCidade'
      'PUF=PUF'
      'PPais=PPais'
      'PTe1=PTe1'
      'PTe2=PTe2'
      'PTe3=PTe3'
      'PCel=PCel'
      'PFax=PFax'
      'PEMail=PEMail'
      'PContato=PContato'
      'Sexo=Sexo'
      'Responsavel=Responsavel'
      'Profissao=Profissao'
      'Cargo=Cargo'
      'Recibo=Recibo'
      'DiaRecibo=DiaRecibo'
      'AjudaEmpV=AjudaEmpV'
      'AjudaEmpP=AjudaEmpP'
      'Cliente1=Cliente1'
      'Cliente2=Cliente2'
      'Cliente3=Cliente3'
      'Cliente4=Cliente4'
      'Fornece1=Fornece1'
      'Fornece2=Fornece2'
      'Fornece3=Fornece3'
      'Fornece4=Fornece4'
      'Fornece5=Fornece5'
      'Fornece6=Fornece6'
      'Fornece7=Fornece7'
      'Fornece8=Fornece8'
      'Terceiro=Terceiro'
      'Cadastro=Cadastro'
      'Informacoes=Informacoes'
      'Logo=Logo'
      'Veiculo=Veiculo'
      'Mensal=Mensal'
      'Observacoes=Observacoes'
      'Tipo=Tipo'
      'Lk=Lk'
      'NOMEENTIDADE=NOMEENTIDADE'
      'NOMEEUF=NOMEEUF'
      'NOMEPUF=NOMEPUF'
      'PCPF_TXT=PCPF_TXT'
      'PTE1_TXT=PTE1_TXT'
      'PTE2_TXT=PTE2_TXT'
      'PTE3_TXT=PTE3_TXT'
      'PCEL_TXT=PCEL_TXT'
      'PFAX_TXT=PFAX_TXT'
      'ETE1_TXT=ETE1_TXT'
      'ETE2_TXT=ETE2_TXT'
      'ETE3_TXT=ETE3_TXT'
      'ECEL_TXT=ECEL_TXT'
      'EFAX_TXT=EFAX_TXT'
      'CNPJ_TXT=CNPJ_TXT'
      'ECEP=ECEP'
      'PCEP=PCEP'
      'Grupo=Grupo'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'CRua=CRua'
      'CCompl=CCompl'
      'CBairro=CBairro'
      'CCidade=CCidade'
      'CUF=CUF'
      'CCEP=CCEP'
      'CPais=CPais'
      'CTel=CTel'
      'CFax=CFax'
      'CCel=CCel'
      'CContato=CContato'
      'LRua=LRua'
      'LCompl=LCompl'
      'LBairro=LBairro'
      'LCidade=LCidade'
      'LUF=LUF'
      'LCEP=LCEP'
      'LPais=LPais'
      'LTel=LTel'
      'LFax=LFax'
      'LCel=LCel'
      'LContato=LContato'
      'Comissao=Comissao'
      'DataCad=DataCad'
      'ECEP_TXT=ECEP_TXT'
      'PCEP_TXT=PCEP_TXT'
      'CCEP_TXT=CCEP_TXT'
      'LCEP_TXT=LCEP_TXT'
      'NOMECAD2=NOMECAD2'
      'NOMEALT2=NOMEALT2'
      'Situacao=Situacao'
      'Nivel=Nivel'
      'CTEL_TXT=CTEL_TXT'
      'CFAX_TXT=CFAX_TXT'
      'CCEL_TXT=CCEL_TXT'
      'LTEL_TXT=LTEL_TXT'
      'LFAX_TXT=LFAX_TXT'
      'LCEL_TXT=LCEL_TXT'
      'NOMEENTIGRUPO=NOMEENTIGRUPO'
      'NOMECUF=NOMECUF'
      'NOMELUF=NOMELUF'
      'Account=Account'
      'NOMEACCOUNT=NOMEACCOUNT'
      'Logo2=Logo2'
      'ELograd=ELograd'
      'PLograd=PLograd'
      'ConjugeNome=ConjugeNome'
      'ConjugeNatal=ConjugeNatal'
      'Nome1=Nome1'
      'Natal1=Natal1'
      'Nome2=Nome2'
      'Natal2=Natal2'
      'Nome3=Nome3'
      'Natal3=Natal3'
      'CreditosI=CreditosI'
      'CreditosL=CreditosL'
      'CreditosD=CreditosD'
      'CreditosU=CreditosU'
      'CreditosV=CreditosV'
      'Motivo=Motivo'
      'QuantI1=QuantI1'
      'QuantI2=QuantI2'
      'QuantI3=QuantI3'
      'QuantI4=QuantI4'
      'Agenda=Agenda'
      'SenhaQuer=SenhaQuer'
      'Senha1=Senha1'
      'Natal4=Natal4'
      'Nome4=Nome4'
      'NOMEMOTIVO=NOMEMOTIVO'
      'CreditosF2=CreditosF2'
      'NOMEELOGRAD=NOMEELOGRAD'
      'NOMEPLOGRAD=NOMEPLOGRAD'
      'CLograd=CLograd'
      'LLograd=LLograd'
      'NOMECLOGRAD=NOMECLOGRAD'
      'NOMELLOGRAD=NOMELLOGRAD'
      'QuantN1=QuantN1'
      'QuantN2=QuantN2'
      'LimiCred=LimiCred'
      'Desco=Desco'
      'CasasApliDesco=CasasApliDesco'
      'CPF_Pai=CPF_Pai'
      'SSP=SSP'
      'CidadeNatal=CidadeNatal'
      'CPF_PAI_TXT=CPF_PAI_TXT'
      'UFNatal=UFNatal'
      'NOMENUF=NOMENUF'
      'FatorCompra=FatorCompra'
      'AdValorem=AdValorem'
      'DMaisC=DMaisC'
      'DMaisD=DMaisD'
      'DataRG=DataRG'
      'Nacionalid=Nacionalid'
      'Empresa=Empresa'
      'NOMEEMPRESA=NOMEEMPRESA'
      'FormaSociet=FormaSociet'
      'Simples=Simples'
      'Atividade=Atividade'
      'EstCivil=EstCivil'
      'NOMEECIVIL=NOMEECIVIL'
      'CPF_Conjuge=CPF_Conjuge'
      'CBE=CBE'
      'SCB=SCB'
      'CPF_Resp1=CPF_Resp1'
      'CPF_Resp1_TXT=CPF_Resp1_TXT'
      'ENumero=ENumero'
      'PNumero=PNumero'
      'CNumero=CNumero'
      'LNumero=LNumero'
      'Banco=Banco'
      'Agencia=Agencia'
      'ContaCorrente=ContaCorrente'
      'CartPref=CartPref'
      'NOMECARTPREF=NOMECARTPREF'
      'RolComis=RolComis'
      'Filial=Filial'
      'IEST=IEST'
      'QuantN3=QuantN3'
      'TempA=TempA'
      'TempD=TempD'
      'PAtividad=PAtividad'
      'EAtividad=EAtividad'
      'PCidadeCod=PCidadeCod'
      'ECidadeCod=ECidadeCod'
      'PPaisCod=PPaisCod'
      'EPaisCod=EPaisCod'
      'Antigo=Antigo'
      'CUF2=CUF2'
      'Contab=Contab'
      'MSN1=MSN1'
      'PastaTxtFTP=PastaTxtFTP'
      'PastaPwdFTP=PastaPwdFTP'
      'Protestar=Protestar'
      'MultaCodi=MultaCodi'
      'MultaDias=MultaDias'
      'MultaValr=MultaValr'
      'MultaPerc=MultaPerc'
      'MultaTiVe=MultaTiVe'
      'JuroSacado=JuroSacado'
      'CPMF=CPMF'
      'Corrido=Corrido'
      'CliInt=CliInt'
      'AltDtPlaCt=AltDtPlaCt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'EEndeRef=EEndeRef'
      'PEndeRef=PEndeRef'
      'CEndeRef=CEndeRef'
      'LEndeRef=LEndeRef'
      'PNUMERO_TXT=PNUMERO_TXT'
      'ENUMERO_TXT=ENUMERO_TXT'
      'CNUMERO_TXT=CNUMERO_TXT'
      'LNUMERO_TXT=LNUMERO_TXT'
      'IE_TXT=IE_TXT'
      'DATARG_TXT=DATARG_TXT'
      'ECodMunici=ECodMunici'
      'PCodMunici=PCodMunici'
      'CCodMunici=CCodMunici'
      'LCodMunici=LCodMunici'
      'CNAE=CNAE'
      'SUFRAMA=SUFRAMA'
      'ECodiPais=ECodiPais'
      'PCodiPais=PCodiPais'
      'L_CNPJ=L_CNPJ'
      'L_Ativo=L_Ativo'
      'L_CNPJ_TXT=L_CNPJ_TXT'
      'NO_DTB_EMUNICI=NO_DTB_EMUNICI'
      'NO_DTB_PMUNICI=NO_DTB_PMUNICI'
      'NO_DTB_CMUNICI=NO_DTB_CMUNICI'
      'NO_DTB_LMUNICI=NO_DTB_LMUNICI'
      'NO_BACEN_EPAIS=NO_BACEN_EPAIS'
      'NO_BACEN_PPAIS=NO_BACEN_PPAIS'
      'CNAE_Nome=CNAE_Nome'
      'NIRE=NIRE'
      'IEST_TXT=IEST_TXT'
      'URL=URL'
      'CRT=CRT'
      'COD_PART=COD_PART'
      'ETe1Tip=ETe1Tip'
      'ECelTip=ECelTip'
      'PTe1Tip=PTe1Tip'
      'PCelTip=PCelTip'
      'PTe1Tip_TXT=PTe1Tip_TXT'
      'PCelTip_TXT=PCelTip_TXT'
      'ETe1Tip_TXT=ETe1Tip_TXT'
      'ECelTip_TXT=ECelTip_TXT'
      'ENATAL_TXT=ENATAL_TXT'
      'PNATAL_TXT=PNATAL_TXT'
      'ENatal=ENatal'
      'PNatal=PNatal'
      'ESite=ESite'
      'PSite=PSite')
    DataSet = QrEntidades
    BCDToCurrency = False
    DataSetOptions = []
    Left = 888
    Top = 136
  end
  object frxCadastro: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41209.957653067140000000
    ReportOptions.LastChange = 41209.957653067140000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCadastroGetValue
    Left = 916
    Top = 136
    Datasets = <
      item
        DataSet = frxDsCadastro
        DataSetName = 'frxDsCadastro'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 818.000000000000000000
          Top = 1.102350000000001000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 846.614720000000000000
        Width = 1009.134510000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 582.000000000000000000
        Top = 204.094620000000000000
        Width = 1009.134510000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCadastro
        DataSetName = 'frxDsCadastro'
        RowCount = 0
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 758.000000000000000000
          Top = 555.905380000000100000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 71.322820000000000000
          Top = 111.936920000000000000
          Width = 200.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PRua"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 71.322820000000000000
          Top = 91.936920000000010000
          Width = 200.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Logradouro')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 271.322820000000000000
          Top = 111.936920000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0;-0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PNumero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 311.322820000000000000
          Top = 111.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PCompl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 491.322820000000000000
          Top = 111.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PBairro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 671.322820000000000000
          Top = 111.936920000000000000
          Width = 140.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PCidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 271.322820000000000000
          Top = 91.936920000000010000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'N'#250'mero')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 311.322820000000000000
          Top = 91.936920000000010000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Complemento')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 491.322820000000000000
          Top = 91.936920000000010000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Bairro')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 671.322820000000000000
          Top = 91.936920000000010000
          Width = 140.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Cidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 835.322820000000000000
          Top = 111.936920000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PCEP_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 835.322820000000000000
          Top = 91.936920000000010000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 811.322820000000000000
          Top = 111.936920000000000000
          Width = 24.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCadastro."NOMEPUF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 811.322820000000000000
          Top = 91.936920000000010000
          Width = 24.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'UF')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 887.322820000000000000
          Top = 111.936920000000000000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PPais"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 71.322820000000000000
          Top = 129.936920000000000000
          Width = 200.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ERua"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 311.322820000000000000
          Top = 129.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ECompl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 491.322820000000000000
          Top = 129.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."EBairro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 671.322820000000000000
          Top = 129.936920000000000000
          Width = 140.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ECidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 835.322820000000000000
          Top = 129.936920000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ECEP_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 811.322820000000000000
          Top = 129.936920000000000000
          Width = 24.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCadastro."NOMEEUF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 887.322820000000000000
          Top = 129.936920000000000000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          DataField = 'EPais'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."EPais"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 71.322820000000000000
          Top = 147.936920000000000000
          Width = 200.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CRua"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 311.322820000000000000
          Top = 147.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CCompl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 491.322820000000000000
          Top = 147.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CBairro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 671.322820000000000000
          Top = 147.936920000000000000
          Width = 140.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CCidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 835.322820000000000000
          Top = 147.936920000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CCEP_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 811.322820000000000000
          Top = 147.936920000000000000
          Width = 24.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCadastro."NOMECUF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 887.322820000000000000
          Top = 147.936920000000000000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          DataField = 'CPais'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CPais"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 71.322820000000000000
          Top = 165.936920000000000000
          Width = 200.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LRua"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 311.322820000000000000
          Top = 165.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LCompl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 491.322820000000000000
          Top = 165.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LBairro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 671.322820000000000000
          Top = 165.936920000000000000
          Width = 140.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LCidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 835.322820000000000000
          Top = 165.936920000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LCEP_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 811.322820000000000000
          Top = 165.936920000000000000
          Width = 24.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCadastro."NOMELUF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 887.322820000000000000
          Top = 165.936920000000000000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          DataField = 'LPais'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LPais"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 887.322820000000000000
          Top = 91.936920000000010000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'PA'#205'S')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 271.322820000000000000
          Top = 129.936920000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0;-0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ENumero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 271.322820000000000000
          Top = 147.936920000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0;-0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CNumero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 271.322820000000000000
          Top = 165.936920000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0;-0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LNumero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 9.322820000000000000
          Top = 111.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Pessoal')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 9.322820000000000000
          Top = 129.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 9.322820000000000000
          Top = 165.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Entrega')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 9.322820000000000000
          Top = 147.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Cobran'#231'a')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 71.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CTEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 71.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 167.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 167.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Fax')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 263.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CCEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 263.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 407.322820000000000000
          Top = 207.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Entrega')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 7.322820000000000000
          Top = 207.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Cobran'#231'a')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 471.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LTEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 567.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 663.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LCEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 7.322820000000000000
          Top = 231.936920000000000000
          Width = 1000.000000000000000000
          Height = 342.000000000000000000
          DataField = 'Observacoes'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."Observacoes"]')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 471.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone')
          ParentFont = False
          WordWrap = False
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 567.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Fax')
          ParentFont = False
          WordWrap = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 663.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
          WordWrap = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 911.322820000000000000
          Top = 211.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 7.322820000000000000
          Top = 3.936919999999987000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Pessoa F'#237'sica')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 335.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'CPF')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 431.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'RG')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 527.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 1')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 623.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 2')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 719.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 3')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 815.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Fax')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 911.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 7.322820000000000000
          Top = 23.936919999999990000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          DataField = 'Nome'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 335.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'PCPF_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PCPF_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 431.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'RG'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."RG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 7.322820000000000000
          Top = 67.936920000000010000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          DataField = 'RazaoSocial'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."RazaoSocial"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 7.322820000000000000
          Top = 47.936919999999990000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 527.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'PTE1_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PTE1_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 623.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'PTE2_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PTE2_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 719.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'PTE3_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PTE3_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 815.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 911.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'PCEL_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PCEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 335.322820000000000000
          Top = 67.936920000000010000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'CNPJ_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CNPJ_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 335.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 431.322820000000000000
          Top = 67.936920000000010000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'IE'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."IE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 431.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'I.E.')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 527.322820000000000000
          Top = 67.936920000000010000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'ETE1_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ETE1_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 527.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 1')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 623.322820000000000000
          Top = 67.936920000000010000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'ETE2_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ETE2_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 623.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 2')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 719.322820000000000000
          Top = 67.936920000000010000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'ETE3_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ETE3_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 719.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 3')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 815.322820000000000000
          Top = 67.936920000000010000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'EFAX_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."EFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 815.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Fax')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 911.322820000000000000
          Top = 67.936920000000010000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'ECEL_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ECEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 911.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 74.000000000000000000
        Top = 68.031540000000010000
        Width = 1009.134510000000000000
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 183.102350000000000000
          Top = 27.086580000000000000
          Width = 828.000000000000000000
          Height = 46.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[ENTIDADE]')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          AllowVectorExport = True
          Left = 7.322820000000000000
          Top = 3.086579999999998000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 183.102350000000000000
          Top = 3.086579999999998000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
    end
  end
  object QrCadastro2: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCadastro2CalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, Observacoes,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome    ) NOMEENTIDADE,'
      'IF(en.Tipo=0, en.Fantasia   , en.Apelido ) FANTASIA_APELIDO,'
      'IF(en.Tipo=0, en.ERua       , en.PRua    ) RUA,'
      'IF(en.Tipo=0, en.ENumero    , en.PNumero ) + 0.000 NUMERO,'
      'IF(en.Tipo=0, en.ECompl     , en.PCompl  ) COMPL,'
      'IF(en.Tipo=0, en.ECEP       , en.PCEP    ) + 0.000 CEP,'
      'IF(en.Tipo=0, en.EBairro    , en.PBairro ) BAIRRO,'
      'IF(en.Tipo=0, en.ECidade    , en.PCidade ) CIDADE,'
      'IF(en.Tipo=0, en.EUF        , en.PUF     ) + 0.000 UF,'
      'IF(en.Tipo=0, en.EPais      , en.PPais   ) PAIS,'
      'IF(en.Tipo=0, en.ETe1       , en.PTe1    ) TE1,'
      'IF(en.Tipo=0, en.ETe2       , en.PTe2    ) TE2,'
      'IF(en.Tipo=0, en.ETe3       , en.PTe3    ) TE3,'
      'IF(en.Tipo=0, en.ECel       , en.PCel    ) CEL,'
      'IF(en.Tipo=0, en.EFax       , en.PFax    ) FAX,'
      'IF(en.Tipo=0, en.EEmail     , en.PEmail  ) EMAIL,'
      'IF(en.Tipo=0, en.EContato   , en.PContato) CONTATO,'
      'IF(en.Tipo=0, en.CNPJ       , en.CPF     ) DOCA,'
      'IF(en.Tipo=0, en.IE         , en.RG      ) DOCB,'
      'IF(en.Tipo=0, " "           , en.Sexo    ) Sexo,'
      
        'uf.Nome NOMEUF,  Cargo, Profissao, ufc.Nome NOMECUF, ufl.Nome NO' +
        'MELUF,'
      'CRua, CCompl, CNumero, CCEP, CBairro, CCidade, CUF, CPais,'
      'LRua, LCompl, LNumero, LCEP, LBairro, LCidade, LUF, LPais,'
      'ENatal, PNatal'
      'FROM entidades en, ufs uf, ufs ufc, ufs ufl'
      'WHERE uf.Codigo=IF(en.Tipo=0, en.EUF, en.PUF)'
      'AND ufc.Codigo=en.CUF AND ufl.Codigo=en.LUF'
      'AND en.Codigo=:P0'
      '')
    Left = 860
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCadastro2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrCadastro2Tipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrCadastro2NOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrCadastro2FANTASIA_APELIDO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FANTASIA_APELIDO'
      Size = 60
    end
    object QrCadastro2RUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 60
    end
    object QrCadastro2COMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 60
    end
    object QrCadastro2BAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrCadastro2CIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CIDADE'
      Size = 60
    end
    object QrCadastro2PAIS: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'PAIS'
    end
    object QrCadastro2TE1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TE1'
    end
    object QrCadastro2TE2: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TE2'
    end
    object QrCadastro2TE3: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TE3'
    end
    object QrCadastro2CEL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CEL'
    end
    object QrCadastro2FAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrCadastro2EMAIL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrCadastro2CONTATO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CONTATO'
      Size = 60
    end
    object QrCadastro2DOCA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'DOCA'
      Size = 18
    end
    object QrCadastro2DOCB: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'DOCB'
    end
    object QrCadastro2Sexo: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Sexo'
      Size = 1
    end
    object QrCadastro2NOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrCadastro2CRua: TWideStringField
      FieldName = 'CRua'
      Origin = 'entidades.CRua'
      Size = 60
    end
    object QrCadastro2CCompl: TWideStringField
      FieldName = 'CCompl'
      Origin = 'entidades.CCompl'
      Size = 60
    end
    object QrCadastro2CCEP: TIntegerField
      FieldName = 'CCEP'
      Origin = 'entidades.CCEP'
    end
    object QrCadastro2CBairro: TWideStringField
      FieldName = 'CBairro'
      Origin = 'entidades.CBairro'
      Size = 60
    end
    object QrCadastro2CCidade: TWideStringField
      FieldName = 'CCidade'
      Origin = 'entidades.CCidade'
      Size = 60
    end
    object QrCadastro2CUF: TSmallintField
      FieldName = 'CUF'
      Origin = 'entidades.CUF'
      Required = True
    end
    object QrCadastro2CPais: TWideStringField
      FieldName = 'CPais'
      Origin = 'entidades.CPais'
    end
    object QrCadastro2LRua: TWideStringField
      FieldName = 'LRua'
      Origin = 'entidades.LRua'
      Size = 60
    end
    object QrCadastro2LCompl: TWideStringField
      FieldName = 'LCompl'
      Origin = 'entidades.LCompl'
      Size = 60
    end
    object QrCadastro2LCEP: TIntegerField
      FieldName = 'LCEP'
      Origin = 'entidades.LCEP'
    end
    object QrCadastro2LBairro: TWideStringField
      FieldName = 'LBairro'
      Origin = 'entidades.LBairro'
      Size = 60
    end
    object QrCadastro2LCidade: TWideStringField
      FieldName = 'LCidade'
      Origin = 'entidades.LCidade'
      Size = 60
    end
    object QrCadastro2LUF: TSmallintField
      FieldName = 'LUF'
      Origin = 'entidades.LUF'
      Required = True
    end
    object QrCadastro2LPais: TWideStringField
      FieldName = 'LPais'
      Origin = 'entidades.LPais'
    end
    object QrCadastro2TE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2TE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE2_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2TE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE3_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2FAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2CEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2DOCA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCA_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2Cargo: TWideStringField
      FieldName = 'Cargo'
      Origin = 'entidades.Cargo'
      Size = 60
    end
    object QrCadastro2Profissao: TWideStringField
      FieldName = 'Profissao'
      Origin = 'entidades.Profissao'
      Size = 60
    end
    object QrCadastro2NUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUM_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 30
      Calculated = True
    end
    object QrCadastro2CNUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNUM_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2LNUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LNUM_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2NOMECUF: TWideStringField
      FieldName = 'NOMECUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrCadastro2NOMELUF: TWideStringField
      FieldName = 'NOMELUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrCadastro2CCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEP_TXT'
      Size = 30
      Calculated = True
    end
    object QrCadastro2LCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEP_TXT'
      Size = 30
      Calculated = True
    end
    object QrCadastro2NATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2ENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrCadastro2PNatal: TDateField
      FieldName = 'PNatal'
      Origin = 'entidades.PNatal'
    end
    object QrCadastro2NUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrCadastro2CNumero: TIntegerField
      FieldName = 'CNumero'
      Origin = 'entidades.CNumero'
    end
    object QrCadastro2LNumero: TIntegerField
      FieldName = 'LNumero'
      Origin = 'entidades.LNumero'
    end
    object QrCadastro2CEP: TFloatField
      FieldName = 'CEP'
    end
    object QrCadastro2UF: TFloatField
      FieldName = 'UF'
      Required = True
    end
    object QrCadastro2Observacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
    end
  end
  object frxDsCadastro2: TfrxDBDataset
    UserName = 'frxDsCadastro2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Tipo=Tipo'
      'NOMEENTIDADE=NOMEENTIDADE'
      'FANTASIA_APELIDO=FANTASIA_APELIDO'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'PAIS=PAIS'
      'TE1=TE1'
      'TE2=TE2'
      'TE3=TE3'
      'CEL=CEL'
      'FAX=FAX'
      'EMAIL=EMAIL'
      'CONTATO=CONTATO'
      'DOCA=DOCA'
      'DOCB=DOCB'
      'Sexo=Sexo'
      'NOMEUF=NOMEUF'
      'CRua=CRua'
      'CCompl=CCompl'
      'CCEP=CCEP'
      'CBairro=CBairro'
      'CCidade=CCidade'
      'CUF=CUF'
      'CPais=CPais'
      'LRua=LRua'
      'LCompl=LCompl'
      'LCEP=LCEP'
      'LBairro=LBairro'
      'LCidade=LCidade'
      'LUF=LUF'
      'LPais=LPais'
      'TE1_TXT=TE1_TXT'
      'TE2_TXT=TE2_TXT'
      'TE3_TXT=TE3_TXT'
      'FAX_TXT=FAX_TXT'
      'CEL_TXT=CEL_TXT'
      'DOCA_TXT=DOCA_TXT'
      'Cargo=Cargo'
      'Profissao=Profissao'
      'NUM_TXT=NUM_TXT'
      'CEP_TXT=CEP_TXT'
      'CNUM_TXT=CNUM_TXT'
      'LNUM_TXT=LNUM_TXT'
      'NOMECUF=NOMECUF'
      'NOMELUF=NOMELUF'
      'CCEP_TXT=CCEP_TXT'
      'LCEP_TXT=LCEP_TXT'
      'NATAL_TXT=NATAL_TXT'
      'ENatal=ENatal'
      'PNatal=PNatal'
      'NUMERO=NUMERO'
      'CNumero=CNumero'
      'LNumero=LNumero'
      'CEP=CEP'
      'UF=UF'
      'Observacoes=Observacoes')
    DataSet = QrCadastro2
    BCDToCurrency = False
    DataSetOptions = []
    Left = 888
    Top = 164
  end
  object frxCadastro2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41209.959630763890000000
    ReportOptions.LastChange = 41209.959630763890000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCadastroGetValue
    Left = 916
    Top = 164
    Datasets = <
      item
        DataSet = frxDsCadastro2
        DataSetName = 'frxDsCadastro2'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 528.000000000000000000
          Top = 0.102350000000001300
          Width = 163.000000000000000000
          Height = 16.000000000000000000
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 12.000000000000000000
        Top = 604.724800000000000000
        Width = 793.701300000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 438.000000000000000000
        Top = 105.826840000000000000
        Width = 793.701300000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCadastro2
        DataSetName = 'frxDsCadastro2'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 8.000000000000000000
          Top = 46.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fone 1:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 44.000000000000000000
          Top = 46.968459999999990000
          Width = 99.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."TE1_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 8.000000000000000000
          Top = 66.968459999999990000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[_CNPJ_CPF]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 44.000000000000000000
          Top = 66.968459999999990000
          Width = 92.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."DOCA_TXT"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 144.000000000000000000
          Top = 46.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fone 2:')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 180.000000000000000000
          Top = 46.968459999999990000
          Width = 99.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."TE2_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 280.000000000000000000
          Top = 46.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fone3:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 316.000000000000000000
          Top = 46.968459999999990000
          Width = 99.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."TE3_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 416.000000000000000000
          Top = 46.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 451.000000000000000000
          Top = 46.968459999999990000
          Width = 100.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."FAX_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 551.000000000000000000
          Top = 46.968459999999990000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Celular:')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 588.000000000000000000
          Top = 46.968459999999990000
          Width = 128.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CEL_TXT"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 136.000000000000000000
          Top = 66.968459999999990000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[_IE_RG]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 172.000000000000000000
          Top = 66.968459999999990000
          Width = 92.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."DOCB"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 7.000000000000000000
          Top = 86.968459999999990000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Contato:')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 47.000000000000000000
          Top = 86.968459999999990000
          Width = 148.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CONTATO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 264.000000000000000000
          Top = 66.968459999999990000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 303.000000000000000000
          Top = 66.968459999999990000
          Width = 412.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."EMAIL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 195.000000000000000000
          Top = 86.968459999999990000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cargo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 235.000000000000000000
          Top = 86.968459999999990000
          Width = 148.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."Cargo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 383.000000000000000000
          Top = 86.968459999999990000
          Width = 48.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Profiss'#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 431.000000000000000000
          Top = 86.968459999999990000
          Width = 148.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."Profissao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 579.000000000000000000
          Top = 86.968459999999990000
          Width = 68.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[_NATAL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 647.000000000000000000
          Top = 86.968459999999990000
          Width = 68.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NATAL_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 7.000000000000000000
          Top = 106.968460000000000000
          Width = 96.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 8.000000000000000000
          Top = 126.968460000000000000
          Width = 24.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Rua:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 31.000000000000000000
          Top = 126.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."RUA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 207.000000000000000000
          Top = 126.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#250'mero:')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 126.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DataField = 'NUMERO'
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NUMERO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 303.000000000000000000
          Top = 126.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Compl:')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 339.000000000000000000
          Top = 126.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."COMPL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 515.000000000000000000
          Top = 126.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 551.000000000000000000
          Top = 126.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."BAIRRO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 7.000000000000000000
          Top = 146.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 43.000000000000000000
          Top = 146.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CIDADE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 207.000000000000000000
          Top = 146.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Estado:')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 146.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DataField = 'NOMEUF'
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NOMEUF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 303.000000000000000000
          Top = 146.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 339.000000000000000000
          Top = 146.968460000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CEP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 411.000000000000000000
          Top = 146.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pa'#237's:')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 447.000000000000000000
          Top = 146.968460000000000000
          Width = 268.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."PAIS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 7.000000000000000000
          Top = 166.968460000000000000
          Width = 96.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cobran'#231'a:')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 7.000000000000000000
          Top = 186.968460000000000000
          Width = 24.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Rua:')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 31.000000000000000000
          Top = 186.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CRua"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 207.000000000000000000
          Top = 186.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#250'mero:')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 186.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CNUM_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 303.000000000000000000
          Top = 186.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Compl:')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 339.000000000000000000
          Top = 186.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CCompl"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 515.000000000000000000
          Top = 186.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 551.000000000000000000
          Top = 186.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CBairro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 7.000000000000000000
          Top = 206.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 43.000000000000000000
          Top = 206.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CCidade"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 207.000000000000000000
          Top = 206.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Estado:')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 206.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NOMECUF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 303.000000000000000000
          Top = 206.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 339.000000000000000000
          Top = 206.968460000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CCEP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 411.000000000000000000
          Top = 206.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pa'#237's:')
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 447.000000000000000000
          Top = 206.968460000000000000
          Width = 268.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CPais"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 7.000000000000000000
          Top = 226.968460000000000000
          Width = 96.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Entrega:')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 7.000000000000000000
          Top = 246.968460000000000000
          Width = 24.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Rua:')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 31.000000000000000000
          Top = 246.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LRua"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 207.000000000000000000
          Top = 246.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#250'mero:')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 246.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LNUM_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 303.000000000000000000
          Top = 246.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Compl:')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 339.000000000000000000
          Top = 246.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LCompl"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 516.000000000000000000
          Top = 246.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 552.000000000000000000
          Top = 246.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LBairro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 7.000000000000000000
          Top = 266.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 43.000000000000000000
          Top = 266.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LCidade"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 207.000000000000000000
          Top = 266.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Estado:')
          ParentFont = False
          WordWrap = False
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 266.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NOMELUF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 303.000000000000000000
          Top = 266.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 339.000000000000000000
          Top = 266.968460000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LCEP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 411.000000000000000000
          Top = 266.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Pa'#237's:')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 447.000000000000000000
          Top = 266.968460000000000000
          Width = 268.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LPais"]')
          ParentFont = False
          WordWrap = False
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 8.000000000000000000
          Top = 286.968460000000000000
          Width = 708.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 8.000000000000000000
          Top = 286.968460000000000000
          Width = 708.000000000000000000
          Height = 148.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[VARF_OBSERVACOES]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 8.000000000000000000
          Top = 2.968459999999993000
          Width = 551.000000000000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 8.000000000000000000
          Top = 26.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'C'#243'digo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 44.000000000000000000
          Top = 26.968459999999990000
          Width = 67.000000000000000000
          Height = 20.000000000000000000
          DataField = 'Codigo'
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 112.000000000000000000
          Top = 26.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 148.000000000000000000
          Top = 26.968459999999990000
          Width = 247.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NOMEENTIDADE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 396.000000000000000000
          Top = 26.968459999999990000
          Width = 71.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[_FANTASIA]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 468.000000000000000000
          Top = 26.968459999999990000
          Width = 247.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsCadastro2."FANTASIA_APELIDO"]')
          ParentFont = False
          WordWrap = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 8.000000000000000000
          Top = 2.968459999999993000
          Width = 708.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 8.000000000000000000
          Top = 2.968459999999993000
          Height = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = 716.000000000000000000
          Top = 2.968459999999993000
          Height = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
    end
  end
  object QrECelTip: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 812
    Top = 80
    object QrECelTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrECelTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrECelTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsECelTip: TDataSource
    DataSet = QrECelTip
    Left = 840
    Top = 80
  end
  object QrPCelTip: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 796
    Top = 280
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object IntegerField3: TIntegerField
      FieldName = 'CodUsu'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPCelTip: TDataSource
    DataSet = QrPCelTip
    Left = 824
    Top = 280
  end
  object QrETe1Tip: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 756
    Top = 80
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
    object IntegerField5: TIntegerField
      FieldName = 'CodUsu'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsETe1Tip: TDataSource
    DataSet = QrETe1Tip
    Left = 784
    Top = 80
  end
  object QrPTe1Tip: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 740
    Top = 280
    object IntegerField6: TIntegerField
      FieldName = 'Codigo'
    end
    object IntegerField7: TIntegerField
      FieldName = 'CodUsu'
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPTe1Tip: TDataSource
    DataSet = QrPTe1Tip
    Left = 768
    Top = 280
  end
  object VUETe1Tip: TdmkValUsu
    dmkEditCB = EdETe1Tip
    Panel = Panel9
    QryCampo = 'ETe1Tip'
    UpdCampo = 'ETe1Tip'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 200
    Top = 548
  end
  object VUPTe1Tip: TdmkValUsu
    dmkEditCB = EdPTe1Tip
    Panel = Panel8
    QryCampo = 'PTe1Tip'
    UpdCampo = 'PTe1Tip'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 228
    Top = 548
  end
  object VUPCelTip: TdmkValUsu
    dmkEditCB = EdPCelTip
    Panel = Panel8
    QryCampo = 'PCelTip'
    UpdCampo = 'PCelTip'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 284
    Top = 548
  end
  object VUECelTip: TdmkValUsu
    dmkEditCB = EdECelTip
    Panel = Panel9
    QryCampo = 'ECelTip'
    UpdCampo = 'ECelTip'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 256
    Top = 548
  end
  object PMImporta: TPopupMenu
    Left = 820
    Top = 160
    object ReceitaFederalNavegador1: TMenuItem
      Caption = 'Receita Federal (Navegador)'
      OnClick = ReceitaFederalNavegador1Click
    end
    object ReceitaEstadual1: TMenuItem
      Caption = 'Receita &Estadual'
      OnClick = ReceitaEstadual1Click
    end
    object ReceitaFederal1: TMenuItem
      Caption = '&Receita Federal (WS)'
      OnClick = ReceitaFederal1Click
    end
    object XMLNFe1: TMenuItem
      Caption = '&XML NF-e'
      OnClick = XMLNFe1Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object CNPj1: TMenuItem
      Caption = 'CNPJ'#225
      OnClick = CNPj1Click
    end
    object AlteraTokendoCNPJa1: TMenuItem
      Caption = '&Altera Token do CNPJ'#225
      OnClick = AlteraTokendoCNPJa1Click
    end
    object CadastroCentralizadodeContribuinteCCC1: TMenuItem
      Caption = 'Cadastro Centralizado de Contribuinte (CCC)'
      OnClick = CadastroCentralizadodeContribuinteCCC1Click
    end
  end
  object QrAtrEntiDef: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ' +
        'ATRITS, '
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,'
      'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp '
      'FROM atrentidef def '
      'LEFT JOIN atrentiits its ON its.Controle=def.AtrIts '
      'LEFT JOIN atrenticad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc>0 '
      ' '
      'UNION  '
      ' '
      'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, '
      'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,'
      'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp '
      'FROM atrentitxt def '
      'LEFT JOIN atrenticad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc>0 '
      ' '
      'ORDER BY NO_CAD, NO_ITS '
      '')
    Left = 840
    Top = 24
    object QrAtrEntiDefID_Item: TIntegerField
      FieldName = 'ID_Item'
      Required = True
    end
    object QrAtrEntiDefID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
      Required = True
    end
    object QrAtrEntiDefAtrCad: TIntegerField
      FieldName = 'AtrCad'
      Required = True
    end
    object QrAtrEntiDefATRITS: TFloatField
      FieldName = 'ATRITS'
      Required = True
    end
    object QrAtrEntiDefCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
    end
    object QrAtrEntiDefCU_ITS: TLargeintField
      FieldName = 'CU_ITS'
    end
    object QrAtrEntiDefNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrAtrEntiDefNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 255
    end
    object QrAtrEntiDefAtrTyp: TSmallintField
      FieldName = 'AtrTyp'
    end
    object QrAtrEntiDefAtrTxt: TWideStringField
      FieldName = 'AtrTxt'
      Size = 255
    end
  end
  object DsAtrEntiDef: TDataSource
    DataSet = QrAtrEntiDef
    Left = 868
    Top = 24
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 484
    Top = 143
  end
  object QrCondImov: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imv.Unidade, cnd.Cliente,'
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_CND,'
      'CONCAT('
      '  IF(imv.Propriet=:Pa,"D",""),'
      '  IF(imv.Conjuge=:Pa,"C",""),'
      '  IF(imv.Usuario=:Pa,"M",""),'
      '  IF(imv.Procurador=:Pa,"P",""),'
      '  IF(imv.BloqEndEnt=:Pa,"T","")'
      ') TIPO '
      'FROM condimov imv'
      'LEFT JOIN cond cnd ON cnd.Codigo=imv.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE Propriet=:Pa'
      'OR Conjuge=:Pa'
      'OR Usuario=:Pa'
      'OR Procurador=:Pa'
      'OR BloqEndEnt=:Pa')
    Left = 660
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end>
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrCondImovCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCondImovNO_CND: TWideStringField
      FieldName = 'NO_CND'
      Size = 100
    end
    object QrCondImovTIPO: TWideStringField
      FieldName = 'TIPO'
      Required = True
      Size = 5
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 688
    Top = 332
  end
  object QrEstCivil: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM listaecivil'
      'ORDER BY Nome'
      '')
    Left = 668
    Top = 80
    object QrEstCivilCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstCivilNome: TWideStringField
      FieldName = 'Nome'
      Size = 10
    end
    object QrEstCivilLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEstCivilDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEstCivilDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEstCivilUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEstCivilUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsEstCivil: TDataSource
    DataSet = QrEstCivil
    Left = 696
    Top = 80
  end
  object QrEmpresas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, en.RazaoSocial'
      'FROM entidades en'
      'WHERE en.Tipo=0'
      'ORDER BY en.RazaoSocial'
      '')
    Left = 1004
    Top = 24
    object QrEmpresasRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 1032
    Top = 24
  end
  object PMConfig: TPopupMenu
    Left = 336
    Top = 80
    object Atualiza9dgito1: TMenuItem
      Caption = '&Atualiza 9 d'#237'gito'
      OnClick = Atualiza9dgito1Click
    end
  end
  object QrEntiGrupos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      '('
      'SELECT COUNT(Codigo)'
      'FROM entigruits'
      'WHERE Codigo = its.Codigo'
      'GROUP BY Codigo'
      ') Total,'
      
        'IF(gru.Principal <> 0, CONCAT(gru.Nome, " [", IF(ent.Tipo=0, ent' +
        '.RazaoSocial, ent.Nome), "]"), gru.Nome) Nome,'
      'its.*'
      'FROM entigruits its'
      'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade'
      'LEFT JOIN entigrupos gru ON gru.Codigo = its.Codigo'
      'WHERE its.Entidade=:P0'
      '')
    Left = 480
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiGruposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiGruposNome: TWideStringField
      FieldName = 'Nome'
      Size = 103
    end
  end
  object DsEntiGrupos: TDataSource
    DataSet = QrEntiGrupos
    Left = 508
    Top = 432
  end
  object VUSexo: TdmkValUsu
    Panel = Panel6
    QryCampo = 'Sexo'
    UpdCampo = 'Sexo'
    UpdType = utYes
    Left = 624
    Top = 720
  end
  object QrEntiTransp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT et.*,'
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMEENTIDADE '
      'FROM entitransp et, entidades en'
      'WHERE et.Transp=en.Codigo'
      'AND et.Codigo=:P0'
      'ORDER BY Ordem'
      '')
    Left = 1012
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTranspCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiTranspConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrEntiTranspOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEntiTranspTransp: TIntegerField
      FieldName = 'Transp'
    end
    object QrEntiTranspNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEntiTransp: TDataSource
    DataSet = QrEntiTransp
    Left = 1040
    Top = 76
  end
  object PMEntiTransp: TPopupMenu
    OnPopup = PMEntiTranspPopup
    Left = 508
    Top = 340
    object Inclui2: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui2Click
    end
    object Remove1: TMenuItem
      Caption = '&Remove'
      OnClick = Remove1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Ordena1: TMenuItem
      Caption = '&Ordena'
      OnClick = Ordena1Click
    end
  end
  object QrLBacen_Pais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bacen_pais'
      'ORDER BY Nome'
      '')
    Left = 848
    Top = 324
    object QrLBacen_PaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLBacen_PaisNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsLBacen_Pais: TDataSource
    DataSet = QrLBacen_Pais
    Left = 848
    Top = 372
  end
  object DataSource1: TDataSource
    DataSet = QrCadastro2
    Left = 500
    Top = 388
  end
  object QrEntidade2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 88
    Top = 320
  end
  object QrEntiStatus: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(est.Status > 0, "SIM", "N'#195'O")'
      'SELECI, ste.Codigo Status, ste.Nome NO_Status'
      'FROM statusenti ste '
      'LEFT JOIN entiStatus est ON ste.Codigo=est.Status '
      'AND est.Codigo =10'
      'ORDER BY NO_Status')
    Left = 216
    Top = 300
    object QrEntiStatusSELECI: TWideStringField
      FieldName = 'SELECI'
      Required = True
      Size = 3
    end
    object QrEntiStatusStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrEntiStatusNO_Status: TWideStringField
      FieldName = 'NO_Status'
      Required = True
      Size = 255
    end
  end
  object DsEntiStatus: TDataSource
    DataSet = QrEntiStatus
    Left = 216
    Top = 348
  end
  object QrCtbCadGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ctbcadgru'
      'ORDER BY Nome')
    Left = 448
    Top = 210
    object QrCtbCadGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtbCadGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCtbCadGru: TDataSource
    DataSet = QrCtbCadGru
    Left = 448
    Top = 262
  end
  object QrPlaAllCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM plaallcad'
      'WHERE AnaliSinte=2'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 560
    Top = 216
    object QrPlaAllCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPlaAllCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPlaAllCadNivel: TSmallintField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPlaAllCadAnaliSinte: TIntegerField
      FieldName = 'AnaliSinte'
      Required = True
    end
    object QrPlaAllCadCredito: TSmallintField
      FieldName = 'Credito'
      Required = True
    end
    object QrPlaAllCadDebito: TSmallintField
      FieldName = 'Debito'
      Required = True
    end
    object QrPlaAllCadOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object DsPlaAllCad: TDataSource
    DataSet = QrPlaAllCad
    Left = 560
    Top = 268
  end
end
