object FmEntiContat: TFmEntiContat
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-010 :: Cadastro de Contato'
  ClientHeight = 622
  ClientWidth = 758
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 435
    Width = 758
    Height = 46
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    object CkContinuar: TCheckBox
      Left = 90
      Top = 4
      Width = 144
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object CkAtivo: TdmkCheckBox
      Left = 15
      Top = 4
      Width = 69
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Ativo'
      Checked = True
      State = cbChecked
      TabOrder = 0
      QryCampo = 'Ativo'
      UpdCampo = 'Ativo'
      UpdType = utYes
      ValCheck = '1'
      ValUncheck = '0'
      OldValor = #0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 59
    Width = 758
    Height = 79
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Dados da entidade do cadastro origin'#225'rio: '
    Enabled = False
    TabOrder = 0
    object Label4: TLabel
      Left = 15
      Top = 25
      Width = 47
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#243'digo:'
    end
    object Label3: TLabel
      Left = 89
      Top = 25
      Width = 131
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome / Raz'#227'o Social:'
      Visible = False
    end
    object CBCodigo: TdmkDBLookupComboBox
      Left = 84
      Top = 44
      Width = 661
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      TabOrder = 0
      dmkEditCB = EdCodigo
      QryCampo = 'Codigo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCodigo: TdmkEditCB
      Left = 15
      Top = 44
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCodigo
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 138
    Width = 758
    Height = 180
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Dados do contato: '
    TabOrder = 1
    object Label6: TLabel
      Left = 15
      Top = 20
      Width = 16
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID:'
    end
    object Label7: TLabel
      Left = 15
      Top = 76
      Width = 37
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome'
    end
    object Label1: TLabel
      Left = 15
      Top = 125
      Width = 98
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cargo / Filia'#231#227'o:'
    end
    object SpeedButton1: TSpeedButton
      Left = 572
      Top = 145
      Width = 26
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label24: TLabel
      Left = 607
      Top = 76
      Width = 75
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nascimento:'
    end
    object Label5: TLabel
      Left = 118
      Top = 20
      Width = 57
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Entidade:'
    end
    object EdControle: TdmkEdit
      Left = 15
      Top = 39
      Width = 98
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 15
      Top = 95
      Width = 583
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      MaxLength = 30
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CBCargo: TdmkDBLookupComboBox
      Left = 84
      Top = 145
      Width = 486
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsCargos
      TabOrder = 6
      dmkEditCB = EdCargo
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCargo: TdmkEditCB
      Left = 15
      Top = 145
      Width = 69
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCargo
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object TPDtaNatal: TdmkEditDateTimePicker
      Left = 607
      Top = 95
      Width = 138
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 40338.632372372680000000
      Time = 40338.632372372680000000
      TabOrder = 4
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'DtaNatal'
      UpdCampo = 'DtaNatal'
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object RGSexo: TdmkRadioGroup
      Left = 606
      Top = 125
      Width = 139
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Sexo: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'NI'
        'M'
        'F')
      TabOrder = 7
      OnClick = RGSexoClick
      OnExit = RGSexoExit
      QryCampo = 'Sexo'
      UpdCampo = 'Sexo'
      UpdType = utYes
      OldValor = 0
    end
    object EdEntidade: TdmkEditCB
      Left = 118
      Top = 39
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Entidade'
      UpdCampo = 'Entidade'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEntidadeChange
      DBLookupComboBox = CBEntidade
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEntidade: TdmkDBLookupComboBox
      Left = 190
      Top = 39
      Width = 555
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsEntidades
      TabOrder = 2
      dmkEditCB = EdEntidade
      QryCampo = 'Entidade'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 758
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 6
    object GB_R: TGroupBox
      Left = 699
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 640
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 294
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Contato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 294
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Contato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 294
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Contato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 481
    Width = 758
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 7
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 754
      Height = 35
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 536
    Width = 758
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 578
      Top = 18
      Width = 178
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 20
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 18
      Width = 576
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 13
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 318
    Width = 758
    Height = 61
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 2
    object Label2: TLabel
      Left = 0
      Top = 41
      Width = 758
      Height = 20
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Caption = 'Precione a tecla F4 para copiar o documento da entidade'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ExplicitWidth = 404
    end
    object RGTipo: TdmkRadioGroup
      Left = 0
      Top = 0
      Width = 164
      Height = 41
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Caption = '  Pessoa: '
      Columns = 2
      Items.Strings = (
        'Jur'#237'dica'
        'F'#237'sica')
      TabOrder = 0
      OnClick = RGTipoClick
      QryCampo = 'Tipo'
      UpdCampo = 'Tipo'
      UpdType = utYes
      OldValor = 0
    end
    object PnCPF: TPanel
      Left = 391
      Top = 0
      Width = 228
      Height = 41
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      Visible = False
      object Label8: TLabel
        Left = 10
        Top = 20
        Width = 29
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'CPF:'
      end
      object EdCPF: TdmkEdit
        Left = 49
        Top = 15
        Width = 138
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 18
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtCPFJ
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'CPF'
        UpdCampo = 'CPF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdCPFKeyDown
      end
    end
    object PnCNPJ: TPanel
      Left = 164
      Top = 0
      Width = 227
      Height = 41
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 2
      Visible = False
      object Label11: TLabel
        Left = 5
        Top = 20
        Width = 38
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'CNPJ:'
      end
      object EdCNPJ: TdmkEdit
        Left = 44
        Top = 15
        Width = 139
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 18
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtCPFJ
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'CNPJ'
        UpdCampo = 'CNPJ'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdCNPJKeyDown
      end
    end
  end
  object CGAplicacao: TdmkCheckGroup
    Left = 0
    Top = 379
    Width = 758
    Height = 56
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Aplica'#231#227'o: '
    Items.Strings = (
      'Autoriza'#231#227'o para obter XML de NFe')
    TabOrder = 3
    QryCampo = 'Aplicacao'
    UpdCampo = 'Aplicacao'
    UpdType = utYes
    Value = 0
    OldValor = 0
  end
  object QrCargos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM enticargos'
      'WHERE Ativo = 1'
      'AND Tipo IN (0, 1)'
      'ORDER BY Nome')
    Left = 408
    Top = 12
    object QrCargosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCargosCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCargosNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsCargos: TDataSource
    DataSet = QrCargos
    Left = 436
    Top = 12
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdCargo
    QryCampo = 'Cargo'
    UpdCampo = 'Cargo'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 464
    Top = 12
  end
  object QrEntidade: TMySQLQuery
    Database = Dmod.MyDB
    Left = 252
    Top = 124
    object QrEntidadeNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrEntidadeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadeCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntidadeCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
  end
  object DsEntidade: TDataSource
    AutoEdit = False
    DataSet = QrEntidade
    Left = 280
    Top = 124
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, CPF, PNatal'
      'FROM entidades'
      'WHERE Tipo = 1'
      'AND Codigo > 0'
      'ORDER BY Nome')
    Left = 540
    Top = 20
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEntidadesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntidadesPNatal: TDateField
      FieldName = 'PNatal'
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 568
    Top = 20
  end
end
