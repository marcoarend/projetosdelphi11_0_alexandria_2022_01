unit EntiRFCNPJ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, 
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, OleCtrls, SHDocVw, ActiveX, JPEG, ShellAPI, MSHTML, Variants,
  UnDmkEnums;

type
  TFmEntiRFCNPJ = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    ProgressBar1: TProgressBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    WB_IE: TWebBrowser;
    MeDados: TMemo;
    MeCodigo: TMemo;
    BtImporta: TBitBtn;
    Panel_Cadastro: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label_end: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    EdRazaoSocial: TEdit;
    EdCNPJ: TEdit;
    EdRua: TEdit;
    EdNumero: TEdit;
    EdCompl: TEdit;
    EdCEP: TEdit;
    EdBairro: TEdit;
    EdCidade: TEdit;
    EdUF: TEdit;
    RGOpcoes: TRadioGroup;
    EdCNAE: TEdit;
    Label1: TLabel;
    Panel6: TPanel;
    SbNavega: TSpeedButton;
    EdURL_WB: TdmkEdit;
    RGNavegador: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure WB_IEProgressChange(ASender: TObject; Progress,
      ProgressMax: Integer);
    procedure BtImportaClick(Sender: TObject);
    procedure WB_IEDocumentComplete(ASender: TObject;
      const pDisp: IDispatch; const URL: OleVariant);
    procedure FormShow(Sender: TObject);
    procedure SbNavegaClick(Sender: TObject);
  private
    { Private declarations }
    HTMLDoc : IHTMLDocument2;
  public
    { Public declarations }
    FTipoLograd, FTipoImport: Integer;
    FCNPJ, FRazaoSocial, FRua, FLogradouro, FNumero, FCompl, FCEP, FBairro,
    FCidade, FUF, FCNAE: String;
  end;

  var
    FmEntiRFCNPJ: TFmEntiRFCNPJ;
  const
    //CO_URL = 'https://www.receita.fazenda.gov.br/PessoaJuridica/CNPJ/cnpjreva/Cnpjreva_Solicitacao3.asp';
    //CO_URL = 'http://servicos.receita.fazenda.gov.br/Servicos/cnpjreva/Cnpjreva_Solicitacao.asp?cnpj=';
    CO_URL = 'http://servicos.receita.fazenda.gov.br/Servicos/cnpjreva/Cnpjreva_Solicitacao.asp';
    //CO_URL = 'https://servicos.receita.fazenda.gov.br/servicos/cnpjreva/Cnpjreva_Comprovante.asp?';

implementation

uses UnMyObjects, Module, ModuleGeral, UnDmkWeb;

{$R *.DFM}

procedure TFmEntiRFCNPJ.BtImportaClick(Sender: TObject);
var
  CNPJ, RazaoSocial, Rua, Numero, Compl, CEP, Bairro, Cidade, UF,
  Txt1, Txt2, Txt3, CNAE: String;
begin
  case RGOpcoes.ItemIndex of
    0:
    begin
      CNPJ        := EdCNPJ.Text;
      RazaoSocial := EdRazaoSocial.Text;
      Rua         := '';
      Numero      := EdNumero.Text;
      Compl       := EdCompl.Text;
      CEP         := EdCEP.Text;
      Bairro      := '';
      Cidade      := '';
      UF          := '';
      CNAE        := Geral.SoNumero_TT(EdCNAE.Text);
      FTipoImport := 0;
    end;
    1:
    begin
      CNPJ        := EdCNPJ.Text;
      RazaoSocial := EdRazaoSocial.Text;
      Rua         := EdRua.Text;
      Numero      := EdNumero.Text;
      Compl       := EdCompl.Text;
      CEP         := EdCEP.Text;
      Bairro      := EdBairro.Text;
      Cidade      := EdCidade.Text;
      UF          := EdUF.Text;
      CNAE        := Geral.SoNumero_TT(EdCNAE.Text);
      FTipoImport := 1;
    end
    else
    begin
      Geral.MB_Aviso('Tipo de importa��o n�o definido!');
      Exit;
    end;
  end;
  if Length(CNPJ) > 0 then
    FCNPJ := Geral.SoNumero_TT(Trim(CNPJ))
  else
    FCNPJ := '';
  //
  if Length(RazaoSocial) > 0 then
    FRazaoSocial := Trim(RazaoSocial)
  else
    FRazaoSocial := '';
  //
  if Length(Rua) > 0 then
  begin
    FRua := Trim(Rua);
    //
    Geral.SeparaLogradouro(0, FRua, Txt1, Txt2, Txt3, nil, nil);
    //
    FLogradouro := Txt3;
    FTipoLograd := Geral.IMV(Txt2);
  end else
  begin
    FRua        := '';
    FLogradouro := '';
    FTipoLograd := 0;
  end;
  //
  if Length(Numero) > 0 then
    FNumero := Trim(Numero)
  else
    FNumero := '';
  //
  if Length(Compl) > 0 then
    FCompl := Trim(Compl)
  else
    FCompl := '';
  //
  if Length(CEP) > 0 then
    FCEP := Trim(CEP)
  else
    FCEP := '';
  //
  if Length(Bairro) > 0 then
    FBairro := Trim(Bairro)
  else
    FBairro := '';
  //
  if Length(Cidade) > 0 then
    FCidade := Trim(Cidade)
  else
    FCidade := '';
  //
  if Length(UF) > 0 then
    FUF := Trim(UF)
  else
    FUF := '';
  //
  if Length(CNAE) > 0 then
    FCNAE := Trim(CNAE)
  else
    FCNAE := '';
  //
  Close;
end;

procedure TFmEntiRFCNPJ.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiRFCNPJ.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmEntiRFCNPJ.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiRFCNPJ.FormShow(Sender: TObject);
var
  URL: String;
begin
  PageControl1.ActivePageIndex := 0;
  TabSheet2.TabVisible         := False;
  //
  if FCNPJ <> '' then
    URL := CO_URL + '?cnpj=' + FCNPJ
  else
    URL := CO_URL;
  //
  EdURL_WB.Text := URL;
  WB_IE.Navigate(URL);
  //
  FTipoImport := -1;
end;

procedure TFmEntiRFCNPJ.SbNavegaClick(Sender: TObject);
begin
  WB_IE.Navigate(EdURL_WB.Text);
{
  case RGNavegador.ItemIndex of
    0:
    begin
      WB_IE.Navigate(EdURL_WB.Text);
    end;
    1: if WB_Ch.Browser <> nil then
    begin
       WB_Ch.Browser.MainFrame.LoadUrl(EdURL_WB.Text);
    end else
    begin
      WB_IE.Navigate('about:blank');
      //
      if WB_Ch.Browser <> nil then
        WB_Ch.Browser.MainFrame.LoadUrl('about:blank');
      //
      Geral.MB_Aviso('Selecione o Navegador!');
    end;
  end;
}
end;

procedure TFmEntiRFCNPJ.WB_IEDocumentComplete(ASender: TObject;
  const pDisp: IDispatch; const URL: OleVariant);
  procedure Preencher();
  var
    i, j: Integer;
    FormItem: Variant;
    //Botao : Variant;
  begin
    if VarIsNull(WB_IE.OleObject.Document) or
      VarisEmpty(WB_IE.OleObject.Document)
    then
      Exit;
    for i := 0 to WB_IE.OleObject.Document.Forms.Length -1 do
    begin
      FormItem := WB_IE.OleObject.Document.Forms.Item(i);
      for j := 0 to FormItem.Length - 1 do
      begin
         MeDados.Lines.Add(FormItem.Item(j).Name+' >> ' + FormItem.Item(j).Value);
        if (FormItem.Item(j).Name = 'cnpj') then
          FormItem.Item(j).Value := FCNPJ;//Edit_CNPJ.Text;
        if (FormItem.Item(j).Value = 'submit1') then
           FormItem.Item(j).Click;
      end;
   end;
  end;
  procedure PreencheDados();
  var
    i, y : Integer;
  begin
    MeDados.Lines.Text := WB_IE.OleObject.Document.documentElement.innerText;
    for i := 0 to MeDados.Lines.Count - 1 do
    begin
      for y := 0 to ComponentCount - 1 do
      begin
        if Components[y] is TEdit then
        begin
          if (Trim((Components[y] as TEdit).Hint) <> '')
            and ((Components[y] as TEdit).Hint = Trim(MeDados.Lines[i]))
          then
            (Components[y] as TEdit).Text := Trim(MeDados.Lines[i+1]);
        end;
      end;
    end;
  end;
  procedure PreencheCodigos();
  var
    Html : IHTMLElement;
  begin
    if Assigned(WB_IE.Document) then
    begin
      Html := (WB_IE.Document as IHTMLDocument2).body;
      while Html.parentElement <> Nil do
        Html := Html.parentElement;
      MeCodigo.Text := Html.outerHTML;
   End;
  end;
begin
  ProgressBar1.Position := 0;
  //
  if Assigned(WB_IE.Document) then
  begin
    htmlDoc := WB_IE.Document as IHTMLDocument2;
    // htmlDoc.onmouseover := (TEventObject.Create(Document_OnMouseOver) as IDispatch);
    // htmlDoc.onclick := (TEventObject.Create(Document_OnClick) as IDispatch);
  end;
  //
  if Pos('cnpjreva_solicitacao2.asp', LowerCase(URL)) > 0 then
     Preencher();
  If Pos('cnpjreva_comprovante.asp',LowerCase(URL)) > 0 Then
     PreencheDados;
  PreencheCodigos;
end;

procedure TFmEntiRFCNPJ.WB_IEProgressChange(ASender: TObject; Progress,
  ProgressMax: Integer);
begin
  if (Progress>=1) and (ProgressMax>1) then
  begin
    //Ele tira uma valor percentual para colocar no Progressbar
    ProgressBar1.Position := Round((Progress * 100) div ProgressMax);
    ProgressBar1.Visible  := True;
  end else
  begin
    ProgressBar1.Position := 1;
    ProgressBar1.Visible  := False;
  end;
end;

end.

