unit EntiConEntUnit;

interface

uses
  Windows, SysUtils, Classes, DB, mySQLDbTables, DmkGeral, DmkDAC_PF, UnAppListas,
  UnInternalConsts, UnDmkProcFunc, frxClass, frxDBSet, Menus, dmkDBLookupComboBox,
  dmkEditCB;

type
  TEntiConEntUnit = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ExcluiContato(QrEntiContat: TmySQLQuery);
  end;

var
  UnEntiConEntUnit: TEntiConEntUnit;


implementation

uses Module, ModuleGeral, MyDBCheck, UMySQLModule;

{ TEntiConEntUnit }

procedure TEntiConEntUnit.ExcluiContato(QrEntiContat: TmySQLQuery);
var
  Qry: TmySQLQuery;
  Codigo, Controle: String;
begin
  Qry := TMySQLQuery.Create(Dmod);
  Qry.Database := Dmod.MyDB;
  //
  Codigo   := Geral.FF0(QrEntiContat.FieldByName('Codigo').AsInteger);
  Controle := Geral.FF0(QrEntiContat.FieldByName('Controle').AsInteger);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT COUNT(*) Itens ',
  'FROM enticonent ',
  'WHERE Codigo<>' + Codigo,
  'AND Codigo<>0 ',
  'AND Controle=' + Controle,
  '']);
  if Qry.FieldByName('Itens').AsInteger = 0 then
  begin
    if Geral.MB_Pergunta(
    'Confirma a exclus�o definitiva deste contato?') = ID_YES then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM enticonent ',
      'WHERE Codigo=' + Codigo,
      'AND Controle=' + Controle,
      '']);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM enticontat ',
      'WHERE Codigo=' + Codigo,
      'AND Controle=' + Controle,
      '']);
    end;
    UnDmkDAC_PF.AbreQuery(QrEntiContat, DMod.MyDB);
  end
  else
  begin
    Geral.MB_Aviso(
    'Contato n�o pode ser exclu�do pois est� atrelado a outra(s) entidades!' +
    sLineBreak +
    'Utilize o gerenciamento de contatos para remov�-lo da entidade desejada!');
  end;
end;

end.
