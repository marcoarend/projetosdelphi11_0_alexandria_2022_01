unit Enti9Digit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, DmkDAC_PF,
  dmkEdit, dmkDBGridZTO, Vcl.Menus, System.Variants;

type
  THackDBGrid = class(TDBGrid);
  TFmEnti9Digit = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtPesquisa: TBitBtn;
    LaTitulo1C: TLabel;
    GridPanel1: TGridPanel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    QrNTelAtz: TmySQLQuery;
    DsNTelAtz: TDataSource;
    QrTelAtz: TmySQLQuery;
    DsTelAtz: TDataSource;
    QrTel: TmySQLQuery;
    PB1: TProgressBar;
    EdTe1: TdmkEdit;
    Label112: TLabel;
    dmkDBGTelAtz: TdmkDBGridZTO;
    QrTelAtzCodigo: TIntegerField;
    QrTelAtzConta: TIntegerField;
    QrTelAtzNome: TWideStringField;
    QrTelAtzTelefone: TWideStringField;
    QrTelAtzTelefoneN: TWideStringField;
    QrNTelAtzCodigo: TIntegerField;
    QrNTelAtzConta: TIntegerField;
    QrNTelAtzNome: TWideStringField;
    QrNTelAtzTelefone: TWideStringField;
    dmkDBGNTelAtz: TdmkDBGridZTO;
    LaNTelAtz: TLabel;
    Panel5: TPanel;
    BtAtzAltera: TBitBtn;
    LaTelAtz: TLabel;
    BtAtzRemove: TBitBtn;
    BtSalvar: TBitBtn;
    QrTelAtzTelefoneN_TXT: TWideStringField;
    QrTelAtzTelefone_TXT: TWideStringField;
    QrNTelAtzTelefone_TXT: TWideStringField;
    Panel6: TPanel;
    BtNAtzAltera: TBitBtn;
    PMTelAtz: TPopupMenu;
    PMNTelAtz: TPopupMenu;
    Altera1: TMenuItem;
    Altera2: TMenuItem;
    Excluiselecionado1: TMenuItem;
    QrTelAtzEhCelular: TSmallintField;
    QrNTelAtzEhCelular: TSmallintField;
    Button1: TButton;
    QrTelAtzAtzCadas: TSmallintField;
    QrNTelAtzAtzCadas: TSmallintField;
    QrTelAtzTipo: TSmallintField;
    QrNTelAtzTipo: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrTelAtzAfterScroll(DataSet: TDataSet);
    procedure QrTelAtzBeforeClose(DataSet: TDataSet);
    procedure QrNTelAtzAfterScroll(DataSet: TDataSet);
    procedure QrNTelAtzBeforeClose(DataSet: TDataSet);
    procedure QrTelAtzCalcFields(DataSet: TDataSet);
    procedure QrNTelAtzCalcFields(DataSet: TDataSet);
    procedure dmkDBGTelAtzDblClick(Sender: TObject);
    procedure dmkDBGNTelAtzDblClick(Sender: TObject);
    procedure PMNTelAtzPopup(Sender: TObject);
    procedure PMTelAtzPopup(Sender: TObject);
    procedure Altera2Click(Sender: TObject);
    procedure BtAtzAlteraClick(Sender: TObject);
    procedure BtNAtzAlteraClick(Sender: TObject);
    procedure BtAtzRemoveClick(Sender: TObject);
    procedure Excluiselecionado1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
  private
    { Private declarations }
    FTmpTabela: String;
    function  VerificaSeAtualiza(var Telefone: String): Boolean;
    procedure ObtemListaTelefones();
    procedure ReopenTelAtz(Query: TmySQLQuery; Atualiza: Boolean);
    procedure AlteraTelefone(Telefone, TelefoneCampo: String; Codigo,
              Conta, EhCelular: Integer; Query: TmySQLQuery);
    procedure RemoveTelefone();
    procedure RemoveEntiTelSemCodigo();
  public
    { Public declarations }
    FExecutou: Boolean;
  end;

  var
  FmEnti9Digit: TFmEnti9Digit;

implementation

uses UnMyObjects, Module, ModuleGeral, UCreate, UMySQLModule;

{$R *.DFM}

procedure TFmEnti9Digit.Altera1Click(Sender: TObject);
begin
  AlteraTelefone(QrNTelAtzTelefone.Value, 'Telefone', QrNTelAtzCodigo.Value,
    QrNTelAtzConta.Value, QrNTelAtzEhCelular.Value, QrNTelAtz);
end;

procedure TFmEnti9Digit.Altera2Click(Sender: TObject);
begin
  AlteraTelefone(QrTelAtzTelefoneN.Value, 'TelefoneN', QrTelAtzCodigo.Value,
    QrTelAtzConta.Value, QrTelAtzEhCelular.Value, QrTelAtz);
end;

procedure TFmEnti9Digit.RemoveEntiTelSemCodigo();
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      DELETE_FROM + ' entitel ',
      'WHERE Codigo = 0 ',
      '']);
  finally
    Qry.Free;
  end;
end;

procedure TFmEnti9Digit.AlteraTelefone(Telefone, TelefoneCampo: String; Codigo,
  Conta, EhCelular: Integer; Query: TmySQLQuery);
var
  Continua: Boolean;
  Txt: String;
begin
  if (Query.State <> dsInactive) and (Query.RecordCount > 0) then
  begin
    Txt := Telefone;
    //
    if InputQuery('Telefone', 'Informe o telefone', Txt) then
    begin
      Txt := Geral.SoNumero_TT(Txt);
      //
      if Geral.MB_Pergunta('Confirma a altera��o do telefone para "' +
        Geral.FormataTelefone_TT_Curto(Txt) + '"?')  <> ID_YES
      then
        Exit;
      //
      if not UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FTmpTabela, False,
        [TelefoneCampo, 'AtzCadas'], ['EhCelular', 'Conta', 'Codigo'],
        [Txt, 1], [EhCelular, Conta, Codigo], False) then
      begin
        Geral.MB_Erro('Falha ao obter lista de telefones!');
        Exit;
      end else
      begin
        ReopenTelAtz(Query, QrTelAtz = Query);
        Query.Locate('Codigo;Conta;EhCelular', VarArrayOf([Codigo, Conta, EhCelular]), []);
      end;
    end;
  end;
end;

procedure TFmEnti9Digit.BtAtzAlteraClick(Sender: TObject);
begin
  AlteraTelefone(QrTelAtzTelefoneN.Value, 'TelefoneN', QrTelAtzCodigo.Value,
    QrTelAtzConta.Value, QrTelAtzEhCelular.Value, QrTelAtz);
end;

procedure TFmEnti9Digit.BtAtzRemoveClick(Sender: TObject);
begin
  RemoveTelefone;
end;

procedure TFmEnti9Digit.BtNAtzAlteraClick(Sender: TObject);
begin
  AlteraTelefone(QrNTelAtzTelefone.Value, 'Telefone', QrNTelAtzCodigo.Value,
    QrNTelAtzConta.Value, QrNTelAtzEhCelular.Value, QrNTelAtz);
end;

procedure TFmEnti9Digit.BtPesquisaClick(Sender: TObject);
begin
  RemoveEntiTelSemCodigo();
  ObtemListaTelefones;
  ReopenTelAtz(QrTelAtz, True);
  ReopenTelAtz(QrNTelAtz, False);
  //
  if QrTelAtz.RecordCount = 0 then
  begin
    FExecutou := True;
    //
    Geral.MB_Aviso('N�o foi localizado nenhum telefone para ser atualizado!');
  end;
end;

procedure TFmEnti9Digit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEnti9Digit.BtSalvarClick(Sender: TObject);

  function AtualizaDados(Codigo, Conta, Tipo, EhCelular: Integer;
    Telefone: String): Boolean;
  var
    ValorIdx: Integer;
    Tabela, CampoTel, CampoIdx: String;
  begin
    Result   := False;
    Tabela   := '';
    CampoTel := '';
    CampoIdx := '';
    ValorIdx := 0;
    //
    if (Codigo <> 0) and (Conta = 0) then //Dados da entidade
    begin
      Tabela   := 'entidades';
      CampoIdx := 'Codigo';
      ValorIdx := Codigo;
      //
      if EhCelular = 1 then
      begin
        if Tipo = 0 then
          CampoTel := 'ECel'
        else
          CampoTel := 'PCel';
      end else
      if EhCelular = 2 then
      begin
        if Tipo = 0 then
          CampoTel := 'ETe2'
        else
          CampoTel := 'PTe2';
      end else
      if EhCelular = 3 then
      begin
        if Tipo = 0 then
          CampoTel := 'ETe3'
        else
          CampoTel := 'PTe3';
      end else
      begin
        if Tipo = 0 then
          CampoTel := 'ETe1'
        else
          CampoTel := 'PTe1';
      end;
    end else
    if (Codigo <> 0) and (Conta <> 0) then //Dados do entitel
    begin
      Tabela   := 'entitel';
      CampoIdx := 'Conta';
      CampoTel := 'Telefone';
      ValorIdx := Conta;
    end;
    //
    if (Tabela <> '') and (CampoTel <> '') and (ValorIdx <> 0) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False,
          [CampoTel], [CampoIdx], [Telefone], [ValorIdx], False)
      then
        Result := True;
    end;
  end;

const
  CO_Msg_Falha = 'Falha ao atualizar dados!';
var
  Telefone: String;
  Codigo, Conta, Tipo, EhCelular: Integer;
  AtzCadas: Boolean;
begin
  if Geral.MB_Pergunta('Confirma a atualiza��o dos cadastros de entidades?' +
    sLineBreak + 'ATEN��O: Este procedimento n�o poder� ser revertido!') <> ID_YES
  then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    QrNTelAtz.DisableControls;
    QrTelAtz.DisableControls;
    //
    dmkDBGTelAtz.Enabled  := False;
    dmkDBGNTelAtz.Enabled := False;
    //
    PB1.Max      := QrNTelAtz.RecordCount + QrTelAtz.RecordCount;
    PB1.Position := 0;
    //
    //Atualiza dados clientes
    if QrNTelAtz.RecordCount > 0 then
    begin
      QrNTelAtz.First;
      while not QrNTelAtz.Eof do
      begin
        AtzCadas := Geral.IntToBool(QrNTelAtzAtzCadas.Value);
        //
        if AtzCadas then
        begin
          Codigo    := QrNTelAtzCodigo.Value;
          Conta     := QrNTelAtzConta.Value;
          Tipo      := QrNTelAtzTipo.Value;
          EhCelular := QrNTelAtzEhCelular.Value;
          Telefone  := QrNTelAtzTelefone.Value;
          //
          if not AtualizaDados(Codigo, Conta, Tipo, EhCelular, Telefone) then
          begin
            Geral.MB_Erro(CO_Msg_Falha);
            Exit;
          end;
        end;
        QrNTelAtz.Next;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
      end;
    end;
    //Atualiza telefones
    if QrTelAtz.RecordCount > 0 then
    begin
      QrTelAtz.First;
      while not QrTelAtz.Eof do
      begin
        Codigo    := QrTelAtzCodigo.Value;
        Conta     := QrTelAtzConta.Value;
        Tipo      := QrTelAtzTipo.Value;
        EhCelular := QrTelAtzEhCelular.Value;
        Telefone  := QrTelAtzTelefoneN.Value;
        //
        if not AtualizaDados(Codigo, Conta, Tipo, EhCelular, Telefone) then
        begin
          Geral.MB_Erro(CO_Msg_Falha);
          Exit;
        end;
        //
        QrTelAtz.Next;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
      end;
    end;
    FExecutou := True;
    //
    Geral.MB_Aviso('Atualiza��o finalizada com sucesso!');
    //
  finally
    QrNTelAtz.EnableControls;
    QrTelAtz.EnableControls;
    //
    dmkDBGTelAtz.Enabled  := True;
    dmkDBGNTelAtz.Enabled := True;
    PB1.Position          := 0;
    //
    Screen.Cursor := crDefault;
  end;
  if FExecutou then
    Close;
end;

procedure TFmEnti9Digit.Button1Click(Sender: TObject);
var
  Tel, TelNew: String;
begin
  Tel    := EdTe1.ValueVariant;
  TelNew := Tel;
  //
  if not VerificaSeAtualiza(TelNew) then
    Geral.MB_Aviso('O telefone informado n�o ser� atualizado!' + sLineBreak +
      'Telefone informado: ' + Geral.FormataTelefone_TT_Curto(Tel))
  else
    Geral.MB_Aviso('O telefone informado foi atualizado!' + sLineBreak +
      'Telefone informado: ' + Geral.FormataTelefone_TT(Tel) + sLineBreak +
      'Telefone atualizado: ' + Geral.FormataTelefone_TT(TelNew));
  //
  EdTe1.ValueVariant := Tel;
end;

procedure TFmEnti9Digit.dmkDBGNTelAtzDblClick(Sender: TObject);
var
  Campo: String;
begin
  if (QrNTelAtz.State <> dsInactive) and (QrNTelAtz.RecordCount > 0) then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if UpperCase(Campo) = UpperCase('Telefone_TXT') then
    begin
      AlteraTelefone(QrNTelAtzTelefone.Value, 'Telefone', QrNTelAtzCodigo.Value,
        QrNTelAtzConta.Value, QrNTelAtzEhCelular.Value, QrNTelAtz);
    end;
  end;
end;

procedure TFmEnti9Digit.dmkDBGTelAtzDblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
  //
  if (UpperCase(Campo) = UpperCase('TelefoneN_TXT')) or
    (UpperCase(Campo) = UpperCase('Telefone_TXT')) then
  begin
    AlteraTelefone(QrTelAtzTelefoneN.Value, 'TelefoneN', QrTelAtzCodigo.Value,
      QrTelAtzConta.Value, QrTelAtzEhCelular.Value, QrTelAtz);
  end;
end;

procedure TFmEnti9Digit.Excluiselecionado1Click(Sender: TObject);
begin
  RemoveTelefone;
end;

procedure TFmEnti9Digit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEnti9Digit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PB1.Position := 0;
  //
  dmkDBGTelAtz.DataSource  := DsTelAtz;
  dmkDBGTelAtz.PopupMenu   := PMTelAtz;
  dmkDBGNTelAtz.DataSource := DsNTelAtz;
  dmkDBGNTelAtz.PopupMenu  := PMNTelAtz;
  //
  dmkDBGTelAtz.Columns[4].Visible  := False;
  dmkDBGTelAtz.Columns[5].Visible  := False;
  dmkDBGNTelAtz.Columns[3].Visible := False;
  dmkDBGNTelAtz.Columns[4].Visible := False;
  //
  BtAtzAltera.Enabled  := False;
  BtAtzRemove.Enabled  := False;
  BtNAtzAltera.Enabled := False;
  BtSalvar.Enabled     := False;
  //
  FExecutou := False;
end;

procedure TFmEnti9Digit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;


function TFmEnti9Digit.VerificaSeAtualiza(var Telefone: String): Boolean;
const
  NovoDig = 9;
var
  Tam: Integer;
  Txt, Txt2: String;
begin
  Result   := False;
  Telefone := Geral.FormataTelefone_TT_Curto(Telefone);
  Telefone := Geral.SoNumero_TT(Telefone);
  Tam      := Length(Telefone);
  //
  //8 d�gitos apenas adiciona
  if Tam = 8 then //Sem DDD
  begin
    if (Copy(Telefone, 1, 1) = '9') or ((Copy(Telefone, 1, 1) = '8')) then
    begin
      Telefone := Geral.FF0(NovoDig) + Telefone;
      Result   := True;
    end;
  end else
  if Tam = 10 then //Com DDD sem o Zero
  begin
    Txt := Copy(Telefone, Length(Telefone) - 8 + 1, 1);
    //
    if (Txt = '9') or (Txt = '8') then
    begin
      Txt      := Copy(Telefone, 1, Length(Telefone) - 8);
      Txt2     := Copy(Telefone, Length(Telefone) - 8 + 1, 8);
      Telefone := Txt + Geral.FF0(NovoDig) + Txt2;
      Result   := True;
    end;
  end else
  if Tam = 11 then //Com DDD com o Zero
  begin
    Txt := Copy(Telefone, Length(Telefone) - 8 + 1, 1);
    //
    if (Txt = '9') or (Txt = '8') then
    begin
      Txt      := Copy(Telefone, 1, Length(Telefone) - 8);
      Txt2     := Copy(Telefone, Length(Telefone) - 8 + 1, 8);
      //
      if (Copy(Txt, 1, 1) = '0') then //Apenas DD (com zero). Para DD + 9 n�o atualizar novamente
      begin
        Telefone := Txt + Geral.FF0(NovoDig) + Txt2;
        Result   := True;
      end;
    end;
  end else
  if Tam in [12,13] then //Com DDD (com e sem Zero) + c�digo do pa�s
  begin
    Txt  := Copy(Telefone, 1, 2); //C�digo do pa�s
    Txt2 := Copy(Telefone, Length(Telefone) - 8 + 1, 1);
    //
    if (Txt = '55') and ((Txt2 = '9') or (Txt2 = '8')) then //C�digo do Brasil
    begin
      Txt  := Copy(Telefone, 1, Length(Telefone) - 8);
      Txt2 := Copy(Telefone, Length(Telefone) - 8 + 1, 8);

      if ((Length(Txt) = 5) and (Copy(Txt, 3, 1) = '0')) or //Apenas C�d pa�s (2 digit.) + DD (com zero)
        ((Length(Txt) = 4) and (Copy(Txt, 1, 2) = '55')) then //Apenas C�d pa�s (2 digit.) + DD (sem zero)
      begin
        Telefone := Txt + Geral.FF0(NovoDig) + Txt2;
        Result   := True;
      end;
    end;
  end;
end;

procedure TFmEnti9Digit.ObtemListaTelefones;
var
  Atualiza: Boolean;
  Codigo, Conta, EhCelular, Tipo: Integer;
  Nome, Telefone, TelefoneN: String;
begin
  FTmpTabela := UCriar.RecriaTempTableNovo(ntrttEnt9Dig, DmodG.QrUpdPID1, True);
  //
  if FTmpTabela = '' then
  begin
    Geral.MB_Erro('Falha ao criar lista de telefones!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrTel, Dmod.MyDB, [
      'SELECT Codigo, Conta, Tipo, Nome, Tel, EhCelular ',
      'FROM ( ',
      'SELECT Codigo, 0 Conta, Tipo, ',
      'IF(Tipo=0, RazaoSocial, Nome) Nome, ',
      'IF(Tipo=0, ETe1, PTe1) Tel, 0 EhCelular ',
      'FROM entidades ',
      'WHERE IF(Tipo=0, ETe1, PTe1) <> "" ',
      ' ',
      'UNION ',
      ' ',
      'SELECT Codigo, 0 Conta, Tipo, ',
      'IF(Tipo=0, RazaoSocial, Nome) Nome, ',
      'IF(Tipo=0, ETe2, PTe2) Tel, 2 EhCelular ',
      'FROM entidades ',
      'WHERE IF(Tipo=0, ETe2, PTe2) <> "" ',
      ' ',
      'UNION ',
      ' ',
      'SELECT Codigo, 0 Conta, Tipo, ',
      'IF(Tipo=0, RazaoSocial, Nome) Nome, ',
      'IF(Tipo=0, ETe3, PTe3) Tel, 3 EhCelular ',
      'FROM entidades ',
      'WHERE IF(Tipo=0, ETe3, PTe3) <> "" ',
      ' ',
      'UNION ',
      ' ',
      'SELECT Codigo, 0 Conta, Tipo, ',
      'IF(Tipo=0, RazaoSocial, Nome) Nome, ',
      'IF(Tipo=0, ECel, PCel) Tel, 1 EhCelular ',
      'FROM entidades ',
      'WHERE IF(Tipo=0, ECel, PCel) <> "" ',
      ' ',
      'UNION ',
      ' ',
      'SELECT ete.Codigo, ete.Conta, ent.Tipo, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome, ',
      'ete.Telefone Tel, 0 EhCelular ',
      'FROM entitel ete ',
      'LEFT JOIN entidades ent ON ent.Codigo = ete.Codigo ',
      'WHERE ete.Telefone <> "" ',
      ' ',
      ') ent ',
      'ORDER BY Nome, Tel ',
      '']);
    if QrTel.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max      := QrTel.RecordCount;
      //
      while not QrTel.Eof do
      begin
        Codigo    := QrTel.FieldByName('Codigo').AsInteger;
        Conta     := QrTel.FieldByName('Conta').AsInteger;
        Nome      := QrTel.FieldByName('Nome').AsString;
        Telefone  := QrTel.FieldByName('Tel').AsString;
        EhCelular := QrTel.FieldByName('EhCelular').AsInteger;
        TelefoneN := Telefone;
        Tipo      := QrTel.FieldByName('Tipo').AsInteger;
        Atualiza  := VerificaSeAtualiza(TelefoneN);
        //
        if not Atualiza then
          TelefoneN := '';
        //
        if not UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FTmpTabela, False,
          ['Tipo', 'Nome', 'Telefone', 'TelefoneN', 'EhCelular', 'Conta'], ['Codigo'],
          [Tipo, Nome, Telefone, TelefoneN, EhCelular, Conta], [Codigo], False) then
        begin
          Geral.MB_Erro('Falha ao obter lista de telefones!');
          Exit;
        end;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        QrTel.Next;
      end;
    end;
  finally
    PB1.Position := 0;
    //
    QrTel.Close;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEnti9Digit.PMNTelAtzPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrNTelAtz.State <> dsInactive) and (QrNTelAtz.RecordCount > 0);
  //
  Altera1.Enabled := Enab;
end;

procedure TFmEnti9Digit.PMTelAtzPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrTelAtz.State <> dsInactive) and (QrTelAtz.RecordCount > 0);
  //
  Altera2.Enabled            := Enab;
  Excluiselecionado1.Enabled := Enab;
end;

procedure TFmEnti9Digit.QrNTelAtzAfterScroll(DataSet: TDataSet);
begin
  LaNTelAtz.Caption := 'Total de itens: ' + Geral.FF0(QrNTelAtz.RecordCount);
  //
  BtNAtzAltera.Enabled := True;
end;

procedure TFmEnti9Digit.QrNTelAtzBeforeClose(DataSet: TDataSet);
begin
  LaNTelAtz.Caption := '';
  //
  BtNAtzAltera.Enabled := False;
end;

procedure TFmEnti9Digit.QrNTelAtzCalcFields(DataSet: TDataSet);
begin
  QrNTelAtzTelefone_TXT.Value  := Geral.FormataTelefone_TT_Curto(QrNTelAtzTelefone.Value);
end;

procedure TFmEnti9Digit.QrTelAtzAfterScroll(DataSet: TDataSet);
begin
  LaTelAtz.Caption := 'Total de itens: ' + Geral.FF0(QrTelAtz.RecordCount);
  //
  BtAtzAltera.Enabled := True;
  BtAtzRemove.Enabled := True;
  BtSalvar.Enabled    := True;
end;

procedure TFmEnti9Digit.QrTelAtzBeforeClose(DataSet: TDataSet);
begin
  LaTelAtz.Caption := '';
  //
  BtAtzAltera.Enabled := False;
  BtAtzRemove.Enabled := False;
  BtSalvar.Enabled    := False;
end;

procedure TFmEnti9Digit.QrTelAtzCalcFields(DataSet: TDataSet);
begin
  QrTelAtzTelefone_TXT.Value  := Geral.FormataTelefone_TT_Curto(QrTelAtzTelefone.Value);
  QrTelAtzTelefoneN_TXT.Value := Geral.FormataTelefone_TT_Curto(QrTelAtzTelefoneN.Value);
end;

procedure TFmEnti9Digit.RemoveTelefone();

  function ExcluiAtual(Codigo, Conta, EhCelular: Integer): Boolean;
  begin
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
                DELETE_FROM + ' '+ FTmpTabela,
                'WHERE Codigo=' + Geral.FF0(Codigo),
                'AND Conta=' + Geral.FF0(Conta),
                'AND EhCelular=' + Geral.FF0(EhCelular),
                '']);
  end;

var
  i, Codigo, Conta, EhCelular: Integer;
begin
  if (QrTelAtz.State <> dsInactive) and (QrTelAtz.RecordCount > 0) then
  begin
    if Geral.MB_Pergunta('Voc� realmente deseja remover da lista os itens selecionados?' +
      sLineBreak + 'AVISO: Os itens removidos da lista n�o ser�o removidos do cadastro de entidades!' +
      sLineBreak + 'Estes itens apenas N�O ser�o atualizados!') <> ID_YES
    then
      Exit;
    //
    if dmkDBGTelAtz.SelectedRows.Count > 1 then
    begin
      with dmkDBGTelAtz.DataSource.DataSet do
      begin
        for i:= 0 to dmkDBGTelAtz.SelectedRows.Count - 1 do
        begin
          //GotoBookmark(dmkDBGTelAtz.SelectedRows.Items[i]);
          GotoBookmark(dmkDBGTelAtz.SelectedRows.Items[i]);
          //
          Codigo    := QrTelAtzCodigo.Value;
          Conta     := QrTelAtzConta.Value;
          EhCelular := QrTelAtzEhCelular.Value;
          //
          ExcluiAtual(Codigo, Conta, EhCelular);
        end;
      end;
    end else
    begin
      Codigo    := QrTelAtzCodigo.Value;
      Conta     := QrTelAtzConta.Value;
      EhCelular := QrTelAtzEhCelular.Value;
      //
      ExcluiAtual(Codigo, Conta, EhCelular);
    end;
  end;
  ReopenTelAtz(QrTelAtz, True);
  QrNTelAtz.Locate('Codigo;Conta;EhCelular', VarArrayOf([Codigo, Conta, EhCelular]), []);

end;

procedure TFmEnti9Digit.ReopenTelAtz(Query: TmySQLQuery; Atualiza: Boolean);
var
  SQLCompl: String;
begin
  if Atualiza then
    SQLCompl := '<>'
  else
    SQLCompl := '=';
  //
  if FTmpTabela <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
      'SELECT * ',
      'FROM ' + FTmpTabela,
      'WHERE TelefoneN ' + SQLCompl + ' ""',
      'ORDER BY Nome, Conta ',
      '']);
  end else
    Query.Close;
end;

end.
