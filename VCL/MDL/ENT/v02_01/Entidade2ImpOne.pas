unit Entidade2ImpOne;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, frxClass, frxDBSet, UnDMkEnums;

type
  TFmEntidade2ImpOne = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEntidade: TmySQLQuery;
    frxDsEntidade: TfrxDBDataset;
    frxEntidade: TfrxReport;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    LaCliente: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadeCodigo: TIntegerField;
    QrEntidadeNO_ENT: TWideStringField;
    QrEntidadeNOME_CNPJ_CPF: TWideStringField;
    QrEntidadeCNPJ_CPF: TWideStringField;
    QrEntidadeNOME_IE_RG: TWideStringField;
    QrEntidadeIE_RG: TWideStringField;
    QrEntidadeNO_2_ENT: TWideStringField;
    QrEntidadeRUA: TWideStringField;
    QrEntidadeCOMPL: TWideStringField;
    QrEntidadeBAIRRO: TWideStringField;
    QrEntidadeCIDADE: TWideStringField;
    QrEntidadeNOMELOGRAD: TWideStringField;
    QrEntidadeNOMEUF: TWideStringField;
    QrEntidadePais: TWideStringField;
    QrEntidadeTE1: TWideStringField;
    QrEntidadeTRATO: TWideStringField;
    QrEntidadeENDEREF: TWideStringField;
    QrEntidadeSSP_NIRE_X: TWideStringField;
    QrEntidadeSSP_NIRE_V: TWideStringField;
    QrEntidadeTipo: TSmallintField;
    QrEntidadeCEP_TXT: TWideStringField;
    QrEntidadeNUMERO_TXT: TWideStringField;
    QrET: TmySQLQuery;
    QrETNOMEETC: TWideStringField;
    QrETCodigo: TIntegerField;
    QrETTelefone: TWideStringField;
    QrETRamal: TWideStringField;
    QrETTEL_TXT: TWideStringField;
    QrETNO_CONTATO: TWideStringField;
    QrETNATAL_TXT: TWideStringField;
    QrETDtaNatal: TDateField;
    QrETNO_CARGO: TWideStringField;
    QrEM: TmySQLQuery;
    QrEMCodigo: TIntegerField;
    QrEMNO_CONTATO: TWideStringField;
    QrEMNO_CARGO: TWideStringField;
    QrEMDtaNatal: TDateField;
    QrEMNOMEETC: TWideStringField;
    QrEMEMail: TWideStringField;
    QrEMNATAL_TXT: TWideStringField;
    frxDsEntiTel: TfrxDBDataset;
    frxDsEntiEma: TfrxDBDataset;
    QrEntidadeTE1_TXT: TWideStringField;
    EdContatos: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdComent: TdmkEdit;
    QrEntidadeCEP: TFloatField;
    QrEntidadeNUMERO: TFloatField;
    QrEntidadeCODMUNICI: TFloatField;
    QrEntidadeCODPAIS: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxEntidadeGetValue(const VarName: string; var Value: Variant);
    procedure QrEntidadeCalcFields(DataSet: TDataSet);
    procedure QrEMCalcFields(DataSet: TDataSet);
    procedure QrETCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEntidade2ImpOne: TFmEntidade2ImpOne;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmEntidade2ImpOne.BtOKClick(Sender: TObject);
var
  DD_Contat_Empty, DD_Observacoes: TfrxDetailData;
begin
  QrEntidade.Close;
  QrEntidade.Params[0].AsInteger := EdEntidade.ValueVariant;
  UnDmkDAC_PF.AbreQuery(QrEntidade, Dmod.MyDB);
  //
  QrET.Close;
  QrET.Params[0].AsInteger := EdEntidade.ValueVariant;
  UnDmkDAC_PF.AbreQuery(QrET, Dmod.MyDB);
  //
  QrEM.Close;
  QrEM.Params[0].AsInteger := EdEntidade.ValueVariant;
  UnDmkDAC_PF.AbreQuery(QrEM, Dmod.MyDB);
  //
  DD_Contat_Empty := frxEntidade.FindObject('DD_Contat_Empty') as TfrxDetailData;
  DD_Contat_Empty.RowCount := EdContatos.ValueVariant;
  DD_Observacoes := frxEntidade.FindObject('DD_Observacoes') as TfrxDetailData;
  DD_Observacoes.RowCount := EdComent.ValueVariant;
  //
  MyObjects.frxDefineDataSets(frxEntidade, [
    DModG.frxDsDono,
    frxDsEntidade,
    frxDsEntiTel,
    frxDsEntiEma
  ]);
  //
  MyObjects.frxMostra(frxEntidade, 'Dados de entidade');
end;

procedure TFmEntidade2ImpOne.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntidade2ImpOne.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntidade2ImpOne.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmEntidade2ImpOne.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidade2ImpOne.frxEntidadeGetValue(const VarName: string;
  var Value: Variant);
  function Qual(Valor: Variant; Zerado: String = ''): Variant;
  begin
    if QrEntidadeCodigo.Value = 0 then
      Result := Zerado
    else
      Result := Valor;
  end;
var
  IE_RG: String;
begin
  if VarName ='VARF_CODIGO' then
    Value := Qual('- N� ' + Geral.FFN(QrEntidadeCodigo.Value, 6), '')
  else
  if VarName ='VARF_DATA' then
    Value := Geral.FDT(Now(), 0)
  else
  if VarName ='VARF_CADASTRO' then
    Value := Qual(QrEntidadeNOME_CNPJ_CPF.Value + ': ' +
      Geral.FormataCNPJ_TT(QrEntidadeCNPJ_CPF.Value), 'CNPJ / CPF:')
  else
  if VarName ='VARF_REGISTRO' then
  begin
    if QrEntidadeTipo.Value = 0 then
      IE_RG := Geral.Formata_IE(QrEntidadeIE_RG.Value, QrEntidadeNOMEUF.Value, '')
    else
      IE_RG := QrEntidadeIE_RG.Value;
    //
    Value := Qual(QrEntidadeNOME_IE_RG.Value + ': ' + IE_RG, 'IE / RG:');
  end else
  if VarName ='VARF_IM' then
    Value := Qual(QrEntidadeSSP_NIRE_X.Value + ': ' +
      QrEntidadeSSP_NIRE_V.Value, 'IM / SSP / Data RG:')
  else
  if VarName ='VARF_LOGRADOURO' then
  begin
    Value := QrEntidadeRUA.Value;
    if Trim(QrEntidadeNOMELOGRAD.Value) <> '' then
    Value := QrEntidadeNOMELOGRAD.Value + ' ' + QrEntidadeRUA.Value;
  end
  else
end;

procedure TFmEntidade2ImpOne.QrEMCalcFields(DataSet: TDataSet);
begin
  QrEMNATAL_TXT.Value := Geral.FDT(QrEMDtaNatal.Value, 3);
end;

procedure TFmEntidade2ImpOne.QrEntidadeCalcFields(DataSet: TDataSet);
begin
  QrEntidadeCEP_TXT.Value := Geral.FormataCEP_NT(QrEntidadeCEP.Value);
  QrEntidadeNumero_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadeRUA.Value,
    QrEntidadeNUMERO.Value, True);
  QrEntidadeTE1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadeTE1.Value);
end;

procedure TFmEntidade2ImpOne.QrETCalcFields(DataSet: TDataSet);
begin
  QrETTEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrETTelefone.Value);
  QrETNATAL_TXT.Value := Geral.FDT(QrETDtaNatal.Value, 3);
end;

end.
