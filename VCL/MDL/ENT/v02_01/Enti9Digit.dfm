object FmEnti9Digit: TFmEnti9Digit
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Atualizar telefones 9 d'#237'gito'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 319
        Height = 32
        Caption = 'Atualizar telefones 9 d'#237'gito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 319
        Height = 32
        Caption = 'Atualizar telefones 9 d'#237'gito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 319
        Height = 32
        Caption = 'Atualizar telefones 9 d'#237'gito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 450
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 450
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 450
        Align = alClient
        TabOrder = 0
        object GridPanel1: TGridPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 434
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alClient
          ColumnCollection = <
            item
              Value = 50.000000000000000000
            end
            item
              Value = 50.000000000000000000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = GroupBox2
              Row = 0
            end
            item
              Column = 1
              Control = GroupBox3
              Row = 0
            end>
          RowCollection = <
            item
              Value = 100.000000000000000000
            end>
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 1
            Top = 1
            Width = 403
            Height = 432
            Align = alClient
            Caption = 'Telefones que ser'#227'o atualizados'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGreen
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            object LaTelAtz: TLabel
              Left = 2
              Top = 363
              Width = 5
              Height = 13
              Align = alBottom
            end
            object dmkDBGTelAtz: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 399
              Height = 348
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Align = alClient
              DataSource = DsTelAtz
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
              ParentFont = False
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clGreen
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = [fsBold]
              RowColors = <>
              OnDblClick = dmkDBGTelAtzDblClick
              FieldsCalcToOrder.Strings = (
                'TelefoneN_TXT=TelefoneN'
                'Telefone_TXT=Telefone')
              Columns = <
                item
                  Expanded = False
                  FieldName = 'TelefoneN_TXT'
                  Title.Caption = 'Telefone Atualizado'
                  Title.Font.Charset = ANSI_CHARSET
                  Title.Font.Color = clWindowText
                  Title.Font.Height = -14
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = [fsBold, fsItalic]
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Telefone_TXT'
                  Title.Caption = 'Telefone'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Raz'#227'o Social / Nome'
                  Width = 300
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Visible = True
                end>
            end
            object Panel5: TPanel
              Left = 2
              Top = 376
              Width = 399
              Height = 55
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object BtAtzAltera: TBitBtn
                Tag = 11
                Left = 12
                Top = 3
                Width = 120
                Height = 40
                Caption = '&Altera'
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtAtzAlteraClick
              end
              object BtAtzRemove: TBitBtn
                Tag = 12
                Left = 136
                Top = 3
                Width = 119
                Height = 40
                Caption = '&Remove sel.'
                NumGlyphs = 2
                TabOrder = 1
                OnClick = BtAtzRemoveClick
              end
            end
          end
          object GroupBox3: TGroupBox
            Left = 404
            Top = 1
            Width = 404
            Height = 432
            Align = alClient
            Caption = 'Telefones que N'#195'O ser'#227'o atualizados'
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            object LaNTelAtz: TLabel
              Left = 2
              Top = 363
              Width = 5
              Height = 13
              Align = alBottom
            end
            object dmkDBGNTelAtz: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 400
              Height = 348
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Align = alClient
              DataSource = DsNTelAtz
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              ParentFont = False
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clRed
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = [fsBold]
              RowColors = <>
              OnDblClick = dmkDBGNTelAtzDblClick
              FieldsCalcToOrder.Strings = (
                'Telefone_TXT=Telefone')
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Telefone_TXT'
                  Title.Caption = 'Telefone'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Raz'#227'o Social / Nome'
                  Width = 300
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Visible = True
                end>
            end
            object Panel6: TPanel
              Left = 2
              Top = 376
              Width = 400
              Height = 55
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object BtNAtzAltera: TBitBtn
                Tag = 11
                Left = 12
                Top = 3
                Width = 120
                Height = 40
                Caption = '&Altera'
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtNAtzAlteraClick
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label112: TLabel
        Left = 315
        Top = 7
        Width = 45
        Height = 13
        Caption = 'Telefone:'
        Visible = False
      end
      object BtPesquisa: TBitBtn
        Tag = 22
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object EdTe1: TdmkEdit
        Left = 315
        Top = 22
        Width = 112
        Height = 21
        TabOrder = 1
        Visible = False
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'PTe1'
        UpdCampo = 'PTe1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object BtSalvar: TBitBtn
        Tag = 14
        Left = 138
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Salvar'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtSalvarClick
      end
      object Button1: TButton
        Left = 433
        Top = 22
        Width = 61
        Height = 21
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Button1'
        TabOrder = 3
        Visible = False
        OnClick = Button1Click
      end
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 498
    Width = 812
    Height = 17
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alBottom
    TabOrder = 4
  end
  object QrNTelAtz: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNTelAtzBeforeClose
    AfterScroll = QrNTelAtzAfterScroll
    OnCalcFields = QrNTelAtzCalcFields
    SQL.Strings = (
      'SELECT ent.*, car.Nome NOMECARTPREF, cna.Nome CNAE_Nome,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'ENTIDADE,'
      
        'CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial ELSE acm.Nome END NOME' +
        'ACCOUNT,'
      
        'eng.Nome NOMEENTIGRUPO, mot.Descricao NOMEMOTIVO, emp.RazaoSocia' +
        'l NOMEEMPRESA,'
      
        'euf.Nome NOMEEUF, puf.nome NOMEPUF, cuf.Nome NOMECUF, luf.nome N' +
        'OMELUF, '
      'nuf.Nome NOMENUF, elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,'
      'clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD, lec.Nome NOMEECIVIL,'
      'mue.Nome NO_DTB_EMUNICI, mup.Nome NO_DTB_PMUNICI,'
      'muc.Nome NO_DTB_CMUNICI, mul.Nome NO_DTB_LMUNICI,'
      'pae.Nome NO_BACEN_EPAIS, pap.Nome NO_BACEN_PPAIS,'
      'ptt.Nome PTe1Tip_TXT, pct.Nome PCelTip_TXT, '
      'ett.Nome ETe1Tip_TXT, ect.Nome ECelTip_TXT'
      'FROM entidades ent'
      'LEFT JOIN entigrupos eng  ON eng.Codigo=ent.Grupo'
      'LEFT JOIN entidades acm   ON acm.Codigo=ent.Account'
      'LEFT JOIN entidades emp   ON emp.Codigo=ent.Empresa'
      'LEFT JOIN motivose mot    ON mot.Codigo=ent.Motivo'
      'LEFT JOIN ufs euf         ON euf.Codigo=ent.EUF'
      'LEFT JOIN ufs puf         ON puf.Codigo=ent.PUF'
      'LEFT JOIN ufs cuf         ON cuf.Codigo=ent.CUF'
      'LEFT JOIN ufs luf         ON luf.Codigo=ent.LUF'
      'LEFT JOIN ufs nuf         ON nuf.Codigo=ent.UFNatal'
      'LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd'
      'LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd'
      'LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd'
      'LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd'
      'LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil'
      'LEFT JOIN carteiras   car ON car.Codigo=ent.CartPref'
      'LEFT JOIN dtb_munici mue ON mue.Codigo=ent.ECodMunici'
      'LEFT JOIN dtb_munici mup ON mup.Codigo=ent.PCodMunici'
      'LEFT JOIN dtb_munici muc ON muc.Codigo=ent.CCodMunici'
      'LEFT JOIN dtb_munici mul ON mul.Codigo=ent.LCodMunici'
      'LEFT JOIN bacen_pais pae ON pae.Codigo=ent.ECodiPais'
      'LEFT JOIN bacen_pais pap ON pap.Codigo=ent.PCodiPais'
      'LEFT JOIN cnae21Cad cna ON cna.CodAlf=ent.CNAE'
      'LEFT JOIN entitipcto ptt  ON ptt.Codigo=ent.PTe1Tip'
      'LEFT JOIN entitipcto pct  ON pct.Codigo=ent.PCelTip'
      'LEFT JOIN entitipcto ett  ON ett.Codigo=ent.ETe1Tip'
      'LEFT JOIN entitipcto ect  ON ect.Codigo=ent.ECelTip'
      'WHERE ent.Codigo>-2')
    Left = 696
    Top = 240
    object QrNTelAtzCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNTelAtzConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNTelAtzNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrNTelAtzTelefone: TWideStringField
      FieldName = 'Telefone'
    end
    object QrNTelAtzTelefone_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Telefone_TXT'
      Size = 40
      Calculated = True
    end
    object QrNTelAtzEhCelular: TSmallintField
      FieldName = 'EhCelular'
    end
    object QrNTelAtzAtzCadas: TSmallintField
      FieldName = 'AtzCadas'
    end
    object QrNTelAtzTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object DsNTelAtz: TDataSource
    DataSet = QrNTelAtz
    Left = 724
    Top = 240
  end
  object QrTelAtz: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTelAtzBeforeClose
    AfterScroll = QrTelAtzAfterScroll
    OnCalcFields = QrTelAtzCalcFields
    SQL.Strings = (
      'SELECT ent.*, car.Nome NOMECARTPREF, cna.Nome CNAE_Nome,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'ENTIDADE,'
      
        'CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial ELSE acm.Nome END NOME' +
        'ACCOUNT,'
      
        'eng.Nome NOMEENTIGRUPO, mot.Descricao NOMEMOTIVO, emp.RazaoSocia' +
        'l NOMEEMPRESA,'
      
        'euf.Nome NOMEEUF, puf.nome NOMEPUF, cuf.Nome NOMECUF, luf.nome N' +
        'OMELUF, '
      'nuf.Nome NOMENUF, elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,'
      'clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD, lec.Nome NOMEECIVIL,'
      'mue.Nome NO_DTB_EMUNICI, mup.Nome NO_DTB_PMUNICI,'
      'muc.Nome NO_DTB_CMUNICI, mul.Nome NO_DTB_LMUNICI,'
      'pae.Nome NO_BACEN_EPAIS, pap.Nome NO_BACEN_PPAIS,'
      'ptt.Nome PTe1Tip_TXT, pct.Nome PCelTip_TXT, '
      'ett.Nome ETe1Tip_TXT, ect.Nome ECelTip_TXT'
      'FROM entidades ent'
      'LEFT JOIN entigrupos eng  ON eng.Codigo=ent.Grupo'
      'LEFT JOIN entidades acm   ON acm.Codigo=ent.Account'
      'LEFT JOIN entidades emp   ON emp.Codigo=ent.Empresa'
      'LEFT JOIN motivose mot    ON mot.Codigo=ent.Motivo'
      'LEFT JOIN ufs euf         ON euf.Codigo=ent.EUF'
      'LEFT JOIN ufs puf         ON puf.Codigo=ent.PUF'
      'LEFT JOIN ufs cuf         ON cuf.Codigo=ent.CUF'
      'LEFT JOIN ufs luf         ON luf.Codigo=ent.LUF'
      'LEFT JOIN ufs nuf         ON nuf.Codigo=ent.UFNatal'
      'LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd'
      'LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd'
      'LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd'
      'LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd'
      'LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil'
      'LEFT JOIN carteiras   car ON car.Codigo=ent.CartPref'
      'LEFT JOIN dtb_munici mue ON mue.Codigo=ent.ECodMunici'
      'LEFT JOIN dtb_munici mup ON mup.Codigo=ent.PCodMunici'
      'LEFT JOIN dtb_munici muc ON muc.Codigo=ent.CCodMunici'
      'LEFT JOIN dtb_munici mul ON mul.Codigo=ent.LCodMunici'
      'LEFT JOIN bacen_pais pae ON pae.Codigo=ent.ECodiPais'
      'LEFT JOIN bacen_pais pap ON pap.Codigo=ent.PCodiPais'
      'LEFT JOIN cnae21Cad cna ON cna.CodAlf=ent.CNAE'
      'LEFT JOIN entitipcto ptt  ON ptt.Codigo=ent.PTe1Tip'
      'LEFT JOIN entitipcto pct  ON pct.Codigo=ent.PCelTip'
      'LEFT JOIN entitipcto ett  ON ett.Codigo=ent.ETe1Tip'
      'LEFT JOIN entitipcto ect  ON ect.Codigo=ent.ECelTip'
      'WHERE ent.Codigo>-2')
    Left = 224
    Top = 176
    object QrTelAtzCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTelAtzConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrTelAtzNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrTelAtzTelefone: TWideStringField
      FieldName = 'Telefone'
    end
    object QrTelAtzTelefoneN: TWideStringField
      FieldName = 'TelefoneN'
    end
    object QrTelAtzTelefone_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Telefone_TXT'
      Size = 40
      Calculated = True
    end
    object QrTelAtzTelefoneN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TelefoneN_TXT'
      Size = 40
      Calculated = True
    end
    object QrTelAtzEhCelular: TSmallintField
      FieldName = 'EhCelular'
    end
    object QrTelAtzAtzCadas: TSmallintField
      FieldName = 'AtzCadas'
    end
    object QrTelAtzTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object DsTelAtz: TDataSource
    DataSet = QrTelAtz
    Left = 252
    Top = 176
  end
  object QrTel: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.*, car.Nome NOMECARTPREF, cna.Nome CNAE_Nome,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'ENTIDADE,'
      
        'CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial ELSE acm.Nome END NOME' +
        'ACCOUNT,'
      
        'eng.Nome NOMEENTIGRUPO, mot.Descricao NOMEMOTIVO, emp.RazaoSocia' +
        'l NOMEEMPRESA,'
      
        'euf.Nome NOMEEUF, puf.nome NOMEPUF, cuf.Nome NOMECUF, luf.nome N' +
        'OMELUF, '
      'nuf.Nome NOMENUF, elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,'
      'clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD, lec.Nome NOMEECIVIL,'
      'mue.Nome NO_DTB_EMUNICI, mup.Nome NO_DTB_PMUNICI,'
      'muc.Nome NO_DTB_CMUNICI, mul.Nome NO_DTB_LMUNICI,'
      'pae.Nome NO_BACEN_EPAIS, pap.Nome NO_BACEN_PPAIS,'
      'ptt.Nome PTe1Tip_TXT, pct.Nome PCelTip_TXT, '
      'ett.Nome ETe1Tip_TXT, ect.Nome ECelTip_TXT'
      'FROM entidades ent'
      'LEFT JOIN entigrupos eng  ON eng.Codigo=ent.Grupo'
      'LEFT JOIN entidades acm   ON acm.Codigo=ent.Account'
      'LEFT JOIN entidades emp   ON emp.Codigo=ent.Empresa'
      'LEFT JOIN motivose mot    ON mot.Codigo=ent.Motivo'
      'LEFT JOIN ufs euf         ON euf.Codigo=ent.EUF'
      'LEFT JOIN ufs puf         ON puf.Codigo=ent.PUF'
      'LEFT JOIN ufs cuf         ON cuf.Codigo=ent.CUF'
      'LEFT JOIN ufs luf         ON luf.Codigo=ent.LUF'
      'LEFT JOIN ufs nuf         ON nuf.Codigo=ent.UFNatal'
      'LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd'
      'LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd'
      'LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd'
      'LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd'
      'LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil'
      'LEFT JOIN carteiras   car ON car.Codigo=ent.CartPref'
      'LEFT JOIN dtb_munici mue ON mue.Codigo=ent.ECodMunici'
      'LEFT JOIN dtb_munici mup ON mup.Codigo=ent.PCodMunici'
      'LEFT JOIN dtb_munici muc ON muc.Codigo=ent.CCodMunici'
      'LEFT JOIN dtb_munici mul ON mul.Codigo=ent.LCodMunici'
      'LEFT JOIN bacen_pais pae ON pae.Codigo=ent.ECodiPais'
      'LEFT JOIN bacen_pais pap ON pap.Codigo=ent.PCodiPais'
      'LEFT JOIN cnae21Cad cna ON cna.CodAlf=ent.CNAE'
      'LEFT JOIN entitipcto ptt  ON ptt.Codigo=ent.PTe1Tip'
      'LEFT JOIN entitipcto pct  ON pct.Codigo=ent.PCelTip'
      'LEFT JOIN entitipcto ett  ON ett.Codigo=ent.ETe1Tip'
      'LEFT JOIN entitipcto ect  ON ect.Codigo=ent.ECelTip'
      'WHERE ent.Codigo>-2')
    Left = 400
    Top = 360
  end
  object PMTelAtz: TPopupMenu
    OnPopup = PMTelAtzPopup
    Left = 152
    Top = 296
    object Altera2: TMenuItem
      Caption = '&Altera'
      OnClick = Altera2Click
    end
    object Excluiselecionado1: TMenuItem
      Caption = '&Exclui selecionado'
      OnClick = Excluiselecionado1Click
    end
  end
  object PMNTelAtz: TPopupMenu
    OnPopup = PMNTelAtzPopup
    Left = 568
    Top = 304
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
  end
end
