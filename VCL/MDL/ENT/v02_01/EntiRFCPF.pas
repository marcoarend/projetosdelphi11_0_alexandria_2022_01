unit EntiRFCPF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, OleCtrls, SHDocVw, ActiveX, JPEG, ShellAPI, MSHTML, Variants,
  UnDmkEnums;

type
  TFmEntiRFCPF = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    ProgressBar1: TProgressBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    WebBrowser: TWebBrowser;
    MeDados: TMemo;
    MeCodigo: TMemo;
    BtImporta: TBitBtn;
    Panel_Cadastro: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    EdNome: TEdit;
    EdCPF: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure WebBrowserProgressChange(ASender: TObject; Progress,
      ProgressMax: Integer);
    procedure BtImportaClick(Sender: TObject);
    procedure WebBrowserDocumentComplete(ASender: TObject;
      const pDisp: IDispatch; const URL: OleVariant);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    HTMLDoc : IHTMLDocument2;
  public
    { Public declarations }
    FNome, FCPF: String;
  end;

  var
    FmEntiRFCPF: TFmEntiRFCPF;
  const
    CO_URL = 'https://www.receita.fazenda.gov.br/Aplicacoes/SSL/ATCTA/CPF/ConsultaSituacao/ConsultaPublica.asp';

implementation

uses UnMyObjects, Module, ModuleGeral;

{$R *.DFM}

procedure TFmEntiRFCPF.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiRFCPF.BtImportaClick(Sender: TObject);
begin
  if Length(EdCPF.Text) > 0 then
    FCPF := EdCPF.Text
  else
    FCPF := '';
  //
  if Length(EdNome.Text) > 0 then
    FNome := EdNome.Text
  else
    FNome := '';
  //
  Close;
end;

procedure TFmEntiRFCPF.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmEntiRFCPF.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiRFCPF.FormShow(Sender: TObject);
var
  URLCompl: String;
begin
  PageControl1.ActivePageIndex := 0;
  TabSheet2.TabVisible         := False;
  //
  if FCPF <> '' then
    URLCompl := '?cpf=' + FCPF
  else
    URLCompl := '';
  //
  WebBrowser.Navigate(CO_URL + URLCompl);
end;

procedure TFmEntiRFCPF.WebBrowserDocumentComplete(ASender: TObject;
  const pDisp: IDispatch; const URL: OleVariant);
  procedure PreencheDados();
  var
    //y,
    i: Integer;
    //TxtLinha,
    Texto: String;
  begin
    MeDados.Lines.Text := WebBrowser.OleObject.Document.documentElement.innerText;
    for i := 0 to MeDados.Lines.Count - 1 do
    begin
      if Pos('No do CPF:', MeDados.Lines[i]) > 0 then
      begin
        Texto := StringReplace(MeDados.Lines[i], 'No do CPF:', '', [rfReplaceAll, rfIgnoreCase]);
        Texto := Trim(Texto);
        Texto := Geral.SoNumero_TT(Texto);
        //
        EdCPF.Text := Texto;
      end;
      if Pos('Nome:', MeDados.Lines[i]) > 0 then
      begin
        Texto := StringReplace(MeDados.Lines[i], 'Nome:', '', [rfReplaceAll, rfIgnoreCase]);
        Texto := Trim(Texto);
        //
        EdNome.Text := Texto;
      end;
    end;
  end;
  procedure PreencheCodigos();
  var
    Html : IHTMLElement;
  begin
    if Assigned(WebBrowser.Document) then
    begin
      Html := (WebBrowser.Document as IHTMLDocument2).body;
      while Html.parentElement <> Nil do
        Html := Html.parentElement;
      MeCodigo.Text := Html.outerHTML;
   End;
  end;
begin
  ProgressBar1.Position := 0;
  //
  if Assigned(WebBrowser.Document) then
  begin
    htmlDoc := WebBrowser.Document as IHTMLDocument2;
  end;
  //
  if Pos('consultapublicaexibir.asp', LowerCase(URL)) > 0 Then
     PreencheDados;
  PreencheCodigos;
end;

procedure TFmEntiRFCPF.WebBrowserProgressChange(ASender: TObject; Progress,
  ProgressMax: Integer);
begin
  if (Progress>=1) and (ProgressMax>1) then
  begin
    //Ele tira uma valor percentual para colocar no Progressbar
    ProgressBar1.Position := Round((Progress * 100) div ProgressMax);
    ProgressBar1.Visible  := True;
  end else
  begin
    ProgressBar1.Position := 1;
    ProgressBar1.Visible  := False;
  end;
end;

end.
