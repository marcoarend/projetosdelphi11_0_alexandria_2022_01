unit EntiDocs2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, Vcl.Menus, dmkDBGridZTO, jpeg, ShellApi;

type
  TFmEntiDocs2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrEntiDocs: TMySQLQuery;
    DsEntiDocs: TDataSource;
    QrEntiDocsCodigo: TIntegerField;
    QrEntiDocsNome: TWideStringField;
    QrEntiDocsCodEnti: TIntegerField;
    QrEntiDocsTipImg: TSmallintField;
    QrEntiDocsLk: TIntegerField;
    QrEntiDocsDataCad: TDateField;
    QrEntiDocsDataAlt: TDateField;
    QrEntiDocsUserCad: TIntegerField;
    QrEntiDocsUserAlt: TIntegerField;
    QrEntiDocsAlterWeb: TSmallintField;
    QrEntiDocsAWServerID: TIntegerField;
    QrEntiDocsAWStatSinc: TSmallintField;
    QrEntiDocsAtivo: TSmallintField;
    QrEntiDocsTIPO: TWideStringField;
    QrEntiDocsCHAVE: TIntegerField;
    QrEntiDocsSEQUENCIA: TIntegerField;
    QrEntiDocsDESCRICAO: TWideStringField;
    QrEntiDocsTIPOARQUIVO: TWideStringField;
    QrEntiDocsDATAATUALIZACAO: TDateTimeField;
    Panel11: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PMInclui: TPopupMenu;
    CarregaArquivoIns1: TMenuItem;
    WebCamIns1: TMenuItem;
    DBGEntiDocs: TdmkDBGridZTO;
    Splitter1: TSplitter;
    PMAltera: TPopupMenu;
    CarregaArquivoAlt1: TMenuItem;
    WebCamAlt1: TMenuItem;
    Scanner1: TMenuItem;
    AbrirScanner1: TMenuItem;
    Carregararquivo1: TMenuItem;
    Scanner2: TMenuItem;
    AbrirScanner2: TMenuItem;
    Carregararquivo2: TMenuItem;
    Descrio1: TMenuItem;
    BtArquivo: TBitBtn;
    PMArquivo: TPopupMenu;
    NoApporiginalseinstalado1: TMenuItem;
    N1: TMenuItem;
    AquiNavegadorembutido1: TMenuItem;
    N2: TMenuItem;
    Mostarnapasta1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WebBrowserDocumentComplete(ASender: TObject;
      const pDisp: IDispatch; const URL: OleVariant);
    procedure QrEntiDocsAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure QrEntiDocsBeforeClose(DataSet: TDataSet);
    procedure FormMouseActivate(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y, HitTest: Integer;
      var MouseActivate: TMouseActivate);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure QrEntiDocsAfterOpen(DataSet: TDataSet);
    procedure CarregaArquivoIns1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure WebCamIns1Click(Sender: TObject);
    procedure DBGEntiDocsDblClick(Sender: TObject);
    procedure CarregaArquivoAlt1Click(Sender: TObject);
    procedure WebCamAlt1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure AbrirScanner1Click(Sender: TObject);
    procedure Carregararquivo1Click(Sender: TObject);
    procedure AbrirScanner2Click(Sender: TObject);
    procedure Carregararquivo2Click(Sender: TObject);
    procedure Descrio1Click(Sender: TObject);
    procedure NoApporiginalseinstalado1Click(Sender: TObject);
    procedure AquiNavegadorembutido1Click(Sender: TObject);
    procedure Mostarnapasta1Click(Sender: TObject);
    procedure BtArquivoClick(Sender: TObject);
  private
    { Private declarations }
    FNaoMostrar: Boolean;
    FWebBrowser: TWebBrowser;
    //
    function  ObtemProximaSEQUENCIA(): Integer;
    function  ObtemScanPathRegEdit(SubReg: String): String;
    procedure SalvaScanPathRegEdit(SubReg, Caminho: String);
  public
    { Public declarations }
    FCaminho: String;
    FTIPO, FNO_CHAVE: String;
    FCHAVE: Integer;
    //
    function  ArquivoAtual(var Arquivo: String): Boolean;
    procedure CarregaArquivoQualquer(SQLType: TSQLType; DirIni: String);
    procedure CarregarArquivoDoScanner(SQLType: TSQLType);
    procedure ChamarScanner();
    procedure DestriWebBrowser();
    procedure MostraDocumento();
    procedure ObtemFoto(SQLType: TSQLType);
    procedure ReopenEntiDocs();
    procedure RecriaWebBrowser();
  end;

  var
  FmEntiDocs2: TFmEntiDocs2;

implementation

uses UnMyObjects, DmkDAC_PF, Module, ModuleGeral, UnEntities, UMySQLModule,
  MyDBCheck, ObtemFoto4;

{$R *.DFM}

procedure TFmEntiDocs2.AbrirScanner1Click(Sender: TObject);
begin
  ChamarScanner();
end;

procedure TFmEntiDocs2.AbrirScanner2Click(Sender: TObject);
begin
  ChamarScanner();
end;

procedure TFmEntiDocs2.AquiNavegadorembutido1Click(Sender: TObject);
begin
  MostraDocumento();
end;

function TFmEntiDocs2.ArquivoAtual(var Arquivo: String): Boolean;
begin
  Result := False;
  if (QrEntiDocs.State <> dsInactive) and (QrEntiDocs.RecordCount > 0 ) then
  begin
    Arquivo := Geral.Substitui(FCaminho + '\' + QrEntiDocsNome.Value, '/', '\');
    Result := FileExists(Arquivo);
  end;
end;

procedure TFmEntiDocs2.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmEntiDocs2.BtArquivoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArquivo, BtArquivo);
end;

procedure TFmEntiDocs2.BtExcluiClick(Sender: TObject);
var
  Arquivo: String;
  Codigo, I: Integer;
  Excluiu: Boolean;
begin
  if Geral.MB_Pergunta('Deseja realmente excluir o registro e o arquivo?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      if ArquivoAtual(Arquivo) then
      begin
        DestriWebBrowser();
        DeleteFile(Arquivo);
        //Excluiu := False;
        for I := 1 to 100 do
        begin
          Excluiu := not FileExists(Arquivo);
          if Excluiu then
            Break
           else
           begin
             Sleep(100);
             DeleteFile(Arquivo);
           end;
        end;
        if not Excluiu then
        begin
          Geral.MB_Erro('N�o foi poss�vel excluir o arquivo"' + Arquivo + '"');
          Exit;
        end else
        begin
          Codigo := QrEntiDocsCodigo.Value;
          if UMyMod.ExcluiRegistroInt1('', 'entiimgs', 'Codigo', Codigo,
          Dmod.MyDB) = ID_YES then
          begin
            //FNaoMostrar := True;
            ReopenEntiDocs();
            //FNaoMostrar := False;
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmEntiDocs2.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmEntiDocs2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiDocs2.CarregaArquivoQualquer(SQLType: TSQLType; DirIni: String);
//const
  //TIPO = 'C'; // >> T i s o l i n .Cliente
var
  SEQUENCIA, Codigo: Integer;
begin
  DestriWebBrowser();
  if SQLType = stIns then
  begin
    SEQUENCIA := ObtemProximaSEQUENCIA();
    Codigo    := 0;
  end else
  begin
    SEQUENCIA := QrEntiDocsSEQUENCIA.Value;
    Codigo    := QrEntiDocsCodigo.Value;
  end;
  if Entities.MostraFormEntiLoad(SQLType, DirIni, FTIPO, FCHAVE, Codigo, SEQUENCIA,
  FNO_CHAVE, FCaminho, Codigo) then
  begin
    FNaoMostrar := True;
    ReopenEntiDocs();
    FNaoMostrar := False;
    QrEntiDocs.Locate('Codigo', Codigo, []);
    //MostraDocumento();
  end;
end;

procedure TFmEntiDocs2.CarregaArquivoAlt1Click(Sender: TObject);
begin
  CarregaArquivoQualquer(stUpd, '');
end;

procedure TFmEntiDocs2.CarregaArquivoIns1Click(Sender: TObject);
begin
  CarregaArquivoQualquer(stIns, '');
end;

procedure TFmEntiDocs2.Carregararquivo1Click(Sender: TObject);
begin
  CarregarArquivoDoScanner(stIns);
end;

procedure TFmEntiDocs2.Carregararquivo2Click(Sender: TObject);
begin
  CarregarArquivoDoScanner(stUpd);
end;

procedure TFmEntiDocs2.CarregarArquivoDoScanner(SQLType: TSQLType);
var
  Diretorio: String;
  Continua: Boolean;
begin
  Diretorio := ObtemScanPathRegEdit('Scanned_Dir');
  Continua := FileExists(Diretorio);
  if not Continua then
  begin
    if MyObjects.FileOpenDialog(Self, '', '', 'Pasta de Arquuivos escaneados', '*.*', [], Diretorio) then
    begin
      SalvaScanPathRegEdit('Scanned_Dir', Diretorio);
      Continua := FileExists(ExtractFileDir(Diretorio));
    end;
  end;
  if Continua then
    CarregaArquivoQualquer(SQLType, Diretorio);
end;

procedure TFmEntiDocs2.ChamarScanner();
var
  Arquivo: String;
  Continua: Boolean;
begin
  Arquivo := ObtemScanPathRegEdit('Scanner_Exe');
  Continua := FileExists(Arquivo);
  if not Continua then
  begin
    if MyObjects.FileOpenDialog(Self, '', '', 'Execut�vel Scanner', '*.exe', [], Arquivo) then
    begin
      SalvaScanPathRegEdit('Scanner_Exe', Arquivo);
      Continua := FileExists(Arquivo);
    end;
  end;
  if Continua then
    ShellExecute(Application.Handle, PChar('open'), PChar(Arquivo), PChar(''),
      nil, SW_NORMAL);
end;

procedure TFmEntiDocs2.DBGEntiDocsDblClick(Sender: TObject);
var
  Arquivo: String;
begin
  if ArquivoAtual(Arquivo) then
    ShellExecute(Application.Handle, PChar('open'), PChar(Arquivo), PChar(''),
      nil, SW_NORMAL);
end;

procedure TFmEntiDocs2.Descrio1Click(Sender: TObject);
var
  Descricao: String;
  Codigo: Integer;
begin
  if (QrEntiDocs.State <> dsInactive) and (QrEntiDocs.RecordCount > 0) then
  begin
    Descricao := QrEntiDocsDescricao.Value;
    if InputQuery('Altera��o de Descri��o', 'Informe a nova descri��o', Descricao) then
    begin
      Codigo := QrEntiDocsCodigo.Value;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entiimgs', False, [
      'DESCRICAO'], [
      'Codigo'], [
      DESCRICAO], [
      Codigo], True) then
      begin
        ReopenEntiDocs();
        QrEntiDocs.Locate('Codigo', Codigo, []);
      end;
    end;
  end;
end;

procedure TFmEntiDocs2.DestriWebBrowser();
var
  Teste: TComponent;
begin
  if(FWebBrowser <> nil) then
  begin
    try
       Teste := FindComponent('WebBrowser');
       if Teste <> nil then
          FWebBrowser.Stop;
    except
      //
    end;
    try
      FWebBrowser.Destroy;
      //FWebBrowser.Free;
      FWebBrowser := nil;
    except
      //
    end;
  end;
end;

procedure TFmEntiDocs2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiDocs2.FormCreate(Sender: TObject);
begin
  FNaoMostrar := True;
  ImgTipo.SQLType := stLok;
end;

procedure TFmEntiDocs2.FormMouseActivate(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y, HitTest: Integer;
  var MouseActivate: TMouseActivate);
begin
  if FWebBrowser <> nil then
  begin
    if (Button= mbRight)
    or (Button= mbLeft) then
    begin
      if (x >= FWebBrowser.Left) and
         (x <= FWebBrowser.Left + FWebBrowser.Width ) and
         (y >= FWebBrowser.Top) and
         (y <= FWebBrowser.Top + FWebBrowser.Height ) then
        MouseActivate := maNoActivateAndEat;
    end;
  end;
end;

procedure TFmEntiDocs2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiDocs2.FormShow(Sender: TObject);
begin
  if FNaoMostrar then
  begin
    FNaoMostrar := False;
{
    if (QrEntiDocs.State <> dsInactive) and
    (QrEntiDocs.RecordCount > 0) then
      MostraDocumento();
}
  end;
end;

procedure TFmEntiDocs2.Mostarnapasta1Click(Sender: TObject);
var
  DestFile: String;
begin
  DestFile := FCaminho + '\' + QrEntiDocsNome.Value;
  //
  if FileExists(DestFile) then
//    ShellExecute(Application.Handle, PChar('open'), PChar(DestFile),
//     PChar(''), nil, SW_NORMAL)
    ShellExecute(0,
           'open',
            PChar('explorer.exe'),
            PChar('/n, /select,' + DestFile),
            nil,  // Se nao funcionar, precisa por o caminho do Windows no par�metro Directory >> PChar('C:\WINDOWS\'),
            SW_SHOWMAXIMIZED)
    else
     Geral.MB_Aviso('O Arquivo "' + DestFile + '" n�o foi encontrado');
end;

procedure TFmEntiDocs2.MostraDocumento();
var
  Arquivo, URL: String;
begin
  //if FNaoMostrar then Exit;
  //
  //Arquivo := 'file:///' + '\\novaServ/Dermatek/Tiso lin/ImagensGerenciaFacil/pessoa/' + QrEntiDocsNome.Value;
  try
    if ArquivoAtual(Arquivo) then
    begin
      URL := 'file:///' + Arquivo;
      RecriaWebBrowser();
      FWebBrowser.Navigate(URL);
    end;
  except
    Geral.MB_Info('N�o foi poss�vel mostrar o arquivo no navegador embutido!');
  end;
end;

procedure TFmEntiDocs2.NoApporiginalseinstalado1Click(Sender: TObject);
var
  DestFile: String;
begin
  DestFile :=   FCaminho + '\' + QrEntiDocsNome.Value;
  //
  if FileExists(DestFile) then
    ShellExecute(Application.Handle, PChar('open'), PChar(DestFile),
     PChar(''), nil, SW_NORMAL)
   else
     Geral.MB_Aviso('O Arquivo "' + DestFile + '" n�o foi encontrado');
end;

function TFmEntiDocs2.ObtemProximaSEQUENCIA(): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT MAX(SEQUENCIA) SEQUENCIA ',
  'FROM entiimgs ',
  'WHERE CHAVE=' + Geral.FF0(FChave),
  '']);
  Result := Dmod.QrAux.FieldByName('SEQUENCIA').AsInteger + 1;
end;

function TFmEntiDocs2.ObtemScanPathRegEdit(SubReg: String): String;
var
  Cam: String;
begin
  Cam := Application.Title + '\' + SubReg + '\';
  Result := Geral.ReadAppKeyLM('Caminho', Cam, TKeyType.ktString, '');
end;

procedure TFmEntiDocs2.QrEntiDocsAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrEntiDocs.RecordCount > 0;
  SpeedButton1.Enabled := Habilita;
  SpeedButton2.Enabled := Habilita;
  SpeedButton3.Enabled := Habilita;
  SpeedButton4.Enabled := Habilita;
  //
  BtAltera.Enabled := Habilita;
  BtExclui.Enabled := Habilita;
  BtArquivo.Enabled := Habilita;
end;

procedure TFmEntiDocs2.QrEntiDocsAfterScroll(DataSet: TDataSet);
begin
  //MostraDocumento();
end;

procedure TFmEntiDocs2.QrEntiDocsBeforeClose(DataSet: TDataSet);
begin
  SpeedButton1.Enabled := False;
  SpeedButton2.Enabled := False;
  SpeedButton3.Enabled := False;
  SpeedButton4.Enabled := False;
  //
  BtAltera.Enabled := False;
  BtExclui.Enabled := False;
  //
  DestriWebBrowser();
end;

procedure TFmEntiDocs2.RecriaWebBrowser();
begin
  DestriWebBrowser();

  FWebBrowser := TWebBrowser.Create(FmEntiDocs2);
  TWinControl(FWebBrowser).Name   := 'WebBrowser';
  // Erro!
  //TWinControl(FWebBrowser).Parent := FmEntiDocs2; //set parent...can be panel, tabs etc
  TWinControl(FWebBrowser).Parent := Panel3;
  FWebBrowser.Silent := True;  //don't show JS errors
  FWebBrowser.Visible:= True;  //visible...by default true

  FWebBrowser.Align := alClient;
  FWebBrowser.Offline := True;
  //don't set width/heigh/top/left before TWinControl(FWebBrowser).Parent := Form1;
{
  FWebBrowser.Top    := 10;
  FWebBrowser.Left   := 10;
  FWebBrowser.Height := 600;
  FWebBrowser.Width  := 800;
}
  //
  FWebBrowser.OnDocumentComplete := WebBrowserDocumentComplete;

////////////////////////////////////////////////////////////////////////////////

{
  if(FWebBrowser = nil) then
  begin
    FWebBrowser        := TWebBrowser.Create(FmEntiDocs2);
    TWinControl(FWebBrowser).Name   := 'WebBrowser';
    TWinControl(FWebBrowser).Parent := Panel3; //set parent...can be panel, tabs etc
    FWebBrowser.Silent := true;  //don't show JS errors
    FWebBrowser.Visible:= true;  //visible...by default true

    //don't set width/heigh/top/left before TWinControl(FWebBrowser).Parent := Form1;
    FWebBrowser.Align := alClient;
    FWebBrowser.OnDocumentComplete  := WebBrowserDocumentComplete;
  end;
}
end;

procedure TFmEntiDocs2.ReopenEntiDocs();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiDocs, Dmod.MyDB, [
  'SELECT * ',
  'FROM entiimgs ',
  'WHERE Tipo="' + FTIPO + '" ',
  'AND Chave=' + Geral.FF0(FChave),
  'ORDER BY SEQUENCIA, Codigo  ',
  '']);
end;

procedure TFmEntiDocs2.SalvaScanPathRegEdit(SubReg, Caminho: String);
var
  Cam: String;
begin
  Cam := Application.Title + '\' + SubReg + '\';
  Geral.WriteAppKeyLM('Caminho', Cam, Caminho, TKeyType.ktString);
end;

procedure TFmEntiDocs2.SpeedButton1Click(Sender: TObject);
begin
  QrEntiDocs.First;
end;

procedure TFmEntiDocs2.SpeedButton2Click(Sender: TObject);
begin
  QrEntiDocs.Prior;
end;

procedure TFmEntiDocs2.SpeedButton3Click(Sender: TObject);
begin
  QrEntiDocs.Next;
end;

procedure TFmEntiDocs2.SpeedButton4Click(Sender: TObject);
begin
  QrEntiDocs.Last;
end;

procedure TFmEntiDocs2.WebBrowserDocumentComplete(ASender: TObject;
  const pDisp: IDispatch; const URL: OleVariant);
begin
  //ShowMessage('WebBrowser1DocumentComplete()');
end;

procedure TFmEntiDocs2.WebCamAlt1Click(Sender: TObject);
begin
  ObtemFoto(stUpd);
end;

procedure TFmEntiDocs2.WebCamIns1Click(Sender: TObject);
begin
  ObtemFoto(stIns);
end;

procedure TFmEntiDocs2.ObtemFoto(SQLType: TSQLType);
var
  Foto: TJPEGImage;
  ExcluiuOuNaoExiste: Boolean;
  Codigo: Integer;
  //
  procedure SalvaFoto();
  var
    Nome, TIPO, DESCRICAO, TIPOARQUIVO, DATAATUALIZACAO, FileExt, NovoArquivo: String;
    I, CodEnti, TipImg, CHAVE, SEQUENCIA: Integer;
  begin
    //
    if SQLType = stIns then
    begin
      Codigo          := 0;
      SEQUENCIA       := ObtemProximaSEQUENCIA();
    end else begin
      Codigo          := QrEntiDocsCodigo.Value;
      SEQUENCIA       := QrEntiDocsSEQUENCIA.Value;
    end;
    FileExt         := '.JPEG';
    Nome            := Geral.FFN(FCHAVE, 7) + '-' + Geral.FFN(SEQUENCIA, 2) + FileExt;
    CodEnti         := 0;
    TipImg          := -1;
    TIPO            := FTIPO; // 'C' => Entidade (T i s o l i n .Cliente)
    CHAVE           := FCHAVE;
    DESCRICAO       := 'FOTO';
    TIPOARQUIVO     := FileExt;
    DATAATUALIZACAO := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    NovoArquivo     := FCaminho + '\' + Nome;
    for I := 1 to 100 do
    begin
      if FileExists(NovoArquivo) then
      begin
        DeleteFile(NovoArquivo);
        sleep(100);
      end
      else
      begin
        ExcluiuOuNaoExiste := True;
      end;
    end;
    if ExcluiuOuNaoExiste then
    begin
      Foto.SaveToFile(NovoArquivo);
      //if FileExists(NovoArquivo) then
      begin
        Codigo := UMyMod.BPGS1I32('entiimgs', 'Codigo', '', '', tsPos, SQLType, Codigo);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entiimgs', False, [
        'Nome', 'CodEnti', 'TipImg',
        'TIPO', 'CHAVE', 'SEQUENCIA',
        'DESCRICAO', 'TIPOARQUIVO', 'DATAATUALIZACAO'], [
        'Codigo'], [
        Nome, CodEnti, TipImg,
        TIPO, CHAVE, SEQUENCIA,
        DESCRICAO, TIPOARQUIVO, DATAATUALIZACAO], [
        Codigo], True) then
        begin
        //
        end;
      end;
    end else
      Geral.MB_Erro('N�o foi poss�vel excluir o arquivo anterior: ' +
      NovoArquivo);
  end;
begin
  Foto := TJPEGImage.Create;
  //
  if DBCheck.CriaFm(TFmObtemFoto4, FmObtemFoto4, afmoNegarComAviso) then
  begin
    FmObtemFoto4.ShowModal;
    //
    Screen.Cursor := crHourGlass;
    try
      if FmObtemFoto4.FJPGImage <> nil then
      begin
        Foto.Assign(FmObtemFoto4.FJPGImage);
        //
        SalvaFoto();
      end;
      FmObtemFoto4.Destroy;
      //
      FNaoMostrar := True;
      ReopenEntiDocs();
      FNaoMostrar := False;
      QrEntiDocs.Locate('Codigo', Codigo, []);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
