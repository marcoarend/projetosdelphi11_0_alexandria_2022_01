object FmEntiRapido0: TFmEntiRapido0
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-018 :: Cadastro R'#225'pido de Entidade [A]'
  ClientHeight = 466
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 400
        Height = 32
        Caption = 'Cadastro R'#225'pido de Entidade [A]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 400
        Height = 32
        Caption = 'Cadastro R'#225'pido de Entidade [A]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 400
        Height = 32
        Caption = 'Cadastro R'#225'pido de Entidade [A]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 304
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 304
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 304
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 66
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object RGTipo: TRadioGroup
            Left = 0
            Top = 0
            Width = 84
            Height = 66
            Align = alLeft
            Caption = ' Cadastro: '
            ItemIndex = 0
            Items.Strings = (
              '?'
              'Empresa'
              'Pessoal')
            TabOrder = 0
            OnClick = RGTipoClick
          end
          object PnDados1: TPanel
            Left = 84
            Top = 0
            Width = 696
            Height = 66
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            Visible = False
            object Label25: TLabel
              Left = 8
              Top = 16
              Width = 31
              Height = 13
              Caption = 'Nome:'
            end
            object Label30: TLabel
              Left = 476
              Top = 16
              Width = 46
              Height = 13
              Caption = 'I.E. / RG:'
            end
            object Label29: TLabel
              Left = 360
              Top = 16
              Width = 61
              Height = 13
              Caption = 'CNPJ / CPF:'
            end
            object Label12: TLabel
              Left = 592
              Top = 16
              Width = 24
              Height = 13
              Caption = 'CEP:'
            end
            object EdCEP: TdmkEdit
              Left = 592
              Top = 32
              Width = 72
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtCEP
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'PCEP'
              UpdCampo = 'PCEP'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnEnter = EdCEPEnter
              OnExit = EdCEPExit
            end
            object BtCEP: TBitBtn
              Left = 668
              Top = 32
              Width = 21
              Height = 21
              Caption = '?'
              TabOrder = 4
              TabStop = False
              OnClick = BtCEPClick
            end
            object EdRazaoNome: TdmkEdit
              Left = 9
              Top = 32
              Width = 348
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Nome'
              UpdCampo = 'Nome'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdRazaoNomeChange
            end
            object EdCNPJCPF: TdmkEdit
              Left = 361
              Top = 32
              Width = 112
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtCPFJ
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'CPF'
              UpdCampo = 'CPF'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnExit = EdCNPJCPFExit
            end
            object EdIERG: TdmkEdit
              Left = 476
              Top = 32
              Width = 112
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'RG'
              UpdCampo = 'RG'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object PnDados2: TPanel
          Left = 2
          Top = 81
          Width = 780
          Height = 128
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Label97: TLabel
            Left = 8
            Top = 4
            Width = 77
            Height = 13
            Caption = 'Tipo logradouro:'
          end
          object Label31: TLabel
            Left = 172
            Top = 4
            Width = 99
            Height = 13
            Caption = 'Nome do logradouro:'
            FocusControl = EdRua
          end
          object Label32: TLabel
            Left = 444
            Top = 4
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
            FocusControl = EdNumero
          end
          object Label34: TLabel
            Left = 612
            Top = 4
            Width = 30
            Height = 13
            Caption = 'Bairro:'
            FocusControl = EdBairro
          end
          object Label35: TLabel
            Left = 488
            Top = 84
            Width = 98
            Height = 13
            Caption = 'Cidade (Texto Livre):'
            FocusControl = EdCidade
          end
          object Label38: TLabel
            Left = 408
            Top = 44
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label15: TLabel
            Left = 632
            Top = 84
            Width = 87
            Height = 13
            Caption = 'Pa'#237's (Texto Livre):'
          end
          object Label16: TLabel
            Left = 504
            Top = 4
            Width = 67
            Height = 13
            Caption = 'Complemento:'
            FocusControl = EdCompl
          end
          object Label17: TLabel
            Left = 8
            Top = 84
            Width = 165
            Height = 13
            Caption = 'Ponto de refer'#234'ncia (do endere'#231'o):'
            FocusControl = EdEndeRef
          end
          object Label106: TLabel
            Left = 8
            Top = 44
            Width = 36
            Height = 13
            Caption = 'Cidade:'
          end
          object Label109: TLabel
            Left = 440
            Top = 44
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
          end
          object Label112: TLabel
            Left = 660
            Top = 44
            Width = 83
            Height = 13
            Caption = 'Telefone 1 (NFe):'
          end
          object CBLograd: TdmkDBLookupComboBox
            Left = 48
            Top = 20
            Width = 120
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsListaLograd
            TabOrder = 1
            dmkEditCB = EdLograd
            QryCampo = 'PLograd'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdRua: TdmkEdit
            Left = 172
            Top = 20
            Width = 268
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PRua'
            UpdCampo = 'PRua'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdNumero: TdmkEdit
            Left = 444
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PNumero'
            UpdCampo = 'PNumero'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdBairro: TdmkEdit
            Left = 612
            Top = 20
            Width = 160
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PBairro'
            UpdCampo = 'PBairro'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCidade: TdmkEdit
            Left = 488
            Top = 100
            Width = 140
            Height = 21
            TabOrder = 13
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PCidade'
            UpdCampo = 'PCidade'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdUF: TdmkEdit
            Left = 408
            Top = 60
            Width = 28
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PUF'
            UpdCampo = 'PUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdPais: TdmkEdit
            Left = 632
            Top = 100
            Width = 140
            Height = 21
            TabOrder = 14
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PPais'
            UpdCampo = 'PPais'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLograd: TdmkEditCB
            Left = 8
            Top = 20
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PLograd'
            UpdCampo = 'PLograd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBLograd
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdCompl: TdmkEdit
            Left = 504
            Top = 20
            Width = 105
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PCompl'
            UpdCampo = 'PCompl'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEndeRef: TdmkEdit
            Left = 8
            Top = 100
            Width = 476
            Height = 21
            TabOrder = 12
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PEndeRef'
            UpdCampo = 'PEndeRef'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCodMunici: TdmkEditCB
            Left = 8
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PCodMunici'
            UpdCampo = 'PCodMunici'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCodMunici
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCodMunici: TdmkDBLookupComboBox
            Left = 64
            Top = 60
            Width = 341
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsMunici
            TabOrder = 7
            dmkEditCB = EdCodMunici
            QryCampo = 'PCodMunici'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCodiPais: TdmkEditCB
            Left = 440
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PCodiPais'
            UpdCampo = 'PCodiPais'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCodiPais
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCodiPais: TdmkDBLookupComboBox
            Left = 496
            Top = 60
            Width = 161
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsBacen_Pais
            TabOrder = 10
            dmkEditCB = EdCodiPais
            QryCampo = 'PCodiPais'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdTe1: TdmkEdit
            Left = 660
            Top = 60
            Width = 112
            Height = 21
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtTelLongo
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PTe1'
            UpdCampo = 'PTe1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object GroupBox4: TGroupBox
          Left = 2
          Top = 209
          Width = 780
          Height = 89
          Align = alTop
          Caption = ' Tipo de cadastro: '
          TabOrder = 2
          object CkCliente1_0: TdmkCheckBox
            Left = 8
            Top = 16
            Width = 152
            Height = 17
            Caption = 'Cliente1'
            TabOrder = 0
            QryCampo = 'Cliente1'
            UpdCampo = 'Cliente1'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkFornece1_0: TdmkCheckBox
            Left = 164
            Top = 16
            Width = 152
            Height = 17
            Caption = 'Fornece 1'
            TabOrder = 4
            QryCampo = 'Fornece1'
            UpdCampo = 'Fornece1'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkFornece2_0: TdmkCheckBox
            Left = 164
            Top = 32
            Width = 152
            Height = 17
            Caption = 'Fornece 2'
            TabOrder = 5
            Visible = False
            QryCampo = 'Fornece2'
            UpdCampo = 'Fornece2'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkFornece3_0: TdmkCheckBox
            Left = 164
            Top = 48
            Width = 152
            Height = 17
            Caption = 'Fornece 3'
            TabOrder = 6
            Visible = False
            QryCampo = 'Fornece3'
            UpdCampo = 'Fornece3'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkFornece4_0: TdmkCheckBox
            Left = 164
            Top = 64
            Width = 152
            Height = 17
            Caption = 'Fornece 4'
            TabOrder = 7
            Visible = False
            QryCampo = 'Fornece4'
            UpdCampo = 'Fornece4'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkTerceiro_0: TdmkCheckBox
            Left = 476
            Top = 16
            Width = 152
            Height = 17
            Caption = 'Terceiro'
            TabOrder = 12
            QryCampo = 'Terceiro'
            UpdCampo = 'Terceiro'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkCliente2_0: TdmkCheckBox
            Left = 8
            Top = 32
            Width = 152
            Height = 17
            Caption = 'Cliente 2'
            TabOrder = 1
            Visible = False
            QryCampo = 'Cliente2'
            UpdCampo = 'Cliente2'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkFornece5_0: TdmkCheckBox
            Left = 320
            Top = 16
            Width = 152
            Height = 17
            Caption = 'Fornece 5'
            TabOrder = 8
            Visible = False
            QryCampo = 'Fornece5'
            UpdCampo = 'Fornece5'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkFornece6_0: TdmkCheckBox
            Left = 320
            Top = 32
            Width = 152
            Height = 17
            Caption = 'Fornece 6'
            TabOrder = 9
            Visible = False
            QryCampo = 'Fornece6'
            UpdCampo = 'Fornece6'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkCliente4_0: TdmkCheckBox
            Left = 8
            Top = 64
            Width = 152
            Height = 17
            Caption = 'Cliente 4'
            TabOrder = 3
            Visible = False
            QryCampo = 'Cliente4'
            UpdCampo = 'Cliente4'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkCliente3_0: TdmkCheckBox
            Left = 8
            Top = 48
            Width = 152
            Height = 17
            Caption = 'Cliente 3'
            TabOrder = 2
            Visible = False
            QryCampo = 'Cliente3'
            UpdCampo = 'Cliente3'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkFornece8_0: TdmkCheckBox
            Left = 320
            Top = 64
            Width = 152
            Height = 17
            Caption = 'Fornece 8'
            TabOrder = 11
            Visible = False
            QryCampo = 'Fornece8'
            UpdCampo = 'Fornece8'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
          object CkFornece7_0: TdmkCheckBox
            Left = 320
            Top = 48
            Width = 152
            Height = 17
            Caption = 'Fornece 7'
            TabOrder = 10
            Visible = False
            QryCampo = 'Fornece7'
            UpdCampo = 'Fornece7'
            UpdType = utYes
            ValCheck = 'V'
            ValUncheck = 'F'
            OldValor = #0
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 352
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 396
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CNPJ_CPF'
      'FROM cadastro_com_itens_its'
      'WHERE CNPJ_CPF=:P0'
      'AND Codigo=:P1')
    Left = 17
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 15
    end
  end
  object PMCEP: TPopupMenu
    Left = 460
    Top = 12
    object Descobrir1: TMenuItem
      Caption = 'BD - &Descobrir'
      OnClick = Descobrir1Click
    end
    object Verificar1: TMenuItem
      Caption = 'BD - &Verificar'
      OnClick = Verificar1Click
    end
    object Internet1: TMenuItem
      Caption = 'Internet - Verificar'
      OnClick = Internet1Click
    end
  end
  object QrDuplic2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM entidades'
      'WHERE CNPJ=:P0'
      'AND Codigo <> :P1')
    Left = 488
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplic2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrListaLograd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM listalograd'
      'ORDER BY Nome')
    Left = 516
    Top = 12
    object QrListaLogradCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaLogradNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 10
    end
  end
  object DsListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 544
    Top = 12
  end
  object QrMunici: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 572
    Top = 12
    object QrMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMunici: TDataSource
    DataSet = QrMunici
    Left = 600
    Top = 12
  end
  object QrBacen_Pais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bacen_pais'
      'ORDER BY Nome'
      '')
    Left = 628
    Top = 12
    object QrBacen_PaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBacen_PaisNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBacen_Pais: TDataSource
    DataSet = QrBacen_Pais
    Left = 656
    Top = 12
  end
end
