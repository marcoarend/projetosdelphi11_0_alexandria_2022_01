object FmEntiJur1: TFmEntiJur1
  Left = 321
  Top = 174
  Caption = 'CLI-JURID-001 :: Clientes Jur'#237'dicos'
  ClientHeight = 768
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 113
    Width = 1241
    Height = 655
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1239
      Height = 502
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel2: TPanel
        Left = 0
        Top = 54
        Width = 1239
        Height = 448
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object PageControl2: TPageControl
          Left = 0
          Top = 0
          Width = 1239
          Height = 448
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet7
          Align = alClient
          TabOrder = 0
          object TabSheet6: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Cadastro '
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 1231
              Height = 417
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GroupBox6: TGroupBox
                Left = 0
                Top = 160
                Width = 1231
                Height = 97
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = ' SPC - Servi'#231'o de prote'#231#227'o ao cr'#233'dito: '
                TabOrder = 1
                object dmkLabel1: TdmkLabel
                  Left = 15
                  Top = 39
                  Width = 164
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Configura'#231#227'o de pesquisa: '
                  UpdType = utYes
                  SQLType = stNil
                end
                object SpeedButton5: TSpeedButton
                  Left = 636
                  Top = 59
                  Width = 26
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '...'
                  OnClick = SpeedButton5Click
                end
                object CBSPC_Config: TdmkDBLookupComboBox
                  Left = 85
                  Top = 59
                  Width = 548
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsSPC_Config
                  TabOrder = 1
                  dmkEditCB = EdSPC_Config
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdSPC_Config: TdmkEditCB
                  Left = 15
                  Top = 59
                  Width = 69
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBSPC_Config
                  IgnoraDBLookupComboBox = False
                end
                object GroupBox7: TGroupBox
                  Left = 669
                  Top = 18
                  Width = 560
                  Height = 77
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alRight
                  Caption = ' Faixa de valores*: '
                  TabOrder = 2
                  object Label66: TLabel
                    Left = 15
                    Top = 20
                    Width = 81
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Valor m'#237'nimo:'
                  end
                  object Label67: TLabel
                    Left = 148
                    Top = 20
                    Width = 85
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Valor m'#225'ximo:'
                  end
                  object Label68: TLabel
                    Left = 286
                    Top = 44
                    Width = 148
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = '*:  Zero (0,00) para infinito'
                  end
                  object EdValMin: TdmkEdit
                    Left = 15
                    Top = 39
                    Width = 129
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    MaxLength = 8
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ValMin'
                    UpdCampo = 'ValMin'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdValMax: TdmkEdit
                    Left = 148
                    Top = 39
                    Width = 129
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    MaxLength = 8
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryCampo = 'ValMax'
                    UpdCampo = 'ValMax'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
              end
              object Panel16: TPanel
                Left = 0
                Top = 0
                Width = 1231
                Height = 160
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label121: TLabel
                  Left = 80
                  Top = 5
                  Width = 104
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Forma societ'#225'ria:'
                  FocusControl = EdFormaSociet
                end
                object Label81: TLabel
                  Left = 214
                  Top = 5
                  Width = 13
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'N:'
                end
                object Label37: TLabel
                  Left = 247
                  Top = 5
                  Width = 114
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Atividade principal:'
                  FocusControl = EdAtividade
                end
                object Label48: TLabel
                  Left = 469
                  Top = 5
                  Width = 245
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Login Windows Messenger: (Deprecado)'
                  Enabled = False
                  FocusControl = EdMSN1
                end
                object Label107: TLabel
                  Left = 5
                  Top = 54
                  Width = 98
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Fator compra %:'
                  FocusControl = EdFatorCompra
                end
                object Label108: TLabel
                  Left = 133
                  Top = 54
                  Width = 89
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Ad Valorem %:'
                  FocusControl = EdAdValorem
                end
                object Label109: TLabel
                  Left = 261
                  Top = 54
                  Width = 70
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'D+ Cheque:'
                  FocusControl = EdDMaisC
                end
                object Label110: TLabel
                  Left = 389
                  Top = 54
                  Width = 80
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'D+ Duplicata:'
                  FocusControl = EdDMaisD
                end
                object Label42: TLabel
                  Left = 517
                  Top = 54
                  Width = 115
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Cr'#233'dito autom'#225'tico:'
                  FocusControl = EdLimiCred
                end
                object Label62: TLabel
                  Left = 635
                  Top = 54
                  Width = 120
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'CPMF espec'#237'fico %:'
                  FocusControl = EdCPMF
                end
                object Label94: TLabel
                  Left = 406
                  Top = 108
                  Width = 30
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'CBE:'
                end
                object CkSimples: TCheckBox
                  Left = 5
                  Top = 27
                  Width = 74
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Simples.'
                  TabOrder = 0
                end
                object EdFormaSociet: TdmkEdit
                  Left = 80
                  Top = 25
                  Width = 128
                  Height = 25
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdNivel: TdmkEdit
                  Left = 214
                  Top = 25
                  Width = 25
                  Height = 25
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  CharCase = ecUpperCase
                  MaxLength = 1
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdAtividade: TdmkEdit
                  Left = 247
                  Top = 25
                  Width = 217
                  Height = 25
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdMSN1: TdmkEdit
                  Left = 469
                  Top = 25
                  Width = 461
                  Height = 25
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Enabled = False
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdFatorCompra: TdmkEdit
                  Left = 5
                  Top = 74
                  Width = 123
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdAdValorem: TdmkEdit
                  Left = 133
                  Top = 74
                  Width = 123
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 6
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdDMaisC: TdmkEdit
                  Left = 261
                  Top = 74
                  Width = 123
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 7
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdDMaisD: TdmkEdit
                  Left = 389
                  Top = 74
                  Width = 123
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 8
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdLimiCred: TdmkEdit
                  Left = 517
                  Top = 74
                  Width = 113
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 9
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCPMF: TdmkEdit
                  Left = 635
                  Top = 74
                  Width = 295
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 10
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCBE: TdmkEdit
                  Left = 406
                  Top = 128
                  Width = 44
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 12
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object CkSCB: TCheckBox
                  Left = 5
                  Top = 130
                  Width = 400
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Cliente sem compensa'#231#227'o banc'#225'ria (Vale o CBE se o CBE > 0).'
                  TabOrder = 11
                end
                object CkAtivo: TdmkCheckBox
                  Left = 462
                  Top = 130
                  Width = 119
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Ativo'
                  TabOrder = 13
                  UpdType = utYes
                  ValCheck = '1'
                  ValUncheck = '0'
                  OldValor = #0
                end
              end
            end
          end
          object TabSheet7: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Exporta/importa '
            ImageIndex = 1
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 1231
              Height = 417
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object RGProtestar: TRadioGroup
                Left = 0
                Top = 183
                Width = 1231
                Height = 197
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = ' Primeira instru'#231#227'o codificada: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  '00-Ausencia de instrucoes'
                  
                    '01-Cobrar juros - Dispens'#225'vel se informado o valor a ser cobrado' +
                    ' por dia de atraso.'
                  '03-Protestar no terceiro dia util apos vencido'
                  '04-Protestar no quarto dia util apos vencido'
                  '05-Protestar no quinto dia util apos vencido'
                  '06-INDICA PROTESTO EM DIAS CORRIDOS, COM PRAZO DE...'
                  '07-Nao protestar'
                  '10-Protestar no 10. dia corrido apos vencido'
                  '15-Protestar no 15. dia corrido apos vencido'
                  '20-Protestar no 20. dia corrido apos vencido'
                  '22-Conceder desconto soh ateh a data estipulada'
                  '25-Protestar no 25. dia corrido apos vencido'
                  '30-Protestar no 30. dia corrido apos vencido'
                  '45-Protestar no 45. dia corrido apos vencido')
                TabOrder = 2
              end
              object Panel12: TPanel
                Left = 0
                Top = 0
                Width = 1231
                Height = 90
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object GroupBox2: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 228
                  Height = 90
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  Caption = ' Exporta'#231#227'o para contabilidade: '
                  TabOrder = 0
                  object Label46: TLabel
                    Left = 15
                    Top = 34
                    Width = 184
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Identifica'#231#227'o da conta cont'#225'bil:'
                    FocusControl = EdContab
                  end
                  object EdContab: TdmkEdit
                    Left = 15
                    Top = 54
                    Width = 188
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    TabOrder = 0
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                end
                object GroupBox1: TGroupBox
                  Left = 228
                  Top = 0
                  Width = 1003
                  Height = 90
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  Caption = ' FTP: '
                  TabOrder = 1
                  object Label50: TLabel
                    Left = 15
                    Top = 34
                    Width = 223
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'A pasta do meu site para este cliente:'
                  end
                  object Label51: TLabel
                    Left = 266
                    Top = 34
                    Width = 321
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'A senha para o cliente acessar sua pasta no meu site:'
                  end
                  object Label52: TLabel
                    Left = 591
                    Top = 34
                    Width = 347
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'N'#227'o informe a pasta ao cliente!!!! Informe a senha.'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -15
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object Label65: TLabel
                    Left = 591
                    Top = 15
                    Width = 259
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'F4 - Libera p/ altera'#231#227'o.  CUIDADO!!!!'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -15
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object EdPastaTxtFTP: TEdit
                    Left = 15
                    Top = 54
                    Width = 246
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    MaxLength = 8
                    TabOrder = 0
                    OnExit = EdPastaTxtFTPExit
                  end
                  object EdPastaPwdFTP: TEdit
                    Left = 266
                    Top = 54
                    Width = 710
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    ReadOnly = True
                    TabOrder = 1
                    OnExit = EdPastaPwdFTPExit
                  end
                end
              end
              object Panel13: TPanel
                Left = 0
                Top = 90
                Width = 1231
                Height = 93
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object GroupBox3: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 814
                  Height = 93
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  Caption = ' Multa padr'#227'o: '
                  TabOrder = 0
                  object Label54: TLabel
                    Left = 364
                    Top = 20
                    Width = 58
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Valor fixo:'
                  end
                  object Label55: TLabel
                    Left = 478
                    Top = 20
                    Width = 84
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Porcentagem:'
                  end
                  object Label56: TLabel
                    Left = 586
                    Top = 20
                    Width = 71
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Dias ap'#243's..:'
                  end
                  object RGMultaCodi: TRadioGroup
                    Left = 2
                    Top = 18
                    Width = 359
                    Height = 73
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alLeft
                    Caption = ' C'#243'digo da multa: '
                    Columns = 2
                    ItemIndex = 2
                    Items.Strings = (
                      'Valor calculado pela %'
                      'Valor fixo (Valor fixo)'
                      'Porcentagem'
                      'Usar padr'#227'o')
                    TabOrder = 0
                  end
                  object EdMultaValr: TdmkEdit
                    Left = 364
                    Top = 39
                    Width = 109
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    MaxLength = 255
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdMultaPerc: TdmkEdit
                    Left = 478
                    Top = 39
                    Width = 98
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    MaxLength = 255
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdMultaDias: TdmkEdit
                    Left = 586
                    Top = 39
                    Width = 79
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    MaxLength = 255
                    TabOrder = 3
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object RGMultaTiVe: TRadioGroup
                    Left = 675
                    Top = 18
                    Width = 137
                    Height = 73
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alRight
                    Caption = ' ... qual data?: '
                    ItemIndex = 1
                    Items.Strings = (
                      'Vencto doc.'
                      'Dia '#250'til vencto.')
                    TabOrder = 4
                  end
                end
                object GroupBox5: TGroupBox
                  Left = 814
                  Top = 0
                  Width = 417
                  Height = 93
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  Caption = ' Taxa de mora ao sacado ap'#243's vencimento: '
                  TabOrder = 1
                  object Label60: TLabel
                    Left = 10
                    Top = 20
                    Width = 49
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = '% Mora:'
                  end
                  object EdJuroSacado: TdmkEdit
                    Left = 10
                    Top = 39
                    Width = 108
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    MaxLength = 255
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
              end
              object EdCorrido: TdmkEdit
                Left = 428
                Top = 325
                Width = 35
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                MaxLength = 255
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
          object TabSheet8: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Coligadas '
            ImageIndex = 2
            object Grade2: TStringGrid
              Left = 0
              Top = 0
              Width = 1231
              Height = 417
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DefaultRowHeight = 19
              RowCount = 7
              TabOrder = 0
            end
          end
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 1239
        Height = 54
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label25: TLabel
          Left = 5
          Top = 5
          Width = 84
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Raz'#227'o Social:'
          FocusControl = EdRazaoSocial
        end
        object Label41: TLabel
          Left = 615
          Top = 5
          Width = 90
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Nome fantasia:'
          FocusControl = EdFantasia
        end
        object Label29: TLabel
          Left = 965
          Top = 6
          Width = 38
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'CNPJ:'
          FocusControl = EdCNPJ
        end
        object Label30: TLabel
          Left = 1108
          Top = 5
          Width = 21
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'I.E.:'
          FocusControl = EdIE
        end
        object EdRazaoSocial: TdmkEdit
          Left = 5
          Top = 25
          Width = 602
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdFantasia: TdmkEdit
          Left = 615
          Top = 25
          Width = 346
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCNPJ: TdmkEdit
          Left = 965
          Top = 25
          Width = 139
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtCPFJ
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdCNPJExit
        end
        object EdIE: TdmkEdit
          Left = 1108
          Top = 25
          Width = 123
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
    object GBRodaPe2: TGroupBox
      Left = 1
      Top = 567
      Width = 1239
      Height = 87
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object PnSaiDesis: TPanel
        Left = 1059
        Top = 18
        Width = 178
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 13
          Left = 15
          Top = 4
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object Panel17: TPanel
        Left = 2
        Top = 18
        Width = 1057
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 15
          Top = 5
          Width = 147
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 113
    Width = 1241
    Height = 655
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 463
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1241
        Height = 54
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label3: TLabel
          Left = 128
          Top = 5
          Width = 84
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Raz'#227'o Social:'
        end
        object Label4: TLabel
          Left = 960
          Top = 5
          Width = 38
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'CNPJ:'
        end
        object Label7: TLabel
          Left = 1103
          Top = 5
          Width = 21
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'I.E.:'
        end
        object Label80: TLabel
          Left = 5
          Top = 5
          Width = 44
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo'
          FocusControl = DBEdit81
        end
        object Label40: TLabel
          Left = 591
          Top = 5
          Width = 90
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Nome fantasia:'
        end
        object DBEdit2: TDBEdit
          Left = 128
          Top = 25
          Width = 459
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'RazaoSocial'
          DataSource = DsEntidades
          TabOrder = 0
        end
        object DBEdit02: TDBEdit
          Left = 960
          Top = 25
          Width = 139
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CNPJ_TXT'
          DataSource = DsEntidades
          TabOrder = 1
        end
        object DBEdit03: TDBEdit
          Left = 1103
          Top = 25
          Width = 124
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'IE'
          DataSource = DsEntidades
          TabOrder = 2
        end
        object DBEdit87: TDBEdit
          Left = 94
          Top = 25
          Width = 28
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Nivel'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
        end
        object DBEdit81: TDBEdit
          Left = 5
          Top = 25
          Width = 87
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
        end
        object DBEdit021: TDBEdit
          Left = 591
          Top = 25
          Width = 365
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Fantasia'
          DataSource = DsEntidades
          TabOrder = 5
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 54
        Width = 1241
        Height = 409
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet13
        Align = alClient
        TabOrder = 1
        object TabSheet13: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Cadastro '
          ImageIndex = 8
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 378
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox8: TGroupBox
              Left = 0
              Top = 153
              Width = 1233
              Height = 93
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = ' SPC - Servi'#231'o de prote'#231#227'o ao cr'#233'dito: '
              TabOrder = 0
              object Label69: TLabel
                Left = 10
                Top = 39
                Width = 161
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Configura'#231#227'o de pesquisa:'
                FocusControl = DBEdit4
              end
              object DBEdit4: TDBEdit
                Left = 10
                Top = 59
                Width = 828
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'SPC_CONFIG_NOME'
                DataSource = DsEntidades
                TabOrder = 0
              end
              object GroupBox9: TGroupBox
                Left = 850
                Top = 18
                Width = 381
                Height = 73
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alRight
                Caption = ' Faixa de valores nas pesquisas (Zero (0,00) para infinito): '
                TabOrder = 1
                object Label70: TLabel
                  Left = 15
                  Top = 20
                  Width = 81
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Valor m'#237'nimo:'
                end
                object Label71: TLabel
                  Left = 158
                  Top = 20
                  Width = 85
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Valor m'#225'ximo:'
                end
                object DBEdit6: TDBEdit
                  Left = 15
                  Top = 39
                  Width = 138
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'SPC_ValMin'
                  DataSource = DsEntidades
                  TabOrder = 0
                end
                object DBEdit7: TDBEdit
                  Left = 158
                  Top = 39
                  Width = 137
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  DataField = 'SPC_ValMax'
                  DataSource = DsEntidades
                  TabOrder = 1
                end
              end
            end
            object Panel15: TPanel
              Left = 0
              Top = 0
              Width = 1233
              Height = 153
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object Label8: TLabel
                Left = 80
                Top = 5
                Width = 104
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Forma societ'#225'ria:'
              end
              object Label49: TLabel
                Left = 464
                Top = 5
                Width = 165
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Login Windows Messenger:'
              end
              object Label38: TLabel
                Left = 228
                Top = 6
                Width = 114
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Atividade principal:'
              end
              object Label22: TLabel
                Left = 5
                Top = 54
                Width = 98
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Fator compra %:'
              end
              object Label23: TLabel
                Left = 133
                Top = 54
                Width = 89
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Ad Valorem %:'
              end
              object Label24: TLabel
                Left = 261
                Top = 54
                Width = 70
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'D+ Cheque:'
              end
              object Label26: TLabel
                Left = 389
                Top = 54
                Width = 80
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'D+ Duplicata:'
              end
              object Label63: TLabel
                Left = 407
                Top = 103
                Width = 30
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'CBE:'
              end
              object Label27: TLabel
                Left = 517
                Top = 54
                Width = 115
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cr'#233'dito autom'#225'tico:'
              end
              object Label64: TLabel
                Left = 645
                Top = 54
                Width = 55
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'CPMF %:'
              end
              object Label47: TLabel
                Left = 729
                Top = 54
                Width = 187
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Identif. conta cont'#225'bil (exportar):'
              end
              object DBCheckBox3: TDBCheckBox
                Left = 5
                Top = 27
                Width = 74
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Simples.'
                DataField = 'Simples'
                DataSource = DsEntidades
                TabOrder = 0
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBEdit01: TDBEdit
                Left = 80
                Top = 25
                Width = 143
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'FormaSociet'
                DataSource = DsEntidades
                TabOrder = 1
              end
              object DBEdit024: TDBEdit
                Left = 464
                Top = 25
                Width = 466
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'MSN1'
                DataSource = DsEntidades
                TabOrder = 2
              end
              object DBEdit020: TDBEdit
                Left = 228
                Top = 25
                Width = 231
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Atividade'
                DataSource = DsEntidades
                TabOrder = 3
              end
              object DBEdit015: TDBEdit
                Left = 5
                Top = 74
                Width = 123
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'FatorCompra'
                DataSource = DsEntidades
                TabOrder = 4
              end
              object DBEdit016: TDBEdit
                Left = 133
                Top = 74
                Width = 123
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'AdValorem'
                DataSource = DsEntidades
                TabOrder = 5
              end
              object DBEdit017: TDBEdit
                Left = 261
                Top = 74
                Width = 123
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'DMaisC'
                DataSource = DsEntidades
                TabOrder = 6
              end
              object DBEdit018: TDBEdit
                Left = 389
                Top = 74
                Width = 123
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'DMaisD'
                DataSource = DsEntidades
                TabOrder = 7
              end
              object DBEdit022: TDBEdit
                Left = 407
                Top = 122
                Width = 45
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'CBE'
                DataSource = DsEntidades
                TabOrder = 8
              end
              object DBCheckBox4: TDBCheckBox
                Left = 5
                Top = 124
                Width = 395
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cliente sem compensa'#231#227'o banc'#225'ria (Vale o CBE se o CBE > 0).'
                DataField = 'SCB'
                DataSource = DsEntidades
                TabOrder = 9
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBEdit019: TDBEdit
                Left = 517
                Top = 74
                Width = 123
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'LimiCred'
                DataSource = DsEntidades
                TabOrder = 10
              end
              object DBEdit025: TDBEdit
                Left = 645
                Top = 74
                Width = 79
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'CPMF'
                DataSource = DsEntidades
                TabOrder = 11
              end
              object DBEdit023: TDBEdit
                Left = 729
                Top = 74
                Width = 201
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Contab'
                DataSource = DsEntidades
                TabOrder = 12
              end
              object DBCheckBox1: TDBCheckBox
                Left = 462
                Top = 124
                Width = 61
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Ativo'
                DataField = 'Ativo'
                DataSource = DsEntidades
                TabOrder = 13
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
            end
          end
        end
        object TabSheet1: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Coligadas '
          object Grade1: TStringGrid
            Left = 0
            Top = 0
            Width = 1233
            Height = 378
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DefaultRowHeight = 19
            Enabled = False
            TabOrder = 0
          end
        end
        object TabSheet2: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Representantes Legais (s'#243'cios) '
          ImageIndex = 1
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 1233
            Height = 378
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsSocios
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Ordem'
                Width = 41
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESOCIO'
                Title.Caption = 'Nome'
                Width = 378
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Cargo'
                Width = 149
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Profissao'
                Title.Caption = 'Profiss'#227'o'
                Width = 174
                Visible = True
              end>
          end
        end
        object TabSheet3: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Taxas autom'#225'ticas '
          ImageIndex = 2
          object GradeTaxas: TDBGrid
            Left = 0
            Top = 26
            Width = 1233
            Height = 352
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsTaxasCli
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Taxa'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETAXA'
                Title.Caption = 'Descri'#231#227'o'
                Width = 311
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'FORMA_TXT'
                Title.Alignment = taCenter
                Title.Caption = 'F'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Title.Alignment = taRightJustify
                Title.Caption = '$ / %'
                Width = 66
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'GENERO_CH'
                Title.Alignment = taCenter
                Title.Caption = 'CH'
                Width = 20
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'GENERO_DU'
                Title.Alignment = taCenter
                Title.Caption = 'DU'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'BASE_TXT'
                Title.Caption = 'Base da cobran'#231'a'
                Width = 254
                Visible = True
              end>
          end
          object Panel14: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            ParentBackground = False
            TabOrder = 1
            object LaAvisoA: TLabel
              Left = 16
              Top = 2
              Width = 965
              Height = 19
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'Para efetiva'#231#227'o da inclus'#227'o autom'#225'tica,  o "Cadastro do cliente"' +
                ' deve ser a op'#231#227'o das "taxas autom'#225'ticas" nas op'#231#245'es do aplicati' +
                'vo.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -17
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoB: TLabel
              Left = 15
              Top = 1
              Width = 965
              Height = 19
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'Para efetiva'#231#227'o da inclus'#227'o autom'#225'tica,  o "Cadastro do cliente"' +
                ' deve ser a op'#231#227'o das "taxas autom'#225'ticas" nas op'#231#245'es do aplicati' +
                'vo.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -17
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
          end
        end
        object TabSheet4: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Ocorr'#234'ncias banc'#225'rias (CNAB) '
          ImageIndex = 3
          object DBGrid2: TDBGrid
            Left = 0
            Top = 0
            Width = 1233
            Height = 378
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsOcorCli
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEOCOR'
                Title.Caption = 'Ocorr'#234'ncia'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Base'
                Title.Caption = 'Valor base'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FormaCNAB_TXT'
                Title.Caption = 'Forma de cobran'#231'a'
                Visible = True
              end>
          end
        end
        object TabSheet5: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' FTP '
          ImageIndex = 4
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 378
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label53: TLabel
              Left = 5
              Top = 6
              Width = 321
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'A senha para o cliente acessar sua pasta no meu site:'
            end
            object DBEdit1: TDBEdit
              Left = 5
              Top = 25
              Width = 946
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'PastaPwdFTP'
              DataSource = DsEntidades
              TabOrder = 0
            end
          end
        end
        object TabSheet10: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Cobran'#231'a banc'#225'ria '
          ImageIndex = 5
          object DBEdit25: TDBEdit
            Left = 428
            Top = 80
            Width = 35
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Corrido'
            DataSource = DsEntidades
            TabOrder = 0
          end
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 378
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Label61: TLabel
              Left = 414
              Top = 11
              Width = 259
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Taxa de juros ao sacado ap'#243's vencimento:'
            end
            object DBEdit3: TDBEdit
              Left = 670
              Top = 5
              Width = 98
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'JuroSacado'
              DataSource = DsEntidades
              TabOrder = 0
            end
            object GroupBox4: TGroupBox
              Left = 0
              Top = 0
              Width = 410
              Height = 378
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              Caption = ' Multa para duplicatas: '
              TabOrder = 1
              object Label57: TLabel
                Left = 192
                Top = 15
                Width = 58
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Valor fixo:'
              end
              object Label58: TLabel
                Left = 305
                Top = 15
                Width = 84
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Porcentagem:'
              end
              object Label59: TLabel
                Left = 192
                Top = 74
                Width = 71
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dias ap'#243's..:'
              end
              object DBRadioGroup3: TDBRadioGroup
                Left = 2
                Top = 18
                Width = 181
                Height = 358
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' C'#243'digo da Multa: '
                DataField = 'MultaCodi'
                DataSource = DsEntidades
                Items.Strings = (
                  'Valor calculado pela %'
                  'Valor fixo (Valor fixo)'
                  'Porcentagem'
                  'Usar padr'#227'o')
                ParentBackground = True
                TabOrder = 0
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3')
              end
              object DBEdit16: TDBEdit
                Left = 192
                Top = 34
                Width = 108
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'MultaValr'
                DataSource = DsEntidades
                TabOrder = 1
              end
              object DBEdit17: TDBEdit
                Left = 305
                Top = 34
                Width = 99
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'MultaPerc'
                DataSource = DsEntidades
                TabOrder = 2
              end
              object DBEdit18: TDBEdit
                Left = 192
                Top = 94
                Width = 79
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'MultaDias'
                DataSource = DsEntidades
                TabOrder = 3
              end
              object DBRadioGroup4: TDBRadioGroup
                Left = 276
                Top = 64
                Width = 129
                Height = 63
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' ... Qual data?: '
                DataField = 'MultaTiVe'
                DataSource = DsEntidades
                Items.Strings = (
                  'Vencto doc.'
                  'Dia '#250'til vencto.')
                ParentBackground = True
                TabOrder = 4
                Values.Strings = (
                  '0'
                  '1')
              end
            end
          end
        end
        object TabSheet11: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Protesto '
          ImageIndex = 6
          object DBRGProtestar: TDBRadioGroup
            Left = 0
            Top = 0
            Width = 1233
            Height = 378
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = ' Primeira instru'#231#227'o codificada: '
            Columns = 2
            DataField = 'Protestar'
            DataSource = DsEntidades
            Items.Strings = (
              '00-Ausencia de instrucoes'
              
                '01-Cobrar juros - Dispensavel se informado o valor a ser cobrado' +
                ' por dia de atraso.'
              '03-Protestar no terceiro dia util apos vencido'
              '04-Protestar no quarto dia util apos vencido'
              '05-Protestar no quinto dia util apos vencido'
              '06-INDICA PROTESTO EM DIAS CORRIDOS, COM PRAZO DE...'
              '07-Nao protestar'
              '10-Protestar no 10. dia corrido apos vencido'
              '15-Protestar no 15. dia corrido apos vencido'
              '20-Protestar no 20. dia corrido apos vencido'
              '22-Conceder desconto soh ateh a data estipulada'
              '25-Protestar no 25. dia corrido apos vencido'
              '30-Protestar no 30. dia corrido apos vencido'
              '45-Protestar no 45. dia corrido apos vencido')
            ParentBackground = True
            TabOrder = 0
            Values.Strings = (
              '0'
              '1'
              '3'
              '4'
              '5'
              '6'
              '7'
              '10'
              '15'
              '20'
              '22'
              '25'
              '30'
              '45')
          end
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 569
      Width = 1241
      Height = 86
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object PainelControle: TPanel
        Left = 2
        Top = 18
        Width = 1237
        Height = 66
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaRegistro: TLabel
          Left = 213
          Top = 1
          Width = 31
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[N]: 0'
        end
        object Panel3: TPanel
          Left = 586
          Top = 0
          Width = 651
          Height = 66
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object BtGestor: TBitBtn
            Tag = 123
            Left = 118
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Hint = 'Altera banco atual'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&S'#243'cios'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtGestorClick
          end
          object BtEntidades2: TBitBtn
            Tag = 122
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Hint = 'Inclui novo banco'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Cliente'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtEntidades2Click
          end
          object BtTaxas: TBitBtn
            Tag = 124
            Left = 231
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Hint = 'Altera banco atual'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Taxas'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtTaxasClick
          end
          object Panel1: TPanel
            Left = 522
            Top = 0
            Width = 129
            Height = 66
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 3
            object BtSaida: TBitBtn
              Tag = 13
              Left = 5
              Top = 5
              Width = 111
              Height = 49
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object BtOcor: TBitBtn
            Tag = 148
            Left = 345
            Top = 5
            Width = 110
            Height = 49
            Cursor = crHandPoint
            Hint = 'Altera banco atual'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Ocorr.'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = BtOcorClick
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 212
          Height = 66
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object SpeedButton4: TBitBtn
            Tag = 4
            Left = 156
            Top = 5
            Width = 50
            Height = 49
            Cursor = crHandPoint
            Hint = 'ltimo registro'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = SpeedButton4Click
          end
          object SpeedButton3: TBitBtn
            Tag = 3
            Left = 108
            Top = 5
            Width = 50
            Height = 49
            Cursor = crHandPoint
            Hint = 'Pr'#243'ximo registro'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = SpeedButton3Click
          end
          object SpeedButton2: TBitBtn
            Tag = 2
            Left = 59
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Hint = 'Registro anterior'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = SpeedButton2Click
          end
          object SpeedButton1: TBitBtn
            Tag = 1
            Left = 10
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Hint = 'Primeiro registro'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = SpeedButton1Click
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 320
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 7
        Top = 5
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 59
        Top = 5
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 111
        Top = 5
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 162
        Top = 5
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 214
        Top = 5
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtEntidades: TBitBtn
        Tag = 132
        Left = 265
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtEntidadesClick
      end
    end
    object GB_M: TGroupBox
      Left = 320
      Top = 0
      Width = 862
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 256
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Clientes Jur'#237'dicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 256
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Clientes Jur'#237'dicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 256
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Clientes Jur'#237'dicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 59
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel18: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 545
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'CBE = Compensa'#231#227'o banc'#225'ria especial fixa limitada ao CBE (0 = no' +
          'rmal).'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 545
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'CBE = Compensa'#231#227'o banc'#225'ria especial fixa limitada ao CBE (0 = no' +
          'rmal).'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntidadesAfterOpen
    AfterScroll = QrEntidadesAfterScroll
    OnCalcFields = QrEntidadesCalcFields
    SQL.Strings = (
      'SELECT spc.Nome SPC_CONFIG_NOME,  spe.SPC_Config,'
      'spe.ValMin SPC_ValMin, spe.ValMax SPC_ValMax, ent.*,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'ENTIDADE,'
      
        'CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial ELSE acm.Nome END NOME' +
        'ACCOUNT,'
      
        'eng.Nome NOMEENTIGRUPO, mot.Descricao NOMEMOTIVO, emp.RazaoSocia' +
        'l NOMEEMPRESA,'
      
        'euf.Nome NOMEEUF, puf.nome NOMEPUF, cuf.Nome NOMECUF, luf.nome N' +
        'OMELUF, '
      'nuf.Nome NOMENUF, elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,'
      'clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD, lec.Nome NOMEECIVIL'
      'FROM entidades ent'
      'LEFT JOIN entigrupos eng  ON eng.Codigo=ent.Grupo'
      'LEFT JOIN entidades acm   ON acm.Codigo=ent.Account'
      'LEFT JOIN entidades emp   ON emp.Codigo=ent.Empresa'
      'LEFT JOIN motivose mot    ON mot.Codigo=ent.Motivo'
      'LEFT JOIN ufs euf         ON euf.Codigo=ent.EUF'
      'LEFT JOIN ufs puf         ON puf.Codigo=ent.PUF'
      'LEFT JOIN ufs cuf         ON cuf.Codigo=ent.EUF'
      'LEFT JOIN ufs luf         ON luf.Codigo=ent.PUF'
      'LEFT JOIN ufs nuf         ON nuf.Codigo=ent.UFNatal'
      'LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd'
      'LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd'
      'LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd'
      'LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd'
      'LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil'
      'LEFT JOIN spc_entida  spe ON ent.Codigo=spe.Entidade'#13
      'LEFT JOIN spc_config  spc ON spc.Codigo=spe.SPC_Config'#10
      ''
      ''
      'WHERE ent.Codigo>-2')
    Left = 472
    Top = 12
    object QrEntidadesECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesPCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesCCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesLCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntidadesRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrEntidadesFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrEntidadesRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrEntidadesRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrEntidadesPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrEntidadesMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrEntidadesCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntidadesIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntidadesFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
    end
    object QrEntidadesSimples: TSmallintField
      FieldName = 'Simples'
      Required = True
    end
    object QrEntidadesIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrEntidadesAtividade: TWideStringField
      FieldName = 'Atividade'
      Size = 50
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrEntidadesApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrEntidadesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntidadesCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrEntidadesRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEntidadesSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrEntidadesDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrEntidadesCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrEntidadesEstCivil: TSmallintField
      FieldName = 'EstCivil'
      Required = True
    end
    object QrEntidadesUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Required = True
    end
    object QrEntidadesNacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Size = 15
    end
    object QrEntidadesERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrEntidadesECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrEntidadesEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrEntidadesECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrEntidadesEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrEntidadesECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEntidadesEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrEntidadesETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrEntidadesEte2: TWideStringField
      FieldName = 'Ete2'
    end
    object QrEntidadesEte3: TWideStringField
      FieldName = 'Ete3'
    end
    object QrEntidadesECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrEntidadesEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrEntidadesEEmail: TWideStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrEntidadesEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrEntidadesENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEntidadesPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrEntidadesPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrEntidadesPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrEntidadesPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrEntidadesPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrEntidadesPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEntidadesPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrEntidadesPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrEntidadesPte2: TWideStringField
      FieldName = 'Pte2'
    end
    object QrEntidadesPte3: TWideStringField
      FieldName = 'Pte3'
    end
    object QrEntidadesPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrEntidadesPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrEntidadesPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrEntidadesPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrEntidadesPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEntidadesSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrEntidadesResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrEntidadesProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrEntidadesCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrEntidadesRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrEntidadesDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrEntidadesAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrEntidadesAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrEntidadesCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrEntidadesCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrEntidadesFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrEntidadesFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrEntidadesFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrEntidadesFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrEntidadesFornece5: TWideStringField
      FieldName = 'Fornece5'
      Size = 1
    end
    object QrEntidadesFornece6: TWideStringField
      FieldName = 'Fornece6'
      Size = 1
    end
    object QrEntidadesTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrEntidadesCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEntidadesInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrEntidadesLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrEntidadesVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrEntidadesMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrEntidadesObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrEntidadesTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntidadesCLograd: TSmallintField
      FieldName = 'CLograd'
      Required = True
    end
    object QrEntidadesCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrEntidadesCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrEntidadesCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrEntidadesCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrEntidadesCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrEntidadesCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrEntidadesCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrEntidadesCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrEntidadesCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrEntidadesCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrEntidadesCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrEntidadesLLograd: TSmallintField
      FieldName = 'LLograd'
      Required = True
    end
    object QrEntidadesLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrEntidadesLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrEntidadesLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrEntidadesLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrEntidadesLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrEntidadesLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrEntidadesLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrEntidadesLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrEntidadesLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrEntidadesLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrEntidadesLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrEntidadesComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrEntidadesSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrEntidadesNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrEntidadesGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrEntidadesAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEntidadesLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrEntidadesConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrEntidadesConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrEntidadesNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrEntidadesNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrEntidadesNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrEntidadesNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrEntidadesNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrEntidadesNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrEntidadesNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrEntidadesNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrEntidadesCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrEntidadesCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrEntidadesCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrEntidadesCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrEntidadesCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrEntidadesCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrEntidadesMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrEntidadesQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrEntidadesQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrEntidadesQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrEntidadesQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrEntidadesQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrEntidadesQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrEntidadesAgenda: TWideStringField
      FieldName = 'Agenda'
      Required = True
      Size = 1
    end
    object QrEntidadesSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Required = True
      Size = 1
    end
    object QrEntidadesSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrEntidadesLimiCred: TFloatField
      FieldName = 'LimiCred'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEntidadesDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrEntidadesCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Required = True
    end
    object QrEntidadesTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrEntidadesBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEntidadesAgencia: TWideStringField
      FieldName = 'Agencia'
      Size = 11
    end
    object QrEntidadesContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEntidadesFatorCompra: TFloatField
      FieldName = 'FatorCompra'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrEntidadesAdValorem: TFloatField
      FieldName = 'AdValorem'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrEntidadesDMaisC: TIntegerField
      FieldName = 'DMaisC'
      DisplayFormat = '0'
    end
    object QrEntidadesDMaisD: TIntegerField
      FieldName = 'DMaisD'
      DisplayFormat = '0'
    end
    object QrEntidadesEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEntidadesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEntidadesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEntidadesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntidadesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEntidadesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEntidadesCPF_Conjuge: TWideStringField
      FieldName = 'CPF_Conjuge'
      Size = 18
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrEntidadesNOMEACCOUNT: TWideStringField
      FieldName = 'NOMEACCOUNT'
      Size = 100
    end
    object QrEntidadesNOMEENTIGRUPO: TWideStringField
      FieldName = 'NOMEENTIGRUPO'
      Size = 100
    end
    object QrEntidadesNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Size = 50
    end
    object QrEntidadesNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrEntidadesNOMEEUF: TWideStringField
      FieldName = 'NOMEEUF'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMECUF: TWideStringField
      FieldName = 'NOMECUF'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMELUF: TWideStringField
      FieldName = 'NOMELUF'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMENUF: TWideStringField
      FieldName = 'NOMENUF'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMEELOGRAD: TWideStringField
      FieldName = 'NOMEELOGRAD'
      Size = 10
    end
    object QrEntidadesNOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Size = 10
    end
    object QrEntidadesNOMECLOGRAD: TWideStringField
      FieldName = 'NOMECLOGRAD'
      Size = 10
    end
    object QrEntidadesNOMELLOGRAD: TWideStringField
      FieldName = 'NOMELLOGRAD'
      Size = 10
    end
    object QrEntidadesCTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CTEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LTEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE2_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE3_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE2_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE3_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesECEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesEFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Required = True
      Size = 10
    end
    object QrEntidadesCBE: TIntegerField
      FieldName = 'CBE'
    end
    object QrEntidadesSCB: TIntegerField
      FieldName = 'SCB'
    end
    object QrEntidadesContab: TWideStringField
      FieldName = 'Contab'
    end
    object QrEntidadesMSN1: TWideStringField
      FieldName = 'MSN1'
      Size = 255
    end
    object QrEntidadesPastaTxtFTP: TWideStringField
      FieldName = 'PastaTxtFTP'
      Size = 8
    end
    object QrEntidadesPastaPwdFTP: TWideStringField
      FieldName = 'PastaPwdFTP'
    end
    object QrEntidadesMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
      Required = True
    end
    object QrEntidadesMultaDias: TSmallintField
      FieldName = 'MultaDias'
      Required = True
    end
    object QrEntidadesMultaValr: TFloatField
      FieldName = 'MultaValr'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesProtestar: TSmallintField
      FieldName = 'Protestar'
      Required = True
    end
    object QrEntidadesMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
      Required = True
    end
    object QrEntidadesJuroSacado: TFloatField
      FieldName = 'JuroSacado'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesCPMF: TFloatField
      FieldName = 'CPMF'
      Required = True
      DisplayFormat = '#,###,##0.0000'
    end
    object QrEntidadesCorrido: TIntegerField
      FieldName = 'Corrido'
    end
    object QrEntidadesENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEntidadesPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEntidadesCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrEntidadesLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrEntidadesSPC_CONFIG_NOME: TWideStringField
      FieldName = 'SPC_CONFIG_NOME'
      Size = 100
    end
    object QrEntidadesSPC_ValMin: TFloatField
      FieldName = 'SPC_ValMin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesSPC_ValMax: TFloatField
      FieldName = 'SPC_ValMax'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesSPC_Config: TIntegerField
      FieldName = 'SPC_Config'
    end
    object QrEntidadesNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEntidadesAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 500
    Top = 12
  end
  object QrDuplic2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM entidades'
      'WHERE CNPJ=:P0'
      'AND Codigo <> :P1')
    Left = 504
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplic2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLoc0: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Codigo) CodMax, MIN(Codigo) CodMin'
      'FROM entidades'
      'WHERE Codigo<1000')
    Left = 472
    Top = 44
    object QrLoc0CodMin: TIntegerField
      FieldName = 'CodMin'
      Required = True
    end
    object QrLoc0CodMax: TIntegerField
      FieldName = 'CodMax'
      Required = True
    end
  end
  object PMCliente: TPopupMenu
    OnPopup = PMClientePopup
    Left = 296
    Top = 416
    object IncluinovoClienteJurdico1: TMenuItem
      Caption = '&Gerencia entidade'
      OnClick = IncluinovoClienteJurdico1Click
    end
    object Alteraclienteatual1: TMenuItem
      Caption = '&Altera dados espec'#237'ficos de cliente atual'
      OnClick = Alteraclienteatual1Click
    end
  end
  object QrSocios: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT so.Empresa Empresa, '
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome '
      'END NOMESOCIO, en.*, so.*,'
      'uf.Nome NOMEPUF, ll.Nome NOMEPLOGRAD, le.Nome NOMEECIVIL'
      'FROM socios so'
      'LEFT JOIN entidades   en ON en.Codigo=so.Socio'
      'LEFT JOIN ufs uf         ON uf.Codigo=en.PUF'
      'LEFT JOIN listalograd ll ON ll.Codigo=en.PLograd'
      'LEFT JOIN listaecivil le ON le.Codigo=en.EstCivil'
      'WHERE so.Empresa=:P0'
      'ORDER BY so.Ordem, NOMESOCIO')
    Left = 592
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSociosNOMESOCIO: TWideStringField
      FieldName = 'NOMESOCIO'
      Size = 100
    end
    object QrSociosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSociosSexo: TWideStringField
      FieldName = 'Sexo'
      Size = 1
    end
    object QrSociosPai: TWideStringField
      FieldName = 'Pai'
      Size = 60
    end
    object QrSociosMae: TWideStringField
      FieldName = 'Mae'
      Size = 60
    end
    object QrSociosPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrSociosCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrSociosNacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Size = 15
    end
    object QrSociosConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrSociosCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrSociosRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrSociosSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrSociosDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrSociosPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrSociosPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrSociosPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrSociosPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrSociosPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrSociosPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrSociosProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrSociosCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrSociosNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Required = True
      Size = 2
    end
    object QrSociosNOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Size = 10
    end
    object QrSociosNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Required = True
      Size = 10
    end
    object QrSociosEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrSociosSocio: TIntegerField
      FieldName = 'Socio'
      Required = True
    end
    object QrSociosOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrSociosPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrSociosRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 100
    end
    object QrSociosFantasia: TWideStringField
      FieldName = 'Fantasia'
      Size = 60
    end
    object QrSociosRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 60
    end
    object QrSociosRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 60
    end
    object QrSociosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrSociosIE: TWideStringField
      FieldName = 'IE'
    end
    object QrSociosFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
    end
    object QrSociosSimples: TSmallintField
      FieldName = 'Simples'
    end
    object QrSociosIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrSociosAtividade: TWideStringField
      FieldName = 'Atividade'
      Size = 50
    end
    object QrSociosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSociosApelido: TWideStringField
      FieldName = 'Apelido'
      Size = 60
    end
    object QrSociosCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrSociosEstCivil: TSmallintField
      FieldName = 'EstCivil'
    end
    object QrSociosUFNatal: TSmallintField
      FieldName = 'UFNatal'
    end
    object QrSociosELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrSociosERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrSociosECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrSociosEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrSociosECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrSociosEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrSociosECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrSociosEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrSociosETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrSociosEte2: TWideStringField
      FieldName = 'Ete2'
    end
    object QrSociosEte3: TWideStringField
      FieldName = 'Ete3'
    end
    object QrSociosECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrSociosEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrSociosEEmail: TWideStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrSociosEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrSociosENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrSociosPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrSociosPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrSociosPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrSociosPte2: TWideStringField
      FieldName = 'Pte2'
    end
    object QrSociosPte3: TWideStringField
      FieldName = 'Pte3'
    end
    object QrSociosPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrSociosPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrSociosPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrSociosResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrSociosRecibo: TSmallintField
      FieldName = 'Recibo'
    end
    object QrSociosDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
    end
    object QrSociosAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
    end
    object QrSociosAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
    end
    object QrSociosCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrSociosCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrSociosFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrSociosFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrSociosFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrSociosFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrSociosFornece5: TWideStringField
      FieldName = 'Fornece5'
      Size = 1
    end
    object QrSociosFornece6: TWideStringField
      FieldName = 'Fornece6'
      Size = 1
    end
    object QrSociosTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrSociosCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrSociosInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrSociosLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrSociosVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrSociosMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrSociosObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSociosTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrSociosCLograd: TSmallintField
      FieldName = 'CLograd'
    end
    object QrSociosCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrSociosCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrSociosCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrSociosCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrSociosCUF: TSmallintField
      FieldName = 'CUF'
    end
    object QrSociosCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrSociosCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrSociosCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrSociosCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrSociosCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrSociosCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrSociosLLograd: TSmallintField
      FieldName = 'LLograd'
    end
    object QrSociosLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrSociosLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrSociosLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrSociosLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrSociosLUF: TSmallintField
      FieldName = 'LUF'
    end
    object QrSociosLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrSociosLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrSociosLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrSociosLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrSociosLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrSociosLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrSociosComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrSociosSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrSociosNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrSociosGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrSociosAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrSociosLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrSociosConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrSociosNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrSociosNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrSociosNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrSociosNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrSociosNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrSociosNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrSociosNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrSociosNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrSociosCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrSociosCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrSociosCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrSociosCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrSociosCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrSociosCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrSociosMotivo: TIntegerField
      FieldName = 'Motivo'
    end
    object QrSociosQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrSociosQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrSociosQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrSociosQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrSociosQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrSociosQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrSociosAgenda: TWideStringField
      FieldName = 'Agenda'
      Size = 1
    end
    object QrSociosSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Size = 1
    end
    object QrSociosSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrSociosLimiCred: TFloatField
      FieldName = 'LimiCred'
    end
    object QrSociosDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSociosCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
    end
    object QrSociosTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrSociosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrSociosAgencia: TWideStringField
      FieldName = 'Agencia'
      Size = 11
    end
    object QrSociosContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrSociosFatorCompra: TFloatField
      FieldName = 'FatorCompra'
    end
    object QrSociosAdValorem: TFloatField
      FieldName = 'AdValorem'
    end
    object QrSociosDMaisC: TIntegerField
      FieldName = 'DMaisC'
    end
    object QrSociosDMaisD: TIntegerField
      FieldName = 'DMaisD'
    end
    object QrSociosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSociosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSociosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSociosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSociosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSociosCPF_Conjuge: TWideStringField
      FieldName = 'CPF_Conjuge'
      Size = 18
    end
    object QrSociosEmpresa_1: TIntegerField
      FieldName = 'Empresa_1'
      Required = True
    end
    object QrSociosENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrSociosPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrSociosCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrSociosLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrSociosNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
  end
  object DsSocios: TDataSource
    DataSet = QrSocios
    Left = 620
    Top = 12
  end
  object PMSocios: TPopupMenu
    OnPopup = PMSociosPopup
    Left = 388
    Top = 416
    object Adiciona1: TMenuItem
      Caption = 'A&diciona novo s'#243'cio'
      OnClick = Adiciona1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera s'#243'cio selecionado'
      OnClick = Altera1Click
    end
    object Remove1: TMenuItem
      Caption = '&Remove s'#243'cio selecionado'
      OnClick = Remove1Click
    end
  end
  object PMImprime: TPopupMenu
    OnPopup = PMImprimePopup
    Left = 52
    Top = 60
    object Scios1: TMenuItem
      Caption = '&S'#243'cios da empresa cliente'
      OnClick = Scios1Click
    end
    object Risco1: TMenuItem
      Caption = '&Risco do cliente'
      OnClick = Risco1Click
    end
    object ContratodeFomentoMercantil1: TMenuItem
      Caption = '&Contrato de fomento mercantil'
      OnClick = ContratodeFomentoMercantil1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Formulrioparapreenchimentomanual1: TMenuItem
      Caption = 'Formul'#225'rio para preenchimento manual'
      OnClick = Formulrioparapreenchimentomanual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Pesquisarantesdeimprimir1: TMenuItem
      Caption = '&Pesquisar antes de imprimir'
      OnClick = Pesquisarantesdeimprimir1Click
    end
  end
  object PMTaxas: TPopupMenu
    OnPopup = PMTaxasPopup
    Left = 488
    Top = 428
    object Adicionataxa1: TMenuItem
      Caption = '&Adiciona taxa'
      OnClick = Adicionataxa1Click
    end
    object Retirataxa1: TMenuItem
      Caption = '&Retira taxa'
      OnClick = Retirataxa1Click
    end
  end
  object QrTaxasCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE WHEN ta.Genero in (1,3) THEN "S" ELSE "N" END GENERO_CH, '
      'CASE WHEN ta.Genero in (2,3) THEN "S" ELSE "N" END GENERO_DU, '
      'CASE WHEN ta.Forma = 0  THEN "$" ELSE "%" END FORMA_TXT,'
      'CASE WHEN ta.Forma = 0  THEN "Valor total dos itens" '
      'ELSE "Contagem dos Itens" END BASE_TXT,'
      'ta.Genero, ta.Forma, ta.Base, ta.Nome NOMETAXA, tc.* '
      'FROM taxascli tc'
      'LEFT JOIN taxas ta ON ta.Codigo=tc.Taxa'
      'WHERE tc.Cliente=:P0'
      'ORDER BY NOMETAXA'
      '')
    Left = 652
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTaxasCliNOMETAXA: TWideStringField
      FieldName = 'NOMETAXA'
      Size = 30
    end
    object QrTaxasCliControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTaxasCliCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrTaxasCliLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTaxasCliDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTaxasCliDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTaxasCliUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTaxasCliUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTaxasCliTaxa: TIntegerField
      FieldName = 'Taxa'
      Required = True
    end
    object QrTaxasCliValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrTaxasCliGENERO_CH: TWideStringField
      FieldName = 'GENERO_CH'
      Size = 1
    end
    object QrTaxasCliGENERO_DU: TWideStringField
      FieldName = 'GENERO_DU'
      Size = 1
    end
    object QrTaxasCliFORMA_TXT: TWideStringField
      FieldName = 'FORMA_TXT'
      Size = 1
    end
    object QrTaxasCliBASE_TXT: TWideStringField
      FieldName = 'BASE_TXT'
      Size = 21
    end
    object QrTaxasCliGenero: TSmallintField
      FieldName = 'Genero'
    end
    object QrTaxasCliForma: TSmallintField
      FieldName = 'Forma'
    end
    object QrTaxasCliBase: TSmallintField
      FieldName = 'Base'
    end
  end
  object DsTaxasCli: TDataSource
    DataSet = QrTaxasCli
    Left = 680
    Top = 12
  end
  object PMOcor: TPopupMenu
    OnPopup = PMOcorPopup
    Left = 576
    Top = 416
    object Incluiocorrncia1: TMenuItem
      Caption = '&Inclui ocorr'#234'ncia'
      OnClick = Incluiocorrncia1Click
    end
    object Alteraocorrncia1: TMenuItem
      Caption = '&Altera ocorr'#234'ncia'
      OnClick = Alteraocorrncia1Click
    end
    object Excluiocorncia1: TMenuItem
      Caption = '&Exclui ocor'#234'ncia'
      OnClick = Excluiocorncia1Click
    end
  end
  object QrOcorCli: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOcorCliCalcFields
    SQL.Strings = (
      'SELECT ocb.Nome NOMEOCOR, occ.* '
      'FROM ocorcli occ'
      'LEFT JOIN ocorbank ocb ON ocb.Codigo=occ.Ocorrencia'
      'WHERE occ.Cliente=:P0')
    Left = 536
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorCliCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrOcorCliControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOcorCliOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrOcorCliBase: TFloatField
      FieldName = 'Base'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrOcorCliFormaCNAB: TSmallintField
      FieldName = 'FormaCNAB'
    end
    object QrOcorCliNOMEOCOR: TWideStringField
      FieldName = 'NOMEOCOR'
      Size = 50
    end
    object QrOcorCliFormaCNAB_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FormaCNAB_TXT'
      Size = 50
      Calculated = True
    end
  end
  object DsOcorCli: TDataSource
    DataSet = QrOcorCli
    Left = 564
    Top = 44
  end
  object QrSPC_Config: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM spc_config'
      'ORDER BY Nome')
    Left = 596
    Top = 44
    object QrSPC_ConfigCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSPC_ConfigNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsSPC_Config: TDataSource
    DataSet = QrSPC_Config
    Left = 624
    Top = 44
  end
  object frxCadastro: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38011.488035601900000000
    ReportOptions.LastChange = 39495.767711585600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 261
    Top = 9
    Datasets = <
      item
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -7
      Font.Name = 'Arial'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 287.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Memo7: TfrxMemoView
          Width = 699.495980000000000000
          Height = 27.102350000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCartas."Titulo"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 60.472431180000000000
        Top = 177.637910000000000000
        Width = 699.213050000000000000
        object Memo16: TfrxMemoView
          Left = 536.693235590000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.850030000000000000
        Top = 102.047310000000000000
        Width = 699.213050000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
        RowCount = 0
        Stretched = True
        object Rich1: TfrxRichView
          Width = 699.213050000000000000
          Height = 18.897637800000000000
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsCartas
          DataSetName = 'frxDsCartas'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C204D532053616E732053657269663B7D7D0D0A7B5C2A5C67656E
            657261746F722052696368656432302031302E302E31343339337D5C76696577
            6B696E64345C756331200D0A5C706172645C66305C667331365C7061720D0A7D
            0D0A00}
        end
      end
    end
  end
  object QrCartas: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntidadesAfterOpen
    AfterScroll = QrEntidadesAfterScroll
    OnCalcFields = QrEntidadesCalcFields
    SQL.Strings = (
      'SELECT cg.Nome NOMECARTAG, ca.* '
      'FROM cartas ca'
      'LEFT JOIN cartag cg ON cg.Codigo=ca.CartaG'
      'WHERE ca.Codigo > 0'
      'AND ca.Tipo = 5'
      'LIMIT 1')
    Left = 289
    Top = 9
    object QrCartasNOMECARTAG: TWideStringField
      FieldName = 'NOMECARTAG'
      Size = 50
    end
    object QrCartasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartasTitulo: TWideStringField
      FieldName = 'Titulo'
      Size = 100
    end
    object QrCartasTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCartasCartaG: TIntegerField
      FieldName = 'CartaG'
    end
    object QrCartasPagina: TIntegerField
      FieldName = 'Pagina'
    end
    object QrCartasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCartasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCartasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCartasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCartasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCartasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCartasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCartasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object frxDsCartas: TfrxDBDataset
    UserName = 'frxDsCartas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECARTAG=NOMECARTAG'
      'Codigo=Codigo'
      'Titulo=Titulo'
      'Texto=Texto'
      'CartaG=CartaG'
      'Pagina=Pagina'
      'Tipo=Tipo'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrCartas
    BCDToCurrency = False
    Left = 317
    Top = 9
  end
  object frxRichObject1: TfrxRichObject
    Left = 345
    Top = 9
  end
end
