object FmEntiSocio1: TFmEntiSocio1
  Left = 344
  Top = 243
  Caption = 'CLI-JURID-007 :: Cadastro R'#225'pido Pessoa F'#237'sica'
  ClientHeight = 486
  ClientWidth = 717
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 717
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 669
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 621
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 383
        Height = 32
        Caption = 'Cadastro R'#225'pido Pessoa F'#237'sica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 383
        Height = 32
        Caption = 'Cadastro R'#225'pido Pessoa F'#237'sica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 383
        Height = 32
        Caption = 'Cadastro R'#225'pido Pessoa F'#237'sica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 372
    Width = 717
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 713
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 416
    Width = 717
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 571
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 569
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 717
    Height = 324
    Align = alClient
    TabOrder = 3
    object PainelDados: TPanel
      Left = 2
      Top = 15
      Width = 713
      Height = 307
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label25: TLabel
        Left = 72
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Nome:'
        FocusControl = EdNome
      end
      object Label43: TLabel
        Left = 296
        Top = 136
        Width = 103
        Height = 13
        Caption = 'Telefone Residencial:'
      end
      object Label36: TLabel
        Left = 412
        Top = 136
        Width = 35
        Height = 13
        Caption = 'Celular:'
        FocusControl = EdCel
      end
      object Label45: TLabel
        Left = 432
        Top = 4
        Width = 59
        Height = 13
        Caption = 'Nascimento:'
      end
      object Label29: TLabel
        Left = 4
        Top = 48
        Width = 23
        Height = 13
        Caption = 'CPF:'
        FocusControl = EdCNPJCPF
      end
      object Label30: TLabel
        Left = 120
        Top = 48
        Width = 19
        Height = 13
        Caption = 'RG:'
        FocusControl = EdIERG
      end
      object Label31: TLabel
        Left = 92
        Top = 92
        Width = 84
        Height = 13
        Caption = 'Nome logradouro:'
        FocusControl = EdRua
      end
      object Label32: TLabel
        Left = 380
        Top = 92
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
        FocusControl = EdPNumero
      end
      object Label33: TLabel
        Left = 444
        Top = 92
        Width = 67
        Height = 13
        Caption = 'Complemento:'
        FocusControl = EdCompl
      end
      object Label34: TLabel
        Left = 4
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Bairro:'
        FocusControl = EdBairro
      end
      object Label39: TLabel
        Left = 216
        Top = 136
        Width = 24
        Height = 13
        Caption = 'CEP:'
        FocusControl = EdCEP
      end
      object Label48: TLabel
        Left = 536
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Sexo:'
      end
      object Label124: TLabel
        Left = 588
        Top = 4
        Width = 57
        Height = 13
        Caption = 'Estado civil:'
      end
      object Label28: TLabel
        Left = 480
        Top = 176
        Width = 24
        Height = 13
        Caption = 'M'#227'e:'
        FocusControl = EdMae
      end
      object Label27: TLabel
        Left = 256
        Top = 176
        Width = 18
        Height = 13
        Caption = 'Pai:'
        FocusControl = EdPai
      end
      object Label83: TLabel
        Left = 528
        Top = 136
        Width = 42
        Height = 13
        Caption = 'Conjuge:'
        FocusControl = EdConjuge
      end
      object Label104: TLabel
        Left = 368
        Top = 48
        Width = 64
        Height = 13
        Caption = 'Cidade Natal:'
        FocusControl = EdCidadeNatal
      end
      object Label119: TLabel
        Left = 564
        Top = 48
        Width = 71
        Height = 13
        Caption = 'Nacionalidade:'
        FocusControl = EdNacionalid
      end
      object Label106: TLabel
        Left = 508
        Top = 48
        Width = 17
        Height = 13
        Caption = 'UF:'
      end
      object Label102: TLabel
        Left = 224
        Top = 48
        Width = 39
        Height = 13
        Caption = 'Emissor:'
        FocusControl = EdSSP
      end
      object Label118: TLabel
        Left = 280
        Top = 48
        Width = 42
        Height = 13
        Caption = 'Emiss'#227'o:'
      end
      object Label97: TLabel
        Left = 4
        Top = 92
        Width = 77
        Height = 13
        Caption = 'Tipo logradouro:'
      end
      object Label50: TLabel
        Left = 4
        Top = 176
        Width = 46
        Height = 13
        Caption = 'Profiss'#227'o:'
        FocusControl = EdProfissao
      end
      object Label51: TLabel
        Left = 120
        Top = 176
        Width = 31
        Height = 13
        Caption = 'Cargo:'
        FocusControl = EdCargo
      end
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 33
        Height = 13
        Caption = 'C'#243'digo'
        FocusControl = EdCodigo
      end
      object EdNome: TdmkEdit
        Left = 72
        Top = 20
        Width = 357
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdTe1: TdmkEdit
        Left = 296
        Top = 152
        Width = 113
        Height = 21
        TabOrder = 17
        FormatType = dmktfString
        MskType = fmtTelLongo
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCel: TdmkEdit
        Left = 412
        Top = 152
        Width = 113
        Height = 21
        TabOrder = 18
        FormatType = dmktfString
        MskType = fmtTelLongo
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object TPNatal: TDateTimePicker
        Left = 433
        Top = 20
        Width = 100
        Height = 21
        Date = 1.992729421297550000
        Time = 1.992729421297550000
        TabOrder = 1
      end
      object EdCNPJCPF: TdmkEdit
        Left = 4
        Top = 64
        Width = 113
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtCPFJ
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdIERG: TdmkEdit
        Left = 120
        Top = 64
        Width = 100
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdRua: TdmkEdit
        Left = 92
        Top = 108
        Width = 285
        Height = 21
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPNumero: TdmkEdit
        Left = 381
        Top = 108
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCompl: TdmkEdit
        Left = 448
        Top = 108
        Width = 257
        Height = 21
        TabOrder = 14
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdBairro: TdmkEdit
        Left = 4
        Top = 152
        Width = 209
        Height = 21
        TabOrder = 15
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCEP: TdmkEdit
        Left = 216
        Top = 152
        Width = 77
        Height = 21
        TabOrder = 16
        FormatType = dmktfString
        MskType = fmtCEP
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CBSexo: TComboBox
        Left = 536
        Top = 20
        Width = 49
        Height = 21
        Style = csDropDownList
        TabOrder = 2
        OnKeyDown = CBSexoKeyDown
        Items.Strings = (
          'M'
          'F')
      end
      object CBEstCivil: TDBLookupComboBox
        Left = 588
        Top = 20
        Width = 113
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEstCivil
        TabOrder = 3
      end
      object EdMae: TdmkEdit
        Left = 481
        Top = 192
        Width = 220
        Height = 21
        TabOrder = 23
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPai: TdmkEdit
        Left = 256
        Top = 192
        Width = 220
        Height = 21
        TabOrder = 22
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdConjuge: TdmkEdit
        Left = 528
        Top = 152
        Width = 173
        Height = 21
        TabOrder = 19
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCidadeNatal: TdmkEdit
        Left = 368
        Top = 64
        Width = 137
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNacionalid: TdmkEdit
        Left = 564
        Top = 64
        Width = 137
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CBPUF: TDBLookupComboBox
        Left = 508
        Top = 64
        Width = 52
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsUFs
        TabOrder = 9
      end
      object EdSSP: TdmkEdit
        Left = 224
        Top = 64
        Width = 53
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object TPDataRG: TDateTimePicker
        Left = 280
        Top = 64
        Width = 85
        Height = 21
        Date = 1.992729421297550000
        Time = 1.992729421297550000
        TabOrder = 7
      end
      object CBPLograd: TDBLookupComboBox
        Left = 4
        Top = 108
        Width = 85
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsListaLograd
        TabOrder = 11
      end
      object EdProfissao: TdmkEdit
        Left = 4
        Top = 192
        Width = 113
        Height = 21
        TabOrder = 20
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCargo: TdmkEdit
        Left = 120
        Top = 192
        Width = 133
        Height = 21
        TabOrder = 21
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 64
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 24
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object GroupBox4: TGroupBox
        Left = 4
        Top = 222
        Width = 437
        Height = 77
        Caption = ' Tipo de cadastro: '
        TabOrder = 25
        object CkCliente1_0: TCheckBox
          Left = 8
          Top = 20
          Width = 100
          Height = 17
          Caption = 'Cliente1'
          TabOrder = 0
        end
        object CkFornece1_0: TCheckBox
          Left = 115
          Top = 20
          Width = 100
          Height = 17
          Caption = 'Fornece 1'
          TabOrder = 2
        end
        object CkFornece2_0: TCheckBox
          Left = 115
          Top = 36
          Width = 100
          Height = 17
          Caption = 'Fornece 2'
          TabOrder = 3
          Visible = False
        end
        object CkFornece3_0: TCheckBox
          Left = 115
          Top = 52
          Width = 100
          Height = 17
          Caption = 'Fornece 3'
          TabOrder = 4
          Visible = False
        end
        object CkFornece4_0: TCheckBox
          Left = 222
          Top = 20
          Width = 100
          Height = 17
          Caption = 'Fornece 4'
          TabOrder = 5
          Visible = False
        end
        object CkTerceiro_0: TCheckBox
          Left = 329
          Top = 20
          Width = 100
          Height = 17
          Caption = 'Terceiro'
          TabOrder = 6
        end
        object CkCliente2_0: TCheckBox
          Left = 8
          Top = 36
          Width = 100
          Height = 17
          Caption = 'Cliente 2'
          TabOrder = 1
          Visible = False
        end
        object CkFornece5_0: TCheckBox
          Left = 222
          Top = 36
          Width = 100
          Height = 17
          Caption = 'Fornece 5'
          TabOrder = 7
          Visible = False
        end
        object CkFornece6_0: TCheckBox
          Left = 222
          Top = 52
          Width = 100
          Height = 17
          Caption = 'Fornece 6'
          TabOrder = 8
          Visible = False
        end
      end
    end
  end
  object QrDuplic2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM entidades'
      'WHERE CNPJ=:P0'
      'AND Codigo <> :P1')
    Left = 220
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplic2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrEstCivil: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM listaecivil'
      'ORDER BY Nome'
      '')
    Left = 8
    Top = 8
    object QrEstCivilCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstCivilNome: TWideStringField
      FieldName = 'Nome'
      Size = 10
    end
    object QrEstCivilLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEstCivilDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEstCivilDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEstCivilUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEstCivilUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsEstCivil: TDataSource
    DataSet = QrEstCivil
    Left = 36
    Top = 8
  end
  object DsUFs: TDataSource
    DataSet = QrUFs
    Left = 100
    Top = 8
  end
  object QrUFs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM ufs'
      'ORDER BY Nome')
    Left = 72
    Top = 9
    object QrUFsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrUFsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 2
    end
  end
  object QrListaLograd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM listalograd'
      'ORDER BY Nome')
    Left = 560
    Top = 6
    object QrListaLogradCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaLogradNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 10
    end
  end
  object DsListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 588
    Top = 6
  end
end
