unit EntiSocio1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, ComCtrls, Variants,
  dmkGeral, dmkEdit, dmkImage, UnDmkEnums;

type
  TFmEntiSocio1 = class(TForm)
    QrDuplic2: TmySQLQuery;
    QrDuplic2Codigo: TIntegerField;
    QrEstCivil: TmySQLQuery;
    QrEstCivilCodigo: TIntegerField;
    QrEstCivilNome: TWideStringField;
    QrEstCivilLk: TIntegerField;
    QrEstCivilDataCad: TDateField;
    QrEstCivilDataAlt: TDateField;
    QrEstCivilUserCad: TIntegerField;
    QrEstCivilUserAlt: TIntegerField;
    DsEstCivil: TDataSource;
    DsUFs: TDataSource;
    QrUFs: TmySQLQuery;
    QrUFsCodigo: TIntegerField;
    QrUFsNome: TWideStringField;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    DsListaLograd: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    GroupBox1: TGroupBox;
    PainelDados: TPanel;
    Label25: TLabel;
    Label43: TLabel;
    Label36: TLabel;
    Label45: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label39: TLabel;
    Label48: TLabel;
    Label124: TLabel;
    Label28: TLabel;
    Label27: TLabel;
    Label83: TLabel;
    Label104: TLabel;
    Label119: TLabel;
    Label106: TLabel;
    Label102: TLabel;
    Label118: TLabel;
    Label97: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label1: TLabel;
    EdNome: TdmkEdit;
    EdTe1: TdmkEdit;
    EdCel: TdmkEdit;
    TPNatal: TDateTimePicker;
    EdCNPJCPF: TdmkEdit;
    EdIERG: TdmkEdit;
    EdRua: TdmkEdit;
    EdPNumero: TdmkEdit;
    EdCompl: TdmkEdit;
    EdBairro: TdmkEdit;
    EdCEP: TdmkEdit;
    CBSexo: TComboBox;
    CBEstCivil: TDBLookupComboBox;
    EdMae: TdmkEdit;
    EdPai: TdmkEdit;
    EdConjuge: TdmkEdit;
    EdCidadeNatal: TdmkEdit;
    EdNacionalid: TdmkEdit;
    CBPUF: TDBLookupComboBox;
    EdSSP: TdmkEdit;
    TPDataRG: TDateTimePicker;
    CBPLograd: TDBLookupComboBox;
    EdProfissao: TdmkEdit;
    EdCargo: TdmkEdit;
    EdCodigo: TdmkEdit;
    GroupBox4: TGroupBox;
    CkCliente1_0: TCheckBox;
    CkFornece1_0: TCheckBox;
    CkFornece2_0: TCheckBox;
    CkFornece3_0: TCheckBox;
    CkFornece4_0: TCheckBox;
    CkTerceiro_0: TCheckBox;
    CkCliente2_0: TCheckBox;
    CkFornece5_0: TCheckBox;
    CkFornece6_0: TCheckBox;
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CBSexoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmEntiSocio1: TFmEntiSocio1;

implementation

uses UnMyObjects, Module, Entidades, ResIntStrings;

{$R *.DFM}

procedure TFmEntiSocio1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiSocio1.BtConfirmaClick(Sender: TObject);
var
  PLograd, Tipo, Codigo, PUF: Integer;
  //
  Nome, CPF, RG, PRua, PBairro, PCompl, PCidade, PCEP, PTe1, PCel, PNatal: String;
  PNum, EstCivil: Integer;
  //
  ConjugeNome, Profissao, Cargo, Pai, Mae: String;
  Cliente1, Cliente2, Fornece1, Fornece2, Fornece3, Fornece4, Fornece5,
  Fornece6, Terceiro: String;
begin
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Entidades',
              'Entidades', 'Codigo');
    Dmod.QrUpdU.SQL.Add('INSERT INTO entidades SET ');
  end else begin
    Codigo := EdCodigo.ValueVariant;
    Dmod.QrUpdU.SQL.Add('UPDATE entidades SET AlterWeb=1, ');
  end;
  ////////////////////////////////////////////////////////////////////////////
  Tipo := 1;
  if (Length(Geral.SoNumero1a9_TT(EdCNPJCPF.Text))>0)
  then begin
    QrDuplic2.Close;
    QrDuplic2.SQL.Clear;
    if Tipo = 0 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CNPJ="'+Geral.SoNumero_TT(EdCNPJCPF.Text)+'"');
      QrDuplic2.SQL.Add('AND Codigo <> '+IntToStr(Codigo));
    end;
    if Tipo = 1 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CPF="'+Geral.SoNumero_TT(EdCNPJCPF.Text)+'"');
      QrDuplic2.SQL.Add('AND Codigo <> '+IntToStr(Codigo));
    end;
    QrDuplic2.Open;
    if GOTOy.Registros(QrDuplic2) > 0 then
    begin
      Application.MessageBox(PChar('Esta entidade j� foi cadastrada com o c�digo n� '+
      IntToStr(QrDuplic2Codigo.Value)+'.'), 'Entidade duplicada', MB_OK+MB_ICONWARNING);
      QrDuplic2.Close;
      Screen.Cursor := crDefault;
      Exit;
    end;
    QrDuplic2.Close;
  end;
  Nome        := EdNome.Text;
  CPF         := MLAGeral.FormataCNPJ_TFT(EdCNPJCPF.Text);
  RG          := EdIERG.Text;
  PRua        := EdRua.Text;
  PNum        := EdPNumero.ValueVariant;
  PCompl      := EdCompl.ValueVariant;
  PBairro     := EdBairro.Text;
  PCidade     := VAR_CIDADEPADRAO;
  ConjugeNome := EdConjuge.Text;
  Profissao   := EdProfissao.Text;
  Cargo       := EdCargo.Text;
  Pai         := EdPai.Text;
  Mae         := EdMae.Text;
  PCEP        := Geral.SoNumero_TT(EdCEP.Text);
  PTe1        := MlaGeral.SoNumeroESinal_TT(EdTe1.Text);
  PCel        := MlaGeral.SoNumeroESinal_TT(EdCel.Text);
  PNatal      := FormatDateTime(VAR_FORMATDATE, TPNatal.Date);
  if CBPUF.KeyValue = NULL then PUF := 0 else PUF := CBPUF.KeyValue;
  if CBEstCivil.KeyValue <> NULL then EstCivil := CBEstCivil.KeyValue else EstCivil := 0;
  if CBPLograd.KeyValue = NULL then PLograd := 0 else PLograd := CBPLograd.KeyValue;
  if CkCliente1_0.Checked then Cliente1 := 'V' else Cliente1 := 'F';
  if CkCliente2_0.Checked then Cliente2 := 'V' else Cliente2 := 'F';
  if CkFornece1_0.Checked then Fornece1 := 'V' else Fornece1 := 'F';
  if CkFornece2_0.Checked then Fornece2 := 'V' else Fornece2 := 'F';
  if CkFornece3_0.Checked then Fornece3 := 'V' else Fornece3 := 'F';
  if CkFornece4_0.Checked then Fornece4 := 'V' else Fornece4 := 'F';
  if CkFornece5_0.Checked then Fornece5 := 'V' else Fornece5 := 'F';
  if CkFornece6_0.Checked then Fornece6 := 'V' else Fornece6 := 'F';
  if CkTerceiro_0.Checked then Terceiro := 'V' else Terceiro := 'F';
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //Dmod.QrUpdU.SQL.Add(VAR_REPRES_ENTIJUR1+' = "V"'); //especified twice
  Dmod.QrUpdU.SQL.Add('  Nome              =:P00');
  Dmod.QrUpdU.SQL.Add(', PNatal            =:P01');
  Dmod.QrUpdU.SQL.Add(', Sexo              =:P02');
  Dmod.QrUpdU.SQL.Add(', EstCivil          =:P03');
  Dmod.QrUpdU.SQL.Add(', CPF               =:P04');
  Dmod.QrUpdU.SQL.Add(', RG                =:P05');
  Dmod.QrUpdU.SQL.Add(', SSP               =:P06');
  Dmod.QrUpdU.SQL.Add(', DataRG            =:P07');
  Dmod.QrUpdU.SQL.Add(', CidadeNatal       =:P08');
  Dmod.QrUpdU.SQL.Add(', PUF               =:P09');
  Dmod.QrUpdU.SQL.Add(', Nacionalid        =:P10');
  Dmod.QrUpdU.SQL.Add(', PLograd           =:P11');
  Dmod.QrUpdU.SQL.Add(', PRua              =:P12');
  Dmod.QrUpdU.SQL.Add(', PNumero           =:P13');
  Dmod.QrUpdU.SQL.Add(', PCompl            =:P14');
  Dmod.QrUpdU.SQL.Add(', PBairro           =:P15');
  Dmod.QrUpdU.SQL.Add(', PCEP              =:P16');
  Dmod.QrUpdU.SQL.Add(', PTe1              =:P17');
  Dmod.QrUpdU.SQL.Add(', PCel              =:P18');
  Dmod.QrUpdU.SQL.Add(', ConjugeNome       =:P19');
  Dmod.QrUpdU.SQL.Add(', Profissao         =:P20');
  Dmod.QrUpdU.SQL.Add(', Cargo             =:P21');
  Dmod.QrUpdU.SQL.Add(', Pai               =:P22');
  Dmod.QrUpdU.SQL.Add(', Mae               =:P23');
  Dmod.QrUpdU.SQL.Add(', Cliente1          =:P24');
  Dmod.QrUpdU.SQL.Add(', Cliente2          =:P25');
  Dmod.QrUpdU.SQL.Add(', Fornece1          =:P26');
  Dmod.QrUpdU.SQL.Add(', Fornece2          =:P27');
  Dmod.QrUpdU.SQL.Add(', Fornece3          =:P28');
  Dmod.QrUpdU.SQL.Add(', Fornece4          =:P29');
  Dmod.QrUpdU.SQL.Add(', Fornece5          =:P30');
  Dmod.QrUpdU.SQL.Add(', Fornece6          =:P31');
  Dmod.QrUpdU.SQL.Add(', Terceiro          =:P32');
  Dmod.QrUpdU.SQL.Add(', CodUsu            =:P33');
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add(', Codigo          =:Pa')
  else
    Dmod.QrUpdU.SQL.Add(' WHERE Codigo     =:Pa');

  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsString  := PNatal;
  Dmod.QrUpdU.Params[02].AsString  := CBSexo.Text;
  Dmod.QrUpdU.Params[03].AsInteger := EstCivil;
  Dmod.QrUpdU.Params[04].AsString  := CPF;
  Dmod.QrUpdU.Params[05].AsString  := RG;
  Dmod.QrUpdU.Params[06].AsString  := EdSSP.Text;
  Dmod.QrUpdU.Params[07].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataRG.Date);
  Dmod.QrUpdU.Params[08].AsString  := EdCidadeNatal.Text;
  Dmod.QrUpdU.Params[09].AsInteger := PUF;
  Dmod.QrUpdU.Params[10].AsString  := EdNacionalid.Text;
  Dmod.QrUpdU.Params[11].AsInteger := PLograd;
  Dmod.QrUpdU.Params[12].AsString  := PRua;
  Dmod.QrUpdU.Params[13].AsInteger := PNum;
  Dmod.QrUpdU.Params[14].AsString  := PCompl;
  Dmod.QrUpdU.Params[15].AsString  := PBairro;
  Dmod.QrUpdU.Params[16].AsString  := PCEP;
  Dmod.QrUpdU.Params[17].AsString  := PTe1;
  Dmod.QrUpdU.Params[18].AsString  := PCel;
  Dmod.QrUpdU.Params[19].AsString  := ConjugeNome;
  Dmod.QrUpdU.Params[20].AsString  := Profissao;
  Dmod.QrUpdU.Params[21].AsString  := Cargo;
  Dmod.QrUpdU.Params[22].AsString  := Pai;
  Dmod.QrUpdU.Params[23].AsString  := Mae;
  Dmod.QrUpdU.Params[24].AsString  := Cliente1;
  Dmod.QrUpdU.Params[25].AsString  := Cliente2;
  Dmod.QrUpdU.Params[26].AsString  := Fornece1;
  Dmod.QrUpdU.Params[27].AsString  := Fornece2;
  Dmod.QrUpdU.Params[28].AsString  := Fornece3;
  Dmod.QrUpdU.Params[29].AsString  := Fornece4;
  Dmod.QrUpdU.Params[30].AsString  := Fornece5;
  Dmod.QrUpdU.Params[31].AsString  := Fornece6;
  Dmod.QrUpdU.Params[32].AsString  := Terceiro;
  Dmod.QrUpdU.Params[33].AsInteger := Codigo;
  //
  Dmod.QrUpdU.Params[34].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  VAR_ENTIDADE := Codigo;
  Screen.Cursor := crDefault;
  Close;
end;

procedure TFmEntiSocio1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiSocio1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  QrEstCivil.Open;
end;

procedure TFmEntiSocio1.CBSexoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=13 then CBEstCivil.SetFocus;
end;

procedure TFmEntiSocio1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrEstCivil.Open;
  QrUFs.Open;
  QrListaLograd.Open;
  //
  CkCliente1_0.Caption := VAR_CLIENTE1;
  CkCliente2_0.Caption := VAR_CLIENTE2;
  CkFornece1_0.Caption := VAR_FORNECE1;
  CkFornece2_0.Caption := VAR_FORNECE2;
  CkFornece3_0.Caption := VAR_FORNECE3;
  CkFornece4_0.Caption := VAR_FORNECE4;
  CkFornece5_0.Caption := VAR_FORNECE5;
  CkFornece6_0.Caption := VAR_FORNECE6;
  CkTerceiro_0.Caption := VAR_TERCEIR2;
  //
  if VAR_CLIENTE2 <> '' then CkCliente2_0.Visible := True;
  if VAR_FORNECE1 <> '' then CkFornece1_0.Visible := True;
  if VAR_FORNECE2 <> '' then CkFornece2_0.Visible := True;
  if VAR_FORNECE3 <> '' then CkFornece3_0.Visible := True;
  if VAR_FORNECE4 <> '' then CkFornece4_0.Visible := True;
  if VAR_FORNECE5 <> '' then CkFornece5_0.Visible := True;
  if VAR_FORNECE6 <> '' then CkFornece6_0.Visible := True;
  //
end;

end.

