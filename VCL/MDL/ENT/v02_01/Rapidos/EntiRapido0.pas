unit EntiRapido0;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, Menus, dmkCheckBox, UnDmkEnums;

type
  TFmEntiRapido0 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    QrLoc: TmySQLQuery;
    QrLocCNPJ_CPF: TWideStringField;
    Panel5: TPanel;
    RGTipo: TRadioGroup;
    PnDados1: TPanel;
    Label25: TLabel;
    Label30: TLabel;
    Label29: TLabel;
    EdCEP: TdmkEdit;
    Label12: TLabel;
    BtCEP: TBitBtn;
    EdRazaoNome: TdmkEdit;
    EdCNPJCPF: TdmkEdit;
    EdIERG: TdmkEdit;
    PnDados2: TPanel;
    Label97: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label38: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label106: TLabel;
    Label109: TLabel;
    Label112: TLabel;
    CBLograd: TdmkDBLookupComboBox;
    EdRua: TdmkEdit;
    EdNumero: TdmkEdit;
    EdBairro: TdmkEdit;
    EdCidade: TdmkEdit;
    EdUF: TdmkEdit;
    EdPais: TdmkEdit;
    EdLograd: TdmkEditCB;
    EdCompl: TdmkEdit;
    EdEndeRef: TdmkEdit;
    EdCodMunici: TdmkEditCB;
    CBCodMunici: TdmkDBLookupComboBox;
    EdCodiPais: TdmkEditCB;
    CBCodiPais: TdmkDBLookupComboBox;
    EdTe1: TdmkEdit;
    PMCEP: TPopupMenu;
    Descobrir1: TMenuItem;
    Verificar1: TMenuItem;
    Internet1: TMenuItem;
    QrDuplic2: TmySQLQuery;
    QrDuplic2Codigo: TIntegerField;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    DsListaLograd: TDataSource;
    QrMunici: TmySQLQuery;
    QrMuniciCodigo: TIntegerField;
    QrMuniciNome: TWideStringField;
    DsMunici: TDataSource;
    QrBacen_Pais: TmySQLQuery;
    QrBacen_PaisCodigo: TIntegerField;
    QrBacen_PaisNome: TWideStringField;
    DsBacen_Pais: TDataSource;
    GroupBox4: TGroupBox;
    CkCliente1_0: TdmkCheckBox;
    CkFornece1_0: TdmkCheckBox;
    CkFornece2_0: TdmkCheckBox;
    CkFornece3_0: TdmkCheckBox;
    CkFornece4_0: TdmkCheckBox;
    CkTerceiro_0: TdmkCheckBox;
    CkCliente2_0: TdmkCheckBox;
    CkFornece5_0: TdmkCheckBox;
    CkFornece6_0: TdmkCheckBox;
    CkCliente4_0: TdmkCheckBox;
    CkCliente3_0: TdmkCheckBox;
    CkFornece8_0: TdmkCheckBox;
    CkFornece7_0: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdCEPExit(Sender: TObject);
    procedure EdCEPEnter(Sender: TObject);
    procedure EdCNPJCPFExit(Sender: TObject);
    procedure BtCEPClick(Sender: TObject);
    procedure Descobrir1Click(Sender: TObject);
    procedure Verificar1Click(Sender: TObject);
    procedure Internet1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdRazaoNomeChange(Sender: TObject);
  private
    { Private declarations }
    FCEP: String;
    //
    function  CNPJCPFDuplicado(): Boolean;
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmEntiRapido0: TFmEntiRapido0;

implementation

uses UnMyObjects, Module, UMySQLModule,
//EntiCEP,
MyDBCheck, UnCEP, UnEntities,
  ModuleGeral;

{$R *.DFM}

procedure TFmEntiRapido0.BtCEPClick(Sender: TObject);
begin
  MyObjects.MostraPMCEP(PMCEP, BtCEP);
  try
    EdNumero.SetFocus;
  except
    ;
  end;
end;

procedure TFmEntiRapido0.BtOKClick(Sender: TObject);
var
  Codigo, CodUsu, Tipo, ECEP, ELograd, ENumero, EUF, ECodMunici, ECodiPais,
  PCEP, PLograd, PNumero, PUF, PCodMunici, PCodiPais: Integer;
  //
  Cliente1, Cliente2, Cliente3, Cliente4, Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6, Fornece7, Fornece8, Terceiro,
  RazaoSocial, CNPJ, IE, ERua, ECompl, EBairro, ECidade, EPais, ETe1, EEndeRef,
  Nome, CPF, RG, PRua, PCompl, PBairro, PCidade, PPais, PTe1, PEndeRef: String;
begin
  if CNPJCPFDuplicado() then
    Exit;
  Tipo := RGTipo.ItemIndex -1;
  //
  if Tipo = 0 then
    if Geral.Valida_IE(EdIERG.Text, EdUF.Text, '??', False) = False then
    begin
      if Geral.MB_Pergunta(
      'Deseja continuar mesmo com d�vidas na valida��o da Inscri��o Estadual?') <> ID_YES then
    Exit;
  end;
  //
  if not Entities.TipoDeCadastroDefinido(CkCliente1_0, CkCliente2_0,
  CkCliente3_0, CkCliente4_0, CkFornece1_0, CkFornece2_0, CkFornece3_0,
  CkFornece4_0, CkFornece5_0, CkFornece6_0, CkFornece7_0, CkFornece8_0,
  CkTerceiro_0, nil) then
    Exit;
  RazaoSocial  := '';
  CNPJ         := '';
  IE           := '';
  ECEP         := 0;
  ELograd      := 0;
  ERua         := '';
  ENumero      := 0;
  ECompl       := '';
  EBairro      := '';
  ECidade      := '';
  EUF          := 0;
  EPais        := '';
  ETe1         := '';
  EEndeRef     := '';
  ECodMunici   := 0;
  ECodiPais    := 0;
  //
  Nome         := '';
  CPF          := '';
  RG           := '';
  PCEP         := 0;
  PLograd      := 0;
  PRua         := '';
  PNumero      := 0;
  PCompl       := '';
  PBairro      := '';
  PCidade      := '';
  PUF          := 0;
  PPais        := '';
  PTe1         := '';
  PEndeRef     := '';
  PCodMunici   := 0;
  PCodiPais    := 0;
  //
  case Tipo of
    0:
    begin
      RazaoSocial  := EdRazaoNome.Text;
      CNPJ         := Geral.SoNumero_TT(EdCNPJCPF.Text);
      IE           := Geral.SoNumero_TT(EdIERG.Text);
      //
      ECEP         := Geral.IMV(Geral.SoNumero_TT(EdCEP.ValueVariant));
      ELograd      := EdLograd.ValueVariant;
      ERua         := EdRua.Text;
      ENumero      := Geral.IMV(Geral.SoNumero_TT(EdNumero.Text));
      ECompl       := EdCompl.Text;
      EBairro      := EdBairro.Text;
      ECidade      := EdCidade.Text;
      EUF          := Geral.GetCodigoUF_da_SiglaUF(EdUF.Text);
      EPais        := EdPais.Text;
      ETe1         := Geral.SoNumero_TT(EdTe1.Text);
      EEndeRef     := EdEndeRef.Text;
      ECodMunici   := EdCodMunici.ValueVariant;
      ECodiPais    := EdCodiPais.ValueVariant;
    end;
    1:
    begin
      Nome         := EdRazaoNome.Text;
      CPF          := Geral.SoNumero_TT(EdCNPJCPF.Text);
      RG           := Geral.SoNumero_TT(EdIERG.Text);
      //
      PCEP         := Geral.IMV(Geral.SoNumero_TT(EdCEP.Text));
      PLograd      := EdLograd.ValueVariant;
      PRua         := EdRua.Text;
      PNumero      := Geral.IMV(Geral.SoNumero_TT(EdNumero.Text));
      PCompl       := EdCompl.Text;
      PBairro      := EdBairro.Text;
      PCidade      := EdCidade.Text;
      PUF          := Geral.GetCodigoUF_da_SiglaUF(EdUF.Text);
      PPais        := EdPais.Text;
      PTe1         := Geral.SoNumero_TT(EdTe1.Text);
      PEndeRef     := EdEndeRef.Text;
      PCodMunici   := EdCodMunici.ValueVariant;
      PCodiPais    := EdCodiPais.ValueVariant;
    end;
    else
      Exit;
  end;
  if CkCliente1_0.Checked then Cliente1 := 'V' else Cliente1 := 'F';
  if CkCliente2_0.Checked then Cliente2 := 'V' else Cliente2 := 'F';
  if CkCliente3_0.Checked then Cliente3 := 'V' else Cliente3 := 'F';
  if CkCliente4_0.Checked then Cliente4 := 'V' else Cliente4 := 'F';
  if CkFornece1_0.Checked then Fornece1 := 'V' else Fornece1 := 'F';
  if CkFornece2_0.Checked then Fornece2 := 'V' else Fornece2 := 'F';
  if CkFornece3_0.Checked then Fornece3 := 'V' else Fornece3 := 'F';
  if CkFornece4_0.Checked then Fornece4 := 'V' else Fornece4 := 'F';
  if CkFornece5_0.Checked then Fornece5 := 'V' else Fornece5 := 'F';
  if CkFornece6_0.Checked then Fornece6 := 'V' else Fornece6 := 'F';
  if CkFornece7_0.Checked then Fornece7 := 'V' else Fornece7 := 'F';
  if CkFornece8_0.Checked then Fornece8 := 'V' else Fornece8 := 'F';
  if CkTerceiro_0.Checked then Terceiro := 'V' else Terceiro := 'F';
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('Entidades', 'Codigo', ImgTipo.SQLType, FCodigo);
  CodUsu := Codigo;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'entidades', False, [
  'Cliente1', 'Cliente2', 'Cliente3', 'Cliente4',
  'Fornece1', 'Fornece2', 'Fornece3', 'Fornece4',
  'Fornece5', 'Fornece6', 'Fornece7', 'Fornece8',
  'Terceiro', 'Tipo',
  'RazaoSocial', 'CNPJ', 'IE', 'ECEP', 'ELograd', 'ERua',
  'ENumero', 'ECompl', 'EBairro', 'ECidade', 'EUF',
  'EPais', 'ETe1', 'EEndeRef', 'ECodMunici', 'ECodiPais',
  'Nome', 'CPF', 'RG', 'PCEP', 'PLograd', 'PRua',
  'PNumero', 'PCompl', 'PBairro', 'PCidade', 'PUF',
  'PPais', 'PTe1', 'PEndeRef', 'PCodMunici', 'PCodiPais'
  ], ['Codigo', 'CodUsu'
  ], [
  Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4,
  Fornece5, Fornece6, Fornece7, Fornece8,
  Terceiro, Tipo,
  RazaoSocial, CNPJ, IE, ECEP, ELograd, ERua,
  ENumero, ECompl, EBairro, ECidade, EUF,
  EPais, ETe1, EEndeRef, ECodMunici, ECodiPais,
  Nome, CPF, RG, PCEP, PLograd, PRua,
  PNumero, PCompl, PBairro, PCidade, PUF,
  PPais, PTe1, PEndeRef, PCodMunici, PCodiPais
  ], [Codigo, CodUsu
  ], True) then
  begin
    FCodigo := Codigo;
    Close;
  end;
end;

procedure TFmEntiRapido0.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmEntiRapido0.CNPJCPFDuplicado(): Boolean;
var
  CNPJCPF: String;
  //Tipo: Integer;
begin
  Result := False;
  CNPJCPF := Geral.SoNumero_TT(EdCNPJCPF.Text);
  if Length(CNPJCPF) > 0 then
  begin
    QrDuplic2.Close;
    QrDuplic2.SQL.Clear;
    {
    if Length(CNPJCPF) < 14 then
      Tipo := 1
    else
      Tipo := 0;
    //
    if Tipo = 0 then
    begin
    }
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CPF="' + CNPJCPF + '"');
      QrDuplic2.SQL.Add('OR CNPJ="' + CNPJCPF + '"');
      //QrDuplic2.SQL.Add('AND Codigo <> '+EdCodigo.Text);
    {
    end;
    if Tipo = 1 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CPF="' + CNPJCPF + '"');
      //QrDuplic2.SQL.Add('AND Codigo <> '+EdCodigo.Text);
    end;
    }
    QrDuplic2.Open;
    if QrDuplic2.RecordCount > 0 then
    begin
      Result := True;
      Geral.MB_Aviso('Esta entidade j� foi cadastrada com o c�digo n� '+
      IntToStr(QrDuplic2Codigo.Value)+'.');
      QrDuplic2.Close;
      Screen.Cursor := crDefault;
      Exit;
    end;
    QrDuplic2.Close;
  end;
end;

procedure TFmEntiRapido0.Descobrir1Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    FmEntiCEP.DBGrid2.Visible := False;
    FmEntiCEP.Painel2.Visible := False;
    FmEntiCEP.ShowModal;
    FmEntiCEP.Destroy;
  end;
}
end;

procedure TFmEntiRapido0.EdCNPJCPFExit(Sender: TObject);
begin
  CNPJCPFDuplicado();
end;

procedure TFmEntiRapido0.EdRazaoNomeChange(Sender: TObject);
begin
  RGTipoClick(Self);
end;

procedure TFmEntiRapido0.EdCEPEnter(Sender: TObject);
begin
  FCEP := Geral.SoNumero_TT(EdCEP.Text);
end;

procedure TFmEntiRapido0.EdCEPExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdCEP.Text);
  if CEP <> FCEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
        EdCidade, EdUF, EdPais, EdNumero, EdCompl, CBLograd,
        EdCodMunici, CBCodMunici, EdCodiPais, CBCodiPais);
  end;
end;

procedure TFmEntiRapido0.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiRapido0.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  //
  FCEP := '';
  FCodigo := 0;
  //
  QrListaLograd.Open;
  UMyMod.AbreQuery(QrMunici, DModG.AllID_DB);
  UMyMod.AbreQuery(QrBacen_Pais, DModG.AllID_DB);
  //
  Entities.ConfiguraTipoDeCadastro(CkCliente1_0, CkCliente2_0, CkCliente3_0,
    CkCliente4_0, CkFornece1_0, CkFornece2_0, CkFornece3_0, CkFornece4_0,
    CkFornece5_0, CkFornece6_0, CkFornece7_0, CkFornece8_0, CkTerceiro_0);
end;

procedure TFmEntiRapido0.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiRapido0.Internet1Click(Sender: TObject);
begin
  U_CEP.ConsultaCEP2(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
    EdCidade, EdUF, EdPais, EdNumero, EdCompl, CBLograd, CBCodMunici,
    CBCodiPais, EdCodMunici, EdCodiPais);
end;

procedure TFmEntiRapido0.RGTipoClick(Sender: TObject);
var
  OK: Boolean;
begin
  OK := RGTipo.ItemIndex > 0;
  BtOK.Enabled := OK and (Trim(EdRazaoNome.Text) <> '');
  PnDados1.Visible := OK;
  PnDados2.Visible := OK;
end;

procedure TFmEntiRapido0.Verificar1Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    with FmEntiCEP do
    begin
      DBGrid1.Visible := False;
      Painel1.Visible := False;
      case VAR_ACTIVEPAGE of
        1: EdCEPLoc.Text := EdCEP.Text;
        (*  Parei Aqui! falta fazer
        2: EdCEPLoc.Text := EdECEP.Text;
        5: EdCEPLoc.Text := EdCCEP.Text;
        6: EdCEPLoc.Text := EdLCEP.Text;
        *)
      end;
      ShowModal;
      Destroy;
    end;
  end;
}
end;

end.
