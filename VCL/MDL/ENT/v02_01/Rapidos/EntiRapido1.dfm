object FmEntiRapido1: TFmEntiRapido1
  Left = 361
  Top = 204
  Caption = 'Cadastro R'#225'pido de Cliente Jur'#237'dico'
  ClientHeight = 404
  ClientWidth = 717
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 717
    Height = 316
    Align = alClient
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 715
      Height = 164
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label25: TLabel
        Left = 4
        Top = 4
        Width = 66
        Height = 13
        Caption = 'Raz'#227'o Social:'
        FocusControl = EdRazaoSocial
      end
      object Label29: TLabel
        Left = 4
        Top = 44
        Width = 30
        Height = 13
        Caption = 'CNPJ:'
        FocusControl = EdCNPJ
      end
      object Label98: TLabel
        Left = 4
        Top = 84
        Width = 77
        Height = 13
        Caption = 'Tipo logradouro:'
      end
      object Label31: TLabel
        Left = 112
        Top = 84
        Width = 84
        Height = 13
        Caption = 'Nome logradouro:'
        FocusControl = EdERua
      end
      object Label30: TLabel
        Left = 120
        Top = 44
        Width = 19
        Height = 13
        Caption = 'I.E.:'
        FocusControl = EdIE
      end
      object Label121: TLabel
        Left = 504
        Top = 4
        Width = 80
        Height = 13
        Caption = 'Forma societ'#225'ria:'
        FocusControl = EdFormaSociet
      end
      object Label33: TLabel
        Left = 364
        Top = 84
        Width = 67
        Height = 13
        Caption = 'Complemento:'
        FocusControl = EdECompl
      end
      object Label32: TLabel
        Left = 312
        Top = 84
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
        FocusControl = EdENumero
      end
      object Label36: TLabel
        Left = 412
        Top = 124
        Width = 20
        Height = 13
        Caption = 'Fax:'
        FocusControl = EdEFax
      end
      object Label123: TLabel
        Left = 528
        Top = 124
        Width = 31
        Height = 13
        Caption = 'E-mail:'
        FocusControl = EdEEMail
      end
      object Label43: TLabel
        Left = 296
        Top = 124
        Width = 45
        Height = 13
        Caption = 'Telefone:'
      end
      object Label39: TLabel
        Left = 216
        Top = 124
        Width = 24
        Height = 13
        Caption = 'CEP:'
        FocusControl = EdECEP
      end
      object Label34: TLabel
        Left = 552
        Top = 84
        Width = 30
        Height = 13
        Caption = 'Bairro:'
        FocusControl = EdEBairro
      end
      object Label122: TLabel
        Left = 224
        Top = 44
        Width = 29
        Height = 13
        Caption = 'NIRE:'
        FocusControl = EdNIRE
      end
      object Label45: TLabel
        Left = 328
        Top = 44
        Width = 74
        Height = 13
        Caption = 'Cadastrado em:'
      end
      object Label1: TLabel
        Left = 432
        Top = 44
        Width = 62
        Height = 13
        Caption = 'Fundada em:'
      end
      object Label14: TLabel
        Left = 162
        Top = 124
        Width = 17
        Height = 13
        Caption = 'UF:'
      end
      object Label2: TLabel
        Left = 4
        Top = 124
        Width = 36
        Height = 13
        Caption = 'Cidade:'
        FocusControl = EdECidade
      end
      object EdRazaoSocial: TdmkEdit
        Left = 4
        Top = 20
        Width = 497
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCNPJ: TdmkEdit
        Left = 4
        Top = 60
        Width = 113
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = EdCNPJExit
      end
      object CBELograd: TDBLookupComboBox
        Left = 4
        Top = 100
        Width = 105
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsListaLograd
        TabOrder = 8
      end
      object EdERua: TdmkEdit
        Left = 112
        Top = 100
        Width = 197
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdIE: TdmkEdit
        Left = 120
        Top = 60
        Width = 100
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdFormaSociet: TdmkEdit
        Left = 504
        Top = 20
        Width = 209
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object CkSimples: TCheckBox
        Left = 536
        Top = 64
        Width = 61
        Height = 17
        Caption = 'Simples.'
        TabOrder = 7
      end
      object EdECompl: TdmkEdit
        Left = 364
        Top = 100
        Width = 185
        Height = 21
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdENumero: TdmkEdit
        Left = 313
        Top = 100
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdEFax: TdmkEdit
        Left = 412
        Top = 140
        Width = 113
        Height = 21
        TabOrder = 17
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = EdEFaxExit
      end
      object EdEEMail: TdmkEdit
        Left = 528
        Top = 140
        Width = 185
        Height = 21
        TabOrder = 18
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdETe1: TdmkEdit
        Left = 296
        Top = 140
        Width = 113
        Height = 21
        TabOrder = 16
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = EdETe1Exit
      end
      object EdECEP: TdmkEdit
        Left = 216
        Top = 140
        Width = 77
        Height = 21
        TabOrder = 15
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = EdECEPExit
      end
      object EdEBairro: TdmkEdit
        Left = 552
        Top = 100
        Width = 161
        Height = 21
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdNIRE: TdmkEdit
        Left = 224
        Top = 60
        Width = 100
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object TPCadastro: TDateTimePicker
        Left = 328
        Top = 60
        Width = 100
        Height = 21
        Date = 1.992729421297550000
        Time = 1.992729421297550000
        TabOrder = 5
      end
      object TPENatal: TDateTimePicker
        Left = 432
        Top = 60
        Width = 100
        Height = 21
        Date = 1.992729421297550000
        Time = 1.992729421297550000
        TabOrder = 6
      end
      object CBEUF: TDBLookupComboBox
        Left = 162
        Top = 140
        Width = 52
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 14
      end
      object EdECidade: TdmkEdit
        Left = 4
        Top = 140
        Width = 155
        Height = 21
        TabOrder = 13
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object Grade1: TStringGrid
      Left = 1
      Top = 165
      Width = 715
      Height = 150
      Align = alClient
      DefaultRowHeight = 19
      RowCount = 7
      TabOrder = 1
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 356
    Width = 717
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 18
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 610
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 717
    Height = 40
    Align = alTop
    Caption = 'Cadastro R'#225'pido de Cliente Jur'#237'dico'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 715
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 713
      ExplicitHeight = 36
    end
  end
  object QrDuplic2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM entidades'
      'WHERE CNPJ=:P0'
      'AND Codigo <> :P1')
    Left = 644
    Top = 5
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplic2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 104
    Top = 230
  end
  object QrListaLograd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM listalograd'
      'ORDER BY Nome')
    Left = 80
    Top = 230
    object QrListaLogradCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaLogradNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 10
    end
  end
end
