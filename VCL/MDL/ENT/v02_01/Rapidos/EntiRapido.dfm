object FmEntiRapido: TFmEntiRapido
  Left = 353
  Top = 367
  Caption = 'Cadastro R'#225'pido'
  ClientHeight = 223
  ClientWidth = 709
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 709
    Height = 135
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 717
    ExplicitHeight = 137
    object Label25: TLabel
      Left = 4
      Top = 4
      Width = 31
      Height = 13
      Caption = 'Nome:'
      FocusControl = EdNome
    end
    object Label43: TLabel
      Left = 296
      Top = 92
      Width = 103
      Height = 13
      Caption = 'Telefone Residencial:'
    end
    object Label36: TLabel
      Left = 412
      Top = 92
      Width = 35
      Height = 13
      Caption = 'Celular:'
      FocusControl = EdCel
    end
    object Label45: TLabel
      Left = 392
      Top = 4
      Width = 59
      Height = 13
      Caption = 'Nascimento:'
    end
    object Label29: TLabel
      Left = 496
      Top = 4
      Width = 61
      Height = 13
      Caption = 'CNPJ / CPF:'
      FocusControl = EdCNPJCPF
    end
    object Label30: TLabel
      Left = 612
      Top = 4
      Width = 46
      Height = 13
      Caption = 'I.E. / RG:'
      FocusControl = EdIERG
    end
    object Label31: TLabel
      Left = 4
      Top = 48
      Width = 84
      Height = 13
      Caption = 'Nome logradouro:'
      FocusControl = EdRua
    end
    object Label32: TLabel
      Left = 368
      Top = 48
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
      FocusControl = EdNumero
    end
    object Label33: TLabel
      Left = 436
      Top = 48
      Width = 67
      Height = 13
      Caption = 'Complemento:'
      FocusControl = EdCompl
    end
    object Label34: TLabel
      Left = 4
      Top = 92
      Width = 30
      Height = 13
      Caption = 'Bairro:'
      FocusControl = EdBairro
    end
    object Label39: TLabel
      Left = 216
      Top = 92
      Width = 24
      Height = 13
      Caption = 'CEP:'
      FocusControl = EdCEP
    end
    object EdNome: TLMDEdit
      Left = 4
      Top = 20
      Width = 385
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 0
      AutoSelect = True
      CustomButtons = <>
      PasswordChar = #0
    end
    object EdTe1: TLMDEdit
      Left = 296
      Top = 108
      Width = 113
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 9
      OnExit = EdTe1Exit
      AutoSelect = True
      CustomButtons = <>
      PasswordChar = #0
    end
    object EdCel: TLMDEdit
      Left = 412
      Top = 108
      Width = 113
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 10
      OnExit = EdCelExit
      AutoSelect = True
      CustomButtons = <>
      PasswordChar = #0
    end
    object TPNatal: TDateTimePicker
      Left = 393
      Top = 20
      Width = 100
      Height = 21
      Date = 1.992729421297550000
      Time = 1.992729421297550000
      TabOrder = 1
    end
    object EdCNPJCPF: TLMDEdit
      Left = 496
      Top = 20
      Width = 113
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 2
      OnExit = EdCNPJCPFExit
      AutoSelect = True
      CustomButtons = <>
      PasswordChar = #0
    end
    object EdIERG: TLMDEdit
      Left = 612
      Top = 20
      Width = 100
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 3
      AutoSelect = True
      CustomButtons = <>
      PasswordChar = #0
    end
    object EdRua: TLMDEdit
      Left = 4
      Top = 64
      Width = 361
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 4
      AutoSelect = True
      CustomButtons = <>
      PasswordChar = #0
    end
    object EdNumero: TLMDEdit
      Left = 369
      Top = 64
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 5
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
    end
    object EdCompl: TLMDEdit
      Left = 436
      Top = 64
      Width = 277
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 6
      AutoSelect = True
      CustomButtons = <>
      PasswordChar = #0
    end
    object EdBairro: TLMDEdit
      Left = 4
      Top = 108
      Width = 209
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 7
      AutoSelect = True
      CustomButtons = <>
      PasswordChar = #0
    end
    object EdCEP: TLMDEdit
      Left = 216
      Top = 108
      Width = 77
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 8
      OnExit = EdCEPExit
      AutoSelect = True
      CustomButtons = <>
      PasswordChar = #0
    end
    object RGTipo: TRadioGroup
      Left = 528
      Top = 92
      Width = 185
      Height = 37
      Caption = ' Tipo de cadastro: '
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Empresa'
        'Pessoal')
      TabOrder = 11
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 175
    Width = 709
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 177
    ExplicitWidth = 717
    object BtConfirma: TBitBtn
      Left = 14
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF5555555555555A655555
        55555555588FF55555555555AAA65555555555558888F55555555555AAA65555
        555555558888FF555555555AAAAA65555555555888888F55555555AAAAAA6555
        5555558888888FF5555552AA65AAA65555555888858888F555552A65555AA655
        55558885555888FF55555555555AAA65555555555558888F555555555555AA65
        555555555555888FF555555555555AA65555555555555888FF555555555555AA
        65555555555555888FF555555555555AA65555555555555888FF555555555555
        5AA6555555555555588855555555555555555555555555555555}
      NumGlyphs = 2
    end
    object BtDesiste: TBitBtn
      Left = 610
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
        3333333777333777FF3333993333339993333377FF3333377FF3399993333339
        993337777FF3333377F3393999333333993337F777FF333337FF993399933333
        399377F3777FF333377F993339993333399377F33777FF33377F993333999333
        399377F333777FF3377F993333399933399377F3333777FF377F993333339993
        399377FF3333777FF7733993333339993933373FF3333777F7F3399933333399
        99333773FF3333777733339993333339933333773FFFFFF77333333999999999
        3333333777333777333333333999993333333333377777333333}
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 709
    Height = 40
    Align = alTop
    
    
    
    
    Caption = 'Cadastro R'#225'pido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    ExplicitWidth = 717
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 713
      Height = 36
      Align = alClient
      Transparent = True
    end
  end
  object QrDuplic2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM entidades'
      'WHERE CNPJ=:P0'
      'AND Codigo <> :P1')
    Left = 220
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplic2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end


