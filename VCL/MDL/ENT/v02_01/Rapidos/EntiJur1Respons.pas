unit EntiJur1Respons;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Variants, dmkGeral, dmkEdit,
  dmkEditCB, dmkDBLookupComboBox, dmkImage, UnDmkEnums;

type
  TFmEntiJur1Respons = class(TForm)
    DsRepresentantes: TDataSource;
    QrRepresentantes: TmySQLQuery;
    QrRepresentantesNOMEENTIDADE: TWideStringField;
    QrRepresentantesCodigo: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    PainelDados: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    SpeedButton5: TSpeedButton;
    CBRepres: TdmkDBLookupComboBox;
    EdRepres: TdmkEditCB;
    EdOrdem: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FEmpresa, FMaxOrdem: Integer;
  end;

var
  FmEntiJur1Respons: TFmEntiJur1Respons;

implementation

uses UnMyObjects, Module, Entidades, EntiSocio1, ModuleGeral, UnReordena,
  EntiJur1;

{$R *.DFM}

procedure TFmEntiJur1Respons.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiJur1Respons.BtConfirmaClick(Sender: TObject);
var
  Ordem, Repres: Integer;
begin
  Repres := EdRepres.ValueVariant;
  //
  if Repres = 0 then
  begin
    Geral.MB_Aviso('Informe um Representante!');
    EdRepres.SetFocus;
    Exit;
  end;
  Ordem := EdOrdem.ValueVariant;
  if Ordem = 0 then
  begin
    Geral.MB_Aviso('Informe a ordem!');
    EdOrdem.SetFocus;
    Exit;
  end;
  //
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE socios SET AlterWeb=1, Ordem=Ordem+1 ');
  Dmod.QrUpdM.SQL.Add('WHERE Ordem>= :P0 AND Empresa=:P1');
  Dmod.QrUpdM.Params[00].AsInteger := Ordem;
  Dmod.QrUpdM.Params[01].AsInteger := FEmpresa;
  Dmod.QrUpdM.ExecSQL;
  //
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO socios SET ');
  Dmod.QrUpdM.SQL.Add('Empresa=:P0, Socio=:P1, Ordem=:P2');

  Dmod.QrUpdM.Params[00].AsInteger := FEmpresa;
  Dmod.QrUpdM.Params[01].AsInteger := Repres;
  Dmod.QrUpdM.Params[02].AsInteger := Ordem;
  Dmod.QrUpdM.ExecSQL;
  VAR_ENTIDADE := Repres;
  Close;
end;

procedure TFmEntiJur1Respons.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrRepresentantes.Close;
  QrRepresentantes.SQL.Clear;
  QrRepresentantes.SQL.Add('SELECT');
  QrRepresentantes.SQL.Add('CASE 1 WHEN Tipo=0 THEN RazaoSocial');
  QrRepresentantes.SQL.Add('ELSE Nome END NOMEENTIDADE, Codigo');
  QrRepresentantes.SQL.Add('FROM entidades');
  QrRepresentantes.SQL.Add('WHERE '+VAR_REPRES_ENTIJUR1+'="V"');
  QrRepresentantes.SQL.Add('ORDER BY NOMEENTIDADE');
  QrRepresentantes.SQL.Add('');
  QrRepresentantes.Open;
end;

procedure TFmEntiJur1Respons.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiJur1Respons.FormShow(Sender: TObject);
begin
  EdOrdem.ValueVariant := UReordena.VerificaOrdem(Dmod.QrAux, Dmod.MyDB,
                            'Empresa', 'Ordem', 'socios',
                            FmEntiJur1.QrSociosEmpresa.Value,
                            EdOrdem.ValueVariant, True);
end;

procedure TFmEntiJur1Respons.SpeedButton5Click(Sender: TObject);
begin
  (*
  if VAR_REPRES_ENTIJUR1 = '' then
    Geral.MB_Aviso('Falta defini��o dos Representantes!')
  else begin
    VAR_ENTIDADE := 0;
    Screen.Cursor := crHourglass;
    Refresh;
    try
      Application.CreateForm(TFmEntiSocio1, FmEntiSocio1);
      FmEntiSocio1.ImgTipo.SQLType      := stIns;
      FmEntiSocio1.CkFornece5_0.Checked := True;
    finally
      Screen.Cursor := crDefault;
    end;
    FmEntiSocio1.ShowModal;
    FmEntiSocio1.Destroy;
    //
    QrRepresentantes.Close;
    QrRepresentantes.Open;
    EdRepres.ValueVariant := VAR_ENTIDADE;
    CBRepres.KeyValue     := VAR_ENTIDADE; 
  end;
  *)
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdRepres.ValueVariant, fmcadEntidade2, fmcadEntidade2,
    False, False, nil, nil, False, uetFornece5);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrRepresentantes, Dmod.MyDB);
    //
    EdRepres.ValueVariant := VAR_ENTIDADE;
    CBRepres.KeyValue     := VAR_ENTIDADE;
    EdRepres.SetFocus;
  end;
end;

procedure TFmEntiJur1Respons.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdOrdem.ValMax := FormatFloat('0', FMaxOrdem);
end;

end.

