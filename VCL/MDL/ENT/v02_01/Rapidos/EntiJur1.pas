unit EntiJur1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids, ComCtrls, Menus, DBGrids,
  dmkGeral, Variants, dmkEdit, dmkEditCB, dmkLabel, dmkDBLookupComboBox, frxRich,
  frxClass, frxDBSet, dmkImage, UnDmkProcFunc, dmkCheckBox, UnDmkEnums;

type
  TFmEntiJur1 = class(TForm)
    PainelEdita: TPanel;
    PainelDados: TPanel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    PainelEdit: TPanel;
    Panel2: TPanel;
    PainelData: TPanel;
    Panel6: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    DBEdit02: TDBEdit;
    DBEdit03: TDBEdit;
    QrDuplic2: TmySQLQuery;
    QrDuplic2Codigo: TIntegerField;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesRazaoSocial: TWideStringField;
    QrEntidadesFantasia: TWideStringField;
    QrEntidadesRespons1: TWideStringField;
    QrEntidadesRespons2: TWideStringField;
    QrEntidadesPai: TWideStringField;
    QrEntidadesMae: TWideStringField;
    QrEntidadesCNPJ: TWideStringField;
    QrEntidadesIE: TWideStringField;
    QrEntidadesFormaSociet: TWideStringField;
    QrEntidadesSimples: TSmallintField;
    QrEntidadesIEST: TWideStringField;
    QrEntidadesAtividade: TWideStringField;
    QrEntidadesNome: TWideStringField;
    QrEntidadesApelido: TWideStringField;
    QrEntidadesCPF: TWideStringField;
    QrEntidadesCPF_Pai: TWideStringField;
    QrEntidadesRG: TWideStringField;
    QrEntidadesSSP: TWideStringField;
    QrEntidadesDataRG: TDateField;
    QrEntidadesCidadeNatal: TWideStringField;
    QrEntidadesEstCivil: TSmallintField;
    QrEntidadesUFNatal: TSmallintField;
    QrEntidadesNacionalid: TWideStringField;
    QrEntidadesERua: TWideStringField;
    QrEntidadesECompl: TWideStringField;
    QrEntidadesEBairro: TWideStringField;
    QrEntidadesECidade: TWideStringField;
    QrEntidadesEUF: TSmallintField;
    QrEntidadesECEP: TIntegerField;
    QrEntidadesEPais: TWideStringField;
    QrEntidadesETe1: TWideStringField;
    QrEntidadesEte2: TWideStringField;
    QrEntidadesEte3: TWideStringField;
    QrEntidadesECel: TWideStringField;
    QrEntidadesEFax: TWideStringField;
    QrEntidadesEEmail: TWideStringField;
    QrEntidadesEContato: TWideStringField;
    QrEntidadesENatal: TDateField;
    QrEntidadesPRua: TWideStringField;
    QrEntidadesPCompl: TWideStringField;
    QrEntidadesPBairro: TWideStringField;
    QrEntidadesPCidade: TWideStringField;
    QrEntidadesPUF: TSmallintField;
    QrEntidadesPCEP: TIntegerField;
    QrEntidadesPPais: TWideStringField;
    QrEntidadesPTe1: TWideStringField;
    QrEntidadesPte2: TWideStringField;
    QrEntidadesPte3: TWideStringField;
    QrEntidadesPCel: TWideStringField;
    QrEntidadesPFax: TWideStringField;
    QrEntidadesPEmail: TWideStringField;
    QrEntidadesPContato: TWideStringField;
    QrEntidadesPNatal: TDateField;
    QrEntidadesSexo: TWideStringField;
    QrEntidadesResponsavel: TWideStringField;
    QrEntidadesProfissao: TWideStringField;
    QrEntidadesCargo: TWideStringField;
    QrEntidadesRecibo: TSmallintField;
    QrEntidadesDiaRecibo: TSmallintField;
    QrEntidadesAjudaEmpV: TFloatField;
    QrEntidadesAjudaEmpP: TFloatField;
    QrEntidadesCliente1: TWideStringField;
    QrEntidadesCliente2: TWideStringField;
    QrEntidadesFornece1: TWideStringField;
    QrEntidadesFornece2: TWideStringField;
    QrEntidadesFornece3: TWideStringField;
    QrEntidadesFornece4: TWideStringField;
    QrEntidadesFornece5: TWideStringField;
    QrEntidadesFornece6: TWideStringField;
    QrEntidadesTerceiro: TWideStringField;
    QrEntidadesCadastro: TDateField;
    QrEntidadesInformacoes: TWideStringField;
    QrEntidadesLogo: TBlobField;
    QrEntidadesVeiculo: TIntegerField;
    QrEntidadesMensal: TWideStringField;
    QrEntidadesObservacoes: TWideMemoField;
    QrEntidadesTipo: TSmallintField;
    QrEntidadesCLograd: TSmallintField;
    QrEntidadesCRua: TWideStringField;
    QrEntidadesCCompl: TWideStringField;
    QrEntidadesCBairro: TWideStringField;
    QrEntidadesCCidade: TWideStringField;
    QrEntidadesCUF: TSmallintField;
    QrEntidadesCCEP: TIntegerField;
    QrEntidadesCPais: TWideStringField;
    QrEntidadesCTel: TWideStringField;
    QrEntidadesCCel: TWideStringField;
    QrEntidadesCFax: TWideStringField;
    QrEntidadesCContato: TWideStringField;
    QrEntidadesLLograd: TSmallintField;
    QrEntidadesLRua: TWideStringField;
    QrEntidadesLCompl: TWideStringField;
    QrEntidadesLBairro: TWideStringField;
    QrEntidadesLCidade: TWideStringField;
    QrEntidadesLUF: TSmallintField;
    QrEntidadesLCEP: TIntegerField;
    QrEntidadesLPais: TWideStringField;
    QrEntidadesLTel: TWideStringField;
    QrEntidadesLCel: TWideStringField;
    QrEntidadesLFax: TWideStringField;
    QrEntidadesLContato: TWideStringField;
    QrEntidadesComissao: TFloatField;
    QrEntidadesSituacao: TSmallintField;
    QrEntidadesNivel: TWideStringField;
    QrEntidadesGrupo: TIntegerField;
    QrEntidadesAccount: TIntegerField;
    QrEntidadesLogo2: TBlobField;
    QrEntidadesConjugeNome: TWideStringField;
    QrEntidadesConjugeNatal: TDateField;
    QrEntidadesNome1: TWideStringField;
    QrEntidadesNatal1: TDateField;
    QrEntidadesNome2: TWideStringField;
    QrEntidadesNatal2: TDateField;
    QrEntidadesNome3: TWideStringField;
    QrEntidadesNatal3: TDateField;
    QrEntidadesNome4: TWideStringField;
    QrEntidadesNatal4: TDateField;
    QrEntidadesCreditosI: TIntegerField;
    QrEntidadesCreditosL: TIntegerField;
    QrEntidadesCreditosF2: TFloatField;
    QrEntidadesCreditosD: TDateField;
    QrEntidadesCreditosU: TDateField;
    QrEntidadesCreditosV: TDateField;
    QrEntidadesMotivo: TIntegerField;
    QrEntidadesQuantI1: TIntegerField;
    QrEntidadesQuantI2: TIntegerField;
    QrEntidadesQuantI3: TIntegerField;
    QrEntidadesQuantI4: TIntegerField;
    QrEntidadesQuantN1: TFloatField;
    QrEntidadesQuantN2: TFloatField;
    QrEntidadesAgenda: TWideStringField;
    QrEntidadesSenhaQuer: TWideStringField;
    QrEntidadesSenha1: TWideStringField;
    QrEntidadesLimiCred: TFloatField;
    QrEntidadesDesco: TFloatField;
    QrEntidadesCasasApliDesco: TSmallintField;
    QrEntidadesTempD: TFloatField;
    QrEntidadesBanco: TIntegerField;
    QrEntidadesAgencia: TWideStringField;
    QrEntidadesContaCorrente: TWideStringField;
    QrEntidadesFatorCompra: TFloatField;
    QrEntidadesAdValorem: TFloatField;
    QrEntidadesDMaisC: TIntegerField;
    QrEntidadesDMaisD: TIntegerField;
    QrEntidadesEmpresa: TIntegerField;
    QrEntidadesLk: TIntegerField;
    QrEntidadesDataCad: TDateField;
    QrEntidadesDataAlt: TDateField;
    QrEntidadesUserCad: TIntegerField;
    QrEntidadesUserAlt: TIntegerField;
    QrEntidadesCPF_Conjuge: TWideStringField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesNOMEACCOUNT: TWideStringField;
    QrEntidadesNOMEENTIGRUPO: TWideStringField;
    QrEntidadesNOMEMOTIVO: TWideStringField;
    QrEntidadesNOMEEMPRESA: TWideStringField;
    QrEntidadesNOMEEUF: TWideStringField;
    QrEntidadesNOMEPUF: TWideStringField;
    QrEntidadesNOMECUF: TWideStringField;
    QrEntidadesNOMELUF: TWideStringField;
    QrEntidadesNOMENUF: TWideStringField;
    QrEntidadesNOMEELOGRAD: TWideStringField;
    QrEntidadesNOMEPLOGRAD: TWideStringField;
    QrEntidadesNOMECLOGRAD: TWideStringField;
    QrEntidadesNOMELLOGRAD: TWideStringField;
    QrEntidadesNOMEECIVIL: TWideStringField;
    QrEntidadesECEP_TXT: TWideStringField;
    QrEntidadesPCEP_TXT: TWideStringField;
    QrEntidadesCCEP_TXT: TWideStringField;
    QrEntidadesLCEP_TXT: TWideStringField;
    QrEntidadesCTEL_TXT: TWideStringField;
    QrEntidadesCFAX_TXT: TWideStringField;
    QrEntidadesCCEL_TXT: TWideStringField;
    QrEntidadesLTEL_TXT: TWideStringField;
    QrEntidadesLFAX_TXT: TWideStringField;
    QrEntidadesLCEL_TXT: TWideStringField;
    QrEntidadesPCPF_TXT: TWideStringField;
    QrEntidadesPTE1_TXT: TWideStringField;
    QrEntidadesPTE2_TXT: TWideStringField;
    QrEntidadesPTE3_TXT: TWideStringField;
    QrEntidadesPCEL_TXT: TWideStringField;
    QrEntidadesPFAX_TXT: TWideStringField;
    QrEntidadesETE1_TXT: TWideStringField;
    QrEntidadesETE2_TXT: TWideStringField;
    QrEntidadesETE3_TXT: TWideStringField;
    QrEntidadesECEL_TXT: TWideStringField;
    QrEntidadesEFAX_TXT: TWideStringField;
    QrEntidadesCNPJ_TXT: TWideStringField;
    DBEdit87: TDBEdit;
    Label80: TLabel;
    DBEdit81: TDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Grade1: TStringGrid;
    TabSheet2: TTabSheet;
    QrLoc0: TmySQLQuery;
    QrLoc0CodMin: TIntegerField;
    QrLoc0CodMax: TIntegerField;
    PMCliente: TPopupMenu;
    IncluinovoClienteJurdico1: TMenuItem;
    Alteraclienteatual1: TMenuItem;
    QrSocios: TmySQLQuery;
    DsSocios: TDataSource;
    DBGrid1: TDBGrid;
    PMSocios: TPopupMenu;
    Adiciona1: TMenuItem;
    Remove1: TMenuItem;
    Altera1: TMenuItem;
    PMImprime: TPopupMenu;
    Scios1: TMenuItem;
    Risco1: TMenuItem;
    QrSociosNOMESOCIO: TWideStringField;
    QrSociosCodigo: TIntegerField;
    QrSociosSexo: TWideStringField;
    QrSociosPai: TWideStringField;
    QrSociosMae: TWideStringField;
    QrSociosPNatal: TDateField;
    QrSociosCidadeNatal: TWideStringField;
    QrSociosNacionalid: TWideStringField;
    QrSociosConjugeNome: TWideStringField;
    QrSociosCPF: TWideStringField;
    QrSociosRG: TWideStringField;
    QrSociosSSP: TWideStringField;
    QrSociosDataRG: TDateField;
    QrSociosPRua: TWideStringField;
    QrSociosPCompl: TWideStringField;
    QrSociosPBairro: TWideStringField;
    QrSociosPCidade: TWideStringField;
    QrSociosPCEP: TIntegerField;
    QrSociosPTe1: TWideStringField;
    QrSociosProfissao: TWideStringField;
    QrSociosCargo: TWideStringField;
    QrSociosNOMEPUF: TWideStringField;
    QrSociosNOMEPLOGRAD: TWideStringField;
    QrSociosNOMEECIVIL: TWideStringField;
    QrSociosEmpresa: TIntegerField;
    QrSociosSocio: TIntegerField;
    QrSociosOrdem: TIntegerField;
    QrSociosPCel: TWideStringField;
    QrSociosRazaoSocial: TWideStringField;
    QrSociosFantasia: TWideStringField;
    QrSociosRespons1: TWideStringField;
    QrSociosRespons2: TWideStringField;
    QrSociosCNPJ: TWideStringField;
    QrSociosIE: TWideStringField;
    QrSociosFormaSociet: TWideStringField;
    QrSociosSimples: TSmallintField;
    QrSociosIEST: TWideStringField;
    QrSociosAtividade: TWideStringField;
    QrSociosNome: TWideStringField;
    QrSociosApelido: TWideStringField;
    QrSociosCPF_Pai: TWideStringField;
    QrSociosEstCivil: TSmallintField;
    QrSociosUFNatal: TSmallintField;
    QrSociosELograd: TSmallintField;
    QrSociosERua: TWideStringField;
    QrSociosECompl: TWideStringField;
    QrSociosEBairro: TWideStringField;
    QrSociosECidade: TWideStringField;
    QrSociosEUF: TSmallintField;
    QrSociosECEP: TIntegerField;
    QrSociosEPais: TWideStringField;
    QrSociosETe1: TWideStringField;
    QrSociosEte2: TWideStringField;
    QrSociosEte3: TWideStringField;
    QrSociosECel: TWideStringField;
    QrSociosEFax: TWideStringField;
    QrSociosEEmail: TWideStringField;
    QrSociosEContato: TWideStringField;
    QrSociosENatal: TDateField;
    QrSociosPLograd: TSmallintField;
    QrSociosPUF: TSmallintField;
    QrSociosPPais: TWideStringField;
    QrSociosPte2: TWideStringField;
    QrSociosPte3: TWideStringField;
    QrSociosPFax: TWideStringField;
    QrSociosPEmail: TWideStringField;
    QrSociosPContato: TWideStringField;
    QrSociosResponsavel: TWideStringField;
    QrSociosRecibo: TSmallintField;
    QrSociosDiaRecibo: TSmallintField;
    QrSociosAjudaEmpV: TFloatField;
    QrSociosAjudaEmpP: TFloatField;
    QrSociosCliente1: TWideStringField;
    QrSociosCliente2: TWideStringField;
    QrSociosFornece1: TWideStringField;
    QrSociosFornece2: TWideStringField;
    QrSociosFornece3: TWideStringField;
    QrSociosFornece4: TWideStringField;
    QrSociosFornece5: TWideStringField;
    QrSociosFornece6: TWideStringField;
    QrSociosTerceiro: TWideStringField;
    QrSociosCadastro: TDateField;
    QrSociosInformacoes: TWideStringField;
    QrSociosLogo: TBlobField;
    QrSociosVeiculo: TIntegerField;
    QrSociosMensal: TWideStringField;
    QrSociosObservacoes: TWideMemoField;
    QrSociosTipo: TSmallintField;
    QrSociosCLograd: TSmallintField;
    QrSociosCRua: TWideStringField;
    QrSociosCCompl: TWideStringField;
    QrSociosCBairro: TWideStringField;
    QrSociosCCidade: TWideStringField;
    QrSociosCUF: TSmallintField;
    QrSociosCCEP: TIntegerField;
    QrSociosCPais: TWideStringField;
    QrSociosCTel: TWideStringField;
    QrSociosCCel: TWideStringField;
    QrSociosCFax: TWideStringField;
    QrSociosCContato: TWideStringField;
    QrSociosLLograd: TSmallintField;
    QrSociosLRua: TWideStringField;
    QrSociosLCompl: TWideStringField;
    QrSociosLBairro: TWideStringField;
    QrSociosLCidade: TWideStringField;
    QrSociosLUF: TSmallintField;
    QrSociosLCEP: TIntegerField;
    QrSociosLPais: TWideStringField;
    QrSociosLTel: TWideStringField;
    QrSociosLCel: TWideStringField;
    QrSociosLFax: TWideStringField;
    QrSociosLContato: TWideStringField;
    QrSociosComissao: TFloatField;
    QrSociosSituacao: TSmallintField;
    QrSociosNivel: TWideStringField;
    QrSociosGrupo: TIntegerField;
    QrSociosAccount: TIntegerField;
    QrSociosLogo2: TBlobField;
    QrSociosConjugeNatal: TDateField;
    QrSociosNome1: TWideStringField;
    QrSociosNatal1: TDateField;
    QrSociosNome2: TWideStringField;
    QrSociosNatal2: TDateField;
    QrSociosNome3: TWideStringField;
    QrSociosNatal3: TDateField;
    QrSociosNome4: TWideStringField;
    QrSociosNatal4: TDateField;
    QrSociosCreditosI: TIntegerField;
    QrSociosCreditosL: TIntegerField;
    QrSociosCreditosF2: TFloatField;
    QrSociosCreditosD: TDateField;
    QrSociosCreditosU: TDateField;
    QrSociosCreditosV: TDateField;
    QrSociosMotivo: TIntegerField;
    QrSociosQuantI1: TIntegerField;
    QrSociosQuantI2: TIntegerField;
    QrSociosQuantI3: TIntegerField;
    QrSociosQuantI4: TIntegerField;
    QrSociosQuantN1: TFloatField;
    QrSociosQuantN2: TFloatField;
    QrSociosAgenda: TWideStringField;
    QrSociosSenhaQuer: TWideStringField;
    QrSociosSenha1: TWideStringField;
    QrSociosLimiCred: TFloatField;
    QrSociosDesco: TFloatField;
    QrSociosCasasApliDesco: TSmallintField;
    QrSociosTempD: TFloatField;
    QrSociosBanco: TIntegerField;
    QrSociosAgencia: TWideStringField;
    QrSociosContaCorrente: TWideStringField;
    QrSociosFatorCompra: TFloatField;
    QrSociosAdValorem: TFloatField;
    QrSociosDMaisC: TIntegerField;
    QrSociosDMaisD: TIntegerField;
    QrSociosLk: TIntegerField;
    QrSociosDataCad: TDateField;
    QrSociosDataAlt: TDateField;
    QrSociosUserCad: TIntegerField;
    QrSociosUserAlt: TIntegerField;
    QrSociosCPF_Conjuge: TWideStringField;
    QrSociosEmpresa_1: TIntegerField;
    ContratodeFomentoMercantil1: TMenuItem;
    TabSheet3: TTabSheet;
    PMTaxas: TPopupMenu;
    Adicionataxa1: TMenuItem;
    QrTaxasCli: TmySQLQuery;
    DsTaxasCli: TDataSource;
    QrTaxasCliNOMETAXA: TWideStringField;
    QrTaxasCliControle: TIntegerField;
    QrTaxasCliCliente: TIntegerField;
    QrTaxasCliLk: TIntegerField;
    QrTaxasCliDataCad: TDateField;
    QrTaxasCliDataAlt: TDateField;
    QrTaxasCliUserCad: TIntegerField;
    QrTaxasCliUserAlt: TIntegerField;
    QrTaxasCliTaxa: TIntegerField;
    QrTaxasCliValor: TFloatField;
    GradeTaxas: TDBGrid;
    QrTaxasCliGENERO_CH: TWideStringField;
    QrTaxasCliGENERO_DU: TWideStringField;
    QrTaxasCliFORMA_TXT: TWideStringField;
    QrTaxasCliBASE_TXT: TWideStringField;
    QrTaxasCliGenero: TSmallintField;
    QrTaxasCliForma: TSmallintField;
    QrTaxasCliBase: TSmallintField;
    Retirataxa1: TMenuItem;
    Label40: TLabel;
    DBEdit021: TDBEdit;
    QrEntidadesCBE: TIntegerField;
    QrEntidadesSCB: TIntegerField;
    QrEntidadesContab: TWideStringField;
    QrEntidadesMSN1: TWideStringField;
    TabSheet4: TTabSheet;
    QrEntidadesPastaTxtFTP: TWideStringField;
    QrEntidadesPastaPwdFTP: TWideStringField;
    QrEntidadesMultaCodi: TSmallintField;
    QrEntidadesMultaDias: TSmallintField;
    QrEntidadesMultaValr: TFloatField;
    QrEntidadesMultaPerc: TFloatField;
    TabSheet5: TTabSheet;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    Grade2: TStringGrid;
    TabSheet10: TTabSheet;
    QrEntidadesProtestar: TSmallintField;
    QrEntidadesMultaTiVe: TSmallintField;
    QrEntidadesJuroSacado: TFloatField;
    TabSheet11: TTabSheet;
    QrEntidadesCPMF: TFloatField;
    QrEntidadesCorrido: TIntegerField;
    DBEdit25: TDBEdit;
    QrEntidadesENumero: TIntegerField;
    QrEntidadesPNumero: TIntegerField;
    QrEntidadesCNumero: TIntegerField;
    QrEntidadesLNumero: TIntegerField;
    QrSociosENumero: TIntegerField;
    QrSociosPNumero: TIntegerField;
    QrSociosCNumero: TIntegerField;
    QrSociosLNumero: TIntegerField;
    DBRGProtestar: TDBRadioGroup;
    Panel4: TPanel;
    Label61: TLabel;
    DBEdit3: TDBEdit;
    GroupBox4: TGroupBox;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    DBRadioGroup3: TDBRadioGroup;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBRadioGroup4: TDBRadioGroup;
    PMOcor: TPopupMenu;
    DBGrid2: TDBGrid;
    QrOcorCli: TmySQLQuery;
    QrOcorCliCliente: TIntegerField;
    QrOcorCliControle: TIntegerField;
    QrOcorCliOcorrencia: TIntegerField;
    QrOcorCliBase: TFloatField;
    QrOcorCliFormaCNAB: TSmallintField;
    DsOcorCli: TDataSource;
    Incluiocorrncia1: TMenuItem;
    Alteraocorrncia1: TMenuItem;
    Excluiocorncia1: TMenuItem;
    QrOcorCliNOMEOCOR: TWideStringField;
    QrOcorCliFormaCNAB_TXT: TWideStringField;
    Panel7: TPanel;
    Panel8: TPanel;
    RGProtestar: TRadioGroup;
    Panel9: TPanel;
    Label25: TLabel;
    EdRazaoSocial: TdmkEdit;
    EdFantasia: TdmkEdit;
    Label41: TLabel;
    TabSheet13: TTabSheet;
    Panel10: TPanel;
    Panel11: TPanel;
    DBEdit1: TDBEdit;
    Label53: TLabel;
    Label29: TLabel;
    EdCNPJ: TdmkEdit;
    EdIE: TdmkEdit;
    Label30: TLabel;
    QrSPC_Config: TmySQLQuery;
    QrSPC_ConfigCodigo: TIntegerField;
    QrSPC_ConfigNome: TWideStringField;
    DsSPC_Config: TDataSource;
    Panel12: TPanel;
    GroupBox2: TGroupBox;
    Label46: TLabel;
    EdContab: TdmkEdit;
    GroupBox1: TGroupBox;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label65: TLabel;
    EdPastaTxtFTP: TEdit;
    EdPastaPwdFTP: TEdit;
    Panel13: TPanel;
    GroupBox3: TGroupBox;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    RGMultaCodi: TRadioGroup;
    EdMultaValr: TdmkEdit;
    EdMultaPerc: TdmkEdit;
    EdMultaDias: TdmkEdit;
    RGMultaTiVe: TRadioGroup;
    GroupBox5: TGroupBox;
    Label60: TLabel;
    EdJuroSacado: TdmkEdit;
    GroupBox8: TGroupBox;
    Panel15: TPanel;
    DBCheckBox3: TDBCheckBox;
    DBEdit01: TDBEdit;
    Label8: TLabel;
    Label49: TLabel;
    DBEdit024: TDBEdit;
    DBEdit020: TDBEdit;
    Label38: TLabel;
    DBEdit015: TDBEdit;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit016: TDBEdit;
    DBEdit017: TDBEdit;
    Label24: TLabel;
    Label26: TLabel;
    DBEdit018: TDBEdit;
    Label63: TLabel;
    DBEdit022: TDBEdit;
    DBCheckBox4: TDBCheckBox;
    DBEdit019: TDBEdit;
    Label27: TLabel;
    Label64: TLabel;
    DBEdit025: TDBEdit;
    DBEdit023: TDBEdit;
    Label47: TLabel;
    QrEntidadesSPC_CONFIG_NOME: TWideStringField;
    QrEntidadesSPC_ValMin: TFloatField;
    QrEntidadesSPC_ValMax: TFloatField;
    Label69: TLabel;
    DBEdit4: TDBEdit;
    GroupBox9: TGroupBox;
    Label70: TLabel;
    Label71: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    GroupBox6: TGroupBox;
    dmkLabel1: TdmkLabel;
    SpeedButton5: TSpeedButton;
    CBSPC_Config: TdmkDBLookupComboBox;
    EdSPC_Config: TdmkEditCB;
    GroupBox7: TGroupBox;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    EdValMin: TdmkEdit;
    EdValMax: TdmkEdit;
    Panel16: TPanel;
    CkSimples: TCheckBox;
    Label121: TLabel;
    EdFormaSociet: TdmkEdit;
    EdNivel: TdmkEdit;
    Label81: TLabel;
    Label37: TLabel;
    EdAtividade: TdmkEdit;
    EdMSN1: TdmkEdit;
    Label48: TLabel;
    Label107: TLabel;
    EdFatorCompra: TdmkEdit;
    Label108: TLabel;
    EdAdValorem: TdmkEdit;
    EdDMaisC: TdmkEdit;
    Label109: TLabel;
    Label110: TLabel;
    EdDMaisD: TdmkEdit;
    EdLimiCred: TdmkEdit;
    Label42: TLabel;
    Label62: TLabel;
    EdCPMF: TdmkEdit;
    Label94: TLabel;
    EdCBE: TdmkEdit;
    CkSCB: TCheckBox;
    QrEntidadesSPC_Config: TIntegerField;
    N2: TMenuItem;
    Formulrioparapreenchimentomanual1: TMenuItem;
    frxCadastro: TfrxReport;
    QrCartas: TmySQLQuery;
    QrCartasNOMECARTAG: TWideStringField;
    QrCartasCodigo: TIntegerField;
    QrCartasTitulo: TWideStringField;
    QrCartasTexto: TWideMemoField;
    QrCartasCartaG: TIntegerField;
    QrCartasPagina: TIntegerField;
    QrCartasTipo: TIntegerField;
    QrCartasLk: TIntegerField;
    QrCartasDataCad: TDateField;
    QrCartasDataAlt: TDateField;
    QrCartasUserCad: TIntegerField;
    QrCartasUserAlt: TIntegerField;
    QrCartasAlterWeb: TSmallintField;
    QrCartasAtivo: TSmallintField;
    frxDsCartas: TfrxDBDataset;
    frxRichObject1: TfrxRichObject;
    QrEntidadesNIRE: TWideStringField;
    QrSociosNIRE: TWideStringField;
    GBRodaPe2: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel17: TPanel;
    BtConfirma: TBitBtn;
    Panel14: TPanel;
    LaAvisoA: TLabel;
    LaAvisoB: TLabel;
    GBRodaPe: TGroupBox;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel3: TPanel;
    BtGestor: TBitBtn;
    BtEntidades2: TBitBtn;
    BtTaxas: TBitBtn;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOcor: TBitBtn;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel18: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEntidadesAtivo: TSmallintField;
    N1: TMenuItem;
    Pesquisarantesdeimprimir1: TMenuItem;
    CkAtivo: TdmkCheckBox;
    BtEntidades: TBitBtn;
    DBCheckBox1: TDBCheckBox;
    EdCorrido: TdmkEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtEntidades2Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEntidadesAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEntidadesBeforeOpen(DataSet: TDataSet);
    procedure QrEntidadesCalcFields(DataSet: TDataSet);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure QrEntidadesAfterScroll(DataSet: TDataSet);
    procedure IncluinovoClienteJurdico1Click(Sender: TObject);
    procedure Alteraclienteatual1Click(Sender: TObject);
    procedure PMClientePopup(Sender: TObject);
    procedure BtGestorClick(Sender: TObject);
    procedure Adiciona1Click(Sender: TObject);
    procedure Remove1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure Scios1Click(Sender: TObject);
    procedure Risco1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure ContratodeFomentoMercantil1Click(Sender: TObject);
    procedure BtTaxasClick(Sender: TObject);
    procedure Adicionataxa1Click(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure Retirataxa1Click(Sender: TObject);
    procedure EdPastaTxtFTPExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPastaPwdFTPExit(Sender: TObject);
    procedure Alteraocorrncia1Click(Sender: TObject);
    procedure Excluiocorncia1Click(Sender: TObject);
    procedure Incluiocorrncia1Click(Sender: TObject);
    procedure BtOcorClick(Sender: TObject);
    procedure QrOcorCliCalcFields(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Formulrioparapreenchimentomanual1Click(Sender: TObject);
    procedure PMSociosPopup(Sender: TObject);
    procedure Pesquisarantesdeimprimir1Click(Sender: TObject);
    procedure EdCNPJExit(Sender: TObject);
    procedure BtEntidadesClick(Sender: TObject);
    procedure PMImprimePopup(Sender: TObject);
    procedure PMTaxasPopup(Sender: TObject);
    procedure PMOcorPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure ConfiguraAlteracao;
    procedure ConfiguraInclusao;
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenSocios;
    procedure ReopenOcorCli(Controle: Integer);
    procedure ReopenEntidades(Codigo: Integer);
    procedure MostraEntiJur1Ocor(SQLType: TSQLType);
    procedure MostraEntiJur1Taxa(SQLType: TSQLType; Entidade: Integer);
    function AtualizaDadosSPC_Config(Entidade: Integer): Boolean;
  public
    { Public declarations }
    FTaxaCli, FSocio: Integer;
    //
    procedure ReopenTaxasCli();
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEntiJur1: TFmEntiJur1;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Principal, EntiJur1Respons, EntiSocio1, SociosImp,
RiscoCli, ContratoImp, MyDBCheck, EntiJur1Ocor, SPC_Config, EntiJur1Taxa,
EntidadesImp, UnEntities, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntiJur1.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEntiJur1.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEntidadesCodigo.Value, LaRegistro.Caption[2]);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEntiJur1.DefParams;
begin
  VAR_GOTOTABELA := 'Entidades';
  VAR_GOTOMySQLTABLE := QrEntidades;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := 'Tipo=0 AND Cliente1="V" ';

  GOTOy.LimpaVAR_SQL;
  //
  VAR_SQLx.Add('SELECT spc.Nome SPC_CONFIG_NOME, spe.SPC_Config, ');
  VAR_SQLx.Add('spe.ValMin SPC_ValMin, spe.ValMax SPC_ValMax, ent.*,');
  VAR_SQLx.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  VAR_SQLx.Add('ELSE ent.Nome END NOMEENTIDADE,');
  VAR_SQLx.Add('CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial');
  VAR_SQLx.Add('ELSE acm.Nome END NOMEACCOUNT,');
  VAR_SQLx.Add('eng.Nome NOMEENTIGRUPO,');
  VAR_SQLx.Add('mot.Descricao NOMEMOTIVO,');
  VAR_SQLx.Add('emp.RazaoSocial NOMEEMPRESA,');
  VAR_SQLx.Add('euf.nome NOMEEUF, puf.nome NOMEPUF,');
  VAR_SQLx.Add('cuf.nome NOMECUF, luf.nome NOMELUF, nuf.Nome NOMENUF, ');
  VAR_SQLx.Add('elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,');
  VAR_SQLx.Add('clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD, lec.Nome NOMEECIVIL');
  VAR_SQLx.Add('FROM entidades ent');
  VAR_SQLx.Add('LEFT JOIN entigrupos eng ON eng.Codigo=ent.Grupo');
  VAR_SQLx.Add('LEFT JOIN entidades acm  ON acm.Codigo=ent.Account');
  VAR_SQLx.Add('LEFT JOIN entidades emp  ON emp.Codigo=ent.Empresa');
  VAR_SQLx.Add('LEFT JOIN motivose mot   ON mot.Codigo=ent.Motivo');
  VAR_SQLx.Add('LEFT JOIN ufs euf        ON euf.Codigo=ent.EUF');
  VAR_SQLx.Add('LEFT JOIN ufs puf        ON puf.Codigo=ent.PUF');
  VAR_SQLx.Add('LEFT JOIN ufs cuf        ON cuf.Codigo=ent.EUF');
  VAR_SQLx.Add('LEFT JOIN ufs luf        ON luf.Codigo=ent.PUF');
  VAR_SQLx.Add('LEFT JOIN ufs nuf        ON nuf.Codigo=ent.UFNatal');
  VAR_SQLx.Add('LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd');
  VAR_SQLx.Add('LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd');
  VAR_SQLx.Add('LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd');
  VAR_SQLx.Add('LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd');
  VAR_SQLx.Add('LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil');
  VAR_SQLx.Add('LEFT JOIN spc_entida  spe ON ent.Codigo=spe.Entidade');
  VAR_SQLx.Add('LEFT JOIN spc_config  spc ON spc.Codigo=spe.SPC_Config');
  VAR_SQLx.Add('WHERE ent.Tipo=0 AND ent.Cliente1="V" ');
  //
  VAR_SQL1.Add('AND ent.Codigo=:P0');
  //
  VAR_SQLa.Add('AND (CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial LIKE:P0');
  VAR_SQLa.Add('ELSE ent.Nome LIKE:P1 END) ');
  // ERRO ao usar pela primeira vez
end;

procedure TFmEntiJur1.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEntiJur1.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEntiJur1.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEntiJur1.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntiJur1.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntiJur1.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntiJur1.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntiJur1.SpeedButton5Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Config, FmSPC_Config, afmoNegarComAviso) then
  begin
    VAR_CADASTRO := 0;
    //
    FmSPC_Config.ShowModal;
    FmSPC_Config.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrSPC_Config.Close;
      QrSPC_Config.Open;
      EdSPC_Config.ValueVariant := VAR_CADASTRO;
      CBSPC_Config.KeyValue     := VAR_CADASTRO;
    end;
  end;
  //
end;

procedure TFmEntiJur1.BtEntidades2Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMCliente, BtEntidades2);
end;

procedure TFmEntiJur1.BtEntidadesClick(Sender: TObject);
begin
  if (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0) then
    DModG.CadastroDeEntidade(QrEntidadesCodigo.Value, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmEntiJur1.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEntidadesCodigo.Value;
  Close;
end;

procedure TFmEntiJur1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType   := stLok;
  LaRegistro.Align  := alClient;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  //
  CriaOForm;
  //
  //LaRegistro.Caption := GOTOy.NomeDireto('');
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  //
  TabSheet5.TabVisible  := False; //FTP n�o existe mais
  TabSheet10.TabVisible := False; //Cobran�a banc�ria usa do CNABCfg
  TabSheet11.TabVisible := False; //Protesto usa do CNABCfg
  GroupBox1.Visible     := False; //FTP n�o existe mais
  GroupBox3.Visible     := False; //Protesto usa do CNABCfg
  GroupBox5.Visible     := False; //Protesto usa do CNABCfg
  RGProtestar.Visible   := False; //Protesto usa do CNABCfg
  EdCorrido.Visible     := False; //Protesto usa do CNABCfg
  Label48.Visible       := False; //N�o existe mais
  EdMSN1.Visible        := False; //N�o existe mais
  //
  UMyMod.AbreQuery(QrSPC_Config, Dmod.MyDB);
  Va(vpLast);
end;

procedure TFmEntiJur1.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEntidadesCodigo.Value,LaRegistro.Caption);
end;

procedure TFmEntiJur1.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEntiJur1.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntiJur1.QrEntidadesAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEntiJur1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Entidades', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmEntiJur1.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEntidadesCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, 'RazaoSocial', 'Entidades', Dmod.MyDB,
  ' AND Tipo=0 AND Cliente1="V" '));
end;

procedure TFmEntiJur1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiJur1.Formulrioparapreenchimentomanual1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxCadastro, 'Cadastro');
end;

procedure TFmEntiJur1.QrEntidadesBeforeOpen(DataSet: TDataSet);
begin
  QrEntidadesCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEntiJur1.QrEntidadesCalcFields(DataSet: TDataSet);
begin
  QrEntidadesETE1_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe1.Value);
  QrEntidadesETE2_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe2.Value);
  QrEntidadesETE3_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe3.Value);
  QrEntidadesEFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesEFax.Value);
  QrEntidadesECEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesECel.Value);
  QrEntidadesCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCNPJ.Value);
  //
  QrEntidadesPTE1_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe1.Value);
  QrEntidadesPTE2_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe2.Value);
  QrEntidadesPTE3_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe3.Value);
  QrEntidadesPFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPFax.Value);
  QrEntidadesPCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPCel.Value);
  QrEntidadesPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF.Value);
  //QrEntidadesCPF_PAI_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF_Pai.Value);
  //
  QrEntidadesCTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCTel.Value);
  QrEntidadesCFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCFax.Value);
  QrEntidadesCCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCCel.Value);
  //
  QrEntidadesLTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLTel.Value);
  QrEntidadesLFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLFax.Value);
  QrEntidadesLCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLCel.Value);
  //
  QrEntidadesECEP_TXT.Value := Geral.FormataCEP_TT(Geral.FF0(QrEntidadesECEP.Value));
  QrEntidadesPCEP_TXT.Value := Geral.FormataCEP_TT(Geral.FF0(QrEntidadesPCEP.Value));
  QrEntidadesCCEP_TXT.Value := Geral.FormataCEP_TT(Geral.FF0(QrEntidadesCCEP.Value));
  QrEntidadesLCEP_TXT.Value := Geral.FormataCEP_TT(Geral.FF0(QrEntidadesLCEP.Value));
  //
  (*case QrEntidadesUserCad.Value of
    -2: QrEntidadesNOMECAD2.Value := 'BOSS [Administrador]';
    -1: QrEntidadesNOMECAD2.Value := 'MASTER [Admin. DERMATEK]';
     0: QrEntidadesNOMECAD2.Value := 'N�o definido';
    else QrEntidadesNOMECAD2.Value := QrEntidadesNOMECAD.Value;
  end;
  case QrEntidadesUserAlt.Value of
    -2: QrEntidadesNOMEALT2.Value := 'BOSS [Administrador]';
    -1: QrEntidadesNOMEALT2.Value := 'MASTER [Admin.DERMATEK]';
     0: QrEntidadesNOMEALT2.Value := '- - -';
    else QrEntidadesNOMEALT2.Value := QrEntidadesNOMEALT.Value;
  end;*)
end;

procedure TFmEntiJur1.ConfiguraAlteracao;
var
  Entidade : Integer;
begin
  Entidade := QrEntidadesCodigo.Value;
  if not UMyMod.SelLockY(Entidade, Dmod.MyDB, 'Entidades', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Entidade, Dmod.MyDB, 'Entidades', 'Codigo');
      //
      ImgTipo.SQLType        := stUpd;
      PainelControle.Visible := False;
      //
      GOTOy.BotoesSb(ImgTipo.SQLType);
      //
      EdRazaoSocial.ValueVariant := QrEntidadesRazaoSocial.Value;
      EdCNPJ.ValueVariant        := QrEntidadesCNPJ_TXT.Value;
      EdIE.ValueVariant          := QrEntidadesIE.Value;
      EdNivel.ValueVariant       := QrEntidadesNivel.Value;
      //
      EdLimiCred.ValueVariant := QrEntidadesLimiCred.Value;
      //
      EdFatorCompra.ValueVariant := QrEntidadesFatorCompra.Value;
      EdAdValorem.ValueVariant   := QrEntidadesAdValorem.Value;
      EdDMaisC.ValueVariant      := QrEntidadesDMaisC.Value;
      EdDMaisD.ValueVariant      := QrEntidadesDMaisD.Value;
      (*
      EdCustodia.ValueVariant       := Geral.FFT(QrEntidadesCustodia.Value, 2, siPositivo);
      EdCobranca.ValueVariant       := Geral.FFT(QrEntidadesCobranca.Value, 2, siPositivo);
      EdR_U.ValueVariant            := Geral.FFT(QrEntidadesR_U.Value, 2, siPositivo);
      EdTarifaPostal.ValueVariant   := Geral.FFT(QrEntidadesTarifaPostal.Value, 2, siPositivo);
      EdTarifaOperacao.ValueVariant := Geral.FFT(QrEntidadesTarifaOperacao.Value, 2, siPositivo);
      EdTxSerasa.ValueVariant       := Geral.FFT(QrEntidadesTxSerasa.Value, 4, siPositivo);
      EdTxAR.ValueVariant           := Geral.FFT(QrEntidadesTxAR.Value, 4, siPositivo);
      *)
      //
      EdFormaSociet.ValueVariant := QrEntidadesFormaSociet.Value;
      CkSimples.Checked          := Geral.IntToBool(QrEntidadesSimples.Value);
      EdNivel.ValueVariant       := QrEntidadesNivel.Value;
      EdAtividade.ValueVariant   := QrEntidadesAtividade.Value;
      EdFantasia.ValueVariant    := QrEntidadesFantasia.Value;
      //
      EdCBE.ValueVariant     := QrEntidadesCBE.Value;
      EdCPMF.ValueVariant    := QrEntidadesCPMF.Value;
      CkSCB.Checked          := Geral.IntToBool_0(QrEntidadesSCB.Value);
      EdContab.ValueVariant  := QrEntidadesContab.Value;
      EdMSN1.ValueVariant    := QrEntidadesMSN1.Value;
      EdPastaTxtFTP.Text     := QrEntidadesPastaTxtFTP.Value;
      EdPastaPwdFTP.Text     := QrEntidadesPastaPwdFTP.Value;
      EdCorrido.ValueVariant := QrEntidadesCorrido.Value;
      //
      EdJuroSacado.ValueVariant := QrEntidadesJuroSacado.Value;
      EdMultaDias.ValueVariant  := QrEntidadesMultaDias.Value;
      EdMultaValr.ValueVariant  := QrEntidadesMultaValr.Value;
      EdMultaPerc.ValueVariant  := QrEntidadesMultaPerc.Value;
      RGMultaCodi.ItemIndex     := QrEntidadesMultaCodi.Value;
      RGProtestar.ItemIndex     := DBRGProtestar.ItemIndex;
      //
      FmPrincipal.AcoesIniciaisDeCadastroDeEntidades(FmEntiJur1, Entidade, Grade2);
      //
      EdSPC_Config.ValueVariant := QrEntidadesSPC_Config.Value;
      CBSPC_Config.KeyValue     := QrEntidadesSPC_Config.Value;
      EdValMin.ValueVariant     := QrEntidadesSPC_ValMin.Value;
      EdValMax.ValueVariant     := QrEntidadesSPC_ValMax.Value;
      //
      CkAtivo.Checked := Geral.IntToBool(QrEntidadesAtivo.Value);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmEntiJur1.ConfiguraInclusao;
begin
  try
    ImgTipo.SQLType        := stIns;
    PainelControle.Visible := False;
    //
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    EdRazaoSocial.ValueVariant := '';
    EdCNPJ.ValueVariant        := '';
    EdIE.ValueVariant          := '';
    EdNivel.ValueVariant       := 'C';
    EdLimiCred.ValueVariant    := 0;
    //
    EdFatorCompra.ValueVariant := 0;
    EdAdValorem.ValueVariant   := 0;
    EdDMaisC.ValueVariant      := 0;
    EdDMaisD.ValueVariant      := 0;
    (*
    EdCustodia.ValueVariant       := '';
    EdCobranca.ValueVariant       := '';
    EdR_U.ValueVariant            := '';
    EdTarifaPostal.ValueVariant   := '';
    EdTarifaOperacao.ValueVariant := '';
    EdTxSerasa.ValueVariant       := '';
    EdTxAR.ValueVariant           := '';
    *)
    //
    EdFormaSociet.ValueVariant := '';
    CkSimples.Checked          := False;
    EdAtividade.ValueVariant   := '';
    EdFantasia.ValueVariant    := '';
    EdJuroSacado.ValueVariant  := 10.00;
    EdCorrido.ValueVariant     := 6;
    //
    CkAtivo.Checked := True;
    //
    FmPrincipal.AcoesIniciaisDeCadastroDeEntidades(FmEntiJur1, 0, Grade2);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmEntiJur1.BtConfirmaClick(Sender: TObject);
var
  Codigo, Tipo, Protestar, Ativo: Integer;
  RazaoSocial, CNPJ, IE: String;
begin
  Tipo := 0;
  Codigo := QrEntidadesCodigo.Value;
  if (Length(Geral.SoNumero1a9_TT(EdCNPJ.Text))>0)
  then begin
    QrDuplic2.Close;
    QrDuplic2.SQL.Clear;
    if Tipo = 0 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CNPJ="'+Geral.SoNumero_TT(EdCNPJ.Text)+'"');
      QrDuplic2.SQL.Add('AND Codigo <> ' + Geral.FF0(Codigo));
    end;
    if Tipo = 1 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CPF="'+Geral.SoNumero_TT(EdCNPJ.Text)+'"');
      QrDuplic2.SQL.Add('AND Codigo <> ' + Geral.FF0(Codigo));
    end;
    QrDuplic2.Open;
    if GOTOy.Registros(QrDuplic2) > 0 then
    begin
      Geral.MB_Aviso('Esta entidade j� foi cadastrada com o c�digo n� ' +
        Geral.FF0(QrDuplic2Codigo.Value) + '!');
      QrDuplic2.Close;
      Screen.Cursor := crDefault;
      Exit;
    end;
    QrDuplic2.Close;
  end;
  //
  if Length(Geral.SoNumero1a9_TT(EdCNPJ.Text)) > 0 then
  begin
    if Entities.CNPJCPFDuplicado(Codigo, 0, EdCNPJ.Text, '', ImgTipo.SQLType) then Exit;
  end;
  //
  Ativo       := Geral.BoolToInt(CkAtivo.Checked);
  RazaoSocial := EdRazaoSocial.ValueVariant;
  CNPJ        := Geral.FormataCNPJ_TFT(EdCNPJ.ValueVariant);
  IE          := EdIE.ValueVariant;
  Protestar   := Geral.IMV(Copy(RGProtestar.Items[RGProtestar.ItemIndex], 1, 2));
  //
  if MyObjects.FIC(RazaoSocial = '', EdRazaoSocial, 'Raz�o social n�o definida!') then Exit;
  //
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    QrLoc0.Close;
    QrLoc0.Open;
    Codigo := QrLoc0CodMax.Value;
    if Codigo <> 999 then Codigo := Codigo + 1 else Codigo :=
      UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Entidades',
      'Entidades', 'Codigo');
    //Dmod.QrUpdU.SQL.Add('INSERT INTO entidades SET ');
  end else begin
    Codigo := QrEntidadesCodigo.Value;
    //Dmod.QrUpdU.SQL.Add('UPDATE entidades SET AlterWeb=1, ');
  end;
  {
  Dmod.QrUpdU.SQL.Add('Cliente1 = "V", RazaoSocial=:P0, CNPJ=:P1, IE=:P2, ');
  Dmod.QrUpdU.SQL.Add('ERua=:P3, ENumero=:P4, ECompl=:P5, EBairro=:P6, ');
  Dmod.QrUpdU.SQL.Add('ECidade=:P7, EUF=:P8, ECEP=:P9, EPais = "BRASIL", ');
  Dmod.QrUpdU.SQL.Add('ETe1=:P10, EFax=:P11, EEmail=:P12, ENatal=:P13, ');
  Dmod.QrUpdU.SQL.Add('Cadastro=:P14, Nivel=:P15, FormaSociet=:P16, ');
  Dmod.QrUpdU.SQL.Add('NIRE=:P17, Simples=:P18, ELograd=:P19, LimiCred=:P20, ');
  Dmod.QrUpdU.SQL.Add('FatorCompra=:P21, AdValorem=:P22, DMaisC=:P23, ');
  Dmod.QrUpdU.SQL.Add('DMaisD=:P24, Tipo=0, Atividade=:P25, Fantasia=:P26, ');
  Dmod.QrUpdU.SQL.Add('CBE=:P27, SCB=:P28, Contab=:P29, MSN1=:P30, ');
  Dmod.QrUpdU.SQL.Add('PastaTxtFTP=:P31, PastaPwdFTP=:P32, MultaCodi=:P33, ');
  Dmod.QrUpdU.SQL.Add('MultaDias=:P34, MultaValr=:P35, MultaPerc=:P36, ');
  Dmod.QrUpdU.SQL.Add('Protestar=:P37, JuroSacado=:P38, CPMF=:P39, ');
  Dmod.QrUpdU.SQL.Add('Observacoes=:P40, Corrido=:P41, ');
  //
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:py, Codigo=:Pz');
  end else begin
    Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:py WHERE Codigo=:Pz');
  end;
  }
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'entidades', False, [
    'Cliente1', 'RazaoSocial', 'CNPJ', 'IE',
    'Nivel', 'FormaSociet',
    'Simples', 'LimiCred',
    'FatorCompra', 'AdValorem', 'DMaisC',
    'DMaisD', 'Tipo', 'Atividade', 'Fantasia',
    'CBE', 'SCB', 'Contab', 'MSN1',
    'PastaTxtFTP', 'PastaPwdFTP', 'MultaCodi',
    'MultaDias', 'MultaValr', 'MultaPerc',
    'Protestar', 'JuroSacado', 'CPMF',
    'Corrido', 'Ativo', 'CodUsu'
  ], ['Codigo'], [
    'V', RazaoSocial, CNPJ, IE,
    EdNivel.Text, EdFormaSociet.Text,
    Geral.BoolToInt(CkSimples.Checked), EdLimiCred.ValueVariant,
    EdFatorCompra.ValueVariant, EdAdValorem.ValueVariant, EdDMaisC.ValueVariant,
    EdDMaisD.ValueVariant, Tipo, EdAtividade.Text, EdFantasia.Text,
    EdCBE.ValueVariant, Geral.BoolToInt(CkSCB.Checked), EdContab.Text, EdMSN1.Text,
    EdPastaTxtFTP.Text, EdPastaPwdFTP.Text, RGMultaCodi.ItemIndex,
    EdMultaDias.ValueVariant, EdMultaValr.ValueVariant, EdMultaPerc.ValueVariant,
    Protestar, EdJuroSacado.ValueVariant, EdCPMF.ValueVariant,
    EdCorrido.ValueVariant, Ativo, Codigo
  ], [Codigo], True) then
  begin
    UMyMod.UpdUnlockY(QrEntidadesCodigo.Value, Dmod.MyDB, 'Entidades', 'Codigo');
    //
    VAR_MUDOUENTIDADES := True;
    ////////////////////////////////////////////////
    VAR_ENTIDADE := Codigo;                       //
    FmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade2, Codigo, False); //
    ////////////////////////////////////////////////
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Entidades', 'Codigo');
    MostraEdicao(0, stLok, 0);
    AtualizaDadosSPC_Config(Codigo);
    ReopenEntidades(Codigo);
    LocCod(Codigo, Codigo);
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmEntiJur1.BtDesisteClick(Sender: TObject);
begin
  UMyMod.UpdUnlockY(QrEntidadesCodigo.Value, Dmod.MyDB, 'Entidades', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmEntiJur1.QrEntidadesAfterScroll(DataSet: TDataSet);
begin
  //BtExclui.Enabled := False;
  FmPrincipal.AcoesIniciaisDeCadastroDeEntidades(FmEntiJur1,
    QrEntidadesCodigo.Value, Grade1);
  ReopenSocios;
  ReopenTaxasCli();
  ReopenOcorCli(0);
end;

procedure TFmEntiJur1.IncluinovoClienteJurdico1Click(Sender: TObject);
begin
  //MostraEdicao(1, stIns, 0); N�o incluir por aqui!
  DModG.CadastroDeEntidade(QrEntidadesCodigo.Value, fmcadEntidade2,
    fmcadEntidade2, False, False, nil, nil, False, uetCliente1);
  //
  if VAR_CADASTRO <> 0 then
    LocCod(VAR_CADASTRO, VAR_CADASTRO)
  else
    Va(vpLast);
end;

procedure TFmEntiJur1.Alteraclienteatual1Click(Sender: TObject);
begin
  MostraEdicao(1, stUpd, QrEntidadesCodigo.Value);
end;

procedure TFmEntiJur1.Pesquisarantesdeimprimir1Click(Sender: TObject);
begin
  MyObjects.FormShow(TFmEntidadesImp, FmEntidadesImp);
end;

procedure TFmEntiJur1.PMClientePopup(Sender: TObject);
begin
  if QrEntidades.State <> dsBrowse then Alteraclienteatual1.Enabled := False
  else Alteraclienteatual1.Enabled := True;
end;

procedure TFmEntiJur1.PMImprimePopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0);
  //
  Scios1.Enabled                            := Enab;
  Risco1.Enabled                            := Enab;
  ContratodeFomentoMercantil1.Enabled       := Enab;
  Formulrioparapreenchimentomanual1.Enabled := Enab;
  Pesquisarantesdeimprimir1.Enabled         := Enab;
end;

procedure TFmEntiJur1.PMOcorPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0);
  Enab2 := (QrOcorCli.State <> dsInactive) and (QrOcorCli.RecordCount > 0);
  //
  Incluiocorrncia1.Enabled := Enab;
  Alteraocorrncia1.Enabled := Enab2;
  Excluiocorncia1.Enabled  := Enab2;
end;

procedure TFmEntiJur1.PMSociosPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0);
  Enab2 := (QrSocios.State <> dsInactive) and (QrSocios.RecordCount > 0);
  //
  Adiciona1.Enabled := Enab;
  Altera1.Enabled   := Enab2;
  Remove1.Enabled   := Enab2;
end;

procedure TFmEntiJur1.PMTaxasPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0);
  Enab2 := (QrTaxasCli.State <> dsInactive) and (QrTaxasCli.RecordCount > 0);
  //
  Adicionataxa1.Enabled := Enab;
  Retirataxa1.Enabled   := Enab2;
end;

procedure TFmEntiJur1.BtGestorClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //
  MyObjects.MostraPopUpDeBotao(PMSocios, BtGestor);
end;

procedure TFmEntiJur1.ReopenSocios;
begin
  QrSocios.Close;
  QrSocios.Params[0].AsInteger := QrEntidadesCodigo.Value;
  QrSocios.Open;
  //
  if FSocio > 0 then QrSocios.Locate('Codigo', FSocio, []);
end;

procedure TFmEntiJur1.ReopenTaxasCli();
begin
  QrTaxasCli.Close;
  QrTaxasCli.Params[0].AsInteger := QrEntidadesCodigo.Value;
  QrTaxasCli.Open;
  //
  if FTaxaCli > 0 then QrTaxasCli.Locate('Controle', FTaxaCli, []);
end;

procedure TFmEntiJur1.ReopenOcorCli(Controle: Integer);
begin
  QrOcorCli.Close;
  QrOcorCli.Params[0].AsInteger := QrEntidadesCodigo.Value;
  QrOcorCli.Open;
  //
  QrOcorCli.Locate('Controle', Controle, []);
end;

procedure TFmEntiJur1.Adiciona1Click(Sender: TObject);
begin
  if VAR_REPRES_ENTIJUR1 = '' then
    Geral.MB_Aviso('Falta defini��o dos Representantes!')
  else begin
    Application.CreateForm(TFmEntiJur1Respons, FmEntiJur1Respons);
    FmEntiJur1Respons.FEmpresa  := QrEntidadesCodigo.Value;
    FmEntiJur1Respons.FMaxOrdem := QrSocios.RecordCount+1;
    FmEntiJur1Respons.ShowModal;
    FmEntiJur1Respons.Destroy;
    //
    FSocio := VAR_ENTIDADE;
    ReopenSocios;
  end;
end;

procedure TFmEntiJur1.Remove1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a retirada do s�cio desta entidade?') = ID_YES then
  begin
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM socios WHERE Empresa=:P0');
    Dmod.QrUpdM.SQL.Add('AND Socio=:P1');
    Dmod.QrUpdM.Params[0].AsInteger := QrSociosEmpresa.Value;
    Dmod.QrUpdM.Params[1].AsInteger := QrSociosSocio.Value;
    Dmod.QrUpdM.ExecSQL;
    //
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('UPDATE socios SET AlterWeb=1, Ordem=Ordem-1, AlterWeb=1 ');
    Dmod.QrUpdM.SQL.Add('WHERE Ordem>= :P0 AND Empresa=:P1');
    Dmod.QrUpdM.Params[00].AsInteger := QrSociosOrdem.Value;
    Dmod.QrUpdM.Params[01].AsInteger := QrSociosEmpresa.Value;
    Dmod.QrUpdM.ExecSQL;
    //
    ReopenSocios;
  end;
end;

procedure TFmEntiJur1.Altera1Click(Sender: TObject);
begin
  FSocio := QrSociosCodigo.Value;
  //
  DModG.CadastroDeEntidade(FSocio, fmcadEntidade2, fmcadEntidade2,
    False, False, nil, nil, False, uetFornece5);
  //
  ReopenSocios;
  (*
  FSocio := QrSociosCodigo.Value;
  if DBCheck.CriaFm(TFmEntiSocio1, FmEntiSocio1, afmoNegarComAviso) then
  begin
    with FmEntiSocio1 do
    begin
      ImgTipo.SQLType      := stUpd;
      EdCodigo.ValueVariant := QrSociosCodigo.Value;
      EdNome.Text         := QrSociosNOMESOCIO.Value;
      EdCNPJCPF.Text      := Geral.FormataCNPJ_TFT(QrSociosCPF.Value);
      EdIERG.Text         := QrSociosRG.Value;
      EdSSP.Text          := QrSociosSSp.Value;
      EdCidadeNatal.Text  := QrSociosCidadeNatal.Value;
      EdNacionalid.Text   := QrSociosNacionalid.Value;
      EdRua.Text          := QrSociosPRua.Value;
      EdPNumero.Text      := Geral.FormataNumeroDeRua(QrSociosPRua.Value, QrSociosPNumero.Value, True);
      EdCompl.Text        := QrSociosPCompl.Value;
      EdBairro.Text       := QrSociosPBairro.Value;
      EdCEP.Text          :=Geral.FormataCEP_NT(QrSociosPCEP.Value);
      EdTe1.Text          := Geral.FormataTelefone_TT(QrSociosPTe1.Value);
      EdCel.Text          := Geral.FormataTelefone_TT(QrSociosPCel.Value);
      EdConjuge.Text      := QrSociosConjugeNome.Value;
      EdProfissao.Text    := QrSociosProfissao.Value;
      EdCargo.Text        := QrSociosCargo.Value;
      EdPai.Text          := QrSociosPai.Value;
      EdMae.Text          := QrSociosMae.Value;
      //
      TPNatal.Date        := QrSociosPNatal.Value;
      CBEstCivil.KeyValue := QrSociosEstCivil.Value;
      TPDataRG.Date       := QrSociosDataRG.Value;
      CBPUF.KeyValue      := QrSociosPUF.Value;
      CBPLograd.KeyValue  := QrSociosPLograd.Value;
      if QrSociosSexo.Value = 'M' then CBSexo.ItemIndex := 0
      else CBSexo.ItemIndex := 1;
      //
      if QrSociosCliente1.Value = 'V' then CkCliente1_0.Checked := True;
      if QrSociosCliente2.Value = 'V' then CkCliente2_0.Checked := True;
      if QrSociosFornece1.Value = 'V' then CkFornece1_0.Checked := True;
      if QrSociosFornece2.Value = 'V' then CkFornece2_0.Checked := True;
      if QrSociosFornece3.Value = 'V' then CkFornece3_0.Checked := True;
      if QrSociosFornece4.Value = 'V' then CkFornece4_0.Checked := True;
      if QrSociosFornece5.Value = 'V' then CkFornece5_0.Checked := True;
      if QrSociosFornece6.Value = 'V' then CkFornece6_0.Checked := True;
      if QrSociosTerceiro.Value = 'V' then CkTerceiro_0.Checked := True;
      //
    end;
    FmEntiSocio1.ShowModal;
    FmEntiSocio1.Destroy;
    //
    ReopenSocios;
  end;
  *)
end;

procedure TFmEntiJur1.SbNovoClick(Sender: TObject);
begin
  // N�o pode, gera erro ao desenhar Grade!!!
  //FmPrincipal.CadastroEntidades;
end;

procedure TFmEntiJur1.Scios1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSociosImp, FmSociosImp, afmoNegarComAviso) then
  begin
    FmSociosImp.EdEmpresa.ValueVariant := QrEntidadesCodigo.Value;
    FmSociosImp.CBEmpresa.KeyValue     := QrEntidadesCodigo.Value;
    FmSociosImp.ShowModal;
    FmSociosImp.Destroy;
  end;
end;

procedure TFmEntiJur1.Risco1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRiscoCli, FmRiscoCli, afmoNegarComAviso) then
  begin
    FmRiscoCli.EdCliente.ValueVariant := QrEntidadesCodigo.Value;
    FmRiscoCli.CBCliente.KeyValue     := QrEntidadesCodigo.Value;
    FmRiscoCli.BtPesquisaClick(Self);
    FmRiscoCli.ShowModal;
    FmRiscoCli.Destroy;
  end;
end;

procedure TFmEntiJur1.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmEntiJur1.ContratodeFomentoMercantil1Click(Sender: TObject);
begin
  Application.CreateForm(TFmContratoImp, FmContratoImp);
  if FmContratoImp.QrContratos.State = dsBrowse then
    FmContratoImp.QrContratos.Locate('Cliente', QrEntidadesCodigo.Value, []);
  FmContratoImp.ShowModal;
  FmContratoImp.Destroy;
end;

procedure TFmEntiJur1.BtTaxasClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 3;
  //
  MyObjects.MostraPopUpDeBotao(PMTaxas, BtTaxas);
end;

procedure TFmEntiJur1.Adicionataxa1Click(Sender: TObject);
begin
  MostraEntiJur1Taxa(stIns, QrEntidadesCodigo.Value);
end;

procedure TFmEntiJur1.BtDesiste2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, QrEntidadesCodigo.Value);
end;

procedure TFmEntiJur1.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      //
      PainelControle.Visible := True;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then ConfiguraInclusao else ConfiguraAlteracao;
      PageControl2.ActivePageIndex := 0;
      EdRazaoSocial.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEntiJur1.Retirataxa1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a Retirada da taxa deste cliente?', 'TaxasCli',
    'Controle', QrTaxasCliControle.Value, Dmod.MyDB) = ID_YES then
  begin
    FTaxaCli := GOTOy.LocalizaPriorNextIntQr(QrTaxasCli, QrTaxasCliControle,
    QrTaxasCliControle.Value);
    ReopenTaxasCli();
  end;
end;

procedure TFmEntiJur1.ReopenEntidades(Codigo: Integer);
begin
  QrEntidades.Close;
  QrEntidades.Open;
  QrEntidades.Locate('Codigo', Codigo, []);
end;

procedure TFmEntiJur1.EdPastaTxtFTPExit(Sender: TObject);
begin
  Randomize;
  EdPastaPwdFTP.Text := dmkPF.PWDExEncode(EdPastaTxtFTP.Text, '/z8niKfqbXvTw9xe');
end;

procedure TFmEntiJur1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then EdPastaPwdFTP.ReadOnly := False;
end;

procedure TFmEntiJur1.EdPastaPwdFTPExit(Sender: TObject);
begin
  EdPastaPwdFTP.ReadOnly := True;
end;

procedure TFmEntiJur1.EdCNPJExit(Sender: TObject);
var
  Num : String;
  CNPJ : String;
begin
  CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
  if CNPJ <> CO_VAZIO then
    begin
    Num := Geral.CalculaCNPJCPF(CNPJ);
    if Geral.FormataCNPJ_TFT(CNPJ) <> Num then
    begin
      Geral.MensagemBox(SMLA_NUMEROINVALIDO2, 'Aviso', MB_OK+MB_ICONWARNING);
      EdCNPJ.SetFocus;
    end else begin
      EdCNPJ.Text := Geral.FormataCNPJ_TT(CNPJ);
      Entities.CNPJCPFDuplicado(QrEntidadesCodigo.Value, 0, EdCNPJ.Text, '', ImgTipo.SQLType);
    end;
  end else EdCNPJ.Text := CO_VAZIO;
end;

procedure TFmEntiJur1.BtOcorClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 4;
  //
  MyObjects.MostraPopUpDeBotao(PMOcor, BtOcor);
end;

procedure TFmEntiJur1.Alteraocorrncia1Click(Sender: TObject);
begin
  MostraEntiJur1Ocor(stUpd);
end;

procedure TFmEntiJur1.Incluiocorrncia1Click(Sender: TObject);
begin
  MostraEntiJur1Ocor(stIns);
end;

procedure TFmEntiJur1.MostraEntiJur1Ocor(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEntiJur1Ocor, FmEntiJur1Ocor, afmoNegarComAviso) then
  begin
    FmEntiJur1Ocor.ImgTipo.SQLType := SQLType;
    FmEntiJur1Ocor.FCliente := QrEntidadesCodigo.Value;
    if SQLType = stUpd then
    begin
      FmEntiJur1Ocor.FControle             := QrOcorCliControle.value;
      FmEntiJur1Ocor.EdOcor.ValueVariant   := QrOcorCliOcorrencia.Value;
      FmEntiJur1Ocor.CBOcor.KeyValue       := QrOcorCliOcorrencia.Value;
      FmEntiJur1Ocor.EdBase.ValueVariant   := QrOcorCliBase.Value;
      FmEntiJur1Ocor.RGFormaCNAB.ItemIndex := QrOcorCliFormaCNAB.Value;
    end else begin
      FmEntiJur1Ocor.FControle             := 0;
      FmEntiJur1Ocor.RGFormaCNAB.ItemIndex := 3;
    end;
    FmEntiJur1Ocor.ShowModal;
    if FmEntiJur1Ocor.FResult then
      ReopenOcorCli(FmEntiJur1Ocor.FControle);
    FmEntiJur1Ocor.Destroy;
  end;
end;

procedure TFmEntiJur1.MostraEntiJur1Taxa(SQLType: TSQLType; Entidade: Integer);
begin
  if DBCheck.CriaFm(TFmEntiJur1Taxa, FmEntiJur1Taxa, afmoNegarComAviso) then
  begin
    with FmEntiJur1Taxa do
    begin
      ImgTipo.SQLType := SQLType;
      if SQLType = stUpd then
      begin
        EdTaxa.ValueVariant  := QrTaxasCliTaxa.Value;
        CBTaxa.KeyValue      := QrTaxasCliTaxa.Value;
        EdValor.ValueVariant := QrTaxasCliValor.Value;
      end;
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmEntiJur1.Excluiocorncia1Click(Sender: TObject);
begin
  UMyMod.SQLDel1(Dmod.QrUpd, QrocorCli, 'ocorcli', 'Controle',
  QrOcorCliControle.Value, True, 'Confirma a exclus�o da ocorr�ncia "' +
  QrOcorCliNOMEOCOR.Value + '"?', False);
end;

procedure TFmEntiJur1.QrOcorCliCalcFields(DataSet: TDataSet);
begin
  case QrOcorCliFormaCNAB.Value of
    0: QrOcorCliFormaCNAB_TXT.Value := 'N�o cobrar';
    1: QrOcorCliFormaCNAB_TXT.Value := 'Cobrar o mesmo valor do banco';
    2: QrOcorCliFormaCNAB_TXT.Value := 'Cobrar o valor base deste cadastro';
    3: QrOcorCliFormaCNAB_TXT.Value := 'Cobrar o valor base do cadastro da ocorr�ncia';
  end;
end;

function TFmEntiJur1.AtualizaDadosSPC_Config(Entidade: Integer): Boolean;
var
  SPC_Config: Integer;
  ValMin, ValMax: Double;
begin
  SPC_Config := EdSPC_Config.ValueVariant;
  ValMin := EdValMin.ValueVariant;
  ValMax := EdValMax.ValueVariant;
  //
  Result := UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'spc_entida', False, [
    'SPC_Config', 'ValMin', 'ValMax'], ['Entidade'], [
    'SPC_Config', 'ValMin', 'ValMax'], [
    SPC_Config, ValMin, ValMax], [Entidade], [
    SPC_Config, ValMin, ValMax], True);
end;

end.

