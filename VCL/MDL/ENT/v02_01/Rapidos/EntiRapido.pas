unit EntiRapido;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls,   UnInternalConsts,
      Buttons,
  DBCtrls, Db, (*DBTables,*) UnMLAGeral, UnGOTOy, Mask, UMySQLModule,
   LMDCustomGroupBox, LMDCustomButtonGroup,
  LMDCustomRadioGroup, LMDRadioGroup, mySQLDbTables, 
   ComCtrls;

type
  TFmEntiRapido = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label25: TLabel;
    EdNome: TLMDEdit;
    Label43: TLabel;
    EdTe1: TLMDEdit;
    Label36: TLabel;
    EdCel: TLMDEdit;
    Label45: TLabel;
    TPNatal: TDateTimePicker;
    Label29: TLabel;
    EdCNPJCPF: TLMDEdit;
    Label30: TLabel;
    EdIERG: TLMDEdit;
    Label31: TLabel;
    EdRua: TLMDEdit;
    Label32: TLabel;
    EdNumero: TLMDEdit;
    Label33: TLabel;
    EdCompl: TLMDEdit;
    Label34: TLabel;
    EdBairro: TLMDEdit;
    Label39: TLabel;
    EdCEP: TLMDEdit;
    RGTipo: TRadioGroup;
    QrDuplic2: TmySQLQuery;
    QrDuplic2Codigo: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdCNPJCPFExit(Sender: TObject);
    procedure EdCEPExit(Sender: TObject);
    procedure EdTe1Exit(Sender: TObject);
    procedure EdCelExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmEntiRapido: TFmEntiRapido;

implementation

uses UnMyObjects, Module, Entidades, ResIntStrings;

{$R *.DFM}

procedure TFmEntiRapido.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiRapido.BtConfirmaClick(Sender: TObject);
var
  PUF, EUF, Veiculo, Codigo, Motivo,
  ELograd, PLograd, CLograd, LLograd, CasasApliDesco, Tipo: Integer;
  Cliente1, Cliente2, Fornece1, Fornece2, Fornece3, Fornece4: String;
  Terceiro, Mensal, Agenda, SenhaQuer: String;
  AjudaV, AjudaP, LimiCred, Comissao, Desco: Double;
  //
  Razao, Nome, CPF, CNPJ, IE, RG, ERua, PRua, EBairro, PBairro, ECompl, PCompl,
  ECidade, PCidade, ECEP, PCEP, ETe1, PTe1, ECel, PCel, ENatal, PNatal: String;
  ENum, PNum: Integer;
begin
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Entidades',
              'Entidades', 'Codigo');
    Tipo := RGTipo.ItemIndex;
    if (Length(Geral.SoNumero1a9_TT(EdCNPJCPF.Text))>0)
    then begin
      QrDuplic2.Close;
      QrDuplic2.SQL.Clear;
      if Tipo = 0 then
      begin
        QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
        QrDuplic2.SQL.Add('WHERE CNPJ="'+Geral.SoNumero_TT(EdCNPJCPF.Text)+'"');
        QrDuplic2.SQL.Add('AND Codigo <> '+IntToStr(Codigo));
      end;
      if Tipo = 1 then
      begin
        QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
        QrDuplic2.SQL.Add('WHERE CPF="'+Geral.SoNumero_TT(EdCNPJCPF.Text)+'"');
        QrDuplic2.SQL.Add('AND Codigo <> '+IntToStr(Codigo));
      end;
      QrDuplic2.Open;
      if GOTOy.Registros(QrDuplic2) > 0 then
      begin
        Application.MessageBox(PChar('Esta entidade j� foi cadastrada com o c�digo n� '+
        IntToStr(QrDuplic2Codigo.Value)+'.'), 'Entidade duplicada', MB_OK+MB_ICONWARNING);
        QrDuplic2.Close;
        Screen.Cursor := crDefault;
        Exit;
      end;
      QrDuplic2.Close;
    end;
    if Tipo = 0 then
    begin
      Razao   := EdNome.Text;
      Nome    := '';
      CNPJ    := MLAGeral.FormataCNPJ_TFT(EdCNPJCPF.Text);
      CPF     := '';
      IE      := EdIERG.Text;
      RG      := '';
      ERua    := EdRua.Text;
      PRua    := '';
      ENum    := Geral.IMV(EdNumero.Text);
      PNum    := 0;
      ECompl  := EdCompl.Text;
      PCompl  := '';
      EBairro := EdBairro.Text;
      PBairro := '';
      ECidade := VAR_CIDADEPADRAO;
      PCidade := '';
      ECEP    := Geral.SoNumero_TT(EdCEP.Text);
      PCEP    := '';
      ETe1    := MlaGeral.SoNumeroESinal_TT(EdTe1.Text);
      PTe1    := '';
      ECel    := MlaGeral.SoNumeroESinal_TT(EdCel.Text);
      PCel    := '';
      ENatal  := FormatDateTime(VAR_FORMATDATE, TPNatal.Date);
      PNatal  := '0000-00-00';
      EUF := VAR_UFPADRAO;
      PUF := 0;
    end else begin
      Nome  := EdNome.Text;
      Razao := '';
      CPF   := MLAGeral.FormataCNPJ_TFT(EdCNPJCPF.Text);
      CNPJ  := '';
      RG    := EdIERG.Text;
      IE    := '';
      ERua    := EdRua.Text;
      PRua    := '';
      PNum    := Geral.IMV(EdNumero.Text);
      ENum    := 0;
      PCompl  := EdCompl.Text;
      ECompl  := '';
      PBairro := EdBairro.Text;
      EBairro := '';
      PCidade := VAR_CIDADEPADRAO;
      ECidade := '';
      PCEP    := Geral.SoNumero_TT(EdCEP.Text);
      ECEP    := '';
      PTe1    := MlaGeral.SoNumeroESinal_TT(EdTe1.Text);
      ETe1    := '';
      PCel    := MlaGeral.SoNumeroESinal_TT(EdCel.Text);
      ECel    := '';
      PNatal  := FormatDateTime(VAR_FORMATDATE, TPNatal.Date);
      ENatal  := '0000-00-00';
      PUF := VAR_UFPADRAO;
      EUF := 0;
    end;
    Mensal   := 'F';
    Veiculo  := 0;
    LimiCred := 0;
    Comissao := 0;
    Desco    := 0;
    CasasApliDesco := 0;
    Motivo := 0;
    ELograd := 0;
    PLograd := 0;
    CLograd := 0;
    LLograd := 0;
    Cliente1 := 'V';
    Cliente2 := 'F';
    Fornece1 := 'F';
    Fornece2 := 'F';
    Fornece3 := 'F';
    Fornece4 := 'F';
    Terceiro := 'F';
    AjudaV := 0;
    AjudaP := 0;
    Agenda := 'F';
    SenhaQuer := 'F';
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO entidades SET ');
    Dmod.QrUpdU.SQL.Add('RazaoSocial=:P0, Fantasia=:P1, Respons1=:P2, ');
    Dmod.QrUpdU.SQL.Add('Respons2=:P3, Pai=:P4, Mae=:P5, CNPJ=:P6, IE=:P7, ');
    Dmod.QrUpdU.SQL.Add('Nome=:P8, Apelido=:P9, CPF=:P10, RG=:P11, ');
    Dmod.QrUpdU.SQL.Add('ERua=:P12, ENumero=:P13, ECompl=:P14, EBairro=:P15, ');
    Dmod.QrUpdU.SQL.Add('ECidade=:P16, EUF=:P17, ECEP=:P18, EPais=:P19, ');
    Dmod.QrUpdU.SQL.Add('ETe1=:P20, ETe2=:P21, ETe3=:P22, ECel=:P23, ');
    Dmod.QrUpdU.SQL.Add('EFax=:P24, EEmail=:P25, EContato=:P26, ENatal=:P27, ');
    Dmod.QrUpdU.SQL.Add('PRua=:P28, PNumero=:P29, PCompl=:P30, PBairro=:P31, ');
    Dmod.QrUpdU.SQL.Add('PCidade=:P32, PUF=:P33, PCEP=:P34, PPais=:P35, ');
    Dmod.QrUpdU.SQL.Add('PTe1=:P36, PTe2=:P37, PTe3=:P38, PCel=:P39, ');
    Dmod.QrUpdU.SQL.Add('PFax=:P40, PEmail=:P41, PContato=:P42, PNatal=:P43, ');
    Dmod.QrUpdU.SQL.Add('Sexo=:P44, Responsavel=:P45, Cliente1=:P46, ');
    Dmod.QrUpdU.SQL.Add('Cliente2=:P47, Fornece1=:P48, Fornece2=:P49, ');
    Dmod.QrUpdU.SQL.Add('Fornece3=:P50, Fornece4=:P51, Terceiro=:P52, ');
    Dmod.QrUpdU.SQL.Add('Cadastro=:P53, Informacoes=:P54, Veiculo=:P55, ');
    Dmod.QrUpdU.SQL.Add('Mensal=:P56, Observacoes=:P57, Tipo=:P58, ');
    Dmod.QrUpdU.SQL.Add('Profissao=:P59, Cargo=:P60, Recibo=:P61, DiaRecibo=:P62,');
    Dmod.QrUpdU.SQL.Add('AjudaEmpV=:P63, AjudaEmpP=:P64, ');
    //
    Dmod.QrUpdU.SQL.Add('CRua=:P65, CNumero=:P66, CCompl=:P67, CBairro=:P68, ');
    Dmod.QrUpdU.SQL.Add('CCidade=:P69, CUF=:P70, CCEP=:P71, CPais=:P72, ');
    Dmod.QrUpdU.SQL.Add('CTel=:P73, CCel=:P74, CFax=:P75, CContato=:P76, ');
    //
    Dmod.QrUpdU.SQL.Add('LRua=:P77, LNumero=:P78, LCompl=:P79, LBairro=:P80, ');
    Dmod.QrUpdU.SQL.Add('LCidade=:P81, LUF=:P82, LCEP=:P83, LPais=:P84, ');
    Dmod.QrUpdU.SQL.Add('LTel=:P85, LCel=:P86, LFax=:P87, LContato=:P88, ');
    Dmod.QrUpdU.SQL.Add('Grupo=:P89, Situacao=:P90, Nivel=:P91, Account=:P92,');
    //
    Dmod.QrUpdU.SQL.Add('ConjugeNome=:P93, Nome1=:P94, Nome2=:P95, Nome3=:P96,');
    Dmod.QrUpdU.SQL.Add('Nome4=:P97, ConjugeNatal=:P98, Natal1=:P99, ');
    Dmod.QrUpdU.SQL.Add('Natal2=:P100, Natal3=:P101, Natal4=:P102, ');
    Dmod.QrUpdU.SQL.Add('Motivo=:P103, Agenda=:P104, SenhaQuer=:P105, ');
    Dmod.QrUpdU.SQL.Add('Senha1=:P106, ELograd=:P107, PLograd=:P108, ');
    Dmod.QrUpdU.SQL.Add('CLograd=:P109, LLograd=:P110, LimiCred=:P111, ');
    //
    Dmod.QrUpdU.SQL.Add('Comissao=:P112, Desco=:P113, CasasApliDesco=:P114,');
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:py, Codigo=:Pz');

    ////////////////////////////////////////////////////////////////////////////

    Dmod.QrUpdU.Params[00].AsString  := Razao;
    Dmod.QrUpdU.Params[01].AsString  := '';//EdFantasia.Text;
    Dmod.QrUpdU.Params[02].AsString  := '';//EdRespons1.Text;
    Dmod.QrUpdU.Params[03].AsString  := '';//EdRespons2.Text;
    Dmod.QrUpdU.Params[04].AsString  := '';//EdPai.Text;
    Dmod.QrUpdU.Params[05].AsString  := '';//EdMae.Text;
    Dmod.QrUpdU.Params[06].AsString  := CNPJ;
    Dmod.QrUpdU.Params[07].AsString  := IE;
    Dmod.QrUpdU.Params[08].AsString  := Nome;
    Dmod.QrUpdU.Params[09].AsString  := '';//EdApelido.Text;
    Dmod.QrUpdU.Params[10].AsString  := CPF;
    Dmod.QrUpdU.Params[11].AsString  := RG;

    Dmod.QrUpdU.Params[12].AsString  := ERua;
    Dmod.QrUpdU.Params[13].AsInteger := ENum;
    Dmod.QrUpdU.Params[14].AsString  := ECompl;
    Dmod.QrUpdU.Params[15].AsString  := EBairro;
    Dmod.QrUpdU.Params[16].AsString  := ECidade;
    Dmod.QrUpdU.Params[17].AsInteger := EUF;
    Dmod.QrUpdU.Params[18].AsString  := ECEP;
    Dmod.QrUpdU.Params[19].AsString  := 'Brasil';
    Dmod.QrUpdU.Params[20].AsString  := ETe1;
    Dmod.QrUpdU.Params[21].AsString  := '';
    Dmod.QrUpdU.Params[22].AsString  := '';
    Dmod.QrUpdU.Params[23].AsString  := ECel;
    Dmod.QrUpdU.Params[24].AsString  := '';
    Dmod.QrUpdU.Params[25].AsString  := '';
    Dmod.QrUpdU.Params[26].AsString  := '';
    Dmod.QrUpdU.Params[27].AsString  := ENatal;

    Dmod.QrUpdU.Params[28].AsString  := PRua;
    Dmod.QrUpdU.Params[29].AsInteger := PNum;
    Dmod.QrUpdU.Params[30].AsString  := PCompl;
    Dmod.QrUpdU.Params[31].AsString  := PBairro;
    Dmod.QrUpdU.Params[32].AsString  := PCidade;
    Dmod.QrUpdU.Params[33].AsInteger := PUF;
    Dmod.QrUpdU.Params[34].AsString  := PCEP;
    Dmod.QrUpdU.Params[35].AsString  := 'Brasil';
    Dmod.QrUpdU.Params[36].AsString  := PTe1;
    Dmod.QrUpdU.Params[37].AsString  := '';
    Dmod.QrUpdU.Params[38].AsString  := '';
    Dmod.QrUpdU.Params[39].AsString  := PCel;
    Dmod.QrUpdU.Params[40].AsString  := '';
    Dmod.QrUpdU.Params[41].AsString  := '';
    Dmod.QrUpdU.Params[42].AsString  := '';
    Dmod.QrUpdU.Params[43].AsString  := PNatal;

    Dmod.QrUpdU.Params[44].AsString  := '';
    Dmod.QrUpdU.Params[45].AsString  := '';
    Dmod.QrUpdU.Params[46].AsString  := Cliente1;
    Dmod.QrUpdU.Params[47].AsString  := Cliente2;
    Dmod.QrUpdU.Params[48].AsString  := Fornece1;
    Dmod.QrUpdU.Params[49].AsString  := Fornece2;
    Dmod.QrUpdU.Params[50].AsString  := Fornece3;
    Dmod.QrUpdU.Params[51].AsString  := Fornece4;
    Dmod.QrUpdU.Params[52].AsString  := Terceiro;
    Dmod.QrUpdU.Params[53].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[54].AsString  := '';
    Dmod.QrUpdU.Params[55].AsInteger := Veiculo;
    Dmod.QrUpdU.Params[56].AsString  := Mensal;
    Dmod.QrUpdU.Params[57].AsString  := '';
    Dmod.QrUpdU.Params[58].AsInteger := Tipo;
    Dmod.QrUpdU.Params[59].AsString  := '';
    Dmod.QrUpdU.Params[60].AsString  := '';
    Dmod.QrUpdU.Params[61].AsInteger := 0;
    Dmod.QrUpdU.Params[62].AsInteger := 1;
    Dmod.QrUpdU.Params[63].AsFloat   := AjudaV;
    Dmod.QrUpdU.Params[64].AsFloat   := AjudaP;

    Dmod.QrUpdU.Params[65].AsString  := '';
    Dmod.QrUpdU.Params[66].AsInteger := 0;
    Dmod.QrUpdU.Params[67].AsString  := '';
    Dmod.QrUpdU.Params[68].AsString  := '';
    Dmod.QrUpdU.Params[69].AsString  := '';
    Dmod.QrUpdU.Params[70].AsInteger := 0;
    Dmod.QrUpdU.Params[71].AsString  := '';
    Dmod.QrUpdU.Params[72].AsString  := '';
    Dmod.QrUpdU.Params[73].AsString  := '';
    Dmod.QrUpdU.Params[74].AsString  := '';
    Dmod.QrUpdU.Params[75].AsString  := '';
    Dmod.QrUpdU.Params[76].AsString  := '';

    Dmod.QrUpdU.Params[77].AsString  := '';
    Dmod.QrUpdU.Params[78].AsInteger := 0;
    Dmod.QrUpdU.Params[79].AsString  := '';
    Dmod.QrUpdU.Params[80].AsString  := '';
    Dmod.QrUpdU.Params[81].AsString  := '';
    Dmod.QrUpdU.Params[82].AsInteger := 0;
    Dmod.QrUpdU.Params[83].AsString  := '';
    Dmod.QrUpdU.Params[84].AsString  := '';
    Dmod.QrUpdU.Params[85].AsString  := '';
    Dmod.QrUpdU.Params[86].AsString  := '';
    Dmod.QrUpdU.Params[87].AsString  := '';
    Dmod.QrUpdU.Params[88].AsString  := '';
    Dmod.QrUpdU.Params[89].AsInteger := 0;
    Dmod.QrUpdU.Params[90].AsInteger := 0;
    Dmod.QrUpdU.Params[91].AsString  := '';
    Dmod.QrUpdU.Params[92].AsInteger := 0;
    //
    Dmod.QrUpdU.Params[93].AsString   := '';
    Dmod.QrUpdU.Params[94].AsString   := '';
    Dmod.QrUpdU.Params[95].AsString   := '';
    Dmod.QrUpdU.Params[96].AsString   := '';
    Dmod.QrUpdU.Params[97].AsString   := '';
    Dmod.QrUpdU.Params[98].AsString   := '0000-00-00';
    Dmod.QrUpdU.Params[99].AsString   := '0000-00-00';;
    Dmod.QrUpdU.Params[100].AsString  := '0000-00-00';;
    Dmod.QrUpdU.Params[101].AsString  := '0000-00-00';;
    Dmod.QrUpdU.Params[102].AsString  := '0000-00-00';;
    Dmod.QrUpdU.Params[103].AsInteger := Motivo;
    Dmod.QrUpdU.Params[104].AsString  := Agenda;
    Dmod.QrUpdU.Params[105].AsString  := SenhaQuer;
    Dmod.QrUpdU.Params[106].AsString  := '';
    Dmod.QrUpdU.Params[107].AsInteger := ELograd;
    Dmod.QrUpdU.Params[108].AsInteger := PLograd;
    Dmod.QrUpdU.Params[109].AsInteger := CLograd;
    Dmod.QrUpdU.Params[110].AsInteger := LLograd;
    Dmod.QrUpdU.Params[111].AsFloat   := LimiCred;
    Dmod.QrUpdU.Params[112].AsFloat   := Comissao;
    Dmod.QrUpdU.Params[113].AsFloat   := Desco;
    Dmod.QrUpdU.Params[114].AsInteger := CasasApliDesco;
    //
    Dmod.QrUpdU.Params[115].AsString := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[116].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.Params[117].AsInteger := Codigo;
    Dmod.QrUpdU.ExecSQL;
    Screen.Cursor := crDefault;
    Close;
end;

procedure TFmEntiRapido.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,
  PainelTitulo, True, 0);
end;

procedure TFmEntiRapido.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiRapido.EdCNPJCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCNPJCPF.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Application.MessageBox(PChar(SMLA_NUMEROINVALIDO2), 'Erro', MB_OK+MB_ICONERROR);
      EdCNPJCPF.SetFocus;
    end else EdCNPJCPF.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCNPJCPF.Text := CO_VAZIO;
end;

procedure TFmEntiRapido.EdCEPExit(Sender: TObject);
begin
  EdCEP.Text := MLAGeral.FormataCEP_TT(EdCEP.Text);
end;

procedure TFmEntiRapido.EdTe1Exit(Sender: TObject);
begin
  EdTe1.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(EdTe1.Text));
end;

procedure TFmEntiRapido.EdCelExit(Sender: TObject);
begin
  EdCel.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(EdCel.Text));
end;

end.

