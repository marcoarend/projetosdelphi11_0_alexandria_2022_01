object FmEntiDocs: TFmEntiDocs
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-033 :: Documentos de Entidades'
  ClientHeight = 629
  ClientWidth = 1055
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1055
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1007
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 959
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 320
        Height = 32
        Caption = 'Documentos de Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 320
        Height = 32
        Caption = 'Documentos de Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 320
        Height = 32
        Caption = 'Documentos de Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1055
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1055
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1055
        Height = 467
        Align = alClient
        TabOrder = 0
        object Panel32: TPanel
          Left = 2
          Top = 15
          Width = 1051
          Height = 450
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Splitter1: TSplitter
            Left = 377
            Top = 0
            Width = 5
            Height = 450
          end
          object Panel33: TPanel
            Left = 0
            Top = 0
            Width = 377
            Height = 450
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 377
              Height = 450
              Align = alClient
              DataSource = DsEntiDocs
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object PageControl1: TPageControl
            Left = 382
            Top = 0
            Width = 669
            Height = 450
            ActivePage = TabSheet1
            Align = alClient
            TabOrder = 1
            object TabSheet1: TTabSheet
              Caption = 'Visualiza'#231#227'o do documento'
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1055
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1051
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1055
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 909
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 907
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrEntiDocs: TMySQLQuery
    Database = Dmod.MyDB
    BeforeScroll = QrEntiDocsBeforeScroll
    AfterScroll = QrEntiDocsAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM entiimgs'
      'WHERE Tipo="C"'
      'AND Chave=1017'
      'ORDER BY SEQUENCIA, Codigo')
    Left = 503
    Top = 375
    object QrEntiDocsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiDocsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiDocsCodEnti: TIntegerField
      FieldName = 'CodEnti'
      Required = True
    end
    object QrEntiDocsTipImg: TSmallintField
      FieldName = 'TipImg'
      Required = True
    end
    object QrEntiDocsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrEntiDocsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEntiDocsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntiDocsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrEntiDocsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrEntiDocsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEntiDocsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrEntiDocsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrEntiDocsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEntiDocsTIPO: TWideStringField
      FieldName = 'TIPO'
      Required = True
      Size = 1
    end
    object QrEntiDocsCHAVE: TIntegerField
      FieldName = 'CHAVE'
      Required = True
    end
    object QrEntiDocsSEQUENCIA: TIntegerField
      FieldName = 'SEQUENCIA'
      Required = True
    end
    object QrEntiDocsDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object QrEntiDocsTIPOARQUIVO: TWideStringField
      FieldName = 'TIPOARQUIVO'
    end
    object QrEntiDocsDATAATUALIZACAO: TDateTimeField
      FieldName = 'DATAATUALIZACAO'
      Required = True
    end
  end
  object DsEntiDocs: TDataSource
    DataSet = QrEntiDocs
    Left = 503
    Top = 427
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    Left = 484
    Top = 315
  end
end
