unit EntiTipCto;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit,
  dmkLabel, dmkRadioGroup, UnDmkProcFunc, dmkImage, UnDmkEnums, dmkCheckBox;

type
  TFmEntiTipCto = class(TForm)
    PainelDados: TPanel;
    DsEntiTipCto: TDataSource;
    QrEntiTipCto: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrEntiTipCtoCodigo: TIntegerField;
    QrEntiTipCtoCodUsu: TIntegerField;
    QrEntiTipCtoNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GroupBox2: TGroupBox;
    PainelEdit: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdCodUsu: TdmkEdit;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    RGTipo: TdmkRadioGroup;
    DBRGTipo: TDBRadioGroup;
    QrEntiTipCtoTipo: TSmallintField;
    CkAtivo: TdmkCheckBox;
    DBCkAtivo: TDBCheckBox;
    QrEntiTipCtoAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEntiTipCtoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEntiTipCtoBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //procedure MostraEntiTipCto;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEntiTipCto: TFmEntiTipCto;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntiTipCto.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEntiTipCto.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEntiTipCtoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEntiTipCto.DefParams;
begin
  VAR_GOTOTABELA := 'EntiTipCto';
  VAR_GOTOMYSQLTABLE := QrEntiTipCto;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome, Tipo, Ativo');
  VAR_SQLx.Add('FROM entitipcto');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmEntiTipCto.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'EntiTipCto', 'CodUsu', [], [], stIns,
      0, siPositivo, EdCodUsu);
end;

procedure TFmEntiTipCto.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      //GBCntrl.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      //GBCntrl.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text    := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text      := '';
        RGTipo.ItemIndex := 0;
        CkAtivo.Checked  := True;
        //...
      end else begin
        EdCodigo.Text    := DBEdCodigo.Text;
        EdNome.Text      := DBEdNome.Text;
        RGTipo.ItemIndex := DBRGTipo.ItemIndex;
        CkAtivo.Checked  := DBCkAtivo.Checked;
        //...
      end;
      EdNome.SetFocus;
    end;
    else
      Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEntiTipCto.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFMEntiTipCto.AlteraRegistro;
var
  EntiTipCto : Integer;
begin
  EntiTipCto := QrEntiTipCtoCodigo.Value;
  if QrEntiTipCtoCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(EntiTipCto, Dmod.MyDB, 'EntiTipCto', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(EntiTipCto, Dmod.MyDB, 'EntiTipCto', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFMEntiTipCto.IncluiRegistro;
var
  Cursor : TCursor;
  EntiTipCto : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    EntiTipCto := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'EntiTipCto', 'EntiTipCto', 'Codigo');
    if Length(FormatFloat(FFormatFloat, EntiTipCto))>Length(FFormatFloat) then
    begin
      Geral.MB_Aviso(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, EntiTipCto);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmEntiTipCto.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEntiTipCto.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEntiTipCto.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntiTipCto.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntiTipCto.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntiTipCto.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntiTipCto.BtAlteraClick(Sender: TObject);
begin
  if (QrEntiTipCto.State <> dsInactive) and (QrEntiTipCto.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrEntiTipCto, [PainelDados],
      [PainelEdita], EdCodUsu, ImgTipo, 'EntiTipCto');
  end;
end;

procedure TFmEntiTipCto.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEntiTipCtoCodigo.Value;
  Close;
end;

procedure TFmEntiTipCto.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('EntiTipCto', 'Codigo', ImgTipo.SQLType,
    QrEntiTipCtoCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FMEntiTipCto, PainelEdit,
    'EntiTipCto', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEntiTipCto.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'EntiTipCto', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'EntiTipCto', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'EntiTipCto', 'Codigo');
end;

procedure TFmEntiTipCto.BtExcluiClick(Sender: TObject);
begin
//
end;

procedure TFmEntiTipCto.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrEntiTipCto, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'EntiTipCto');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'EntiTipCto', 'CodUsu', [], [], stIns,
    0, siPositivo, EdCodUsu);
end;

procedure TFmEntiTipCto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmEntiTipCto.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEntiTipCtoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntiTipCto.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmEntiTipCto.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEntiTipCto.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEntiTipCtoCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEntiTipCto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntiTipCto.QrEntiTipCtoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEntiTipCto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiTipCto.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEntiTipCtoCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'EntiTipCto', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEntiTipCto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiTipCto.QrEntiTipCtoBeforeOpen(DataSet: TDataSet);
begin
  QrEntiTipCtoCodigo.DisplayFormat := FFormatFloat;
end;

{
procedure TFmEntiTipCto.MostraEntiTipCto;
begin
  if DBCheck.CriaFm(TFmEntiTipCto, FmEntiTipCto, afmoNegarComAviso) then
  begin
    FmEntiTipCto.ShowModal;
    FmEntiTipCto.Destroy;
  end;
end;
}

end.

