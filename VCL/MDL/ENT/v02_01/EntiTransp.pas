unit EntiTransp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Variants, dmkGeral,
  dmkImage, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkValUsu;

type
  TFmEntiTransp = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    CBTransp: TdmkDBLookupComboBox;
    DsTransportadoras: TDataSource;
    EdTransp: TdmkEditCB;
    EdOrdem: TdmkEdit;
    Label3: TLabel;
    QrTransportadoras: TmySQLQuery;
    QrTransportadorasNOMEENTIDADE: TWideStringField;
    QrTransportadorasCodigo: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenTransportadoras();
  public
    { Public declarations }
    FCodigo, FConta: Integer;
  end;

var
  FmEntiTransp: TFmEntiTransp;

implementation

uses UnMyObjects, Module, Entidades, DmkDAC_PF, UnDmkEnums, UnReordena;

{$R *.DFM}

procedure TFmEntiTransp.BtOKClick(Sender: TObject);
var
  Codigo, Ordem, Conta, Transp: Integer;
begin
  Codigo := FCodigo;
  Transp := EdTransp.ValueVariant;
  //
  if MyObjects.FIC(Codigo = 0, nil, 'C�digo n�o informado!') then Exit;
  if MyObjects.FIC(Transp = 0, EdTransp, 'Informe a transportadora!') then Exit;
  //
  Conta := UMyMod.BuscaEmLivreY_Def('entitransp', 'Conta', ImgTipo.SQLType, FConta);
  Ordem := EdOrdem.ValueVariant;
  //
  if MyObjects.FIC(Conta = 0, nil, 'ID n�o informado!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'entitransp', False,
    ['Transp', 'Ordem', 'Codigo'], ['Conta'], [Transp, Ordem, Codigo], [Conta], False) then
  begin
    if CkContinuar.Checked then
    begin
      Geral.MB_Aviso('Dados salvos com sucesso!');
      //
      ImgTipo.SQLType := stIns;
      EdTransp.ValueVariant := 0;
      CBTransp.KeyValue     := null;
      EdOrdem.ValueVariant  := UReordena.VerificaOrdem(Dmod.QrAux, Dmod.MyDB,
                                 'Codigo', 'Ordem', 'entitransp',
                                 Codigo, EdOrdem.ValueVariant, True);
      EdTransp.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmEntiTransp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiTransp.FormCreate(Sender: TObject);
begin
  ReopenTransportadoras();
  //
  ImgTipo.SQLType := stIns;
  //
  FCodigo := 0;
  FConta  := 0;
end;

procedure TFmEntiTransp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiTransp.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := True;
  end else
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
  end;
end;

procedure TFmEntiTransp.ReopenTransportadoras;
var
  SQL: String;
begin
  if VAR_FORNECET <> '' then
    SQL := 'WHERE Fornece' + VAR_FORNECET + '="V"'
  else
    SQL := '';
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrTransportadoras, Dmod.MyDB, [
    'SELECT CASE 1 WHEN Tipo=0 THEN RazaoSocial ',
    'ELSE Nome END NOMEENTIDADE, Codigo ',
    'FROM entidades ',
    SQL,
    'ORDER BY NOMEENTIDADE ',
    '']);
end;

procedure TFmEntiTransp.FormActivate(Sender: TObject);
var
  Zerar: Boolean;
begin
  MyObjects.CorIniComponente();
  //
  Zerar                := ImgTipo.SQLType = stIns;
  CBTransp.ListSource  := DsTransportadoras;
  EdOrdem.ValueVariant := UReordena.VerificaOrdem(Dmod.QrAux, Dmod.MyDB,
                            'Codigo', 'Ordem', 'entitransp', FCodigo,
                            EdOrdem.ValueVariant, Zerar);
end;

end.
