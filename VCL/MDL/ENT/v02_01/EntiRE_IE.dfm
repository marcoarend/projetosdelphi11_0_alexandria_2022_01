object FmEntiRE_IE: TFmEntiRE_IE
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-023 :: Consulta IE no SINTEGRA'
  ClientHeight = 906
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 390
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Consulta IE no SINTEGRA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 390
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Consulta IE no SINTEGRA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 390
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Consulta IE no SINTEGRA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 707
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 707
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1241
        Height = 707
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object ProgressBar1: TProgressBar
          Left = 2
          Top = 684
          Width = 1237
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          TabOrder = 0
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 15
          Width = 1237
          Height = 587
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 1
          object TabSheet1: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'HTML'
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 1229
              Height = 46
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              TabOrder = 0
              object RGNavegador: TRadioGroup
                Left = 1
                Top = 1
                Width = 173
                Height = 44
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Navegador: '
                Columns = 2
                Items.Strings = (
                  'IExplore'
                  'Chrome')
                TabOrder = 0
                OnClick = RGNavegadorClick
              end
              object Panel8: TPanel
                Left = 980
                Top = 1
                Width = 248
                Height = 44
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 1
                object SbNavega: TSpeedButton
                  Left = 4
                  Top = 10
                  Width = 26
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '>'
                  OnClick = SbNavegaClick
                end
                object LaRE: TLabel
                  Left = 33
                  Top = 15
                  Width = 17
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'UF:'
                end
                object SpeedButton1: TSpeedButton
                  Left = 169
                  Top = 10
                  Width = 50
                  Height = 21
                  Caption = 'Sintegra'
                  OnClick = SpeedButton1Click
                end
                object CBRE: TComboBox
                  Left = 54
                  Top = 10
                  Width = 50
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  AutoComplete = False
                  CharCase = ecUpperCase
                  Enabled = False
                  TabOrder = 0
                  OnChange = CBREChange
                  OnClick = CBREClick
                  Items.Strings = (
                    'AC'
                    'AL'
                    'AM'
                    'AP'
                    'BA'
                    'CE'
                    'DF'
                    'ES'
                    'GO'
                    'MA'
                    'MG'
                    'MS'
                    'MT'
                    'PA'
                    'PB'
                    'PE'
                    'PI'
                    'PR'
                    'RJ'
                    'RN'
                    'RO'
                    'RR'
                    'RS'
                    'SC'
                    'SE'
                    'SP'
                    'TO'
                    'SU')
                end
                object CkCCC: TCheckBox
                  Left = 112
                  Top = 12
                  Width = 49
                  Height = 17
                  Caption = 'CCC'
                  Enabled = False
                  TabOrder = 1
                end
              end
              object PnURL: TPanel
                Left = 174
                Top = 1
                Width = 806
                Height = 44
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 2
                OnResize = PnURLResize
                object EdURL_WB: TdmkEdit
                  Left = 1
                  Top = 10
                  Width = 800
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = True
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object Panel7: TPanel
              Left = 0
              Top = 46
              Width = 1229
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = 'Selecione o Navegador!'
              Font.Charset = ANSI_CHARSET
              Font.Color = clRed
              Font.Height = -19
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object WB_IE: TWebBrowser
              Left = 0
              Top = 71
              Width = 1229
              Height = 488
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              TabOrder = 2
              OnProgressChange = WB_IEProgressChange
              OnDownloadComplete = WB_IEDownloadComplete
              OnDocumentComplete = WB_IEDocumentComplete
              ExplicitLeft = 4
              ControlData = {
                4C000000057F0000703200000000000000000000000000000000000000000000
                000000004C000000000000000000000001000000E0D057007335CF11AE690800
                2B2E12620A000000000000004C0000000114020000000000C000000000000046
                8000000000000000000000000000000000000000000000000000000000000000
                00000000000000000100000000000000000000000000000000000000}
            end
          end
          object TabSheet2: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Dados separados'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel_Cadastro: TPanel
              Left = 0
              Top = 0
              Width = 1229
              Height = 559
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              TabOrder = 0
              object LaRazaoSocial: TLabel
                Left = 240
                Top = 70
                Width = 66
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'Raz'#227'o Social:'
              end
              object LaCNPJ: TLabel
                Left = 276
                Top = 11
                Width = 30
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'CNPJ:'
              end
              object LaLogradouro: TLabel
                Left = 249
                Top = 130
                Width = 57
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'Logradouro:'
              end
              object LaCEP: TLabel
                Left = 282
                Top = 276
                Width = 24
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'CEP:'
              end
              object LaNumero: TLabel
                Left = 293
                Top = 159
                Width = 15
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'N'#186':'
              end
              object LaComplemento: TLabel
                Left = 241
                Top = 189
                Width = 67
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'Complemento:'
              end
              object LaBairro: TLabel
                Left = 276
                Top = 218
                Width = 30
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'Bairro:'
              end
              object LaMunicipio: TLabel
                Left = 256
                Top = 248
                Width = 50
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'Munic'#237'pio:'
              end
              object LaUF: TLabel
                Left = 291
                Top = 306
                Width = 17
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'UF:'
              end
              object LaCNAE: TLabel
                Left = 274
                Top = 335
                Width = 32
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'CNAE:'
              end
              object LaIE: TLabel
                Left = 293
                Top = 41
                Width = 13
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'IE:'
              end
              object LaTelefone: TLabel
                Left = 263
                Top = 365
                Width = 45
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'Telefone:'
              end
              object LaEmail: TLabel
                Left = 280
                Top = 395
                Width = 28
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'Email:'
              end
              object LaFantasia: TLabel
                Left = 265
                Top = 99
                Width = 43
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'Fantasia:'
              end
              object LaSUFRAMA: TLabel
                Left = 253
                Top = 423
                Width = 55
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'SUFRAMA:'
              end
              object Label1: TLabel
                Left = 179
                Top = 451
                Width = 129
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Caption = 'Data in'#237'cio das atividades: '
              end
              object EdRazaoSocial: TEdit
                Left = 310
                Top = 66
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 2
              end
              object EdCNPJ: TEdit
                Left = 310
                Top = 7
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
              end
              object EdLogradouro: TEdit
                Left = 310
                Top = 126
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 4
              end
              object EdNumero: TEdit
                Left = 310
                Top = 156
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 5
              end
              object EdCompl: TEdit
                Left = 310
                Top = 185
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 6
              end
              object EdCEP: TEdit
                Left = 310
                Top = 272
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 9
              end
              object EdBairro: TEdit
                Left = 310
                Top = 215
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 7
              end
              object EdCidade: TEdit
                Left = 400
                Top = 244
                Width = 649
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 8
              end
              object EdUF: TEdit
                Left = 310
                Top = 302
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 10
              end
              object EdCNAE: TEdit
                Left = 310
                Top = 332
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 11
              end
              object EdIE: TEdit
                Left = 310
                Top = 37
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
              end
              object EdTelefone: TEdit
                Left = 310
                Top = 361
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 12
              end
              object EdEmail: TEdit
                Left = 310
                Top = 391
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 13
              end
              object EdFantasia: TEdit
                Left = 310
                Top = 96
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 3
              end
              object EdSUFRAMA: TEdit
                Left = 310
                Top = 420
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 14
              end
              object EdENatal: TEdit
                Left = 310
                Top = 448
                Width = 739
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 15
              end
              object EdCodMunici: TEdit
                Left = 310
                Top = 244
                Width = 87
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 16
              end
            end
          end
          object MS: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'MS'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Splitter1: TSplitter
              Left = 0
              Top = 176
              Width = 1229
              Height = 5
              Cursor = crVSplit
              Align = alTop
            end
            object GradeMS: TStringGrid
              Left = 0
              Top = 0
              Width = 1229
              Height = 176
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              ColCount = 3
              DefaultColWidth = 32
              RowCount = 20
              TabOrder = 0
              ExplicitHeight = 559
              ColWidths = (
                32
                177
                491)
            end
            object MeMS: TMemo
              Left = 0
              Top = 181
              Width = 1229
              Height = 378
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              TabOrder = 1
              ExplicitTop = 176
              ExplicitHeight = 383
            end
          end
          object TabSheet3: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeCodigo: TMemo
              Left = 0
              Top = 0
              Width = 1229
              Height = 559
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Lines.Strings = (
                '')
              ScrollBars = ssVertical
              TabOrder = 0
            end
          end
          object TabSheet4: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Texto Plano'
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeDados: TMemo
              Left = 0
              Top = 0
              Width = 1229
              Height = 559
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ScrollBars = ssVertical
              TabOrder = 0
            end
          end
          object TabSheet5: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo Chrome'
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeChrome: TMemo
              Left = 0
              Top = 0
              Width = 1229
              Height = 559
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              TabOrder = 0
            end
          end
          object TabSheet7: TTabSheet
            Caption = ' Texto do site pesquisado'
            ImageIndex = 7
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeTxtSite: TMemo
              Left = 0
              Top = 0
              Width = 1229
              Height = 559
              Align = alClient
              TabOrder = 0
              OnChange = MeTxtSiteChange
            end
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 602
          Width = 1237
          Height = 82
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          TabOrder = 2
          object EdURL_A: TEdit
            Left = -211
            Top = 15
            Width = 1222
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ReadOnly = True
            TabOrder = 0
          end
          object EdURL_B: TEdit
            Left = 5
            Top = 44
            Width = 1222
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ReadOnly = True
            TabOrder = 1
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 766
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1237
      Height = 37
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 820
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1062
      Top = 15
      Width = 177
      Height = 69
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1060
      Height = 69
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtLeDados: TBitBtn
        Tag = 39
        Left = 341
        Top = 12
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'L'#234' Dados'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtLeDadosClick
      end
      object RGOpcoes: TRadioGroup
        Left = 4
        Top = 0
        Width = 329
        Height = 55
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es'
        ItemIndex = 1
        Items.Strings = (
          'Importar apenas o CNPJ, Raz'#227'o Social, CNAE principal e CEP'
          'Importar tudo')
        TabOrder = 1
      end
      object Button5: TButton
        Left = 593
        Top = 20
        Width = 80
        Height = 30
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Button5'
        TabOrder = 2
        Visible = False
        OnClick = Button5Click
      end
      object BtImporta: TBitBtn
        Tag = 10
        Left = 465
        Top = 12
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Importa'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtImportaClick
      end
    end
  end
end
