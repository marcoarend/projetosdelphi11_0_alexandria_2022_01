unit EntiContat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, ComCtrls, dmkEditDateTimePicker, dmkRadioGroup,
  UnDmkEnums, dmkCheckGroup, dmkCheckBox;

type
  TFmEntiContat = class(TForm)
    Panel1: TPanel;
    QrCargos: TmySQLQuery;
    DsCargos: TDataSource;
    CkContinuar: TCheckBox;
    QrCargosCodigo: TIntegerField;
    QrCargosCodUsu: TIntegerField;
    QrCargosNome: TWideStringField;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CBCargo: TdmkDBLookupComboBox;
    EdCargo: TdmkEditCB;
    Label1: TLabel;
    dmkValUsu1: TdmkValUsu;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    TPDtaNatal: TdmkEditDateTimePicker;
    Label24: TLabel;
    RGSexo: TdmkRadioGroup;
    QrEntidade: TmySQLQuery;
    QrEntidadeNO_ENT: TWideStringField;
    QrEntidadeCodigo: TIntegerField;
    DsEntidade: TDataSource;
    Panel3: TPanel;
    RGTipo: TdmkRadioGroup;
    PnCPF: TPanel;
    PnCNPJ: TPanel;
    EdCNPJ: TdmkEdit;
    Label11: TLabel;
    EdCPF: TdmkEdit;
    Label8: TLabel;
    CGAplicacao: TdmkCheckGroup;
    Label2: TLabel;
    QrEntidadeCNPJ: TWideStringField;
    QrEntidadeCPF: TWideStringField;
    CkAtivo: TdmkCheckBox;
    QrEntidades: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label5: TLabel;
    QrEntidadesCPF: TWideStringField;
    QrEntidadesPNatal: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RGSexoClick(Sender: TObject);
    procedure RGSexoExit(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure EdCNPJKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdCPFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdEntidadeChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEntiContat(Controle: Integer);
    function  ValidaAutorizacaoXMLNFe(Aplicacao, Pessoa: Integer; CNPJ,
              CPF: String): Boolean;
  public
    { Public declarations }
    FEntidadeOri, FEntidadeAtu: Integer;
    //FQrEntidades,
    FQrEntiContat: TmySQLQuery;
    //FDsEntidades: TDataSource;
  end;

  var
  FmEntiContat: TFmEntiContat;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, Principal, MyDBCheck,
  ModuleGeral, dmkDAC_PF, UnEntities;

{$R *.DFM}

procedure TFmEntiContat.BtOKClick(Sender: TObject);
var
  Nome, DtaNatal, CNPJ, CPF: String;
  Codigo, Controle, Entidade, Cargo, Sexo, Tipo, Aplicacao, Ativo: Integer;
begin
  if Trim(EdNome.Text) = '' then
  begin
    Geral.MB_Aviso('Informe o nome do contato!');
    EdNome.SetFocus;
    Exit;
  end;
  //
{
  if UMyMod.ExecSQLInsUpdFm(FmEntiContat, ImgTipo.SQLType, 'EntiContat',
  Controle, Dmod.QrUpd) then
}
  Codigo         := QrEntidadeCodigo.Value;
  Controle       := 0;
  Entidade       := EdEntidade.ValueVariant;
  Nome           := EdNome.Text;
  UMyMod.ObtemCodigoDeCodUsu(EdCargo, Cargo, '');
  DtaNatal       := Geral.FDT(TPDtaNatal.DateTime, 1);
  Sexo           := RGSexo.ItemIndex;
  Tipo           := RGTipo.ItemIndex;
  Aplicacao      := CGAplicacao.Value;
  Ativo          := Geral.BoolToInt(CkAtivo.Checked);
  //
  if Aplicacao <> 0 then
  begin
    if Tipo = 0 then
    begin
      CNPJ           := Geral.SoNumero_TT(EdCNPJ.Text);
      CPF            := '';
      if MyObjects.FIC(CNPJ = '', EdCNPJ, 'Informe o CNPJ!') then Exit;
      if MyObjects.FIC(Length(CNPJ) <> 14, EdCNPJ, 'O CNPJ deve conter 14 algarismos!') then Exit;
    end else
    if Tipo = 1 then
    begin
      CNPJ           := '';
      CPF            := Geral.SoNumero_TT(EdCPF.Text);
      if MyObjects.FIC(CPF = '', EdCPF, 'Informe o CPF!') then Exit;
      if MyObjects.FIC(Length(CPF) <> 11, EdCPF, 'O CPF deve conter 11 algarismos!') then Exit;
    end else
      if MyObjects.FIC(Tipo < 0, RGTipo, 'Informe se o contato � do tipo Jur�dica ou F�sica!') then Exit;
  end;
  if not ValidaAutorizacaoXMLNFe(CGAplicacao.Value, RGTipo.ItemIndex,
    EdCNPJ.ValueVariant, EdCPF.ValueVariant) then
  begin
    Geral.MB_Aviso('Voc� deve cadastrar um CNPJ/CPF para autoriza��o para obter XML de NFe diferente do CNPJ/CPF da entidade!');
    Exit;
  end;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('enticontat', 'Controle',
    ImgTipo.SQLType, EdControle.ValueVariant);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'enticontat', False, [
  'Codigo', 'Entidade', 'Nome', 'Cargo',
  (*'DiarioAdd',*) 'DtaNatal', 'Sexo',
  'Tipo', 'CNPJ', 'CPF', 'Aplicacao', 'Ativo'], [
  'Controle'], [
  Codigo, Entidade, Nome, Cargo,
  (*DiarioAdd,*) DtaNatal, Sexo,
  Tipo, CNPJ, CPF, Aplicacao, Ativo], [
  Controle], True) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando entidades de contatos!');
    DModG.AtualizaEntiConEnt();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    ReopenEntiContat(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdEntidade.ValueVariant  := 0;
      CBEntidade.KeyValue      := Null;
      EdCargo.ValueVariant     := 0;
      CBCargo.KeyValue         := Null;
      EdNome.ValueVariant      := '';
      EdEntidade.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmEntiContat.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiContat.EdCNPJKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdCNPJ.ValueVariant := QrEntidadeCNPJ.Value;
  end;
end;

procedure TFmEntiContat.EdCPFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdCPF.ValueVariant := QrEntidadeCPF.Value;
  end;
end;

procedure TFmEntiContat.EdEntidadeChange(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := EdEntidade.ValueVariant;
  //
  if Entidade <> 0 then
  begin
    EdNome.ValueVariant := Copy(QrEntidadesNome.Value, 1, 27);
    TPDtaNatal.Date     := QrEntidadesPNatal.Value;
    RGTipo.ItemIndex    := 1;
    EdCPF.ValueVariant  := QrEntidadesCPF.Value;
    //
    if Length(QrEntidadesNome.Value) > 27 then
      EdNome.ValueVariant := EdNome.ValueVariant + '...';
  end;
end;

procedure TFmEntiContat.FormActivate(Sender: TObject);
var
  Codigo: Integer;
begin
  if ImgTipo.SQLType = stIns then
    TPDtaNatal.Date := 0;
  //
{
  CBCodigo.ListSource := FDsEntidades;
  if FQrEntidades <> nil then
  begin
    if FEntidadeOri = FEntidadeAtu then
    begin
    Codigo := FQrEntidades.FieldByName('Codigo').AsInteger;
    EdCodigo.ValueVariant := Codigo;
    CBCodigo.KeyValue := Codigo;
  end else
    EdCodigo.ValueVariant := 0;
}
  if FEntidadeOri <> 0 then
    Codigo := FEntidadeOri
  else
    Codigo := FEntidadeAtu;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntidade, Dmod.MyDB, [
    'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT, CNPJ, CPF ',
    'FROM entidades ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
  //
  MyObjects.CorIniComponente();
end;

procedure TFmEntiContat.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrCargos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmEntiContat.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiContat.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    TPDtaNatal.Date     := 0;
    CkContinuar.Checked := False;
    CkContinuar.Visible := True;
  end else
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
  end;
end;

procedure TFmEntiContat.ReopenEntiContat(Controle: Integer);
begin
  if FQrEntiContat <> nil then
  begin
    FQrEntiContat.Close;
    if FQrEntiContat.ParamCount > 0 then
      FQrEntiContat.Params[0].AsInteger := QrEntidadeCodigo.Value;
    UnDmkDAC_PF.AbreQuery(FQrEntiContat, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrEntiContat.Locate('Controle', Controle, []);
  end;
end;

procedure TFmEntiContat.RGSexoClick(Sender: TObject);
var
  Msg: String;
begin
  case RGSexo.ItemIndex of
      0: Msg := 'NI = N�o informado';
      1: Msg := 'M = Masculino';
      2: Msg := 'F = Feminino';
    else Msg := '...';
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Msg);
end;

procedure TFmEntiContat.RGSexoExit(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmEntiContat.RGTipoClick(Sender: TObject);
begin
  PnCNPJ.Visible := RGTipo.ItemIndex = 0;
  PnCPF.Visible  := RGTipo.ItemIndex = 1;
end;

procedure TFmEntiContat.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  Entities.MostraFormEntiCargos(dmkValUsu1.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdCargo, CBCargo, QrCargos, VAR_CADASTRO);
    //
    EdCargo.SetFocus;
  end;
end;

function TFmEntiContat.ValidaAutorizacaoXMLNFe(Aplicacao, Pessoa: Integer; CNPJ,
  CPF: String): Boolean;
begin
  if Geral.IntInConjunto(1, Aplicacao) then
  begin
    if Pessoa = 0 then //Jur�dica
    begin
      if Geral.SoNumero_TT(CNPJ) = QrEntidadeCNPJ.Value then
        Result := False
      else
        Result := True;
    end else //F�sica
    begin
      if Geral.SoNumero_TT(CPF) = QrEntidadeCPF.Value then
        Result := False
      else
        Result := True;
    end;
  end else
    Result := True;
end;

end.
