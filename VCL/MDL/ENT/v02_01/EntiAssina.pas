{// ini Delphi 28 Alexandria
//{$I dmk.inc}
// ini Delphi 28 Alexandria

unit EntiAssina;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, ComCtrls,
  UnInternalConsts, dmkDBGrid, DBCGrids, DmkDAC_PF, UnDmkProcFunc, UnDmkEnums,
  dmkImage, Vcl.Menus, Variants, frxClass, frxPreview, frxDBSet;

type
  TFmEntiAssina = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PnMenu: TPanel;
    LaTitulo1C: TLabel;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    QrEntidades: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    PainelData: TPanel;
    Painel1: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    EdPesquisa: TdmkEdit;
    PainelEdita: TPanel;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    BtDesiste: TBitBtn;
    Panel7: TPanel;
    BtConfirma: TBitBtn;
    GroupBox3: TGroupBox;
    PainelEdit: TPanel;
    Label7: TLabel;
    Label1: TLabel;
    Label10: TLabel;
    Label2: TLabel;
    Label20: TLabel;
    SbRespTec1: TSpeedButton;
    EdCodigo: TdmkEdit;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    EdNome: TdmkEdit;
    EdDados: TdmkEdit;
    EdImagem: TdmkEdit;
    QrEntiAssina: TmySQLQuery;
    DsEntiAssina: TDataSource;
    QrEntiAssinaCodigo: TIntegerField;
    QrEntiAssinaNome: TWideStringField;
    QrEntiAssinaDados: TWideStringField;
    QrEntiAssinaImagem: TWideStringField;
    QrEntiAssinaEntidade: TIntegerField;
    QrEntiAssinaEntNome: TWideStringField;
    QrEntiAssinaEntAtivo: TSmallintField;
    frxPreview1: TfrxPreview;
    frxENT_GEREN_030_001: TfrxReport;
    frxDsEntiAssina: TfrxDBDataset;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPesquisaEnter(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure SbRespTec1Click(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure frxENT_GEREN_030_001GetValue(const VarName: string;
      var Value: Variant);
    procedure QrEntiAssinaAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure MostraFrx();
    procedure MostraEdicao(SQLType: TSQLType);
    procedure ReopenEntiAssina(Codigo: Integer);
  public
    { Public declarations }
    FEntidade: Integer;
  end;

  var
    FmEntiAssina: TFmEntiAssina;
  const
    FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UMySQLModule, MyDBCheck, ModuleGeral, UnGOTOy,
  UMySQLDB;

{$R *.DFM}

procedure TFmEntiAssina.MostraFrx();
begin
  MyObjects.frxDefineDataSets(frxENT_GEREN_030_001, [
    frxDsEntiAssina
    ]);
  with frxENT_GEREN_030_001 do
  begin
    Preview := frxPreview1;
    PreviewOptions.ZoomMode := zmPageWidth;
    ShowReport;
  end;
end;

procedure TFmEntiAssina.QrEntiAssinaAfterScroll(DataSet: TDataSet);
begin
  MostraFrx();
end;

procedure TFmEntiAssina.BtConfirmaClick(Sender: TObject);
var
  Codigo, Entidade: Integer;
  Nome, Dados, Imagem: String;
begin
  Entidade := EdEntidade.ValueVariant;
  Nome     := EdNome.ValueVariant;
  Dados    := EdDados.ValueVariant;
  Imagem   := EdImagem.ValueVariant;
  //
  if MyObjects.FIC(Entidade = 0, EdEntidade, 'Defina a entidade!') then Exit;
  if MyObjects.FIC(Nome = '', EdNome, 'Informe o nome do signat�rio!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('entiassina', 'Codigo', ImgTipo.SQLType,
              EdCodigo.ValueVariant);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'entiassina', False,
    ['Entidade', 'Nome', 'Dados', 'Imagem'], ['Codigo'],
    [Entidade, Nome, Dados, Imagem], [Codigo], True) then
  begin
    ReopenEntiAssina(Codigo);
    MostraEdicao(stLok);
  end;
end;

procedure TFmEntiAssina.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(stLok);
end;

procedure TFmEntiAssina.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrEntiAssina.State <> dsInactive) and (QrEntiAssina.RecordCount > 0) then
  begin
    Codigo := QrEntiAssinaCodigo.Value;
    //
    if USQLDB.ExcluiRegistroInt1('Confirma a exclus�o da assinatura da entidade '
      + 'ID n�mero: ' + Geral.FF0(Codigo) + '?', 'entiassina', 'Codigo', Codigo,
      Dmod.MyDB) = ID_YES then
    begin
      UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
      ReopenEntiAssina(0);
    end;
  end;
end;

procedure TFmEntiAssina.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(stIns);
end;

procedure TFmEntiAssina.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiAssina.EdEntidadeChange(Sender: TObject);
begin
  EdNome.ValueVariant := CBEntidade.Text;
end;

procedure TFmEntiAssina.EdPesquisaEnter(Sender: TObject);
begin
  EdPesquisa.ValueVariant := '';
end;

procedure TFmEntiAssina.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiAssina.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEntidade.ListSource := DsEntidades;
  dmkDBGrid1.DataSource := DsEntiAssina;
  //
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmEntiAssina.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiAssina.FormShow(Sender: TObject);
begin
  ReopenEntiAssina(0);
end;

procedure TFmEntiAssina.frxENT_GEREN_030_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'LogoAssinaExiste' then
    Value := FileExists(QrEntiAssinaImagem.Value)
  else if VarName = 'LogoAssinaPath' then
    Value := QrEntiAssinaImagem.Value;
end;

procedure TFmEntiAssina.MostraEdicao(SQLType: TSQLType);
begin
  if SQLType = stLok then
  begin
    PainelData.Enabled  := True;
    PainelEdita.Visible := False;
    GBRodaPe.Visible    := True;
  end else
  begin
    PainelData.Enabled  := False;
    PainelEdita.Visible := True;
    GBRodaPe.Visible    := False;
    //
    if SQLType = stIns then
    begin
      EdCodigo.Text           := FormatFloat(FFormatFloat, 0);
      EdEntidade.ValueVariant := FEntidade;
      CBEntidade.KeyValue     := FEntidade;
      EdDados.ValueVariant    := '';
      EdImagem.ValueVariant   := '';
      //
      if CBEntidade.Text <> '' then
        EdNome.ValueVariant := CBEntidade.Text
      else
        EdNome.ValueVariant := '';
    end else
    begin
      EdCodigo.ValueVariant   := FormatFloat(FFormatFloat, QrEntiAssinaCodigo.Value);
      EdEntidade.ValueVariant := QrEntiAssinaEntidade.Value;
      CBEntidade.KeyValue     := QrEntiAssinaEntidade.Value;
      EdNome.ValueVariant     := QrEntiAssinaNome.Value;
      EdDados.ValueVariant    := QrEntiAssinaDados.Value;
      EdImagem.ValueVariant   := QrEntiAssinaImagem.Value;
    end;
    EdEntidade.SetFocus;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEntiAssina.ReopenEntiAssina(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreQuery(QrEntiAssina, Dmod.MyDB);
  //
  if Codigo <> 0 then
    QrEntiAssina.Locate('Codigo', Codigo, [])
  else
  if FEntidade <> 0 then
    QrEntiAssina.Locate('Entidade', FEntidade, []);
end;

procedure TFmEntiAssina.SbRespTec1Click(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdImagem);
end;

end.
