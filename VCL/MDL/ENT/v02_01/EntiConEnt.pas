unit EntiConEnt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, dmkEdit, dmkEditCB,
  Variants, dmkDBLookupComboBox, UnDMkEnums;

type
  TFmEntiConEnt = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_ENT: TWideStringField;
    CBEntidade: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    Label1: TLabel;
    Label2: TLabel;
    EdContato: TdmkEditCB;
    CBContato: TdmkDBLookupComboBox;
    QrEntiContat: TmySQLQuery;
    DsEntiContat: TDataSource;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    BtContatos: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtContatosClick(Sender: TObject);
  private
    { Private declarations }
    FCodigo: Integer;
  public
    { Public declarations }
    FQrEntiConEnt: TmySQLQuery;
    FDatabase: TmySQLDatabase;
    procedure ReopenEntidades(EntidadeExclusa: Integer);
    procedure ReopenEntiContat(Entidade: Integer);
  end;

  var
  FmEntiConEnt: TFmEntiConEnt;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral;

{$R *.DFM}

procedure TFmEntiConEnt.BtContatosClick(Sender: TObject);
begin
  EdEntidade.ValueVariant := 0;
  CBEntidade.KeyValue     := Null;
  ReopenEntiContat(0);
end;

procedure TFmEntiConEnt.BtOKClick(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  Controle := EdContato.ValueVariant;
  if MyObjects.FIC(Controle=0, EdContato, 'Informe o contato!') then
    Exit;
  if EdContato.Enabled = False then
    Codigo := EdEntidade.ValueVariant
  else
    Codigo := FCodigo;
  //Duplicate entry '1505-3328' for key 1.
  if DModG.ContatoJaAtrelado(Codigo, Controle, True) then
    Exit;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'enticonent', False, [], [
  'Codigo', 'Controle'], [], [
  Codigo, Controle], True) then
  begin
    if FQrEntiConEnt <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrEntiConEnt, FDatabase);
      FQrEntiConEnt.Locate('Codigo;Controle', VarArrayOf([Codigo,Controle]), []);
    end
  end;
end;

procedure TFmEntiConEnt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiConEnt.EdEntidadeChange(Sender: TObject);
var
  Entidade: Integer;
begin
  QrEntiContat.Close;
  Entidade := EdEntidade.ValueVariant;
  if Entidade <> 0 then
    ReopenEntiContat(Entidade);
end;

procedure TFmEntiConEnt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiConEnt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmEntiConEnt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiConEnt.ReopenEntiContat(Entidade: Integer);
var
  SQL_WHERE: String;
begin
  if Entidade <> 0 then
    SQL_WHERE := 'WHERE eco.Codigo=' + Geral.FF0(Entidade)
  else
    SQL_WHERE := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiContat, Dmod.MyDB, [
  'SELECT DISTINCT eco.Controle, eco.Nome ',
  'FROM enticontat eco ',
  'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
  SQL_WHERE,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmEntiConEnt.ReopenEntidades(EntidadeExclusa: Integer);
begin
  FCodigo := EntidadeExclusa;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB, [
  'SELECT Codigo, ',
  'IF(Tipo=0, RazaoSocial, Nome) NO_ENT ',
  'FROM entidades ',
  'WHERE Codigo NOT IN (0, ' + Geral.FF0(EntidadeExclusa) + ') ',
  'ORDER BY NO_ENT ',
  '']);
end;

end.
