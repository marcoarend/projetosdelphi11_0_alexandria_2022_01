unit UnCEP;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, dmkEdit, dmkDBLookupComboBox, dmkGeral,
  //  CEP - descriptografia
  DCPsha1, DCPTwoFish,
  //
  OmniXML, OmniXMLUtils,
  //
  dmkEditCB, mySQLDBTables, UnDmkEnums, UnGrl_DmkREST, UnServices;

type
  TUnCEP = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ObtemCodigoDTBMunici(Cidade, UF: String): Integer;
    procedure ConsultaCEP(EdCEP, EdLogradTxt, EdRuaTxt, EdNumero,
              EdBairroTxt, EdCidadeTxt, EdUF, EdPais, EdNumeTxt, EdCompl:
              TdmkEdit; CBLograd: TdmkDBLookupComboBox;
              EdCodMunici: TdmkEdit; CBCodMunici: TdmkDBLookupComboBox;
              EdCodiPais: TdmkEdit; CBCodiPais: TdmkDBLookupComboBox;
              CBCodiUF: TComboBox = nil);
    procedure ConsultaCEP2(EdCEP, EdLogradTxt, EdRuaTxt, EdNumero,
              EdBairroTxt, EdCidadeTxt, EdUF, EdPais, EdNumeTxt, EdCompl:
              TdmkEdit; CBLograd, CBCodMunici, CBCodiPais: TdmkDBLookupComboBox;
              EdCodMunici, EdCodiPais: TdmkEditCB);
    procedure ConsultaCEP3(EdCEP, EdLogradTxt, EdRuaTxt, EdNumero,
              EdBairroTxt, EdCidadeTxt, EdUF, EdPais, EdNumeTxt, EdCompl:
              TdmkEdit; CBLograd: TdmkDBLookupComboBox;
              EdCodMunici: TdmkEdit; CBCodMunici: TdmkDBLookupComboBox;
              EdCodiPais: TdmkEdit; CBCodiPais: TdmkDBLookupComboBox;
              CBCodiUF: TComboBox = nil);
    procedure ConsultaCEP4(EdCEP, EdLogradTxt, EdRuaTxt, EdNumero,
              EdBairroTxt, EdCidadeTxt, EdUF, EdPais, EdNumeTxt, EdCompl:
              TdmkEdit; CBLograd: TdmkDBLookupComboBox;
              EdCodMunici: TdmkEdit; CBCodMunici: TdmkDBLookupComboBox;
              EdCodiPais: TdmkEdit; CBCodiPais: TdmkDBLookupComboBox;
              CBCodiUF: TComboBox = nil);
{$ifNDef sACBr}
    procedure ConsultaCEP5(EdCEP, EdLogradTxt, EdRuaTxt, EdNumero,
              EdBairroTxt, EdCidadeTxt, EdUF, EdPais, EdNumeTxt, EdCompl:
              TdmkEdit; CBLograd: TdmkDBLookupComboBox;
              EdCodMunici: TdmkEdit; CBCodMunici: TdmkDBLookupComboBox;
              EdCodiPais: TdmkEdit; CBCodiPais: TdmkDBLookupComboBox;
              CBCodiUF: TComboBox = nil);
{$EndIf}
    procedure DescobrirCEP(EdCEP, EdLograd, EdRua, EdBairro, EdCidade, EdUF,
              EdPais, EdCodMunici, EdCodiPais: TdmkEdit;
              CBLograd, CBCodMunici, CBCodiPais: TdmkDBLookupComboBox);
    function  DeCryptCEP(Crypt: string; twofish: TDCP_twofish): string;
    function  EnCryptCEP(CEP: string; twofish: TDCP_twofish): string;
  end;

var
  U_CEP: TUnCEP;

implementation

uses UnMyObjects, Module, MyDBCheck,
//2023-12-11 {$ifNDef sACBr} UnDmkACBr_PF, {$Endif}
{$IfDef cDbTable}
EntiCEP,
{$Else}
ConsultaCEP,
{$ENdIf}
EntiCEP2, CEP_Result, DmkDAC_PF, UnDmkProcFunc,
  ModuleGeral, UMySQLDB;

procedure TUnCEP.ConsultaCEP(EdCEP, EdLogradTxt, EdRuaTxt, EdNumero,
              EdBairroTxt, EdCidadeTxt, EdUF, EdPais, EdNumeTxt, EdCompl:
              TdmkEdit; CBLograd: TdmkDBLookupComboBox;
              EdCodMunici: TdmkEdit; CBCodMunici: TdmkDBLookupComboBox;
              EdCodiPais: TdmkEdit; CBCodiPais: TdmkDBLookupComboBox;
              CBCodiUF: TComboBox = nil);
{$IfDef cDbTable}
var
  FConectou: Boolean;
  CEP: String;
  CodMunici, LocalCod: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  FConectou := False;
  //
  CEP := EdCEP.Text;
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    with FmEntiCEP do
    begin
      FConectou := DNCEP.Connected;
      if FConectou then
      begin
        DBGrid1.Visible    := False;
        Painel1.Visible    := False;
        EdCEPLoc.Text      := CEP;
        BtLocaliza2Click(FmEntiCEP);
        DefineDadosPesquisaCEP();
        if EdCEP       <> nil then EdCEP.Text         := FCEP;
        if EdLogradTxt <> nil then EdLogradTxt.Text   := FormatFloat('0', FLogrTip);
        if CBLograd    <> nil then CBLograd.KeyValue  := FLogrTip;
        //FLogrCod  := QrGRULOG_NU_SEQUENCIAL.Value;
        //
        if (EdLogradTxt = nil) and (CBLograd = nil) and (EdRuaTxt <> nil) then
          EdRuaTxt.Text := FLogrTipTxt + ' ' + Geral.Maiusculas(FLogrTxt, True)
        else if EdRuaTxt <> nil then
          EdRuaTxt.Text := Geral.Maiusculas(FLogrTxt, True);
        //
        //FBairICod := QrGRUBAI_NU_SEQUENCIAL.Value;
        if EdCidadeTxt <> nil then EdCidadeTxt.Text   := Geral.Maiusculas(FLocalTxt, True);
        LocalCod := FLocalCod;
        if EdPais      <> nil then EdPais.Text        := 'BRASIL';
        if EdUF        <> nil then EdUF.Text          := FUF_Txt;
        if CBCodiUF    <> nil then CBCodiUF.ItemIndex := (FUF_Cod + 1) * -1;
        if EdBairroTxt <> nil then
        begin
          if (Trim(FBairroI) <> '') and (Trim(FBairroF) <> '') then
          begin
            Application.CreateForm(TFmCEP_Result, FmCEP_Result);
            FmCEP_Result.RGBairro.Items.Clear;
            FmCEP_Result.RGBairro.Items.Add('*** NENHUM ***');
            FmCEP_Result.RGBairro.Items.Add(FBairroI);
            FmCEP_Result.RGBairro.Items.Add(FBairroF);
            FmCEP_Result.ShowModal;
            case FmCEP_Result.RGBairro.ItemIndex of
              1: EdBairroTxt.Text := Geral.Maiusculas(FBairroI, True);
              2: EdBairroTxt.Text := Geral.Maiusculas(FBairroF, True);
            end;
            FmCEP_Result.Destroy;
          end else
            EdBairroTxt.Text   := Geral.Maiusculas(FBairroI, True);
          //FBairroF  := Geral.Maiusculas(QrGRUBAI_NO.Value, True);
        end;
        if EdNumeTxt <> nil then
        begin
          if Trim(FNumeTxt) <> '' then
          begin
            EdNumeTxt.Text := Trim(FNumeTxt);
            try
              EdCompl.SetFocus;
            except
              // nada
            end;
          end else begin
            try
              EdNumero.SetFocus;
            except
              // nada
            end;
          end;
        end;
        // 2012-05-28
        CodMunici := 0;
        if (EdCodMunici <> nil) then
        begin
          if LocalCod > 0 then
          begin
            if USQLDB.TabelaExiste('dtb_munici', (*Dmod.QrMas,*) DModG.AllID_DB) then
            begin
              DModG.QrAllAux.SQL.Clear;
              DModG.QrAllAux.SQL.Add('SELECT Codigo FROM dtb_munici');
              DModG.QrAllAux.SQL.Add('WHERE DNE=' + Geral.FF0(LocalCod));
              UnDmkDAC_PF.AbreQueryApenas(DModG.QrAllAux);
              CodMunici := DModG.QrAllAux.FieldByName('Codigo').AsInteger;
            end;
          end;
          if CodMunici = 0 then
            CodMunici := ObtemCodigoDTBMunici(FLocalTxt, FUF_Txt);
          if CodMunici > 0 then
          begin
            if EdCodMunici <> nil then
              EdCodMunici.ValueVariant := CodMunici;
            if CBCodMunici <> nil then
              CBCodMunici.KeyValue := CodMunici;
            if EdCodiPais <> nil then
              EdCodiPais.ValueVariant := 1058;
            if CBCodiPais <> nil then
              CBCodiPais.KeyValue := 1058;
          end else
          begin
            if EdCodMunici.ValueVariant > 0 then
            begin
              Geral.MB_Aviso('N�o foi poss�vel definir o c�digo da cidade!' +
              sLineBreak + 'Verifique manualmente se ele est� correto!');
            end;
          end;
        end;




      end else
        Screen.Cursor := crDefault;
      // fim 2012-05-28
      Destroy;
    end;
  end;
  //
  if FConectou then
    Screen.Cursor := crHourGlass;
{$Else}
  //    TODO Berlin
  //Geral.MB_Info('Solicite implementa��o � Dermatek! (DBE Table)');
var
  ws: AtendeCliente;
  Endereco: TEndereco;
  //
  //FConectou: Boolean;
  CEP: String;
  CodMunici, UFCod: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    ws := GetAtendeCliente();
    try
      CEP := Geral.SoNumero_TT(EdCEP.Text);
      Endereco := ws.consultaCEP(CEP);
      if Endereco <> nil then
      begin
        //Memo1.Lines.Add(Endereco.cep);
        // n�o. usu�rio informou!
        //Memo1.Lines.Add(Endereco.Logradouro);
        //EdLogradTxt,
        if EdRuaTxt <> nil then
          EdRuaTxt.ValueVariant := Endereco.Logradouro;
        //EdNumero,
        //Memo1.Lines.Add(Endereco.bairro);
        if EdBairroTxt <> nil then
          EdBairroTxt.ValueVariant := Endereco.bairro;
        //Memo1.Lines.Add(Endereco.cidade);
        if EdCidadeTxt <> nil then
          EdCidadeTxt.ValueVariant := Endereco.cidade;
        // Memo1.Lines.Add(Endereco.complemento);
        //Memo1.Lines.Add(Endereco.complemento2);
        if EdCompl <> nil then
        begin
{         de ... at� num tal ????
          EdCompl.ValueVariant := Endereco.complemento;
          if (Endereco.complemento <> EmptyStr)
          and (Endereco.complemento2 <> EmptyStr) then
          EdCompl.ValueVariant := EdCompl.ValueVariant + ' ';
          EdCompl.ValueVariant := Endereco.complemento2;
}
        end;
        //Memo1.Lines.Add(Endereco.uf);
        if EdUF <> nil then
          EdUF.ValueVariant := Endereco.uf;
        if EdPais <> nil then
          EdPais.Text := 'BRASIL';

        //  codigos munice e pais
        CodMunici := 0;
        if (EdCodMunici <> nil) and (Endereco.uf <> EmptyStr)
        and (Endereco.cidade <> EmptyStr) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
          'SELECT DTB ',
          'FROM ufs ',
          'WHERE Nome = "' + Uppercase(Endereco.uf) + '"  ',
          EmptyStr]);
          UFCod := Dmod.QrAux.FieldByName('DTB').AsInteger;
          if UFCod > 0 then
          begin
            if USQLDB.TabelaExiste('dtb_munici', (*Dmod.QrMas,*) DModG.AllID_DB) then
            begin
              UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAllAux, DModG.AllID_DB, [
              'SELECT Codigo',
              'FROM dtb_munici ',
              'WHERE Nome = "' + Endereco.cidade + '"',
              'AND Substr(Codigo, 1, 2) = "' + Geral.FF0(UFCod) + '"',
              EmptyStr]);
              CodMunici := DModG.QrAllAux.FieldByName('Codigo').AsInteger;
            end;
          end;
          //if CodMunici = 0 then
            //CodMunici := ObtemCodigoDTBMunici(FLocalTxt, FUF_Txt);
          if CodMunici > 0 then
          begin
            if EdCodMunici <> nil then
              EdCodMunici.ValueVariant := CodMunici;
            if CBCodMunici <> nil then
              CBCodMunici.KeyValue := CodMunici;
            if EdCodiPais <> nil then
              EdCodiPais.ValueVariant := 1058;
            if CBCodiPais <> nil then
              CBCodiPais.KeyValue := 1058;
          end else
          begin
            if EdCodMunici.ValueVariant > 0 then
            begin
              Geral.MB_Aviso('N�o foi poss�vel definir o c�digo da cidade!' +
              sLineBreak + 'Verifique manualmente se ele est� correto!');
            end;
          end;
        end;
  (*
        EdNumeTxt,
                TdmkEdit; CBLograd: TdmkDBLookupComboBox;
                EdCodMunici: TdmkEdit; CBCodMunici: TdmkDBLookupComboBox;
                EdCodiPais: TdmkEdit; CBCodiPais: TdmkDBLookupComboBox;
                CBCodiUF: TComboBox = nil);
  *)
      end;
    except
      on E: SigepClienteException do
        Geral.MB_Erro(E.Value);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

{$EndIf}

procedure TUnCEP.ConsultaCEP2(EdCEP, EdLogradTxt, EdRuaTxt, EdNumero,
  EdBairroTxt, EdCidadeTxt, EdUF, EdPais, EdNumeTxt, EdCompl: TdmkEdit;
  CBLograd, CBCodMunici, CBCodiPais: TdmkDBLookupComboBox; EdCodMunici,
  EdCodiPais: TdmkEditCB);
begin
  if DBCheck.CriaFm(TFmEntiCEP2, FmEntiCEP2, afmoNegarComAviso) then
  begin
    FmEntiCEP2.ShowModal;
    //
    if EdCEP <> nil then
      EdCEP.ValueVariant := FmEntiCEP2.FCEP;
    if EdLogradTxt <> nil then
      EdLogradTxt.ValueVariant := Geral.FF0(FmEntiCEP2.FTipoLograd);
    if CBLograd <> nil then
      CBLograd.KeyValue := FmEntiCEP2.FTipoLograd;
    if EdRuaTxt <> nil then
      EdRuaTxt.ValueVariant := Geral.Maiusculas(FmEntiCEP2.FLogradouro, True);
    if EdCidadeTxt <> nil then
      EdCidadeTxt.ValueVariant := Geral.Maiusculas(FmEntiCEP2.FCidade, True);
    if EdCodMunici <> nil then
      EdCodMunici.ValueVariant := ObtemCodigoDTBMunici(FmEntiCEP2.FCidade,
                                    FmEntiCEP2.FUF);
    if CBCodMunici <> nil then
      CBCodMunici.KeyValue := ObtemCodigoDTBMunici(FmEntiCEP2.FCidade,
                                FmEntiCEP2.FUF);
    if EdPais <> nil then
      EdPais.ValueVariant := 'BRASIL';
    if EdCodiPais <> nil then
      EdCodiPais.ValueVariant := 1058;
    if CBCodiPais <> nil then
      CBCodiPais.KeyValue := 1058;
    if EdUF <> nil then
      EdUF.ValueVariant := UpperCase(FmEntiCEP2.FUF);
    if EdBairroTxt <> nil then
      EdBairroTxt.ValueVariant := Geral.Maiusculas(FmEntiCEP2.FBairro, True);
    //
    FmEntiCEP2.Destroy;
    //
    if EdNumero.Enabled then
      EdNumero.SetFocus;
  end;
end;

procedure TUnCEP.ConsultaCEP3(EdCEP, EdLogradTxt, EdRuaTxt, EdNumero,
              EdBairroTxt, EdCidadeTxt, EdUF, EdPais, EdNumeTxt, EdCompl:
              TdmkEdit; CBLograd: TdmkDBLookupComboBox;
              EdCodMunici: TdmkEdit; CBCodMunici: TdmkDBLookupComboBox;
              EdCodiPais: TdmkEdit; CBCodiPais: TdmkDBLookupComboBox;
              CBCodiUF: TComboBox = nil);

var
  //FConectou: Boolean;
  CEP, CEP_UF, UF, xCEP: String;
  CodMunici(*, LocalCod*): Integer;
  FUF_Txt, FLocalTxt: String;
  ListSource: TDataSource;
  Qry: TmySQLQuery;
begin
  Screen.Cursor := crHourGlass;
  //
  try
    CEP := Geral.SoNumero_TT(EdCEP.Text);
    CEP_UF := Copy(CEP, 1, 5);
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrRV_CEP, DModG.RV_CEP_DB, [
    'SELECT * FROM uf ',
    'WHERE "' + CEP_UF + '" BETWEEN Cep1 AND cep2 ',
    ' ']);
    if (DModG.QrRV_CEP.RecordCount > 0) and
    (DModG.QrRV_CEP.FieldByName('uf').AsWideString <> EmptyStr) then
    begin
      UF := DModG.QrRV_CEP.FieldByName('uf').AsWideString;
      xCEP := CEP_UF + '-' + Copy(CEP, 6);
      UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrRV_CEP, DModG.RV_CEP_DB, [
      'SELECT * FROM ' + UF,
      'WHERE cep="' + xCEP + '"',
      ' ']);
      if (DModG.QrRV_CEP.RecordCount > 0) and
      (DModG.QrRV_CEP.FieldByName('cep').AsWideString = xCEP) then
      begin
        if EdCidadeTxt <> nil then
          EdCidadeTxt.ValueVariant := DModG.QrRV_CEP.FieldByName('cidade').AsWideString;
        if EdRuaTxt <> nil then
          EdRuaTxt.ValueVariant := DModG.QrRV_CEP.FieldByName('logradouro').AsWideString;
        if EdBairroTxt <> nil then
          EdBairroTxt.ValueVariant := DModG.QrRV_CEP.FieldByName('bairro').AsWideString;
        ListSource := TDataSource(CBLograd.ListSource);
        if ListSource <> nil then
        begin
          Qry := TmySQLQuery(ListSource.DataSet);
          if Qry <> nil then
          if Qry.Locate('Nome', DModG.QrRV_CEP.FieldByName('tp_logradouro').AsWideString, []) then
          begin
            EdLogradTxt.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
            CBLograd.KeyValue := Qry.FieldByName('Codigo').AsInteger;
          end;
        end;
        //
        CodMunici := 0;
        if (EdCodMunici <> nil) then
        begin
          (*
          if LocalCod > 0 then
          begin
            if USQLDB.TabelaExiste('dtb_munici', DModG.AllID_DB) then
            begin
              DModG.QrAllAux.SQL.Clear;
              DModG.QrAllAux.SQL.Add('SELECT Codigo FROM dtb_munici');
              DModG.QrAllAux.SQL.Add('WHERE DNE=' + Geral.FF0(LocalCod));
              UnDmkDAC_PF.AbreQueryApenas(DModG.QrAllAux);
              CodMunici := DModG.QrAllAux.FieldByName('Codigo').AsInteger;
            end;
          end;
          *)
          if CodMunici = 0 then
          begin
            //FUF_Txt := Geral.FF0(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UF));
            FLocalTxt := DModG.QrRV_CEP.FieldByName('cidade').AsWideString;
            CodMunici := ObtemCodigoDTBMunici(FLocalTxt, UF);
          end;
          if CodMunici > 0 then
          begin
            if EdCodMunici <> nil then
              EdCodMunici.ValueVariant := CodMunici;
            if CBCodMunici <> nil then
              CBCodMunici.KeyValue := CodMunici;
            if EdCodiPais <> nil then
              EdCodiPais.ValueVariant := 1058;
            if CBCodiPais <> nil then
              CBCodiPais.KeyValue := 1058;
          end else
          begin
            if EdCodMunici.ValueVariant > 0 then
            begin
              Geral.MB_Aviso('N�o foi poss�vel definir o c�digo da cidade!' +
              sLineBreak + 'Verifique manualmente se ele est� correto! (CEP3)');
            end;
          end;
        end;


        if EdNumeTxt <> nil then
           EdNumero.SetFocus;

      end;
    end;
  except
    ; // nada
  end;
  Screen.Cursor := crDefault;
end;

procedure TUnCEP.ConsultaCEP4(EdCEP, EdLogradTxt, EdRuaTxt, EdNumero,
              EdBairroTxt, EdCidadeTxt, EdUF, EdPais, EdNumeTxt, EdCompl:
              TdmkEdit; CBLograd: TdmkDBLookupComboBox;
              EdCodMunici: TdmkEdit; CBCodMunici: TdmkDBLookupComboBox;
              EdCodiPais: TdmkEdit; CBCodiPais: TdmkDBLookupComboBox;
              CBCodiUF: TComboBox = nil);

var
  //FConectou: Boolean;
  CEP, CEP_UF, UF, xCEP: String;
  CodMunici(*, LocalCod*): Integer;
  FUF_Txt, FLocalTxt, Json: String;
  ListSource: TDataSource;
  Qry: TmySQLQuery;
  ObjCep: TCep;
begin
  Screen.Cursor := crHourGlass;
  //
  try
    CEP := EdCEP.Text;
    //
    if Grl_DmkREST.ObtemDadosCep(False, CEP, Json) then
    begin
      ObjCep := TCep.Create(Json);
      //
      if ObjCep.Logradouro = '' then
      begin
        Geral.MB_Aviso('CEP n�o localizado!');
        Screen.Cursor := crDefault;
        exit;
      end;
      //
      if EdCidadeTxt <> nil then
        EdCidadeTxt.ValueVariant := ObjCep.Cidade;
      if EdRuaTxt <> nil then
        EdRuaTxt.ValueVariant := ObjCep.Logradouro;
      if EdBairroTxt <> nil then
        EdBairroTxt.ValueVariant := ObjCep.Bairro;
      //
      ListSource := TDataSource(CBLograd.ListSource);
      if ListSource <> nil then
      begin
        Qry := TmySQLQuery(ListSource.DataSet);
        if Qry <> nil then
        if Qry.Locate('Nome', ObjCep.TpLogradouro, [loCaseInsensitive]) then
        begin
          EdLogradTxt.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
          CBLograd.KeyValue        := Qry.FieldByName('Codigo').AsInteger;
        end;
      end;
      //
      CodMunici := 0;
      if (EdCodMunici <> nil) then
      begin
        if CodMunici = 0 then
        begin
          FLocalTxt := ObjCep.Cidade;
          CodMunici := ObtemCodigoDTBMunici(FLocalTxt, ObjCep.Uf);
        end;
        if CodMunici > 0 then
        begin
          if EdCodMunici <> nil then
            EdCodMunici.ValueVariant := CodMunici;
          if CBCodMunici <> nil then
            CBCodMunici.KeyValue := CodMunici;
          if EdCodiPais <> nil then
            EdCodiPais.ValueVariant := 1058;
          if CBCodiPais <> nil then
            CBCodiPais.KeyValue := 1058;
        end else
        begin
          if EdCodMunici.ValueVariant > 0 then
          begin
            Geral.MB_Aviso('N�o foi poss�vel definir o c�digo da cidade!' +
            sLineBreak + 'Verifique manualmente se ele est� correto! (CEP3)');
          end;
        end;
      end;
      //
      if EdNumeTxt <> nil then
         EdNumero.SetFocus;
    end;
  except
    ;
  end;
  Screen.Cursor := crDefault;
end;

// 2022-03
{$ifNDef sACBr}
procedure TUnCEP.ConsultaCEP5(EdCEP, EdLogradTxt, EdRuaTxt, EdNumero,
  EdBairroTxt, EdCidadeTxt, EdUF, EdPais, EdNumeTxt, EdCompl: TdmkEdit;
  CBLograd: TdmkDBLookupComboBox; EdCodMunici: TdmkEdit;
  CBCodMunici: TdmkDBLookupComboBox; EdCodiPais: TdmkEdit;
  CBCodiPais: TdmkDBLookupComboBox; CBCodiUF: TComboBox);
var
(*
  //FConectou: Boolean;
  CEP, CEP_UF, UF, xCEP: String;
  FUF_Txt, FLocalTxt, Json: String;
  ObjCep: TCep;*)
  //
  ListSource: TDataSource;
  Qry: TmySQLQuery;
  CodMunici: Integer;
  //
  CEP, XML: String;
  xmlDoc: IXMLDocument;
  Rua, Complemento, Complemento2, Complementos, Bairro, xCidade, FUF_Txt: String;
begin
(* 2023-12-11
  Screen.Cursor := crHourGlass;
  //
  try
    CEP := EdCEP.Text;
    //
    XML := DmkACBr_PF.ObtemDadosCEP(Geral.SoNumero_TT(CEP));
    //Geral.MB_Teste(XML);
    //
    Rua          := UTF8ToString(DmkPF.SeparaDadosCampoDeXML(XML, 'end'));
    Bairro       := UTF8ToString(DmkPF.SeparaDadosCampoDeXML(XML, 'bairro'));
    xCidade      := UTF8ToString(DmkPF.SeparaDadosCampoDeXML(XML, 'cidade'));
    FUF_Txt      := UTF8ToString(DmkPF.SeparaDadosCampoDeXML(XML, 'uf'));
    //
    Complemento  := UTF8ToString(DmkPF.SeparaDadosCampoDeXML(XML, 'complemento'));  // Existe ?????
    Complemento2 := UTF8ToString(DmkPF.SeparaDadosCampoDeXML(XML, 'complemento2'));
    Complementos := Complemento;
    if Complementos <> EmptyStr then
     Complementos := Trim(Complemento + ' ' + Complemento2);
    //
    Geral.MB_Info(
      'Rua: ' + Rua + sLineBreak +
      'Complemento:' + Complementos + sLineBreak +
      'Bairro:' + Bairro + sLineBreak +
      'Cidade:' + xCidade + sLineBreak +
      'UF:' + FUF_Txt + sLineBreak +
      '');

{
    xmlDoc := TXMLDocument.Create;
    try
      if not XMLLoadFromAnsiString(xmlDoc, FTextoArq) then
      begin
        Result := False;
        dmkPF.LeTexto_Permanente(FTextoArq, FXML_Load_Failure);
      end else Result := True;
    finally
      FreeAndNil(XMLDoc);
    end;

    if Grl_DmkREST.ObtemDadosCep(False, CEP, Json) then
    begin
      ObjCep := TCep.Create(Json);
      //
      if ObjCep.Logradouro = '' then
      begin
        Geral.MB_Aviso('CEP n�o localizado!');
        Screen.Cursor := crDefault;
        exit;
      end;
      //
}
      if EdCidadeTxt <> nil then
        EdCidadeTxt.ValueVariant := xCidade;
      if EdRuaTxt <> nil then
        EdRuaTxt.ValueVariant := Rua;
      if EdBairroTxt <> nil then
        EdBairroTxt.ValueVariant := Bairro;
      //
      ListSource := TDataSource(CBLograd.ListSource);
{
      if ListSource <> nil then
      begin
        Qry := TmySQLQuery(ListSource.DataSet);
        if Qry <> nil then
        if Qry.Locate('Nome', ObjCep.TpLogradouro, [loCaseInsensitive]) then
        begin
          EdLogradTxt.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
          CBLograd.KeyValue        := Qry.FieldByName('Codigo').AsInteger;
        end;
      end;
}
      //
      CodMunici := 0;
      if (EdCodMunici <> nil) then
      begin
        if CodMunici = 0 then
        begin
          //FLocalTxt := xCidade;
          CodMunici := ObtemCodigoDTBMunici(xCidade, FUF_Txt);
        end;
        if CodMunici > 0 then
        begin
          if EdCodMunici <> nil then
            EdCodMunici.ValueVariant := CodMunici;
          if CBCodMunici <> nil then
            CBCodMunici.KeyValue := CodMunici;
          if EdCodiPais <> nil then
            EdCodiPais.ValueVariant := 1058;
          if CBCodiPais <> nil then
            CBCodiPais.KeyValue := 1058;
        end else
        begin
          if EdCodMunici.ValueVariant > 0 then
          begin
            Geral.MB_Aviso('N�o foi poss�vel definir o c�digo da cidade!' +
            sLineBreak + 'Verifique manualmente se ele est� correto! (CEP3)');
          end;
        end;
      end;
      //
      if EdNumeTxt <> nil then
         EdNumero.SetFocus;
//    end;
  except
    ;
  end;
  Screen.Cursor := crDefault;
*)
end;
{$EndIf}

function TUnCEP.DeCryptCEP(Crypt: string; twofish: TDCP_twofish): string;
begin
  twofish.InitStr('gpbe', TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.DecryptString(Crypt); // Descriptografa o CEP.
  twofish.Burn();
end;

procedure TUnCEP.DescobrirCEP(EdCEP, EdLograd, EdRua, EdBairro, EdCidade, EdUF,
  EdPais, EdCodMunici, EdCodiPais: TdmkEdit; CBLograd, CBCodMunici,
  CBCodiPais: TdmkDBLookupComboBox);
begin
{$IfDef cDbTable}
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    FmEntiCEP.EdCEPLoc.Text := EdCEP.Text;
    //
    FmEntiCEP.FEdCEP       :=  EdCEP;
    FmEntiCEP.FEdLograd    :=  EdLograd;
    FmEntiCEP.FCBLograd    :=  CBLograd;
    FmEntiCEP.FEdRua       :=  EdRua;
    FmEntiCEP.FEdBairro    :=  EdBairro;
    FmEntiCEP.FEdCidade    :=  EdCidade;
    FmEntiCEP.FEdUF        :=  EdUF;
    FmEntiCEP.FEdPais      :=  EdPais;
    FmEntiCEP.FEdCodMunici :=  EdCodMunici;
    FmEntiCEP.FCBCodMunici :=  CBCodMunici;
    FmEntiCEP.FEdCodiPais  :=  EdCodiPais;
    FmEntiCEP.FCBCodiPais  :=  CBCodiPais;
    //
    FmEntiCEP.DBGrid2.Visible := False;
    FmEntiCEP.Painel2.Visible := False;
    FmEntiCEP.ShowModal;
    FmEntiCEP.Destroy;
  end;
{$Else}
  //    TODO Berlin
  Geral.MB_Info('Solicite implementa��o � Dermatek! (DBE Table)');
{$EndIf}
end;

function TUnCEP.EnCryptCEP(CEP: string; twofish: TDCP_twofish): string;
begin
  twofish.InitStr('gpbe', TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.EncryptString(CEP); // Criptografa o CEP.
  twofish.Burn();
end;

function TUnCEP.ObtemCodigoDTBMunici(Cidade, UF: String): Integer;
var
  QrLoc: TmySQLQuery;
begin
  QrLoc := TmySQLQuery.Create(Dmod);
  QrLoc.Close;
  QrLoc.Database := DModG.AllID_DB;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT mun.Codigo');
  QrLoc.SQL.Add('FROM dtb_munici mun');
  QrLoc.SQL.Add('WHERE LEFT(Codigo, 2) = :P0');
  QrLoc.SQL.Add('AND mun.Nome=:P1');
  QrLoc.Params[0].AsString := Geral.FF0(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UF));
  QrLoc.Params[1].AsString := Cidade;
  //
  UnDmkDAC_PF.AbreQueryApenas(QrLoc);
  if QrLoc.RecordCount > 0 then
    Result := QrLoc.FieldByName('Codigo').AsInteger
  else
    Result := 0;
end;

end.
