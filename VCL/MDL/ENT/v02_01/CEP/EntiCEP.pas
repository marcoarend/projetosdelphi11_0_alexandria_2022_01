unit EntiCEP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) Grids, DBGrids, StdCtrls, DBCtrls, UnInternalConsts,
  Buttons, ExtCtrls, mySQLDbTables, Variants, DCPcrypt2,
  DCPblockciphers, DCPtwofish, Mask, ComCtrls, dmkGeral, dmkImage,
  dmkEdit, dmkDBLookupComboBox, UnDMkEnums, mySQLDirectQuery;

type
  TFmEntiCEP = class(TForm)
    DNCEP: TDatabase;
    TbLOG_BAIRRO: TTable;
    TbLOG_BAIRROBAI_NU_SEQUENCIAL: TIntegerField;
    TbLOG_BAIRROUFE_SG: TWideStringField;
    TbLOG_BAIRROLOC_NU_SEQUENCIAL: TIntegerField;
    TbLOG_BAIRROBAI_NO: TWideStringField;
    TbLOG_BAIRROBAI_NO_ABREV: TWideStringField;
    TbLOG_CEP: TTable;
    TbLOG_CPC: TTable;
    TbLOG_FAIXA_BAIRRO: TTable;
    DsLOG_FAIXA_BAIRRO: TDataSource;
    DsLOG_FAIXA_CPC: TDataSource;
    TbLOG_FAIXA_CPC: TTable;
    DsLOG_FAIXA_LOCALIDADE: TDataSource;
    TbLOG_FAIXA_LOCALIDADE: TTable;
    TbLOG_FAIXA_UF: TTable;
    DsLOG_FAIXA_UF: TDataSource;
    DsLOG_FAIXA_UOP: TDataSource;
    TbLOG_FAIXA_UOP: TTable;
    DsLOG_GRANDE_USUARIO: TDataSource;
    TbLOG_GRANDE_USUARIO: TTable;
    DsLOG_LOCALIDADE: TDataSource;
    TbLOG_LOCALIDADE: TTable;
    TbLOG_LOCALIDADELOC_NU_SEQUENCIAL: TIntegerField;
    TbLOG_LOCALIDADELOC_NOSUB: TWideStringField;
    TbLOG_LOCALIDADELOC_NO: TWideStringField;
    TbLOG_LOCALIDADECEP: TWideStringField;
    TbLOG_LOCALIDADEUFE_SG: TWideStringField;
    TbLOG_LOCALIDADELOC_IN_SITUACAO: TIntegerField;
    TbLOG_LOCALIDADELOC_IN_TIPO_LOCALIDADE: TWideStringField;
    TbLOG_LOCALIDADELOC_NU_SEQUENCIAL_SUB: TIntegerField;
    TbLOG_LOGRADOURO: TTable;
    TbLOG_LOGRADOUROLOG_NU_SEQUENCIAL: TIntegerField;
    TbLOG_LOGRADOUROUFE_SG: TWideStringField;
    TbLOG_LOGRADOUROLOC_NU_SEQUENCIAL: TIntegerField;
    TbLOG_LOGRADOUROLOG_NO: TWideStringField;
    TbLOG_LOGRADOUROLOG_NOME: TWideStringField;
    TbLOG_LOGRADOUROBAI_NU_SEQUENCIAL_INI: TIntegerField;
    TbLOG_LOGRADOUROBAI_NU_SEQUENCIAL_FIM: TIntegerField;
    TbLOG_LOGRADOUROCEP: TWideStringField;
    TbLOG_LOGRADOUROLOG_COMPLEMENTO: TWideStringField;
    TbLOG_LOGRADOUROLOG_TIPO_LOGRADOURO: TWideStringField;
    TbLOG_LOGRADOUROLOG_STATUS_TIPO_LOG: TWideStringField;
    TbLOG_LOGRADOUROLOG_NO_SEM_ACENTO: TWideStringField;
    DsLOG_LOGRADOURO: TDataSource;
    DsLOG_TIPO_LOGR: TDataSource;
    TbLOG_TIPO_LOGR: TTable;
    TbLOG_UNID_OPER: TTable;
    TbLOG_UNID_OPERUOP_NU_SEQUENCIAL: TIntegerField;
    TbLOG_UNID_OPERUFE_SG: TWideStringField;
    TbLOG_UNID_OPERLOC_NU_SEQUENCIAL: TIntegerField;
    TbLOG_UNID_OPERLOG_NU_SEQUENCIAL: TIntegerField;
    TbLOG_UNID_OPERBAI_NU_SEQUENCIAL: TIntegerField;
    TbLOG_UNID_OPERUOP_NO: TWideStringField;
    TbLOG_UNID_OPERCEP: TWideStringField;
    TbLOG_UNID_OPERUOP_ENDERECO: TWideMemoField;
    TbLOG_UNID_OPERUOP_IN_CP: TWideStringField;
    DsLOG_UNID_OPER: TDataSource;
    DsCEP: TDataSource;
    TbLOG_TIPO_LOGRTipoLogradouro: TWideStringField;
    DsLOG_BAIRRO: TDataSource;
    DsLOG_CEP: TDataSource;
    DsLOG_CPC: TDataSource;
    DsUF: TDataSource;
    DsCidades: TDataSource;
    DsTipo: TDataSource;
    QrUF2: TmySQLQuery;
    QrUF2Codigo: TIntegerField;
    QrUF2Nome: TWideStringField;
    PainelDados: TPanel;
    Painel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    LaCEP: TLabel;
    CBUF: TDBLookupComboBox;
    CBCidade: TDBLookupComboBox;
    CBTipo: TDBLookupComboBox;
    EdRua: TEdit;
    BtLocaliza1: TBitBtn;
    Painel2: TPanel;
    Label3: TLabel;
    EdCEPLoc: TEdit;
    BtLocaliza2: TBitBtn;
    Panel1: TPanel;
    QrCEP: TQuery;
    QrCEPNOMEBAIRROINI: TWideStringField;
    QrCEPNOMEBAIRROFIM: TWideStringField;
    QrCEPUFE_SG: TWideStringField;
    QrCEPLOG_NOME: TWideStringField;
    QrCEPCEP: TWideStringField;
    QrCEPLOG_NU_SEQUENCIAL: TIntegerField;
    QrCEPLOC_NU_SEQUENCIAL: TIntegerField;
    QrCEPLOG_NO: TWideStringField;
    QrCEPBAI_NU_SEQUENCIAL_INI: TIntegerField;
    QrCEPBAI_NU_SEQUENCIAL_FIM: TIntegerField;
    QrCEPLOG_COMPLEMENTO: TWideStringField;
    QrCEPLOG_TIPO_LOGRADOURO: TWideStringField;
    QrCEPLOG_STATUS_TIPO_LOG: TWideStringField;
    QrCEPLOG_NO_SEM_ACENTO: TWideStringField;
    QrCEPcidade: TWideStringField;
    QrCEPUF: TIntegerField;
    QrUF: TQuery;
    QrUFUFE_SG: TWideStringField;
    QrCidades: TQuery;
    QrCidadesLOC_NU_SEQUENCIAL: TIntegerField;
    QrCidadesLOC_NOSUB: TWideStringField;
    QrCidadesLOC_NO: TWideStringField;
    QrCidadesCEP: TWideStringField;
    QrCidadesUFE_SG: TWideStringField;
    QrCidadesLOC_IN_SITUACAO: TIntegerField;
    QrCidadesLOC_IN_TIPO_LOCALIDADE: TWideStringField;
    QrCidadesLOC_NU_SEQUENCIAL_SUB: TIntegerField;
    QrTipo: TQuery;
    QrTipoTipoLogradouro: TWideStringField;
    QrUFPadrao: TmySQLQuery;
    QrUFPadraoNome: TWideStringField;
    Panel3: TPanel;
    CkCEP: TCheckBox;
    CkLogradouro: TCheckBox;
    CkBairroIni: TCheckBox;
    CkBairroFim: TCheckBox;
    CkCidade: TCheckBox;
    CkUF: TCheckBox;
    CkBrasil: TCheckBox;
    CkTipoLograd: TCheckBox;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    QrListaLogradLk: TIntegerField;
    QrListaLogradDataCad: TDateField;
    QrListaLogradDataAlt: TDateField;
    QrListaLogradUserCad: TIntegerField;
    QrListaLogradUserAlt: TIntegerField;
    QrTipoCodigo: TIntegerField;
    DCP_twofish1: TDCP_twofish;
    QrCEPCEP_DECRYPT: TWideStringField;
    QrCidadesCEP_DECRYPT: TWideStringField;
    QrCidadesLOC_KEY_DNE: TWideStringField;
    TabControl1: TTabControl;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    QrGRU: TQuery;
    QrGRUGRU_NU_SEQUENCIAL: TIntegerField;
    QrGRUUFE_SG: TWideStringField;
    QrGRULOC_NU_SEQUENCIAL: TIntegerField;
    QrGRULOG_NU_SEQUENCIAL: TIntegerField;
    QrGRUBAI_NU_SEQUENCIAL: TIntegerField;
    QrGRUGRU_NO: TWideStringField;
    QrGRUCEP: TWideStringField;
    QrGRUGRU_ENDERECO: TWideStringField;
    QrGRUGRU_KEY_DNE: TWideStringField;
    QrGRUTEMP: TWideStringField;
    DsGRU: TDataSource;
    ST1: TStaticText;
    DBGrid3: TDBGrid;
    QrGRUCEP_DECRYPT: TWideStringField;
    QrGRULOC_NO: TWideStringField;
    QrGRUBAI_NO: TWideStringField;
    QrUOP: TQuery;
    DsUOP: TDataSource;
    QrUOPLOC_NO: TWideStringField;
    QrUOPBAI_NO: TWideStringField;
    QrUOPUOP_NU_SEQUENCIAL: TIntegerField;
    QrUOPUFE_SG: TWideStringField;
    QrUOPLOC_NU_SEQUENCIAL: TIntegerField;
    QrUOPLOG_NU_SEQUENCIAL: TIntegerField;
    QrUOPBAI_NU_SEQUENCIAL: TIntegerField;
    QrUOPUOP_NO: TWideStringField;
    QrUOPCEP: TWideStringField;
    QrUOPUOP_ENDERECO: TWideStringField;
    QrUOPUOP_IN_CP: TWideStringField;
    QrUOPUOP_KEY_DNE: TWideStringField;
    QrUOPTEMP: TWideStringField;
    QrUOPCEP_DECRYPT: TWideStringField;
    DBGrid4: TDBGrid;
    QrCPC: TQuery;
    QrCPCLOC_NO: TWideStringField;
    QrCPCCPC_NU_SEQUENCIAL: TIntegerField;
    QrCPCUFE_SG: TWideStringField;
    QrCPCLOC_NU_SEQUENCIAL: TIntegerField;
    QrCPCCEP: TWideStringField;
    QrCPCCPC_NO: TWideStringField;
    QrCPCCPC_ENDERECO: TWideStringField;
    QrCPCCPC_TIPO: TWideStringField;
    QrCPCCPC_ABRANGENCIA: TWideStringField;
    QrCPCCPC_KEY_DNE: TWideStringField;
    QrCPCTEMP: TWideStringField;
    QrCPCCEP_DECRYPT: TWideStringField;
    DBGrid5: TDBGrid;
    DsCPC: TDataSource;
    EdCEP: TEdit;
    LaUnico: TLabel;
    QrUnico: TQuery;
    QrUnicoLOG_NU_SEQUENCIAL: TIntegerField;
    QrUnicoBAI_NU_SEQUENCIAL_INI: TIntegerField;
    Label6: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    DBGrid6: TDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    DBGrid7: TDBGrid;
    mySQLDirectQuery1: TmySQLDirectQuery;
    procedure FormCreate(Sender: TObject);
    procedure CBUFClick(Sender: TObject);
    procedure CBCidadeClick(Sender: TObject);
    procedure CBTipoClick(Sender: TObject);
    procedure BtLocaliza2Click(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure CkBairroIniClick(Sender: TObject);
    procedure CkBairroFimClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure QrCEPCalcFields(DataSet: TDataSet);
    procedure QrCidadesCalcFields(DataSet: TDataSet);
    procedure EdRuaChange(Sender: TObject);
    procedure TabControl1Changing(Sender: TObject; var AllowChange: Boolean);
    procedure TabControl1Change(Sender: TObject);
    procedure EdCEPLocEnter(Sender: TObject);
    procedure QrGRUCalcFields(DataSet: TDataSet);
    procedure QrUOPCalcFields(DataSet: TDataSet);
    procedure QrCPCCalcFields(DataSet: TDataSet);
    procedure EdCEPChange(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtLocaliza1Click(Sender: TObject);
  private
    { Private declarations }
    F_CEP_B, F_CEP_C: String;

    function TamCrypt(Texto: String): Integer;
    function PesquisaTipoLogradouro(const LogrC, LogrN: String;
             var LogrCod: Integer; var LogrTipTxt, LogrNome, NumeTxt: String;
             const Tipo: Integer): Boolean;
    procedure PesquisaLogradouros();
    procedure PesquisaGrandesUsuarios();
    procedure PesquisaUnidadesOperacionais();
    procedure PesquisaCaixasPostaisComunitarias();
    procedure OcultaDados();
  public
    { Public declarations }
    FUF_Txt, FCEP, FLogrTipTxt, FLogrTxt, FNumeTxt, FBairroI, FBairroF,
    FLocalTxt, FPais: String;
    FLogrTip, FLogrCod, FUF_Cod, FLocalCod, FBairICod: Integer;
    FConfirmou: Boolean;
    FEdCEP: TdmkEdit;
    FEdLograd: TdmkEdit;
    FCBLograd: TdmkDBLookupComboBox;
    FEdRua: TdmkEdit;
    FEdBairro: TdmkEdit;
    FEdCidade: TdmkEdit;
    FEdUF: TdmkEdit;
    FEdPais: TDmkEdit;
    FEdCodMunici: TdmkEdit;
    FCBCodMunici: TdmkDBLookupComboBox;
    FEdCodiPais: TdmkEdit;
    FCBCodiPais: TdmkDBLookupComboBox;
    //
    procedure DefineDadosPesquisaCEP();
    function  DefineCodMunici_CodiPais(LocalCod: Integer;
              EdCodMunici, EdCodiPais: TdmkEdit; CBCodMunici,
              CBCodiPais: TdmkDBLookupComboBox): Boolean;
  end;

var
  FmEntiCEP: TFmEntiCEP;
implementation

uses UnMyObjects, Module, Principal, Entidade2, DmkDAC_PF, ModuleGeral, UnCEP,
  EntidadesNew, UMySQLModule, UMySQLDB;

{$R *.DFM}

const
  TbLog = ' Logradouro ';
  TbGRU = ' Grande usu�rio ';
  TbUOP = ' Unidade operacional ';
  TbCPC = ' Caixa postal comunit�ria ';

procedure TFmEntiCEP.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  try 
    DNCEP.Connected := True;
    //
    FConfirmou := False;
    {
    PageControl1.ActivePageIndex := 0;
    for i := 0 to PageControl1.PageCount - 1 do
      PageControl1.Pages[i].Visible := False;
    TabSheet1.Visible := False;
    TabSheet2.Visible := False;
    TabSheet3.Visible := False;
    TabSheet4.Visible := False;
    }
    //
    try
      UMyMod.AbreBDEQuery(QrUF);
      UnDmkDAC_PF.AbreQuery(QrListaLograd, Dmod.MyDB);
    except
      // VOSTRO_1320
      raise;
      //Geral.MB_Aviso('N�o foi poss�vel abrir as tabelas de CEP!');
    end;
    DBGrid2.Align := alClient;
    if VAR_UFPADRAO <> 0 then
    begin
      QrUFPadrao.Close;
      QrUFPadrao.Params[0].AsInteger := VAR_UFPADRAO;
      UnDmkDAC_PF.AbreQuery(QrUFPadrao, Dmod.MyDB);
      if QrUF.Locate('UFE_SG', QrUFPadraoNome.Value, [loCaseInsensitive]) then
      begin
        CBUF.KeyValue := QrUFPadraoNome.Value;
        CBUFClick(Self);
      end;
    end;
    DBGrid1.Align := alClient;
    DBGrid2.Align := alClient;
    DBGrid3.Align := alClient;
    DBGrid4.Align := alClient;
    DBGrid5.Align := alClient;
  except
    Close;
  end;
end;

procedure TFmEntiCEP.CBUFClick(Sender: TObject);
begin
  QrCidades.Close;
  QrCidades.Params[0].AsString := CBUF.KeyValue;
  UMyMod.AbreBDEQuery(QrCidades);
  if QrCidades.Locate('LOC_NOSUB', VAR_CIDADEPADRAO, [loCaseInsensitive]) then
  begin
    CBCidade.KeyValue := QrCidadesLOC_NU_SEQUENCIAL.Value;
    CBCidadeClick(Self);
  end else begin
    CBCidade.KeyValue := Null;
    QrTipo.Close;
    CBTipo.KeyValue := Null;
  end;
end;

procedure TFmEntiCEP.CBCidadeClick(Sender: TObject);
begin
  CBTipo.KeyValue := Null;
  QrTipo.Close;
  if QrCidadesCEP.Value <> '' then
  begin
    CBTipo.KeyValue := Null;
    //QrTipo.Close;
    EdCEP.Text := Geral.FormataCEP_TT(QrCidadesCEP_DECRYPT.Value);
    EdCEP.Visible := True;
  end else begin
    EdCEP.Text    := '';
    EdCEP.Visible := False;
    UMyMod.AbreBDEQuery(QrTipo);
  end;
end;

procedure TFmEntiCEP.CBTipoClick(Sender: TObject);
begin
  EdRua.Enabled := True;
end;

procedure TFmEntiCEP.EdCEPChange(Sender: TObject);
begin
  LaUnico.Visible := Trim(EdCEP.Text) <> '';
end;

procedure TFmEntiCEP.EdCEPLocEnter(Sender: TObject);
begin
  OcultaDados;
end;

procedure TFmEntiCEP.EdRuaChange(Sender: TObject);
begin
  if Length(Trim(EdRua.Text)) > 2 then
    BtLocaliza1.Enabled := True
  else
    BtLocaliza1.Enabled := False;
end;

procedure TFmEntiCEP.BtLocaliza1Click(Sender: TObject);
var
  Liga: String;
begin
  OcultaDados;
  //
  QrCEP.Close;
  QrCEP.SQL.Clear;
  //
  if CBTipo.KeyValue  <> 'Outros' then
    Liga := 'AND lo.LOG_TIPO_LOGRADOURO='''+CBTipo.Text+''''
  else Liga := 'AND lo.LOG_TIPO_LOGRADOURO<>'''+CBTipo.Text+'''';
  //
  QrCEP.SQL.Add('SELECT lo.*');
  QrCEP.SQL.Add('FROM log_logradouro lo');
  QrCEP.SQL.Add('WHERE lo.UFE_SG='''+CBUF.Text+'''');
  QrCEP.SQL.Add('AND lo.LOC_NU_SEQUENCIAL='+IntToStr(CBCidade.KeyValue));
  QrCEP.SQL.Add('AND  lo.LOG_NO LIKE ''%'+EdRua.Text+'%''');
  QrCEP.SQL.Add(Liga);
  QrCEP.SQL.Add('');
  UMyMod.AbreBDEQuery(QrCEP);
  //
  if QrCEP.RecordCount > 0 then
  begin
    TabControl1.Tabs.Add(TbLog);
    DBGrid1.Visible := Panel1.Visible;
    DBGrid2.Visible := Panel2.Visible;
    //
    TabControl1.Visible := True;
    TabControl1Change(Self);
  end;
end;

procedure TFmEntiCEP.BtLocaliza2Click(Sender: TObject);
begin
  OcultaDados;
  //
  F_CEP_B := Geral.SoNumero_TT(EdCEPLoc.Text);
  if Length(F_CEP_B) < 3 then
  begin
    Geral.MB_Aviso('Informe pelo menos os primeiros 3 n�meros do CEP.');
    Exit;
  end;
  F_CEP_C := F_CEP_B;
  while Length(F_CEP_C) < 8 do F_CEP_C := F_CEP_C + '0';
  F_CEP_C := U_CEP.EnCryptCEP(F_CEP_C, DCP_twofish1);
  F_CEP_C := Copy(F_CEP_C, 1, TamCrypt(F_CEP_B));
  //
  PesquisaLogradouros();
  //
  PesquisaGrandesUsuarios();
  //
  PesquisaUnidadesOperacionais();
  //
  PesquisaCaixasPostaisComunitarias();
end;

procedure TFmEntiCEP.BtConfirmaClick(Sender: TObject);
var
  TBn: String;
begin
  FConfirmou := True;
  //
  if TabControl1.Tabs.Count = 0 then
  begin
    if Panel1.Visible then
    begin
      FCEP      := EdCEP.Text;
      FLocalTxt := Geral.Maiusculas(CBCidade.Text, True);
      FLocalCod := QrCidadesLOC_NU_SEQUENCIAL.Value;
      FUF_Txt   := CBUF.Text;
      FUF_Cod   := Geral.GetCodigoUF_da_SiglaUF(CBUF.Text);
      //
      QrUnico.Close;
      QrUnico.Params[0].AsInteger := QrCidadesLOC_NU_SEQUENCIAL.Value;
      UMyMod.AbreBDEQuery(QrUnico);
      if QrUnico.RecordCount > 0 then
      begin
        FBairICod := QrUnicoBAI_NU_SEQUENCIAL_INI.Value;
        FLogrCod  := QrUnicoLOG_NU_SEQUENCIAL.Value;
      end;
    end;
  end else begin
    TBn := TabControl1.Tabs[TabControl1.TabIndex];
    if (TBn = TbLog) and (DBGrid1.Visible or DBGrid2.Visible) then
    begin
      FCEP      := Geral.FormataCEP_TT(QrCEPCEP_DECRYPT.Value);
      PesquisaTipoLogradouro(QrCEPLOG_NOME.Value, QrCEPLOG_NO.Value,
        FLogrTip, FLogrTipTxt, FLogrTxt, FNumeTxt, 1);
      FLogrCod  := QrCEPLOG_NU_SEQUENCIAL.Value;
      FBairICod := QrCEPBAI_NU_SEQUENCIAL_INI.Value;
      FBairroI  := Geral.Maiusculas(QrCEPNOMEBAIRROINI.Value, True);
      FBairroF  := Geral.Maiusculas(QrCEPNOMEBAIRROFIM.Value, True);
      FLocalTxt := Geral.Maiusculas(QrCEPcidade.Value, True);
      FLocalCod := QrCEPLOC_NU_SEQUENCIAL.Value;
      FPais     := Geral.Maiusculas(CkBrasil.Caption, True);
      FUF_Cod   := QrCEPUF.Value;
      FUF_Txt   := Geral.GetSiglaUF_do_CodigoUF(QrCEPUF.Value);
    end else
    if (TBn = TbGRU) and DBGrid3.Visible  then
    begin
      FCEP      := Geral.FormataCEP_TT(QrGRUCEP_DECRYPT.Value);
      PesquisaTipoLogradouro(QrGRUGRU_ENDERECO.Value, QrGRUGRU_ENDERECO.Value,
        FLogrTip, FLogrTipTxt, FLogrTxt, FNumeTxt, 2);
      FBairICod := QrGRUBAI_NU_SEQUENCIAL.Value;
      FBairroI  := Geral.Maiusculas(QrGRUBAI_NO.Value, True);
      FBairroF  := Geral.Maiusculas(QrGRUBAI_NO.Value, True);
      FLocalTxt := Geral.Maiusculas(QrGRULOC_NO.Value, True);
      FLocalCod := QrGRULOC_NU_SEQUENCIAL.Value;
      FPais     := Geral.Maiusculas(CkBrasil.Caption, True);
      FUF_Cod   := Geral.GetCodigoUF_da_SiglaUF(QrUFUFE_SG.Value);
      FUF_Txt   := QrUFUFE_SG.Value;
    end else
    if (TBn = TbUOP) and DBGrid4.Visible  then
    begin
      FCEP      := Geral.FormataCEP_TT(QrUOPCEP_DECRYPT.Value);
      PesquisaTipoLogradouro(QrUOPUOP_ENDERECO.Value, QrUOPUOP_ENDERECO.Value,
        FLogrTip, FLogrTipTxt, FLogrTxt, FNumeTxt, 2);
      FLogrCod  := QrUOPLOG_NU_SEQUENCIAL.Value;
      FBairICod := QrUOPBAI_NU_SEQUENCIAL.Value;
      FBairroI  := Geral.Maiusculas(QrUOPBAI_NO.Value, True);
      FBairroF  := Geral.Maiusculas(QrUOPBAI_NO.Value, True);
      FLocalTxt := Geral.Maiusculas(QrUOPLOC_NO.Value, True);
      FLocalCod := QrUOPLOC_NU_SEQUENCIAL.Value;
      FPais     := Geral.Maiusculas(CkBrasil.Caption, True);
      FUF_Cod   := Geral.GetCodigoUF_da_SiglaUF(QrUFUFE_SG.Value);
      FUF_Txt   := QrUFUFE_SG.Value;
    end else
    if (TBn = TbCPC) and DBGrid5.Visible  then
    begin
      FCEP      := Geral.FormataCEP_TT(QrCPCCEP_DECRYPT.Value);
      PesquisaTipoLogradouro(QrCPCCPC_ENDERECO.Value, QrCPCCPC_ENDERECO.Value,
        FLogrTip, FLogrTipTxt, FLogrTxt, FNumeTxt, 2);
      FLogrCod  := 0;//QrCPCLOG_NU_SEQUENCIAL.Value;
      FBairICod := 0;
      FBairroI  := '';
      FBairroF  := '';
      FLocalTxt := Geral.Maiusculas(QrCPCLOC_NO.Value, True);
      FLocalCod := QrCPCLOC_NU_SEQUENCIAL.Value;
      FPais     := Geral.Maiusculas(CkBrasil.Caption, True);
      FUF_Cod   := Geral.GetCodigoUF_da_SiglaUF(QrUFUFE_SG.Value);
      FUF_Txt   := QrUFUFE_SG.Value;
    end;
  end;
  //if QrCEP.RecordCount > 0 then
  //begin
    case VAR_ACTIVEPAGE of
      1:
      begin
        if CkCEP.Checked then FmEntidadesNew.EdPCEP.Text               := FCEP;
        if CkTipoLograd.Checked then FmEntidadesNew.CBPLograd.KeyValue := FLogrTip;
        if CkLogradouro.Checked then FmEntidadesNew.EdPRua.Text        := FLogrTxt;
        if CkBairroIni.Checked then FmEntidadesNew.EdPBairro.Text      := FBairroI;
        if CkBairroFim.Checked then FmEntidadesNew.EdPBairro.Text      := FBairroF;
        if CkCidade.Checked then FmEntidadesNew.EdPCidade.Text         := FLocalTxt;
        if CkUF.Checked then FmEntidadesNew.EdPUF.Text                 := FUF_Txt;
        if CkBrasil.Checked then FmEntidadesNew.EdPPais.Text           := FPais;
      end;
      2:
      begin
        if CkCEP.Checked then FmEntidadesNew.EdECEP.Text               := FCEP;
        if CkTipoLograd.Checked then FmEntidadesNew.CBELograd.KeyValue := FLogrTip;
        if CkLogradouro.Checked then FmEntidadesNew.EdERua.Text        := FLogrTxt;
        if CkBairroIni.Checked then FmEntidadesNew.EdEBairro.Text      := FBairroI;
        if CkBairroFim.Checked then FmEntidadesNew.EdEBairro.Text      := FBairroF;
        if CkCidade.Checked then FmEntidadesNew.EdECidade.Text         := FLocalTxt;
        if CkUF.Checked then FmEntidade2.EdEUF.Text                    := FUF_Txt;
        if CkBrasil.Checked then FmEntidadesNew.EdEPais.Text           := FPais;
      end;
      5:
      begin
        if CkCEP.Checked then FmEntidadesNew.EdCCEP.Text               := FCEP;
        if CkTipoLograd.Checked then FmEntidadesNew.CBCLograd.KeyValue := FLogrTip;
        if CkLogradouro.Checked then FmEntidadesNew.EdCRua.Text        := FLogrTxt;
        if CkBairroIni.Checked then FmEntidadesNew.EdCBairro.Text      := FBairroI;
        if CkBairroFim.Checked then FmEntidadesNew.EdCBairro.Text      := FBairroF;
        if CkCidade.Checked then FmEntidadesNew.EdCCidade.Text         := FLocalTxt;
        if CkUF.Checked then FmEntidade2.EdCUF.Text                    := FUF_Txt;
        if CkBrasil.Checked then FmEntidadesNew.EdCPais.Text           := FPais;
      end;
      6:
      begin
        if CkCEP.Checked then FmEntidadesNew.EdLCEP.Text               := FCEP;
        if CkTipoLograd.Checked then FmEntidadesNew.CBLLograd.KeyValue := FLogrTip;
        if CkLogradouro.Checked then FmEntidadesNew.EdLRua.Text        := FLogrTxt;
        if CkBairroIni.Checked then FmEntidadesNew.EdLBairro.Text      := FBairroI;
        if CkBairroFim.Checked then FmEntidadesNew.EdLBairro.Text      := FBairroF;
        if CkCidade.Checked then FmEntidadesNew.EdLCidade.Text         := FLocalTxt;
        if CkUF.Checked then FmEntidade2.EdLUF.Text                    := FUF_Txt;
        if CkBrasil.Checked then FmEntidadesNew.EdLPais.Text           := FPais;
      end;
      { N�o usar! implementar as vari�veis mais abaixo no FmEntidade2!
      7:
      begin
        if CkCEP.Checked then FmEntidade2.EdPCEP.Text               := FCEP;
        if CkTipoLograd.Checked then FmEntidade2.CBPLograd.KeyValue := FLogrTip;
        if CkLogradouro.Checked then FmEntidade2.EdPRua.Text        := FLogrTxt;
        if CkBairroIni.Checked then FmEntidade2.EdPBairro.Text      := FBairroI;
        if CkBairroFim.Checked then FmEntidade2.EdPBairro.Text      := FBairroF;
        if CkCidade.Checked then FmEntidade2.EdPCidade.Text         := FLocalTxt;
        if CkUF.Checked then FmEntidade2.EdPUF.Text                 := FUF_Txt;
        if CkBrasil.Checked then FmEntidade2.EdPPais.Text           := FPais;
      end;
      }
      else
      begin
        if (FEdCEP <> nil) and (CkCEP.Checked) then
          FEdCEP.Text := FCEP;
        if (FEdLograd <> nil) and (CkTipoLograd.Checked) then
          FEdLograd.ValueVariant := FLogrTip;
        if (FCBLograd <> nil) and (CkTipoLograd.Checked) then
          FCBLograd.KeyValue := FLogrTip;
        if (FEdRua <> nil) and (CkLogradouro.Checked) then
          FEdRua.Text := FLogrTxt;
        if (FEdBairro <> nil) and (CkBairroIni.Checked) then
          FEdBairro.Text := FBairroI;
        if (FEdBairro <> nil) and (CkBairroFim.Checked) then
          FEdBairro.Text := FBairroF;
        if (FEdCidade <> nil) and (CkCidade.Checked) then
          FEdCidade.Text := FLocalTxt;
        if (FEdUF <> nil) and (CkUF.Checked) then
          FEdUF.Text := FUF_Txt;
        if (FEdPais <> nil) and (CkBrasil.Checked) then
          FEdPais.Text := FPais;

        if ((FEdCodMunici <> nil) or (FCBCodMunici <> nil)
        or (FEdCodiPais <> nil) or (FCBCodiPais <> nil))
        and (FLocalCod <> 0) then
          DefineCodMunici_CodiPais(FLocalCod, FEdCodMunici, FEdCodiPais,
            FCBCodMunici, FCBCodiPais);
      end;
    end;
  //end;
  Close;
end;

procedure TFmEntiCEP.CkBairroIniClick(Sender: TObject);
begin
  if CkBairroIni.Checked then CkBairroFim.Checked := False;
end;

procedure TFmEntiCEP.CkBairroFimClick(Sender: TObject);
begin
  if CkBairroFim.Checked then CkBairroIni.Checked := False;
end;

procedure TFmEntiCEP.BtTudoClick(Sender: TObject);
begin
  CkCEP.Checked        := True;
  CkTipoLograd.Checked := True;
  CkLogradouro.Checked := True;
  CkBairroIni.Checked  := True;
  //CkBairroFim.Checked := True; s� pode um bairro
  CkCidade.Checked     := True;
  CkUF.Checked         := True;
  CkBrasil.Checked     := True;
end;

procedure TFmEntiCEP.BtNenhumClick(Sender: TObject);
begin
  CkCEP.Checked        := False;
  CkTipoLograd.Checked := False;
  CkLogradouro.Checked := False;
  CkBairroIni.Checked  := False;
  CkBairroFim.Checked  := False;
  CkCidade.Checked     := False;
  CkUF.Checked         := False;
  CkBrasil.Checked     := False;
end;

procedure TFmEntiCEP.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiCEP.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiCEP.QrCidadesCalcFields(DataSet: TDataSet);
begin
  QrCidadesCEP_DECRYPT.Value := Geral.FormataCEP_TT(
    U_CEP.DeCryptCEP(QrCidadesCEP.Value, DCP_twofish1));
end;

procedure TFmEntiCEP.QrCPCCalcFields(DataSet: TDataSet);
begin
  QrCPCCEP_DECRYPT.Value := Geral.FormataCEP_TT(
    U_CEP.DeCryptCEP(QrCPCCEP.Value, DCP_twofish1));
end;

procedure TFmEntiCEP.QrGRUCalcFields(DataSet: TDataSet);
begin
  QrGRUCEP_DECRYPT.Value := Geral.FormataCEP_TT(
    U_CEP.DeCryptCEP(QrGRUCEP.Value, DCP_twofish1));
end;

procedure TFmEntiCEP.QrCEPCalcFields(DataSet: TDataSet);
begin
  QrCEPCEP_DECRYPT.Value := Geral.FormataCEP_TT(
    U_CEP.DeCryptCEP(QrCEPCEP.Value, DCP_twofish1));
end;

procedure TFmEntiCEP.QrUOPCalcFields(DataSet: TDataSet);
begin
  QrUOPCEP_DECRYPT.Value := Geral.FormataCEP_TT(
    U_CEP.DeCryptCEP(QrUOPCEP.Value, DCP_twofish1));
end;

procedure TFmEntiCEP.TabControl1Change(Sender: TObject);
var
  Txt: String;
begin
  ST1.Visible := True;
  if TabControl1.Tabs[TabControl1.TabIndex] = TbGRU then
  begin
    case QrGRU.RecordCount of
      0: Txt := 'N�o foi encontrado nenhum grande usu�rio';
      1: Txt := 'Foi encontrado um grande usu�rio';
      else Txt := 'Foram encontrados ' + IntToStr(QrGRU.RecordCount) + ' grandes usu�rios';
    end;
    ST1.Caption := sLineBreak + Txt;
    DBGrid3.Visible := True;
  end
  else
  if TabControl1.Tabs[TabControl1.TabIndex]  = TbLog then
  begin
    case QrCEP.RecordCount of
      0: Txt := 'N�o foi encontrado nenhum logradouro';
      1: Txt := 'Foi encontrado um logradouro';
      else Txt := 'Foram encontrados ' + IntToStr(QrCEP.RecordCount) + ' logradouros';
    end;
    ST1.Caption := sLineBreak + Txt;
    DBGrid1.Visible := Panel1.Visible;
    DBGrid2.Visible := Panel2.Visible;
  end
  else
  if TabControl1.Tabs[TabControl1.TabIndex]  = TbUOP then
  begin
    case QrUOP.RecordCount of
      0: Txt := 'N�o foi encontrado nenhuma unidade operacional';
      1: Txt := 'Foi encontrada uma unidade operacional';
      else Txt := 'Foram encontradas ' + IntToStr(QrUOP.RecordCount) + ' unidades operacionais';
    end;
    ST1.Caption := sLineBreak + Txt;
    DBGrid4.Visible := True;
  end
  else
  if TabControl1.Tabs[TabControl1.TabIndex]  = TbCPC then
  begin
    case QrCPC.RecordCount of
      0: Txt := 'N�o foi encontrado nenhuma caixa postal comunit�ria';
      1: Txt := 'Foi encontrada uma caixa postal comunit�ria';
      else Txt := 'Foram encontradas ' + IntToStr(QrCPC.RecordCount) + ' caixas postais comunit�rias';
    end;
    ST1.Caption := sLineBreak + Txt;
    DBGrid5.Visible := True;
  end;
end;

procedure TFmEntiCEP.TabControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  DBGrid1.Visible := False;
  DBGrid2.Visible := False;
  DBGrid3.Visible := False;
  DBGrid4.Visible := False;
  DBGrid5.Visible := False;
end;

procedure TFmEntiCEP.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if Painel1.Visible and QrTipo.Active then CBTipo.SetFocus;
end;

procedure TFmEntiCEP.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiCEP.PesquisaCaixasPostaisComunitarias();
begin
  QrCPC.Close;
  QrCPC.SQL.Clear;
  //
  QrCPC.SQL.Add('SELECT LOC.LOC_NO, CPC.*');
  QrCPC.SQL.Add('FROM log_cpc CPC, LOG_LOCALIDADE LOC');
  QrCPC.SQL.Add('WHERE LOC.LOC_NU_SEQUENCIAL=CPC.LOC_NU_SEQUENCIAL');
  if Length(F_CEP_B)  = 8 then
    QrCPC.SQL.Add('AND CPC.CEP = ''' + F_CEP_C + '''')
  else
    QrCPC.SQL.Add('AND CPC.CEP LIKE ''' + F_CEP_C + '%''');
  UMyMod.AbreBDEQuery(QrCPC);
  //
  if QrCPC.RecordCount > 0 then
  begin
    TabControl1.Tabs.Add(TbCPC);
    TabControl1.Visible := True;
    TabControl1Change(Self);
  end;
end;

procedure TFmEntiCEP.PesquisaGrandesUsuarios();
begin
  Screen.Cursor := crHourGlass;
  QrGRU.Close;
  QrGRU.SQL.Clear;
  QrGRU.SQL.Add('SELECT LOC.LOC_NO, BAI.BAI_NO, GRU.*');
  QrGRU.SQL.Add('FROM log_grande_usuario GRU,');
  QrGRU.SQL.Add('  LOG_BAIRRO BAI, LOG_LOCALIDADE LOC');
  QrGRU.SQL.Add('WHERE BAI.BAI_NU_SEQUENCIAL=GRU.BAI_NU_SEQUENCIAL');
  QrGRU.SQL.Add('AND LOC.LOC_NU_SEQUENCIAL=GRU.LOC_NU_SEQUENCIAL');
  QrGRU.SQL.Add('AND GRU.CEP LIKE ''' + F_CEP_C + '%''');
  UMyMod.AbreBDEQuery(QrGRU);
  QrGRU.First;
  if QrGRU.RecordCount > 0 then
  begin
    TabControl1.Visible := True;
    TabControl1.Tabs.Add(TbGRU);
    if TabControl1.Tabs.Count = 1 then
    begin
      DBGrid3.Visible := True;
      TabControl1Change(Self);
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmEntiCEP.PesquisaLogradouros();
begin
  QrCEP.Close;
  QrCEP.SQL.Clear;
  //
  QrCEP.SQL.Add('SELECT lo.*');
  QrCEP.SQL.Add('FROM log_logradouro lo');
  if Length(F_CEP_B)  = 8 then
    QrCEP.SQL.Add('WHERE lo.CEP = ''' + F_CEP_C + '''')
  else
    QrCEP.SQL.Add('WHERE lo.CEP LIKE ''' + F_CEP_C + '%''');
  UMyMod.AbreBDEQuery(QrCEP);
  //
  if QrCEP.RecordCount > 0 then
  begin
    TabControl1.Tabs.Add(TbLog);
    DBGrid1.Visible := Panel1.Visible;
    DBGrid2.Visible := Panel2.Visible;
    //
    TabControl1.Visible := True;
    TabControl1Change(Self);
  end;
end;

function TFmEntiCEP.PesquisaTipoLogradouro(const LogrC, LogrN: String;
  var LogrCod: Integer; var LogrTipTxt, LogrNome, NumeTxt: String;
  const Tipo: Integer): Boolean;
var
  n, k: integer;
  Aux: String;
begin
  NumeTxt := '';
  n := pos(' ', LogrC);
  LogrTipTxt := Copy(LogrC, 1, n -1);
  if QrListaLograd.Locate('Nome', LogrTipTxt, [loCaseInsensitive]) then
  begin
    LogrCod  := QrListaLogradCodigo.Value;
    case Tipo of
      1: LogrNome := LogrN;
      2:
      begin
        Aux := Copy(LogrC, n+1);
        k := pos(', ', Aux);
        if k = 0 then
          LogrNome := Aux
        else begin
          LogrNome := Copy(Aux, 1, k-1);
          NumeTxt  := Copy(Aux, k+2);
        end;
      end;
    end;
  end else begin
    LogrCod := 0;
    LogrNome := LogrC;
  end;
  LogrNome := Uppercase(LogrNome);
  Result := True;
end;

procedure TFmEntiCEP.PesquisaUnidadesOperacionais;
begin
  QrUOP.Close;
  QrUOP.SQL.Clear;
  //
  QrUOP.SQL.Add('SELECT LOC.LOC_NO, BAI.BAI_NO, UOP.*');
  QrUOP.SQL.Add('FROM log_unid_oper UOP,');
  QrUOP.SQL.Add('  LOG_BAIRRO BAI, LOG_LOCALIDADE LOC');
  QrUOP.SQL.Add('WHERE BAI.BAI_NU_SEQUENCIAL=UOP.BAI_NU_SEQUENCIAL');
  QrUOP.SQL.Add('AND LOC.LOC_NU_SEQUENCIAL=UOP.LOC_NU_SEQUENCIAL');
  if Length(F_CEP_B)  = 8 then
    QrUOP.SQL.Add('AND UOP.CEP = ''' + F_CEP_C + '''')
  else
    QrUOP.SQL.Add('AND UOP.CEP LIKE ''' + F_CEP_C + '%''');
  UMyMod.AbreBDEQuery(QrUOP);
  //
  if QrUOP.RecordCount > 0 then
  begin
    TabControl1.Tabs.Add(TbUOP);
    TabControl1.Visible := True;
    TabControl1Change(Self);
  end;
end;

function TFmEntiCEP.TamCrypt(Texto: String): Integer;
begin
  Result := 0;
  case Length(Texto) of
    3: Result := 4;
    4: Result := 6;
    5: Result := 7;
    6: Result := 8;
    7: Result := 10;
    8: Result := 12
  end;
end;

procedure TFmEntiCEP.OcultaDados();
begin
  TabControl1.Visible  := False;
  TabControl1.TabIndex := 0;
  TabControl1.Tabs.Clear;
  //
  DBGrid1.Visible := False;
  DBGrid2.Visible := False;
  DBGrid3.Visible := False;
  DBGrid4.Visible := False;
  DBGrid5.Visible := False;
end;

function TFmEntiCEP.DefineCodMunici_CodiPais(LocalCod: Integer;
  EdCodMunici, EdCodiPais: TdmkEdit; CBCodMunici,
  CBCodiPais: TdmkDBLookupComboBox): Boolean;
var
  CodMunici: Integer;
begin
  CodMunici := 0;
  if LocalCod > 0 then
  begin
    if USQLDB.TabelaExiste('dtb_munici', (*Dmod.QrMas,*) DModG.AllID_DB) then
    begin
      DModG.QrAllAux.SQL.Clear;
      DModG.QrAllAux.SQL.Add('SELECT Codigo FROM dtb_munici');
      DModG.QrAllAux.SQL.Add('WHERE DNE=' + Geral.FF0(LocalCod));
      UnDmkDAC_PF.AbreQuery(DModG.QrAllAux, DModG.AllID_DB);
      CodMunici := DModG.QrAllAux.FieldByName('Codigo').AsInteger;
    end;
  end;
  if CodMunici = 0 then
    CodMunici := U_CEP.ObtemCodigoDTBMunici(FLocalTxt, FUF_Txt);
  if CodMunici > 0 then
  begin
    if EdCodMunici <> nil then
      EdCodMunici.ValueVariant := CodMunici;
    if CBCodMunici <> nil then
      CBCodMunici.KeyValue := CodMunici;
    if EdCodiPais <> nil then
      EdCodiPais.ValueVariant := 1058;
    if CBCodiPais <> nil then
      CBCodiPais.KeyValue := 1058;
    //
  end else
  begin
    if EdCodMunici.ValueVariant > 0 then
    begin
      Geral.MB_Aviso('N�o foi poss�vel definir o c�digo da cidade!' +
      sLineBreak + 'Verifique manualmente se ele est� correto!');
    end;
  end;
  Result := CodMunici <> 0;
end;

procedure TFmEntiCEP.DefineDadosPesquisaCEP();
begin
  if (QrGRU.State <> dsInactive) and (QrGRU.RecordCount > 0) then
  begin
    FCEP      := Geral.FormataCEP_TT(QrGRUCEP_DECRYPT.Value);
    PesquisaTipoLogradouro(QrGRUGRU_ENDERECO.Value, QrGRUGRU_ENDERECO.Value,
      FLogrTip, FLogrTipTxt, FLogrTxt, FNumeTxt, 2);
    FLogrCod  := QrGRULOG_NU_SEQUENCIAL.Value;
    FBairICod := QrGRUBAI_NU_SEQUENCIAL.Value;
    FBairroI  := Geral.Maiusculas(QrGRUBAI_NO.Value, True);
    FBairroF  := Geral.Maiusculas(QrGRUBAI_NO.Value, True);
    FLocalTxt := Geral.Maiusculas(QrGRULOC_NO.Value, True);
    FLocalCod := QrGRULOC_NU_SEQUENCIAL.Value;
    FPais     := Geral.Maiusculas(CkBrasil.Caption, True);
    FUF_Cod   := Geral.GetCodigoUF_da_SiglaUF(QrUFUFE_SG.Value);
    FUF_Txt   := QrUFUFE_SG.Value;
  end else
  if (QrCEP.State <> dsInactive) and (QrCEP.RecordCount > 0) then
  begin
    FCEP      := Geral.FormataCEP_TT(QrCEPCEP_DECRYPT.Value);
    PesquisaTipoLogradouro(QrCEPLOG_NOME.Value, QrCEPLOG_NO.Value,
      FLogrTip, FLogrTipTxt, FLogrTxt, FNumeTxt, 1);
    FLogrCod  := QrCEPLOG_NU_SEQUENCIAL.Value;
    FBairICod := QrCEPBAI_NU_SEQUENCIAL_INI.Value;
    FBairroI  := Geral.Maiusculas(QrCEPNOMEBAIRROINI.Value, True);
    FBairroF  := Geral.Maiusculas(QrCEPNOMEBAIRROFIM.Value, True);
    FLocalTxt := Geral.Maiusculas(QrCEPcidade.Value, True);
    FLocalCod := QrCEPLOC_NU_SEQUENCIAL.Value;
    FPais     := Geral.Maiusculas(CkBrasil.Caption, True);
    FUF_Cod   := QrCEPUF.Value;
    FUF_Txt   := Geral.GetSiglaUF_do_CodigoUF(QrCEPUF.Value);
  end else
  if (QrUOP.State <> dsInactive) and (QrUOP.RecordCount > 0) then
  begin
    FCEP      := Geral.FormataCEP_TT(QrUOPCEP_DECRYPT.Value);
    PesquisaTipoLogradouro(QrUOPUOP_ENDERECO.Value, QrUOPUOP_ENDERECO.Value,
      FLogrTip, FLogrTipTxt, FLogrTxt, FNumeTxt, 2);
    FLogrCod  := QrUOPLOG_NU_SEQUENCIAL.Value;
    FBairICod := QrUOPBAI_NU_SEQUENCIAL.Value;
    FBairroI  := Geral.Maiusculas(QrUOPBAI_NO.Value, True);
    FBairroF  := Geral.Maiusculas(QrUOPBAI_NO.Value, True);
    FLocalTxt := Geral.Maiusculas(QrUOPLOC_NO.Value, True);
    FLocalCod := QrUOPLOC_NU_SEQUENCIAL.Value;
    FPais     := Geral.Maiusculas(CkBrasil.Caption, True);
    FUF_Cod   := Geral.GetCodigoUF_da_SiglaUF(QrUFUFE_SG.Value);
    FUF_Txt   := QrUFUFE_SG.Value;
  end else
  if (QrCPC.State <> dsInactive) and (QrCPC.RecordCount > 0) then
  begin
    FCEP      := Geral.FormataCEP_TT(QrCPCCEP_DECRYPT.Value);
    PesquisaTipoLogradouro(QrCPCCPC_ENDERECO.Value, QrCPCCPC_ENDERECO.Value,
      FLogrTip, FLogrTipTxt, FLogrTxt, FNumeTxt, 2);
    FLogrCod  := 0;//QrCPCLOG_NU_SEQUENCIAL.Value;
    FBairICod := 0;
    FBairroI  := '';
    FBairroF  := '';
    FLocalTxt := Geral.Maiusculas(QrCPCLOC_NO.Value, True);
    FLocalCod := QrCPCLOC_NU_SEQUENCIAL.Value;
    FPais     := Geral.Maiusculas(CkBrasil.Caption, True);
    FUF_Cod   := Geral.GetCodigoUF_da_SiglaUF(QrUFUFE_SG.Value);
    FUF_Txt   := QrUFUFE_SG.Value;
  end;
end;

end.

