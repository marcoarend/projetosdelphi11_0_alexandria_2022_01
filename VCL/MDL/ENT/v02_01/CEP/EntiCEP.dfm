object FmEntiCEP: TFmEntiCEP
  Left = 344
  Top = 171
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSizeToolWin
  Caption = 'ENT-GEREN-004 :: Localiza'#231#227'o de Endere'#231'o ou CEP'
  ClientHeight = 905
  ClientWidth = 1077
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 1077
    Height = 705
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Painel1: TPanel
      Left = 0
      Top = 0
      Width = 1077
      Height = 119
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 10
        Top = 10
        Width = 21
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'UF:'
      end
      object Label2: TLabel
        Left = 84
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cidade:'
      end
      object Label4: TLabel
        Left = 10
        Top = 64
        Width = 123
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tipo de Logradouro:'
      end
      object Label5: TLabel
        Left = 167
        Top = 64
        Width = 208
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Parte ou todo nome do logradouro:'
      end
      object LaCEP: TLabel
        Left = 758
        Top = 79
        Width = 33
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'CEP: '
        Visible = False
      end
      object LaUnico: TLabel
        Left = 635
        Top = 34
        Width = 191
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Localidade com CEP '#250'nico!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object Label6: TLabel
        Left = 970
        Top = 34
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Codigo'
        FocusControl = DBLookupComboBox1
        Visible = False
      end
      object CBUF: TDBLookupComboBox
        Left = 10
        Top = 30
        Width = 70
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'UFE_SG'
        ListField = 'UFE_SG'
        ListSource = DsUF
        TabOrder = 0
        OnClick = CBUFClick
      end
      object CBCidade: TDBLookupComboBox
        Left = 84
        Top = 30
        Width = 538
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'LOC_NU_SEQUENCIAL'
        ListField = 'LOC_NO'
        ListSource = DsCidades
        TabOrder = 1
        OnClick = CBCidadeClick
      end
      object CBTipo: TDBLookupComboBox
        Left = 10
        Top = 84
        Width = 154
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'TipoLogradouro'
        ListField = 'TipoLogradouro'
        ListSource = DsTipo
        TabOrder = 2
        OnClick = CBTipoClick
      end
      object EdRua: TEdit
        Left = 167
        Top = 84
        Width = 455
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        TabOrder = 3
        OnChange = EdRuaChange
      end
      object BtLocaliza1: TBitBtn
        Tag = 22
        Left = 635
        Top = 60
        Width = 111
        Height = 50
        Hint = 'Pesquisa'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Pesquisa'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtLocaliza1Click
      end
      object EdCEP: TEdit
        Left = 801
        Top = 64
        Width = 164
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -28
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        OnChange = EdCEPChange
      end
      object DBLookupComboBox1: TDBLookupComboBox
        Left = 970
        Top = 54
        Width = 82
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Codigo'
        DataSource = DsTipo
        TabOrder = 6
        Visible = False
      end
    end
    object Painel2: TPanel
      Left = 0
      Top = 119
      Width = 1077
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label3: TLabel
        Left = 5
        Top = 5
        Width = 189
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pelo menos 3 n'#250'meros do CEP:'
      end
      object EdCEPLoc: TEdit
        Left = 5
        Top = 25
        Width = 183
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 0
        OnEnter = EdCEPLocEnter
      end
      object BtLocaliza2: TBitBtn
        Tag = 22
        Left = 197
        Top = 4
        Width = 111
        Height = 49
        Hint = 'Pesquisa'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtLocaliza2Click
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 178
      Width = 1077
      Height = 495
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object TabControl1: TTabControl
        Left = 1
        Top = 1
        Width = 1075
        Height = 493
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        OnChange = TabControl1Change
        OnChanging = TabControl1Changing
        object DBGrid1: TDBGrid
          Left = 4
          Top = 222
          Width = 1067
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          DataSource = DsCEP
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          Columns = <
            item
              Expanded = False
              FieldName = 'LOG_NOME'
              Title.Caption = 'Logradouro'
              Width = 275
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CEP_DECRYPT'
              Title.Alignment = taCenter
              Title.Caption = 'CEP'
              Width = 77
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBAIRROINI'
              Title.Caption = 'Bairro Inicial'
              Width = 236
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBAIRROFIM'
              Title.Caption = 'Bairro Final'
              Width = 236
              Visible = True
            end>
        end
        object DBGrid2: TDBGrid
          Left = 4
          Top = 271
          Width = 1067
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          DataSource = DsCEP
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          Columns = <
            item
              Expanded = False
              FieldName = 'LOG_NOME'
              Title.Caption = 'Logradouro'
              Width = 275
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBAIRROINI'
              Title.Caption = 'Bairro Inicial'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBAIRROFIM'
              Title.Caption = 'Bairro Final'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'cidade'
              Title.Caption = 'Cidade'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CEP_DECRYPT'
              Title.Alignment = taCenter
              Title.Caption = 'CEP'
              Width = 77
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UFE_SG'
              Title.Caption = 'UF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOC_NU_SEQUENCIAL'
              Title.Caption = 'C'#243'digo DNE'
              Visible = True
            end>
        end
        object ST1: TStaticText
          Left = 4
          Top = 6
          Width = 1067
          Height = 48
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = #13#10'----'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          Visible = False
        end
        object DBGrid3: TDBGrid
          Left = 4
          Top = 320
          Width = 1067
          Height = 79
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          DataSource = DsGRU
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          Columns = <
            item
              Expanded = False
              FieldName = 'GRU_NO'
              Title.Caption = 'Nome'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GRU_ENDERECO'
              Title.Caption = 'Endere'#231'o'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CEP_DECRYPT'
              Title.Caption = 'CEP'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOC_NO'
              Title.Caption = 'Localidade'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BAI_NO'
              Title.Caption = 'Bairro'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UFE_SG'
              Title.Caption = 'UF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOG_NU_SEQUENCIAL'
              Title.Caption = 'Sequencial'
              Visible = True
            end>
        end
        object DBGrid4: TDBGrid
          Left = 4
          Top = 399
          Width = 1067
          Height = 79
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          DataSource = DsUOP
          TabOrder = 4
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          Columns = <
            item
              Expanded = False
              FieldName = 'UOP_NO'
              Title.Caption = 'Nome'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UOP_ENDERECO'
              Title.Caption = 'Endere'#231'o'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CEP_DECRYPT'
              Title.Caption = 'CEP'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOC_NO'
              Title.Caption = 'Localidade'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BAI_NO'
              Title.Caption = 'Bairro'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UFE_SG'
              Title.Caption = 'UF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOG_NU_SEQUENCIAL'
              Title.Caption = 'Sequencial'
              Visible = True
            end>
        end
        object DBGrid5: TDBGrid
          Left = 4
          Top = 478
          Width = 1067
          Height = 79
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          DataSource = DsCPC
          TabOrder = 5
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          Columns = <
            item
              Expanded = False
              FieldName = 'CPC_NO'
              Title.Caption = 'Nome'
              Width = 204
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPC_ENDERECO'
              Title.Caption = 'Endere'#231'o'
              Width = 234
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CEP_DECRYPT'
              Title.Caption = 'CEP'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOC_NO'
              Title.Caption = 'Localidade'
              Width = 224
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UFE_SG'
              Title.Caption = 'UF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPC_NU_SEQUENCIAL'
              Title.Caption = 'Sequencial'
              Visible = True
            end>
        end
        object DBGrid6: TDBGrid
          Left = 4
          Top = 381
          Width = 1067
          Height = 108
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          DataSource = DsCidades
          TabOrder = 6
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object DBGrid7: TDBGrid
          Left = 4
          Top = 54
          Width = 1067
          Height = 168
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          DataSource = DsCEP
          TabOrder = 7
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 673
      Width = 1077
      Height = 32
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object CkCEP: TCheckBox
        Left = 10
        Top = 5
        Width = 50
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'CEP'
        TabOrder = 0
      end
      object CkLogradouro: TCheckBox
        Left = 197
        Top = 5
        Width = 90
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Logradouro'
        TabOrder = 2
      end
      object CkBairroIni: TCheckBox
        Left = 300
        Top = 5
        Width = 95
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Bairro inicial'
        TabOrder = 3
        OnClick = CkBairroIniClick
      end
      object CkBairroFim: TCheckBox
        Left = 399
        Top = 5
        Width = 90
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Bairro final'
        TabOrder = 4
        OnClick = CkBairroFimClick
      end
      object CkCidade: TCheckBox
        Left = 492
        Top = 5
        Width = 70
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cidade'
        TabOrder = 5
      end
      object CkUF: TCheckBox
        Left = 566
        Top = 5
        Width = 46
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'UF'
        TabOrder = 6
      end
      object CkBrasil: TCheckBox
        Left = 615
        Top = 5
        Width = 56
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Brasil'
        TabOrder = 7
      end
      object CkTipoLograd: TCheckBox
        Left = 74
        Top = 5
        Width = 119
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tipo logardouro'
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1077
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1018
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 959
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 478
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Localiza'#231#227'o de Endere'#231'o ou CEP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 478
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Localiza'#231#227'o de Endere'#231'o ou CEP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 478
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Localiza'#231#227'o de Endere'#231'o ou CEP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 764
    Width = 1077
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1073
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 818
    Width = 1077
    Height = 87
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 898
      Top = 18
      Width = 177
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 18
      Width = 896
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 5
        Top = 4
        Width = 148
        Height = 49
        Cursor = crHandPoint
        Hint = 'Exporta itens marcados'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BtTudo: TBitBtn
        Tag = 127
        Left = 337
        Top = 5
        Width = 111
        Height = 49
        Hint = 'Marca todos itens'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Tudo'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTudoClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 453
        Top = 5
        Width = 111
        Height = 49
        Hint = 'Desmarca todos itens'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNenhumClick
      end
    end
  end
  object DsLOG_FAIXA_BAIRRO: TDataSource
    Left = 740
    Top = 164
  end
  object DsLOG_FAIXA_CPC: TDataSource
    Left = 740
    Top = 192
  end
  object DsLOG_FAIXA_LOCALIDADE: TDataSource
    Left = 740
    Top = 220
  end
  object DsLOG_FAIXA_UF: TDataSource
    Left = 740
    Top = 248
  end
  object DsLOG_FAIXA_UOP: TDataSource
    Left = 740
    Top = 276
  end
  object DsLOG_GRANDE_USUARIO: TDataSource
    Left = 740
    Top = 304
  end
  object DsLOG_LOCALIDADE: TDataSource
    Left = 740
    Top = 332
  end
  object DsLOG_LOGRADOURO: TDataSource
    Left = 740
    Top = 360
  end
  object DsLOG_TIPO_LOGR: TDataSource
    Left = 740
    Top = 388
  end
  object DsLOG_UNID_OPER: TDataSource
    Left = 740
    Top = 416
  end
  object DsCEP: TDataSource
    Left = 120
    Top = 196
  end
  object DsLOG_BAIRRO: TDataSource
    Left = 740
    Top = 80
  end
  object DsLOG_CEP: TDataSource
    Left = 740
    Top = 108
  end
  object DsLOG_CPC: TDataSource
    Left = 740
    Top = 136
  end
  object DsUF: TDataSource
    Left = 36
    Top = 88
  end
  object DsCidades: TDataSource
    Left = 332
    Top = 64
  end
  object DsTipo: TDataSource
    Left = 472
    Top = 64
  end
  object QrUF2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ufs')
    Left = 64
    Top = 196
    object QrUF2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ufs.Codigo'
      Required = True
    end
    object QrUF2Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
  end
  object QrUFPadrao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome '
      'FROM ufs'
      'WHERE Codigo=:P0')
    Left = 320
    Top = 153
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUFPadraoNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 2
    end
  end
  object QrListaLograd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM listalograd')
    Left = 348
    Top = 153
    object QrListaLogradCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaLogradNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 10
    end
    object QrListaLogradLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrListaLogradDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrListaLogradDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrListaLogradUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrListaLogradUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DCP_twofish1: TDCP_twofish
    Id = 6
    Algorithm = 'Twofish'
    MaxKeySize = 256
    BlockSize = 128
    Left = 36
    Top = 196
  end
  object DsGRU: TDataSource
    Left = 176
    Top = 196
  end
  object DsUOP: TDataSource
    Left = 232
    Top = 196
  end
  object DsCPC: TDataSource
    Left = 288
    Top = 196
  end
  object mySQLDirectQuery1: TMySQLDirectQuery
    Database = DModG.AllID_DB
    Left = 28
    Top = 208
  end
end
