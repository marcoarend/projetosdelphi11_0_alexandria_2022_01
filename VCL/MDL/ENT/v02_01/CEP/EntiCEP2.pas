unit EntiCEP2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, OleCtrls, SHDocVw, ActiveX, JPEG, ShellAPI, MSHTML, UnDmkEnums;

type
  TFmEntiCEP2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    ProgressBar1: TProgressBar;
    RGTipo: TRadioGroup;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    WebBrowser: TWebBrowser;
    MeTexto: TMemo;
    MeEndereco: TMemo;
    BtImporta: TBitBtn;
    SbReabre: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure WebBrowserProgressChange(ASender: TObject; Progress,
      ProgressMax: Integer);
    procedure FormCreate(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure SbReabreClick(Sender: TObject);
    procedure WebBrowserDocumentComplete(ASender: TObject;
      const pDisp: IDispatch; const URL: OleVariant);
  private
    { Private declarations }
    procedure LocalizaEndereco();
    procedure LocalizaCEP();
  public
    { Public declarations }
    FTipoLograd: Integer;
    FLogradouro, FBairro, FCidade, FUF, FCEP: String;
  end;

  var
  FmEntiCEP2: TFmEntiCEP2;

implementation

uses UnMyObjects, Module, ModuleGeral;

{$R *.DFM}

procedure TFmEntiCEP2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiCEP2.BtImportaClick(Sender: TObject);
var
  Pos1, Pos2, Pos3: Integer;
  Txt1, Txt2, Txt3: String;
  WB: IHTMLElement;
begin
  MeTexto.Lines.Clear;
  MeEndereco.Lines.Clear;
  try
    Screen.Cursor := crHourGlass;
    if Assigned(WebBrowser.Document) then
    begin
      WB := (WebBrowser.Document as IHTMLDocument2).body;
      while WB.parentElement <> nil do
      begin
        WB := WB.parentElement;
      end;
      MeTexto.Text := WB.outerHTML;
      //
      Pos1 := Pos('javascript:detalharcep', LowerCase(MeTexto.Text));
      if Pos1 > 0 then
      begin
        MeEndereco.Text := Copy(MeTexto.Text, Pos1);
        //
        Pos2 := Pos('<td', LowerCase(MeEndereco.Text));
        Pos3 := Pos('</table>', LowerCase(MeEndereco.Text));
        //
        if (Pos2 > 0) and (Pos3 > 0) then
        begin
          MeEndereco.Text := Copy(MeEndereco.Text, Pos2, Pos3 - Pos2);
          MeEndereco.Text := Geral.RemoveTagsHTML(MeEndereco.Text);
          //
          FLogradouro := MeEndereco.Lines.Strings[0];
          FBairro     := MeEndereco.Lines.Strings[1];
          FCidade     := MeEndereco.Lines.Strings[2];
          FUF         := MeEndereco.Lines.Strings[3];
          FCEP        := MeEndereco.Lines.Strings[4];
          //
          Geral.SeparaLogradouro(0, FLogradouro, Txt1, Txt2, Txt3, nil, nil);
          //
          FLogradouro := Txt3;
          FTipoLograd := Geral.IMV(Txt2);
        end;
      end;
    end;
  finally
    {
    Geral.MB_Info('Logradouro: ' + Geral.FF0(FTipoLograd) + ' ' + FLogradouro
      + sLineBreak + 'Bairro: ' + FBairro + sLineBreak + 'Cidade: ' + FCidade + sLineBreak +
      'UF: ' + FUF + sLineBreak + 'CEP: ' + FCEP);
    }
    Screen.Cursor := crDefault;
    Close;
  end;
end;

procedure TFmEntiCEP2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmEntiCEP2.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  TabSheet2.TabVisible         := False;
  LocalizaEndereco();
end;

procedure TFmEntiCEP2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiCEP2.LocalizaCEP;
var
  Url: String;
begin
  Url  := 'http://www.dermatek.net.br/plugins/busca_cep.htm';
  //
  WebBrowser.Navigate(Url);
end;

procedure TFmEntiCEP2.LocalizaEndereco();
var
  Url: String;
begin
  Url  := 'http://www.dermatek.net.br/plugins/busca_por_cep.htm';
  //
  WebBrowser.Navigate(Url);
end;

procedure TFmEntiCEP2.RGTipoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  case RGTipo.ItemIndex of
    0: LocalizaEndereco();
    1: LocalizaCEP();
  end;
  RGTipo.ItemIndex := -1;
end;

procedure TFmEntiCEP2.SbReabreClick(Sender: TObject);
begin
  WebBrowser.Refresh;
end;

procedure TFmEntiCEP2.WebBrowserDocumentComplete(ASender: TObject;
  const pDisp: IDispatch; const URL: OleVariant);
begin
  ProgressBar1.Position := 0;
end;

procedure TFmEntiCEP2.WebBrowserProgressChange(ASender: TObject; Progress,
  ProgressMax: Integer);
begin
  if (Progress>=1) and (ProgressMax>1) then
  begin
    //Ele tira uma valor percentual para colocar no Progressbar
    ProgressBar1.Position := Round((Progress * 100) div ProgressMax);
    ProgressBar1.Visible  := True;
  end else
  begin
    ProgressBar1.Position := 1;
    ProgressBar1.Visible  := False;
  end;
end;

end.
