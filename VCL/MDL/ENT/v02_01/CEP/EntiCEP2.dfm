object FmEntiCEP2: TFmEntiCEP2
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-004 :: Localiza'#231#227'o de endere'#231'o ou CEP'
  ClientHeight = 772
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 402
        Height = 32
        Caption = 'Localiza'#231#227'o de endere'#231'o ou CEP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 402
        Height = 32
        Caption = 'Localiza'#231#227'o de endere'#231'o ou CEP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 402
        Height = 32
        Caption = 'Localiza'#231#227'o de endere'#231'o ou CEP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 610
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 610
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 610
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 55
          Align = alTop
          TabOrder = 0
          object RGTipo: TRadioGroup
            Left = 7
            Top = 7
            Width = 220
            Height = 40
            Caption = 'RGTipo'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Busca por CEP'
              'Busca CEP')
            TabOrder = 0
            OnClick = RGTipoClick
          end
          object SbReabre: TBitBtn
            Tag = 18
            Left = 230
            Top = 7
            Width = 40
            Height = 40
            NumGlyphs = 2
            TabOrder = 1
            OnClick = SbReabreClick
          end
        end
        object ProgressBar1: TProgressBar
          Left = 2
          Top = 591
          Width = 1004
          Height = 17
          Align = alBottom
          TabOrder = 1
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 70
          Width = 1004
          Height = 521
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 2
          object TabSheet1: TTabSheet
            Caption = 'HTML'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object WebBrowser: TWebBrowser
              Left = 0
              Top = 0
              Width = 996
              Height = 493
              Align = alClient
              TabOrder = 0
              OnProgressChange = WebBrowserProgressChange
              OnDocumentComplete = WebBrowserDocumentComplete
              ExplicitWidth = 701
              ExplicitHeight = 498
              ControlData = {
                4C000000F1660000F43200000000000000000000000000000000000000000000
                000000004C000000000000000000000001000000E0D057007335CF11AE690800
                2B2E126208000000000000004C0000000114020000000000C000000000000046
                8000000000000000000000000000000000000000000000000000000000000000
                00000000000000000100000000000000000000000000000000000000}
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Texto plano'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeTexto: TMemo
              Left = 0
              Top = 0
              Width = 996
              Height = 217
              Align = alTop
              Lines.Strings = (
                'MeTexto')
              ScrollBars = ssVertical
              TabOrder = 0
            end
            object MeEndereco: TMemo
              Left = 0
              Top = 217
              Width = 996
              Height = 276
              Align = alClient
              Lines.Strings = (
                'MeEndereco')
              ScrollBars = ssVertical
              TabOrder = 1
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 658
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 702
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImporta: TBitBtn
        Tag = 39
        Left = 7
        Top = 3
        Width = 120
        Height = 40
        Caption = 'Importa Dados'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImportaClick
      end
    end
  end
end
