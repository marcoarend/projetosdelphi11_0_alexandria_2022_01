unit EntiImporCfg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, (*&PlannerMonthView, DBPlannerMonthView, Planner, DBPlanner,*)
  dmkRadioGroup, UnDmkEnums;

type
  TFmEntiImporCfg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    LaUF: TLabel;
    CBUF: TComboBox;
    RGTipo: TdmkRadioGroup;
    LaUFInfo: TLabel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTipoEnt: Integer;
    FUF: String;
  end;

  var
  FmEntiImporCfg: TFmEntiImporCfg;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmEntiImporCfg.BtOKClick(Sender: TObject);
begin
  if RGTipo.Visible = False then
  begin
    FUF      := Uppercase(CBUF.Text);
    FTipoEnt := 0;
  end else
  begin
    FUF      := '';
    FTipoEnt := RGTipo.ItemIndex;
  end;
  Close;
end;

procedure TFmEntiImporCfg.BtSaidaClick(Sender: TObject);
begin
  FUF      := '';
  FTipoEnt := -1;
  Close;
end;

procedure TFmEntiImporCfg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiImporCfg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FUF      := '';
  FTipoEnt := -1;
end;

procedure TFmEntiImporCfg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
