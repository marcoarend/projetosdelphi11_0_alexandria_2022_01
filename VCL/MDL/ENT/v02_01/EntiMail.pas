unit EntiMail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral, Variants, dmkValUsu,
  dmkImage, UnDmkEnums, dmkPermissoes;

type
  TFmEntiMail = class(TForm)
    Panel1: TPanel;
    QrEntiTipCto: TmySQLQuery;
    DsEntiTipCto: TDataSource;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdCodUsu: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    dmkValUsu1: TdmkValUsu;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    SpeedButton2: TSpeedButton;
    EdConta: TdmkEdit;
    EdEMail: TdmkEdit;
    CBEntiTipCto: TdmkDBLookupComboBox;
    EdEntiTipCto: TdmkEditCB;
    DBEdContatoCod: TdmkDBEdit;
    Label1: TLabel;
    DBEdContatoNom: TDBEdit;
    Label7: TLabel;
    DBEdContatoCrg: TDBEdit;
    Label6: TLabel;
    QrEntiTipCtoCodigo: TIntegerField;
    QrEntiTipCtoCodUsu: TIntegerField;
    QrEntiTipCtoNome: TWideStringField;
    Label11: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label10: TLabel;
    EdOrdem: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEntiMail(Conta: Integer);
  public
    { Public declarations }
    FQrEntiMail, FQrEntidades, FQrEntiContat: TmySQLQuery;
    FDsEntiContat, FDsEntidades: TDataSource;
  end;

  var
  FmEntiMail: TFmEntiMail;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, Principal, EntiContat,
  MyDBCheck, DmkDAC_PF, UnEntities;

{$R *.DFM}

procedure TFmEntiMail.BtOKClick(Sender: TObject);
var
  Conta: Integer;
begin
  if DBEdCodigo.Text = '' then
    DBEdCodigo.UpdCampo := '';
  //
  if Trim(EdEMail.Text) = '' then
  begin
    Geral.MB_Aviso('Informe o e-mail do contato!');
    EdEMail.SetFocus;
    Exit;
  end;
  Conta := UMyMod.BuscaEmLivreY_Def('entimail', 'Conta', ImgTipo.SQLType,
  EdConta.ValueVariant);
  //
  if UMyMod.ExecSQLInsUpdFm(Fmentimail, ImgTipo.SQLType, 'entimail',
  Conta, Dmod.QrUpd) then
  begin
    DBEdCodigo.UpdCampo := 'Codigo';
    ReopenEntiMail(Conta);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdConta.ValueVariant      := 0;
      EdEntiTipCto.ValueVariant := 0;
      CBEntiTipCto.KeyValue     := Null;
      EdEMail.ValueVariant      := '';
      EdOrdem.ValueVariant      := EdOrdem.ValueVariant + 1;
      EdEMail.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmEntiMail.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiMail.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdEmail.CharCase := ecNormal;
  //
  DBEdCodigo.DataSource := FDsEntidades;
  DBEdCodUsu.DataSource := FDsEntidades;
  DBEdNome.DataSource := FDsEntidades;
  //
  DBEdContatoCod.DataSource := FDsEntiContat;
  DBEdContatoNom.DataSource := FDsEntiContat;
  DBEdContatoCrg.DataSource := FDsEntiContat;
end;

procedure TFmEntiMail.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  Entities.ReopenTipCto(QrEntiTipCto, etcEmail);
end;

procedure TFmEntiMail.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiMail.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := True;
  end else
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
  end;
end;

procedure TFmEntiMail.SpeedButton2Click(Sender: TObject);
begin
  Entities.MostraFormMostraEntiTipCto(dmkValUsu1.ValueVariant, EdEntiTipCto,
    CBEntiTipCto, QrEntiTipCto);
end;

procedure TFmEntiMail.ReopenEntiMail(Conta: Integer);
begin
  if (FQrEntiMail <> nil) and (FQrEntiContat <> nil) then
  begin
    FQrEntiMail.Close;
    if FQrEntiMail.Params.Count > 0 then
      FQrEntiMail.Params[0].AsInteger :=
        FQrEntiContat.FieldByName('Controle').AsInteger;
    UnDmkDAC_PF.AbreQuery(FQrEntiMail, Dmod.MyDB);
    //
    if Conta <> 0 then
      FQrEntiMail.Locate('Conta', Conta, []);
  end;
end;

end.
