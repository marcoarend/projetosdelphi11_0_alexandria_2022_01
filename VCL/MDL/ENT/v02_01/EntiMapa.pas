unit EntiMapa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, UnDmkWeb,
  // Componente TChromium :  https://code.google.com/p/delphichromiumembedded/
  //cefvcl, ceflib,
  // Fim componente
  Vcl.OleCtrls, SHDocVw;

type
  TFmEntiMapa = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel6: TPanel;
    Label5: TLabel;
    SpeedButton4: TSpeedButton;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EdEndere: TdmkEdit;
    BitBtn1: TBitBtn;
    LBEndereco: TListBox;
    BtUp: TBitBtn;
    BtDown: TBitBtn;
    BtExclui: TBitBtn;
    CBModoViagem: TComboBox;
    CkDirectionsPanel: TCheckBox;
    EdDescri: TdmkEdit;
    EdNome: TdmkEdit;
    LBDescricao: TListBox;
    BtMapa: TBitBtn;
    PB1: TProgressBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    WB_Ch: TWebBrowser;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtDownClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtMapaClick(Sender: TObject);
    procedure LBEnderecoClick(Sender: TObject);
    procedure BtUpClick(Sender: TObject);
(*  Somente at� o Delphi Tokyo?
    procedure Chromium1LoadStart(Sender: TObject; const browser: ICefBrowser;
      const frame: ICefFrame);
    procedure Chromium1LoadEnd(Sender: TObject; const browser: ICefBrowser;
      const frame: ICefFrame; httpStatusCode: Integer; out Result: Boolean);
*)
    procedure WB_ChProgressChange(ASender: TObject; Progress,
      ProgressMax: Integer);
    procedure LBEnderecoDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure PesquisaEnderecoEntidade(var Endereco, Descricao: String);
  public
    { Public declarations }
    FTipoForm: TFormCadEnti;
    procedure CarregaMapa();
  end;

  var
    FmEntiMapa: TFmEntiMapa;
  const
    FURLMapa = 'http://dermatek.net.br/gadgets/GoogleMaps.php';

implementation

uses UnMyObjects, Module, MeuDBUses, UnConsultasWeb;

{$R *.DFM}

procedure TFmEntiMapa.CarregaMapa();
var
  i: Integer;
  URL, Str: String;
begin
  if LBEndereco.Count = 0 then
  begin
    WB_Ch.Visible := True;
    WB_Ch.Navigate('about:blank');
(*  Somente at� o Delphi Tokyo?
    Chromium1.Browser.MainFrame.LoadUrl('about:blank');
*)
  end else
  begin
    Str := 'enderecos=';
    //
    for i := 0 to LBEndereco.Count - 1 do
    begin
      if i = LBEndereco.Count - 1 then
        Str := Str + LBEndereco.Items[i]
      else
        Str := Str + LBEndereco.Items[i] + '||';
    end;
    Str := Str + '&modo=' + UConsultasWeb.GoogleMaps_TraduzModoDeDeslocamento(CBModoViagem.ItemIndex);
    //
    if CkDirectionsPanel.Checked then
      Str := Str + '&direcoes=True'
    else
      Str := Str + '&direcoes=False';
    URL := FURLMapa + '?' + Str;
    //
    WB_Ch.Visible := True;
    WB_Ch.Navigate(URL);
(*  Somente at� o Delphi Tokyo?
    Chromium1.Browser.MainFrame.LoadUrl(URL);
*)
  end;
end;

(*  Somente at� o Delphi Tokyo?
procedure TFmEntiMapa.Chromium1LoadEnd(Sender: TObject;
  const browser: ICefBrowser; const frame: ICefFrame; httpStatusCode: Integer;
  out Result: Boolean);
begin
  Screen.Cursor := crDefault;
end;

procedure TFmEntiMapa.Chromium1LoadStart(Sender: TObject;
  const browser: ICefBrowser; const frame: ICefFrame);
begin
  Screen.Cursor := crHourGlass;
end;
*)

procedure TFmEntiMapa.PesquisaEnderecoEntidade(var Endereco, Descricao: String);
var
  Codigo: Integer;
begin
  Endereco  := '';
  Descricao := '';
  //
  (*
  MyObjects.CriaForm_AcessoTotal(TFmPesqNome, FmPesqNome);
  FmPesqNome.FND := True;
  FmPesqNome.FNomeTabela      := 'entidades';
  FmPesqNome.FNomeCampoNome   := 'RazaoSocial';
  FmPesqNome.FNomeCampoCodigo := 'Codigo';
  FmPesqNome.ShowModal;
  FmPesqNome.Destroy;
  *)
  FmMeuDBUses.PesquisaNome('entidades', 'RazaoSocial', 'Codigo', '');
  //
  Codigo := VAR_CADASTRO;
  //
  if Codigo <> 0 then
    UConsultasWeb.MapasDeEntidadesMontaEndereco(FTipoForm, Codigo, Endereco, Descricao);
end;

procedure TFmEntiMapa.SpeedButton4Click(Sender: TObject);
var
  Endereco, Descricao: String;
begin
  PesquisaEnderecoEntidade(Endereco, Descricao);
  //
  EdEndere.ValueVariant := Endereco;
  EdDescri.ValueVariant := Descricao;
end;

procedure TFmEntiMapa.WB_ChProgressChange(ASender: TObject; Progress,
  ProgressMax: Integer);
begin
  if Progress > 0 then
  begin
    PB1.Visible  := True;
    PB1.Max      := ProgressMax;
    PB1.Position := Progress
  end else
  begin
    PB1.Visible  := False;
    PB1.Position := 0
  end;
end;

procedure TFmEntiMapa.BitBtn1Click(Sender: TObject);
var
  Endereco, Descri: String;
begin
  Endereco := EdEndere.ValueVariant;
  Descri   := EdDescri.ValueVariant;
  //
  if MyObjects.FIC(Endereco = '', EdEndere, 'Defina um endere�o / latitude longitude!') then Exit;
  if MyObjects.FIC(Descri = '', EdDescri, 'Defina uma descri��o!') then Exit;
  //
  LBEndereco.Items.Add(Geral.SemAcento(Endereco));
  LBDescricao.Items.Add(Descri);
  //
  EdEndere.ValueVariant := '';
  EdDescri.ValueVariant := '';
  EdEndere.SetFocus;
end;

procedure TFmEntiMapa.BtDownClick(Sender: TObject);
  procedure MoveDown(Lista: TListBox);
  var
    CurrIndex, LastIndex: Integer;
  begin
    with Lista do
    begin
      CurrIndex := ItemIndex;
      LastIndex := Items.Count;
      if ItemIndex <> -1 then
      begin
        if CurrIndex + 1 < LastIndex then
        begin
          Items.Move(ItemIndex, (CurrIndex + 1));
          ItemIndex := CurrIndex + 1;
        end;
      end;
    end;
  end;
begin
  MoveDown(LBEndereco);
  MoveDown(LBDescricao);
end;

procedure TFmEntiMapa.BtExcluiClick(Sender: TObject);
begin
  LBEndereco.DeleteSelected;
  LBDescricao.DeleteSelected;
end;

procedure TFmEntiMapa.BtMapaClick(Sender: TObject);
begin
  if MyObjects.FIC(LBEndereco.Count = 0, nil, 'Defina pelo menos um endere�o!') then Exit;
  if MyObjects.FIC(CBModoViagem.ItemIndex < 0, CBModoViagem, 'Defina o modo de deslocamento!') then Exit;
  //
  CarregaMapa();
end;

procedure TFmEntiMapa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiMapa.BtUpClick(Sender: TObject);
  procedure MoveUp(Lista: TListBox);
  var
    CurrIndex: Integer;
  begin
    with Lista do
    begin
      if ItemIndex > 0 then
      begin
        CurrIndex := ItemIndex;
        Items.Move(ItemIndex, (CurrIndex - 1));
        ItemIndex := CurrIndex - 1;
      end;
    end;
  end;
begin
  MoveUp(LBEndereco);
  MoveUp(LBDescricao);
end;

procedure TFmEntiMapa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiMapa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType              := stLok;
  PageControl1.ActivePageIndex := 1;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'D� um duplo clique no endere�o desejado na lista de endere�os para abrir o mapa no navegador padr�o!');
end;

procedure TFmEntiMapa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiMapa.LBEnderecoClick(Sender: TObject);
begin
  if LBEndereco.ItemIndex >= 0 then
    EdNome.ValueVariant := LBDescricao.Items[LBEndereco.ItemIndex];
end;

procedure TFmEntiMapa.LBEnderecoDblClick(Sender: TObject);
(*
const
  SiteURL = 'https://www.google.com.br/maps/place/';
var
  URL: String;
begin
  URL := LBEndereco.Items.Text;
  URL := StringReplace(URL, ' ', '+', [rfReplaceAll, rfIgnoreCase]);
  URL := SiteURL + URL;
  //
  DmkWeb.MostraWebBrowser(URL, True, False, 0, 0);
*)
begin
  DmkWeb.MostraGoogleMapsPlace(LBEndereco.Items.Text, True, False, 0, 0);
end;

{
object Chromium1: TChromium
  Left = 0
  Top = 0
  Width = 731
  Height = 617
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Align = alClient
  TabOrder = 0
  OnLoadStart = Chromium1LoadStart
  OnLoadEnd = Chromium1LoadEnd
  Options.AcceleratedPaintingDisabled = False
  Options.AcceleratedFiltersDisabled = False
  Options.AcceleratedPluginsDisabled = False
end
}

end.
