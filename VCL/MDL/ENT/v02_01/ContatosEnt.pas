unit ContatosEnt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBGrid, mySQLDbTables, Variants,
  Menus, dmkEdit, UnDMkEnums;

type
  TFmContatosEnt = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Panel7: TPanel;
    EdPesq: TEdit;
    DBGEntiContat: TdmkDBGrid;
    LaPesq: TLabel;
    QrEntiContat: TmySQLQuery;
    DsEntiContat: TDataSource;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    QrEntiContatDtaNatal: TDateField;
    QrEntiContatSexo: TSmallintField;
    QrEntiContatSEXO_TXT: TWideStringField;
    QrEntiContatDTANATAL_TXT: TWideStringField;
    QrEntiContatDiarioAdd: TIntegerField;
    QrEntiMail: TmySQLQuery;
    QrEntiMailNOMEETC: TWideStringField;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailEntiTipCto: TIntegerField;
    QrEntiMailOrdem: TIntegerField;
    DsEntiMail: TDataSource;
    QrEntiTel: TmySQLQuery;
    QrEntiTelNOMEETC: TWideStringField;
    QrEntiTelConta: TIntegerField;
    QrEntiTelTelefone: TWideStringField;
    QrEntiTelEntiTipCto: TIntegerField;
    QrEntiTelTEL_TXT: TWideStringField;
    QrEntiTelRamal: TWideStringField;
    DsEntiTel: TDataSource;
    Panel6: TPanel;
    Panel8: TPanel;
    Splitter1: TSplitter;
    Panel10: TPanel;
    Splitter2: TSplitter;
    DBGEntiMail: TDBGrid;
    DBGEntiTel: TDBGrid;
    DBGEntidades: TDBGrid;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    CkMaskManual: TCheckBox;
    BtContato: TBitBtn;
    BtTelefones: TBitBtn;
    BtEMails: TBitBtn;
    BtEntidade: TBitBtn;
    PMContatos: TPopupMenu;
    IncluiContato1: TMenuItem;
    porIncluso1: TMenuItem;
    AlteraContato1: TMenuItem;
    ExcluiContato1: TMenuItem;
    N2: TMenuItem;
    PMTelefones: TPopupMenu;
    IncluiTelefone1: TMenuItem;
    AlteraTelefone1: TMenuItem;
    ExcluiTelefone1: TMenuItem;
    PMEMails: TPopupMenu;
    IncluiEmail1: TMenuItem;
    AlteraEmail1: TMenuItem;
    ExcluiEmail1: TMenuItem;
    PMEntidades: TPopupMenu;
    IncluiEntidade1: TMenuItem;
    ExcluiEntidade1: TMenuItem;
    Mesclacontatos1: TMenuItem;
    QrEntidadesCodUsu: TIntegerField;
    QrEntiContatCargo: TIntegerField;
    QrEntiContatCodigo: TIntegerField;
    QrEntiContatNOME_CARGO: TWideStringField;
    QrPsq: TmySQLQuery;
    QrPsqItens: TLargeintField;
    Label2: TLabel;
    EdControle: TdmkEdit;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesRazaoSocial: TWideStringField;
    QrEntidadesNome: TWideStringField;
    QrEntidadesFantasia: TWideStringField;
    QrEntidadesApelido: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure QrEntiContatBeforeClose(DataSet: TDataSet);
    procedure QrEntiContatAfterScroll(DataSet: TDataSet);
    procedure CkMaskManualClick(Sender: TObject);
    procedure BtContatoClick(Sender: TObject);
    procedure BtEntidadeClick(Sender: TObject);
    procedure BtTelefonesClick(Sender: TObject);
    procedure BtEMailsClick(Sender: TObject);
    procedure PMContatosPopup(Sender: TObject);
    procedure PMEMailsPopup(Sender: TObject);
    procedure PMTelefonesPopup(Sender: TObject);
    procedure PMEntidadesPopup(Sender: TObject);
    procedure QrEntiContatAfterOpen(DataSet: TDataSet);
    procedure porIncluso1Click(Sender: TObject);
    procedure AlteraContato1Click(Sender: TObject);
    procedure ExcluiContato1Click(Sender: TObject);
    procedure IncluiTelefone1Click(Sender: TObject);
    procedure AlteraTelefone1Click(Sender: TObject);
    procedure ExcluiTelefone1Click(Sender: TObject);
    procedure IncluiEmail1Click(Sender: TObject);
    procedure AlteraEmail1Click(Sender: TObject);
    procedure ExcluiEmail1Click(Sender: TObject);
    procedure DBGEntidadesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QrEntiTelCalcFields(DataSet: TDataSet);
    procedure IncluiEntidade1Click(Sender: TObject);
    procedure ExcluiEntidade1Click(Sender: TObject);
    procedure Mesclacontatos1Click(Sender: TObject);
    procedure EdControleChange(Sender: TObject);
    procedure QrEntidadesCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenContatos(Controle: Integer);
    procedure ReopenEntidades(Codigo: Integer);
    procedure MostraEntiContat(SQLType: TSQLType);
    procedure ReopenEntiMail(Conta: Integer);
    procedure ReopenEntiTel(Conta: Integer);
//    procedure MostraSelEnti(SQLType: TSQLType);
    procedure MostraEntiMail(SQLType: TSQLType);
    procedure MostraEntiTel(SQLType: TSQLType);
  public
    { Public declarations }
    procedure IniciaPesqELocContato(QrEC: TmySQLQuery; Nome: String;
      Controle, Codigo: Integer);
  end;

  var
  FmContatosEnt: TFmContatosEnt;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, EntiContat, EntiMail,
EntiTel, EntiConEnt, MyDBCheck;

{$R *.DFM}

procedure TFmContatosEnt.AlteraContato1Click(Sender: TObject);
begin
  MostraEntiContat(stUpd);
end;

procedure TFmContatosEnt.AlteraEmail1Click(Sender: TObject);
begin
  MostraEntiMail(stUpd);
end;

procedure TFmContatosEnt.AlteraTelefone1Click(Sender: TObject);
begin
  MostraEntiTel(stUpd);
end;

procedure TFmContatosEnt.BtContatoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMContatos, BtContato);
end;

procedure TFmContatosEnt.BtEMailsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEMails, BtEMails);
end;

procedure TFmContatosEnt.BtEntidadeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntidades, BtEntidade);
end;

procedure TFmContatosEnt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContatosEnt.BtTelefonesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTelefones, BtTelefones);
end;

procedure TFmContatosEnt.CkMaskManualClick(Sender: TObject);
begin
  ReopenContatos(0);
end;

procedure TFmContatosEnt.DBGEntidadesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if (QrEntidadesCodigo.Value = QrEntiContatCodigo.Value) then
  begin
    Cor := clBlue;
    MyObjects.DesenhaTextoEmDBGrid(
      TDbGrid(DBGEntidades), Rect, Cor, clWhite,
      Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmContatosEnt.EdControleChange(Sender: TObject);
var
  Habilitado: Boolean;
begin
  Habilitado := EdControle.ValueVariant = 0;
  LaPesq.Enabled       := Habilitado;
  EdPesq.Enabled       := Habilitado;
  CkMaskManual.Enabled := Habilitado;
  ReopenContatos(0);
end;

procedure TFmContatosEnt.EdPesqChange(Sender: TObject);
begin
  ReopenContatos(0);
end;

procedure TFmContatosEnt.ExcluiContato1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntiContat, TDBGrid(DBGEntiContat),
    'enticontat', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmContatosEnt.ExcluiEmail1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntimail, DBGEntiMail,
    'entimail', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmContatosEnt.ExcluiEntidade1Click(Sender: TObject);
const
  Pergunta = 'Deseja realmente retirar a entidade do contato?';
  Tabela =  'enticonent';
  Campo1 = 'Codigo';
  Campo2 = 'Controle';
var
  Inteiro1, inteiro2, Codigo, Controle: Integer;
  ZeraCodigo: Boolean;
begin
  Inteiro1 := QrEntidadesCodigo.Value;
  inteiro2 := QrEntiContatControle.Value;
  //
  Codigo   := QrEntiContatCodigo.Value;
  Controle := QrEntiContatControle.Value;
  //
  ZeraCodigo := QrEntiContatCodigo.Value = QrEntidadesCodigo.Value;
  //
  if UMyMod.ExcluiRegistroInt2(Pergunta, Tabela, Campo1, Campo2,
  Inteiro1, Inteiro2) = ID_YES then
  begin
    if ZeraCodigo then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE enticontat SET Codigo=0 ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Controle=' + Geral.FF0(Controle),
      '']);
    end;
    ReopenContatos(Controle);
  end;
end;

procedure TFmContatosEnt.ExcluiTelefone1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntitel, DBGEntiTel,
    'entitel', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmContatosEnt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContatosEnt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmContatosEnt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContatosEnt.IncluiEmail1Click(Sender: TObject);
begin
  MostraEntiMail(stIns);
end;

procedure TFmContatosEnt.IncluiEntidade1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrEntiContatControle.Value;
  if DBCheck.CriaFm(TFmEntiConEnt, FmEntiConEnt, afmoNegarComAviso) then
  begin
    FmEntiConEnt.ReopenEntidades(QrEntidadesCodigo.Value);
    FmEntiConEnt.FQrEntiConEnt := QrEntiContat;
    FmEntiConEnt.FDatabase := Dmod.MyDB;
    FmEntiConEnt.EdContato.ValueVariant := QrEntiContatControle.Value;
    FmEntiConEnt.EdContato.Enabled := False;
    FmEntiConEnt.CBContato.KeyValue := QrEntiContatControle.Value;
    FmEntiConEnt.CBContato.Enabled := False;
    FmEntiConEnt.ShowModal;
    FmEntiConEnt.Destroy;
    //
    QrEntiContat.Locate('Controle', Controle, []);
  end;
end;

procedure TFmContatosEnt.IncluiTelefone1Click(Sender: TObject);
begin
  MostraEntiTel(stIns);
end;

procedure TFmContatosEnt.IniciaPesqELocContato(QrEC: TmySQLQuery; Nome: String;
  Controle, Codigo: Integer);
begin
  if (QrEC.State <> dsInactive) and (QrEC.RecordCount > 0) then
  begin
    EdPesq.Text := Nome;
    if QrEntiContat.Locate('Controle', Controle, []) then
      QrEntidades.Locate('Codigo', Codigo, [])
    else
      Geral.MB_Erro('N�o foi poss�vel localizar o contato n� ' + Geral.FF0(
      Controle) + sLineBreak + 'Nome: "' + Nome + '"');
  end;
end;

procedure TFmContatosEnt.Mesclacontatos1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Contato Receptor';
  Prompt = 'Informe o contato que receber� os cadastros:';
  Campo  = 'Descricao';
var
  Codigo, Controle, Conta: Integer;
  EntiContat: Variant;
begin
  Controle := QrEntiContatControle.Value;
  EntiContat := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Controle Codigo, Nome ' + Campo,
  'FROM enticontat ',
  'WHERE Controle <> ' + Geral.FF0(Controle),
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, True);
  if EntiContat <> Null then
  begin
    // Entidades
    QrEntidades.First;
    while not QrEntidades.Eof do
    begin
      Codigo := QrEntidadesCodigo.Value;
      UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
      'SELECT COUNT(*) Itens ',
      'FROM enticonent ',
      'WHERE Codigo =' + Geral.FF0(Codigo),
      'AND Controle =' + Geral.FF0(EntiContat),
      '']);
      if QrPsqItens.Value = 0 then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE enticonent ',
        'SET Controle =' + Geral.FF0(EntiContat),
        'WHERE Codigo =' + Geral.FF0(Codigo),
        'AND Controle =' + Geral.FF0(Controle),
        '']);
      end else
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        DELETE_FROM + ' enticonent ',
        'WHERE Codigo =' + Geral.FF0(Codigo),
        'AND Controle =' + Geral.FF0(Controle),
        '']);
      end;
      //
      QrEntidades.Next;
    end;
    // Emails
    QrEntiMail.First;
    while not QrEntiMail.Eof do
    begin
      Conta := QrEntiMailConta.Value;
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE entimail ',
      'SET Controle =' + Geral.FF0(EntiContat),
      'WHERE Conta =' + Geral.FF0(Conta),
      '']);
      //
      QrEntiMail.Next;
    end;
    // Telefones
    QrEntiTel.First;
    while not QrEntiTel.Eof do
    begin
      Conta := QrEntiTelConta.Value;
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE entitel ',
      'SET Controle =' + Geral.FF0(EntiContat),
      'WHERE Conta =' + Geral.FF0(Conta),
      '']);
      //
      QrEntiTel.Next;
    end;
    //
    //Excluir EntiContat!
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    DELETE_FROM + ' enticontat ',
    'WHERE Controle =' + Geral.FF0(Controle),
    '']);
    //
    ReopenContatos(EntiContat);
  end;
end;

procedure TFmContatosEnt.MostraEntiContat(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiContat, FmEntiContat, afmoNegarComAviso,
  QrEntiContat, SQLType) then
  begin
    FmEntiContat.FQrEntiContat := QrEntiContat;
    FmEntiContat.FEntidadeOri  := QrEntiContatCodigo.Value;
    FmEntiContat.FEntidadeAtu  := QrEntidadesCodigo.Value;
    //
    FmEntiContat.ShowModal;
    FmEntiContat.Destroy;
  end;
end;

procedure TFmContatosEnt.MostraEntiMail(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiMail, FmEntiMail, afmoNegarComAviso,
  QrEntiMail, SQLType) then
  begin
    FmEntiMail.FQrEntiMail := QrEntiMail;
    FmEntiMail.FQrEntidades := QrEntidades;
    FmEntiMail.FDsEntidades := DsEntidades;
    FmEntiMail.FQrEntiContat  := QrEntiContat;
    FmEntiMail.FDsEntiContat  := DsEntiContat;
    //
    FmEntiMail.DBEdCodigo.DataSource := DsEntidades;
    FmEntiMail.DBEdCodUsu.DataSource := DsEntidades;
    FmEntiMail.DBEdNome.DataSource := DsEntidades;
    //
    FmEntiMail.DBEdContatoCod.DataSource := DsEntiContat;
    FmEntiMail.DBEdContatoNom.DataSource := DsEntiContat;
    FmEntiMail.DBEdContatoCrg.DataSource := DsEntiContat;
    //
    FmEntiMail.ShowModal;
    FmEntiMail.Destroy;
  end;
end;

procedure TFmContatosEnt.MostraEntiTel(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiTel, FmEntiTel, afmoNegarComAviso,
  QrEntiTel, SQLType) then
  begin
    FmEntiTel.FQrEntiTel := QrEntiTel;
    FmEntiTel.FQrEntidades := QrEntidades;
    FmEntiTel.FDsEntidades := DsEntidades;
    FmEntiTel.FQrEntiContat  := QrEntiContat;
    FmEntiTel.FDsEntiContat  := DsEntiContat;
    //
    FmEntiTel.DBEdCodigo.DataSource := DsEntidades;
    FmEntiTel.DBEdCodUsu.DataSource := DsEntidades;
    FmEntiTel.DBEdNome.DataSource := DsEntidades;
    //
    FmEntiTel.DBEdContatoCod.DataSource := DsEntiContat;
    FmEntiTel.DBEdContatoNom.DataSource := DsEntiContat;
    FmEntiTel.DBEdContatoCrg.DataSource := DsEntiContat;
    //
    FmEntiTel.ShowModal;
    FmEntiTel.Destroy;
  end;
end;

{
procedure TFmContatosEnt.MostraSelEnti(SQLType: TSQLType);
begin
//
end;
}

procedure TFmContatosEnt.PMContatosPopup(Sender: TObject);
begin
  //MyObjects.HabilitaMenuItemItsIns(IncluiContato1, QrEntidades);
  MyObjects.HabilitaMenuItemItsUpd(AlteraContato1, QrEntiContat);
  MyObjects.HabilitaMenuItemItsUpd(Mesclacontatos1, QrEntiContat);
  MyObjects.HabilitaMenuItemCabDelC1I3(ExcluiContato1, QrEntiContat,
    QrEntidades, QrEntiTel, QrEntiMail);
end;

procedure TFmContatosEnt.PMEMailsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiEmail1, QrEntiContat);
  MyObjects.HabilitaMenuItemItsUpd(AlteraEmail1, QrEntiMail);
  MyObjects.HabilitaMenuItemItsDel(ExcluiEmail1, QrEntiMail);
end;

procedure TFmContatosEnt.PMEntidadesPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiEntidade1, QrEntiContat);
  //MyObjects.HabilitaMenuItemItsUpd(AlteraEntidade1, QrEntidades);
  MyObjects.HabilitaMenuItemItsDel(ExcluiEntidade1, QrEntidades);
end;

procedure TFmContatosEnt.PMTelefonesPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiTelefone1, QrEntiContat);
  MyObjects.HabilitaMenuItemItsUpd(AlteraTelefone1, QrEntiTel);
  MyObjects.HabilitaMenuItemItsDel(ExcluiTelefone1, QrEntiTel);
end;

procedure TFmContatosEnt.porIncluso1Click(Sender: TObject);
begin
  MostraEntiContat(stIns);
end;

procedure TFmContatosEnt.QrEntiContatAfterOpen(DataSet: TDataSet);
begin
  if QrEntiContat.RecordCount > 0 then
  begin
    BtEntidade.Enabled := True;
    BtEMails.Enabled := True;
    BtTelefones.Enabled := True;
  end;
end;

procedure TFmContatosEnt.QrEntiContatAfterScroll(DataSet: TDataSet);
begin
  ReopenEntidades(0);
  ReopenEntiTel(0);
  ReopenEntiMail(0);
end;

procedure TFmContatosEnt.QrEntiContatBeforeClose(DataSet: TDataSet);
begin
  QrEntidades.Close;
  QrEntiMail.Close;
  QrEntiTel.Close;
end;

procedure TFmContatosEnt.QrEntidadesCalcFields(DataSet: TDataSet);
var
  NO_ENT: String;
begin
  //Usar calc para compatibilizar o MySQL  4 e 5
  NO_ENT := '';
  if QrEntidades.FieldByName('RazaoSocial').AsString <> '' then
    NO_ENT := NO_ENT + ' [R] ' + QrEntidades.FieldByName('RazaoSocial').AsString;
  if QrEntidades.FieldByName('Nome').AsString <> '' then
    NO_ENT := NO_ENT + ' [N] ' + QrEntidades.FieldByName('Nome').AsString;
  if QrEntidades.FieldByName('Fantasia').AsString <> '' then
    NO_ENT := NO_ENT + ' [F] ' + QrEntidades.FieldByName('Fantasia').AsString;
  if QrEntidades.FieldByName('Apelido').AsString <> '' then
    NO_ENT := NO_ENT + ' [A] ' + QrEntidades.FieldByName('Apelido').AsString;
  //
  QrEntidadesNOMEENTIDADE.Value := NO_ENT;
end;

procedure TFmContatosEnt.QrEntiTelCalcFields(DataSet: TDataSet);
begin
  QrEntiTelTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntiTelTelefone.Value);
end;

procedure TFmContatosEnt.ReopenContatos(Controle: Integer);
var
  Mascara, SQL: String;
  Ctrl: Integer;
begin
  Ctrl := EdControle.ValueVariant;
  //
  if (Ctrl <> 0) or (Length(EdPesq.Text) > 2) then
  begin
    if Ctrl <> 0 then
    begin
      SQL := 'WHERE ece.Controle=' + Geral.FF0(Ctrl);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrEntiContat, DMod.MyDB, [
      'SELECT DISTINCT ece.Codigo, ece.Controle, eco.Nome, eco.Cargo,',
      'eco.DiarioAdd, eca.Nome NOME_CARGO, eco.DtaNatal, eco.Sexo,',
      'ELT(eco.Sexo+1, "NI", "M", "F", "?") SEXO_TXT,',
      'IF(eco.DtaNatal = 0, "", DATE_FORMAT(eco.DtaNatal, "%d/%m/%y")) DTANATAL_TXT',
      'FROM enticonent ece',
      'LEFT JOIN enticontat eco ON ece.Controle=eco.Controle',
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo',
      SQL,
      'ORDER BY eco.Nome ',
      '']);
    end
    else
    begin
      if CkMaskManual.Checked then
        Mascara := ''
      else
        Mascara := '%';
      SQL := 'WHERE eco.Nome LIKE "' + Mascara + EdPesq.Text + Mascara + '" ';
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrEntiContat, DMod.MyDB, [
      'SELECT DISTINCT eco.Codigo, eco.Controle, eco.Nome, eco.Cargo,',
      'eco.DiarioAdd, eca.Nome NOME_CARGO, eco.DtaNatal, eco.Sexo,',
      'ELT(eco.Sexo+1, "NI", "M", "F", "?") SEXO_TXT,',
      'IF(eco.DtaNatal = 0, "", DATE_FORMAT(eco.DtaNatal, "%d/%m/%y")) DTANATAL_TXT',
      'FROM enticontat eco',
      'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle',
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo',
      SQL,
      'ORDER BY eco.Nome ',
      '']);
      //
    end;
    QrEntiContat.Locate('Controle', Controle, []);
  end
  else
    QrEntiContat.Close;
end;

procedure TFmContatosEnt.ReopenEntidades(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, DMod.MyDB, [
    'SELECT ent.Codigo, ent.CodUsu, ent.RazaoSocial, ',
    'ent.Nome, ent.Fantasia, ent.Apelido ',
    'FROM entidades ent',
    'LEFT JOIN enticonent ece ON ece.Codigo=ent.Codigo',
    'WHERE ece.Controle=' + Geral.FF0(QrEntiContatControle.Value),
    'ORDER BY ent.RazaoSocial, ent.Nome, ent.Fantasia, ent.Apelido',
    '']);
end;

procedure TFmContatosEnt.ReopenEntiMail(Conta: Integer);
begin
  QrEntiMail.Close;
  QrEntiMail.Params[0].AsInteger := QrEntiContatControle.Value;
  UMyMod.AbreQuery(QrEntiMail, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiMail.Locate('Conta', Conta, []);
end;

procedure TFmContatosEnt.ReopenEntiTel(Conta: Integer);
begin
  QrEntiTel.Close;
  QrEntiTel.Params[0].AsInteger := QrEntiContatControle.Value;
  UMyMod.AbreQuery(QrEntiTel, DMod.MyDB);
  //
  if Conta <> 0 then
    QrEntiTel.Locate('Conta', Conta, []);
end;

end.

