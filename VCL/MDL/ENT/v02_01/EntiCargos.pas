unit EntiCargos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit,
  dmkLabel, dmkRadioGroup, UnDmkProcFunc, dmkImage, UnDmkEnums, dmkCheckBox;

type
  TFmEntiCargos = class(TForm)
    PainelDados: TPanel;
    DsEntiCargos: TDataSource;
    QrEntiCargos: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrEntiCargosCodigo: TIntegerField;
    QrEntiCargosCodUsu: TIntegerField;
    QrEntiCargosNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    GroupBox1: TGroupBox;
    PainelEdit: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdCodUsu: TdmkEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GroupBox2: TGroupBox;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    DBRGTipo: TDBRadioGroup;
    DBCkAtivo: TDBCheckBox;
    RGTipo: TdmkRadioGroup;
    CkAtivo: TdmkCheckBox;
    QrEntiCargosTipo: TSmallintField;
    QrEntiCargosAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEntiCargosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEntiCargosBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEntiCargos: TFmEntiCargos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntiCargos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEntiCargos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEntiCargosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEntiCargos.DefParams;
begin
  VAR_GOTOTABELA := 'EntiCargos';
  VAR_GOTOMYSQLTABLE := QrEntiCargos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome, Tipo, Ativo');
  VAR_SQLx.Add('FROM enticargos');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmEntiCargos.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'EntiCargos', 'CodUsu', [], [],
      stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmEntiCargos.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      GBCntrl.Visible := True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      GBCntrl.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEntiCargos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmEntiCargos.AlteraRegistro;
var
  EntiCargos : Integer;
begin
  EntiCargos := QrEntiCargosCodigo.Value;
  if QrEntiCargosCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(EntiCargos, Dmod.MyDB, 'EntiCargos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(EntiCargos, Dmod.MyDB, 'EntiCargos', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmEntiCargos.IncluiRegistro;
var
  Cursor : TCursor;
  EntiCargos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    EntiCargos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'EntiCargos', 'EntiCargos', 'Codigo');
    if Length(FormatFloat(FFormatFloat, EntiCargos))>Length(FFormatFloat) then
    begin
      Geral.MB_Aviso(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, EntiCargos);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmEntiCargos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEntiCargos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEntiCargos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntiCargos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntiCargos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntiCargos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntiCargos.BtAlteraClick(Sender: TObject);
begin
  if (QrEntiCargos.State <> dsInactive) and (QrEntiCargos.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrEntiCargos, [PainelDados],
      [PainelEdita], EdCodUsu, ImgTipo, 'EntiCargos');
  end;
end;

procedure TFmEntiCargos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEntiCargosCodigo.Value;
  Close;
end;

procedure TFmEntiCargos.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('EntiCargos', 'Codigo', ImgTipo.SQLType,
    QrEntiCargosCodigo.Value);
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmEntiCargos, PainelEdit,
    'EntiCargos', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEntiCargos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'EntiCargos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'EntiCargos', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'EntiCargos', 'Codigo');
end;

procedure TFmEntiCargos.BtExcluiClick(Sender: TObject);
begin
//
end;

procedure TFmEntiCargos.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrEntiCargos, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'EntiCargos');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'EntiCargos', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmEntiCargos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmEntiCargos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEntiCargosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntiCargos.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmEntiCargos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEntiCargos.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEntiCargosCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEntiCargos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntiCargos.QrEntiCargosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEntiCargos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiCargos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEntiCargosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'EntiCargos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEntiCargos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiCargos.QrEntiCargosBeforeOpen(DataSet: TDataSet);
begin
  QrEntiCargosCodigo.DisplayFormat := FFormatFloat;
end;

end.

