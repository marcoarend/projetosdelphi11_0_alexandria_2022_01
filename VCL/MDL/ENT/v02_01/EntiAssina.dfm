object FmEntiAssina: TFmEntiAssina
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-030 :: Cadastros de Entidades - Assinaturas'
  ClientHeight = 562
  ClientWidth = 821
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 821
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 967
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 908
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 540
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastros de Entidades - Assinaturas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 540
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastros de Entidades - Assinaturas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 540
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastros de Entidades - Assinaturas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 91
    Width = 821
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 821
      Height = 403
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1026
      ExplicitHeight = 503
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 821
        Height = 403
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 1026
        ExplicitHeight = 503
        object PainelData: TPanel
          Left = 2
          Top = 15
          Width = 209
          Height = 386
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitTop = 18
          ExplicitWidth = 12
          ExplicitHeight = 221
          object Painel1: TPanel
            Left = 0
            Top = 0
            Width = 209
            Height = 30
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 414
            ExplicitHeight = 127
            object dmkDBGrid1: TdmkDBGrid
              Left = 0
              Top = 26
              Width = 414
              Height = 101
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EntNome'
                  Title.Caption = 'Entidade'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EntAtivo'
                  Title.Caption = 'Ativo'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Entidade'
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsEntiAssina
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EntNome'
                  Title.Caption = 'Entidade'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EntAtivo'
                  Title.Caption = 'Ativo'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Entidade'
                  Visible = True
                end>
            end
            object EdPesquisa: TdmkEdit
              Left = 0
              Top = 0
              Width = 414
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'Pesquisa'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'Pesquisa'
              ValWarn = False
              OnEnter = EdPesquisaEnter
            end
          end
          object frxPreview1: TfrxPreview
            AlignWithMargins = True
            Left = 3
            Top = 33
            Width = 203
            Height = 350
            Align = alBottom
            OutlineVisible = True
            OutlineWidth = 121
            ThumbnailVisible = False
            FindFmVisible = False
            UseReportHints = True
            OutlineTreeSortType = dtsUnsorted
            HideScrolls = False
            ExplicitTop = 130
            ExplicitWidth = 408
          end
        end
        object PainelEdita: TPanel
          Left = 211
          Top = 15
          Width = 608
          Height = 386
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          Color = clBtnShadow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          Visible = False
          ExplicitLeft = 416
          ExplicitTop = 18
          ExplicitHeight = 483
          object GroupBox2: TGroupBox
            Left = 0
            Top = 300
            Width = 608
            Height = 86
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            TabOrder = 1
            ExplicitTop = 397
            object Panel5: TPanel
              Left = 429
              Top = 18
              Width = 177
              Height = 66
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BtDesiste: TBitBtn
                Tag = 13
                Left = 17
                Top = 4
                Width = 147
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Desiste'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtDesisteClick
              end
            end
            object Panel7: TPanel
              Left = 2
              Top = 18
              Width = 427
              Height = 66
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object BtConfirma: TBitBtn
                Tag = 14
                Left = 15
                Top = 4
                Width = 147
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&OK'
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtConfirmaClick
              end
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 0
            Width = 608
            Height = 230
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            TabOrder = 0
            object PainelEdit: TPanel
              Left = 2
              Top = 18
              Width = 604
              Height = 210
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label7: TLabel
                Left = 5
                Top = 5
                Width = 16
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'ID:'
              end
              object Label1: TLabel
                Left = 78
                Top = 6
                Width = 57
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Entidade:'
              end
              object Label10: TLabel
                Left = 5
                Top = 53
                Width = 64
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Signat'#225'rio:'
              end
              object Label2: TLabel
                Left = 5
                Top = 101
                Width = 354
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dados do signat'#225'rio: (Tipo de documento / UF / N'#250'mero, ...)'
              end
              object Label20: TLabel
                Left = 5
                Top = 155
                Width = 136
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Imagem da assinatura:'
              end
              object SbRespTec1: TSpeedButton
                Left = 568
                Top = 175
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SbRespTec1Click
              end
              object EdCodigo: TdmkEdit
                Left = 5
                Top = 25
                Width = 69
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ReadOnly = True
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Codigo'
                UpdCampo = 'Codigo'
                UpdType = utInc
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdEntidade: TdmkEditCB
                Left = 78
                Top = 25
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Entidade'
                UpdCampo = 'Entidade'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdEntidadeChange
                DBLookupComboBox = CBEntidade
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEntidade: TdmkDBLookupComboBox
                Left = 148
                Top = 25
                Width = 446
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEntidades
                TabOrder = 2
                dmkEditCB = EdEntidade
                QryCampo = 'Entidade'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdNome: TdmkEdit
                Left = 5
                Top = 73
                Width = 589
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Nome'
                UpdCampo = 'Nome'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDados: TdmkEdit
                Left = 5
                Top = 121
                Width = 589
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Dados'
                UpdCampo = 'Dados'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdImagem: TdmkEdit
                Left = 5
                Top = 175
                Width = 560
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Imagem'
                UpdCampo = 'Imagem'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 47
    Width = 821
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1022
      Height = 35
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 494
    Width = 821
    Height = 68
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 847
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object PnMenu: TPanel
      Left = 2
      Top = 18
      Width = 845
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 17
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 168
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtExcluiClick
      end
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidades'
      'WHERE Tipo = 1'
      'AND Ativo = 1'
      'AND Codigo > 0'
      'AND Codigo NOT IN'#11
      '('
      'SELECT Entidade'
      'FROM entiassina'
      ')'
      'ORDER BY Nome')
    Left = 348
    Top = 180
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 376
    Top = 180
  end
  object QrEntiAssina: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEntiAssinaAfterScroll
    SQL.Strings = (
      'SELECT ent.Nome EntNome, '
      'ent.Ativo EntAtivo, ass.*'
      'FROM entiassina ass'
      'LEFT JOIN entidades ent ON ent.Codigo = ass.Entidade'
      'ORDER BY EntNome')
    Left = 132
    Top = 212
    object QrEntiAssinaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiAssinaNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEntiAssinaDados: TWideStringField
      FieldName = 'Dados'
      Size = 100
    end
    object QrEntiAssinaImagem: TWideStringField
      FieldName = 'Imagem'
      Size = 255
    end
    object QrEntiAssinaEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrEntiAssinaEntNome: TWideStringField
      FieldName = 'EntNome'
      Size = 100
    end
    object QrEntiAssinaEntAtivo: TSmallintField
      FieldName = 'EntAtivo'
      MaxValue = 1
    end
  end
  object DsEntiAssina: TDataSource
    DataSet = QrEntiAssina
    Left = 160
    Top = 212
  end
  object frxENT_GEREN_030_001: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42801.365596932900000000
    ReportOptions.LastChange = 42801.368383923600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure PictureAssinaOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <LogoAssinaExiste> = True then'
      '    PictureAssina.LoadFromFile(<LogoAssinaPath>);  '
      'end;'
      ''
      'begin'
      '  '
      'end.')
    OnGetValue = frxENT_GEREN_030_001GetValue
    Left = 600
    Top = 200
    Datasets = <
      item
        DataSet = frxDsEntiAssina
        DataSetName = 'frxDsEntiAssina'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 209.999791670000000000
      PaperHeight = 79.375000000000000000
      PaperSize = 256
      LeftMargin = 19.999854166666670000
      RightMargin = 10.001250000000000000
      TopMargin = 10.001250000000000000
      BottomMargin = 10.001250000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 170.078850000000000000
        Top = 18.897650000000000000
        Width = 680.310439379473400000
        RowCount = 1
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315351180000000000
          Height = 170.078850000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object PictureAssina: TfrxPictureView
          AllowVectorExport = True
          Left = 175.748031500000000000
          Top = 3.779530000000000000
          Width = 328.819110000000000000
          Height = 139.842610000000000000
          OnBeforePrint = 'PictureAssinaOnBeforePrint'
          Center = True
          Frame.Typ = []
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 175.748031500000000000
          Top = 109.606370000000000000
          Width = 328.819110000000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiAssina."Nome"]'
            '[frxDsEntiAssina."Dados"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
    end
  end
  object frxDsEntiAssina: TfrxDBDataset
    UserName = 'frxDsEntiAssina'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'Dados=Dados'
      'Imagem=Imagem'
      'Entidade=Entidade'
      'EntNome=EntNome'
      'EntAtivo=EntAtivo')
    DataSet = QrEntiAssina
    BCDToCurrency = False
    
    Left = 188
    Top = 212
  end
end
