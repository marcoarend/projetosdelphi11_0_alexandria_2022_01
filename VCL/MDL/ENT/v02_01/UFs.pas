unit UFs;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, dmkImage, DmkDAC_PF, UnDmkEnums;

type
  TFmUFs = class(TForm)
    PainelDados: TPanel;
    DsUFs: TDataSource;
    QrUFs: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdDescricao: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdNome: TdmkEdit;
    PMCondicoes: TPopupMenu;
    QrUFsICMS: TmySQLQuery;
    DsUFsICMS: TDataSource;
    QrUFsCodigo: TIntegerField;
    QrUFsNome: TWideStringField;
    QrUFsICMS_C: TFloatField;
    QrUFsICMS_V: TFloatField;
    QrUFsLk: TIntegerField;
    QrUFsDataCad: TDateField;
    QrUFsDataAlt: TDateField;
    QrUFsUserCad: TIntegerField;
    QrUFsUserAlt: TIntegerField;
    QrUFsAlterWeb: TSmallintField;
    QrUFsAtivo: TSmallintField;
    QrUFsDescricao: TWideStringField;
    QrUFsICMSNome: TWideStringField;
    QrUFsICMSUFDest: TWideStringField;
    QrUFsICMSICMS_V: TFloatField;
    QrUFsICMSLk: TIntegerField;
    QrUFsICMSDataCad: TDateField;
    QrUFsICMSDataAlt: TDateField;
    QrUFsICMSUserCad: TIntegerField;
    QrUFsICMSUserAlt: TIntegerField;
    QrUFsICMSAlterWeb: TSmallintField;
    QrUFsICMSAtivo: TSmallintField;
    PMICMS: TPopupMenu;
    IncluinovoICMS1: TMenuItem;
    AlteraICMSatua1: TMenuItem;
    ExcluiICMSatual1: TMenuItem;
    QrMy: TmySQLQuery;
    N1: TMenuItem;
    Recriapadres1: TMenuItem;
    QrUFsICMSDESCRI_DEST: TWideStringField;
    IncluinovaUF1: TMenuItem;
    AlteraUFatual1: TMenuItem;
    ExcluiUFatual1: TMenuItem;
    QrMyUF: TFloatField;
    Panel6: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel4: TPanel;
    DBGICMS: TdmkDBGrid;
    BtICMS: TBitBtn;
    BtCondicoes: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrUFsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrUFsBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtCondicoesClick(Sender: TObject);
    procedure PMCondicoesPopup(Sender: TObject);
    procedure QrUFsBeforeClose(DataSet: TDataSet);
    procedure QrUFsAfterScroll(DataSet: TDataSet);
    procedure BtICMSClick(Sender: TObject);
    procedure IncluinovoICMS1Click(Sender: TObject);
    procedure AlteraICMSatua1Click(Sender: TObject);
    procedure ExcluiICMSatual1Click(Sender: TObject);
    procedure PMICMSPopup(Sender: TObject);
    procedure Recriapadres1Click(Sender: TObject);
    procedure IncluinovaUF1Click(Sender: TObject);
    procedure AlteraUFatual1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenUFsICMS(UFDest: String);
  end;

var
  FmUFs: TFmUFs;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UFsICMS, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmUFs.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmUFs.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrUFsCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmUFs.DefParams;
begin
  VAR_GOTOTABELA := 'UFs';
  VAR_GOTOMYSQLTABLE := QrUFs;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT * ');
  VAR_SQLx.Add('FROM ufs ');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmUFs.ExcluiICMSatual1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrUFsICMS, TDBGrid(DBGICMS), 'ufsicms',
  ['nome','UFDest'], ['nome','UFDest'], istPergunta, '');
end;

procedure TFmUFs.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmUFs.PMCondicoesPopup(Sender: TObject);
begin
  AlteraUFatual1.Enabled :=
    (QrUFs.State <> dsInactive) and (QrUFs.RecordCount > 0);
end;

procedure TFmUFs.PMICMSPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (QrUFsICMS.State <> dsInactive)
  and
    (QrUFsICMS.RecordCount > 0);
  //
  AlteraICMSatua1.Enabled := Habilita;
  ExcluiICMSatual1.Enabled := Habilita;
end;

procedure TFmUFs.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  UMyMod.AbreQuery(QrMy, Dmod.MyDB);
  if QrMyUF.Value <> 0 then
    LocCod(Trunc(QrMyUF.Value), Trunc(QrMyUF.Value))
  else
    Va(vpFirst);
end;

procedure TFmUFs.QueryPrincipalAfterOpen;
begin
end;

procedure TFmUFs.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmUFs.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmUFs.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmUFs.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmUFs.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmUFs.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrUFsCodigo.Value;
  Close;
end;

procedure TFmUFs.AlteraICMSatua1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmUFsICMS, FmUFsICMS, afmoNegarComAviso,
    QrUFsICMS, stUpd);
end;

procedure TFmUFs.AlteraUFatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrUFs, [PainelDados],
  [PainelEdita], EdDescricao, ImgTipo, 'ufs');
  EdNome.Enabled := False;
end;

procedure TFmUFs.BtCondicoesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCondicoes, BtCondicoes);
end;

procedure TFmUFs.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('UFs', 'Codigo', ImgTipo.SQLType,
    QrUFsCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmUFs, PainelEdit,
    'UFs', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmUFs.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'UFs', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'UFs', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'UFs', 'Codigo');
end;

procedure TFmUFs.BtICMSClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMICMS, BtICMS);
end;

procedure TFmUFs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  Panel4.Align      := alClient;
  CriaOForm;
end;

procedure TFmUFs.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrUFsCodigo.Value, LaRegistro.Caption);
end;

procedure TFmUFs.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmUFs.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmUFs.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrUFsCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmUFs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmUFs.QrUFsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmUFs.QrUFsAfterScroll(DataSet: TDataSet);
begin
  BtICMS.Enabled := QrUFsCodigo.Value <> 0;
  ReopenUFsICMS('');
end;

procedure TFmUFs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmUFs.SbQueryClick(Sender: TObject);
begin
  LocCod(QrUFsCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'UFs', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmUFs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmUFs.IncluinovaUF1Click(Sender: TObject);
begin
  EdNome.Enabled := True;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrUFs, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'ufs');
end;

procedure TFmUFs.IncluinovoICMS1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmUFsICMS, FmUFsICMS, afmoNegarComAviso,
    QrUFsICMS, stIns);
end;

procedure TFmUFs.QrUFsBeforeClose(DataSet: TDataSet);
begin
  QrUFsICMS.Close;
end;

procedure TFmUFs.QrUFsBeforeOpen(DataSet: TDataSet);
begin
  QrUFsCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmUFs.Recriapadres1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add(DELETE_FROM + ' ufs WHERE Codigo <=0 ');
  Dmod.QrUpd.ExecSQL;
  //
  DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'ufs', 'ufs', nil,
  False, True, nil, nil, nil, nil);
  LocCod(QrUfsCodigo.Value, QrUfsCodigo.Value);
  Screen.Cursor := crDefault;
end;

procedure TFmUFs.ReopenUFsICMS(UFDest: String);
begin
  QrUFsICMS.Close;
  QrUFsICMS.Params[0].AsString := QrUFsNome.Value;
  UnDmkDAC_PF.AbreQuery(QrUFsICMS, Dmod.MyDB);
  //
  if UFDest <> '' then
    QrUFsICMS.Locate('UFDest', UFDEst, [loCaseInsensitive]);
end;

end.


