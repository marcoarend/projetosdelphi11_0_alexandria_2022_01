object FmUFs: TFmUFs
  Left = 368
  Top = 194
  Caption = 'UFS-CADAS-001 :: Cadastro de UFs'
  ClientHeight = 456
  ClientWidth = 624
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 624
    Height = 360
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 57
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 52
        Top = 4
        Width = 17
        Height = 13
        Caption = 'UF:'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 88
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 44
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdDescricao: TdmkEdit
        Left = 88
        Top = 20
        Width = 524
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Descricao'
        UpdCampo = 'Descricao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 52
        Top = 20
        Width = 32
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        MaxLength = 2
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '??'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '??'
        ValWarn = False
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 296
      Width = 624
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 620
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 476
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 624
    Height = 360
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 88
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 52
        Top = 4
        Width = 17
        Height = 13
        Caption = 'UF:'
        FocusControl = DBEdit1
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 44
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsUFs
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 88
        Top = 20
        Width = 524
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Descricao'
        DataSource = DsUFs
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 52
        Top = 20
        Width = 32
        Height = 21
        DataField = 'Nome'
        DataSource = DsUFs
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 296
      Width = 624
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 216
        Top = 15
        Width = 406
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 273
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtICMS: TBitBtn
          Left = 132
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&ICMS'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtICMSClick
        end
        object BtCondicoes: TBitBtn
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&UF'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCondicoesClick
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 173
      Width = 624
      Height = 123
      Align = alBottom
      TabOrder = 2
      object DBGICMS: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 622
        Height = 121
        Align = alClient
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'UFDest'
            Title.Alignment = taCenter
            Title.Caption = 'UF'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRI_DEST'
            Title.Caption = 'Estado (UF) destino da mercadoria'
            Width = 192
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ICMS_V'
            Title.Caption = '% ICMS'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsUFsICMS
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'UFDest'
            Title.Alignment = taCenter
            Title.Caption = 'UF'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRI_DEST'
            Title.Caption = 'Estado (UF) destino da mercadoria'
            Width = 192
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ICMS_V'
            Title.Caption = '% ICMS'
            Visible = True
          end>
      end
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 360
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 207
        Height = 32
        Caption = 'Cadastro de UFs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 207
        Height = 32
        Caption = 'Cadastro de UFs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 207
        Height = 32
        Caption = 'Cadastro de UFs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 624
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsUFs: TDataSource
    DataSet = QrUFs
    Left = 40
    Top = 12
  end
  object QrUFs: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrUFsBeforeOpen
    AfterOpen = QrUFsAfterOpen
    BeforeClose = QrUFsBeforeClose
    AfterScroll = QrUFsAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM ufs ppc')
    Left = 12
    Top = 12
    object QrUFsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrUFsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 2
    end
    object QrUFsICMS_C: TFloatField
      FieldName = 'ICMS_C'
    end
    object QrUFsICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrUFsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrUFsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrUFsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrUFsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrUFsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrUFsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrUFsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrUFsDescricao: TWideStringField
      FieldName = 'Descricao'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 68
    Top = 12
  end
  object PMCondicoes: TPopupMenu
    OnPopup = PMCondicoesPopup
    Left = 344
    Top = 376
    object IncluinovaUF1: TMenuItem
      Caption = '&Inclui nova UF'
      OnClick = IncluinovaUF1Click
    end
    object AlteraUFatual1: TMenuItem
      Caption = '&Altera UF atual'
      OnClick = AlteraUFatual1Click
    end
    object ExcluiUFatual1: TMenuItem
      Caption = '&Exclui UF atual'
      Enabled = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Recriapadres1: TMenuItem
      Caption = '&Recria padr'#245'es'
      OnClick = Recriapadres1Click
    end
  end
  object QrUFsICMS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT uf.Descricao DESCRI_DEST, ui.* '
      'FROM ufsicms ui'
      'LEFT JOIN ufs uf ON uf.Nome=ui.UFDest'
      'WHERE ui.Nome=:P0')
    Left = 100
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUFsICMSNome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
    object QrUFsICMSUFDest: TWideStringField
      FieldName = 'UFDest'
      Size = 2
    end
    object QrUFsICMSICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '0.00'
    end
    object QrUFsICMSLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrUFsICMSDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrUFsICMSDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrUFsICMSUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrUFsICMSUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrUFsICMSAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrUFsICMSAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrUFsICMSDESCRI_DEST: TWideStringField
      FieldName = 'DESCRI_DEST'
    end
  end
  object DsUFsICMS: TDataSource
    DataSet = QrUFsICMS
    Left = 128
    Top = 12
  end
  object PMICMS: TPopupMenu
    OnPopup = PMICMSPopup
    Left = 412
    Top = 376
    object IncluinovoICMS1: TMenuItem
      Caption = '&Inclui novo ICMS'
      OnClick = IncluinovoICMS1Click
    end
    object AlteraICMSatua1: TMenuItem
      Caption = '&Altera ICMS atua'
      OnClick = AlteraICMSatua1Click
    end
    object ExcluiICMSatual1: TMenuItem
      Caption = '&Exclui ICMS atual'
      OnClick = ExcluiICMSatual1Click
    end
  end
  object QrMy: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF'
      'FROM controle co'
      'LEFT JOIN entidades en ON en.Codigo=co.Dono')
    Left = 16
    Top = 104
    object QrMyUF: TFloatField
      FieldName = 'UF'
    end
  end
end
