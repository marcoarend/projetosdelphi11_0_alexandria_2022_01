unit Entidade2_Copia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, ComCtrls, dmkEditDateTimePicker, dmkDBLookupComboBox,
  dmkEditCB, dmkDBEdit, dmkCheckBox, UnDmkProcFunc, dmkImage, frxClass, frxDBSet,
  ClipBrd,
  dmkMemo, dmkValUsu, ShellAPI, UnDmkEnums, dmkCompoStore, Variants,
  mySQLDirectQuery;

type
  TFmEntidade2 = class(TForm)
    PainelDados: TPanel;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtContatos: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    PMEntidades: TPopupMenu;
    BtEntidade: TBitBtn;
    BtEMails: TBitBtn;
    QrEntiMail: TmySQLQuery;
    DsEntiMail: TDataSource;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesRazaoSocial: TWideStringField;
    QrEntidadesFantasia: TWideStringField;
    QrEntidadesRespons1: TWideStringField;
    QrEntidadesRespons2: TWideStringField;
    QrEntidadesPai: TWideStringField;
    QrEntidadesMae: TWideStringField;
    QrEntidadesCNPJ: TWideStringField;
    QrEntidadesIE: TWideStringField;
    QrEntidadesNome: TWideStringField;
    QrEntidadesApelido: TWideStringField;
    QrEntidadesCPF: TWideStringField;
    QrEntidadesRG: TWideStringField;
    QrEntidadesERua: TWideStringField;
    QrEntidadesECompl: TWideStringField;
    QrEntidadesEBairro: TWideStringField;
    QrEntidadesECidade: TWideStringField;
    QrEntidadesEUF: TSmallintField;
    QrEntidadesEPais: TWideStringField;
    QrEntidadesETe1: TWideStringField;
    QrEntidadesETe2: TWideStringField;
    QrEntidadesETe3: TWideStringField;
    QrEntidadesECel: TWideStringField;
    QrEntidadesEFax: TWideStringField;
    QrEntidadesEEMail: TWideStringField;
    QrEntidadesEContato: TWideStringField;
    QrEntidadesPRua: TWideStringField;
    QrEntidadesPCompl: TWideStringField;
    QrEntidadesPBairro: TWideStringField;
    QrEntidadesPCidade: TWideStringField;
    QrEntidadesPUF: TSmallintField;
    QrEntidadesPPais: TWideStringField;
    QrEntidadesPTe1: TWideStringField;
    QrEntidadesPTe2: TWideStringField;
    QrEntidadesPTe3: TWideStringField;
    QrEntidadesPCel: TWideStringField;
    QrEntidadesPFax: TWideStringField;
    QrEntidadesPEMail: TWideStringField;
    QrEntidadesPContato: TWideStringField;
    QrEntidadesSexo: TWideStringField;
    QrEntidadesResponsavel: TWideStringField;
    QrEntidadesProfissao: TWideStringField;
    QrEntidadesCargo: TWideStringField;
    QrEntidadesRecibo: TSmallintField;
    QrEntidadesDiaRecibo: TSmallintField;
    QrEntidadesAjudaEmpV: TFloatField;
    QrEntidadesAjudaEmpP: TFloatField;
    QrEntidadesCliente1: TWideStringField;
    QrEntidadesCliente2: TWideStringField;
    QrEntidadesCliente3: TWideStringField;
    QrEntidadesCliente4: TWideStringField;
    QrEntidadesFornece1: TWideStringField;
    QrEntidadesFornece2: TWideStringField;
    QrEntidadesFornece3: TWideStringField;
    QrEntidadesFornece4: TWideStringField;
    QrEntidadesFornece5: TWideStringField;
    QrEntidadesFornece6: TWideStringField;
    QrEntidadesFornece7: TWideStringField;
    QrEntidadesFornece8: TWideStringField;
    QrEntidadesTerceiro: TWideStringField;
    QrEntidadesCadastro: TDateField;
    QrEntidadesInformacoes: TWideStringField;
    QrEntidadesVeiculo: TIntegerField;
    QrEntidadesMensal: TWideStringField;
    QrEntidadesObservacoes: TWideMemoField;
    QrEntidadesTipo: TSmallintField;
    QrEntidadesLk: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesNOMEEUF: TWideStringField;
    QrEntidadesNOMEPUF: TWideStringField;
    QrEntidadesPCPF_TXT: TWideStringField;
    QrEntidadesPTE1_TXT: TWideStringField;
    QrEntidadesPTE2_TXT: TWideStringField;
    QrEntidadesPTE3_TXT: TWideStringField;
    QrEntidadesPCEL_TXT: TWideStringField;
    QrEntidadesPFAX_TXT: TWideStringField;
    QrEntidadesETE1_TXT: TWideStringField;
    QrEntidadesETE2_TXT: TWideStringField;
    QrEntidadesETE3_TXT: TWideStringField;
    QrEntidadesECEL_TXT: TWideStringField;
    QrEntidadesEFAX_TXT: TWideStringField;
    QrEntidadesCNPJ_TXT: TWideStringField;
    QrEntidadesECEP: TIntegerField;
    QrEntidadesPCEP: TIntegerField;
    QrEntidadesGrupo: TIntegerField;
    QrEntidadesDataAlt: TDateField;
    QrEntidadesUserCad: TSmallintField;
    QrEntidadesUserAlt: TSmallintField;
    QrEntidadesCRua: TWideStringField;
    QrEntidadesCCompl: TWideStringField;
    QrEntidadesCBairro: TWideStringField;
    QrEntidadesCCidade: TWideStringField;
    QrEntidadesCUF: TSmallintField;
    QrEntidadesCCEP: TIntegerField;
    QrEntidadesCPais: TWideStringField;
    QrEntidadesCTel: TWideStringField;
    QrEntidadesCFax: TWideStringField;
    QrEntidadesCCel: TWideStringField;
    QrEntidadesCContato: TWideStringField;
    QrEntidadesLRua: TWideStringField;
    QrEntidadesLCompl: TWideStringField;
    QrEntidadesLBairro: TWideStringField;
    QrEntidadesLCidade: TWideStringField;
    QrEntidadesLUF: TSmallintField;
    QrEntidadesLCEP: TIntegerField;
    QrEntidadesLPais: TWideStringField;
    QrEntidadesLTel: TWideStringField;
    QrEntidadesLFax: TWideStringField;
    QrEntidadesLCel: TWideStringField;
    QrEntidadesLContato: TWideStringField;
    QrEntidadesComissao: TFloatField;
    QrEntidadesDataCad: TDateField;
    QrEntidadesECEP_TXT: TWideStringField;
    QrEntidadesPCEP_TXT: TWideStringField;
    QrEntidadesCCEP_TXT: TWideStringField;
    QrEntidadesLCEP_TXT: TWideStringField;
    QrEntidadesNOMECAD2: TWideStringField;
    QrEntidadesNOMEALT2: TWideStringField;
    QrEntidadesSituacao: TSmallintField;
    QrEntidadesNivel: TWideStringField;
    QrEntidadesCTEL_TXT: TWideStringField;
    QrEntidadesCFAX_TXT: TWideStringField;
    QrEntidadesCCEL_TXT: TWideStringField;
    QrEntidadesLTEL_TXT: TWideStringField;
    QrEntidadesLFAX_TXT: TWideStringField;
    QrEntidadesLCEL_TXT: TWideStringField;
    QrEntidadesNOMEENTIGRUPO: TWideStringField;
    QrEntidadesNOMECUF: TWideStringField;
    QrEntidadesNOMELUF: TWideStringField;
    QrEntidadesAccount: TIntegerField;
    QrEntidadesNOMEACCOUNT: TWideStringField;
    QrEntidadesELograd: TSmallintField;
    QrEntidadesPLograd: TSmallintField;
    QrEntidadesConjugeNome: TWideStringField;
    QrEntidadesConjugeNatal: TDateField;
    QrEntidadesNome1: TWideStringField;
    QrEntidadesNatal1: TDateField;
    QrEntidadesNome2: TWideStringField;
    QrEntidadesNatal2: TDateField;
    QrEntidadesNome3: TWideStringField;
    QrEntidadesNatal3: TDateField;
    QrEntidadesCreditosI: TIntegerField;
    QrEntidadesCreditosL: TIntegerField;
    QrEntidadesCreditosD: TDateField;
    QrEntidadesCreditosU: TDateField;
    QrEntidadesCreditosV: TDateField;
    QrEntidadesMotivo: TIntegerField;
    QrEntidadesQuantI1: TIntegerField;
    QrEntidadesQuantI2: TIntegerField;
    QrEntidadesQuantI3: TIntegerField;
    QrEntidadesQuantI4: TIntegerField;
    QrEntidadesAgenda: TWideStringField;
    QrEntidadesSenhaQuer: TWideStringField;
    QrEntidadesSenha1: TWideStringField;
    QrEntidadesNatal4: TDateField;
    QrEntidadesNome4: TWideStringField;
    QrEntidadesNOMEMOTIVO: TWideStringField;
    QrEntidadesCreditosF2: TFloatField;
    QrEntidadesNOMEELOGRAD: TWideStringField;
    QrEntidadesNOMEPLOGRAD: TWideStringField;
    QrEntidadesCLograd: TSmallintField;
    QrEntidadesLLograd: TSmallintField;
    QrEntidadesNOMECLOGRAD: TWideStringField;
    QrEntidadesNOMELLOGRAD: TWideStringField;
    QrEntidadesQuantN1: TFloatField;
    QrEntidadesQuantN2: TFloatField;
    QrEntidadesLimiCred: TFloatField;
    QrEntidadesDesco: TFloatField;
    QrEntidadesCasasApliDesco: TSmallintField;
    QrEntidadesCPF_Pai: TWideStringField;
    QrEntidadesSSP: TWideStringField;
    QrEntidadesCidadeNatal: TWideStringField;
    QrEntidadesCPF_PAI_TXT: TWideStringField;
    QrEntidadesUFNatal: TSmallintField;
    QrEntidadesNOMENUF: TWideStringField;
    QrEntidadesFatorCompra: TFloatField;
    QrEntidadesAdValorem: TFloatField;
    QrEntidadesDMaisC: TIntegerField;
    QrEntidadesDMaisD: TIntegerField;
    QrEntidadesDataRG: TDateField;
    QrEntidadesNacionalid: TWideStringField;
    QrEntidadesEmpresa: TIntegerField;
    QrEntidadesNOMEEMPRESA: TWideStringField;
    QrEntidadesFormaSociet: TWideStringField;
    QrEntidadesSimples: TSmallintField;
    QrEntidadesAtividade: TWideStringField;
    QrEntidadesEstCivil: TSmallintField;
    QrEntidadesNOMEECIVIL: TWideStringField;
    QrEntidadesCPF_Conjuge: TWideStringField;
    QrEntidadesCBE: TIntegerField;
    QrEntidadesSCB: TIntegerField;
    QrEntidadesCPF_Resp1: TWideStringField;
    QrEntidadesCPF_Resp1_TXT: TWideStringField;
    QrEntidadesENumero: TIntegerField;
    QrEntidadesPNumero: TIntegerField;
    QrEntidadesCNumero: TIntegerField;
    QrEntidadesLNumero: TIntegerField;
    QrEntidadesBanco: TIntegerField;
    QrEntidadesAgencia: TWideStringField;
    QrEntidadesContaCorrente: TWideStringField;
    QrEntidadesCartPref: TIntegerField;
    QrEntidadesNOMECARTPREF: TWideStringField;
    QrEntidadesRolComis: TIntegerField;
    QrEntidadesFilial: TIntegerField;
    DsEntidades: TDataSource;
    Incluinovaentidade1: TMenuItem;
    Alteraentidadeatual1: TMenuItem;
    Excluientidadeatual1: TMenuItem;
    PCEditGer: TPageControl;
    TabSheet1: TTabSheet;
    PnEdit2: TPanel;
    PnEdit1: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    RGTipo: TdmkRadioGroup;
    PnEdits: TPanel;
    PnEdit2PF: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    Label8: TLabel;
    EdNome: TdmkEdit;
    EdCPF: TdmkEdit;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    Panel8: TPanel;
    Label12: TLabel;
    EdPCEP: TdmkEdit;
    PnEdit2PJ: TPanel;
    Panel7: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label11: TLabel;
    EdRazaoSocial: TdmkEdit;
    EdFantasia: TdmkEdit;
    EdCNPJ: TdmkEdit;
    Label97: TLabel;
    CBPLograd: TdmkDBLookupComboBox;
    EdPRua: TdmkEdit;
    Label31: TLabel;
    EdPNumero: TdmkEdit;
    Label32: TLabel;
    Label34: TLabel;
    EdPBairro: TdmkEdit;
    EdPLograd: TdmkEditCB;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    DsEListaLograd: TDataSource;
    DsLListaLograd: TDataSource;
    DsCListaLograd: TDataSource;
    DsPListaLograd: TDataSource;
    PMCEP: TPopupMenu;
    Descobrir1: TMenuItem;
    Verificar1: TMenuItem;
    BtCEP_P: TBitBtn;
    Label16: TLabel;
    EdPCompl: TdmkEdit;
    EdPEndeRef: TdmkEdit;
    Label17: TLabel;
    QrEntidadesIEST: TWideStringField;
    QrEntidadesQuantN3: TFloatField;
    QrEntidadesTempA: TFloatField;
    QrEntidadesTempD: TFloatField;
    QrEntidadesPAtividad: TIntegerField;
    QrEntidadesEAtividad: TIntegerField;
    QrEntidadesPCidadeCod: TIntegerField;
    QrEntidadesECidadeCod: TIntegerField;
    QrEntidadesPPaisCod: TIntegerField;
    QrEntidadesEPaisCod: TIntegerField;
    QrEntidadesAntigo: TWideStringField;
    QrEntidadesCUF2: TWideStringField;
    QrEntidadesContab: TWideStringField;
    QrEntidadesMSN1: TWideStringField;
    QrEntidadesPastaTxtFTP: TWideStringField;
    QrEntidadesPastaPwdFTP: TWideStringField;
    QrEntidadesProtestar: TSmallintField;
    QrEntidadesMultaCodi: TSmallintField;
    QrEntidadesMultaDias: TSmallintField;
    QrEntidadesMultaValr: TFloatField;
    QrEntidadesMultaPerc: TFloatField;
    QrEntidadesMultaTiVe: TSmallintField;
    QrEntidadesJuroSacado: TFloatField;
    QrEntidadesCPMF: TFloatField;
    QrEntidadesCorrido: TIntegerField;
    QrEntidadesCliInt: TIntegerField;
    QrEntidadesAltDtPlaCt: TDateField;
    QrEntidadesAlterWeb: TSmallintField;
    QrEntidadesAtivo: TSmallintField;
    QrEntidadesEEndeRef: TWideStringField;
    QrEntidadesPEndeRef: TWideStringField;
    QrEntidadesCEndeRef: TWideStringField;
    QrEntidadesLEndeRef: TWideStringField;
    PageControl3: TPageControl;
    TabSheet3: TTabSheet;
    Panel9: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    BtCEP_E: TBitBtn;
    EdECep: TdmkEdit;
    CBELograd: TdmkDBLookupComboBox;
    EdERua: TdmkEdit;
    EdENumero: TdmkEdit;
    EdEBairro: TdmkEdit;
    EdECompl: TdmkEdit;
    EdEEndeRef: TdmkEdit;
    PMContatos: TPopupMenu;
    Incluinovocontato1: TMenuItem;
    AlteraContatoatual1: TMenuItem;
    Excluicontatos1: TMenuItem;
    DsEntiTel: TDataSource;
    QrEntiTel: TmySQLQuery;
    QrEntidadesCodUsu: TIntegerField;
    PMEMails: TPopupMenu;
    NovoEMail1: TMenuItem;
    Alteraemailselecionado1: TMenuItem;
    Excluiemailselecionado1: TMenuItem;
    QrEntiMailNOMEETC: TWideStringField;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailEntiTipCto: TIntegerField;
    QrEntiTelNOMEETC: TWideStringField;
    QrEntiTelConta: TIntegerField;
    QrEntiTelTelefone: TWideStringField;
    QrEntiTelEntiTipCto: TIntegerField;
    QrEntiTelTEL_TXT: TWideStringField;
    BtTelefones: TBitBtn;
    PMTelefones: TPopupMenu;
    Incluinovotelefone1: TMenuItem;
    Alteratelefoneatual1: TMenuItem;
    Excluitelefones1: TMenuItem;
    QrEntiTelRamal: TWideStringField;
    EdCodUsu: TdmkEdit;
    Label29: TLabel;
    SpeedButton5: TSpeedButton;
    Panel11: TPanel;
    Panel12: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    SpeedButton6: TSpeedButton;
    dmkDBEdit1: TdmkDBEdit;
    dmkRadioGroup1: TDBRadioGroup;
    dmkDBEdit2: TdmkDBEdit;
    PnShowEnd: TPanel;
    PCShowGer: TPageControl;
    TabSheet4: TTabSheet;
    PnShow2Pj: TPanel;
    TabSheet7: TTabSheet;
    Panel13: TPanel;
    GroupBox1: TGroupBox;
    CkCliente1: TDBCheckBox;
    CkFornece1: TDBCheckBox;
    CkFornece2: TDBCheckBox;
    CkFornece3: TDBCheckBox;
    CkTerceiro: TDBCheckBox;
    CkFornece4: TDBCheckBox;
    CkCliente2: TDBCheckBox;
    CkFornece5: TDBCheckBox;
    CkFornece6: TDBCheckBox;
    CkCliente3: TDBCheckBox;
    CkCliente4: TDBCheckBox;
    CkFornece8: TDBCheckBox;
    CkFornece7: TDBCheckBox;
    TabSheet8: TTabSheet;
    QrEntidadesPNUMERO_TXT: TWideStringField;
    QrEntidadesENUMERO_TXT: TWideStringField;
    QrEntidadesCNUMERO_TXT: TWideStringField;
    QrEntidadesLNUMERO_TXT: TWideStringField;
    QrEntidadesIE_TXT: TWideStringField;
    QrEntidadesDATARG_TXT: TWideStringField;
    QrDuplic2: TmySQLQuery;
    QrDuplic2Codigo: TIntegerField;
    QrNext: TmySQLQuery;
    QrNextCodUsu: TIntegerField;
    Label10: TLabel;
    EdIE: TdmkEdit;
    PMAtributos: TPopupMenu;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    Panel4: TPanel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label70: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    dmkDBEdit35: TdmkDBEdit;
    dmkDBEdit36: TdmkDBEdit;
    dmkDBEdit37: TdmkDBEdit;
    dmkDBEdit38: TdmkDBEdit;
    dmkDBEdit39: TdmkDBEdit;
    dmkDBEdit41: TdmkDBEdit;
    dmkDBEdit43: TdmkDBEdit;
    dmkDBEdit44: TdmkDBEdit;
    dmkDBEdit45: TdmkDBEdit;
    Panel14: TPanel;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label80: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    dmkDBEdit46: TdmkDBEdit;
    dmkDBEdit47: TdmkDBEdit;
    dmkDBEdit48: TdmkDBEdit;
    dmkDBEdit49: TdmkDBEdit;
    dmkDBEdit50: TdmkDBEdit;
    dmkDBEdit52: TdmkDBEdit;
    dmkDBEdit54: TdmkDBEdit;
    dmkDBEdit55: TdmkDBEdit;
    dmkDBEdit56: TdmkDBEdit;
    TabSheet13: TTabSheet;
    Panel17: TPanel;
    Label84: TLabel;
    Label85: TLabel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label92: TLabel;
    Label93: TLabel;
    BtCEP_C: TBitBtn;
    EdCCep: TdmkEdit;
    CBCLograd: TdmkDBLookupComboBox;
    EdCRua: TdmkEdit;
    EdCNumero: TdmkEdit;
    EdCBairro: TdmkEdit;
    EdCLograd: TdmkEditCB;
    EdCCompl: TdmkEdit;
    EdCEndeRef: TdmkEdit;
    TabSheet14: TTabSheet;
    Panel20: TPanel;
    Label94: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label103: TLabel;
    Label104: TLabel;
    BtCEP_L: TBitBtn;
    EdLCep: TdmkEdit;
    CBLLograd: TdmkDBLookupComboBox;
    EdLRua: TdmkEdit;
    EdLNumero: TdmkEdit;
    EdLBairro: TdmkEdit;
    EdLLograd: TdmkEditCB;
    EdLCompl: TdmkEdit;
    EdLEndeRef: TdmkEdit;
    Panel21: TPanel;
    GroupBox4: TGroupBox;
    CkCliente1_0: TdmkCheckBox;
    CkFornece1_0: TdmkCheckBox;
    CkFornece2_0: TdmkCheckBox;
    CkFornece3_0: TdmkCheckBox;
    CkFornece4_0: TdmkCheckBox;
    CkTerceiro_0: TdmkCheckBox;
    CkCliente2_0: TdmkCheckBox;
    CkFornece5_0: TdmkCheckBox;
    CkFornece6_0: TdmkCheckBox;
    CkCliente4_0: TdmkCheckBox;
    CkCliente3_0: TdmkCheckBox;
    CkFornece8_0: TdmkCheckBox;
    CkFornece7_0: TdmkCheckBox;
    QrEntiRespon: TmySQLQuery;
    DsEntiRespon: TDataSource;
    PMRespon: TPopupMenu;
    Incluinovoresponsvel1: TMenuItem;
    Alteraresponsvelatual1: TMenuItem;
    Excluiresponsveleis1: TMenuItem;
    QrEntiResponCodigo: TIntegerField;
    QrEntiResponControle: TIntegerField;
    QrEntiResponNome: TWideStringField;
    QrEntiResponCargo: TIntegerField;
    QrEntiResponAssina: TSmallintField;
    QrEntiResponOrdemLista: TIntegerField;
    QrEntiResponObserv: TWideStringField;
    QrEntiResponNOME_CARGO: TWideStringField;
    QrEntiResponNO_ASSINA: TWideStringField;
    EdECodMunici: TdmkEditCB;
    CBECodMunici: TdmkDBLookupComboBox;
    Label105: TLabel;
    QrEMunici: TmySQLQuery;
    DsEMunici: TDataSource;
    QrEMuniciCodigo: TIntegerField;
    QrEMuniciNome: TWideStringField;
    QrEntidadesECodMunici: TIntegerField;
    QrEntidadesPCodMunici: TIntegerField;
    QrEntidadesCCodMunici: TIntegerField;
    QrEntidadesLCodMunici: TIntegerField;
    QrEntidadesCNAE: TWideStringField;
    QrEntidadesSUFRAMA: TWideStringField;
    Label106: TLabel;
    EdPCodMunici: TdmkEditCB;
    CBPCodMunici: TdmkDBLookupComboBox;
    Label107: TLabel;
    EdCCodMunici: TdmkEditCB;
    CBCCodMunici: TdmkDBLookupComboBox;
    Label108: TLabel;
    EdLCodMunici: TdmkEditCB;
    CBLCodMunici: TdmkDBLookupComboBox;
    DsLMunici: TDataSource;
    DsCMunici: TDataSource;
    DsPMunici: TDataSource;
    QrPMunici: TmySQLQuery;
    QrCMunici: TmySQLQuery;
    QrLMunici: TmySQLQuery;
    QrPMuniciCodigo: TIntegerField;
    QrPMuniciNome: TWideStringField;
    QrCMuniciCodigo: TIntegerField;
    QrCMuniciNome: TWideStringField;
    QrLMuniciCodigo: TIntegerField;
    QrLMuniciNome: TWideStringField;
    Label109: TLabel;
    EdPCodiPais: TdmkEditCB;
    CBPCodiPais: TdmkDBLookupComboBox;
    Label110: TLabel;
    EdECodiPais: TdmkEditCB;
    CBECodiPais: TdmkDBLookupComboBox;
    QrEBacen_Pais: TmySQLQuery;
    DsEBacen_Pais: TDataSource;
    QrEBacen_PaisCodigo: TIntegerField;
    QrEBacen_PaisNome: TWideStringField;
    DsPBacen_Pais: TDataSource;
    QrPBacen_Pais: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    QrEntidadesECodiPais: TIntegerField;
    QrEntidadesPCodiPais: TIntegerField;
    Label112: TLabel;
    EdPTe1: TdmkEdit;
    Label113: TLabel;
    EdL_CNPJ: TdmkEdit;
    RGL_Ativo: TdmkRadioGroup;
    QrEntidadesL_CNPJ: TWideStringField;
    QrEntidadesL_Ativo: TSmallintField;
    Label114: TLabel;
    DBEdit1: TDBEdit;
    QrEntidadesL_CNPJ_TXT: TWideStringField;
    DBRadioGroup1: TDBRadioGroup;
    Label115: TLabel;
    DBEdit2: TDBEdit;
    QrEntidadesNO_DTB_EMUNICI: TWideStringField;
    QrEntidadesNO_DTB_PMUNICI: TWideStringField;
    QrEntidadesNO_DTB_CMUNICI: TWideStringField;
    QrEntidadesNO_DTB_LMUNICI: TWideStringField;
    QrEntidadesNO_BACEN_EPAIS: TWideStringField;
    QrEntidadesNO_BACEN_PPAIS: TWideStringField;
    DBEdit3: TDBEdit;
    Label124: TLabel;
    EdCodCNAE: TdmkEditCB;
    CBCodCNAE: TdmkDBLookupComboBox;
    QrCNAE21Cad: TmySQLQuery;
    DsCNAE21Cad: TDataSource;
    QrEntidadesCNAE_Nome: TWideStringField;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    DsCarteiras: TDataSource;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    Label127: TLabel;
    Label128: TLabel;
    dmkDBEdit57: TdmkDBEdit;
    PMDBanc: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    QrEntiCtas: TmySQLQuery;
    QrEntiCtasCodigo: TIntegerField;
    QrEntiCtasControle: TIntegerField;
    QrEntiCtasOrdem: TIntegerField;
    QrEntiCtasBanco: TIntegerField;
    QrEntiCtasAgencia: TIntegerField;
    QrEntiCtasContaCor: TWideStringField;
    QrEntiCtasContaTip: TWideStringField;
    QrEntiCtasDAC_A: TWideStringField;
    QrEntiCtasDAC_C: TWideStringField;
    QrEntiCtasDAC_AC: TWideStringField;
    QrEntiCtasLk: TIntegerField;
    QrEntiCtasDataCad: TDateField;
    QrEntiCtasDataAlt: TDateField;
    QrEntiCtasUserCad: TIntegerField;
    QrEntiCtasUserAlt: TIntegerField;
    QrEntiCtasAlterWeb: TSmallintField;
    QrEntiCtasAtivo: TSmallintField;
    QrEntiCtasNOMEBANCO: TWideStringField;
    DsEntiCtas: TDataSource;
    BitBtn1: TBitBtn;
    QrEntidadesNIRE: TWideStringField;
    EdELograd: TdmkEditCB;
    Label129: TLabel;
    EdIEST: TdmkEdit;
    EdNIRE: TdmkEdit;
    Label130: TLabel;
    QrEntidadesIEST_TXT: TWideStringField;
    QrEntiResponMandatoIni: TDateField;
    QrEntiResponMandatoFim: TDateField;
    QrEntiResponMandatoIni_TXT: TWideStringField;
    QrEntiResponMandatoFim_TXT: TWideStringField;
    QrEntiMailOrdem: TIntegerField;
    PMQuery: TPopupMenu;
    LocalizarEntidades1: TMenuItem;
    Nome1: TMenuItem;
    Dados2: TMenuItem;
    LocalizarClienteporNotaFiscal1: TMenuItem;
    porNotaFiscal1: TMenuItem;
    Produtoquecompra1: TMenuItem;
    LocalizarFornecedorporNoitaFiscal1: TMenuItem;
    porNotaFiscal2: TMenuItem;
    Produtoquevende1: TMenuItem;
    N1: TMenuItem;
    ConferirCdigosdeusurio1: TMenuItem;
    CNPJCPF1: TMenuItem;
    QrEntidadesURL: TWideStringField;
    QrEntidadesCRT: TSmallintField;
    QrEntidadesCOD_PART: TWideStringField;
    Label139: TLabel;
    EdSUFRAMA: TdmkEdit;
    Label137: TLabel;
    EdCOD_PART: TdmkEdit;
    Label138: TLabel;
    dmkDBEdit58: TdmkDBEdit;
    dmkDBEdit59: TdmkDBEdit;
    Label140: TLabel;
    QrEntiContat: TmySQLQuery;
    DsEntiContat: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel22: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Internet1: TMenuItem;
    SbMapa: TBitBtn;
    QrEntiSrvPro: TmySQLQuery;
    DsEntiSrvPro: TDataSource;
    QrEntiSrvProNO_ORGAO: TWideStringField;
    QrEntiSrvProCodigo: TIntegerField;
    QrEntiSrvProControle: TIntegerField;
    QrEntiSrvProOrgao: TIntegerField;
    QrEntiSrvProData: TDateField;
    QrEntiSrvProHora: TTimeField;
    QrEntiSrvProNumero: TWideStringField;
    QrEntiSrvProResultado: TWideStringField;
    PMEntiSrvPro: TPopupMenu;
    Inclui4: TMenuItem;
    Altera4: TMenuItem;
    Exclui4: TMenuItem;
    QrCNAE21CadCodAlf: TWideStringField;
    QrCNAE21CadNome: TWideStringField;
    CkAtivo: TdmkCheckBox;
    DBCheckBox2: TDBCheckBox;
    PMImprime: TPopupMenu;
    Pesquisas1: TMenuItem;
    Dados1: TMenuItem;
    Dadoscompactosdaentidadeatual1: TMenuItem;
    frxDsCadastro: TfrxDBDataset;
    frxCadastro: TfrxReport;
    QrCadastro2: TmySQLQuery;
    QrCadastro2Codigo: TIntegerField;
    QrCadastro2Tipo: TSmallintField;
    QrCadastro2NOMEENTIDADE: TWideStringField;
    QrCadastro2FANTASIA_APELIDO: TWideStringField;
    QrCadastro2RUA: TWideStringField;
    QrCadastro2COMPL: TWideStringField;
    QrCadastro2BAIRRO: TWideStringField;
    QrCadastro2CIDADE: TWideStringField;
    QrCadastro2PAIS: TWideStringField;
    QrCadastro2TE1: TWideStringField;
    QrCadastro2TE2: TWideStringField;
    QrCadastro2TE3: TWideStringField;
    QrCadastro2CEL: TWideStringField;
    QrCadastro2FAX: TWideStringField;
    QrCadastro2EMAIL: TWideStringField;
    QrCadastro2CONTATO: TWideStringField;
    QrCadastro2DOCA: TWideStringField;
    QrCadastro2DOCB: TWideStringField;
    QrCadastro2Sexo: TWideStringField;
    QrCadastro2NOMEUF: TWideStringField;
    QrCadastro2CRua: TWideStringField;
    QrCadastro2CCompl: TWideStringField;
    QrCadastro2CCEP: TIntegerField;
    QrCadastro2CBairro: TWideStringField;
    QrCadastro2CCidade: TWideStringField;
    QrCadastro2CUF: TSmallintField;
    QrCadastro2CPais: TWideStringField;
    QrCadastro2LRua: TWideStringField;
    QrCadastro2LCompl: TWideStringField;
    QrCadastro2LCEP: TIntegerField;
    QrCadastro2LBairro: TWideStringField;
    QrCadastro2LCidade: TWideStringField;
    QrCadastro2LUF: TSmallintField;
    QrCadastro2LPais: TWideStringField;
    QrCadastro2TE1_TXT: TWideStringField;
    QrCadastro2TE2_TXT: TWideStringField;
    QrCadastro2TE3_TXT: TWideStringField;
    QrCadastro2FAX_TXT: TWideStringField;
    QrCadastro2CEL_TXT: TWideStringField;
    QrCadastro2DOCA_TXT: TWideStringField;
    QrCadastro2Cargo: TWideStringField;
    QrCadastro2Profissao: TWideStringField;
    QrCadastro2NUM_TXT: TWideStringField;
    QrCadastro2CEP_TXT: TWideStringField;
    QrCadastro2CNUM_TXT: TWideStringField;
    QrCadastro2LNUM_TXT: TWideStringField;
    QrCadastro2NOMECUF: TWideStringField;
    QrCadastro2NOMELUF: TWideStringField;
    QrCadastro2CCEP_TXT: TWideStringField;
    QrCadastro2LCEP_TXT: TWideStringField;
    QrCadastro2NATAL_TXT: TWideStringField;
    QrCadastro2ENatal: TDateField;
    QrCadastro2PNatal: TDateField;
    QrCadastro2NUMERO: TFloatField;
    QrCadastro2CNumero: TIntegerField;
    QrCadastro2LNumero: TIntegerField;
    frxDsCadastro2: TfrxDBDataset;
    frxCadastro2: TfrxReport;
    QrCadastro2CEP: TFloatField;
    QrCadastro2UF: TFloatField;
    Dadosdestaentidade2012111: TMenuItem;
    SbImagens: TBitBtn;
    TabSheet18: TTabSheet;
    MeObservacoes_Edit: TdmkMemo;
    TabSheet19: TTabSheet;
    EdPTe1Tip: TdmkEditCB;
    CBPTe1Tip: TdmkDBLookupComboBox;
    Label143: TLabel;
    Label144: TLabel;
    EdPCelTip: TdmkEditCB;
    CBPCelTip: TdmkDBLookupComboBox;
    EdPCel: TdmkEdit;
    Label145: TLabel;
    Memo1: TMemo;
    Label134: TLabel;
    EdCRT: TdmkEdit;
    DBEdit5: TDBEdit;
    DBEdit4: TDBEdit;
    Label116: TLabel;
    Label148: TLabel;
    EdETe1Tip: TdmkEditCB;
    CBETe1Tip: TdmkDBLookupComboBox;
    Label149: TLabel;
    EdETe1: TdmkEdit;
    EdECelTip: TdmkEditCB;
    Label150: TLabel;
    CBECelTip: TdmkDBLookupComboBox;
    EdECel: TdmkEdit;
    Label151: TLabel;
    QrECelTip: TmySQLQuery;
    QrECelTipCodigo: TIntegerField;
    QrECelTipCodUsu: TIntegerField;
    QrECelTipNome: TWideStringField;
    DsECelTip: TDataSource;
    QrPCelTip: TmySQLQuery;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    StringField2: TWideStringField;
    DsPCelTip: TDataSource;
    QrETe1Tip: TmySQLQuery;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    StringField3: TWideStringField;
    DsETe1Tip: TDataSource;
    QrPTe1Tip: TmySQLQuery;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    StringField4: TWideStringField;
    DsPTe1Tip: TDataSource;
    QrEntidadesETe1Tip: TIntegerField;
    QrEntidadesECelTip: TIntegerField;
    QrEntidadesPTe1Tip: TIntegerField;
    QrEntidadesPCelTip: TIntegerField;
    QrEntidadesPTe1Tip_TXT: TWideStringField;
    QrEntidadesPCelTip_TXT: TWideStringField;
    QrEntidadesETe1Tip_TXT: TWideStringField;
    QrEntidadesECelTip_TXT: TWideStringField;
    Label6: TLabel;
    EdApelido: TdmkEdit;
    Label9: TLabel;
    EdRG: TdmkEdit;
    Label13: TLabel;
    EdSSP: TdmkEdit;
    Label35: TLabel;
    TPDataRG: TdmkEditDateTimePicker;
    SpeedButton7: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton10: TSpeedButton;
    Label45: TLabel;
    TPENatal: TdmkEditDateTimePicker;
    Label14: TLabel;
    TPPNatal: TdmkEditDateTimePicker;
    EdFilial: TdmkEdit;
    LaFilial: TLabel;
    QrEntidadesENATAL_TXT: TWideStringField;
    QrEntidadesPNATAL_TXT: TWideStringField;
    VUETe1Tip: TdmkValUsu;
    VUPTe1Tip: TdmkValUsu;
    VUPCelTip: TdmkValUsu;
    VUECelTip: TdmkValUsu;
    PMImporta: TPopupMenu;
    ReceitaFederal1: TMenuItem;
    ReceitaEstadual1: TMenuItem;
    XMLNFe1: TMenuItem;
    elefone1: TMenuItem;
    EdEUF: TdmkEdit;
    Label24: TLabel;
    EdPUF: TdmkEdit;
    Label38: TLabel;
    EdCUF: TdmkEdit;
    Label90: TLabel;
    EdLUF: TdmkEdit;
    Label101: TLabel;
    QrEntidadesENatal: TDateField;
    QrEntidadesPNatal: TDateField;
    BtImporta: TBitBtn;
    Gerenciacontatos1: TMenuItem;
    N2: TMenuItem;
    Incluinovocontatoporatrelamento1: TMenuItem;
    N3: TMenuItem;
    IncluiAtributo1: TMenuItem;
    AlteraAtributo1: TMenuItem;
    ExcluiAtributo1: TMenuItem;
    QrAtrEntiDef: TmySQLQuery;
    QrAtrEntiDefID_Item: TIntegerField;
    QrAtrEntiDefID_Sorc: TIntegerField;
    QrAtrEntiDefAtrCad: TIntegerField;
    QrAtrEntiDefATRITS: TFloatField;
    QrAtrEntiDefCU_CAD: TIntegerField;
    QrAtrEntiDefCU_ITS: TLargeintField;
    QrAtrEntiDefNO_CAD: TWideStringField;
    QrAtrEntiDefNO_ITS: TWideStringField;
    QrAtrEntiDefAtrTyp: TSmallintField;
    QrAtrEntiDefAtrTxt: TWideStringField;
    DsAtrEntiDef: TDataSource;
    QrEntidadesESite: TWideStringField;
    QrEntidadesPSite: TWideStringField;
    Label79: TLabel;
    EdESite: TdmkEdit;
    Label81: TLabel;
    EdPEmail: TdmkEdit;
    CSTabSheetChamou: TdmkCompoStore;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    QrEntiContatCargo: TIntegerField;
    QrEntiContatNOME_CARGO: TWideStringField;
    QrEntiContatDtaNatal: TDateField;
    QrEntiContatDTANATAL_TXT: TWideStringField;
    QrEntiContatSexo: TSmallintField;
    QrEntiContatSEXO_TXT: TWideStringField;
    QrEntiContatCodigo: TIntegerField;
    Label111: TLabel;
    EdEEmail: TdmkEdit;
    Label123: TLabel;
    EdPSite: TdmkEdit;
    QrEntiResponTe1: TWideStringField;
    QrEntiResponCel: TWideStringField;
    QrEntiResponEmail: TWideStringField;
    QrEntiResponEntidade: TIntegerField;
    QrEntiResponTe1_TXT: TWideStringField;
    QrEntiResponCel_TXT: TWideStringField;
    TabSheet20: TTabSheet;
    DBGrid3: TDBGrid;
    QrCondImov: TmySQLQuery;
    QrCondImovUnidade: TWideStringField;
    QrCondImovCliente: TIntegerField;
    QrCondImovNO_CND: TWideStringField;
    QrCondImovTIPO: TWideStringField;
    DsCondImov: TDataSource;
    TabSheet21: TTabSheet;
    TabSheet22: TTabSheet;
    Panel23: TPanel;
    CkEstrangDef: TdmkCheckBox;
    GBEstrangeiro: TGroupBox;
    Panel24: TPanel;
    RGEstrangTip: TdmkRadioGroup;
    EdEstrangNum: TdmkEdit;
    Label135: TLabel;
    LaindIEDest: TLabel;
    EdindIEDest: TdmkEdit;
    EdindIEDest_TXT: TEdit;
    DBEdindIEDest_TXT: TEdit;
    QrEntidadesindIEDest: TSmallintField;
    DBEdindIEDest: TDBEdit;
    Panel25: TPanel;
    DBGBEstrangeiro: TGroupBox;
    Panel26: TPanel;
    Label147: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBRadioGroup2: TDBRadioGroup;
    QrEntidadesEstrangDef: TSmallintField;
    QrEntidadesEstrangTip: TSmallintField;
    QrEntidadesEstrangNum: TWideStringField;
    DBEdit25: TDBEdit;
    QrEntiContatTipo: TIntegerField;
    QrEntiContatCNPJ: TWideStringField;
    QrEntiContatCPF: TWideStringField;
    QrEntiContatAplicacao: TIntegerField;
    QrEntiContatCNPJ_CPF: TWideStringField;
    DBLaindIEDest: TLabel;
    PB1: TProgressBar;
    QrEstCivil: TmySQLQuery;
    QrEstCivilCodigo: TIntegerField;
    QrEstCivilNome: TWideStringField;
    QrEstCivilLk: TIntegerField;
    QrEstCivilDataCad: TDateField;
    QrEstCivilDataAlt: TDateField;
    QrEstCivilUserCad: TIntegerField;
    QrEstCivilUserAlt: TIntegerField;
    DsEstCivil: TDataSource;
    Label146: TLabel;
    EdEstCivil: TdmkEditCB;
    CBEstCivil: TdmkDBLookupComboBox;
    EdNacionalid: TdmkEdit;
    Label141: TLabel;
    EdEFax: TdmkEdit;
    Label155: TLabel;
    Label154: TLabel;
    EdProfissao: TdmkEdit;
    EdCargo: TdmkEdit;
    Label157: TLabel;
    CBPEmpresa: TdmkDBLookupComboBox;
    EdPEmpresa: TdmkEditCB;
    Label160: TLabel;
    QrEmpresas: TmySQLQuery;
    QrEmpresasRazaoSocial: TWideStringField;
    QrEmpresasCodigo: TIntegerField;
    DsEmpresas: TDataSource;
    SBAssinaturas: TBitBtn;
    PMConfig: TPopupMenu;
    Atualiza9dgito1: TMenuItem;
    QrEntiContatAtivo: TSmallintField;
    SBConfig: TBitBtn;
    TabSheet23: TTabSheet;
    PnContatos: TPanel;
    Splitter1: TSplitter;
    DBGEntiContat: TdmkDBGrid;
    Panel10: TPanel;
    Splitter2: TSplitter;
    DBGEntiMail: TdmkDBGrid;
    DBGEntiTel: TdmkDBGrid;
    TabSheet9: TTabSheet;
    GradeDefAtr: TdmkDBGrid;
    TabSheet24: TTabSheet;
    Label125: TLabel;
    DBGEntiRespon: TDBGrid;
    TabSheet25: TTabSheet;
    DBGEntiCtas: TDBGrid;
    TabSheet10: TTabSheet;
    DBGEntiSrvPro: TDBGrid;
    PnShow2PF: TPanel;
    PCShowRes: TPageControl;
    TabSheet6: TTabSheet;
    Panel16: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label46: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label119: TLabel;
    Label120: TLabel;
    Label47: TLabel;
    Label61: TLabel;
    Label69: TLabel;
    Label71: TLabel;
    Label59: TLabel;
    Label91: TLabel;
    SBPSite: TSpeedButton;
    Label102: TLabel;
    Label152: TLabel;
    Label153: TLabel;
    Label158: TLabel;
    Label159: TLabel;
    Label161: TLabel;
    dmkDBEdit8: TdmkDBEdit;
    dmkDBEdit9: TdmkDBEdit;
    dmkDBEdit10: TdmkDBEdit;
    dmkDBEdit11: TdmkDBEdit;
    dmkDBEdit12: TdmkDBEdit;
    dmkDBEdit14: TdmkDBEdit;
    dmkDBEdit16: TdmkDBEdit;
    dmkDBEdit17: TdmkDBEdit;
    dmkDBEdit18: TdmkDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    dmkDBEdit13: TdmkDBEdit;
    dmkDBEdit28: TdmkDBEdit;
    dmkDBEdit40: TdmkDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    dmkDBEdit42: TdmkDBEdit;
    PCShowCom: TPageControl;
    TabSheet5: TTabSheet;
    Panel19: TPanel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label60: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label117: TLabel;
    Label121: TLabel;
    Label126: TLabel;
    Label131: TLabel;
    Label132: TLabel;
    Label133: TLabel;
    Label136: TLabel;
    Label118: TLabel;
    Label122: TLabel;
    Label15: TLabel;
    Label25: TLabel;
    Label23: TLabel;
    SBESite: TSpeedButton;
    Label89: TLabel;
    Label100: TLabel;
    Label156: TLabel;
    Label142: TLabel;
    dmkDBEdit23: TdmkDBEdit;
    dmkDBEdit24: TdmkDBEdit;
    dmkDBEdit25: TdmkDBEdit;
    dmkDBEdit26: TdmkDBEdit;
    dmkDBEdit27: TdmkDBEdit;
    dmkDBEdit29: TdmkDBEdit;
    dmkDBEdit31: TdmkDBEdit;
    dmkDBEdit32: TdmkDBEdit;
    dmkDBEdit33: TdmkDBEdit;
    DBEdit6: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    Memo2: TMemo;
    DBEdCRT: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit23: TDBEdit;
    dmkDBEdit15: TdmkDBEdit;
    dmkDBEdit30: TdmkDBEdit;
    DBEdit28: TDBEdit;
    dmkDBEdit60: TdmkDBEdit;
    Panel15: TPanel;
    PnShow2PF2: TPanel;
    Label28: TLabel;
    Label30: TLabel;
    Label33: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label39: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit34: TdmkDBEdit;
    PnShow2Pj2: TPanel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    dmkDBEdit19: TdmkDBEdit;
    dmkDBEdit20: TdmkDBEdit;
    dmkDBEdit21: TdmkDBEdit;
    dmkDBEdit22: TdmkDBEdit;
    TabSheet15: TTabSheet;
    Panel18: TPanel;
    BtGrupos: TBitBtn;
    DBGrid1: TDBGrid;
    QrEntiGrupos: TmySQLQuery;
    DsEntiGrupos: TDataSource;
    QrEntiGruposCodigo: TIntegerField;
    QrEntiGruposNome: TWideStringField;
    QrEntiContatEntidade: TIntegerField;
    CBSexo: TComboBox;
    Label162: TLabel;
    VUSexo: TdmkValUsu;
    Label163: TLabel;
    DBEdit48: TDBEdit;
    TabSheet16: TTabSheet;
    Panel27: TPanel;
    BtDBanc: TBitBtn;
    Panel28: TPanel;
    BtAtributos: TBitBtn;
    Panel29: TPanel;
    BtRespon: TBitBtn;
    Panel30: TPanel;
    BtEntiSrvPro: TBitBtn;
    DBGEntiTransp: TDBGrid;
    Panel31: TPanel;
    BtEntiTransp: TBitBtn;
    QrEntiTransp: TmySQLQuery;
    QrEntiTranspCodigo: TIntegerField;
    QrEntiTranspConta: TIntegerField;
    QrEntiTranspOrdem: TIntegerField;
    QrEntiTranspTransp: TIntegerField;
    QrEntiTranspNOMEENTIDADE: TWideStringField;
    DsEntiTransp: TDataSource;
    N4: TMenuItem;
    DefinircomofonedaNFe1: TMenuItem;
    PMEntiTransp: TPopupMenu;
    Inclui2: TMenuItem;
    Remove1: TMenuItem;
    Ordena1: TMenuItem;
    N5: TMenuItem;
    BtPrazos: TBitBtn;
    EdLCodiPais: TdmkEditCB;
    Label164: TLabel;
    CBLCodiPais: TdmkDBLookupComboBox;
    QrLBacen_Pais: TmySQLQuery;
    DsLBacen_Pais: TDataSource;
    QrLBacen_PaisCodigo: TIntegerField;
    QrLBacen_PaisNome: TWideStringField;
    EdL_CPF: TdmkEdit;
    Label165: TLabel;
    Label166: TLabel;
    EdL_Nome: TdmkEdit;
    EdLEmail: TdmkEdit;
    Label167: TLabel;
    EdL_IE: TdmkEdit;
    Label168: TLabel;
    QrEntidadesL_CPF: TWideStringField;
    QrEntidadesL_Nome: TWideStringField;
    QrEntidadesLCodiPais: TIntegerField;
    QrEntidadesLEmail: TWideStringField;
    QrEntidadesL_IE: TWideStringField;
    QrEntidadesNO_BACEN_LPAIS: TWideStringField;
    Label169: TLabel;
    Label170: TLabel;
    Label171: TLabel;
    Label172: TLabel;
    Label173: TLabel;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    QrEntidadesL_CPF_TXT: TWideStringField;
    DBEdit40: TDBEdit;
    Label174: TLabel;
    Label175: TLabel;
    EdLTel: TdmkEdit;
    esteObservaes1: TMenuItem;
    N6: TMenuItem;
    QrCadastro2Observacoes: TWideMemoField;
    DataSource1: TDataSource;
    QrEntidade2: TMySQLQuery;
    MeObservacoes_Show: TMemo;
    WebServiceVerificar1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEntidadesAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEntidadesBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtEntidadeClick(Sender: TObject);
    procedure PMEntidadesPopup(Sender: TObject);
    procedure QrEntidadesBeforeClose(DataSet: TDataSet);
    procedure QrEntidadesAfterScroll(DataSet: TDataSet);
    procedure BtContatosClick(Sender: TObject);
    procedure QrEntidadesCalcFields(DataSet: TDataSet);
    procedure Incluinovaentidade1Click(Sender: TObject);
    procedure Alteraentidadeatual1Click(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure BtCEP_PClick(Sender: TObject);
    procedure EdPCEPEnter(Sender: TObject);
    procedure EdPCEPExit(Sender: TObject);
    procedure Descobrir1Click(Sender: TObject);
    procedure Verificar1Click(Sender: TObject);
    procedure BtCEP_EClick(Sender: TObject);
    procedure EdECepEnter(Sender: TObject);
    procedure EdECepExit(Sender: TObject);
    procedure Incluinovocontato1Click(Sender: TObject);
    procedure AlteraContatoatual1Click(Sender: TObject);
    procedure Excluicontatos1Click(Sender: TObject);
    procedure PMContatosPopup(Sender: TObject);
    procedure NovoEMail1Click(Sender: TObject);
    procedure Alteraemailselecionado1Click(Sender: TObject);
    procedure Excluiemailselecionado1Click(Sender: TObject);
    procedure QrEntiContatAfterScroll(DataSet: TDataSet);
    procedure PMEMailsPopup(Sender: TObject);
    procedure BtEMailsClick(Sender: TObject);
    procedure QrEntiContatBeforeClose(DataSet: TDataSet);
    procedure QrEntiTelCalcFields(DataSet: TDataSet);
    procedure BtTelefonesClick(Sender: TObject);
    procedure Incluinovotelefone1Click(Sender: TObject);
    procedure Alteratelefoneatual1Click(Sender: TObject);
    procedure Excluitelefones1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure PMTelefonesPopup(Sender: TObject);
    procedure EdEUFChange(Sender: TObject);
    procedure BtAtributosClick(Sender: TObject);
    procedure BtCEP_CClick(Sender: TObject);
    procedure BtCEP_LClick(Sender: TObject);
    procedure EdCCepEnter(Sender: TObject);
    procedure EdCCepExit(Sender: TObject);
    procedure EdLCepEnter(Sender: TObject);
    procedure EdLCepExit(Sender: TObject);
    procedure BtResponClick(Sender: TObject);
    procedure PMResponPopup(Sender: TObject);
    procedure Incluinovoresponsvel1Click(Sender: TObject);
    procedure Alteraresponsvelatual1Click(Sender: TObject);
    procedure Excluiresponsveleis1Click(Sender: TObject);
    procedure EdCodCNAEKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure BtDBancClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure PMDBancPopup(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EdCNPJExit(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure QrEntiResponCalcFields(DataSet: TDataSet);
    procedure Nome1Click(Sender: TObject);
    procedure Dados2Click(Sender: TObject);
    procedure CNPJCPF1Click(Sender: TObject);
    procedure dmkEditCB1Change(Sender: TObject);
    procedure EdCRTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCRTChange(Sender: TObject);
    procedure DBEdCRTChange(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Internet1Click(Sender: TObject);
    procedure SbMapaClick(Sender: TObject);
    procedure EdCUFExit(Sender: TObject);
    procedure EdEUFExit(Sender: TObject);
    procedure EdLUFExit(Sender: TObject);
    procedure EdPUFExit(Sender: TObject);
    procedure DefinircomofonedaNFe1Click(Sender: TObject);
    procedure Inclui4Click(Sender: TObject);
    procedure Altera4Click(Sender: TObject);
    procedure Exclui4Click(Sender: TObject);
    procedure BtEntiSrvProClick(Sender: TObject);
    procedure PMEntiSrvProPopup(Sender: TObject);
    procedure Pesquisas1Click(Sender: TObject);
    procedure Dados1Click(Sender: TObject);
    procedure Dadoscompactosdaentidadeatual1Click(Sender: TObject);
    procedure QrCadastro2CalcFields(DataSet: TDataSet);
    procedure frxCadastroGetValue(const VarName: string; var Value: Variant);
    procedure Dadosdestaentidade2012111Click(Sender: TObject);
    procedure SbImagensClick(Sender: TObject);
    procedure Excluientidadeatual1Click(Sender: TObject);
    procedure QrEntiContatCalcFields(DataSet: TDataSet);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure XMLNFe1Click(Sender: TObject);
    procedure ReceitaFederal1Click(Sender: TObject);
    procedure ReceitaEstadual1Click(Sender: TObject);
    procedure elefone1Click(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure Gerenciacontatos1Click(Sender: TObject);
    procedure Incluinovocontatoporatrelamento1Click(Sender: TObject);
    procedure IncluiAtributo1Click(Sender: TObject);
    procedure AlteraAtributo1Click(Sender: TObject);
    procedure ExcluiAtributo1Click(Sender: TObject);
    procedure PMAtributosPopup(Sender: TObject);
    procedure SBESiteClick(Sender: TObject);
    procedure SBPSiteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGEntiResponDblClick(Sender: TObject);
    procedure PCShowGerChange(Sender: TObject);
    procedure EdindIEDestChange(Sender: TObject);
    procedure EdindIEDestKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkEstrangDefClick(Sender: TObject);
    procedure DBEdindIEDestChange(Sender: TObject);
    procedure Atualiza9dgito1Click(Sender: TObject);
    procedure SBConfigClick(Sender: TObject);
    procedure SBAssinaturasClick(Sender: TObject);
    procedure BtGruposClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGEntiContatDblClick(Sender: TObject);
    procedure DefinircomofonedaNFe2Click(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure Remove1Click(Sender: TObject);
    procedure BtEntiTranspClick(Sender: TObject);
    procedure PMEntiTranspPopup(Sender: TObject);
    procedure Ordena1Click(Sender: TObject);
    procedure BtPrazosClick(Sender: TObject);
    procedure EdL_CNPJRedefinido(Sender: TObject);
    procedure EdL_CPFRedefinido(Sender: TObject);
    procedure esteObservaes1Click(Sender: TObject);
    procedure WebServiceVerificar1Click(Sender: TObject);
  private
    FFormAtivo: Boolean;
    F_indIEDest: MyArrayLista;
    FBotaoCEP: TBitBtn;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure MostraPMCEP(Botao: TBitBtn);
    // Atributos
    procedure ReopenEntiCtas();
    procedure ReopenEntiSrvPro();
    procedure ReopenMunici(Query: TmySQLQuery; UF: String);
    procedure ReopenEntiTransp(Conta: Integer);
    procedure ExcluiEntidadeToolrent(Codigo: Integer);
    procedure ConfiguraComponentesCEP();
    procedure MostraEntiTipCto(Query: TmySQLQuery; EditCB: TdmkEditCB;
              ComboBox: TdmkDBLookupComboBox; ValUsu: TdmkValUsu);
    procedure ImportaCadastroReceita(Estadual: Boolean);
    procedure MostraFormAtrEntiDef(SQLType: TSQLType);
    procedure AbreWEBSiteEntidade(Site: String);
    procedure CarregaAbaEspecifica(Aba: Integer);
    procedure ConfiguraTipoEntidade(EntTipo: TUnEntTipo);
    function  PesquisaTelefone(): Integer;
    function  CNPJDuplicado(): Boolean;
    function  TipoDeCadastroDefinido(): Boolean;
    procedure MostraFormEntiGrupos(Codigo: Integer);
    function  VerificaUmSohDocumentoLocalDeEntrega(): Boolean;
  public
    { Public declarations }
    FSetando: Boolean;
    FEditingByMatriz: Boolean;
    FEntTipo: TUnEntTipo;
    FEntiContat: Integer;
    FPCEP, FECEP, FCCEP, FLCEP, F_CNPJ_A_CADASTRAR: String;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenEntiGrupos(Codigo: Integer);
    procedure ReopenEntiContat(Controle: Integer);
    procedure ReopenEntiRespon(Controle: Integer);
    procedure ReopenEntiMail(Conta: Integer);
    procedure ReopenEntiTel(Conta: Integer);
    function  CNPJCPFDuplicado(CNPJCPF: String; Tipo: Integer): Boolean;
    procedure CadastroPorXML_NFe_1(const TipoCad: TTipoCadNFe; var Arquivo: String);
    procedure CadastroPorXML_NFe_2(TipoCad: TTipoCadNFe; CNPJ, CPF, xNome,
              xFant, IE, xLgr, nro, xBairro: WideString; cMun: Integer; xMun,
              UF: WideString; CEP,  cPais: Integer; xPais, fone, IEST, IM, CNAE,
              xCpl, xEnder: WideString);
    procedure PreCadastro(CNPJ, CPF: String; Cliente1, Cliente2, Cliente3,
              Cliente4, Fornece1, Fornece2, Fornece3, Fornece4, Fornece5,
              Fornece6, Fornece7, Fornece8, Terceiro: Boolean);
    procedure MostraEntiContat(SQLType: TSQLType);
    procedure MostraEntiMail(SQLType: TSQLType);
    procedure MostraEntiTel(SQLType: TSQLType);
    procedure MostraEntiSrvPro(SQLType: TSQLType);
    procedure ReopenAtrEntiDef(ID_Item: Integer);
  end;

var
  FmEntidade2: TFmEntidade2;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck,
{$IfDef cDbTable}
EntiCEP,
{$EndIf}
UMySQLDB,
UnCEP, EntiContat, EntiMail,
  EntiTel, ModuleGeral, EntiRespon, EntiCtas, CNAE21Cad, PesqRCNAE21,
  UnitMyXML, OmniXML, DmkDAC_PF, UnConsultasWeb, MyGlyfs,
  {$IfNDef NO_FINANCEIRO}UnFinanceiro, {$Else} UnGrl_Geral, {$EndIf}
  {$IfNDef sPraz}UnPraz_PF, {$EndIf}
  Entidade2Imp, MyListas, EntiSrvPro, Principal, PesqCNAE21, CNAE21,
  Entidade2ImpOne, EntiTipCto, EntiImporCfg, Curinga, UnEntities, ContatosEnt,
  EntiConEnt, CfgAtributos, (*Enti9Digit,*) EntiTransp, UnReordena;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntidade2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEntidade2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEntidadesCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmEntidade2.Verificar1Click(Sender: TObject);
begin
{$IfDef cDbTable}
  VAR_ACTIVEPAGE := 7;
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    with FmEntiCEP do
    begin
      DBGrid1.Visible := False;
      Painel1.Visible := False;
      ConfiguraComponentesCEP();
      ShowModal;
      Destroy;
    end;
  end;
{$Else}
  //    TODO Berlin
  Geral.MB_Info('Solicite implementa��o � Dermatek! (DBE Table)');
{$EndIf}
end;

function TFmEntidade2.VerificaUmSohDocumentoLocalDeEntrega(): Boolean;
begin
  Result := not (MyObjects.FIC((Trim(EdL_CNPJ.Text) <> '') and (Trim(EdL_CPF.Text) <> ''),
  nil, 'Defina apenas o CNPJ ou o CPF no endere�o de entrega!'));
end;

procedure TFmEntidade2.WebServiceVerificar1Click(Sender: TObject);
begin
  if FBotaoCep = BtCEP_P then
    EdPCepExit(EdPCep)
  else
  if FBotaoCep = BtCEP_E then
    EdECepExit(EdECep)
  else
  if FBotaoCep = BtCEP_C then
    EdCCepExit(EdCCep)
  else
  if FBotaoCep = BtCEP_L then
    EdLCepExit(EdLCep)
end;

procedure TFmEntidade2.XMLNFe1Click(Sender: TObject);
var
  Arquivo: String;
begin
  Arquivo := '';
  CadastroPorXML_NFe_1(entNFe_emit(*, entNFe_dest, entNFe_transp*), Arquivo);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEntidade2.DefParams;
begin
  VAR_GOTOTABELA := 'Entidades';
  VAR_GOTOMySQLTABLE := QrEntidades;
  if FEditingByMatriz then
    VAR_GOTONEG := gotoAll
  else
    VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  //
  VAR_SQLx.Add('SELECT ent.*, ');
  {$IfNDef NO_FINANCEIRO}
  VAR_SQLx.Add('car.Nome NOMECARTPREF, ');
  {$Else}
  VAR_SQLx.Add('"" NOMECARTPREF,');
  {$EndIf}
  VAR_SQLx.Add('cna.Nome CNAE_Nome, ');
  VAR_SQLx.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENTIDADE,');
  VAR_SQLx.Add('CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial ELSE acm.Nome END NOMEACCOUNT,');
  VAR_SQLx.Add('eng.Nome NOMEENTIGRUPO, mot.Descricao NOMEMOTIVO, emp.RazaoSocial NOMEEMPRESA,');
  VAR_SQLx.Add('euf.Nome NOMEEUF, puf.nome NOMEPUF, cuf.Nome NOMECUF, luf.nome NOMELUF, ');
  VAR_SQLx.Add('nuf.Nome NOMENUF, elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,');
  VAR_SQLx.Add('clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD, lec.Nome NOMEECIVIL,');
  VAR_SQLx.Add('mue.Nome NO_DTB_EMUNICI, mup.Nome NO_DTB_PMUNICI,');
  VAR_SQLx.Add('muc.Nome NO_DTB_CMUNICI, mul.Nome NO_DTB_LMUNICI,');
  VAR_SQLx.Add('pae.Nome NO_BACEN_EPAIS, pap.Nome NO_BACEN_PPAIS,');
  VAR_SQLx.Add('pal.Nome NO_BACEN_LPAIS,');
  VAR_SQLx.Add('ptt.Nome PTe1Tip_TXT, pct.Nome PCelTip_TXT,');
  VAR_SQLx.Add('ett.Nome ETe1Tip_TXT, ect.Nome ECelTip_TXT');
  VAR_SQLx.Add('FROM entidades ent');
  VAR_SQLx.Add('LEFT JOIN entigrupos eng  ON eng.Codigo=ent.Grupo');
  VAR_SQLx.Add('LEFT JOIN entidades acm   ON acm.Codigo=ent.Account');
  VAR_SQLx.Add('LEFT JOIN entidades emp   ON emp.Codigo=ent.Empresa');
  VAR_SQLx.Add('LEFT JOIN motivose mot    ON mot.Codigo=ent.Motivo');
  VAR_SQLx.Add('LEFT JOIN ufs euf         ON euf.Codigo=ent.EUF');
  VAR_SQLx.Add('LEFT JOIN ufs puf         ON puf.Codigo=ent.PUF');
  VAR_SQLx.Add('LEFT JOIN ufs cuf         ON cuf.Codigo=ent.CUF');
  VAR_SQLx.Add('LEFT JOIN ufs luf         ON luf.Codigo=ent.LUF');
  VAR_SQLx.Add('LEFT JOIN ufs nuf         ON nuf.Codigo=ent.UFNatal');
  VAR_SQLx.Add('LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd');
  VAR_SQLx.Add('LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd');
  VAR_SQLx.Add('LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd');
  VAR_SQLx.Add('LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd');
  VAR_SQLx.Add('LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil');
  {$IfNDef NO_FINANCEIRO}
  VAR_SQLx.Add('LEFT JOIN carteiras   car ON car.Codigo=ent.CartPref');
  {$EndIf}
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mue ON mue.Codigo=ent.ECodMunici');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mup ON mup.Codigo=ent.PCodMunici');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici muc ON muc.Codigo=ent.CCodMunici');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mul ON mul.Codigo=ent.LCodMunici');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pae ON pae.Codigo=ent.ECodiPais');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pap ON pap.Codigo=ent.PCodiPais');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pal ON pal.Codigo=ent.LCodiPais');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.cnae21cad cna ON cna.CodAlf=ent.CNAE');
  VAR_SQLx.Add('LEFT JOIN entitipcto ptt  ON ptt.Codigo=ent.PTe1Tip');
  VAR_SQLx.Add('LEFT JOIN entitipcto pct  ON pct.Codigo=ent.PCelTip');
  VAR_SQLx.Add('LEFT JOIN entitipcto ett  ON ett.Codigo=ent.ETe1Tip');
  VAR_SQLx.Add('LEFT JOIN entitipcto ect  ON ect.Codigo=ent.ECelTip');  
  VAR_SQLx.Add('WHERE ent.Codigo>-1000');
  //
  VAR_SQL1.Add('AND ent.Codigo=:P0');
  //
  VAR_SQLa.Add('AND (CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial LIKE:P0');
  VAR_SQLa.Add('ELSE ent.Nome LIKE:P1 END) ');
  // ERRO ao usar pela primeira vez
end;


procedure TFmEntidade2.Descobrir1Click(Sender: TObject);
begin
{$IfDef cDbTable}
  VAR_ACTIVEPAGE := 7;
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    FmEntiCEP.DBGrid2.Visible := False;
    FmEntiCEP.Painel2.Visible := False;
    ConfiguraComponentesCEP();
    FmEntiCEP.ShowModal;
    FmEntiCEP.Destroy;
  end;
{$Else}
  Geral.MB_Info('Solicite implementa��o � Dermatek! (DBE Table)');  //  TODO Berlin
{$EndIf}
end;

procedure TFmEntidade2.dmkEditCB1Change(Sender: TObject);
begin
{$IFNDEF NO_FINANCEIRO}
  Memo1.Text := UFinanceiro.CRT_Get(EdCRT.ValueVariant);
{$EndIf}
end;

procedure TFmEntidade2.EdCRTChange(Sender: TObject);
begin
{$IFNDEF NO_FINANCEIRO}
  Memo1.Text := UFinanceiro.CRT_Get(EdCRT.ValueVariant);
{$ENDIF}
end;

procedure TFmEntidade2.EdCRTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
{$IFNDEF NO_FINANCEIRO}
begin
  if Key = VK_F3 then
    EdCRT.Text := UFinanceiro.ListaDeCRT();
{$Else}
begin
  Grl_Geral.InfoSemModulo(mdlappFinanceiro);
{$EndIf}
end;

procedure TFmEntidade2.EdCUFExit(Sender: TObject);
begin
  ReopenMunici(QrCMunici, EdCUF.ValueVariant);
end;

procedure TFmEntidade2.EdCCepEnter(Sender: TObject);
begin
  FCCEP := Geral.SoNumero_TT(EdCCEP.Text);
end;

procedure TFmEntidade2.EdCCepExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdCCEP.Text);
  if CEP <> FCCEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdCCEP, EdCLograd, EdCRua, EdCNumero, EdCBairro,
        nil, EdCUF, nil, EdCNumero, EdCCompl, CBCLograd,
        EdCCodMunici, CBCCodMunici, nil, nil);
  end;
end;

procedure TFmEntidade2.EdCNPJExit(Sender: TObject);
begin
  CNPJDuplicado();
end;

procedure TFmEntidade2.EdCodCNAEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Codigo: String;
begin
  if Key = VK_F3 then
  begin
    if DBCheck.CriaFm(TFmPesqRCNAE21, FmPesqRCNAE21, afmoNegarComAviso) then
    begin
      FmPesqRCNAE21.ShowModal;
      //
      Codigo := FmPesqRCNAE21.FCodAlf;
      //
      FmPesqRCNAE21.Destroy;
    end;
    EdCodCNAE.ValueVariant := Codigo;
    CBCodCNAE.KeyValue     := Codigo;
  end;
  if Key = VK_F4 then
  begin
    if DBCheck.CriaFm(TFmPesqCNAE21, FmPesqCNAE21, afmoNegarComAviso) then
    begin
      FmPesqCNAE21.ShowModal;
      //
      Codigo := FmPesqCNAE21.FCodAlf;
      //
      FmPesqCNAE21.Destroy;
    end;
    EdCodCNAE.ValueVariant := Codigo;
    CBCodCNAE.KeyValue     := Codigo;
  end;
end;

procedure TFmEntidade2.EdCPFExit(Sender: TObject);
begin
  CNPJDuplicado();
end;

procedure TFmEntidade2.EdECepEnter(Sender: TObject);
begin
  FECEP := Geral.SoNumero_TT(EdECEP.Text);
end;

procedure TFmEntidade2.EdECepExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdECEP.Text);
  if CEP <> FECEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdECEP, EdELograd, EdERua, EdENumero, EdEBairro,
        nil, EdEUF, nil, EdENumero, EdECompl, CBELograd,
        EdECodMunici, CBECodMunici, EdECodiPais, CBECodiPais);
  end;
end;

procedure TFmEntidade2.EdEUFChange(Sender: TObject);
begin
  if EdEUF.Text <> '' then
    EdIE.LinkMsk := EdEUF.Text;
end;

procedure TFmEntidade2.EdEUFExit(Sender: TObject);
begin
  ReopenMunici(QrEMunici, EdEUF.ValueVariant);
end;

procedure TFmEntidade2.EdindIEDestChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdindIEDest.Text, F_indIEDest, Texto, 0, 1);
  EdindIEDest_TXT.Text := Texto;
end;

procedure TFmEntidade2.EdindIEDestKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdindIEDest.Text := Geral.SelecionaItem(F_indIEDest, 0,
    'SEL-LISTA-000 :: Indicador de I.E.',
    TitCols, Screen.Width)
  end;
end;

procedure TFmEntidade2.EdLCepEnter(Sender: TObject);
begin
  FLCEP := Geral.SoNumero_TT(EdLCEP.Text);
end;

procedure TFmEntidade2.EdLCepExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdLCEP.Text);
  if CEP <> FLCEP then
  begin
    if Trim(CEP) <> '' then
//  Ini 2020-01-25
      U_CEP.ConsultaCEP(EdLCEP, EdLLograd, EdLRua, EdLNumero, EdLBairro,
        nil, EdLUF, nil, EdLNumero, EdLCompl, CBLLograd,
        EdLCodMunici, CBLCodMunici, nil, nil);
  //Fim 2020-01-25
  end;
end;

procedure TFmEntidade2.EdLUFExit(Sender: TObject);
begin
  ReopenMunici(QrLMunici, EdLUF.ValueVariant);
end;

procedure TFmEntidade2.EdL_CNPJRedefinido(Sender: TObject);
begin
  VerificaUmSohDocumentoLocalDeEntrega();
end;

procedure TFmEntidade2.EdL_CPFRedefinido(Sender: TObject);
begin
  VerificaUmSohDocumentoLocalDeEntrega();
end;

procedure TFmEntidade2.EdPCEPEnter(Sender: TObject);
begin
  FPCEP := Geral.SoNumero_TT(EdPCEP.Text);
end;

procedure TFmEntidade2.EdPCEPExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdPCEP.Text);
  if CEP <> FPCEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdPCEP, EdPLograd, EdPRua, EdPNumero, EdPBairro,
        nil, EdPUF, nil, EdPNumero, EdPCompl, CBPLograd,
        EdPCodMunici, CBPCodMunici, EdPCodiPais, CBPCodiPais);
  end;
end;

procedure TFmEntidade2.EdPUFExit(Sender: TObject);
begin
  ReopenMunici(QrPMunici, EdPUF.ValueVariant);
end;

procedure TFmEntidade2.elefone1Click(Sender: TObject);
begin
  LocCod(QrEntidadesCodigo.Value, PesquisaTelefone());
end;

procedure TFmEntidade2.esteObservaes1Click(Sender: TObject);
begin
  //ShowMessage(QrEntidadesObservacoes.Value);
end;

function TFmEntidade2.PesquisaTelefone(): Integer;
var
  Telefone: String;
begin
  Telefone := '';
  Result   := 0;
  //
  if InputQuery('Procura e Lista', 'Digite parte do telefone:', Telefone) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrSB, Dmod.MyDB, [
    'SELECT ent.Codigo, ent.CodUsu, ',
    'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome ',
    'FROM entidades ent ',
    'LEFT JOIN entitel tel ON ent.Codigo = tel.Codigo ',
    'WHERE IF(ent.Tipo=0, ETe1, PTe1) LIKE "%' + Telefone + '%" ',
    'OR IF(ent.Tipo=0, ECel, PCel) LIKE "%' + Telefone + '%" ',
    'OR tel.Telefone LIKE "%' + Telefone + '%" ',
    'GROUP BY ent.Codigo ',
    '']);
    //
    MyObjects.FormShow(TFmCuringa, FmCuringa);
    //
    Result := VAR_CODIGO;
  end;
end;

procedure TFmEntidade2.Exclui1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntiCtas, TDBGrid(DBGEntiCtas),
    'entictas', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmEntidade2.Excluicontatos1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntiContat, TDBGrid(DBGEntiContat),
    'enticontat', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmEntidade2.Excluiemailselecionado1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntimail, TDBGrid(DBGEntiMail),
    'entimail', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmEntidade2.Excluientidadeatual1Click(Sender: TObject);
begin
  ExcluiEntidadeToolrent(QrEntidadesCodigo.Value);
end;

procedure TFmEntidade2.ExcluiEntidadeToolrent(Codigo: Integer);
var
  TabLctA, TabLctB,TabLctD: String;
begin
  if UpperCase(Application.Title) = 'TOOLRENT' then
  begin
    //Verifica se o c�digo � negativo
    if Codigo <= 0 then
    begin
      Geral.MB_Info('Exclus�o cancelada!' + sLineBreak + 'Motivo: O C�digo da entidade deve ser positivo!');
      Exit;
    end;
    //Verifica as loca��es
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT Codigo');
    DModG.QrAux.SQL.Add('FROM locccon');
    DModG.QrAux.SQL.Add('WHERE Cliente=:P0');
    DModG.QrAux.Params[0].AsInteger := Codigo;
    UMyMod.AbreQuery(DModG.QrAux, DMod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Info('Exclus�o cancelada!' + sLineBreak +
        'Motivo: A entidade selecionada foi utilizada na loca��o ID n�mero ' +
        Geral.FF0(DModG.QrAux.FieldByName('Codigo').AsInteger) + '!');
      Exit;
    end;
    //Verifica lan ctos
    TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, DModG.QrFiliLogFilial.Value);
    TabLctB := DmodG.NomeTab(TMeuDB, ntLct, False, ttB, DModG.QrFiliLogFilial.Value);
    TabLctD := DmodG.NomeTab(TMeuDB, ntLct, False, ttD, DModG.QrFiliLogFilial.Value);
    //
    if (TabLctA <> '') and (TabLctB <> '') and (TabLctD <> '') then
    begin
      DModG.QrAux.Close;
      DModG.QrAux.SQL.Clear;
      DModG.QrAux.SQL.Add('');
      DModG.QrAux.SQL.Add('SELECT Controle');
      DModG.QrAux.SQL.Add('FROM ' + TabLctA);
      DModG.QrAux.SQL.Add('WHERE Cliente = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR Fornecedor = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR CliInt = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR ForneceI = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR Vendedor = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR Account = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add('');
      DModG.QrAux.SQL.Add('UNION');
      DModG.QrAux.SQL.Add('');
      DModG.QrAux.SQL.Add('SELECT Controle');
      DModG.QrAux.SQL.Add('FROM ' + TabLctB);
      DModG.QrAux.SQL.Add('WHERE Cliente = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR Fornecedor = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR CliInt = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR ForneceI = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR Vendedor = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR Account = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add('');
      DModG.QrAux.SQL.Add('UNION');
      DModG.QrAux.SQL.Add('');
      DModG.QrAux.SQL.Add('SELECT Controle');
      DModG.QrAux.SQL.Add('FROM ' + TabLctD);
      DModG.QrAux.SQL.Add('WHERE Cliente = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR Fornecedor = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR CliInt = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR ForneceI = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR Vendedor = ' + Geral.FF0(Codigo));
      DModG.QrAux.SQL.Add(' OR Account = ' + Geral.FF0(Codigo));
      UMyMod.AbreQuery(DModG.QrAux, DMod.MyDB);
      if DModG.QrAux.RecordCount > 0 then
      begin
        Geral.MB_Info('Exclus�o cancelada!' + sLineBreak +
          'Motivo: A entidade selecionada foi utilizada no lan�amento financeiro ID n�mero ' +
          Geral.FF0(DModG.QrAux.FieldByName('Controle').AsInteger) + '!');
        Exit;
      end;
    end else
    begin
      Geral.MB_Info('Exclus�o cancelada!' + sLineBreak +
        'Motivo: A tabela de lan�amentos n�o foi definida!');
      Exit;
    end;
    //Bloquetos
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT Controle');
    DModG.QrAux.SQL.Add('FROM arreits');
    DModG.QrAux.SQL.Add('WHERE Entidade=:P0');
    DModG.QrAux.Params[0].AsInteger := Codigo;
    UMyMod.AbreQuery(DModG.QrAux, DMod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Info('Exclus�o cancelada!' + sLineBreak +
        'Motivo: A entidade selecionada foi utilizada no bloqueto ID n�mero ' +
        Geral.FF0(DModG.QrAux.FieldByName('Controle').AsInteger) + '!');
      Exit;
    end;
    //Bloquetos arrecada��es base
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT Controle');
    DModG.QrAux.SQL.Add('FROM bloarreits');
    DModG.QrAux.SQL.Add('WHERE Entidade=:P0');
    DModG.QrAux.Params[0].AsInteger := Codigo;
    UMyMod.AbreQuery(DModG.QrAux, DMod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Info('Exclus�o cancelada!' + sLineBreak +
        'Motivo: A entidade selecionada foi utilizada na arrecada��o base ID n�mero ' +
        Geral.FF0(DModG.QrAux.FieldByName('Controle').AsInteger) + '!');
      Exit;
    end;
    //Protocolos
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT Codigo');
    DModG.QrAux.SQL.Add('FROM protocolos');
    DModG.QrAux.SQL.Add('WHERE Def_Client=:P0');
    DModG.QrAux.SQL.Add(' OR Def_Sender=:P1');
    DModG.QrAux.Params[0].AsInteger := Codigo;
    DModG.QrAux.Params[1].AsInteger := Codigo;
    UMyMod.AbreQuery(DModG.QrAux, DMod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Info('Exclus�o cancelada!' + sLineBreak +
        'Motivo: A entidade selecionada foi utilizada no protocolo ID n�mero ' +
        Geral.FF0(DModG.QrAux.FieldByName('Codigo').AsInteger) + '!');
      Exit;
    end;
    //Pedidos / Faturamentos
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT Codigo');
    DModG.QrAux.SQL.Add('FROM pedivda');
    DModG.QrAux.SQL.Add('WHERE Cliente=:P0');
    DModG.QrAux.SQL.Add(' OR Represen=:P1');
    DModG.QrAux.SQL.Add(' OR Transporta=:P2');
    DModG.QrAux.SQL.Add(' OR Redespacho=:P3');
    DModG.QrAux.Params[0].AsInteger := Codigo;
    DModG.QrAux.Params[1].AsInteger := Codigo;
    DModG.QrAux.Params[2].AsInteger := Codigo;
    DModG.QrAux.Params[3].AsInteger := Codigo;
    UMyMod.AbreQuery(DModG.QrAux, DMod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Info('Exclus�o cancelada!' + sLineBreak +
        'Motivo: A entidade selecionada foi utilizada no faturamento ID n�mero ' +
        Geral.FF0(DModG.QrAux.FieldByName('Codigo').AsInteger) + '!');
      Exit;
    end;
    //NF-e
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT IDCtrl');
    DModG.QrAux.SQL.Add('FROM nfecaba');
    DModG.QrAux.SQL.Add('WHERE CodInfoDest=:P0');
    DModG.QrAux.SQL.Add(' OR CodInfoEmit=:P1');
    DModG.QrAux.SQL.Add(' OR CodInfoTrsp=:P2');
    DModG.QrAux.Params[0].AsInteger := Codigo;
    DModG.QrAux.Params[1].AsInteger := Codigo;
    DModG.QrAux.Params[2].AsInteger := Codigo;
    UMyMod.AbreQuery(DModG.QrAux, DMod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Info('Exclus�o cancelada!' + sLineBreak +
        'Motivo: A entidade selecionada foi utilizada na NF-e ID n�mero ' +
        Geral.FF0(DModG.QrAux.FieldByName('IDCtrl').AsInteger) + '!');
      Exit;
    end;
    //NFS-e
    DModG.QrAux.Close;
    DModG.QrAux.SQL.Clear;
    DModG.QrAux.SQL.Add('SELECT Codigo');
    DModG.QrAux.SQL.Add('FROM nfsedpscab');
    DModG.QrAux.SQL.Add('WHERE Cliente=:P0');
    DModG.QrAux.SQL.Add(' OR Intermediario=:P1');
    DModG.QrAux.SQL.Add(' OR Empresa=:P2');
    DModG.QrAux.Params[0].AsInteger := Codigo;
    DModG.QrAux.Params[1].AsInteger := Codigo;
    DModG.QrAux.Params[2].AsInteger := Codigo;
    UMyMod.AbreQuery(DModG.QrAux, DMod.MyDB);
    if DModG.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Info('Exclus�o cancelada!' + sLineBreak +
        'Motivo: A entidade selecionada foi utilizada na NFS-e ID n�mero ' +
        Geral.FF0(DModG.QrAux.FieldByName('Codigo').AsInteger) + '!');
      Exit;
    end;
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da entidade atual?' + sLineBreak + sLineBreak
      + 'ATEN��O: Antes de excluir verifique se a entidade n�o foi utilizada em nenhum local do aplicativo'
      + sLineBreak + 'pois ao exclu�-la o hist�rico desta entidade ser� exclu�do tamb�m!' + sLineBreak
      + sLineBreak + 'ESTE PROCEDIMENTO N�O PODER� SER REVERTIDO!', 'entidades', 'Codigo', Codigo, Dmod.MyDB) = ID_YES then
    begin
      Va(vpLast);
    end;
  end;  
end;

procedure TFmEntidade2.ExcluiAtributo1Click(Sender: TObject);
var
  ID_Item: Integer;
  Tabela: String;
begin
  case TTypTabAtr(QrAtrEntiDefAtrTyp.Value) of
    ttaPreDef: Tabela := 'atrentidef';
    ttaTxtFree: Tabela := 'atrentitxt';
    else Tabela := '????';
  end;
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do atributo selecionado?',
  Tabela, 'ID_Item', QrAtrEntiDefID_Item.Value, Dmod.MyDB) = ID_YES then
  begin
    ID_Item := GOTOy.LocalizaPriorNextIntQr(QrAtrEntiDef, QrAtrEntiDefID_Item,
    QrAtrEntiDefID_Item.Value);
    ReopenAtrEntiDef(ID_Item);
  end;
end;

procedure TFmEntidade2.Excluiresponsveleis1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntiRespon, DBGEntiRespon,
    'entiRespon', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmEntidade2.Excluitelefones1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntitel, TDBGrid(DBGEntiTel),
    'entitel', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmEntidade2.Inclui4Click(Sender: TObject);
begin
  MostraEntiSrvPro(stIns);
end;

procedure TFmEntidade2.Altera4Click(Sender: TObject);
begin
  MostraEntiSrvPro(stUpd);
end;

procedure TFmEntidade2.Exclui4Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntiSrvPro, DBGEntiSrvPro,
    'entisrvpro', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmEntidade2.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType, TFmEntidade2(Self));
end;

procedure TFmEntidade2.MostraEntiContat(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiContat, FmEntiContat, afmoNegarComAviso,
    QrEntiContat, SQLType, 'enticontat') then
  begin
    FmEntiContat.FQrEntiContat := QrEntiContat;
    FmEntiContat.FEntidadeOri  := QrEntiContatCodigo.Value;
    FmEntiContat.FEntidadeAtu  := QrEntidadesCodigo.Value;
    FmEntiContat.ShowModal;
    FmEntiContat.Destroy;
  end;
end;

procedure TFmEntidade2.MostraEntiMail(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiMail, FmEntiMail, afmoNegarComAviso,
  QrEntiMail, SQLType) then
  begin
    FmEntiMail.FQrEntiMail := QrEntiMail;
    FmEntiMail.FQrEntidades := QrEntidades;
    FmEntiMail.FDsEntidades := DsEntidades;
    FmEntiMail.FQrEntiContat  := QrEntiContat;
    FmEntiMail.FDsEntiContat  := DsEntiContat;
    //
    FmEntiMail.DBEdCodigo.DataSource := DsEntidades;
    FmEntiMail.DBEdCodUsu.DataSource := DsEntidades;
    FmEntiMail.DBEdNome.DataSource := DsEntidades;
    //
    FmEntiMail.DBEdContatoCod.DataSource := DsEntiContat;
    FmEntiMail.DBEdContatoNom.DataSource := DsEntiContat;
    FmEntiMail.DBEdContatoCrg.DataSource := DsEntiContat;
    //
    FmEntiMail.ShowModal;
    FmEntiMail.Destroy;
    //
    Entities.ReopenTipCto(QrPTe1Tip, etcTel);
    Entities.ReopenTipCto(QrPCelTip, etcTel);
    Entities.ReopenTipCto(QrECelTip, etcTel);
    Entities.ReopenTipCto(QrETe1Tip, etcTel);
  end;
end;

procedure TFmEntidade2.MostraEntiSrvPro(SQLType: TSQLType);
var
  Data: TDateTime;
  Hora: TTime;
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiSrvPro, FmEntiSrvPro, afmoNegarComAviso,
  QrEntiSrvPro, SQLType) then
  begin
    FmEntiSrvPro.FQrEntiSrvPro := QrEntiSrvPro;
    FmEntiSrvPro.FQrEntidades := QrEntidades;
    FmEntiSrvPro.FDsEntidades := DsEntidades;
    //
    if SQLType = stIns then
    begin
      DModG.ObtemDataHora(Data, Hora);
      //
      FmEntiSrvPro.TPData.Date := Data;
      FmEntiSrvPro.EdHora.Text := FormatDateTime('hh:nn:ss', Hora);
    end;
    FmEntiSrvPro.ShowModal;
    FmEntiSrvPro.Destroy;
  end;
end;

procedure TFmEntidade2.MostraEntiTel(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiTel, FmEntiTel, afmoNegarComAviso,
  QrEntiTel, SQLType) then
  begin
    FmEntiTel.FQrEntiTel    := QrEntiTel;
    FmEntiTel.FQrEntidades  := QrEntidades;
    FmEntiTel.FDsEntidades  := DsEntidades;
    FmEntiTel.FQrEntiContat := QrEntiContat;
    FmEntiTel.FDsEntiContat := DsEntiContat;
    //
    FmEntiTel.DBEdCodigo.DataSource := DsEntidades;
    FmEntiTel.DBEdCodUsu.DataSource := DsEntidades;
    FmEntiTel.DBEdNome.DataSource   := DsEntidades;
    //
    FmEntiTel.DBEdContatoCod.DataSource := DsEntiContat;
    FmEntiTel.DBEdContatoNom.DataSource := DsEntiContat;
    FmEntiTel.DBEdContatoCrg.DataSource := DsEntiContat;
    //
    FmEntiTel.ShowModal;
    FmEntiTel.Destroy;
    //
    Entities.ReopenTipCto(QrPTe1Tip, etcTel);
    Entities.ReopenTipCto(QrPCelTip, etcTel);
    Entities.ReopenTipCto(QrECelTip, etcTel);
    Entities.ReopenTipCto(QrETe1Tip, etcTel);
  end;
end;

procedure TFmEntidade2.MostraPMCEP(Botao: TBitBtn);
begin
  FBotaoCEP := Botao;
  MyObjects.MostraPMCEP(PMCEP, Botao);
end;

procedure TFmEntidade2.Nome1Click(Sender: TObject);
begin
{
  LocCod(QrEntidadesCodigo.Value, CuringaLoc.CriaForm0(
    CO_CODIGO, 'RazaoSocial', CO_NOME, 'Entidades', Dmod.MyDB, CO_VAZIO));
}
  LocCod(QrEntidadesCodigo.Value,
  CuringaLoc.CriaFormEnti(CO_CODIGO, 'RazaoSocial', CO_NOME,
  'Fantasia', 'Apelido', 'Entidades', 'Tipo',
  0, Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEntidade2.NovoEMail1Click(Sender: TObject);
begin
  MostraEntiMail(stIns);
end;

procedure TFmEntidade2.Ordena1Click(Sender: TObject);
begin
  UReordena.ReordenaItens(QrEntiTransp, Dmod.QrUpd, 'entitransp',
    'Ordem', 'Conta', 'NOMEENTIDADE', '', '', '', nil);
  //
  ReopenEntiTransp(0);
end;

procedure TFmEntidade2.CarregaAbaEspecifica(Aba: Integer);
var
  Codigo: Integer;
begin
  if Aba = 5 then //Dados espec�ficos de entidade
  begin
    if (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0) and
      (VAR_KIND_DEPTO = kdUH) then
    begin
      Codigo := QrEntidadesCodigo.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrCondImov, Dmod.MyDB, [
        'SELECT imv.Unidade, cnd.Cliente, ',
        'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_CND, ',
        'CONCAT( ',
        '  IF(imv.Propriet=:Pa,"D",""), ',
        '  IF(imv.Conjuge=:Pa,"C",""), ',
        '  IF(imv.Usuario=:Pa,"M",""), ',
        '  IF(imv.Procurador=:Pa,"P",""), ',
        '  IF(imv.BloqEndEnt=:Pa,"T","") ',
        ') TIPO ',
        'FROM condimov imv ',
        'LEFT JOIN cond cnd ON cnd.Codigo=imv.Codigo ',
        'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente ',
        'WHERE Propriet=' + Geral.FF0(Codigo),
        'OR Conjuge=' + Geral.FF0(Codigo),
        'OR Usuario=' + Geral.FF0(Codigo),
        'OR Procurador=' + Geral.FF0(Codigo),
        'OR BloqEndEnt=' + Geral.FF0(Codigo),
        '']);
    end else
      QrCondImov.Close;
  end;
end;

procedure TFmEntidade2.CkEstrangDefClick(Sender: TObject);
begin
  GBEstrangeiro.Visible := CkEstrangDef.Checked;
end;

procedure TFmEntidade2.PCShowGerChange(Sender: TObject);
begin
  CarregaAbaEspecifica(PCShowGer.ActivePageIndex);
end;

procedure TFmEntidade2.Pesquisas1Click(Sender: TObject);
begin
  MyObjects.FormShow(TFmEntidade2Imp, FmEntidade2Imp);
end;

procedure TFmEntidade2.PMAtributosPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiAtributo1, QrEntidades);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtributo1, QrAtrEntiDef);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtributo1, QrAtrEntiDef);
end;

procedure TFmEntidade2.PMContatosPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount > 0);
  AlteraContatoatual1.Enabled := Habilita;
  Habilita := Habilita and
    (QrEntiMail.State <> dsInactive) and (QrEntiMail.RecordCount = 0) and
    (QrEntiTel.State <> dsInactive) and (QrEntiTel.RecordCount = 0);
  Excluicontatos1.Enabled := Habilita;
end;

procedure TFmEntidade2.PMDBancPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEntiCtas.State <> dsInactive) and (QrEntiCtas.RecordCount > 0);
  //
  Altera1.Enabled := Habilita;
  Exclui1.Enabled := Habilita;  
end;

procedure TFmEntidade2.PMEMailsPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount > 0);
  NovoEMail1.Enabled := Habilita;
  Habilita := (QrEntiMail.State <> dsInactive) and (QrEntiMail.RecordCount > 0);
  Alteraemailselecionado1.Enabled := Habilita;
  Excluiemailselecionado1.Enabled := Habilita;
end;

procedure TFmEntidade2.PMEntidadesPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0);
  //
  if (Enab = True) and (QrEntidadesCodigo.Value < 0) then
  begin
    Enab := FEditingByMatriz = True;
  end;
  //
  Alteraentidadeatual1.Enabled := Enab;
  //
  //Excluir somente para o Toolrent
  if UpperCase(Application.Title) = 'TOOLRENT' then
  begin
    Excluientidadeatual1.Enabled :=
    (QrEntidades.State <> dsInactive) and
    (
      (QrEntidades.RecordCount > 0) or FEditingByMatriz
    );
  end;
end;

procedure TFmEntidade2.PMEntiSrvProPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEntiSrvPro.State <> dsInactive) and (QrEntiSrvPro.RecordCount > 0);
  //
  Altera4.Enabled := Habilita;
  Exclui4.Enabled := Habilita;
end;

procedure TFmEntidade2.PMEntiTranspPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0);
  Enab2 := (QrEntiTransp.State <> dsInactive) and (QrEntiTransp.RecordCount > 0);
  //
  Inclui2.Enabled := Enab;
  Remove1.Enabled := Enab and Enab2;
  Ordena1.Enabled := Enab and Enab2;
end;

procedure TFmEntidade2.PMResponPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEntiRespon.State <> dsInactive) and (QrEntiRespon.RecordCount > 0);
  //
  Alteraresponsvelatual1.Enabled := Habilita;
  Excluiresponsveleis1.Enabled   := Habilita;
end;

procedure TFmEntidade2.PMTelefonesPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount > 0);
  //
  Incluinovotelefone1.Enabled := Habilita;
  //
  Habilita := (QrEntiTel.State <> dsInactive) and (QrEntiTel.RecordCount > 0);
  //
  Alteratelefoneatual1.Enabled  := Habilita;
  Excluitelefones1.Enabled      := Habilita;
  DefinircomofonedaNFe1.Enabled := Habilita;
end;

procedure TFmEntidade2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
  //QrEntidades. Open;
  //UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  Va(vpLast);
end;

procedure TFmEntidade2.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEntidade2.Dados1Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxCadastro, [
    frxDsCadastro,
    DModG.frxDsMaster
    ]);
  MyObjects.frxMostra(frxCadastro, 'Cadastro');
end;

procedure TFmEntidade2.Dados2Click(Sender: TObject);
begin
  MyObjects.FormShow(TFmEntidade2Imp, FmEntidade2Imp);
  LocCod(VAR_ENTIDADE, VAR_ENTIDADE);
end;

procedure TFmEntidade2.Dadoscompactosdaentidadeatual1Click(Sender: TObject);
{
var
  i: Integer;
}
begin
  //ARRUMAR IMPRESS�O DA OBSERVACAO
  QrCadastro2.Close;
  QrCadastro2.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrCadastro2);
  //
  MyObjects.frxDefineDataSets(frxCadastro2, [
    frxDsCadastro2,
    DModG.frxDsMaster
    ]);
  MyObjects.frxMostra(frxCadastro2, 'Cadastro da entidade selecionada');
  QrCadastro2.Close;
end;

procedure TFmEntidade2.Dadosdestaentidade2012111Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidade2ImpOne, FmEntidade2ImpOne, afmoNegarComAviso) then
  begin
    FmEntidade2ImpOne.EdEntidade.ValueVariant := QrEntidadesCodigo.Value;
    FmEntidade2ImpOne.CBEntidade.KeyValue     := QrEntidadesCodigo.Value;
    FmEntidade2ImpOne.ShowModal;
    FmEntidade2ImpOne.Destroy;
  end;
end;

procedure TFmEntidade2.DBEdCRTChange(Sender: TObject);
begin
{$IFNDEF NO_FINANCEIRO}
  Memo2.Text := UFinanceiro.CRT_Get(Geral.IMV(DBEdCRT.Text));
{$ENDIF}
end;

procedure TFmEntidade2.DBEdindIEDestChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(DBEdindIEDest.Text, F_indIEDest, Texto, 0, 1);
  DBEdindIEDest_TXT.Text := Texto;
end;

procedure TFmEntidade2.DBGEntiContatDblClick(Sender: TObject);
var
  Entidade: Integer;
begin
  if (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount <> 0) then
  begin
    Entidade := QrEntiContatEntidade.Value;
    //
    if Entidade <> 0 then
      LocCod(Entidade, Entidade);
  end;
end;

procedure TFmEntidade2.DBGEntiResponDblClick(Sender: TObject);
var
  Entidade: Integer;
begin
  if (QrEntiRespon.State <> dsInactive) and (QrEntiRespon.RecordCount <> 0) then
  begin
    Entidade := QrEntiResponEntidade.Value;
    //
    if Entidade <> 0 then
      LocCod(Entidade, Entidade);
  end;
end;

procedure TFmEntidade2.DBGrid1DblClick(Sender: TObject);
begin
  MostraFormEntiGrupos(QrEntiGruposCodigo.Value);
end;

procedure TFmEntidade2.DefineONomeDoForm;
begin
end;

procedure TFmEntidade2.DefinircomofonedaNFe1Click(Sender: TObject);
var
  Te1, Campo: String;
  Codigo: Integer;
begin
  if Geral.MB_Pergunta('Confirma a defini��o do telefone:' + sLineBreak +
  QrEntiTelTEL_TXT.Value + sLineBreak + 'como o telefone 1?') = ID_YES then
  begin
    Te1 := Geral.SoNumero_TT(QrEntiTelTelefone.Value);
    Codigo := QrEntidadesCodigo.Value;
    if QrEntidadesTipo.Value = 1 then
      Campo := 'PTe1'
    else
      Campo := 'ETe1';
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entidades', False, [
    Campo], ['Codigo'], [Te1], [Codigo], True) then
    begin
      LocCod(QrEntidadesCodigo.Value, QrEntidadesCodigo.Value);
    end;
  end;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEntidade2.DefinircomofonedaNFe2Click(Sender: TObject);
var
  Te1, Campo: String;
  Codigo: Integer;
begin
  Te1 := Geral.SoNumero_TT(QrEntiTelTelefone.Value);
  //
  if Te1 <> '' then
  begin
    if Geral.MB_Pergunta('Confirma a defini��o do telefone:' + sLineBreak +
      QrEntiTelTEL_TXT.Value + sLineBreak + 'como o telefone 1?') = ID_YES then
    begin
      Codigo := QrEntidadesCodigo.Value;
      //
      if QrEntidadesTipo.Value = 1 then
        Campo := 'PTe1'
      else
        Campo := 'ETe1';
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entidades', False, [
        Campo], ['Codigo'], [Te1], [Codigo], True) then
      begin
        LocCod(QrEntidadesCodigo.Value, QrEntidadesCodigo.Value);
      end;
    end;
  end;
end;

procedure TFmEntidade2.SpeedButton10Click(Sender: TObject);
begin
  MostraEntiTipCto(QrPCelTip, EdPCelTip, CBPCelTip, VUPCelTip);
end;

procedure TFmEntidade2.SBESiteClick(Sender: TObject);
begin
  AbreWEBSiteEntidade(QrEntidadesESite.Value);
end;

procedure TFmEntidade2.SBPSiteClick(Sender: TObject);
begin
  AbreWEBSiteEntidade(QrEntidadesPSite.Value);
end;

procedure TFmEntidade2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntidade2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntidade2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntidade2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntidade2.SpeedButton5Click(Sender: TObject);
begin
  EdCodUsu.ValueVariant := DBCheck.ObtemCodUsuZStepCodUni(tscEntidades);
end;

procedure TFmEntidade2.SpeedButton7Click(Sender: TObject);
begin
  MostraEntiTipCto(QrETe1Tip, EdETe1Tip, CBETe1Tip, VUETe1Tip);
end;

procedure TFmEntidade2.SpeedButton8Click(Sender: TObject);
begin
  MostraEntiTipCto(QrECelTip, EdECelTip, CBECelTip, VUECelTip);
end;

procedure TFmEntidade2.SpeedButton9Click(Sender: TObject);
begin
  MostraEntiTipCto(QrPTe1Tip, EdPTe1Tip, CBPTe1Tip, VUPTe1Tip);
end;

procedure TFmEntidade2.MostraEntiTipCto(Query: TmySQLQuery; EditCB: TdmkEditCB;
  ComboBox: TdmkDBLookupComboBox; ValUsu: TdmkValUsu);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmEntiTipCto, FmEntiTipCto, afmoNegarComAviso) then
  begin
    if ValUsu.ValueVariant <> 0 then
      FmEntiTipCto.LocCod(ValUsu.ValueVariant, ValUsu.ValueVariant);    
    FmEntiTipCto.ShowModal;
    FmEntiTipCto.Destroy;
    //
    Entities.ReopenTipCto(QrPTe1Tip, etcTel);
    Entities.ReopenTipCto(QrPCelTip, etcTel);
    Entities.ReopenTipCto(QrECelTip, etcTel);
    Entities.ReopenTipCto(QrETe1Tip, etcTel);
  end;
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EditCB, ComboBox, Query, VAR_CADASTRO);
    //
    EditCB.SetFocus;
  end;
end;

procedure TFmEntidade2.MostraFormAtrEntiDef(SQLType: TSQLType);
begin
  UnCfgAtributos.InsAltAtrDef_ITS(QrEntidadesCodigo.Value,
    QrAtrEntiDefID_Item.Value, 'atrentidef', 'atrenticad', 'atrentiits',
    'atrentitxt', SQLType,  QrAtrEntiDefAtrCad.Value,
    Trunc(QrAtrEntiDefAtrIts.Value), QrAtrEntiDefAtrTxt.Value,
    QrAtrEntiDef, QrAtrEntiDef, True);
end;

procedure TFmEntidade2.MostraFormEntiGrupos(Codigo: Integer);
begin
  VAR_CADASTRO := 0;
  //
  Entities.MostraFormEntiGrupos(Codigo);
  //
  if VAR_CADASTRO <> 0 then
    ReopenEntiGrupos(VAR_CADASTRO);
end;

function TFmEntidade2.TipoDeCadastroDefinido(): Boolean;
begin
  Result :=
  Entities.TipoDeCadastroDefinido(CkCliente1_0, CkCliente2_0, CkCliente3_0,
    CkCliente4_0, CkFornece1_0, CkFornece2_0, CkFornece3_0, CkFornece4_0,
    CkFornece5_0, CkFornece6_0, CkFornece7_0, CkFornece8_0, CkTerceiro_0,
    PCEditGer);
end;

procedure TFmEntidade2.BtResponClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRespon, BtRespon);
end;

procedure TFmEntidade2.BitBtn1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAE21Cad, FmCNAE21Cad, afmoNegarComAviso) then
  begin
    FmCNAE21Cad.ShowModal;
    FmCNAE21Cad.Destroy;
  end;
end;

procedure TFmEntidade2.BtImportaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImporta, BtImporta);
end;

procedure TFmEntidade2.BtPrazosClick(Sender: TObject);
begin
  {$IfNDef sPraz}
  Praz_PF.MostraFormPediPrzEnt(QrEntidadesCodigo.Value);
  {$EndIf}
end;

procedure TFmEntidade2.CadastroPorXML_NFe_1(const TipoCad: TTipoCadNFe; var Arquivo: String);
var
  xmlDoc : IXMLDocument;
  xmlNode: IXMLNode;
  //xmlList: IXMLNodeList;
  //
  Texto: WideString;
  //
  CNPJ, CPF, xNome, xFant, IE, xLgr, nro, xBairro, cMun, xMun, UF, CEP, cPais,
  xPais, fone, IEST, IM, CNAE, xCpl, xEnder: WideString;
  Lgr_I, Tipo: Integer;
  Rua: String;
  // TranspEnder
  nLgr, sLgr, sCpl, xNum, sBairro: String;
begin
  if Trim(Arquivo) = '' then
    MyObjects.FileOpenDialog(Self, 'C:\Dermatek\NFE', '',
      'Informe o aqruivo XML', '', [], Arquivo);
  if Trim(Arquivo) <> '' then
  begin
    if not UnMyXML.CarregaXML(Arquivo, Texto) then
      Exit;
    if UnMyXML.DefineXMLDoc(xmlDoc, Texto) then
    begin
{
  <CNPJ>96734892000123</CNPJ>
  <xNome>TFL do Brasil Ind. Quim. Ltda</xNome>
  <xFant>TFL DO BRASIL IND.QUIMICA LTDA</xFant>
- <enderEmit>
  <xLgr>RUA SANTO AGOSTINHO</xLgr>
  <nro>1099</nro>
  <xBairro>SAO MIGUEL</xBairro>
  <cMun>4318705</cMun>
  <xMun>SAO LEOPOLDO</xMun>
  <UF>RS</UF>
  <CEP>93025700</CEP>
  <cPais>1058</cPais>
  <xPais>Brasil</xPais>
  <fone>5140092222</fone>
  </enderEmit>
  <IE>1240007512</IE>
}
      // Verifica se � envio de NFe
      xmlNode := xmlDoc.SelectSingleNode('/nfeProc');
      if assigned(xmlNode) then
      begin
        CNPJ    := '';
        CPF     := '';
        xNome   := '';
        xFant   := '';
        xLgr    := '';
        nro     := '';
        xBairro := '';
        cMun    := '';
        xMun    := '';
        UF      := '';
        CEP     := '';
        cPais   := '';
        xPais   := '';
        fone    := '';
        IEST    := '';
        IM      := '';
        CNAE    := '';
        xEnder  := '';
        //
        case TipoCad of
          entNFe_emit   : xmlNode := xmlDoc.SelectSingleNode('/nfeProc/NFe/infNFe/emit');
          entNFe_dest   : xmlNode := xmlDoc.SelectSingleNode('/nfeProc/NFe/infNFe/dest');
          entNFe_transp : xmlNode := xmlDoc.SelectSingleNode('/nfeProc/NFe/infNFe/transp/transporta');
        end;
        if assigned(xmlNode) then
        begin
          // emit, dest e transp
          CNPJ    := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
          CPF     := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
          xNome   := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_xNome);
          IE      := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_IE);
          // emit e dest
          xFant   := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_xFant);
          // transp
          xEnder  := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_xEnder);
          xMun    := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_xMun);
          UF      := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_UF);
          //
          case TipoCad of
            entNFe_emit   : xmlNode := xmlDoc.SelectSingleNode('/nfeProc/NFe/infNFe/emit/enderEmit');
            entNFe_dest   : xmlNode := xmlDoc.SelectSingleNode('/nfeProc/NFe/infNFe/dest/enderDest');
            entNFe_transp :
            begin
              xmlNode := nil;
              Geral.SeparaEndereco(0, xEnder, xNum, sCpl, sBairro, sLgr, nLgr, Rua);
              nro     := xNum;
              xCpl    := sCpl;
              xBairro := sBairro;
              if nLgr <> '' then
                Lgr_I := Geral.IMV(nLgr);
            end;
          end;
          if assigned(xmlNode) then
          begin
            xLgr    := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_xLgr);
            nro     := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_nro);
            xCpl    := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_xCpl);
            xBairro := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_xBairro);
            cMun    := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_cMun);
            xMun    := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_xMun);
            UF      := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_UF);
            CEP     := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_CEP);
            cPais   := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_cPais);
            xPais   := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_xPais);
            fone    := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_fone);
            IEST    := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_IEST);
            IM      := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_IM);
            CNAE    := UnMyXML.LeNoXML(xmlNode, tnxTextStr, ttx_CNAE);
            //
            DModG.ObtemTipoDeLogradouroERuaDeLogradouro(xLgr, Lgr_I, Rua);
          end;
          Tipo := -1;
          if CNPJ <> '' then Tipo := 0 else
          if CPF  <> '' then Tipo := 1;
          //
          if Tipo = -1 then
          begin
             Tipo := 0;
             Geral.MB_Aviso('N�o foi poss�vel definir o tipo de pessoa!' +
             sLineBreak + 'Ser� setado como Pessoa jur�dica!');
          end;
          RGTipo.ItemIndex := Tipo;
          case Tipo of
            0:
            begin
              EdCNPJ.Text               := CNPJ;
              EdRazaoSocial.Text        := xNome;
              EdFantasia.Text           := xFant;
              EdIE.LinkMsk              := UF;
              EdIE.Text                 := IE;
              //
              EdECEP.Text               := CEP;
              EdELograd.ValueVariant    := Lgr_I;
              CBELograd.KeyValue        := Lgr_I;
              EdERua.Text               := Rua;
              EdENumero.ValueVariant    := Geral.SoNumero_TT(nro);
              EdECompl.Text             := xCpl;
              EdEBairro.Text            := xBairro;
              EdEUF.ValueVariant        := UF;
              if Trim(cMun) <> '' then
              begin
                EdECodMunici.Text       := cMun;
                CBECodMunici.KeyValue   := cMun;
              end;
              if cPais <> '' then
              begin
                EdECodiPais.Text        := cPais;
                CBECodiPais.KeyValue    := cPais;
              end;  
              EdETe1.Text               := fone;
            end;
            1:
            begin
              EdCPF.Text                := CPF;
              EdNome.Text               := xNome;
              EdApelido.Text            := xFant;
              EdRG.Text                 := IE;
              //
              EdPCEP.Text               := CEP;
              EdPLograd.ValueVariant    := Lgr_I;
              CBPLograd.KeyValue        := Lgr_I;
              EdPRua.Text               := Rua;
              EdPNumero.ValueVariant    := Geral.SoNumero_TT(nro);
              EdPCompl.Text             := xCpl;
              EdPBairro.Text            := xBairro;
              EdPUF.ValueVariant        := UF;
              if Trim(cMun) <> '' then
              begin
                EdPCodMunici.Text       := cMun;
                CBPCodMunici.KeyValue   := cMun;
              end;
              if cPais <> '' then
              begin
                EdPCodiPais.Text        := cPais;
                CBPCodiPais.KeyValue    := cPais;
              end;
              EdPTe1.Text               := fone;
            end;
          end;
          EdIEST.Text                   := IEST;
          EdNIRE.Text                   := IM;
          EdCodCNAE.Text                := CNAE;
          if CNAE <> '' then
            CBCodCNAE.KeyValue          := CNAE;
        end;
      end else Geral.MB_Aviso('O arquivo n�o � uma NF-e!');
    end;
  end;
end;

procedure TFmEntidade2.CadastroPorXML_NFe_2(TipoCad: TTipoCadNFe; CNPJ, CPF, xNome,
              xFant, IE, xLgr, nro, xBairro: WideString; cMun: Integer; xMun,
              UF: WideString; CEP,  cPais: Integer; xPais, fone, IEST, IM, CNAE,
              xCpl, xEnder: WideString);

  procedure ChecaCadastro(Texto: String);
  var
    I, N: Integer;
  begin
    for I := 0 to ComponentCount - 1 do
    begin
      if (Components[i] is TdmkCheckBox) then
      begin
        N := Length(Texto);
        if Copy(TdmkCheckBox(Components[i]).Caption, 1, N) = Texto then
          TdmkCheckBox(Components[i]).Checked := True;
      end;
    end;
  end;

var
  Lgr_I, Tipo: Integer; Rua: String;
  // TranspEnder
  nLgr, sLgr, sCpl, xNum, sBairro: String;
begin
  case TipoCad of
    entNFe_emit:
    begin
      ChecaCadastro(CO_FORNECE);
    end;
    entNFe_dest:
    begin
      ChecaCadastro(CO_CLIENTE);
    end;
    entNFe_transp:
    begin
      Geral.SeparaEndereco(0, xEnder, xNum, sCpl, sBairro, sLgr, nLgr, Rua);
      nro     := xNum;
      xCpl    := sCpl;
      xBairro := sBairro;
      if nLgr <> '' then
        Lgr_I := Geral.IMV(nLgr);
      //
      ChecaCadastro(CO_TRANSPORTADOR);
    end;
  end;
  //
  DModG.ObtemTipoDeLogradouroERuaDeLogradouro(xLgr, Lgr_I, Rua);
  //
  Tipo := -1;
  if CNPJ <> '' then Tipo := 0 else
  if CPF  <> '' then Tipo := 1;
  //
  if Tipo = -1 then
  begin
     Tipo := 0;
     Geral.MB_Aviso('N�o foi poss�vel definir o tipo de pessoa!' +
     sLineBreak + 'Ser� setado como Pessoa jur�dica!');
  end;
  case Tipo of
    0:
    begin
      EdCNPJ.Text                 := CNPJ;
      EdRazaoSocial.Text          := xNome;
      EdFantasia.Text             := xFant;
      //
      EdECEP.ValueVariant         := CEP;
      EdELograd.ValueVariant      := Lgr_I;
      CBELograd.KeyValue          := Lgr_I;
      EdERua.Text                 := Rua;
      EdENumero.Text              := nro;
      EdECompl.Text               := xCpl;
      EdEBairro.Text              := xBairro;
      if cMun <> 0 then
      begin
        EdECodMunici.ValueVariant := cMun;
        CBECodMunici.KeyValue     := cMun;
      end;
      EdEUF.Text                  := UF;
      if cPais <> 0 then
      begin
        EdECodiPais.ValueVariant  := cPais;
        CBECodiPais.KeyValue      := cPais;
      end;
      EdETe1.Text                 := fone;
      EdIE.LinkMsk                := UF;
      EdIE.Text                   := IE;
    end;
    1:
    begin
      EdCPF.Text                  := CPF;
      EdNome.Text                 := xNome;
      EdApelido.Text              := xFant;
      EdRG.Text                   := IE;
      //
      EdPCEP.ValueVariant         := CEP;
      EdPLograd.ValueVariant      := Lgr_I;
      CBPLograd.KeyValue          := Lgr_I;
      EdPRua.Text                 := Rua;
      EdPNumero.Text              := nro;
      EdPCompl.Text               := xCpl;
      EdPBairro.Text              := xBairro;
      if cMun <> 0 then
      begin
        EdPCodMunici.ValueVariant := cMun;
        CBPCodMunici.KeyValue     := cMun;
      end;
      EdPUF.Text                  := UF;
      if cPais <> 0 then
      begin
        EdPCodiPais.ValueVariant  := cPais;
        CBPCodiPais.KeyValue      := cPais;
      end;
      EdPTe1.Text                 := fone;
    end;
  end;
  RGTipo.ItemIndex                := Tipo;
  EdIEST.Text                     := IEST;
  EdNIRE.Text                     := IM;
  EdCodCNAE.Text                  := CNAE;
  if CNAE <> '' then
    CBCodCNAE.KeyValue            := CNAE;
end;

procedure TFmEntidade2.PreCadastro(CNPJ, CPF: String; Cliente1, Cliente2,
  Cliente3, Cliente4, Fornece1, Fornece2, Fornece3, Fornece4, Fornece5,
  Fornece6, Fornece7, Fornece8, Terceiro: Boolean);
begin
  if CNPJ <> '' then
  begin
    RGTipo.ItemIndex    := 0;
    EdCNPJ.ValueVariant := CNPJ;
    EdCPF.ValueVariant  := '';
  end else
  if CPF <> '' then
  begin
    RGTipo.ItemIndex    := 0;
    EdCNPJ.ValueVariant := '';
    EdCPF.ValueVariant  := CPF;
  end else
  begin
    RGTipo.ItemIndex    := -1;
    EdCNPJ.ValueVariant := '';
    EdCPF.ValueVariant  := '';
  end;
  CkCliente1_0.Checked := Cliente1;
  CkCliente2_0.Checked := Cliente2;
  CkCliente3_0.Checked := Cliente3;
  CkCliente4_0.Checked := Cliente4;
  CkFornece1_0.Checked := Fornece1;
  CkFornece2_0.Checked := Fornece2;
  CkFornece3_0.Checked := Fornece3;
  CkFornece4_0.Checked := Fornece4;
  CkFornece5_0.Checked := Fornece5;
  CkFornece6_0.Checked := Fornece6;
  CkFornece7_0.Checked := Fornece7;
  CkFornece8_0.Checked := Fornece8;
  CkTerceiro_0.Checked := Terceiro;
end;

procedure TFmEntidade2.BtAtributosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAtributos, BtAtributos);
end;

procedure TFmEntidade2.BtCEP_CClick(Sender: TObject);
begin
  MostraPMCEP(BtCEP_C);
  try
    EdCNumero.SetFocus;
  except
    ;
  end;
end;

procedure TFmEntidade2.BtCEP_EClick(Sender: TObject);
begin
  MostraPMCEP(BtCEP_E);
  try
    EdENumero.SetFocus;
  except
    ;
  end;
end;

procedure TFmEntidade2.BtCEP_LClick(Sender: TObject);
begin
  MostraPMCEP(BtCEP_L);
  try
    EdLNumero.SetFocus;
  except
    ;
  end;
end;

procedure TFmEntidade2.BtCEP_PClick(Sender: TObject);
begin
  MostraPMCEP(BtCEP_P);
  try
    EdPNumero.SetFocus;
  except
    ;
  end;
end;

procedure TFmEntidade2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEntidadesCodigo.Value;
  VAR_ENTIDADE := QrEntidadesCodigo.Value;
  //
  if QrEntidadesTipo.Value = 0 then
    VAR_CAD_NOME := QrEntidadesRazaoSocial.Value
  else
    VAR_CAD_NOME := QrEntidadesNome.Value;
  //
  if TFmEntidade2(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmEntidade2.BtTelefonesClick(Sender: TObject);
begin
  PCShowGer.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMTelefones, BtTelefones);
end;

procedure TFmEntidade2.BtEntiTranspClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntiTransp, BtEntiTransp);
end;

procedure TFmEntidade2.AbreWEBSiteEntidade(Site: String);
begin
  if Site <> '' then
    ShellExecute(Application.Handle, nil, PChar(Site), nil, nil, sw_hide);
end;

procedure TFmEntidade2.Altera1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiCtas, FmEntiCtas, afmoNegarComAviso) then
  begin
    FmEntiCtas.ImgTipo.SQLType            := stUpd;
    FmEntiCtas.EdCodigo.ValueVariant      := IntToStr(QrEntiCtasCodigo.Value);
    FmEntiCtas.EdControle.ValueVariant    := IntToStr(QrEntiCtasControle.Value);
    FmEntiCtas.EdCedBanco.ValueVariant    := IntToStr(QrEntiCtasBanco.Value);
    FmEntiCtas.EdOrdem.ValueVariant       := IntToStr(QrEntiCtasOrdem.Value);
    FmEntiCtas.EdCedAgencia.ValueVariant  := IntToStr(QrEntiCtasAgencia.Value);
    FmEntiCtas.EdCedDAC_A.ValueVariant    := QrEntiCtasDAC_A.Value;
    FmEntiCtas.EdCedConta.ValueVariant    := QrEntiCtasContaCor.Value;
    FmEntiCtas.EdCedDAC_C.ValueVariant    := QrEntiCtasDAC_C.Value;
    FmEntiCtas.EdCedDAC_AC.ValueVariant   := QrEntiCtasDAC_AC.Value;
    FmEntiCtas.EdCedContaTip.ValueVariant := QrEntiCtasContaTip.Value;
    FmEntiCtas.ShowModal;
    FmEntiCtas.Destroy;
    ReopenEntiCtas;
  end;  
end;

procedure TFmEntidade2.AlteraContatoatual1Click(Sender: TObject);
begin
  MostraEntiContat(stUpd);
end;

procedure TFmEntidade2.Alteraemailselecionado1Click(Sender: TObject);
begin
  MostraEntiMail(stUpd);
end;

procedure TFmEntidade2.Alteraentidadeatual1Click(Sender: TObject);
var
  Visivel: Boolean;
begin
  if (QrEntidades.State = dsInactive) or (QrEntidades.RecordCount = 0) or
    (QrEntidadesCodigo.Value = 0)
 then
   Exit;
  //
  MeObservacoes_Edit.Text := MeObservacoes_Show.Text;
  PCEditGer.ActivePageIndex := 0;
  Visivel := QrEntidadesCodigo.Value < -10;
  LaFilial.Visible := Visivel;
  EdFilial.Visible := Visivel;
  //
  if QrEntidadesTipo.Value = 0 then
    EdIE.LinkMsk := Geral.GetSiglaUF_do_CodigoUF(QrEntidadesEUF.Value)
  else
    EdIE.LinkMsk := Geral.GetSiglaUF_do_CodigoUF(QrEntidadesPUF.Value);
  //
  FSetando := True;
  //
  ReopenMunici(QrEMunici, EdEUF.ValueVariant);
  ReopenMunici(QrPMunici, EdPUF.ValueVariant);
  ReopenMunici(QrLMunici, EdLUF.ValueVariant);
  ReopenMunici(QrCMunici, EdCUF.ValueVariant);
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrEntidades, [PainelDados],
    [PainelEdita], RGTipo, ImgTipo, 'entidades');
  //
  if UpperCase(QrEntidadesSexo.Value) = 'M' then
    CBSexo.ItemIndex := 0
  else if UpperCase(QrEntidadesSexo.Value) = 'F' then
    CBSexo.ItemIndex := 1
  else
    CBSexo.ItemIndex := -1;
  //
  CBSexo.Text := QrEntidadesSexo.Value;
  //
  FSetando := False;
end;

procedure TFmEntidade2.AlteraAtributo1Click(Sender: TObject);
begin
  MostraFormAtrEntiDef(stUpd);
end;

procedure TFmEntidade2.Alteraresponsvelatual1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrEntiResponControle.Value;
  //
  UmyMod.FormInsUpd_Show(TFmEntiRespon, FmEntiRespon, afmoNegarComAviso,
    QrEntiRespon, stUpd);
  //
  ReopenEntiRespon(Controle);
end;

procedure TFmEntidade2.Alteratelefoneatual1Click(Sender: TObject);
begin
  MostraEntiTel(stUpd);
end;

procedure TFmEntidade2.Atualiza9dgito1Click(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmEnti9Digit, FmEnti9Digit, afmoNegarComAviso) then
  begin
    FmEnti9Digit.ShowModal;
    FmEnti9Digit.Destroy;
  end;
*)
end;

procedure TFmEntidade2.BtEMailsClick(Sender: TObject);
begin
  PCShowGer.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMEMails, BtEMails);
end;

procedure TFmEntidade2.BtEntidadeClick(Sender: TObject);
begin
  PCShowGer.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMEntidades, BtEntidade);
end;

procedure TFmEntidade2.BtEntiSrvProClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntiSrvPro, BtEntiSrvPro);
end;

procedure TFmEntidade2.BtGruposClick(Sender: TObject);
begin
  MostraFormEntiGrupos(0);
end;

procedure TFmEntidade2.SBAssinaturasClick(Sender: TObject);
begin
  Entities.MostraFormEntiAssina(QrEntidadesCodigo.Value);
end;

procedure TFmEntidade2.SBConfigClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConfig, SBConfig);
end;

procedure TFmEntidade2.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome, Sexo, Msg: String;
  Status: TSQLType;
  Aba: Boolean;
  //Confirma: Boolean;
begin
  //Confirma := True;
  if (ImgTipo.SQLType = stUpd) and (QrEntidadesCodigo.Value < -10) and
    (EdFilial.ValueVariant = 0) then
  begin
    PCEditGer.ActivePageIndex := 0;
    Geral.MB_Aviso('Informe a filial!');
    Exit;
  end;
  //
  if EdCodUsu.ValueVariant = 0 then
  begin
    QrNext.Close;
    UMyMod.AbreQuery(QrNext, Dmod.MyDB);
    if QrNextCodUsu.Value < 1 then
      EdCodUsu.ValueVariant := 1
    else
      EdCodUsu.ValueVariant := QrNextCodUsu.Value + 1;
  end;
  //
  case RGTipo.ItemIndex of
    0: Nome := EdRazaoSocial.Text;
    1: Nome := EdNome.Text;
  end;
  if Length(Trim(Nome)) = 0 then
  begin
    PCEditGer.ActivePageIndex := 0;
    Geral.MB_Aviso('Defina uma descri��o para a entidade!');
    Exit;
  end;
  if not TipoDeCadastroDefinido() then Exit;
  if ((RGTipo.ItemIndex = 0) and (Length(Geral.SoNumero1a9_TT(EdCNPJ.Text))>0))
  or ((RGTipo.ItemIndex = 1) and (Length(Geral.SoNumero1a9_TT(EdCPF.Text))>0))
  then
  begin
    case RGTipo.ItemIndex of
      0: if CNPJCPFDuplicado(EdCNPJ.Text, 0) then Exit;
      1: if CNPJCPFDuplicado(EdCPF.Text, 1) then Exit;
    end;
  end;
  case RGTipo.ItemIndex of
    0: if not Geral.ConfereUFeMunici_IBGE(EdEUF.Text, EdECodMunici.ValueVariant, 'Pessoa Jur�dica') then Exit;
    1: if not Geral.ConfereUFeMunici_IBGE(EdPUF.Text, EdPCodMunici.ValueVariant, 'Pessoa F�sica') then Exit;
  end;
  if not Geral.ConfereUFeMunici_IBGE(EdCUF.Text, EdCCodMunici.ValueVariant, 'Endere�o de coleta') then Exit;
  if not Geral.ConfereUFeMunici_IBGE(EdLUF.Text, EdLCodMunici.ValueVariant, 'Endere�o de entrega') then Exit;
  if not VerificaUmSohDocumentoLocalDeEntrega() then Exit;
  (*
  if MyObjects.FIC((Trim(EdL_CNPJ.Text) <> '') and (Trim(EdL_CPF.Text) <> ''),
  nil, 'Defina apenas o CNPJ ou o CPF no endere�o de entrega!') then Exit;
  *)
  //
  if (ImgTipo.SQLType = stIns) and (RGTipo.ItemIndex  = 0) and (EdIE.ValueVariant = '') then
  begin
    if VAR_SOMAIUSCULAS = True then
      EdIE.ValueVariant := 'ISENTO'
    else
      EdIE.ValueVariant := 'Isento';
  end;
  //
  if (ImgTipo.SQLType = stIns) and (EdindIEDest.ValueVariant = 0) or
    (RGTipo.ItemIndex = 1) (* Pessoa f�sica *) then
  begin
    if RGTipo.ItemIndex = 0 then //Juridica
      EdindIEDest.ValueVariant := Entities.ObtemindIEDestAuto(EdIE.ValueVariant,
        Geral.GetCodigoUF_da_SiglaUF(EdEUF.ValueVariant), RGTipo.ItemIndex)
    else
      EdindIEDest.ValueVariant := Entities.ObtemindIEDestAuto('',
        Geral.GetCodigoUF_da_SiglaUF(EdPUF.ValueVariant), RGTipo.ItemIndex);
  end;
  //
  if (RGTipo.ItemIndex = 1) then (* Pessoa f�sica *)
  begin
    EdCNPJ.ValueVariant := '';
    EdIE.ValueVariant   := '';
  end else
  begin
    EdCPF.ValueVariant := '';
    EdRG.ValueVariant  := '';
  end;

  if not Entities.ValidaIndicadorDeIEEntidade(EdIE.ValueVariant,
    EdindIEDest.ValueVariant, RGTipo.ItemIndex, True, Msg) then
  begin
    PCEditGer.ActivePageIndex := 0;
    Geral.MB_Aviso(Msg);
    EdIE.SetFocus;
    Exit;
  end;
  //
  VUSexo.ValueVariant := CBSexo.Text;
  //
  Status := ImgTipo.SQLType;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('Entidades', 'Codigo', Status, QrEntidadesCodigo.Value);
  //
  if UMyMod.ExecSQLInsUpdPanel(Status, TFmEntidade2(Self), PainelEdita, 'entidades',
    Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True, False) then
  begin
    //Atualiza dados tabela master
    Entities.AtualizaDadosEntidadeDono(Codigo, RGTipo.ItemIndex,
      EdNome.ValueVariant, EdRazaoSocial.ValueVariant, EdCPF.ValueVariant,
      EdCNPJ.ValueVariant);

    ////////////////////////////////////////////////
    VAR_ENTIDADE := Codigo;

    if TFmEntidade2(Self).Owner is TApplication then
      Aba := False
    else
      Aba := True;

    if FEntTipo = uetNenhum then
      FmPrincipal.AcoesExtrasDeCadastroDeEntidades(nil, Codigo, Aba);
    ////////////////////////////////////////////////
    PCEditGer.ActivePageIndex := 0;
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEntidade2.BtDBancClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDBanc, BtDBanc);
end;

procedure TFmEntidade2.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Entidades', Codigo);
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Entidades', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmEntidade2.BtContatosClick(Sender: TObject);
begin
  PCShowGer.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMContatos, BtContatos);
end;

procedure TFmEntidade2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FFormAtivo      := False;
  FEntiContat     := 0;
  F_indIEDest     := dmkPF.ListaIndicadorDeIE();
  FEntTipo        := uetNenhum;
  //
  TabSheet20.TabVisible := VAR_KIND_DEPTO = kdUH;
  //
  {$IfNDef sPraz}
  BtPrazos.Visible := True;
  {$Else}
  BtPrazos.Visible := False;
  {$EndIf}
  //
  Panel11.Color     := clBtnFace;
  PnShow2PF.Color   := clBtnFace;
  PainelDados.Color := clBtnFace;
  PnShow2Pj.Color   := clBtnFace;
  PnEdit2PJ.Color   := clBtnFace;
  PnEdit2PF.Color   := clBtnFace;
  PainelEdita.Color := clBtnFace;
  PnEdit2.Color     := clBtnFace;
  //
  FEditingByMatriz          := False;
  PCEditGer.ActivePageIndex := 0;
  PCShowGer.ActivePageIndex := 0;
  //
  PCEditGer.Align    := alClient;
  PageControl2.Align := alClient;
  PageControl3.Align := alClient;
  PnEdits.Align      := alClient;
  PnEdit2.Align      := alClient;
  PnEdit2PJ.Align    := alClient;
  PnEdit2PF.Align    := alClient;
  //
  PnShow2PJ.Align := alClient;
  PnShow2PF.Align := alClient;
  PnShowEnd.Align := alClient;
  PCShowCom.Align := alClient;
  PCShowRes.Align := alClient;
  Panel11.Align   := alClient;
  //
  CriaOForm;
  //
  TPPNatal.Date := 0;
  TPENatal.Date := 0;
  TPDataRG.Date := 0;
  //
  UMyMod.AbreQuery(QrListaLograd, Dmod.MyDB);
  //
  Entities.ReopenTipCto(QrPTe1Tip, etcTel);
  Entities.ReopenTipCto(QrPCelTip, etcTel);
  Entities.ReopenTipCto(QrECelTip, etcTel);
  Entities.ReopenTipCto(QrETe1Tip, etcTel);
  //
  UMyMod.AbreQuery(QrEstCivil, Dmod.MyDB);
  UMyMod.AbreQuery(QrEmpresas, Dmod.MyDB);
  //
  ReopenMunici(QrEMunici, '');
  ReopenMunici(QrPMunici, '');
  ReopenMunici(QrCMunici, '');
  ReopenMunici(QrLMunici, '');
  //
  try
    UMyMod.AbreQuery(QrEBacen_Pais, DModG.AllID_DB);
    UMyMod.AbreQuery(QrPBacen_Pais, DModG.AllID_DB);
    UMyMod.AbreQuery(QrLBacen_Pais, DModG.AllID_DB);
    UMyMod.AbreQuery(QrCNAE21Cad, DModG.AllID_DB);
  {$IfNDef NO_FINANCEIRO}
    UMyMod.AbreQuery(QrCarteiras, Dmod.MyDB);
  {$EndIf}
  except
    DBCheck.AvisaDBTerceiros();
  end;
  Entities.ConfiguraTipoDeCadastro(TCheckBox(CkCliente1), TCheckBox(CkCliente2),
    TCheckBox(CkCliente3), TCheckBox(CkCliente4), TCheckBox(CkFornece1),
    TCheckBox(CkFornece2), TCheckBox(CkFornece3), TCheckBox(CkFornece4),
    TCheckBox(CkFornece5), TCheckBox(CkFornece6), TCheckBox(CkFornece7),
    TCheckBox(CkFornece8), TCheckBox(CkTerceiro));
  //
  Entities.ConfiguraTipoDeCadastro(CkCliente1_0, CkCliente2_0, CkCliente3_0,
    CkCliente4_0, CkFornece1_0, CkFornece2_0, CkFornece3_0, CkFornece4_0,
    CkFornece5_0, CkFornece6_0, CkFornece7_0, CkFornece8_0, CkTerceiro_0);
  //
  PB1.Visible := False;
  //
  DBGEntiTransp.DataSource := DsEntiTransp;
  DBGEntiContat.PopupMenu  := PMContatos;
  DBGEntiTel.PopupMenu     := PMTelefones;
  DBGEntiMail.PopupMenu    := PMEMails;
end;

procedure TFmEntidade2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEntidadesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidade2.SbImagensClick(Sender: TObject);
begin
  Entities.MostraEntiImagens(QrEntidadesCodigo.Value, 0, -1, False);
end;

procedure TFmEntidade2.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmEntidade2.SbMapaClick(Sender: TObject);
var
  MapaEndereco, MapaDescri: String;
begin
  if (QrEntidades.State = dsInactive) or (QrEntidades.RecordCount = 0) then Exit;
  //  
  {$IfDef UsaWSuport}
  UConsultasWeb.MapasDeEntidadesMontaEndereco(fmcadEntidade2,
    QrEntidadesCodigo.Value, MapaEndereco, MapaDescri);
  //
  UConsultasWeb.MapasDeEntidades(fmcadEntidade2, MapaEndereco, MapaDescri);
  {$EndIf}
end;

procedure TFmEntidade2.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEntidade2.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuery, SbNovo);
end;

procedure TFmEntidade2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action             := GOTOy.Fechar(ImgTipo.SQLType);
  F_CNPJ_A_CADASTRAR := '';
end;

procedure TFmEntidade2.QrCadastro2CalcFields(DataSet: TDataSet);
begin
  QrCadastro2TE1_TXT.Value  := Geral.FormataTelefone_TT_Curto(QrCadastro2Te1.Value);
  QrCadastro2TE2_TXT.Value  := Geral.FormataTelefone_TT_Curto(QrCadastro2Te2.Value);
  QrCadastro2TE3_TXT.Value  := Geral.FormataTelefone_TT_Curto(QrCadastro2Te3.Value);
  QrCadastro2FAX_TXT.Value  := Geral.FormataTelefone_TT_Curto(QrCadastro2Fax.Value);
  QrCadastro2CEL_TXT.Value  := Geral.FormataTelefone_TT_Curto(QrCadastro2Cel.Value);
  QrCadastro2DOCA_TXT.Value := Geral.FormataCNPJ_TT(QrCadastro2DOCA.Value);
  ////
  QrCadastro2CEP_TXT.Value   := Geral.FormataCEP_NT(QrCadastro2CEP.Value);
  QrCadastro2CCEP_TXT.Value  := Geral.FormataCEP_NT(QrCadastro2CCEP.Value);
  QrCadastro2LCEP_TXT.Value  := Geral.FormataCEP_NT(QrCadastro2LCEP.Value);
  ////
  if QrCadastro2Numero.Value = 0 then
     QrCadastro2NUM_TXT.Value := 'S/N' else
     QrCadastro2NUM_TXT.Value :=
     FloatToStr(QrCadastro2Numero.Value);
  if QrCadastro2CNumero.Value = 0 then
     QrCadastro2CNUM_TXT.Value := 'S/N' else
     QrCadastro2CNUM_TXT.Value :=
     FloatToStr(QrCadastro2CNumero.Value);
  if QrCadastro2LNumero.Value = 0 then
     QrCadastro2LNUM_TXT.Value := 'S/N' else
     QrCadastro2LNUM_TXT.Value :=
     FloatToStr(QrCadastro2LNumero.Value);
  //
  if QrCadastro2Tipo.Value = 0 then
    QrCadastro2NATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE3, QrCadastro2ENatal.Value)
  else
    QrCadastro2NATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE3, QrCadastro2PNatal.Value);
end;

procedure TFmEntidade2.QrEntiContatAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiMail(0);
  ReopenEntiTel(0);
end;

procedure TFmEntidade2.QrEntiContatBeforeClose(DataSet: TDataSet);
begin
  QrEntiMail.Close;
  QrEntiTel.Close;
end;

procedure TFmEntidade2.QrEntiContatCalcFields(DataSet: TDataSet);
begin
  QrEntiContatDTANATAL_TXT.Value := dmkPF.FDT_NULO(QrEntiContatDtaNatal.Value, 2);
end;

procedure TFmEntidade2.QrEntidadesAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  QueryPrincipalAfterOpen;
  Habilita := QrEntidades.RecordCount > 0;
  BtDBanc.Enabled      := Habilita;
  BtContatos.Enabled   := Habilita;
  BtRespon.Enabled     := Habilita;
  BtTelefones.Enabled  := Habilita;
  BtEMails.Enabled     := Habilita;
  BtEntiSrvPro.Enabled := Habilita;
  // 2012-05-05
  //BtAtributos.Enabled := Habilita;
  //Geral.MB_Info(QrEntidades.SQL.Text);
end;

procedure TFmEntidade2.QrEntidadesAfterScroll(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  case QrEntidadesTipo.Value of
    0:
    begin
      PnShow2PJ2.Visible := True;
      PnShow2PJ.Visible  := True;
      PnShow2PF.Visible  := False;
      PnShow2PF2.Visible := False;
      //
      DBLaindIEDest.Visible     := True;
      DBEdindIEDest.Visible     := True;
      DBEdindIEDest_TXT.Visible := True;
    end;
    1:
    begin
      PnShow2PF.Visible  := True;
      PnShow2PF2.Visible := True;
      PnShow2PJ.Visible  := False;
      PnShow2PJ2.Visible := False;
      //
      DBLaindIEDest.Visible     := False;
      DBEdindIEDest.Visible     := False;
      DBEdindIEDest_TXT.Visible := False;
    end;
  end;
  //
{
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqEntidades, DModG.MyCompressDB, [
  'SELECT Observacoes ',
  'FROM entidades ',
  'WHERE Codigo=' + Geral.FF0(QrEntidadesCodigo.Value),  // 843. S o f t C o u r o
  '']);
  MeObservacoes_Show.Text := USQLDB.Str(DqEntidades, 'Observacoes');
  //MeObservacoes_Edit.Text := USQLDB.Str(DqEntidades, 'Observacoes');
}
  MeObservacoes_Show.Text := DMod.MyDB.SelectStringDef(
      'SELECT Observacoes FROM entidades WHERE Codigo=' +
      Geral.FF0(QrEntidadesCodigo.Value), '');

  //
  ReopenEntiGrupos(0);
  ReopenEntiContat(0);
  ReopenEntiRespon(0);
  ReopenEntiCtas();
  ReopenEntiSrvPro();
  ReopenAtrEntiDef(0);
  ReopenEntiTransp(0);
  //
  Habilita := QrEntidadesCodigo.Value <> 0;
  //
  SBPSite.Enabled := Habilita;
  SBESite.Enabled := Habilita;
  //
  MyObjects.HabilitaBtnToPopupMenuIts(BtDBanc     , Habilita, QrEntiCtas);
  MyObjects.HabilitaBtnToPopupMenuIts(BtContatos  , Habilita, QrEntiContat);
  MyObjects.HabilitaBtnToPopupMenuIts(BtTelefones , Habilita, QrEntiTel);
  MyObjects.HabilitaBtnToPopupMenuIts(BtEMails    , Habilita, QrEntiMail);
  MyObjects.HabilitaBtnToPopupMenuIts(BtAtributos , Habilita, QrAtrEntiDef);
  MyObjects.HabilitaBtnToPopupMenuIts(BtRespon    , Habilita, QrEntiRespon);
  MyObjects.HabilitaBtnToPopupMenuIts(BtEntiSrvPro, Habilita, QrEntiSrvPro);
  MyObjects.HabilitaBtnToPopupMenuIts(BtEntiTransp, Habilita, QrEntiTransp);
  //
  CarregaAbaEspecifica(PCShowGer.ActivePageIndex);
  DBGBEstrangeiro.Visible := QrEntidadesEstrangDef.Value = 1;
end;

procedure TFmEntidade2.FormActivate(Sender: TObject);
var
  CNPJ: String;
  Tam: Integer;
begin
  FFormAtivo := True;
  //
  if TFmEntidade2(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
    CNPJ := Geral.SoNumero_TT(F_CNPJ_A_CADASTRAR);
    Tam := Length(CNPJ);
    if Tam > 10 then
    begin
      F_CNPJ_A_CADASTRAR := '';
      Clipboard.asText := CNPJ;
      Incluinovaentidade1Click(Self);
      if Tam > 11 then
        RGTipo.ItemIndex := 0
      else
        RGTipo.ItemIndex := 1;
      Geral.MB_Info('O CNPJ ' + Geral.FormataCNPJ_TT(CNPJ) +
      ' foi copiado para � de transfer�ncia e poder� ser colado manualmente no local adequado na p�gina do navegador!');
      MyObjects.MostraPopUpDeBotao(PMImporta, BtImporta);
    end;
  end;
  try
    if RGTipo.Focused then
      case RGTipo.ItemIndex of
        0:
        begin
        try
          EdRazaoSocial.SetFocus;
        except
          ;
        end;
        end;
        1:
        begin
          try
            EdNome.SetFocus;
          except
            ;
          end;
        end;
      end;
  except
    //
  end;
end;

procedure TFmEntidade2.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEntidadesCodigo.Value,
    CuringaLoc.CriaFormEnti(CO_CODIGO, 'RazaoSocial', CO_NOME, 'Fantasia',
      'Apelido', 'Entidades', 'Tipo', 0, Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEntidade2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidade2.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmEntidade2.frxCadastroGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'ENTIDADE' then
  begin
    if QrEntidadesCodigo.Value > 0 then
      Value := 'Dados cadastrais de '+
      QrEntidadesNOMEENTIDADE.Value+' ['+
      FormatFloat('000', QrEntidadesCodigo.Value)+']'
    else Value := 'Dados cadastrais de:';
  end;
  if VarName = '_FANTASIA' then
    if QrEntidadesTipo.Value = 0 then Value := 'Fantasia:' else Value := 'Apelido:'
  else
  if VarName = '_CNPJ_CPF' then
    if QrEntidadesTipo.Value = 0 then Value := 'CNPJ:' else Value := 'CPF:'
  else
  if VarName = '_IE_RG' then
    if QrEntidadesTipo.Value = 0 then Value := 'I.E.:' else Value := 'RG:'
  else
  if VarName = '_NATAL' then
    if QrEntidadesTipo.Value = 0 then Value := 'Funda��o:' else Value := 'Nascimento:'
  else
  if VarName = 'VARF_OBSERVACOES' then
  begin
    Value := DMod.MyDB.SelectStringDef(
      'SELECT Observacoes FROM entidades WHERE Codigo=' +
      Geral.FF0(QrEntidadesCodigo.Value), '');
  end;
end;

procedure TFmEntidade2.Gerenciacontatos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContatosEnt, FmContatosEnt, afmoNegarComAviso) then
  begin
    FmContatosEnt.IniciaPesqELocContato(QrEntiContat, QrEntiContatNome.Value,
      QrEntiContatControle.Value, QrEntiContatCodigo.Value);
    FmContatosEnt.ShowModal;
    FmContatosEnt.Destroy;
    //
    ReopenEnticontat(0);
  end;
end;

procedure TFmEntidade2.ImportaCadastroReceita(Estadual: Boolean);

  procedure PesquisaSINTEGRA(UF: String);
  begin
    if Length(UF) = 2 then
    begin
      RGTipo.Itemindex := 0;
      UConsultasWeb.ConsultaIE(UF, EdCNPJ, EdIE, EdRazaoSocial, EdECep, EdERua,
        EdELograd, EdECompl, EdENumero, EdEBairro, nil, EdEUF, nil,
        CBELograd, CBECodMunici, CBECodiPais, CBCodCNAE, EdECodMunici,
        EdECodiPais, EdCodCNAE, EdETe1, nil, EdSUFRAMA);
    end;
  end;

  procedure ImportaRF(Tipo: Integer);
  begin
    case Tipo of
      0:
        UConsultasWeb.ConsultaCNPJ(EdCNPJ, EdIE, EdRazaoSocial, EdECep, EdERua,
          EdELograd, EdECompl, EdENumero, EdEBairro, nil, EdEUF, nil, CBELograd,
          CBECodMunici, CBECodiPais, CBCodCNAE, EdECodMunici, EdECodiPais,
          EdCodCNAE);
      1:
        UConsultasWeb.ConsultaCPF(EdNome, EdCPF);
      else
        Geral.MB_Aviso('Defina o tipo de pessoa e tente novamente!');
    end;
  end;
var
  TipoImport, TipoEnt: Integer;
  UFImport, UF: String;
begin
  UF         := '';
  TipoImport := RGTipo.ItemIndex;
  //
  case TipoImport of
      0: UFImport := EdEUF.ValueVariant;
      1: UFImport := EdPUF.ValueVariant;
    else UFImport := '';
  end;
  //
  Application.CreateForm(TFmEntiImporCfg, FmEntiImporCfg);
  if Estadual then
  begin
    FmEntiImporCfg.LaUF.Visible     := True;
    FmEntiImporCfg.CBUF.Visible     := True;
    FmEntiImporCfg.LaUFInfo.Visible := True;
    FmEntiImporCfg.LaUF.Top         := 12;
    FmEntiImporCfg.CBUF.Top         := 29;
    FmEntiImporCfg.LaUFInfo.Top     := 32;
    FmEntiImporCfg.RGTipo.Visible   := False;
  end else
  begin
    FmEntiImporCfg.LaUF.Visible     := False;
    FmEntiImporCfg.CBUF.Visible     := False;
    FmEntiImporCfg.LaUFInfo.Visible := False;
    FmEntiImporCfg.RGTipo.Visible   := True;
    FmEntiImporCfg.RGTipo.Top       := 12;
  end;
  FmEntiImporCfg.CBUF.Text        := UFImport;
  FmEntiImporCfg.RGTipo.ItemIndex := TipoImport;
  FmEntiImporCfg.ShowModal;
  //
  if Estadual then
  begin
    TipoEnt := -1;
    UF      := FmEntiImporCfg.FUF;
  end else
  begin
    TipoEnt := FmEntiImporCfg.FTipoEnt;
    UF      := '';
  end;
  //
  FmEntiImporCfg.Destroy;
  //
  if (UF = '') and (TipoEnt = -1) then Exit;
  //
  if UF <> '' then
  begin
    RGTipo.ItemIndex := 0;
    //
    PesquisaSINTEGRA(UF)
  end else if TipoEnt in [0,1] then
  begin
    RGTipo.ItemIndex := TipoEnt;
    //
    ImportaRF(TipoEnt);
  end;
end;

procedure TFmEntidade2.Inclui1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiCtas, FmEntiCtas, afmoNegarComAviso) then
  begin
    FmEntiCtas.ImgTipo.SQLType       := stIns;
    FmEntiCtas.EdCodigo.ValueVariant := QrEntidadesCodigo.Value;
    FmEntiCtas.ShowModal;
    FmEntiCtas.Destroy;
    ReopenEntiCtas;
  end;
end;

procedure TFmEntidade2.Inclui2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiTransp, FmEntiTransp, afmoNegarComAviso) then
  begin
    FmEntiTransp.FCodigo := QrEntidadesCodigo.Value;
    FmEntiTransp.ShowModal;
    FmEntiTransp.Destroy;
    //
    ReopenEntiTransp(0);
  end;
end;

procedure TFmEntidade2.Internet1Click(Sender: TObject);
begin
  case RGTipo.Itemindex of
    0:
    U_CEP.ConsultaCEP2(EdECEP, EdELograd, EdERua, EdENumero, EdEBairro,
      nil, EdEUF, nil, EdENumero, EdECompl, CBELograd, CBECodMunici,
      CBECodiPais, EdECodMunici, EdECodiPais);
    1:
    U_CEP.ConsultaCEP2(EdPCEP, EdPLograd, EdPRua, EdPNumero, EdPBairro,
      nil, EdPUF, nil, EdPNumero, EdPCompl, CBPLograd, CBPCodMunici,
      CBPCodiPais, EdPCodMunici, EdPCodiPais);
    else Geral.MB_Aviso('Selecione o tipo de cadastro!');
  end;
end;

procedure TFmEntidade2.Incluinovaentidade1Click(Sender: TObject);
begin
  MeObservacoes_Show.Text := '';
  //
  PCEditGer.ActivePageIndex := 0;
  //Evitar mensagem dmkGeral
  EdIE.Text := '';
  FSetando  := True;
  //
  ReopenMunici(QrEMunici, '');
  ReopenMunici(QrPMunici, '');
  ReopenMunici(QrLMunici, '');
  ReopenMunici(QrCMunici, '');
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrEntidades, [PainelDados],
    [PainelEdita], RGTipo, ImgTipo, 'entidades');
  //
  ConfiguraTipoEntidade(FEntTipo);
  //
  CBSexo.ItemIndex := -1;
  RGTipo.ItemIndex := -1;
  //
  RGTipoClick(Self);
  //
  FSetando := False;
end;

procedure TFmEntidade2.Incluinovocontato1Click(Sender: TObject);
begin
  MostraEntiContat(stIns);
end;

procedure TFmEntidade2.Incluinovocontatoporatrelamento1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiConEnt, FmEntiConEnt, afmoNegarComAviso) then
  begin
    FmEntiConEnt.ReopenEntidades(QrEntidadesCodigo.Value);
    FmEntiConEnt.FQrEntiConEnt := QrEntiContat;
    FmEntiConEnt.FDatabase := Dmod.MyDB;
    FmEntiConEnt.ShowModal;
    FmEntiConEnt.Destroy;
  end;
end;

procedure TFmEntidade2.Incluinovoresponsvel1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmEntiRespon, FmEntiRespon, afmoNegarComAviso,
    QrEntiRespon, stIns);
  //
  ReopenEntiRespon(0);
end;

procedure TFmEntidade2.Incluinovotelefone1Click(Sender: TObject);
begin
  MostraEntiTel(stIns);
end;

procedure TFmEntidade2.IncluiAtributo1Click(Sender: TObject);
begin
  MostraFormAtrEntiDef(stIns);
end;

procedure TFmEntidade2.QrEntidadesBeforeClose(DataSet: TDataSet);
begin
  MeObservacoes_Show.Text := '';
  //
  QrEntiGrupos.Close;
  QrEntiContat.Close;
  QrEntiSrvPro.Close;
  QrAtrEntiDef.Close;
  QrEntiTransp.Close;
  //
  QrCondImov.Close;
  //
  DBLaindIEDest.Visible     := False;
  DBEdindIEDest.Visible     := False;
  DBEdindIEDest_TXT.Visible := False;
  //
  SBPSite.Enabled := False;
  SBESite.Enabled := False;
  //
  BtDBanc.Enabled      := False;
  BtContatos.Enabled   := False;
  BtRespon.Enabled     := False;
  BtTelefones.Enabled  := False;
  BtEMails.Enabled     := False;
  BtEntiSrvPro.Enabled := False;
  BtAtributos.Enabled  := False;
end;

procedure TFmEntidade2.QrEntidadesBeforeOpen(DataSet: TDataSet);
begin
  QrEntidadesCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEntidade2.QrEntidadesCalcFields(DataSet: TDataSet);
begin
  QrEntidadesETE1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesETe1.Value);
  QrEntidadesETE2_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesETe2.Value);
  QrEntidadesETE3_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesETe3.Value);
  QrEntidadesEFAX_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesEFax.Value);
  QrEntidadesECEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesECel.Value);
  QrEntidadesCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCNPJ.Value);
  QrEntidadesL_CNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesL_CNPJ.Value);
  QrEntidadesL_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesL_CPF.Value);
  //
  QrEntidadesPTE1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesPTe1.Value);
  QrEntidadesPTE2_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesPTe2.Value);
  QrEntidadesPTE3_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesPTe3.Value);
  QrEntidadesPFAX_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesPFax.Value);
  QrEntidadesPCEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesPCel.Value);
  QrEntidadesPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF.Value);
  QrEntidadesCPF_PAI_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF_Pai.Value);
  QrEntidadesCPF_Resp1_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF_Resp1.Value);
  //
  QrEntidadesCTEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesCTel.Value);
  QrEntidadesCFAX_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesCFax.Value);
  QrEntidadesCCEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesCCel.Value);
  //
  QrEntidadesLTEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesLTel.Value);
  QrEntidadesLFAX_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesLFax.Value);
  QrEntidadesLCEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesLCel.Value);
  //
  QrEntidadesECEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesECEP.Value));
  QrEntidadesPCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesPCEP.Value));
  QrEntidadesCCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesCCEP.Value));
  QrEntidadesLCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesLCEP.Value));
  //
  QrEntidadesENUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesERua.Value, QrEntidadesENumero.Value, False);
  QrEntidadesPNUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesPRua.Value, QrEntidadesPNumero.Value, False);
  QrEntidadesCNUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesCRua.Value, QrEntidadesCNumero.Value, False);
  QrEntidadesLNUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesLRua.Value, QrEntidadesLNumero.Value, False);
  //
  QrEntidadesIE_TXT.Value := Geral.Formata_IE(QrEntidadesIE.Value, QrEntidadesEUF.Value, '??');
  QrEntidadesIEST_TXT.Value := Geral.Formata_IE(QrEntidadesIEST.Value, QrEntidadesEUF.Value, '??');
  //
  QrEntidadesDATARG_TXT.Value := dmkPF.FDT_NULO(QrEntidadesDataRG.Value, 2);
  QrEntidadesENATAL_TXT.Value := dmkPF.FDT_NULO(QrEntidadesENatal.Value, 2);
  QrEntidadesPNATAL_TXT.Value := dmkPF.FDT_NULO(QrEntidadesPNatal.Value, 2);
  //
  {
  case QrEntidadesUserCad.Value of
    -2: QrEntidadesNOMECAD2.Value := 'BOSS [Administrador]';
    -1: QrEntidadesNOMECAD2.Value := 'MASTER [Admin. DERMATEK]';
     0: QrEntidadesNOMECAD2.Value := 'N�o definido';
    else QrEntidadesNOMECAD2.Value := QrEntidadesNOMECAD.Value;
  end;
  case QrEntidadesUserAlt.Value of
    -2: QrEntidadesNOMEALT2.Value := 'BOSS [Administrador]';
    -1: QrEntidadesNOMEALT2.Value := 'MASTER [Admin.DERMATEK]';
     0: QrEntidadesNOMEALT2.Value := '- - -';
    else QrEntidadesNOMEALT2.Value := QrEntidadesNOMEALT.Value;
  end;
  }
end;

procedure TFmEntidade2.QrEntiResponCalcFields(DataSet: TDataSet);
begin
  QrEntiResponMandatoIni_TXT.Value := Geral.FDT(QrEntiResponMandatoIni.Value, 3);
  QrEntiResponMandatoFim_TXT.Value := Geral.FDT(QrEntiResponMandatoFim.Value, 3);
  QrEntiResponTe1_TXT.Value        := Geral.FormataTelefone_TT_Curto(QrEntiResponTe1.Value);
  QrEntiResponCel_TXT.Value        := Geral.FormataTelefone_TT_Curto(QrEntiResponCel.Value);
end;

procedure TFmEntidade2.QrEntiTelCalcFields(DataSet: TDataSet);
begin
  QrEntiTelTEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntiTelTelefone.Value);
end;

procedure TFmEntidade2.ReopenAtrEntiDef(ID_Item: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrEntiDef, Dmod.MyDB, [
    'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ATRITS, ',
    'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,',
    'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp ',
    'FROM atrentidef def ',
    'LEFT JOIN atrentiits its ON its.Controle=def.AtrIts ',
    'LEFT JOIN atrenticad cad ON cad.Codigo=def.AtrCad ',
    'WHERE def.ID_Sorc=' + Geral.FF0(QrEntidadesCodigo.Value),
    ' ',
    'UNION  ',
    ' ',
    'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, ',
    'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt,',
    'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp ',
    'FROM atrentitxt def ',
    'LEFT JOIN atrenticad cad ON cad.Codigo=def.AtrCad ',
    'WHERE def.ID_Sorc=' + Geral.FF0(QrEntidadesCodigo.Value),
    ' ',
    'ORDER BY NO_CAD, NO_ITS ',
    '',
    '']);
  QrAtrEntiDef.Locate('ID_Item', ID_Item, []);
end;

procedure TFmEntidade2.ReopenEntiGrupos(Codigo: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrEntiGrupos, Dmod.MyDB, [
    'SELECT ',
    'IF(gru.Principal <> 0, CONCAT(gru.Nome, " [", IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome), "]"), gru.Nome) Nome, ',
    'its.* ',
    'FROM entigruits its ',
    'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade ',
    'LEFT JOIN entigrupos gru ON gru.Codigo = its.Codigo ',
    'WHERE its.Entidade=' + Geral.FF0(QrEntidadesCodigo.Value),
    '']);
  if Codigo <> 0 then
    QrEntiGrupos.Locate('Codigo', Codigo, []);
end;

procedure TFmEntidade2.ReopenEntiTransp(Conta: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrEntiTransp, Dmod.MyDB, [
    'SELECT et.*, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ',
    'ELSE en.Nome END NOMEENTIDADE ',
    'FROM entitransp et, entidades en ',
    'WHERE et.Transp=en.Codigo ',
    'AND et.Codigo=' + Geral.FF0(QrEntidadesCodigo.Value),
    'ORDER BY Ordem ',
    '']);
  //
  if Conta <> 0 then
    QrEntiTransp.Locate('Conta', Conta, []);
end;

procedure TFmEntidade2.ReopenEntiContat(Controle: Integer);
var
  Ctrl: Integer;
begin
(*
  QrEntiContat.Close;
  QrEntiContat.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrEntiContat, Dmod.MyDB);
  //
*)
  if Controle <> 0 then
    Ctrl := Controle
  else if FEntiContat <> 0 then
    Ctrl := FEntiContat
  else
    Ctrl := 0;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrEntiContat, Dmod.MyDB, [
    'SELECT eco.Codigo, eco.Controle, eco.Nome, eco.Cargo,',
    'eca.Nome NOME_CARGO, eco.DtaNatal, eco.Sexo, eco.Entidade,',
    'ELT(eco.Sexo+1, "NI", "M", "F", "?") SEXO_TXT,',
    'IF(eco.DtaNatal <= "1899-12-30", "", ',
    ' DATE_FORMAT(eco.DtaNatal, "%d/%m/%Y")) DTANATAL_TXT, ',
    // NFe 3.10
    'IF(eco.Tipo=0, CNPJ, CPF) CNPJ_CPF, ',
    'eco.Tipo, eco.CNPJ, eco.CPF, eco.Aplicacao, eco.Ativo ',
    //
    'FROM enticontat eco',
    'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle',
    'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo',
    'WHERE ece.Codigo=' + Geral.FF0(QrEntidadesCodigo.Value),
    '']);
  //
  if Ctrl <> 0 then
    QrEntiContat.Locate('Controle', Ctrl, []);
end;

procedure TFmEntidade2.ReopenEntiCtas;
begin
  QrEntiCtas.Close;
  QrEntiCtas.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrEntiCtas, DMod.MyDB);
end;

procedure TFmEntidade2.ReopenEntiRespon(Controle: Integer);
begin
  QrEntiRespon.Close;
  QrEntiRespon.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrEntiRespon, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrEntiRespon.Locate('Controle', Controle, []);
end;

procedure TFmEntidade2.ReopenEntiSrvPro;
begin
  QrEntiSrvPro.Close;
  QrEntiSrvPro.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrEntiSrvPro, Dmod.MyDB);
end;

procedure TFmEntidade2.ReopenEntiMail(Conta: Integer);
begin
  QrEntiMail.Close;
  QrEntiMail.Params[0].AsInteger := QrEntiContatControle.Value;
  UMyMod.AbreQuery(QrEntiMail, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiMail.Locate('Conta', Conta, []);
end;

procedure TFmEntidade2.ReopenEntiTel(Conta: Integer);
begin
  QrEntiTel.Close;
  QrEntiTel.Params[0].AsInteger := QrEntiContatControle.Value;
  UMyMod.AbreQuery(QrEntiTel, DMod.MyDB);
  //
  if Conta <> 0 then
    QrEntiTel.Locate('Conta', Conta, []);
end;

procedure TFmEntidade2.RGTipoClick(Sender: TObject);

  procedure ObtemCoMuniciEUFDeFilial(var CodMunici: Integer; var UF: String);
  begin
    CodMunici := 0;
    UF        := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT uf.Nome UF, ',
      'IF(en.Tipo = 0, en.ECodMunici, en.PCodMunici) CodMunici ',
      'FROM entidades en ',
      'LEFT JOIN ufs uf ON uf.Codigo = if(en.Tipo=0, en.EUF, en.PUF) ',
      'WHERE en.Codigo=' + Geral.FF0(DModG.QrFiliLogCodigo.Value),
      '']);
    if DModG.QrAux.RecordCount > 0 then
    begin
      CodMunici := DModG.QrAux.FieldByName('CodMunici').AsInteger;
      UF        := DModG.QrAux.FieldByName('UF').AsString;
    end;
  end;
var
  CodMunici: Integer;
  UF: String;
begin
  if RGTipo.ItemIndex = 0 then
  begin
    LaindIEDest.Visible     := True;
    EdindIEDest.Visible     := True;
    EdindIEDest_TXT.Visible := True;
  end else
  begin
    LaindIEDest.Visible     := False;
    EdindIEDest.Visible     := False;
    EdindIEDest_TXT.Visible := False;
  end;
  //
  BtConfirma.Enabled := True;
  //
  case RGTipo.ItemIndex of
    -1:
    begin
      PnEdit2PJ.Visible  := False;
      PnEdit2PF.Visible  := False;
      BtConfirma.Enabled := False;
    end;
    0:
    begin
      PnEdit2PJ.Visible  := True;
      PnEdit2PF.Visible  := False;
      if not FSetando then
      try
        CodMunici := EdECodMunici.ValueVariant;
        UF        := EdEUF.ValueVariant;
        //
        if (ImgTipo.SQLType = stIns) and (CodMunici = 0) and (UF = '') then
        begin
          ObtemCoMuniciEUFDeFilial(CodMunici, UF);
          //
          EdECodMunici.ValueVariant := CodMunici;
          CBECodMunici.KeyValue     := CodMunici;
          EdEUF.ValueVariant        := UF;
          EdIE.LinkMsk              := UF;
        end;
        if FFormAtivo then
          EdRazaoSocial.SetFocus;
      except
        ;
      end;
    end;
    1:
    begin
      PnEdit2PF.Visible  := True;
      PnEdit2PJ.Visible  := False;
      if not FSetando then
      try
        CodMunici := EdPCodMunici.ValueVariant;
        UF        := EdPUF.ValueVariant;
        //
        if (ImgTipo.SQLType = stIns) and (CodMunici = 0) and (UF = '') then
        begin
          ObtemCoMuniciEUFDeFilial(CodMunici, UF);
          //
          EdPCodMunici.ValueVariant := CodMunici;
          CBPCodMunici.KeyValue     := CodMunici;
          EdPUF.ValueVariant        := UF;
        end;
        if FFormAtivo then
          EdNome.SetFocus;
      except
        ;
      end;
    end;
  end;
end;

{
procedure TFmEntidade2.ReopenPediPrzEmp(Controle: Integer);
begin
  QrPediPrzEmp.Close;
  QrPediPrzEmp.Params[0].AsInteger :=   QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrPediPrzEmp, DMod.MyDB);
  //
  if Controle <> 0 then
    QrPediPrzEmp.Locate('Controle', Controle, []);
end;
}

procedure TFmEntidade2.CNPJCPF1Click(Sender: TObject);
begin
  LocCod(QrEntidadesCodigo.Value,
  CuringaLoc.CriaFormEnti(CO_CODIGO, 'CNPJ', 'CPF', '', '', 'entidades', 'Tipo',
  0, Dmod.MyDB, CO_VAZIO));
end;

function TFmEntidade2.CNPJCPFDuplicado(CNPJCPF: String; Tipo: Integer): Boolean;
begin
  Result := False;
  QrDuplic2.Close;
  QrDuplic2.SQL.Clear;
  if Tipo = 0 then
  begin
    QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
    QrDuplic2.SQL.Add('WHERE CNPJ="'+Geral.SoNumero_TT(EdCNPJ.Text)+'"');
    QrDuplic2.SQL.Add('AND Codigo <> '+EdCodigo.Text);
  end;
  if Tipo = 1 then
  begin
    QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
    QrDuplic2.SQL.Add('WHERE CPF="'+Geral.SoNumero_TT(EdCPF.Text)+'"');
    QrDuplic2.SQL.Add('AND Codigo <> '+EdCodigo.Text);
  end;
  UMyMod.AbreQuery(QrDuplic2, Dmod.MyDB);
  if QrDuplic2.RecordCount > 0 then
  begin
    Result := True;
    Geral.MB_Aviso('Esta entidade j� foi cadastrada com o c�digo n� '+
    IntToStr(QrDuplic2Codigo.Value)+'.');
    QrDuplic2.Close;
    Screen.Cursor := crDefault;
    Exit;
  end;
  QrDuplic2.Close;
end;

function TFmEntidade2.CNPJDuplicado(): Boolean;
begin
  Result := False;
  if ImgTipo.SQLType = stIns then
  begin
    if ((RGTipo.ItemIndex = 0) and (Length(Geral.SoNumero1a9_TT(EdCNPJ.Text))>0))
    or ((RGTipo.ItemIndex = 1) and (Length(Geral.SoNumero1a9_TT(EdCPF.Text))>0))
    then
    begin
      case RGTipo.ItemIndex of
        0: Result := CNPJCPFDuplicado(EdCNPJ.Text, 0);
        1: Result := CNPJCPFDuplicado(EdCPF.Text, 1);
      end;
    end;
  end;
end;

procedure TFmEntidade2.ConfiguraComponentesCEP();
begin
{$IfDef cDbTable}
  if FBotaoCep = BtCEP_P then
  begin
    FmEntiCEP.EdCEPLoc.Text := EdPCEP.Text;
    //
    FmEntiCEP.FEdCEP       :=  EdPCEP;
    FmEntiCEP.FEdLograd    :=  EdPLograd;
    FmEntiCEP.FCBLograd    :=  CBPLograd;
    FmEntiCEP.FEdRua       :=  EdPRua;
    FmEntiCEP.FEdBairro    :=  EdPBairro;
    FmEntiCEP.FEdUF        :=  EdPUF;
    FmEntiCEP.FEdCodMunici :=  EdPCodMunici;
    FmEntiCEP.FCBCodMunici :=  CBPCodMunici;
    FmEntiCEP.FEdCodiPais  :=  EdPCodiPais;
    FmEntiCEP.FCBCodiPais  :=  CBPCodiPais;
  end else
  if FBotaoCep = BtCEP_E then
  begin
    FmEntiCEP.EdCEPLoc.Text := EdECEP.Text;
    //
    FmEntiCEP.FEdCEP       :=  EdECEP;
    FmEntiCEP.FEdLograd    :=  EdELograd;
    FmEntiCEP.FCBLograd    :=  CBELograd;
    FmEntiCEP.FEdRua       :=  EdERua;
    FmEntiCEP.FEdBairro    :=  EdEBairro;
    FmEntiCEP.FEdUF        :=  EdEUF;
    FmEntiCEP.FEdCodMunici :=  EdECodMunici;
    FmEntiCEP.FCBCodMunici :=  CBECodMunici;
    FmEntiCEP.FEdCodiPais  :=  EdECodiPais;
    FmEntiCEP.FCBCodiPais  :=  CBECodiPais;
  end else
  if FBotaoCep = BtCEP_C then
  begin
    FmEntiCEP.EdCEPLoc.Text := EdCCEP.Text;
    //
    FmEntiCEP.FEdCEP       :=  EdCCEP;
    FmEntiCEP.FEdLograd    :=  EdCLograd;
    FmEntiCEP.FCBLograd    :=  CBCLograd;
    FmEntiCEP.FEdRua       :=  EdCRua;
    FmEntiCEP.FEdBairro    :=  EdCBairro;
    FmEntiCEP.FEdUF        :=  EdCUF;
    FmEntiCEP.FEdCodMunici :=  EdCCodMunici;
    FmEntiCEP.FCBCodMunici :=  CBCCodMunici;
    //FmEntiCEP.FEdCodiPais  :=  EdCCodiPais;
    //FmEntiCEP.FCBCodiPais  :=  CBCCodiPais;
  end else
  if FBotaoCep = BtCEP_L then
  begin
    FmEntiCEP.EdCEPLoc.Text := EdLCEP.Text;
    //
    FmEntiCEP.FEdCEP       :=  EdLCEP;
    FmEntiCEP.FEdLograd    :=  EdLLograd;
    FmEntiCEP.FCBLograd    :=  CBLLograd;
    FmEntiCEP.FEdRua       :=  EdLRua;
    FmEntiCEP.FEdBairro    :=  EdLBairro;
    FmEntiCEP.FEdUF        :=  EdLUF;
    FmEntiCEP.FEdCodMunici :=  EdLCodMunici;
    FmEntiCEP.FCBCodMunici :=  CBLCodMunici;
    //FmEntiCEP.FEdCodiPais  :=  EdLCodiPais;
    //FmEntiCEP.FCBCodiPais  :=  CBLCodiPais;
  end;


{$Else}


  //Geral.MB_Info('Solicite implementa��o � Dermatek! (DBE Table)');  //  TODO Berlin
{$EndIf}
end;

procedure TFmEntidade2.ConfiguraTipoEntidade(EntTipo: TUnEntTipo);
begin
  case EntTipo of
    uetNenhum:
      ;
    uetCliente1:
      CkCliente1_0.Checked := True;
    uetCliente2:
      CkCliente2_0.Checked := True;
    uetCliente3:
      CkCliente3_0.Checked := True;
    uetCliente4:
      CkCliente4_0.Checked := True;
    uetFornece1:
      CkFornece1_0.Checked := True;
    uetFornece2:
      CkFornece2_0.Checked := True;
    uetFornece3:
      CkFornece3_0.Checked := True;
    uetFornece4:
      CkFornece4_0.Checked := True;
    uetFornece5:
      CkFornece5_0.Checked := True;
    uetFornece6:
      CkFornece6_0.Checked := True;
    uetFornece7:
      CkFornece7_0.Checked := True;
    uetFornece8:
      CkFornece8_0.Checked := True;
    uetTerceiro:
      CkTerceiro_0.Checked := True;
  end;
end;

procedure TFmEntidade2.ReceitaEstadual1Click(Sender: TObject);
begin
  ImportaCadastroReceita(True);
end;

procedure TFmEntidade2.ReceitaFederal1Click(Sender: TObject);
begin
  ImportaCadastroReceita(False);
end;

procedure TFmEntidade2.Remove1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntiTransp, DBGEntiTransp,
    'entitransp', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmEntidade2.ReopenMunici(Query: TmySQLQuery; UF: String);
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT mun.Codigo, mun.Nome');
  Query.SQL.Add('FROM dtb_munici mun');
  if Length(UF) <> 0 then
    Query.SQL.Add('WHERE LEFT(mun.Codigo, 2)="' +
    Geral.FF0(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UF)) + '"');
  Query.SQL.Add('ORDER BY mun.Nome');
  try
    UMyMod.AbreQuery(Query, DModG.AllID_DB);
  except
    DBCheck.AvisaDBTerceiros();
  end;
end;

{
object QrEntidadesLogo: TBlobField
  FieldName = 'Logo'
  Origin = 'entidades.Logo'
  Size = 4
end
object QrEntidadesLogo2: TBlobField
  FieldName = 'Logo2'
  Origin = 'entidades.Logo2'
  Size = 4
end
}
{
object DBMemo1: TDBMemo
  Left = 0
  Top = 0
  Width = 1000
  Height = 289
  Align = alClient
  DataField = 'Observacoes'
  DataSource = DsEntidades
  TabOrder = 0
end
}




end.

