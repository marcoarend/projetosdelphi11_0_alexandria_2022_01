unit EntiGrupos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, Variants,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO;

type
  TFmEntiGrupos = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtGrupo: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEntiGrupos: TmySQLQuery;
    QrEntiGruposCodigo: TIntegerField;
    DsEntiGrupos: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    PMGrupo: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    PMEntidade: TPopupMenu;
    Inclui2: TMenuItem;
    Exclui2: TMenuItem;
    DBGEntiGrupIts: TdmkDBGridZTO;
    QrEntiGruIts: TmySQLQuery;
    DsEntiGruIts: TDataSource;
    QrEntiGruItsControle: TIntegerField;
    QrEntiGruItsEntidade: TIntegerField;
    QrEntiGruItsEntiCargo: TIntegerField;
    QrEntiGruItsEntidade_TXT: TWideStringField;
    QrEntiGruItsEntiCargo_TXT: TWideStringField;
    BtEntidade: TBitBtn;
    Altera2: TMenuItem;
    QrEntiGruposPrincipal: TIntegerField;
    QrEntiGruposNome: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtEntidadeClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEntiGruposAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEntiGruposBeforeOpen(DataSet: TDataSet);
    procedure BtGrupoClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure PMGrupoPopup(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure QrEntiGruposAfterScroll(DataSet: TDataSet);
    procedure QrEntiGruposBeforeClose(DataSet: TDataSet);
    procedure PMEntidadePopup(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure Altera2Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenEntiGruIts(Controle: Integer);
    procedure MostraFormEntiGruIts(SQLType: TSQLType);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEntiGrupos: TFmEntiGrupos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, EntiGruIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntiGrupos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEntiGrupos.MostraFormEntiGruIts(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiGruIts, FmEntiGruIts, afmoNegarComAviso,
    QrEntiGruIts, SQLType) then
  begin
    FmEntiGruIts.FQrEntiGruIts := QrEntiGruIts;
    FmEntiGruIts.FDsEntiGrupos := DsEntiGrupos;
    //
    FmEntiGruIts.DBEdCodigo.DataSource := DsEntiGrupos;
    FmEntiGruIts.DBEdNome.DataSource   := DsEntiGrupos;
    //
    FmEntiGruIts.ShowModal;
    FmEntiGruIts.Destroy;
    //
    Codigo := QrEntiGruposCodigo.Value;
    //
    LocCod(Codigo, Codigo);
    ReopenEntiGruIts(0);
  end;
end;

procedure TFmEntiGrupos.PMEntidadePopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrEntiGrupos.State <> dsInactive) and (QrEntiGrupos.RecordCount > 0);
  Enab2 := (QrEntiGruIts.State <> dsInactive) and (QrEntiGruIts.RecordCount > 0);
  //
  Inclui2.Enabled := Enab;
  Altera2.Enabled := Enab and Enab2;
  Exclui2.Enabled := Enab and Enab2;
end;

procedure TFmEntiGrupos.PMGrupoPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrEntiGrupos.State <> dsInactive) and (QrEntiGrupos.RecordCount > 0);
  //
  Altera1.Enabled := Enab;
  Exclui1.Enabled := Enab;
end;

procedure TFmEntiGrupos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEntiGruposCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEntiGrupos.DefParams;
begin
  VAR_GOTOTABELA := 'entigrupos';
  VAR_GOTOMYSQLTABLE := QrEntiGrupos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add('IF(gru.Principal <> 0, CONCAT(gru.Nome, " [", IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome), "]"), gru.Nome) Nome, ');
  VAR_SQLx.Add('gru.* ');
  VAR_SQLx.Add('FROM entigrupos gru ');
  VAR_SQLx.Add('LEFT JOIN entigruits its ON its.Controle = gru.Principal ');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = its.Entidade ');
  VAR_SQLx.Add('WHERE gru.Codigo > 0');
  //
  VAR_SQL1.Add('AND gru.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND gru.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND gru.Nome Like :P0');
  //
end;

procedure TFmEntiGrupos.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrEntiGrupos.State <> dsInactive) and (QrEntiGrupos.RecordCount > 0) then
  begin
    Codigo := QrEntiGruposCodigo.Value;
    //
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do grupo atual?',
      'entigrupos', 'Codigo', Codigo, Dmod.MyDB) = ID_YES then
    begin
      Va(vpLast);
    end;
  end;
end;

procedure TFmEntiGrupos.Exclui2Click(Sender: TObject);
var
  Codigo, Principal: Integer;
  Qry, QryUpd: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  QryUpd := TmySQLQuery.Create(Dmod);
  try
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntiGruIts, TDBGrid(DBGEntiGrupIts),
      'entigruits', ['Controle'], ['Controle'], istPergunta, '');
    //
    Codigo    := QrEntiGruposCodigo.Value;
    Principal := QrEntiGruposPrincipal.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM entigruits ',
      'WHERE Controle = ' + Geral.FF0(Principal),
      '']);
    if Dmod.QrAux.RecordCount = 0 then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE entigrupos SET Principal=0 ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
    end;
    LocCod(Codigo, Codigo);
  finally
    Qry.Free;
    QryUpd.Free;
  end;
end;

procedure TFmEntiGrupos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEntiGrupos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEntiGrupos.ReopenEntiGruIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiGruIts, Dmod.MyDB, [
    'SELECT IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome) Entidade_TXT, ',
    'car.Nome EntiCargo_TXT, its.* ',
    'FROM entigruits its ',
    'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade ',
    'LEFT JOIN enticargos car ON car.Codigo = its.EntiCargo ',
    'WHERE its.Codigo=' + Geral.FF0(QrEntiGruposCodigo.Value),
    '']);
  if Controle <> 0 then
    QrEntiGruIts.Locate('Controle', Controle, []);
end;

procedure TFmEntiGrupos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEntiGrupos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntiGrupos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntiGrupos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntiGrupos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntiGrupos.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiGrupos.Altera1Click(Sender: TObject);
begin
  if (QrEntiGrupos.State <> dsInactive) and (QrEntiGrupos.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEntiGrupos, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'entigrupos');
  end;
end;

procedure TFmEntiGrupos.BtEntidadeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntidade, BtEntidade);
end;

procedure TFmEntiGrupos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEntiGruposCodigo.Value;
  Close;
end;

procedure TFmEntiGrupos.Altera2Click(Sender: TObject);
begin
  MostraFormEntiGruIts(stUpd);
end;

procedure TFmEntiGrupos.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('entigrupos', 'Codigo', '', '', tsPos,
              ImgTipo.SQLType, QrEntiGruposCodigo.Value);
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'entigrupos', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEntiGrupos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'entigrupos', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEntiGrupos.BtGrupoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMGrupo, BtGrupo);
end;

procedure TFmEntiGrupos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType      := stLok;
  GBEdita.Align        := alClient;
  DBGEntiGrupIts.Align := alClient;
  //
  CriaOForm;
end;

procedure TFmEntiGrupos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEntiGruposCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntiGrupos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEntiGrupos.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEntiGruposCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntiGrupos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntiGrupos.QrEntiGruposAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEntiGrupos.QrEntiGruposAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiGruIts(0);
end;

procedure TFmEntiGrupos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiGrupos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEntiGruposCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'entigrupos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEntiGrupos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiGrupos.Inclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEntiGrupos, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'entigrupos');
end;

procedure TFmEntiGrupos.Inclui2Click(Sender: TObject);
begin
  MostraFormEntiGruIts(stIns);
end;

procedure TFmEntiGrupos.QrEntiGruposBeforeClose(DataSet: TDataSet);
begin
  QrEntiGruIts.Close;
end;

procedure TFmEntiGrupos.QrEntiGruposBeforeOpen(DataSet: TDataSet);
begin
  QrEntiGruposCodigo.DisplayFormat := FFormatFloat;
end;

end.

