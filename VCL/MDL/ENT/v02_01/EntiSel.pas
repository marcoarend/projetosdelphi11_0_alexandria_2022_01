unit EntiSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB,
  DBGrids, dmkDBGrid, DBCtrls, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmEntiSel = class(TForm)
    Panel1: TPanel;
    DBGrid1: TdmkDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEntiSel: TFmEntiSel;

implementation

uses UnMyObjects, EntidadesImp, DmkDAC_PF(*&, UnDmkABS_PF*);

{$R *.DFM}

procedure TFmEntiSel.BtNenhumClick(Sender: TObject);
begin
(*&
  FmEntidadesImp.Query.Close;
  FmEntidadesImp.Query.SQL.Clear;
  FmEntidadesImp.Query.SQL.Add('UPDATE entisel SET Ativo=0; ');
  FmEntidadesImp.Query.SQL.Add('SELECT * FROM entisel;');
  DmkABS_PF.AbreQuery(FmEntidadesImp.Query);
*)
end;

procedure TFmEntiSel.BtOKClick(Sender: TObject);
begin
(*&
  FmEntidadesImp.FCadastrar := True;
  FmEntidadesImp.Query.Filter   := 'Ativo=1';
  FmEntidadesImp.Query.Filtered := True;
  //
  if FmEntidadesImp.Query.RecordCount > 0 then
    Close
  else begin
    FmEntidadesImp.Query.Filtered := False;
    Geral.MB_Aviso('Nenhum item foi selecionado!');
  end;
*)
end;

procedure TFmEntiSel.BtSaidaClick(Sender: TObject);
begin
  FmEntidadesImp.FCadastrar := False;
  Close;
end;

procedure TFmEntiSel.BtTudoClick(Sender: TObject);
begin
(*&
  FmEntidadesImp.Query.Close;
  FmEntidadesImp.Query.SQL.Clear;
  FmEntidadesImp.Query.SQL.Add('UPDATE entisel SET Ativo=1; ');
  FmEntidadesImp.Query.SQL.Add('SELECT * FROM entisel;');
  DmkABS_PF.AbreQuery(FmEntidadesImp.Query);
*)
end;

procedure TFmEntiSel.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
begin
(*&
  if Column.FieldName = 'Ativo' then
  begin
    if FmEntidadesImp.Query.FieldByName('Ativo').AsInteger = 1 then
      Status := 0
    else
      Status := 1;
    //
    FmEntidadesImp.Query.Edit;
    FmEntidadesImp.Query.FieldByName('Ativo').AsInteger := Status;
    FmEntidadesImp.Query.Post;
  end;
*)
end;

procedure TFmEntiSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  FmEntidadesImp.FCadastrar := False;
end;

procedure TFmEntiSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

