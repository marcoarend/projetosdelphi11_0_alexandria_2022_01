object FmEntiCfgRel: TFmEntiCfgRel
  Left = 339
  Top = 185
  Caption = 'FIN-PRINT-002 :: Gerencia Config. de Impress'#227'o de Balancete'
  ClientHeight = 629
  ClientWidth = 834
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 834
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 786
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 738
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 544
        Height = 32
        Caption = 'Gerencia Config. de Impress'#227'o de Balancete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 544
        Height = 32
        Caption = 'Gerencia Config. de Impress'#227'o de Balancete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 544
        Height = 32
        Caption = 'Gerencia Config. de Impress'#227'o de Balancete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 834
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 834
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 834
        Height = 467
        Align = alClient
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 2
          Top = 15
          Width = 830
          Height = 450
          ActivePage = TabSheet2
          Align = alClient
          TabOrder = 0
          OnChange = PageControl1Change
          ExplicitHeight = 451
          object TabSheet1: TTabSheet
            Caption = 'Filiais'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Splitter1: TSplitter
              Left = 0
              Top = 152
              Width = 822
              Height = 10
              Cursor = crVSplit
              Align = alBottom
              Beveled = True
              ExplicitTop = 158
              ExplicitWidth = 824
            end
            object dmkDBGrid1: TdmkDBGrid
              Left = 0
              Top = 0
              Width = 822
              Height = 152
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'Entidade'
                  Width = 51
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Filial'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEENT'
                  Title.Caption = 'Nome'
                  Width = 272
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CNPJCPF_TXT'
                  Title.Caption = 'CNPJ/CPF'
                  Width = 112
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IERG'
                  Title.Caption = 'I.E./RG'
                  Width = 112
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsFiliais
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'Entidade'
                  Width = 51
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Filial'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEENT'
                  Title.Caption = 'Nome'
                  Width = 272
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CNPJCPF_TXT'
                  Title.Caption = 'CNPJ/CPF'
                  Width = 112
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IERG'
                  Title.Caption = 'I.E./RG'
                  Width = 112
                  Visible = True
                end>
            end
            object PageControl2: TPageControl
              Left = 0
              Top = 162
              Width = 822
              Height = 260
              ActivePage = TabSheet4
              Align = alBottom
              TabOrder = 1
              OnChange = PageControl2Change
              ExplicitTop = 168
              ExplicitWidth = 824
              object TabSheet4: TTabSheet
                Caption = 'Relat'#243'rios '
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object DBGrid2: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 221
                  Height = 235
                  Align = alLeft
                  DataSource = DsEntiCfgRel
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NO_RelTipo'
                      Title.Caption = 'Tipo de relat'#243'rio'
                      Width = 182
                      Visible = True
                    end>
                end
                object DBGrid3: TDBGrid
                  Left = 221
                  Top = 0
                  Width = 596
                  Height = 235
                  Align = alClient
                  DataSource = DsEntiCfgRel_01
                  TabOrder = 1
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Ordem'
                      Width = 38
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'RelItem'
                      Title.Caption = 'Sub-relat.'
                      Width = 50
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'RelTitu'
                      Title.Caption = 'T'#237'tulo do sub-relat'#243'rio'
                      Width = 200
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_RelItem'
                      Title.Caption = 'Descri'#231#227'o item'
                      Width = 200
                      Visible = True
                    end>
                end
              end
              object TabSheet3: TTabSheet
                Caption = 'Parecer (balancete) '
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object RETxtParecer: TRichEdit
                  Left = 0
                  Top = 0
                  Width = 817
                  Height = 235
                  Align = alClient
                  ReadOnly = True
                  TabOrder = 0
                end
              end
              object TabSheet5: TTabSheet
                Caption = 'Logo (capa do balancete)'
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object ImgLogo: TImage
                  Left = 0
                  Top = 0
                  Width = 817
                  Height = 235
                  Align = alClient
                  Center = True
                  Proportional = True
                end
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Relat'#243'rios padr'#245'es para todas filiais '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 822
              Height = 422
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitWidth = 824
              ExplicitHeight = 428
              object Panel10: TPanel
                Left = 0
                Top = 0
                Width = 224
                Height = 422
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitHeight = 428
                object Panel11: TPanel
                  Left = 0
                  Top = 0
                  Width = 224
                  Height = 44
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object BtRelMtrz: TBitBtn
                    Tag = 5
                    Left = 8
                    Top = 1
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Relat'#243'rios'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtRelMtrzClick
                  end
                end
                object DBGrid4: TDBGrid
                  Left = 0
                  Top = 44
                  Width = 224
                  Height = 382
                  Align = alClient
                  DataSource = DsMtrzCfgRel
                  TabOrder = 1
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NO_RelTipo'
                      Title.Caption = 'Tipo de relat'#243'rio'
                      Width = 182
                      Visible = True
                    end>
                end
              end
              object PageControl3: TPageControl
                Left = 224
                Top = 0
                Width = 598
                Height = 422
                ActivePage = TabSheet8
                Align = alClient
                TabOrder = 1
                OnChange = PageControl3Change
                ExplicitWidth = 600
                ExplicitHeight = 428
                object TabSheet8: TTabSheet
                  Caption = 'Relat'#243'rios'
                  ImageIndex = 2
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object DBGrid5: TDBGrid
                    Left = 0
                    Top = 0
                    Width = 593
                    Height = 401
                    Align = alClient
                    DataSource = DsMtrzCfgRel_01
                    TabOrder = 0
                    TitleFont.Charset = ANSI_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Ordem'
                        Width = 38
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'RelItem'
                        Title.Caption = 'ID relat'#243'rio'
                        Width = 80
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'RelTitu'
                        Title.Caption = 'T'#237'tulo do sub-relat'#243'rio'
                        Width = 418
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_RelItem'
                        Title.Caption = 'Descri'#231#227'o item'
                        Width = 404
                        Visible = True
                      end>
                  end
                end
                object TabSheet6: TTabSheet
                  Caption = 'Parecer (balancete) '
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object RETxtParecerMtrz: TRichEdit
                    Left = 0
                    Top = 0
                    Width = 593
                    Height = 401
                    Align = alClient
                    ReadOnly = True
                    TabOrder = 0
                  end
                end
                object TabSheet7: TTabSheet
                  Caption = 'Logo (capa do balancete)'
                  ImageIndex = 1
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object ImgMtrz: TImage
                    Left = 0
                    Top = 0
                    Width = 593
                    Height = 401
                    Align = alClient
                    Center = True
                    Proportional = True
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 834
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 830
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 834
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 688
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 686
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtRelatorios: TBitBtn
        Tag = 5
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Relat'#243'rios'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtRelatoriosClick
      end
    end
  end
  object PMRelatorios: TPopupMenu
    OnPopup = PMRelatoriosPopup
    Left = 64
    Top = 464
    object Balancete1: TMenuItem
      Caption = '&Balancete'
      object Incluiitem1: TMenuItem
        Caption = '&Inclui item'
        OnClick = Incluiitem1Click
      end
      object Alteraitem1: TMenuItem
        Caption = '&Altera item'
        OnClick = Alteraitem1Click
      end
      object Retiraitem1: TMenuItem
        Caption = '&Retira item'
        OnClick = Retiraitem1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Itematual1: TMenuItem
        Caption = 'Ite&m atual'
        object Texto1: TMenuItem
          Caption = '&Texto'
          OnClick = Texto1Click
        end
        object Logo1: TMenuItem
          Caption = '&Logo'
          OnClick = Logo1Click
        end
        object Removelogo2: TMenuItem
          Caption = '&Remove logo'
          OnClick = Removelogo2Click
        end
      end
    end
  end
  object PMRelMtrz: TPopupMenu
    OnPopup = PMRelMtrzPopup
    Left = 112
    Top = 104
    object Balancete2: TMenuItem
      Caption = '&Balancete'
      object Incluiitem2: TMenuItem
        Caption = '&Inclui item'
        OnClick = Incluiitem2Click
      end
      object Alteraitem2: TMenuItem
        Caption = '&Altera item'
        OnClick = Alteraitem2Click
      end
      object Retiraitem2: TMenuItem
        Caption = '&Retira item'
        OnClick = Retiraitem2Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object ItemAtual2: TMenuItem
        Caption = 'Item Atual'
        object exto2: TMenuItem
          Caption = '&Texto'
          OnClick = exto2Click
        end
        object Logo2: TMenuItem
          Caption = '&Logo'
          OnClick = Logo2Click
        end
        object Removelogo1: TMenuItem
          Caption = '&Remove logo'
          OnClick = Removelogo1Click
        end
      end
    end
  end
  object QrFiliais: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFiliaisBeforeClose
    AfterScroll = QrFiliaisAfterScroll
    OnCalcFields = QrFiliaisCalcFields
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF, '
      'IF(ent.Tipo=0, ent.IE, ent.RG) IERG'
      'FROM entidades ent'
      'WHERE ent.Codigo < -10')
    Left = 260
    Top = 140
    object QrFiliaisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFiliaisFilial: TIntegerField
      FieldName = 'Filial'
      Required = True
    end
    object QrFiliaisNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrFiliaisCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrFiliaisIERG: TWideStringField
      FieldName = 'IERG'
    end
    object QrFiliaisCNPJCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_TXT'
      Size = 40
      Calculated = True
    end
  end
  object DsFiliais: TDataSource
    DataSet = QrFiliais
    Left = 288
    Top = 140
  end
  object QrEntiCfgRel: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntiCfgRelBeforeClose
    AfterScroll = QrEntiCfgRelAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT(RelTipo),'
      'ELT(RelTipo, "Balancete", "?") NO_RelTipo '
      'FROM enticfgrel'
      'WHERE Codigo=:P0')
    Left = 88
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCfgRelRelTipo: TIntegerField
      FieldName = 'RelTipo'
    end
    object QrEntiCfgRelNO_RelTipo: TWideStringField
      FieldName = 'NO_RelTipo'
      Size = 9
    end
  end
  object DsEntiCfgRel: TDataSource
    DataSet = QrEntiCfgRel
    Left = 116
    Top = 336
  end
  object QrEntiCfgRel_01: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntiCfgRel_01BeforeClose
    AfterScroll = QrEntiCfgRel_01AfterScroll
    SQL.Strings = (
      'SELECT cta.Nome NO_Genero, ecr.*, ELT(ecr.RelItem+1,'
      '"[NENHUM]","Capa","Parecer dos Revisores",'
      
        '"Extrato anual de entradas por conta por empresa -  pelo m'#234's (da' +
        'ta) da entrada",'
      
        '"Extrato anual de entradas por conta por empresa -  pelo m'#234's de ' +
        'compet'#234'ncia",'
      '"Extrato das contas correntes no per'#237'odo",'
      '"Saldos das contas do plano de contas com saldo controlado",'
      '"Saldos das contas do plano de contas",'
      '"Demonstrativo de receitas e despesas no per'#237'odo",'
      '"Mensalidades pr'#233'-estipuladas",'
      '"Resultados mensais",'
      '"Composi'#231#227'o da provis'#227'o efetuada para a arrecada'#231#227'o",'
      '"Cheques compensados no per'#237'odo",'
      '"???") NO_RelItem '
      'FROM enticfgrel ecr'
      'LEFT JOIN contas cta ON cta.Codigo=ecr.Genero'
      'WHERE ecr.RelTipo=1 '
      'AND ecr.Codigo=:P0'
      'ORDER BY ecr.Ordem')
    Left = 88
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCfgRel_01Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiCfgRel_01Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEntiCfgRel_01RelTipo: TIntegerField
      FieldName = 'RelTipo'
    end
    object QrEntiCfgRel_01RelTitu: TWideStringField
      FieldName = 'RelTitu'
      Size = 100
    end
    object QrEntiCfgRel_01Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEntiCfgRel_01RelPrcr: TWideMemoField
      FieldName = 'RelPrcr'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrEntiCfgRel_01Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEntiCfgRel_01NO_Genero: TWideStringField
      FieldName = 'NO_Genero'
      Size = 50
    end
    object QrEntiCfgRel_01RelItem: TIntegerField
      FieldName = 'RelItem'
      Required = True
    end
    object QrEntiCfgRel_01NO_RelItem: TWideStringField
      FieldName = 'NO_RelItem'
      Size = 41
    end
    object QrEntiCfgRel_01LogoPath: TWideStringField
      FieldName = 'LogoPath'
      Size = 255
    end
    object QrEntiCfgRel_01FonteTam: TSmallintField
      FieldName = 'FonteTam'
    end
    object QrEntiCfgRel_01CpaLin1FonNom: TWideStringField
      FieldName = 'CpaLin1FonNom'
      Size = 255
    end
    object QrEntiCfgRel_01CpaLin1FonTam: TSmallintField
      FieldName = 'CpaLin1FonTam'
    end
    object QrEntiCfgRel_01CpaLin1MrgSup: TIntegerField
      FieldName = 'CpaLin1MrgSup'
    end
    object QrEntiCfgRel_01CpaLin1AltLin: TIntegerField
      FieldName = 'CpaLin1AltLin'
    end
    object QrEntiCfgRel_01CpaLin1AliTex: TSmallintField
      FieldName = 'CpaLin1AliTex'
    end
    object QrEntiCfgRel_01CpaLin2FonNom: TWideStringField
      FieldName = 'CpaLin2FonNom'
      Size = 255
    end
    object QrEntiCfgRel_01CpaLin2FonTam: TSmallintField
      FieldName = 'CpaLin2FonTam'
    end
    object QrEntiCfgRel_01CpaLin2MrgSup: TIntegerField
      FieldName = 'CpaLin2MrgSup'
    end
    object QrEntiCfgRel_01CpaLin2AltLin: TIntegerField
      FieldName = 'CpaLin2AltLin'
    end
    object QrEntiCfgRel_01CpaLin2AliTex: TSmallintField
      FieldName = 'CpaLin2AliTex'
    end
    object QrEntiCfgRel_01CpaLin3FonNom: TWideStringField
      FieldName = 'CpaLin3FonNom'
      Size = 255
    end
    object QrEntiCfgRel_01CpaLin3FonTam: TSmallintField
      FieldName = 'CpaLin3FonTam'
    end
    object QrEntiCfgRel_01CpaLin3MrgSup: TIntegerField
      FieldName = 'CpaLin3MrgSup'
    end
    object QrEntiCfgRel_01CpaLin3AltLin: TIntegerField
      FieldName = 'CpaLin3AltLin'
    end
    object QrEntiCfgRel_01CpaLin3AliTex: TSmallintField
      FieldName = 'CpaLin3AliTex'
    end
    object QrEntiCfgRel_01CpaImgMrgSup: TIntegerField
      FieldName = 'CpaImgMrgSup'
    end
    object QrEntiCfgRel_01CpaImgMrgEsq: TIntegerField
      FieldName = 'CpaImgMrgEsq'
    end
    object QrEntiCfgRel_01CpaImgAlt: TIntegerField
      FieldName = 'CpaImgAlt'
    end
    object QrEntiCfgRel_01CpaImgLar: TIntegerField
      FieldName = 'CpaImgLar'
    end
    object QrEntiCfgRel_01CpaImgStre: TSmallintField
      FieldName = 'CpaImgStre'
    end
    object QrEntiCfgRel_01CpaImgProp: TSmallintField
      FieldName = 'CpaImgProp'
    end
    object QrEntiCfgRel_01CpaImgTran: TSmallintField
      FieldName = 'CpaImgTran'
    end
    object QrEntiCfgRel_01CpaImgTranCol: TIntegerField
      FieldName = 'CpaImgTranCol'
    end
  end
  object DsEntiCfgRel_01: TDataSource
    DataSet = QrEntiCfgRel_01
    Left = 116
    Top = 364
  end
  object QrOrdena: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ecr.Controle'
      'FROM enticfgrel ecr'
      'WHERE ecr.RelTipo=:P0'
      'AND ecr.Codigo=:P1'
      'ORDER BY ecr.Ordem')
    Left = 144
    Top = 337
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOrdenaControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrMtrzCfgRel: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrMtrzCfgRelBeforeClose
    AfterScroll = QrMtrzCfgRelAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT(RelTipo),'
      'ELT(RelTipo, "Balancete", "?") NO_RelTipo '
      'FROM enticfgrel'
      'WHERE Codigo=0')
    Left = 432
    Top = 268
    object QrMtrzCfgRelRelTipo: TIntegerField
      FieldName = 'RelTipo'
    end
    object QrMtrzCfgRelNO_RelTipo: TWideStringField
      FieldName = 'NO_RelTipo'
      Size = 9
    end
  end
  object DsMtrzCfgRel: TDataSource
    DataSet = QrMtrzCfgRel
    Left = 460
    Top = 268
  end
  object QrMtrzCfgRel_01: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrMtrzCfgRel_01BeforeClose
    AfterScroll = QrMtrzCfgRel_01AfterScroll
    SQL.Strings = (
      'SELECT cta.Nome NO_Genero, ecr.*, ELT(ecr.RelItem+1,'
      '"[NENHUM]","Capa","Parecer dos Revisores",'
      
        '"Extrato anual de entradas por conta por empresa -  pelo m'#234's (da' +
        'ta) da entrada",'
      
        '"Extrato anual de entradas por conta por empresa -  pelo m'#234's de ' +
        'compet'#234'ncia",'
      '"Extrato das contas correntes no per'#237'odo",'
      '"Saldos das contas do plano de contas com saldo controlado",'
      '"Saldos das contas do plano de contas",'
      '"Demonstrativo de receitas e despesas no per'#237'odo",'
      '"Mensalidades pr'#233'-estipuladas",'
      '"Resultados mensais",'
      '"Composi'#231#227'o da provis'#227'o efetuada para a arrecada'#231#227'o",'
      '"Cheques compensados no per'#237'odo",'
      '"???") NO_RelItem '
      'FROM enticfgrel ecr'
      'LEFT JOIN contas cta ON cta.Codigo=ecr.Genero'
      'WHERE ecr.RelTipo=1 '
      'AND ecr.Codigo=:P0'
      'ORDER BY ecr.Ordem')
    Left = 432
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMtrzCfgRel_01NO_Genero: TWideStringField
      FieldName = 'NO_Genero'
      Size = 50
    end
    object QrMtrzCfgRel_01Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMtrzCfgRel_01Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMtrzCfgRel_01RelTipo: TIntegerField
      FieldName = 'RelTipo'
    end
    object QrMtrzCfgRel_01RelTitu: TWideStringField
      FieldName = 'RelTitu'
      Size = 100
    end
    object QrMtrzCfgRel_01RelPrcr: TWideMemoField
      FieldName = 'RelPrcr'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMtrzCfgRel_01Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrMtrzCfgRel_01Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrMtrzCfgRel_01Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMtrzCfgRel_01DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMtrzCfgRel_01DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMtrzCfgRel_01UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMtrzCfgRel_01UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMtrzCfgRel_01AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMtrzCfgRel_01Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMtrzCfgRel_01LogoPath: TWideStringField
      FieldName = 'LogoPath'
      Size = 255
    end
    object QrMtrzCfgRel_01FonteTam: TSmallintField
      FieldName = 'FonteTam'
    end
    object QrMtrzCfgRel_01PBB: TSmallintField
      FieldName = 'PBB'
    end
    object QrMtrzCfgRel_01NO_RelItem: TWideStringField
      FieldName = 'NO_RelItem'
      Size = 77
    end
    object QrMtrzCfgRel_01CpaLin1FonNom: TWideStringField
      FieldName = 'CpaLin1FonNom'
      Size = 255
    end
    object QrMtrzCfgRel_01CpaLin1FonTam: TSmallintField
      FieldName = 'CpaLin1FonTam'
    end
    object QrMtrzCfgRel_01CpaLin1MrgSup: TIntegerField
      FieldName = 'CpaLin1MrgSup'
    end
    object QrMtrzCfgRel_01CpaLin1AltLin: TIntegerField
      FieldName = 'CpaLin1AltLin'
    end
    object QrMtrzCfgRel_01CpaLin1AliTex: TSmallintField
      FieldName = 'CpaLin1AliTex'
    end
    object QrMtrzCfgRel_01CpaLin2FonNom: TWideStringField
      FieldName = 'CpaLin2FonNom'
      Size = 255
    end
    object QrMtrzCfgRel_01CpaLin2FonTam: TSmallintField
      FieldName = 'CpaLin2FonTam'
    end
    object QrMtrzCfgRel_01CpaLin2MrgSup: TIntegerField
      FieldName = 'CpaLin2MrgSup'
    end
    object QrMtrzCfgRel_01CpaLin2AltLin: TIntegerField
      FieldName = 'CpaLin2AltLin'
    end
    object QrMtrzCfgRel_01CpaLin2AliTex: TSmallintField
      FieldName = 'CpaLin2AliTex'
    end
    object QrMtrzCfgRel_01CpaLin3FonNom: TWideStringField
      FieldName = 'CpaLin3FonNom'
      Size = 255
    end
    object QrMtrzCfgRel_01CpaLin3FonTam: TSmallintField
      FieldName = 'CpaLin3FonTam'
    end
    object QrMtrzCfgRel_01CpaLin3MrgSup: TIntegerField
      FieldName = 'CpaLin3MrgSup'
    end
    object QrMtrzCfgRel_01CpaLin3AltLin: TIntegerField
      FieldName = 'CpaLin3AltLin'
    end
    object QrMtrzCfgRel_01CpaLin3AliTex: TSmallintField
      FieldName = 'CpaLin3AliTex'
    end
    object QrMtrzCfgRel_01CpaImgMrgSup: TIntegerField
      FieldName = 'CpaImgMrgSup'
    end
    object QrMtrzCfgRel_01CpaImgMrgEsq: TIntegerField
      FieldName = 'CpaImgMrgEsq'
    end
    object QrMtrzCfgRel_01CpaImgAlt: TIntegerField
      FieldName = 'CpaImgAlt'
    end
    object QrMtrzCfgRel_01CpaImgLar: TIntegerField
      FieldName = 'CpaImgLar'
    end
    object QrMtrzCfgRel_01CpaImgStre: TSmallintField
      FieldName = 'CpaImgStre'
    end
    object QrMtrzCfgRel_01CpaImgProp: TSmallintField
      FieldName = 'CpaImgProp'
    end
    object QrMtrzCfgRel_01CpaImgTran: TSmallintField
      FieldName = 'CpaImgTran'
    end
    object QrMtrzCfgRel_01CpaImgTranCol: TIntegerField
      FieldName = 'CpaImgTranCol'
    end
    object QrMtrzCfgRel_01RelItem: TIntegerField
      FieldName = 'RelItem'
    end
  end
  object DsMtrzCfgRel_01: TDataSource
    DataSet = QrMtrzCfgRel_01
    Left = 460
    Top = 296
  end
end
