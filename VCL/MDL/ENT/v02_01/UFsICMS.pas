unit UFsICMS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral, Variants, dmkValUsu,
  dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmUFsICMS = class(TForm)
    Panel1: TPanel;
    QrUFs: TmySQLQuery;
    DsUFs: TDataSource;
    EdUFDest: TdmkEditCB;
    CBUFDest: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdICMS_V: TdmkEdit;
    Label2: TLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    CkContinuar: TCheckBox;
    DBEdCodigo: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit1: TdmkDBEdit;
    DBEdNome: TDBEdit;
    Label5: TLabel;
    QrUFsNome: TWideStringField;
    QrUFsDescricao: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmUFsICMS: TFmUFsICMS;

implementation

uses UnMyObjects, Module, UFs, UMySQLModule;

{$R *.DFM}

procedure TFmUFsICMS.BtOKClick(Sender: TObject);
var
  Nome, UFDest: String;
  //ICMS_V: Double;
begin
  if CBUFDest.KeyValue = Null then
    UFDest := ''
  else
    UFDest := Trim(CBUFDest.KeyValue);
  if UFDest = '' then
  begin
    Geral.MB_Aviso('Defina a UF destino!');
    EdUFDest.SetFocus;
    Exit;
  end;
  Nome := FmUFs.QrUFsNome.Value;
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'ufsicms', '', Dmod.QrUpd) then
  {
  begin
    ICMS_V := EdICMS_V.ValueVariant;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ufsicms', False, [
    'ICMS_V'], ['Nome', 'UFDest'], [ICMS_V], [Nome, UFDest], True) then
  end;
  }
  begin
    FmUFs.ReopenUFsICMS(UFDest);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType        := stIns;
      EdUFDest.ValueVariant := '';
      CBUFDest.KeyValue     := Null;
      EdICMS_V.ValueVariant := 0;
      EdUFDest.SetFocus;
    end else Close;
  end;
end;

procedure TFmUFsICMS.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmUFsICMS.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmUFsICMS.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrUFs, DMod.MyDB);
end;

procedure TFmUFsICMS.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
