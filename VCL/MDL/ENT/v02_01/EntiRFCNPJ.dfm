object FmEntiRFCNPJ: TFmEntiRFCNPJ
  Left = 339
  Top = 185
  Caption = 'ENT-GEREN-020 :: Consulta CNPJ Receita Federal'
  ClientHeight = 662
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 1008
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 960
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1168
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 912
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 386
        Height = 32
        Caption = 'Consulta CNPJ Receita Federal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 386
        Height = 32
        Caption = 'Consulta CNPJ Receita Federal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 386
        Height = 32
        Caption = 'Consulta CNPJ Receita Federal'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1264
    Height = 500
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 1008
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1264
      Height = 500
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1008
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1264
        Height = 500
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 1008
        object ProgressBar1: TProgressBar
          Left = 2
          Top = 481
          Width = 1260
          Height = 17
          Align = alBottom
          TabOrder = 0
          ExplicitWidth = 1004
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 15
          Width = 1260
          Height = 466
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 1
          ExplicitWidth = 1004
          object TabSheet1: TTabSheet
            Caption = 'HTML'
            ExplicitWidth = 996
            object WB_IE: TWebBrowser
              Left = 0
              Top = 46
              Width = 1252
              Height = 392
              Align = alClient
              TabOrder = 0
              OnProgressChange = WB_IEProgressChange
              OnDocumentComplete = WB_IEDocumentComplete
              ExplicitTop = 0
              ExplicitWidth = 983
              ExplicitHeight = 434
              ControlData = {
                4C00000066810000842800000000000000000000000000000000000000000000
                000000004C000000000000000000000001000000E0D057007335CF11AE690800
                2B2E12620A000000000000004C0000000114020000000000C000000000000046
                8000000000000000000000000000000000000000000000000000000000000000
                00000000000000000100000000000000000000000000000000000000}
            end
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 1252
              Height = 46
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              TabOrder = 1
              ExplicitTop = 4
              ExplicitWidth = 996
              object SbNavega: TSpeedButton
                Left = 1108
                Top = 10
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '>'
                OnClick = SbNavegaClick
              end
              object EdURL_WB: TdmkEdit
                Left = 182
                Top = 10
                Width = 922
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object RGNavegador: TRadioGroup
                Left = 1
                Top = 1
                Width = 173
                Height = 44
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Navegador: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'IExplore'
                  'Chrome')
                TabOrder = 1
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Texto plano'
            ImageIndex = 1
            ExplicitWidth = 996
            object MeDados: TMemo
              Left = 0
              Top = 0
              Width = 1252
              Height = 217
              Align = alTop
              Lines.Strings = (
                'MeTexto')
              ScrollBars = ssVertical
              TabOrder = 0
              ExplicitWidth = 996
            end
            object MeCodigo: TMemo
              Left = 0
              Top = 217
              Width = 1252
              Height = 53
              Align = alClient
              Lines.Strings = (
                'MeEndereco')
              ScrollBars = ssVertical
              TabOrder = 1
              ExplicitWidth = 996
            end
            object Panel_Cadastro: TPanel
              Left = 0
              Top = 270
              Width = 1252
              Height = 168
              Align = alBottom
              TabOrder = 2
              ExplicitWidth = 996
              object Label3: TLabel
                Left = -2
                Top = 35
                Width = 63
                Height = 13
                Alignment = taRightJustify
                Caption = 'Raz'#227'o Social'
              end
              object Label4: TLabel
                Left = 34
                Top = 13
                Width = 27
                Height = 13
                Alignment = taRightJustify
                Caption = 'CNPJ'
              end
              object Label_end: TLabel
                Left = 15
                Top = 57
                Width = 46
                Height = 13
                Alignment = taRightJustify
                Caption = 'Endere'#231'o'
              end
              object Label5: TLabel
                Left = 42
                Top = 79
                Width = 19
                Height = 13
                Alignment = taRightJustify
                Caption = 'Cep'
              end
              object Label6: TLabel
                Left = 329
                Top = 57
                Width = 12
                Height = 13
                Alignment = taRightJustify
                Caption = 'N'#186
              end
              object Label7: TLabel
                Left = 403
                Top = 57
                Width = 32
                Height = 13
                Alignment = taRightJustify
                Caption = 'Compl.'
              end
              object Label8: TLabel
                Left = 34
                Top = 101
                Width = 27
                Height = 13
                Alignment = taRightJustify
                Caption = 'Bairro'
              end
              object Label9: TLabel
                Left = 16
                Top = 123
                Width = 45
                Height = 13
                Alignment = taRightJustify
                Caption = 'Municipio'
              end
              object Label10: TLabel
                Left = 327
                Top = 123
                Width = 14
                Height = 13
                Alignment = taRightJustify
                Caption = 'UF'
              end
              object Label1: TLabel
                Left = 29
                Top = 148
                Width = 32
                Height = 13
                Alignment = taRightJustify
                Caption = 'CNAE:'
              end
              object EdRazaoSocial: TEdit
                Left = 64
                Top = 32
                Width = 257
                Height = 21
                Hint = 'NOME EMPRESARIAL'
                ReadOnly = True
                TabOrder = 0
              end
              object EdCNPJ: TEdit
                Left = 64
                Top = 10
                Width = 257
                Height = 21
                Hint = 'N'#218'MERO DE INSCRI'#199#195'O'
                ReadOnly = True
                TabOrder = 1
              end
              object EdRua: TEdit
                Left = 64
                Top = 54
                Width = 257
                Height = 21
                Hint = 'LOGRADOURO'
                ReadOnly = True
                TabOrder = 2
              end
              object EdNumero: TEdit
                Left = 343
                Top = 54
                Width = 56
                Height = 21
                Hint = 'N'#218'MERO'
                ReadOnly = True
                TabOrder = 3
              end
              object EdCompl: TEdit
                Left = 437
                Top = 54
                Width = 56
                Height = 21
                Hint = 'COMPLEMENTO'
                ReadOnly = True
                TabOrder = 4
              end
              object EdCEP: TEdit
                Left = 64
                Top = 76
                Width = 105
                Height = 21
                Hint = 'CEP'
                ReadOnly = True
                TabOrder = 5
              end
              object EdBairro: TEdit
                Left = 64
                Top = 98
                Width = 257
                Height = 21
                Hint = 'BAIRRO/DISTRITO'
                ReadOnly = True
                TabOrder = 6
              end
              object EdCidade: TEdit
                Left = 64
                Top = 120
                Width = 257
                Height = 21
                Hint = 'MUNIC'#205'PIO'
                ReadOnly = True
                TabOrder = 7
              end
              object EdUF: TEdit
                Left = 343
                Top = 120
                Width = 34
                Height = 21
                Hint = 'UF'
                ReadOnly = True
                TabOrder = 8
              end
              object EdCNAE: TEdit
                Left = 64
                Top = 145
                Width = 257
                Height = 21
                Hint = 'C'#211'DIGO E DESCRI'#199#195'O DA ATIVIDADE ECON'#212'MICA PRINCIPAL'
                ReadOnly = True
                TabOrder = 9
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 548
    Width = 1264
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 1008
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1260
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1004
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 592
    Width = 1264
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 1008
    object PnSaiDesis: TPanel
      Left = 1118
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 862
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1116
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 860
      object BtImporta: TBitBtn
        Tag = 39
        Left = 7
        Top = 3
        Width = 120
        Height = 40
        Caption = 'Importa Dados'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImportaClick
      end
      object RGOpcoes: TRadioGroup
        Left = 133
        Top = 0
        Width = 332
        Height = 45
        Caption = 'Op'#231#245'es'
        ItemIndex = 0
        Items.Strings = (
          'Importar apenas o CNPJ, Raz'#227'o Social, CNAE principal e CEP'
          'Importar tudo')
        TabOrder = 1
      end
    end
  end
end
