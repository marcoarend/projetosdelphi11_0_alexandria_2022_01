object FmEntiCfgRel_01: TFmEntiCfgRel_01
  Left = 339
  Top = 185
  Caption = 'FIN-PRINT-001 :: Configura'#231#227'o de Impress'#227'o de Balancete'
  ClientHeight = 711
  ClientWidth = 990
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 321
    Width = 990
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label2: TLabel
      Left = 15
      Top = 5
      Width = 471
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 
        'Titulo do Item: [F4] > Copia texto do item. [F5] Copia texto do ' +
        'item em mai'#250'sculo.'
    end
    object EdRelTitu: TdmkEdit
      Left = 15
      Top = 25
      Width = 615
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'RelTitu'
      UpdCampo = 'RelTitu'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnKeyDown = EdRelTituKeyDown
    end
    object RGFonteTam: TdmkRadioGroup
      Left = 635
      Top = 1
      Width = 321
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Fonte referencial do texto:'
      Columns = 3
      Enabled = False
      ItemIndex = 1
      Items.Strings = (
        'Pequena'
        'M'#233'dia'
        'Grande')
      TabOrder = 1
      OnClick = RGFonteTamClick
      QryCampo = 'FonteTam'
      UpdCampo = 'FonteTam'
      UpdType = utYes
      OldValor = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 59
    Width = 990
    Height = 262
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label5: TLabel
      Left = 15
      Top = 54
      Width = 47
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID Tipo:'
    end
    object Label1: TLabel
      Left = 615
      Top = 241
      Width = 342
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = #185': Extrato anual de entradas por conta '#250'nica por empresa. '
    end
    object Label3: TLabel
      Left = 15
      Top = 103
      Width = 44
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID Item:'
    end
    object Label4: TLabel
      Left = 15
      Top = 153
      Width = 98
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Ordem Item [F4]:'
    end
    object Label6: TLabel
      Left = 15
      Top = 5
      Width = 72
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID entidade:'
    end
    object EdRelTipo: TdmkEdit
      Left = 15
      Top = 74
      Width = 95
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdCampo = 'RelTipo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
    end
    object EdControle: TdmkEdit
      Left = 15
      Top = 123
      Width = 95
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utIdx
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdOrdem: TdmkEdit
      Left = 15
      Top = 172
      Width = 95
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Ordem'
      UpdCampo = 'Ordem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdOrdemKeyDown
    end
    object EdCodigo: TdmkEdit
      Left = 15
      Top = 25
      Width = 95
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGRelItem: TdmkRadioGroup
      Left = 118
      Top = 0
      Width = 838
      Height = 238
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Item de Relat'#243'rio: '
      Columns = 2
      Items.Strings = (
        'DModG.ListaDeItensDeBalancete()')
      TabOrder = 4
      OnClick = RGRelItemClick
      QryCampo = 'RelItem'
      UpdCampo = 'RelItem'
      UpdType = utYes
      OldValor = 0
    end
  end
  object Pn00: TPanel
    Left = 0
    Top = 449
    Width = 990
    Height = 10
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
  end
  object Pn03: TPanel
    Left = 0
    Top = 380
    Width = 990
    Height = 69
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    Visible = False
    object Label7: TLabel
      Left = 15
      Top = 5
      Width = 164
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Conta (do plano de contas):'
    end
    object EdGenero: TdmkEditCB
      Left = 15
      Top = 25
      Width = 54
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Genero'
      UpdCampo = 'Genero'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGenero
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGenero: TdmkDBLookupComboBox
      Left = 69
      Top = 25
      Width = 873
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 1
      dmkEditCB = EdGenero
      QryCampo = 'Genero'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object Pn0: TPanel
    Left = 0
    Top = 459
    Width = 990
    Height = 112
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    object PGCapa: TPageControl
      Left = 0
      Top = 0
      Width = 990
      Height = 112
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet4
      Align = alClient
      TabOrder = 0
      Visible = False
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Linha 1'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object LaCred: TLabel
          Left = 395
          Top = 5
          Width = 134
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Margem superior: (cm)'
        end
        object Label8: TLabel
          Left = 549
          Top = 5
          Width = 116
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Altura da linha: (cm)'
        end
        object Label9: TLabel
          Left = 9
          Top = 7
          Width = 91
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Noma da fonte:'
        end
        object Label10: TLabel
          Left = 265
          Top = 7
          Width = 112
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tamanho da fonte:'
        end
        object EdCpaLin1MrgSup: TdmkEdit
          Left = 395
          Top = 28
          Width = 148
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCpaLin1AltLin: TdmkEdit
          Left = 549
          Top = 28
          Width = 148
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGCpaLin1AliTex: TdmkRadioGroup
          Left = 704
          Top = 5
          Width = 258
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Alinhamento do texto '
          Columns = 3
          ItemIndex = 1
          Items.Strings = (
            'Esquerda'
            'Direita'
            'Centro')
          TabOrder = 4
          OnClick = RGFonteTamClick
          UpdType = utYes
          OldValor = 0
        end
        object PFCpaLin1FonNom: TdmkPopOutFntCBox
          Left = 9
          Top = 28
          Width = 246
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          OnChange = PFCpaLin1FonNomChange
          TabOrder = 0
          MoveUsedToTop = True
          PopUpHeight = 75
          FonteNome = 'MS Sans Serif'
          UpdType = utYes
        end
        object CBCpaLin1FonTam: TComboBox
          Left = 265
          Top = 28
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 1
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Linha 2'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label11: TLabel
          Left = 9
          Top = 7
          Width = 91
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Noma da fonte:'
        end
        object Label12: TLabel
          Left = 265
          Top = 7
          Width = 112
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tamanho da fonte:'
        end
        object Label13: TLabel
          Left = 395
          Top = 5
          Width = 134
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Margem superior: (cm)'
        end
        object Label14: TLabel
          Left = 549
          Top = 5
          Width = 116
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Altura da linha: (cm)'
        end
        object PFCpaLin2FonNom: TdmkPopOutFntCBox
          Left = 9
          Top = 27
          Width = 246
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          OnChange = PFCpaLin2FonNomChange
          TabOrder = 0
          MoveUsedToTop = True
          PopUpHeight = 75
          FonteNome = 'MS Sans Serif'
          UpdType = utYes
        end
        object CBCpaLin2FonTam: TComboBox
          Left = 265
          Top = 28
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 1
        end
        object EdCpaLin2MrgSup: TdmkEdit
          Left = 395
          Top = 28
          Width = 148
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCpaLin2AltLin: TdmkEdit
          Left = 549
          Top = 28
          Width = 148
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGCpaLin2AliTex: TdmkRadioGroup
          Left = 704
          Top = 5
          Width = 258
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Alinhamento do texto '
          Columns = 3
          ItemIndex = 1
          Items.Strings = (
            'Esquerda'
            'Direita'
            'Centro')
          TabOrder = 4
          OnClick = RGFonteTamClick
          UpdType = utYes
          OldValor = 0
        end
      end
      object TabSheet3: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Linha 3'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label15: TLabel
          Left = 9
          Top = 7
          Width = 91
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Noma da fonte:'
        end
        object Label16: TLabel
          Left = 265
          Top = 7
          Width = 112
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tamanho da fonte:'
        end
        object Label17: TLabel
          Left = 395
          Top = 5
          Width = 134
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Margem superior: (cm)'
        end
        object Label18: TLabel
          Left = 549
          Top = 5
          Width = 116
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Altura da linha: (cm)'
        end
        object PFCpaLin3FonNom: TdmkPopOutFntCBox
          Left = 9
          Top = 27
          Width = 246
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          OnChange = PFCpaLin3FonNomChange
          TabOrder = 0
          MoveUsedToTop = True
          PopUpHeight = 75
          FonteNome = 'MS Sans Serif'
          UpdType = utYes
        end
        object CBCpaLin3FonTam: TComboBox
          Left = 265
          Top = 28
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 1
        end
        object EdCpaLin3MrgSup: TdmkEdit
          Left = 395
          Top = 28
          Width = 148
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCpaLin3AltLin: TdmkEdit
          Left = 549
          Top = 28
          Width = 148
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGCpaLin3AliTex: TdmkRadioGroup
          Left = 704
          Top = 5
          Width = 258
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Alinhamento do texto '
          Columns = 3
          ItemIndex = 1
          Items.Strings = (
            'Esquerda'
            'Direita'
            'Centro')
          TabOrder = 4
          OnClick = RGFonteTamClick
          UpdType = utYes
          OldValor = 0
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Logo'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label19: TLabel
          Left = 9
          Top = 5
          Width = 134
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Margem superior: (cm)'
        end
        object Label20: TLabel
          Left = 165
          Top = 5
          Width = 143
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Margem esquerda: (cm)'
        end
        object Label21: TLabel
          Left = 321
          Top = 5
          Width = 137
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Altura da imagem: (cm)'
        end
        object Label22: TLabel
          Left = 478
          Top = 5
          Width = 149
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Largura da imagem: (cm)'
        end
        object Label23: TLabel
          Left = 874
          Top = 9
          Width = 24
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cor:'
          Visible = False
        end
        object SpeedButton6: TSpeedButton
          Left = 926
          Top = 28
          Width = 25
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Visible = False
          OnClick = SpeedButton6Click
        end
        object EdCpaImgMrgSup: TdmkEdit
          Left = 9
          Top = 28
          Width = 147
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCpaImgMrgEsq: TdmkEdit
          Left = 165
          Top = 28
          Width = 148
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCpaImgAlt: TdmkEdit
          Left = 321
          Top = 28
          Width = 148
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCpaImgLar: TdmkEdit
          Left = 478
          Top = 28
          Width = 147
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object CkCpaImgProp: TdmkCheckBox
          Left = 633
          Top = 30
          Width = 135
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Manter propor'#231#227'o'
          TabOrder = 5
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object CkCpaImgStre: TdmkCheckBox
          Left = 633
          Top = 5
          Width = 135
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Esticar imagem'
          TabOrder = 4
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object CkCpaImgTran: TdmkCheckBox
          Left = 769
          Top = 5
          Width = 99
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tranparente'
          TabOrder = 6
          OnClick = CkCpaImgTranClick
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object EdCpaImgTranCol: TdmkEdit
          Left = 874
          Top = 28
          Width = 49
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Enabled = False
          ReadOnly = True
          TabOrder = 7
          Visible = False
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 990
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object GB_R: TGroupBox
      Left = 930
      Top = 0
      Width = 60
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 871
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 588
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#227'o de Impress'#227'o de Balancete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 588
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#227'o de Impress'#227'o de Balancete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 588
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#227'o de Impress'#227'o de Balancete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 571
    Width = 990
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 6
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 986
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 625
    Width = 990
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 7
    object PnSaiDesis: TPanel
      Left = 811
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 18
      Width = 809
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrMaxOrd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens'
      'FROM enticfgrel'
      'WHERE RelTipo=:P0 '
      'AND Codigo=:P1')
    Left = 176
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMaxOrdItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 236
    Top = 72
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 208
    Top = 72
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrMax: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens'
      'FROM enticfgrel'
      'WHERE RelTipo=:P0'
      'AND Codigo=:P1')
    Left = 264
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMaxItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object ColorDialog1: TColorDialog
    Left = 580
    Top = 136
  end
end
