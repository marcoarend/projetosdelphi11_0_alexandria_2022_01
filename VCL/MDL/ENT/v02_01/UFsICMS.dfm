object FmUFsICMS: TFmUFsICMS
  Left = 339
  Top = 185
  Caption = 'UFS-CADAS-002 :: ICMS por UF'
  ClientHeight = 287
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 97
    Width = 624
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 54
      Height = 13
      Caption = 'UF destino:'
    end
    object Label2: TLabel
      Left = 568
      Top = 4
      Width = 29
      Height = 13
      Caption = 'ICMS:'
    end
    object EdUFDest: TdmkEditCB
      Left = 12
      Top = 20
      Width = 32
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'UFDest'
      UpdCampo = 'UFDest'
      UpdType = utIdx
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      DBLookupComboBox = CBUFDest
      IgnoraDBLookupComboBox = False
    end
    object CBUFDest: TdmkDBLookupComboBox
      Left = 44
      Top = 20
      Width = 521
      Height = 21
      KeyField = 'Nome'
      ListField = 'Descricao'
      ListSource = DsUFs
      TabOrder = 1
      dmkEditCB = EdUFDest
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdICMS_V: TdmkEdit
      Left = 568
      Top = 20
      Width = 41
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ICMS_V'
      UpdCampo = 'ICMS_V'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 624
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label3: TLabel
      Left = 12
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label4: TLabel
      Left = 60
      Top = 4
      Width = 17
      Height = 13
      Caption = 'UF:'
      FocusControl = DBEdit1
    end
    object Label5: TLabel
      Left = 96
      Top = 4
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TDBEdit
      Left = 12
      Top = 20
      Width = 44
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmUFs.DsUFs
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
    end
    object DBEdit1: TdmkDBEdit
      Left = 60
      Top = 20
      Width = 32
      Height = 21
      DataField = 'Nome'
      DataSource = FmUFs.DsUFs
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utIdx
      Alignment = taLeftJustify
    end
    object DBEdNome: TDBEdit
      Left = 96
      Top = 20
      Width = 513
      Height = 21
      Hint = 'Nome do banco'
      Color = clWhite
      DataField = 'Descricao'
      DataSource = FmUFs.DsUFs
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 148
    Width = 624
    Height = 31
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object CkContinuar: TCheckBox
      Left = 8
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 528
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 158
        Height = 32
        Caption = 'ICMS por UF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 158
        Height = 32
        Caption = 'ICMS por UF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 158
        Height = 32
        Caption = 'ICMS por UF'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 179
    Width = 624
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 223
    Width = 624
    Height = 64
    Align = alBottom
    TabOrder = 5
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 476
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fechar'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrUFs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Descricao'
      'FROM ufs'
      'ORDER BY Descricao'
      '')
    Left = 8
    Top = 12
    object QrUFsNome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
    object QrUFsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 30
    end
  end
  object DsUFs: TDataSource
    DataSet = QrUFs
    Left = 36
    Top = 12
  end
end
