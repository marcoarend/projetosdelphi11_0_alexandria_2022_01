object FmEntidadesImp: TFmEntidadesImp
  Left = 337
  Top = 171
  Caption = 'ENT-GEREN-017 :: Pesquisa Entidades'
  ClientHeight = 640
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 478
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 309
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object RGOrigem: TRadioGroup
        Left = 12
        Top = 160
        Width = 353
        Height = 41
        Caption = ' Origem dos dados: '
        Columns = 5
        ItemIndex = 0
        Items.Strings = (
          'Entidade'
          'Pessoal'
          'Empresa'
          'Cobran'#231'a'
          'Entrega')
        TabOrder = 1
        OnClick = RGOrigemClick
      end
      object GroupBox1: TGroupBox
        Left = 12
        Top = 210
        Width = 365
        Height = 72
        Caption = ' Tipo de Cadastro: '
        TabOrder = 6
        object CkCliente1: TCheckBox
          Left = 8
          Top = 20
          Width = 90
          Height = 17
          Caption = 'Cliente 1'
          TabOrder = 0
          Visible = False
          OnClick = CkCliente1Click
        end
        object CkCliente2: TCheckBox
          Left = 8
          Top = 40
          Width = 90
          Height = 17
          Caption = 'Cliente 2'
          TabOrder = 1
          Visible = False
          OnClick = CkCliente2Click
        end
        object CkFornece1: TCheckBox
          Left = 104
          Top = 20
          Width = 90
          Height = 17
          Caption = 'Fornecedor 1'
          TabOrder = 2
          OnClick = CkFornece1Click
        end
        object CkFornece2: TCheckBox
          Left = 104
          Top = 40
          Width = 90
          Height = 17
          Caption = 'Fornecedor 2'
          TabOrder = 3
          Visible = False
          OnClick = CkFornece2Click
        end
        object CkFornece3: TCheckBox
          Left = 200
          Top = 20
          Width = 90
          Height = 17
          Caption = 'Fornecedor 3'
          TabOrder = 4
          Visible = False
          OnClick = CkFornece3Click
        end
        object CkFornece4: TCheckBox
          Left = 200
          Top = 40
          Width = 90
          Height = 17
          Caption = 'Fornecedor 4'
          TabOrder = 5
          Visible = False
          OnClick = CkFornece4Click
        end
        object CkTerceiro: TCheckBox
          Left = 296
          Top = 20
          Width = 65
          Height = 17
          Caption = 'Terceiro'
          TabOrder = 6
          Visible = False
          OnClick = CkTerceiroClick
        end
      end
      object GBFiltro: TGroupBox
        Left = 16
        Top = 4
        Width = 773
        Height = 153
        Caption = ' Filtros: '
        TabOrder = 0
        object Label1: TLabel
          Left = 684
          Top = 40
          Width = 15
          Height = 13
          Caption = 'at'#233
        end
        object CBUF: TdmkDBLookupComboBox
          Left = 8
          Top = 36
          Width = 49
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsUF
          TabOrder = 1
          OnClick = CBUFClick
          UpdType = utYes
          LocF7SQLText.Strings = (
            'SELECT Codigo _Codigo,'
            'Nome _Nome'
            'FROM ufs'
            'WHERE Nome LIKE "%$#%"')
          LocF7SQLMasc = '$#'
        end
        object CBCidade: TdmkDBLookupComboBox
          Left = 60
          Top = 36
          Width = 185
          Height = 21
          KeyField = 'CIDADE'
          ListField = 'CIDADE'
          ListSource = DsCidades
          TabOrder = 3
          UpdType = utYes
          LocF7SQLText.Strings = (
            'SELECT Codigo _Codigo,'
            'IF(Tipo=0, ECidade, PCidade) _Nome'
            'FROM entidades'
            'WHERE IF(Tipo=0, ECidade, PCidade) LIKE "%$#%"'
            '')
          LocF7SQLMasc = '$#'
        end
        object EdCEPIni: TdmkEdit
          Left = 620
          Top = 36
          Width = 62
          Height = 21
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdCEPIniExit
        end
        object CkUF: TCheckBox
          Left = 8
          Top = 18
          Width = 45
          Height = 17
          Caption = 'UF'
          TabOrder = 0
          OnClick = CkUFClick
        end
        object CkCidade: TCheckBox
          Left = 56
          Top = 18
          Width = 181
          Height = 17
          Caption = 'Cidade'
          TabOrder = 2
          OnClick = CkCidadeClick
        end
        object CkBairro: TCheckBox
          Left = 248
          Top = 18
          Width = 157
          Height = 17
          Caption = 'Bairro'
          TabOrder = 4
          OnClick = CkBairroClick
        end
        object CkRua: TCheckBox
          Left = 412
          Top = 18
          Width = 201
          Height = 17
          Caption = 'Rua'
          TabOrder = 6
          OnClick = CkRuaClick
        end
        object CkCEP: TCheckBox
          Left = 620
          Top = 18
          Width = 45
          Height = 17
          Caption = 'CEP'
          TabOrder = 8
          OnClick = CkCEPClick
        end
        object CBBairro: TdmkDBLookupComboBox
          Left = 248
          Top = 36
          Width = 161
          Height = 21
          KeyField = 'BAIRRO'
          ListField = 'BAIRRO'
          ListSource = DsBairros
          TabOrder = 5
          UpdType = utYes
          LocF7SQLText.Strings = (
            'SELECT Codigo _Codigo,'
            'IF(Tipo=0, EBairro, PBairro) _Nome'
            'FROM entidades'
            'WHERE IF(Tipo=0, EBairro, PBairro) LIKE "%$#%"')
          LocF7SQLMasc = '$#'
        end
        object CBRua: TdmkDBLookupComboBox
          Left = 412
          Top = 36
          Width = 205
          Height = 21
          KeyField = 'Rua'
          ListField = 'Rua'
          ListSource = DsRuas
          TabOrder = 7
          UpdType = utYes
          LocF7SQLText.Strings = (
            'SELECT Codigo _Codigo,'
            'IF(Tipo=0, ERua, PRua) _Nome'
            'FROM entidades'
            'WHERE IF(Tipo=0, ERua, PRua) LIKE "%$#%"')
          LocF7SQLMasc = '$#'
        end
        object EdCEPFim: TdmkEdit
          Left = 704
          Top = 36
          Width = 62
          Height = 21
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdCEPFimExit
        end
        object CkCPF: TCheckBox
          Left = 160
          Top = 62
          Width = 45
          Height = 17
          Caption = 'CPF'
          TabOrder = 13
          OnClick = CkCEPClick
        end
        object EdCPF: TdmkEdit
          Left = 160
          Top = 80
          Width = 105
          Height = 21
          TabOrder = 14
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdCPFExit
        end
        object EdCNPJ: TdmkEdit
          Left = 268
          Top = 80
          Width = 105
          Height = 21
          TabOrder = 16
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdCNPJExit
        end
        object CkCNPJ: TCheckBox
          Left = 268
          Top = 62
          Width = 45
          Height = 17
          Caption = 'CNPJ'
          TabOrder = 15
          OnClick = CkCEPClick
        end
        object CkProfiss: TCheckBox
          Left = 376
          Top = 62
          Width = 201
          Height = 17
          Caption = 'Profiss'#227'o'
          TabOrder = 17
          OnClick = CkRuaClick
        end
        object CBProfiss: TdmkDBLookupComboBox
          Left = 376
          Top = 80
          Width = 161
          Height = 21
          KeyField = 'Profissao'
          ListField = 'Profissao'
          ListSource = DsProfiss
          TabOrder = 18
          UpdType = utYes
          LocF7SQLText.Strings = (
            'SELECT Codigo _Codigo,'
            'Profissao _Nome'
            'FROM entidades'
            'WHERE Profissao LIKE "%$#%"')
          LocF7SQLMasc = '$#'
        end
        object CkApelido: TCheckBox
          Left = 540
          Top = 106
          Width = 65
          Height = 17
          Caption = 'Apelido'
          TabOrder = 21
          Visible = False
          OnClick = CkCEPClick
        end
        object EdApelido: TdmkEdit
          Left = 540
          Top = 124
          Width = 109
          Height = 21
          TabOrder = 22
          Visible = False
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdCPFExit
        end
        object CkFantasia: TCheckBox
          Left = 652
          Top = 106
          Width = 89
          Height = 17
          Caption = 'Fantasia'
          TabOrder = 23
          Visible = False
          OnClick = CkCEPClick
        end
        object EdFantasia: TdmkEdit
          Left = 652
          Top = 124
          Width = 113
          Height = 21
          TabOrder = 24
          Visible = False
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdCNPJExit
        end
        object CkPais: TCheckBox
          Left = 8
          Top = 62
          Width = 149
          Height = 17
          Caption = 'Pa'#237's'
          TabOrder = 11
        end
        object CBPais: TdmkDBLookupComboBox
          Left = 8
          Top = 80
          Width = 150
          Height = 21
          KeyField = 'PAIS'
          ListField = 'PAIS'
          ListSource = DsPais
          TabOrder = 12
          UpdType = utYes
          LocF7SQLText.Strings = (
            'SELECT Codigo _Codigo,'
            'IF(Tipo=0, EPais, PPais) _Nome'
            'FROM entidades'
            'WHERE IF(Tipo=0, EPais, PPais) LIKE "%$#%"')
          LocF7SQLMasc = '$#'
        end
        object EdTelefones: TdmkEdit
          Left = 540
          Top = 80
          Width = 225
          Height = 21
          TabOrder = 20
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdTelefonesExit
        end
        object CkTelefones: TCheckBox
          Left = 540
          Top = 62
          Width = 149
          Height = 17
          Caption = 'Telefones, fax, celular:'
          TabOrder = 19
        end
        object CkNome: TCheckBox
          Left = 8
          Top = 106
          Width = 353
          Height = 17
          Caption = 'Nome, Raz'#227'o Social, Apelido ou Fantasia:'
          TabOrder = 25
        end
        object EdNome: TdmkEdit
          Left = 8
          Top = 124
          Width = 529
          Height = 21
          TabOrder = 26
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdTelefonesExit
        end
      end
      object CkGrupo: TCheckBox
        Left = 576
        Top = 162
        Width = 201
        Height = 17
        Caption = 'Grupo'
        TabOrder = 4
        OnClick = CkRuaClick
      end
      object CBGrupo: TdmkDBLookupComboBox
        Left = 576
        Top = 180
        Width = 205
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEntiGrupos
        TabOrder = 5
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object BtTudo: TBitBtn
        Tag = 127
        Left = 380
        Top = 216
        Width = 101
        Height = 65
        Cursor = crHandPoint
        Hint = 'Todos tipos de cadastro'
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnClick = BtTudoClick
      end
      object CkAccount: TCheckBox
        Left = 368
        Top = 162
        Width = 201
        Height = 17
        Caption = 'Representante'
        TabOrder = 2
        OnClick = CkRuaClick
      end
      object CBAccount: TdmkDBLookupComboBox
        Left = 368
        Top = 180
        Width = 205
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsAccounts
        TabOrder = 3
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGEMail: TRadioGroup
        Left = 680
        Top = 210
        Width = 97
        Height = 72
        Caption = ' E-mail'
        ItemIndex = 0
        Items.Strings = (
          'Todos'
          'Cadastrados'
          'A cadastrar')
        TabOrder = 10
        OnClick = RGEMailClick
      end
      object CkCliInt: TCheckBox
        Left = 12
        Top = 288
        Width = 153
        Height = 17
        Caption = 'Somente clientes internos.'
        TabOrder = 11
      end
      object GroupBox2: TGroupBox
        Left = 484
        Top = 210
        Width = 193
        Height = 72
        Caption = '                                     '
        TabOrder = 9
        object Label3: TLabel
          Left = 84
          Top = 20
          Width = 28
          Height = 13
          Caption = 'At'#233' $:'
        end
        object Label2: TLabel
          Left = 8
          Top = 20
          Width = 26
          Height = 13
          Caption = 'De $:'
        end
        object EdLimiCredMax: TdmkEdit
          Left = 83
          Top = 36
          Width = 106
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '999.999.999.999,99'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 999999999999.990000000000000000
          ValWarn = False
          OnChange = EdLimiCredMaxChange
        end
        object EdLimiCredMin: TdmkEdit
          Left = 7
          Top = 36
          Width = 74
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdLimiCredMinChange
        end
      end
      object CkLimiCred: TCheckBox
        Left = 500
        Top = 208
        Width = 97
        Height = 17
        Caption = 'Limite de cr'#233'dito: '
        TabOrder = 8
      end
      object CkAtivo: TCheckBox
        Left = 173
        Top = 288
        Width = 112
        Height = 17
        Caption = 'Somente ativos'
        TabOrder = 12
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 309
      Width = 792
      Height = 169
      Align = alClient
      TabOrder = 1
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 790
        Height = 167
        Align = alClient
        DataSource = DsEntidades
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = DBGrid1DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEENTIDADE'
            Title.Caption = 'Nome /  Raz'#227'o Social'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEFANTASIA'
            Title.Caption = 'Apelido / Fantasia'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONTATO'
            Title.Caption = 'Contato'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RUA'
            Title.Caption = 'Logradouro'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NUM_TXT'
            Title.Caption = 'N'#250'mero'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Compl'
            Title.Caption = 'Complemento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Bairro'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cidade'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEUF'
            Title.Caption = 'UF'
            Width = 23
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CEP_TXT'
            Title.Caption = 'CEP'
            Width = 61
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pais'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ETE1_TXT'
            Title.Caption = 'E. Tel 1'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CNPJ_CPF_TXT'
            Title.Caption = 'CNPJ / CPF'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IE_RG'
            Title.Caption = 'I.E. / RG'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ETE2_TXT'
            Title.Caption = 'E. Tel 2'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ETE3_TXT'
            Title.Caption = 'E. Tel 3'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EFAX_TXT'
            Title.Caption = 'E. Fax'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ECEL_TXT'
            Title.Caption = 'E. Celular'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PTE1_TXT'
            Title.Caption = 'P. Tel  1'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PTE2_TXT'
            Title.Caption = 'P. Tel  2'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PTE3_TXT'
            Title.Caption = 'P. Tel  3'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PFAX_TXT'
            Title.Caption = 'P. Fax'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PCEL_TXT'
            Title.Caption = 'P. Celular'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CTEL_TXT'
            Title.Caption = 'Cobran'#231'a Tel.'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CFAX_TXT'
            Title.Caption = 'Cobran'#231'a Fax'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CCEL_TXT'
            Title.Caption = 'Cobran'#231'a Celular'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LTEL_TXT'
            Title.Caption = 'Entrega Tel.'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LFAX_TXT'
            Title.Caption = 'Entrega Fax'
            Width = 105
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LCEL_TXT'
            Title.Caption = 'Entrega Celular'
            Width = 105
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 240
        Height = 32
        Caption = 'Pesquisa Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 240
        Height = 32
        Caption = 'Pesquisa Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 240
        Height = 32
        Caption = 'Pesquisa Entidades'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 526
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 570
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 646
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSair: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSairClick
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 644
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtLocaliza1: TBitBtn
        Tag = 22
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtLocaliza1Click
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Imprime'
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtSelecionar: TBitBtn
        Tag = 5
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sele'#231#227'o'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtSelecionarClick
      end
    end
  end
  object DsUF: TDataSource
    DataSet = QrUF
    Left = 76
    Top = 428
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 16
    Top = 428
  end
  object DsCidades: TDataSource
    DataSet = QrCidades
    Left = 176
    Top = 428
  end
  object DsBairros: TDataSource
    DataSet = QrBairros
    Left = 300
    Top = 428
  end
  object DsRuas: TDataSource
    DataSet = QrRuas
    Left = 428
    Top = 424
  end
  object PMImprime: TPopupMenu
    Left = 188
    Top = 576
    object ListaTelefnica1: TMenuItem
      Caption = '&Lista Telef'#244'nica'
      object Modelo11: TMenuItem
        Caption = '&Modelo 1'
        OnClick = Modelo11Click
      end
      object Modelo21: TMenuItem
        Caption = 'M&odelo 2'
        OnClick = Modelo21Click
      end
    end
    object Endereos1: TMenuItem
      Caption = '&Endere'#231'os'
      OnClick = Endereos1Click
    end
    object EMails1: TMenuItem
      Caption = 'E-&Mails'
      OnClick = EMails1Click
    end
    object LimitedeCrdito1: TMenuItem
      Caption = 'Limite de &Cr'#233'dito'
      OnClick = LimitedeCrdito1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ExportaparaExcel1: TMenuItem
      Caption = 'Exporta para Excel'
      OnClick = ExportaparaExcel1Click
    end
  end
  object DsProfiss: TDataSource
    DataSet = QrProfiss
    Left = 232
    Top = 428
  end
  object QrProfiss: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT'
      'Profissao'
      'FROM entidades'
      'WHERE Profissao<>'#39#39
      'ORDER BY Profissao')
    Left = 232
    Top = 380
    object QrProfissProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntidadesAfterOpen
    AfterClose = QrEntidadesAfterClose
    OnCalcFields = QrEntidadesCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.LimiCred,'
      'IF (en.Tipo=0, en.RazaoSocial, en.Nome) NOMEENTIDADE,'
      'IF (en.Tipo=0, en.Fantasia, en.Apelido) NOMEFANTASIA,'
      'IF (en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF,'
      'IF (en.Tipo=0, en.IE, en.RG) IE_RG,'
      'IF (en.Tipo=0, en.EContato, en.PContato) CONTATO,'
      'IF (en.Tipo=0, en.ERua,    en.PRua    ) RUA,'
      'IF (en.Tipo=0, en.ENumero+0.000, en.PNumero+0.000 ) Numero,'
      'IF (en.Tipo=0, en.ECompl,  en.PCompl  ) Compl ,'
      'IF (en.Tipo=0, en.EBairro, en.PBairro ) Bairro,'
      'IF (en.Tipo=0, en.ECidade, en.PCidade ) Cidade,'
      'IF (en.Tipo=0, en.EUF,     en.PUF     ) UF    ,'
      'IF (en.Tipo=0, en.ECEP,    en.PCEP    ) CEP   ,'
      'IF (en.Tipo=0, en.EPais,   en.PPais   ) Pais  ,'
      'IF (en.Tipo=0, en.EEMail,   en.PEMail   ) EMail  ,'
      'IF (en.Tipo=0, en.ETel1,   en.PTel1   ) Te1  ,'
      'IF (en.Tipo=0, en.ETel2,   en.PTel2   ) Te2  ,'
      'IF (en.Tipo=0, en.ETel3,   en.PTel3   ) Te3  ,'
      'IF (en.Tipo=0, en.EFax,   en.PFax   ) Fax  ,'
      'IF (en.Tipo=0, en.ECel,   en.PCel   ) Cel ,'
      'en.PTe1, en.PTe2, en.PTe3, en.ETe1, en.ETe2, en.ETe3, '
      'en.PFax, en.EFax, en.PCel, en.ECel, '
      'en.CTel, en.CCel, en.CFax, en.LTel, en.LCel, en.LFax,'
      'uf.Nome NOMEUF'
      'FROM entidades en, UFs uf'
      
        'WHERE uf.Codigo=(CASE WHEN en.Tipo=0 THEN en.EUF else en.PUF END' +
        ')'
      'AND en.Codigo>0'
      
        'AND (en.Cliente1 = "V" OR en.Fornece1 = "V" OR en.Fornece2 = "V"' +
        ' OR en.Fornece3 = "V" OR en.Terceiro = "V")'
      'AND en.Fantasia LIKE "%%"'
      'ORDER BY IF (en.Tipo=0, en.RazaoSocial, en.Nome)')
    Left = 16
    Top = 380
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Required = True
      Size = 100
    end
    object QrEntidadesCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntidadesIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEntidadesCONTATO: TWideStringField
      FieldName = 'CONTATO'
      Size = 60
    end
    object QrEntidadesRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEntidadesCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrEntidadesBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrEntidadesCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrEntidadesUF: TFloatField
      FieldName = 'UF'
      Required = True
    end
    object QrEntidadesCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEntidadesPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrEntidadesPTe1: TWideStringField
      FieldName = 'PTe1'
      Origin = 'entidades.PTe1'
    end
    object QrEntidadesPTe2: TWideStringField
      FieldName = 'PTe2'
      Origin = 'entidades.Pte2'
    end
    object QrEntidadesPTe3: TWideStringField
      FieldName = 'PTe3'
      Origin = 'entidades.Pte3'
    end
    object QrEntidadesETe1: TWideStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrEntidadesETe2: TWideStringField
      FieldName = 'ETe2'
      Origin = 'entidades.Ete2'
    end
    object QrEntidadesETe3: TWideStringField
      FieldName = 'ETe3'
      Origin = 'entidades.Ete3'
    end
    object QrEntidadesPFax: TWideStringField
      FieldName = 'PFax'
      Origin = 'entidades.PFax'
    end
    object QrEntidadesEFax: TWideStringField
      FieldName = 'EFax'
      Origin = 'entidades.EFax'
    end
    object QrEntidadesPCel: TWideStringField
      FieldName = 'PCel'
      Origin = 'entidades.PCel'
    end
    object QrEntidadesECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrEntidadesCTel: TWideStringField
      FieldName = 'CTel'
      Origin = 'entidades.CTel'
    end
    object QrEntidadesCCel: TWideStringField
      FieldName = 'CCel'
      Origin = 'entidades.CCel'
    end
    object QrEntidadesCFax: TWideStringField
      FieldName = 'CFax'
      Origin = 'entidades.CFax'
    end
    object QrEntidadesLTel: TWideStringField
      FieldName = 'LTel'
      Origin = 'entidades.LTel'
    end
    object QrEntidadesLCel: TWideStringField
      FieldName = 'LCel'
      Origin = 'entidades.LCel'
    end
    object QrEntidadesLFax: TWideStringField
      FieldName = 'LFax'
      Origin = 'entidades.LFax'
    end
    object QrEntidadesNUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUM_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesETE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE2_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesETE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE3_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesEFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EFAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesECEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesPTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesPTE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE2_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesPTE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE3_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesPFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PFAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesPCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesCCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesCFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CFAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesCTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CTEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesLTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LTEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesLCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesLFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LFAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesRUA_NUM_COMPL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'RUA_NUM_COMPL'
      Size = 255
      Calculated = True
    end
    object QrEntidadesBAIRRO_CIDADE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'BAIRRO_CIDADE'
      Size = 255
      Calculated = True
    end
    object QrEntidadesCEP_UF_PAIS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_UF_PAIS'
      Size = 255
      Calculated = True
    end
    object QrEntidadesNOMEFANTASIA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEFANTASIA'
      Required = True
      Size = 60
    end
    object QrEntidadesEMail: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'EMail'
      Size = 100
    end
    object QrEntidadesLimiCred: TFloatField
      FieldName = 'LimiCred'
      Origin = 'entidades.LimiCred'
    end
    object QrEntidadesCODIGO_NOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CODIGO_NOME'
      Size = 200
      Calculated = True
    end
    object QrEntidadesNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEntidadesTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrEntidadesTe2: TWideStringField
      FieldName = 'Te2'
    end
    object QrEntidadesTe3: TWideStringField
      FieldName = 'Te3'
    end
    object QrEntidadesCel: TWideStringField
      FieldName = 'Cel'
    end
    object QrEntidadesFax: TWideStringField
      FieldName = 'Fax'
    end
    object QrEntidadesTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesTE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE2_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesTE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE3_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntidadesFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
  end
  object QrUF: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ufs'
      'ORDER BY Nome')
    Left = 76
    Top = 380
    object QrUFCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUFNome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
    object QrUFLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrUFDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrUFDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrUFUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrUFUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrUFICMS_C: TFloatField
      FieldName = 'ICMS_C'
    end
    object QrUFICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
  end
  object QrCidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT'
      'IF (Tipo=0, ECidade, PCidade) CIDADE '
      'FROM entidades'
      'WHERE IF (Tipo=0, ECidade, PCidade) <>'#39#39
      'ORDER BY CIDADE')
    Left = 176
    Top = 380
    object QrCidadesCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
  end
  object QrBairros: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT'
      'IF (Tipo=0, EBairro, PBairro) BAIRRO'
      'FROM entidades'
      'WHERE IF (Tipo=0, EBairro, PBairro) <>'#39#39
      'ORDER BY Bairro')
    Left = 300
    Top = 380
    object QrBairrosBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
  end
  object QrRuas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT'
      'IF (Tipo=0, ERua, PRua) Rua'
      'FROM entidades'
      'WHERE IF (Tipo=0, ERua, PRua) <>'#39#39
      'ORDER BY Rua')
    Left = 428
    Top = 376
    object QrRuasRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
  end
  object QrEntiGrupos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM entigrupos'
      'ORDER BY Nome')
    Left = 364
    Top = 380
  end
  object DsEntiGrupos: TDataSource
    DataSet = QrEntiGrupos
    Left = 364
    Top = 424
  end
  object QrAccounts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEENTIDADE'
      '')
    Left = 488
    Top = 376
    object QrAccountsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAccountsNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsAccounts: TDataSource
    DataSet = QrAccounts
    Left = 488
    Top = 424
  end
  object QrPais: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT'
      'IF (Tipo=0, EPais, PPais) PAIS '
      'FROM entidades'
      'WHERE IF (Tipo=0, EPais, PPais) <>'#39#39
      'ORDER BY PAIS')
    Left = 124
    Top = 380
    object QrPaisPAIS: TWideStringField
      FieldName = 'PAIS'
    end
  end
  object DsPais: TDataSource
    DataSet = QrPais
    Left = 124
    Top = 428
  end
  object frxDsLista: TfrxDBDataset
    UserName = 'frxDsLista'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NOMEENTIDADE=NOMEENTIDADE'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'CONTATO=CONTATO'
      'RUA=RUA'
      'Compl=Compl'
      'Bairro=Bairro'
      'Cidade=Cidade'
      'UF=UF'
      'CEP=CEP'
      'Pais=Pais'
      'PTe1=PTe1'
      'PTe2=PTe2'
      'PTe3=PTe3'
      'ETe1=ETe1'
      'ETe2=ETe2'
      'ETe3=ETe3'
      'PFax=PFax'
      'EFax=EFax'
      'PCel=PCel'
      'ECel=ECel'
      'CTel=CTel'
      'CCel=CCel'
      'CFax=CFax'
      'LTel=LTel'
      'LCel=LCel'
      'LFax=LFax'
      'NUM_TXT=NUM_TXT'
      'ETE1_TXT=ETE1_TXT'
      'ETE2_TXT=ETE2_TXT'
      'ETE3_TXT=ETE3_TXT'
      'EFAX_TXT=EFAX_TXT'
      'ECEL_TXT=ECEL_TXT'
      'CNPJ_CPF_TXT=CNPJ_CPF_TXT'
      'PTE1_TXT=PTE1_TXT'
      'PTE2_TXT=PTE2_TXT'
      'PTE3_TXT=PTE3_TXT'
      'PFAX_TXT=PFAX_TXT'
      'PCEL_TXT=PCEL_TXT'
      'CCEL_TXT=CCEL_TXT'
      'CFAX_TXT=CFAX_TXT'
      'CTEL_TXT=CTEL_TXT'
      'LTEL_TXT=LTEL_TXT'
      'LCEL_TXT=LCEL_TXT'
      'LFAX_TXT=LFAX_TXT'
      'CEP_TXT=CEP_TXT'
      'NOMEUF=NOMEUF'
      'RUA_NUM_COMPL=RUA_NUM_COMPL'
      'BAIRRO_CIDADE=BAIRRO_CIDADE'
      'CEP_UF_PAIS=CEP_UF_PAIS'
      'NOMEFANTASIA=NOMEFANTASIA'
      'EMail=EMail'
      'LimiCred=LimiCred'
      'CODIGO_NOME=CODIGO_NOME'
      'Numero=Numero'
      'Te1=Te1'
      'Te2=Te2'
      'Te3=Te3'
      'Cel=Cel'
      'Fax=Fax'
      'TE1_TXT=TE1_TXT'
      'TE2_TXT=TE2_TXT'
      'TE3_TXT=TE3_TXT'
      'CEL_TXT=CEL_TXT'
      'FAX_TXT=FAX_TXT')
    DataSet = QrEntidades
    BCDToCurrency = False
    Left = 700
    Top = 325
  end
  object frxLista: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38007.434370358800000000
    ReportOptions.LastChange = 39540.764909571800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 672
    Top = 325
    Datasets = <
      item
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo32: TfrxMemoView
          Left = 557.779530000000000000
          Top = 5.763760000000001000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 24.000000000000000000
        Top = 347.716760000000000000
        Width = 755.906000000000000000
        object Memo31: TfrxMemoView
          Left = 489.338590000000000000
          Top = 2.739949999999965000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 5.338590000000000000
          Top = 2.519479999999987000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 68.000000000000000000
        Top = 219.212740000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 273.338590000000000000
          Top = 25.905380000000010000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."ETE1_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 365.338590000000000000
          Top = 25.905380000000010000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."ETE2_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 457.338590000000000000
          Top = 25.905380000000010000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."ETE3_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 549.338590000000000000
          Top = 25.905380000000010000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."EFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 641.338590000000000000
          Top = 25.905380000000010000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."ECEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 229.338590000000000000
          Top = 25.905380000000010000
          Width = 44.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 273.338590000000000000
          Top = 39.905379999999980000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."CTEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 365.338590000000000000
          Top = 39.905379999999980000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 457.338590000000000000
          Top = 39.905379999999980000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 549.338590000000000000
          Top = 39.905379999999980000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."CFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 641.338590000000000000
          Top = 39.905379999999980000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."CCEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 229.338590000000000000
          Top = 39.905379999999980000
          Width = 44.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Cobran'#231'a')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 273.338590000000000000
          Top = 53.905379999999980000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."LTEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 365.338590000000000000
          Top = 53.905379999999980000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 457.338590000000000000
          Top = 53.905379999999980000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 549.338590000000000000
          Top = 53.905379999999980000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."LFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 641.338590000000000000
          Top = 53.905379999999980000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."LCEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 229.338590000000000000
          Top = 53.905379999999980000
          Width = 44.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Entrega')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 5.338590000000000000
          Top = 53.905379999999980000
          Width = 224.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."CEP_UF_PAIS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 5.338590000000000000
          Top = 39.905379999999980000
          Width = 224.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Memo.UTF8W = (
            '[frxDsLista."BAIRRO_CIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 5.338590000000000000
          Top = 25.905380000000010000
          Width = 224.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Memo.UTF8W = (
            '[frxDsLista."RUA_NUM_COMPL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 5.338590000000000000
          Top = 11.905380000000010000
          Width = 224.000000000000000000
          Height = 13.984251970000000000
          DataField = 'NOMEFANTASIA'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Memo.UTF8W = (
            '[frxDsLista."NOMEFANTASIA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 273.338590000000000000
          Top = 11.905380000000010000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."PTE1_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 365.338590000000000000
          Top = 11.905380000000010000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."PTE2_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 457.338590000000000000
          Top = 11.905380000000010000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."PTE3_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 549.338590000000000000
          Top = 11.905380000000010000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."PFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 641.338590000000000000
          Top = 11.905380000000010000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."PCEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 229.338590000000000000
          Top = 11.905380000000010000
          Width = 44.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Pessoal')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 85.338590000000000000
          Width = 648.000000000000000000
          Height = 12.000000000000000000
          DataField = 'NOMEENTIDADE'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Memo.UTF8W = (
            '[frxDsLista."NOMEENTIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 5.338590000000000000
          Width = 80.000000000000000000
          Height = 12.000000000000000000
          DataField = 'Codigo'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Memo.UTF8W = (
            '[frxDsLista."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 90.000000000000000000
        Top = 68.031540000000010000
        Width = 755.906000000000000000
        object Memo35: TfrxMemoView
          Left = 273.338590000000000000
          Top = 69.070810000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 1')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 5.338590000000000000
          Top = 1.070809999999995000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 177.338590000000000000
          Top = 5.070809999999994000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 177.338590000000000000
          Top = 33.070809999999990000
          Width = 552.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'RELA'#199#195'O TELEF'#212'NICA PESSOAL')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 5.338590000000000000
          Top = 69.070810000000000000
          Width = 224.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 365.338590000000000000
          Top = 69.070810000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 2')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 457.338590000000000000
          Top = 69.070810000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 3')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 549.338590000000000000
          Top = 69.070810000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Fax')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 641.338590000000000000
          Top = 69.070810000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 229.338590000000000000
          Top = 69.070810000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
        end
      end
    end
  end
  object frxEndereco: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38007.434370937500000000
    ReportOptions.LastChange = 39540.780309965310000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 672
    Top = 353
    Datasets = <
      item
        DataSet = frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 287.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 1084.725110000000000000
        object Memo32: TfrxMemoView
          Left = 858.000000000000000000
          Top = 9.543290000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 24.000000000000000000
        Top = 291.023810000000000000
        Width = 1084.725110000000000000
        object Memo31: TfrxMemoView
          Left = 790.000000000000000000
          Top = 9.196660000000010000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 30.000000000000000000
          Top = 5.196660000000010000
          Width = 1008.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 14.000000000000000000
        Top = 215.433210000000000000
        Width = 1084.725110000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEndereco
        DataSetName = 'frxDsEndereco'
        RowCount = 0
        object Memo14: TfrxMemoView
          Left = 30.000000000000000000
          Width = 224.000000000000000000
          Height = 14.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEndereco."NOMEENTIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 254.000000000000000000
          Width = 188.000000000000000000
          Height = 14.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEndereco."RUA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 442.000000000000000000
          Width = 36.000000000000000000
          Height = 14.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEndereco."NUM_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 478.000000000000000000
          Width = 120.000000000000000000
          Height = 14.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEndereco."Compl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 598.000000000000000000
          Width = 156.000000000000000000
          Height = 14.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEndereco."Bairro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 754.000000000000000000
          Width = 124.000000000000000000
          Height = 14.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEndereco."Cidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 902.000000000000000000
          Width = 52.000000000000000000
          Height = 14.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEndereco."CEP_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 878.000000000000000000
          Width = 24.000000000000000000
          Height = 14.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEndereco."NOMEUF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 954.000000000000000000
          Width = 80.000000000000000000
          Height = 14.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEndereco."Pais"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929133858267700000
        Top = 68.031540000000000000
        Width = 1084.725110000000000000
        object Picture2: TfrxPictureView
          Left = 30.000000000000000000
          Top = 1.291280000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 202.000000000000000000
          Top = 5.291280000000000000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 202.000000000000000000
          Top = 33.291280000000000000
          Width = 552.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'RELA'#199#195'O DE ENDERE'#199'OS')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 254.000000000000000000
          Top = 68.031496062992100000
          Width = 188.000000000000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Logradouro')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 30.000000000000000000
          Top = 68.031496062992100000
          Width = 224.000000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 442.000000000000000000
          Top = 68.031496062992100000
          Width = 36.000000000000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 478.000000000000000000
          Top = 68.031496062992100000
          Width = 120.000000000000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Complemento')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 598.000000000000000000
          Top = 68.031496062992100000
          Width = 156.000000000000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Bairro')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 754.000000000000000000
          Top = 68.031496062992100000
          Width = 124.000000000000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Cidade')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 902.000000000000000000
          Top = 68.031496062992100000
          Width = 52.000000000000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 878.000000000000000000
          Top = 68.031496062992100000
          Width = 24.000000000000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 954.000000000000000000
          Top = 68.031496062992100000
          Width = 80.000000000000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Pa'#237's')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEndereco: TfrxDBDataset
    UserName = 'frxDsEndereco'
    CloseDataSource = False
    DataSet = QrEntidades
    BCDToCurrency = False
    Left = 700
    Top = 353
  end
  object frxEmail: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.545935868100000000
    ReportOptions.LastChange = 39717.545935868100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 672
    Top = 381
    Datasets = <
      item
        DataSet = frxDsEntidades
        DataSetName = 'frxDsEntidades'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 18.897637800000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 554.220470000000000000
          Top = 3.779527560000000000
          Width = 164.000000000000000000
          Height = 15.118110236220500000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 24.000000000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 474.000000000000000000
          Top = 9.196660000000010000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 2.000000000000000000
          Top = 5.196660000000010000
          Width = 720.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346456692900000
        Top = 230.551330000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEntidades
        DataSetName = 'frxDsEntidades'
        RowCount = 0
        object Memo14: TfrxMemoView
          Left = 50.000000000000000000
          Width = 224.000000000000000000
          Height = 13.228346460000000000
          DataField = 'NOMEENTIDADE'
          DataSet = frxDsEntidades
          DataSetName = 'frxDsEntidades'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidades."NOMEENTIDADE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 274.000000000000000000
          Width = 444.000000000000000000
          Height = 13.228346460000000000
          DataField = 'EMail'
          DataSet = frxDsEntidades
          DataSetName = 'frxDsEntidades'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidades."EMail"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 2.000000000000000000
          Width = 48.000000000000000000
          Height = 13.228346460000000000
          DataField = 'Codigo'
          DataSet = frxDsEntidades
          DataSetName = 'frxDsEntidades'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntidades."Codigo"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 109.968460000000000000
        Top = 60.472480000000000000
        Width = 718.110700000000000000
        object Memo35: TfrxMemoView
          Left = 274.000000000000000000
          Top = 91.968460000000000000
          Width = 444.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'E-Mail')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 2.000000000000000000
          Top = 23.968460000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 174.000000000000000000
          Top = 27.968460000000000000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 174.000000000000000000
          Top = 55.968460000000000000
          Width = 552.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'RELA'#199#195'O DE E-MAILS')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 50.000000000000000000
          Top = 91.968460000000000000
          Width = 224.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 2.000000000000000000
          Top = 91.968460000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEntidades: TfrxDBDataset
    UserName = 'frxDsEntidades'
    CloseDataSource = False
    DataSet = QrEntidades
    BCDToCurrency = False
    Left = 700
    Top = 381
  end
  object frxLimiCred: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.550239432900000000
    ReportOptions.LastChange = 39717.550239432900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxLimiCredGetValue
    Left = 672
    Top = 409
    Datasets = <
      item
        DataSet = frxDsEntidades
        DataSetName = 'frxDsEntidades'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 550.440940000000000000
          Top = 5.763760000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 31.858070000000000000
        Top = 325.039580000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 470.440940000000000000
          Top = 10.299010000000000000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 1.559060000000000000
          Top = 2.519479999999990000
          Width = 713.102350000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 30.236220470000000000
        Top = 234.330860000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEntidades
        DataSetName = 'frxDsEntidades'
        RowCount = 0
        object Memo15: TfrxMemoView
          Left = 351.496062990000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidades."PTE1_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 442.204724410000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidades."PTE2_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 532.913385830000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidades."PTE3_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 623.622047240000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidades."PCEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 351.496062990000000000
          Top = 15.118110240000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidades."ETE1_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 442.204724410000000000
          Top = 15.118110240000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidades."ETE2_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 532.913385830000000000
          Top = 15.118110240000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidades."ETE3_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 623.622047240000000000
          Top = 15.118110240000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEntidades."ECEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 260.787401570000000000
          Width = 90.708661420000000000
          Height = 30.236220470000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEntidades."LimiCred"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Width = 260.787401570000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 4.338590000000000000
          Top = 15.118110240000000000
          Width = 250.881880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxDsEntidades."RUA_NUM_COMPL"] - [frxDsEntidades."BAIRRO_CIDAD' +
              'E"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 4.338590000000000000
          Width = 250.881880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEntidades."CODIGO"] - [frxDsEntidades."NOMEENTIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 105.968460000000000000
        Top = 68.031540000000000000
        Width = 718.110700000000000000
        object Memo35: TfrxMemoView
          Left = 351.496062990000000000
          Top = 87.968460000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Telefone 1')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 5.338590000000000000
          Top = 12.409400000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 177.338590000000000000
          Top = 23.968460000000000000
          Width = 540.661410000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 177.338590000000000000
          Top = 51.968460000000000000
          Width = 540.661410000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'RELA'#199#195'O DE LIMITES DE CR'#201'DITO ENTRE [MIN_MAX]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Top = 87.968460000000000000
          Width = 260.787404020000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 442.204724410000000000
          Top = 87.968460000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Telefone 2')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 532.913385830000000000
          Top = 87.968460000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Telefone 3')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 623.622047240000000000
          Top = 87.968460000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 260.787401570000000000
          Top = 87.968460000000000000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Limite')
          ParentFont = False
        end
      end
    end
  end
  object DataSource1: TDataSource
    Left = 40
    Top = 12
  end
  object frxLista2: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38007.434370358800000000
    ReportOptions.LastChange = 39540.764909571800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 644
    Top = 325
    Datasets = <
      item
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo32: TfrxMemoView
          Left = 557.779530000000000000
          Top = 5.763760000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 24.000000000000000000
        Top = 294.803340000000000000
        Width = 755.906000000000000000
        object Memo31: TfrxMemoView
          Left = 489.338590000000000000
          Top = 2.739950000000000000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 2.519480000000000000
          Width = 758.456710000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 14.000000000000000000
        Top = 219.212740000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLista
        DataSetName = 'frxDsLista'
        RowCount = 0
        object Memo15: TfrxMemoView
          Left = 296.015770000000000000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          DataField = 'TE1_TXT'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."TE1_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 388.015770000000000000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          DataField = 'TE2_TXT'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."TE2_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 480.015770000000000000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          DataField = 'TE3_TXT'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."TE3_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 572.015770000000000000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          DataField = 'FAX_TXT'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."FAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 664.015770000000000000
          Width = 92.000000000000000000
          Height = 14.000000000000000000
          DataField = 'CEL_TXT'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."CEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 69.165354330000000000
          Width = 226.771653540000000000
          Height = 13.984251970000000000
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsLista."NOMEENTIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Width = 68.787401570000000000
          Height = 13.984251970000000000
          DataField = 'Codigo'
          DataSet = frxDsLista
          DataSetName = 'frxDsLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLista."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 90.000000000000000000
        Top = 68.031540000000000000
        Width = 755.906000000000000000
        object Memo35: TfrxMemoView
          Left = 296.015770000000000000
          Top = 69.070810000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 1')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 5.338590000000000000
          Top = 1.070809999999990000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 177.338590000000000000
          Top = 5.070809999999990000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 177.338590000000000000
          Top = 33.070810000000000000
          Width = 552.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'RELA'#199#195'O TELEF'#212'NICA PESSOAL')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 69.165381180000000000
          Top = 69.070810000000000000
          Width = 226.771626690000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 388.015770000000000000
          Top = 69.070810000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 2')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 480.015770000000000000
          Top = 69.070810000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 3')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 572.015770000000000000
          Top = 69.070810000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Fax')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 664.015770000000000000
          Top = 69.070810000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Top = 69.031540000000000000
          Width = 68.787401570000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
      end
    end
  end
end
