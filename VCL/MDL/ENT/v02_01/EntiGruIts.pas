unit EntiGruIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral, Variants, dmkValUsu,
  dmkImage, UnDmkEnums, dmkCheckBox;

type
  TFmEntiGruIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    SpeedButton2: TSpeedButton;
    EdControle: TdmkEdit;
    CBCargo: TdmkDBLookupComboBox;
    EdCargo: TdmkEditCB;
    Label11: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrCargos: TmySQLQuery;
    QrCargosCodigo: TIntegerField;
    QrCargosCodUsu: TIntegerField;
    QrCargosNome: TWideStringField;
    DsCargos: TDataSource;
    dmkValUsu1: TdmkValUsu;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    DsEntidades: TDataSource;
    QrEntidades: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrEntidadesNome: TWideStringField;
    CkPrincipal: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEntiTipCto(Codigo: Integer);
    procedure AtualizaPrincipal(Codigo, Controle: Integer);
    procedure ReopenEntidades(Codigo: Integer);
  public
    { Public declarations }
    FQrEntiGruIts: TmySQLQuery;
    FDsEntiGrupos: TDataSource;
  end;

  var
  FmEntiGruIts: TFmEntiGruIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, Principal, EntiContat,
  EntiTipCto, MyDBCheck, UnEntities, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmEntiGruIts.AtualizaPrincipal(Codigo, Controle: Integer);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE entigrupos SET ',
    'Principal = ' + Geral.FF0(Controle),
    'WHERE Codigo = ' + Geral.FF0(Codigo),
    '']);
end;

procedure TFmEntiGruIts.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  if DBEdCodigo.Text = '' then
    DBEdCodigo.UpdCampo := '';
  //
  if MyObjects.FIC(DBEdCodigo.Text = '', nil, 'Grupo n�o informado!') then Exit;
  if MyObjects.FIC(EdEntidade.ValueVariant = 0, EdEntidade, 'Informe a entidade!') then Exit;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('entigruits', 'Controle', ImgTipo.SQLType,
                EdControle.ValueVariant);
  //
  if UMyMod.ExecSQLInsUpdFm(FmEntiGruIts, ImgTipo.SQLType, 'entigruits',
    Controle, Dmod.QrUpd) then
  begin
    if CkPrincipal.Checked then
      AtualizaPrincipal(Geral.IMV(DBEdCodigo.Text), Controle);
    //
    DBEdCodigo.UpdCampo := 'Codigo';
    //
    UMyMod.AbreQuery(FQrEntiGruIts, Dmod.MyDB);
    //
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType         := stIns;
      EdControle.ValueVariant := 0;
      EdEntidade.ValueVariant := 0;
      CBEntidade.KeyValue     := 0;
      EdCargo.ValueVariant    := 0;
      CBCargo.KeyValue        := Null;
      CkPrincipal.Checked     := False;
      EdEntidade.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmEntiGruIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiGruIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  DBEdCodigo.DataSource := FDsEntiGrupos;
  DBEdNome.DataSource   := FDsEntiGrupos;
end;

procedure TFmEntiGruIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrCargos, Dmod.MyDB);
end;

procedure TFmEntiGruIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiGruIts.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    CkContinuar.Checked := True;
    CkContinuar.Visible := True;
  end else
  begin
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
  end;
  ReopenEntidades(Geral.IMV(DBEdCodigo.Text));
end;

procedure TFmEntiGruIts.SpeedButton1Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdEntidade.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  UMyMod.AbreQuery(QrEntidades, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdEntidade, CBEntidade, QrEntidades, VAR_CADASTRO);
end;

procedure TFmEntiGruIts.SpeedButton2Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  Entities.MostraFormEntiCargos(dmkValUsu1.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdCargo, CBCargo, QrCargos, VAR_CADASTRO);
    //
    EdCargo.SetFocus;
  end;
end;

procedure TFmEntiGruIts.MostraEntiTipCto(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmEntiTipCto, FmEntiTipCto, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEntiTipCto.LocCod(Codigo, Codigo);
    FmEntiTipCto.ShowModal;
    FmEntiTipCto.Destroy;
  end;
end;

procedure TFmEntiGruIts.ReopenEntidades(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB, [
    'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) Nome ',
    'FROM entidades ',
    'WHERE Ativo = 1 ',
    'AND Codigo NOT IN ',
    '( ',
    'SELECT Entidade ',
    'FROM entigruits ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    ') ',
    'ORDER BY Codigo ',
    '']);
end;

end.
