unit EntiAtivo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, dmkDBGrid,
  UnEntities, mySQLDbTables, UnProjGroup_Consts;

type
  Tenttip = (tetEntidade=0, tetContratos=1, tetArrecadacoes=2, tetUsrWeb=3);
  THackDBGrid = class(TDBGrid);
  TFmEntiAtivo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PnOutros: TGridPanel;
    PnEntidade: TPanel;
    GBEntidades: TGroupBox;
    GBContratos: TGroupBox;
    GBArrecada: TGroupBox;
    GBUsrWeb: TGroupBox;
    DBGEntidades: TdmkDBGrid;
    DBGContratos: TdmkDBGrid;
    DBGArrecada: TdmkDBGrid;
    DBGUsrWeb: TdmkDBGrid;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesAtivo: TSmallintField;
    QrEntidadesNome: TWideStringField;
    QrContratos: TmySQLQuery;
    DsContratos: TDataSource;
    QrArrecada: TmySQLQuery;
    DsArrecada: TDataSource;
    QrUsrWeb: TmySQLQuery;
    DsUsrWeb: TDataSource;
    QrContratosCodigo: TIntegerField;
    QrContratosNome: TWideStringField;
    QrArrecadaControle: TIntegerField;
    QrArrecadaAtivo: TSmallintField;
    QrArrecadaNome: TWideStringField;
    QrUsrWebCodigo: TIntegerField;
    QrUsrWebAtivo: TSmallintField;
    QrUsrWebUsername: TWideStringField;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    QrContratosStatus: TIntegerField;
    QrContratosDContrato_TXT: TWideStringField;
    QrContratosDtaPrxRenw_TXT: TWideStringField;
    QrContratosDtaCntrFim_TXT: TWideStringField;
    QrContratosStatus_TXT: TWideStringField;
    QrContratosDtaCntrFim: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrEntidadesBeforeClose(DataSet: TDataSet);
    procedure QrEntidadesAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGEntidadesCellClick(Column: TColumn);
    procedure DBGContratosCellClick(Column: TColumn);
    procedure DBGArrecadaCellClick(Column: TColumn);
    procedure DBGUsrWebCellClick(Column: TColumn);
    procedure DBGEntidadesDblClick(Sender: TObject);
    procedure DBGContratosDblClick(Sender: TObject);
    procedure DBGArrecadaDblClick(Sender: TObject);
    procedure DBGUsrWebDblClick(Sender: TObject);
    procedure QrContratosCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FContratos, FArrecadacoes, FUsrWeb: Boolean;
    procedure ReopenEntidades(Codigo: Integer);
    procedure ReopenContratos(Contratante, Codigo: Integer);
    procedure ReopenArrecada(Entidade, Controle: Integer);
    procedure ReopenUsrWeb(Entidade, Codigo: Integer);
    procedure ConfiguraJanela(Entidade: Integer);
    procedure AtivaItem(Ativo, ID: Integer; Tipo: Tenttip);
  public
    { Public declarations }
    FEntidade: Integer;
  end;

  var
  FmEntiAtivo: TFmEntiAtivo;

implementation

uses UnMyObjects, Module, ModuleGeral, UnDmkWeb, MyListas, DmkDAC_PF, MyDBCheck,
  {$IfNDef sCNTR}Contratos, UnContratUnit, {$EndIf}
  {$IfNDef sBLQ}UnBloquetos, {$EndIf}
  {$IfNDef sWEB}UnWUsersJan, {$EndIf}
  UMySQLModule;

{$R *.DFM}

procedure TFmEntiAtivo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiAtivo.ConfiguraJanela(Entidade: Integer);
var
  Msg: String;
begin
  PnEntidade.Visible := False;
  PnOutros.Visible   := False;
  //
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Dmod.MyDBn, Msg)
  then
    FUsrWeb := True;
  //
  if FUsrWeb = True then
  begin
    if not DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      Geral.MB_Aviso('Para realizar este procedimento voc� deve estar conectado na internet!');
      FUsrWeb := False;
      Exit;
    end;
  end;
  PnEntidade.Visible := True;
  PnOutros.Visible   := True;
  //
  {$IfNDef sCNTR}
  GBContratos.Visible := True;
  FContratos          := True;
  {$Else}
  GBContratos.Visible := False;
  FContratos          := False;
  {$EndIf}
  {$IfNDef sBLQ}
  GBArrecada.Visible := True;
  FArrecadacoes      := True;
  {$Else}
  GBArrecada.Visible := False;
  FArrecadacoes      := False;
  {$EndIf}
  {$IfNDef sWEB}
  GBUsrWeb.Visible := FUsrWeb;
  GBUsrWeb.Visible := FUsrWeb;
  {$Else}
  GBUsrWeb.Visible := False;
  GBUsrWeb.Visible := False;
  {$EndIf}
  //
  ReopenEntidades(Entidade);
end;

procedure TFmEntiAtivo.DBGArrecadaCellClick(Column: TColumn);
{$IfNDef sBLQ}
var
  Ativo, Controle: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Ativo    := QrArrecadaAtivo.Value;
    Controle := QrArrecadaControle.Value;
    //
    UBloquetos.AtivaDesativaArreIts(Controle, Ativo);
    ReopenArrecada(QrEntidadesCodigo.Value, Controle);
  end;
{$Else}
begin
{$EndIf}
end;

procedure TFmEntiAtivo.DBGArrecadaDblClick(Sender: TObject);
{$IfNDef sBLQ}
var
  Controle: Integer;
  Campo: String;
begin
  if (QrArrecada.State <> dsInactive) and (QrArrecada.RecordCount > 0) then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if UpperCase(Campo) = UpperCase('Controle') then
    begin
      Controle := QrArrecadaControle.Value;
      //
      UBloquetos.MostraBloArre(0, Controle);
    end;
  end;
{$Else}
begin
{$EndIf}
end;

procedure TFmEntiAtivo.DBGContratosCellClick(Column: TColumn);
{$IfNDef sCNTR}
var
  Status, Codigo: Integer;
  DtaCntrFim: TDate;
begin
  if Column.FieldName = 'Status_TXT' then
  begin
    Codigo     := QrContratosCodigo.Value;
    Status     := QrContratosStatus.Value;
    DtaCntrFim := QrContratosDtaCntrFim.Value;
    //
    ContratUnit.MostraCtrStatus(Codigo, Status, DtaCntrFim);
    ReopenContratos(QrEntidadesCodigo.Value, Codigo);
  end;
{$Else}
begin
{$EndIf}
end;

procedure TFmEntiAtivo.DBGContratosDblClick(Sender: TObject);
{$IfNDef sCNTR}
var
  Codigo: Integer;
  Campo: String;
begin
  if (QrContratos.State <> dsInactive) and (QrContratos.RecordCount > 0) then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if UpperCase(Campo) = UpperCase('Codigo') then
    begin
      Codigo := QrContratosCodigo.Value;
      //
      if DBCheck.CriaFm(TFmContratos, FmContratos, afmoNegarComAviso) then
      begin
        if Codigo <> 0 then
          FmContratos.LocCod(Codigo, Codigo);
        FmContratos.ShowModal;
        FmContratos.Destroy;
      end;
    end;
  end;
{$Else}
begin
{$EndIf}
end;

procedure TFmEntiAtivo.DBGEntidadesCellClick(Column: TColumn);
var
  Codigo, Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Ativo  := QrEntidadesAtivo.Value;
    Codigo := QrEntidadesCodigo.Value;
    //
    AtivaItem(Ativo, Codigo, tetEntidade);
    ReopenEntidades(Codigo);
  end;
end;

procedure TFmEntiAtivo.DBGEntidadesDblClick(Sender: TObject);
var
  Codigo: Integer;
  Campo: String;
begin
  if (QrEntidades.State <> dsInactive) and (QrEntidades.RecordCount > 0) then
  begin
    Campo  := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    Codigo := QrEntidadesCodigo.Value;
    //
    if UpperCase(Campo) = UpperCase('Codigo') then
      DModG.CadastroDeEntidade(Codigo, fmcadEntidade2, fmcadEntidade2);
  end;
end;

procedure TFmEntiAtivo.DBGUsrWebCellClick(Column: TColumn);
var
  Ativo, Codigo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Ativo  := QrUsrWebAtivo.Value;
    Codigo := QrUsrWebCodigo.Value;
    //
    AtivaItem(Ativo, Codigo, tetUsrWeb);
    ReopenUsrWeb(QrEntidadesCodigo.Value, Codigo);
  end;
end;

procedure TFmEntiAtivo.DBGUsrWebDblClick(Sender: TObject);
{$IfNDef sWEB}
var
  Codigo: Integer;
  Campo: String;
begin
  if (QrUsrWeb.State <> dsInactive) and (QrUsrWeb.RecordCount > 0) then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if UpperCase(Campo) = UpperCase('Codigo') then
    begin
      Codigo := QrUsrWebCodigo.Value;
      //
      UWUsersJan.MostraWUsers(Codigo);
    end;
  end;
{$Else}
begin
{$EndIf}
end;

procedure TFmEntiAtivo.AtivaItem(Ativo, ID: Integer; Tipo: Tenttip);
var
  Qry: TmySQLQuery;
  Tabela, CampoID: String;
begin
  Qry    := nil;
  Tabela := '';
  //
  case tipo of
    tetEntidade:
    begin
      Qry     := Dmod.QrUpd;
      Tabela  := 'entidades';
      CampoID := 'Codigo';
    end;
    tetContratos:
    begin
      Qry     := Dmod.QrUpd;
      Tabela  := 'contratos';
      CampoID := 'Codigo';
    end;
    tetArrecadacoes:
    begin
      Qry     := Dmod.QrUpd;
      Tabela  := 'bloarreits';
      CampoID := 'Controle';
    end;
    tetUsrWeb:
    begin
      {$IfNDef sWEB}
      Qry     := Dmod.QrUpdN;
      Tabela  := 'wusers';
      CampoID := 'Codigo';
      {$Else}
      Qry     := nil;
      Tabela  := '';
      CampoID := '';
      {$EndIf}
    end;
  end;
  if (Qry <> nil) and (Tabela <> '') and (CampoID <> '') then
  begin
    if Ativo = 0 then
      Ativo := 1
    else
      Ativo := 0;
    //
    UMyMod.SQLInsUpd(Qry, stUpd, Tabela, False,
      ['Ativo'], [CampoID], [Ativo], [ID], True);
    if tipo = tetEntidade then
    begin
      //Espec�fico por app
      if CO_DMKID_APP = 17 then //DControl
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'clientes', False,
          ['Ativo'], ['Cliente'], [Ativo], [ID], True);
      end;
    end;
  end;
end;

procedure TFmEntiAtivo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiAtivo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DBGEntidades.DataSource := DsEntidades;
  DBGContratos.DataSource := DsContratos;
  DBGArrecada.DataSource  := DsArrecada;
  DBGUsrWeb.DataSource    := DsUsrWeb;
  //
  FContratos    := False;
  FArrecadacoes := False;
  FUsrWeb       := False;
end;

procedure TFmEntiAtivo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiAtivo.FormShow(Sender: TObject);
begin
  ConfiguraJanela(FEntidade);
end;

procedure TFmEntiAtivo.QrContratosCalcFields(DataSet: TDataSet);
begin
  {$IfNDef sCNTR}
  QrContratosStatus_TXT.Value := CO_CONTRATO_STATUS_Str[QrContratosStatus.Value];
  {$Else}
  QrContratosStatus_TXT.Value := '';
  {$EndIf}
end;

procedure TFmEntiAtivo.QrEntidadesAfterScroll(DataSet: TDataSet);
var
  Entidade: Integer;
begin
  Entidade := QrEntidadesCodigo.Value;
  //
  if FContratos = True then
    ReopenContratos(Entidade, 0);
  if FArrecadacoes = True then
    ReopenArrecada(Entidade, 0);
  if FUsrWeb = True then
    ReopenUsrWeb(Entidade, 0);
end;

procedure TFmEntiAtivo.QrEntidadesBeforeClose(DataSet: TDataSet);
begin
  QrContratos.Close;
  QrArrecada.Close;
  QrUsrWeb.Close;
end;

procedure TFmEntiAtivo.ReopenArrecada(Entidade, Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrArrecada, Dmod.MyDB, [
    'SELECT its.Controle, its.Ativo, arr.Nome ',
    'FROM bloarreits its ',
    'LEFT JOIN bloarre arr ON arr.Codigo = its.Codigo ',
    'WHERE its.Entidade=' + Geral.FF0(Entidade),
    '']);
  if Controle > 0 then
    QrArrecada.Locate('Controle', Controle, []);
end;

procedure TFmEntiAtivo.ReopenContratos(Contratante, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratos, Dmod.MyDB, [
    'SELECT Codigo, Nome, Status, DtaCntrFim, ',
    'IF(DContrato < "1900-01-01", "", DATE_FORMAT(DContrato, "%d/%m/%Y")) DContrato_TXT, ',
    'IF(DtaPrxRenw < "1900-01-01", "", DATE_FORMAT(DtaPrxRenw, "%d/%m/%Y")) DtaPrxRenw_TXT, ',
    'IF(DtaCntrFim < "1900-01-01", "", DATE_FORMAT(DtaCntrFim, "%d/%m/%Y")) DtaCntrFim_TXT ',
    'FROM contratos ',
    'WHERE Contratante=' + Geral.FF0(Contratante),
    '']);
  if Codigo > 0 then
    QrContratos.Locate('Codigo', Codigo, []);
end;

procedure TFmEntiAtivo.ReopenEntidades(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB, [
    'SELECT Codigo, Ativo, ',
    'IF(Tipo=0, RazaoSocial, Nome) Nome ',
    'FROM entidades ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
end;

procedure TFmEntiAtivo.ReopenUsrWeb(Entidade, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUsrWeb, Dmod.MyDBn, [
    'SELECT Codigo, Username, Ativo ',
    'FROM wusers ',
    'WHERE Entidade=' + Geral.FF0(Entidade),
    '']);
  if Codigo > 0 then
    QrUsrWeb.Locate('Codigo', Codigo, []);
end;

end.
