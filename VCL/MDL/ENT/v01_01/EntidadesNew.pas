unit EntidadesNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ResIntStrings, UnInternalConsts, UnInternalConsts2, UMySQLModule,
  ComCtrls, mySQLDbTables, Grids, DBGrids, Menus, ZCF2,
  UnGotoy, dmkEdit, Variants, dmkEditCB, dmkDBLookupComboBox, dmkGeral,
  dmkEditDateTimePicker, dmkImage, UnDMkEnums, UnDmkProcFunc, dmkLabel;

type
  TFmEntidadesNew = class(TForm)
    PainelDados: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel3: TPanel;
    Memo: TMemo;
    DsEUfs: TDataSource;
    DsPUFs: TDataSource;
    QrControle: TmySQLQuery;
    QrControleDono: TIntegerField;
    QrLoc: TmySQLQuery;
    QrLocCodigo: TIntegerField;
    QrLocRazaoSocial: TWideStringField;
    QrLocFantasia: TWideStringField;
    QrLocRespons1: TWideStringField;
    QrLocRespons2: TWideStringField;
    QrLocPai: TWideStringField;
    QrLocMae: TWideStringField;
    QrLocCNPJ: TWideStringField;
    QrLocIE: TWideStringField;
    QrLocNome: TWideStringField;
    QrLocApelido: TWideStringField;
    QrLocCPF: TWideStringField;
    QrLocRG: TWideStringField;
    QrLocERua: TWideStringField;
    QrLocECompl: TWideStringField;
    QrLocEBairro: TWideStringField;
    QrLocECidade: TWideStringField;
    QrLocEUF: TSmallintField;
    QrLocEPais: TWideStringField;
    QrLocETe1: TWideStringField;
    QrLocETe2: TWideStringField;
    QrLocETe3: TWideStringField;
    QrLocECel: TWideStringField;
    QrLocEFax: TWideStringField;
    QrLocEEMail: TWideStringField;
    QrLocEContato: TWideStringField;
    QrLocENatal: TDateField;
    QrLocPRua: TWideStringField;
    QrLocPCompl: TWideStringField;
    QrLocPBairro: TWideStringField;
    QrLocPCidade: TWideStringField;
    QrLocPUF: TSmallintField;
    QrLocPPais: TWideStringField;
    QrLocPTe1: TWideStringField;
    QrLocPTe2: TWideStringField;
    QrLocPTe3: TWideStringField;
    QrLocPCel: TWideStringField;
    QrLocPFax: TWideStringField;
    QrLocPEMail: TWideStringField;
    QrLocPContato: TWideStringField;
    QrLocPNatal: TDateField;
    QrLocSexo: TWideStringField;
    QrLocResponsavel: TWideStringField;
    QrLocProfissao: TWideStringField;
    QrLocCargo: TWideStringField;
    QrLocRecibo: TSmallintField;
    QrLocDiaRecibo: TSmallintField;
    QrLocAjudaEmpV: TFloatField;
    QrLocAjudaEmpP: TFloatField;
    QrLocCliente1: TWideStringField;
    QrLocCliente2: TWideStringField;
    QrLocFornece1: TWideStringField;
    QrLocFornece2: TWideStringField;
    QrLocFornece3: TWideStringField;
    QrLocFornece4: TWideStringField;
    QrLocTerceiro: TWideStringField;
    QrLocCadastro: TDateField;
    QrLocInformacoes: TWideStringField;
    QrLocLogo: TBlobField;
    QrLocVeiculo: TIntegerField;
    QrLocMensal: TWideStringField;
    QrLocObservacoes: TWideMemoField;
    QrLocTipo: TSmallintField;
    QrLocLk: TIntegerField;
    QrUFs: TmySQLQuery;
    QrUFsCodigo: TIntegerField;
    QrUFsNome: TWideStringField;
    QrLocCNPJ_TXT: TWideStringField;
    QrLocETE1_TXT: TWideStringField;
    QrLocETE2_TXT: TWideStringField;
    QrLocETE3_TXT: TWideStringField;
    QrLocEFax_TXT: TWideStringField;
    QrLocECEL_TXT: TWideStringField;
    QrLocECEP: TIntegerField;
    QrLocPCEP: TIntegerField;
    QrLocGrupo: TIntegerField;
    QrLocDataAlt: TDateField;
    QrLocUserCad: TSmallintField;
    QrLocUserAlt: TSmallintField;
    QrLocCRua: TWideStringField;
    QrLocCCompl: TWideStringField;
    QrLocCBairro: TWideStringField;
    QrLocCCidade: TWideStringField;
    QrLocCUF: TSmallintField;
    QrLocCCEP: TIntegerField;
    QrLocCPais: TWideStringField;
    QrLocCTel: TWideStringField;
    QrLocCFax: TWideStringField;
    QrLocCCel: TWideStringField;
    QrLocCContato: TWideStringField;
    QrLocLRua: TWideStringField;
    QrLocLCompl: TWideStringField;
    QrLocLBairro: TWideStringField;
    QrLocLCidade: TWideStringField;
    QrLocLUF: TSmallintField;
    QrLocLCEP: TIntegerField;
    QrLocLPais: TWideStringField;
    QrLocLTel: TWideStringField;
    QrLocLFax: TWideStringField;
    QrLocLCel: TWideStringField;
    QrLocLContato: TWideStringField;
    QrLocComissao: TFloatField;
    QrLocDataCad: TDateField;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    BtExclui: TBitBtn;
    BtInclui: TBitBtn;
    QrEntiTransp: TmySQLQuery;
    DsEntiTransp: TDataSource;
    QrEntiTranspCodigo: TIntegerField;
    QrEntiTranspConta: TIntegerField;
    QrEntiTranspOrdem: TIntegerField;
    QrEntiTranspTransp: TIntegerField;
    QrEntiTranspNOMEENTIDADE: TWideStringField;
    PMCEP: TPopupMenu;
    Descobrir1: TMenuItem;
    Verificar1: TMenuItem;
    QrDuplic2: TmySQLQuery;
    QrDuplic2Codigo: TIntegerField;
    DsEntiGrupos: TDataSource;
    QrEntiGrupos: TmySQLQuery;
    QrEntiGruposCodigo: TIntegerField;
    QrEntiGruposNome: TWideStringField;
    QrEntiGruposDataCad: TDateField;
    QrEntiGruposDataAlt: TDateField;
    QrEntiGruposUserCad: TSmallintField;
    QrEntiGruposUserAlt: TSmallintField;
    QrEntiGruposLk: TIntegerField;
    DsAccounts: TDataSource;
    QrAccounts: TmySQLQuery;
    QrAccountsCodigo: TIntegerField;
    QrAccountsNOMEENTIDADE: TWideStringField;
    QrMotivos: TmySQLQuery;
    DsMotivos: TDataSource;
    QrMotivosCodigo: TIntegerField;
    QrMotivosDescricao: TWideStringField;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    DsEListaLograd: TDataSource;
    DsCListaLograd: TDataSource;
    DsLListaLograd: TDataSource;
    DsPListaLograd: TDataSource;
    TabSheet7: TTabSheet;
    PainelEntiCreds2: TPanel;
    Grade1: TStringGrid;
    QrEmpresas: TmySQLQuery;
    DsEmpresas: TDataSource;
    QrEmpresasRazaoSocial: TWideStringField;
    QrEmpresasCodigo: TIntegerField;
    QrEstCivil: TmySQLQuery;
    QrEstCivilCodigo: TIntegerField;
    QrEstCivilNome: TWideStringField;
    QrEstCivilLk: TIntegerField;
    QrEstCivilDataCad: TDateField;
    QrEstCivilDataAlt: TDateField;
    QrEstCivilUserCad: TIntegerField;
    QrEstCivilUserAlt: TIntegerField;
    DsEstCivil: TDataSource;
    QrLocENumero: TIntegerField;
    QrLocPNumero: TIntegerField;
    QrLocELograd: TSmallintField;
    QrLocPLograd: TSmallintField;
    TabSheet8: TTabSheet;
    Panel5: TPanel;
    BtIncluiCtas: TBitBtn;
    BtAlteraCtas: TBitBtn;
    BtExcluiCtas: TBitBtn;
    QrEntiCtas: TmySQLQuery;
    DsEntiCtas: TDataSource;
    QrEntiCtasCodigo: TIntegerField;
    QrEntiCtasControle: TIntegerField;
    QrEntiCtasBanco: TIntegerField;
    QrEntiCtasAgencia: TIntegerField;
    QrEntiCtasContaCor: TWideStringField;
    QrEntiCtasDAC_A: TWideStringField;
    QrEntiCtasDAC_C: TWideStringField;
    QrEntiCtasDAC_AC: TWideStringField;
    QrEntiCtasLk: TIntegerField;
    QrEntiCtasDataCad: TDateField;
    QrEntiCtasDataAlt: TDateField;
    QrEntiCtasUserCad: TIntegerField;
    QrEntiCtasUserAlt: TIntegerField;
    QrEntiCtasAlterWeb: TSmallintField;
    QrEntiCtasAtivo: TSmallintField;
    QrEntiCtasOrdem: TIntegerField;
    QrEntiCtasContaTip: TWideStringField;
    QrEntiCtasNOMEBANCO: TWideStringField;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    Panel7: TPanel;
    Panel6: TPanel;
    Label127: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    DBGrid2: TDBGrid;
    DsEquiCom: TDataSource;
    QrEquiCom: TmySQLQuery;
    QrEquiComCodigo: TIntegerField;
    QrEquiComNome: TWideStringField;
    QrNomes: TmySQLQuery;
    DsNomes: TDataSource;
    QrNomesCodigo: TIntegerField;
    QrNomesNOME_ENT: TWideStringField;
    Panel8: TPanel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    Label13: TLabel;
    TPCadastro: TdmkEditDateTimePicker;
    EdNome_2: TdmkEdit;
    Label79: TLabel;
    Label80: TLabel;
    EdRazao2: TdmkEdit;
    QrNomesSONORIZACAO: TWideStringField;
    QrSom: TmySQLQuery;
    QrSomNome: TWideStringField;
    PnPessoal05: TPanel;
    PnEmpresa001: TPanel;
    Label2: TLabel;
    Label129: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label126: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    SpeedButton1: TSpeedButton;
    EdRazao: TdmkEdit;
    EdFilial: TdmkEdit;
    EdFantasia: TdmkEdit;
    EdRespons1: TdmkEdit;
    EdCPF_Resp1: TdmkEdit;
    EdRespons2: TdmkEdit;
    EdCNPJ: TdmkEdit;
    EdIE: TdmkEdit;
    PnEmpresa002: TPanel;
    Label15: TLabel;
    Label98: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    EdECEP: TdmkEdit;
    CBELograd: TdmkDBLookupComboBox;
    EdERua: TdmkEdit;
    EdENumero: TdmkEdit;
    EdECompl: TdmkEdit;
    EdEBairro: TdmkEdit;
    EdECidade: TdmkEdit;
    EdEPais: TdmkEdit;
    BtCEP2: TBitBtn;
    PnEmpresa003: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label24: TLabel;
    Label23: TLabel;
    Label22: TLabel;
    Label21: TLabel;
    Label121: TLabel;
    Label122: TLabel;
    Label123: TLabel;
    EdETe1: TdmkEdit;
    EdETe2: TdmkEdit;
    EdETe3: TdmkEdit;
    EdECel: TdmkEdit;
    EdEEmail: TdmkEdit;
    TPENatal: TdmkEditDateTimePicker;
    EdEContato: TdmkEdit;
    EdEFax: TdmkEdit;
    EdFormaSociet: TdmkEdit;
    EdNIRE: TdmkEdit;
    CkSimples: TCheckBox;
    EdAtividade: TdmkEdit;
    PnEmpresa004: TPanel;
    GroupBox4: TGroupBox;
    CkCliente1_0: TCheckBox;
    CkFornece1_0: TCheckBox;
    CkFornece2_0: TCheckBox;
    CkFornece3_0: TCheckBox;
    CkFornece4_0: TCheckBox;
    CkTerceiro_0: TCheckBox;
    CkCliente2_0: TCheckBox;
    CkFornece5_0: TCheckBox;
    CkFornece6_0: TCheckBox;
    CkCliente4_0: TCheckBox;
    CkCliente3_0: TCheckBox;
    CkFornece8_0: TCheckBox;
    CkFornece7_0: TCheckBox;
    RGTipo_0: TRadioGroup;
    PnEmpresa005: TPanel;
    Panel10: TPanel;
    Label78: TLabel;
    Label49: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label128: TLabel;
    Label85: TLabel;
    Label89: TLabel;
    Label94: TLabel;
    Label86: TLabel;
    Label90: TLabel;
    Label87: TLabel;
    Label91: TLabel;
    Label88: TLabel;
    Label92: TLabel;
    Label93: TLabel;
    Label99: TLabel;
    Label100: TLabel;
    Label101: TLabel;
    GroupBox1: TGroupBox;
    CkCliente1_2: TCheckBox;
    CkFornece1_2: TCheckBox;
    CkFornece2_2: TCheckBox;
    CkFornece3_2: TCheckBox;
    CkFornece4_2: TCheckBox;
    CkTerceiro_2: TCheckBox;
    CkCliente2_2: TCheckBox;
    CkFornece5_2: TCheckBox;
    CkFornece6_2: TCheckBox;
    CkCliente3_2: TCheckBox;
    CkCliente4_2: TCheckBox;
    CkFornece7_2: TCheckBox;
    CkFornece8_2: TCheckBox;
    RGTipo_2: TRadioGroup;
    RGRecibo: TRadioGroup;
    GroupBox2: TGroupBox;
    Label53: TLabel;
    Label54: TLabel;
    EdAjudaV: TdmkEdit;
    EdAjudaP: TdmkEdit;
    CBGrupo: TDBLookupComboBox;
    EdInformacoes: TdmkEdit;
    CkDono: TCheckBox;
    RGSituacao: TRadioGroup;
    EdNivel: TdmkEdit;
    CBAccount: TDBLookupComboBox;
    EdRolComis: TdmkEditCB;
    CBRolComis: TdmkDBLookupComboBox;
    EdNome1: TdmkEdit;
    EdNome3: TdmkEdit;
    CBMotivo: TDBLookupComboBox;
    TPNatal1: TdmkEditDateTimePicker;
    TPNatal3: TdmkEditDateTimePicker;
    CkAgenda: TCheckBox;
    EdNome2: TdmkEdit;
    EdNome4: TdmkEdit;
    TPNatal4: TdmkEditDateTimePicker;
    CkSenhaQuer: TCheckBox;
    EdSenha1: TdmkEdit;
    EdComissao: TdmkEdit;
    EdDesco: TdmkEdit;
    EdCasasApliDesco: TdmkEdit;
    TPNatal2: TdmkEditDateTimePicker;
    EdDiaRecibo: TdmkEdit;
    Panel11: TPanel;
    Label62: TLabel;
    Label96: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label52: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label67: TLabel;
    Label66: TLabel;
    Label65: TLabel;
    EdCCEP: TdmkEdit;
    BtCEP3: TBitBtn;
    CBCLograd: TdmkDBLookupComboBox;
    EdCRua: TdmkEdit;
    EdCNumero: TdmkEdit;
    EdCCompl: TdmkEdit;
    EdCBairro: TdmkEdit;
    EdCCidade: TdmkEdit;
    EdCPais: TdmkEdit;
    EdCTel: TdmkEdit;
    EdCFax: TdmkEdit;
    EdCCel: TdmkEdit;
    EdCContato: TdmkEdit;
    Panel12: TPanel;
    Label131: TLabel;
    Label95: TLabel;
    Label55: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Label61: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label77: TLabel;
    EdLCEP: TdmkEdit;
    EdCEP4: TBitBtn;
    CBLLograd: TdmkDBLookupComboBox;
    EdLRua: TdmkEdit;
    EdLNumero: TdmkEdit;
    EdLCompl: TdmkEdit;
    EdLBairro: TdmkEdit;
    EdLTel: TdmkEdit;
    EdLFax: TdmkEdit;
    EdLCidade: TdmkEdit;
    EdLCel: TdmkEdit;
    EdLContato: TdmkEdit;
    EdLPais: TdmkEdit;
    Panel4: TPanel;
    Label42: TLabel;
    Label136: TLabel;
    EdLimiCred: TdmkEdit;
    EdCBE: TdmkEdit;
    CkSCB: TCheckBox;
    PainelEntiCreds1: TPanel;
    Label111: TLabel;
    Label116: TLabel;
    Label117: TLabel;
    Label112: TLabel;
    Label107: TLabel;
    Label108: TLabel;
    Label113: TLabel;
    Label109: TLabel;
    Label110: TLabel;
    Label114: TLabel;
    Label115: TLabel;
    EdCustodia: TdmkEdit;
    EdTxSerasa: TdmkEdit;
    EdTxAR: TdmkEdit;
    EdCobranca: TdmkEdit;
    EdFatorCompra: TdmkEdit;
    EdAdValorem: TdmkEdit;
    EdR_U: TdmkEdit;
    EdDMaisC: TdmkEdit;
    EdDMaisD: TdmkEdit;
    EdTarifaPostal: TdmkEdit;
    EdTarifaOperacao: TdmkEdit;
    EdELograd: TdmkEditCB;
    EdEUF: TdmkEdit;
    EdCLograd: TdmkEditCB;
    EdCUF: TdmkEdit;
    EdLUF: TdmkEdit;
    EdLLograd: TdmkEditCB;
    Panel14: TPanel;
    PnPessoal01: TPanel;
    Label25: TLabel;
    Label45: TLabel;
    Label26: TLabel;
    Label48: TLabel;
    Label124: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label47: TLabel;
    Label103: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label102: TLabel;
    Label118: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label120: TLabel;
    SpeedButton2: TSpeedButton;
    EdNome: TdmkEdit;
    TPPNatal: TdmkEditDateTimePicker;
    EdApelido: TdmkEdit;
    CBSexo: TComboBox;
    CBEstCivil: TDBLookupComboBox;
    EdPai: TdmkEdit;
    EdMae: TdmkEdit;
    EdResponsavel: TdmkEdit;
    EdCPF_Pai: TdmkEdit;
    EdCPF: TdmkEdit;
    EdRG: TdmkEdit;
    EdSSP: TdmkEdit;
    TPDataRG: TdmkEditDateTimePicker;
    EdProfissao: TdmkEdit;
    EdCargo: TdmkEdit;
    CBEmpresa: TDBLookupComboBox;
    PnPessoal02: TPanel;
    Label39: TLabel;
    Label97: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label38: TLabel;
    Label41: TLabel;
    BtCEP1: TBitBtn;
    EdPCEP: TdmkEdit;
    CBPLograd: TdmkDBLookupComboBox;
    EdPRua: TdmkEdit;
    EdPNumero: TdmkEdit;
    EdPCompl: TdmkEdit;
    EdPBairro: TdmkEdit;
    EdPCidade: TdmkEdit;
    EdPPais: TdmkEdit;
    EdPLograd: TdmkEditCB;
    EdPUF: TdmkEdit;
    PnPessoal03: TPanel;
    Label43: TLabel;
    Label44: TLabel;
    Label105: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label40: TLabel;
    Label119: TLabel;
    Label106: TLabel;
    Label104: TLabel;
    Label46: TLabel;
    Label125: TLabel;
    Label84: TLabel;
    Label83: TLabel;
    EdPTe1: TdmkEdit;
    EdPTe2: TdmkEdit;
    EdPTe3: TdmkEdit;
    EdPCel: TdmkEdit;
    EdPFax: TdmkEdit;
    EdPEmail: TdmkEdit;
    EdNacionalid: TdmkEdit;
    CBNUF: TDBLookupComboBox;
    EdCidadeNatal: TdmkEdit;
    EdConjuge: TdmkEdit;
    EdPContato: TdmkEdit;
    EdCPF_Conjuge: TdmkEdit;
    TPConjugeNatal: TdmkEditDateTimePicker;
    PnPessoal04: TPanel;
    GroupBox3: TGroupBox;
    CkCliente1_1: TCheckBox;
    CkFornece1_1: TCheckBox;
    CkFornece2_1: TCheckBox;
    CkFornece3_1: TCheckBox;
    CkFornece4_1: TCheckBox;
    CkTerceiro_1: TCheckBox;
    CkCliente2_1: TCheckBox;
    CkFornece5_1: TCheckBox;
    CkFornece6_1: TCheckBox;
    CkCliente3_1: TCheckBox;
    CkCliente4_1: TCheckBox;
    CkFornece7_1: TCheckBox;
    CkFornece8_1: TCheckBox;
    RGTipo_1: TRadioGroup;
    Panel15: TPanel;
    DBGrid3: TDBGrid;
    Panel9: TPanel;
    Label130: TLabel;
    DBEdit1: TDBEdit;
    GBRodaPe: TGroupBox;
    Panel13: TPanel;
    Panel16: TPanel;
    BtDesiste: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel17: TPanel;
    BtConfirma: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel18: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkLabel1: TdmkLabel;
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure EdTelefoneExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdCNPJExit(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure EdPCEPExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdECEPExit(Sender: TObject);
    procedure EdDiaReciboExit(Sender: TObject);
    procedure RGReciboClick(Sender: TObject);
    procedure EdAjudaVExit(Sender: TObject);
    procedure EdAjudaPExit(Sender: TObject);
    procedure QrLocCalcFields(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
    procedure EdCCEPExit(Sender: TObject);
    procedure EdLCEPExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtCEP3Click(Sender: TObject);
    procedure Descobrir1Click(Sender: TObject);
    procedure Verificar1Click(Sender: TObject);
    procedure EdNomeChange(Sender: TObject);
    procedure EdRazaoChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure CkCliente1_1Click(Sender: TObject);
    procedure CkCliente1_0Click(Sender: TObject);
    procedure CkCliente1_2Click(Sender: TObject);
    procedure EdCPF_PaiExit(Sender: TObject);
    procedure EdPPaisEnter(Sender: TObject);
    procedure EdEPaisEnter(Sender: TObject);
    procedure EdCPF_ConjugeExit(Sender: TObject);
    procedure EdCPF_Resp1Exit(Sender: TObject);
    procedure BtIncluiCtasClick(Sender: TObject);
    procedure BtAlteraCtasClick(Sender: TObject);
    procedure BtExcluiCtasClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BtCEP1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure EdCEP4Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdPCEPEnter(Sender: TObject);
    procedure EdECEPEnter(Sender: TObject);
    procedure EdCCEPEnter(Sender: TObject);
    procedure EdLCEPEnter(Sender: TObject);
    procedure EdEUFChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FOnCreate,
    FOnClick: Boolean;
    FPCEP, FECEP, FCCEP, FLCEP: String;
    procedure ReopenEntiTransp;
    procedure ReopenEntiCtas;
    procedure MostraPMCEP(Botao: TBitBtn);
    procedure AtualizaTipos;
    procedure VerificaNomesParecidos();
    procedure ConfiguraTipoEntidade(EntTipo: TUnEntTipo);
  public
    { Public declarations }
    FEntTipo: TUnEntTipo;
  end;

var
  FmEntidadesNew: TFmEntidadesNew;
  Fechar : Boolean;

implementation

uses Entidades, Module, EntidadesLoc, EntiTransp,
{$IfDef cDbTable}
EntiCEP,
{$EndIf}

Principal,
  MyDBCheck, EntiCtas, UnMyObjects, UnCEP, ModuleGeral, UnEntities,
  DmkDAC_PF;


{$R *.DFM}

procedure TFmEntidadesNew.BtConfirmaClick(Sender: TObject);
var
  EstCivil, PUF, EUF, CUF, LUF, NUF, Veiculo, Codigo, EntiGrupo, Account, Motivo,
  ELograd, PLograd, CLograd, LLograd, CasasApliDesco, Empresa, CodUsu: Integer;
  Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4,
  Fornece5, Fornece6, Fornece7, Fornece8: String;
  Terceiro, Mensal, Dono, Agenda, SenhaQuer: String;
  AjudaV, AjudaP, LimiCred, Comissao, Desco: Double;
  ECEP, PCEP, CCEP, LCEP: String;
begin
  Codigo := 0;
  //
  Geral.Valida_IE(EdIE.Text, EdEUF.Text, '??');
  //////////////////////////////////////////////////////////////////////////////
  Screen.Cursor := crHourGlass;
  if (RGTipo_0.ItemIndex = 0) and (Trim(EdRazao.Text) = '') then
  begin
    Geral.MB_Aviso('Tipo de cadastro empresa sem Raz�o Social definida!');
    PageControl1.ActivePageIndex := 1;
    EdRazao.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end else if (RGTipo_0.ItemIndex = 1) and (Trim(EdNome.Text) = '') then
  begin
    Geral.MB_Aviso('Tipo de cadastro pessoal sem Nome definido!');
    PageControl1.ActivePageIndex := 0;
    EdNome.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if ((RGTipo_2.ItemIndex = 0) and (Length(Geral.SoNumero1a9_TT(EdCNPJ.Text))>0))
  or ((RGTipo_2.ItemIndex = 1) and (Length(Geral.SoNumero1a9_TT(EdCPF.Text))>0))
  then
  begin
    case RGTipo_2.ItemIndex of
      0: if Entities.CNPJCPFDuplicado(Codigo, 0, EdCNPJ.Text, EdCPF.Text, ImgTipo.SQLType) then Exit;
      1: if Entities.CNPJCPFDuplicado(Codigo, 1, EdCNPJ.Text, EdCPF.Text, ImgTipo.SQLType) then Exit;
    end;
  end;
  Mensal   := 'F';
  Veiculo  := 0;
  Codigo   := Geral.IMV(EdCodigo.Text);
  LimiCred := Geral.DMV(EdLimiCred.Text);
  Comissao := Geral.DMV(EdComissao.Text);
  Desco    := Geral.DMV(EdDesco.Text);
  CasasApliDesco := Geral.IMV(EdCasasApliDesco.Text);
  if CBGrupo.KeyValue <> Null then EntiGrupo := CBGrupo.KeyValue else Entigrupo := 0;
  if CBAccount.KeyValue <> Null then Account := CBAccount.KeyValue else Account := 0;
  if CBEmpresa.KeyValue <> Null then Empresa := CBEmpresa.KeyValue else Empresa := 0;
  if CBMotivo.KeyValue <> Null then Motivo := CBMotivo.KeyValue else Motivo := 0;
  if CBEstCivil.KeyValue <> Null then EstCivil := CBEstCivil.KeyValue else EstCivil := 0;
  //if CBEUF.KeyValue = Null then EUF := 0 else EUF := CBEUF.KeyValue;
  EUF := Geral.GetCodigoUF_da_SiglaUF(EdEUF.Text);
  //if CBPUF.KeyValue = Null then PUF := 0 else PUF := CBPUF.KeyValue;
  PUF := Geral.GetCodigoUF_da_SiglaUF(EdPUF.Text);
  //if CBCUF.KeyValue = Null then CUF := 0 else CUF := CBCUF.KeyValue;
  CUF := Geral.GetCodigoUF_da_SiglaUF(EdCUF.Text);
  //if CBLUF.KeyValue = Null then LUF := 0 else LUF := CBLUF.KeyValue;
  LUF := Geral.GetCodigoUF_da_SiglaUF(EdLUF.Text);
  if CBNUF.KeyValue = Null then NUF := 0 else NUF := CBNUF.KeyValue;
  if CBELograd.KeyValue = Null then ELograd := 0 else ELograd := CBELograd.KeyValue;
  if CBPLograd.KeyValue = Null then PLograd := 0 else PLograd := CBPLograd.KeyValue;
  if CBCLograd.KeyValue = Null then CLograd := 0 else CLograd := CBCLograd.KeyValue;
  if CBLLograd.KeyValue = Null then LLograd := 0 else LLograd := CBLLograd.KeyValue;
  if CkCliente1_2.Checked then Cliente1 := 'V' else Cliente1 := 'F';
  if CkCliente2_2.Checked then Cliente2 := 'V' else Cliente2 := 'F';
  if CkCliente3_2.Checked then Cliente3 := 'V' else Cliente3 := 'F';
  if CkCliente4_2.Checked then Cliente4 := 'V' else Cliente4 := 'F';
  if CkFornece1_2.Checked then Fornece1 := 'V' else Fornece1 := 'F';
  if CkFornece2_2.Checked then Fornece2 := 'V' else Fornece2 := 'F';
  if CkFornece3_2.Checked then Fornece3 := 'V' else Fornece3 := 'F';
  if CkFornece4_2.Checked then Fornece4 := 'V' else Fornece4 := 'F';
  if CkFornece5_2.Checked then Fornece5 := 'V' else Fornece5 := 'F';
  if CkFornece6_2.Checked then Fornece6 := 'V' else Fornece6 := 'F';
  if CkFornece7_2.Checked then Fornece7 := 'V' else Fornece7 := 'F';
  if CkFornece8_2.Checked then Fornece8 := 'V' else Fornece8 := 'F';
  if CkTerceiro_2.Checked then Terceiro := 'V' else Terceiro := 'F';
  if
  (Cliente1 = 'F') and (Cliente2 = 'F') and (Cliente3 = 'F') and (Cliente4 = 'F') and
  (Fornece1 = 'F') and (Fornece2 = 'F') and (Fornece3 = 'F') and (Fornece4 = 'F') and
  (Fornece5 = 'F') and (Fornece6 = 'F') and (Fornece7 = 'F') and (Fornece8 = 'F') and
  (Terceiro = 'F') then
  begin
    Geral.MB_Aviso(
    'Defina pelo menos um "tipo de cadastro" em "Miscel�nea"');
    Screen.Cursor := crDefault;
    Exit;
  end;
  AjudaV := Geral.DMV(EdAjudaV.Text);
  AjudaP := Geral.DMV(EdAjudaP.Text);
  if CkAgenda.Checked then Agenda := 'V' else Agenda := 'F';
  if CkSenhaQuer.Checked then SenhaQuer := 'V' else SenhaQuer := 'F';
  //////////////////////////////////////////////////////////////////////////////
  if ImgTipo.SQLType = stIns then
    CodUsu := Codigo
  else CodUsu := FmEntidades.QrEntidadesCodUsu.Value;
  //////////////////////////////////////////////////////////////////////////////
  ECEP := Geral.SoNumero_TT(EdECEP.Text); if ECEP = '' then ECEP := '0';
  PCEP := Geral.SoNumero_TT(EdPCEP.Text); if pCEP = '' then PCEP := '0';
  CCEP := Geral.SoNumero_TT(EdCCEP.Text); if CCEP = '' then CCEP := '0';
  LCEP := Geral.SoNumero_TT(EdLCEP.Text); if LCEP = '' then LCEP := '0';
  ///
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO entidades SET ')
  else
    Dmod.QrUpdU.SQL.Add('UPDATE entidades SET AlterWeb=1, ');
  Dmod.QrUpdU.SQL.Add('RazaoSocial=:P0, Fantasia=:P1, Respons1=:P2, ');
  Dmod.QrUpdU.SQL.Add('Respons2=:P3, Pai=:P4, Mae=:P5, CNPJ=:P6, IE=:P7, ');
  Dmod.QrUpdU.SQL.Add('Nome=:P8, Apelido=:P9, CPF=:P10, RG=:P11, ');
  Dmod.QrUpdU.SQL.Add('ERua=:P12, ENumero=:P13, ECompl=:P14, EBairro=:P15, ');
  Dmod.QrUpdU.SQL.Add('ECidade=:P16, EUF=:P17, ECEP=:P18, EPais=:P19, ');
  Dmod.QrUpdU.SQL.Add('ETe1=:P20, ETe2=:P21, ETe3=:P22, ECel=:P23, ');
  Dmod.QrUpdU.SQL.Add('EFax=:P24, EEmail=:P25, EContato=:P26, ENatal=:P27, ');
  Dmod.QrUpdU.SQL.Add('PRua=:P28, PNumero=:P29, PCompl=:P30, PBairro=:P31, ');
  Dmod.QrUpdU.SQL.Add('PCidade=:P32, PUF=:P33, PCEP=:P34, PPais=:P35, ');
  Dmod.QrUpdU.SQL.Add('PTe1=:P36, PTe2=:P37, PTe3=:P38, PCel=:P39, ');
  Dmod.QrUpdU.SQL.Add('PFax=:P40, PEmail=:P41, PContato=:P42, PNatal=:P43, ');
  Dmod.QrUpdU.SQL.Add('Sexo=:P44, Responsavel=:P45, Cliente1=:P46, ');
  Dmod.QrUpdU.SQL.Add('Cliente2=:P47, Fornece1=:P48, Fornece2=:P49, ');
  Dmod.QrUpdU.SQL.Add('Fornece3=:P50, Fornece4=:P51, Terceiro=:P52, ');
  Dmod.QrUpdU.SQL.Add('Cadastro=:P53, Informacoes=:P54, Veiculo=:P55, ');
  Dmod.QrUpdU.SQL.Add('Mensal=:P56, Observacoes=:P57, Tipo=:P58, ');
  Dmod.QrUpdU.SQL.Add('Profissao=:P59, Cargo=:P60, Recibo=:P61, DiaRecibo=:P62,');
  Dmod.QrUpdU.SQL.Add('AjudaEmpV=:P63, AjudaEmpP=:P64, ');
  //
  Dmod.QrUpdU.SQL.Add('CRua=:P65, CNumero=:P66, CCompl=:P67, CBairro=:P68, ');
  Dmod.QrUpdU.SQL.Add('CCidade=:P69, CUF=:P70, CCEP=:P71, CPais=:P72, ');
  Dmod.QrUpdU.SQL.Add('CTel=:P73, CCel=:P74, CFax=:P75, CContato=:P76, ');
  //
  Dmod.QrUpdU.SQL.Add('LRua=:P77, LNumero=:P78, LCompl=:P79, LBairro=:P80, ');
  Dmod.QrUpdU.SQL.Add('LCidade=:P81, LUF=:P82, LCEP=:P83, LPais=:P84, ');
  Dmod.QrUpdU.SQL.Add('LTel=:P85, LCel=:P86, LFax=:P87, LContato=:P88, ');
  Dmod.QrUpdU.SQL.Add('Grupo=:P89, Situacao=:P90, Nivel=:P91, Account=:P92,');
  //
  Dmod.QrUpdU.SQL.Add('ConjugeNome=:P93, Nome1=:P94, Nome2=:P95, Nome3=:P96,');
  Dmod.QrUpdU.SQL.Add('Nome4=:P97, ConjugeNatal=:P98, Natal1=:P99, ');
  Dmod.QrUpdU.SQL.Add('Natal2=:P100, Natal3=:P101, Natal4=:P102, ');
  Dmod.QrUpdU.SQL.Add('Motivo=:P103, Agenda=:P104, SenhaQuer=:P105, ');
  Dmod.QrUpdU.SQL.Add('Senha1=:P106, ELograd=:P107, PLograd=:P108, ');
  Dmod.QrUpdU.SQL.Add('CLograd=:P109, LLograd=:P110, LimiCred=:P111, ');
  //
  Dmod.QrUpdU.SQL.Add('Comissao=:P112, Desco=:P113, CasasApliDesco=:P114,');
  //
  Dmod.QrUpdU.SQL.Add('CPF_Pai=:P115, CidadeNatal=:P116, SSP=:P117, ');
  Dmod.QrUpdU.SQL.Add('UFNatal=:P118, Fornece5=:P119, Fornece6=:P120, ');
  //
  Dmod.QrUpdU.SQL.Add('FatorCompra=:P121, AdValorem=:P122, DMaisC=:P123, ');
  Dmod.QrUpdU.SQL.Add('DMaisD=:P124, DataRG=:P125, Nacionalid=:P126, ');
  Dmod.QrUpdU.SQL.Add('Empresa=:P127, FormaSociet=:P128, NIRE=:P129, ');
  Dmod.QrUpdU.SQL.Add('Simples=:P130, Atividade=:P131, EstCivil=:P132, ');
  Dmod.QrUpdU.SQL.Add('CPF_Conjuge=:P133, CBE=:P134, SCB=:P135, ');
  Dmod.QrUpdU.SQL.Add('CPF_Resp1=:P136, Cliente3=:P137, Cliente4=:P138, ');
  Dmod.QrUpdU.SQL.Add('CartPref=:P139, RolComis=:P140, Filial=:P141, ');
  Dmod.QrUpdU.SQL.Add('Fornece7=:P142, Fornece8=:P143, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('CodUsu=:Pa, DataCad=:Px, UserCad=:py, Codigo=:Pz')
  else
    Dmod.QrUpdU.SQL.Add('CodUsu=:Pa, DataAlt=:Px, UserAlt=:py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := EdRazao.Text;
  Dmod.QrUpdU.Params[01].AsString  := EdFantasia.Text;
  Dmod.QrUpdU.Params[02].AsString  := EdRespons1.Text;
  Dmod.QrUpdU.Params[03].AsString  := EdRespons2.Text;
  Dmod.QrUpdU.Params[04].AsString  := EdPai.Text;
  Dmod.QrUpdU.Params[05].AsString  := EdMae.Text;
  Dmod.QrUpdU.Params[06].AsString  := Geral.FormataCNPJ_TFT(EdCNPJ.Text);
  Dmod.QrUpdU.Params[07].AsString  := EdIE.Text;
  Dmod.QrUpdU.Params[08].AsString  := EdNome.Text;
  Dmod.QrUpdU.Params[09].AsString  := EdApelido.Text;
  Dmod.QrUpdU.Params[10].AsString  := Geral.FormataCNPJ_TFT(EdCPF.Text);
  Dmod.QrUpdU.Params[11].AsString  := EdRG.Text;

  Dmod.QrUpdU.Params[12].AsString  := EdERua.Text;
  Dmod.QrUpdU.Params[13].AsInteger := Geral.IMV(Geral.SoNumero_TT(EdENumero.Text)); // arrumado?
  Dmod.QrUpdU.Params[14].AsString  := EdECompl.Text;
  Dmod.QrUpdU.Params[15].AsString  := EdEBairro.Text;
  Dmod.QrUpdU.Params[16].AsString  := EdECidade.Text;
  Dmod.QrUpdU.Params[17].AsInteger := EUF;
  Dmod.QrUpdU.Params[18].AsString  := ECEP;
  Dmod.QrUpdU.Params[19].AsString  := EdEPais.Text;
  Dmod.QrUpdU.Params[20].AsString  := Geral.SoNumeroESinal_TT(EdETe1.Text);
  Dmod.QrUpdU.Params[21].AsString  := Geral.SoNumeroESinal_TT(EdETe2.Text);
  Dmod.QrUpdU.Params[22].AsString  := Geral.SoNumeroESinal_TT(EdETe3.Text);
  Dmod.QrUpdU.Params[23].AsString  := Geral.SoNumeroESinal_TT(EdECel.Text);
  Dmod.QrUpdU.Params[24].AsString  := Geral.SoNumeroESinal_TT(EdEFax.Text);
  Dmod.QrUpdU.Params[25].AsString  := EdEEmail.Text;
  Dmod.QrUpdU.Params[26].AsString  := EdEContato.Text;
  Dmod.QrUpdU.Params[27].AsString  := FormatDateTime(VAR_FORMATDATE, TPENatal.Date);

  Dmod.QrUpdU.Params[28].AsString  := EdPRua.Text;
  Dmod.QrUpdU.Params[29].AsInteger := Geral.IMV(EdPNumero.Text);
  Dmod.QrUpdU.Params[30].AsString  := EdPCompl.Text;
  Dmod.QrUpdU.Params[31].AsString  := EdPBairro.Text;
  Dmod.QrUpdU.Params[32].AsString  := EdPCidade.Text;
  Dmod.QrUpdU.Params[33].AsInteger := PUF;
  Dmod.QrUpdU.Params[34].AsString  := PCEP;
  Dmod.QrUpdU.Params[35].AsString  := EdPPais.Text;
  Dmod.QrUpdU.Params[36].AsString  := Geral.SoNumeroESinal_TT(EdPTe1.Text);
  Dmod.QrUpdU.Params[37].AsString  := Geral.SoNumeroESinal_TT(EdPTe2.Text);
  Dmod.QrUpdU.Params[38].AsString  := Geral.SoNumeroESinal_TT(EdPTe3.Text);
  Dmod.QrUpdU.Params[39].AsString  := Geral.SoNumeroESinal_TT(EdPCel.Text);
  Dmod.QrUpdU.Params[40].AsString  := Geral.SoNumeroESinal_TT(EdPFax.Text);
  Dmod.QrUpdU.Params[41].AsString  := EdPEmail.Text;
  Dmod.QrUpdU.Params[42].AsString  := EdPContato.Text;
  Dmod.QrUpdU.Params[43].AsString  := FormatDateTime(VAR_FORMATDATE, TPPNatal.Date);

  Dmod.QrUpdU.Params[44].AsString  := CBSexo.Text;
  Dmod.QrUpdU.Params[45].AsString  := EdResponsavel.Text;
  Dmod.QrUpdU.Params[46].AsString  := Cliente1;
  Dmod.QrUpdU.Params[47].AsString  := Cliente2;
  Dmod.QrUpdU.Params[48].AsString  := Fornece1;
  Dmod.QrUpdU.Params[49].AsString  := Fornece2;
  Dmod.QrUpdU.Params[50].AsString  := Fornece3;
  Dmod.QrUpdU.Params[51].AsString  := Fornece4;
  Dmod.QrUpdU.Params[52].AsString  := Terceiro;
  Dmod.QrUpdU.Params[53].AsString  := FormatDateTime(VAR_FORMATDATE, TPCadastro.Date);
  Dmod.QrUpdU.Params[54].AsString  := EdInformacoes.Text;
  Dmod.QrUpdU.Params[55].AsInteger := Veiculo;
  Dmod.QrUpdU.Params[56].AsString  := Mensal;
  Dmod.QrUpdU.Params[57].AsString  := Memo.Text;
  Dmod.QrUpdU.Params[58].AsInteger := RGTipo_2.ItemIndex;
  Dmod.QrUpdU.Params[59].AsString  := EdProfissao.Text;
  Dmod.QrUpdU.Params[60].AsString  := EdCargo.Text;
  Dmod.QrUpdU.Params[61].AsInteger := RGRecibo.ItemIndex;
  Dmod.QrUpdU.Params[62].AsInteger := Geral.IMV(EdDiaRecibo.Text);
  Dmod.QrUpdU.Params[63].AsFloat   := AjudaV;
  Dmod.QrUpdU.Params[64].AsFloat   := AjudaP;

  Dmod.QrUpdU.Params[65].AsString  := EdCRua.Text;
  Dmod.QrUpdU.Params[66].AsInteger := Geral.IMV(EdCNumero.Text);
  Dmod.QrUpdU.Params[67].AsString  := EdCCompl.Text;
  Dmod.QrUpdU.Params[68].AsString  := EdCBairro.Text;
  Dmod.QrUpdU.Params[69].AsString  := EdCCidade.Text;
  Dmod.QrUpdU.Params[70].AsInteger := CUF;
  Dmod.QrUpdU.Params[71].AsString  := CCEP;
  Dmod.QrUpdU.Params[72].AsString  := EdCPais.Text;
  Dmod.QrUpdU.Params[73].AsString  := Geral.SoNumeroESinal_TT(EdCTel.Text);
  Dmod.QrUpdU.Params[74].AsString  := Geral.SoNumeroESinal_TT(EdCCel.Text);
  Dmod.QrUpdU.Params[75].AsString  := Geral.SoNumeroESinal_TT(EdCFax.Text);
  Dmod.QrUpdU.Params[76].AsString  := EdCContato.Text;

  Dmod.QrUpdU.Params[77].AsString  := EdLRua.Text;
  Dmod.QrUpdU.Params[78].AsInteger := Geral.IMV(EdLNumero.Text);
  Dmod.QrUpdU.Params[79].AsString  := EdLCompl.Text;
  Dmod.QrUpdU.Params[80].AsString  := EdLBairro.Text;
  Dmod.QrUpdU.Params[81].AsString  := EdLCidade.Text;
  Dmod.QrUpdU.Params[82].AsInteger := LUF;
  Dmod.QrUpdU.Params[83].AsString  := LCEP;
  Dmod.QrUpdU.Params[84].AsString  := EdLPais.Text;
  Dmod.QrUpdU.Params[85].AsString  := Geral.SoNumeroESinal_TT(EdLTel.Text);
  Dmod.QrUpdU.Params[86].AsString  := Geral.SoNumeroESinal_TT(EdLCel.Text);
  Dmod.QrUpdU.Params[87].AsString  := Geral.SoNumeroESinal_TT(EdLFax.Text);
  Dmod.QrUpdU.Params[88].AsString  := EdLContato.Text;
  Dmod.QrUpdU.Params[89].AsInteger := Entigrupo;
  Dmod.QrUpdU.Params[90].AsInteger := RGSituacao.ItemIndex;
  Dmod.QrUpdU.Params[91].AsString  := EdNivel.Text;
  Dmod.QrUpdU.Params[92].AsInteger := Account;
  //
  Dmod.QrUpdU.Params[93].AsString   := EdConjuge.Text;
  Dmod.QrUpdU.Params[94].AsString   := EdNome1.Text;
  Dmod.QrUpdU.Params[95].AsString   := EdNome2.Text;
  Dmod.QrUpdU.Params[96].AsString   := EdNome3.Text;
  Dmod.QrUpdU.Params[97].AsString   := EdNome4.Text;
  Dmod.QrUpdU.Params[98].AsString   := FormatDateTime(VAR_FORMATDATE, TPConjugeNatal.Date);
  Dmod.QrUpdU.Params[99].AsString   := FormatDateTime(VAR_FORMATDATE, TPNatal1.Date);
  Dmod.QrUpdU.Params[100].AsString  := FormatDateTime(VAR_FORMATDATE, TPNatal2.Date);
  Dmod.QrUpdU.Params[101].AsString  := FormatDateTime(VAR_FORMATDATE, TPNatal3.Date);
  Dmod.QrUpdU.Params[102].AsString  := FormatDateTime(VAR_FORMATDATE, TPNatal4.Date);
  Dmod.QrUpdU.Params[103].AsInteger := Motivo;
  Dmod.QrUpdU.Params[104].AsString  := Agenda;
  Dmod.QrUpdU.Params[105].AsString  := SenhaQuer;
  Dmod.QrUpdU.Params[106].AsString  := EdSenha1.Text;
  Dmod.QrUpdU.Params[107].AsInteger := ELograd;
  Dmod.QrUpdU.Params[108].AsInteger := PLograd;
  Dmod.QrUpdU.Params[109].AsInteger := CLograd;
  Dmod.QrUpdU.Params[110].AsInteger := LLograd;
  Dmod.QrUpdU.Params[111].AsFloat   := LimiCred;
  Dmod.QrUpdU.Params[112].AsFloat   := Comissao;
  Dmod.QrUpdU.Params[113].AsFloat   := Desco;
  Dmod.QrUpdU.Params[114].AsInteger := CasasApliDesco;
  Dmod.QrUpdU.Params[115].AsString  := Geral.FormataCNPJ_TFT(EdCPF_Pai.Text);
  Dmod.QrUpdU.Params[116].AsString  := EdCidadeNatal.Text;
  Dmod.QrUpdU.Params[117].AsString  := EdSSP.Text;
  Dmod.QrUpdU.Params[118].AsInteger := NUF;
  Dmod.QrUpdU.Params[119].AsString  := Fornece5;
  Dmod.QrUpdU.Params[120].AsString  := Fornece6;
  //
  Dmod.QrUpdU.Params[121].AsFloat   := Geral.DMV(EdFatorCompra.Text);
  Dmod.QrUpdU.Params[122].AsFloat   := Geral.DMV(EdAdValorem.Text);
  Dmod.QrUpdU.Params[123].AsInteger := Geral.IMV(EdDMaisC.Text);
  Dmod.QrUpdU.Params[124].AsInteger := Geral.IMV(EdDMaisD.Text);
  Dmod.QrUpdU.Params[125].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataRG.Date);
  Dmod.QrUpdU.Params[126].AsString  := EdNacionalid.Text;
  Dmod.QrUpdU.Params[127].AsInteger := Empresa;
  Dmod.QrUpdU.Params[128].AsString  := EdFormaSociet.Text;
  Dmod.QrUpdU.Params[129].AsString  := EdNIRE.Text;
  Dmod.QrUpdU.Params[130].AsInteger := Geral.BoolToInt(CkSimples.Checked);
  Dmod.QrUpdU.Params[131].AsString  := EdAtividade.Text;
  Dmod.QrUpdU.Params[132].AsInteger := EstCivil;
  Dmod.QrUpdU.Params[133].AsString  := Geral.FormataCNPJ_TFT(EdCPF_Conjuge.Text);
  Dmod.QrUpdU.Params[134].AsInteger := Geral.IMV(EdCBE.Text);
  Dmod.QrUpdU.Params[135].AsInteger := Geral.BoolToInt(CkSCB.Checked);
  Dmod.QrUpdU.Params[136].AsString  := Geral.FormataCNPJ_TFT(EdCPF_Resp1.Text);
  Dmod.QrUpdU.Params[137].AsString  := Cliente3;
  Dmod.QrUpdU.Params[138].AsString  := Cliente4;
  Dmod.QrUpdU.Params[139].AsInteger := Geral.IMV(EdCarteira.Text);
  Dmod.QrUpdU.Params[140].AsInteger := Geral.IMV(EdRolComis.Text);
  Dmod.QrUpdU.Params[141].AsInteger := Geral.IMV(EdFilial.Text);
  Dmod.QrUpdU.Params[142].AsString  := Fornece7;
  Dmod.QrUpdU.Params[143].AsString  := Fornece8;
  //
  Dmod.QrUpdU.Params[144].AsInteger := CodUsu;
  Dmod.QrUpdU.Params[145].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[146].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[147].AsInteger := Codigo;
  //
  Dmod.QrUpdU.ExecSQL;
  FmEntidades.ImgTipo.SQLType := stLok;
  // Dono do programa:
  if CkDono.Checked then
  begin
    if RGTipo_2.ItemIndex = 0 then Dono := Geral.FormataCNPJ_TFT(EdCNPJ.Text)
    else Dono := Geral.FormataCNPJ_TFT(EdCPF.Text);
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('UPDATE controle SET Dono=:P1, CNPJ=:P2');
    Dmod.QrUpdU.Params[00].AsInteger := Codigo;
    Dmod.QrUpdU.Params[01].AsString := Dono;
    Dmod.QrUpdU.ExecSQL;
    DmodG.ReopenMaster('TFmEntidadesNew.1016');
  end;
  UMyMod.UpdUnlockY(FmEntidades.QrEntidadesCodigo.Value, Dmod.MyDB, 'Entidades', 'Codigo');
  ImgTipo.SQLType := stLok;
  VAR_MUDOUENTIDADES := True;
  ////////////////////////////////////////////////
  VAR_ENTIDADE := Codigo;
  FmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1, Codigo, False);
  ////////////////////////////////////////////////
  Close;
  Screen.Cursor := crDefault;
end;

procedure TFmEntidadesNew.BtDesisteClick(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Entidades', Geral.IMV(EdCodigo.Text))
  else
     UMyMod.UpdUnlockY(FmEntidades.QrEntidadesCodigo.Value, Dmod.MyDB, 'Entidades', 'Codigo');
  FmEntidades.ImgTipo.SQLType := stLok;
  ImgTipo.SQLType := stLok;
  Close;
end;

procedure TFmEntidadesNew.EdTelefoneExit(Sender: TObject);
begin
  //EdTelefone.EditMask := Geral.FormataTelefone(Length(EdTelefone.Text));
end;

procedure TFmEntidadesNew.FormActivate(Sender: TObject);
begin
  EdEEmail.CharCase := ecNormal;
  EdPEmail.CharCase := ecNormal;
  MyObjects.CorIniComponente();
  MyObjects.UperCaseComponente;
  EdPEmail.CharCase := ecNormal;
  EdEEmail.CharCase := ecNormal;
  PageControl1.Hide;
  PageControl1.Show;
  case VAR_ACTIVEPAGE of
    1: PageControl1.ActivePage := TabSheet1;
    2: PageControl1.ActivePage := TabSheet2;
    3: PageControl1.ActivePage := TabSheet3;
    4: PageControl1.ActivePage := TabSheet4;
  end;
  case VAR_ACTIVEPAGE of
    1: EdNome.SetFocus;
    2: EdRazao.SetFocus;
    3: CkCliente1_2.SetFocus;
    4: Memo.SetFocus;
  end;
  case VAR_ACTIVEPAGE of
    1: //Pessoa 
    begin
      RGTipo_0.ItemIndex := 1;
      RGTipo_1.ItemIndex := 1;
      RGTipo_2.ItemIndex := 1;
    end;
    2: //Empresa
    begin
      RGTipo_0.ItemIndex := 0;
      RGTipo_1.ItemIndex := 0;
      RGTipo_2.ItemIndex := 0;
    end;
  end;
(*  EdTelefone.EditMask := Geral.FormataTelefone(Length(EdTelefone.Text));
  EdFax.EditMask := Geral.FormataTelefone(Length(EdFax.Text));
  EdCelular.EditMask := Geral.FormataTelefone(Length(EdCelular.Text));
  EdCNPJ.Text := Geral.FormataCNPJ_TT(EdCNPJ.Text);*)
  FOnCreate := False;
end;

////Pessoal

procedure TFmEntidadesNew.EdCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := Geral.CalculaCNPJCPF(CPF);
    if Geral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MB_Aviso(SMLA_NUMEROINVALIDO2);
      EdCPF.SetFocus;
    end else begin
      EdCPF.Text := Geral.FormataCNPJ_TT(CPF);
      Entities.CNPJCPFDuplicado(EdCodigo.ValueVariant, 1, EdCNPJ.Text, EdCPF.Text, ImgTipo.SQLType);
    end;
  end else EdCPF.Text := CO_VAZIO;
end;

procedure TFmEntidadesNew.EdPCEPEnter(Sender: TObject);
begin
  FPCEP := Geral.SoNumero_TT(EdPCEP.Text);
end;

procedure TFmEntidadesNew.EdPCEPExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdPCEP.Text);
  if CEP <> FPCEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdPCEP, EdPLograd, EdPRua, EdPNumero, EdPBairro,
        EdPCidade, EdPUF, EdPPais, EdPNumero, EdPCompl, CBPLograd,
        nil, nil, nil, nil);
  end;
end;

procedure TFmEntidadesNew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PnPessoal01.BevelOuter := bvNone;
  PnPessoal02.BevelOuter := bvNone;
  PnPessoal03.BevelOuter := bvNone;
  PnPessoal04.BevelOuter := bvNone;
  PnPessoal05.BevelOuter := bvNone;
  //
  FOnCreate := True;
  FEntTipo  := uetNenhum;
  //
  Entities.ConfiguraTipoDeCadastro(CkCliente1_0, CkCliente2_0, CkCliente3_0,
    CkCliente4_0, CkFornece1_0, CkFornece2_0, CkFornece3_0, CkFornece4_0,
    CkFornece5_0, CkFornece6_0, CkFornece7_0, CkFornece8_0, CkTerceiro_0);
  //
  Entities.ConfiguraTipoDeCadastro(CkCliente1_1, CkCliente2_1, CkCliente3_1,
    CkCliente4_1, CkFornece1_1, CkFornece2_1, CkFornece3_1, CkFornece4_1,
    CkFornece5_1, CkFornece6_1, CkFornece7_1, CkFornece8_1, CkTerceiro_1);
  //
  Entities.ConfiguraTipoDeCadastro(CkCliente1_2, CkCliente2_2, CkCliente3_2,
    CkCliente4_2, CkFornece1_2, CkFornece2_2, CkFornece3_2, CkFornece4_2,
    CkFornece5_2, CkFornece6_2, CkFornece7_2, CkFornece8_2, CkTerceiro_2);
  //
  try
    Dmod.ReopenControle();
    if (Geral.IMV(EdCodigo.Text) = 1)
    and (QrControleDono.Value=FmEntidades.QrEntidadesCodigo.Value) then
    begin
      EdCNPJ.ReadOnly := True;
      EdCPF.ReadOnly := True;
      EdCNPJ.TabStop := False;
      EdCPF.TabStop := False;
    end;
    QrCarteiras.Close;
    UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  except
    raise;
    ZZTerminate := True;
    Application.Terminate;
 end;
 //
 UnDmkDAC_PF.AbreQuery(QrUFs, Dmod.MyDB);
 UnDmkDAC_PF.AbreQuery(QrEntiGrupos, Dmod.MyDB);
 UnDmkDAC_PF.AbreQuery(QrAccounts, Dmod.MyDB);
 UnDmkDAC_PF.AbreQuery(QrMotivos, Dmod.MyDB);
 UnDmkDAC_PF.AbreQuery(QrListaLograd, Dmod.MyDB);
 UnDmkDAC_PF.AbreQuery(QrEmpresas, Dmod.MyDB);
 UnDmkDAC_PF.AbreQuery(QrEstCivil, Dmod.MyDB);
 UnDmkDAC_PF.AbreQuery(QrEquiCom, Dmod.MyDB);
 PageControl1.Visible := False;
 PageControl1.Visible := True;
 //
 case VAR_ACTIVEPAGE of
   1: PageControl1.ActivePage := TabSheet1;
   2: PageControl1.ActivePage := TabSheet2;
   3: PageControl1.ActivePage := TabSheet3;
   4: PageControl1.ActivePage := TabSheet4;
   5: PageControl1.ActivePage := TabSheet5;
   6: PageControl1.ActivePage := TabSheet6;
   else PageControl1.ActivePage := TabSheet1;
 end;
 PainelEntiCreds1.Visible := VAR_ENTICREDS;
 (*if VAR_PARADOX_CONNECTED then PainelEntiCreds2.Visible := True
 else PainelEntiCreds2.Visible := False;*)
end;

procedure TFmEntidadesNew.EdCNPJExit(Sender: TObject);
var
  Num : String;
  CNPJ : String;
begin
  CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
  if CNPJ <> CO_VAZIO then
    begin
    Num := Geral.CalculaCNPJCPF(CNPJ);
    if Geral.FormataCNPJ_TFT(CNPJ) <> Num then
    begin
      Geral.MB_Aviso(SMLA_NUMEROINVALIDO2);
      EdCNPJ.SetFocus;
    end else begin
      EdCNPJ.Text := Geral.FormataCNPJ_TT(CNPJ);
      Entities.CNPJCPFDuplicado(EdCodigo.ValueVariant, 0, EdCNPJ.Text, EdCPF.Text, ImgTipo.SQLType);
    end;
  end else EdCNPJ.Text := CO_VAZIO;
end;

procedure TFmEntidadesNew.EdECEPEnter(Sender: TObject);
begin
  FECEP := Geral.SoNumero_TT(EdECEP.Text);
end;

procedure TFmEntidadesNew.EdECEPExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdECEP.Text);
  if CEP <> FECEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdECEP, EdELograd, EdERua, EdENumero, EdEBairro,
        EdECidade, EdEUF, EdEPais, EdENumero, EdECompl, CBPLograd,
        nil, nil, nil, nil);
  end;
end;

procedure TFmEntidadesNew.EdDiaReciboExit(Sender: TObject);
begin
  EdDiaRecibo.Text := Geral.TFT_MinMax(EdDiaRecibo.Text, 0, 31, 0);
end;

procedure TFmEntidadesNew.RGReciboClick(Sender: TObject);
begin
  if RGRecibo.ItemIndex = 3 then EdDiaRecibo.Enabled := True
  else EdDiaRecibo.Enabled := False;
end;

procedure TFmEntidadesNew.BtCEP1Click(Sender: TObject);
begin
  MostraPMCEP(BtCEP1);
  EdPNumero.SetFocus;
end;

procedure TFmEntidadesNew.SpeedButton1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidadesLoc, FmEntidadesLoc, afmoNegarComAviso) then
  begin
    FmEntidadesLoc.ShowModal;
    if VAR_ENTIDADE <> 0 then
    begin
      QrLoc.Close;
      QrLoc.Params[0].AsInteger := VAR_ENTIDADE;
      UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
      //
      EdRazao.Text       := QrLocRazaoSocial.Value;
      EdFantasia.Text    := QrLocFantasia.Value;
      EdRespons1.Text    := QrLocRespons1.Value;
      EdRespons2.Text    := QrLocRespons2.Value;
      EdCNPJ.Text        := QrLocCNPJ_TXT.Value;
      EdIE.Text          := QrLocIE.Value;
      EdERua.Text        := QrLocERua.Value;
      EdENumero.Text     := IntToStr(QrLocENumero.Value);
      EdECompl.Text      := QrLocECompl.Value;
      EdEBairro.Text     := QrLocEBairro.Value;
      EdECidade.Text     := QrLocECidade.Value;
      EdEUF.ValueVariant := QrLocEUF.Value;
      EdECEP.Text        :=Geral.FormataCEP_NT(QrLocECEP.Value);
      EdEPais.Text       := QrLocEPais.Value;
      EdETe1.Text        := QrLocETE1_TXT.Value;
      EdETe2.Text        := QrLocETE2_TXT.Value;
      EdETe3.Text        := QrLocETE3_TXT.Value;
      EdECel.Text        := QrLocECEL_TXT.Value;
      EdEFax.Text        := QrLocEFAX_TXT.Value;
      EdEEmail.Text      := QrLocEEMail.Value;
      EdEContato.Text    := QrLocEContato.Value;
      TPENatal.Date      := QrLocENatal.Value;
    end;
    FmEntidadesLoc.Destroy;
  end;
end;

procedure TFmEntidadesNew.SpeedButton2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidadesLoc, FmEntidadesLoc, afmoNegarComAviso) then
  begin
    FmEntidadesLoc.ShowModal;
    if VAR_ENTIDADE <> 0 then
    begin
      QrLoc.Close;
      QrLoc.Params[0].AsInteger := VAR_ENTIDADE;
      UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
      //
      EdPLograd.ValueVariant := QrLocPLograd.Value;
      CBPLograd.KeyValue     := QrLocPLograd.Value;
      EdPRua.Text            := QrLocPRua.Value;
      EdPNumero.Text         := IntToStr(QrLocPNumero.Value);
      EdPCompl.Text          := QrLocPCompl.Value;
      EdPBairro.Text         := QrLocPBairro.Value;
      EdPCidade.Text         := QrLocPCidade.Value;
      EdPUF.ValueVariant     := QrLocPUF.Value;
      EdPCEP.Text            :=Geral.FormataCEP_NT(QrLocPCEP.Value);
      EdPPais.Text           := QrLocPPais.Value;
    end;
    FmEntidadesLoc.Destroy;
  end;
end;

procedure TFmEntidadesNew.EdAjudaVExit(Sender: TObject);
begin
  if Geral.IMV(EdAjudaV.Text) > 0 then EdAjudaP.Text := Geral.FFT(0, 4, siPositivo);
end;

procedure TFmEntidadesNew.EdAjudaPExit(Sender: TObject);
begin
  if Geral.IMV(EdAjudaP.Text) > 0 then EdAjudaV.Text := Geral.FFT(0, 2, siPositivo);
end;

procedure TFmEntidadesNew.QrLocCalcFields(DataSet: TDataSet);
begin
  QrLocETE1_TXT.Value := Geral.FormataTelefone_TT(QrLocETe1.Value);
  QrLocETE2_TXT.Value := Geral.FormataTelefone_TT(QrLocETe2.Value);
  QrLocETE3_TXT.Value := Geral.FormataTelefone_TT(QrLocETe3.Value);
  QrLocEFAX_TXT.Value := Geral.FormataTelefone_TT(QrLocEFax.Value);
  QrLocECEL_TXT.Value := Geral.FormataTelefone_TT(QrLocECel.Value);
  QrLocCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrLocCNPJ.Value);
  //
end;

procedure TFmEntidadesNew.BtIncluiClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiTransp, FmEntiTransp, afmoNegarComAviso) then
  begin
    FmEntiTransp.FCodigo := EdCodigo.ValueVariant;
    FmEntiTransp.ShowModal;
    FmEntiTransp.Destroy;
    //
    ReopenEntiTransp;
  end;
end;

procedure TFmEntidadesNew.BtExcluiClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o desta transportadora desta entidade?') = ID_YES then
  begin
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add(DELETE_FROM + ' entitransp WHERE Codigo=:P0');
    Dmod.QrUpdM.SQL.Add('AND Conta=:P0 AND Ordem=:P2');
    Dmod.QrUpdM.Params[0].AsInteger := QrEntiTranspCodigo.Value;
    Dmod.QrUpdM.Params[1].AsInteger := QrEntiTranspConta.Value;
    Dmod.QrUpdM.Params[2].AsInteger := QrEntiTranspOrdem.Value;
    Dmod.QrUpdM.ExecSQL;
    ReopenEntiTransp;
  end;
end;

procedure TFmEntidadesNew.ReopenEntiTransp;
begin
  QrEntiTransp.Close;
  QrEntiTransp.Params[0].AsInteger := Geral.IMV(EdCodigo.Text);
  UnDmkDAC_PF.AbreQuery(QrEntiTransp, Dmod.MyDB);
  //
  if (ImgTipo.SQLType = stIns) and (QrEntiTransp.RecordCount > 0) then
    BtDesiste.Enabled := False
  else
    BtDesiste.Enabled := True;
end;

procedure TFmEntidadesNew.ReopenEntiCtas;
begin
  QrEntiCtas.Close;
  QrEntiCtas.Params[0].AsInteger := Geral.IMV(EdCodigo.Text);
  UnDmkDAC_PF.AbreQuery(QrEntiCtas, Dmod.MyDB);

  if (ImgTipo.SQLType = stIns) and (QrEntiCtas.RecordCount > 0) then
    BtDesiste.Enabled := False
  else
    BtDesiste.Enabled := True;
end;

procedure TFmEntidadesNew.EdCodigoChange(Sender: TObject);
begin
  ReopenEntiTransp;
  ReopenEntiCtas;
end;

procedure TFmEntidadesNew.EdCCEPEnter(Sender: TObject);
begin
  FCCEP := Geral.SoNumero_TT(EdCCEP.Text);
end;

procedure TFmEntidadesNew.EdCCEPExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdCCEP.Text);
  if CEP <> FCCEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdCCEP, EdCLograd, EdCRua, EdCNumero, EdCBairro,
        EdCCidade, EdCUF, EdCPais, EdCNumero, EdCCompl, CBPLograd,
        nil, nil, nil, nil);
  end;
end;

procedure TFmEntidadesNew.EdLCEPEnter(Sender: TObject);
begin
  FLCEP := Geral.SoNumero_TT(EdLCEP.Text);
end;

procedure TFmEntidadesNew.EdLCEPExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdLCEP.Text);
  if CEP <> FLCEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdLCEP, EdLLograd, EdLRua, EdLNumero, EdLBairro,
        EdLCidade, EdLUF, EdLPais, EdLNumero, EdLCompl, CBPLograd,
        nil, nil, nil, nil);
  end;
end;

procedure TFmEntidadesNew.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
  if PageControl1.ActivePage = TabSheet3 then VAR_ACTIVEPAGE := 3
  else if PageControl1.ActivePage = TabSheet4 then VAR_ACTIVEPAGE := 4
  else if PageControl1.ActivePage = TabSheet5 then VAR_ACTIVEPAGE := 5
  else if PageControl1.ActivePage = TabSheet6 then VAR_ACTIVEPAGE := 6
  else if RGTipo_2.ItemIndex = 1 then VAR_ACTIVEPAGE := 1 else VAR_ACTIVEPAGE := 2;
  if Action = caFree then
  begin
    QrUFs.Close;
    QrEntiGrupos.Close;
    QrAccounts.Close;
    QrMotivos.Close;
end;

end;

procedure TFmEntidadesNew.BtCEP3Click(Sender: TObject);
begin
  MostraPMCEP(BtCEP3);
  EdCNumero.SetFocus;
end;

procedure TFmEntidadesNew.MostraPMCEP(Botao: TBitBtn);
{
var
  Lista: TStringList;
  i: Integer;
  Achou: Boolean;
}  
begin
  {
  if VAR_REGEDIT_MyCEP then
  begin
    Geral.MB_Aviso('Foi configurada a base de dados de pesquisa'+
    ' de CEP. � necess�rio reiniciar o aplicativo!');
    Exit;
  end;
  Achou := False;
  Lista := TStringList.Create;
  Session.GetDatabaseNames(Lista);
  if Lista.Count > 0 then
  begin
    for i := 0 to Lista.Count-1 do
      if Lista[i] = 'MyCEP' then
      begin
        Achou := True;
        Break;
      end;
  end;
  if not Achou then
  begin
    Geral.WriteAppKey_Total('Driver', '\Software\ODBC\ODBC.INI\MyCEP',
    'C:\WINDOWS\system32\odbcjt32.dll', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('DBQ', '\Software\ODBC\ODBC.INI\MyCEP',
    'C:\Arquivos de programas\ECT\GPBE\dbgpb.mdb', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('Description', '\Software\ODBC\ODBC.INI\MyCEP',
    'Consulta de CEP', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('DriverId', '\Software\ODBC\ODBC.INI\MyCEP',
    25, ktInteger, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('FIL', '\Software\ODBC\ODBC.INI\MyCEP',
    'MS Access;', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('PWD', '\Software\ODBC\ODBC.INI\MyCEP',
    '�����в�ޢ����', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('SafeTransactions', '\Software\ODBC\ODBC.INI\MyCEP',
    0, ktInteger, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('UID', '\Software\ODBC\ODBC.INI\MyCEP',
    '', ktString, HKEY_CURRENT_USER);
    //
    Geral.WriteAppKey_Total('ImplicitCommitSync', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    '', ktString, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('MaxBufferSize', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    2048, ktInteger, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('PageTimeout', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    5, ktInteger, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('Threads', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    3, ktInteger, HKEY_CURRENT_USER);
    Geral.WriteAppKey_Total('UserCommitSync', '\Software\ODBC\ODBC.INI\MyCEP\Engines\JET',
    'Yes', ktString, HKEY_CURRENT_USER);
    //
    Geral.WriteAppKey_Total('MyCEP', '\Software\ODBC\ODBC.INI\ODBC Data Sources',
    'Driver do Microsoft Access (*.mdb)', ktString, HKEY_CURRENT_USER);
    VAR_REGEDIT_MyCEP := True;
  end;
  if VAR_REGEDIT_MyCEP then
  begin
    Geral.MB_Aviso('Foi configurada a base de dados de pesquisa'+
    ' de CEP. � necess�rio reiniciar o aplicativo!');
    Exit;
  end;
  }
  if PageControl1.ActivePage = TabSheet1 then VAR_ACTIVEPAGE := 1
  else if PageControl1.ActivePage = TabSheet2 then VAR_ACTIVEPAGE := 2
  else if PageControl1.ActivePage = TabSheet3 then VAR_ACTIVEPAGE := 3
  else if PageControl1.ActivePage = TabSheet4 then VAR_ACTIVEPAGE := 4
  else if PageControl1.ActivePage = TabSheet5 then VAR_ACTIVEPAGE := 5
  else if PageControl1.ActivePage = TabSheet6 then VAR_ACTIVEPAGE := 6
  else if RGTipo_2.ItemIndex = 1 then VAR_ACTIVEPAGE := 1 else VAR_ACTIVEPAGE := 2;
  //
  MyObjects.MostraPMCEP(PMCEP, Botao);
end;

procedure TFmEntidadesNew.Descobrir1Click(Sender: TObject);
begin
{$IfDef cDbTable}
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    FmEntiCEP.DBGrid2.Visible := False;
    FmEntiCEP.Painel2.Visible := False;
    FmEntiCEP.ShowModal;
    FmEntiCEP.Destroy;
  end;
{$Else}
  //    TODO Berlin
  Geral.MB_Info('Solicite implementa��o � Dermatek! (DBE Table)');
{$EndIf}
end;

procedure TFmEntidadesNew.VerificaNomesParecidos();
var
  Nome: String;
begin
  case PageControl1.ActivePageIndex of
    0: Nome := EdNome.Text;
    1: Nome := EdRazao.Text;
    else Nome := '';
  end;
  //
  QrSom.Close;
  QrSom.Params[0].AsString := Nome;
  UnDmkDAC_PF.AbreQuery(QrSom, Dmod.MyDB);
  //
  if Nome <> '' then
  Nome := '%' + Geral.Substitui(Nome, ' ', '%') + '%';
  //
  QrNomes.Close;
  QrNomes.Params[0].AsString := Nome;
  UnDmkDAC_PF.AbreQuery(QrNomes, Dmod.MyDB);
end;

procedure TFmEntidadesNew.Verificar1Click(Sender: TObject);
begin
{$IfDef cDbTable}
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    with FmEntiCEP do
    begin
      DBGrid1.Visible := False;
      Painel1.Visible := False;
      case VAR_ACTIVEPAGE of
        1: EdCEPLoc.Text := EdPCEP.Text;
        2: EdCEPLoc.Text := EdECEP.Text;
        5: EdCEPLoc.Text := EdCCEP.Text;
        6: EdCEPLoc.Text := EdLCEP.Text;
      end;
      ShowModal;
      Destroy;
    end;
  end;
{$Else}
  Geral.MB_Info('Solicite implementa��o � Dermatek! (DBE Table)');  //  TODO Berlin
{$EndIf}
end;

procedure TFmEntidadesNew.PageControl1Change(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: //Pessoal
    begin
      RGTipo_0.ItemIndex := 1;
      RGTipo_1.ItemIndex := 1;
      RGTipo_2.ItemIndex := 1;
    end;
    1: //Empresa
    begin
      RGTipo_0.ItemIndex := 0;
      RGTipo_1.ItemIndex := 0;
      RGTipo_2.ItemIndex := 0;
    end
  end;
  if PageControl1.ActivePageIndex = 1 then
    VerificaNomesParecidos();
end;

procedure TFmEntidadesNew.EdNomeChange(Sender: TObject);
begin
  EdNome_2.Text := EdNome.Text;
  if PageControl1.ActivePageIndex = 0 then
    VerificaNomesParecidos();
end;

procedure TFmEntidadesNew.EdRazaoChange(Sender: TObject);
begin
  EdRazao2.Text := EdRazao.Text;
  if PageControl1.ActivePageIndex = 1 then
    VerificaNomesParecidos();
end;

procedure TFmEntidadesNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidadesNew.FormShow(Sender: TObject);
begin
  ConfiguraTipoEntidade(FEntTipo);
end;

procedure TFmEntidadesNew.AtualizaTipos;
var
  Cliente1, Cliente2, Cliente3, Cliente4, Fornece1, Fornece2, Fornece3,
  Fornece4, Fornece5, Fornece6, Fornece7, Fornece8, Terceiro: TCheckBox;
  Tipo: TRadioGroup;
begin
  if (not FOnCreate) and (not FOnClick) then
  begin
    FOnClick := True;
    if PageControl1.ActivePage = TabSheet1 then
    begin
      Cliente1 := CkCliente1_1;
      Cliente2 := CkCliente2_1;
      Cliente3 := CkCliente3_1;
      Cliente4 := CkCliente4_1;
      Fornece1 := CkFornece1_1;
      Fornece2 := CkFornece2_1;
      Fornece3 := CkFornece3_1;
      Fornece4 := CkFornece4_1;
      Fornece5 := CkFornece5_1;
      Fornece6 := CkFornece6_1;
      Fornece7 := CkFornece7_1;
      Fornece8 := CkFornece8_1;
      Terceiro := CkTerceiro_1;
      Tipo := RGTipo_1;
    end else if PageControl1.ActivePage = TabSheet2 then
    begin
      Cliente1 := CkCliente1_0;
      Cliente2 := CkCliente2_0;
      Cliente3 := CkCliente3_0;
      Cliente4 := CkCliente4_0;
      Fornece1 := CkFornece1_0;
      Fornece2 := CkFornece2_0;
      Fornece3 := CkFornece3_0;
      Fornece4 := CkFornece4_0;
      Fornece5 := CkFornece5_0;
      Fornece6 := CkFornece6_0;
      Fornece7 := CkFornece7_0;
      Fornece8 := CkFornece8_0;
      Terceiro := CkTerceiro_0;
      Tipo := RGTipo_0;
    end else
    begin
      Cliente1 := CkCliente1_2;
      Cliente2 := CkCliente2_2;
      Cliente3 := CkCliente3_2;
      Cliente4 := CkCliente4_2;
      Fornece1 := CkFornece1_2;
      Fornece2 := CkFornece2_2;
      Fornece3 := CkFornece3_2;
      Fornece4 := CkFornece4_2;
      Fornece5 := CkFornece5_2;
      Fornece6 := CkFornece6_2;
      Fornece7 := CkFornece7_2;
      Fornece8 := CkFornece8_2;
      Terceiro := CkTerceiro_2;
      Tipo := RGTipo_2;
    end;
    if (Uppercase(TMeuDB) = 'EXULT')
    or (Uppercase(TMeuDB) = 'EXUL2') then
    begin
      if Cliente2.Checked then
      begin
        Cliente1.Checked := False;
        Fornece3.Checked := False;
        Fornece4.Checked := False;
      end;
    end;
    CkCliente1_1.Checked := Cliente1.Checked;
    CkCliente2_1.Checked := Cliente2.Checked;
    CkCliente3_1.Checked := Cliente3.Checked;
    CkCliente4_1.Checked := Cliente4.Checked;
    CkFornece1_1.Checked := Fornece1.Checked;
    CkFornece2_1.Checked := Fornece2.Checked;
    CkFornece3_1.Checked := Fornece3.Checked;
    CkFornece4_1.Checked := Fornece4.Checked;
    CkFornece5_1.Checked := Fornece5.Checked;
    CkFornece6_1.Checked := Fornece6.Checked;
    CkFornece7_1.Checked := Fornece7.Checked;
    CkFornece8_1.Checked := Fornece8.Checked;
    CkTerceiro_1.Checked := Terceiro.Checked;
    CkCliente1_0.Checked := Cliente1.Checked;
    CkCliente2_0.Checked := Cliente2.Checked;
    CkCliente3_0.Checked := Cliente3.Checked;
    CkCliente4_0.Checked := Cliente4.Checked;
    CkFornece1_0.Checked := Fornece1.Checked;
    CkFornece2_0.Checked := Fornece2.Checked;
    CkFornece3_0.Checked := Fornece3.Checked;
    CkFornece4_0.Checked := Fornece4.Checked;
    CkFornece5_0.Checked := Fornece5.Checked;
    CkFornece6_0.Checked := Fornece6.Checked;
    CkFornece7_0.Checked := Fornece7.Checked;
    CkFornece8_0.Checked := Fornece8.Checked;
    CkTerceiro_0.Checked := Terceiro.Checked;
    CkCliente1_2.Checked := Cliente1.Checked;
    CkCliente2_2.Checked := Cliente2.Checked;
    CkCliente3_2.Checked := Cliente3.Checked;
    CkCliente4_2.Checked := Cliente4.Checked;
    CkFornece1_2.Checked := Fornece1.Checked;
    CkFornece2_2.Checked := Fornece2.Checked;
    CkFornece3_2.Checked := Fornece3.Checked;
    CkFornece4_2.Checked := Fornece4.Checked;
    CkFornece5_2.Checked := Fornece5.Checked;
    CkFornece6_2.Checked := Fornece6.Checked;
    CkFornece7_2.Checked := Fornece7.Checked;
    CkFornece8_2.Checked := Fornece8.Checked;
    CkTerceiro_2.Checked := Terceiro.Checked;
    RGTipo_0.ItemIndex := Tipo.ItemIndex;
    RGTipo_1.ItemIndex := Tipo.ItemIndex;
    RGTipo_2.ItemIndex := Tipo.ItemIndex;
    FOnClick := False;
  end;
end;

procedure TFmEntidadesNew.CkCliente1_1Click(Sender: TObject);
begin
  AtualizaTipos;
end;

procedure TFmEntidadesNew.CkCliente1_0Click(Sender: TObject);
begin
  AtualizaTipos;
end;

procedure TFmEntidadesNew.CkCliente1_2Click(Sender: TObject);
begin
  AtualizaTipos;
end;

procedure TFmEntidadesNew.ConfiguraTipoEntidade(EntTipo: TUnEntTipo);
begin
  case EntTipo of
    uetNenhum:
      ;
    uetCliente1:
    begin
      CkCliente1_0.Checked := True;
      CkCliente1_1.Checked := True;
    end;
    uetCliente2:
    begin
      CkCliente2_0.Checked := True;
      CkCliente2_1.Checked := True;
    end;
    uetCliente3:
    begin
      CkCliente3_0.Checked := True;
      CkCliente3_1.Checked := True;
    end;
    uetCliente4:
    begin
      CkCliente4_0.Checked := True;
      CkCliente4_1.Checked := True;
    end;
    uetFornece1:
    begin
      CkFornece1_0.Checked := True;
      CkFornece1_1.Checked := True;
    end;
    uetFornece2:
    begin
      CkFornece2_0.Checked := True;
      CkFornece2_1.Checked := True;
    end;
    uetFornece3:
    begin
      CkFornece3_0.Checked := True;
      CkFornece3_1.Checked := True;
    end;
    uetFornece4:
    begin
      CkFornece4_0.Checked := True;
      CkFornece4_1.Checked := True;
    end;
    uetFornece5:
    begin
      CkFornece5_0.Checked := True;
      CkFornece5_1.Checked := True;
    end;
    uetFornece6:
    begin
      CkFornece6_0.Checked := True;
      CkFornece6_1.Checked := True;
    end;
    uetFornece7:
    begin
      CkFornece7_0.Checked := True;
      CkFornece7_1.Checked := True;
    end;
    uetFornece8:
    begin
      CkFornece8_0.Checked := True;
      CkFornece8_1.Checked := True;
    end;
    uetTerceiro:
    begin
      CkTerceiro_0.Checked := True;
      CkTerceiro_1.Checked := True;
    end;
  end;
end;

procedure TFmEntidadesNew.EdCPF_PaiExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF_Pai.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := Geral.CalculaCNPJCPF(CPF);
    if Geral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MB_Aviso(SMLA_NUMEROINVALIDO2);
      EdCPF_Pai.SetFocus;
    end else EdCPF_Pai.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF_Pai.Text := CO_VAZIO;
end;

procedure TFmEntidadesNew.EdPPaisEnter(Sender: TObject);
begin
  if Trim(EdPPais.Text) = '' then EdPPais.Text := 'BRASIL';
end;

procedure TFmEntidadesNew.EdEPaisEnter(Sender: TObject);
begin
  if Trim(EdEPais.Text) = '' then EdEPais.Text := 'BRASIL';
end;

procedure TFmEntidadesNew.EdEUFChange(Sender: TObject);
begin
  EdIE.LinkMsk := EdEUF.Text;
end;

procedure TFmEntidadesNew.EdCPF_ConjugeExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF_Conjuge.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := Geral.CalculaCNPJCPF(CPF);
    if Geral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MB_Aviso(SMLA_NUMEROINVALIDO2);
      EdCPF_Conjuge.SetFocus;
    end else EdCPF_Conjuge.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF_Conjuge.Text := CO_VAZIO;
end;

procedure TFmEntidadesNew.EdCPF_Resp1Exit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF_Resp1.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := Geral.CalculaCNPJCPF(CPF);
    if Geral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MB_Aviso(SMLA_NUMEROINVALIDO2);
      EdCPF_Resp1.SetFocus;
    end else EdCPF_Resp1.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF_Resp1.Text := CO_VAZIO;
end;

procedure TFmEntidadesNew.BtIncluiCtasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiCtas, FmEntiCtas, afmoNegarComAviso) then
  begin
    FmEntiCtas.ImgTipo.SQLType := stIns;
    FmEntiCtas.EdCodigo.Text  := EdCodigo.Text;
    FmEntiCtas.ShowModal;
    FmEntiCtas.Destroy;
    ReopenEntiCtas;
  end;
end;

procedure TFmEntidadesNew.EdCEP4Click(Sender: TObject);
begin
  MostraPMCEP(BtCEP2);
  EdENumero.SetFocus;
end;

procedure TFmEntidadesNew.BtAlteraCtasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiCtas, FmEntiCtas, afmoNegarComAviso) then
  begin
    FmEntiCtas.ImgTipo.SQLType                := stUpd;
    FmEntiCtas.EdCodigo.Text                 := IntToStr(QrEntiCtasCodigo.Value);
    FmEntiCtas.EdControle.Text               := IntToStr(QrEntiCtasControle.Value);
    FmEntiCtas.EdCedBanco.ValueVariant       := IntToStr(QrEntiCtasBanco.Value);
    FmEntiCtas.EdOrdem.Text                  := IntToStr(QrEntiCtasOrdem.Value);
    FmEntiCtas.EdCedAgencia.ValueVariant     := IntToStr(QrEntiCtasAgencia.Value);
    FmEntiCtas.EdCedDAC_A.ValueVariant       := QrEntiCtasDAC_A.Value;
    FmEntiCtas.EdCedConta.ValueVariant       := QrEntiCtasContaCor.Value;
    FmEntiCtas.EdCedDAC_C.ValueVariant       := QrEntiCtasDAC_C.Value;
    FmEntiCtas.EdCedDAC_AC.ValueVariant      := QrEntiCtasDAC_AC.Value;
    FmEntiCtas.EdCedContaTip.ValueVariant    := QrEntiCtasContaTip.Value;
    FmEntiCtas.ShowModal;
    FmEntiCtas.Destroy;
    ReopenEntiCtas;
  end;  
end;

procedure TFmEntidadesNew.BtExcluiCtasClick(Sender: TObject);
var
  Controle: Integer;
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da conta banc�ria"'+
  QrEntiCtasContaCor.Value+'" ?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Controle := QrEntiCtasControle.Value;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + ' entictas WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    ReopenEntiCtas;
    Screen.Cursor := crDefault;
  end;
end;

end.

