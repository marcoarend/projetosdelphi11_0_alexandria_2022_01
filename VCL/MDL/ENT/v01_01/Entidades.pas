unit Entidades;
//{$DEFINE UV_LOAD_ENT1}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ClipBrd, ZCF2, Grids, DBGrids, ResIntStrings, UnMsgInt, UnGOTOy,
  UnInternalConsts2, UnInternalConsts, UMySQLModule, ComCtrls,
  mySQLDbTables, Menus, UnMySQLCuringa, frxClass, frxDBSet, Variants,
  dmkPermissoes, dmkGeral, dmkEdit, UnDmkProcFunc, dmkImage, UnDMkEnums;

type
  TFmEntidades = class(TForm)
    PainelDados: TPanel;
    Panel1: TPanel;
    DsEntidades: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label17: TLabel;
    DBEdit17: TDBEdit;
    Label18: TLabel;
    DBEdit18: TDBEdit;
    Label19: TLabel;
    DBEdit19: TDBEdit;
    Label20: TLabel;
    DBEdit20: TDBEdit;
    Label21: TLabel;
    DBEdit21: TDBEdit;
    Label22: TLabel;
    DBEdit22: TDBEdit;
    Label23: TLabel;
    DBEdit23: TDBEdit;
    Label24: TLabel;
    DBEdit24: TDBEdit;
    TabSheet4: TTabSheet;
    DBMemo1: TDBMemo;
    DBEdit50: TDBEdit;
    OpenPictureDialog1: TOpenPictureDialog;
    DsEntidade: TDataSource;
    TabSheet8: TTabSheet;
    Panel4: TPanel;
    DBImage2: TDBImage;
    TbEntidade: TmySQLTable;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesRazaoSocial: TWideStringField;
    QrEntidadesFantasia: TWideStringField;
    QrEntidadesRespons1: TWideStringField;
    QrEntidadesRespons2: TWideStringField;
    QrEntidadesPai: TWideStringField;
    QrEntidadesMae: TWideStringField;
    QrEntidadesCNPJ: TWideStringField;
    QrEntidadesIE: TWideStringField;
    QrEntidadesNome: TWideStringField;
    QrEntidadesApelido: TWideStringField;
    QrEntidadesCPF: TWideStringField;
    QrEntidadesRG: TWideStringField;
    QrEntidadesERua: TWideStringField;
    QrEntidadesECompl: TWideStringField;
    QrEntidadesEBairro: TWideStringField;
    QrEntidadesECidade: TWideStringField;
    QrEntidadesEUF: TSmallintField;
    QrEntidadesEPais: TWideStringField;
    QrEntidadesETe1: TWideStringField;
    QrEntidadesETe2: TWideStringField;
    QrEntidadesETe3: TWideStringField;
    QrEntidadesECel: TWideStringField;
    QrEntidadesEFax: TWideStringField;
    QrEntidadesEEMail: TWideStringField;
    QrEntidadesEContato: TWideStringField;
    QrEntidadesENatal: TDateField;
    QrEntidadesPRua: TWideStringField;
    QrEntidadesPCompl: TWideStringField;
    QrEntidadesPBairro: TWideStringField;
    QrEntidadesPCidade: TWideStringField;
    QrEntidadesPUF: TSmallintField;
    QrEntidadesPPais: TWideStringField;
    QrEntidadesPTe1: TWideStringField;
    QrEntidadesPTe2: TWideStringField;
    QrEntidadesPTe3: TWideStringField;
    QrEntidadesPCel: TWideStringField;
    QrEntidadesPFax: TWideStringField;
    QrEntidadesPEMail: TWideStringField;
    QrEntidadesPContato: TWideStringField;
    QrEntidadesPNatal: TDateField;
    QrEntidadesSexo: TWideStringField;
    QrEntidadesResponsavel: TWideStringField;
    QrEntidadesProfissao: TWideStringField;
    QrEntidadesCargo: TWideStringField;
    QrEntidadesRecibo: TSmallintField;
    QrEntidadesDiaRecibo: TSmallintField;
    QrEntidadesAjudaEmpV: TFloatField;
    QrEntidadesAjudaEmpP: TFloatField;
    QrEntidadesCliente1: TWideStringField;
    QrEntidadesCliente2: TWideStringField;
    QrEntidadesCliente3: TWideStringField;
    QrEntidadesCliente4: TWideStringField;
    QrEntidadesFornece1: TWideStringField;
    QrEntidadesFornece2: TWideStringField;
    QrEntidadesFornece3: TWideStringField;
    QrEntidadesFornece4: TWideStringField;
    QrEntidadesTerceiro: TWideStringField;
    QrEntidadesCadastro: TDateField;
    QrEntidadesInformacoes: TWideStringField;
    QrEntidadesLogo: TBlobField;
    QrEntidadesVeiculo: TIntegerField;
    QrEntidadesMensal: TWideStringField;
    QrEntidadesObservacoes: TWideMemoField;
    QrEntidadesTipo: TSmallintField;
    QrEntidadesLk: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesNOMEEUF: TWideStringField;
    QrEntidadesNOMEPUF: TWideStringField;
    TbEntidadeCodigo: TIntegerField;
    TbEntidadeLogo: TBlobField;
    QrEntidadesPCPF_TXT: TWideStringField;
    QrEntidadesPTE1_TXT: TWideStringField;
    QrEntidadesPTE2_TXT: TWideStringField;
    QrEntidadesPTE3_TXT: TWideStringField;
    QrEntidadesPCEL_TXT: TWideStringField;
    QrEntidadesPFAX_TXT: TWideStringField;
    QrEntidadesETE1_TXT: TWideStringField;
    QrEntidadesETE2_TXT: TWideStringField;
    QrEntidadesETE3_TXT: TWideStringField;
    QrEntidadesECEL_TXT: TWideStringField;
    QrEntidadesEFAX_TXT: TWideStringField;
    QrEntidadesCNPJ_TXT: TWideStringField;
    GroupBox1: TGroupBox;
    CkCliente1: TDBCheckBox;
    CkFornece1: TDBCheckBox;
    CkFornece2: TDBCheckBox;
    CkFornece3: TDBCheckBox;
    CkTerceiro: TDBCheckBox;
    CkFornece4: TDBCheckBox;
    CkFornece5: TDBCheckBox;
    CkFornece6: TDBCheckBox;
    CkFornece7: TDBCheckBox;
    CkFornece8: TDBCheckBox;
    DBRadioGroup1: TDBRadioGroup;
    GroupBox2: TGroupBox;
    Label53: TLabel;
    Label54: TLabel;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    Label49: TLabel;
    DBEdit49: TDBEdit;
    DBRadioGroup2: TDBRadioGroup;
    CkCliente2: TDBCheckBox;
    Grupo: TLabel;
    DBEdit56: TDBEdit;
    QrEntidadesECEP: TIntegerField;
    QrEntidadesPCEP: TIntegerField;
    QrEntidadesGrupo: TIntegerField;
    QrEntidadesDataAlt: TDateField;
    QrEntidadesUserCad: TSmallintField;
    QrEntidadesUserAlt: TSmallintField;
    QrEntidadesCRua: TWideStringField;
    QrEntidadesCCompl: TWideStringField;
    QrEntidadesCBairro: TWideStringField;
    QrEntidadesCCidade: TWideStringField;
    QrEntidadesCUF: TSmallintField;
    QrEntidadesCCEP: TIntegerField;
    QrEntidadesCPais: TWideStringField;
    QrEntidadesCTel: TWideStringField;
    QrEntidadesCFax: TWideStringField;
    QrEntidadesCCel: TWideStringField;
    QrEntidadesCContato: TWideStringField;
    QrEntidadesLRua: TWideStringField;
    QrEntidadesLCompl: TWideStringField;
    QrEntidadesLBairro: TWideStringField;
    QrEntidadesLCidade: TWideStringField;
    QrEntidadesLUF: TSmallintField;
    QrEntidadesLCEP: TIntegerField;
    QrEntidadesLPais: TWideStringField;
    QrEntidadesLTel: TWideStringField;
    QrEntidadesLFax: TWideStringField;
    QrEntidadesLCel: TWideStringField;
    QrEntidadesLContato: TWideStringField;
    QrEntidadesComissao: TFloatField;
    QrEntidadesDataCad: TDateField;
    QrEntidadesECEP_TXT: TWideStringField;
    QrEntidadesPCEP_TXT: TWideStringField;
    QrEntidadesCCEP_TXT: TWideStringField;
    QrEntidadesLCEP_TXT: TWideStringField;
    QrEntidadesNOMECAD2: TWideStringField;
    QrEntidadesNOMEALT2: TWideStringField;
    DsSenhas: TDataSource;
    QrSenhas: TmySQLQuery;
    QrSenhasLogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasSenha: TWideStringField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasLk: TIntegerField;
    QrEntidadesNOMECAD: TWideStringField;
    QrEntidadesNOMEALT: TWideStringField;
    TabSheet5: TTabSheet;
    Label56: TLabel;
    DBEdit57: TDBEdit;
    Label59: TLabel;
    DBEdit59: TDBEdit;
    Label64: TLabel;
    DBEdit64: TDBEdit;
    DBEdit67: TDBEdit;
    Label67: TLabel;
    Label60: TLabel;
    DBEdit60: TDBEdit;
    Label65: TLabel;
    DBEdit65: TDBEdit;
    Label57: TLabel;
    DBEdit58: TDBEdit;
    Label61: TLabel;
    DBEdit61: TDBEdit;
    Label66: TLabel;
    DBEdit66: TDBEdit;
    Label62: TLabel;
    DBEdit62: TDBEdit;
    Label58: TLabel;
    DBEdit63: TDBEdit;
    Label63: TLabel;
    DBEdit68: TDBEdit;
    TabSheet6: TTabSheet;
    Panel6: TPanel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    DBEdit72: TDBEdit;
    DBEdit73: TDBEdit;
    DBEdit75: TDBEdit;
    DBEdit76: TDBEdit;
    DBEdit77: TDBEdit;
    DBEdit78: TDBEdit;
    DBEdit79: TDBEdit;
    DBEdit74: TDBEdit;
    DBEdit80: TDBEdit;
    Panel7: TPanel;
    DBGrid2: TDBGrid;
    QrEntidadesSituacao: TSmallintField;
    QrEntidadesNivel: TWideStringField;
    QrEntidadesCFAX_TXT: TWideStringField;
    QrEntidadesCCEL_TXT: TWideStringField;
    QrEntidadesCTEL_TXT: TWideStringField;
    QrEntidadesLTEL_TXT: TWideStringField;
    QrEntidadesLFAX_TXT: TWideStringField;
    QrEntidadesLCEL_TXT: TWideStringField;
    QrEntidadesNOMEENTIGRUPO: TWideStringField;
    QrEntidadesNOMECUF: TWideStringField;
    QrEntidadesNOMELUF: TWideStringField;
    Panel8: TPanel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    DBEdit81: TDBEdit;
    DBEdit82: TDBEdit;
    DBEntiNome: TDBEdit;
    DBEdit84: TDBEdit;
    DBEdit85: TDBEdit;
    DBEdit86: TDBEdit;
    DBEdit87: TDBEdit;
    QrEntiTransp: TmySQLQuery;
    QrEntiTranspCodigo: TIntegerField;
    QrEntiTranspConta: TIntegerField;
    QrEntiTranspOrdem: TIntegerField;
    QrEntiTranspTransp: TIntegerField;
    DsEntiTransp: TDataSource;
    QrEntiTranspNOMEENTIDADE: TWideStringField;
    PMQuery: TPopupMenu;
    LocalizarClienteporNotaFiscal1: TMenuItem;
    porNotaFiscal1: TMenuItem;
    Produtoquecompra1: TMenuItem;
    LocalizarFornecedorporNoitaFiscal1: TMenuItem;
    porNotaFiscal2: TMenuItem;
    Produtoquevende1: TMenuItem;
    PMImprime: TPopupMenu;
    Pesquisas1: TMenuItem;
    Dados1: TMenuItem;
    LocalizarEntidades1: TMenuItem;
    QrLoc: TmySQLQuery;
    QrLocCodigo: TIntegerField;
    QrLocNOMEENTIDADE: TWideStringField;
    QrLocCNPJ_CPF: TWideStringField;
    QrLocIE_RG: TWideStringField;
    QrLocCONTATO: TWideStringField;
    QrLocRUA: TWideStringField;
    QrLocCompl: TWideStringField;
    QrLocBairro: TWideStringField;
    QrLocCidade: TWideStringField;
    QrLocUF: TLargeintField;
    QrLocCEP: TLargeintField;
    QrLocPais: TWideStringField;
    QrLocNOMEUF: TWideStringField;
    QrLocPTe1: TWideStringField;
    QrLocPTe2: TWideStringField;
    QrLocPTe3: TWideStringField;
    QrLocETe1: TWideStringField;
    QrLocETe2: TWideStringField;
    QrLocETe3: TWideStringField;
    QrLocPFax: TWideStringField;
    QrLocEFax: TWideStringField;
    QrLocPCel: TWideStringField;
    QrLocECel: TWideStringField;
    QrLocCTel: TWideStringField;
    QrLocCCel: TWideStringField;
    QrLocCFax: TWideStringField;
    QrLocLTel: TWideStringField;
    QrLocLCel: TWideStringField;
    QrLocLFax: TWideStringField;
    DsLoc: TDataSource;
    QrCadastro2: TmySQLQuery;
    QrCadastro2Codigo: TIntegerField;
    QrCadastro2Tipo: TSmallintField;
    QrCadastro2NOMEENTIDADE: TWideStringField;
    QrCadastro2FANTASIA_APELIDO: TWideStringField;
    QrCadastro2RUA: TWideStringField;
    QrCadastro2COMPL: TWideStringField;
    QrCadastro2CEP: TLargeintField;
    QrCadastro2BAIRRO: TWideStringField;
    QrCadastro2CIDADE: TWideStringField;
    QrCadastro2UF: TLargeintField;
    QrCadastro2PAIS: TWideStringField;
    QrCadastro2TE1: TWideStringField;
    QrCadastro2TE2: TWideStringField;
    QrCadastro2TE3: TWideStringField;
    QrCadastro2CEL: TWideStringField;
    QrCadastro2FAX: TWideStringField;
    QrCadastro2EMAIL: TWideStringField;
    QrCadastro2CONTATO: TWideStringField;
    QrCadastro2NATAL: TWideStringField;
    QrCadastro2DOCA: TWideStringField;
    QrCadastro2DOCB: TWideStringField;
    QrCadastro2Sexo: TWideStringField;
    QrCadastro2NOMEUF: TWideStringField;
    QrCadastro2CRua: TWideStringField;
    QrCadastro2CCompl: TWideStringField;
    QrCadastro2CCEP: TIntegerField;
    QrCadastro2CBairro: TWideStringField;
    QrCadastro2CCidade: TWideStringField;
    QrCadastro2CUF: TSmallintField;
    QrCadastro2CPais: TWideStringField;
    QrCadastro2LRua: TWideStringField;
    QrCadastro2LCompl: TWideStringField;
    QrCadastro2LCEP: TIntegerField;
    QrCadastro2LBairro: TWideStringField;
    QrCadastro2LCidade: TWideStringField;
    QrCadastro2LUF: TSmallintField;
    QrCadastro2LPais: TWideStringField;
    QrCadastro2TE1_TXT: TWideStringField;
    QrCadastro2TE2_TXT: TWideStringField;
    QrCadastro2TE3_TXT: TWideStringField;
    QrCadastro2FAX_TXT: TWideStringField;
    QrCadastro2CEL_TXT: TWideStringField;
    Dadoscompactosdaentidadeatual1: TMenuItem;
    QrCadastro2DOCA_TXT: TWideStringField;
    QrCadastro2Cargo: TWideStringField;
    QrCadastro2Profissao: TWideStringField;
    QrCadastro2NUM_TXT: TWideStringField;
    QrCadastro2CEP_TXT: TWideStringField;
    QrCadastro2CNUM_TXT: TWideStringField;
    QrCadastro2LNUM_TXT: TWideStringField;
    QrCadastro2NOMECUF: TWideStringField;
    QrCadastro2NOMELUF: TWideStringField;
    QrCadastro2CCEP_TXT: TWideStringField;
    QrCadastro2LCEP_TXT: TWideStringField;
    QrCadastro2NATAL_TXT: TWideStringField;
    QrCadastro2ENatal: TDateField;
    QrCadastro2PNatal: TDateField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    QrEntidadesAccount: TIntegerField;
    QrEntidadesNOMEACCOUNT: TWideStringField;
    Label55: TLabel;
    Label50: TLabel;
    BtImagem: TBitBtn;
    BtImagem1: TBitBtn;
    DBImage1: TDBImage;
    TbEntidadeLogo2: TBlobField;
    DBImage: TDBImage;
    QrEntidadesLogo2: TBlobField;
    DBImage3: TDBImage;
    QrCadastro2Observacoes: TWideMemoField;
    QrEntidadesELograd: TSmallintField;
    QrEntidadesPLograd: TSmallintField;
    QrEntidadesConjugeNome: TWideStringField;
    QrEntidadesConjugeNatal: TDateField;
    QrEntidadesNome1: TWideStringField;
    QrEntidadesNatal1: TDateField;
    QrEntidadesNome2: TWideStringField;
    QrEntidadesNatal2: TDateField;
    QrEntidadesNome3: TWideStringField;
    QrEntidadesNatal3: TDateField;
    QrEntidadesCreditosI: TIntegerField;
    QrEntidadesCreditosL: TIntegerField;
    QrEntidadesCreditosD: TDateField;
    QrEntidadesCreditosU: TDateField;
    QrEntidadesCreditosV: TDateField;
    QrEntidadesMotivo: TIntegerField;
    QrEntidadesQuantI1: TIntegerField;
    QrEntidadesQuantI2: TIntegerField;
    QrEntidadesQuantI3: TIntegerField;
    QrEntidadesQuantI4: TIntegerField;
    QrEntidadesAgenda: TWideStringField;
    QrEntidadesSenhaQuer: TWideStringField;
    QrEntidadesSenha1: TWideStringField;
    QrEntidadesNatal4: TDateField;
    QrEntidadesNome4: TWideStringField;
    Label88: TLabel;
    DBEdit89: TDBEdit;
    Label89: TLabel;
    DBEdit90: TDBEdit;
    Label90: TLabel;
    DBEdit91: TDBEdit;
    Label91: TLabel;
    DBEdit92: TDBEdit;
    Label92: TLabel;
    DBEdit93: TDBEdit;
    Label93: TLabel;
    DBEdit94: TDBEdit;
    Label94: TLabel;
    DBEdit95: TDBEdit;
    Label95: TLabel;
    DBEdit96: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label96: TLabel;
    DBEdit97: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    DBEdit99: TDBEdit;
    Label98: TLabel;
    Label99: TLabel;
    DBEdit100: TDBEdit;
    Label100: TLabel;
    DBEdit101: TDBEdit;
    Label101: TLabel;
    DBEdit102: TDBEdit;
    Label102: TLabel;
    DBEdit103: TDBEdit;
    Label103: TLabel;
    DBEdit104: TDBEdit;
    LaQuantI1: TLabel;
    DBEdit105: TDBEdit;
    QrEntidadesNOMEMOTIVO: TWideStringField;
    DBEdit98: TDBEdit;
    QrEntidadesCreditosF2: TFloatField;
    DBEdit106: TDBEdit;
    Label97: TLabel;
    Label104: TLabel;
    QrEntidadesNOMEELOGRAD: TWideStringField;
    QrEntidadesNOMEPLOGRAD: TWideStringField;
    QrEntidadesCLograd: TSmallintField;
    QrEntidadesLLograd: TSmallintField;
    QrEntidadesNOMECLOGRAD: TWideStringField;
    QrEntidadesNOMELLOGRAD: TWideStringField;
    Label15: TLabel;
    DBEdit108: TDBEdit;
    DBEdit69: TDBEdit;
    Label39: TLabel;
    Label68: TLabel;
    DBEdit109: TDBEdit;
    Nome1: TMenuItem;
    Dados2: TMenuItem;
    QrEntidadesQuantN1: TFloatField;
    QrEntidadesQuantN2: TFloatField;
    QrEntidadesLimiCred: TFloatField;
    DBEdit111: TDBEdit;
    Label108: TLabel;
    Label109: TLabel;
    DBEdit112: TDBEdit;
    QrEntidadesDesco: TFloatField;
    QrEntidadesCasasApliDesco: TSmallintField;
    Label110: TLabel;
    DBEdit113: TDBEdit;
    Label111: TLabel;
    Label112: TLabel;
    DBEdit114: TDBEdit;
    QrEntidadesCPF_Pai: TWideStringField;
    QrEntidadesSSP: TWideStringField;
    QrEntidadesCidadeNatal: TWideStringField;
    Label115: TLabel;
    QrEntidadesCPF_PAI_TXT: TWideStringField;
    QrEntidadesUFNatal: TSmallintField;
    QrEntidadesNOMENUF: TWideStringField;
    QrEntidadesFornece7: TWideStringField;
    QrEntidadesFornece8: TWideStringField;
    QrEntidadesFatorCompra: TFloatField;
    QrEntidadesAdValorem: TFloatField;
    QrEntidadesDMaisC: TIntegerField;
    QrEntidadesDMaisD: TIntegerField;
    TabSheet7: TTabSheet;
    Label107: TLabel;
    DBEdit110: TDBEdit;
    DBEdit115: TDBEdit;
    DBEdit116: TDBEdit;
    DBEdit117: TDBEdit;
    DBEdit118: TDBEdit;
    DBEdit119: TDBEdit;
    DBEdit120: TDBEdit;
    Label118: TLabel;
    Label119: TLabel;
    Label120: TLabel;
    Label121: TLabel;
    Label122: TLabel;
    Label123: TLabel;
    Label124: TLabel;
    Label125: TLabel;
    Label126: TLabel;
    Label127: TLabel;
    Label128: TLabel;
    DBEdit121: TDBEdit;
    DBEdit122: TDBEdit;
    DBEdit123: TDBEdit;
    DBEdit124: TDBEdit;
    DBEdit125: TDBEdit;
    PainelEntiCreds1: TPanel;
    PainelEntiCreds0: TPanel;
    Grade1: TStringGrid;
    QrEntidadesDataRG: TDateField;
    QrEntidadesNacionalid: TWideStringField;
    QrEntidadesEmpresa: TIntegerField;
    QrEntidadesNOMEEMPRESA: TWideStringField;
    Label132: TLabel;
    Label133: TLabel;
    DBCheckBox3: TDBCheckBox;
    QrEntidadesFormaSociet: TWideStringField;
    QrEntidadesSimples: TSmallintField;
    QrEntidadesAtividade: TWideStringField;
    Label134: TLabel;
    QrEntidadesEstCivil: TSmallintField;
    QrEntidadesNOMEECIVIL: TWideStringField;
    PMNovo: TPopupMenu;
    Abaixode10001: TMenuItem;
    Acimade10001: TMenuItem;
    QrLoc1000: TmySQLQuery;
    QrLoc1000CodMin: TIntegerField;
    QrLoc0: TmySQLQuery;
    QrLoc0CodMin: TIntegerField;
    QrLoc0CodMax: TIntegerField;
    QrEntidadesCPF_Conjuge: TWideStringField;
    QrEntidadesCBE: TIntegerField;
    QrEntidadesSCB: TIntegerField;
    DBCheckBox4: TDBCheckBox;
    Label136: TLabel;
    QrEntidadesCPF_Resp1: TWideStringField;
    QrEntidadesCPF_Resp1_TXT: TWideStringField;
    Label137: TLabel;
    QrLocNumero: TFloatField;
    QrCadastro2NUMERO: TFloatField;
    QrEntidadesENumero: TIntegerField;
    QrEntidadesPNumero: TIntegerField;
    QrEntidadesCNumero: TIntegerField;
    QrEntidadesLNumero: TIntegerField;
    QrCadastro2CNumero: TIntegerField;
    QrCadastro2LNumero: TIntegerField;
    CkCliente3: TDBCheckBox;
    CkCliente4: TDBCheckBox;
    frxCadastro: TfrxReport;
    frxDsCadastro: TfrxDBDataset;
    frxCadastro2: TfrxReport;
    frxDsCadastro2: TfrxDBDataset;
    TabSheet9: TTabSheet;
    QrEntidadesBanco: TIntegerField;
    QrEntidadesAgencia: TWideStringField;
    QrEntidadesContaCorrente: TWideStringField;
    QrEntiCtas: TmySQLQuery;
    DsEntiCtas: TDataSource;
    QrEntiCtasCodigo: TIntegerField;
    QrEntiCtasControle: TIntegerField;
    QrEntiCtasOrdem: TIntegerField;
    QrEntiCtasBanco: TIntegerField;
    QrEntiCtasAgencia: TIntegerField;
    QrEntiCtasContaCor: TWideStringField;
    QrEntiCtasContaTip: TWideStringField;
    QrEntiCtasDAC_A: TWideStringField;
    QrEntiCtasDAC_C: TWideStringField;
    QrEntiCtasDAC_AC: TWideStringField;
    QrEntiCtasLk: TIntegerField;
    QrEntiCtasDataCad: TDateField;
    QrEntiCtasDataAlt: TDateField;
    QrEntiCtasUserCad: TIntegerField;
    QrEntiCtasUserAlt: TIntegerField;
    QrEntiCtasAlterWeb: TSmallintField;
    QrEntiCtasAtivo: TSmallintField;
    QrEntiCtasNOMEBANCO: TWideStringField;
    QrEntidadesCartPref: TIntegerField;
    QrEntidadesNOMECARTPREF: TWideStringField;
    Panel2: TPanel;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    Label138: TLabel;
    DBEdit126: TDBEdit;
    DBEdit127: TDBEdit;
    QrEntidadesRolComis: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    QrEntidadesFilial: TIntegerField;
    Label140: TLabel;
    DBEdit128: TDBEdit;
    frxDsMaster_: TfrxDBDataset;
    QrEntidadesFornece5: TWideStringField;
    QrEntidadesFornece6: TWideStringField;
    N1: TMenuItem;
    ConferirCdigosdeusurio1: TMenuItem;
    QrEntidadesCodUsu: TIntegerField;
    PMImporta: TPopupMenu;
    CdAdm1: TMenuItem;
    QrEntidadesNIRE: TWideStringField;
    Panel9: TPanel;
    Label25: TLabel;
    DBEdit25: TDBEdit;
    Label26: TLabel;
    DBEdit26: TDBEdit;
    Label48: TLabel;
    DBEdit48: TDBEdit;
    Label135: TLabel;
    Label27: TLabel;
    DBEdit27: TDBEdit;
    Label28: TLabel;
    DBEdit28: TDBEdit;
    Label47: TLabel;
    DBEdit47: TDBEdit;
    Label114: TLabel;
    Label29: TLabel;
    DBEdit29: TDBEdit;
    Label30: TLabel;
    DBEdit30: TDBEdit;
    Label116: TLabel;
    Label131: TLabel;
    DBEdit31: TDBEdit;
    Label32: TLabel;
    DBEdit32: TDBEdit;
    Label31: TLabel;
    Label33: TLabel;
    DBEdit33: TDBEdit;
    Label34: TLabel;
    DBEdit34: TDBEdit;
    Label38: TLabel;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    Label41: TLabel;
    Label42: TLabel;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    Label43: TLabel;
    DBEdit35: TDBEdit;
    Label35: TLabel;
    Label36: TLabel;
    DBEdit36: TDBEdit;
    Label44: TLabel;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    Label105: TLabel;
    Label106: TLabel;
    DBEdit107: TDBEdit;
    Label37: TLabel;
    DBEdit37: TDBEdit;
    Label40: TLabel;
    DBEdit40: TDBEdit;
    Label46: TLabel;
    DBEdit46: TDBEdit;
    Label45: TLabel;
    DBEdit45: TDBEdit;
    Label87: TLabel;
    DBEdit88: TDBEdit;
    DBEdit55: TDBEdit;
    Label86: TLabel;
    Label130: TLabel;
    Label117: TLabel;
    Label113: TLabel;
    Label51: TLabel;
    DBEdit51: TDBEdit;
    Label52: TLabel;
    DBEdit52: TDBEdit;
    Label129: TLabel;
    Panel10: TPanel;
    DsCondImov: TDataSource;
    QrCondImov: TmySQLQuery;
    DBGrid3: TDBGrid;
    QrCondImovUnidade: TWideStringField;
    QrCondImovCliente: TIntegerField;
    QrCondImovNO_CND: TWideStringField;
    QrCondImovTIPO: TWideStringField;
    DBEdit83: TDBEdit;
    DBEdit129: TDBEdit;
    DBEdit130: TDBEdit;
    DBEdit131: TDBEdit;
    DBEdit132: TDBEdit;
    DBEdit133: TDBEdit;
    DBEdit134: TDBEdit;
    DBEdit135: TDBEdit;
    DBEdit136: TDBEdit;
    DBEdit137: TDBEdit;
    DBEdit138: TDBEdit;
    DBEdit139: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox3: TGroupBox;
    BtEntidades2: TBitBtn;
    BtImporta: TBitBtn;
    GBCntrl: TGroupBox;
    Panel11: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel12: TPanel;
    Panel13: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    SbMapa: TBitBtn;
    N2: TMenuItem;
    Exportacelularesdepessoas1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEntidadesCalcFields(DataSet: TDataSet);
    procedure QrEntidadesAfterScroll(DataSet: TDataSet);
    procedure BtImagemClick(Sender: TObject);
    procedure QrEntidadesAfterOpen(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure Pesquisas1Click(Sender: TObject);
    procedure Dados1Click(Sender: TObject);
    procedure porNotaFiscal1Click(Sender: TObject);
    procedure porNotaFiscal2Click(Sender: TObject);
    procedure Produtoquecompra1Click(Sender: TObject);
    procedure Produtoquevende1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrCadastro2CalcFields(DataSet: TDataSet);
    procedure Dadoscompactosdaentidadeatual1Click(Sender: TObject);
    procedure BtImagem1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure Dados2Click(Sender: TObject);
    procedure Nome1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtEntidades2Click(Sender: TObject);
    procedure QrEntidadesBeforeClose(DataSet: TDataSet);
    procedure Abaixode10001Click(Sender: TObject);
    procedure Acimade10001Click(Sender: TObject);
    procedure frxCadastroGetValue(const VarName: String;
      var Value: Variant);
    procedure ConferirCdigosdeusurio1Click(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure CdAdm1Click(Sender: TObject);
    procedure SbMapaClick(Sender: TObject);
    procedure Exportacelularesdepessoas1Click(Sender: TObject);
  private
    { Private declarations }
    FNovoEnti: Integer;
    procedure CriaOForm;
    procedure SubQuery1Reopen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure TravaOForm;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenEntiTransp;
    procedure ReopenEntiCtas;
    procedure LocalizaEntidade1(Tipo: Integer);
    procedure SbPesquisa;
    procedure MostraTabSheet;

  public
    { Public declarations }
    FPodeNeg: Boolean;
    FEditingByMatriz: Boolean;
    FEntTipo: TUnEntTipo;
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmEntidades: TFmEntidades;

implementation
  uses Curinga, Module, EntidadesNew, EntiProd, MyDBCheck, UnMyLinguas,
  EntiTransfere, Principal, EntidadesImp,
  {$IFDEF UV_LOAD_ENT1} EntiLoad01, {$ENDIF} GetValor, MeuDBUses, ModuleGeral,
  UnMyObjects, DmkDAC_PF, UnConsultasWeb;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntidades.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEntidades.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEntidadesCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmEntidades.DefParams;
begin
  VAR_GOTOTABELA := 'Entidades';
  VAR_GOTOMySQLTABLE := QrEntidades;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  //
  VAR_SQLx.Add('SELECT ent.*, ');
  VAR_SQLx.Add('car.Nome NOMECARTPREF,');
  VAR_SQLx.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  VAR_SQLx.Add('ELSE ent.Nome END NOMEENTIDADE,');
  VAR_SQLx.Add('CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial');
  VAR_SQLx.Add('ELSE acm.Nome END NOMEACCOUNT,');
  VAR_SQLx.Add('eng.Nome NOMEENTIGRUPO,');
  VAR_SQLx.Add('mot.Descricao NOMEMOTIVO,');
  VAR_SQLx.Add('emp.RazaoSocial NOMEEMPRESA,');
  VAR_SQLx.Add('euf.nome NOMEEUF, puf.nome NOMEPUF,');
  VAR_SQLx.Add('cuf.nome NOMECUF, luf.nome NOMELUF, nuf.Nome NOMENUF, ');
  VAR_SQLx.Add('elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,');
  VAR_SQLx.Add('clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD, lec.Nome NOMEECIVIL');
  VAR_SQLx.Add('FROM entidades ent');
  VAR_SQLx.Add('LEFT JOIN entigrupos eng ON eng.Codigo=ent.Grupo');
  VAR_SQLx.Add('LEFT JOIN entidades acm  ON acm.Codigo=ent.Account');
  VAR_SQLx.Add('LEFT JOIN entidades emp  ON emp.Codigo=ent.Empresa');
  VAR_SQLx.Add('LEFT JOIN motivose mot   ON mot.Codigo=ent.Motivo');
  VAR_SQLx.Add('LEFT JOIN ufs euf        ON euf.Codigo=ent.EUF');
  VAR_SQLx.Add('LEFT JOIN ufs puf        ON puf.Codigo=ent.PUF');
  VAR_SQLx.Add('LEFT JOIN ufs cuf        ON cuf.Codigo=ent.EUF');
  VAR_SQLx.Add('LEFT JOIN ufs luf        ON luf.Codigo=ent.PUF');
  VAR_SQLx.Add('LEFT JOIN ufs nuf        ON nuf.Codigo=ent.UFNatal');
  VAR_SQLx.Add('LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd');
  VAR_SQLx.Add('LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd');
  VAR_SQLx.Add('LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd');
  VAR_SQLx.Add('LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd');
  VAR_SQLx.Add('LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil');
  VAR_SQLx.Add('LEFT JOIN carteiras   car ON car.Codigo=ent.CartPref');
  VAR_SQLx.Add('WHERE ent.Codigo>-1000');
  //
  VAR_SQL1.Add('AND ent.Codigo=:P0');
  //
  VAR_SQLa.Add('AND (CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial LIKE:P0');
  VAR_SQLa.Add('ELSE ent.Nome LIKE:P1 END) ');
  // ERRO ao usar pela primeira vez
end;

procedure TFmEntidades.Exportacelularesdepessoas1Click(Sender: TObject);
const
  ArqOSWin = 'C:\Dermatek\Celulares.csv';
  ArqMySQL = 'C://Dermatek//Celulares.csv';
var
  Continua: Boolean;
begin
  if FileExists(ArqOSWin) then
    Continua := DeleteFile(ArqOSWin)
  else
    Continua := True;
  //
  if Continua then
  begin
    UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'SELECT TRIM(SUBSTRING(TRIM(ent.Nome), 1, ',
    'IF(LOCATE(" ", TRIM(ent.Nome)) > 0, ',
    'LOCATE(" ", ent.Nome), 100))) PrimoName, ',
    'ent.PCel, ent.PNatal, ent.Sexo, ',
    'IF(lec.Codigo=0, "", lec.Nome) NO_ESTCIVIL, ',
    'ent.PCidade, ufs.Nome NO_UF, ent.Codigo  ',
    'FROM Entidades ent',
    'LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil ',
    'LEFT JOIN ufs ufs ON ufs.Codigo=ent.PUF',
    'WHERE ent.Tipo=1 ',
    'AND ent.PCel <> "" ',
    'ORDER BY PrimoName ',
    'INTO OUTFILE "' + ArqMySQL + '" ',
    'FIELDS TERMINATED BY ";" ',
    '']);
    //
    if FileExists(ArqOSWin) then
      Geral.MB_Aviso('Os dados foram exportados para: ' + ArqOSWin);
  end else
    if FileExists(ArqOSWin) then
      Geral.MB_ERRO('N�o foi poss�vel excluir a exporta��o anterior de: ' +
      ArqOSWin + sLineBreak + 'Feche o arquivo excel caso ele esteja aberto!');
end;

procedure TFmEntidades.SubQuery1Reopen;
begin
  //
end;

procedure TFmEntidades.CdAdm1Click(Sender: TObject);
begin
  {$IFDEF UV_LOAD_ENT1}
  if DBCheck.CriaFm(TFmEntiLoad01, FmEntiLoad01, afmoLiberado) then
  begin
    FmEntiLoad01.ShowModal;
    FmEntiLoad01.Destroy;
  end;
  {$ENDIF}
end;

procedure TFmEntidades.ConferirCdigosdeusurio1Click(Sender: TObject);
var
  JaExiste: Boolean;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SHOW INDEX FROM entidades');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  JaExiste := False;
  while not Dmod.QrAux.Eof do
  begin
    if Uppercase(Dmod.QrAux.FieldByName('Column_name').AsString) = 'CODUSU' then
    begin
      JaExiste := True;
      Break
    end;
    Dmod.QrAux.Next;
  end;
  if JaExiste then
    Geral.MB_Aviso('N�o h� cadastros sem c�digo de usu�rio!')
  else
  begin
    {
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, 1, 0, 0, 1,
     High(Integer), True, 'C�digo de Usu�rio', 'Informe o c�digo inicial:',
     200, Codigo) then
     begin
     end;
    }
    if Geral.MB_Pergunta('Confirma a atribui��o do ID para o ' +
    'c�digo do usu�rio para todos cadastros de entidades?') = ID_YES then
    begin
      try
        Screen.Cursor := crHourGlass;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE entidades SET CodUsu=Codigo');
        Dmod.QrUpd.ExecSQL;
        //
        LocCod(QrEntidadesCodigo.Value, QrEntidadesCodigo.Value);
        Screen.Cursor := crDefault;
        Geral.MB_Info('Entidades atualizadas com sucesso!');
      except
        Screen.Cursor := crDefault;
        raise;
      end;
    end;
  end;
end;

procedure TFmEntidades.CriaOForm;
begin
  DefParams;
  Va(vpLast);
  if VAR_ENTIDADE > 0 then LocCod(QrEntidadesCodigo.Value, VAR_ENTIDADE);
end;

procedure TFmEntidades.AlteraRegistro;
var
  Entidade : Integer;
begin
  Entidade := QrEntidadesCodigo.Value;
  if not UMyMod.SelLockY(Entidade, Dmod.MyDB, 'Entidades', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Entidade, Dmod.MyDB, 'Entidades', 'Codigo');
      ImgTipo.SQLType := stUpd;
      GOTOy.BotoesSb(ImgTipo.SQLType);
      if DBCheck.CriaFm(TFmEntidadesNew, FmEntidadesNew, afmoNegarComAviso) then
      begin
        with FmEntidadesNew do
        begin
          ImgTipo.SQLType := stUpd;
          EdCodigo.Text := IntToStr(Entidade);
          TPCadastro.Date := QrEntidadesCadastro.Value;
          if Dmod.QrControle.FieldByName('Dono').AsInteger = QrEntidadesCodigo.Value
          then CkDono.Checked := True;
          //
          EdNome.Text := QrEntidadesNome.Value;
          EdApelido.Text := QrEntidadesApelido.Value;
          EdPai.Text := QrEntidadesPai.Value;
          EdMae.Text := QrEntidadesMae.Value;
          EdResponsavel.Text := QrEntidadesResponsavel.Value;
          EdCPF_Pai.Text := QrEntidadesCPF_Pai_TXT.Value;
          EdCPF_Resp1.Text := QrEntidadesCPF_Resp1_TXT.Value;
          EdCPF.Text := QrEntidadesPCPF_TXT.Value;
          EdRG.Text := QrEntidadesRG.Value;
          EdPRua.Text := QrEntidadesPRua.Value;
          EdPNumero.Text := IntToStr(QrEntidadesPNumero.Value);
          EdPCompl.Text := QrEntidadesPCompl.Value;
          EdPBairro.Text := QrEntidadesPBairro.Value;
          EdPCidade.Text := QrEntidadesPCidade.Value;
          EdCidadeNatal.Text := QrEntidadesCidadeNatal.Value;
          //CBPUF.KeyValue := QrEntidadesPUF.Value;
          EdPUF.Text := Geral.GetSiglaUF_do_CodigoUF(QrEntidadesPUF.Value);
          EdEUF.Text := Geral.GetSiglaUF_do_CodigoUF(QrEntidadesEUF.Value);
          EdCUF.Text := Geral.GetSiglaUF_do_CodigoUF(QrEntidadesCUF.Value);
          EdLUF.Text := Geral.GetSiglaUF_do_CodigoUF(QrEntidadesLUF.Value);
          CBNUF.KeyValue := QrEntidadesUFNatal.Value;
          EdPCEP.Text :=Geral.FormataCEP_NT(QrEntidadesPCEP.Value);
          EdPPais.Text := QrEntidadesPPais.Value;
          EdPTe1.Text := QrEntidadesPTE1_TXT.Value;
          EdPTe2.Text := QrEntidadesPTE2_TXT.Value;
          EdPTe3.Text := QrEntidadesPTE3_TXT.Value;
          EdPCel.Text := QrEntidadesPCEL_TXT.Value;
          EdPFax.Text := QrEntidadesPFAX_TXT.Value;
          EdPEmail.Text := QrEntidadesPEMail.Value;
          EdPContato.Text := QrEntidadesPContato.Value;
          TPPNatal.Date := QrEntidadesPNatal.Value;
          CBELograd.KeyValue     := QrEntidadesELograd.Value;
          EdELograd.ValueVariant := QrEntidadesELograd.Value;
          CBPLograd.KeyValue     := QrEntidadesPLograd.Value;
          EdPLograd.ValueVariant := QrEntidadesPLograd.Value;
          CBCLograd.KeyValue     := QrEntidadesCLograd.Value;
          EdCLograd.ValueVariant := QrEntidadesCLograd.Value;
          CBLLograd.KeyValue     := QrEntidadesLLograd.Value;
          EdLLograd.ValueVariant := QrEntidadesLLograd.Value;
          if QrEntidadesSexo.Value = 'M' then CBSexo.ItemIndex := 0
          else CBSexo.ItemIndex := 1;
          //
          EdRazao.Text := QrEntidadesRazaoSocial.Value;
          EdFantasia.Text := QrEntidadesFantasia.Value;
          EdRespons1.Text := QrEntidadesRespons1.Value;
          EdRespons2.Text := QrEntidadesRespons2.Value;
          EdCNPJ.Text := QrEntidadesCNPJ_TXT.Value;
          EdIE.Text := QrEntidadesIE.Value;
          EdERua.Text := QrEntidadesERua.Value;
          EdENumero.Text := IntToStr(QrEntidadesENumero.Value);
          EdECompl.Text := QrEntidadesECompl.Value;
          EdEBairro.Text := QrEntidadesEBairro.Value;
          EdECidade.Text := QrEntidadesECidade.Value;
          //CBEUF.KeyValue := QrEntidadesEUF.Value;
          EdECEP.Text :=Geral.FormataCEP_NT(QrEntidadesECEP.Value);
          EdEPais.Text := QrEntidadesEPais.Value;
          EdETe1.Text := QrEntidadesETE1_TXT.Value;
          EdETe2.Text := QrEntidadesETE2_TXT.Value;
          EdETe3.Text := QrEntidadesETE3_TXT.Value;
          EdECel.Text := QrEntidadesECEL_TXT.Value;
          EdEFax.Text := QrEntidadesEFAX_TXT.Value;
          EdEEmail.Text := QrEntidadesEEMail.Value;
          EdEContato.Text := QrEntidadesEContato.Value;
          TPENatal.Date := QrEntidadesENatal.Value;
          EdProfissao.Text := QrEntidadesProfissao.Value;
          EdCargo.Text := QrEntidadesCargo.Value;
          EdDiaRecibo.Text := IntToStr(QrEntidadesDiaRecibo.Value);
          EdAjudaV.ValueVariant := QrEntidadesAjudaEmpV.Value;
          EdAjudaP.ValueVariant := QrEntidadesAjudaEmpP.Value;
          //
          EdCRua.Text := QrEntidadesCRua.Value;
          EdCNumero.Text := IntToStr(QrEntidadesCNumero.Value);
          EdCCompl.Text := QrEntidadesCCompl.Value;
          EdCBairro.Text := QrEntidadesCBairro.Value;
          EdCCidade.Text := QrEntidadesCCidade.Value;
          EdCUF.ValueVariant := Geral.GetSiglaUF_do_CodigoUF(QrEntidadesCUF.Value);
          EdCCEP.Text :=Geral.FormataCEP_NT(QrEntidadesCCEP.Value);
          EdCPais.Text := QrEntidadesCPais.Value;
          EdCTel.Text := QrEntidadesCTEL_TXT.Value;
          EdCCel.Text := QrEntidadesCCEL_TXT.Value;
          EdCFax.Text := QrEntidadesCFAX_TXT.Value;
          EdCContato.Text := QrEntidadesCContato.Value;

          EdLRua.Text := QrEntidadesLRua.Value;
          EdLNumero.Text := IntToStr(QrEntidadesLNumero.Value);
          EdLCompl.Text := QrEntidadesLCompl.Value;
          EdLBairro.Text := QrEntidadesLBairro.Value;
          EdLCidade.Text := QrEntidadesLCidade.Value;
          EdLUF.ValueVariant := Geral.GetSiglaUF_do_CodigoUF(QrEntidadesLUF.Value);
          EdLCEP.Text :=Geral.FormataCEP_NT(QrEntidadesLCEP.Value);
          EdLPais.Text := QrEntidadesLPais.Value;
          EdLTel.Text := QrEntidadesLTEL_TXT.Value;
          EdLCel.Text := QrEntidadesLCEL_TXT.Value;
          EdLFax.Text := QrEntidadesLFAX_TXT.Value;
          EdLContato.Text := QrEntidadesLContato.Value;

          EdNivel.Text := QrEntidadesNivel.Value;

          EdInformacoes.Text := QrEntidadesInformacoes.Value;
          Memo.Lines.Text := QrEntidadesObservacoes.Value;
          //
          if QrEntidadesCliente1.Value = 'V' then CkCliente1_0.Checked := True;
          if QrEntidadesCliente2.Value = 'V' then CkCliente2_0.Checked := True;
          if QrEntidadesCliente3.Value = 'V' then CkCliente3_0.Checked := True;
          if QrEntidadesCliente4.Value = 'V' then CkCliente4_0.Checked := True;
          if QrEntidadesFornece1.Value = 'V' then CkFornece1_0.Checked := True;
          if QrEntidadesFornece2.Value = 'V' then CkFornece2_0.Checked := True;
          if QrEntidadesFornece3.Value = 'V' then CkFornece3_0.Checked := True;
          if QrEntidadesFornece4.Value = 'V' then CkFornece4_0.Checked := True;
          if QrEntidadesFornece5.Value = 'V' then CkFornece5_0.Checked := True;
          if QrEntidadesFornece6.Value = 'V' then CkFornece6_0.Checked := True;
          if QrEntidadesFornece7.Value = 'V' then CkFornece7_0.Checked := True;
          if QrEntidadesFornece8.Value = 'V' then CkFornece8_0.Checked := True;
          if QrEntidadesTerceiro.Value = 'V' then CkTerceiro_0.Checked := True;
          //
          if QrEntidadesCliente1.Value = 'V' then CkCliente1_1.Checked := True;
          if QrEntidadesCliente2.Value = 'V' then CkCliente2_1.Checked := True;
          if QrEntidadesCliente3.Value = 'V' then CkCliente3_1.Checked := True;
          if QrEntidadesCliente4.Value = 'V' then CkCliente4_1.Checked := True;
          if QrEntidadesFornece1.Value = 'V' then CkFornece1_1.Checked := True;
          if QrEntidadesFornece2.Value = 'V' then CkFornece2_1.Checked := True;
          if QrEntidadesFornece3.Value = 'V' then CkFornece3_1.Checked := True;
          if QrEntidadesFornece4.Value = 'V' then CkFornece4_1.Checked := True;
          if QrEntidadesFornece5.Value = 'V' then CkFornece5_1.Checked := True;
          if QrEntidadesFornece6.Value = 'V' then CkFornece6_1.Checked := True;
          if QrEntidadesFornece7.Value = 'V' then CkFornece7_1.Checked := True;
          if QrEntidadesFornece8.Value = 'V' then CkFornece8_1.Checked := True;
          if QrEntidadesTerceiro.Value = 'V' then CkTerceiro_1.Checked := True;
          //
          if QrEntidadesCliente1.Value = 'V' then CkCliente1_2.Checked := True;
          if QrEntidadesCliente2.Value = 'V' then CkCliente2_2.Checked := True;
          if QrEntidadesCliente3.Value = 'V' then CkCliente3_2.Checked := True;
          if QrEntidadesCliente4.Value = 'V' then CkCliente4_2.Checked := True;
          if QrEntidadesFornece1.Value = 'V' then CkFornece1_2.Checked := True;
          if QrEntidadesFornece2.Value = 'V' then CkFornece2_2.Checked := True;
          if QrEntidadesFornece3.Value = 'V' then CkFornece3_2.Checked := True;
          if QrEntidadesFornece4.Value = 'V' then CkFornece4_2.Checked := True;
          if QrEntidadesFornece5.Value = 'V' then CkFornece5_2.Checked := True;
          if QrEntidadesFornece6.Value = 'V' then CkFornece6_2.Checked := True;
          if QrEntidadesFornece7.Value = 'V' then CkFornece7_2.Checked := True;
          if QrEntidadesFornece8.Value = 'V' then CkFornece8_2.Checked := True;
          if QrEntidadesTerceiro.Value = 'V' then CkTerceiro_2.Checked := True;
          //
          RGTipo_0.ItemIndex := QrEntidadesTipo.Value;
          RGTipo_1.ItemIndex := QrEntidadesTipo.Value;
          RGTipo_2.ItemIndex := QrEntidadesTipo.Value;
          RGRecibo.ItemIndex := QrEntidadesRecibo.Value;
          CBGrupo.KeyValue := QrEntidadesGrupo.Value;
          CBAccount.KeyValue := QrEntidadesAccount.Value;

          EdConjuge.Text := QrEntidadesConjugeNome.Value;
          EdCPF_Conjuge.Text := Geral.FormataCNPJ_TT(QrEntidadesCPF_Conjuge.Value);
          EdNome1.Text := QrEntidadesNome1.Value;
          EdNome2.Text := QrEntidadesNome2.Value;
          EdNome3.Text := QrEntidadesNome3.Value;
          EdNome4.Text := QrEntidadesNome4.Value;
          TPConjugeNatal.Date := QrEntidadesConjugeNatal.Value;
          TPNatal1.Date := QrEntidadesNatal1.Value;
          TPNatal2.Date := QrEntidadesNatal2.Value;
          TPNatal3.Date := QrEntidadesNatal3.Value;
          TPNatal4.Date := QrEntidadesNatal4.Value;
          CBMotivo.KeyValue := QrEntidadesMotivo.Value;
          if QrEntidadesAgenda.Value = 'V' then CkAgenda.Checked := True;
          if QrEntidadesSenhaQuer.Value = 'V' then CkSenhaQuer.Checked := True;
          EdSenha1.Text := QrEntidadesSenha1.Value;
          EdLimiCred.Text := FormatFloat('0.00', QrEntidadesLimiCred.Value);
          EdComissao.Text := FormatFloat('0.000000', QrEntidadesComissao.Value);
          EdSSP.Text := QrEntidadesSSP.Value;
          //
          EdFatorCompra.Text := Geral.FFT(QrEntidadesFatorCompra.Value, 4, siPositivo);
          EdAdValorem.Text := Geral.FFT(QrEntidadesAdValorem.Value, 4, siPositivo);
          EdDMaisC.Text := Geral.FFT(QrEntidadesDMaisC.Value, 0, siPositivo);
          EdDMaisD.Text := Geral.FFT(QrEntidadesDMaisD.Value, 0, siPositivo);
          (*EdCustodia.Text := Geral.FFT(QrEntidadesCustodia.Value, 2, siPositivo);
          EdCobranca.Text := Geral.FFT(QrEntidadesCobranca.Value, 2, siPositivo);
          EdR_U.Text := Geral.FFT(QrEntidadesR_U.Value, 2, siPositivo);
          EdTarifaPostal.Text := Geral.FFT(QrEntidadesTarifaPostal.Value, 2, siPositivo);
          EdTarifaOperacao.Text := Geral.FFT(QrEntidadesTarifaOperacao.Value, 2, siPositivo);
          EdTxSerasa.Text := Geral.FFT(QrEntidadesTxSerasa.Value, 4, siPositivo);
          EdTxAR.Text := Geral.FFT(QrEntidadesTxAR.Value, 4, siPositivo);*)
          //
          TPDataRG.Date := QrEntidadesDataRG.Value;
          EdNacionalid.Text := QrEntidadesNacionalid.Value;
          EdFormaSociet.Text := QrEntidadesFormaSociet.Value;
          EdNIRE.Text := QrEntidadesNIRE.Value;
          CkSimples.Checked := Geral.IntToBool(QrEntidadesSimples.Value);
          if QrEntidadesEmpresa.Value = 0 then CBEmpresa.KeyValue := Null
          else CBEmpresa.KeyValue := QrEntidadesEmpresa.Value;
          CBEstCivil.KeyValue := QrEntidadesEstCivil.Value;
          EdAtividade.Text        := QrEntidadesAtividade.Value;
          EdCBE.Text              := IntToStr(QrEntidadesCBE.Value);
          CkSCB.Checked           := Geral.IntToBool_0(QrEntidadesSCB.Value);
          EdCarteira.ValueVariant := QrEntidadesCartPref.Value;
          CBCarteira.KeyValue     := QrEntidadesCartPref.Value;
          EdRolComis.ValueVariant := QrEntidadesRolComis.Value;
          CBRolComis.KeyValue     := QrEntidadesRolComis.Value;
          EdFilial.ValueVariant   := QrEntidadesFilial.Value;
          //
          FmPrincipal.AcoesIniciaisDeCadastroDeEntidades(FmEntidadesNew, Entidade, Grade1);
        end;
        FmEntidadesNew.ShowModal;
        FmEntidadesNew.Destroy;
        LocCod(QrEntidadesCodigo.Value, Entidade);
      end;
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmEntidades.IncluiRegistro;
var
  Cursor : TCursor;
  Entidade : Integer;
  LocNormal: Boolean;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  LocNormal := True;
  Entidade := 0;
  try
    if FmPrincipal.FTipoNovoEnti > 0 then
    begin
      LocNormal := False;
      if FNovoEnti = 1000 then
      begin
        QrLoc1000.Close;
        UnDmkDAC_PF.AbreQuery(QrLoc1000, Dmod.MyDB);
        Entidade := QrLoc1000CodMin.Value + 1;
        if Entidade = 1 then Entidade := 1001;
      end else begin
        QrLoc0.Close;
        UnDmkDAC_PF.AbreQuery(QrLoc0, Dmod.MyDB);
        Entidade := QrLoc0CodMax.Value;
        if Entidade = 999 then LocNormal := True else Entidade := Entidade + 1;
      end;
    end;
    if DBCheck.CriaFm(TFmEntidadesNew, FmEntidadesNew, afmoNegarComAviso) then
    begin
      if LocNormal then
        Entidade := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'Entidades', 'Entidades', 'Codigo');
      FmEntidadesNew.ImgTipo.SQLType := stIns;
      FmEntidadesNew.EdCodigo.Text := IntToStr(Entidade);
      FmEntidadesNew.TPCadastro.Date := Date;
      //
      FmEntidadesNew.CkCliente1_0.Checked := VAR_DEF_CLI1;
      FmEntidadesNew.CkCliente2_0.Checked := VAR_DEF_CLI2;
      FmEntidadesNew.CkCliente3_0.Checked := VAR_DEF_CLI3;
      FmEntidadesNew.CkCliente4_0.Checked := VAR_DEF_CLI4;
      FmEntidadesNew.CkFornece1_0.Checked := VAR_DEF_FOR1;
      FmEntidadesNew.CkFornece2_0.Checked := VAR_DEF_FOR2;
      FmEntidadesNew.CkFornece3_0.Checked := VAR_DEF_FOR3;
      FmEntidadesNew.CkFornece4_0.Checked := VAR_DEF_FOR4;
      FmEntidadesNew.CkFornece5_0.Checked := VAR_DEF_FOR5;
      FmEntidadesNew.CkFornece6_0.Checked := VAR_DEF_FOR6;
      FmEntidadesNew.CkFornece7_0.Checked := VAR_DEF_FOR7;
      FmEntidadesNew.CkFornece8_0.Checked := VAR_DEF_FOR8;
      FmEntidadesNew.CkTerceiro_0.Checked := VAR_DEF_CORI;
      //
      FmEntidadesNew.CkCliente1_1.Checked := VAR_DEF_CLI1;
      FmEntidadesNew.CkCliente2_1.Checked := VAR_DEF_CLI2;
      FmEntidadesNew.CkCliente3_1.Checked := VAR_DEF_CLI3;
      FmEntidadesNew.CkCliente4_1.Checked := VAR_DEF_CLI4;
      FmEntidadesNew.CkFornece1_1.Checked := VAR_DEF_FOR1;
      FmEntidadesNew.CkFornece2_1.Checked := VAR_DEF_FOR2;
      FmEntidadesNew.CkFornece3_1.Checked := VAR_DEF_FOR3;
      FmEntidadesNew.CkFornece4_1.Checked := VAR_DEF_FOR4;
      FmEntidadesNew.CkFornece5_1.Checked := VAR_DEF_FOR5;
      FmEntidadesNew.CkFornece6_1.Checked := VAR_DEF_FOR6;
      FmEntidadesNew.CkFornece7_1.Checked := VAR_DEF_FOR7;
      FmEntidadesNew.CkFornece8_1.Checked := VAR_DEF_FOR8;
      FmEntidadesNew.CkTerceiro_1.Checked := VAR_DEF_CORI;
      //
      FmEntidadesNew.CkCliente1_2.Checked := VAR_DEF_CLI1;
      FmEntidadesNew.CkCliente2_2.Checked := VAR_DEF_CLI2;
      FmEntidadesNew.CkCliente3_2.Checked := VAR_DEF_CLI3;
      FmEntidadesNew.CkCliente4_2.Checked := VAR_DEF_CLI4;
      FmEntidadesNew.CkFornece1_2.Checked := VAR_DEF_FOR1;
      FmEntidadesNew.CkFornece2_2.Checked := VAR_DEF_FOR2;
      FmEntidadesNew.CkFornece3_2.Checked := VAR_DEF_FOR3;
      FmEntidadesNew.CkFornece4_2.Checked := VAR_DEF_FOR4;
      FmEntidadesNew.CkFornece5_2.Checked := VAR_DEF_FOR5;
      FmEntidadesNew.CkFornece6_2.Checked := VAR_DEF_FOR6;
      FmEntidadesNew.CkFornece7_2.Checked := VAR_DEF_FOR7;
      FmEntidadesNew.CkFornece8_2.Checked := VAR_DEF_FOR8;
      FmEntidadesNew.CkTerceiro_2.Checked := VAR_DEF_CORI;
      //
      //FmEntidadesNew.CBEUF.KeyValue        := VAR_UFPADRAO;
      FmEntidadesNew.EdEUF.Text            := Geral.GetSiglaUF_do_CodigoUF(VAR_UFPADRAO);
      FmEntidadesNew.EdECidade.Text        := VAR_CIDADEPADRAO;
      //FmEntidadesNew.CBPUF.KeyValue        := VAR_UFPADRAO;
      FmEntidadesNew.EdPUF.Text            := Geral.GetSiglaUF_do_CodigoUF(VAR_UFPADRAO);
      FmEntidadesNew.EdPCidade.Text        := VAR_CIDADEPADRAO;
      //
      FmEntidadesNew.FEntTipo := FEntTipo;
      //
      GOTOy.BotoesSb(ImgTipo.SQLType);
      FmPrincipal.AcoesIniciaisDeCadastroDeEntidades(FmEntidadesNew, Entidade, Grade1);
      FmEntidadesNew.ShowModal;
      FmEntidadesNew.Destroy;
      LocCod(QrEntidadesCodigo.Value, Entidade);
    end else begin
      Screen.Cursor := crDefault;
      Exit;
    end;
  finally
    Screen.Cursor := Cursor;
  end;
end;


///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////


procedure TFmEntidades.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntidades.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntidades.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntidades.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntidades.BtIncluiClick(Sender: TObject);
begin
  if FmPrincipal.FTipoNovoEnti = 1000 then
    MyObjects.MostraPopUpDeBotao(PMNovo, BtInclui)
  else begin
    IncluiRegistro;
    GOTOy.BotoesSb(ImgTipo.SQLType);
  end;
end;

procedure TFmEntidades.BtImportaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImporta, BtImporta);
end;

procedure TFmEntidades.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEntidades.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO     := QrEntidadesCodigo.Value;
  VAR_ENTIDADE     := QrEntidadesCodigo.Value;
  VAR_ENTIDADENOME := DBEntiNome.Text;
  if CkTerceiro.Checked then VAR_TERCEIRO := QrEntidadesCodigo.Value;
  Close;
end;

procedure TFmEntidades.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FEditingByMatriz := False;
  FEntTipo         := uetNenhum;
  //Geral.WriteTextString(100, 100, 'Teste', FmEntidades, True);
  {
  frxCadastro.DataSets.Add(Dmod.frxDsMaster);
  frxCadastro.DataSets.Add(frxDsCadastro);
  frxCadastro.DataSets.Add(frxDsCadastro2);
  //
  frxCadastro2.DataSets.Add(Dmod.frxDsMaster);
  frxCadastro2.DataSets.Add(frxDsCadastro2);
  frxCadastro2.DataSets.Add(frxDsCadastro);
  }
  frxDsMaster_.DataSet := DmodG.QrMaster;
  //
  CriaOForm;
  CkCliente1.Caption := VAR_CLIENTE1;
  CkCliente2.Caption := VAR_CLIENTE2;
  CkCliente3.Caption := VAR_CLIENTE3;
  CkCliente4.Caption := VAR_CLIENTE4;
  CkFornece1.Caption := VAR_FORNECE1;
  CkFornece2.Caption := VAR_FORNECE2;
  CkFornece3.Caption := VAR_FORNECE3;
  CkFornece4.Caption := VAR_FORNECE4;
  CkFornece5.Caption := VAR_FORNECE5;
  CkFornece6.Caption := VAR_FORNECE6;
  CkFornece7.Caption := VAR_FORNECE7;
  CkFornece8.Caption := VAR_FORNECE8;
  CkTerceiro.Caption := VAR_TERCEIR2;
  //
  CkCliente2.Visible := False;
  CkCliente3.Visible := False;
  CkCliente4.Visible := False;
  CkFornece1.Visible := False;
  CkFornece2.Visible := False;
  CkFornece3.Visible := False;
  CkFornece4.Visible := False;
  CkFornece5.Visible := False;
  CkFornece6.Visible := False;
  CkFornece7.Visible := False;
  CkFornece8.Visible := False;
  CkTerceiro.Visible := False;
  //
  if VAR_CLIENTE2 <> '' then CkCliente2.Visible := True;
  if VAR_CLIENTE3 <> '' then CkCliente3.Visible := True;
  if VAR_CLIENTE4 <> '' then CkCliente4.Visible := True;
  if VAR_FORNECE1 <> '' then CkFornece1.Visible := True;
  if VAR_FORNECE2 <> '' then CkFornece2.Visible := True;
  if VAR_FORNECE3 <> '' then CkFornece3.Visible := True;
  if VAR_FORNECE4 <> '' then CkFornece4.Visible := True;
  if VAR_FORNECE5 <> '' then CkFornece5.Visible := True;
  if VAR_FORNECE6 <> '' then CkFornece6.Visible := True;
  if VAR_FORNECE7 <> '' then CkFornece7.Visible := True;
  if VAR_FORNECE8 <> '' then CkFornece8.Visible := True;
  if VAR_TERCEIR2 <> '' then CkTerceiro.Visible := True;
  //
  LaQuantI1.Caption := VAR_QUANTI1NOME;
  UnDmkDAC_PF.AbreQuery(QrSenhas, Dmod.MyDB);
 PainelEntiCreds1.Visible := VAR_ENTICREDS;

 (*if VAR_PARADOX_CONNECTED then PainelEntiCreds2.Visible := True
 else PainelEntiCreds2.Visible := False;*)
 //if Uppercase(Application.Title) = 'SYNDIC' then

end;

procedure TFmEntidades.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  PageControl1.Hide;
  PageControl1.Show;
  PageControl1.ActivePage := TabSheet1;
end;

procedure TFmEntidades.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := //GOTOy.Codigo(QrEntidadesCodigo.Value,
  GOTOy.Codigo(QrEntidadesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidades.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEntidades.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntidades.QrEntidadesCalcFields(DataSet: TDataSet);
begin
  QrEntidadesETE1_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe1.Value);
  QrEntidadesETE2_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe2.Value);
  QrEntidadesETE3_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesETe3.Value);
  QrEntidadesEFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesEFax.Value);
  QrEntidadesECEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesECel.Value);
  QrEntidadesCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCNPJ.Value);
  //
  QrEntidadesPTE1_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe1.Value);
  QrEntidadesPTE2_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe2.Value);
  QrEntidadesPTE3_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPTe3.Value);
  QrEntidadesPFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPFax.Value);
  QrEntidadesPCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesPCel.Value);
  QrEntidadesPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF.Value);
  QrEntidadesCPF_PAI_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF_Pai.Value);
  QrEntidadesCPF_Resp1_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF_Resp1.Value);
  //
  QrEntidadesCTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCTel.Value);
  QrEntidadesCFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCFax.Value);
  QrEntidadesCCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesCCel.Value);
  //
  QrEntidadesLTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLTel.Value);
  QrEntidadesLFAX_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLFax.Value);
  QrEntidadesLCEL_TXT.Value := Geral.FormataTelefone_TT(QrEntidadesLCel.Value);
  //
  QrEntidadesECEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesECEP.Value));
  QrEntidadesPCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesPCEP.Value));
  QrEntidadesCCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesCCEP.Value));
  QrEntidadesLCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesLCEP.Value));
  //
  case QrEntidadesUserCad.Value of
    -2: QrEntidadesNOMECAD2.Value := 'BOSS [Administrador]';
    -1: QrEntidadesNOMECAD2.Value := 'MASTER [Admin. DERMATEK]';
     0: QrEntidadesNOMECAD2.Value := 'N�o definido';
    else QrEntidadesNOMECAD2.Value := QrEntidadesNOMECAD.Value;
  end;
  case QrEntidadesUserAlt.Value of
    -2: QrEntidadesNOMEALT2.Value := 'BOSS [Administrador]';
    -1: QrEntidadesNOMEALT2.Value := 'MASTER [Admin.DERMATEK]';
     0: QrEntidadesNOMEALT2.Value := '- - -';
    else QrEntidadesNOMEALT2.Value := QrEntidadesNOMEALT.Value;
  end;
end;

procedure TFmEntidades.QrEntidadesAfterScroll(DataSet: TDataSet);
begin
  (*if VAR_PARADOX_CONNECTED then
  begin
    QrEntidadez.Close;
    QrEntidadez.Params[0].AsInteger := QrEntidadesCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrEntidadez, Dmod.MyDB);
  end;*)
  SubQuery1Reopen;
  ReopenEntiTransp;
  ReopenEntiCtas;
  MostraTabSheet;
  BtExclui.Enabled := False;
  FmPrincipal.AcoesIniciaisDeCadastroDeEntidades(FmEntidades,
    QrEntidadesCodigo.Value, Grade1);
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    QrCondImov.Close;
    QrCondImov.ParamByName('Pa').AsInteger := QrEntidadesCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrCondImov, Dmod.MyDB);
  end;
end;

procedure TFmEntidades.BtImagemClick(Sender: TObject);
{
var
  Logo : TBitmap;
}
begin
{
  if OpenPictureDialog1.execute then
  begin
    Logo := TBitmap.Create;
    try
      Logo.LoadFromFile(OpenPictureDialog1.FileName);
      TbEntidade.Close;
      TbEntidade. O p e n ;
      if TbEntidade.Locate('Codigo', QrEntidadesCodigo.Value, []) then
      begin
        TbEntidade.Edit;
        DBImage1.Picture.Assign(Logo);
        TbEntidade.Post;
        TbEntidade.Refresh;
      end;
    finally
      Logo.Free;
    end;
    LocCod(QrEntidadesCodigo.Value, QrEntidadesCodigo.Value);
  end;
}
end;

procedure TFmEntidades.QrEntidadesAfterOpen(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  Enab := ((VAR_SENHA = Uppercase(VAR_BOSSSENHA)) or (VAR_SENHA = CO_MASTER)
            or (VAR_SENHA = Uppercase(VAR_ADMIN)));
  //
  ReopenEntiTransp;
  //
  if (QrEntidadesCodigo.Value < 1) and (QrEntidadesCodigo.Value <> -1) and
    (not FPodeNeg) and (Enab) then
    BtAltera.Enabled := False
  else
    BtAltera.Enabled := True;
end;

procedure TFmEntidades.ReopenEntiTransp;
begin
  QrEntiTransp.Close;

  QrEntiTransp.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrEntiTransp, Dmod.MyDB);
end;

procedure TFmEntidades.ReopenEntiCtas;
begin
  QrEntiCtas.Close;
  QrEntiCtas.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrEntiCtas, Dmod.MyDB);
end;

procedure TFmEntidades.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidades.LocalizaEntidade1(Tipo: Integer);
{###
var
  Num: String;
}
begin
{ ver lct 2010-10-15
  VAR_ENTIDADE := QrEntidadesCodigo.Value;
  if Tipo = 1 then
  begin
    Num := InputBox('Localiza��o de cliente por NF.','Digite a Nota Fiscal.', '' );
    QrLoc.SQL.Add('AND la.Cliente=en.Codigo');
    if Num = '' then Exit;
  end;
  if Tipo in ([1,2]) then
  begin
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT en.Codigo,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.RazaoSocial, en.Nome) NOMEENTIDADE,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.IE, en.RG) IE_RG,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.EContato, en.PContato) CONTATO,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.ERua,    en.PRua    ) RUA,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.ENumero, en.PNumero ) Numero,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.ECompl,  en.PCompl  ) Compl ,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.EBairro, en.PBairro ) Bairro,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.ECidade, en.PCidade ) Cidade,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.EUF,     en.PUF     ) UF    ,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.ECEP,    en.PCEP    ) CEP   ,');
    QrLoc.SQL.Add('IF (en.Tipo=0, en.EPais,   en.PPais   ) Pais  ,');
    QrLoc.SQL.Add('en.PTe1, en.PTe2, en.PTe3, en.ETe1, en.ETe2, en.ETe3,');
    QrLoc.SQL.Add('en.PFax, en.EFax, en.PCel, en.ECel,');
    QrLoc.SQL.Add('en.CTel, en.CCel, en.CFax, en.LTel, en.LCel, en.LFax,');
    QrLoc.SQL.Add('uf.Nome NOMEUF');
    QrLoc.SQL.Add('FROM entidades en, ' + lct + ' la, ufs uf');
    QrLoc.SQL.Add('WHERE en.Codigo>0');
    QrLoc.SQL.Add('AND uf.Codigo=(CASE WHEN en.Tipo=0 THEN en.EUF ELSE en.PUF END)');
    if Tipo = 2 then
    begin
      Num := InputBox('Localiza��o de fornecedor por NF.','Digite a Nota Fiscal.', '' );
      QrLoc.SQL.Add('AND la.Fornecedor=en.Codigo');
    end;
    QrLoc.SQL.Add('AND la.NotaFiscal='+Num);
    UnDmkDAC_PF.AbreQuery(QrLoc, ?
    Geral.FormShow(TFmEntiLocNF, FmEntiLocNF);
    QrLoc.Close;
  end else
  if Tipo in ([3,4]) then
  begin
    if DBCheck.CriaFm(TFmEntiProd, FmEntiProd, afmoNegarComAviso) then
    begin
      if Tipo = 3 then FmEntiProd.P a i n e l T i t u l o.Caption := 'Cliente';
      if Tipo = 4 then FmEntiProd.P a i n e l T i t u l o.Caption := 'Fornecedor';
      FmEntiProd.ShowModal;
      FmEntiProd.Destroy;
    end;
  end;
  LocCod(QrEntidadesCodigo.Value, VAR_ENTIDADE);
}

end;

procedure TFmEntidades.SbPesquisa;
begin
  MyObjects.FormShow(TFmEntidadesImp, FmEntidadesImp);
  LocCod(VAR_ENTIDADE, VAR_ENTIDADE);
end;

procedure TFmEntidades.Pesquisas1Click(Sender: TObject);
begin
  SbPesquisa;
end;

procedure TFmEntidades.porNotaFiscal1Click(Sender: TObject);
begin
  LocalizaEntidade1(1);
end;

procedure TFmEntidades.porNotaFiscal2Click(Sender: TObject);
begin
  LocalizaEntidade1(2);
end;

procedure TFmEntidades.Produtoquecompra1Click(Sender: TObject);
begin
  LocalizaEntidade1(3);
end;

procedure TFmEntidades.Produtoquevende1Click(Sender: TObject);
begin
  LocalizaEntidade1(4);
end;

procedure TFmEntidades.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmEntidades.SbMapaClick(Sender: TObject);
var
  MapaEndereco, MapaDescri: String;
begin
  if (QrEntidades.State = dsInactive) or (QrEntidades.RecordCount = 0) then Exit;
  //  
  {$IfDef UsaWSuport}
  UConsultasWeb.MapasDeEntidadesMontaEndereco(fmcadEntidades,
    QrEntidadesCodigo.Value, MapaEndereco, MapaDescri);
  //
  UConsultasWeb.MapasDeEntidades(fmcadEntidades, MapaEndereco, MapaDescri);
    {$ENDIF}
end;

procedure TFmEntidades.SbQueryClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuery, SbQuery);
end;

procedure TFmEntidades.SbNovoClick(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := QrEntidadesCodigo.Value;
  LocCod(Entidade, Entidade);
  //
  MyObjects.frxMostra(frxCadastro, 'Cadastro de entidades pesquisadas');
end;

procedure TFmEntidades.QrCadastro2CalcFields(DataSet: TDataSet);
begin
  QrCadastro2TE1_TXT.Value  := Geral.FormataTelefone_TT(QrCadastro2Te1.Value);
  QrCadastro2TE2_TXT.Value  := Geral.FormataTelefone_TT(QrCadastro2Te2.Value);
  QrCadastro2TE3_TXT.Value  := Geral.FormataTelefone_TT(QrCadastro2Te3.Value);
  QrCadastro2FAX_TXT.Value  := Geral.FormataTelefone_TT(QrCadastro2Fax.Value);
  QrCadastro2CEL_TXT.Value  := Geral.FormataTelefone_TT(QrCadastro2Cel.Value);
  QrCadastro2DOCA_TXT.Value := Geral.FormataCNPJ_TT(QrCadastro2DOCA.Value);
  ////
  QrCadastro2CEP_TXT.Value  :=Geral.FormataCEP_NT(QrCadastro2CEP.Value);
  QrCadastro2CCEP_TXT.Value  :=Geral.FormataCEP_NT(QrCadastro2CCEP.Value);
  QrCadastro2LCEP_TXT.Value  :=Geral.FormataCEP_NT(QrCadastro2LCEP.Value);
  ////
  if QrCadastro2Numero.Value = 0 then
     QrCadastro2NUM_TXT.Value := 'S/N' else
     QrCadastro2NUM_TXT.Value :=
     FloatToStr(QrCadastro2Numero.Value);
  if QrCadastro2CNumero.Value = 0 then
     QrCadastro2CNUM_TXT.Value := 'S/N' else
     QrCadastro2CNUM_TXT.Value :=
     FloatToStr(QrCadastro2CNumero.Value);
  if QrCadastro2LNumero.Value = 0 then
     QrCadastro2LNUM_TXT.Value := 'S/N' else
     QrCadastro2LNUM_TXT.Value :=
     FloatToStr(QrCadastro2LNumero.Value);
  //
  if QrCadastro2Tipo.Value = 0 then
  QrCadastro2NATAL_TXT.Value  := FormatDateTime(VAR_FORMATDATE3, QrCadastro2ENatal.Value)
  else QrCadastro2NATAL_TXT.Value  := FormatDateTime(VAR_FORMATDATE3, QrCadastro2PNatal.Value);
end;

procedure TFmEntidades.Dados1Click(Sender: TObject);
{
var
  i: Integer;
}
begin
  MyObjects.frxMostra(frxCadastro, 'Cadastro');
{
  for i := 0 to frxCadastro.DataSets.Count - 1 do
    ShowMessage('frxDataSet (' + IntToStr(i) + ') = ' +
    frxCadastro.DataSets[i].DataSetName);
}
end;

procedure TFmEntidades.Dadoscompactosdaentidadeatual1Click(
  Sender: TObject);
var
  i: Integer;
begin
  QrCadastro2.Close;
  QrCadastro2.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCadastro2, Dmod.MyDB);
  MyObjects.frxMostra(frxCadastro2, 'Cadastro da entidade selecionada');
  QrCadastro2.Close;
  for i := 0 to frxCadastro2.DataSets.Count - 1 do
    ShowMessage('frxDataSet (' + IntToStr(i) + ') = ' +
    frxCadastro2.DataSets[i].DataSetName);
end;

procedure TFmEntidades.BtImagem1Click(Sender: TObject);
{
var
  Logo : TBitmap;
}
begin
{
  if OpenPictureDialog1.execute then
  begin
    Logo := TBitmap.Create;
    try
      Logo.LoadFromFile(OpenPictureDialog1.FileName);
      TbEntidade.Close;
      UnDmkDAC_PF.Abre??(TbEntidade
      if TbEntidade.Locate('Codigo', QrEntidadesCodigo.Value, []) then
      begin
        TbEntidade.Edit;
        DBImage3.Picture.Assign(Logo);
        TbEntidade.Post;
        TbEntidade.Refresh;
      end;
    finally
      Logo.Free;
    end;
    LocCod(QrEntidadesCodigo.Value, QrEntidadesCodigo.Value);
  end;
}
end;

procedure TFmEntidades.MostraTabSheet;
begin
  case VAR_ACTIVEPAGE of
    3: PageControl1.ActivePage := TabSheet3;
    4: PageControl1.ActivePage := TabSheet4;
    5: PageControl1.ActivePage := TabSheet5;
    6: PageControl1.ActivePage := TabSheet6;
    else begin
      if QrEntidadesTipo.Value = 1 then
        PageControl1.ActivePage := TabSheet1
      else PageControl1.ActivePage := TabSheet2;
    end;
  end;
end;

procedure TFmEntidades.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePage = TabSheet3 then VAR_ACTIVEPAGE := 3
  else if PageControl1.ActivePage = TabSheet4 then VAR_ACTIVEPAGE := 4
  else if PageControl1.ActivePage = TabSheet5 then VAR_ACTIVEPAGE := 5
  else if PageControl1.ActivePage = TabSheet6 then VAR_ACTIVEPAGE := 6
  else if PageControl1.ActivePage = TabSheet7 then VAR_ACTIVEPAGE := 7
  else if QrEntidadesTipo.Value = 1 then VAR_ACTIVEPAGE := 1
  else VAR_ACTIVEPAGE := 2;
  //
end;

procedure TFmEntidades.Dados2Click(Sender: TObject);
begin
  SbPesquisa;
end;

procedure TFmEntidades.Nome1Click(Sender: TObject);
begin
  LocCod(QrEntidadesCodigo.Value,
  CuringaLoc.CriaFormEnti(CO_CODIGO, 'RazaoSocial', CO_NOME, 'Fantasia',
  'Apelido', 'CNPJ', 'CPF', 'EUF', 'PUF', 'Entidades', 'Tipo', 0, Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEntidades.BtExcluiClick(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := QrEntidadesCodigo.Value;
  if DModG.NaoPermiteExclusao(ivTabPerfis, 'Exclus�o de entidade', 0) then Exit;
  if Geral.MB_Pergunta('Confirma e exclus�o permanente do cadastro '+
  'de "'+QrEntidadesNOMEENTIDADE.Value+'"?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + ' entidades WHERE Codigo='+IntToStr(Entidade));
    Dmod.QrUpd.ExecSQL;
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Entidades', Entidade);
    Sleep(500);
    QrEntidades.Close;
  end;
end;

procedure TFmEntidades.BtEntidades2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiTransfere, FmEntiTransfere, afmoNegarComAviso) then
  begin
    FmEntiTransfere.FEntidadeAtual := QrEntidadesCodigo.Value;
    FmEntiTransfere.ShowModal;
    FmEntiTransfere.Destroy;
  end;
end;

procedure TFmEntidades.QrEntidadesBeforeClose(DataSet: TDataSet);
begin
  //QrEntidadez.Close;
  //BtExclui.Enabled := False;
end;

procedure TFmEntidades.Abaixode10001Click(Sender: TObject);
begin
  FNovoEnti := 0;
  IncluiRegistro;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEntidades.Acimade10001Click(Sender: TObject);
begin
  FNovoEnti := 1000;
  IncluiRegistro;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEntidades.frxCadastroGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'ENTIDADE' then
  begin
    if QrEntidadesCodigo.Value > 0 then
      Value := 'Dados cadastrais de '+
      QrEntidadesNOMEENTIDADE.Value+' ['+
      FormatFloat('000', QrEntidadesCodigo.Value)+']'
    else Value := 'Dados cadastrais de:';
  end;
  if VarName = '_FANTASIA' then
    if QrEntidadesTipo.Value = 0 then Value := 'Fantasia:' else Value := 'Apelido:';
  if VarName = '_CNPJ_CPF' then
    if QrEntidadesTipo.Value = 0 then Value := 'CNPJ:' else Value := 'CPF:';
  if VarName = '_IE_RG' then
    if QrEntidadesTipo.Value = 0 then Value := 'I.E.:' else Value := 'RG:';
  if VarName = '_NATAL' then
    if QrEntidadesTipo.Value = 0 then Value := 'Funda��o:' else Value := 'Nascimento:';
end;

end.

