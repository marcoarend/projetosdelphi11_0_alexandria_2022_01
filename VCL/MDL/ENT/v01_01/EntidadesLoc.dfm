object FmEntidadesLoc: TFmEntidadesLoc
  Left = 342
  Top = 194
  Caption = 'ENT-GEREN-003 :: Localiza'#231#227'o de Empresas Para C'#243'pia'
  ClientHeight = 505
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 688
    Height = 343
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 350
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 686
      Height = 341
      Align = alClient
      DataSource = DsLoc
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CNPJ_TXT'
          Title.Caption = 'CNPJ'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RazaoSocial'
          Title.Caption = 'Raz'#227'o Social'
          Width = 239
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Nome [Pessoa]'
          Width = 191
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = -96
    ExplicitWidth = 784
    object GB_R: TGroupBox
      Left = 640
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 161
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbNumero: TBitBtn
        Tag = 7
        Left = 8
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbNumeroClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 89
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbQueryClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 49
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNomeClick
      end
    end
    object GB_M: TGroupBox
      Left = 161
      Top = 0
      Width = 479
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitLeft = 48
      ExplicitWidth = 688
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 453
        Height = 32
        Caption = 'Localiza'#231#227'o de Empresas Para C'#243'pia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 453
        Height = 32
        Caption = 'Localiza'#231#227'o de Empresas Para C'#243'pia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 453
        Height = 32
        Caption = 'Localiza'#231#227'o de Empresas Para C'#243'pia'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 391
    Width = 688
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitLeft = -96
    ExplicitTop = 378
    ExplicitWidth = 784
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 684
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 435
    Width = 688
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -96
    ExplicitTop = 422
    ExplicitWidth = 784
    object PnSaiDesis: TPanel
      Left = 542
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 638
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 540
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 636
      object BtConfirma: TBitBtn
        Tag = 104
        Left = 24
        Top = 5
        Width = 120
        Height = 40
        Hint = 'Copia os dados da entidade selecionada'
        Caption = 'Copi&a'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object DsLoc: TDataSource
    DataSet = QrLoc
    Left = 20
    Top = 69
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM entidades')
    Left = 48
    Top = 69
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrLocFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrLocRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrLocRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrLocPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrLocMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrLocCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrLocIE: TWideStringField
      FieldName = 'IE'
      Size = 15
    end
    object QrLocNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrLocApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrLocCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrLocRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrLocERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrLocECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrLocEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrLocECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 15
    end
    object QrLocEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrLocEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrLocETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrLocETe2: TWideStringField
      FieldName = 'ETe2'
    end
    object QrLocETe3: TWideStringField
      FieldName = 'ETe3'
    end
    object QrLocECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrLocEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrLocEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrLocEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrLocENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrLocPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrLocPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrLocPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrLocPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 15
    end
    object QrLocPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrLocPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrLocPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrLocPTe2: TWideStringField
      FieldName = 'PTe2'
    end
    object QrLocPTe3: TWideStringField
      FieldName = 'PTe3'
    end
    object QrLocPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrLocPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrLocPEMail: TWideStringField
      FieldName = 'PEMail'
      Size = 100
    end
    object QrLocPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrLocPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrLocSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrLocResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrLocProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrLocCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrLocRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrLocDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrLocAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrLocAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrLocCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrLocCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrLocFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrLocFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrLocFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrLocFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrLocTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrLocCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrLocInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrLocLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrLocVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrLocMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrLocObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrLocENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrLocPNumero: TIntegerField
      FieldName = 'PNumero'
    end
  end
end
