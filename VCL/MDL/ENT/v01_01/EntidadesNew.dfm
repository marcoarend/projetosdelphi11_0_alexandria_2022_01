object FmEntidadesNew: TFmEntidadesNew
  Left = 321
  Top = 165
  BorderStyle = bsSizeToolWin
  Caption = 'ENT-GEREN-002 :: Edi'#231#227'o de Cadastro de Pessoas e Empresas'
  ClientHeight = 666
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 555
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object dmkLabel1: TdmkLabel
      Left = 68
      Top = 60
      Width = 52
      Height = 13
      Caption = 'dmkLabel1'
      UpdType = utYes
      SQLType = stNil
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 49
      Width = 1008
      Height = 506
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = ' &Pessoal '
        object PnPessoal05: TPanel
          Left = 0
          Top = 471
          Width = 1000
          Height = 7
          Align = alBottom
          ParentBackground = False
          TabOrder = 0
          ExplicitTop = 476
          ExplicitWidth = 1002
        end
        object Panel14: TPanel
          Left = 0
          Top = 0
          Width = 792
          Height = 471
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          ExplicitHeight = 476
          object PnPessoal01: TPanel
            Left = 0
            Top = 0
            Width = 792
            Height = 120
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label25: TLabel
              Left = 4
              Top = 0
              Width = 31
              Height = 13
              Caption = 'Nome:'
              FocusControl = EdNome
            end
            object Label45: TLabel
              Left = 356
              Top = 0
              Width = 59
              Height = 13
              Caption = 'Nascimento:'
            end
            object Label26: TLabel
              Left = 472
              Top = 0
              Width = 38
              Height = 13
              Caption = 'Apelido:'
              FocusControl = EdApelido
            end
            object Label48: TLabel
              Left = 616
              Top = 0
              Width = 27
              Height = 13
              Caption = 'Sexo:'
            end
            object Label124: TLabel
              Left = 668
              Top = 0
              Width = 57
              Height = 13
              Caption = 'Estado civil:'
            end
            object Label27: TLabel
              Left = 4
              Top = 40
              Width = 18
              Height = 13
              Caption = 'Pai:'
              FocusControl = EdPai
            end
            object Label28: TLabel
              Left = 228
              Top = 40
              Width = 24
              Height = 13
              Caption = 'M'#227'e:'
              FocusControl = EdMae
            end
            object Label47: TLabel
              Left = 452
              Top = 40
              Width = 206
              Height = 13
              Caption = 'Respons'#225'vel (cadastro de menor de idade):'
              FocusControl = EdResponsavel
            end
            object Label103: TLabel
              Left = 668
              Top = 40
              Width = 88
              Height = 13
              Caption = 'CPF Respons'#225'vel:'
              FocusControl = EdCPF_Pai
            end
            object Label29: TLabel
              Left = 4
              Top = 80
              Width = 23
              Height = 13
              Caption = 'CPF:'
              FocusControl = EdCPF
            end
            object Label30: TLabel
              Left = 120
              Top = 80
              Width = 19
              Height = 13
              Caption = 'RG:'
              FocusControl = EdRG
            end
            object Label102: TLabel
              Left = 224
              Top = 80
              Width = 39
              Height = 13
              Caption = 'Emissor:'
              FocusControl = EdSSP
            end
            object Label118: TLabel
              Left = 280
              Top = 80
              Width = 42
              Height = 13
              Caption = 'Emiss'#227'o:'
            end
            object Label50: TLabel
              Left = 396
              Top = 80
              Width = 46
              Height = 13
              Caption = 'Profiss'#227'o:'
              FocusControl = EdProfissao
            end
            object Label51: TLabel
              Left = 496
              Top = 80
              Width = 31
              Height = 13
              Caption = 'Cargo:'
              FocusControl = EdCargo
            end
            object Label120: TLabel
              Left = 588
              Top = 80
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object SpeedButton2: TSpeedButton
              Left = 316
              Top = 16
              Width = 37
              Height = 21
              Caption = 'copiar'
              OnClick = SpeedButton2Click
            end
            object EdNome: TdmkEdit
              Left = 4
              Top = 16
              Width = 313
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdNomeChange
            end
            object TPPNatal: TdmkEditDateTimePicker
              Left = 356
              Top = 16
              Width = 112
              Height = 21
              Date = 44698.000000000000000000
              Time = 0.992729421297553900
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdApelido: TdmkEdit
              Left = 472
              Top = 16
              Width = 141
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object CBSexo: TComboBox
              Left = 616
              Top = 16
              Width = 49
              Height = 21
              Style = csDropDownList
              TabOrder = 3
              Items.Strings = (
                'M'
                'F')
            end
            object CBEstCivil: TDBLookupComboBox
              Left = 668
              Top = 16
              Width = 113
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsEstCivil
              TabOrder = 4
            end
            object EdPai: TdmkEdit
              Left = 4
              Top = 56
              Width = 220
              Height = 21
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdMae: TdmkEdit
              Left = 229
              Top = 56
              Width = 220
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdResponsavel: TdmkEdit
              Left = 452
              Top = 56
              Width = 213
              Height = 21
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCPF_Pai: TdmkEdit
              Left = 668
              Top = 56
              Width = 113
              Height = 21
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtCPFJ
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnExit = EdCPF_PaiExit
            end
            object EdCPF: TdmkEdit
              Left = 4
              Top = 96
              Width = 113
              Height = 21
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtCPFJ
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnExit = EdCPFExit
            end
            object EdRG: TdmkEdit
              Left = 120
              Top = 96
              Width = 100
              Height = 21
              TabOrder = 10
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdSSP: TdmkEdit
              Left = 224
              Top = 96
              Width = 53
              Height = 21
              MaxLength = 10
              TabOrder = 11
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object TPDataRG: TdmkEditDateTimePicker
              Left = 280
              Top = 96
              Width = 112
              Height = 21
              Date = 44698.000000000000000000
              Time = 0.992729421297553900
              TabOrder = 12
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdProfissao: TdmkEdit
              Left = 396
              Top = 96
              Width = 97
              Height = 21
              TabOrder = 13
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCargo: TdmkEdit
              Left = 496
              Top = 96
              Width = 89
              Height = 21
              TabOrder = 14
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object CBEmpresa: TDBLookupComboBox
              Left = 588
              Top = 96
              Width = 192
              Height = 21
              KeyField = 'Codigo'
              ListField = 'RazaoSocial'
              ListSource = DsEmpresas
              TabOrder = 15
            end
          end
          object PnPessoal02: TPanel
            Left = 0
            Top = 120
            Width = 792
            Height = 78
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Label39: TLabel
              Left = 4
              Top = 0
              Width = 24
              Height = 13
              Caption = 'CEP:'
              FocusControl = EdPCEP
            end
            object Label97: TLabel
              Left = 108
              Top = 0
              Width = 77
              Height = 13
              Caption = 'Tipo logradouro:'
            end
            object Label31: TLabel
              Left = 268
              Top = 0
              Width = 84
              Height = 13
              Caption = 'Nome logradouro:'
              FocusControl = EdPRua
            end
            object Label32: TLabel
              Left = 504
              Top = 0
              Width = 40
              Height = 13
              Caption = 'N'#250'mero:'
              FocusControl = EdPNumero
            end
            object Label33: TLabel
              Left = 556
              Top = 0
              Width = 67
              Height = 13
              Caption = 'Complemento:'
              FocusControl = EdPCompl
            end
            object Label34: TLabel
              Left = 4
              Top = 40
              Width = 30
              Height = 13
              Caption = 'Bairro:'
              FocusControl = EdPBairro
            end
            object Label35: TLabel
              Left = 216
              Top = 40
              Width = 36
              Height = 13
              Caption = 'Cidade:'
              FocusControl = EdPCidade
            end
            object Label38: TLabel
              Left = 472
              Top = 40
              Width = 17
              Height = 13
              Caption = 'UF:'
            end
            object Label41: TLabel
              Left = 504
              Top = 40
              Width = 25
              Height = 13
              Caption = 'Pa'#237's:'
              FocusControl = EdPPais
            end
            object BtCEP1: TBitBtn
              Left = 80
              Top = 16
              Width = 21
              Height = 21
              HelpContext = 90000
              Caption = '...'
              TabOrder = 10
              TabStop = False
              OnClick = BtCEP1Click
            end
            object EdPCEP: TdmkEdit
              Left = 4
              Top = 16
              Width = 76
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtCEP
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnEnter = EdPCEPEnter
              OnExit = EdPCEPExit
            end
            object CBPLograd: TdmkDBLookupComboBox
              Left = 148
              Top = 16
              Width = 117
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsPListaLograd
              TabOrder = 2
              dmkEditCB = EdPLograd
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdPRua: TdmkEdit
              Left = 268
              Top = 16
              Width = 233
              Height = 21
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPNumero: TdmkEdit
              Left = 504
              Top = 16
              Width = 49
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdPCompl: TdmkEdit
              Left = 556
              Top = 16
              Width = 225
              Height = 21
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPBairro: TdmkEdit
              Left = 4
              Top = 56
              Width = 209
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPCidade: TdmkEdit
              Left = 216
              Top = 56
              Width = 253
              Height = 21
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPPais: TdmkEdit
              Left = 504
              Top = 56
              Width = 277
              Height = 21
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnEnter = EdPPaisEnter
            end
            object EdPLograd: TdmkEditCB
              Left = 108
              Top = 16
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBPLograd
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdPUF: TdmkEdit
              Left = 472
              Top = 56
              Width = 28
              Height = 21
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtUF
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'PUF'
              UpdCampo = 'PUF'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object PnPessoal03: TPanel
            Left = 0
            Top = 198
            Width = 792
            Height = 78
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 2
            object Label43: TLabel
              Left = 4
              Top = 0
              Width = 103
              Height = 13
              Caption = 'Telefone Residencial:'
            end
            object Label44: TLabel
              Left = 120
              Top = 0
              Width = 94
              Height = 13
              Caption = 'Telefone Comercial:'
            end
            object Label105: TLabel
              Left = 236
              Top = 0
              Width = 85
              Height = 13
              Caption = 'Telefone Contato:'
            end
            object Label36: TLabel
              Left = 352
              Top = 0
              Width = 35
              Height = 13
              Caption = 'Celular:'
              FocusControl = EdPCel
            end
            object Label37: TLabel
              Left = 468
              Top = 0
              Width = 20
              Height = 13
              Caption = 'Fax:'
              FocusControl = EdPFax
            end
            object Label40: TLabel
              Left = 584
              Top = 0
              Width = 31
              Height = 13
              Caption = 'E-mail:'
              FocusControl = EdPEmail
            end
            object Label119: TLabel
              Left = 200
              Top = 40
              Width = 71
              Height = 13
              Caption = 'Nacionalidade:'
              FocusControl = EdNacionalid
            end
            object Label106: TLabel
              Left = 144
              Top = 40
              Width = 17
              Height = 13
              Caption = 'UF:'
            end
            object Label104: TLabel
              Left = 4
              Top = 40
              Width = 64
              Height = 13
              Caption = 'Cidade Natal:'
              FocusControl = EdCidadeNatal
            end
            object Label46: TLabel
              Left = 696
              Top = 40
              Width = 40
              Height = 13
              Caption = 'Contato:'
              FocusControl = EdPContato
            end
            object Label125: TLabel
              Left = 580
              Top = 40
              Width = 65
              Height = 13
              Caption = 'CPF Conjuge:'
              FocusControl = EdCPF_Conjuge
            end
            object Label84: TLabel
              Left = 464
              Top = 40
              Width = 59
              Height = 13
              Caption = 'Nascimento:'
            end
            object Label83: TLabel
              Left = 308
              Top = 40
              Width = 42
              Height = 13
              Caption = 'Conjuge:'
              FocusControl = EdConjuge
            end
            object EdPTe1: TdmkEdit
              Left = 4
              Top = 16
              Width = 113
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtTelCurto
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPTe2: TdmkEdit
              Left = 120
              Top = 16
              Width = 113
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtTelCurto
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPTe3: TdmkEdit
              Left = 236
              Top = 16
              Width = 113
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtTelCurto
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPCel: TdmkEdit
              Left = 352
              Top = 16
              Width = 113
              Height = 21
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtTelCurto
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPFax: TdmkEdit
              Left = 468
              Top = 16
              Width = 113
              Height = 21
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtTelCurto
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPEmail: TdmkEdit
              Left = 584
              Top = 16
              Width = 197
              Height = 21
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = True
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdNacionalid: TdmkEdit
              Left = 200
              Top = 56
              Width = 105
              Height = 21
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object CBNUF: TDBLookupComboBox
              Left = 144
              Top = 56
              Width = 52
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsPUFs
              TabOrder = 7
            end
            object EdCidadeNatal: TdmkEdit
              Left = 4
              Top = 56
              Width = 137
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdConjuge: TdmkEdit
              Left = 308
              Top = 56
              Width = 153
              Height = 21
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPContato: TdmkEdit
              Left = 696
              Top = 56
              Width = 85
              Height = 21
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCPF_Conjuge: TdmkEdit
              Left = 579
              Top = 56
              Width = 113
              Height = 21
              TabOrder = 11
              FormatType = dmktfString
              MskType = fmtCPFJ
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnExit = EdCPF_ConjugeExit
            end
            object TPConjugeNatal: TdmkEditDateTimePicker
              Left = 464
              Top = 56
              Width = 112
              Height = 21
              Date = 44698.000000000000000000
              Time = 0.992729421297553900
              TabOrder = 10
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
          object PnPessoal04: TPanel
            Left = 0
            Top = 276
            Width = 792
            Height = 195
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 3
            ExplicitHeight = 200
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 636
              Height = 197
              Align = alLeft
              Caption = ' Tipo de cadastro: '
              TabOrder = 0
              object CkCliente1_1: TCheckBox
                Left = 8
                Top = 16
                Width = 152
                Height = 17
                Caption = 'Cliente1'
                TabOrder = 0
                OnClick = CkCliente1_1Click
              end
              object CkFornece1_1: TCheckBox
                Left = 164
                Top = 16
                Width = 152
                Height = 17
                Caption = 'Fornece 1'
                TabOrder = 4
                OnClick = CkCliente1_1Click
              end
              object CkFornece2_1: TCheckBox
                Left = 164
                Top = 32
                Width = 152
                Height = 17
                Caption = 'Fornece 2'
                TabOrder = 5
                Visible = False
                OnClick = CkCliente1_1Click
              end
              object CkFornece3_1: TCheckBox
                Left = 164
                Top = 48
                Width = 152
                Height = 17
                Caption = 'Fornece 3'
                TabOrder = 6
                Visible = False
                OnClick = CkCliente1_1Click
              end
              object CkFornece4_1: TCheckBox
                Left = 164
                Top = 64
                Width = 152
                Height = 17
                Caption = 'Fornece 4'
                TabOrder = 7
                Visible = False
                OnClick = CkCliente1_1Click
              end
              object CkTerceiro_1: TCheckBox
                Left = 476
                Top = 16
                Width = 152
                Height = 17
                Caption = 'Terceiro'
                TabOrder = 12
                OnClick = CkCliente1_1Click
              end
              object CkCliente2_1: TCheckBox
                Left = 8
                Top = 32
                Width = 152
                Height = 17
                Caption = 'Cliente 2'
                TabOrder = 1
                Visible = False
                OnClick = CkCliente1_1Click
              end
              object CkFornece5_1: TCheckBox
                Left = 320
                Top = 16
                Width = 152
                Height = 17
                Caption = 'Fornece 5'
                TabOrder = 8
                Visible = False
                OnClick = CkCliente1_1Click
              end
              object CkFornece6_1: TCheckBox
                Left = 320
                Top = 32
                Width = 152
                Height = 17
                Caption = 'Fornece 6'
                TabOrder = 9
                Visible = False
                OnClick = CkCliente1_1Click
              end
              object CkCliente3_1: TCheckBox
                Left = 8
                Top = 48
                Width = 152
                Height = 17
                Caption = 'Cliente 3'
                TabOrder = 2
                Visible = False
                OnClick = CkCliente1_2Click
              end
              object CkCliente4_1: TCheckBox
                Left = 8
                Top = 64
                Width = 152
                Height = 17
                Caption = 'Cliente 4'
                TabOrder = 3
                Visible = False
                OnClick = CkCliente1_2Click
              end
              object CkFornece7_1: TCheckBox
                Left = 320
                Top = 48
                Width = 152
                Height = 17
                Caption = 'Fornece 7'
                TabOrder = 10
                Visible = False
                OnClick = CkCliente1_1Click
              end
              object CkFornece8_1: TCheckBox
                Left = 320
                Top = 64
                Width = 152
                Height = 17
                Caption = 'Fornece 8'
                TabOrder = 11
                Visible = False
                OnClick = CkCliente1_1Click
              end
            end
            object RGTipo_1: TRadioGroup
              Left = 636
              Top = 0
              Width = 104
              Height = 197
              Align = alLeft
              Caption = ' Tipo de pessoa: '
              ItemIndex = 1
              Items.Strings = (
                'Empresa'
                'Pessoal')
              TabOrder = 1
              OnClick = CkCliente1_1Click
            end
          end
        end
        object Panel15: TPanel
          Left = 792
          Top = 0
          Width = 208
          Height = 471
          Align = alClient
          ParentBackground = False
          TabOrder = 2
          ExplicitWidth = 210
          ExplicitHeight = 476
          object DBGrid3: TDBGrid
            Left = 1
            Top = 1
            Width = 208
            Height = 472
            Align = alClient
            DataSource = DsNomes
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NOME_ENT'
                Title.Caption = 'Nome'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Emp&resa '
        ImageIndex = 1
        object PnEmpresa001: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 78
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label2: TLabel
            Left = 4
            Top = 0
            Width = 64
            Height = 13
            Caption = 'Razao social:'
            FocusControl = EdRazao
          end
          object Label129: TLabel
            Left = 392
            Top = 0
            Width = 23
            Height = 13
            Caption = 'Filial:'
          end
          object Label3: TLabel
            Left = 428
            Top = 0
            Width = 71
            Height = 13
            Caption = 'Nome fantasia:'
            FocusControl = EdFantasia
          end
          object Label4: TLabel
            Left = 224
            Top = 40
            Width = 74
            Height = 13
            Caption = 'Respons'#225'vel 1:'
            FocusControl = EdRespons1
          end
          object Label126: TLabel
            Left = 428
            Top = 40
            Width = 97
            Height = 13
            Caption = 'CPF Respons'#225'vel 1:'
            FocusControl = EdCPF_Resp1
          end
          object Label5: TLabel
            Left = 544
            Top = 40
            Width = 74
            Height = 13
            Caption = 'Respons'#225'vel 2:'
            FocusControl = EdRespons2
          end
          object Label6: TLabel
            Left = 4
            Top = 40
            Width = 30
            Height = 13
            Caption = 'CNPJ:'
            FocusControl = EdCNPJ
          end
          object Label7: TLabel
            Left = 120
            Top = 40
            Width = 19
            Height = 13
            Caption = 'I.E.:'
            FocusControl = EdIE
          end
          object SpeedButton1: TSpeedButton
            Left = 352
            Top = 16
            Width = 37
            Height = 21
            Caption = 'copiar'
            OnClick = SpeedButton1Click
          end
          object EdRazao: TdmkEdit
            Left = 4
            Top = 16
            Width = 348
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdRazaoChange
          end
          object EdFilial: TdmkEdit
            Left = 392
            Top = 16
            Width = 33
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdFantasia: TdmkEdit
            Left = 428
            Top = 16
            Width = 352
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdRespons1: TdmkEdit
            Left = 224
            Top = 56
            Width = 201
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCPF_Resp1: TdmkEdit
            Left = 428
            Top = 56
            Width = 113
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdCPF_Resp1Exit
          end
          object EdRespons2: TdmkEdit
            Left = 545
            Top = 56
            Width = 236
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCNPJ: TdmkEdit
            Left = 4
            Top = 56
            Width = 113
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdCNPJExit
          end
          object EdIE: TdmkEdit
            Left = 120
            Top = 56
            Width = 100
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtIE
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object PnEmpresa002: TPanel
          Left = 0
          Top = 78
          Width = 1000
          Height = 78
          Align = alTop
          ParentBackground = False
          TabOrder = 1
          object Label15: TLabel
            Left = 4
            Top = 0
            Width = 24
            Height = 13
            Caption = 'CEP:'
            FocusControl = EdECEP
          end
          object Label98: TLabel
            Left = 104
            Top = 0
            Width = 77
            Height = 13
            Caption = 'Tipo logradouro:'
          end
          object Label8: TLabel
            Left = 272
            Top = 0
            Width = 84
            Height = 13
            Caption = 'Nome logradouro:'
            FocusControl = EdERua
          end
          object Label9: TLabel
            Left = 564
            Top = 0
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
            FocusControl = EdENumero
          end
          object Label10: TLabel
            Left = 632
            Top = 0
            Width = 67
            Height = 13
            Caption = 'Complemento:'
            FocusControl = EdECompl
          end
          object Label12: TLabel
            Left = 216
            Top = 40
            Width = 36
            Height = 13
            Caption = 'Cidade:'
            FocusControl = EdECidade
          end
          object Label11: TLabel
            Left = 4
            Top = 40
            Width = 30
            Height = 13
            Caption = 'Bairro:'
            FocusControl = EdEBairro
          end
          object Label14: TLabel
            Left = 472
            Top = 40
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label16: TLabel
            Left = 504
            Top = 40
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
            FocusControl = EdEPais
          end
          object EdECEP: TdmkEdit
            Left = 4
            Top = 16
            Width = 76
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtCEP
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnEnter = EdECEPEnter
            OnExit = EdECEPExit
          end
          object CBELograd: TdmkDBLookupComboBox
            Left = 144
            Top = 16
            Width = 125
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEListaLograd
            TabOrder = 2
            dmkEditCB = EdELograd
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdERua: TdmkEdit
            Left = 272
            Top = 16
            Width = 289
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdENumero: TdmkEdit
            Left = 564
            Top = 16
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdECompl: TdmkEdit
            Left = 632
            Top = 16
            Width = 148
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEBairro: TdmkEdit
            Left = 4
            Top = 56
            Width = 209
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdECidade: TdmkEdit
            Left = 216
            Top = 56
            Width = 253
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEPais: TdmkEdit
            Left = 504
            Top = 56
            Width = 276
            Height = 21
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnEnter = EdEPaisEnter
          end
          object BtCEP2: TBitBtn
            Left = 80
            Top = 16
            Width = 21
            Height = 21
            HelpContext = 90000
            Caption = '...'
            TabOrder = 10
            TabStop = False
            OnClick = EdCEP4Click
          end
          object EdELograd: TdmkEditCB
            Left = 104
            Top = 16
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBELograd
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdEUF: TdmkEdit
            Left = 472
            Top = 56
            Width = 28
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PUF'
            UpdCampo = 'PUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdEUFChange
          end
        end
        object PnEmpresa003: TPanel
          Left = 0
          Top = 156
          Width = 1000
          Height = 78
          Align = alTop
          ParentBackground = False
          TabOrder = 2
          object Label17: TLabel
            Left = 4
            Top = 0
            Width = 54
            Height = 13
            Caption = 'Telefone 1:'
            FocusControl = EdETe1
          end
          object Label18: TLabel
            Left = 120
            Top = 0
            Width = 54
            Height = 13
            Caption = 'Telefone 2:'
            FocusControl = EdETe2
          end
          object Label19: TLabel
            Left = 236
            Top = 0
            Width = 54
            Height = 13
            Caption = 'Telefone 3:'
            FocusControl = EdETe3
          end
          object Label20: TLabel
            Left = 352
            Top = 0
            Width = 35
            Height = 13
            Caption = 'Celular:'
            FocusControl = EdECel
          end
          object Label24: TLabel
            Left = 120
            Top = 40
            Width = 51
            Height = 13
            Caption = 'Funda'#231#227'o:'
          end
          object Label23: TLabel
            Left = 4
            Top = 40
            Width = 40
            Height = 13
            Caption = 'Contato:'
            FocusControl = EdEContato
          end
          object Label22: TLabel
            Left = 584
            Top = 0
            Width = 31
            Height = 13
            Caption = 'E-mail:'
            FocusControl = EdEEmail
          end
          object Label21: TLabel
            Left = 468
            Top = 0
            Width = 20
            Height = 13
            Caption = 'Fax:'
            FocusControl = EdEFax
          end
          object Label121: TLabel
            Left = 236
            Top = 40
            Width = 80
            Height = 13
            Caption = 'Forma societ'#225'ria:'
            FocusControl = EdFormaSociet
          end
          object Label122: TLabel
            Left = 384
            Top = 40
            Width = 29
            Height = 13
            Caption = 'NIRE:'
            FocusControl = EdNIRE
          end
          object Label123: TLabel
            Left = 556
            Top = 40
            Width = 89
            Height = 13
            Caption = 'Atividade principal:'
            FocusControl = EdAtividade
          end
          object EdETe1: TdmkEdit
            Left = 4
            Top = 16
            Width = 113
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtTelCurto
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdETe2: TdmkEdit
            Left = 120
            Top = 16
            Width = 113
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtTelCurto
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdETe3: TdmkEdit
            Left = 236
            Top = 16
            Width = 113
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtTelCurto
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdECel: TdmkEdit
            Left = 352
            Top = 16
            Width = 113
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtTelCurto
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEEmail: TdmkEdit
            Left = 584
            Top = 16
            Width = 196
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = True
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object TPENatal: TdmkEditDateTimePicker
            Left = 120
            Top = 56
            Width = 112
            Height = 21
            Date = 40338.000000000000000000
            Time = 0.632372372681857100
            TabOrder = 5
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object EdEContato: TdmkEdit
            Left = 4
            Top = 56
            Width = 113
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEFax: TdmkEdit
            Left = 468
            Top = 16
            Width = 113
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtTelCurto
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFormaSociet: TdmkEdit
            Left = 236
            Top = 56
            Width = 145
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdNIRE: TdmkEdit
            Left = 384
            Top = 56
            Width = 100
            Height = 21
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CkSimples: TCheckBox
            Left = 492
            Top = 60
            Width = 61
            Height = 17
            Caption = 'Simples.'
            TabOrder = 10
          end
          object EdAtividade: TdmkEdit
            Left = 556
            Top = 56
            Width = 224
            Height = 21
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object PnEmpresa004: TPanel
          Left = 0
          Top = 234
          Width = 1000
          Height = 87
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 3
          object GroupBox4: TGroupBox
            Left = 0
            Top = 0
            Width = 636
            Height = 87
            Align = alLeft
            Caption = ' Tipo de cadastro: '
            TabOrder = 0
            object CkCliente1_0: TCheckBox
              Left = 8
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Cliente1'
              TabOrder = 0
              OnClick = CkCliente1_0Click
            end
            object CkFornece1_0: TCheckBox
              Left = 164
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Fornece 1'
              TabOrder = 4
              OnClick = CkCliente1_0Click
            end
            object CkFornece2_0: TCheckBox
              Left = 164
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Fornece 2'
              TabOrder = 5
              Visible = False
              OnClick = CkCliente1_0Click
            end
            object CkFornece3_0: TCheckBox
              Left = 164
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Fornece 3'
              TabOrder = 6
              Visible = False
              OnClick = CkCliente1_0Click
            end
            object CkFornece4_0: TCheckBox
              Left = 164
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Fornece 4'
              TabOrder = 7
              Visible = False
              OnClick = CkCliente1_0Click
            end
            object CkTerceiro_0: TCheckBox
              Left = 476
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Terceiro'
              TabOrder = 12
              OnClick = CkCliente1_0Click
            end
            object CkCliente2_0: TCheckBox
              Left = 8
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Cliente 2'
              TabOrder = 1
              Visible = False
              OnClick = CkCliente1_0Click
            end
            object CkFornece5_0: TCheckBox
              Left = 320
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Fornece 5'
              TabOrder = 8
              Visible = False
              OnClick = CkCliente1_0Click
            end
            object CkFornece6_0: TCheckBox
              Left = 320
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Fornece 6'
              TabOrder = 9
              Visible = False
              OnClick = CkCliente1_0Click
            end
            object CkCliente4_0: TCheckBox
              Left = 8
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Cliente 4'
              TabOrder = 3
              Visible = False
              OnClick = CkCliente1_2Click
            end
            object CkCliente3_0: TCheckBox
              Left = 8
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Cliente 3'
              TabOrder = 2
              Visible = False
              OnClick = CkCliente1_2Click
            end
            object CkFornece8_0: TCheckBox
              Left = 320
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Fornece 8'
              TabOrder = 11
              Visible = False
              OnClick = CkCliente1_1Click
            end
            object CkFornece7_0: TCheckBox
              Left = 320
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Fornece 7'
              TabOrder = 10
              Visible = False
              OnClick = CkCliente1_1Click
            end
          end
          object RGTipo_0: TRadioGroup
            Left = 636
            Top = 0
            Width = 104
            Height = 87
            Align = alLeft
            Caption = ' Tipo de pessoa: '
            ItemIndex = 1
            Items.Strings = (
              'Empresa'
              'Pessoal')
            TabOrder = 1
            OnClick = CkCliente1_0Click
          end
        end
        object PnEmpresa005: TPanel
          Left = 0
          Top = 321
          Width = 1000
          Height = 157
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 4
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' &Miscel'#226'nea '
        ImageIndex = 2
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 478
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 1002
          ExplicitHeight = 483
          object Label78: TLabel
            Left = 292
            Top = 92
            Width = 86
            Height = 13
            Caption = 'Conv'#234'nio (Grupo):'
          end
          object Label49: TLabel
            Left = 292
            Top = 130
            Width = 187
            Height = 13
            Caption = 'Como obteve informa'#231#245'es da empresa?'
            FocusControl = EdInformacoes
          end
          object Label81: TLabel
            Left = 291
            Top = 169
            Width = 11
            Height = 13
            Caption = 'N:'
          end
          object Label82: TLabel
            Left = 320
            Top = 169
            Width = 73
            Height = 13
            Caption = 'Representante:'
          end
          object Label128: TLabel
            Left = 291
            Top = 209
            Width = 86
            Height = 13
            Caption = 'Rol de comiss'#245'es:'
          end
          object Label85: TLabel
            Left = 4
            Top = 252
            Width = 62
            Height = 13
            Caption = 'Autorizado 1:'
            FocusControl = EdNome1
          end
          object Label89: TLabel
            Left = 4
            Top = 292
            Width = 62
            Height = 13
            Caption = 'Autorizado 3:'
            FocusControl = EdNome3
          end
          object Label94: TLabel
            Left = 4
            Top = 332
            Width = 35
            Height = 13
            Caption = 'Motivo:'
          end
          object Label86: TLabel
            Left = 240
            Top = 252
            Width = 59
            Height = 13
            Caption = 'Nascimento:'
          end
          object Label90: TLabel
            Left = 240
            Top = 292
            Width = 59
            Height = 13
            Caption = 'Nascimento:'
          end
          object Label87: TLabel
            Left = 344
            Top = 252
            Width = 62
            Height = 13
            Caption = 'Autorizado 2:'
            FocusControl = EdNome2
          end
          object Label91: TLabel
            Left = 344
            Top = 292
            Width = 62
            Height = 13
            Caption = 'Autorizado 4:'
            FocusControl = EdNome4
          end
          object Label88: TLabel
            Left = 580
            Top = 252
            Width = 59
            Height = 13
            Caption = 'Nascimento:'
          end
          object Label92: TLabel
            Left = 580
            Top = 292
            Width = 59
            Height = 13
            Caption = 'Nascimento:'
          end
          object Label93: TLabel
            Left = 684
            Top = 292
            Width = 34
            Height = 13
            Caption = 'Senha:'
            FocusControl = EdSenha1
          end
          object Label99: TLabel
            Left = 414
            Top = 332
            Width = 48
            Height = 13
            Caption = 'Comiss'#227'o:'
            FocusControl = EdComissao
          end
          object Label100: TLabel
            Left = 518
            Top = 332
            Width = 88
            Height = 13
            Caption = '% Desc. Min M'#225'x.:'
            FocusControl = EdDesco
          end
          object Label101: TLabel
            Left = 614
            Top = 332
            Width = 89
            Height = 13
            Caption = 'Casas apli. desco.:'
            FocusControl = EdCasasApliDesco
          end
          object GroupBox1: TGroupBox
            Left = 4
            Top = 0
            Width = 636
            Height = 89
            Caption = ' Tipo de cadastro: '
            TabOrder = 0
            object CkCliente1_2: TCheckBox
              Left = 8
              Top = 20
              Width = 152
              Height = 17
              Caption = 'Cliente 1'
              TabOrder = 0
              OnClick = CkCliente1_2Click
            end
            object CkFornece1_2: TCheckBox
              Left = 164
              Top = 20
              Width = 152
              Height = 17
              Caption = 'Fornece 1'
              TabOrder = 4
              OnClick = CkCliente1_2Click
            end
            object CkFornece2_2: TCheckBox
              Left = 164
              Top = 36
              Width = 152
              Height = 17
              Caption = 'Fornece 2'
              TabOrder = 5
              Visible = False
              OnClick = CkCliente1_2Click
            end
            object CkFornece3_2: TCheckBox
              Left = 164
              Top = 52
              Width = 152
              Height = 17
              Caption = 'Fornece 3'
              TabOrder = 6
              Visible = False
              OnClick = CkCliente1_2Click
            end
            object CkFornece4_2: TCheckBox
              Left = 164
              Top = 68
              Width = 152
              Height = 17
              Caption = 'Fornece 4'
              TabOrder = 7
              Visible = False
              OnClick = CkCliente1_2Click
            end
            object CkTerceiro_2: TCheckBox
              Left = 476
              Top = 20
              Width = 152
              Height = 17
              Caption = 'Terceiro'
              TabOrder = 12
              OnClick = CkCliente1_2Click
            end
            object CkCliente2_2: TCheckBox
              Left = 8
              Top = 36
              Width = 152
              Height = 17
              Caption = 'Cliente 2'
              TabOrder = 1
              Visible = False
              OnClick = CkCliente1_2Click
            end
            object CkFornece5_2: TCheckBox
              Left = 320
              Top = 20
              Width = 152
              Height = 17
              Caption = 'Fornece 5'
              TabOrder = 8
              Visible = False
              OnClick = CkCliente1_2Click
            end
            object CkFornece6_2: TCheckBox
              Left = 320
              Top = 36
              Width = 152
              Height = 17
              Caption = 'Fornece 6'
              TabOrder = 9
              Visible = False
              OnClick = CkCliente1_2Click
            end
            object CkCliente3_2: TCheckBox
              Left = 8
              Top = 52
              Width = 152
              Height = 17
              Caption = 'Cliente 3'
              TabOrder = 2
              Visible = False
              OnClick = CkCliente1_2Click
            end
            object CkCliente4_2: TCheckBox
              Left = 8
              Top = 68
              Width = 152
              Height = 17
              Caption = 'Cliente 4'
              TabOrder = 3
              Visible = False
              OnClick = CkCliente1_2Click
            end
            object CkFornece7_2: TCheckBox
              Left = 320
              Top = 52
              Width = 152
              Height = 17
              Caption = 'Fornece 7'
              TabOrder = 10
              Visible = False
              OnClick = CkCliente1_1Click
            end
            object CkFornece8_2: TCheckBox
              Left = 320
              Top = 68
              Width = 152
              Height = 17
              Caption = 'Fornece 8'
              TabOrder = 11
              Visible = False
              OnClick = CkCliente1_1Click
            end
          end
          object RGTipo_2: TRadioGroup
            Left = 644
            Top = 0
            Width = 104
            Height = 89
            Caption = ' Tipo de cadastro: '
            ItemIndex = 1
            Items.Strings = (
              'Empresa'
              'Pessoal')
            TabOrder = 1
            OnClick = CkCliente1_2Click
          end
          object RGRecibo: TRadioGroup
            Left = 752
            Top = 0
            Width = 237
            Height = 145
            Caption = ' Emiss'#227'o de recibos: '
            Items.Strings = (
              'N'#227'o necessita de recibo'
              'Emitir um recibo no total'
              'Emitir um recibo para cada parcela'
              'Emitir recibos mensais. dia')
            TabOrder = 2
            OnClick = RGReciboClick
          end
          object GroupBox2: TGroupBox
            Left = 4
            Top = 92
            Width = 281
            Height = 61
            Caption = ' Ajuda de custos pela empresa: '
            TabOrder = 3
            object Label53: TLabel
              Left = 10
              Top = 16
              Width = 27
              Height = 13
              Caption = 'Valor:'
              FocusControl = EdAjudaV
            end
            object Label54: TLabel
              Left = 114
              Top = 16
              Width = 66
              Height = 13
              Caption = 'Porcentagem:'
              FocusControl = EdAjudaP
            end
            object EdAjudaV: TdmkEdit
              Left = 9
              Top = 32
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = EdAjudaVExit
            end
            object EdAjudaP: TdmkEdit
              Left = 113
              Top = 32
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMax = '100'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = EdAjudaPExit
            end
          end
          object CBGrupo: TDBLookupComboBox
            Left = 292
            Top = 106
            Width = 452
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEntiGrupos
            TabOrder = 4
          end
          object EdInformacoes: TdmkEdit
            Left = 292
            Top = 144
            Width = 452
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CkDono: TCheckBox
            Left = 755
            Top = 145
            Width = 240
            Height = 21
            Caption = 'Este '#233' o cadasrtro do usu'#225'rio deste aplicativo.'
            TabOrder = 6
          end
          object RGSituacao: TRadioGroup
            Left = 4
            Top = 156
            Width = 281
            Height = 93
            Caption = ' Situa'#231#227'o do cadastro: '
            ItemIndex = 0
            Items.Strings = (
              'Ativo'
              'Stand by'
              'Inativo')
            TabOrder = 7
          end
          object EdNivel: TdmkEdit
            Left = 291
            Top = 184
            Width = 23
            Height = 21
            CharCase = ecUpperCase
            MaxLength = 1
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'C'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'C'
            ValWarn = False
          end
          object CBAccount: TDBLookupComboBox
            Left = 320
            Top = 184
            Width = 461
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsAccounts
            TabOrder = 9
          end
          object EdRolComis: TdmkEditCB
            Left = 291
            Top = 225
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBRolComis
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBRolComis: TdmkDBLookupComboBox
            Left = 349
            Top = 225
            Width = 432
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEquiCom
            TabOrder = 11
            dmkEditCB = EdRolComis
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdNome1: TdmkEdit
            Left = 4
            Top = 268
            Width = 232
            Height = 21
            TabOrder = 12
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdNome3: TdmkEdit
            Left = 4
            Top = 308
            Width = 232
            Height = 21
            TabOrder = 13
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CBMotivo: TDBLookupComboBox
            Left = 4
            Top = 346
            Width = 233
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Descricao'
            ListSource = DsMotivos
            TabOrder = 14
          end
          object TPNatal1: TdmkEditDateTimePicker
            Left = 240
            Top = 268
            Width = 100
            Height = 21
            Date = 44698.000000000000000000
            Time = 0.992729421297553900
            TabOrder = 15
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPNatal3: TdmkEditDateTimePicker
            Left = 240
            Top = 308
            Width = 100
            Height = 21
            Date = 44698.000000000000000000
            Time = 0.992729421297553900
            TabOrder = 16
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkAgenda: TCheckBox
            Left = 243
            Top = 350
            Width = 62
            Height = 17
            Caption = 'Agenda.'
            TabOrder = 17
          end
          object EdNome2: TdmkEdit
            Left = 344
            Top = 268
            Width = 232
            Height = 21
            TabOrder = 18
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdNome4: TdmkEdit
            Left = 344
            Top = 308
            Width = 232
            Height = 21
            TabOrder = 19
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object TPNatal4: TdmkEditDateTimePicker
            Left = 580
            Top = 308
            Width = 100
            Height = 21
            Date = 44698.000000000000000000
            Time = 0.992729421297553900
            TabOrder = 20
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkSenhaQuer: TCheckBox
            Left = 684
            Top = 270
            Width = 97
            Height = 17
            Caption = 'Quer Senha.'
            TabOrder = 21
          end
          object EdSenha1: TdmkEdit
            Left = 684
            Top = 308
            Width = 94
            Height = 21
            MaxLength = 6
            TabOrder = 22
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdComissao: TdmkEdit
            Left = 413
            Top = 346
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 23
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdDesco: TdmkEdit
            Left = 517
            Top = 346
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 24
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCasasApliDesco: TdmkEdit
            Left = 613
            Top = 346
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 25
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '6'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object TPNatal2: TdmkEditDateTimePicker
            Left = 580
            Top = 268
            Width = 100
            Height = 21
            Date = 44698.000000000000000000
            Time = 0.992729421297553900
            TabOrder = 26
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object EdDiaRecibo: TdmkEdit
            Left = 904
            Top = 116
            Width = 49
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            MaxLength = 2
            TabOrder = 27
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdDiaReciboExit
          end
        end
      end
      object TabSheet8: TTabSheet
        Caption = ' Dados banc'#225'rios '
        ImageIndex = 8
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 100
          Height = 478
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitHeight = 483
          object BtIncluiCtas: TBitBtn
            Tag = 10
            Left = 5
            Top = 5
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Inclui novo registro'
            Caption = '&Inclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            TabStop = False
            OnClick = BtIncluiCtasClick
          end
          object BtAlteraCtas: TBitBtn
            Tag = 11
            Left = 5
            Top = 47
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Exclui registro atual'
            Caption = '&Altera'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            TabStop = False
            OnClick = BtAlteraCtasClick
          end
          object BtExcluiCtas: TBitBtn
            Tag = 12
            Left = 5
            Top = 90
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Exclui registro atual'
            Caption = 'Excl&ui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            TabStop = False
            OnClick = BtExcluiCtasClick
          end
        end
        object Panel7: TPanel
          Left = 100
          Top = 0
          Width = 900
          Height = 478
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Panel7'
          ParentBackground = False
          TabOrder = 1
          ExplicitWidth = 902
          ExplicitHeight = 483
          object Panel6: TPanel
            Left = 0
            Top = 429
            Width = 900
            Height = 49
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitTop = 434
            ExplicitWidth = 902
            object Label127: TLabel
              Left = 0
              Top = 5
              Width = 97
              Height = 13
              Caption = 'Carteira preferencial:'
            end
            object EdCarteira: TdmkEditCB
              Left = 0
              Top = 21
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCarteira
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCarteira: TdmkDBLookupComboBox
              Left = 58
              Top = 21
              Width = 603
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCarteiras
              TabOrder = 1
              dmkEditCB = EdCarteira
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
          object DBGrid2: TDBGrid
            Left = 0
            Top = 0
            Width = 900
            Height = 429
            Align = alClient
            DataSource = DsEntiCtas
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Width = 53
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEBANCO'
                Title.Caption = 'Banco'
                Width = 278
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Agencia'
                Title.Caption = 'Ag'#234'ncia'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DAC_A'
                Title.Caption = '[A]'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ContaCor'
                Title.Caption = 'Conta'
                Width = 73
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DAC_C'
                Title.Caption = '[C]'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DAC_AC'
                Title.Caption = '[AC]'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ContaTip'
                Title.Caption = 'Tipo'
                Visible = True
              end>
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' &Observa'#231#245'es '
        ImageIndex = 3
        object Memo: TMemo
          Left = 0
          Top = 0
          Width = 1002
          Height = 481
          Align = alClient
          Color = clWhite
          TabOrder = 0
        end
      end
      object TabSheet5: TTabSheet
        Caption = ' Endere'#231'o de co&bran'#231'a '
        ImageIndex = 4
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 478
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 1002
          ExplicitHeight = 483
          object Label62: TLabel
            Left = 4
            Top = 0
            Width = 24
            Height = 13
            Caption = 'CEP:'
            FocusControl = EdCCEP
          end
          object Label96: TLabel
            Left = 104
            Top = 0
            Width = 77
            Height = 13
            Caption = 'Tipo logradouro:'
          end
          object Label56: TLabel
            Left = 248
            Top = 0
            Width = 84
            Height = 13
            Caption = 'Nome logradouro:'
            FocusControl = EdCRua
          end
          object Label57: TLabel
            Left = 540
            Top = 0
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
            FocusControl = EdCNumero
          end
          object Label58: TLabel
            Left = 628
            Top = 0
            Width = 67
            Height = 13
            Caption = 'Complemento:'
            FocusControl = EdCCompl
          end
          object Label59: TLabel
            Left = 4
            Top = 40
            Width = 30
            Height = 13
            Caption = 'Bairro:'
            FocusControl = EdCBairro
          end
          object Label60: TLabel
            Left = 216
            Top = 40
            Width = 36
            Height = 13
            Caption = 'Cidade:'
            FocusControl = EdCCidade
          end
          object Label52: TLabel
            Left = 428
            Top = 40
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label63: TLabel
            Left = 460
            Top = 40
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
            FocusControl = EdCPais
          end
          object Label64: TLabel
            Left = 4
            Top = 80
            Width = 45
            Height = 13
            Caption = 'Telefone:'
            FocusControl = EdCTel
          end
          object Label67: TLabel
            Left = 128
            Top = 80
            Width = 20
            Height = 13
            Caption = 'Fax:'
            FocusControl = EdCFax
          end
          object Label66: TLabel
            Left = 376
            Top = 80
            Width = 40
            Height = 13
            Caption = 'Contato:'
            FocusControl = EdCContato
          end
          object Label65: TLabel
            Left = 252
            Top = 80
            Width = 35
            Height = 13
            Caption = 'Celular:'
            FocusControl = EdCCel
          end
          object EdCCEP: TdmkEdit
            Left = 4
            Top = 16
            Width = 76
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtCEP
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnEnter = EdCCEPEnter
            OnExit = EdCCEPExit
          end
          object BtCEP3: TBitBtn
            Left = 80
            Top = 16
            Width = 21
            Height = 21
            HelpContext = 90000
            Caption = '...'
            TabOrder = 1
            TabStop = False
            OnClick = BtCEP3Click
          end
          object CBCLograd: TdmkDBLookupComboBox
            Left = 144
            Top = 16
            Width = 100
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCListaLograd
            TabOrder = 3
            dmkEditCB = EdCLograd
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCRua: TdmkEdit
            Left = 248
            Top = 16
            Width = 289
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCNumero: TdmkEdit
            Left = 540
            Top = 16
            Width = 85
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCCompl: TdmkEdit
            Left = 628
            Top = 16
            Width = 152
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCBairro: TdmkEdit
            Left = 4
            Top = 56
            Width = 209
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCCidade: TdmkEdit
            Left = 216
            Top = 56
            Width = 209
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCPais: TdmkEdit
            Left = 460
            Top = 56
            Width = 320
            Height = 21
            TabOrder = 10
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCTel: TdmkEdit
            Left = 4
            Top = 96
            Width = 121
            Height = 21
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtTelCurto
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCFax: TdmkEdit
            Left = 128
            Top = 96
            Width = 121
            Height = 21
            TabOrder = 12
            FormatType = dmktfString
            MskType = fmtTelCurto
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCCel: TdmkEdit
            Left = 252
            Top = 96
            Width = 121
            Height = 21
            TabOrder = 13
            FormatType = dmktfString
            MskType = fmtTelCurto
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCContato: TdmkEdit
            Left = 376
            Top = 96
            Width = 405
            Height = 21
            TabOrder = 14
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCLograd: TdmkEditCB
            Left = 104
            Top = 16
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCLograd
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdCUF: TdmkEdit
            Left = 428
            Top = 56
            Width = 28
            Height = 21
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PUF'
            UpdCampo = 'PUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' Local de e&ntrega '
        ImageIndex = 5
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 125
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel12: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 125
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label131: TLabel
              Left = 4
              Top = 0
              Width = 24
              Height = 13
              Caption = 'CEP:'
              FocusControl = EdLCEP
            end
            object Label95: TLabel
              Left = 104
              Top = 0
              Width = 77
              Height = 13
              Caption = 'Tipo logradouro:'
            end
            object Label55: TLabel
              Left = 248
              Top = 0
              Width = 84
              Height = 13
              Caption = 'Nome logradouro:'
              FocusControl = EdLRua
            end
            object Label74: TLabel
              Left = 544
              Top = 0
              Width = 40
              Height = 13
              Caption = 'N'#250'mero:'
              FocusControl = EdLNumero
            end
            object Label75: TLabel
              Left = 632
              Top = 0
              Width = 67
              Height = 13
              Caption = 'Complemento:'
              FocusControl = EdLCompl
            end
            object Label61: TLabel
              Left = 4
              Top = 40
              Width = 30
              Height = 13
              Caption = 'Bairro:'
              FocusControl = EdLBairro
            end
            object Label68: TLabel
              Left = 4
              Top = 80
              Width = 45
              Height = 13
              Caption = 'Telefone:'
              FocusControl = EdLTel
            end
            object Label69: TLabel
              Left = 128
              Top = 80
              Width = 20
              Height = 13
              Caption = 'Fax:'
              FocusControl = EdLFax
            end
            object Label70: TLabel
              Left = 216
              Top = 40
              Width = 36
              Height = 13
              Caption = 'Cidade:'
              FocusControl = EdLCidade
            end
            object Label71: TLabel
              Left = 252
              Top = 80
              Width = 35
              Height = 13
              Caption = 'Celular:'
              FocusControl = EdLCel
            end
            object Label72: TLabel
              Left = 376
              Top = 80
              Width = 40
              Height = 13
              Caption = 'Contato:'
              FocusControl = EdLContato
            end
            object Label73: TLabel
              Left = 428
              Top = 40
              Width = 17
              Height = 13
              Caption = 'UF:'
            end
            object Label77: TLabel
              Left = 460
              Top = 40
              Width = 25
              Height = 13
              Caption = 'Pa'#237's:'
              FocusControl = EdLPais
            end
            object EdLCEP: TdmkEdit
              Left = 4
              Top = 16
              Width = 76
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnEnter = EdLCEPEnter
              OnExit = EdLCEPExit
            end
            object EdCEP4: TBitBtn
              Left = 80
              Top = 16
              Width = 21
              Height = 21
              HelpContext = 90000
              Caption = '...'
              TabOrder = 1
              TabStop = False
              OnClick = EdCEP4Click
            end
            object CBLLograd: TdmkDBLookupComboBox
              Left = 144
              Top = 16
              Width = 100
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsLListaLograd
              TabOrder = 3
              dmkEditCB = EdLLograd
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdLRua: TdmkEdit
              Left = 248
              Top = 16
              Width = 293
              Height = 21
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLNumero: TdmkEdit
              Left = 544
              Top = 16
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdLCompl: TdmkEdit
              Left = 632
              Top = 16
              Width = 148
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLBairro: TdmkEdit
              Left = 4
              Top = 56
              Width = 209
              Height = 21
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLTel: TdmkEdit
              Left = 4
              Top = 96
              Width = 121
              Height = 21
              TabOrder = 11
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLFax: TdmkEdit
              Left = 128
              Top = 96
              Width = 121
              Height = 21
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLCidade: TdmkEdit
              Left = 216
              Top = 56
              Width = 209
              Height = 21
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLCel: TdmkEdit
              Left = 252
              Top = 96
              Width = 121
              Height = 21
              TabOrder = 13
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLContato: TdmkEdit
              Left = 376
              Top = 96
              Width = 404
              Height = 21
              TabOrder = 14
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLPais: TdmkEdit
              Left = 460
              Top = 56
              Width = 320
              Height = 21
              TabOrder = 10
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLUF: TdmkEdit
              Left = 428
              Top = 56
              Width = 28
              Height = 21
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtUF
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'PUF'
              UpdCampo = 'PUF'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLLograd: TdmkEditCB
              Left = 104
              Top = 16
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBLLograd
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
          end
        end
        object Panel2: TPanel
          Left = 0
          Top = 125
          Width = 1000
          Height = 353
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object DBGrid1: TDBGrid
            Left = 497
            Top = 0
            Width = 503
            Height = 353
            Align = alRight
            DataSource = DsEntiTransp
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Transp'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENTIDADE'
                Title.Caption = 'Nome Transportadora'
                Width = 400
                Visible = True
              end>
          end
          object BtExclui: TBitBtn
            Tag = 106
            Left = 184
            Top = 67
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Exclui registro atual'
            Caption = '&Retira'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            TabStop = False
            OnClick = BtExcluiClick
          end
          object BtInclui: TBitBtn
            Tag = 105
            Left = 184
            Top = 21
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Inclui novo registro'
            Caption = '&Adiciona'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            TabStop = False
            OnClick = BtIncluiClick
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = ' Cr'#233'dito '
        ImageIndex = 6
        object PainelEntiCreds2: TPanel
          Left = 0
          Top = 330
          Width = 1002
          Height = 151
          Align = alBottom
          TabOrder = 0
          object Grade1: TStringGrid
            Left = 1
            Top = 1
            Width = 1000
            Height = 149
            Align = alClient
            DefaultRowHeight = 19
            TabOrder = 0
            Visible = False
            ColWidths = (
              64
              64
              64
              64
              64)
            RowHeights = (
              19
              19
              19
              19
              19)
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1002
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Label42: TLabel
            Left = 12
            Top = 4
            Width = 91
            Height = 13
            Caption = 'Cr'#233'dito autom'#225'tico:'
            FocusControl = EdLimiCred
          end
          object Label136: TLabel
            Left = 116
            Top = 4
            Width = 344
            Height = 13
            Caption = 
              'CBE = Compensa'#231#227'o banc'#225'ria especial fixa limitada ao CBE (0 = no' +
              'rmal).'
          end
          object EdLimiCred: TdmkEdit
            Left = 12
            Top = 18
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCBE: TdmkEdit
            Left = 116
            Top = 18
            Width = 33
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object CkSCB: TCheckBox
            Left = 152
            Top = 20
            Width = 325
            Height = 17
            Caption = 'Cliente sem compensa'#231#227'o banc'#225'ria (Vale o CBE se o CBE > 0).'
            TabOrder = 2
          end
        end
        object PainelEntiCreds1: TPanel
          Left = 0
          Top = 48
          Width = 1002
          Height = 88
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          Visible = False
          object Label111: TLabel
            Left = 428
            Top = 4
            Width = 73
            Height = 13
            Caption = 'Tarifa cust'#243'dia:'
            FocusControl = EdCustodia
            Visible = False
          end
          object Label116: TLabel
            Left = 324
            Top = 44
            Width = 73
            Height = 13
            Caption = 'Taxa SERASA:'
            FocusControl = EdTxSerasa
            Visible = False
          end
          object Label117: TLabel
            Left = 428
            Top = 44
            Width = 45
            Height = 13
            Caption = 'Taxa AR:'
            FocusControl = EdTxAR
            Visible = False
          end
          object Label112: TLabel
            Left = 532
            Top = 4
            Width = 78
            Height = 13
            Caption = 'Tarifa cobran'#231'a:'
            FocusControl = EdCobranca
            Visible = False
          end
          object Label107: TLabel
            Left = 12
            Top = 4
            Width = 76
            Height = 13
            Caption = 'Fator compra %:'
            FocusControl = EdFatorCompra
          end
          object Label108: TLabel
            Left = 116
            Top = 4
            Width = 68
            Height = 13
            Caption = 'Ad Valorem %:'
            FocusControl = EdAdValorem
          end
          object Label113: TLabel
            Left = 12
            Top = 44
            Width = 25
            Height = 13
            Caption = 'R.U.:'
            FocusControl = EdR_U
            Visible = False
          end
          object Label109: TLabel
            Left = 220
            Top = 4
            Width = 57
            Height = 13
            Caption = 'D+ Cheque:'
            FocusControl = EdDMaisC
          end
          object Label110: TLabel
            Left = 324
            Top = 4
            Width = 65
            Height = 13
            Caption = 'D+ Duplicata:'
            FocusControl = EdDMaisD
          end
          object Label114: TLabel
            Left = 116
            Top = 44
            Width = 61
            Height = 13
            Caption = 'Tarifa postal:'
            FocusControl = EdTarifaPostal
            Visible = False
          end
          object Label115: TLabel
            Left = 220
            Top = 44
            Width = 78
            Height = 13
            Caption = 'Tarifa opera'#231#227'o:'
            FocusControl = EdTarifaOperacao
            Visible = False
          end
          object EdCustodia: TdmkEdit
            Left = 428
            Top = 18
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTxSerasa: TdmkEdit
            Left = 324
            Top = 58
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTxAR: TdmkEdit
            Left = 428
            Top = 58
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCobranca: TdmkEdit
            Left = 532
            Top = 18
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdFatorCompra: TdmkEdit
            Left = 12
            Top = 18
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdAdValorem: TdmkEdit
            Left = 116
            Top = 18
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdR_U: TdmkEdit
            Left = 12
            Top = 58
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdDMaisC: TdmkEdit
            Left = 220
            Top = 18
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdDMaisD: TdmkEdit
            Left = 324
            Top = 18
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTarifaPostal: TdmkEdit
            Left = 116
            Top = 58
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTarifaOperacao: TdmkEdit
            Left = 220
            Top = 58
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 49
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 10
          Top = 4
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
          FocusControl = EdCodigo
        end
        object Label13: TLabel
          Left = 80
          Top = 4
          Width = 45
          Height = 13
          Caption = 'Cadastro:'
        end
        object Label79: TLabel
          Left = 284
          Top = 4
          Width = 67
          Height = 13
          Caption = 'Pessoa f'#237'sica:'
          FocusControl = EdNome_2
        end
        object Label80: TLabel
          Left = 536
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = EdRazao2
        end
        object EdCodigo: TdmkEdit
          Left = 10
          Top = 20
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCodigoChange
        end
        object TPCadastro: TdmkEditDateTimePicker
          Left = 80
          Top = 20
          Width = 112
          Height = 21
          Date = 37843.000000000000000000
          Time = 0.992729421297553900
          Enabled = False
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdNome_2: TdmkEdit
          Left = 284
          Top = 20
          Width = 249
          Height = 21
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdRazao2: TdmkEdit
          Left = 536
          Top = 20
          Width = 249
          Height = 21
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object Panel9: TPanel
        Left = 792
        Top = 0
        Width = 216
        Height = 49
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label130: TLabel
          Left = 4
          Top = 4
          Width = 62
          Height = 13
          Caption = 'Sonoriza'#231#227'o:'
        end
        object DBEdit1: TDBEdit
          Left = 4
          Top = 20
          Width = 201
          Height = 21
          DataField = 'Nome'
          TabOrder = 0
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 603
    Width = 1008
    Height = 63
    Align = alBottom
    TabOrder = 1
    object Panel13: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 46
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel16: TPanel
        Left = 848
        Top = 0
        Width = 156
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 16
          Top = 0
          Width = 120
          Height = 40
          Hint = 'Desiste da inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object Panel17: TPanel
        Left = 0
        Top = 0
        Width = 152
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 16
          Top = 0
          Width = 120
          Height = 40
          Hint = 'Confirma inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
      object GBAvisos1: TGroupBox
        Left = 152
        Top = 0
        Width = 696
        Height = 46
        Align = alClient
        Caption = ' Avisos: '
        TabOrder = 2
        object Panel18: TPanel
          Left = 2
          Top = 15
          Width = 692
          Height = 29
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 13
            Top = 2
            Width = 12
            Height = 17
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 12
            Top = 1
            Width = 12
            Height = 17
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 543
        Height = 32
        Caption = 'Edi'#231#227'o de Cadastro de Pessoas e Empresas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 543
        Height = 32
        Caption = 'Edi'#231#227'o de Cadastro de Pessoas e Empresas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 543
        Height = 32
        Caption = 'Edi'#231#227'o de Cadastro de Pessoas e Empresas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DsEUfs: TDataSource
    DataSet = QrUFs
    Left = 720
    Top = 2
  end
  object DsPUFs: TDataSource
    DataSet = QrUFs
    Left = 748
    Top = 2
  end
  object QrControle: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Dono FROM controle')
    Left = 636
    Top = 1
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
      Required = True
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLocCalcFields
    SQL.Strings = (
      'SELECT * FROM entidades'
      'WHERE Codigo=:P0')
    Left = 664
    Top = 1
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrLocFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrLocRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrLocRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrLocPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrLocMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrLocCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrLocIE: TWideStringField
      FieldName = 'IE'
      Size = 15
    end
    object QrLocNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrLocApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrLocCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrLocRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrLocERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrLocECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrLocEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrLocECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 15
    end
    object QrLocEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrLocEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrLocETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrLocETe2: TWideStringField
      FieldName = 'ETe2'
    end
    object QrLocETe3: TWideStringField
      FieldName = 'ETe3'
    end
    object QrLocECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrLocEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrLocEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrLocEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrLocENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrLocPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrLocPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrLocPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrLocPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 15
    end
    object QrLocPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrLocPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrLocPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrLocPTe2: TWideStringField
      FieldName = 'PTe2'
    end
    object QrLocPTe3: TWideStringField
      FieldName = 'PTe3'
    end
    object QrLocPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrLocPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrLocPEMail: TWideStringField
      FieldName = 'PEMail'
      Size = 100
    end
    object QrLocPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrLocPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrLocSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrLocResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrLocProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrLocCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrLocRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrLocDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrLocAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrLocAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrLocCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrLocCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrLocFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrLocFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrLocFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrLocFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrLocTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrLocCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrLocInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrLocLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrLocVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrLocMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrLocObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrLocETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrLocETE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE2_TXT'
      Size = 30
      Calculated = True
    end
    object QrLocETE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE3_TXT'
      Size = 30
      Calculated = True
    end
    object QrLocEFax_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EFax_TXT'
      Calculated = True
    end
    object QrLocECEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEL_TXT'
      Calculated = True
    end
    object QrLocECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrLocPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrLocGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrLocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLocUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLocCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrLocCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrLocCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrLocCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 15
    end
    object QrLocCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrLocCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrLocCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrLocCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrLocCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrLocCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrLocCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrLocLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrLocLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrLocLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrLocLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 15
    end
    object QrLocLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrLocLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrLocLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrLocLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrLocLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrLocLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrLocLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrLocComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrLocDataCad: TDateField
      FieldName = 'DataCad'
      Required = True
    end
    object QrLocENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrLocPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrLocELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrLocPLograd: TSmallintField
      FieldName = 'PLograd'
    end
  end
  object QrUFs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM ufs'
      'ORDER BY Nome')
    Left = 692
    Top = 1
    object QrUFsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrUFsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 2
    end
  end
  object QrEntiTransp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT et.*,'
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMEENTIDADE '
      'FROM entitransp et, entidades en'
      'WHERE et.Transp=en.Codigo'
      'AND et.Codigo=:P0'
      'ORDER BY Ordem'
      '')
    Left = 916
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTranspCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiTranspConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrEntiTranspOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEntiTranspTransp: TIntegerField
      FieldName = 'Transp'
    end
    object QrEntiTranspNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEntiTransp: TDataSource
    DataSet = QrEntiTransp
    Left = 944
    Top = 132
  end
  object PMCEP: TPopupMenu
    Left = 944
    Top = 352
    object Descobrir1: TMenuItem
      Caption = '&Descobrir'
      OnClick = Descobrir1Click
    end
    object Verificar1: TMenuItem
      Caption = '&Verificar'
      OnClick = Verificar1Click
    end
  end
  object QrDuplic2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM entidades'
      'WHERE CNPJ=:P0'
      'AND Codigo <> :P1')
    Left = 916
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplic2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsEntiGrupos: TDataSource
    DataSet = QrEntiGrupos
    Left = 944
    Top = 160
  end
  object QrEntiGrupos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM entigrupos'
      'ORDER BY Nome')
    Left = 916
    Top = 160
    object QrEntiGruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiGruposNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEntiGruposDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEntiGruposDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntiGruposUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrEntiGruposUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrEntiGruposLk: TIntegerField
      FieldName = 'Lk'
    end
  end
  object DsAccounts: TDataSource
    DataSet = QrAccounts
    Left = 944
    Top = 188
  end
  object QrAccounts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEENTIDADE'
      '')
    Left = 916
    Top = 188
    object QrAccountsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAccountsNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrMotivos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM motivose'
      'ORDER BY Descricao')
    Left = 916
    Top = 324
    object QrMotivosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMotivosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
  end
  object DsMotivos: TDataSource
    DataSet = QrMotivos
    Left = 944
    Top = 324
  end
  object QrListaLograd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM listalograd'
      'ORDER BY Nome')
    Left = 208
    Top = 502
    object QrListaLogradCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaLogradNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 10
    end
  end
  object DsEListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 236
    Top = 502
  end
  object DsCListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 296
    Top = 502
  end
  object DsLListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 324
    Top = 502
  end
  object DsPListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 268
    Top = 502
  end
  object QrEmpresas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, en.RazaoSocial'
      'FROM entidades en'
      'WHERE en.Tipo=0'
      'ORDER BY en.RazaoSocial'
      '')
    Left = 916
    Top = 216
    object QrEmpresasRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 944
    Top = 216
  end
  object QrEstCivil: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM listaecivil'
      'ORDER BY Nome'
      '')
    Left = 684
    Top = 52
    object QrEstCivilCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstCivilNome: TWideStringField
      FieldName = 'Nome'
      Size = 10
    end
    object QrEstCivilLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEstCivilDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEstCivilDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEstCivilUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEstCivilUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsEstCivil: TDataSource
    DataSet = QrEstCivil
    Left = 712
    Top = 52
  end
  object QrEntiCtas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.*, ban.Nome NOMEBANCO'
      'FROM entictas cta'
      'LEFT JOIN entidades ent ON ent.Codigo=cta.Codigo'
      'LEFT JOIN bancos ban ON ban.Codigo=cta.Banco'
      'WHERE cta.Codigo=:P0'
      'ORDER BY cta.Ordem')
    Left = 916
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCtasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiCtasControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiCtasBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrEntiCtasAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrEntiCtasContaCor: TWideStringField
      FieldName = 'ContaCor'
      Size = 10
    end
    object QrEntiCtasDAC_A: TWideStringField
      FieldName = 'DAC_A'
      Size = 1
    end
    object QrEntiCtasDAC_C: TWideStringField
      FieldName = 'DAC_C'
      Size = 1
    end
    object QrEntiCtasDAC_AC: TWideStringField
      FieldName = 'DAC_AC'
      Size = 1
    end
    object QrEntiCtasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEntiCtasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEntiCtasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntiCtasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEntiCtasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEntiCtasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEntiCtasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEntiCtasOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEntiCtasContaTip: TWideStringField
      FieldName = 'ContaTip'
      Size = 10
    end
    object QrEntiCtasNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
  end
  object DsEntiCtas: TDataSource
    DataSet = QrEntiCtas
    Left = 944
    Top = 300
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 916
    Top = 272
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 944
    Top = 272
  end
  object DsEquiCom: TDataSource
    DataSet = QrEquiCom
    Left = 944
    Top = 244
  end
  object QrEquiCom: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM equicom'
      '')
    Left = 916
    Top = 244
    object QrEquiComCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEquiComNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrNomes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOME_ENT,'
      'SOUNDEX(IF(Tipo=0, RazaoSocial, Nome)) SONORIZACAO'
      'FROM entidades'
      'WHERE IF(Tipo=0, RazaoSocial, Nome) LIKE :P0'
      'ORDER BY NOME_ENT')
    Left = 840
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNomesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNomesNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Required = True
      Size = 100
    end
    object QrNomesSONORIZACAO: TWideStringField
      FieldName = 'SONORIZACAO'
      Required = True
      Size = 100
    end
  end
  object DsNomes: TDataSource
    DataSet = QrNomes
    Left = 868
    Top = 128
  end
  object QrSom: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SOUNDEX(:P0) Nome')
    Left = 840
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomNome: TWideStringField
      FieldName = 'Nome'
      Size = 4
    end
  end
end
