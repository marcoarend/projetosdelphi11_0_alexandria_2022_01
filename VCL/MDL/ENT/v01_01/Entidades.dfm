object FmEntidades: TFmEntidades
  Left = 310
  Top = 186
  Caption = 'ENT-GEREN-001 :: Pessoas e Empresas'
  ClientHeight = 662
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label115: TLabel
    Left = 228
    Top = 240
    Width = 19
    Height = 13
    Caption = 'RG:'
  end
  object PainelDados: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 546
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 546
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 89
        Width = 1008
        Height = 457
        ActivePage = TabSheet6
        Align = alClient
        TabOrder = 0
        OnChange = PageControl1Change
        object TabSheet1: TTabSheet
          Caption = ' &Pessoal '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 789
            Height = 431
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label25: TLabel
              Left = 4
              Top = 0
              Width = 31
              Height = 13
              Caption = 'Nome:'
              FocusControl = DBEdit25
            end
            object Label26: TLabel
              Left = 392
              Top = 0
              Width = 38
              Height = 13
              Caption = 'Apelido:'
              FocusControl = DBEdit26
            end
            object Label48: TLabel
              Left = 652
              Top = 0
              Width = 27
              Height = 13
              Caption = 'Sexo:'
              FocusControl = DBEdit48
            end
            object Label135: TLabel
              Left = 688
              Top = 0
              Width = 57
              Height = 13
              Caption = 'Estado civil:'
              FocusControl = DBEdit11
            end
            object Label27: TLabel
              Left = 4
              Top = 40
              Width = 18
              Height = 13
              Caption = 'Pai:'
              FocusControl = DBEdit27
            end
            object Label28: TLabel
              Left = 228
              Top = 40
              Width = 24
              Height = 13
              Caption = 'M'#227'e:'
              FocusControl = DBEdit28
            end
            object Label47: TLabel
              Left = 452
              Top = 40
              Width = 206
              Height = 13
              Caption = 'Respons'#225'vel (cadastro de menor de idade):'
              FocusControl = DBEdit47
            end
            object Label114: TLabel
              Left = 668
              Top = 40
              Width = 88
              Height = 13
              Caption = 'CPF Respons'#225'vel:'
              FocusControl = DBEdit2
            end
            object Label29: TLabel
              Left = 4
              Top = 80
              Width = 23
              Height = 13
              Caption = 'CPF:'
              FocusControl = DBEdit29
            end
            object Label30: TLabel
              Left = 120
              Top = 80
              Width = 19
              Height = 13
              Caption = 'RG:'
              FocusControl = DBEdit30
            end
            object Label116: TLabel
              Left = 224
              Top = 80
              Width = 39
              Height = 13
              Caption = 'Emissor:'
              FocusControl = DBEdit30
            end
            object Label131: TLabel
              Left = 280
              Top = 80
              Width = 42
              Height = 13
              Caption = 'Emiss'#227'o:'
              FocusControl = DBEdit7
            end
            object Label32: TLabel
              Left = 456
              Top = 80
              Width = 57
              Height = 13
              Caption = 'Logradouro:'
              FocusControl = DBEdit32
            end
            object Label31: TLabel
              Left = 356
              Top = 80
              Width = 92
              Height = 13
              Caption = 'Tipo de logradouro:'
              FocusControl = DBEdit31
            end
            object Label33: TLabel
              Left = 732
              Top = 80
              Width = 40
              Height = 13
              Caption = 'N'#250'mero:'
              FocusControl = DBEdit33
            end
            object Label34: TLabel
              Left = 4
              Top = 120
              Width = 67
              Height = 13
              Caption = 'Complemento:'
              FocusControl = DBEdit34
            end
            object Label38: TLabel
              Left = 4
              Top = 160
              Width = 17
              Height = 13
              Caption = 'UF:'
              FocusControl = DBEdit38
            end
            object Label41: TLabel
              Left = 44
              Top = 160
              Width = 24
              Height = 13
              Caption = 'CEP:'
              FocusControl = DBEdit38
            end
            object Label42: TLabel
              Left = 132
              Top = 160
              Width = 25
              Height = 13
              Caption = 'Pa'#237's:'
              FocusControl = DBEdit41
            end
            object Label43: TLabel
              Left = 320
              Top = 160
              Width = 103
              Height = 13
              Caption = 'Telefone Residencial:'
              FocusControl = DBEdit42
            end
            object Label35: TLabel
              Left = 316
              Top = 120
              Width = 30
              Height = 13
              Caption = 'Bairro:'
              FocusControl = DBEdit35
            end
            object Label36: TLabel
              Left = 528
              Top = 120
              Width = 36
              Height = 13
              Caption = 'Cidade:'
              FocusControl = DBEdit36
            end
            object Label44: TLabel
              Left = 436
              Top = 160
              Width = 94
              Height = 13
              Caption = 'Telefone Comercial:'
              FocusControl = DBEdit43
            end
            object Label105: TLabel
              Left = 552
              Top = 160
              Width = 85
              Height = 13
              Caption = 'Telefone Contato:'
              FocusControl = DBEdit44
            end
            object Label106: TLabel
              Left = 668
              Top = 160
              Width = 35
              Height = 13
              Caption = 'Celular:'
              FocusControl = DBEdit107
            end
            object Label37: TLabel
              Left = 4
              Top = 200
              Width = 20
              Height = 13
              Caption = 'Fax:'
              FocusControl = DBEdit37
            end
            object Label40: TLabel
              Left = 120
              Top = 200
              Width = 31
              Height = 13
              Caption = 'E-mail:'
              FocusControl = DBEdit40
            end
            object Label46: TLabel
              Left = 472
              Top = 200
              Width = 40
              Height = 13
              Caption = 'Contato:'
              FocusControl = DBEdit46
            end
            object Label45: TLabel
              Left = 708
              Top = 200
              Width = 59
              Height = 13
              Caption = 'Nascimento:'
              FocusControl = DBEdit45
            end
            object Label87: TLabel
              Left = 708
              Top = 240
              Width = 59
              Height = 13
              Caption = 'Nascimento:'
              FocusControl = DBEdit88
            end
            object Label86: TLabel
              Left = 472
              Top = 240
              Width = 42
              Height = 13
              Caption = 'Conjuge:'
              FocusControl = DBEdit55
            end
            object Label130: TLabel
              Left = 272
              Top = 240
              Width = 71
              Height = 13
              Caption = 'Nacionalidade:'
              FocusControl = DBEdit6
            end
            object Label117: TLabel
              Left = 232
              Top = 240
              Width = 17
              Height = 13
              Caption = 'UF:'
              FocusControl = DBEdit4
            end
            object Label113: TLabel
              Left = 4
              Top = 240
              Width = 64
              Height = 13
              Caption = 'Cidade Natal:'
              FocusControl = DBEdit1
            end
            object Label51: TLabel
              Left = 4
              Top = 280
              Width = 46
              Height = 13
              Caption = 'Profiss'#227'o:'
              FocusControl = DBEdit51
            end
            object Label52: TLabel
              Left = 240
              Top = 280
              Width = 31
              Height = 13
              Caption = 'Cargo:'
              FocusControl = DBEdit52
            end
            object Label129: TLabel
              Left = 472
              Top = 280
              Width = 44
              Height = 13
              Caption = 'Empresa:'
              FocusControl = DBEdit5
            end
            object DBEdit25: TDBEdit
              Left = 4
              Top = 16
              Width = 385
              Height = 24
              DataField = 'Nome'
              DataSource = DsEntidades
              TabOrder = 0
            end
            object DBEdit26: TDBEdit
              Left = 392
              Top = 16
              Width = 257
              Height = 24
              DataField = 'Apelido'
              DataSource = DsEntidades
              TabOrder = 1
            end
            object DBEdit48: TDBEdit
              Left = 652
              Top = 16
              Width = 32
              Height = 24
              DataField = 'Sexo'
              DataSource = DsEntidades
              TabOrder = 2
            end
            object DBEdit11: TDBEdit
              Left = 688
              Top = 16
              Width = 93
              Height = 24
              DataField = 'NOMEECIVIL'
              DataSource = DsEntidades
              TabOrder = 3
            end
            object DBEdit27: TDBEdit
              Left = 4
              Top = 56
              Width = 220
              Height = 24
              DataField = 'Pai'
              DataSource = DsEntidades
              TabOrder = 4
            end
            object DBEdit28: TDBEdit
              Left = 228
              Top = 56
              Width = 220
              Height = 24
              DataField = 'Mae'
              DataSource = DsEntidades
              TabOrder = 5
            end
            object DBEdit47: TDBEdit
              Left = 452
              Top = 56
              Width = 213
              Height = 24
              DataField = 'Responsavel'
              DataSource = DsEntidades
              TabOrder = 6
            end
            object DBEdit2: TDBEdit
              Left = 668
              Top = 56
              Width = 113
              Height = 24
              DataField = 'CPF_PAI_TXT'
              DataSource = DsEntidades
              TabOrder = 7
            end
            object DBEdit29: TDBEdit
              Left = 4
              Top = 96
              Width = 113
              Height = 24
              DataField = 'PCPF_TXT'
              DataSource = DsEntidades
              TabOrder = 8
            end
            object DBEdit30: TDBEdit
              Left = 120
              Top = 96
              Width = 100
              Height = 24
              DataField = 'RG'
              DataSource = DsEntidades
              TabOrder = 9
            end
            object DBEdit3: TDBEdit
              Left = 224
              Top = 96
              Width = 53
              Height = 24
              DataField = 'SSP'
              DataSource = DsEntidades
              TabOrder = 10
            end
            object DBEdit7: TDBEdit
              Left = 280
              Top = 96
              Width = 72
              Height = 24
              DataField = 'DataRG'
              DataSource = DsEntidades
              TabOrder = 11
            end
            object DBEdit31: TDBEdit
              Left = 356
              Top = 96
              Width = 97
              Height = 24
              DataField = 'NOMEPLOGRAD'
              DataSource = DsEntidades
              TabOrder = 12
            end
            object DBEdit32: TDBEdit
              Left = 456
              Top = 96
              Width = 273
              Height = 24
              DataField = 'PRua'
              DataSource = DsEntidades
              TabOrder = 13
            end
            object DBEdit33: TDBEdit
              Left = 732
              Top = 96
              Width = 49
              Height = 24
              DataField = 'PNumero'
              DataSource = DsEntidades
              TabOrder = 14
            end
            object DBEdit34: TDBEdit
              Left = 4
              Top = 136
              Width = 309
              Height = 24
              DataField = 'PCompl'
              DataSource = DsEntidades
              TabOrder = 15
            end
            object DBEdit38: TDBEdit
              Left = 4
              Top = 176
              Width = 37
              Height = 24
              DataField = 'NOMEPUF'
              DataSource = DsEntidades
              TabOrder = 16
            end
            object DBEdit39: TDBEdit
              Left = 44
              Top = 176
              Width = 85
              Height = 24
              DataField = 'PCEP_TXT'
              DataSource = DsEntidades
              TabOrder = 17
            end
            object DBEdit41: TDBEdit
              Left = 132
              Top = 176
              Width = 185
              Height = 24
              DataField = 'PPais'
              DataSource = DsEntidades
              TabOrder = 18
            end
            object DBEdit42: TDBEdit
              Left = 320
              Top = 176
              Width = 113
              Height = 24
              DataField = 'PTE1_TXT'
              DataSource = DsEntidades
              TabOrder = 19
            end
            object DBEdit35: TDBEdit
              Left = 316
              Top = 136
              Width = 209
              Height = 24
              DataField = 'PBairro'
              DataSource = DsEntidades
              TabOrder = 20
            end
            object DBEdit36: TDBEdit
              Left = 528
              Top = 136
              Width = 253
              Height = 24
              DataField = 'PCidade'
              DataSource = DsEntidades
              TabOrder = 21
            end
            object DBEdit43: TDBEdit
              Left = 436
              Top = 176
              Width = 113
              Height = 24
              DataField = 'PTE2_TXT'
              DataSource = DsEntidades
              TabOrder = 22
            end
            object DBEdit44: TDBEdit
              Left = 552
              Top = 176
              Width = 113
              Height = 24
              DataField = 'PTE3_TXT'
              DataSource = DsEntidades
              TabOrder = 23
            end
            object DBEdit107: TDBEdit
              Left = 668
              Top = 176
              Width = 113
              Height = 24
              DataField = 'PCEL_TXT'
              DataSource = DsEntidades
              TabOrder = 24
            end
            object DBEdit37: TDBEdit
              Left = 4
              Top = 216
              Width = 113
              Height = 24
              DataField = 'PFAX_TXT'
              DataSource = DsEntidades
              TabOrder = 25
            end
            object DBEdit40: TDBEdit
              Left = 120
              Top = 216
              Width = 349
              Height = 24
              DataField = 'PEMail'
              DataSource = DsEntidades
              TabOrder = 26
            end
            object DBEdit46: TDBEdit
              Left = 472
              Top = 216
              Width = 233
              Height = 24
              DataField = 'PContato'
              DataSource = DsEntidades
              TabOrder = 27
            end
            object DBEdit45: TDBEdit
              Left = 708
              Top = 216
              Width = 72
              Height = 24
              DataField = 'PNatal'
              DataSource = DsEntidades
              TabOrder = 28
            end
            object DBEdit88: TDBEdit
              Left = 708
              Top = 256
              Width = 72
              Height = 24
              DataField = 'ConjugeNatal'
              DataSource = DsEntidades
              TabOrder = 29
            end
            object DBEdit55: TDBEdit
              Left = 472
              Top = 256
              Width = 233
              Height = 24
              DataField = 'ConjugeNome'
              DataSource = DsEntidades
              TabOrder = 30
            end
            object DBEdit6: TDBEdit
              Left = 272
              Top = 256
              Width = 197
              Height = 24
              DataField = 'Nacionalid'
              DataSource = DsEntidades
              TabOrder = 31
            end
            object DBEdit4: TDBEdit
              Left = 232
              Top = 256
              Width = 37
              Height = 24
              DataField = 'NOMENUF'
              DataSource = DsEntidades
              TabOrder = 32
            end
            object DBEdit1: TDBEdit
              Left = 4
              Top = 256
              Width = 225
              Height = 24
              DataField = 'CidadeNatal'
              DataSource = DsEntidades
              TabOrder = 33
            end
            object DBEdit51: TDBEdit
              Left = 4
              Top = 296
              Width = 233
              Height = 24
              DataField = 'Profissao'
              DataSource = DsEntidades
              TabOrder = 34
            end
            object DBEdit52: TDBEdit
              Left = 240
              Top = 296
              Width = 229
              Height = 24
              DataField = 'Cargo'
              DataSource = DsEntidades
              TabOrder = 35
            end
            object DBEdit5: TDBEdit
              Left = 472
              Top = 296
              Width = 309
              Height = 24
              DataField = 'NOMEEMPRESA'
              DataSource = DsEntidades
              TabOrder = 36
            end
          end
          object Panel10: TPanel
            Left = 789
            Top = 0
            Width = 213
            Height = 431
            Align = alClient
            ParentBackground = False
            TabOrder = 1
            object DBGrid3: TDBGrid
              Left = 1
              Top = 1
              Width = 211
              Height = 430
              Align = alClient
              DataSource = DsCondImov
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Unidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TIPO'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CND'
                  Title.Caption = 'Empresa'
                  Visible = True
                end>
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Emp&resa '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label2: TLabel
            Left = 4
            Top = 0
            Width = 64
            Height = 13
            Caption = 'Razao social:'
            FocusControl = DBEdit2
          end
          object Label3: TLabel
            Left = 436
            Top = 0
            Width = 71
            Height = 13
            Caption = 'Nome fantasia:'
            FocusControl = DBEdit3
          end
          object Label4: TLabel
            Left = 4
            Top = 40
            Width = 65
            Height = 13
            Caption = 'Respons'#225'vel:'
            FocusControl = DBEdit4
          end
          object Label5: TLabel
            Left = 392
            Top = 40
            Width = 65
            Height = 13
            Caption = 'Respons'#225'vel:'
            FocusControl = DBEdit5
          end
          object Label6: TLabel
            Left = 4
            Top = 80
            Width = 30
            Height = 13
            Caption = 'CNPJ:'
            FocusControl = DBEdit6
          end
          object Label7: TLabel
            Left = 120
            Top = 80
            Width = 19
            Height = 13
            Caption = 'I.E.:'
            FocusControl = DBEdit7
          end
          object Label8: TLabel
            Left = 224
            Top = 80
            Width = 92
            Height = 13
            Caption = 'Tipo de logradouro:'
            FocusControl = DBEdit8
          end
          object Label9: TLabel
            Left = 716
            Top = 80
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
            FocusControl = DBEdit9
          end
          object Label10: TLabel
            Left = 4
            Top = 120
            Width = 67
            Height = 13
            Caption = 'Complemento:'
            FocusControl = DBEdit10
          end
          object Label11: TLabel
            Left = 316
            Top = 120
            Width = 30
            Height = 13
            Caption = 'Bairro:'
            FocusControl = DBEdit11
          end
          object Label12: TLabel
            Left = 528
            Top = 120
            Width = 36
            Height = 13
            Caption = 'Cidade:'
            FocusControl = DBEdit12
          end
          object Label14: TLabel
            Left = 4
            Top = 160
            Width = 17
            Height = 13
            Caption = 'UF:'
            FocusControl = DBEdit14
          end
          object Label16: TLabel
            Left = 132
            Top = 160
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
            FocusControl = DBEdit16
          end
          object Label17: TLabel
            Left = 320
            Top = 160
            Width = 54
            Height = 13
            Caption = 'Telefone 1:'
            FocusControl = DBEdit17
          end
          object Label18: TLabel
            Left = 436
            Top = 160
            Width = 54
            Height = 13
            Caption = 'Telefone 2:'
            FocusControl = DBEdit18
          end
          object Label19: TLabel
            Left = 552
            Top = 160
            Width = 54
            Height = 13
            Caption = 'Telefone 3:'
            FocusControl = DBEdit19
          end
          object Label20: TLabel
            Left = 668
            Top = 160
            Width = 35
            Height = 13
            Caption = 'Celular:'
            FocusControl = DBEdit20
          end
          object Label21: TLabel
            Left = 4
            Top = 200
            Width = 20
            Height = 13
            Caption = 'Fax:'
            FocusControl = DBEdit21
          end
          object Label22: TLabel
            Left = 120
            Top = 200
            Width = 31
            Height = 13
            Caption = 'E-mail:'
            FocusControl = DBEdit22
          end
          object Label23: TLabel
            Left = 476
            Top = 200
            Width = 40
            Height = 13
            Caption = 'Contato:'
            FocusControl = DBEdit23
          end
          object Label24: TLabel
            Left = 716
            Top = 200
            Width = 51
            Height = 13
            Caption = 'Funda'#231#227'o:'
            FocusControl = DBEdit24
          end
          object Label97: TLabel
            Left = 352
            Top = 80
            Width = 57
            Height = 13
            Caption = 'Logradouro:'
            FocusControl = DBEdit106
          end
          object Label104: TLabel
            Left = 44
            Top = 160
            Width = 24
            Height = 13
            Caption = 'CEP:'
            FocusControl = DBEdit14
          end
          object Label132: TLabel
            Left = 4
            Top = 240
            Width = 80
            Height = 13
            Caption = 'Forma societ'#225'ria:'
            FocusControl = DBEdit8
          end
          object Label133: TLabel
            Left = 272
            Top = 240
            Width = 29
            Height = 13
            Caption = 'NIRE:'
            FocusControl = DBEdit9
          end
          object Label134: TLabel
            Left = 444
            Top = 240
            Width = 89
            Height = 13
            Caption = 'Atividade principal:'
            FocusControl = DBEdit10
          end
          object Label137: TLabel
            Left = 276
            Top = 40
            Width = 97
            Height = 13
            Caption = 'CPF Respons'#225'vel 1:'
            FocusControl = DBEdit13
          end
          object Label140: TLabel
            Left = 392
            Top = 0
            Width = 23
            Height = 13
            Caption = 'Filial:'
            FocusControl = DBEdit128
          end
          object DBEdit8: TDBEdit
            Left = 224
            Top = 96
            Width = 125
            Height = 24
            DataField = 'NOMEELOGRAD'
            DataSource = DsEntidades
            TabOrder = 0
          end
          object DBEdit9: TDBEdit
            Left = 716
            Top = 96
            Width = 64
            Height = 24
            DataField = 'ENumero'
            DataSource = DsEntidades
            TabOrder = 1
          end
          object DBEdit10: TDBEdit
            Left = 4
            Top = 136
            Width = 309
            Height = 24
            DataField = 'ECompl'
            DataSource = DsEntidades
            TabOrder = 2
          end
          object DBEdit12: TDBEdit
            Left = 528
            Top = 136
            Width = 253
            Height = 24
            DataField = 'ECidade'
            DataSource = DsEntidades
            TabOrder = 3
          end
          object DBEdit14: TDBEdit
            Left = 4
            Top = 176
            Width = 37
            Height = 24
            DataField = 'NOMEEUF'
            DataSource = DsEntidades
            TabOrder = 4
          end
          object DBEdit15: TDBEdit
            Left = 44
            Top = 176
            Width = 85
            Height = 24
            DataField = 'ECEP_TXT'
            DataSource = DsEntidades
            TabOrder = 6
          end
          object DBEdit16: TDBEdit
            Left = 132
            Top = 176
            Width = 185
            Height = 24
            DataField = 'EPais'
            DataSource = DsEntidades
            TabOrder = 8
          end
          object DBEdit17: TDBEdit
            Left = 320
            Top = 176
            Width = 113
            Height = 24
            DataField = 'ETE1_TXT'
            DataSource = DsEntidades
            TabOrder = 10
          end
          object DBEdit18: TDBEdit
            Left = 436
            Top = 176
            Width = 113
            Height = 24
            DataField = 'ETE2_TXT'
            DataSource = DsEntidades
            TabOrder = 12
          end
          object DBEdit19: TDBEdit
            Left = 552
            Top = 176
            Width = 113
            Height = 24
            DataField = 'ETE3_TXT'
            DataSource = DsEntidades
            TabOrder = 14
          end
          object DBEdit20: TDBEdit
            Left = 668
            Top = 176
            Width = 113
            Height = 24
            DataField = 'ECEL_TXT'
            DataSource = DsEntidades
            TabOrder = 16
          end
          object DBEdit21: TDBEdit
            Left = 4
            Top = 216
            Width = 113
            Height = 24
            DataField = 'EFAX_TXT'
            DataSource = DsEntidades
            TabOrder = 18
          end
          object DBEdit22: TDBEdit
            Left = 120
            Top = 216
            Width = 353
            Height = 24
            DataField = 'EEMail'
            DataSource = DsEntidades
            TabOrder = 5
          end
          object DBEdit23: TDBEdit
            Left = 476
            Top = 216
            Width = 237
            Height = 24
            DataField = 'EContato'
            DataSource = DsEntidades
            TabOrder = 7
          end
          object DBEdit24: TDBEdit
            Left = 716
            Top = 216
            Width = 64
            Height = 24
            DataField = 'ENatal'
            DataSource = DsEntidades
            TabOrder = 9
          end
          object DBEdit106: TDBEdit
            Left = 352
            Top = 96
            Width = 361
            Height = 24
            DataField = 'ERua'
            DataSource = DsEntidades
            TabOrder = 11
          end
          object DBCheckBox3: TDBCheckBox
            Left = 380
            Top = 260
            Width = 61
            Height = 17
            Caption = 'Simples.'
            DataField = 'Simples'
            DataSource = DsEntidades
            TabOrder = 13
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit13: TDBEdit
            Left = 276
            Top = 56
            Width = 113
            Height = 24
            DataField = 'CPF_Resp1_TXT'
            DataSource = DsEntidades
            TabOrder = 15
          end
          object DBEdit128: TDBEdit
            Left = 392
            Top = 16
            Width = 40
            Height = 24
            DataField = 'Filial'
            DataSource = DsEntidades
            TabOrder = 17
          end
          object DBEdit83: TDBEdit
            Left = 4
            Top = 16
            Width = 385
            Height = 24
            DataField = 'RazaoSocial'
            DataSource = DsEntidades
            TabOrder = 19
          end
          object DBEdit129: TDBEdit
            Left = 436
            Top = 16
            Width = 344
            Height = 24
            DataField = 'Fantasia'
            DataSource = DsEntidades
            TabOrder = 20
          end
          object DBEdit130: TDBEdit
            Left = 4
            Top = 56
            Width = 269
            Height = 24
            DataField = 'Respons1'
            DataSource = DsEntidades
            TabOrder = 21
          end
          object DBEdit131: TDBEdit
            Left = 392
            Top = 56
            Width = 388
            Height = 24
            DataField = 'Respons2'
            DataSource = DsEntidades
            TabOrder = 22
          end
          object DBEdit132: TDBEdit
            Left = 4
            Top = 96
            Width = 113
            Height = 24
            DataField = 'CNPJ'
            DataSource = DsEntidades
            TabOrder = 23
          end
          object DBEdit133: TDBEdit
            Left = 120
            Top = 96
            Width = 101
            Height = 24
            DataField = 'IE'
            DataSource = DsEntidades
            TabOrder = 24
          end
          object DBEdit134: TDBEdit
            Left = 316
            Top = 136
            Width = 209
            Height = 24
            DataField = 'EBairro'
            DataSource = DsEntidades
            TabOrder = 25
          end
          object DBEdit135: TDBEdit
            Left = 272
            Top = 256
            Width = 105
            Height = 24
            DataField = 'NIRE'
            DataSource = DsEntidades
            TabOrder = 26
          end
          object DBEdit136: TDBEdit
            Left = 444
            Top = 256
            Width = 336
            Height = 24
            DataField = 'Atividade'
            DataSource = DsEntidades
            TabOrder = 27
          end
          object DBEdit137: TDBEdit
            Left = 4
            Top = 256
            Width = 264
            Height = 24
            DataField = 'FormaSociet'
            DataSource = DsEntidades
            TabOrder = 28
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' &Miscel'#226'nea '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label49: TLabel
            Left = 292
            Top = 126
            Width = 187
            Height = 13
            Caption = 'Como obteve informa'#231#245'es da empresa?'
            FocusControl = DBEdit49
          end
          object Grupo: TLabel
            Left = 292
            Top = 90
            Width = 80
            Height = 13
            Caption = 'Cov'#234'nio (Grupo):'
            FocusControl = DBEdit56
          end
          object Label88: TLabel
            Left = 4
            Top = 164
            Width = 59
            Height = 13
            Caption = 'Autorizado1:'
            FocusControl = DBEdit89
          end
          object Label89: TLabel
            Left = 276
            Top = 164
            Width = 59
            Height = 13
            Caption = 'Nascimento:'
            FocusControl = DBEdit90
          end
          object Label90: TLabel
            Left = 344
            Top = 164
            Width = 62
            Height = 13
            Caption = 'Autorizado 2:'
            FocusControl = DBEdit91
          end
          object Label91: TLabel
            Left = 616
            Top = 164
            Width = 59
            Height = 13
            Caption = 'Nascimento:'
            FocusControl = DBEdit92
          end
          object Label92: TLabel
            Left = 4
            Top = 204
            Width = 62
            Height = 13
            Caption = 'Autorizado 3:'
            FocusControl = DBEdit93
          end
          object Label93: TLabel
            Left = 276
            Top = 204
            Width = 59
            Height = 13
            Caption = 'Nascimento:'
            FocusControl = DBEdit94
          end
          object Label94: TLabel
            Left = 344
            Top = 204
            Width = 62
            Height = 13
            Caption = 'Autorizado 4:'
            FocusControl = DBEdit95
          end
          object Label95: TLabel
            Left = 616
            Top = 204
            Width = 59
            Height = 13
            Caption = 'Nascimento:'
            FocusControl = DBEdit96
          end
          object Label96: TLabel
            Left = 686
            Top = 204
            Width = 34
            Height = 13
            Caption = 'Senha:'
            FocusControl = DBEdit97
          end
          object Label98: TLabel
            Left = 444
            Top = 244
            Width = 41
            Height = 13
            Caption = 'Cr'#233'ditos:'
            FocusControl = DBEdit99
          end
          object Label99: TLabel
            Left = 376
            Top = 244
            Width = 30
            Height = 13
            Caption = 'Limite:'
            FocusControl = DBEdit100
          end
          object Label100: TLabel
            Left = 512
            Top = 244
            Width = 39
            Height = 13
            Caption = 'Compra:'
            FocusControl = DBEdit101
          end
          object Label101: TLabel
            Left = 580
            Top = 244
            Width = 39
            Height = 13
            Caption = 'Ult. uso:'
            FocusControl = DBEdit102
          end
          object Label102: TLabel
            Left = 648
            Top = 244
            Width = 59
            Height = 13
            Caption = 'Vencimento:'
            FocusControl = DBEdit103
          end
          object Label103: TLabel
            Left = 4
            Top = 244
            Width = 35
            Height = 13
            Caption = 'Motivo:'
            FocusControl = DBEdit104
          end
          object LaQuantI1: TLabel
            Left = 716
            Top = 244
            Width = 27
            Height = 13
            Caption = '????:'
            FocusControl = DBEdit105
          end
          object Label108: TLabel
            Left = 96
            Top = 288
            Width = 48
            Height = 13
            Caption = 'Comiss'#227'o:'
            FocusControl = DBEdit111
          end
          object Label109: TLabel
            Left = 180
            Top = 288
            Width = 88
            Height = 13
            Caption = '% Desc. Min M'#225'x.:'
            FocusControl = DBEdit112
          end
          object Label110: TLabel
            Left = 272
            Top = 288
            Width = 89
            Height = 13
            Caption = 'Casas apli. desco.:'
            FocusControl = DBEdit113
          end
          object Label111: TLabel
            Left = 376
            Top = 288
            Width = 87
            Height = 13
            Caption = 'Valor dos cr'#233'ditos:'
            FocusControl = DBEdit113
          end
          object Label112: TLabel
            Left = 648
            Top = 288
            Width = 114
            Height = 13
            Caption = 'Saldo em arquivo morto:'
            FocusControl = DBEdit113
          end
          object GroupBox1: TGroupBox
            Left = 4
            Top = 4
            Width = 636
            Height = 85
            Caption = ' Tipo de cadastro: '
            TabOrder = 1
            object CkCliente1: TDBCheckBox
              Left = 8
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Cliente'
              DataField = 'Cliente1'
              DataSource = DsEntidades
              TabOrder = 0
              ValueChecked = 'V'
              ValueUnchecked = 'F'
            end
            object CkFornece1: TDBCheckBox
              Left = 164
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Fornecedor'
              DataField = 'Fornece1'
              DataSource = DsEntidades
              TabOrder = 4
              ValueChecked = 'V'
              ValueUnchecked = 'F'
            end
            object CkFornece2: TDBCheckBox
              Left = 164
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Transportador'
              DataField = 'Fornece2'
              DataSource = DsEntidades
              TabOrder = 5
              ValueChecked = 'V'
              ValueUnchecked = 'F'
            end
            object CkFornece3: TDBCheckBox
              Left = 164
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Vendedor'
              DataField = 'Fornece3'
              DataSource = DsEntidades
              TabOrder = 6
              ValueChecked = 'V'
              ValueUnchecked = 'F'
            end
            object CkTerceiro: TDBCheckBox
              Left = 476
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Terceiro'
              DataField = 'Terceiro'
              DataSource = DsEntidades
              TabOrder = 12
              ValueChecked = 'V'
              ValueUnchecked = 'F'
            end
            object CkFornece4: TDBCheckBox
              Left = 164
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Fornece 4'
              DataField = 'Fornece4'
              DataSource = DsEntidades
              TabOrder = 7
              ValueChecked = 'V'
              ValueUnchecked = 'F'
              Visible = False
            end
            object CkCliente2: TDBCheckBox
              Left = 8
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Cliente 2'
              DataField = 'Cliente2'
              DataSource = DsEntidades
              TabOrder = 1
              ValueChecked = 'V'
              ValueUnchecked = 'F'
              Visible = False
            end
            object CkFornece5: TDBCheckBox
              Left = 320
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Fornece 5'
              DataField = 'Fornece5'
              DataSource = DsEntidades
              TabOrder = 8
              ValueChecked = 'V'
              ValueUnchecked = 'F'
              Visible = False
            end
            object CkFornece6: TDBCheckBox
              Left = 320
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Fornece 6'
              DataField = 'Fornece6'
              DataSource = DsEntidades
              TabOrder = 9
              ValueChecked = 'V'
              ValueUnchecked = 'F'
              Visible = False
            end
            object CkCliente3: TDBCheckBox
              Left = 8
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Cliente 3'
              DataField = 'Cliente3'
              DataSource = DsEntidades
              TabOrder = 2
              ValueChecked = 'V'
              ValueUnchecked = 'F'
              Visible = False
            end
            object CkCliente4: TDBCheckBox
              Left = 8
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Cliente 4'
              DataField = 'Cliente4'
              DataSource = DsEntidades
              TabOrder = 3
              ValueChecked = 'V'
              ValueUnchecked = 'F'
              Visible = False
            end
            object CkFornece8: TDBCheckBox
              Left = 320
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Fornece 8'
              DataField = 'Fornece6'
              DataSource = DsEntidades
              TabOrder = 11
              ValueChecked = 'V'
              ValueUnchecked = 'F'
              Visible = False
            end
            object CkFornece7: TDBCheckBox
              Left = 320
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Fornece 7'
              DataField = 'Fornece5'
              DataSource = DsEntidades
              TabOrder = 10
              ValueChecked = 'V'
              ValueUnchecked = 'F'
              Visible = False
            end
          end
          object DBRadioGroup1: TDBRadioGroup
            Left = 644
            Top = 4
            Width = 104
            Height = 85
            Caption = ' Foco do cadastro: '
            DataField = 'Tipo'
            DataSource = DsEntidades
            Items.Strings = (
              'Empresa'
              'Pessoa')
            TabOrder = 2
            Values.Strings = (
              '0'
              '1')
          end
          object GroupBox2: TGroupBox
            Left = 4
            Top = 96
            Width = 281
            Height = 65
            Caption = ' Ajuda de custos pela empresa: '
            TabOrder = 3
            object Label53: TLabel
              Left = 10
              Top = 20
              Width = 27
              Height = 13
              Caption = 'Valor:'
              FocusControl = DBEdit53
            end
            object Label54: TLabel
              Left = 114
              Top = 20
              Width = 66
              Height = 13
              Caption = 'Porcentagem:'
              FocusControl = DBEdit54
            end
            object DBEdit53: TDBEdit
              Left = 9
              Top = 36
              Width = 100
              Height = 24
              DataField = 'AjudaEmpV'
              DataSource = DsEntidades
              TabOrder = 0
            end
            object DBEdit54: TDBEdit
              Left = 113
              Top = 36
              Width = 100
              Height = 24
              DataField = 'AjudaEmpP'
              DataSource = DsEntidades
              TabOrder = 1
            end
          end
          object DBEdit49: TDBEdit
            Left = 292
            Top = 140
            Width = 452
            Height = 24
            DataField = 'Informacoes'
            DataSource = DsEntidades
            TabOrder = 5
          end
          object DBRadioGroup2: TDBRadioGroup
            Left = 752
            Top = 4
            Width = 236
            Height = 158
            Caption = ' Emiss'#227'o de recibos: '
            DataField = 'Recibo'
            DataSource = DsEntidades
            Items.Strings = (
              'N'#227'o necessita de recibo'
              'Emitir um recibo no total'
              'Emitir um recibo para cada parcela'
              'Emitir recibos mensais. dia')
            TabOrder = 6
            Values.Strings = (
              '0'
              '1'
              '2'
              '3')
          end
          object DBEdit50: TDBEdit
            Left = 904
            Top = 132
            Width = 49
            Height = 24
            DataField = 'DiaRecibo'
            DataSource = DsEntidades
            TabOrder = 0
          end
          object DBEdit56: TDBEdit
            Left = 292
            Top = 104
            Width = 452
            Height = 24
            DataField = 'NOMEENTIGRUPO'
            DataSource = DsEntidades
            TabOrder = 4
          end
          object DBEdit89: TDBEdit
            Left = 4
            Top = 180
            Width = 268
            Height = 24
            DataField = 'Nome1'
            DataSource = DsEntidades
            TabOrder = 7
          end
          object DBEdit90: TDBEdit
            Left = 276
            Top = 180
            Width = 64
            Height = 24
            DataField = 'Natal1'
            DataSource = DsEntidades
            TabOrder = 8
          end
          object DBEdit91: TDBEdit
            Left = 344
            Top = 180
            Width = 268
            Height = 24
            DataField = 'Nome2'
            DataSource = DsEntidades
            TabOrder = 9
          end
          object DBEdit92: TDBEdit
            Left = 616
            Top = 180
            Width = 64
            Height = 24
            DataField = 'Natal2'
            DataSource = DsEntidades
            TabOrder = 10
          end
          object DBEdit93: TDBEdit
            Left = 4
            Top = 220
            Width = 268
            Height = 24
            DataField = 'Nome3'
            DataSource = DsEntidades
            TabOrder = 11
          end
          object DBEdit94: TDBEdit
            Left = 276
            Top = 220
            Width = 64
            Height = 24
            DataField = 'Natal3'
            DataSource = DsEntidades
            TabOrder = 12
          end
          object DBEdit95: TDBEdit
            Left = 344
            Top = 220
            Width = 268
            Height = 24
            DataField = 'Nome4'
            DataSource = DsEntidades
            TabOrder = 13
          end
          object DBEdit96: TDBEdit
            Left = 616
            Top = 220
            Width = 64
            Height = 24
            DataField = 'Natal4'
            DataSource = DsEntidades
            TabOrder = 14
          end
          object DBCheckBox1: TDBCheckBox
            Left = 684
            Top = 184
            Width = 97
            Height = 17
            Caption = 'Quer senha.'
            DataField = 'SenhaQuer'
            DataSource = DsEntidades
            TabOrder = 15
            ValueChecked = 'V'
            ValueUnchecked = 'F'
          end
          object DBEdit97: TDBEdit
            Left = 685
            Top = 220
            Width = 94
            Height = 24
            DataField = 'Senha1'
            DataSource = DsEntidades
            TabOrder = 16
          end
          object DBCheckBox2: TDBCheckBox
            Left = 287
            Top = 262
            Width = 62
            Height = 17
            Caption = 'Agenda.'
            DataField = 'Agenda'
            DataSource = DsEntidades
            TabOrder = 17
            ValueChecked = 'V'
            ValueUnchecked = 'F'
          end
          object DBEdit99: TDBEdit
            Left = 444
            Top = 260
            Width = 64
            Height = 24
            DataField = 'CreditosI'
            DataSource = DsEntidades
            TabOrder = 18
          end
          object DBEdit100: TDBEdit
            Left = 376
            Top = 260
            Width = 64
            Height = 24
            DataField = 'CreditosL'
            DataSource = DsEntidades
            TabOrder = 19
          end
          object DBEdit101: TDBEdit
            Left = 512
            Top = 260
            Width = 64
            Height = 24
            DataField = 'CreditosD'
            DataSource = DsEntidades
            TabOrder = 20
          end
          object DBEdit102: TDBEdit
            Left = 580
            Top = 260
            Width = 64
            Height = 24
            DataField = 'CreditosU'
            DataSource = DsEntidades
            TabOrder = 21
          end
          object DBEdit103: TDBEdit
            Left = 648
            Top = 260
            Width = 64
            Height = 24
            DataField = 'CreditosV'
            DataSource = DsEntidades
            TabOrder = 22
          end
          object DBEdit104: TDBEdit
            Left = 4
            Top = 260
            Width = 268
            Height = 24
            DataField = 'NOMEMOTIVO'
            DataSource = DsEntidades
            TabOrder = 23
          end
          object DBEdit105: TDBEdit
            Left = 716
            Top = 260
            Width = 64
            Height = 24
            DataField = 'QuantI1'
            DataSource = DsEntidades
            TabOrder = 24
          end
          object DBEdit98: TDBEdit
            Left = 376
            Top = 304
            Width = 132
            Height = 24
            DataField = 'CreditosF2'
            DataSource = DsEntidades
            TabOrder = 25
          end
          object DBEdit111: TDBEdit
            Left = 96
            Top = 304
            Width = 81
            Height = 24
            DataField = 'Comissao'
            DataSource = DsEntidades
            TabOrder = 26
          end
          object DBEdit112: TDBEdit
            Left = 180
            Top = 304
            Width = 89
            Height = 24
            DataField = 'Desco'
            DataSource = DsEntidades
            TabOrder = 27
          end
          object DBEdit113: TDBEdit
            Left = 272
            Top = 304
            Width = 89
            Height = 24
            DataField = 'CasasApliDesco'
            DataSource = DsEntidades
            TabOrder = 28
          end
          object DBEdit114: TDBEdit
            Left = 648
            Top = 304
            Width = 132
            Height = 24
            DataField = 'QuantN2'
            DataSource = DsEntidades
            TabOrder = 29
          end
        end
        object TabSheet9: TTabSheet
          Caption = ' Dados banc'#225'rios '
          ImageIndex = 8
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel2: TPanel
            Left = 100
            Top = 0
            Width = 902
            Height = 431
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel2'
            ParentBackground = False
            TabOrder = 0
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 902
              Height = 387
              Align = alClient
              DataSource = DsEntiCtas
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Banco'
                  Width = 53
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEBANCO'
                  Title.Caption = 'Banco'
                  Width = 278
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Agencia'
                  Title.Caption = 'Ag'#234'ncia'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DAC_A'
                  Title.Caption = '[A]'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ContaCor'
                  Title.Caption = 'Conta'
                  Width = 73
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DAC_C'
                  Title.Caption = '[C]'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DAC_AC'
                  Title.Caption = '[AC]'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ContaTip'
                  Title.Caption = 'Tipo'
                  Visible = True
                end>
            end
            object TPanel
              Left = 0
              Top = 387
              Width = 902
              Height = 44
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object Label138: TLabel
                Left = 4
                Top = 4
                Width = 97
                Height = 13
                Caption = 'Carteira preferencial:'
                FocusControl = DBEdit126
              end
              object DBEdit126: TDBEdit
                Left = 4
                Top = 20
                Width = 38
                Height = 24
                DataField = 'CartPref'
                DataSource = DsEntidades
                TabOrder = 0
              end
              object DBEdit127: TDBEdit
                Left = 41
                Top = 20
                Width = 600
                Height = 24
                DataField = 'NOMECARTPREF'
                DataSource = DsEntidades
                TabOrder = 1
              end
            end
          end
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 100
            Height = 431
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' &Observa'#231#245'es '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo1: TDBMemo
            Left = 0
            Top = 0
            Width = 1002
            Height = 431
            Align = alClient
            DataField = 'Observacoes'
            DataSource = DsEntidades
            TabOrder = 0
          end
        end
        object TabSheet8: TTabSheet
          Caption = ' Lo&gotipo (1:2,625) '
          ImageIndex = 7
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 1002
            Height = 431
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            Visible = False
            object Label55: TLabel
              Left = 13
              Top = 61
              Width = 129
              Height = 13
              Caption = 'Relat'#243'rios: 168 x 64 pontos'
            end
            object Label50: TLabel
              Left = 408
              Top = 60
              Width = 106
              Height = 13
              Caption = 'Extra: 350 x 96 pontos'
            end
            object DBImage2: TDBImage
              Left = 405
              Top = 77
              Width = 350
              Height = 96
              DataField = 'Logo2'
              DataSource = DsEntidades
              TabOrder = 4
            end
            object DBImage: TDBImage
              Left = 13
              Top = 77
              Width = 168
              Height = 64
              DataField = 'Logo'
              DataSource = DsEntidades
              TabOrder = 5
            end
            object DBImage1: TDBImage
              Left = 0
              Top = 418
              Width = 1002
              Height = 13
              Align = alBottom
              DataField = 'Logo'
              DataSource = DsEntidade
              TabOrder = 2
              Visible = False
            end
            object BtImagem: TBitBtn
              Tag = 102
              Left = 14
              Top = 18
              Width = 168
              Height = 40
              Caption = 'Imagem &1'
              Enabled = False
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000120B0000120B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
                5555555555555555555555555555555555555555555555555555555555555555
                555555555555555555555555555555555555555FFFFFFFFFF555550000000000
                55555577777777775F55500B8B8B8B8B05555775F555555575F550F0B8B8B8B8
                B05557F75F555555575F50BF0B8B8B8B8B0557F575FFFFFFFF7F50FBF0000000
                000557F557777777777550BFBFBFBFB0555557F555555557F55550FBFBFBFBF0
                555557F555555FF7555550BFBFBF00055555575F555577755555550BFBF05555
                55555575FFF75555555555700007555555555557777555555555555555555555
                5555555555555555555555555555555555555555555555555555}
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtImagemClick
            end
            object BtImagem1: TBitBtn
              Tag = 102
              Left = 406
              Top = 18
              Width = 350
              Height = 40
              Caption = 'Imagem &2'
              Enabled = False
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000120B0000120B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
                5555555555555555555555555555555555555555555555555555555555555555
                555555555555555555555555555555555555555FFFFFFFFFF555550000000000
                55555577777777775F55500B8B8B8B8B05555775F555555575F550F0B8B8B8B8
                B05557F75F555555575F50BF0B8B8B8B8B0557F575FFFFFFFF7F50FBF0000000
                000557F557777777777550BFBFBFBFB0555557F555555557F55550FBFBFBFBF0
                555557F555555FF7555550BFBFBF00055555575F555577755555550BFBF05555
                55555575FFF75555555555700007555555555557777555555555555555555555
                5555555555555555555555555555555555555555555555555555}
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtImagem1Click
            end
            object DBImage3: TDBImage
              Left = 0
              Top = 404
              Width = 1002
              Height = 14
              Align = alBottom
              DataField = 'Logo2'
              DataSource = DsEntidade
              TabOrder = 3
              Visible = False
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = ' Local de Co&bran'#231'a '
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label56: TLabel
            Left = 104
            Top = 0
            Width = 23
            Height = 13
            Caption = 'Rua:'
            FocusControl = DBEdit57
          end
          object Label59: TLabel
            Left = 4
            Top = 44
            Width = 30
            Height = 13
            Caption = 'Bairro:'
            FocusControl = DBEdit59
          end
          object Label64: TLabel
            Left = 4
            Top = 88
            Width = 45
            Height = 13
            Caption = 'Telefone:'
            FocusControl = DBEdit64
          end
          object Label67: TLabel
            Left = 128
            Top = 88
            Width = 20
            Height = 13
            Caption = 'Fax:'
            FocusControl = DBEdit67
          end
          object Label60: TLabel
            Left = 216
            Top = 44
            Width = 36
            Height = 13
            Caption = 'Cidade:'
            FocusControl = DBEdit60
          end
          object Label65: TLabel
            Left = 252
            Top = 88
            Width = 35
            Height = 13
            Caption = 'Celular:'
            FocusControl = DBEdit65
          end
          object Label57: TLabel
            Left = 404
            Top = 0
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
            FocusControl = DBEdit58
          end
          object Label61: TLabel
            Left = 448
            Top = 44
            Width = 17
            Height = 13
            Caption = 'UF:'
            FocusControl = DBEdit61
          end
          object Label66: TLabel
            Left = 376
            Top = 88
            Width = 40
            Height = 13
            Caption = 'Contato:'
            FocusControl = DBEdit66
          end
          object Label62: TLabel
            Left = 484
            Top = 44
            Width = 24
            Height = 13
            Caption = 'CEP:'
            FocusControl = DBEdit62
          end
          object Label58: TLabel
            Left = 492
            Top = 0
            Width = 67
            Height = 13
            Caption = 'Complemento:'
            FocusControl = DBEdit63
          end
          object Label63: TLabel
            Left = 580
            Top = 44
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
            FocusControl = DBEdit68
          end
          object Label15: TLabel
            Left = 4
            Top = 0
            Width = 92
            Height = 13
            Caption = 'Tipo de logradouro:'
            FocusControl = DBEdit108
          end
          object DBEdit57: TDBEdit
            Left = 104
            Top = 16
            Width = 297
            Height = 24
            DataField = 'CRua'
            DataSource = DsEntidades
            TabOrder = 0
          end
          object DBEdit59: TDBEdit
            Left = 4
            Top = 60
            Width = 209
            Height = 24
            DataField = 'CBairro'
            DataSource = DsEntidades
            TabOrder = 1
          end
          object DBEdit64: TDBEdit
            Left = 4
            Top = 104
            Width = 121
            Height = 24
            DataField = 'CTEL_TXT'
            DataSource = DsEntidades
            TabOrder = 2
          end
          object DBEdit67: TDBEdit
            Left = 128
            Top = 104
            Width = 121
            Height = 24
            DataField = 'CFAX_TXT'
            DataSource = DsEntidades
            TabOrder = 3
          end
          object DBEdit60: TDBEdit
            Left = 216
            Top = 60
            Width = 229
            Height = 24
            DataField = 'CCidade'
            DataSource = DsEntidades
            TabOrder = 4
          end
          object DBEdit65: TDBEdit
            Left = 252
            Top = 104
            Width = 121
            Height = 24
            DataField = 'CCEL_TXT'
            DataSource = DsEntidades
            TabOrder = 5
          end
          object DBEdit58: TDBEdit
            Left = 404
            Top = 16
            Width = 85
            Height = 24
            DataField = 'CNumero'
            DataSource = DsEntidades
            TabOrder = 6
          end
          object DBEdit61: TDBEdit
            Left = 448
            Top = 60
            Width = 33
            Height = 24
            DataField = 'NOMECUF'
            DataSource = DsEntidades
            TabOrder = 7
          end
          object DBEdit66: TDBEdit
            Left = 376
            Top = 104
            Width = 405
            Height = 24
            DataField = 'CContato'
            DataSource = DsEntidades
            TabOrder = 8
          end
          object DBEdit62: TDBEdit
            Left = 484
            Top = 60
            Width = 92
            Height = 24
            DataField = 'CCEP_TXT'
            DataSource = DsEntidades
            TabOrder = 9
          end
          object DBEdit63: TDBEdit
            Left = 492
            Top = 16
            Width = 289
            Height = 24
            DataField = 'CCompl'
            DataSource = DsEntidades
            TabOrder = 10
          end
          object DBEdit68: TDBEdit
            Left = 580
            Top = 60
            Width = 201
            Height = 24
            DataField = 'CPais'
            DataSource = DsEntidades
            TabOrder = 11
          end
          object DBEdit108: TDBEdit
            Left = 4
            Top = 16
            Width = 97
            Height = 24
            DataField = 'NOMECLOGRAD'
            DataSource = DsEntidades
            TabOrder = 12
          end
        end
        object TabSheet6: TTabSheet
          Caption = ' Local de e&ntrega '
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 133
            Align = alTop
            BevelOuter = bvNone
            Caption = 'Panel6'
            ParentBackground = False
            TabOrder = 0
            object Label69: TLabel
              Left = 4
              Top = 44
              Width = 30
              Height = 13
              Caption = 'Bairro:'
              FocusControl = DBEdit70
            end
            object Label70: TLabel
              Left = 4
              Top = 88
              Width = 45
              Height = 13
              Caption = 'Telefone:'
              FocusControl = DBEdit71
            end
            object Label71: TLabel
              Left = 128
              Top = 88
              Width = 20
              Height = 13
              Caption = 'Fax:'
              FocusControl = DBEdit72
            end
            object Label72: TLabel
              Left = 216
              Top = 44
              Width = 36
              Height = 13
              Caption = 'Cidade:'
              FocusControl = DBEdit73
            end
            object Label75: TLabel
              Left = 404
              Top = 0
              Width = 40
              Height = 13
              Caption = 'N'#250'mero:'
              FocusControl = DBEdit75
            end
            object Label76: TLabel
              Left = 492
              Top = 0
              Width = 67
              Height = 13
              Caption = 'Complemento:'
              FocusControl = DBEdit76
            end
            object Label77: TLabel
              Left = 448
              Top = 44
              Width = 17
              Height = 13
              Caption = 'UF:'
              FocusControl = DBEdit77
            end
            object Label78: TLabel
              Left = 484
              Top = 44
              Width = 24
              Height = 13
              Caption = 'CEP:'
              FocusControl = DBEdit78
            end
            object Label79: TLabel
              Left = 580
              Top = 44
              Width = 25
              Height = 13
              Caption = 'Pa'#237's:'
              FocusControl = DBEdit79
            end
            object Label73: TLabel
              Left = 252
              Top = 88
              Width = 35
              Height = 13
              Caption = 'Celular:'
              FocusControl = DBEdit74
            end
            object Label74: TLabel
              Left = 376
              Top = 88
              Width = 40
              Height = 13
              Caption = 'Contato:'
              FocusControl = DBEdit80
            end
            object Label39: TLabel
              Left = 104
              Top = 0
              Width = 23
              Height = 13
              Caption = 'Rua:'
              FocusControl = DBEdit69
            end
            object Label68: TLabel
              Left = 4
              Top = 0
              Width = 92
              Height = 13
              Caption = 'Tipo de logradouro:'
              FocusControl = DBEdit109
            end
            object DBEdit70: TDBEdit
              Left = 4
              Top = 60
              Width = 209
              Height = 21
              DataField = 'LBairro'
              DataSource = DsEntidades
              TabOrder = 0
            end
            object DBEdit71: TDBEdit
              Left = 4
              Top = 104
              Width = 121
              Height = 21
              DataField = 'LTEL_TXT'
              DataSource = DsEntidades
              TabOrder = 1
            end
            object DBEdit72: TDBEdit
              Left = 128
              Top = 104
              Width = 121
              Height = 21
              DataField = 'LFAX_TXT'
              DataSource = DsEntidades
              TabOrder = 2
            end
            object DBEdit73: TDBEdit
              Left = 216
              Top = 60
              Width = 229
              Height = 21
              DataField = 'LCidade'
              DataSource = DsEntidades
              TabOrder = 3
            end
            object DBEdit75: TDBEdit
              Left = 404
              Top = 16
              Width = 85
              Height = 21
              DataField = 'LNumero'
              DataSource = DsEntidades
              TabOrder = 4
            end
            object DBEdit76: TDBEdit
              Left = 492
              Top = 16
              Width = 289
              Height = 21
              DataField = 'LCompl'
              DataSource = DsEntidades
              TabOrder = 5
            end
            object DBEdit77: TDBEdit
              Left = 448
              Top = 60
              Width = 33
              Height = 21
              DataField = 'NOMELUF'
              DataSource = DsEntidades
              TabOrder = 6
            end
            object DBEdit78: TDBEdit
              Left = 484
              Top = 60
              Width = 92
              Height = 21
              DataField = 'LCEP_TXT'
              DataSource = DsEntidades
              TabOrder = 7
            end
            object DBEdit79: TDBEdit
              Left = 580
              Top = 60
              Width = 201
              Height = 21
              DataField = 'LPais'
              DataSource = DsEntidades
              TabOrder = 8
            end
            object DBEdit74: TDBEdit
              Left = 252
              Top = 104
              Width = 121
              Height = 21
              DataField = 'LCEL_TXT'
              DataSource = DsEntidades
              TabOrder = 9
            end
            object DBEdit80: TDBEdit
              Left = 376
              Top = 104
              Width = 405
              Height = 21
              DataField = 'LContato'
              DataSource = DsEntidades
              TabOrder = 10
            end
            object DBEdit69: TDBEdit
              Left = 104
              Top = 16
              Width = 297
              Height = 21
              DataField = 'LRua'
              DataSource = DsEntidades
              TabOrder = 11
            end
            object DBEdit109: TDBEdit
              Left = 4
              Top = 16
              Width = 97
              Height = 21
              DataField = 'NOMELLOGRAD'
              DataSource = DsEntidades
              TabOrder = 12
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 133
            Width = 1000
            Height = 296
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object DBGrid2: TDBGrid
              Left = 497
              Top = 0
              Width = 503
              Height = 296
              Align = alRight
              DataSource = DsEntiTransp
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Transp'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEENTIDADE'
                  Title.Caption = 'Nome Transportadora'
                  Width = 400
                  Visible = True
                end>
            end
          end
        end
        object TabSheet7: TTabSheet
          Caption = ' Cr'#233'dito '
          ImageIndex = 7
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PainelEntiCreds0: TPanel
            Left = 0
            Top = 0
            Width = 1002
            Height = 48
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            Visible = False
            object Label107: TLabel
              Left = 12
              Top = 4
              Width = 91
              Height = 13
              Caption = 'Cr'#233'dito autom'#225'tico:'
              FocusControl = DBEdit110
            end
            object Label136: TLabel
              Left = 116
              Top = 4
              Width = 344
              Height = 13
              Caption = 
                'CBE = Compensa'#231#227'o banc'#225'ria especial fixa limitada ao CBE (0 = no' +
                'rmal).'
            end
            object DBEdit110: TDBEdit
              Left = 12
              Top = 18
              Width = 101
              Height = 24
              DataField = 'LimiCred'
              DataSource = DsEntidades
              TabOrder = 0
            end
            object DBCheckBox4: TDBCheckBox
              Left = 152
              Top = 20
              Width = 321
              Height = 17
              Caption = 'Cliente sem compensa'#231#227'o banc'#225'ria (Vale o CBE se o CBE > 0).'
              DataField = 'SCB'
              DataSource = DsEntidades
              TabOrder = 1
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
          end
          object PainelEntiCreds1: TPanel
            Left = 0
            Top = 48
            Width = 1002
            Height = 88
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            Visible = False
            object Label118: TLabel
              Left = 12
              Top = 4
              Width = 76
              Height = 13
              Caption = 'Fator compra %:'
            end
            object Label119: TLabel
              Left = 116
              Top = 4
              Width = 68
              Height = 13
              Caption = 'Ad Valorem %:'
            end
            object Label120: TLabel
              Left = 220
              Top = 4
              Width = 57
              Height = 13
              Caption = 'D+ Cheque:'
            end
            object Label121: TLabel
              Left = 324
              Top = 4
              Width = 65
              Height = 13
              Caption = 'D+ Duplicata:'
            end
            object Label122: TLabel
              Left = 428
              Top = 4
              Width = 73
              Height = 13
              Caption = 'Tarifa cust'#243'dia:'
              Visible = False
            end
            object Label123: TLabel
              Left = 532
              Top = 4
              Width = 78
              Height = 13
              Caption = 'Tarifa cobran'#231'a:'
              Visible = False
            end
            object Label124: TLabel
              Left = 12
              Top = 44
              Width = 25
              Height = 13
              Caption = 'R.U.:'
              Visible = False
            end
            object Label125: TLabel
              Left = 116
              Top = 44
              Width = 61
              Height = 13
              Caption = 'Tarifa postal:'
              Visible = False
            end
            object Label126: TLabel
              Left = 220
              Top = 44
              Width = 78
              Height = 13
              Caption = 'Tarifa opera'#231#227'o:'
              Visible = False
            end
            object Label127: TLabel
              Left = 324
              Top = 44
              Width = 73
              Height = 13
              Caption = 'Taxa SERASA:'
              Visible = False
            end
            object Label128: TLabel
              Left = 428
              Top = 44
              Width = 45
              Height = 13
              Caption = 'Taxa AR:'
              Visible = False
            end
            object DBEdit115: TDBEdit
              Left = 12
              Top = 18
              Width = 100
              Height = 24
              DataField = 'FatorCompra'
              DataSource = DsEntidades
              TabOrder = 0
            end
            object DBEdit116: TDBEdit
              Left = 116
              Top = 18
              Width = 100
              Height = 24
              DataField = 'AdValorem'
              DataSource = DsEntidades
              TabOrder = 1
            end
            object DBEdit117: TDBEdit
              Left = 220
              Top = 18
              Width = 100
              Height = 24
              DataField = 'DMaisC'
              DataSource = DsEntidades
              TabOrder = 2
            end
            object DBEdit118: TDBEdit
              Left = 324
              Top = 18
              Width = 100
              Height = 24
              DataField = 'DMaisD'
              DataSource = DsEntidades
              TabOrder = 3
            end
            object DBEdit119: TDBEdit
              Left = 428
              Top = 18
              Width = 100
              Height = 24
              TabOrder = 4
              Visible = False
            end
            object DBEdit120: TDBEdit
              Left = 532
              Top = 18
              Width = 100
              Height = 24
              TabOrder = 5
              Visible = False
            end
            object DBEdit121: TDBEdit
              Left = 12
              Top = 58
              Width = 100
              Height = 24
              TabOrder = 6
              Visible = False
            end
            object DBEdit122: TDBEdit
              Left = 116
              Top = 58
              Width = 100
              Height = 24
              TabOrder = 7
              Visible = False
            end
            object DBEdit123: TDBEdit
              Left = 220
              Top = 58
              Width = 100
              Height = 24
              TabOrder = 8
              Visible = False
            end
            object DBEdit124: TDBEdit
              Left = 324
              Top = 58
              Width = 100
              Height = 24
              TabOrder = 9
              Visible = False
            end
            object DBEdit125: TDBEdit
              Left = 428
              Top = 58
              Width = 100
              Height = 24
              TabOrder = 10
              Visible = False
            end
          end
          object Grade1: TStringGrid
            Left = 0
            Top = 136
            Width = 1002
            Height = 295
            Align = alClient
            DefaultRowHeight = 19
            TabOrder = 2
            Visible = False
          end
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 89
        Align = alTop
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object Label80: TLabel
          Left = 10
          Top = 4
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
          FocusControl = DBEdit81
        end
        object Label81: TLabel
          Left = 108
          Top = 4
          Width = 45
          Height = 13
          Caption = 'Cadastro:'
        end
        object Label82: TLabel
          Left = 316
          Top = 4
          Width = 60
          Height = 13
          Caption = 'Cadastro de:'
          FocusControl = DBEntiNome
        end
        object Label83: TLabel
          Left = 212
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Alterado:'
        end
        object Label84: TLabel
          Left = 8
          Top = 44
          Width = 63
          Height = 13
          Caption = 'Cadastro por:'
        end
        object Label85: TLabel
          Left = 192
          Top = 44
          Width = 60
          Height = 13
          Caption = 'Alterado por:'
        end
        object Label1: TLabel
          Left = 376
          Top = 44
          Width = 32
          Height = 13
          Caption = 'Grupo:'
        end
        object Label13: TLabel
          Left = 560
          Top = 44
          Width = 73
          Height = 13
          Caption = 'Representante:'
        end
        object DBEdit81: TDBEdit
          Left = 10
          Top = 20
          Width = 71
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
        object DBEdit82: TDBEdit
          Left = 108
          Top = 20
          Width = 100
          Height = 21
          DataField = 'Cadastro'
          DataSource = DsEntidades
          TabOrder = 1
        end
        object DBEntiNome: TDBEdit
          Left = 316
          Top = 20
          Width = 469
          Height = 21
          DataField = 'NOMEENTIDADE'
          DataSource = DsEntidades
          TabOrder = 2
        end
        object DBEdit84: TDBEdit
          Left = 212
          Top = 20
          Width = 100
          Height = 21
          DataField = 'DataAlt'
          DataSource = DsEntidades
          TabOrder = 3
        end
        object DBEdit85: TDBEdit
          Left = 8
          Top = 60
          Width = 180
          Height = 21
          DataField = 'NOMECAD2'
          DataSource = DsEntidades
          TabOrder = 4
        end
        object DBEdit86: TDBEdit
          Left = 192
          Top = 60
          Width = 180
          Height = 21
          DataField = 'NOMEALT2'
          DataSource = DsEntidades
          TabOrder = 5
        end
        object DBEdit87: TDBEdit
          Left = 82
          Top = 20
          Width = 23
          Height = 21
          TabStop = False
          DataField = 'Nivel'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
        end
        object DBEdit138: TDBEdit
          Left = 376
          Top = 60
          Width = 180
          Height = 21
          DataField = 'NOMEENTIGRUPO'
          DataSource = DsEntidades
          TabOrder = 7
        end
        object DBEdit139: TDBEdit
          Left = 560
          Top = 60
          Width = 225
          Height = 21
          DataField = 'NOMEACCOUNT'
          DataSource = DsEntidades
          TabOrder = 8
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 260
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object SbMapa: TBitBtn
        Tag = 471
        Left = 214
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = SbMapaClick
      end
    end
    object GB_M: TGroupBox
      Left = 260
      Top = 0
      Width = 281
      Height = 52
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 257
        Height = 32
        Caption = 'Pessoas e Empresas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 257
        Height = 32
        Caption = 'Pessoas e Empresas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 257
        Height = 32
        Caption = 'Pessoas e Empresas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 541
      Top = 0
      Width = 331
      Height = 52
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 327
        Height = 35
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 12
          Height = 17
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 12
          Height = 17
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object GroupBox3: TGroupBox
      Left = 872
      Top = 0
      Width = 88
      Height = 52
      Align = alRight
      TabOrder = 4
      object BtEntidades2: TBitBtn
        Tag = 101
        Left = 4
        Top = 7
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtEntidades2Click
      end
      object BtImporta: TBitBtn
        Tag = 39
        Left = 44
        Top = 7
        Width = 40
        Height = 40
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 1
        OnClick = BtImportaClick
      end
    end
  end
  object GBCntrl: TGroupBox
    Left = 0
    Top = 598
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 2
    ExplicitLeft = -8
    ExplicitTop = 602
    object Panel11: TPanel
      Left = 2
      Top = 15
      Width = 172
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object LaRegistro: TStaticText
      Left = 174
      Top = 15
      Width = 30
      Height = 17
      Align = alClient
      BevelInner = bvLowered
      BevelKind = bkFlat
      Caption = '[N]: 0'
      TabOrder = 2
    end
    object Panel12: TPanel
      Left = 485
      Top = 15
      Width = 521
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Panel13: TPanel
        Left = 388
        Top = 0
        Width = 133
        Height = 47
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 9
        Top = 4
        Width = 96
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui nova entidade'
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 109
        Top = 4
        Width = 96
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera entidade atual'
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 209
        Top = 4
        Width = 96
        Height = 40
        Cursor = crHandPoint
        Hint = 'Exclui entidade atual'
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtExcluiClick
      end
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 477
    Top = 56
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 
      'All (*.gif;*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf)|*.gif;*.jpg;*.j' +
      'peg;*.bmp;*.ico;*.emf;*.wmf|GIF Image (*.gif)|*.gif|JPEG Image F' +
      'ile (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bitmaps (*.bmp' +
      ')|*.bmp|Icons (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf|Met' +
      'afiles (*.wmf)|*.wmf'
    Left = 581
    Top = 62
  end
  object DsEntidade: TDataSource
    DataSet = TbEntidade
    Left = 645
    Top = 51
  end
  object TbEntidade: TMySQLTable
    Database = Dmod.MyDB
    TableName = 'entidades'
    Left = 617
    Top = 50
    object TbEntidadeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbEntidadeLogo: TBlobField
      DisplayWidth = 4000000
      FieldName = 'Logo'
      Size = 4
    end
    object TbEntidadeLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntidadesAfterOpen
    BeforeClose = QrEntidadesBeforeClose
    AfterScroll = QrEntidadesAfterScroll
    OnCalcFields = QrEntidadesCalcFields
    SQL.Strings = (
      'SELECT ent.*, car.Nome NOMECARTPREF,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'ENTIDADE,'
      
        'CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial ELSE acm.Nome END NOME' +
        'ACCOUNT,'
      
        'eng.Nome NOMEENTIGRUPO, mot.Descricao NOMEMOTIVO, emp.RazaoSocia' +
        'l NOMEEMPRESA,'
      
        'euf.Nome NOMEEUF, puf.nome NOMEPUF, cuf.Nome NOMECUF, luf.nome N' +
        'OMELUF, '
      'nuf.Nome NOMENUF, elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,'
      'clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD, lec.Nome NOMEECIVIL'
      'FROM entidades ent'
      'LEFT JOIN entigrupos eng  ON eng.Codigo=ent.Grupo'
      'LEFT JOIN entidades acm   ON acm.Codigo=ent.Account'
      'LEFT JOIN entidades emp   ON emp.Codigo=ent.Empresa'
      'LEFT JOIN motivose mot    ON mot.Codigo=ent.Motivo'
      'LEFT JOIN ufs euf         ON euf.Codigo=ent.EUF'
      'LEFT JOIN ufs puf         ON puf.Codigo=ent.PUF'
      'LEFT JOIN ufs cuf         ON cuf.Codigo=ent.EUF'
      'LEFT JOIN ufs luf         ON luf.Codigo=ent.PUF'
      'LEFT JOIN ufs nuf         ON nuf.Codigo=ent.UFNatal'
      'LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd'
      'LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd'
      'LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd'
      'LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd'
      'LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil'
      'LEFT JOIN carteiras   car ON car.Codigo=ent.CartPref'
      'WHERE ent.Codigo>-2')
    Left = 449
    Top = 56
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntidadesRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrEntidadesFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrEntidadesRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrEntidadesRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrEntidadesPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrEntidadesMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrEntidadesCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntidadesIE: TWideStringField
      FieldName = 'IE'
      Size = 15
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrEntidadesApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrEntidadesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntidadesRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEntidadesERua: TWideStringField
      FieldName = 'ERua'
      Size = 60
    end
    object QrEntidadesECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 60
    end
    object QrEntidadesEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 60
    end
    object QrEntidadesECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 60
    end
    object QrEntidadesEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrEntidadesEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrEntidadesETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrEntidadesETe2: TWideStringField
      FieldName = 'ETe2'
    end
    object QrEntidadesETe3: TWideStringField
      FieldName = 'ETe3'
    end
    object QrEntidadesECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrEntidadesEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrEntidadesEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrEntidadesEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrEntidadesENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEntidadesPRua: TWideStringField
      FieldName = 'PRua'
      Size = 60
    end
    object QrEntidadesPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 60
    end
    object QrEntidadesPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 60
    end
    object QrEntidadesPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 60
    end
    object QrEntidadesPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrEntidadesPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrEntidadesPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrEntidadesPTe2: TWideStringField
      FieldName = 'PTe2'
    end
    object QrEntidadesPTe3: TWideStringField
      FieldName = 'PTe3'
    end
    object QrEntidadesPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrEntidadesPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrEntidadesPEMail: TWideStringField
      FieldName = 'PEMail'
      Size = 100
    end
    object QrEntidadesPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrEntidadesPNatal: TDateField
      FieldName = 'PNatal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEntidadesSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrEntidadesResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrEntidadesProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrEntidadesCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrEntidadesRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrEntidadesDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrEntidadesAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrEntidadesAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrEntidadesCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrEntidadesCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrEntidadesCliente3: TWideStringField
      FieldName = 'Cliente3'
      Size = 1
    end
    object QrEntidadesCliente4: TWideStringField
      FieldName = 'Cliente4'
      Size = 1
    end
    object QrEntidadesFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrEntidadesFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrEntidadesFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrEntidadesFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrEntidadesFornece5: TWideStringField
      FieldName = 'Fornece5'
      Size = 1
    end
    object QrEntidadesFornece6: TWideStringField
      FieldName = 'Fornece6'
      Size = 1
    end
    object QrEntidadesFornece7: TWideStringField
      FieldName = 'Fornece7'
      Size = 1
    end
    object QrEntidadesFornece8: TWideStringField
      FieldName = 'Fornece8'
      Size = 1
    end
    object QrEntidadesTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrEntidadesCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEntidadesInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrEntidadesLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrEntidadesVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrEntidadesMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrEntidadesObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrEntidadesTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntidadesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrEntidadesNOMEEUF: TWideStringField
      FieldName = 'NOMEEUF'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Required = True
      Size = 2
    end
    object QrEntidadesPCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE2_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE3_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE2_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE3_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesECEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesEFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEntidadesPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEntidadesGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrEntidadesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntidadesUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrEntidadesUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrEntidadesCRua: TWideStringField
      FieldName = 'CRua'
      Size = 60
    end
    object QrEntidadesCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 60
    end
    object QrEntidadesCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 60
    end
    object QrEntidadesCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 60
    end
    object QrEntidadesCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrEntidadesCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrEntidadesCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrEntidadesCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrEntidadesCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrEntidadesCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrEntidadesCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrEntidadesLRua: TWideStringField
      FieldName = 'LRua'
      Size = 60
    end
    object QrEntidadesLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 60
    end
    object QrEntidadesLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 60
    end
    object QrEntidadesLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 60
    end
    object QrEntidadesLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrEntidadesLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrEntidadesLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrEntidadesLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrEntidadesLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrEntidadesLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrEntidadesLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrEntidadesComissao: TFloatField
      FieldName = 'Comissao'
      DisplayFormat = '0.000000'
    end
    object QrEntidadesDataCad: TDateField
      FieldName = 'DataCad'
      Required = True
    end
    object QrEntidadesECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesPCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesCCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesLCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Size = 100
      Calculated = True
    end
    object QrEntidadesNOMEALT2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEALT2'
      Size = 100
      Calculated = True
    end
    object QrEntidadesNOMECAD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECAD'
      LookupDataSet = QrSenhas
      LookupKeyFields = 'Numero'
      LookupResultField = 'Login'
      KeyFields = 'UserCad'
      Size = 100
      Lookup = True
    end
    object QrEntidadesNOMEALT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMEALT'
      LookupDataSet = QrSenhas
      LookupKeyFields = 'Numero'
      LookupResultField = 'Login'
      KeyFields = 'UserAlt'
      Size = 100
      Lookup = True
    end
    object QrEntidadesSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrEntidadesNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrEntidadesCTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CTEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LTEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesNOMEENTIGRUPO: TWideStringField
      FieldName = 'NOMEENTIGRUPO'
      Size = 100
    end
    object QrEntidadesNOMECUF: TWideStringField
      FieldName = 'NOMECUF'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMELUF: TWideStringField
      FieldName = 'NOMELUF'
      Required = True
      Size = 2
    end
    object QrEntidadesAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEntidadesNOMEACCOUNT: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEACCOUNT'
      Size = 100
    end
    object QrEntidadesLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrEntidadesELograd: TSmallintField
      FieldName = 'ELograd'
      Required = True
    end
    object QrEntidadesPLograd: TSmallintField
      FieldName = 'PLograd'
      Required = True
    end
    object QrEntidadesConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrEntidadesConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEntidadesNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrEntidadesNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrEntidadesNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrEntidadesNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrEntidadesNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrEntidadesNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrEntidadesCreditosI: TIntegerField
      FieldName = 'CreditosI'
      Required = True
    end
    object QrEntidadesCreditosL: TIntegerField
      FieldName = 'CreditosL'
      Required = True
    end
    object QrEntidadesCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrEntidadesCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrEntidadesCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrEntidadesMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrEntidadesQuantI1: TIntegerField
      FieldName = 'QuantI1'
      Required = True
    end
    object QrEntidadesQuantI2: TIntegerField
      FieldName = 'QuantI2'
      Required = True
    end
    object QrEntidadesQuantI3: TIntegerField
      FieldName = 'QuantI3'
      Required = True
    end
    object QrEntidadesQuantI4: TIntegerField
      FieldName = 'QuantI4'
      Required = True
    end
    object QrEntidadesAgenda: TWideStringField
      FieldName = 'Agenda'
      Required = True
      Size = 1
    end
    object QrEntidadesSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Required = True
      Size = 1
    end
    object QrEntidadesSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrEntidadesNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrEntidadesNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrEntidadesNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Size = 50
    end
    object QrEntidadesCreditosF2: TFloatField
      FieldName = 'CreditosF2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesNOMEELOGRAD: TWideStringField
      FieldName = 'NOMEELOGRAD'
      Size = 10
    end
    object QrEntidadesNOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Size = 10
    end
    object QrEntidadesCLograd: TSmallintField
      FieldName = 'CLograd'
      Required = True
    end
    object QrEntidadesLLograd: TSmallintField
      FieldName = 'LLograd'
      Required = True
    end
    object QrEntidadesNOMECLOGRAD: TWideStringField
      FieldName = 'NOMECLOGRAD'
      Size = 10
    end
    object QrEntidadesNOMELLOGRAD: TWideStringField
      FieldName = 'NOMELLOGRAD'
      Size = 10
    end
    object QrEntidadesQuantN1: TFloatField
      FieldName = 'QuantN1'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesQuantN2: TFloatField
      FieldName = 'QuantN2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesLimiCred: TFloatField
      FieldName = 'LimiCred'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesDesco: TFloatField
      FieldName = 'Desco'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrEntidadesCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Required = True
    end
    object QrEntidadesCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrEntidadesSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrEntidadesCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrEntidadesCPF_PAI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_PAI_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Required = True
    end
    object QrEntidadesNOMENUF: TWideStringField
      FieldName = 'NOMENUF'
      Required = True
      Size = 2
    end
    object QrEntidadesFatorCompra: TFloatField
      FieldName = 'FatorCompra'
      DisplayFormat = '#,###,###0.0000'
    end
    object QrEntidadesAdValorem: TFloatField
      FieldName = 'AdValorem'
      DisplayFormat = '#,###,###0.0000'
    end
    object QrEntidadesDMaisC: TIntegerField
      FieldName = 'DMaisC'
      DisplayFormat = '0'
    end
    object QrEntidadesDMaisD: TIntegerField
      FieldName = 'DMaisD'
      DisplayFormat = '0'
    end
    object QrEntidadesDataRG: TDateField
      FieldName = 'DataRG'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEntidadesNacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Size = 15
    end
    object QrEntidadesEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEntidadesNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrEntidadesFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
    end
    object QrEntidadesSimples: TSmallintField
      FieldName = 'Simples'
      Required = True
    end
    object QrEntidadesAtividade: TWideStringField
      FieldName = 'Atividade'
      Size = 50
    end
    object QrEntidadesEstCivil: TSmallintField
      FieldName = 'EstCivil'
      Required = True
    end
    object QrEntidadesNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Required = True
      Size = 10
    end
    object QrEntidadesCPF_Conjuge: TWideStringField
      FieldName = 'CPF_Conjuge'
      Size = 18
    end
    object QrEntidadesCBE: TIntegerField
      FieldName = 'CBE'
    end
    object QrEntidadesSCB: TIntegerField
      FieldName = 'SCB'
    end
    object QrEntidadesCPF_Resp1: TWideStringField
      FieldName = 'CPF_Resp1'
      Size = 18
    end
    object QrEntidadesCPF_Resp1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_Resp1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEntidadesPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEntidadesCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrEntidadesLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrEntidadesBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrEntidadesAgencia: TWideStringField
      DisplayWidth = 4
      FieldName = 'Agencia'
      Size = 11
    end
    object QrEntidadesContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEntidadesCartPref: TIntegerField
      FieldName = 'CartPref'
      Required = True
    end
    object QrEntidadesNOMECARTPREF: TWideStringField
      FieldName = 'NOMECARTPREF'
      Size = 100
    end
    object QrEntidadesRolComis: TIntegerField
      FieldName = 'RolComis'
      Required = True
    end
    object QrEntidadesFilial: TIntegerField
      FieldName = 'Filial'
      Required = True
    end
    object QrEntidadesCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrEntidadesNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
  end
  object DsSenhas: TDataSource
    Left = 237
    Top = 270
  end
  object QrSenhas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM senhas')
    Left = 209
    Top = 271
    object QrSenhasLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'senhas.Login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'senhas.Numero'
    end
    object QrSenhasSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'senhas.Senha'
      Size = 30
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'senhas.Perfil'
    end
    object QrSenhasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'senhas.Lk'
    end
  end
  object QrEntiTransp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT et.*,'
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMEENTIDADE '
      'FROM entitransp et, entidades en'
      'WHERE et.Transp=en.Codigo'
      'AND et.Codigo=:P0'
      'ORDER BY Ordem')
    Left = 716
    Top = 206
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTranspCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiTranspConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrEntiTranspOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEntiTranspTransp: TIntegerField
      FieldName = 'Transp'
    end
    object QrEntiTranspNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEntiTransp: TDataSource
    DataSet = QrEntiTransp
    Left = 745
    Top = 204
  end
  object PMQuery: TPopupMenu
    Left = 300
    Top = 16
    object LocalizarEntidades1: TMenuItem
      Caption = 'Localizar &Entidades por...'
      object Nome1: TMenuItem
        Caption = '&Nome'
        OnClick = Nome1Click
      end
      object Dados2: TMenuItem
        Caption = '&Dados'
        OnClick = Dados2Click
      end
    end
    object LocalizarClienteporNotaFiscal1: TMenuItem
      Caption = 'Localizar &Cliente por ...'
      object porNotaFiscal1: TMenuItem
        Caption = '&Nota Fiscal'
        OnClick = porNotaFiscal1Click
      end
      object Produtoquecompra1: TMenuItem
        Caption = '&Produto que compra'
        Enabled = False
        OnClick = Produtoquecompra1Click
      end
    end
    object LocalizarFornecedorporNoitaFiscal1: TMenuItem
      Caption = 'Localizar &Fornecedor por ...'
      object porNotaFiscal2: TMenuItem
        Caption = '&Nota Fiscal'
        OnClick = porNotaFiscal2Click
      end
      object Produtoquevende1: TMenuItem
        Caption = '&Produto que vende'
        OnClick = Produtoquevende1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ConferirCdigosdeusurio1: TMenuItem
      Caption = 'Atribuir c'#243'digos de usu'#225'rio'
      OnClick = ConferirCdigosdeusurio1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Exportacelularesdepessoas1: TMenuItem
      Caption = 'Exporta celulares de pessoas'
      OnClick = Exportacelularesdepessoas1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 24
    Top = 12
    object Pesquisas1: TMenuItem
      Caption = '&Pesquisar antes de imprimir'
      OnClick = Pesquisas1Click
    end
    object Dados1: TMenuItem
      Caption = '&Dados das entidades selecionadas'
      OnClick = Dados1Click
    end
    object Dadoscompactosdaentidadeatual1: TMenuItem
      Caption = 'Dados compactos da entidade &atual'
      OnClick = Dadoscompactosdaentidadeatual1Click
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 541
    Top = 317
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Required = True
      Size = 100
    end
    object QrLocCNPJ_CPF: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrLocIE_RG: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'IE_RG'
    end
    object QrLocCONTATO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CONTATO'
      Size = 60
    end
    object QrLocRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 60
    end
    object QrLocCompl: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Compl'
      Size = 60
    end
    object QrLocBairro: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Bairro'
      Size = 60
    end
    object QrLocCidade: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Cidade'
      Size = 60
    end
    object QrLocUF: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'UF'
      Required = True
    end
    object QrLocCEP: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'CEP'
    end
    object QrLocPais: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Pais'
    end
    object QrLocNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrLocPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrLocPTe2: TWideStringField
      FieldName = 'PTe2'
    end
    object QrLocPTe3: TWideStringField
      FieldName = 'PTe3'
    end
    object QrLocETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrLocETe2: TWideStringField
      FieldName = 'ETe2'
    end
    object QrLocETe3: TWideStringField
      FieldName = 'ETe3'
    end
    object QrLocPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrLocEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrLocPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrLocECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrLocCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrLocCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrLocCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrLocLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrLocLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrLocLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrLocNumero: TFloatField
      FieldName = 'Numero'
    end
  end
  object DsLoc: TDataSource
    DataSet = QrEntiTransp
    Left = 569
    Top = 317
  end
  object QrCadastro2: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCadastro2CalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, Observacoes,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial else en.Nome     END NOM' +
        'EENTIDADE,'
      
        'CASE WHEN en.Tipo=0 THEN en.Fantasia    else en.Apelido  END FAN' +
        'TASIA_APELIDO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        else en.PRua     END RUA' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero+0.000     else en.PNumero+0.' +
        '000  END NUMERO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      else en.PCompl   END COM' +
        'PL,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        else en.PCEP     END CEP' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     else en.PBairro  END BAI' +
        'RRO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     else en.PCidade  END CID' +
        'ADE,'
      'CASE WHEN en.Tipo=0 THEN en.EUF         else en.PUF      END UF,'
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       else en.PPais    END PAI' +
        'S,'
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        else en.PTe1     END TE1' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.ETe2        else en.PTe2     END TE2' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.ETe3        else en.PTe3     END TE3' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.ECel        else en.PCel     END CEL' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        else en.PFax     END FAX' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.EEmail      else en.PEmail   END EMA' +
        'IL,'
      
        'CASE WHEN en.Tipo=0 THEN en.ENatal      else en.PNatal   END NAT' +
        'AL,'
      
        'CASE WHEN en.Tipo=0 THEN en.EContato    else en.PContato END CON' +
        'TATO,'
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        else en.CPF      END DOC' +
        'A,'
      
        'CASE WHEN en.Tipo=0 THEN en.IE          else en.RG       END DOC' +
        'B,'
      
        'CASE WHEN en.Tipo=0 THEN " "            else en.Sexo     END Sex' +
        'o,'
      
        'uf.Nome NOMEUF,  Cargo, Profissao, ufc.Nome NOMECUF, ufl.Nome NO' +
        'MELUF,'
      'CRua, CCompl, CNumero, CCEP, CBairro, CCidade, CUF, CPais,'
      'LRua, LCompl, LNumero, LCEP, LBairro, LCidade, LUF, LPais,'
      'ENatal, PNatal'
      'FROM entidades en, ufs uf, ufs ufc, ufs ufl'
      
        'WHERE uf.Codigo=(CASE WHEN en.Tipo=0 THEN en.EUF else en.PUF END' +
        ')'
      'AND ufc.Codigo=en.CUF AND ufl.Codigo=en.LUF'
      'AND en.Codigo=:P0')
    Left = 385
    Top = 373
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCadastro2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrCadastro2Tipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrCadastro2NOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrCadastro2FANTASIA_APELIDO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FANTASIA_APELIDO'
      Size = 60
    end
    object QrCadastro2RUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 60
    end
    object QrCadastro2COMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 60
    end
    object QrCadastro2CEP: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'CEP'
    end
    object QrCadastro2BAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrCadastro2CIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CIDADE'
      Size = 60
    end
    object QrCadastro2UF: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'UF'
    end
    object QrCadastro2PAIS: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'PAIS'
    end
    object QrCadastro2TE1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TE1'
    end
    object QrCadastro2TE2: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TE2'
    end
    object QrCadastro2TE3: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TE3'
    end
    object QrCadastro2CEL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CEL'
    end
    object QrCadastro2FAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrCadastro2EMAIL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrCadastro2CONTATO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CONTATO'
      Size = 60
    end
    object QrCadastro2NATAL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NATAL'
      Size = 10
    end
    object QrCadastro2DOCA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'DOCA'
      Size = 18
    end
    object QrCadastro2DOCB: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'DOCB'
    end
    object QrCadastro2Sexo: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Sexo'
      Size = 1
    end
    object QrCadastro2NOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrCadastro2CRua: TWideStringField
      FieldName = 'CRua'
      Origin = 'entidades.CRua'
      Size = 60
    end
    object QrCadastro2CCompl: TWideStringField
      FieldName = 'CCompl'
      Origin = 'entidades.CCompl'
      Size = 60
    end
    object QrCadastro2CCEP: TIntegerField
      FieldName = 'CCEP'
      Origin = 'entidades.CCEP'
    end
    object QrCadastro2CBairro: TWideStringField
      FieldName = 'CBairro'
      Origin = 'entidades.CBairro'
      Size = 60
    end
    object QrCadastro2CCidade: TWideStringField
      FieldName = 'CCidade'
      Origin = 'entidades.CCidade'
      Size = 60
    end
    object QrCadastro2CUF: TSmallintField
      FieldName = 'CUF'
      Origin = 'entidades.CUF'
      Required = True
    end
    object QrCadastro2CPais: TWideStringField
      FieldName = 'CPais'
      Origin = 'entidades.CPais'
    end
    object QrCadastro2LRua: TWideStringField
      FieldName = 'LRua'
      Origin = 'entidades.LRua'
      Size = 60
    end
    object QrCadastro2LCompl: TWideStringField
      FieldName = 'LCompl'
      Origin = 'entidades.LCompl'
      Size = 60
    end
    object QrCadastro2LCEP: TIntegerField
      FieldName = 'LCEP'
      Origin = 'entidades.LCEP'
    end
    object QrCadastro2LBairro: TWideStringField
      FieldName = 'LBairro'
      Origin = 'entidades.LBairro'
      Size = 60
    end
    object QrCadastro2LCidade: TWideStringField
      FieldName = 'LCidade'
      Origin = 'entidades.LCidade'
      Size = 60
    end
    object QrCadastro2LUF: TSmallintField
      FieldName = 'LUF'
      Origin = 'entidades.LUF'
      Required = True
    end
    object QrCadastro2LPais: TWideStringField
      FieldName = 'LPais'
      Origin = 'entidades.LPais'
    end
    object QrCadastro2TE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2TE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE2_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2TE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE3_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2FAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2CEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2DOCA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCA_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2Cargo: TWideStringField
      FieldName = 'Cargo'
      Origin = 'entidades.Cargo'
      Size = 60
    end
    object QrCadastro2Profissao: TWideStringField
      FieldName = 'Profissao'
      Origin = 'entidades.Profissao'
      Size = 60
    end
    object QrCadastro2NUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUM_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 30
      Calculated = True
    end
    object QrCadastro2CNUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNUM_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2LNUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LNUM_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2NOMECUF: TWideStringField
      FieldName = 'NOMECUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrCadastro2NOMELUF: TWideStringField
      FieldName = 'NOMELUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrCadastro2CCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEP_TXT'
      Size = 30
      Calculated = True
    end
    object QrCadastro2LCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEP_TXT'
      Size = 30
      Calculated = True
    end
    object QrCadastro2NATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 50
      Calculated = True
    end
    object QrCadastro2ENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrCadastro2PNatal: TDateField
      FieldName = 'PNatal'
      Origin = 'entidades.PNatal'
    end
    object QrCadastro2Observacoes: TWideMemoField
      FieldName = 'Observacoes'
      Origin = 'entidades.Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCadastro2NUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrCadastro2CNumero: TIntegerField
      FieldName = 'CNumero'
      Origin = 'entidades.CNumero'
    end
    object QrCadastro2LNumero: TIntegerField
      FieldName = 'LNumero'
      Origin = 'entidades.LNumero'
    end
  end
  object PMNovo: TPopupMenu
    Left = 364
    Top = 445
    object Abaixode10001: TMenuItem
      Caption = 'A&baixo de 1.000'
      OnClick = Abaixode10001Click
    end
    object Acimade10001: TMenuItem
      Caption = 'A&cima de 1.000'
      OnClick = Acimade10001Click
    end
  end
  object QrLoc1000: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Codigo) CodMin'
      'FROM entidades'
      'WHERE Codigo>1000')
    Left = 720
    Top = 37
    object QrLoc1000CodMin: TIntegerField
      FieldName = 'CodMin'
      Required = True
    end
  end
  object QrLoc0: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Codigo) CodMax, MIN(Codigo) CodMin'
      'FROM entidades'
      'WHERE Codigo<1000')
    Left = 732
    Top = 65
    object QrLoc0CodMin: TIntegerField
      FieldName = 'CodMin'
      Required = True
    end
    object QrLoc0CodMax: TIntegerField
      FieldName = 'CodMax'
      Required = True
    end
  end
  object frxCadastro: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38011.488035601900000000
    ReportOptions.LastChange = 39495.767711585600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCadastroGetValue
    Left = 440
    Top = 345
    Datasets = <
      item
        DataSet = frxDsCadastro
        DataSetName = 'frxDsCadastro'
      end
      item
        DataSet = frxDsMaster_
        DataSetName = 'frxDsMaster_'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -7
      Font.Name = 'Arial'
      Font.Style = []
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 287.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 1084.725110000000000000
        object Memo32: TfrxMemoView
          Left = 818.000000000000000000
          Top = 1.102350000000001000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 24.000000000000000000
        Top = 846.614720000000000000
        Width = 1084.725110000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 582.000000000000000000
        Top = 204.094620000000000000
        Width = 1084.725110000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCadastro
        DataSetName = 'frxDsCadastro'
        RowCount = 0
        object Memo31: TfrxMemoView
          Left = 758.000000000000000000
          Top = 555.905380000000100000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 71.322820000000000000
          Top = 111.936920000000000000
          Width = 200.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PRua"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 71.322820000000000000
          Top = 91.936919999999960000
          Width = 200.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Logradouro')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          Left = 271.322820000000000000
          Top = 111.936920000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0;-0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PNumero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 311.322820000000000000
          Top = 111.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PCompl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 491.322820000000000000
          Top = 111.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PBairro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 671.322820000000000000
          Top = 111.936920000000000000
          Width = 140.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PCidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 271.322820000000000000
          Top = 91.936919999999960000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'N'#250'mero')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 311.322820000000000000
          Top = 91.936919999999960000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Complemento')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 491.322820000000000000
          Top = 91.936919999999960000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Bairro')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 671.322820000000000000
          Top = 91.936919999999960000
          Width = 140.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Cidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 835.322820000000000000
          Top = 111.936920000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PCEP_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 835.322820000000000000
          Top = 91.936919999999960000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 811.322820000000000000
          Top = 111.936920000000000000
          Width = 24.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCadastro."NOMEPUF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 811.322820000000000000
          Top = 91.936919999999960000
          Width = 24.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'UF')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 887.322820000000000000
          Top = 111.936920000000000000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PPais"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 71.322820000000000000
          Top = 129.936920000000000000
          Width = 200.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ERua"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 311.322820000000000000
          Top = 129.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ECompl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 491.322820000000000000
          Top = 129.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."EBairro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 671.322820000000000000
          Top = 129.936920000000000000
          Width = 140.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ECidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 835.322820000000000000
          Top = 129.936920000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ECEP_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 811.322820000000000000
          Top = 129.936920000000000000
          Width = 24.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCadastro."NOMEEUF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 887.322820000000000000
          Top = 129.936920000000000000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          DataField = 'EPais'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."EPais"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 71.322820000000000000
          Top = 147.936920000000000000
          Width = 200.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CRua"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 311.322820000000000000
          Top = 147.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CCompl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 491.322820000000000000
          Top = 147.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CBairro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 671.322820000000000000
          Top = 147.936920000000000000
          Width = 140.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CCidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 835.322820000000000000
          Top = 147.936920000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CCEP_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 811.322820000000000000
          Top = 147.936920000000000000
          Width = 24.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCadastro."NOMECUF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 887.322820000000000000
          Top = 147.936920000000000000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          DataField = 'CPais'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CPais"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 71.322820000000000000
          Top = 165.936920000000000000
          Width = 200.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LRua"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 311.322820000000000000
          Top = 165.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LCompl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 491.322820000000000000
          Top = 165.936920000000000000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LBairro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 671.322820000000000000
          Top = 165.936920000000000000
          Width = 140.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LCidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 835.322820000000000000
          Top = 165.936920000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LCEP_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 811.322820000000000000
          Top = 165.936920000000000000
          Width = 24.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCadastro."NOMELUF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 887.322820000000000000
          Top = 165.936920000000000000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          DataField = 'LPais'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LPais"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 887.322820000000000000
          Top = 91.936919999999960000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'PA'#205'S')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 271.322820000000000000
          Top = 129.936920000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0;-0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ENumero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 271.322820000000000000
          Top = 147.936920000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0;-0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CNumero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 271.322820000000000000
          Top = 165.936920000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0;-0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LNumero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 9.322820000000000000
          Top = 111.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Pessoal')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 9.322820000000000000
          Top = 129.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          Left = 9.322820000000000000
          Top = 165.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Entrega')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          Left = 9.322820000000000000
          Top = 147.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Cobran'#231'a')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          Left = 71.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CTEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          Left = 71.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          Left = 167.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 167.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Fax')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          Left = 263.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CCEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 263.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          Left = 407.322820000000000000
          Top = 207.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Entrega')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          Left = 7.322820000000000000
          Top = 207.936920000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Cobran'#231'a')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          Left = 471.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LTEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          Left = 567.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          Left = 663.322820000000000000
          Top = 207.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."LCEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          Left = 7.322820000000000000
          Top = 231.936920000000000000
          Width = 1000.000000000000000000
          Height = 342.000000000000000000
          DataField = 'Observacoes'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."Observacoes"]')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Left = 471.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone')
          ParentFont = False
          WordWrap = False
        end
        object Memo94: TfrxMemoView
          Left = 567.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Fax')
          ParentFont = False
          WordWrap = False
        end
        object Memo95: TfrxMemoView
          Left = 663.322820000000000000
          Top = 187.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
          WordWrap = False
        end
        object Memo96: TfrxMemoView
          Left = 911.322820000000000000
          Top = 211.936920000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 7.322820000000000000
          Top = 3.936919999999987000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Pessoa F'#237'sica')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 335.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'CPF')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          Left = 431.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'RG')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          Left = 527.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 1')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          Left = 623.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 2')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          Left = 719.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 3')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 815.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Fax')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          Left = 911.322820000000000000
          Top = 3.936919999999987000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 7.322820000000000000
          Top = 23.936919999999990000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          DataField = 'Nome'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 335.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'PCPF_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PCPF_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Left = 431.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'RG'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."RG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 7.322820000000000000
          Top = 67.936919999999960000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          DataField = 'RazaoSocial'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."RazaoSocial"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 7.322820000000000000
          Top = 47.936919999999990000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          Left = 527.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'PTE1_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PTE1_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 623.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'PTE2_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PTE2_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 719.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'PTE3_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PTE3_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 815.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 911.322820000000000000
          Top = 23.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'PCEL_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."PCEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 335.322820000000000000
          Top = 67.936919999999960000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'CNPJ_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."CNPJ_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 335.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          Left = 431.322820000000000000
          Top = 67.936919999999960000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'IE'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."IE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 431.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'I.E.')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          Left = 527.322820000000000000
          Top = 67.936919999999960000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'ETE1_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ETE1_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 527.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 1')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          Left = 623.322820000000000000
          Top = 67.936919999999960000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'ETE2_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ETE2_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 623.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 2')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          Left = 719.322820000000000000
          Top = 67.936919999999960000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'ETE3_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ETE3_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 719.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Telefone 3')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          Left = 815.322820000000000000
          Top = 67.936919999999960000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'EFAX_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."EFAX_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 815.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Fax')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          Left = 911.322820000000000000
          Top = 67.936919999999960000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DataField = 'ECEL_TXT'
          DataSet = frxDsCadastro
          DataSetName = 'frxDsCadastro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro."ECEL_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 911.322820000000000000
          Top = 47.936919999999990000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 74.000000000000000000
        Top = 68.031540000000010000
        Width = 1084.725110000000000000
        object Memo7: TfrxMemoView
          Left = 183.102350000000000000
          Top = 27.086580000000000000
          Width = 828.000000000000000000
          Height = 46.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[ENTIDADE]')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 7.322820000000000000
          Top = 3.086579999999998000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 183.102350000000000000
          Top = 3.086579999999998000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster_."Em"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsCadastro: TfrxDBDataset
    UserName = 'frxDsCadastro'
    CloseDataSource = False
    DataSet = QrEntidades
    BCDToCurrency = False
    Left = 412
    Top = 345
  end
  object frxCadastro2: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38011.488035601900000000
    ReportOptions.LastChange = 39540.733483067100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCadastroGetValue
    Left = 440
    Top = 373
    Datasets = <
      item
        DataSet = frxDsCadastro2
        DataSetName = 'frxDsCadastro2'
      end
      item
        DataSet = frxDsMaster_
        DataSetName = 'frxDsMaster_'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo32: TfrxMemoView
          Left = 528.000000000000000000
          Top = 0.102350000000001300
          Width = 163.000000000000000000
          Height = 16.000000000000000000
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 12.000000000000000000
        Top = 604.724800000000000000
        Width = 755.906000000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 438.000000000000000000
        Top = 105.826840000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCadastro2
        DataSetName = 'frxDsCadastro2'
        RowCount = 0
        object Memo7: TfrxMemoView
          Left = 8.000000000000000000
          Top = 46.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Fone 1:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 44.000000000000000000
          Top = 46.968459999999990000
          Width = 99.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."TE1_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 8.000000000000000000
          Top = 66.968459999999990000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[_CNPJ_CPF]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 44.000000000000000000
          Top = 66.968459999999990000
          Width = 92.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."DOCA_TXT"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 144.000000000000000000
          Top = 46.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Fone 2:')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 180.000000000000000000
          Top = 46.968459999999990000
          Width = 99.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."TE2_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 280.000000000000000000
          Top = 46.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Fone3:')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 316.000000000000000000
          Top = 46.968459999999990000
          Width = 99.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."TE3_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 416.000000000000000000
          Top = 46.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 451.000000000000000000
          Top = 46.968459999999990000
          Width = 100.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."FAX_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 551.000000000000000000
          Top = 46.968459999999990000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Celular:')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 588.000000000000000000
          Top = 46.968459999999990000
          Width = 128.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CEL_TXT"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 136.000000000000000000
          Top = 66.968459999999990000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[_IE_RG]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 172.000000000000000000
          Top = 66.968459999999990000
          Width = 92.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."DOCB"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 7.000000000000000000
          Top = 86.968459999999990000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Contato:')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 47.000000000000000000
          Top = 86.968459999999990000
          Width = 148.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CONTATO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 264.000000000000000000
          Top = 66.968459999999990000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 303.000000000000000000
          Top = 66.968459999999990000
          Width = 412.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."EMAIL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 195.000000000000000000
          Top = 86.968459999999990000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cargo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 235.000000000000000000
          Top = 86.968459999999990000
          Width = 148.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."Cargo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 383.000000000000000000
          Top = 86.968459999999990000
          Width = 48.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Profiss'#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 431.000000000000000000
          Top = 86.968459999999990000
          Width = 148.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."Profissao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 579.000000000000000000
          Top = 86.968459999999990000
          Width = 68.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[_NATAL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 647.000000000000000000
          Top = 86.968459999999990000
          Width = 68.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NATAL_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 7.000000000000000000
          Top = 106.968460000000000000
          Width = 96.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Left = 8.000000000000000000
          Top = 126.968460000000000000
          Width = 24.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Rua:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 31.000000000000000000
          Top = 126.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."RUA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 207.000000000000000000
          Top = 126.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'N'#250'mero:')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          Left = 247.000000000000000000
          Top = 126.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DataField = 'NUMERO'
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NUMERO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          Left = 303.000000000000000000
          Top = 126.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Compl:')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 339.000000000000000000
          Top = 126.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."COMPL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          Left = 515.000000000000000000
          Top = 126.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          Left = 551.000000000000000000
          Top = 126.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."BAIRRO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          Left = 7.000000000000000000
          Top = 146.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          Left = 43.000000000000000000
          Top = 146.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CIDADE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          Left = 207.000000000000000000
          Top = 146.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Estado:')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          Left = 247.000000000000000000
          Top = 146.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DataField = 'NOMEUF'
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NOMEUF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 303.000000000000000000
          Top = 146.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 339.000000000000000000
          Top = 146.968460000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CEP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          Left = 411.000000000000000000
          Top = 146.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Pa'#237's:')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          Left = 447.000000000000000000
          Top = 146.968460000000000000
          Width = 268.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."PAIS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          Left = 7.000000000000000000
          Top = 166.968460000000000000
          Width = 96.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cobran'#231'a:')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 7.000000000000000000
          Top = 186.968460000000000000
          Width = 24.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Rua:')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 31.000000000000000000
          Top = 186.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CRua"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          Left = 207.000000000000000000
          Top = 186.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'N'#250'mero:')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          Left = 247.000000000000000000
          Top = 186.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CNUM_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          Left = 303.000000000000000000
          Top = 186.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Compl:')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 339.000000000000000000
          Top = 186.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CCompl"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          Left = 515.000000000000000000
          Top = 186.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          Left = 551.000000000000000000
          Top = 186.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CBairro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          Left = 7.000000000000000000
          Top = 206.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          Left = 43.000000000000000000
          Top = 206.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CCidade"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          Left = 207.000000000000000000
          Top = 206.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Estado:')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          Left = 247.000000000000000000
          Top = 206.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NOMECUF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          Left = 303.000000000000000000
          Top = 206.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          Left = 339.000000000000000000
          Top = 206.968460000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CCEP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          Left = 411.000000000000000000
          Top = 206.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Pa'#237's:')
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          Left = 447.000000000000000000
          Top = 206.968460000000000000
          Width = 268.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."CPais"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 7.000000000000000000
          Top = 226.968460000000000000
          Width = 96.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Entrega:')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          Left = 7.000000000000000000
          Top = 246.968460000000000000
          Width = 24.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Rua:')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          Left = 31.000000000000000000
          Top = 246.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LRua"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          Left = 207.000000000000000000
          Top = 246.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'N'#250'mero:')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          Left = 247.000000000000000000
          Top = 246.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LNUM_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          Left = 303.000000000000000000
          Top = 246.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Compl:')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          Left = 339.000000000000000000
          Top = 246.968460000000000000
          Width = 176.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LCompl"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          Left = 516.000000000000000000
          Top = 246.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          Left = 552.000000000000000000
          Top = 246.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LBairro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          Left = 7.000000000000000000
          Top = 266.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          Left = 43.000000000000000000
          Top = 266.968460000000000000
          Width = 164.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LCidade"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          Left = 207.000000000000000000
          Top = 266.968460000000000000
          Width = 40.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Estado:')
          ParentFont = False
          WordWrap = False
        end
        object Memo80: TfrxMemoView
          Left = 247.000000000000000000
          Top = 266.968460000000000000
          Width = 56.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NOMELUF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          Left = 303.000000000000000000
          Top = 266.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          Left = 339.000000000000000000
          Top = 266.968460000000000000
          Width = 72.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LCEP_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          Left = 411.000000000000000000
          Top = 266.968460000000000000
          Width = 36.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Pa'#237's:')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          Left = 447.000000000000000000
          Top = 266.968460000000000000
          Width = 268.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."LPais"]')
          ParentFont = False
          WordWrap = False
        end
        object Line3: TfrxLineView
          Left = 8.000000000000000000
          Top = 286.968460000000000000
          Width = 708.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo85: TfrxMemoView
          Left = 8.000000000000000000
          Top = 286.968460000000000000
          Width = 708.000000000000000000
          Height = 148.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsCadastro2."Observacoes"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 8.000000000000000000
          Top = 2.968459999999993000
          Width = 551.000000000000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster_."Em"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          Left = 8.000000000000000000
          Top = 26.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'digo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 44.000000000000000000
          Top = 26.968459999999990000
          Width = 67.000000000000000000
          Height = 20.000000000000000000
          DataField = 'Codigo'
          DataSet = frxDsCadastro2
          DataSetName = 'frxDsCadastro2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 112.000000000000000000
          Top = 26.968459999999990000
          Width = 35.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 148.000000000000000000
          Top = 26.968459999999990000
          Width = 247.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."NOMEENTIDADE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 396.000000000000000000
          Top = 26.968459999999990000
          Width = 71.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[_FANTASIA]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 468.000000000000000000
          Top = 26.968459999999990000
          Width = 247.000000000000000000
          Height = 20.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCadastro2."FANTASIA_APELIDO"]')
          ParentFont = False
          WordWrap = False
        end
        object Line1: TfrxLineView
          Left = 8.000000000000000000
          Top = 2.968459999999993000
          Width = 708.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line2: TfrxLineView
          Left = 8.000000000000000000
          Top = 2.968459999999993000
          Height = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line4: TfrxLineView
          Left = 716.000000000000000000
          Top = 2.968459999999993000
          Height = 284.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object Memo31: TfrxMemoView
        Left = 760.000000000000000000
        Top = 759.000000000000000000
        Width = 244.000000000000000000
        Height = 14.000000000000000000
        StretchMode = smMaxHeight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Width = 2.000000000000000000
        HAlign = haRight
        ParentFont = False
      end
    end
  end
  object frxDsCadastro2: TfrxDBDataset
    UserName = 'frxDsCadastro2'
    CloseDataSource = False
    DataSet = QrCadastro2
    BCDToCurrency = False
    Left = 412
    Top = 373
  end
  object QrEntiCtas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.*, ban.Nome NOMEBANCO'
      'FROM entictas cta'
      'LEFT JOIN entidades ent ON ent.Codigo=cta.Codigo'
      'LEFT JOIN bancos ban ON ban.Codigo=cta.Banco'
      'WHERE cta.Codigo=:P0'
      'ORDER BY cta.Ordem')
    Left = 716
    Top = 250
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCtasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiCtasControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiCtasOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEntiCtasBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrEntiCtasAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrEntiCtasContaCor: TWideStringField
      FieldName = 'ContaCor'
      Size = 10
    end
    object QrEntiCtasContaTip: TWideStringField
      FieldName = 'ContaTip'
      Size = 10
    end
    object QrEntiCtasDAC_A: TWideStringField
      FieldName = 'DAC_A'
      Size = 1
    end
    object QrEntiCtasDAC_C: TWideStringField
      FieldName = 'DAC_C'
      Size = 1
    end
    object QrEntiCtasDAC_AC: TWideStringField
      FieldName = 'DAC_AC'
      Size = 1
    end
    object QrEntiCtasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEntiCtasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEntiCtasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntiCtasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEntiCtasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEntiCtasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEntiCtasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEntiCtasNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
  end
  object DsEntiCtas: TDataSource
    DataSet = QrEntiCtas
    Left = 745
    Top = 251
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtSaida
    CanUpd01 = BtImporta
    CanDel01 = BtEntidades2
    Left = 52
    Top = 12
  end
  object frxDsMaster_: TfrxDBDataset
    UserName = 'frxDsMaster_'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 468
    Top = 344
  end
  object PMImporta: TPopupMenu
    Left = 680
    Top = 24
    object CdAdm1: TMenuItem
      Caption = '&CdAdm'
      OnClick = CdAdm1Click
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 688
    Top = 308
  end
  object QrCondImov: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imv.Unidade, cnd.Cliente,'
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_CND,'
      'CONCAT('
      '  IF(imv.Propriet=:Pa,"D",""),'
      '  IF(imv.Conjuge=:Pa,"C",""),'
      '  IF(imv.Usuario=:Pa,"M",""),'
      '  IF(imv.Procurador=:Pa,"P",""),'
      '  IF(imv.BloqEndEnt=:Pa,"T","")'
      ') TIPO '
      'FROM condimov imv'
      'LEFT JOIN cond cnd ON cnd.Codigo=imv.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE Propriet=:Pa'
      'OR Conjuge=:Pa'
      'OR Usuario=:Pa'
      'OR Procurador=:Pa'
      'OR BloqEndEnt=:Pa')
    Left = 660
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Pa'
        ParamType = ptUnknown
      end>
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrCondImovCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCondImovNO_CND: TWideStringField
      FieldName = 'NO_CND'
      Size = 100
    end
    object QrCondImovTIPO: TWideStringField
      FieldName = 'TIPO'
      Required = True
      Size = 5
    end
  end
end
