unit EntidadesLoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Db, (*DBTables,*) Grids, DBGrids, UnInternalConsts,
  mySQLDbTables, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmEntidadesLoc = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    DsLoc: TDataSource;
    QrLoc: TmySQLQuery;
    QrLocCodigo: TIntegerField;
    QrLocRazaoSocial: TWideStringField;
    QrLocFantasia: TWideStringField;
    QrLocRespons1: TWideStringField;
    QrLocRespons2: TWideStringField;
    QrLocPai: TWideStringField;
    QrLocMae: TWideStringField;
    QrLocCNPJ: TWideStringField;
    QrLocIE: TWideStringField;
    QrLocNome: TWideStringField;
    QrLocApelido: TWideStringField;
    QrLocCPF: TWideStringField;
    QrLocRG: TWideStringField;
    QrLocERua: TWideStringField;
    QrLocECompl: TWideStringField;
    QrLocEBairro: TWideStringField;
    QrLocECidade: TWideStringField;
    QrLocEUF: TSmallintField;
    QrLocEPais: TWideStringField;
    QrLocETe1: TWideStringField;
    QrLocETe2: TWideStringField;
    QrLocETe3: TWideStringField;
    QrLocECel: TWideStringField;
    QrLocEFax: TWideStringField;
    QrLocEEMail: TWideStringField;
    QrLocEContato: TWideStringField;
    QrLocENatal: TDateField;
    QrLocPRua: TWideStringField;
    QrLocPCompl: TWideStringField;
    QrLocPBairro: TWideStringField;
    QrLocPCidade: TWideStringField;
    QrLocPUF: TSmallintField;
    QrLocPPais: TWideStringField;
    QrLocPTe1: TWideStringField;
    QrLocPTe2: TWideStringField;
    QrLocPTe3: TWideStringField;
    QrLocPCel: TWideStringField;
    QrLocPFax: TWideStringField;
    QrLocPEMail: TWideStringField;
    QrLocPContato: TWideStringField;
    QrLocPNatal: TDateField;
    QrLocSexo: TWideStringField;
    QrLocResponsavel: TWideStringField;
    QrLocProfissao: TWideStringField;
    QrLocCargo: TWideStringField;
    QrLocRecibo: TSmallintField;
    QrLocDiaRecibo: TSmallintField;
    QrLocAjudaEmpV: TFloatField;
    QrLocAjudaEmpP: TFloatField;
    QrLocCliente1: TWideStringField;
    QrLocCliente2: TWideStringField;
    QrLocFornece1: TWideStringField;
    QrLocFornece2: TWideStringField;
    QrLocFornece3: TWideStringField;
    QrLocFornece4: TWideStringField;
    QrLocTerceiro: TWideStringField;
    QrLocCadastro: TDateField;
    QrLocInformacoes: TWideStringField;
    QrLocLogo: TBlobField;
    QrLocVeiculo: TIntegerField;
    QrLocMensal: TWideStringField;
    QrLocObservacoes: TWideMemoField;
    QrLocTipo: TSmallintField;
    QrLocLk: TIntegerField;
    QrLocCNPJ_TXT: TWideStringField;
    QrLocENumero: TIntegerField;
    QrLocPNumero: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbNumero: TBitBtn;
    SbQuery: TBitBtn;
    SbNome: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrLocCalcFields(DataSet: TDataSet);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmEntidadesLoc: TFmEntidadesLoc;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmEntidadesLoc.BtDesisteClick(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  Close;
end;

procedure TFmEntidadesLoc.SbNomeClick(Sender: TObject);
var
  Nome : String;
begin
  Nome := '%%';
  if InputQuery('Localizar pelo Nome', 'Digite parte da descri��o', Nome ) then
  begin
    if Nome <> CO_VAZIO then
    begin
      QrLoc.Close;
      QrLoc.SQL.Clear;
      QrLoc.SQL.Add('SELECT * FROM entidades');
      QrLoc.SQL.Add('WHERE');
      QrLoc.SQL.Add('(CASE WHEN Tipo=0 THEN RazaoSocial');
      QrLoc.SQL.Add('ELSE NOME END) LIKE :P0');
      QrLoc.SQL.Add('ORDER BY');
      QrLoc.SQL.Add('(CASE WHEN Tipo=0 THEN RazaoSocial');
      QrLoc.SQL.Add('ELSE NOME END)');
      QrLoc.Params[0].AsString := Nome;
      UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
    end;
  end;  
end;

procedure TFmEntidadesLoc.SbNumeroClick(Sender: TObject);
var
  Num : String;
  Cod : Integer;
begin
  Num := '';
  if InputQuery('Localizar pelo C�digo','Digite o c�digo.', Num ) then
  begin
    try
      Cod := StrToInt(Num);
      if Cod <> 0 then
      begin
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT * FROM entidades ');
        QrLoc.SQL.Add('WHERE Codigo=:P0');
        QrLoc.SQL.Add('ORDER BY Codigo');
        QrLoc.Params[0].AsInteger := Cod;
        UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
      end;
    except
      on EConvertError do
        if Num <> '' then
          Geral.MB_Aviso('N�mero inv�lido');
    end;
  end;
end;

procedure TFmEntidadesLoc.SbQueryClick(Sender: TObject);
var
  Nome : String;
begin
  Nome := CO_VAZIO;
  if InputQuery('Localizar pelo CNPJ', 'Digite o CNPJ', Nome) then
  begin
    Nome := Geral.SoNumero_TT(Nome);
    if Nome <> CO_VAZIO then
    begin
      QrLoc.Close;
      QrLoc.SQL.Clear;
      QrLoc.SQL.Add('SELECT * FROM entidades ');
      QrLoc.SQL.Add('WHERE CNPJ=:P0');
      QrLoc.SQL.Add('ORDER BY RazaoSocial');
      QrLoc.Params[0].AsString := Nome;
      UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
    end;
  end;
end;

procedure TFmEntidadesLoc.BtConfirmaClick(Sender: TObject);
begin
  VAR_ENTIDADE := QrLocCodigo.Value;
  Close;
end;

procedure TFmEntidadesLoc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntidadesLoc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
end;

procedure TFmEntidadesLoc.QrLocCalcFields(DataSet: TDataSet);
begin
  QrLocCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrLocCNPJ.Value);
end;

procedure TFmEntidadesLoc.DBGrid1DblClick(Sender: TObject);
begin
  VAR_ENTIDADE := QrLocCodigo.Value;
  Close;
end;

procedure TFmEntidadesLoc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

