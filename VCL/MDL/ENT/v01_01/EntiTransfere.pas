unit EntiTransfere;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Grids, DBGrids,
  dmkGeral, dmkImage, dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmEntiTransfere = class(TForm)
    PainelDados: TPanel;
    DsEntidades: TDataSource;
    QrEntidades: TmySQLQuery;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesCodigo: TIntegerField;
    Panel1: TPanel;
    Label1: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label2: TLabel;
    QrEntiT: TmySQLQuery;
    QrEntiTTabela: TWideStringField;
    QrEntiTCampo: TWideStringField;
    QrEntiTRegistros: TIntegerField;
    QrReg: TmySQLQuery;
    QrRegRegistros: TLargeintField;
    QrEntiTransfere: TmySQLQuery;
    StringField1: TWideStringField;
    StringField2: TWideStringField;
    IntegerField1: TIntegerField;
    DBGrid1: TDBGrid;
    DsEntiTransfere: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FEntidadeAtual: Integer;
    procedure PesquisaTabelas;
  end;

var
  FmEntiTransfere: TFmEntiTransfere;

implementation

uses UnMyObjects, Module, Entidades, DmkDAC_PF;

{$R *.DFM}

procedure TFmEntiTransfere.BtOKClick(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := Geral.IMV(EdEntidade.Text);
  if Entidade = 0 then
  begin
    Geral.MB_Aviso('Defina a entidade receptora!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  QrEntiT.Close;
  UnDmkDAC_PF.AbreQuery(QrEntiT, Dmod.MyDB);
  QrEntiT.First;
  while not QrEntiT.Eof do
  begin
    if QrEntiTRegistros.Value > 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + Lowercase(QrEntiTTabela.Value));
      Dmod.QrUpd.SQL.Add('SET '+QrEntiTCampo.Value);
      Dmod.QrUpd.SQL.Add('='+IntToStr(Entidade));
      Dmod.QrUpd.SQL.Add('WHERE '+QrEntiTCampo.Value);
      Dmod.QrUpd.SQL.Add('='+IntToStr(FEntidadeAtual));
      Dmod.QrUpd.ExecSQL;
    end;
    QrEntiT.Next;
  end;
  Screen.Cursor := crDefault;
  //
  FmEntidades.BtExclui.Enabled := True;
  Close;
end;

procedure TFmEntiTransfere.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiTransfere.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiTransfere.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  QrEntidades.Close;
  QrEntidades.Params[0].AsInteger := FEntidadeAtual;
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  PesquisaTabelas;
end;

procedure TFmEntiTransfere.PesquisaTabelas;
var
  Abre: Boolean;
begin
  Abre := False;
  QrEntiT.Close;
  UnDmkDAC_PF.AbreQuery(QrEntiT, Dmod.MyDB);
  if QrEntiT.RecordCount = 0 then
  begin
    Geral.MB_Erro('Este applicativo n�o est� habilitado para '+
    'tranferir dados entre entidades. Entre em contato com a Dermatek!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE entitransfere SET Registros=0');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE entitransfere SET Registros=:P00');
  Dmod.QrUpd.SQL.Add('WHERE Tabela=:P01');
  Dmod.QrUpd.SQL.Add('AND Campo=:P02');
  while not QrEntiT.Eof do
  begin
    Abre := True;
    try
      QrReg.Close;
      QrReg.SQL.Clear;
      QrReg.SQL.Add('SELECT COUNT('+QrEntiTCampo.Value+') Registros ');
      QrReg.SQL.Add('FROM '+ Lowercase(QrEntiTTabela.Value)+' ');
      QrReg.SQL.Add('WHERE '+QrEntiTCampo.Value+'='+IntToStr(FEntidadeAtual));
      UnDmkDAC_PF.AbreQuery(QrReg, Dmod.MyDB);
      //
      if QrRegRegistros.Value > 0 then
      begin
        Dmod.QrUpd.Params[00].AsInteger := QrRegRegistros.Value;
        Dmod.QrUpd.Params[01].AsString  := QrEntiTTabela.Value;
        Dmod.QrUpd.Params[02].AsString  := QrEntiTCampo.Value;
        Dmod.QrUpd.ExecSQL;
      end;
    except
      Screen.Cursor := crDefault;
      Geral.MB_Erro('N�o foi poss�vel verificar os registros '+
      'do campo"'+QrEntiTCampo.Value+'" da tabela "'+QrEntiTTabela.Value+
      '". Entre em contato com a Dermatek!');
      raise;
    end;
    QrEntiT.Next;
  end;
  QrEntiTransfere.Close;
  if Abre then
  begin
    UnDmkDAC_PF.AbreQuery(QrEntiTransfere, Dmod.MyDB);
    if QrEntiTransfere.RecordCount > 0 then BtConfirma.Enabled := True;
  end;
  Screen.Cursor := crDefault;
end;

end.

