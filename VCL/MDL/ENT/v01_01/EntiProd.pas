unit EntiProd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts,Buttons,DBCtrls, Db, (*DBTables,*)
  UnGOTOy, UMySQLModule, Mask,Grids, DBGrids, mySQLDbTables,
  Variants, dmkGeral, dmkImage, dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmEntiProd = class(TForm)
    PainelDados: TPanel;
    DsProdutos: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    EdProduto: TdmkEditCB;
    Label2: TLabel;
    CBProduto: TdmkDBLookupComboBox;
    DBGrid1: TDBGrid;
    DsEntidades: TDataSource;
    QrProdutos: TmySQLQuery;
    QrProdutosCodigo: TIntegerField;
    QrProdutosNome: TWideStringField;
    QrEntidades: TmySQLQuery;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesCODIGO: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    BtLocaliza1: TBitBtn;
    procedure EdProdutoChange(Sender: TObject);
    procedure CBProdutoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrEntidadesAfterOpen(DataSet: TDataSet);
    procedure QrEntidadesAfterClose(DataSet: TDataSet);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtLocaliza1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmEntiProd: TFmEntiProd;

implementation

uses UnMyObjects, Module, Entidades, DmkDAC_PF;

{$R *.DFM}

procedure TFmEntiProd.BtSaidaClick(Sender: TObject);
begin
  if QrEntidadesCODIGO.Value > 0 then VAR_ENTIDADE := QrEntidadesCODIGO.Value;
  Close;
end;

procedure TFmEntiProd.EdProdutoChange(Sender: TObject);
begin
  QrEntidades.Close;
end;

procedure TFmEntiProd.CBProdutoClick(Sender: TObject);
begin
  QrEntidades.Close;
end;

procedure TFmEntiProd.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(QrProdutos, Dmod.MyDB);
end;

procedure TFmEntiProd.QrEntidadesAfterOpen(DataSet: TDataSet);
begin
  if GOTOy.Registros(QrEntidades) > 0 then
  begin
    BtConfirma.Enabled := True;
  end else BtConfirma.Enabled := False;
end;

procedure TFmEntiProd.QrEntidadesAfterClose(DataSet: TDataSet);
begin
  BtConfirma.Enabled := False;
end;

procedure TFmEntiProd.DBGrid1DblClick(Sender: TObject);
begin
  BtConfirmaClick(Self);
end;

procedure TFmEntiProd.BtLocaliza1Click(Sender: TObject);
begin
  if CBProduto.KeyValue = Null then
  begin
    Geral.MB_Aviso('Informe um produto!');
    EdProduto.SetFocus;
    Exit;
  end;
  QrEntidades.Close;
  QrEntidades.SQL.Clear;
  QrEntidades.SQL.Add('SELECT');
  QrEntidades.SQL.Add('CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrEntidades.SQL.Add('ELSE en.Nome END NOMEENTIDADE,');
  if LaTitulo1A.Caption = 'Fornecedor' then
  begin
    QrEntidades.SQL.Add('pf.Fornece CODIGO');
    QrEntidades.SQL.Add('FROM entidades en, produtosfor pf');
    QrEntidades.SQL.Add('WHERE en.Codigo=pf.Fornece');
    QrEntidades.SQL.Add('AND pf.Codigo=:P0');
  end else
    Geral.MB_Info('Em Construção!');
  QrEntidades.Params[0].AsInteger := CBProduto.KeyValue;
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmEntiProd.BtConfirmaClick(Sender: TObject);
begin
  if QrEntidadesCODIGO.Value > 0 then
  begin
    VAR_ENTIDADE := QrEntidadesCODIGO.Value;
    Close;
  end;
end;

procedure TFmEntiProd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiProd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.

