unit Entidade3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, ShellAPI,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, dmkDBGrid,
  dmkCompoStore;

type
  TFmEntidade3 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesCodUsu: TIntegerField;
    QrEntidadesRazaoSocial: TWideStringField;
    QrEntidadesFantasia: TWideStringField;
    QrEntidadesRespons1: TWideStringField;
    QrEntidadesRespons2: TWideStringField;
    QrEntidadesPai: TWideStringField;
    QrEntidadesMae: TWideStringField;
    QrEntidadesCNPJ: TWideStringField;
    QrEntidadesIE: TWideStringField;
    QrEntidadesNome: TWideStringField;
    QrEntidadesApelido: TWideStringField;
    QrEntidadesCPF: TWideStringField;
    QrEntidadesRG: TWideStringField;
    QrEntidadesERua: TWideStringField;
    QrEntidadesECompl: TWideStringField;
    QrEntidadesEBairro: TWideStringField;
    QrEntidadesECidade: TWideStringField;
    QrEntidadesEUF: TSmallintField;
    QrEntidadesEPais: TWideStringField;
    QrEntidadesETe1: TWideStringField;
    QrEntidadesETe2: TWideStringField;
    QrEntidadesETe3: TWideStringField;
    QrEntidadesECel: TWideStringField;
    QrEntidadesEFax: TWideStringField;
    QrEntidadesEEMail: TWideStringField;
    QrEntidadesEContato: TWideStringField;
    QrEntidadesPRua: TWideStringField;
    QrEntidadesPCompl: TWideStringField;
    QrEntidadesPBairro: TWideStringField;
    QrEntidadesPCidade: TWideStringField;
    QrEntidadesPUF: TSmallintField;
    QrEntidadesPPais: TWideStringField;
    QrEntidadesPTe1: TWideStringField;
    QrEntidadesPTe2: TWideStringField;
    QrEntidadesPTe3: TWideStringField;
    QrEntidadesPCel: TWideStringField;
    QrEntidadesPFax: TWideStringField;
    QrEntidadesPEMail: TWideStringField;
    QrEntidadesPContato: TWideStringField;
    QrEntidadesSexo: TWideStringField;
    QrEntidadesResponsavel: TWideStringField;
    QrEntidadesProfissao: TWideStringField;
    QrEntidadesCargo: TWideStringField;
    QrEntidadesRecibo: TSmallintField;
    QrEntidadesDiaRecibo: TSmallintField;
    QrEntidadesAjudaEmpV: TFloatField;
    QrEntidadesAjudaEmpP: TFloatField;
    QrEntidadesCliente1: TWideStringField;
    QrEntidadesCliente2: TWideStringField;
    QrEntidadesCliente3: TWideStringField;
    QrEntidadesCliente4: TWideStringField;
    QrEntidadesFornece1: TWideStringField;
    QrEntidadesFornece2: TWideStringField;
    QrEntidadesFornece3: TWideStringField;
    QrEntidadesFornece4: TWideStringField;
    QrEntidadesFornece5: TWideStringField;
    QrEntidadesFornece6: TWideStringField;
    QrEntidadesFornece7: TWideStringField;
    QrEntidadesFornece8: TWideStringField;
    QrEntidadesTerceiro: TWideStringField;
    QrEntidadesCadastro: TDateField;
    QrEntidadesInformacoes: TWideStringField;
    QrEntidadesLogo: TBlobField;
    QrEntidadesVeiculo: TIntegerField;
    QrEntidadesMensal: TWideStringField;
    QrEntidadesObservacoes: TWideMemoField;
    QrEntidadesTipo: TSmallintField;
    QrEntidadesLk: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesNOMEEUF: TWideStringField;
    QrEntidadesNOMEPUF: TWideStringField;
    QrEntidadesPCPF_TXT: TWideStringField;
    QrEntidadesPTE1_TXT: TWideStringField;
    QrEntidadesPTE2_TXT: TWideStringField;
    QrEntidadesPTE3_TXT: TWideStringField;
    QrEntidadesPCEL_TXT: TWideStringField;
    QrEntidadesPFAX_TXT: TWideStringField;
    QrEntidadesETE1_TXT: TWideStringField;
    QrEntidadesETE2_TXT: TWideStringField;
    QrEntidadesETE3_TXT: TWideStringField;
    QrEntidadesECEL_TXT: TWideStringField;
    QrEntidadesEFAX_TXT: TWideStringField;
    QrEntidadesCNPJ_TXT: TWideStringField;
    QrEntidadesECEP: TIntegerField;
    QrEntidadesPCEP: TIntegerField;
    QrEntidadesGrupo: TIntegerField;
    QrEntidadesDataAlt: TDateField;
    QrEntidadesUserCad: TSmallintField;
    QrEntidadesUserAlt: TSmallintField;
    QrEntidadesCRua: TWideStringField;
    QrEntidadesCCompl: TWideStringField;
    QrEntidadesCBairro: TWideStringField;
    QrEntidadesCCidade: TWideStringField;
    QrEntidadesCUF: TSmallintField;
    QrEntidadesCCEP: TIntegerField;
    QrEntidadesCPais: TWideStringField;
    QrEntidadesCTel: TWideStringField;
    QrEntidadesCFax: TWideStringField;
    QrEntidadesCCel: TWideStringField;
    QrEntidadesCContato: TWideStringField;
    QrEntidadesLRua: TWideStringField;
    QrEntidadesLCompl: TWideStringField;
    QrEntidadesLBairro: TWideStringField;
    QrEntidadesLCidade: TWideStringField;
    QrEntidadesLUF: TSmallintField;
    QrEntidadesLCEP: TIntegerField;
    QrEntidadesLPais: TWideStringField;
    QrEntidadesLTel: TWideStringField;
    QrEntidadesLFax: TWideStringField;
    QrEntidadesLCel: TWideStringField;
    QrEntidadesLContato: TWideStringField;
    QrEntidadesComissao: TFloatField;
    QrEntidadesDataCad: TDateField;
    QrEntidadesECEP_TXT: TWideStringField;
    QrEntidadesPCEP_TXT: TWideStringField;
    QrEntidadesCCEP_TXT: TWideStringField;
    QrEntidadesLCEP_TXT: TWideStringField;
    QrEntidadesNOMECAD2: TWideStringField;
    QrEntidadesNOMEALT2: TWideStringField;
    QrEntidadesSituacao: TSmallintField;
    QrEntidadesNivel: TWideStringField;
    QrEntidadesCTEL_TXT: TWideStringField;
    QrEntidadesCFAX_TXT: TWideStringField;
    QrEntidadesCCEL_TXT: TWideStringField;
    QrEntidadesLTEL_TXT: TWideStringField;
    QrEntidadesLFAX_TXT: TWideStringField;
    QrEntidadesLCEL_TXT: TWideStringField;
    QrEntidadesNOMEENTIGRUPO: TWideStringField;
    QrEntidadesNOMECUF: TWideStringField;
    QrEntidadesNOMELUF: TWideStringField;
    QrEntidadesAccount: TIntegerField;
    QrEntidadesNOMEACCOUNT: TWideStringField;
    QrEntidadesLogo2: TBlobField;
    QrEntidadesELograd: TSmallintField;
    QrEntidadesPLograd: TSmallintField;
    QrEntidadesConjugeNome: TWideStringField;
    QrEntidadesConjugeNatal: TDateField;
    QrEntidadesNome1: TWideStringField;
    QrEntidadesNatal1: TDateField;
    QrEntidadesNome2: TWideStringField;
    QrEntidadesNatal2: TDateField;
    QrEntidadesNome3: TWideStringField;
    QrEntidadesNatal3: TDateField;
    QrEntidadesCreditosI: TIntegerField;
    QrEntidadesCreditosL: TIntegerField;
    QrEntidadesCreditosD: TDateField;
    QrEntidadesCreditosU: TDateField;
    QrEntidadesCreditosV: TDateField;
    QrEntidadesMotivo: TIntegerField;
    QrEntidadesQuantI1: TIntegerField;
    QrEntidadesQuantI2: TIntegerField;
    QrEntidadesQuantI3: TIntegerField;
    QrEntidadesQuantI4: TIntegerField;
    QrEntidadesAgenda: TWideStringField;
    QrEntidadesSenhaQuer: TWideStringField;
    QrEntidadesSenha1: TWideStringField;
    QrEntidadesNatal4: TDateField;
    QrEntidadesNome4: TWideStringField;
    QrEntidadesNOMEMOTIVO: TWideStringField;
    QrEntidadesCreditosF2: TFloatField;
    QrEntidadesNOMEELOGRAD: TWideStringField;
    QrEntidadesNOMEPLOGRAD: TWideStringField;
    QrEntidadesCLograd: TSmallintField;
    QrEntidadesLLograd: TSmallintField;
    QrEntidadesNOMECLOGRAD: TWideStringField;
    QrEntidadesNOMELLOGRAD: TWideStringField;
    QrEntidadesQuantN1: TFloatField;
    QrEntidadesQuantN2: TFloatField;
    QrEntidadesLimiCred: TFloatField;
    QrEntidadesDesco: TFloatField;
    QrEntidadesCasasApliDesco: TSmallintField;
    QrEntidadesCPF_Pai: TWideStringField;
    QrEntidadesSSP: TWideStringField;
    QrEntidadesCidadeNatal: TWideStringField;
    QrEntidadesCPF_PAI_TXT: TWideStringField;
    QrEntidadesUFNatal: TSmallintField;
    QrEntidadesNOMENUF: TWideStringField;
    QrEntidadesFatorCompra: TFloatField;
    QrEntidadesAdValorem: TFloatField;
    QrEntidadesDMaisC: TIntegerField;
    QrEntidadesDMaisD: TIntegerField;
    QrEntidadesDataRG: TDateField;
    QrEntidadesNacionalid: TWideStringField;
    QrEntidadesEmpresa: TIntegerField;
    QrEntidadesNOMEEMPRESA: TWideStringField;
    QrEntidadesFormaSociet: TWideStringField;
    QrEntidadesSimples: TSmallintField;
    QrEntidadesAtividade: TWideStringField;
    QrEntidadesEstCivil: TSmallintField;
    QrEntidadesNOMEECIVIL: TWideStringField;
    QrEntidadesCPF_Conjuge: TWideStringField;
    QrEntidadesCBE: TIntegerField;
    QrEntidadesSCB: TIntegerField;
    QrEntidadesCPF_Resp1: TWideStringField;
    QrEntidadesCPF_Resp1_TXT: TWideStringField;
    QrEntidadesENumero: TIntegerField;
    QrEntidadesPNumero: TIntegerField;
    QrEntidadesCNumero: TIntegerField;
    QrEntidadesLNumero: TIntegerField;
    QrEntidadesBanco: TIntegerField;
    QrEntidadesAgencia: TWideStringField;
    QrEntidadesContaCorrente: TWideStringField;
    QrEntidadesCartPref: TIntegerField;
    QrEntidadesNOMECARTPREF: TWideStringField;
    QrEntidadesRolComis: TIntegerField;
    QrEntidadesFilial: TIntegerField;
    QrEntidadesIEST: TWideStringField;
    QrEntidadesQuantN3: TFloatField;
    QrEntidadesTempA: TFloatField;
    QrEntidadesTempD: TFloatField;
    QrEntidadesPAtividad: TIntegerField;
    QrEntidadesEAtividad: TIntegerField;
    QrEntidadesPCidadeCod: TIntegerField;
    QrEntidadesECidadeCod: TIntegerField;
    QrEntidadesPPaisCod: TIntegerField;
    QrEntidadesEPaisCod: TIntegerField;
    QrEntidadesAntigo: TWideStringField;
    QrEntidadesCUF2: TWideStringField;
    QrEntidadesContab: TWideStringField;
    QrEntidadesMSN1: TWideStringField;
    QrEntidadesPastaTxtFTP: TWideStringField;
    QrEntidadesPastaPwdFTP: TWideStringField;
    QrEntidadesProtestar: TSmallintField;
    QrEntidadesMultaCodi: TSmallintField;
    QrEntidadesMultaDias: TSmallintField;
    QrEntidadesMultaValr: TFloatField;
    QrEntidadesMultaPerc: TFloatField;
    QrEntidadesMultaTiVe: TSmallintField;
    QrEntidadesJuroSacado: TFloatField;
    QrEntidadesCPMF: TFloatField;
    QrEntidadesCorrido: TIntegerField;
    QrEntidadesCliInt: TIntegerField;
    QrEntidadesAltDtPlaCt: TDateField;
    QrEntidadesAlterWeb: TSmallintField;
    QrEntidadesAtivo: TSmallintField;
    QrEntidadesEEndeRef: TWideStringField;
    QrEntidadesPEndeRef: TWideStringField;
    QrEntidadesCEndeRef: TWideStringField;
    QrEntidadesLEndeRef: TWideStringField;
    QrEntidadesPNUMERO_TXT: TWideStringField;
    QrEntidadesENUMERO_TXT: TWideStringField;
    QrEntidadesCNUMERO_TXT: TWideStringField;
    QrEntidadesLNUMERO_TXT: TWideStringField;
    QrEntidadesIE_TXT: TWideStringField;
    QrEntidadesDATARG_TXT: TWideStringField;
    QrEntidadesECodMunici: TIntegerField;
    QrEntidadesPCodMunici: TIntegerField;
    QrEntidadesCCodMunici: TIntegerField;
    QrEntidadesLCodMunici: TIntegerField;
    QrEntidadesCNAE: TWideStringField;
    QrEntidadesSUFRAMA: TWideStringField;
    QrEntidadesECodiPais: TIntegerField;
    QrEntidadesPCodiPais: TIntegerField;
    QrEntidadesL_CNPJ: TWideStringField;
    QrEntidadesL_Ativo: TSmallintField;
    QrEntidadesL_CNPJ_TXT: TWideStringField;
    QrEntidadesNO_DTB_EMUNICI: TWideStringField;
    QrEntidadesNO_DTB_PMUNICI: TWideStringField;
    QrEntidadesNO_DTB_CMUNICI: TWideStringField;
    QrEntidadesNO_DTB_LMUNICI: TWideStringField;
    QrEntidadesNO_BACEN_EPAIS: TWideStringField;
    QrEntidadesNO_BACEN_PPAIS: TWideStringField;
    QrEntidadesCNAE_Nome: TWideStringField;
    QrEntidadesNIRE: TWideStringField;
    QrEntidadesIEST_TXT: TWideStringField;
    QrEntidadesURL: TWideStringField;
    QrEntidadesCRT: TSmallintField;
    QrEntidadesCOD_PART: TWideStringField;
    QrEntidadesETe1Tip: TIntegerField;
    QrEntidadesECelTip: TIntegerField;
    QrEntidadesPTe1Tip: TIntegerField;
    QrEntidadesPCelTip: TIntegerField;
    QrEntidadesPTe1Tip_TXT: TWideStringField;
    QrEntidadesPCelTip_TXT: TWideStringField;
    QrEntidadesETe1Tip_TXT: TWideStringField;
    QrEntidadesECelTip_TXT: TWideStringField;
    QrEntidadesENATAL_TXT: TWideStringField;
    QrEntidadesPNATAL_TXT: TWideStringField;
    QrEntidadesENatal: TDateField;
    QrEntidadesPNatal: TDateField;
    QrEntidadesESite: TWideStringField;
    QrEntidadesPSite: TWideStringField;
    QrEntidadesindIEDest: TSmallintField;
    QrEntidadesEstrangDef: TSmallintField;
    QrEntidadesEstrangTip: TSmallintField;
    QrEntidadesEstrangNum: TWideStringField;
    QrEntiContat: TmySQLQuery;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    QrEntiContatCargo: TIntegerField;
    QrEntiContatNOME_CARGO: TWideStringField;
    QrEntiContatDtaNatal: TDateField;
    QrEntiContatDTANATAL_TXT: TWideStringField;
    QrEntiContatSexo: TSmallintField;
    QrEntiContatSEXO_TXT: TWideStringField;
    QrEntiContatCodigo: TIntegerField;
    QrEntiContatTipo: TIntegerField;
    QrEntiContatCNPJ: TWideStringField;
    QrEntiContatCPF: TWideStringField;
    QrEntiContatAplicacao: TIntegerField;
    QrEntiContatCNPJ_CPF: TWideStringField;
    QrEntiContatAtivo: TSmallintField;
    DsEntiContat: TDataSource;
    QrEntiTel: TmySQLQuery;
    QrEntiTelNOMEETC: TWideStringField;
    QrEntiTelConta: TIntegerField;
    QrEntiTelTelefone: TWideStringField;
    QrEntiTelEntiTipCto: TIntegerField;
    QrEntiTelTEL_TXT: TWideStringField;
    QrEntiTelRamal: TWideStringField;
    DsEntiTel: TDataSource;
    QrEntiMail: TmySQLQuery;
    QrEntiMailNOMEETC: TWideStringField;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailEntiTipCto: TIntegerField;
    QrEntiMailOrdem: TIntegerField;
    DsEntiMail: TDataSource;
    QrAtrEntiDef: TmySQLQuery;
    QrAtrEntiDefID_Item: TIntegerField;
    QrAtrEntiDefID_Sorc: TIntegerField;
    QrAtrEntiDefAtrCad: TIntegerField;
    QrAtrEntiDefATRITS: TFloatField;
    QrAtrEntiDefCU_CAD: TIntegerField;
    QrAtrEntiDefCU_ITS: TLargeintField;
    QrAtrEntiDefNO_CAD: TWideStringField;
    QrAtrEntiDefNO_ITS: TWideStringField;
    QrAtrEntiDefAtrTyp: TSmallintField;
    QrAtrEntiDefAtrTxt: TWideStringField;
    DsAtrEntiDef: TDataSource;
    QrEntiRespon: TmySQLQuery;
    QrEntiResponCodigo: TIntegerField;
    QrEntiResponControle: TIntegerField;
    QrEntiResponNome: TWideStringField;
    QrEntiResponCargo: TIntegerField;
    QrEntiResponAssina: TSmallintField;
    QrEntiResponOrdemLista: TIntegerField;
    QrEntiResponObserv: TWideStringField;
    QrEntiResponNOME_CARGO: TWideStringField;
    QrEntiResponNO_ASSINA: TWideStringField;
    QrEntiResponMandatoIni: TDateField;
    QrEntiResponMandatoFim: TDateField;
    QrEntiResponMandatoIni_TXT: TWideStringField;
    QrEntiResponMandatoFim_TXT: TWideStringField;
    QrEntiResponTe1: TWideStringField;
    QrEntiResponCel: TWideStringField;
    QrEntiResponEmail: TWideStringField;
    QrEntiResponEntidade: TIntegerField;
    QrEntiResponTe1_TXT: TWideStringField;
    QrEntiResponCel_TXT: TWideStringField;
    DsEntiRespon: TDataSource;
    DsEntiCtas: TDataSource;
    QrEntiCtas: TmySQLQuery;
    QrEntiCtasCodigo: TIntegerField;
    QrEntiCtasControle: TIntegerField;
    QrEntiCtasOrdem: TIntegerField;
    QrEntiCtasBanco: TIntegerField;
    QrEntiCtasAgencia: TIntegerField;
    QrEntiCtasContaCor: TWideStringField;
    QrEntiCtasContaTip: TWideStringField;
    QrEntiCtasDAC_A: TWideStringField;
    QrEntiCtasDAC_C: TWideStringField;
    QrEntiCtasDAC_AC: TWideStringField;
    QrEntiCtasLk: TIntegerField;
    QrEntiCtasDataCad: TDateField;
    QrEntiCtasDataAlt: TDateField;
    QrEntiCtasUserCad: TIntegerField;
    QrEntiCtasUserAlt: TIntegerField;
    QrEntiCtasAlterWeb: TSmallintField;
    QrEntiCtasAtivo: TSmallintField;
    QrEntiCtasNOMEBANCO: TWideStringField;
    QrEntiSrvPro: TmySQLQuery;
    QrEntiSrvProNO_ORGAO: TWideStringField;
    QrEntiSrvProCodigo: TIntegerField;
    QrEntiSrvProControle: TIntegerField;
    QrEntiSrvProOrgao: TIntegerField;
    QrEntiSrvProData: TDateField;
    QrEntiSrvProHora: TTimeField;
    QrEntiSrvProNumero: TWideStringField;
    QrEntiSrvProResultado: TWideStringField;
    DsEntiSrvPro: TDataSource;
    PnDados: TPanel;
    GBDados: TGroupBox;
    Label3: TLabel;
    Label2: TLabel;
    DBLaindIEDest: TLabel;
    dmkDBEdit2: TDBEdit;
    dmkDBEdit1: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    DBEdindIEDest: TDBEdit;
    DBEdindIEDest_TXT: TEdit;
    dmkRadioGroup1: TDBRadioGroup;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PCDados: TPageControl;
    TSComercial: TTabSheet;
    ScrollBox1: TScrollBox;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label62: TLabel;
    Label60: TLabel;
    Label118: TLabel;
    Label117: TLabel;
    Label58: TLabel;
    Label133: TLabel;
    Label132: TLabel;
    Label131: TLabel;
    Label63: TLabel;
    Label121: TLabel;
    Label89: TLabel;
    Label136: TLabel;
    Label126: TLabel;
    Label122: TLabel;
    Label15: TLabel;
    Label25: TLabel;
    Label23: TLabel;
    Label156: TLabel;
    Label100: TLabel;
    Label142: TLabel;
    SBESite: TSpeedButton;
    Label54: TLabel;
    dmkDBEdit31: TDBEdit;
    dmkDBEdit24: TDBEdit;
    dmkDBEdit25: TDBEdit;
    dmkDBEdit26: TDBEdit;
    dmkDBEdit32: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    dmkDBEdit29: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit9: TDBEdit;
    dmkDBEdit27: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    dmkDBEdit33: TDBEdit;
    DBEdit14: TDBEdit;
    dmkDBEdit15: TDBEdit;
    Memo2: TMemo;
    DBEdCRT: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit28: TDBEdit;
    dmkDBEdit30: TDBEdit;
    dmkDBEdit60: TDBEdit;
    dmkDBEdit23: TDBEdit;
    TSResidencial: TTabSheet;
    ScrollBox2: TScrollBox;
    Label40: TLabel;
    Label158: TLabel;
    Label159: TLabel;
    Label152: TLabel;
    Label153: TLabel;
    Label43: TLabel;
    Label48: TLabel;
    Label42: TLabel;
    Label41: TLabel;
    Label44: TLabel;
    Label119: TLabel;
    Label46: TLabel;
    Label120: TLabel;
    Label102: TLabel;
    Label49: TLabel;
    Label47: TLabel;
    Label61: TLabel;
    Label69: TLabel;
    Label71: TLabel;
    Label59: TLabel;
    Label91: TLabel;
    SBPSite: TSpeedButton;
    Label161: TLabel;
    dmkDBEdit8: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    dmkDBEdit17: TDBEdit;
    dmkDBEdit11: TDBEdit;
    dmkDBEdit10: TDBEdit;
    dmkDBEdit9: TDBEdit;
    dmkDBEdit16: TDBEdit;
    dmkDBEdit12: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    dmkDBEdit14: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    dmkDBEdit40: TDBEdit;
    dmkDBEdit18: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    dmkDBEdit13: TDBEdit;
    dmkDBEdit28: TDBEdit;
    dmkDBEdit42: TDBEdit;
    TSContatos: TTabSheet;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    DBGEntiContat: TdmkDBGrid;
    DBGEntiTel: TdmkDBGrid;
    DBGEntiMail: TdmkDBGrid;
    TSTipo: TTabSheet;
    Label128: TLabel;
    Label138: TLabel;
    Label140: TLabel;
    GroupBox1: TGroupBox;
    CkCliente1: TDBCheckBox;
    CkFornece1: TDBCheckBox;
    CkFornece2: TDBCheckBox;
    CkFornece3: TDBCheckBox;
    CkTerceiro: TDBCheckBox;
    CkFornece4: TDBCheckBox;
    CkCliente2: TDBCheckBox;
    CkFornece5: TDBCheckBox;
    CkFornece6: TDBCheckBox;
    CkCliente3: TDBCheckBox;
    CkCliente4: TDBCheckBox;
    CkFornece8: TDBCheckBox;
    CkFornece7: TDBCheckBox;
    dmkDBEdit57: TDBEdit;
    dmkDBEdit58: TDBEdit;
    dmkDBEdit59: TDBEdit;
    TSEndCobran: TTabSheet;
    ScrollBox3: TScrollBox;
    Label64: TLabel;
    Label65: TLabel;
    Label68: TLabel;
    Label73: TLabel;
    Label116: TLabel;
    Label70: TLabel;
    Label72: TLabel;
    Label67: TLabel;
    Label66: TLabel;
    dmkDBEdit35: TDBEdit;
    dmkDBEdit43: TDBEdit;
    dmkDBEdit36: TDBEdit;
    dmkDBEdit39: TDBEdit;
    dmkDBEdit45: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    dmkDBEdit41: TDBEdit;
    dmkDBEdit44: TDBEdit;
    dmkDBEdit38: TDBEdit;
    dmkDBEdit37: TDBEdit;
    TSEndEntrega: TTabSheet;
    ScrollBox4: TScrollBox;
    Label114: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label82: TLabel;
    Label80: TLabel;
    Label115: TLabel;
    Label78: TLabel;
    Label83: TLabel;
    DBEdit1: TDBEdit;
    dmkDBEdit46: TDBEdit;
    dmkDBEdit54: TDBEdit;
    dmkDBEdit47: TDBEdit;
    dmkDBEdit48: TDBEdit;
    dmkDBEdit49: TDBEdit;
    dmkDBEdit55: TDBEdit;
    dmkDBEdit52: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    dmkDBEdit50: TDBEdit;
    dmkDBEdit56: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    TSObservacoes: TTabSheet;
    DBMemo1: TDBMemo;
    TSEstrangeiro: TTabSheet;
    DBGBEstrangeiro: TGroupBox;
    Panel26: TPanel;
    Label147: TLabel;
    DBRadioGroup2: TDBRadioGroup;
    DBEdit25: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    TSAtributos: TTabSheet;
    GradeDefAtr: TdmkDBGrid;
    TSResponsaveis: TTabSheet;
    Label125: TLabel;
    DBGEntiRespon: TDBGrid;
    TSDadosBancarios: TTabSheet;
    DBGEntiCtas: TDBGrid;
    TSConsultas: TTabSheet;
    DBGEntiSrvPro: TDBGrid;
    PCPessoa: TPageControl;
    TSJuridica: TTabSheet;
    Label52: TLabel;
    Label53: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    dmkDBEdit22: TDBEdit;
    dmkDBEdit21: TDBEdit;
    dmkDBEdit19: TDBEdit;
    dmkDBEdit20: TDBEdit;
    TSFisica: TTabSheet;
    Label33: TLabel;
    Label30: TLabel;
    Label28: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label39: TLabel;
    dmkDBEdit5: TDBEdit;
    dmkDBEdit4: TDBEdit;
    dmkDBEdit3: TDBEdit;
    dmkDBEdit6: TDBEdit;
    dmkDBEdit7: TDBEdit;
    dmkDBEdit34: TDBEdit;
    CSTabSheetChamou: TdmkCompoStore;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEntidadesAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEntidadesBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrEntidadesCalcFields(DataSet: TDataSet);
    procedure QrEntidadesAfterScroll(DataSet: TDataSet);
    procedure SBESiteClick(Sender: TObject);
    procedure SBPSiteClick(Sender: TObject);
    procedure QrEntiContatCalcFields(DataSet: TDataSet);
    procedure QrEntidadesBeforeClose(DataSet: TDataSet);
    procedure QrEntiTelCalcFields(DataSet: TDataSet);
    procedure QrEntiContatBeforeClose(DataSet: TDataSet);
    procedure QrEntiContatAfterScroll(DataSet: TDataSet);
    procedure QrEntiResponCalcFields(DataSet: TDataSet);
    procedure DBGEntiResponDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(SQLType: TSQLType);
    procedure AbreWEBSiteEntidade(Site: String);
    procedure ReopenEntiContat(Controle: Integer);
    procedure ReopenEntiTel(Conta: Integer);
    procedure ReopenEntiMail(Conta: Integer);
    procedure ReopenAtrEntiDef(ID_Item: Integer);
    procedure ReopenEntiRespon(Controle: Integer);
    procedure ReopenEntiCtas();
    procedure ReopenEntiSrvPro();
  public
    { Public declarations }
  end;

var
  FmEntidade3: TFmEntidade3;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, MyGlyfs, Principal;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEntidade3.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEntidade3.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEntidadesCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEntidade3.DefParams;
begin
  VAR_GOTOTABELA := 'entidades';
  VAR_GOTOMYSQLTABLE := QrEntidades;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ent.*, ');
  {$IfNDef NO_FINANCEIRO}
  VAR_SQLx.Add('car.Nome NOMECARTPREF, ');
  {$Else}
  VAR_SQLx.Add('"" NOMECARTPREF,');
  {$EndIf}
  VAR_SQLx.Add('cna.Nome CNAE_Nome, ');
  VAR_SQLx.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENTIDADE,');
  VAR_SQLx.Add('CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial ELSE acm.Nome END NOMEACCOUNT,');
  VAR_SQLx.Add('eng.Nome NOMEENTIGRUPO, mot.Descricao NOMEMOTIVO, emp.RazaoSocial NOMEEMPRESA,');
  VAR_SQLx.Add('euf.Nome NOMEEUF, puf.nome NOMEPUF, cuf.Nome NOMECUF, luf.nome NOMELUF, ');
  VAR_SQLx.Add('nuf.Nome NOMENUF, elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,');
  VAR_SQLx.Add('clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD, lec.Nome NOMEECIVIL,');
  VAR_SQLx.Add('mue.Nome NO_DTB_EMUNICI, mup.Nome NO_DTB_PMUNICI,');
  VAR_SQLx.Add('muc.Nome NO_DTB_CMUNICI, mul.Nome NO_DTB_LMUNICI,');
  VAR_SQLx.Add('pae.Nome NO_BACEN_EPAIS, pap.Nome NO_BACEN_PPAIS,');
  VAR_SQLx.Add('ptt.Nome PTe1Tip_TXT, pct.Nome PCelTip_TXT,');
  VAR_SQLx.Add('ett.Nome ETe1Tip_TXT, ect.Nome ECelTip_TXT');
  VAR_SQLx.Add('FROM entidades ent');
  VAR_SQLx.Add('LEFT JOIN entigrupos eng  ON eng.Codigo=ent.Grupo');
  VAR_SQLx.Add('LEFT JOIN entidades acm   ON acm.Codigo=ent.Account');
  VAR_SQLx.Add('LEFT JOIN entidades emp   ON emp.Codigo=ent.Empresa');
  VAR_SQLx.Add('LEFT JOIN motivose mot    ON mot.Codigo=ent.Motivo');
  VAR_SQLx.Add('LEFT JOIN ufs euf         ON euf.Codigo=ent.EUF');
  VAR_SQLx.Add('LEFT JOIN ufs puf         ON puf.Codigo=ent.PUF');
  VAR_SQLx.Add('LEFT JOIN ufs cuf         ON cuf.Codigo=ent.CUF');
  VAR_SQLx.Add('LEFT JOIN ufs luf         ON luf.Codigo=ent.LUF');
  VAR_SQLx.Add('LEFT JOIN ufs nuf         ON nuf.Codigo=ent.UFNatal');
  VAR_SQLx.Add('LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd');
  VAR_SQLx.Add('LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd');
  VAR_SQLx.Add('LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd');
  VAR_SQLx.Add('LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd');
  VAR_SQLx.Add('LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil');
  {$IfNDef NO_FINANCEIRO}
  VAR_SQLx.Add('LEFT JOIN carteiras   car ON car.Codigo=ent.CartPref');
  {$EndIf}
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mue ON mue.Codigo=ent.ECodMunici');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mup ON mup.Codigo=ent.PCodMunici');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici muc ON muc.Codigo=ent.CCodMunici');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mul ON mul.Codigo=ent.LCodMunici');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pae ON pae.Codigo=ent.ECodiPais');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pap ON pap.Codigo=ent.PCodiPais');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.cnae21cad cna ON cna.CodAlf=ent.CNAE');
  VAR_SQLx.Add('LEFT JOIN entitipcto ptt  ON ptt.Codigo=ent.PTe1Tip');
  VAR_SQLx.Add('LEFT JOIN entitipcto pct  ON pct.Codigo=ent.PCelTip');
  VAR_SQLx.Add('LEFT JOIN entitipcto ett  ON ett.Codigo=ent.ETe1Tip');
  VAR_SQLx.Add('LEFT JOIN entitipcto ect  ON ect.Codigo=ent.ECelTip');
  VAR_SQLx.Add('WHERE ent.Codigo>-1000');
  //
  VAR_SQL1.Add('AND ent.Codigo=:P0');
  //
  VAR_SQLa.Add('AND (CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial LIKE:P0');
  VAR_SQLa.Add('ELSE ent.Nome LIKE:P1 END) ');
end;

procedure TFmEntidade3.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEntidade3.MostraEdicao(SQLType: TSQLType);
var
  Tipo: Integer;
begin
  if SQLType = stLok then
  begin
    DBGBEstrangeiro.Visible := QrEntidadesEstrangDef.Value = 1;
    Tipo := QrEntidadesTipo.Value;
    //
    if Tipo = 0 then
    begin
      PCDados.ActivePageIndex  := 0;
      PCPessoa.ActivePageIndex := 0;
    end else
    begin
      PCDados.ActivePageIndex  := 1;
      PCPessoa.ActivePageIndex := 1;
    end;
  end;
end;

procedure TFmEntidade3.AbreWEBSiteEntidade(Site: String);
begin
  if Site <> '' then
    ShellExecute(Application.Handle, nil, PChar(Site), nil, nil, sw_hide);
end;

procedure TFmEntidade3.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEntidade3.DBGEntiResponDblClick(Sender: TObject);
var
  Entidade: Integer;
begin
  if (QrEntiRespon.State <> dsInactive) and (QrEntiRespon.RecordCount <> 0) then
  begin
    Entidade := QrEntiResponEntidade.Value;
    //
    if Entidade <> 0 then
      LocCod(Entidade, Entidade);
  end;
end;

procedure TFmEntidade3.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmEntidade3.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEntidade3.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEntidade3.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEntidade3.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEntidade3.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntidade3.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEntidadesCodigo.Value;
  //
  if TFmEntidade3(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmEntidade3.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PCDados.ActivePageIndex  := 0;
  PCPessoa.ActivePageIndex := 0;
  //
  CriaOForm;
end;

procedure TFmEntidade3.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEntidadesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidade3.SBPSiteClick(Sender: TObject);
begin
  AbreWEBSiteEntidade(QrEntidadesPSite.Value);
end;

procedure TFmEntidade3.SBESiteClick(Sender: TObject);
begin
  AbreWEBSiteEntidade(QrEntidadesESite.Value);
end;

procedure TFmEntidade3.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEntidade3.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEntidadesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEntidade3.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEntidade3.QrEntiContatAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiMail(0);
  ReopenEntiTel(0);
end;

procedure TFmEntidade3.QrEntiContatBeforeClose(DataSet: TDataSet);
begin
  QrEntiMail.Close;
  QrEntiTel.Close;
end;

procedure TFmEntidade3.QrEntiContatCalcFields(DataSet: TDataSet);
begin
  QrEntiContatDTANATAL_TXT.Value := dmkPF.FDT_NULO(QrEntiContatDtaNatal.Value, 2);
end;

procedure TFmEntidade3.QrEntidadesAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEntidade3.QrEntidadesAfterScroll(DataSet: TDataSet);
begin
  MostraEdicao(stLok);
  ReopenEntiContat(0);
  ReopenAtrEntiDef(0);
  ReopenEntiRespon(0);
  ReopenEntiCtas();
  ReopenEntiSrvPro();
end;

procedure TFmEntidade3.FormActivate(Sender: TObject);
begin
  if TFmEntidade3(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmEntidade3.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEntidadesCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'entidades', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEntidade3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntidade3.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmEntidade3.QrEntidadesBeforeClose(DataSet: TDataSet);
begin
  QrEntiContat.Close;
  QrAtrEntiDef.Close;
  QrEntiCtas.Close;
  QrEntiSrvPro.Close;
end;

procedure TFmEntidade3.QrEntidadesBeforeOpen(DataSet: TDataSet);
begin
  QrEntidadesCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEntidade3.QrEntidadesCalcFields(DataSet: TDataSet);
begin
  QrEntidadesETE1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesETe1.Value);
  QrEntidadesETE2_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesETe2.Value);
  QrEntidadesETE3_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesETe3.Value);
  QrEntidadesEFAX_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesEFax.Value);
  QrEntidadesECEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesECel.Value);
  QrEntidadesCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCNPJ.Value);
  QrEntidadesL_CNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesL_CNPJ.Value);
  //
  QrEntidadesPTE1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesPTe1.Value);
  QrEntidadesPTE2_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesPTe2.Value);
  QrEntidadesPTE3_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesPTe3.Value);
  QrEntidadesPFAX_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesPFax.Value);
  QrEntidadesPCEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesPCel.Value);
  QrEntidadesPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF.Value);
  QrEntidadesCPF_PAI_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF_Pai.Value);
  QrEntidadesCPF_Resp1_TXT.Value := Geral.FormataCNPJ_TT(QrEntidadesCPF_Resp1.Value);
  //
  QrEntidadesCTEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesCTel.Value);
  QrEntidadesCFAX_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesCFax.Value);
  QrEntidadesCCEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesCCel.Value);
  //
  QrEntidadesLTEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesLTel.Value);
  QrEntidadesLFAX_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesLFax.Value);
  QrEntidadesLCEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntidadesLCel.Value);
  //
  QrEntidadesECEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesECEP.Value));
  QrEntidadesPCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesPCEP.Value));
  QrEntidadesCCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesCCEP.Value));
  QrEntidadesLCEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrEntidadesLCEP.Value));
  //
  QrEntidadesENUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesERua.Value, QrEntidadesENumero.Value, False);
  QrEntidadesPNUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesPRua.Value, QrEntidadesPNumero.Value, False);
  QrEntidadesCNUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesCRua.Value, QrEntidadesCNumero.Value, False);
  QrEntidadesLNUMERO_TXT.Value := Geral.FormataNumeroDeRua(QrEntidadesLRua.Value, QrEntidadesLNumero.Value, False);
  //
  QrEntidadesIE_TXT.Value := Geral.Formata_IE(QrEntidadesIE.Value, QrEntidadesEUF.Value, '??');
  QrEntidadesIEST_TXT.Value := Geral.Formata_IE(QrEntidadesIEST.Value, QrEntidadesEUF.Value, '??');
  //
  QrEntidadesDATARG_TXT.Value := dmkPF.FDT_NULO(QrEntidadesDataRG.Value, 2);
  QrEntidadesENATAL_TXT.Value := dmkPF.FDT_NULO(QrEntidadesENatal.Value, 2);
  QrEntidadesPNATAL_TXT.Value := dmkPF.FDT_NULO(QrEntidadesPNatal.Value, 2);
end;

procedure TFmEntidade3.QrEntiResponCalcFields(DataSet: TDataSet);
begin
  QrEntiResponMandatoIni_TXT.Value := Geral.FDT(QrEntiResponMandatoIni.Value, 3);
  QrEntiResponMandatoFim_TXT.Value := Geral.FDT(QrEntiResponMandatoFim.Value, 3);
  QrEntiResponTe1_TXT.Value        := Geral.FormataTelefone_TT_Curto(QrEntiResponTe1.Value);
  QrEntiResponCel_TXT.Value        := Geral.FormataTelefone_TT_Curto(QrEntiResponCel.Value);
end;

procedure TFmEntidade3.QrEntiTelCalcFields(DataSet: TDataSet);
begin
  QrEntiTelTEL_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntiTelTelefone.Value);
end;

procedure TFmEntidade3.ReopenEntiContat(Controle: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrEntiContat, Dmod.MyDB, [
    'SELECT eco.Codigo, eco.Controle, eco.Nome, eco.Cargo,',
    'eca.Nome NOME_CARGO, eco.DtaNatal, eco.Sexo,',
    'ELT(eco.Sexo+1, "NI", "M", "F", "?") SEXO_TXT,',
    'IF(eco.DtaNatal <= "1899-12-30", "", ',
    ' DATE_FORMAT(eco.DtaNatal, "%d/%m/%Y")) DTANATAL_TXT, ',
    // NFe 3.10
    'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF, ',
    'eco.Tipo, eco.CNPJ, eco.CPF, eco.Aplicacao, eco.Ativo ',
    //
    'FROM enticontat eco',
    'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle',
    'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo',
    'WHERE ece.Codigo=' + Geral.FF0(QrEntidadesCodigo.Value),
    '']);
  //
  if Controle <> 0 then
    QrEntiContat.Locate('Controle', Controle, []);
end;

procedure TFmEntidade3.ReopenEntiTel(Conta: Integer);
begin
  QrEntiTel.Close;
  QrEntiTel.Params[0].AsInteger := QrEntiContatControle.Value;
  UMyMod.AbreQuery(QrEntiTel, DMod.MyDB);
  //
  if Conta <> 0 then
    QrEntiTel.Locate('Conta', Conta, []);
end;

procedure TFmEntidade3.ReopenEntiMail(Conta: Integer);
begin
  QrEntiMail.Close;
  QrEntiMail.Params[0].AsInteger := QrEntiContatControle.Value;
  UMyMod.AbreQuery(QrEntiMail, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiMail.Locate('Conta', Conta, []);
end;

procedure TFmEntidade3.ReopenAtrEntiDef(ID_Item: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrEntiDef, Dmod.MyDB, [
    'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ATRITS, ',
    'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,',
    'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp ',
    'FROM atrentidef def ',
    'LEFT JOIN atrentiits its ON its.Controle=def.AtrIts ',
    'LEFT JOIN atrenticad cad ON cad.Codigo=def.AtrCad ',
    'WHERE def.ID_Sorc=' + Geral.FF0(QrEntidadesCodigo.Value),
    ' ',
    'UNION  ',
    ' ',
    'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, ',
    'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt,',
    'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp ',
    'FROM atrentitxt def ',
    'LEFT JOIN atrenticad cad ON cad.Codigo=def.AtrCad ',
    'WHERE def.ID_Sorc=' + Geral.FF0(QrEntidadesCodigo.Value),
    ' ',
    'ORDER BY NO_CAD, NO_ITS ',
    '',
    '']);
  QrAtrEntiDef.Locate('ID_Item', ID_Item, []);
end;

procedure TFmEntidade3.ReopenEntiRespon(Controle: Integer);
begin
  QrEntiRespon.Close;
  QrEntiRespon.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrEntiRespon, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrEntiRespon.Locate('Controle', Controle, []);
end;

procedure TFmEntidade3.ReopenEntiCtas;
begin
  QrEntiCtas.Close;
  QrEntiCtas.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrEntiCtas, DMod.MyDB);
end;

procedure TFmEntidade3.ReopenEntiSrvPro;
begin
  QrEntiSrvPro.Close;
  QrEntiSrvPro.Params[0].AsInteger := QrEntidadesCodigo.Value;
  UMyMod.AbreQuery(QrEntiSrvPro, Dmod.MyDB);
end;

end.

