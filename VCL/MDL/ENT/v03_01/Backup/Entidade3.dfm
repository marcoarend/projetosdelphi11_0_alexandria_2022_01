object FmEntidade3: TFmEntidade3
  Left = 368
  Top = 194
  Caption = 'ENT-GEREN-000 :: Cadastros de Pessoas F'#237'sicas e Jur'#237'dicas'
  ClientHeight = 721
  ClientWidth = 1006
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnEdita: TPanel
    Left = 0
    Top = 118
    Width = 1006
    Height = 603
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1006
      Height = 154
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 20
        Top = 20
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 94
        Top = 20
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 20
        Top = 39
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 94
        Top = 39
        Width = 851
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 525
      Width = 1006
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 835
        Top = 18
        Width = 169
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 147
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 118
    Width = 1006
    Height = 603
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1006
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label3: TLabel
        Left = 10
        Top = 28
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 134
        Top = 28
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
      end
      object DBLaindIEDest: TLabel
        Left = 293
        Top = 28
        Width = 125
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Indicador da I.E.: [F4]'
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 59
        Top = 23
        Width = 69
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Codigo'
        DataSource = DsEntidades
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 158
        Top = 23
        Width = 69
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CodUsu'
        DataSource = DsEntidades
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clInactiveCaption
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Alignment = taRightJustify
      end
      object DBCheckBox2: TDBCheckBox
        Left = 233
        Top = 26
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsEntidades
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdindIEDest: TDBEdit
        Left = 425
        Top = 25
        Width = 36
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'indIEDest'
        DataSource = DsEntidades
        TabOrder = 3
      end
      object DBEdindIEDest_TXT: TEdit
        Left = 460
        Top = 25
        Width = 365
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ReadOnly = True
        TabOrder = 4
        Text = 'N'#227'o definido.'
      end
      object dmkRadioGroup1: TDBRadioGroup
        Left = 831
        Top = 5
        Width = 162
        Height = 45
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '  Pessoa: '
        Columns = 2
        DataField = 'Tipo'
        DataSource = DsEntidades
        Items.Strings = (
          'Jur'#237'dica'
          'F'#237'sica')
        ParentBackground = True
        TabOrder = 5
        Values.Strings = (
          '0'
          '1')
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 524
      Width = 1006
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 149
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 363
        Top = 18
        Width = 641
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 310
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 158
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 478
          Top = 0
          Width = 163
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object PCDados: TPageControl
      Left = 0
      Top = 155
      Width = 1006
      Height = 369
      ActivePage = TSComercial
      Align = alClient
      TabOrder = 2
      ExplicitTop = 150
      ExplicitHeight = 320
      object TSComercial: TTabSheet
        Caption = ' Comercial'
        ImageIndex = 11
        ExplicitHeight = 289
        object ScrollBox1: TScrollBox
          Left = 0
          Top = 0
          Width = 998
          Height = 338
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 289
          object Label55: TLabel
            Left = 10
            Top = 39
            Width = 100
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo logradouro:'
          end
          object Label56: TLabel
            Left = 330
            Top = 39
            Width = 128
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Nome do logradouro:'
            FocusControl = dmkDBEdit25
          end
          object Label57: TLabel
            Left = 881
            Top = 39
            Width = 51
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'N'#250'mero:'
            FocusControl = dmkDBEdit26
          end
          object Label62: TLabel
            Left = 994
            Top = 39
            Width = 87
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Complemento:'
            FocusControl = dmkDBEdit32
          end
          object Label60: TLabel
            Left = 854
            Top = 94
            Width = 21
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'UF:'
          end
          object Label118: TLabel
            Left = 891
            Top = 94
            Width = 30
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pa'#237's:'
          end
          object Label117: TLabel
            Left = 398
            Top = 94
            Width = 47
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cidade:'
          end
          object Label58: TLabel
            Left = 10
            Top = 94
            Width = 39
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Bairro:'
            FocusControl = dmkDBEdit27
          end
          object Label133: TLabel
            Left = 10
            Top = 143
            Width = 21
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'I.E.:'
          end
          object Label132: TLabel
            Left = 153
            Top = 143
            Width = 45
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'I.E.S.T.:'
          end
          object Label131: TLabel
            Left = 295
            Top = 143
            Width = 65
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'I.M. / NIRE:'
          end
          object Label63: TLabel
            Left = 441
            Top = 143
            Width = 208
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Ponto de refer'#234'ncia (do endere'#231'o):'
            FocusControl = dmkDBEdit33
          end
          object Label121: TLabel
            Left = 1167
            Top = 196
            Width = 31
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Filial:'
          end
          object Label89: TLabel
            Left = 821
            Top = 196
            Width = 79
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Site da WEB:'
            FocusControl = dmkDBEdit15
          end
          object Label136: TLabel
            Left = 393
            Top = 196
            Width = 216
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'CRT (C'#243'digo da Regime Tribut'#225'rio):'
          end
          object Label126: TLabel
            Left = 10
            Top = 196
            Width = 353
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'CNAE (Classifica'#231#227'o Nacional de Atividades Econ'#244'micas):'
          end
          object Label122: TLabel
            Left = 10
            Top = 246
            Width = 101
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo de telefone:'
          end
          object Label15: TLabel
            Left = 155
            Top = 246
            Width = 57
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Telefone:'
          end
          object Label25: TLabel
            Left = 300
            Top = 246
            Width = 101
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo de telefone:'
          end
          object Label23: TLabel
            Left = 446
            Top = 246
            Width = 45
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Celular:'
          end
          object Label156: TLabel
            Left = 590
            Top = 246
            Width = 25
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fax:'
            FocusControl = DBEdit28
          end
          object Label100: TLabel
            Left = 735
            Top = 246
            Width = 41
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'E-mail:'
            FocusControl = dmkDBEdit30
          end
          object Label142: TLabel
            Left = 1063
            Top = 246
            Width = 64
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Funda'#231#227'o:'
          end
          object SBESite: TSpeedButton
            Left = 1132
            Top = 215
            Width = 26
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SBESiteClick
          end
          object Label54: TLabel
            Left = 10
            Top = 18
            Width = 30
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'CEP:'
          end
          object dmkDBEdit31: TdmkDBEdit
            Left = 10
            Top = 59
            Width = 69
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ELograd'
            DataSource = DsEntidades
            TabOrder = 0
            QryCampo = 'ELograd'
            UpdCampo = 'ELograd'
            UpdType = utYes
            Alignment = taRightJustify
          end
          object dmkDBEdit24: TdmkDBEdit
            Left = 79
            Top = 59
            Width = 246
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMEELOGRAD'
            DataSource = DsEntidades
            TabOrder = 1
            QryCampo = 'ELograd'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit25: TdmkDBEdit
            Left = 330
            Top = 59
            Width = 546
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ERua'
            DataSource = DsEntidades
            TabOrder = 2
            QryCampo = 'ERua'
            UpdCampo = 'ERua'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit26: TdmkDBEdit
            Left = 881
            Top = 59
            Width = 109
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ENUMERO_TXT'
            DataSource = DsEntidades
            TabOrder = 3
            QryCampo = 'ENumero'
            UpdCampo = 'ENumero'
            UpdType = utYes
            Alignment = taCenter
          end
          object dmkDBEdit32: TdmkDBEdit
            Left = 994
            Top = 59
            Width = 212
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ECompl'
            DataSource = DsEntidades
            TabOrder = 4
            QryCampo = 'ECompl'
            UpdCampo = 'ECompl'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBEdit7: TDBEdit
            Left = 960
            Top = 113
            Width = 246
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_BACEN_EPAIS'
            DataSource = DsEntidades
            TabOrder = 5
          end
          object DBEdit8: TDBEdit
            Left = 891
            Top = 113
            Width = 69
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ECodiPais'
            DataSource = DsEntidades
            TabOrder = 6
          end
          object dmkDBEdit29: TdmkDBEdit
            Left = 854
            Top = 113
            Width = 35
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMEEUF'
            DataSource = DsEntidades
            TabOrder = 7
            QryCampo = 'EUF'
            UpdCampo = 'EUF'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBEdit6: TDBEdit
            Left = 466
            Top = 113
            Width = 384
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_DTB_EMUNICI'
            DataSource = DsEntidades
            TabOrder = 8
          end
          object DBEdit9: TDBEdit
            Left = 398
            Top = 113
            Width = 68
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ECodMunici'
            DataSource = DsEntidades
            TabOrder = 9
          end
          object dmkDBEdit27: TdmkDBEdit
            Left = 10
            Top = 113
            Width = 384
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'EBairro'
            DataSource = DsEntidades
            TabOrder = 10
            QryCampo = 'EBairro'
            UpdCampo = 'EBairro'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBEdit21: TDBEdit
            Left = 10
            Top = 162
            Width = 138
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'IE_TXT'
            DataSource = DsEntidades
            TabOrder = 11
          end
          object DBEdit19: TDBEdit
            Left = 153
            Top = 162
            Width = 137
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'IEST'
            DataSource = DsEntidades
            TabOrder = 12
          end
          object DBEdit20: TDBEdit
            Left = 295
            Top = 162
            Width = 138
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NIRE'
            DataSource = DsEntidades
            TabOrder = 13
          end
          object dmkDBEdit33: TdmkDBEdit
            Left = 441
            Top = 162
            Width = 765
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'EEndeRef'
            DataSource = DsEntidades
            TabOrder = 14
            QryCampo = 'EEndeRef'
            UpdCampo = 'EEndeRef'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBEdit14: TDBEdit
            Left = 1167
            Top = 215
            Width = 40
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Filial'
            DataSource = DsEntidades
            TabOrder = 15
          end
          object dmkDBEdit15: TdmkDBEdit
            Left = 821
            Top = 215
            Width = 308
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ESite'
            DataSource = DsEntidades
            TabOrder = 16
            UpdCampo = 'ERua'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object Memo2: TMemo
            Left = 437
            Top = 215
            Width = 375
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ReadOnly = True
            TabOrder = 17
          end
          object DBEdCRT: TDBEdit
            Left = 393
            Top = 215
            Width = 43
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CRT'
            DataSource = DsEntidades
            TabOrder = 18
          end
          object DBEdit18: TDBEdit
            Left = 79
            Top = 215
            Width = 307
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CNAE_Nome'
            DataSource = DsEntidades
            TabOrder = 19
          end
          object DBEdit17: TDBEdit
            Left = 10
            Top = 215
            Width = 69
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CNAE'
            DataSource = DsEntidades
            TabOrder = 20
          end
          object DBEdit15: TDBEdit
            Left = 10
            Top = 265
            Width = 138
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ETe1Tip_TXT'
            DataSource = DsEntidades
            TabOrder = 21
          end
          object DBEdit22: TDBEdit
            Left = 155
            Top = 266
            Width = 138
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ETE1_TXT'
            DataSource = DsEntidades
            TabOrder = 22
          end
          object DBEdit24: TDBEdit
            Left = 300
            Top = 266
            Width = 138
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ECelTip_TXT'
            DataSource = DsEntidades
            TabOrder = 23
          end
          object DBEdit23: TDBEdit
            Left = 446
            Top = 266
            Width = 137
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ECEL_TXT'
            DataSource = DsEntidades
            TabOrder = 24
          end
          object DBEdit28: TDBEdit
            Left = 590
            Top = 266
            Width = 139
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'EFAX_TXT'
            DataSource = DsEntidades
            TabOrder = 25
          end
          object dmkDBEdit30: TdmkDBEdit
            Left = 735
            Top = 266
            Width = 322
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'EEMail'
            DataSource = DsEntidades
            TabOrder = 26
            UpdCampo = 'ERua'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit60: TdmkDBEdit
            Left = 1063
            Top = 266
            Width = 143
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ENATAL_TXT'
            DataSource = DsEntidades
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 27
            QryCampo = 'SSP'
            UpdCampo = 'SSP'
            UpdType = utYes
            Alignment = taCenter
          end
          object dmkDBEdit23: TdmkDBEdit
            Left = 98
            Top = 14
            Width = 89
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ECEP_TXT'
            DataSource = DsEntidades
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 28
            QryCampo = 'ECEP'
            UpdCampo = 'ECEP'
            UpdType = utYes
            Alignment = taLeftJustify
          end
        end
      end
      object TSResidencial: TTabSheet
        Caption = ' Residencial'
        ImageIndex = 3
        ExplicitHeight = 289
        object ScrollBox2: TScrollBox
          Left = 0
          Top = 0
          Width = 998
          Height = 338
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 289
          object Label40: TLabel
            Left = 10
            Top = 10
            Width = 30
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'CEP:'
          end
          object Label158: TLabel
            Left = 434
            Top = 10
            Width = 60
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Profiss'#227'o:'
          end
          object Label159: TLabel
            Left = 629
            Top = 10
            Width = 40
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cargo:'
          end
          object Label152: TLabel
            Left = 788
            Top = 10
            Width = 92
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Nacionalidade:'
            FocusControl = DBEdit26
          end
          object Label153: TLabel
            Left = 1015
            Top = 10
            Width = 72
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Estado civil:'
            FocusControl = DBEdit27
          end
          object Label43: TLabel
            Left = 881
            Top = 39
            Width = 51
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'N'#250'mero:'
            FocusControl = dmkDBEdit11
          end
          object Label48: TLabel
            Left = 994
            Top = 39
            Width = 87
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Complemento:'
            FocusControl = dmkDBEdit17
          end
          object Label42: TLabel
            Left = 330
            Top = 39
            Width = 128
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Nome do logradouro:'
            FocusControl = dmkDBEdit10
          end
          object Label41: TLabel
            Left = 10
            Top = 39
            Width = 100
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo logradouro:'
          end
          object Label44: TLabel
            Left = 10
            Top = 94
            Width = 39
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Bairro:'
            FocusControl = dmkDBEdit12
          end
          object Label119: TLabel
            Left = 398
            Top = 94
            Width = 47
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cidade:'
          end
          object Label46: TLabel
            Left = 853
            Top = 94
            Width = 21
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'UF:'
          end
          object Label120: TLabel
            Left = 891
            Top = 94
            Width = 30
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pa'#237's:'
          end
          object Label102: TLabel
            Left = 853
            Top = 143
            Width = 41
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'E-mail:'
            FocusControl = dmkDBEdit40
          end
          object Label49: TLabel
            Left = 10
            Top = 143
            Width = 208
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Ponto de refer'#234'ncia (do endere'#231'o):'
            FocusControl = dmkDBEdit18
          end
          object Label47: TLabel
            Left = 10
            Top = 197
            Width = 101
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo de telefone:'
          end
          object Label61: TLabel
            Left = 155
            Top = 197
            Width = 57
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Telefone:'
          end
          object Label69: TLabel
            Left = 300
            Top = 197
            Width = 101
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo de telefone:'
          end
          object Label71: TLabel
            Left = 446
            Top = 197
            Width = 45
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Celular:'
          end
          object Label59: TLabel
            Left = 590
            Top = 197
            Width = 75
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Nascimento:'
          end
          object Label91: TLabel
            Left = 734
            Top = 197
            Width = 79
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Site da WEB:'
            FocusControl = dmkDBEdit28
          end
          object SBPSite: TSpeedButton
            Left = 1180
            Top = 217
            Width = 26
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SBPSiteClick
          end
          object Label161: TLabel
            Left = 10
            Top = 246
            Width = 58
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Empresa:'
            FocusControl = dmkDBEdit42
          end
          object dmkDBEdit8: TdmkDBEdit
            Left = 98
            Top = 6
            Width = 89
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PCEP_TXT'
            DataSource = DsEntidades
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            QryCampo = 'PCEP'
            UpdCampo = 'PCEP'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBEdit32: TDBEdit
            Left = 505
            Top = 6
            Width = 119
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Profissao'
            DataSource = DsEntidades
            TabOrder = 1
          end
          object DBEdit33: TDBEdit
            Left = 673
            Top = 6
            Width = 110
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Cargo'
            DataSource = DsEntidades
            TabOrder = 2
          end
          object DBEdit26: TDBEdit
            Left = 880
            Top = 6
            Width = 129
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Nacionalid'
            DataSource = DsEntidades
            TabOrder = 3
          end
          object DBEdit27: TDBEdit
            Left = 1090
            Top = 6
            Width = 115
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMEECIVIL'
            DataSource = DsEntidades
            TabOrder = 4
          end
          object dmkDBEdit17: TdmkDBEdit
            Left = 994
            Top = 59
            Width = 212
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PCompl'
            DataSource = DsEntidades
            TabOrder = 5
            QryCampo = 'PCompl'
            UpdCampo = 'PCompl'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit11: TdmkDBEdit
            Left = 881
            Top = 59
            Width = 109
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PNUMERO_TXT'
            DataSource = DsEntidades
            TabOrder = 6
            QryCampo = 'PNumero'
            UpdCampo = 'PNumero'
            UpdType = utYes
            Alignment = taCenter
          end
          object dmkDBEdit10: TdmkDBEdit
            Left = 330
            Top = 59
            Width = 546
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PRua'
            DataSource = DsEntidades
            TabOrder = 7
            QryCampo = 'PRua'
            UpdCampo = 'PRua'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit9: TdmkDBEdit
            Left = 79
            Top = 59
            Width = 246
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMEPLOGRAD'
            DataSource = DsEntidades
            TabOrder = 8
            QryCampo = 'PLograd'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit16: TdmkDBEdit
            Left = 10
            Top = 59
            Width = 69
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PLograd'
            DataSource = DsEntidades
            TabOrder = 9
            QryCampo = 'PLograd'
            UpdCampo = 'PLograd'
            UpdType = utYes
            Alignment = taRightJustify
          end
          object dmkDBEdit12: TdmkDBEdit
            Left = 10
            Top = 113
            Width = 384
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PBairro'
            DataSource = DsEntidades
            TabOrder = 10
            QryCampo = 'PBairro'
            UpdCampo = 'PBairro'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBEdit10: TDBEdit
            Left = 398
            Top = 113
            Width = 68
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PCodMunici'
            DataSource = DsEntidades
            TabOrder = 11
          end
          object DBEdit11: TDBEdit
            Left = 466
            Top = 113
            Width = 382
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_DTB_PMUNICI'
            DataSource = DsEntidades
            TabOrder = 12
          end
          object dmkDBEdit14: TdmkDBEdit
            Left = 853
            Top = 113
            Width = 34
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMEPUF'
            DataSource = DsEntidades
            TabOrder = 13
            QryCampo = 'PUF'
            UpdCampo = 'PUF'
            UpdType = utYes
            Alignment = taCenter
          end
          object DBEdit12: TDBEdit
            Left = 891
            Top = 113
            Width = 69
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PCodiPais'
            DataSource = DsEntidades
            TabOrder = 14
          end
          object DBEdit13: TDBEdit
            Left = 960
            Top = 113
            Width = 246
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_BACEN_PPAIS'
            DataSource = DsEntidades
            TabOrder = 15
          end
          object dmkDBEdit40: TdmkDBEdit
            Left = 853
            Top = 162
            Width = 353
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PEMail'
            DataSource = DsEntidades
            TabOrder = 16
            UpdCampo = 'PCompl'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit18: TdmkDBEdit
            Left = 10
            Top = 162
            Width = 838
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PEndeRef'
            DataSource = DsEntidades
            TabOrder = 17
            QryCampo = 'PEndeRef'
            UpdCampo = 'PEndeRef'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBEdit16: TDBEdit
            Left = 10
            Top = 217
            Width = 138
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PTe1Tip_TXT'
            DataSource = DsEntidades
            TabOrder = 18
          end
          object DBEdit29: TDBEdit
            Left = 155
            Top = 217
            Width = 138
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PTE1_TXT'
            DataSource = DsEntidades
            TabOrder = 19
          end
          object DBEdit30: TDBEdit
            Left = 300
            Top = 217
            Width = 138
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PCelTip_TXT'
            DataSource = DsEntidades
            TabOrder = 20
          end
          object DBEdit31: TDBEdit
            Left = 446
            Top = 217
            Width = 137
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PCEL_TXT'
            DataSource = DsEntidades
            TabOrder = 21
          end
          object dmkDBEdit13: TdmkDBEdit
            Left = 590
            Top = 217
            Width = 137
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PNATAL_TXT'
            DataSource = DsEntidades
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 22
            QryCampo = 'SSP'
            UpdCampo = 'SSP'
            UpdType = utYes
            Alignment = taCenter
          end
          object dmkDBEdit28: TdmkDBEdit
            Left = 734
            Top = 217
            Width = 443
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PSite'
            DataSource = DsEntidades
            TabOrder = 23
            UpdCampo = 'PCompl'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit42: TdmkDBEdit
            Left = 10
            Top = 265
            Width = 283
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMEEMPRESA'
            DataSource = DsEntidades
            TabOrder = 24
            UpdCampo = 'PCompl'
            UpdType = utYes
            Alignment = taLeftJustify
          end
        end
      end
      object TSContatos: TTabSheet
        Caption = ' Contatos'
        ImageIndex = 2
        ExplicitHeight = 289
        object Splitter1: TSplitter
          Left = 463
          Top = 0
          Width = 6
          Height = 338
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ExplicitTop = 17
          ExplicitHeight = 305
        end
        object Splitter2: TSplitter
          Left = 814
          Top = 0
          Width = 6
          Height = 338
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ExplicitLeft = 887
          ExplicitTop = -32
          ExplicitHeight = 305
        end
        object DBGEntiContat: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 463
          Height = 338
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Columns = <
            item
              Expanded = False
              FieldName = 'DTANATAL_TXT'
              Title.Caption = 'Nascimento'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_CARGO'
              Title.Caption = 'Cargo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJ_CPF'
              Title.Caption = 'CNPJ / CPF'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsEntiContat
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'DTANATAL_TXT'
              Title.Caption = 'Nascimento'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_CARGO'
              Title.Caption = 'Cargo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJ_CPF'
              Title.Caption = 'CNPJ / CPF'
              Visible = True
            end>
        end
        object DBGEntiTel: TdmkDBGrid
          Left = 469
          Top = 0
          Width = 345
          Height = 338
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMEETC'
              Title.Caption = 'Tipo de telefone'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TEL_TXT'
              Title.Caption = 'Telefone'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ramal'
              Width = 37
              Visible = True
            end>
          Color = clWindow
          DataSource = DsEntiTel
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMEETC'
              Title.Caption = 'Tipo de telefone'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TEL_TXT'
              Title.Caption = 'Telefone'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ramal'
              Width = 37
              Visible = True
            end>
        end
        object DBGEntiMail: TdmkDBGrid
          Left = 820
          Top = 0
          Width = 178
          Height = 338
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 39
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEETC'
              Title.Caption = 'Tipo de e-mail'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EMail'
              Title.Caption = 'E-mail'
              Width = 496
              Visible = True
            end>
          Color = clWindow
          DataSource = DsEntiMail
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 39
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEETC'
              Title.Caption = 'Tipo de e-mail'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EMail'
              Title.Caption = 'E-mail'
              Width = 496
              Visible = True
            end>
        end
      end
      object TSTipo: TTabSheet
        Caption = ' Tipo'
        ImageIndex = 2
        ExplicitHeight = 289
        object Label128: TLabel
          Left = 5
          Top = 117
          Width = 124
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Carteira preferencial:'
        end
        object Label138: TLabel
          Left = 5
          Top = 167
          Width = 174
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'd. participante SPED EFD:'
        end
        object Label140: TLabel
          Left = 222
          Top = 167
          Width = 69
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'SUFRAMA:'
        end
        object GroupBox1: TGroupBox
          Left = 5
          Top = 5
          Width = 783
          Height = 105
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Tipo de cadastro: '
          TabOrder = 0
          object CkCliente1: TDBCheckBox
            Left = 10
            Top = 20
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente'
            DataField = 'Cliente1'
            DataSource = DsEntidades
            TabOrder = 0
            ValueChecked = 'V'
            ValueUnchecked = 'F'
          end
          object CkFornece1: TDBCheckBox
            Left = 202
            Top = 20
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fornecedor'
            DataField = 'Fornece1'
            DataSource = DsEntidades
            TabOrder = 4
            ValueChecked = 'V'
            ValueUnchecked = 'F'
          end
          object CkFornece2: TDBCheckBox
            Left = 202
            Top = 39
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Transportador'
            DataField = 'Fornece2'
            DataSource = DsEntidades
            TabOrder = 5
            ValueChecked = 'V'
            ValueUnchecked = 'F'
          end
          object CkFornece3: TDBCheckBox
            Left = 202
            Top = 59
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vendedor'
            DataField = 'Fornece3'
            DataSource = DsEntidades
            TabOrder = 6
            ValueChecked = 'V'
            ValueUnchecked = 'F'
          end
          object CkTerceiro: TDBCheckBox
            Left = 586
            Top = 20
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Terceiro'
            DataField = 'Terceiro'
            DataSource = DsEntidades
            TabOrder = 12
            ValueChecked = 'V'
            ValueUnchecked = 'F'
          end
          object CkFornece4: TDBCheckBox
            Left = 202
            Top = 79
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fornece 4'
            DataField = 'Fornece4'
            DataSource = DsEntidades
            TabOrder = 7
            ValueChecked = 'V'
            ValueUnchecked = 'F'
            Visible = False
          end
          object CkCliente2: TDBCheckBox
            Left = 10
            Top = 39
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente 2'
            DataField = 'Cliente2'
            DataSource = DsEntidades
            TabOrder = 1
            ValueChecked = 'V'
            ValueUnchecked = 'F'
            Visible = False
          end
          object CkFornece5: TDBCheckBox
            Left = 394
            Top = 20
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fornece 5'
            DataField = 'Fornece5'
            DataSource = DsEntidades
            TabOrder = 8
            ValueChecked = 'V'
            ValueUnchecked = 'F'
            Visible = False
          end
          object CkFornece6: TDBCheckBox
            Left = 394
            Top = 39
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fornece 6'
            DataField = 'Fornece6'
            DataSource = DsEntidades
            TabOrder = 9
            ValueChecked = 'V'
            ValueUnchecked = 'F'
            Visible = False
          end
          object CkCliente3: TDBCheckBox
            Left = 10
            Top = 59
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente 3'
            DataField = 'Cliente3'
            DataSource = DsEntidades
            TabOrder = 2
            ValueChecked = 'V'
            ValueUnchecked = 'F'
            Visible = False
          end
          object CkCliente4: TDBCheckBox
            Left = 10
            Top = 79
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente 4'
            DataField = 'Cliente4'
            DataSource = DsEntidades
            TabOrder = 3
            ValueChecked = 'V'
            ValueUnchecked = 'F'
            Visible = False
          end
          object CkFornece8: TDBCheckBox
            Left = 394
            Top = 79
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fornece 8'
            DataField = 'Fornece8'
            DataSource = DsEntidades
            TabOrder = 11
            ValueChecked = 'V'
            ValueUnchecked = 'F'
            Visible = False
          end
          object CkFornece7: TDBCheckBox
            Left = 394
            Top = 59
            Width = 187
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fornece 7'
            DataField = 'Fornece7'
            DataSource = DsEntidades
            TabOrder = 10
            ValueChecked = 'V'
            ValueUnchecked = 'F'
            Visible = False
          end
        end
        object dmkDBEdit57: TdmkDBEdit
          Left = 5
          Top = 137
          Width = 783
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMECARTPREF'
          DataSource = DsEntidades
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit58: TdmkDBEdit
          Left = 5
          Top = 187
          Width = 212
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'COD_PART'
          DataSource = DsEntidades
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit59: TdmkDBEdit
          Left = 222
          Top = 187
          Width = 211
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'SUFRAMA'
          DataSource = DsEntidades
          TabOrder = 3
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
      object TSEndCobran: TTabSheet
        Caption = ' Endere'#231'o de cobran'#231'a '
        ImageIndex = 3
        ExplicitHeight = 289
        object ScrollBox3: TScrollBox
          Left = 0
          Top = 0
          Width = 998
          Height = 338
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 289
          object Label64: TLabel
            Left = 10
            Top = 10
            Width = 30
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'CEP:'
          end
          object Label65: TLabel
            Left = 10
            Top = 39
            Width = 100
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo logradouro:'
          end
          object Label68: TLabel
            Left = 10
            Top = 94
            Width = 39
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Bairro:'
            FocusControl = dmkDBEdit39
          end
          object Label73: TLabel
            Left = 10
            Top = 143
            Width = 69
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Refer'#234'ncia:'
            FocusControl = dmkDBEdit45
          end
          object Label116: TLabel
            Left = 398
            Top = 94
            Width = 47
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cidade:'
          end
          object Label70: TLabel
            Left = 1172
            Top = 94
            Width = 21
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'UF:'
          end
          object Label72: TLabel
            Left = 994
            Top = 39
            Width = 87
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Complemento:'
            FocusControl = dmkDBEdit44
          end
          object Label67: TLabel
            Left = 881
            Top = 39
            Width = 51
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'N'#250'mero:'
            FocusControl = dmkDBEdit38
          end
          object Label66: TLabel
            Left = 330
            Top = 39
            Width = 128
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Nome do logradouro:'
            FocusControl = dmkDBEdit37
          end
          object dmkDBEdit35: TdmkDBEdit
            Left = 98
            Top = 6
            Width = 89
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CCEP_TXT'
            DataSource = DsEntidades
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            QryCampo = 'PCEP'
            UpdCampo = 'PCEP'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit43: TdmkDBEdit
            Left = 10
            Top = 59
            Width = 69
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CLograd'
            DataSource = DsEntidades
            TabOrder = 1
            QryCampo = 'PLograd'
            UpdCampo = 'PLograd'
            UpdType = utYes
            Alignment = taRightJustify
          end
          object dmkDBEdit36: TdmkDBEdit
            Left = 79
            Top = 59
            Width = 246
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMECLOGRAD'
            DataSource = DsEntidades
            TabOrder = 2
            QryCampo = 'PLograd'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit39: TdmkDBEdit
            Left = 10
            Top = 113
            Width = 384
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CBairro'
            DataSource = DsEntidades
            TabOrder = 3
            QryCampo = 'PBairro'
            UpdCampo = 'PBairro'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit45: TdmkDBEdit
            Left = 10
            Top = 162
            Width = 1196
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CEndeRef'
            DataSource = DsEntidades
            TabOrder = 4
            QryCampo = 'PEndeRef'
            UpdCampo = 'PEndeRef'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBEdit4: TDBEdit
            Left = 398
            Top = 113
            Width = 70
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CCodMunici'
            DataSource = DsEntidades
            TabOrder = 5
          end
          object DBEdit5: TDBEdit
            Left = 469
            Top = 113
            Width = 695
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_DTB_CMUNICI'
            DataSource = DsEntidades
            TabOrder = 6
          end
          object dmkDBEdit41: TdmkDBEdit
            Left = 1172
            Top = 113
            Width = 34
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMECUF'
            DataSource = DsEntidades
            TabOrder = 7
            QryCampo = 'PUF'
            UpdCampo = 'PUF'
            UpdType = utYes
            Alignment = taCenter
          end
          object dmkDBEdit44: TdmkDBEdit
            Left = 994
            Top = 59
            Width = 212
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CCompl'
            DataSource = DsEntidades
            TabOrder = 8
            QryCampo = 'PCompl'
            UpdCampo = 'PCompl'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit38: TdmkDBEdit
            Left = 881
            Top = 59
            Width = 109
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CNUMERO_TXT'
            DataSource = DsEntidades
            TabOrder = 9
            QryCampo = 'PNumero'
            UpdCampo = 'PNumero'
            UpdType = utYes
            Alignment = taCenter
          end
          object dmkDBEdit37: TdmkDBEdit
            Left = 330
            Top = 59
            Width = 546
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CRua'
            DataSource = DsEntidades
            TabOrder = 10
            QryCampo = 'PRua'
            UpdCampo = 'PRua'
            UpdType = utYes
            Alignment = taLeftJustify
          end
        end
      end
      object TSEndEntrega: TTabSheet
        Caption = ' Endere'#231'o de entrega'
        ImageIndex = 4
        ExplicitHeight = 289
        object ScrollBox4: TScrollBox
          Left = 0
          Top = 0
          Width = 998
          Height = 338
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 289
          object Label114: TLabel
            Left = 10
            Top = 10
            Width = 38
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'CNPJ:'
            FocusControl = DBEdit1
          end
          object Label74: TLabel
            Left = 202
            Top = 10
            Width = 30
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'CEP:'
          end
          object Label75: TLabel
            Left = 10
            Top = 39
            Width = 100
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo logradouro:'
          end
          object Label76: TLabel
            Left = 330
            Top = 39
            Width = 128
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Nome do logradouro:'
            FocusControl = dmkDBEdit48
          end
          object Label77: TLabel
            Left = 881
            Top = 39
            Width = 51
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'N'#250'mero:'
            FocusControl = dmkDBEdit49
          end
          object Label82: TLabel
            Left = 994
            Top = 39
            Width = 87
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Complemento:'
            FocusControl = dmkDBEdit55
          end
          object Label80: TLabel
            Left = 1172
            Top = 94
            Width = 21
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'UF:'
          end
          object Label115: TLabel
            Left = 398
            Top = 94
            Width = 47
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cidade:'
          end
          object Label78: TLabel
            Left = 10
            Top = 94
            Width = 39
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Bairro:'
            FocusControl = dmkDBEdit50
          end
          object Label83: TLabel
            Left = 10
            Top = 143
            Width = 69
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Refer'#234'ncia:'
            FocusControl = dmkDBEdit56
          end
          object DBEdit1: TDBEdit
            Left = 49
            Top = 5
            Width = 139
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'L_CNPJ_TXT'
            DataSource = DsEntidades
            TabOrder = 0
          end
          object dmkDBEdit46: TdmkDBEdit
            Left = 236
            Top = 5
            Width = 89
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'LCEP_TXT'
            DataSource = DsEntidades
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            QryCampo = 'PCEP'
            UpdCampo = 'PCEP'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit54: TdmkDBEdit
            Left = 10
            Top = 59
            Width = 69
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'LLograd'
            DataSource = DsEntidades
            TabOrder = 2
            QryCampo = 'PLograd'
            UpdCampo = 'PLograd'
            UpdType = utYes
            Alignment = taRightJustify
          end
          object dmkDBEdit47: TdmkDBEdit
            Left = 79
            Top = 59
            Width = 246
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMELLOGRAD'
            DataSource = DsEntidades
            TabOrder = 3
            QryCampo = 'PLograd'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit48: TdmkDBEdit
            Left = 330
            Top = 59
            Width = 546
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'LRua'
            DataSource = DsEntidades
            TabOrder = 4
            QryCampo = 'PRua'
            UpdCampo = 'PRua'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit49: TdmkDBEdit
            Left = 881
            Top = 59
            Width = 109
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'LNUMERO_TXT'
            DataSource = DsEntidades
            TabOrder = 5
            QryCampo = 'PNumero'
            UpdCampo = 'PNumero'
            UpdType = utYes
            Alignment = taCenter
          end
          object dmkDBEdit55: TdmkDBEdit
            Left = 994
            Top = 59
            Width = 212
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'LCompl'
            DataSource = DsEntidades
            TabOrder = 6
            QryCampo = 'PCompl'
            UpdCampo = 'PCompl'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit52: TdmkDBEdit
            Left = 1172
            Top = 113
            Width = 34
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMELUF'
            DataSource = DsEntidades
            TabOrder = 7
            QryCampo = 'PUF'
            UpdCampo = 'PUF'
            UpdType = utYes
            Alignment = taCenter
          end
          object DBEdit3: TDBEdit
            Left = 469
            Top = 113
            Width = 695
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_DTB_LMUNICI'
            DataSource = DsEntidades
            TabOrder = 8
          end
          object DBEdit2: TDBEdit
            Left = 398
            Top = 113
            Width = 70
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'LCodMunici'
            DataSource = DsEntidades
            TabOrder = 9
          end
          object dmkDBEdit50: TdmkDBEdit
            Left = 10
            Top = 113
            Width = 384
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'LBairro'
            DataSource = DsEntidades
            TabOrder = 10
            QryCampo = 'PBairro'
            UpdCampo = 'PBairro'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit56: TdmkDBEdit
            Left = 10
            Top = 162
            Width = 1003
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'LEndeRef'
            DataSource = DsEntidades
            TabOrder = 11
            QryCampo = 'PEndeRef'
            UpdCampo = 'PEndeRef'
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBRadioGroup1: TDBRadioGroup
            Left = 1023
            Top = 143
            Width = 183
            Height = 45
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Entregar neste endere'#231'o?: '
            Columns = 2
            DataField = 'L_Ativo'
            DataSource = DsEntidades
            Items.Strings = (
              'N'#227'o'
              'Sim')
            ParentBackground = True
            TabOrder = 12
            Values.Strings = (
              '0'
              '1')
          end
        end
      end
      object TSObservacoes: TTabSheet
        Caption = ' Observa'#231#245'es'
        ImageIndex = 5
        ExplicitHeight = 289
        object DBMemo1: TDBMemo
          Left = 0
          Top = 0
          Width = 998
          Height = 338
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataField = 'Observacoes'
          DataSource = DsEntidades
          TabOrder = 0
          ExplicitHeight = 289
        end
      end
      object TSEstrangeiro: TTabSheet
        Caption = ' Estrangeiro'
        ImageIndex = 6
        ExplicitHeight = 289
        object DBGBEstrangeiro: TGroupBox
          Left = 15
          Top = 34
          Width = 1202
          Height = 164
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '  Dados espec'#237'ficos de estrangeiro: '
          TabOrder = 0
          Visible = False
          object Panel26: TPanel
            Left = 2
            Top = 18
            Width = 1198
            Height = 144
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label147: TLabel
              Left = 10
              Top = 84
              Width = 140
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'N'#250'mero do documento:'
            end
            object DBRadioGroup2: TDBRadioGroup
              Left = 10
              Top = 0
              Width = 1178
              Height = 80
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Tipo de documento: '
              DataField = 'EstrangTip'
              DataSource = DsEntidades
              Items.Strings = (
                'N'#227'o definido'
                'Passaporte')
              ParentBackground = True
              TabOrder = 0
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9')
            end
            object DBEdit25: TDBEdit
              Left = 10
              Top = 103
              Width = 325
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'EstrangNum'
              DataSource = DsEntidades
              TabOrder = 1
            end
          end
        end
        object DBCheckBox1: TDBCheckBox
          Left = 10
          Top = 10
          Width = 119
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Estrangeiro'
          DataField = 'EstrangDef'
          DataSource = DsEntidades
          TabOrder = 1
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
      object TSAtributos: TTabSheet
        Caption = ' Atributos'
        ImageIndex = 7
        ExplicitHeight = 289
        object GradeDefAtr: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 998
          Height = 338
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'CODUSU_CAD'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_CAD'
              Title.Caption = 'Atributo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CODUSU_ITS'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_ITS'
              Title.Caption = 'Descri'#231#227'o do item do atributo'
              Width = 236
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CTRL_ATR'
              Title.Caption = 'ID'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsAtrEntiDef
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CODUSU_CAD'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_CAD'
              Title.Caption = 'Atributo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CODUSU_ITS'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_ITS'
              Title.Caption = 'Descri'#231#227'o do item do atributo'
              Width = 236
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CTRL_ATR'
              Title.Caption = 'ID'
              Visible = True
            end>
        end
      end
      object TSResponsaveis: TTabSheet
        Caption = ' Respons'#225'veis'
        ImageIndex = 8
        ExplicitHeight = 289
        object Label125: TLabel
          Left = 0
          Top = 0
          Width = 998
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taCenter
          Caption = 
            'D'#234' um duplo clique na linha correspondente para localizar o cada' +
            'stro da entidade vinculada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitWidth = 640
        end
        object DBGEntiRespon: TDBGrid
          Left = 0
          Top = 16
          Width = 998
          Height = 322
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsEntiRespon
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGEntiResponDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'OrdemLista'
              Title.Caption = 'Ordem lista'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Entidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_CARGO'
              Title.Caption = 'Cargo'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ASSINA'
              Title.Caption = 'Assina'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MandatoIni_TXT'
              Title.Caption = 'Ini. mandato'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MandatoFim_TXT'
              Title.Caption = 'Fim mandato'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Entidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Te1_TXT'
              Title.Caption = 'Telefone'
              Width = 110
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cel_TXT'
              Title.Caption = 'Celular'
              Width = 110
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Email'
              Title.Caption = 'E-mail'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Width = 350
              Visible = True
            end>
        end
      end
      object TSDadosBancarios: TTabSheet
        Caption = ' Dados Banc'#225'rios'
        ImageIndex = 9
        ExplicitHeight = 289
        object DBGEntiCtas: TDBGrid
          Left = 0
          Top = 0
          Width = 998
          Height = 338
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsEntiCtas
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco'
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBANCO'
              Title.Caption = 'Banco'
              Width = 278
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag'#234'ncia'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DAC_A'
              Title.Caption = '[A]'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ContaCor'
              Title.Caption = 'Conta'
              Width = 73
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DAC_C'
              Title.Caption = '[C]'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DAC_AC'
              Title.Caption = '[AC]'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ContaTip'
              Title.Caption = 'Tipo'
              Visible = True
            end>
        end
      end
      object TSConsultas: TTabSheet
        Caption = ' Consultas'
        ImageIndex = 10
        ExplicitHeight = 289
        object DBGEntiSrvPro: TDBGrid
          Left = 0
          Top = 0
          Width = 998
          Height = 338
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsEntiSrvPro
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_ORGAO'
              Title.Caption = #211'rg'#227'o'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Numero'
              Title.Caption = 'N'#250'mero da consulta'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Hora'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Resultado'
              Width = 647
              Visible = True
            end>
        end
      end
    end
    object PCPessoa: TPageControl
      Left = 0
      Top = 55
      Width = 1006
      Height = 100
      ActivePage = TSJuridica
      Align = alTop
      TabOrder = 3
      object TSJuridica: TTabSheet
        Caption = ' Jur'#237'dica'
        ExplicitHeight = 64
        object Label52: TLabel
          Left = 812
          Top = 10
          Width = 38
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'CNPJ:'
        end
        object Label53: TLabel
          Left = 829
          Top = 39
          Width = 21
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'I.E.:'
        end
        object Label50: TLabel
          Left = 10
          Top = 10
          Width = 84
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Raz'#227'o Social:'
        end
        object Label51: TLabel
          Left = 10
          Top = 39
          Width = 95
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Nome Fantasia:'
        end
        object dmkDBEdit22: TdmkDBEdit
          Left = 853
          Top = 34
          Width = 140
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'IE_TXT'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          QryCampo = 'IE'
          UpdCampo = 'IE'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit21: TdmkDBEdit
          Left = 853
          Top = 5
          Width = 140
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CNPJ_TXT'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          QryCampo = 'CNPJ'
          UpdCampo = 'CNPJ'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit19: TdmkDBEdit
          Left = 108
          Top = 5
          Width = 700
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'RazaoSocial'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          QryCampo = 'RazaoSocial'
          UpdCampo = 'RazaoSocial'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit20: TdmkDBEdit
          Left = 108
          Top = 34
          Width = 700
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Fantasia'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          QryCampo = 'Fantasia'
          UpdCampo = 'Fantasia'
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
      object TSFisica: TTabSheet
        Caption = ' F'#237'sica'
        ImageIndex = 1
        ExplicitHeight = 64
        object Label33: TLabel
          Left = 821
          Top = 10
          Width = 29
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'CPF:'
        end
        object Label30: TLabel
          Left = 10
          Top = 10
          Width = 40
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Nome:'
        end
        object Label28: TLabel
          Left = 10
          Top = 39
          Width = 50
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Apelido:'
        end
        object Label36: TLabel
          Left = 370
          Top = 39
          Width = 23
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'RG:'
        end
        object Label37: TLabel
          Left = 545
          Top = 39
          Width = 92
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = #211'rg'#227'o emissor:'
        end
        object Label39: TLabel
          Left = 745
          Top = 39
          Width = 106
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Data da emiss'#227'o:'
        end
        object dmkDBEdit5: TdmkDBEdit
          Left = 853
          Top = 5
          Width = 140
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'PCPF_TXT'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          QryCampo = 'CPF'
          UpdCampo = 'CPF'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit4: TdmkDBEdit
          Left = 108
          Top = 5
          Width = 700
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Nome'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit3: TdmkDBEdit
          Left = 108
          Top = 33
          Width = 258
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Apelido'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          QryCampo = 'Apelido'
          UpdCampo = 'Apelido'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit6: TdmkDBEdit
          Left = 399
          Top = 33
          Width = 138
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'RG'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          QryCampo = 'RG'
          UpdCampo = 'RG'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit7: TdmkDBEdit
          Left = 640
          Top = 33
          Width = 98
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'SSP'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          QryCampo = 'SSP'
          UpdCampo = 'SSP'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit34: TdmkDBEdit
          Left = 853
          Top = 33
          Width = 140
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'DATARG_TXT'
          DataSource = DsEntidades
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          QryCampo = 'SSP'
          UpdCampo = 'SSP'
          UpdType = utYes
          Alignment = taCenter
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1006
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 947
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 681
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 603
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastros de Pessoas F'#237'sicas e Jur'#237'dicas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 603
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastros de Pessoas F'#237'sicas e Jur'#237'dicas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 603
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastros de Pessoas F'#237'sicas e Jur'#237'dicas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 1006
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1002
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrEntidadesBeforeOpen
    AfterOpen = QrEntidadesAfterOpen
    BeforeClose = QrEntidadesBeforeClose
    AfterScroll = QrEntidadesAfterScroll
    OnCalcFields = QrEntidadesCalcFields
    SQL.Strings = (
      'SELECT ent.*, car.Nome NOMECARTPREF, cna.Nome CNAE_Nome,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'ENTIDADE,'
      
        'CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial ELSE acm.Nome END NOME' +
        'ACCOUNT,'
      
        'eng.Nome NOMEENTIGRUPO, mot.Descricao NOMEMOTIVO, emp.RazaoSocia' +
        'l NOMEEMPRESA,'
      
        'euf.Nome NOMEEUF, puf.nome NOMEPUF, cuf.Nome NOMECUF, luf.nome N' +
        'OMELUF, '
      'nuf.Nome NOMENUF, elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,'
      'clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD, lec.Nome NOMEECIVIL,'
      'mue.Nome NO_DTB_EMUNICI, mup.Nome NO_DTB_PMUNICI,'
      'muc.Nome NO_DTB_CMUNICI, mul.Nome NO_DTB_LMUNICI,'
      'pae.Nome NO_BACEN_EPAIS, pap.Nome NO_BACEN_PPAIS,'
      'ptt.Nome PTe1Tip_TXT, pct.Nome PCelTip_TXT, '
      'ett.Nome ETe1Tip_TXT, ect.Nome ECelTip_TXT'
      'FROM entidades ent'
      'LEFT JOIN entigrupos eng  ON eng.Codigo=ent.Grupo'
      'LEFT JOIN entidades acm   ON acm.Codigo=ent.Account'
      'LEFT JOIN entidades emp   ON emp.Codigo=ent.Empresa'
      'LEFT JOIN motivose mot    ON mot.Codigo=ent.Motivo'
      'LEFT JOIN ufs euf         ON euf.Codigo=ent.EUF'
      'LEFT JOIN ufs puf         ON puf.Codigo=ent.PUF'
      'LEFT JOIN ufs cuf         ON cuf.Codigo=ent.CUF'
      'LEFT JOIN ufs luf         ON luf.Codigo=ent.LUF'
      'LEFT JOIN ufs nuf         ON nuf.Codigo=ent.UFNatal'
      'LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd'
      'LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd'
      'LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd'
      'LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd'
      'LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil'
      'LEFT JOIN carteiras   car ON car.Codigo=ent.CartPref'
      'LEFT JOIN dtb_munici mue ON mue.Codigo=ent.ECodMunici'
      'LEFT JOIN dtb_munici mup ON mup.Codigo=ent.PCodMunici'
      'LEFT JOIN dtb_munici muc ON muc.Codigo=ent.CCodMunici'
      'LEFT JOIN dtb_munici mul ON mul.Codigo=ent.LCodMunici'
      'LEFT JOIN bacen_pais pae ON pae.Codigo=ent.ECodiPais'
      'LEFT JOIN bacen_pais pap ON pap.Codigo=ent.PCodiPais'
      'LEFT JOIN cnae21Cad cna ON cna.CodAlf=ent.CNAE'
      'LEFT JOIN entitipcto ptt  ON ptt.Codigo=ent.PTe1Tip'
      'LEFT JOIN entitipcto pct  ON pct.Codigo=ent.PCelTip'
      'LEFT JOIN entitipcto ett  ON ett.Codigo=ent.ETe1Tip'
      'LEFT JOIN entitipcto ect  ON ect.Codigo=ent.ECelTip'
      'WHERE ent.Codigo>-2')
    Left = 64
    Top = 64
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEntidadesCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'entidades.CodUsu'
      Required = True
    end
    object QrEntidadesRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Origin = 'entidades.RazaoSocial'
      Required = True
      Size = 100
    end
    object QrEntidadesFantasia: TWideStringField
      FieldName = 'Fantasia'
      Origin = 'entidades.Fantasia'
      Required = True
      Size = 60
    end
    object QrEntidadesRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrEntidadesRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Required = True
      Size = 60
    end
    object QrEntidadesPai: TWideStringField
      FieldName = 'Pai'
      Origin = 'entidades.Pai'
      Required = True
      Size = 60
    end
    object QrEntidadesMae: TWideStringField
      FieldName = 'Mae'
      Origin = 'entidades.Mae'
      Required = True
      Size = 60
    end
    object QrEntidadesCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'entidades.CNPJ'
      Size = 18
    end
    object QrEntidadesIE: TWideStringField
      FieldName = 'IE'
      Origin = 'entidades.IE'
      Size = 15
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'entidades.Nome'
      Required = True
      Size = 100
    end
    object QrEntidadesApelido: TWideStringField
      FieldName = 'Apelido'
      Origin = 'entidades.Apelido'
      Required = True
      Size = 60
    end
    object QrEntidadesCPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'entidades.CPF'
      Size = 18
    end
    object QrEntidadesRG: TWideStringField
      FieldName = 'RG'
      Origin = 'entidades.RG'
      Size = 15
    end
    object QrEntidadesERua: TWideStringField
      DisplayWidth = 60
      FieldName = 'ERua'
      Origin = 'entidades.ERua'
      Size = 60
    end
    object QrEntidadesECompl: TWideStringField
      DisplayWidth = 60
      FieldName = 'ECompl'
      Origin = 'entidades.ECompl'
      Size = 60
    end
    object QrEntidadesEBairro: TWideStringField
      DisplayWidth = 60
      FieldName = 'EBairro'
      Origin = 'entidades.EBairro'
      Size = 60
    end
    object QrEntidadesECidade: TWideStringField
      DisplayWidth = 60
      FieldName = 'ECidade'
      Origin = 'entidades.ECidade'
      Size = 60
    end
    object QrEntidadesEUF: TSmallintField
      FieldName = 'EUF'
      Origin = 'entidades.EUF'
      Required = True
    end
    object QrEntidadesEPais: TWideStringField
      DisplayWidth = 60
      FieldName = 'EPais'
      Origin = 'entidades.EPais'
      Size = 60
    end
    object QrEntidadesETe1: TWideStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrEntidadesETe2: TWideStringField
      FieldName = 'ETe2'
      Origin = 'entidades.Ete2'
    end
    object QrEntidadesETe3: TWideStringField
      FieldName = 'ETe3'
      Origin = 'entidades.Ete3'
    end
    object QrEntidadesECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrEntidadesEFax: TWideStringField
      FieldName = 'EFax'
      Origin = 'entidades.EFax'
    end
    object QrEntidadesEEMail: TWideStringField
      FieldName = 'EEMail'
      Origin = 'entidades.EEmail'
      Size = 100
    end
    object QrEntidadesEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrEntidadesPRua: TWideStringField
      DisplayWidth = 60
      FieldName = 'PRua'
      Origin = 'entidades.PRua'
      Size = 60
    end
    object QrEntidadesPCompl: TWideStringField
      DisplayWidth = 60
      FieldName = 'PCompl'
      Origin = 'entidades.PCompl'
      Size = 60
    end
    object QrEntidadesPBairro: TWideStringField
      DisplayWidth = 60
      FieldName = 'PBairro'
      Origin = 'entidades.PBairro'
      Size = 60
    end
    object QrEntidadesPCidade: TWideStringField
      DisplayWidth = 60
      FieldName = 'PCidade'
      Origin = 'entidades.PCidade'
      Size = 60
    end
    object QrEntidadesPUF: TSmallintField
      FieldName = 'PUF'
      Origin = 'entidades.PUF'
      Required = True
    end
    object QrEntidadesPPais: TWideStringField
      DisplayWidth = 60
      FieldName = 'PPais'
      Origin = 'entidades.PPais'
      Size = 60
    end
    object QrEntidadesPTe1: TWideStringField
      FieldName = 'PTe1'
      Origin = 'entidades.PTe1'
    end
    object QrEntidadesPTe2: TWideStringField
      FieldName = 'PTe2'
      Origin = 'entidades.Pte2'
    end
    object QrEntidadesPTe3: TWideStringField
      FieldName = 'PTe3'
      Origin = 'entidades.Pte3'
    end
    object QrEntidadesPCel: TWideStringField
      FieldName = 'PCel'
      Origin = 'entidades.PCel'
    end
    object QrEntidadesPFax: TWideStringField
      FieldName = 'PFax'
      Origin = 'entidades.PFax'
    end
    object QrEntidadesPEMail: TWideStringField
      FieldName = 'PEMail'
      Origin = 'entidades.PEmail'
      Size = 100
    end
    object QrEntidadesPContato: TWideStringField
      FieldName = 'PContato'
      Origin = 'entidades.PContato'
      Size = 60
    end
    object QrEntidadesSexo: TWideStringField
      FieldName = 'Sexo'
      Origin = 'entidades.Sexo'
      Required = True
      Size = 1
    end
    object QrEntidadesResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Origin = 'entidades.Responsavel'
      Size = 60
    end
    object QrEntidadesProfissao: TWideStringField
      FieldName = 'Profissao'
      Origin = 'entidades.Profissao'
      Size = 60
    end
    object QrEntidadesCargo: TWideStringField
      FieldName = 'Cargo'
      Origin = 'entidades.Cargo'
      Size = 60
    end
    object QrEntidadesRecibo: TSmallintField
      FieldName = 'Recibo'
      Origin = 'entidades.Recibo'
      Required = True
    end
    object QrEntidadesDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Origin = 'entidades.DiaRecibo'
      Required = True
    end
    object QrEntidadesAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Origin = 'entidades.AjudaEmpV'
      Required = True
    end
    object QrEntidadesAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Origin = 'entidades.AjudaEmpP'
      Required = True
    end
    object QrEntidadesCliente1: TWideStringField
      FieldName = 'Cliente1'
      Origin = 'entidades.Cliente1'
      Size = 1
    end
    object QrEntidadesCliente2: TWideStringField
      FieldName = 'Cliente2'
      Origin = 'entidades.Cliente2'
      Size = 1
    end
    object QrEntidadesCliente3: TWideStringField
      FieldName = 'Cliente3'
      Origin = 'entidades.Cliente3'
      Size = 1
    end
    object QrEntidadesCliente4: TWideStringField
      FieldName = 'Cliente4'
      Origin = 'entidades.Cliente4'
      Size = 1
    end
    object QrEntidadesFornece1: TWideStringField
      FieldName = 'Fornece1'
      Origin = 'entidades.Fornece1'
      Size = 1
    end
    object QrEntidadesFornece2: TWideStringField
      FieldName = 'Fornece2'
      Origin = 'entidades.Fornece2'
      Size = 1
    end
    object QrEntidadesFornece3: TWideStringField
      FieldName = 'Fornece3'
      Origin = 'entidades.Fornece3'
      Size = 1
    end
    object QrEntidadesFornece4: TWideStringField
      FieldName = 'Fornece4'
      Origin = 'entidades.Fornece4'
      Size = 1
    end
    object QrEntidadesFornece5: TWideStringField
      FieldName = 'Fornece5'
      Origin = 'entidades.Fornece5'
      Size = 1
    end
    object QrEntidadesFornece6: TWideStringField
      FieldName = 'Fornece6'
      Origin = 'entidades.Fornece6'
      Size = 1
    end
    object QrEntidadesFornece7: TWideStringField
      FieldName = 'Fornece7'
      Origin = 'entidades.Fornece7'
      Size = 1
    end
    object QrEntidadesFornece8: TWideStringField
      FieldName = 'Fornece8'
      Origin = 'entidades.Fornece8'
      Size = 1
    end
    object QrEntidadesTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Origin = 'entidades.Terceiro'
      Size = 1
    end
    object QrEntidadesCadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrEntidadesInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Origin = 'entidades.Informacoes'
      Size = 255
    end
    object QrEntidadesLogo: TBlobField
      FieldName = 'Logo'
      Origin = 'entidades.Logo'
      Size = 4
    end
    object QrEntidadesVeiculo: TIntegerField
      FieldName = 'Veiculo'
      Origin = 'entidades.Veiculo'
    end
    object QrEntidadesMensal: TWideStringField
      FieldName = 'Mensal'
      Origin = 'entidades.Mensal'
      Size = 1
    end
    object QrEntidadesObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      Origin = 'entidades.Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrEntidadesTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrEntidadesLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'entidades.Lk'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrEntidadesNOMEEUF: TWideStringField
      FieldName = 'NOMEEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesPCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE2_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE3_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE2_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE3_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesECEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesEFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesECEP: TIntegerField
      FieldName = 'ECEP'
      Origin = 'entidades.ECEP'
    end
    object QrEntidadesPCEP: TIntegerField
      FieldName = 'PCEP'
      Origin = 'entidades.PCEP'
    end
    object QrEntidadesGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'entidades.Grupo'
    end
    object QrEntidadesDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'entidades.DataAlt'
    end
    object QrEntidadesUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'entidades.UserCad'
    end
    object QrEntidadesUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'entidades.UserAlt'
    end
    object QrEntidadesCRua: TWideStringField
      FieldName = 'CRua'
      Origin = 'entidades.CRua'
      Size = 60
    end
    object QrEntidadesCCompl: TWideStringField
      FieldName = 'CCompl'
      Origin = 'entidades.CCompl'
      Size = 60
    end
    object QrEntidadesCBairro: TWideStringField
      FieldName = 'CBairro'
      Origin = 'entidades.CBairro'
      Size = 60
    end
    object QrEntidadesCCidade: TWideStringField
      FieldName = 'CCidade'
      Origin = 'entidades.CCidade'
      Size = 60
    end
    object QrEntidadesCUF: TSmallintField
      FieldName = 'CUF'
      Origin = 'entidades.CUF'
      Required = True
    end
    object QrEntidadesCCEP: TIntegerField
      FieldName = 'CCEP'
      Origin = 'entidades.CCEP'
    end
    object QrEntidadesCPais: TWideStringField
      FieldName = 'CPais'
      Origin = 'entidades.CPais'
    end
    object QrEntidadesCTel: TWideStringField
      FieldName = 'CTel'
      Origin = 'entidades.CTel'
    end
    object QrEntidadesCFax: TWideStringField
      FieldName = 'CFax'
      Origin = 'entidades.CFax'
    end
    object QrEntidadesCCel: TWideStringField
      FieldName = 'CCel'
      Origin = 'entidades.CCel'
    end
    object QrEntidadesCContato: TWideStringField
      FieldName = 'CContato'
      Origin = 'entidades.CContato'
      Size = 60
    end
    object QrEntidadesLRua: TWideStringField
      FieldName = 'LRua'
      Origin = 'entidades.LRua'
      Size = 60
    end
    object QrEntidadesLCompl: TWideStringField
      FieldName = 'LCompl'
      Origin = 'entidades.LCompl'
      Size = 60
    end
    object QrEntidadesLBairro: TWideStringField
      FieldName = 'LBairro'
      Origin = 'entidades.LBairro'
      Size = 60
    end
    object QrEntidadesLCidade: TWideStringField
      FieldName = 'LCidade'
      Origin = 'entidades.LCidade'
      Size = 60
    end
    object QrEntidadesLUF: TSmallintField
      FieldName = 'LUF'
      Origin = 'entidades.LUF'
      Required = True
    end
    object QrEntidadesLCEP: TIntegerField
      FieldName = 'LCEP'
      Origin = 'entidades.LCEP'
    end
    object QrEntidadesLPais: TWideStringField
      FieldName = 'LPais'
      Origin = 'entidades.LPais'
    end
    object QrEntidadesLTel: TWideStringField
      FieldName = 'LTel'
      Origin = 'entidades.LTel'
    end
    object QrEntidadesLFax: TWideStringField
      FieldName = 'LFax'
      Origin = 'entidades.LFax'
    end
    object QrEntidadesLCel: TWideStringField
      FieldName = 'LCel'
      Origin = 'entidades.LCel'
    end
    object QrEntidadesLContato: TWideStringField
      FieldName = 'LContato'
      Origin = 'entidades.LContato'
      Size = 60
    end
    object QrEntidadesComissao: TFloatField
      FieldName = 'Comissao'
      Origin = 'entidades.Comissao'
      DisplayFormat = '0.000000'
    end
    object QrEntidadesDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'entidades.DataCad'
      Required = True
    end
    object QrEntidadesECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesPCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesCCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesLCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Size = 100
      Calculated = True
    end
    object QrEntidadesNOMEALT2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEALT2'
      Size = 100
      Calculated = True
    end
    object QrEntidadesSituacao: TSmallintField
      FieldName = 'Situacao'
      Origin = 'entidades.Situacao'
    end
    object QrEntidadesNivel: TWideStringField
      FieldName = 'Nivel'
      Origin = 'entidades.Nivel'
      Size = 1
    end
    object QrEntidadesCTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CTEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LTEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesNOMEENTIGRUPO: TWideStringField
      FieldName = 'NOMEENTIGRUPO'
      Origin = 'entigrupos.Nome'
      Size = 100
    end
    object QrEntidadesNOMECUF: TWideStringField
      FieldName = 'NOMECUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMELUF: TWideStringField
      FieldName = 'NOMELUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesAccount: TIntegerField
      FieldName = 'Account'
      Origin = 'entidades.Account'
    end
    object QrEntidadesNOMEACCOUNT: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEACCOUNT'
      Size = 100
    end
    object QrEntidadesLogo2: TBlobField
      FieldName = 'Logo2'
      Origin = 'entidades.Logo2'
      Size = 4
    end
    object QrEntidadesELograd: TSmallintField
      FieldName = 'ELograd'
      Origin = 'entidades.ELograd'
      Required = True
    end
    object QrEntidadesPLograd: TSmallintField
      FieldName = 'PLograd'
      Origin = 'entidades.PLograd'
      Required = True
    end
    object QrEntidadesConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Origin = 'entidades.ConjugeNome'
      Size = 35
    end
    object QrEntidadesConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
      Origin = 'entidades.ConjugeNatal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEntidadesNome1: TWideStringField
      FieldName = 'Nome1'
      Origin = 'entidades.Nome1'
      Size = 30
    end
    object QrEntidadesNatal1: TDateField
      FieldName = 'Natal1'
      Origin = 'entidades.Natal1'
    end
    object QrEntidadesNome2: TWideStringField
      FieldName = 'Nome2'
      Origin = 'entidades.Nome2'
      Size = 30
    end
    object QrEntidadesNatal2: TDateField
      FieldName = 'Natal2'
      Origin = 'entidades.Natal2'
    end
    object QrEntidadesNome3: TWideStringField
      FieldName = 'Nome3'
      Origin = 'entidades.Nome3'
      Size = 30
    end
    object QrEntidadesNatal3: TDateField
      FieldName = 'Natal3'
      Origin = 'entidades.Natal3'
    end
    object QrEntidadesCreditosI: TIntegerField
      FieldName = 'CreditosI'
      Origin = 'entidades.CreditosI'
      Required = True
    end
    object QrEntidadesCreditosL: TIntegerField
      FieldName = 'CreditosL'
      Origin = 'entidades.CreditosL'
      Required = True
    end
    object QrEntidadesCreditosD: TDateField
      FieldName = 'CreditosD'
      Origin = 'entidades.CreditosD'
    end
    object QrEntidadesCreditosU: TDateField
      FieldName = 'CreditosU'
      Origin = 'entidades.CreditosU'
    end
    object QrEntidadesCreditosV: TDateField
      FieldName = 'CreditosV'
      Origin = 'entidades.CreditosV'
    end
    object QrEntidadesMotivo: TIntegerField
      FieldName = 'Motivo'
      Origin = 'entidades.Motivo'
      Required = True
    end
    object QrEntidadesQuantI1: TIntegerField
      FieldName = 'QuantI1'
      Origin = 'entidades.QuantI1'
      Required = True
    end
    object QrEntidadesQuantI2: TIntegerField
      FieldName = 'QuantI2'
      Origin = 'entidades.QuantI2'
      Required = True
    end
    object QrEntidadesQuantI3: TIntegerField
      FieldName = 'QuantI3'
      Origin = 'entidades.QuantI3'
      Required = True
    end
    object QrEntidadesQuantI4: TIntegerField
      FieldName = 'QuantI4'
      Origin = 'entidades.QuantI4'
      Required = True
    end
    object QrEntidadesAgenda: TWideStringField
      FieldName = 'Agenda'
      Origin = 'entidades.Agenda'
      Required = True
      Size = 1
    end
    object QrEntidadesSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Origin = 'entidades.SenhaQuer'
      Required = True
      Size = 1
    end
    object QrEntidadesSenha1: TWideStringField
      FieldName = 'Senha1'
      Origin = 'entidades.Senha1'
      Size = 6
    end
    object QrEntidadesNatal4: TDateField
      FieldName = 'Natal4'
      Origin = 'entidades.Natal4'
    end
    object QrEntidadesNome4: TWideStringField
      FieldName = 'Nome4'
      Origin = 'entidades.Nome4'
      Size = 30
    end
    object QrEntidadesNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Origin = 'motivose.Descricao'
      Size = 50
    end
    object QrEntidadesCreditosF2: TFloatField
      FieldName = 'CreditosF2'
      Origin = 'entidades.CreditosF2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesNOMEELOGRAD: TWideStringField
      FieldName = 'NOMEELOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesNOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesCLograd: TSmallintField
      FieldName = 'CLograd'
      Origin = 'entidades.CLograd'
      Required = True
    end
    object QrEntidadesLLograd: TSmallintField
      FieldName = 'LLograd'
      Origin = 'entidades.LLograd'
      Required = True
    end
    object QrEntidadesNOMECLOGRAD: TWideStringField
      FieldName = 'NOMECLOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesNOMELLOGRAD: TWideStringField
      FieldName = 'NOMELLOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesQuantN1: TFloatField
      FieldName = 'QuantN1'
      Origin = 'entidades.QuantN1'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesQuantN2: TFloatField
      FieldName = 'QuantN2'
      Origin = 'entidades.QuantN2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesLimiCred: TFloatField
      FieldName = 'LimiCred'
      Origin = 'entidades.LimiCred'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesDesco: TFloatField
      FieldName = 'Desco'
      Origin = 'entidades.Desco'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrEntidadesCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Origin = 'entidades.CasasApliDesco'
      Required = True
    end
    object QrEntidadesCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Origin = 'entidades.CPF_Pai'
      Size = 18
    end
    object QrEntidadesSSP: TWideStringField
      FieldName = 'SSP'
      Origin = 'entidades.SSP'
      Size = 10
    end
    object QrEntidadesCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Origin = 'entidades.CidadeNatal'
      Size = 30
    end
    object QrEntidadesCPF_PAI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_PAI_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Origin = 'entidades.UFNatal'
      Required = True
    end
    object QrEntidadesNOMENUF: TWideStringField
      FieldName = 'NOMENUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesFatorCompra: TFloatField
      FieldName = 'FatorCompra'
      Origin = 'entidades.FatorCompra'
      DisplayFormat = '#,###,###0.0000'
    end
    object QrEntidadesAdValorem: TFloatField
      FieldName = 'AdValorem'
      Origin = 'entidades.AdValorem'
      DisplayFormat = '#,###,###0.0000'
    end
    object QrEntidadesDMaisC: TIntegerField
      FieldName = 'DMaisC'
      Origin = 'entidades.DMaisC'
      DisplayFormat = '0'
    end
    object QrEntidadesDMaisD: TIntegerField
      FieldName = 'DMaisD'
      Origin = 'entidades.DMaisD'
      DisplayFormat = '0'
    end
    object QrEntidadesDataRG: TDateField
      FieldName = 'DataRG'
      Origin = 'entidades.DataRG'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEntidadesNacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Origin = 'entidades.Nacionalid'
      Size = 15
    end
    object QrEntidadesEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'entidades.Empresa'
    end
    object QrEntidadesNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'entidades.RazaoSocial'
      Size = 100
    end
    object QrEntidadesFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
      Origin = 'entidades.FormaSociet'
    end
    object QrEntidadesSimples: TSmallintField
      FieldName = 'Simples'
      Origin = 'entidades.Simples'
      Required = True
    end
    object QrEntidadesAtividade: TWideStringField
      FieldName = 'Atividade'
      Origin = 'entidades.Atividade'
      Size = 50
    end
    object QrEntidadesEstCivil: TSmallintField
      FieldName = 'EstCivil'
      Origin = 'entidades.EstCivil'
      Required = True
    end
    object QrEntidadesNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Origin = 'listaecivil.Nome'
      Required = True
      Size = 10
    end
    object QrEntidadesCPF_Conjuge: TWideStringField
      FieldName = 'CPF_Conjuge'
      Origin = 'entidades.CPF_Conjuge'
      Size = 18
    end
    object QrEntidadesCBE: TIntegerField
      FieldName = 'CBE'
      Origin = 'entidades.CBE'
    end
    object QrEntidadesSCB: TIntegerField
      FieldName = 'SCB'
      Origin = 'entidades.SCB'
    end
    object QrEntidadesCPF_Resp1: TWideStringField
      FieldName = 'CPF_Resp1'
      Origin = 'entidades.CPF_Resp1'
      Size = 18
    end
    object QrEntidadesCPF_Resp1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_Resp1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesENumero: TIntegerField
      FieldName = 'ENumero'
      Origin = 'entidades.ENumero'
    end
    object QrEntidadesPNumero: TIntegerField
      FieldName = 'PNumero'
      Origin = 'entidades.PNumero'
    end
    object QrEntidadesCNumero: TIntegerField
      FieldName = 'CNumero'
      Origin = 'entidades.CNumero'
    end
    object QrEntidadesLNumero: TIntegerField
      FieldName = 'LNumero'
      Origin = 'entidades.LNumero'
    end
    object QrEntidadesBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'entidades.Banco'
      DisplayFormat = '000'
    end
    object QrEntidadesAgencia: TWideStringField
      DisplayWidth = 4
      FieldName = 'Agencia'
      Origin = 'entidades.Agencia'
      Size = 11
    end
    object QrEntidadesContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'entidades.ContaCorrente'
      Size = 15
    end
    object QrEntidadesCartPref: TIntegerField
      FieldName = 'CartPref'
      Origin = 'entidades.CartPref'
      Required = True
    end
    object QrEntidadesNOMECARTPREF: TWideStringField
      FieldName = 'NOMECARTPREF'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrEntidadesRolComis: TIntegerField
      FieldName = 'RolComis'
      Origin = 'entidades.RolComis'
      Required = True
    end
    object QrEntidadesFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
      Required = True
    end
    object QrEntidadesIEST: TWideStringField
      FieldName = 'IEST'
      Origin = 'entidades.IEST'
    end
    object QrEntidadesQuantN3: TFloatField
      FieldName = 'QuantN3'
      Origin = 'entidades.QuantN3'
    end
    object QrEntidadesTempA: TFloatField
      FieldName = 'TempA'
      Origin = 'entidades.TempA'
    end
    object QrEntidadesTempD: TFloatField
      FieldName = 'TempD'
      Origin = 'entidades.TempD'
    end
    object QrEntidadesPAtividad: TIntegerField
      FieldName = 'PAtividad'
      Origin = 'entidades.PAtividad'
    end
    object QrEntidadesEAtividad: TIntegerField
      FieldName = 'EAtividad'
      Origin = 'entidades.EAtividad'
    end
    object QrEntidadesPCidadeCod: TIntegerField
      FieldName = 'PCidadeCod'
      Origin = 'entidades.PCidadeCod'
    end
    object QrEntidadesECidadeCod: TIntegerField
      FieldName = 'ECidadeCod'
      Origin = 'entidades.ECidadeCod'
    end
    object QrEntidadesPPaisCod: TIntegerField
      FieldName = 'PPaisCod'
      Origin = 'entidades.PPaisCod'
    end
    object QrEntidadesEPaisCod: TIntegerField
      FieldName = 'EPaisCod'
      Origin = 'entidades.EPaisCod'
    end
    object QrEntidadesAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'entidades.Antigo'
    end
    object QrEntidadesCUF2: TWideStringField
      FieldName = 'CUF2'
      Origin = 'entidades.CUF2'
      Size = 2
    end
    object QrEntidadesContab: TWideStringField
      FieldName = 'Contab'
      Origin = 'entidades.Contab'
    end
    object QrEntidadesMSN1: TWideStringField
      FieldName = 'MSN1'
      Origin = 'entidades.MSN1'
      Size = 255
    end
    object QrEntidadesPastaTxtFTP: TWideStringField
      FieldName = 'PastaTxtFTP'
      Origin = 'entidades.PastaTxtFTP'
      Size = 8
    end
    object QrEntidadesPastaPwdFTP: TWideStringField
      FieldName = 'PastaPwdFTP'
      Origin = 'entidades.PastaPwdFTP'
      Size = 100
    end
    object QrEntidadesProtestar: TSmallintField
      FieldName = 'Protestar'
      Origin = 'entidades.Protestar'
      Required = True
    end
    object QrEntidadesMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
      Origin = 'entidades.MultaCodi'
      Required = True
    end
    object QrEntidadesMultaDias: TSmallintField
      FieldName = 'MultaDias'
      Origin = 'entidades.MultaDias'
      Required = True
    end
    object QrEntidadesMultaValr: TFloatField
      FieldName = 'MultaValr'
      Origin = 'entidades.MultaValr'
      Required = True
    end
    object QrEntidadesMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'entidades.MultaPerc'
      Required = True
    end
    object QrEntidadesMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
      Origin = 'entidades.MultaTiVe'
      Required = True
    end
    object QrEntidadesJuroSacado: TFloatField
      FieldName = 'JuroSacado'
      Origin = 'entidades.JuroSacado'
      Required = True
    end
    object QrEntidadesCPMF: TFloatField
      FieldName = 'CPMF'
      Origin = 'entidades.CPMF'
      Required = True
    end
    object QrEntidadesCorrido: TIntegerField
      FieldName = 'Corrido'
      Origin = 'entidades.Corrido'
      Required = True
    end
    object QrEntidadesCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
      Required = True
    end
    object QrEntidadesAltDtPlaCt: TDateField
      FieldName = 'AltDtPlaCt'
      Origin = 'entidades.AltDtPlaCt'
      Required = True
    end
    object QrEntidadesAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'entidades.AlterWeb'
      Required = True
    end
    object QrEntidadesAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'entidades.Ativo'
      Required = True
    end
    object QrEntidadesEEndeRef: TWideStringField
      FieldName = 'EEndeRef'
      Origin = 'entidades.EEndeRef'
      Size = 100
    end
    object QrEntidadesPEndeRef: TWideStringField
      FieldName = 'PEndeRef'
      Origin = 'entidades.PEndeRef'
      Size = 100
    end
    object QrEntidadesCEndeRef: TWideStringField
      FieldName = 'CEndeRef'
      Origin = 'entidades.CEndeRef'
      Size = 100
    end
    object QrEntidadesLEndeRef: TWideStringField
      FieldName = 'LEndeRef'
      Origin = 'entidades.LEndeRef'
      Size = 100
    end
    object QrEntidadesPNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PNUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesENUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesCNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesLNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LNUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesDATARG_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATARG_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntidadesECodMunici: TIntegerField
      FieldName = 'ECodMunici'
      Origin = 'entidades.ECodMunici'
    end
    object QrEntidadesPCodMunici: TIntegerField
      FieldName = 'PCodMunici'
      Origin = 'entidades.PCodMunici'
    end
    object QrEntidadesCCodMunici: TIntegerField
      FieldName = 'CCodMunici'
      Origin = 'entidades.CCodMunici'
    end
    object QrEntidadesLCodMunici: TIntegerField
      FieldName = 'LCodMunici'
      Origin = 'entidades.LCodMunici'
    end
    object QrEntidadesCNAE: TWideStringField
      FieldName = 'CNAE'
      Origin = 'entidades.CNAE'
      Size = 7
    end
    object QrEntidadesSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Origin = 'entidades.SUFRAMA'
      Size = 9
    end
    object QrEntidadesECodiPais: TIntegerField
      FieldName = 'ECodiPais'
      Origin = 'entidades.ECodiPais'
    end
    object QrEntidadesPCodiPais: TIntegerField
      FieldName = 'PCodiPais'
      Origin = 'entidades.PCodiPais'
    end
    object QrEntidadesL_CNPJ: TWideStringField
      FieldName = 'L_CNPJ'
      Origin = 'entidades.L_CNPJ'
      Size = 14
    end
    object QrEntidadesL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
      Origin = 'entidades.L_Ativo'
    end
    object QrEntidadesL_CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'L_CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesNO_DTB_EMUNICI: TWideStringField
      FieldName = 'NO_DTB_EMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_DTB_PMUNICI: TWideStringField
      FieldName = 'NO_DTB_PMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_DTB_CMUNICI: TWideStringField
      FieldName = 'NO_DTB_CMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_DTB_LMUNICI: TWideStringField
      FieldName = 'NO_DTB_LMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_BACEN_EPAIS: TWideStringField
      FieldName = 'NO_BACEN_EPAIS'
      Origin = 'bacen_pais.Nome'
      Size = 100
    end
    object QrEntidadesNO_BACEN_PPAIS: TWideStringField
      FieldName = 'NO_BACEN_PPAIS'
      Origin = 'bacen_pais.Nome'
      Size = 100
    end
    object QrEntidadesCNAE_Nome: TWideStringField
      FieldName = 'CNAE_Nome'
      Origin = 'cnae20n.Nome'
      Size = 255
    end
    object QrEntidadesNIRE: TWideStringField
      FieldName = 'NIRE'
      Origin = 'entidades.NIRE'
      Size = 15
    end
    object QrEntidadesIEST_TXT: TWideStringField
      DisplayWidth = 30
      FieldKind = fkCalculated
      FieldName = 'IEST_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesURL: TWideStringField
      FieldName = 'URL'
      Origin = 'entidades.URL'
      Size = 60
    end
    object QrEntidadesCRT: TSmallintField
      FieldName = 'CRT'
    end
    object QrEntidadesCOD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEntidadesETe1Tip: TIntegerField
      FieldName = 'ETe1Tip'
    end
    object QrEntidadesECelTip: TIntegerField
      FieldName = 'ECelTip'
    end
    object QrEntidadesPTe1Tip: TIntegerField
      FieldName = 'PTe1Tip'
    end
    object QrEntidadesPCelTip: TIntegerField
      FieldName = 'PCelTip'
    end
    object QrEntidadesPTe1Tip_TXT: TWideStringField
      FieldName = 'PTe1Tip_TXT'
      Size = 30
    end
    object QrEntidadesPCelTip_TXT: TWideStringField
      FieldName = 'PCelTip_TXT'
      Size = 30
    end
    object QrEntidadesETe1Tip_TXT: TWideStringField
      FieldName = 'ETe1Tip_TXT'
      Size = 30
    end
    object QrEntidadesECelTip_TXT: TWideStringField
      FieldName = 'ECelTip_TXT'
      Size = 30
    end
    object QrEntidadesENATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENATAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntidadesPNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PNATAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntidadesENatal: TDateField
      FieldName = 'ENatal'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEntidadesPNatal: TDateField
      FieldName = 'PNatal'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEntidadesESite: TWideStringField
      FieldName = 'ESite'
      Size = 100
    end
    object QrEntidadesPSite: TWideStringField
      FieldName = 'PSite'
      Size = 100
    end
    object QrEntidadesindIEDest: TSmallintField
      FieldName = 'indIEDest'
    end
    object QrEntidadesEstrangDef: TSmallintField
      FieldName = 'EstrangDef'
    end
    object QrEntidadesEstrangTip: TSmallintField
      FieldName = 'EstrangTip'
    end
    object QrEntidadesEstrangNum: TWideStringField
      FieldName = 'EstrangNum'
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntiContatBeforeClose
    AfterScroll = QrEntiContatAfterScroll
    OnCalcFields = QrEntiContatCalcFields
    SQL.Strings = (
      'SELECT eco.Codigo, eco.Controle, eco.Nome, eco.Cargo,'
      'eca.Nome NOME_CARGO, eco.DtaNatal, eco.Sexo,'
      'ELT(eco.Sexo+1, "NI", "M", "F", "?") SEXO_TXT,'
      
        'IF(eco.DtaNatal <= "1899-12-30", "", DATE_FORMAT(eco.DtaNatal, "' +
        '%d/%m/%Y")) DTANATAL_TXT'
      'FROM enticontat eco'
      'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo'
      'WHERE ece.Codigo=:P0')
    Left = 240
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiContatCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrEntiContatNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrEntiContatDtaNatal: TDateField
      FieldName = 'DtaNatal'
    end
    object QrEntiContatDTANATAL_TXT: TWideStringField
      FieldName = 'DTANATAL_TXT'
      Size = 10
    end
    object QrEntiContatSexo: TSmallintField
      FieldName = 'Sexo'
    end
    object QrEntiContatSEXO_TXT: TWideStringField
      FieldName = 'SEXO_TXT'
      Size = 2
    end
    object QrEntiContatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiContatTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrEntiContatCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntiContatCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntiContatAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrEntiContatCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntiContatAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 268
    Top = 72
  end
  object QrEntiTel: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiTelCalcFields
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, emt.Conta, '
      'emt.Telefone, emt.EntiTipCto, emt.Ramal'
      'FROM entitel emt'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto'#13
      'WHERE emt.Controle=:P0')
    Left = 296
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTelNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Origin = 'entitipcto.Nome'
      Size = 30
    end
    object QrEntiTelConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'entitel.Conta'
      Required = True
    end
    object QrEntiTelTelefone: TWideStringField
      FieldName = 'Telefone'
      Origin = 'entitel.Telefone'
    end
    object QrEntiTelEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'entitel.EntiTipCto'
      Required = True
    end
    object QrEntiTelTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiTelRamal: TWideStringField
      FieldName = 'Ramal'
      Origin = 'entitel.Ramal'
    end
  end
  object DsEntiTel: TDataSource
    DataSet = QrEntiTel
    Left = 324
    Top = 72
  end
  object QrEntiMail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, ema.Conta, '
      'ema.EMail, ema.EntiTipCto, ema.Ordem'
      'FROM entimail ema'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto'#13
      'WHERE ema.Controle=:P0'
      'ORDER BY ema.Ordem, ema.Conta'#10)
    Left = 352
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiMailNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrEntiMailConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Required = True
      Size = 255
    end
    object QrEntiMailEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
    object QrEntiMailOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsEntiMail: TDataSource
    DataSet = QrEntiMail
    Left = 380
    Top = 72
  end
  object QrAtrEntiDef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ' +
        'ATRITS, '
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,'
      'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp '
      'FROM atrentidef def '
      'LEFT JOIN atrentiits its ON its.Controle=def.AtrIts '
      'LEFT JOIN atrenticad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc>0 '
      ' '
      'UNION  '
      ' '
      'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, '
      'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,'
      'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp '
      'FROM atrentitxt def '
      'LEFT JOIN atrenticad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc>0 '
      ' '
      'ORDER BY NO_CAD, NO_ITS '
      '')
    Left = 408
    Top = 72
    object QrAtrEntiDefID_Item: TIntegerField
      FieldName = 'ID_Item'
      Required = True
    end
    object QrAtrEntiDefID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
      Required = True
    end
    object QrAtrEntiDefAtrCad: TIntegerField
      FieldName = 'AtrCad'
      Required = True
    end
    object QrAtrEntiDefATRITS: TFloatField
      FieldName = 'ATRITS'
      Required = True
    end
    object QrAtrEntiDefCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
    end
    object QrAtrEntiDefCU_ITS: TLargeintField
      FieldName = 'CU_ITS'
    end
    object QrAtrEntiDefNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrAtrEntiDefNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 255
    end
    object QrAtrEntiDefAtrTyp: TSmallintField
      FieldName = 'AtrTyp'
    end
    object QrAtrEntiDefAtrTxt: TWideStringField
      FieldName = 'AtrTxt'
      Size = 255
    end
  end
  object DsAtrEntiDef: TDataSource
    DataSet = QrAtrEntiDef
    Left = 436
    Top = 72
  end
  object QrEntiRespon: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiResponCalcFields
    SQL.Strings = (
      'SELECT ere.*, eca.Nome NOME_CARGO ,'
      'ELT(ere.Assina+1, "N'#227'o", "Sim", "???") NO_ASSINA,'
      'IF(ent.Tipo=0, ent.ETe1, PTe1) Te1,'
      'IF(ent.Tipo=0, ent.ECel, PCel) Cel,'
      'IF(ent.Tipo=0, ent.EEmail, PEmail) Email'
      'FROM entirespon ere'
      'LEFT JOIN enticargos eca ON eca.Codigo=ere.Cargo'
      'LEFT JOIN entidades ent ON ent.Codigo = ere.Entidade'
      'WHERE ere.Codigo=:P0'
      ''
      ''
      ''
      '')
    Left = 464
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiResponCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiResponControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiResponNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiResponCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrEntiResponAssina: TSmallintField
      FieldName = 'Assina'
      Required = True
    end
    object QrEntiResponOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Required = True
    end
    object QrEntiResponObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrEntiResponNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrEntiResponNO_ASSINA: TWideStringField
      FieldName = 'NO_ASSINA'
      Size = 3
    end
    object QrEntiResponMandatoIni: TDateField
      FieldName = 'MandatoIni'
    end
    object QrEntiResponMandatoFim: TDateField
      FieldName = 'MandatoFim'
    end
    object QrEntiResponMandatoIni_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoIni_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntiResponMandatoFim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoFim_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntiResponTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrEntiResponCel: TWideStringField
      FieldName = 'Cel'
    end
    object QrEntiResponEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrEntiResponEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrEntiResponTe1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiResponCel_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Cel_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsEntiRespon: TDataSource
    DataSet = QrEntiRespon
    Left = 492
    Top = 72
  end
  object DsEntiCtas: TDataSource
    DataSet = QrEntiCtas
    Left = 548
    Top = 72
  end
  object QrEntiCtas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.*, ban.Nome NOMEBANCO'
      'FROM entictas cta'
      'LEFT JOIN entidades ent ON ent.Codigo=cta.Codigo'
      'LEFT JOIN bancos ban ON ban.Codigo=cta.Banco'
      'WHERE cta.Codigo=:P0'
      'ORDER BY cta.Ordem')
    Left = 520
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCtasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiCtasControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiCtasOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEntiCtasBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrEntiCtasAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrEntiCtasContaCor: TWideStringField
      FieldName = 'ContaCor'
      Size = 10
    end
    object QrEntiCtasContaTip: TWideStringField
      FieldName = 'ContaTip'
      Size = 10
    end
    object QrEntiCtasDAC_A: TWideStringField
      FieldName = 'DAC_A'
      Size = 1
    end
    object QrEntiCtasDAC_C: TWideStringField
      FieldName = 'DAC_C'
      Size = 1
    end
    object QrEntiCtasDAC_AC: TWideStringField
      FieldName = 'DAC_AC'
      Size = 1
    end
    object QrEntiCtasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEntiCtasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEntiCtasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntiCtasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEntiCtasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEntiCtasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEntiCtasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEntiCtasNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
  end
  object QrEntiSrvPro: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etp.Nome NO_ORGAO, esp.* '
      'FROM entisrvpro esp'
      'LEFT JOIN entitippro etp ON etp.Codigo=esp.Orgao'
      'WHERE esp.Codigo=:P0'
      'ORDER BY Controle DESC')
    Left = 576
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiSrvProNO_ORGAO: TWideStringField
      FieldName = 'NO_ORGAO'
      Size = 60
    end
    object QrEntiSrvProCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiSrvProControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEntiSrvProOrgao: TIntegerField
      FieldName = 'Orgao'
    end
    object QrEntiSrvProData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEntiSrvProHora: TTimeField
      FieldName = 'Hora'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrEntiSrvProNumero: TWideStringField
      FieldName = 'Numero'
      Size = 60
    end
    object QrEntiSrvProResultado: TWideStringField
      FieldName = 'Resultado'
      Size = 255
    end
  end
  object DsEntiSrvPro: TDataSource
    DataSet = QrEntiSrvPro
    Left = 604
    Top = 72
  end
end
