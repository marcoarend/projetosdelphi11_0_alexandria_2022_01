unit TedCRemCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  ComCtrls, Grids, DBGrids, Menus, Variants, dmkEdit, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkLabel, dmkDBGrid, dmkImage, UnBancos,
  UnMyLinguas, frxClass, UnDmkProcFunc, DmkDAC_PF, frxDBSet, UnDmkEnums;

type
  TTipoGera = (tgEnvio, tgTeste);
  dceAlinha = (posEsquerda, posCentro, posDireita);
  TFmTedCRemCab = class(TForm)
    PainelDados: TPanel;
    DsTedCRemCab: TDataSource;
    QrTedCRemCab: TmySQLQuery;
    PainelEdita: TPanel;
    QrTedCRemIts: TmySQLQuery;
    DsTedCRemIts: TDataSource;
    PMLot: TPopupMenu;
    Crianovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMCheques: TPopupMenu;
    Inclui1: TMenuItem;
    Retira1: TMenuItem;
    QrTedCRemCabMyDATAS: TWideStringField;
    QrTedCCfgCab: TmySQLQuery;
    Splitter1: TSplitter;
    QrTedCRemCabMyDATAG: TWideStringField;
    QrTedCCfgCabNome: TWideStringField;
    QrTedCCfgCabCodigo: TIntegerField;
    QrLot: TmySQLQuery;
    QrLotCodUsu: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel10: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox3: TGroupBox;
    BtConfirma: TBitBtn;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    GBEdita: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Label10: TLabel;
    BtGera: TBitBtn;
    BtCheques: TBitBtn;
    BtLot: TBitBtn;
    Edit1: TEdit;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    Panel7: TPanel;
    Panel9: TPanel;
    GroupBox2: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label12: TLabel;
    DBEdit1: TDBEdit;
    DBEdit4: TDBEdit;
    GroupBox4: TGroupBox;
    Panel1: TPanel;
    Label1: TLabel;
    Label8: TLabel;
    Label21: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit9: TDBEdit;
    QrTedCCfgCabCartDep: TIntegerField;
    QrTedCCfgCabEmpresa: TIntegerField;
    QrTedCCfgCabCodCliInt: TIntegerField;
    QrTedCCfgCabNO_CARTDEP: TWideStringField;
    QrTedCCfgCabNO_EMPRESA: TWideStringField;
    Panel8: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label3: TLabel;
    TPDataG: TdmkEditDateTimePicker;
    Label11: TLabel;
    TPHoraG: TDateTimePicker;
    Label20: TLabel;
    EdCodUsu: TdmkEdit;
    Label7: TLabel;
    EdTedC_Cfg: TdmkEditCB;
    CBTedC_Cfg: TdmkDBLookupComboBox;
    PnIndiretos: TPanel;
    Label13: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    QrTedCRemCabNO_TedCCfgCab: TWideStringField;
    QrTedCRemCabNO_EMPRESA: TWideStringField;
    QrTedCRemCabNO_CARTDEP: TWideStringField;
    QrTedCRemCabCodigo: TIntegerField;
    QrTedCRemCabCodUsu: TIntegerField;
    QrTedCRemCabDataG: TDateField;
    QrTedCRemCabHoraG: TTimeField;
    QrTedCRemCabTedC_Cfg: TIntegerField;
    QrTedCRemCabDataS: TDateField;
    QrTedCRemCabHoraS: TTimeField;
    QrTedCRemCabEmpresa: TIntegerField;
    QrTedCRemCabCartDep: TIntegerField;
    QrTedCRemCabSeqArq: TIntegerField;
    QrTedCRemCabBanco: TIntegerField;
    QrTedCRemCabTamReg: TIntegerField;
    N1: TMenuItem;
    QrTedCRemItsCodigo: TIntegerField;
    QrTedCRemItsControle: TIntegerField;
    QrTedCRemItsLctCtrl: TIntegerField;
    QrTedCRemItsLctSub: TIntegerField;
    QrTedCRemItsLctFatParc: TIntegerField;
    QrTedCRemItsBomPara: TDateField;
    QrTedCRemItsValor: TFloatField;
    QrTedCRemItsCMC7: TWideStringField;
    QrTedCRemItsLctSitAnt: TIntegerField;
    QrTedCRemItsLctCtrAnt: TIntegerField;
    QrTedCRemItsOCORR_TXT: TWideStringField;
    QrTedCRemItsOcorrBco: TIntegerField;
    QrTedCRemItsMyOcorr: TIntegerField;
    QrTedCRemCabCodEnti: TIntegerField;
    QrEmit: TmySQLQuery;
    QrEmitCNPJCPF: TWideStringField;
    QrEmitEmitente: TWideStringField;
    QrEmitTIPO: TIntegerField;
    QrEmitDOCUM: TWideStringField;
    QrTedCRemItsLctFatID: TIntegerField;
    QrTedCRemItsLctFatNum: TLargeintField;
    QrTedCRemCabDOC_EMPRESA: TWideStringField;
    QrTedCRemCabTIPO_ENT: TSmallintField;
    GBArqSalvo: TGroupBox;
    EdArqSalvo: TEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    TabSheet2: TTabSheet;
    TCRegistros: TTabControl;
    QrTedCRemCabEncerrado: TSmallintField;
    N2: TMenuItem;
    Reabrelote1: TMenuItem;
    QrTedCRemCabPathRem: TWideStringField;
    TabSheet3: TTabSheet;
    QrCheque: TmySQLQuery;
    DsCheque: TDataSource;
    QrChequeBanco: TIntegerField;
    QrChequeAgencia: TIntegerField;
    QrChequeContaCorrente: TWideStringField;
    QrChequeDocumento: TFloatField;
    QrChequePraca: TIntegerField;
    QrChequeCredito: TFloatField;
    DBGrid2: TDBGrid;
    QrChequeCNPJCPF: TWideStringField;
    QrChequeEmitente: TWideStringField;
    QrTedCRemItsLoReCaPrev: TIntegerField;
    QrTedCRemItsLoReItPrev: TIntegerField;
    QrTedCRemItsLoReCaProc: TIntegerField;
    QrTedCRemItsLoReItProc: TIntegerField;
    QrTedCRemItsLoReMyOcor: TIntegerField;
    QrTedCRemCabCedente: TWideStringField;
    QrTedCRemCabCartQtd: TIntegerField;
    QrTedCRemCabCartVal: TFloatField;
    QrTedCRemCabEntrQtd: TIntegerField;
    QrTedCRemCabEntrVal: TFloatField;
    Panel2: TPanel;
    GroupBox5: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    GroupBox6: TGroupBox;
    Panel12: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    GroupBox7: TGroupBox;
    DBEdit2: TDBEdit;
    DBEdit16: TDBEdit;
    GroupBox8: TGroupBox;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    frxTED_CHQUE_001_01: TfrxReport;
    frxDsTedCRemCab: TfrxDBDataset;
    frxDsTedCRemIts: TfrxDBDataset;
    QrTedCRemItsContaCorrente: TWideStringField;
    QrTedCRemItsBanco: TIntegerField;
    QrTedCRemItsAgencia: TIntegerField;
    QrTedCRemItsDocumento: TFloatField;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    DBGrid1: TdmkDBGrid;
    Panel11: TPanel;
    BtExclui: TBitBtn;
    BtInclui: TBitBtn;
    Panel13: TPanel;
    DBGDespProf: TDBGrid;
    QrLcts: TmySQLQuery;
    QrLctsNOMECARTEIRA: TWideStringField;
    QrLctsNOMECONTA: TWideStringField;
    QrLctsDescricao: TWideStringField;
    QrLctsCredito: TFloatField;
    QrLctsVencimento: TDateField;
    QrLctsData: TDateField;
    QrLctsSEQ: TIntegerField;
    QrLctsControle: TIntegerField;
    QrLctsSub: TIntegerField;
    QrLctsGenero: TIntegerField;
    QrLctsCartao: TIntegerField;
    QrLctsSit: TIntegerField;
    QrLctsTipo: TIntegerField;
    QrLctsID_Pgto: TIntegerField;
    QrLctsCarteira: TIntegerField;
    QrLctsCliInt: TIntegerField;
    DsLcts: TDataSource;
    QrLctsDebito: TFloatField;
    SpeedButton5: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtLotClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTedCRemCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrTedCRemCabAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTedCRemCabBeforeOpen(DataSet: TDataSet);
    procedure Crianovolote1Click(Sender: TObject);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Excluiloteatual1Click(Sender: TObject);
    procedure PMLotPopup(Sender: TObject);
    procedure BtChequesClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure QrTedCRemCabCalcFields(DataSet: TDataSet);
    procedure BtDesiste3Click(Sender: TObject);
    procedure Retira1Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure QrTedCRemItsCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure EdTedC_CfgChange(Sender: TObject);
    procedure PMChequesPopup(Sender: TObject);
    procedure BtGeraClick(Sender: TObject);
    procedure QrEmitCalcFields(DataSet: TDataSet);
    procedure TCRegistrosChange(Sender: TObject);
    procedure QrTedCRemCabBeforeClose(DataSet: TDataSet);
    procedure Reabrelote1Click(Sender: TObject);
    procedure QrTedCRemItsBeforeClose(DataSet: TDataSet);
    procedure QrTedCRemItsAfterScroll(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure QrLctsCalcFields(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    FSeqLin, FMaxReg: Integer;
    MyArrGrades: array of TDBGrid;
    MyArrQueris: array of TmySQLQuery;
    MyArrDatSrc: array of TDataSource;
    FRegistros: TList;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure CriaTiposDeRegistros(Layout, SeqLay: Integer;
              Registros: array of Integer;
              SubRegs: array of Integer; Segmentos: array of String;
              SubSegs: array of Integer; Nomes: array of String);
    function  ObtemProximoLote(): Integer;
    //
    procedure GeraTED_Uno_0_Ene_1_Uno9(Layout, SeqLay: Integer);
    procedure GeraLns_Uno_0_Ene_1_Uno9(Item, Layout, Registro, SubReg:
              Integer; Segmento: String; SubSeg: Integer; Tabela: String);
    procedure ReopenLcts();
    //
    function  GeraLinhaHeader(const Layout, Registro, Subreg: Integer;
              const Segmento: String; const SubSeg: Integer;
              var Seq: Integer; const Tabela: String): Boolean;
    function  GeraLinhaDetail(const Layout, Registro, Subreg: Integer;
              const Segmento: String; const SubSeg: Integer;
              var Seq: Integer; const Tabela: String): Boolean;
    function  GeraLinhaTrailler(const Layout, Registro, Subreg: Integer;
              const Segmento: String; const SubSeg: Integer;
              var Seq: Integer; const Tabela: String): Boolean;
    function  Gera_ID_HEXADECIMAL(): String;
  public
    { Public declarations }
    //
    // Vari�veis do TedC_Aux
    FPosAnt, FPosIni, FPosFim, FPosTam, FTamInt, FTamFlu: Integer;
    FLinha, FTxt1, FTxt2: String;
    FContinua: Boolean;
    FReg: TFldTabRec;
    FLista: TList;
    // Fim vari�veis do TedC_Aux
    //
    FSeq: Integer;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenTedCRemIts(LctFatParc: Integer);
  end;

var
  FmTedCRemCab: TFmTedCRemCab;

const
  F_CR = sLineBreak;
  FFormatFloat = '00000';
  FArquivoSalvaArq = 'TedCheque.txt';
  FTamCNAB = 240;

implementation

uses UnMyObjects, Module, ModuleBco, UCreate, MyDBCheck, MyListas, TedCRemInn,
  TedC_Aux, ModuleGeral, ModuleTedC, CfgExpFile, TedCRemChg, UnFinanceiro,
  LctEdit, Principal;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTedCRemCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTedCRemCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTedCRemCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTedCRemCab.DefParams;
begin
  VAR_GOTOTABELA := 'tedcremcab';
  VAR_GOTOMYSQLTABLE := QrTedCRemCab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT tcc.Nome NO_TedCCfgCab, tcc.Banco, ');
  VAR_SQLx.Add('tcc.TamReg, tcc.PathRem, tcc.Cedente, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOC_EMPRESA,');
  VAR_SQLx.Add('ent.Tipo TIPO_ENT, car.Nome NO_CARTDEP, eci.CodEnti, trc.*');
  VAR_SQLx.Add('FROM tedcremcab trc');
  VAR_SQLx.Add('LEFT JOIN tedccfgcab tcc ON tcc.Codigo=trc.TedC_Cfg');
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo=trc.CartDep');
  VAR_SQLx.Add('LEFT JOIN enticliint eci ON eci.CodCliInt=trc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=eci.CodEnti');
  //
  VAR_SQL1.Add('WHERE trc.Codigo=:P0');
  //
  VAR_SQLa.Add(''); //WHERE trc.Nome Like :P0
  //
end;

procedure TFmTedCRemCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      Memo1.Align            := alTop;
      Memo1.Align            := alBottom;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text         := '';
        TPDataG.Date          := Date;
        TPHoraG.Time          := Time;
        EdTedC_Cfg.Text       := '0';
        CBTedC_Cfg.KeyValue  := NULL;
        EdCodUsu.ValueVariant := ObtemProximoLote();
      end else begin
        EdCodigo.Text         := IntToStr(QrTedCRemCabCodigo.Value);
        TPDataG.Date          := QrTedCRemCabDataG.Value;
        TPHoraG.Time          := QrTedCRemCabHoraG.Value;
        EdTedC_Cfg.Text       := IntToStr(QrTedCRemCabTedC_Cfg.Value);
        CBTedC_Cfg.KeyValue   := QrTedCRemCabTedC_Cfg.Value;
        EdCodUsu.ValueVariant := QrTedCRemCabCodUsu.Value;
      end;
      TPDataG.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

function TFmTedCRemCab.ObtemProximoLote(): Integer;
begin
  QrLot.Close;
  UMyMod.AbreQuery(QrLot, Dmod.MyDB);
  Result := QrLotCodUsu.Value + 1;
end;

procedure TFmTedCRemCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTedCRemCab.CriaTiposDeRegistros(Layout, SeqLay: Integer;
  Registros: array of Integer;
  SubRegs: array of Integer; Segmentos: array of String;
  SubSegs: array of Integer; Nomes: array of String);
var
  I, N, HighReg: Integer;
  TED: TArqExpTED;
  Tabela: String;
begin
  /// Destroy grades caso existam
  for I := 0 to FMaxReg - 1 do
  begin
    if MyArrGrades[I] <> nil then
      MyArrGrades[I].Free;
    if MyArrQueris[I] <> nil then
      MyArrQueris[I].Free;
    if MyArrDatSrc[I] <> nil then
      MyArrDatSrc[I].Free;
  end;
  //
  /// Destroy Lista de registros caso exista...
  if FRegistros <> nil then
    FRegistros.Free;
  //... e recria lista vazia
  FRegistros := TList.Create;
  //
  // Limpa lista de Tabs do TCRegistros
  TCRegistros.Tabs.Clear;
  //
  // Define quantidade de tipos de registros
  // e cria TmySQLQuery + TDataSource + TDBGrid para cada tipo
  HighReg := High(Registros);
  if HighReg = High(SubRegs) then
  if HighReg = High(Segmentos) then
  if HighReg = High(SubSegs) then
  begin
    FMaxReg := HighReg + 1;
    SetLength(MyArrGrades, FMaxReg);
    SetLength(MyArrQueris, FMaxReg);
    SetLength(MyArrDatSrc, FMaxReg);
    //
    for I := 0 to FMaxReg - 1 do
    begin
      New(TED);
      TED.Registro        := Registros[I];
      TED.SubReg          := SubRegs[I];
      TED.Segmento        := Segmentos[I];
      TED.SubSeg          := SubSegs[I];
      TED.Nome            := Nomes[I];
      FRegistros.Add(TED);
      //
      TCRegistros.Tabs.Add(Geral.FF0(Registros[I]) + '-' + Nomes[I]);
      Tabela := TedC_Ax.MotaNomeTabela(Registros[I], SubRegs[I], Segmentos[I], SubSegs[I]);
      TedC_Ax.CriaTabAux(Layout, Registros[I], Subregs[I], Segmentos[I], SubSegs[I], Tabela);
      //
      // Cria texto do arquivo de exporta��o
      case SeqLay of
        (*1*) VAR_TED_U0N1U9: GeraLns_Uno_0_Ene_1_Uno9(I, Layout, Registros[I],
                              SubRegs[I], Segmentos[I], SubSegs[I], Tabela);
         else Geral.MensagemBox('"SeqLay" n�o definido (B): ' + Geral.FF0(
              SeqLay), 'ERRO', MB_OK+MB_ICONERROR);
      end;
      // Abre em query os dados gerados do tipo registro rec�m terminado
      MyArrQueris[I] := TMySQLQuery.Create(FmTedCRemCab);
      MyArrQueris[I].Database := DModG.MyPID_DB;
      MyArrQueris[I].SQL.Clear;
      MyArrQueris[I].SQL.Add('SELECT * FROM ' + Tabela);
      MyArrQueris[I].SQL.Add('ORDER BY Linha');
      MyArrQueris[I].Open;
      //
      MyArrDatSrc[I] := TDataSource.Create(FmTedCRemCab);
      MyArrDatSrc[I].DataSet := MyArrQueris[I];
      //
      MyArrGrades[I] := TDBGrid.Create(FmTedCRemCab);
      MyArrGrades[I].Parent := TCRegistros;
      MyArrGrades[I].Visible := False;
      MyArrGrades[I].Align := alClient;
      MyArrGrades[I].Font.Name := 'Courrier New';
      MyArrGrades[I].DataSource := MyArrDatSrc[I];
      //
      DmTedC.QrTCCIts.First;
      while not DmTedC.QrTCCIts.Eof do
      begin
        //for N := 0 to MyArrGrades[I].Columns.Count - 1 do
        N := DmTedC.QrTCCIts.RecNo - 1;
        MyArrGrades[I].Columns[N].Title.Caption := DmTedC.QrTCCItsCampo.Value;
        //
        DmTedC.QrTCCIts.Next;
      end;
    end;
    TCRegistros.TabIndex := 0;
    TCRegistrosChange(Self);
  end else
  begin
    FMaxReg := 0;
    Geral.MensagemBox('ERRO na cria��o de Tipos de registros!' + sLineBreak +
    'AVISE A DERMATEK!', 'ERRO', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmTedCRemCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTedCRemCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTedCRemCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTedCRemCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTedCRemCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTedCRemCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTedCRemCab.SpeedButton5Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdTedC_Cfg.ValueVariant;
  //
  TedC_Ax.MostraTedCCfgCab(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrTedCCfgCab, Dmod.MyDB);
    //
    EdTedC_Cfg.ValueVariant := VAR_CADASTRO;
    CBTedC_Cfg.KeyValue     := VAR_CADASTRO;
    EdTedC_Cfg.SetFocus;
  end;
end;

procedure TFmTedCRemCab.TCRegistrosChange(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to FMaxReg - 1 do
    MyArrGrades[I].Visible := TCRegistros.TabIndex = I;
end;

procedure TFmTedCRemCab.BtLotClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLot, BtLot);
end;

procedure TFmTedCRemCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTedCRemCabCodigo.Value;
  Close;
end;

procedure TFmTedCRemCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, TedC_Cfg, CodUsu, CartDep, Empresa: Integer;
  DataG, HoraG: String;
begin
  DataG  := Geral.FDT(TPDataG.Date, 1);
  HoraG  := Geral.FDT(TPHoraG.Time, 100);
  CodUsu := EdCodUsu.ValueVariant;
  TedC_Cfg := Geral.IMV(EdTedC_Cfg.Text);
  if MyObjects.FIC(TedC_Cfg = 0, EdTedC_Cfg, 'Informe o layout a ser utilizado!') then
    Exit;
  CartDep := QrTedCCfgCabCartDep.Value;
  if MyObjects.FIC(CartDep = 0, EdTedC_Cfg,
  'O layout informado n�o tem a carteira de dep�sito definida!') then
    Exit;
  Empresa := QrTedCCfgCabEMpresa.Value;
  if MyObjects.FIC(Empresa = 0, EdTedC_Cfg,
  'O layout informado n�o tem a empresa definida!') then
    Exit;
  if MyObjects.FIC(QrTedCCfgCabCodCliInt.Value <> Empresa, EdTedC_Cfg,
  'A carteira de dep�sito n�o pertence a empresa no layout informado!') then
    Exit;
  //
  Codigo :=
    UMyMod.BPGS1I32('tedcremcab', 'Codigo', '', '', tsPos,
    ImgTipo.SQLType, QrTedCRemCabCodigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'tedcremcab', False, [
  'CodUsu', 'DataG', 'HoraG',
  'TedC_Cfg', (*'DataS', 'HoraS',
  'SeqArq',*) 'CartDep', 'Empresa'], [
  'Codigo'], [
  CodUsu, DataG, HoraG,
  TedC_Cfg, (*DataS, HoraS,
  SeqArq,*) CartDep, Empresa], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'tedcremcab', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmTedCRemCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'tedcremcab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'tedcremcab', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmTedCRemCab.BtExcluiClick(Sender: TObject);
begin
  if (QrTedCRemCab.State <> dsInactive) and (QrTedCRemCab.RecordCount > 0) and
    (QrLcts.State <> dsInactive) and (QrLcts.RecordCount > 0) then
  begin
    UFinanceiro.ExcluiItemCarteira(QrLctsControle.Value, QrLctsData.Value,
      QrLctsCarteira.Value, QrLctsSub.Value, QrLctsGenero.Value,
      QrLctsCartao.Value, QrLctsSit.Value, QrLctsTipo.Value, 0,
      QrLctsID_Pgto.Value, QrLcts, nil, True, QrLctsCarteira.Value, 311,
      CO_TabLctA, True, True);
    ReopenLcts();
  end;
end;

procedure TFmTedCRemCab.BtGeraClick(Sender: TObject);
var
  ArqSalvo, DataS, HoraS: String;
  Agora: TDateTime;
  Codigo, Layout, SeqLay: Integer;
begin
  if (QrTedCRemCab.State <> dsInactive) and (QrTedCRemCab.RecordCount > 0) then Exit;
  //
  if (QrTedCRemCabBanco.Value = 74) or (QrTedCRemCabBanco.Value = 422) then
  begin
    Geral.MB_Aviso('A gera��o do arquivo TED est� implementada apenas para os bancos: 74 e 422?' +
      sLineBreak + 'Gera��o de arquivo abortada!');
    Exit;
  end;
  //
  if Geral.MensagemBox(
  'Confirma a gera��o do arquivo TED? Esta a��o ir� encerrar o lote!',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    Exit;
  //
  Codigo := QrTedCRemCabCodigo.Value;
  if DmTedC.AtualizaLoteTedEnvio(Codigo,
  QrTedCRemCabEmpresa.Value, (*QrTedCRemCabProduto.Value
  QrTedCRemCabFilial.Value, QrTedCRemCabCNPJCPF.Value,*)
  QrTedCRemCabBanco.Value, QrTedCRemCabCedente.Value) then
    LocCod(QrTedCRemCabCodigo.Value, QrTedCRemCabCodigo.Value)
  else
    Exit;
  if Codigo <> QrTedCRemCabCodigo.Value then
  begin
    Geral.MensagemBox('Erro ao reabrir tabela!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end; 
  //
  FSeqLin := 0;
  Agora := DModG.ObtemAgora();
  ArqSalvo := '';
  if not TedC_Ax.PreparaExportacao(Memo1, FContinua) then
    Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmTedC.QrTCCCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM tedccfgcab ',
  'WHERE Codigo=' + Geral.FF0(QrTedCRemCabTedC_Cfg.Value),
  '']);
  if DmTedC.QrTCCCab.RecordCount = 0 then
  begin
    Geral.MensagemBox('Configura��o (layout) TED Cheque n�o localizado: ' +
    Geral.FF0(QrTedCRemCabTedC_Cfg.Value), 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Layout := QrTedCRemCabTedC_Cfg.Value;
  SeqLay := DmTedC.QrTCCCabSeqLay.Value;
  case SeqLay of
    (*1*) VAR_TED_U0N1U9: GeraTED_Uno_0_Ene_1_Uno9(Layout, SeqLay);
    else Geral.MensagemBox('"SeqLay" n�o definido (A): ' + Geral.FF0(
    DmTedC.QrTCCCabSeqLay.Value), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  //
  //  G R A V A    A R Q U I V O :
  if DBCheck.CriaFm(TFmCfgExpFile, FmCfgExpFile, afmoNegarComAviso) then
  begin
    FmCfgExpFile.Memo.WantReturns := Memo1.WantReturns;
    FmCfgExpFile.Memo.WantTabs    := Memo1.WantTabs;
    FmCfgExpFile.Memo.WordWrap    := Memo1.WordWrap;
    FmCfgExpFile.Memo.Text        := Memo1.Text;
    //
    FmCfgExpFile.EdDir.Text       := QrTedCRemCabPathRem.Value;
    FmCfgExpFile.EdArq.Text       := Geral.FFN(QrTedCRemCabEmpresa.Value, 3) +
                                     '_' +
                                     Geral.FFN(QrTedCRemCabCodUsu.Value, 6);
    FmCfgExpFile.EdExt.Text       := 'txt';
    //
    FmCfgExpFile.ShowModal;
    ArqSalvo := FmCfgExpFile.FArqSalvo;
    FmCfgExpFile.Destroy;
  end;
  if Trim(ArqSalvo) <> '' then
  begin
    DataS := Geral.FDT(Agora, 1);
    HoraS := Geral.FDT(Agora, 100);
    Codigo := QrTedCRemCabCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcremcab', False, [
      'DataS', 'HoraS', 'Encerrado'], ['Codigo'], [
      DataS, HoraS, 1], [Codigo], True) then
    begin
      Geral.MensagemBox('Arquivo salvo com sucesso!' + sLineBreak +
      ArqSalvo, 'Arquivo Salvo', MB_OK+MB_ICONINFORMATION);
      LocCod(Codigo, Codigo);
      EdArqSalvo.Text := ArqSalvo;
      GBArqSalvo.Visible := True;
    end;
  end else
    Geral.MensagemBox('O arquivo n�o pode ser salvo!',
    'ERRO', MB_OK+MB_ICONERROR);
  //
  //  FIM GRAVA��O ARQUIVO
  //
end;

procedure TFmTedCRemCab.BtIncluiClick(Sender: TObject);
begin
  if (QrTedCRemCab.State <> dsInactive) and (QrTedCRemCab.RecordCount > 0) then
  begin
    if UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit, lfProprio,
      afmoNegarComAviso, QrLcts, (*FmPrincipal.QrCarteiras*) nil,
      tgrInclui, QrLctsControle.Value, QrLctsSub.Value,
      0(*Genero*), 0(*Juros*), 0(*Multa*), nil, VAR_FATID_0373, 0,
      QrTedCRemCabCodigo.Value, QrTedCRemCabCartDep.Value, 0, 0, True,
      0(*Cliente*), 0(*Fornecedor*), Dmod.QrControleDono.Value(*cliInt*),
      Dmod.QrControleDono.Value(*ForneceI*), 0(*Account*), 0(*Vendedor*),
      True(*LockCliInt*), False(*LockForneceI*), True(*LockAccount*),
      False(*LockVendedor*), QrTedCRemCabDataG.Value, QrTedCRemCabDataG.Value,
      QrTedCRemCabDataG.Value, 3, 0, CO_TabLctA, 0, 0) > 0 then
    begin
      ReopenLcts();
    end;
  end;
end;

{
procedure TFmTedCRemCab.BtGeraClick(Sender: TObject);
{
  function EnderecoSacado(): String;
  begin
    Result := '';
    if Result <> '' then Result := Result + ' ';

    Result := Result + QrTedCRemItsSACADO_RUA.Value;
    if QrTedCRemItsSACADO_RUA.Value <> '' then Result := Result + ', ';

    Result := Result + QrTedCRemItsSACADO_NUMERO_TXT.Value;
    if QrTedCRemItsSACADO_NUMERO_TXT.Value <> '' then Result := Result + ' ';

    if QrTedCRemItsSACADO_COMPL.Value <> '' then Result := Result + '- ';
    Result := Result + QrTedCRemItsSACADO_COMPL.Value;
  end;
  function Endereco(): String;
  begin
    Result := DmBco.QrAddr3NOMELOGRAD.Value;
    if Result <> '' then Result := Result + ' ';

    Result := Result + DmBco.QrAddr3RUA.Value;
    if DmBco.QrAddr3RUA.Value <> '' then Result := Result + ', ';

    Result := Result + DmBco.QrAddr3NUMERO_TXT.Value;
    if DmBco.QrAddr3NUMERO_TXT.Value <> '' then Result := Result + ' ';

    if DmBco.QrAddr3COMPL.Value <> '' then Result := Result + '- ';
    Result := Result + DmBco.QrAddr3COMPL.Value;
  end;
const
  // PAREI AQUI Ver!!!
  Def_Client = -11;
var
  CedenteTipo, SacadorTipo, Item, Addr3_Tipo, n: Integer;
  SQL, CedenteNome, CedenteCNPJ, SacadorNome, SacadorCNPJ, NomeCedente,
  NomeSacador, CEPSacado_, CEPSacador, Addr3_CNPJ, Addr3_NOMEENT,
  Addr3_NOMELOGRAD, Addr3_RUA, Addr3_NUMERO_TXT, Addr3_COMPL,
  Addr3_BAIRRO, Addr3_CIDADE, Addr3_NOMEUF: String;
  JurosPerc, MultaPerc, JurosValr, MultaValr: Double;
  (*HoraI,*) DataMora: TDateTime;
  g: TStringGrid;
  (*
  Vezes, i: Integer;
  HoraF: TDateTime;
  *)
  CNAB_Cfg, SeqArq, Lote: Integer;
  SQL_HEADE_ARQ, SQL_HEADE_LOT, SQL_ITENS_LOT, AVALISTA: String;
begin
  CNAB_Cfg := QrTedCRemCabCNAB_Cfg.Value;
  SeqArq   := QrTedCRemCabSeqArq.Value;
  Lote     := QrTedCRemCabCodigo.Value;
  //
  SQL_HEADE_ARQ := MLAGeral.Linhas(
  ['SELECT con.Nome NOMECONFIG, cob.CodUsu NUM_REM, cob.* ',
  'FROM cnab_lot cob',
  'LEFT JOIN cnab_cfg con ON con.Codigo=cob.CNAB_Cfg',
  'WHERE cob.Codigo = ' + FormatFloat('0', Lote)]);
  //
  SQL_HEADE_LOT := MLAGeral.Linhas(
  ['SELECT con.Nome NOMECONFIG, cob.Codigo NUM_LOT, ',
  'cob.*, con.*',
  'FROM cnab_lot cob',
  'LEFT JOIN cnab_cfg con ON con.Codigo=cob.CNAB_Cfg',
  'WHERE cob.Codigo = ' + FormatFloat('0', Lote)]);
  //
  SQL_ITENS_LOT := MLAGeral.Linhas(
  ['SELECT loi.Duplicata, loi.Data, loi.Vencimento, ',
  'loi.Bruto, loi.Desco Abatimento, ',
  'loi.FatParcela + 0.000000000000000000 SeuNumero, ',
  'loi.FatParcela + 0.000000000000000000 Sequencial, ',
  'ent.Tipo AVALISTA_TIPO, ',
  'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END AVALISTA_NOME, ',
  'CASE WHEN ent.Tipo=0 THEN ent.CNPJ    ELSE ent.CPF     END AVALISTA_CNPJ, ',
  'CASE WHEN ent.Tipo=0 THEN ent.ERua    ELSE ent.PRua    END AVALISTA_RUA, ',
  'CASE WHEN ent.Tipo=0 THEN ent.ENumero+0.000 ELSE ent.PNumero+0.000 END AVALISTA_NUMERO, ',
  'CASE WHEN ent.Tipo=0 THEN ent.ECompl  ELSE ent.PCompl  END AVALISTA_COMPL, ',
  'CASE WHEN ent.Tipo=0 THEN ent.EBairro ELSE ent.PBairro END AVALISTA_BAIRRO, ',
  'CASE WHEN ent.Tipo=0 THEN ent.ECEP    ELSE ent.PCEP    END AVALISTA_CEP, ',
  'CASE WHEN ent.Tipo=0 THEN ent.ECidade ELSE ent.PCidade END AVALISTA_CIDADE, ',
  'CASE WHEN ent.Tipo=0 THEN ufe.Nome    ELSE ufp.Nome    END AVALISTA_xUF, ',

  'sac.CNPJ SACADO_CNPJ, sac.Nome SACADO_NOME, sac.Rua SACADO_RUA,',
  'sac.Numero SACADO_NUMERO, sac.Compl SACADO_COMPL, ',
  'sac.Bairro SACADO_BAIRRO, sac.Bairro SACADO_BAIRRO,',
  'sac.Cidade SACADO_CIDADE, sac.UF SACADO_xUF, ',
  'sac.CEP SACADO_CEP, IF(LENGTH(sac.CNPJ) >=14, 0, 1) SACADO_TIPO',
  'FROM ' + CO_TabLctA + ' loi ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum',
  'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente',
  'LEFT JOIN sacados   sac ON sac.CNPJ=loi.CNPJCPF ',
  'LEFT JOIN ufs       ufe ON ent.EUF=ufe.Codigo ',
  'LEFT JOIN ufs       ufp ON ent.PUF=ufp.Codigo ',
  'WHERE loi.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND loi.CNAB_Lot=:P0',
  'ORDER BY Duplicata']);
  //
  SQL := 'SELECT 0 Itens';
  if not DmBco.ReopenCNAB_Cfg(QrTedCRemCabCNAB_Cfg.Value,
    Def_Client, SQL,
    CedenteTipo, CedenteNome, CedenteCNPJ,
    SacadorTipo, SacadorNome, SacadorCNPJ) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  case DmBco.QrCNAB_CfgCNAB.Value of
    240:
    begin
      if DBCheck.CriaFm(TFmCNAB_Rem2, FmCNAB_Rem2, afmoNegarComAviso) then
      begin
        if FmCNAB_Rem2.CarregaDadosNoForm(CNAB_Cfg, SeqArq, Lote, SQL_HEADE_ARQ,
        SQL_HEADE_LOT, SQL_ITENS_LOT, [ftInteger], ['NUM_LOT']) then
          FmCNAB_Rem2.ShowModal;
        FmCNAB_Rem2.Destroy;
      end;
    end;
    400:
    begin
      if QrTedCRemCabCNAB_Cfg.Value = 0 then Exit;
      //HoraI := Now();

      Screen.Cursor := crHourGlass;
      //
      // N�o precisa de confer�ncia de cliente interno ?
      //
      if DmBco.QrCNAB_CfgCedNome.Value <> '' then
        NomeCedente := DmBco.QrCNAB_CfgCedNome.Value
      else NomeCedente := CedenteNome;
      //
      if QrTedCRemItsNOMECLI.Value <> '' then
        NomeSacador := QrTedCRemItsNOMECLI.Value
      else
        NomeSacador := SacadorNome;
      //
      if QrTedCRemCabSeqArq.Value > 0 then
      begin
        if Application.MessageBox(PChar('Este lote j� possui um n�mero sequencial ' +
        'de remessa de arquivo! Ser� utilizado o mesmo n�mero.' + sLineBreak +
        'Alguns bancos podem n�o aceitar o mesmo n�mero sequencial mais de uma vez!.' + sLineBreak +
        '- Caso voc� ainda n�o enviou este arquivo, desconsidere este aviso.' + sLineBreak +
        '- Caso queira usar um n�mero espec�fico informe o n�mero anterior a ele na configura��o de remessa correspondente.'+ sLineBreak+
        '- Caso queira zerar o n�mero cancele e use o menu do bot�o de lote deste formul�rio'+ sLineBreak +
        '- Caso queira continuar assim mesmo confirme esta a��o'), 'Pergunta',
        MB_YESNOCANCEL+MB_ICONWARNING) <> ID_YES then Exit;
      end;
      UCriar.RecriaTabelaLocal('CNAB_Rem', 1);
      //
      Item := 0;
      if DBCheck.CriaFm(TFmCNAB_Rem, FmCNAB_Rem, afmoNegarComAviso) then
      begin
        with FmCNAB_Rem do
        begin
          // Dados para gera��o do arquivo
          // Banco
          FBanco := DmBco.QrCNAB_CfgCedBanco.Value;
          // Lote
          FLote := QrTedCRemCabCodigo.Value;
          // N�mero de remessa do arquivo (Bradesco)
          FSeqArqRem := QrTedCRemCabSeqArq.Value;
          ////////////////// -0- R E G I S T R O    H E A D E R ////////////////////
          //dmkEd_0_010.ValueVariant := 0;
          dmkEd_0_001.ValueVariant := DmBco.QrCNAB_CfgCedBanco.Value;
          dmkEd_0_003.ValueVariant := 1;
          dmkEd_0_004.ValueVariant := 'COBRANCA';
          dmkEd_0_005.ValueVariant := 1;
          dmkEd_0_006.ValueVariant := 'REMESSA';
          dmkEd_0_020.ValueVariant := DmBco.QrCNAB_CfgCedAgencia.Value;
          dmkEd_0_021.ValueVariant := DmBco.QrCNAB_CfgCedConta.Value;
          dmkEd_0_022.ValueVariant := DmBco.QrCNAB_CfgCedDAC_A.Value;
          dmkEd_0_023.ValueVariant := DmBco.QrCNAB_CfgCedDAC_C.Value;
          dmkEd_0_024.ValueVariant := DmBco.QrCNAB_CfgCedDAC_AC.Value;
          dmkEd_0_402.ValueVariant := NomeCedente;
          dmkEd_0_410.ValueVariant := DmBco.QrCNAB_CfgCodEmprBco.Value;
          dmkEd_0_699.ValueVariant := DmBco.QrCNAB_CfgCodOculto.Value;
          dmkEd_0_990.ValueVariant := (*QrProtocoPak*)QrTedCRemCabDataG.Value;
          //
          ///////////////// -1- R E G I S T R O    D E T A L H E ///////////////////

          JurosPerc := DmBco.QrCNAB_CfgJurosPerc.Value;
          MultaPerc := DmBco.QrCNAB_CfgMultaPerc.Value;

          QrTedCRemIts.First;
          while not QrTedCRemIts.Eof do
          begin
            inc(Item, 1);
            g := Grade_1;
            n := AddLinha(FLinG1, Grade_1);
            //
            JurosValr := Round(QrTedCRemItsCredito.Value * JurosPerc / 30) / 100;
            (*  Parei Aqui Fazer!!!
            if QrTedCRemItsMoraDiaVal.Value > JurosValr then
            JurosValr := QrTedCRemItsMoraDiaVal.Value;
            *)
            //
            MultaValr := Round(QrTedCRemItsCredito.Value * MultaPerc) / 100;
            //
            DataMora  := QrTedCRemItsVencimento.Value + DmBco.QrCNAB_CfgJurosDias.Value;
            //
            AddCampo(g, n, F1_000, '0000'    , Item);
            AddCampo(g, n, F1_011, '0'       , 1);
            AddCampo(g, n, F1_400, '0'       , CedenteTipo);
            AddCampo(g, n, F1_401, 'CNPJ'    , CedenteCNPJ);
            AddCampo(g, n, F1_020, '0000'    , DmBco.QrCNAB_CfgCedAgencia.Value);
            AddCampo(g, n, F1_021, ''        , DmBco.QrCNAB_CfgCedConta.Value);
            AddCampo(g, n, F1_022, ''        , DmBco.QrCNAB_CfgCedDAC_A.Value);
            AddCampo(g, n, F1_023, ''        , DmBco.QrCNAB_CfgCedDAC_C.Value);
            AddCampo(g, n, F1_024, ''        , DmBco.QrCNAB_CfgCedDAC_AC.Value);
            AddCampo(g, n, F1_506, '0'       , QrTedCRemItsFatParcela.Value);
            AddCampo(g, n, F1_501, '0'       , QrTedCRemItsFatParcela.Value);
            AddCampo(g, n, F1_509, ''        , DmBco.QrCNAB_CfgCartNum.Value);
            AddCampo(g, n, F1_508, ''        , DmBco.QrCNAB_CfgCartCod.Value);
            AddCampo(g, n, F1_504, ''        , '001'); // Minha ocorr�ncia para Remessa (Registro do t�tulo)
            AddCampo(g, n, F1_502, ''        , QrTedCRemItsDuplicata.Value);
            AddCampo(g, n, F1_580, 'dd/mm/yy', QrTedCRemItsVencimento.Value);
            AddCampo(g, n, F1_550, '0.00'    , QrTedCRemItsCredito.Value);
            AddCampo(g, n, F1_001, '000'     , DmBco.QrCNAB_CfgCedBanco.Value);
            AddCampo(g, n, F1_507, ''        , DmBco.QrCNAB_CfgEspecieTit.Value);
            AddCampo(g, n, F1_520, '0'       , DmBco.QrCNAB_CfgAceiteTit.Value);
            AddCampo(g, n, F1_583, 'dd/mm/yy', QrTedCRemItsData.Value);

            //Protesto - C�digo e dias para o banco 237
            //Pois � junto na Instru��o de cobran�a 1 e 2
            if DmBco.QrCNAB_CfgCedBanco.Value = 237 then
            begin
              if (Length(DmBco.QrCNAB_CfgInstrCobr1.Value) = 0) and
                (Length(DmBco.QrCNAB_CfgInstrCobr2.Value) = 0) then
              begin
                AddCampo(g, n, F1_701, ''        , DmBco.QrCNAB_CfgProtesCod.Value);
                AddCampo(g, n, F1_702, ''        , DmBco.QrCNAB_CfgProtesDds.Value);
              end else
              begin
                AddCampo(g, n, F1_701, ''        , DmBco.QrCNAB_CfgInstrCobr1.Value);
                AddCampo(g, n, F1_702, ''        , DmBco.QrCNAB_CfgInstrCobr2.Value);
              end;
            end;
            AddCampo(g, n, F1_950, '00'      , DmBco.QrCNAB_CfgInstrDias.Value);
            AddCampo(g, n, F1_572, '0.00'    , JurosValr);
            AddCampo(g, n, F1_573, '0.00'    , MultaValr);
            AddCampo(g, n, F1_574, '0.00'    , JurosPerc);
            AddCampo(g, n, F1_575, '0.00'    , MultaPerc);
            AddCampo(g, n, F1_576, '0'       , DmBco.QrCNAB_CfgJurosTipo.Value);
            AddCampo(g, n, F1_577, '0'       , DmBco.QrCNAB_CfgMultaTipo.Value);
            //
(*
            DmBco.QrAddr3.Close;
            DmBco.QrAddr3.Params[0].AsInteger := QrTedCRemItsCliente.Value;
            DmBco.QrAddr3. Open;
*)
            CEPSacado_ := Geral.SoNumero_TT(QrTedCRemItsSACADO_CEP_TXT.Value);
            //
            AddCampo(g, n, F1_801, '0'       , QrTedCRemItsSACADO_TIPO.Value);
            AddCampo(g, n, F1_802, 'CNPJ'    , QrTedCRemItsSACADO_CNPJ.Value);
            AddCampo(g, n, F1_803, ''        , QrTedCRemItsSACADO_NOME.Value);
            AddCampo(g, n, F1_811, ''        , (*DmBco.QrAddr3NOMELOGRAD.Value)*)'');
            AddCampo(g, n, F1_812, ''        , QrTedCRemItsSACADO_RUA.Value);
            AddCampo(g, n, F1_813, ''        , QrTedCRemItsSACADO_NUMERO_TXT.Value);
            AddCampo(g, n, F1_814, ''        , QrTedCRemItsSACADO_COMPL.Value);
            AddCampo(g, n, F1_805, ''        , QrTedCRemItsSACADO_BAIRRO.Value);
            AddCampo(g, n, F1_806, ''        , CEPSacado_);
            AddCampo(g, n, F1_807, ''        , QrTedCRemItsSACADO_CIDADE.Value);
            AddCampo(g, n, F1_808, ''        , QrTedCRemItsSACADO_xUF.Value);
            AddCampo(g, n, F1_853, ''        , NomeSacador);
            AddCampo(g, n, F1_584, 'dd/mm/yy', DataMora);
            AddCampo(g, n, F1_621, '0'       , DmBco.QrCNAB_CfgQuemPrint.Value);
            AddCampo(g, n, F1_639, '0'       , DmBco.QrCNAB_Cfg_237Mens1.Value);
            //
            //Obter os dados do sacador avalista para o banco 237
            //Pois � junto na mensagem 2
            if DmBco.QrCNAB_CfgCedBanco.Value = 237 then
            begin
              if Length(DmBco.QrCNAB_Cfg_237Mens2.Value) = 0 then
              begin
                if Length(QrTedCRemItsDOCUMCLI.Value) = 14 then
                begin
                  AVALISTA := Geral.SoNumero_TT(QrTedCRemItsDOCUMCLI.Value);
                  AVALISTA := FormatFloat('000000000000000', Geral.DMV(AVALISTA));
                end else
                if Length(QrTedCRemItsDOCUMCLI.Value) = 11 then
                begin
                  AVALISTA := Copy(QrTedCRemItsDOCUMCLI.Value, 10, 2);
                  AVALISTA := AVALISTA + '0000';
                  AVALISTA := AVALISTA + Copy(QrTedCRemItsDOCUMCLI.Value, 1, 9);
                end else
                  AVALISTA := '';
                //
                if Length(AVALISTA) > 0 then
                begin
                  AVALISTA := AVALISTA + '  ';
                  if Length(QrTedCRemItsNOMECLI.Value) > 43 then
                    AVALISTA := AVALISTA + Copy(QrTedCRemItsNOMECLI.Value, 1, 43)
                  else
                    AVALISTA := AVALISTA + QrTedCRemItsNOMECLI.Value;
                end;
                AddCampo(g, n, F1_640, '0'       , AVALISTA);
              end else
                AddCampo(g, n, F1_640, '0'       , DmBco.QrCNAB_Cfg_237Mens2.Value);
            end else
              AddCampo(g, n, F1_640, '0'       , DmBco.QrCNAB_Cfg_237Mens2.Value);

            //  I N F O R M A T I V O S
            // Endere�o completo do logradouro

            //O banco 237 coloca a cidade e UF junto no endere�o
            if DmBco.QrCNAB_CfgCedBanco.Value = 237 then
              AddCampo(g, n, F1_804, ''        , EnderecoSacado() + ' ' +
                QrTedCRemItsSACADO_CIDADE.Value + ' ' + QrTedCRemItsSACADO_xUF.Value)
            else
              AddCampo(g, n, F1_804, ''        , EnderecoSacado());

            //  N � O   I M P L E M E N T A D O S
            // Data limite para desconto (n�o tem)
            AddCampo(g, n, F1_586, ''        , '00/00/00');
            // Valor do desconto
            AddCampo(g, n, F1_552, ''        , 0);
            // Valor do IOF
            AddCampo(g, n, F1_569, ''        , 0);
            // Valor do abatimento
            AddCampo(g, n, F1_551, ''        , 0);
            // Valor do deconto bonifica��o
            AddCampo(g, n, F1_558, ''        , 0);


        //
        ///////////////// -5- R E G I S T R O    D E T A L H E ///////////////////
            if ((DmBco.QrCNAB_CfgEnvEmeio.Value = 1) and
            (DmBco.QrAddr3EMail.Value <> '')) or
            (QrTedCRemItsCliente.Value <> 0) then
            begin
              // � o mesmo do registro 1
              //inc(Item, 1);
              g := Grade_5;
              n := AddLinha(FLinG5, Grade_5);
              AddCampo(g, n, F5_000, '0000' , Item);
              AddCampo(g, n, F5_011, '0'      , 5);
              //AddCampo(g, n, F5_000, '000000' , Linha);
              //AddCampo(g, n, F5_011, '0'      , 5);
              AddCampo(g, n, F5_815, ''       , DmBco.QrAddr3EMail.Value);
              //
              DmBco.QrAddr3.Close;
              if QrTedCRemItsCliente.Value <> 0 then
              begin
                DmBco.QrAddr3.Params[0].AsInteger := QrTedCRemItsCliente.Value;
                UMyMod.AbreQuery(DmBco.QrAddr3);

                CEPSacador       := Geral.SoNumero_TT(DmBco.QrAddr3ECEP_TXT.Value);
                Addr3_Tipo       := DmBco.QrAddr3Tipo.Value;
                Addr3_CNPJ       := DmBco.QrAddr3CNPJ_CPF.Value;
                Addr3_NOMEENT    := DmBco.QrAddr3NOMEENT.Value;
                Addr3_NOMELOGRAD := DmBco.QrAddr3NOMELOGRAD.Value;
                Addr3_RUA        := DmBco.QrAddr3RUA.Value;
                Addr3_NUMERO_TXT := DmBco.QrAddr3NUMERO_TXT.Value;
                Addr3_COMPL      := DmBco.QrAddr3COMPL.Value;
                Addr3_BAIRRO     := DmBco.QrAddr3BAIRRO.Value;
                Addr3_CIDADE     := DmBco.QrAddr3CIDADE.Value;
                Addr3_NOMEUF     := DmBco.QrAddr3NOMEUF.Value;
              end else begin
                CEPSacador       := '';
                Addr3_Tipo       := -1;
                Addr3_CNPJ       := '';
                Addr3_NOMEENT    := '';
                Addr3_NOMELOGRAD := '';
                Addr3_RUA        := '';
                Addr3_NUMERO_TXT := '';
                Addr3_COMPL      := '';
                Addr3_BAIRRO     := '';
                Addr3_CIDADE     := '';
                Addr3_NOMEUF     := '';
              end;
              //
              AddCampo(g, n, F5_851, '0'      , Addr3_Tipo);
              AddCampo(g, n, F5_852, 'CNPJ'   , Addr3_CNPJ);
              AddCampo(g, n, F5_861, ''       , Addr3_NOMELOGRAD);
              AddCampo(g, n, F5_862, ''       , Addr3_RUA);
              AddCampo(g, n, F5_863, ''       , Addr3_NUMERO_TXT);
              AddCampo(g, n, F5_864, ''       , Addr3_COMPL);
              AddCampo(g, n, F5_855, ''       , Addr3_BAIRRO);
              AddCampo(g, n, F5_856, ''       , CEPSacador);
              AddCampo(g, n, F5_857, ''       , Addr3_CIDADE);
              AddCampo(g, n, F5_858, ''       , Addr3_NOMEUF);
              //
              //  I N F O R M A T I V O S
              // Endere�o completo do logradouro
              AddCampo(g, n, F5_854, ''        , Endereco());
            end;
            (*QrProtPakIts*)QrTedCRemIts.Next;
          end;
          (*
          end;
          *)
          (* Parei Aqui
          HoraF := Now();
          dmkEdHoraF.ValueVariant := HoraF;
          dmkEdTempo.ValueVariant := (HoraF - HoraI) * 24 * 60 * 60;
          *)
          Screen.Cursor := crDefault;
          ShowModal;
          Destroy;
          // Parei Aqui Fazer!!!
          //ReopenProtocoPak((*QrProtocoPak*)QrTedCRemCabControle.Value);
        end;
      end;
    end;
  end;
  Screen.Cursor := crDefault;
end;
}

procedure TFmTedCRemCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType    := stLok;
  FSeq               := 0;
  FMaxReg            := 0;
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  GBEdita.Align      := alClient;
  PageControl2.Align := alClient;
  //
  PageControl2.ActivePageIndex := 0;
  //
  CriaOForm;
  PageControl1.ActivePageIndex := 0;
  UMyMod.AbreQuery(QrTedCCfgCab, Dmod.MyDB);
end;

procedure TFmTedCRemCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTedCRemCabCodigo.Value,LaRegistro.Caption);
end;

procedure TFmTedCRemCab.SbImprimeClick(Sender: TObject);
begin
  if (QrTedCRemIts.State <> dsInactive) and (QrTedCRemCab.RecordCount > 0) then
    MyObjects.frxMostra(frxTED_CHQUE_001_01, 'Remessa TED Cheque');
end;

procedure TFmTedCRemCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTedCRemCab.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmTedCRemCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if FRegistros <> nil then
    FRegistros.Free;
  //
  if FLista <> nil then
    FLista.Free;
  //
  VAR_MARCA := QrTedCRemCabCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTedCRemCab.QrEmitCalcFields(DataSet: TDataSet);
begin
  if Length(QrEmitCNPJCPF.Value) = 15 then
    QrEmitDOCUM.Value := Copy(QrEmitCNPJCPF.Value, 2)
  else
    QrEmitDOCUM.Value := QrEmitCNPJCPF.Value;
  //
  case Length(QrEmitDOCUM.Value) of
    11: QrEmitTIPO.Value := 1;
    14: QrEmitTIPO.Value := 0;
    else QrEmitTIPO.Value := -1;
  end;
end;

procedure TFmTedCRemCab.QrLctsCalcFields(DataSet: TDataSet);
begin
  QrLctsSEQ.Value := QrLcts.RecNo;
end;

procedure TFmTedCRemCab.QrTedCRemCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTedCRemCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'CNAB_Lot', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, stIns, 0);
end;

procedure TFmTedCRemCab.QrTedCRemCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTedCRemIts(0);
  ReopenLcts();
  //
  if QrTedCRemCabEncerrado.Value  = 1 then
  begin
    BtCheques.Enabled := False;
    BtGera.Enabled := False;
  end else
  begin
    BtCheques.Enabled := True;
    if QrTedCRemCabCodigo.Value < 1 then
      BtGera.Enabled := False
    else
      BtGera.Enabled := True;
  end;
end;

procedure TFmTedCRemCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTedCRemCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'tedcremcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTedCRemCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmTedCRemCab.GeraLinhaDetail(const Layout, Registro, Subreg: Integer;
              const Segmento: String; const SubSeg: Integer;
              var Seq: Integer; const Tabela: String): Boolean;
var
//  Valor: Variant;
  TipoDoc: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
  'SELECT CNPJCPF, Emitente ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0301,
  'AND Controle=' + Geral.FF0(QrTedCRemItsLctCtrl.Value),
  'AND Sub=' + Geral.FF0(QrTedCRemItsLctSub.Value),
  'AND FatParcela=' + Geral.FF0(QrTedCRemItsLctFatParc.Value),
  '']);
  if QrEmit.RecordCount = 0 then
    TipoDoc := -1
  else
    TipoDoc := QrEmitTIPO.Value;
  // Limpar lista
  TedC_Ax.CriaLista(Layout, Registro, SubReg, Segmento, SubSeg, FReg,
    FLista, FPosAnt, FTxt1);
  //
  //////////////////////////////////////////////////////////////////////////////
  // Passa TList de registros(C�digo e valor) com informa��es que podem ser
  // usadas. (A funcionalidade de cada c�digo encontra-se na tabela "tedccfgsys".
  //////////////////////////////////////////////////////////////////////////////
  ///
  // 422: C�digo da ocorr�ncia (Remessa/Retorno)
  TedC_Ax.AddFld(FReg, FLista, 101, QrTedCRemItsOcorrBco.Value);
  // 422: Uso da empresa (Hexadecimal: lct.Controle + lct.Sub + lct.FatNum + lct.FatParcela)
  TedC_Ax.AddFld(FReg, FLista, 102, Gera_ID_HEXADECIMAL());
  // 422: Data do evento
  TedC_Ax.AddFld(FReg, FLista, 104, QrTedCRemCabDataG.Value);
  // 422: Identifica��o do cheque (CMC7)
  TedC_Ax.AddFld(FReg, FLista, 105, QrTedCRemItsCMC7.Value);
  // 422: Emitente do cheque - Tipo de inscri��o
  TedC_Ax.AddFld(FReg, FLista, 106, TipoDoc);
  // 422: Emitente do cheque - N�mero da inscri��o
  TedC_Ax.AddFld(FReg, FLista, 107, QrEmitDOCUM.Value);
  // 422: Emitente do cheque - Emitente do cheque - Nome
  TedC_Ax.AddFld(FReg, FLista, 108, QrEmitEmitente.Value);
  // 422: Cheque - Valor (Remessa)
  TedC_Ax.AddFld(FReg, FLista, 109, QrTedCRemItsValor.Value);
  // 422: Cheque - Data boa para compensa��o (Remessa)
  TedC_Ax.AddFld(FReg, FLista, 110, QrTedCRemItsBomPara.Value);
  // 422: Seu Numero > (TedCRemIts.Controle)
  TedC_Ax.AddFld(FReg, FLista, 103, QrTedCRemItsControle.Value);

////////////////////////////////////////////////////////////////////////////////
{         Parei aqui! Ver o que fazer! O Mijol�rio N�o usa?                    }
{}// 422: C�digo do cedente para transfer�ncia                                {}
{}TedC_Ax.AddFld(FReg, FLista, 117, '');                                        {}
{}// 422: Data da opera��o cess�o                                             {}
{}TedC_Ax.AddFld(FReg, FLista, 118, EncodeDate(1899, 12, 30));                  {}
{}// 422: N�mero do contrato                                                  {}
{}TedC_Ax.AddFld(FReg, FLista, 119, '');                                        {}
{}// 422: N�mero da parcela                                                   {}
{}TedC_Ax.AddFld(FReg, FLista, 120, 0);                                         {}
{}// 422: Data emiss�o                                                        {}
{}TedC_Ax.AddFld(FReg, FLista, 121, EncodeDate(1899, 12, 30));                  {}
{         FIM Parei aqui! Ver o que fazer! O Mijol�rio N�o usa?                }
////////////////////////////////////////////////////////////////////////////////

  // 422: N�mero sequencial do registro
  TedC_Ax.AddFld(FReg, FLista, 099, QrTedCRemIts.RecNo + 1);

  Result := TedC_Ax.GeraLinha(Layout, Registro, Subreg, Segmento, SubSeg,
  FLinha, FContinua, FPosIni, FPosFim, FPosTam, FTamInt, FTamFlu, FTxt1, FTxt2,
  FPosAnt, Memo1, FReg, FLista, Tabela, Seq);
end;

function TFmTedCRemCab.GeraLinhaHeader(const Layout, Registro, Subreg: Integer;
              const Segmento: String; const SubSeg: Integer;
              var Seq: Integer; const Tabela: String): Boolean;
begin
  // Limpar lista
  TedC_Ax.CriaLista(Layout, Registro, SubReg, Segmento, SubSeg, FReg,
    FLista, FPosAnt, FTxt1);
  //
  //////////////////////////////////////////////////////////////////////////////
  // Passa TList de registros(C�digo e valor) com informa��es que podem ser
  // usadas. (A funcionalidade de cada c�digo encontra-se na tabela "tedccfgsys".
  //////////////////////////////////////////////////////////////////////////////
  ///
  // 422: Tipo de inscri��o CNPJ/CPF
  TedC_Ax.AddFld(FReg, FLista, 008, QrTedCRemCabTIPO_ENT.Value);
  // 422: N�mero da inscri��o CNPJ/CPF
  TedC_Ax.AddFld(FReg, FLista, 009, QrTedCRemCabDOC_EMPRESA.Value);
  // 422: Nome da empresa
  TedC_Ax.AddFld(FReg, FLista, 010, QrTedCRemCabNO_EMPRESA.Value);
  // 422: Data da grava��o do arquivo (gera��o do lote no sistema)
  TedC_Ax.AddFld(FReg, FLista, 090, QrTedCRemCabDataG.Value);
  // 422: N�mero do arquivo (C�digo do lote de envio)
  TedC_Ax.AddFld(FReg, FLista, 091, QrTedCRemCabCodUsu.Value);
  //
  Result := TedC_Ax.GeraLinha(Layout, Registro, Subreg, Segmento, SubSeg,
  FLinha, FContinua, FPosIni, FPosFim, FPosTam, FTamInt, FTamFlu, FTxt1, FTxt2,
  FPosAnt, Memo1, FReg, FLista, Tabela, Seq);
end;

function TFmTedCRemCab.GeraLinhaTrailler(const Layout, Registro, Subreg: Integer;
              const Segmento: String; const SubSeg: Integer;
              var Seq: Integer; const Tabela: String): Boolean;
begin
  // Limpar lista
  TedC_Ax.CriaLista(Layout, Registro, SubReg, Segmento, SubSeg, FReg, FLista,
    FPosAnt, FTxt1);
  //
  //////////////////////////////////////////////////////////////////////////////
  // Passa TList de registros(C�digo e valor) com informa��es que podem ser
  // usadas. (A funcionalidade de cada c�digo encontra-se na tabela "tedccfgsys".
  //////////////////////////////////////////////////////////////////////////////
  ///
  // 422: N�mero sequencial do registro
  TedC_Ax.AddFld(FReg, FLista, 099, QrTedCRemIts.RecordCount + 2);
  TedC_Ax.AddFld(FReg, FLista, 902, QrTedCRemCabCartVal.Value);
  TedC_Ax.AddFld(FReg, FLista, 903, QrTedCRemCabEntrQtd.Value);
  TedC_Ax.AddFld(FReg, FLista, 904, QrTedCRemCabEntrVal.Value);

  //
  Result := TedC_Ax.GeraLinha(Layout, Registro, Subreg, Segmento, SubSeg,
  FLinha, FContinua, FPosIni, FPosFim, FPosTam, FTamInt, FTamFlu, FTxt1, FTxt2,
  FPosAnt, Memo1, FReg, FLista, Tabela, Seq);
end;

procedure TFmTedCRemCab.GeraLns_Uno_0_Ene_1_Uno9(Item, Layout, Registro, SubReg:
  Integer; Segmento: String; SubSeg: Integer; Tabela: String);
begin
  case Item of
    0: GeraLinhaHeader(Layout, Registro, SubReg, Segmento, SubSeg, FSeqLin, Tabela);
    1:
    begin
      QrTedCRemIts.DisableControls;
      try
        QrTedCRemIts.First;
        while not QrTedCRemIts.Eof do
        begin
          GeraLinhaDetail(Layout, Registro, SubReg, Segmento, SubSeg, FSeqLin, Tabela);
          //
          QrTedCRemIts.Next;
        end;
      finally
        QrTedCRemIts.EnableControls;
      end;
    end;
    2: GeraLinhaTrailler(Layout, Registro, SubReg, Segmento, SubSeg, FSeqLin, Tabela);
  end;
end;

procedure TFmTedCRemCab.GeraTED_Uno_0_Ene_1_Uno9(Layout, SeqLay: Integer);
begin
  CriaTiposDeRegistros(Layout, SeqLay,
  [0,1,9],
  [0,0,0],
  ['','',''],
  [0,0,0],
  ['Header','Detail','Trailler']);
  //
end;

procedure TFmTedCRemCab.QrTedCRemCabBeforeClose(DataSet: TDataSet);
begin
  QrTedCRemIts.Close;
  QrLcts.Close;
  //
  BtCheques.Enabled := False;
  BtGera.Enabled := False;
end;

procedure TFmTedCRemCab.QrTedCRemCabBeforeOpen(DataSet: TDataSet);
begin
  QrTedCRemCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTedCRemCab.Reabrelote1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if Geral.MensagemBox('Confirma a reabertura do lote?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Codigo := QrTedCRemCabCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcremcab', False, [
      'Encerrado'], ['Codigo'], [0], [Codigo], True) then
    LocCod(Codigo, Codigo);  
  end;
end;

procedure TFmTedCRemCab.ReopenLcts;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, Dmod.MyDB, [
    'SELECT lan.Controle, lan.Data, lan.Vencimento, ',
    'lan.Credito, lan.Debito, lan.Descricao, lan.Sub, lan.Genero,  ',
    'lan.Cartao, lan.Sit, lan.Tipo, lan.ID_Pgto, lan.Carteira, ',
    'car.Nome NOMECARTEIRA, con.Nome NOMECONTA, ',
    'lan.CliInt ',
    'FROM ' + CO_TabLctA + ' lan',
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
    'LEFT JOIN contas con ON con.Codigo=lan.Genero ',
    'WHERE lan.FatID=' + Geral.FF0(VAR_FATID_0373),
    'AND lan.FatNum=' + Geral.FF0(QrTedCRemCabCodigo.Value),
    '']);
end;

procedure TFmTedCRemCab.ReopenTedCRemIts(LctFatParc: Integer);
begin
 {
  QrTedCRemIts.Close;
  QrTedCRemIts.Params[0].AsInteger := QrTedCRemCabCodigo.Value;
  QrTedCRemIts. Open;
 }
  UnDmkDAC_PF.AbreMySQLQuery0(QrTedCRemIts, Dmod.MyDB, [
{
  'SELECT lot.Cliente, lot.Lote, ent.MultaCodi, ent.MultaDias, ',
  'ent.MultaValr, ent.MultaPerc, ent.MultaTiVe, ent.Protestar, ',
  'ent.JuroSacado, ent.Tipo TipoCLI, ',
  'PUF, EUF, uf0.Nome UFE, uf1.Nome UFP, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI, ',
  'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCUMCLI, ',
  'IF(ent.Tipo=0, ent.CNPJ   , ent.CPF    ) CNPJCLI, ',
  'IF(ent.Tipo=0, ent.ERua   , ent.PRua   ) RuaCLI, ',
  'IF(ent.Tipo=0, ent.ENumero+0.0, ent.PNumero+0.0) NumCLI, ',
  'IF(ent.Tipo=0, ent.ECompl , ent.PCompl ) CplCLI, ',
  'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BrrCLI, ',
  'IF(ent.Tipo=0, ent.ECEP   , ent.PCEP   ) CEPCLI, ',
  'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CidCLI, ',
  'ent.Corrido, sac.CNPJ SACADO_CNPJ, ',
  'sac.Nome SACADO_NOME, sac.Rua SACADO_RUA, ',
  'sac.Numero SACADO_NUMERO, sac.Compl SACADO_COMPL, ',
  'sac.Bairro SACADO_BAIRRO, ',
  'sac.Cidade SACADO_CIDADE, sac.UF SACADO_xUF, ',
  'sac.CEP SACADO_CEP, IF(LENGTH(sac.CNPJ) >=14, 0, 1) SACADO_TIPO, ',
  'loi.FatParcela, loi.Emitente, loi.CNPJCPF, ',
  'loi.Bruto, loi.Desco, loi.Credito, ',
  'loi.DCompra, loi.Vencimento, loi.DDeposito, ',
  'loi.Duplicata, loi.Data, ',
  'sac.* ',
  ' ',
  'FROM ' + CO_TabLctA + ' loi ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum ',
  'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente ',
  'LEFT JOIN sacados   sac ON sac.CNPJ=loi.CNPJCPF ',
  'LEFT JOIN ufs       uf0 ON uf0.Codigo=ent.EUF ',
  'LEFT JOIN ufs       uf1 ON uf1.Codigo=ent.PUF ',
  'WHERE loi.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND loi.CNAB_Lot=' + FormatFloat('0', QrTedCRemCabCodigo.Value),
  'ORDER BY loi.Duplicata ',
}
  'SELECT tri.*, lct.ContaCorrente, lct.Banco, lct.Agencia, lct.Documento ',
  'FROM tedcremits tri ',
  'LEFT JOIN ' + CO_TabLctA + ' lct ON lct.Controle = tri.LctCtrl ',
  'WHERE tri.Codigo=' + Geral.FF0(QrTedCRemCabCodigo.Value),
  '']);
  //
  if LctFatParc > 0 then
    QrTedCRemIts.Locate('LctFatParc', LctFatParc, []);
end;

procedure TFmTedCRemCab.PMChequesPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsDel(Retira1, QrTedCRemIts);
  MyObjects.HabilitaMenuItemItsIns(Inclui1, QrTedCRemCab);
end;

procedure TFmTedCRemCab.PMLotPopup(Sender: TObject);
begin
  if QrTedCRemCabCodigo.Value = 0 then
  begin
    Alteraloteatual1.Enabled := False;
    Excluiloteatual1.Enabled := False;
    Reabrelote1.Enabled := False;
  end else begin
    Alteraloteatual1.Enabled := True;
    Excluiloteatual1.Enabled := QrTedCRemIts.RecordCount > 0;
    Reabrelote1.Enabled := QrTedCRemCabEncerrado.Value = 1;
  end;
end;

procedure TFmTedCRemCab.Crianovolote1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmTedCRemCab.Alteraloteatual1Click(Sender: TObject);
var
  tedcremcab : Integer;
begin
  tedcremcab := QrTedCRemCabCodigo.Value;
  if not UMyMod.SelLockY(tedcremcab, Dmod.MyDB, 'tedcremcab', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(tedcremcab, Dmod.MyDB, 'tedcremcab', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmTedCRemCab.EdTedC_CfgChange(Sender: TObject);
begin
  PnIndiretos.Visible := EdTedC_Cfg.ValueVariant <> 0;
end;

procedure TFmTedCRemCab.Excluiloteatual1Click(Sender: TObject);
var
  Atual, Proximo: Integer;
begin
  if Application.MessageBox('Confirma a exclus�o deste lote?', 'Pergunta',
  MB_YESNOCANCEL) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Atual := QrTedCRemCabCodigo.Value;
    Proximo := UMyMod.ProximoRegistro(QrTedCRemCab, 'Codigo', Atual);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM tedcremcab WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Atual;
    Dmod.QrUpd.ExecSQL;
    //
    LocCod(Atual, Proximo);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTedCRemCab.BtChequesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCheques, BtCheques);
end;

function TFmTedCRemCab.Gera_ID_HEXADECIMAL(): String;
begin
  Result :=
    IntToHex(QrTedCRemItsLctCtrl.Value, 8) +
    IntToHex(QrTedCRemItsLctSub.Value, 1) +
    IntToHex(QrTedCRemItsLctFatNum.Value, 8) +
    IntToHex(QrTedCRemItsLctFatParc.Value, 8);
end;

procedure TFmTedCRemCab.Inclui1Click(Sender: TObject);
var
  OcorrBco, MyOcorr: Integer;
  SQLType: TSQLType;
begin
  //MostraEdicao(2, stIns, 0);
  if TedC_Ax.TedChequeSelecionaOcorrencia(QrTedCRemCabBanco.Value,
  ecnabRemessa, QrTedCRemCabTamReg.Value, OcorrBco, MyOcorr, SQLType) then
  begin
    case MyOcorr of
      1: // Entrada
      begin
        if DBCheck.CriaFm(TFmTedCRemInn, FmTedCRemInn, afmoNegarComAviso) then
        begin
          FmTedCRemInn.FOcorrBco := OcorrBco;
          FmTedCRemInn.FMyOcorr := MyOcorr;
          FmTedCRemInn.FSQLType := SQLType;
          FmTedCRemInn.ShowModal;
          FmTedCRemInn.Destroy;
        end;
      end;
      2,9: // Altera��o de data boa ou baixa por instru��o
      begin
        if DBCheck.CriaFm(TFmTedCRemChg, FmTedCRemChg, afmoNegarComAviso) then
        begin
          FmTedCRemChg.FOcorrBco := OcorrBco;
          FmTedCRemChg.FMyOcorr := MyOcorr;
          FmTedCRemChg.FSQLType := SQLType;
          case MyOcorr of
            {2}CO_MYOCORR_TEDC_ALTERAR_BOM_PARA: FmTedCRemChg.PageControl1.ActivePageIndex := 0; // Altera��o de data boa
            {9}CO_MYOCORR_TEDC_INSTRUCAO_BAIXAR: FmTedCRemChg.PageControl1.ActivePageIndex := 1; // Instru��o de baixa
          end;
          FmTedCRemChg.ShowModal;
          FmTedCRemChg.Destroy;
        end;
      end;
   end;
   ReopenTedCRemIts(QrTedCRemItsLctFatParc.Value);
  end;
end;

procedure TFmTedCRemCab.QrTedCRemCabCalcFields(DataSet: TDataSet);
begin
  if QrTedCRemCabDataG.Value = 0 then
    QrTedCRemCabMyDataG.Value := '00/00/0000'
  else QrTedCRemCabMyDataG.Value := Geral.FDT(QrTedCRemCabDataG.Value, 2);
  if QrTedCRemCabDataS.Value = 0 then
    QrTedCRemCabMyDataS.Value := '00/00/0000'
  else QrTedCRemCabMyDataS.Value := Geral.FDT(QrTedCRemCabDataS.Value, 2);
end;

procedure TFmTedCRemCab.BtDesiste3Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmTedCRemCab.Retira1Click(Sender: TObject);
var
  TedRem_Its, TedRem_Sit, Controle, Sub, FatID, FatParcela: Integer;
  FatNum: Double;
  //CamposDel, Dta: String;
begin
  // Ver se o banco j� n�o recebeu alguma instru��o antes de retirar!
  if (QrTedCRemItsLoReCaPrev.Value <> 0)
  or (QrTedCRemItsLoReCaProc.Value <> 0) then
  begin
    Geral.MensagemBox(
    'O item n�o pode ser retirado do lote porque j� foi aceito pelo banco!',
    'Aviso', MB_OK+MB_ICONWARNING);
    //
    Exit;
  end;
  TedRem_Its := QrTedCRemItsLctCtrAnt.Value;
  TedRem_Sit := QrTedCRemItsLctSitAnt.Value;
  //
  Controle := QrTedCRemItsLctCtrl.Value;
  Sub := QrTedCRemItsLctSub.Value;
  FatID := QrTedCRemItsLctFatID.Value;
  FatNum := QrTedCRemItsLctFatNum.Value;
  FatParcela := QrTedCRemItsLctFatParc.Value;
  //
  if Geral.MensagemBox('Confirma a retirada do cheque selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
    'TedRem_Sit', 'TedRem_Its'], [
    'Controle', 'Sub',
    'FatID', 'FatNum', 'FatParcela'], [
    TedRem_Sit, TedRem_Its], [
    Controle, Sub,
    FatID, FatNum, FatParcela], True) then
    begin
      //N�o excluir da base de dados! Mover para a tabela TedCRemDel
      if UMyMod.MoveRegistroEntreTabelas(Dmod.QrUpd, 'tedcremits', 'tedcremdel',
      ['Controle'], ['='], [QrTedCRemItsControle.Value], amrInsAndDel) then
        LocCod(QrTedCRemCabCodigo.Value, QrTedCRemCabCodigo.Value);
    end;
  end;
end;

procedure TFmTedCRemCab.Memo1Change(Sender: TObject);
begin
  Edit1.Text := IntToStr(Memo1.Lines.Count);
end;

procedure TFmTedCRemCab.QrTedCRemItsAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCheque, Dmod.MyDB, [
  'SELECT Banco, Agencia, ContaCorrente, ',
  'Documento, Praca, Credito, CNPJCPF, Emitente ',
  'FROM ' + CO_TabLctA,
  'WHERE Controle=' + Geral.FF0(QrTedCRemItsLctCtrl.Value),
  'AND Sub=' + Geral.FF0(QrTedCRemItsLctSub.Value),
  'AND FatID=' + Geral.FF0(QrTedCRemItsLctFatID.Value),
  'AND FatNum=' + Geral.FF0(QrTedCRemItsLctFatNum.Value),
  'AND FatParcela=' + Geral.FF0(QrTedCRemItsLctFatParc.Value),
  '']);
end;

procedure TFmTedCRemCab.QrTedCRemItsBeforeClose(DataSet: TDataSet);
begin
  QrCheque.Close;
end;

procedure TFmTedCRemCab.QrTedCRemItsCalcFields(DataSet: TDataSet);
begin
  QrTedCRemItsOCORR_TXT.Value :=
    TedC_Ax.DescricaoDeMyOcorrTedC(QrTedCRemItsMyOcorr.Value);
end;

end.

