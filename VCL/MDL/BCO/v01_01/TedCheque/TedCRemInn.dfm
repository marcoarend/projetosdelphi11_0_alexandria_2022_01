object FmTedCRemInn: TFmTedCRemInn
  Left = 339
  Top = 185
  Caption = 'TED-CHQUE-102 :: Adi'#231#227'o de Cheque em Lote TED'
  ClientHeight = 321
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 390
        Height = 32
        Caption = 'Adi'#231#227'o de Cheque em Lote TED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 390
        Height = 32
        Caption = 'Adi'#231#227'o de Cheque em Lote TED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 390
        Height = 32
        Caption = 'Adi'#231#227'o de Cheque em Lote TED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 159
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 159
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 159
        Align = alClient
        Caption = ' Localiza'#231#227'o do cheque pela captura CMC7: '
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 50
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label36: TLabel
            Left = 8
            Top = 8
            Width = 143
            Height = 13
            Caption = 'Leitura pela banda magn'#233'tica:'
          end
          object Label210: TLabel
            Left = 324
            Top = 8
            Width = 24
            Height = 13
            Caption = 'Tipo:'
            Enabled = False
          end
          object Label37: TLabel
            Left = 348
            Top = 8
            Width = 33
            Height = 13
            Caption = 'Comp.:'
            Enabled = False
          end
          object Label38: TLabel
            Left = 384
            Top = 8
            Width = 34
            Height = 13
            Caption = 'Banco:'
            Enabled = False
          end
          object Label39: TLabel
            Left = 420
            Top = 8
            Width = 42
            Height = 13
            Caption = 'Ag'#234'ncia:'
            Enabled = False
          end
          object Label40: TLabel
            Left = 464
            Top = 8
            Width = 73
            Height = 13
            Caption = 'Conta corrente:'
            Enabled = False
          end
          object Label41: TLabel
            Left = 544
            Top = 8
            Width = 42
            Height = 13
            Caption = 'N'#186' cheq:'
            Enabled = False
          end
          object SpeedButton1: TSpeedButton
            Left = 296
            Top = 24
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object Label1: TLabel
            Left = 596
            Top = 8
            Width = 32
            Height = 13
            Caption = 'CMC7:'
            Enabled = False
          end
          object EdBanda: TdmkEdit
            Left = 8
            Top = 24
            Width = 285
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 34
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdBandaChange
            OnKeyDown = EdBandaKeyDown
          end
          object EdCMC_7: TdmkEdit
            Left = 596
            Top = 24
            Width = 177
            Height = 21
            Alignment = taCenter
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdCMC_7Change
          end
          object EdTipific: TdmkEdit
            Left = 324
            Top = 24
            Width = 21
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdRegiaoCompe: TdmkEdit
            Left = 348
            Top = 24
            Width = 33
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBanco: TdmkEdit
            Left = 384
            Top = 24
            Width = 33
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdAgencia: TdmkEdit
            Left = 420
            Top = 24
            Width = 41
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 4
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdConta: TdmkEdit
            Left = 464
            Top = 24
            Width = 77
            Height = 21
            Alignment = taCenter
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCheque: TdmkEdit
            Left = 544
            Top = 24
            Width = 48
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 65
          Width = 780
          Height = 92
          Align = alClient
          DataSource = DsLct
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Banco'
              Width = 37
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag'#234'nc.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ContaCorrente'
              Title.Caption = 'Conta corrente'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Title.Caption = 'Cheque'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 228
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJCPF'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 207
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 251
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Banco, Agencia, ContaCorrente, Documento,'
      'Credito, Data, DDeposito, CNPJCPF, Emitente,'
      'TedRem_Sit, TedRem_Its, Controle, Sub, '
      'FatID, FatNum, FatParcela, Depositado'
      'FROM lct0001a'
      'WHERE FatID=301'
      'AND Banco=1'
      'AND Agencia=3767'
      'AND ContaCorrente="1300065617"'
      'AND Documento=850276'
      'ORDER BY Vencimento DESC'
      '')
    Left = 472
    Top = 12
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrLctAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrLctContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000'
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDDeposito: TDateField
      FieldName = 'DDeposito'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrLctEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLctTedRem_Sit: TSmallintField
      FieldName = 'TedRem_Sit'
    end
    object QrLctTedRem_Its: TIntegerField
      FieldName = 'TedRem_Its'
    end
    object QrLctDepositado: TSmallintField
      FieldName = 'Depositado'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 500
    Top = 12
  end
end
