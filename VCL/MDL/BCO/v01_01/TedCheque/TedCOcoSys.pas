unit TedCOcoSys;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkRadioGroup, ComCtrls,
  dmkImage, dmkDBLookupComboBox, dmkEditCB, Menus, Variants, Grids, DBGrids,
  UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TFmTedCOcoSys = class(TForm)
    PainelDados: TPanel;
    DsTedCOcoSys: TDataSource;
    QrTedCOcoSys: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrTedCOcoSysCodigo: TIntegerField;
    QrTedCOcoSysNome: TWideStringField;
    QrTedCOcoSysLk: TIntegerField;
    QrTedCOcoSysDataCad: TDateField;
    QrTedCOcoSysDataAlt: TDateField;
    QrTedCOcoSysUserCad: TIntegerField;
    QrTedCOcoSysUserAlt: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtIts: TBitBtn;
    BtCab: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    Label8: TLabel;
    EdPlaGen: TdmkEditCB;
    CBPlaGen: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    BtContas: TBitBtn;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    ItsExclui1: TMenuItem;
    N1: TMenuItem;
    Nomeiagrupo1: TMenuItem;
    RGRemRet: TdmkRadioGroup;
    QrTedCOcoSysRemRet: TSmallintField;
    QrTedCOcoSysNO_PLAGEN: TWideStringField;
    QrTedCOcoSysPlaGen: TIntegerField;
    QrTedCOcoSysAlterWeb: TSmallintField;
    QrTedCOcoSysAtivo: TSmallintField;
    QrTedCOcoTxa: TmySQLQuery;
    DsTedCOcoTxa: TDataSource;
    QrTedCOcoTxaNO_BANCO: TWideStringField;
    QrTedCOcoTxaCodigo: TIntegerField;
    QrTedCOcoTxaBanco: TIntegerField;
    QrTedCOcoTxaCedente: TWideStringField;
    QrTedCOcoTxaTxaFat: TFloatField;
    QrTedCOcoTxaTxaTip: TSmallintField;
    DBGrid1: TDBGrid;
    QrTedCOcoTxaNO_TxaTip: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTedCOcoSysAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTedCOcoSysBeforeOpen(DataSet: TDataSet);
    procedure BtCabClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrTedCOcoSysBeforeClose(DataSet: TDataSet);
    procedure QrTedCOcoSysAfterScroll(DataSet: TDataSet);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFmTedCOcoTxa(SQLType: TSQLType);
  public
    { Public declarations }
    procedure ReopenTedCOcoTxa(Banco: Integer; Cedente: String);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmTedCOcoSys: TFmTedCOcoSys;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, Contas, TedCOcoTxa;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTedCOcoSys.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTedCOcoSys.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTedCOcoSysCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTedCOcoSys.DefParams;
begin
  VAR_GOTOTABELA := 'tedcocosys';
  VAR_GOTOMYSQLTABLE := QrTedCOcoSys;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cnt.Nome NO_PLAGEN, ocb.*');
  VAR_SQLx.Add('FROM tedcocosys ocb');
  VAR_SQLx.Add('LEFT JOIN contas cnt ON cnt.Codigo=ocb.PlaGen');
  VAR_SQLx.Add('WHERE ocb.Codigo <> 0');
  //
  VAR_SQL1.Add('AND ocb.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND ocb.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ocb.Nome Like :P0');
  //
end;

procedure TFmTedCOcoSys.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      if SQLType = stIns then
      begin
{
        EdCodigo.ValueVariant    := FormatFloat(FFormatFloat, Codigo);
        EdNome.ValueVariant      := '';
        EdBase.ValueVariant      := 0;
        RGFormaCNAB.ItemIndex    := -1;
        RGEnvio.ItemIndex        := -1;
        EdMovimento.ValueVariant := 0;
        EdMovim_TXT.ValueVariant := '';
        //
}
      end else begin
        EdCodigo.ValueVariant    := QrTedCOcoSysCodigo.Value;
        EdNome.ValueVariant      := QrTedCOcoSysNome.Value;
        RGRemret.ItemIndex       := QrTedCOcoSysRemRet.Value;
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmTedCOcoSys.MostraFmTedCOcoTxa(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmTedCOcoTxa, FmTedCOcoTxa, afmoNegarComAviso) then
  begin
    FmTedCOcoTxa.ImgTipo.SQLType := SQLType;
    FmTedCOcoTxa.EdCodigo.ValueVariant := QrTedCOcoSysCodigo.Value;
    FmTedCOcoTxa.Ednome.Text := QrTedCOcoSysNome.Value;
    if SQLType = stUpd then
    begin
      FmTedCOcoTxa.EdOldBco.ValueVariant := QrTedCOcoTxaBanco.Value;
      FmTedCOcoTxa.EdOldCeden.Text := QrTedCOcoTxaCedente.Value;
      //
      FmTedCOcoTxa.EdBanco.ValueVariant := QrTedCOcoTxaBanco.Value;
      FmTedCOcoTxa.EdCedente.Text := QrTedCOcoTxaCedente.Value;
      FmTedCOcoTxa.RGTxaTip.ItemIndex := QrTedCOcoTxaTxaTip.Value;
      FmTedCOcoTxa.EdTxaFat.ValueVariant := QrTedCOcoTxaTxaFat.Value;
      //
    end;
    FmTedCOcoTxa.ShowModal;
    FmTedCOcoTxa.Destroy;
  end;
end;

procedure TFmTedCOcoSys.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTedCOcoSys);
  //MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTedCOcoSys, QrTedCOcoTxa);
end;

procedure TFmTedCOcoSys.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrTedCOcoSys);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrTedCOcoTxa);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrTedCOcoTxa);
  //
  MyObjects.HabilitaMenuItemItsDel(Nomeiagrupo1, QrTedCOcoTxa);

end;

procedure TFmTedCOcoSys.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrTedCOcoSys, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'tedcocosys');
end;

procedure TFmTedCOcoSys.CabInclui1Click(Sender: TObject);
begin
  // Usu�rio n�o pode cadastrar pos � controlado por CO_MYOCORR_TEDC_...
  Screen.Cursor := crHourGlass;
  DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'tedcocosys', 'tedcocosys',
    nil, False, False, LaAviso1, LaAviso2, nil, nil);
  Geral.MensagemBox('Registros recriados com sucesso!', 'Informa��o',
    MB_OK+MB_ICONINFORMATION);
  Screen.Cursor := crDefault;
end;

procedure TFmTedCOcoSys.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTedCOcoSys.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTedCOcoSys.ReopenTedCOcoTxa(Banco: Integer; Cedente: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTedCOcoTxa, Dmod.MyDB, [
  'SELECT ban.Nome NO_BANCO,  ',
  'ELT(tct.TxaTip + 1, "$", "%", "?") NO_TxaTip,  ',
  'tct.*  ',
  'FROM tedcocotxa tct ',
  'LEFT JOIN bancos ban ON ban.Codigo=tct.Banco ',
  'WHERE tct.Codigo=' + Geral.FF0(QrTedCOcoSysCodigo.Value),
  'ORDER BY tct.Banco, tct.Cedente ',
  '']);
  //
  QrTedCOcoTxa.Locate('Banco;Cedente', VarArrayOf([Banco, Cedente]), []);
end;

procedure TFmTedCOcoSys.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTedCOcoSys.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTedCOcoSys.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTedCOcoSys.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTedCOcoSys.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTedCOcoSys.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.ShowModal;
    FmContas.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdPlaGen, CBPlaGen, QrContas, VAR_CADASTRO);
  end;
end;

procedure TFmTedCOcoSys.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTedCOcoSysCodigo.Value;
  Close;
end;

procedure TFmTedCOcoSys.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Codigo := EdCodigo.ValueVariant;
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Codigo = 0, EdCodigo, 'Defina o ID!') then Exit;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(EdPlaGen.ValueVariant = 0, EdPlaGen,
    'Defina uma conta (do plano de contas)!') then Exit;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PainelEdita,
    'tedcocosys', EdCodigo.ValueVariant, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTedCOcoSys.BtContasClick(Sender: TObject);
{
var
  Codigo, PlaGen, SubGrupo: Integer;
  Nome, Nome2, Credito, Debito: String;
}
begin
{
  if Geral.MensagemBox(
  'A a��o a seguir ir� criar contas novas no plano de contas, ' + sLineBreak +
  'sendo criada uma conta para cada ocorr�ncia sem conta definida!' + sLineBreak +
  '' + sLineBreak +
  'Antes de executar esta a��o tenha certeza que n�o existe cadastro ' + sLineBreak +
  'de contas que poderia ser usada para alguma ocorr�ncia!' + sLineBreak +
  '' + sLineBreak +
  'Somente ocorr�ncias j� utilizadas ter�o contas criadas e atreladas!' + sLineBreak +
  '' + sLineBreak +
  'Deseja realmente criar as contas e atrel�-las �s ocorr�ncias sem contas?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM tedcocosys ',
    'WHERE PlaGen=0 ',
    'AND Codigo <> 0 ',
    'AND Codigo IN ',
    '     ( ',
    '     SELECT DISTINCT Ocorrencia ',
    '     FROM ocorreu ',
    '     ) ',
    '']);
    //
    if Dmod.QrAux.RecordCount > 0 then
    begin
      if Geral.MensagemBox(
      'Foram localizadas ' + Geral.FF0(Dmod.QrAux.RecordCount) +
      ' ocorr�ncias usadas e sem conta atrelada!' + sLineBreak +
      '' + sLineBreak +
      'Deseja realmente criar as contas e atrel�-las a estas ocorr�ncias?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Dmod.QrAux.First;
        while not Dmod.QrAux.Eof do
        begin
          Nome := Dmod.QrAux.FieldByName('Nome').AsString;
          Nome2 := Nome;
          Credito := 'V';
          Debito := 'F';
          Subgrupo := 0;
          //
          Codigo := UMyMod.BuscaEmLivreY_Def('contas', 'Codigo', stIns, 0);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'contas', False, [
          'Nome', 'Nome2',
          'Subgrupo', 'Credito', 'Debito'], [
          'Codigo'], [
          Nome, Nome2,
          Subgrupo, Credito, Debito], [
          Codigo], False) then
          begin
            PlaGen := Codigo;
            Codigo := Dmod.QrAux.FieldByName('Codigo').AsInteger;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcocosys', False, [
            'PlaGen'], ['Codigo'], [PlaGen], [Codigo], False);
          end;
          //
          Dmod.QrAux.Next;
        end;
        //
        LocCod(QrTedCOcoSysCodigo.Value, QrTedCOcoSysCodigo.Value);
      end;
    end else Geral.MensagemBox(
      'N�o foram localizadas ocorr�ncias usadas sem conta atrelada!', 'Mensagem',
      MB_OK+MB_ICONINFORMATION);
  end;
}
end;

procedure TFmTedCOcoSys.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'tedcocosys', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'tedcocosys', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'tedcocosys', 'Codigo');
end;

procedure TFmTedCOcoSys.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTedCOcoSys.BtCabClick(Sender: TObject);
begin
{ N�o inclui! � gerenciado pelo aplicativo!
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrTedCOcoSys, [PainelDados],
  [PainelEdita], EdCodigo, ImgTipo, 'tedcocosys');
}
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTedCOcoSys.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  DBGrid1.Align   := alClient;
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
  CriaOForm;
end;

procedure TFmTedCOcoSys.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTedCOcoSysCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTedCOcoSys.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmTedCOcoSys.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTedCOcoSys.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTedCOcoSysCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTedCOcoSys.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTedCOcoSys.QrTedCOcoSysAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTedCOcoSys.QrTedCOcoSysAfterScroll(DataSet: TDataSet);
begin
  ReopenTedCOcoTxa(0, '');
end;

procedure TFmTedCOcoSys.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTedCOcoSys.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTedCOcoSysCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'tedcocosys', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTedCOcoSys.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTedCOcoSys.ItsAltera1Click(Sender: TObject);
begin
  MostraFmTedCOcoTxa(stUpd);

end;

procedure TFmTedCOcoSys.ItsExclui1Click(Sender: TObject);
begin
  UMyMod.ExcluiRegistros('Confirma a exclus�o da taxa selecionada?', Dmod.QrUpd,
  'tedcocotxa', ['Codigo', 'Banco', 'Cedente'], ['=','=','='], [
  QrTedCOcoTxaCodigo.Value, QrTedCOcoTxaBanco.Value, QrTedCOcoTxaCedente.Value],
  '');
end;

procedure TFmTedCOcoSys.ItsInclui1Click(Sender: TObject);
begin
  MostraFmTedCOcoTxa(stIns);
end;

procedure TFmTedCOcoSys.QrTedCOcoSysBeforeClose(DataSet: TDataSet);
begin
  QrTedCOcoTxa.Close;
end;

procedure TFmTedCOcoSys.QrTedCOcoSysBeforeOpen(DataSet: TDataSet);
begin
  QrTedCOcoSysCodigo.DisplayFormat := FFormatFloat;
end;

end.

