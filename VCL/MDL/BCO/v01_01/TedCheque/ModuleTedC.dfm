object DmTedC: TDmTedC
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 371
  Width = 448
  object QrTCCCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM tedccfgcab'
      'WHERE Codigo=8')
    Left = 68
    Top = 8
    object QrTCCCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTCCCabBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrTCCCabAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrTCCCabVersao: TWideStringField
      FieldName = 'Versao'
    end
    object QrTCCCabTamReg: TIntegerField
      FieldName = 'TamReg'
    end
    object QrTCCCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrTCCCabSeqLay: TIntegerField
      FieldName = 'SeqLay'
    end
    object QrTCCCabRemRet: TIntegerField
      FieldName = 'RemRet'
    end
    object QrTCCCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTCCCabCartDep: TIntegerField
      FieldName = 'CartDep'
    end
  end
  object QrTCCSet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ValBco '
      'FROM tedccfgset'
      'WHERE Codigo=16'
      'AND Registro=0'
      'AND SubReg=0'
      'AND Segmento=""'
      'AND SubSeg=0'
      'AND PosIni=40'
      'AND ValSis="0"'
      '')
    Left = 12
    Top = 8
    object QrTCCSetValBco: TWideStringField
      FieldName = 'ValBco'
      Size = 255
    end
  end
  object QrTCCIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM tedccfgits'
      'WHERE Codigo=8'
      'ORDER BY Registro, SubReg, PosIni')
    Left = 12
    Top = 56
    object QrTCCItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'tedccfgits.Codigo'
    end
    object QrTCCItsRegistro: TIntegerField
      FieldName = 'Registro'
      Origin = 'tedccfgits.Registro'
    end
    object QrTCCItsSubReg: TIntegerField
      FieldName = 'SubReg'
      Origin = 'tedccfgits.SubReg'
    end
    object QrTCCItsSegmento: TWideStringField
      FieldName = 'Segmento'
      Origin = 'tedccfgits.Segmento'
      Size = 10
    end
    object QrTCCItsSubSeg: TIntegerField
      FieldName = 'SubSeg'
      Origin = 'tedccfgits.SubSeg'
    end
    object QrTCCItsPosIni: TIntegerField
      FieldName = 'PosIni'
      Origin = 'tedccfgits.PosIni'
    end
    object QrTCCItsPosFim: TIntegerField
      FieldName = 'PosFim'
      Origin = 'tedccfgits.PosFim'
    end
    object QrTCCItsPosTam: TIntegerField
      FieldName = 'PosTam'
      Origin = 'tedccfgits.PosTam'
    end
    object QrTCCItsCampo: TWideStringField
      FieldName = 'Campo'
      Origin = 'tedccfgits.Campo'
      Size = 30
    end
    object QrTCCItsRestricao: TWideStringField
      FieldName = 'Restricao'
      Origin = 'tedccfgits.Restricao'
      Size = 1
    end
    object QrTCCItsTamInt: TIntegerField
      FieldName = 'TamInt'
      Origin = 'tedccfgits.TamInt'
    end
    object QrTCCItsTamFlu: TIntegerField
      FieldName = 'TamFlu'
      Origin = 'tedccfgits.TamFlu'
    end
    object QrTCCItsFmtDta: TWideStringField
      FieldName = 'FmtDta'
      Origin = 'tedccfgits.FmtDta'
    end
    object QrTCCItsPreenchmto: TIntegerField
      FieldName = 'Preenchmto'
      Origin = 'tedccfgits.Preenchmto'
    end
    object QrTCCItsCodSis: TIntegerField
      FieldName = 'CodSis'
      Origin = 'tedccfgits.CodSis'
    end
    object QrTCCItsDefaultRem: TWideStringField
      FieldName = 'DefaultRem'
      Origin = 'tedccfgits.DefaultRem'
      Size = 50
    end
    object QrTCCItsDefaultRet: TWideStringField
      FieldName = 'DefaultRet'
      Origin = 'tedccfgits.DefaultRet'
      Size = 50
    end
    object QrTCCItsObrigatori: TSmallintField
      FieldName = 'Obrigatori'
      Origin = 'tedccfgits.Obrigatori'
    end
    object QrTCCItsCliPreench: TSmallintField
      FieldName = 'CliPreench'
      Origin = 'tedccfgits.CliPreench'
    end
    object QrTCCItsDescri01: TWideStringField
      FieldName = 'Descri01'
      Origin = 'tedccfgits.Descri01'
      Size = 80
    end
    object QrTCCItsDescri02: TWideStringField
      FieldName = 'Descri02'
      Origin = 'tedccfgits.Descri02'
      Size = 80
    end
    object QrTCCItsDescri03: TWideStringField
      FieldName = 'Descri03'
      Origin = 'tedccfgits.Descri03'
      Size = 80
    end
    object QrTCCItsDescri04: TWideStringField
      FieldName = 'Descri04'
      Origin = 'tedccfgits.Descri04'
      Size = 80
    end
    object QrTCCItsDescri05: TWideStringField
      FieldName = 'Descri05'
      Origin = 'tedccfgits.Descri05'
      Size = 80
    end
    object QrTCCItsFulSize: TSmallintField
      FieldName = 'FulSize'
      Origin = 'tedccfgits.FulSize'
    end
    object QrTCCItsInvertAlin: TSmallintField
      FieldName = 'InvertAlin'
    end
    object QrTCCItsCortaStr: TSmallintField
      FieldName = 'CortaStr'
    end
  end
  object QrEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    Left = 68
    Top = 56
  end
  object QrLocLct1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lct.Controle, lct.TedRem_Sit, lct.TedRem_Its, '
      'lct.TedRetPrev, lct.TedRetExeC, lct.TedRetExeS, '
      'lct.DDeposito, lct.Compensado, lct.Sit, '
      'tcri.Controle TCRI_Ctrl '
      'FROM lct0001a lct'
      'LEFT JOIN tedcretits tcri ON tcri.Controle=lct.TedRetExeC'
      'WHERE lct.Controle=123481')
    Left = 68
    Top = 104
    object QrLocLct1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocLct1DDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrLocLct1Compensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLocLct1Sit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLocLct1TedRem_Sit: TSmallintField
      FieldName = 'TedRem_Sit'
    end
    object QrLocLct1TedRem_Its: TIntegerField
      FieldName = 'TedRem_Its'
    end
    object QrLocLct1TedRetPrev: TIntegerField
      FieldName = 'TedRetPrev'
    end
    object QrLocLct1TedRetExeC: TIntegerField
      FieldName = 'TedRetExeC'
    end
    object QrLocLct1TedRetExeS: TIntegerField
      FieldName = 'TedRetExeS'
    end
    object QrLocLct1TCRI_Lot: TIntegerField
      FieldName = 'TCRI_Lot'
      Required = True
    end
  end
  object QrTCTCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM tedcretcab'
      'WHERE  Produto = '#39#39
      'AND Cedente = '#39#39
      'AND Filial = '#39#39
      'AND CNPJCPF = '#39#39
      'AND Banco = 0'
      'AND NumSeqGer = 0'
      'AND DataGer = 0')
    Left = 12
    Top = 104
    object QrTCTCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTCTCabStep: TIntegerField
      FieldName = 'Step'
    end
  end
  object QrTCMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, LoReCaPrev, LoReItPrev,'
      'LoReCaProc, LoReItProc, LoReMyOcor '
      'FROM tedcremits'
      'WHERE Controle=14')
    Left = 12
    Top = 152
    object QrTCMILoReCaPrev: TIntegerField
      FieldName = 'LoReCaPrev'
    end
    object QrTCMILoReItPrev: TIntegerField
      FieldName = 'LoReItPrev'
    end
    object QrTCMILoReCaProc: TIntegerField
      FieldName = 'LoReCaProc'
    end
    object QrTCMILoReItProc: TIntegerField
      FieldName = 'LoReItProc'
    end
    object QrTCMILoReMyOcor: TIntegerField
      FieldName = 'LoReMyOcor'
    end
    object QrTCMIControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrTCTI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM tedcretits'
      'WHERE Codigo=12'
      'AND MyOcorr=230')
    Left = 68
    Top = 152
    object QrTCTICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTCTILinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrTCTISeuNumero: TLargeintField
      FieldName = 'SeuNumero'
    end
    object QrTCTIRemItsCtrl: TIntegerField
      FieldName = 'RemItsCtrl'
    end
    object QrTCTILctCtrl: TIntegerField
      FieldName = 'LctCtrl'
    end
    object QrTCTILctSub: TIntegerField
      FieldName = 'LctSub'
    end
    object QrTCTILctFatID: TIntegerField
      FieldName = 'LctFatID'
    end
    object QrTCTILctFatNum: TLargeintField
      FieldName = 'LctFatNum'
    end
    object QrTCTILctFatParc: TIntegerField
      FieldName = 'LctFatParc'
    end
    object QrTCTIMyOcorr: TIntegerField
      FieldName = 'MyOcorr'
    end
    object QrTCTIControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrTedCRetIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTedCRetItsCalcFields
    SQL.Strings = (
      'SELECT tcri.*, tcos.Nome NO_MyOcorr,'
      'tinc.Nome NO_Inconsiste'
      'FROM tedcretits tcri'
      'LEFT JOIN tedcocosys tcos ON tcos.Codigo=tcri.MyOcorr'
      'LEFT JOIN tedcretinc tinc ON tinc.Codigo=tcri.Inconsiste'
      'WHERE tcri.Codigo=13'
      '')
    Left = 132
    Top = 56
    object QrTedCRetItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTedCRetItsLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrTedCRetItsLctCtrl: TIntegerField
      FieldName = 'LctCtrl'
    end
    object QrTedCRetItsLctSub: TIntegerField
      FieldName = 'LctSub'
    end
    object QrTedCRetItsLctFatID: TIntegerField
      FieldName = 'LctFatID'
    end
    object QrTedCRetItsLctFatNum: TLargeintField
      FieldName = 'LctFatNum'
    end
    object QrTedCRetItsLctFatParc: TIntegerField
      FieldName = 'LctFatParc'
    end
    object QrTedCRetItsOcorrBco: TIntegerField
      FieldName = 'OcorrBco'
    end
    object QrTedCRetItsMyOcorr: TIntegerField
      FieldName = 'MyOcorr'
    end
    object QrTedCRetItsStep: TSmallintField
      FieldName = 'Step'
    end
    object QrTedCRetItsCedente: TWideStringField
      FieldName = 'Cedente'
    end
    object QrTedCRetItsFilial: TWideStringField
      FieldName = 'Filial'
      Size = 10
    end
    object QrTedCRetItsUsoEmpresa: TWideStringField
      FieldName = 'UsoEmpresa'
      Size = 25
    end
    object QrTedCRetItsDtaCaptura: TDateField
      FieldName = 'DtaCaptura'
    end
    object QrTedCRetItsCMC7: TWideStringField
      FieldName = 'CMC7'
      Size = 30
    end
    object QrTedCRetItsRemValor: TFloatField
      FieldName = 'RemValor'
    end
    object QrTedCRetItsRemDataBoa: TDateField
      FieldName = 'RemDataBoa'
    end
    object QrTedCRetItsBorderoBco: TIntegerField
      FieldName = 'BorderoBco'
    end
    object QrTedCRetItsSeuNumero: TLargeintField
      FieldName = 'SeuNumero'
    end
    object QrTedCRetItsAlinea: TIntegerField
      FieldName = 'Alinea'
    end
    object QrTedCRetItsCodRejeito: TIntegerField
      FieldName = 'CodRejeito'
    end
    object QrTedCRetItsNossoNum: TLargeintField
      FieldName = 'NossoNum'
    end
    object QrTedCRetItsRetValor: TFloatField
      FieldName = 'RetValor'
    end
    object QrTedCRetItsRetDataBoa: TDateField
      FieldName = 'RetDataBoa'
    end
    object QrTedCRetItsTrfCedente: TWideStringField
      FieldName = 'TrfCedente'
      Size = 18
    end
    object QrTedCRetItsTrfDtaOper: TDateField
      FieldName = 'TrfDtaOper'
    end
    object QrTedCRetItsTrfContrat: TWideStringField
      FieldName = 'TrfContrat'
      Size = 25
    end
    object QrTedCRetItsTrfParcela: TIntegerField
      FieldName = 'TrfParcela'
    end
    object QrTedCRetItsTrfDtaCntr: TDateField
      FieldName = 'TrfDtaCntr'
    end
    object QrTedCRetItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrTedCRetItsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrTedCRetItsContaCorre: TWideStringField
      FieldName = 'ContaCorre'
      Size = 10
    end
    object QrTedCRetItsCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrTedCRetItsPraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrTedCRetItsRetInscTip: TWideStringField
      FieldName = 'RetInscTip'
      Size = 3
    end
    object QrTedCRetItsRetInscNum: TWideStringField
      FieldName = 'RetInscNum'
    end
    object QrTedCRetItsRetEmitNom: TWideStringField
      FieldName = 'RetEmitNom'
      Size = 30
    end
    object QrTedCRetItsRemItsCtrl: TIntegerField
      FieldName = 'RemItsCtrl'
    end
    object QrTedCRetItsInconsiste: TIntegerField
      FieldName = 'Inconsiste'
    end
    object QrTedCRetItsNO_MyOcorr: TWideStringField
      FieldName = 'NO_MyOcorr'
      Size = 100
    end
    object QrTedCRetItsNO_Inconsiste: TWideStringField
      FieldName = 'NO_Inconsiste'
      Size = 60
    end
    object QrTedCRetItsTEXTO_PARA_USR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO_PARA_USR'
      Size = 255
      Calculated = True
    end
  end
  object QrTedCRetCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTedCRetCabBeforeClose
    AfterScroll = QrTedCRetCabAfterScroll
    OnCalcFields = QrTedCRetCabCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM tedcretcab'
      'WHERE Codigo=12')
    Left = 132
    Top = 8
    object QrTedCRetCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTedCRetCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTedCRetCabTedC_Cfg: TIntegerField
      FieldName = 'TedC_Cfg'
    end
    object QrTedCRetCabStep: TIntegerField
      FieldName = 'Step'
    end
    object QrTedCRetCabProduto: TWideStringField
      FieldName = 'Produto'
      Size = 10
    end
    object QrTedCRetCabCedente: TWideStringField
      FieldName = 'Cedente'
    end
    object QrTedCRetCabFilial: TWideStringField
      FieldName = 'Filial'
      Size = 10
    end
    object QrTedCRetCabCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrTedCRetCabBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrTedCRetCabDataGer: TDateField
      FieldName = 'DataGer'
    end
    object QrTedCRetCabNumSeqGer: TIntegerField
      FieldName = 'NumSeqGer'
    end
    object QrTedCRetCabDepositari: TWideStringField
      FieldName = 'Depositari'
      Size = 10
    end
    object QrTedCRetCabTerceira: TWideStringField
      FieldName = 'Terceira'
      Size = 10
    end
    object QrTedCRetCabCartQtd: TIntegerField
      FieldName = 'CartQtd'
    end
    object QrTedCRetCabCartVal: TFloatField
      FieldName = 'CartVal'
    end
    object QrTedCRetCabEntrQtd: TIntegerField
      FieldName = 'EntrQtd'
    end
    object QrTedCRetCabEntrVal: TFloatField
      FieldName = 'EntrVal'
    end
    object QrTedCRetCabDepoQtd: TIntegerField
      FieldName = 'DepoQtd'
    end
    object QrTedCRetCabDepoVal: TFloatField
      FieldName = 'DepoVal'
    end
    object QrTedCRetCabBaxaQtd: TIntegerField
      FieldName = 'BaxaQtd'
    end
    object QrTedCRetCabBaxaVal: TFloatField
      FieldName = 'BaxaVal'
    end
    object QrTedCRetCabDevlQtd: TIntegerField
      FieldName = 'DevlQtd'
    end
    object QrTedCRetCabDevlVal: TFloatField
      FieldName = 'DevlVal'
    end
    object QrTedCRetCabReapQtd: TIntegerField
      FieldName = 'ReapQtd'
    end
    object QrTedCRetCabReapVal: TFloatField
      FieldName = 'ReapVal'
    end
    object QrTedCRetCabUltLin: TIntegerField
      FieldName = 'UltLin'
    end
    object QrTedCRetCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrTedCRetCabItens: TIntegerField
      FieldName = 'Itens'
    end
  end
  object frxTedCRet: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    Left = 200
    Top = 104
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxTedCRetCab
        DataSetName = 'frxTedCRetCab'
      end
      item
        DataSet = frxTedCRetIts
        DataSetName = 'frxTedCRetIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 971.339210000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 668.976810000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Arquivo Retorno de TED Cheque')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 820.158010000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 25.322844410000000000
        Top = 234.330860000000000000
        Width = 971.339210000000000000
        Condition = 'frxTedCRetIts."MyOcorr"'
        object Memo51: TfrxMemoView
          Width = 351.496290000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'Ocorr'#234'ncia: [frxTedCRetIts."MyOcorr"] - [frxTedCRetIts."NO_MyOco' +
              'rr"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Top = 15.118120000000000000
          Width = 102.047244094488200000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CMC7')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 117.165430000000000000
          Top = 15.118120000000000000
          Width = 15.118110240000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 132.283550000000000000
          Top = 15.118120000000000000
          Width = 18.897637800000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag'#234'n.')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 151.181200000000000000
          Top = 15.118120000000000000
          Width = 37.795275590000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta Corr.')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 188.976500000000000000
          Top = 15.118120000000000000
          Width = 26.456692910000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cheque')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 929.764380000000000000
          Top = 15.118120000000000000
          Width = 41.574800710000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Lan'#231'to.fin.')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 782.362710000000000000
          Top = 15.118120000000000000
          Width = 26.456692910000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dta Boa')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 808.819420000000000000
          Top = 15.118120000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 672.756340000000000000
          Top = 15.118120000000000000
          Width = 26.456692910000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dta Boa')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 699.213050000000000000
          Top = 15.118120000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 442.205010000000000000
          Top = 15.118120000000000000
          Width = 124.724416770000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do emitente')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 748.346940000000000000
          Width = 109.606370000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Remessa')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 351.496290000000000000
          Width = 396.850650000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Retorno')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 215.433210000000000000
          Top = 15.118120000000000000
          Width = 136.063006770000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status ou Inconsist'#234'ncia')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 377.953000000000000000
          Top = 15.118120000000000000
          Width = 64.251968503937010000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo e CNPJ/CPF')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 102.047310000000000000
          Top = 15.118120000000000000
          Width = 15.118110240000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'P'#231'a.')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 657.638220000000000000
          Top = 15.118120000000000000
          Width = 15.118110240000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Rej.')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 582.047620000000000000
          Top = 15.118120000000000000
          Width = 37.795275590000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Border'#244' bco')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 619.842920000000000000
          Top = 15.118120000000000000
          Width = 37.795275590000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Num. Bco.')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 566.929500000000000000
          Top = 15.118120000000000000
          Width = 15.118110240000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lin')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 857.953310000000000000
          Top = 15.118120000000000000
          Width = 34.015748030000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 891.969080000000000000
          Top = 15.118120000000000000
          Width = 37.795278030000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID Item')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 748.346940000000000000
          Top = 15.118120000000000000
          Width = 34.015748030000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID no Lote')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 857.953310000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Meu Border'#244)
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 351.496290000000000000
          Top = 15.118120000000000000
          Width = 26.456692910000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dta Boa')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 317.480520000000000000
        Width = 971.339210000000000000
        object Memo58: TfrxMemoView
          Width = 971.339210000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 393.071120000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 154.960730000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 612.283860000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 151.181200000000000000
          Width = 207.874150000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              '1: Lote de border'#244' de compra de direitos       2. Lote de envio ' +
              'de cheque ao banco')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 94.488250000000000000
        Top = 117.165430000000000000
        Width = 971.339210000000000000
        DataSet = frxTedCRetCab
        DataSetName = 'frxTedCRetCab'
        RowCount = 0
        object Memo50: TfrxMemoView
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Arquivo: [frxTedCRetCab."Nome"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 3.779530000000000000
          Top = 22.677180000000000000
          Width = 963.780150000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'Empresa: [frxTedCRetCab."Empresa"] |  C'#243'digo: [frxTedCRetCab."Co' +
              'digo"] |  Layout: [frxTedCRetCab."TedC_Cfg"] |  Produto: [frxTed' +
              'CRetCab."Produto"] |  Cedente: [frxTedCRetCab."Cedente"] |  CNPJ' +
              ': [frxTedCRetCab."CNPJCPF"] |  Banco: [frxTedCRetCab."Banco"] | ' +
              'Gera'#231#227'o: [frxTedCRetCab."DataGer"] |  N'#186' sequencial: [frxTedCRet' +
              'Cab."NumSeqGer"] |  Itens: [<frxTedCRetCab."Itens">] |')
          ParentFont = False
          WordWrap = False
        end
        object Shape1: TfrxShapeView
          Top = 37.795300000000000000
          Width = 971.339210000000000000
          Height = 52.913420000000000000
          ShowHint = False
          Frame.Style = fsDot
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          Left = 245.669450000000000000
          Top = 41.574830000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Em carteira:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 245.669450000000000000
          Top = 56.692950000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."CartQtd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 245.669450000000000000
          Top = 71.811070000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."CartVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 332.598640000000000000
          Top = 41.574830000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Entrada:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 332.598640000000000000
          Top = 56.692950000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."EntrQtd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 332.598640000000000000
          Top = 71.811070000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."EntrVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 188.976500000000000000
          Top = 41.574830000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Informa'#231#245'es:')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 188.976500000000000000
          Top = 56.692950000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quantidade:')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 188.976500000000000000
          Top = 71.811070000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor:')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 419.527830000000000000
          Top = 41.574830000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dep'#243'sito:')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 419.527830000000000000
          Top = 56.692950000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."DepoQtd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 419.527830000000000000
          Top = 71.811070000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."DepoVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 506.457020000000000000
          Top = 41.574830000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Baixa:')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 506.457020000000000000
          Top = 56.692950000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."BaxaQtd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 506.457020000000000000
          Top = 71.811070000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."BaxaVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 593.386210000000000000
          Top = 41.574830000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Devolu'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 593.386210000000000000
          Top = 56.692950000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."DevlQtd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 593.386210000000000000
          Top = 71.811070000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."DevlVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 680.315400000000000000
          Top = 41.574830000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reapresenta'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 680.315400000000000000
          Top = 56.692950000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."ReapQtd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 680.315400000000000000
          Top = 71.811070000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetCab."ReapVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object DetailData1: TfrxDetailData
        Height = 10.204724410000000000
        Top = 283.464750000000000000
        Width = 971.339210000000000000
        DataSet = frxTedCRetIts
        DataSetName = 'frxTedCRetIts'
        RowCount = 0
        object Memo55: TfrxMemoView
          Width = 102.047244094488200000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'CMC7'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxTedCRetIts."CMC7"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 117.165430000000000000
          Width = 15.118110240000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'Banco'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%3.3d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxTedCRetIts."Banco"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 132.283550000000000000
          Width = 18.897637800000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'Agencia'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%4.4d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxTedCRetIts."Agencia"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 151.181200000000000000
          Width = 37.795275590000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'ContaCorre'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxTedCRetIts."ContaCorre"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 188.976500000000000000
          Width = 26.456692910000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'Cheque'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%6.6d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxTedCRetIts."Cheque"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 929.764380000000000000
          Width = 41.574800710000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'LctCtrl'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetIts."LctCtrl"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 782.362710000000000000
          Width = 26.456692910000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'RemDataBoa'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxTedCRetIts."RemDataBoa"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 808.819459060000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'RemValor'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetIts."RemValor"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 672.756340000000000000
          Width = 26.456692910000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'RetDataBoa'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxTedCRetIts."RetDataBoa"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 699.213089060000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'RetValor'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetIts."RetValor"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 442.205010000000000000
          Width = 124.724416770000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'RetEmitNom'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxTedCRetIts."RetEmitNom"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 393.071120000000000000
          Width = 49.133858267716540000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'RetInscNum'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxTedCRetIts."RetInscNum"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 215.433210000000000000
          Width = 136.063006770000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'TEXTO_PARA_USR'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxTedCRetIts."TEXTO_PARA_USR"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 377.953000000000000000
          Width = 15.118110236220470000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'RetInscTip'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%3.3d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxTedCRetIts."RetInscTip"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 102.047310000000000000
          Width = 15.118110240000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'Praca'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%3.3d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxTedCRetIts."Praca"]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 657.638220000000000000
          Width = 15.118110240000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'CodRejeito'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxTedCRetIts."CodRejeito"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 582.047620000000000000
          Width = 37.795275590000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'BorderoBco'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetIts."BorderoBco"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 619.842920000000000000
          Width = 37.795275590000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'NossoNum'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetIts."NossoNum"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 566.929500000000000000
          Width = 15.118110240000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'Linha'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxTedCRetIts."Linha"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 857.953310000000000000
          Width = 34.015748030000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'LctFatNum'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetIts."LctFatNum"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 891.969080000000000000
          Width = 37.795278030000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'LctFatParc'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetIts."LctFatParc"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 748.346940000000000000
          Width = 34.015748030000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'RemItsCtrl'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxTedCRetIts."RemItsCtrl"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 351.496290000000000000
          Width = 26.456692910000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'RemDataBoa'
          DataSet = frxTedCRetIts
          DataSetName = 'frxTedCRetIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxTedCRetIts."RemDataBoa"]')
          ParentFont = False
        end
      end
    end
  end
  object frxTedCRetCab: TfrxDBDataset
    UserName = 'frxTedCRetCab'
    CloseDataSource = False
    DataSet = QrTedCRetCab
    BCDToCurrency = False
    Left = 200
    Top = 8
  end
  object frxTedCRetIts: TfrxDBDataset
    UserName = 'frxTedCRetIts'
    CloseDataSource = False
    DataSet = QrTedCRetIts
    BCDToCurrency = False
    Left = 200
    Top = 56
  end
  object QrStepI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Linha) Itens, SUM(IF(Step=9, 1, 0) ) Step9'
      'FROM tedcretits'
      'WHERE Codigo=13')
    Left = 132
    Top = 104
    object QrStepIItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrStepIStep9: TFloatField
      FieldName = 'Step9'
    end
  end
  object QrJaTah: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM tedcremits'
      'WHERE Controle=11')
    Left = 132
    Top = 152
    object QrJaTahCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrCustodia: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Controle) Itens, '
      'SUM(Credito) Credito'
      'FROM lct0001a'
      'WHERE TedRem_Sit IN (3,4,5,6)')
    Left = 268
    Top = 8
    object QrCustodiaCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrCustodiaItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrLastRet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT (CartVal + EntrVal) Valor'
      'FROM tedcretcab'
      'WHERE Empresa=1'
      'AND Produto='#39'900'#39
      'AND Cedente <> '#39#39
      'AND Filial <> '#39#39
      'AND CNPJCPF <> '#39#39
      'AND Banco=422'
      'ORDER BY DataGer DESC'
      'LIMIT 0,1')
    Left = 268
    Top = 56
    object QrLastRetValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
  end
  object QrCHEnvi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Controle) Itens, SUM(Valor) Valor '
      'FROM tedcremits'
      'WHERE Codigo=2'
      'AND MyOcorr=1')
    Left = 268
    Top = 104
    object QrCHEnviItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrCHEnviValor: TFloatField
      FieldName = 'Valor'
    end
  end
end
