unit TedCRemChg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmTedCRemChg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    QrLct: TmySQLQuery;
    QrLctBanco: TIntegerField;
    QrLctAgencia: TIntegerField;
    QrLctContaCorrente: TWideStringField;
    QrLctDocumento: TFloatField;
    QrLctCredito: TFloatField;
    QrLctData: TDateField;
    QrLctDDeposito: TDateField;
    QrLctCNPJCPF: TWideStringField;
    QrLctEmitente: TWideStringField;
    Panel5: TPanel;
    EdTipific: TdmkEdit;
    EdRegiaoCompe: TdmkEdit;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdCheque: TdmkEdit;
    DBGrid1: TDBGrid;
    DsLct: TDataSource;
    QrLctControle: TIntegerField;
    QrLctSub: TSmallintField;
    QrLctFatParcela: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatNum: TFloatField;
    CkTipo: TCheckBox;
    CkPraca: TCheckBox;
    CkBanco: TCheckBox;
    CkAgencia: TCheckBox;
    CkContaCorrente: TCheckBox;
    CkCheque: TCheckBox;
    BtPesquisa: TBitBtn;
    CkCNPJCPF: TCheckBox;
    EdCPF: TdmkEdit;
    CkEmitente: TCheckBox;
    EdEmitente: TdmkEdit;
    QrLctTipific: TSmallintField;
    QrLctPraca: TIntegerField;
    CkSoEnviados: TCheckBox;
    BtOK: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    Label2: TLabel;
    TPDataBoa: TdmkEditDateTimePicker;
    Panel7: TPanel;
    QrTCRI: TmySQLQuery;
    QrTCRICMC7: TWideStringField;
    GB_CMC7: TGroupBox;
    Panel8: TPanel;
    Label36: TLabel;
    SpeedButton1: TSpeedButton;
    Label1: TLabel;
    EdBanda: TdmkEdit;
    EdCMC_7: TdmkEdit;
    QrLctNo_TedRem_Sit: TWideStringField;
    QrLctTedRem_Sit: TSmallintField;
    QrLctTedRem_Its: TIntegerField;
    SpeedButton2: TSpeedButton;
    QrLctDepositado: TSmallintField;
    QrLctCliente: TIntegerField;
    QrLctVencimento: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure EdCMC_7Change(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure CkTipoClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrLctCalcFields(DataSet: TDataSet);
    procedure QrLctBeforeClose(DataSet: TDataSet);
    procedure QrLctAfterOpen(DataSet: TDataSet);
    procedure PageControl1Changing(Sender: TObject; var AllowChange: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    FAllowChange: Boolean;
    procedure FechaPesquisa();
  public
    FOcorrBco, FMyOcorr: Integer;
    FSQLType: TSQLType;
    { Public declarations }
  end;

  var
  FmTedCRemChg: TFmTedCRemChg;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, MyDBCheck, TedCRemInnDis,
  TedC_Aux, TedCRemCab, MyListas, ModuleTedC;

{$R *.DFM}

procedure TFmTedCRemChg.BtOKClick(Sender: TObject);
var
  Continua: Integer;
  //
  CMC7, BACC, BomPara, Mens: String;
  LctFatNum, FatNum, Valor: Double;
  TedRem_Sit, TedRem_Its, FatID, FatParcela, Sub, LctSitAnt, LctCtrAnt, OcorrBco,
  MyOcorr, Codigo, Controle, LctCtrl, LctFatID, LctFatParc, LctSub: Integer;
  Banda: TBandaMagnetica;
begin
//
{ TODO :    A :: URGENTE!!! Criar ocorr�ncia Taxa, etc para prorrogar, quitar! }
  BACC := Geral.FFN(QrLctBanco.Value, 3)  + ' - ' +
          Geral.FFN(QrLctAgencia.Value, 4) + ' - ' +
          QrLctContaCorrente.Value + ' - ' +
          Geral.FFN(Trunc(QrLctDocumento.Value), 6);
  case  FSQLType of
    stUpd: TedRem_Sit := CO_SIT_TEDC_INSTRUCAO_ALTERACAO_NO_BANCO; // Adicionado em lote (instru��o), aguardando altera��o no banco
    else begin
      Geral.MensagemBox(
      'Adi��o a lote concelada!' + sLineBreak +
      '"FSQLType" incorreto!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  Codigo         := FmTedCRemCab.QrTedCRemCabCodigo.Value;
  LctCtrl        := QrLctControle.Value;
  LctSub         := QrLctSub.Value;
  LctFatID       := QrLctFatID.Value;
  LctFatNum      := QrLctFatNum.Value;
  LctFatParc     := QrLctFatParcela.Value;
  OcorrBco       := FOcorrBco;
  MyOcorr        := FMyOcorr;
  LctSitAnt      := QrLctTedRem_Sit.Value;
  LctCtrAnt      := QrLctTedRem_Its.Value;
  CMC7 := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTCRI, Dmod.MyDB, [
  'SELECT CMC7 ',
  'FROM tedcremits ',
  'WHERE LENGTH(CMC7) = 30 ',
  'AND LctCtrl=' + Geral.FF0(LctCtrl),
  'AND LctSub=' + Geral.FF0(LctSub),
  'AND LctFatID=' + Geral.FF0(LctFatID),
  'AND LctFatNum=' + Geral.FF0(Trunc(LctFatNum)),
  'AND LctFatParc=' + Geral.FF0(LctFatParc),
  'ORDER BY Controle DESC',
  '']);
  CMC7 := QrTCRICMC7.Value;
  if Length(CMC7) <> 30 then
  begin
    if InputQuery('CMC7 incorreto', 'Informe o CMC7:', CMC7) then
    begin
      CMC7 := Geral.SoNumero_TT(CMC7);
      if not MLAGeral.CalculaCMC7(CMC7) = 0 then
      begin
        Geral.MensagemBox(
        'Adi��o a lote concelada!' + sLineBreak +
        'CMC7 incorreto!',
        'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
      Banda := TBandaMagnetica.Create;
      Banda.BandaMagnetica := CMC7;
      if (Geral.IMV(Banda.Banco) <> QrLctBanco.Value)
      or (Geral.IMV(Banda.Agencia) <> QrLctAgencia.Value)
      or (Banda.Conta <> QrLctContaCorrente.Value)
      or (Geral.IMV(Banda.Numero) <> QrLctDocumento.Value)
      then
      begin
        Geral.MensagemBox(
        'Adi��o a lote concelada!' + sLineBreak +
        'CMC7 n�o confere com o cheque selecionado!',
        'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end else
    begin
      Geral.MensagemBox(
      'Adi��o a lote concelada!' + sLineBreak +
      'CMC7 n�o informado!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  //
  // Verifica se o item j� n�o est� no lote!
  UnDmkDAC_PF.AbreMySQLQuery0(DmTedC.QrJaTah, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM tedcremits ',
  'WHERE Controle=' + Geral.FF0(QrLctTedRem_Its.Value),
  '']);
  if DmTedC.QrJaTahCodigo.Value = FmTedCRemCab.QrTedCRemCabCodigo.Value then
  begin
    Continua := ID_CANCEL;
    Geral.MensagemBox(
    'Adi��o a lote concelada!' + sLineBreak +
    'O cheque selecionado j� pertence ao lote atual!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end else
  if QrLctTedRem_Its.Value <> 0  then
    Continua := ID_YES
  else
    Continua := Geral.MensagemBox(
    'O cheque n�o pertence a nenhum lote enviado ao banco!' + sLineBreak +
    'Deseja enviar instru��o ao banco assim mesmo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
  //
  if Continua = ID_YES then
  begin
{
  CO_SIT_TEDC_DESCONHECIDO = -1;
  CO_SIT_TEDC_NADA = 0;
  CO_SIT_TEDC_INSTRUCAO_ENTRADA_NO_BANCO = 1;
  CO_SIT_TEDC_LIMBO_ENTRADA_NO_BANCO = 2;
  CO_SIT_TEDC_ENTROU_NO_BANCO = 3;
  CO_SIT_TEDC_INSTRUCAO_ALTERACAO_NO_BANCO = 4;
  CO_SIT_TEDC_LIMBO_ALTERACAO_NO_BANCO = 5;
  CO_SIT_TEDC_ALTEROU_NO_BANCO = 6;
  CO_SIT_TEDC_BAIXOU_NO_BANCO = 9;
}
    if QrLctTedRem_Sit.Value in ([
    CO_SIT_TEDC_ENTROU_NO_BANCO, CO_SIT_TEDC_LIMBO_ALTERACAO_NO_BANCO]) then
    Continua := ID_YES
  else
    Continua := Geral.MensagemBox(
    'O cheque n�o est� no banco!' + sLineBreak +
    'Deseja enviar instru��o ao banco assim mesmo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
  end;
  if Continua = ID_YES then
  begin
    if TedC_Ax.AceitaChequeEmLote(FSQLType, QrLctDepositado.Value,
    QrLctTedRem_Sit.Value) then
    begin
      if MyOcorr = CO_MYOCORR_TEDC_ALTERAR_BOM_PARA then
      begin
        if TPDataBoa.Date <= Date then
        begin
          Geral.MensagemBox('Data inv�lida para novo dep�sito!',
          'Aviso', MB_OK+MB_ICONWARNING);
          Exit;
        end else
          BomPara := Geral.FDT(TPDataBoa.Date, 1);
      end else
        BomPara := Geral.FDT(QrLctDDeposito.Value, 1);
      //
      Valor := QrLctCredito.Value;
      //
      Controle :=
        UMyMod.BPGS1I32('tedcremits', 'Controle', '', '', tsPos, stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'tedcremits', False, [
      'Codigo', 'LctCtrl', 'LctSub',
      'LctFatID', 'LctFatNum', 'LctFatParc',
      'BomPara', 'Valor', 'OcorrBco',
      'MyOcorr', 'CMC7', 'LctSitAnt',
      'LctCtrAnt'], [
      'Controle'], [
      Codigo, LctCtrl, LctSub,
      LctFatID, LctFatNum, LctFatParc,
      BomPara, Valor, OcorrBco,
      MyOcorr, CMC7, LctSitAnt,
      LctCtrAnt], [
      Controle], True) then
      begin
        case  FSQLType of
          stIns: TedRem_Sit := CO_SIT_TEDC_INSTRUCAO_ENTRADA_NO_BANCO; // Adicionado em lote (entrada), aguardando entrada no banco
          stUpd: TedRem_Sit := CO_SIT_TEDC_INSTRUCAO_ALTERACAO_NO_BANCO; // Adicionado em lote (instru��o), aguardando altera��o no banco
        end;
        // Deve ser antes
        TedRem_Its := Controle;
        // Deve ser depois
        Controle := QrLctControle.Value;
        //
        Sub := QrLctSub.Value;
        FatID := QrLctFatID.Value;
        FatNum := QrLctFatNum.Value;
        FatParcela := QrLctFatParcela.Value;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
        'TedRem_Sit', 'TedRem_Its'], [
        'Controle', 'Sub',
        'FatID', 'FatNum', 'FatParcela'], [
        TedRem_Sit, TedRem_Its], [
        Controle, Sub,
        FatID, FatNum, FatParcela], True);
      end;
      EdBanda.Text := '';
      Mens := 'O cheque ' + BACC + ' foi adicionado ao lote!';
      Geral.MensagemBox(Mens, 'Mensagem', MB_OK+MB_ICONINFORMATION);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, Mens);
    end;
  end;
end;

procedure TFmTedCRemChg.BtPesquisaClick(Sender: TObject);
var
  Tip, Cmp, Bco, Age, Cta, Chq, Doc, Nom: String;
begin
  Tip := Geral.FF0(EdTipific.ValueVariant);
  Cmp := Geral.FF0(EdRegiaoCompe.ValueVariant);
  Bco := Geral.FF0(EdBanco.ValueVariant);
  Age := Geral.FF0(EdAgencia.ValueVariant);
  Cta := '"%' + EdConta.Text + '%"';
  Chq := Geral.FF0(EdCheque.ValueVariant);
  Doc := '"' + Geral.SoNumero_TT(EdCPF.Text) + '"';
  Nom := '"%' + EdEmitente.Text + '%"';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLct, Dmod.MyDB, [
  'SELECT Data, Tipific, Praca, Banco, Agencia, ',
  'ContaCorrente, Documento, Cliente, Vencimento, ',
  'CNPJCPF, Emitente, Credito, DDeposito, TedRem_Sit, TedRem_Its, ',
  'Controle, Sub, FatID, FatNum, FatParcela, Depositado ',
  'FROM ' + CO_TabLcta,
  'WHERE FatID=' + TXT_VAR_FATID_0301,
  Geral.ATS_if(CkTipo.Checked, ['AND Tipific=' + Tip]),
  Geral.ATS_if(CkPraca.Checked, ['AND Praca=' + Cmp]),
  Geral.ATS_if(CkBanco.Checked, ['AND Banco=' + Bco]),
  Geral.ATS_if(CkAgencia.Checked, ['AND Agencia=' + Age]),
  Geral.ATS_if(CkContaCorrente.Checked, ['AND ContaCorrente LIKE ' + Cta]),
  Geral.ATS_if(CkCheque.Checked, ['AND Documento=' + Chq]),
  Geral.ATS_if(CkCNPJCPF.Checked, ['AND CNPJCPF=' + Doc]),
  Geral.ATS_if(CkEmitente.Checked, ['AND Emitente LIKE ' + Nom]),
  Geral.ATS_if(CkSoEnviados.Checked, ['AND TedRem_Its <> 0 ']),
  '']);
end;

procedure TFmTedCRemChg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTedCRemChg.CkTipoClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTedCRemChg.EdBandaChange(Sender: TObject);
begin
{
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
    EdCMC_7.Text := Geral.SoNumero_TT(EdBanda.Text);
}
end;

procedure TFmTedCRemChg.EdCMC_7Change(Sender: TObject);
{
var
  Banda: TBandaMagnetica;
}
begin
{
  if MLAGeral.CalculaCMC7(EdCMC_7.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7.Text;
    EdTipific.Text       := Banda.Tipo;
    EdRegiaoCompe.Text   := Banda.Compe;
    EdBanco.Text   := Banda.Banco;
    EdAgencia.Text := Banda.Agencia;
    EdConta.Text   := Banda.Conta;
    EdCheque.Text  := Banda.Numero;
    //
    LocalizaChequePelaBanda();
    //
  end;
}
end;

procedure TFmTedCRemChg.FechaPesquisa();
begin
  QrLct.Close;
end;

procedure TFmTedCRemChg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FAllowChange := False;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmTedCRemChg.FormCreate(Sender: TObject);
begin
  TPDataBoa.Date := 0;
  FAllowChange := True;
end;

procedure TFmTedCRemChg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTedCRemChg.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  AllowChange := FAllowChange;
end;

procedure TFmTedCRemChg.QrLctAfterOpen(DataSet: TDataSet);
begin
  BtOK.Enabled := QrLct.RecordCount > 0;
end;

procedure TFmTedCRemChg.QrLctBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

procedure TFmTedCRemChg.QrLctCalcFields(DataSet: TDataSet);
begin
  QrLctNO_TedRem_Sit.Value := TedC_Ax.DescricaoDeTedRem_Sit(QrLctTedRem_Sit.Value);
end;

procedure TFmTedCRemChg.SpeedButton1Click(Sender: TObject);
var
  CMC7: String;
  I: Integer;
begin
  if Geral.MensagemBox('A janela que ser� aberta seve somente para teste!' + sLineBreak +
  'N�o selecione nenhum cheque nela para enviar ao banco quando n�o for envio de teste!' + sLineBreak +
  'Confirma que a sele��o de cheques na janela ser� apenas para envio de teste?!',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if DBCheck.CriaFm(TFmTedCRemInnDis, FmTedCRemInnDis, afmoNegarComAviso) then
    begin
      FmTedCRemInnDis.ShowModal;
      CMC7 := FmTedCRemInnDis.FCMC7;
      FmTedCRemInnDis.Destroy;
      Application.ProcessMessages;
      if CMC7 <> '' then
      begin
        EdBanda.Text := '';
        for I := 1 to Length(CMC7) do
        begin
          EdBanda.Perform(WM_CHAR, Ord(CMC7[I]), 1);
          Sleep(25);
        end;
      end;
    end;
  end;
end;

procedure TFmTedCRemChg.SpeedButton2Click(Sender: TObject);
const
  NaoDeposita = True;
  SQLType = stIns;
var
  NovaData: TDateTime;
begin
  if Dmod.FazProrrogacao(QrLctFatParcela.Value,  QrLctCliente.Value,
  QrLctVencimento.Value, QrLctDDeposito.Value, QrLctCredito.Value,
  NaoDeposita, Self.Name, SQLType, NovaData) then
  begin
    TPDataBoa.Date := NovaData;
    //
{ TODO :    A :: URGENTE!!! Continuar aqui }
  end;
end;

end.
