object FmTedCOcoSys: TFmTedCOcoSys
  Left = 368
  Top = 194
  Caption = 'TED-CHQUE-003 :: Ocorr'#234'ncias Banc'#225'rias de TED Cheque'
  ClientHeight = 470
  ClientWidth = 761
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 109
    Width = 761
    Height = 361
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 761
      Height = 93
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 65
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label10: TLabel
        Left = 8
        Top = 48
        Width = 131
        Height = 13
        Caption = 'Conta (do plano de contas):'
        Enabled = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 24
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTedCOcoSys
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 65
        Top = 24
        Width = 684
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTedCOcoSys
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 64
        Top = 64
        Width = 685
        Height = 21
        DataField = 'NO_PLAGEN'
        DataSource = DsTedCOcoSys
        Enabled = False
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 8
        Top = 64
        Width = 56
        Height = 21
        DataField = 'PlaGen'
        DataSource = DsTedCOcoSys
        Enabled = False
        TabOrder = 2
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 297
      Width = 761
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 368
        Top = 15
        Width = 391
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtIts: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Taxas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtItsClick
        end
        object BtCab: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Ocorr'#234'ncia'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object Panel2: TPanel
          Left = 282
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtContas: TBitBtn
          Tag = 10087
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Contas'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtContasClick
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 93
      Width = 761
      Height = 124
      Align = alTop
      DataSource = DsTedCOcoTxa
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Banco'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_BANCO'
          Title.Caption = 'Nome do banco'
          Width = 378
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cedente'
          Width = 167
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'NO_TxaTip'
          Title.Alignment = taCenter
          Title.Caption = 'Tipo'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TxaFat'
          Title.Caption = 'Fator'
          Width = 103
          Visible = True
        end>
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 109
    Width = 761
    Height = 361
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 761
      Height = 169
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 12
        Top = 20
        Width = 43
        Height = 13
        Caption = 'My Ocor:'
      end
      object Label9: TLabel
        Left = 69
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label8: TLabel
        Left = 12
        Top = 108
        Width = 131
        Height = 13
        Caption = 'Conta (do plano de contas):'
        Enabled = False
      end
      object SpeedButton5: TSpeedButton
        Left = 689
        Top = 124
        Width = 21
        Height = 21
        Caption = '...'
        Enabled = False
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 36
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 69
        Top = 36
        Width = 640
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPlaGen: TdmkEditCB
        Left = 12
        Top = 124
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PlaGen'
        UpdCampo = 'PlaGen'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPlaGen
        IgnoraDBLookupComboBox = False
      end
      object CBPlaGen: TdmkDBLookupComboBox
        Left = 68
        Top = 124
        Width = 617
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 4
        dmkEditCB = EdPlaGen
        QryCampo = 'PlaGen'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGRemRet: TdmkRadioGroup
        Left = 12
        Top = 60
        Width = 697
        Height = 45
        Caption = ' Tipo de envio: '
        Columns = 4
        Enabled = False
        ItemIndex = 0
        Items.Strings = (
          'Nenhum'
          'Remessa'
          'Retorno'
          'Ambos')
        TabOrder = 2
        QryCampo = 'RemRet'
        UpdCampo = 'RemRet'
        UpdType = utYes
        OldValor = 0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 298
      Width = 761
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 651
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 761
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 713
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 497
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 477
        Height = 32
        Caption = 'Ocorr'#234'ncias Banc'#225'rias de TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 477
        Height = 32
        Caption = 'Ocorr'#234'ncias Banc'#225'rias de TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 477
        Height = 32
        Caption = 'Ocorr'#234'ncias Banc'#225'rias de TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 761
    Height = 57
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 757
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 757
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object DsTedCOcoSys: TDataSource
    DataSet = QrTedCOcoSys
    Left = 164
    Top = 60
  end
  object QrTedCOcoSys: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTedCOcoSysBeforeOpen
    AfterOpen = QrTedCOcoSysAfterOpen
    BeforeClose = QrTedCOcoSysBeforeClose
    AfterScroll = QrTedCOcoSysAfterScroll
    SQL.Strings = (
      'SELECT cnt.Nome NO_PLAGEN, tos.* '
      'FROM tedcocosys tos'
      'LEFT JOIN contas cnt ON cnt.Codigo=tos.PlaGen')
    Left = 136
    Top = 60
    object QrTedCOcoSysNO_PLAGEN: TWideStringField
      FieldName = 'NO_PLAGEN'
      Size = 50
    end
    object QrTedCOcoSysCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTedCOcoSysNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrTedCOcoSysRemRet: TSmallintField
      FieldName = 'RemRet'
    end
    object QrTedCOcoSysPlaGen: TIntegerField
      FieldName = 'PlaGen'
    end
    object QrTedCOcoSysLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTedCOcoSysDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTedCOcoSysDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTedCOcoSysUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTedCOcoSysUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTedCOcoSysAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTedCOcoSysAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Panel2
    CanUpd01 = BtCab
    CanDel01 = BtIts
    Left = 192
    Top = 60
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 136
    Top = 92
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 164
    Top = 92
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 260
    Top = 388
    object CabInclui1: TMenuItem
      Caption = '&Inclui obrigat'#243'rios'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
    end
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 352
    Top = 388
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
      Enabled = False
    end
    object Nomeiagrupo1: TMenuItem
      Caption = '&Renomeia grupo'
      Enabled = False
    end
  end
  object QrTedCOcoTxa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ban.Nome NO_BANCO, '
      'ELT(tct.TxaTip + 1, "$", "%", "?") NO_TxaTip, '
      'tct.* '
      'FROM tedcocotxa tct'
      'LEFT JOIN bancos ban ON ban.Codigo=tct.Banco'
      'WHERE tct.Codigo=1'
      'ORDER BY tct.Banco, tct.Cedente')
    Left = 220
    Top = 60
    object QrTedCOcoTxaNO_BANCO: TWideStringField
      FieldName = 'NO_BANCO'
      Size = 100
    end
    object QrTedCOcoTxaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTedCOcoTxaBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrTedCOcoTxaCedente: TWideStringField
      FieldName = 'Cedente'
      Size = 30
    end
    object QrTedCOcoTxaTxaFat: TFloatField
      FieldName = 'TxaFat'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrTedCOcoTxaTxaTip: TSmallintField
      FieldName = 'TxaTip'
    end
    object QrTedCOcoTxaNO_TxaTip: TWideStringField
      FieldName = 'NO_TxaTip'
      Size = 1
    end
  end
  object DsTedCOcoTxa: TDataSource
    DataSet = QrTedCOcoTxa
    Left = 248
    Top = 60
  end
end
