unit TedCCfgCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, DmkDAC_PF,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Menus, Grids, DBGrids, dmkDBLookupComboBox, dmkEditCB, Variants, UnDmkProcFunc,
  UnDmkEnums;

type
  TFmTedCCfgCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrTedCCfgCab: TmySQLQuery;
    DsTedCCfgCab: TDataSource;
    QrTedCCfgIts: TmySQLQuery;
    DsTedCCfgIts: TDataSource;
    PMIts: TPopupMenu;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrTedCCfgCabCodigo: TIntegerField;
    QrTedCCfgCabBanco: TIntegerField;
    QrTedCCfgCabAnoMes: TIntegerField;
    QrTedCCfgCabVersao: TWideStringField;
    QrTedCCfgCabTamReg: TIntegerField;
    QrTedCCfgCabNome: TWideStringField;
    QrTedCCfgCabSeqLay: TIntegerField;
    QrTedCCfgCabRemRet: TIntegerField;
    QrTedCCfgCabLk: TIntegerField;
    QrTedCCfgCabDataCad: TDateField;
    QrTedCCfgCabDataAlt: TDateField;
    QrTedCCfgCabUserCad: TIntegerField;
    QrTedCCfgCabUserAlt: TIntegerField;
    QrTedCCfgCabAlterWeb: TSmallintField;
    QrTedCCfgCabAtivo: TSmallintField;
    QrTedCCfgItsCodigo: TIntegerField;
    QrTedCCfgItsRegistro: TIntegerField;
    QrTedCCfgItsSubReg: TIntegerField;
    QrTedCCfgItsPosIni: TIntegerField;
    QrTedCCfgItsPosFim: TIntegerField;
    QrTedCCfgItsPosTam: TIntegerField;
    QrTedCCfgItsCampo: TWideStringField;
    QrTedCCfgItsRestricao: TWideStringField;
    QrTedCCfgItsTamInt: TIntegerField;
    QrTedCCfgItsTamFlu: TIntegerField;
    QrTedCCfgItsFmtDta: TWideStringField;
    QrTedCCfgItsPreenchmto: TIntegerField;
    QrTedCCfgItsCodSis: TIntegerField;
    QrTedCCfgItsDefaultRem: TWideStringField;
    QrTedCCfgItsDefaultRet: TWideStringField;
    QrTedCCfgItsObrigatori: TSmallintField;
    QrTedCCfgItsCliPreench: TSmallintField;
    QrTedCCfgItsDescri01: TWideStringField;
    QrTedCCfgItsDescri02: TWideStringField;
    QrTedCCfgItsDescri03: TWideStringField;
    QrTedCCfgItsDescri04: TWideStringField;
    QrTedCCfgItsDescri05: TWideStringField;
    QrTedCCfgItsLk: TIntegerField;
    QrTedCCfgItsDataCad: TDateField;
    QrTedCCfgItsDataAlt: TDateField;
    QrTedCCfgItsUserCad: TIntegerField;
    QrTedCCfgItsUserAlt: TIntegerField;
    QrTedCCfgItsAlterWeb: TSmallintField;
    QrTedCCfgItsAtivo: TSmallintField;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    CkCliInfo: TCheckBox;
    QrTedCCfgCabEmpresa: TIntegerField;
    QrTedCCfgCabCartDep: TIntegerField;
    QrTedCCfgCabNO_ENT: TWideStringField;
    QrTedCCfgCabNO_CARTDEP: TWideStringField;
    Label3: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label4: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdEmpresa: TdmkEditCB;
    EdCartDep: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    CBCartDep: TdmkDBLookupComboBox;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrTedCCfgItsFulSize: TSmallintField;
    QrTedCCfgCabPathRem: TWideStringField;
    QrTedCCfgCabPathRet: TWideStringField;
    Label8: TLabel;
    DBEdit10: TDBEdit;
    Label10: TLabel;
    DBEdit11: TDBEdit;
    EdPathRem: TdmkEdit;
    EdPathRet: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    EdCedente: TdmkEdit;
    Label13: TLabel;
    Label14: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    QrTedCCfgCabCedente: TWideStringField;
    SpeedButton7: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTedCCfgCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTedCCfgCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTedCCfgCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure CkCliInfoClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraTedCCfgIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenTedCCfgIts(Registro, SubReg, PosIni: Integer);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmTedCCfgCab: TFmTedCCfgCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, TedCCfgIts, TedC_Tabs, TedC_Aux,
  UnFinanceiroJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTedCCfgCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTedCCfgCab.MostraTedCCfgIts(SQLType: TSQLType);
var
  Tam: Integer;
begin
  Tam := 0;
  if DBCheck.CriaFm(TFmTedCCfgIts, FmTedCCfgIts, afmoNegarComAviso) then
  begin
    FmTedCCfgIts.ImgTipo.SQLType := SQLType;
    FmTedCCfgIts.EdCampo.Text := QrTedCCfgItsCampo.Value;
    FmTedCCfgIts.EdDescri01.Text := QrTedCCfgItsDescri01.Value;
    FmTedCCfgIts.EdDescri02.Text := QrTedCCfgItsDescri02.Value;
    FmTedCCfgIts.EdDescri03.Text := QrTedCCfgItsDescri03.Value;
    FmTedCCfgIts.EdDescri04.Text := QrTedCCfgItsDescri04.Value;
    FmTedCCfgIts.EdDescri05.Text := QrTedCCfgItsDescri05.Value;
    if SQLType = stUpd then
    begin
      Tam := 0;
      if Uppercase(QrTedCCfgItsRestricao.Value) = 'X' then (*nada*)
      else
      if Uppercase(QrTedCCfgItsRestricao.Value) = 'D' then
        FmTedCCfgIts.EdDefaultRem.FormatType := dmktfInteger
      else
      if QrTedCCfgItsRestricao.Value = '9' then
      begin
        if QrTedCCfgItsTamFlu.Value > 0 then
        begin
          //Tam := 1; N�o tem virgula!
          FmTedCCfgIts.EdDefaultRem.DecimalSize := QrTedCCfgItsTamFlu.Value;
          FmTedCCfgIts.EdDefaultRem.FormatType := dmktfDouble;
        end;
        FmTedCCfgIts.EdDefaultRem.LeftZeros := QrTedCCfgItsPosTam.Value;
        FmTedCCfgIts.EdDefaultRem.FormatType := dmktfInt64;
      end;
      FmTedCCfgIts.EdDefaultRem.Text := QrTedCCfgItsDefaultRem.Value;
    end;
    FmTedCCfgIts.EdDefaultRem.MaxLength := QrTedCCfgItsPosTam.Value + Tam;
    FmTedCCfgIts.ShowModal;
    FmTedCCfgIts.Destroy;
  end;
end;

procedure TFmTedCCfgCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTedCCfgCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTedCCfgCab, QrTedCCfgIts);
end;

procedure TFmTedCCfgCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrTedCCfgIts);
end;

procedure TFmTedCCfgCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTedCCfgCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTedCCfgCab.DefParams;
begin
  VAR_GOTOTABELA := 'tedccfgcab';
  VAR_GOTOMYSQLTABLE := QrTedCCfgCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ');
  VAR_SQLx.Add('car.Nome NO_CARTDEP, tcc.*');
  VAR_SQLx.Add('FROM tedccfgcab tcc');
  VAR_SQLx.Add('LEFT JOIN enticliint eci ON eci.CodCliInt=tcc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=eci.CodEnti');
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo=tcc.CartDep');
  VAR_SQLx.Add('WHERE tcc.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND tcc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND tcc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND tcc.Nome Like :P0');
  //
end;

procedure TFmTedCCfgCab.ItsAltera1Click(Sender: TObject);
begin
  MostraTedCCfgIts(stUpd);
end;

procedure TFmTedCCfgCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmTedCCfgCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTedCCfgCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTedCCfgCab.ReopenTedCCfgIts(Registro, SubReg, PosIni: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTedCCfgIts, Dmod.MyDB, [
  'SELECT * FROM TedCCfgIts ',
  'WHERE Codigo=' + Geral.FF0(QrTedCCfgCabCodigo.Value),
  Geral.ATS_if(CkCliInfo.Checked, ['AND CliPreench=1']),
  'ORDER BY Registro, SubReg, PosIni',
  '']);
  //
  QrTedCCfgIts.Locate('Registro;SubReg;PosIni', VarArrayOf([Registro, SubReg, PosIni]), []);
end;


procedure TFmTedCCfgCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTedCCfgCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTedCCfgCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTedCCfgCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTedCCfgCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTedCCfgCab.SpeedButton5Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdPathRem);
end;

procedure TFmTedCCfgCab.SpeedButton6Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdPathRet);
end;

procedure TFmTedCCfgCab.SpeedButton7Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdCartDep.ValueVariant;
  //
  FinanceiroJan.CadastroDeCarteiras(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrCarteiras, Dmod.MyDB);
    //
    EdCartDep.ValueVariant := VAR_CADASTRO;
    CBCartDep.KeyValue     := VAR_CADASTRO;
    EdCartDep.SetFocus;
  end;
end;

procedure TFmTedCCfgCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTedCCfgCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTedCCfgCabCodigo.Value;
  Close;
end;

procedure TFmTedCCfgCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTedCCfgCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'tedccfgcab');
end;

procedure TFmTedCCfgCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Cedente, Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then
    Exit;
  //
  Cedente := EdCedente.ValueVariant;
  if MyObjects.FIC(Length(Cedente) = 0, EdCedente,
  'Informe o c�digo da empresa no banco!') then
    Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('tedccfgcab', 'Codigo', ImgTipo.SQLType,
    QrTedCCfgCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmTedCCfgCab, PnEdita,
    'tedccfgcab', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmTedCCfgCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'tedccfgcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'tedccfgcab', 'Codigo');
end;

procedure TFmTedCCfgCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTedCCfgCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTedCCfgCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  UMyMod.AbreQuery(QrCarteiras, Dmod.MyDB);
end;

procedure TFmTedCCfgCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTedCCfgCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTedCCfgCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTedCCfgCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTedCCfgCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTedCCfgCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTedCCfgCab.QrTedCCfgCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTedCCfgCab.QrTedCCfgCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTedCCfgIts(0, 0, 0);
end;

procedure TFmTedCCfgCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTedCCfgCabCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmTedCCfgCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTedCCfgCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'tedccfgcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTedCCfgCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTedCCfgCab.CabInclui1Click(Sender: TObject);
const
  Lista: array[0..0] of String = (
  '422. Banco Safra - produto 900 - de abril de 2007'
  );
var
  Layout, Codigo: Integer;
  Criou: Boolean;
  Texto: String;
begin
  Criou := False;
{
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTedCCfgCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'tedccfgcab');
}
  Layout := MyObjects.SelRadioGroup('Layout de envio de cheques',
  'Selecione o layout', Lista, 1);
  if Layout > -1 then
  begin
    Texto := Lista[Layout];
    if Geral.MensagemBox('Deseja realmente criar o layout:' + sLineBreak +
    '"' + Texto + '" ?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_Yes then
    begin
      Codigo :=
        UMyMod.BPGS1I32('tedccfgcab', 'Codigo', '', '', tsPos, stIns, 0);
      //
      case Layout of
        0: Criou := TedC_Ax.SQL_422_2007_04_300(Codigo);
      end;
      //
      if Criou then
      begin
        Geral.MensagemBox('Layout ' + Geral.FF0(Codigo) + ' criado com sucesso!',
        'Mensagem', MB_OK+MB_ICONWARNING);
        LocCod(Codigo, Codigo);
      end;
    end;
  end;
end;

procedure TFmTedCCfgCab.CkCliInfoClick(Sender: TObject);
begin
  ReopenTedCCfgIts(0, 0, 0);
end;

procedure TFmTedCCfgCab.QrTedCCfgCabBeforeOpen(DataSet: TDataSet);
begin
  QrTedCCfgCabCodigo.DisplayFormat := FFormatFloat;
end;

{ TODO :   AA :: URGENTE! Colocar dados da tabela tedccfgset. }

end.

