unit ModuleTedC;

interface

uses
  Windows, SysUtils, Messages, Dialogs, Controls, Classes, DB, mySQLDbTables,
  dmkGeral, StdCtrls, frxClass, frxDBSet, DmkDAC_PF, UnDmkEnums;

type
  TDmTedC = class(TDataModule)
    QrTCCCab: TmySQLQuery;
    QrTCCCabCodigo: TIntegerField;
    QrTCCCabBanco: TIntegerField;
    QrTCCCabAnoMes: TIntegerField;
    QrTCCCabVersao: TWideStringField;
    QrTCCCabTamReg: TIntegerField;
    QrTCCCabNome: TWideStringField;
    QrTCCCabSeqLay: TIntegerField;
    QrTCCCabRemRet: TIntegerField;
    QrTCCCabEmpresa: TIntegerField;
    QrTCCCabCartDep: TIntegerField;
    QrTCCSet: TmySQLQuery;
    QrTCCSetValBco: TWideStringField;
    QrTCCIts: TmySQLQuery;
    QrTCCItsCodigo: TIntegerField;
    QrTCCItsRegistro: TIntegerField;
    QrTCCItsSubReg: TIntegerField;
    QrTCCItsSegmento: TWideStringField;
    QrTCCItsSubSeg: TIntegerField;
    QrTCCItsPosIni: TIntegerField;
    QrTCCItsPosFim: TIntegerField;
    QrTCCItsPosTam: TIntegerField;
    QrTCCItsCampo: TWideStringField;
    QrTCCItsRestricao: TWideStringField;
    QrTCCItsTamInt: TIntegerField;
    QrTCCItsTamFlu: TIntegerField;
    QrTCCItsFmtDta: TWideStringField;
    QrTCCItsPreenchmto: TIntegerField;
    QrTCCItsCodSis: TIntegerField;
    QrTCCItsDefaultRem: TWideStringField;
    QrTCCItsDefaultRet: TWideStringField;
    QrTCCItsObrigatori: TSmallintField;
    QrTCCItsCliPreench: TSmallintField;
    QrTCCItsDescri01: TWideStringField;
    QrTCCItsDescri02: TWideStringField;
    QrTCCItsDescri03: TWideStringField;
    QrTCCItsDescri04: TWideStringField;
    QrTCCItsDescri05: TWideStringField;
    QrTCCItsFulSize: TSmallintField;
    QrTCCItsInvertAlin: TSmallintField;
    QrTCCItsCortaStr: TSmallintField;
    QrEmpresa: TmySQLQuery;
    QrLocLct1: TmySQLQuery;
    QrLocLct1Controle: TIntegerField;
    QrTCTCab: TmySQLQuery;
    QrTCTCabCodigo: TIntegerField;
    QrTCTCabStep: TIntegerField;
    QrTCMI: TmySQLQuery;
    QrTCMILoReCaPrev: TIntegerField;
    QrTCMILoReItPrev: TIntegerField;
    QrTCMILoReCaProc: TIntegerField;
    QrTCMILoReItProc: TIntegerField;
    QrTCMILoReMyOcor: TIntegerField;
    QrTCMIControle: TIntegerField;
    QrTCTI: TmySQLQuery;
    QrTCTICodigo: TIntegerField;
    QrTCTILinha: TIntegerField;
    QrTCTISeuNumero: TLargeintField;
    QrTCTIRemItsCtrl: TIntegerField;
    QrTCTILctCtrl: TIntegerField;
    QrTCTILctSub: TIntegerField;
    QrTCTILctFatID: TIntegerField;
    QrTCTILctFatNum: TLargeintField;
    QrTCTILctFatParc: TIntegerField;
    QrLocLct1DDeposito: TDateField;
    QrLocLct1Compensado: TDateField;
    QrLocLct1Sit: TIntegerField;
    QrTCTIMyOcorr: TIntegerField;
    QrTedCRetIts: TmySQLQuery;
    QrTedCRetCab: TmySQLQuery;
    QrTedCRetCabCodigo: TIntegerField;
    QrTedCRetCabEmpresa: TIntegerField;
    QrTedCRetCabTedC_Cfg: TIntegerField;
    QrTedCRetCabStep: TIntegerField;
    QrTedCRetCabProduto: TWideStringField;
    QrTedCRetCabCedente: TWideStringField;
    QrTedCRetCabFilial: TWideStringField;
    QrTedCRetCabCNPJCPF: TWideStringField;
    QrTedCRetCabBanco: TIntegerField;
    QrTedCRetCabDataGer: TDateField;
    QrTedCRetCabNumSeqGer: TIntegerField;
    QrTedCRetCabDepositari: TWideStringField;
    QrTedCRetCabTerceira: TWideStringField;
    QrTedCRetCabCartQtd: TIntegerField;
    QrTedCRetCabCartVal: TFloatField;
    QrTedCRetCabEntrQtd: TIntegerField;
    QrTedCRetCabEntrVal: TFloatField;
    QrTedCRetCabDepoQtd: TIntegerField;
    QrTedCRetCabDepoVal: TFloatField;
    QrTedCRetCabBaxaQtd: TIntegerField;
    QrTedCRetCabBaxaVal: TFloatField;
    QrTedCRetCabDevlQtd: TIntegerField;
    QrTedCRetCabDevlVal: TFloatField;
    QrTedCRetCabReapQtd: TIntegerField;
    QrTedCRetCabReapVal: TFloatField;
    QrTedCRetCabUltLin: TIntegerField;
    QrTedCRetCabNome: TWideStringField;
    QrTedCRetCabItens: TIntegerField;
    QrTedCRetItsCodigo: TIntegerField;
    QrTedCRetItsLinha: TIntegerField;
    QrTedCRetItsLctCtrl: TIntegerField;
    QrTedCRetItsLctSub: TIntegerField;
    QrTedCRetItsLctFatID: TIntegerField;
    QrTedCRetItsLctFatNum: TLargeintField;
    QrTedCRetItsLctFatParc: TIntegerField;
    QrTedCRetItsOcorrBco: TIntegerField;
    QrTedCRetItsMyOcorr: TIntegerField;
    QrTedCRetItsStep: TSmallintField;
    QrTedCRetItsCedente: TWideStringField;
    QrTedCRetItsFilial: TWideStringField;
    QrTedCRetItsUsoEmpresa: TWideStringField;
    QrTedCRetItsDtaCaptura: TDateField;
    QrTedCRetItsCMC7: TWideStringField;
    QrTedCRetItsRemValor: TFloatField;
    QrTedCRetItsRemDataBoa: TDateField;
    QrTedCRetItsBorderoBco: TIntegerField;
    QrTedCRetItsSeuNumero: TLargeintField;
    QrTedCRetItsAlinea: TIntegerField;
    QrTedCRetItsCodRejeito: TIntegerField;
    QrTedCRetItsNossoNum: TLargeintField;
    QrTedCRetItsRetValor: TFloatField;
    QrTedCRetItsRetDataBoa: TDateField;
    QrTedCRetItsTrfCedente: TWideStringField;
    QrTedCRetItsTrfDtaOper: TDateField;
    QrTedCRetItsTrfContrat: TWideStringField;
    QrTedCRetItsTrfParcela: TIntegerField;
    QrTedCRetItsTrfDtaCntr: TDateField;
    QrTedCRetItsBanco: TIntegerField;
    QrTedCRetItsAgencia: TIntegerField;
    QrTedCRetItsContaCorre: TWideStringField;
    QrTedCRetItsCheque: TIntegerField;
    QrTedCRetItsPraca: TIntegerField;
    QrTedCRetItsRetInscTip: TWideStringField;
    QrTedCRetItsRetInscNum: TWideStringField;
    QrTedCRetItsRetEmitNom: TWideStringField;
    QrTedCRetItsRemItsCtrl: TIntegerField;
    QrTedCRetItsInconsiste: TIntegerField;
    frxTedCRet: TfrxReport;
    frxTedCRetCab: TfrxDBDataset;
    frxTedCRetIts: TfrxDBDataset;
    QrTedCRetItsNO_MyOcorr: TWideStringField;
    QrTedCRetItsNO_Inconsiste: TWideStringField;
    QrTedCRetItsTEXTO_PARA_USR: TWideStringField;
    QrStepI: TmySQLQuery;
    QrStepIItens: TLargeintField;
    QrStepIStep9: TFloatField;
    QrLocLct1TedRem_Sit: TSmallintField;
    QrLocLct1TedRem_Its: TIntegerField;
    QrLocLct1TedRetPrev: TIntegerField;
    QrLocLct1TedRetExeC: TIntegerField;
    QrLocLct1TedRetExeS: TIntegerField;
    QrLocLct1TCRI_Lot: TIntegerField;
    QrTCTIControle: TIntegerField;
    QrJaTah: TmySQLQuery;
    QrJaTahCodigo: TIntegerField;
    QrCustodia: TmySQLQuery;
    QrCustodiaCredito: TFloatField;
    QrCustodiaItens: TLargeintField;
    QrLastRet: TmySQLQuery;
    QrLastRetValor: TFloatField;
    QrCHEnvi: TmySQLQuery;
    QrCHEnviItens: TLargeintField;
    QrCHEnviValor: TFloatField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrTedCRetCabCalcFields(DataSet: TDataSet);
    procedure QrTedCRetCabAfterScroll(DataSet: TDataSet);
    procedure QrTedCRetCabBeforeClose(DataSet: TDataSet);
    procedure QrTedCRetItsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    function  ReopenLocLct1(LctCtrl, LctSub, FatID: Integer; FatNum: Double;
              FatParcela: Integer; TabLctA: String): Boolean;
    function  ReopenTCCIts(Layout, Registro, Subreg: Integer;
              Segmento: String; SubSeg: Integer): Boolean;
    function  ReopenTCTCab(Produto, Cedente, Filial, CNPJCPF: String; Banco,
              NumSeqGer: Integer; DataGer: TDateTime): Boolean;
    procedure ReopenCustodia();
    procedure ReopenLastRet(Empresa, Banco: Integer;
              (*Produto, Filial, CNPJCPF,*) Cedente: String);
    procedure ReopenCHEnvi(Codigo: Integer);
    //
    procedure TedCCfgSys_MostraTexto(CodSis: Integer; TxtPre, TxtPos:
              String; LaAviso1, LaAviso2: TLabel; var NoErro: String);
    function  TedCCfgSys_ObtemNome(CodSis: Integer): String;
    procedure MostraLoteRetorno(TedCRetCab: Integer);
    procedure MostraLoteRetorno_Abre(TedCRetCabs: String);
    procedure AtualizaStepRetorno(Codigo: Integer);
    function  AtualizaLoteTedEnvio(Codigo, Empresa, Banco: Integer;(*; Produto,
              Filial, CNPJCPF,*)Cedente: String): Boolean;
  end;

var
  DmTedC: TDmTedC;

implementation

uses UnMyObjects, Module, UMySQLModule, UnMLAGeral, TedC_Aux, MyListas;

{$R *.dfm}

function TDmTedC.AtualizaLoteTedEnvio(Codigo, Empresa, Banco: Integer;
(*; Produto, Filial, CNPJCPF,*) Cedente: String): Boolean;
  procedure RelatorioDeChequesEmCustodia;
  begin
{ TODO : Fazer relat�rio de cheques em cust�dia! }
  end;
var
  CartQtd, EntrQtd: Integer;
  CartVal, EntrVal: Double;
  Mensagem: String;
begin
  Result := False;
  ReopenCustodia();
  ReopenLastRet(Empresa, Banco, Cedente);
  if (QrCustodiaItens.Value > 0) or (QrLastRet.RecordCount > 0) then
  begin
    if Trunc(QrCustodiaCredito.Value * 100) <> Trunc(QrLastRetValor.Value * 100) then
    begin
      Mensagem := 'Erro! Valor de cust�dia no aplicativo: $ ' + Geral.FFT(
      QrCustodiaCredito.Value, 2, siNegativo) + sLineBreak +
      'N�o confere com o valor do �ltimo lote de retorno do banco: $ ' +
      Geral.FFT(QrLastRetValor.Value, 2, siNegativo) + sLineBreak +
      'Verifique se n�o h� retorno do banco para ser carregado no aplicativo!';
      //
      MLAGeral.MessageDlgCheck(
      Mensagem, mtConfirmation, [mbOK], 0, mrOK, True, True,
      'Desejo visualizar o relat�rio de cheques em cust�dia no sistema.',
      @RelatorioDeChequesEmCustodia);
    end;
  end;
  ReopenCHEnvi(Codigo);
  //
  CartQtd        := QrCustodiaItens.Value;
  CartVal        := QrCustodiaCredito.Value;
  EntrQtd        := QrCHEnviItens.Value;
  EntrVal        := QrCHEnviValor.Value;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcremcab', False, [
  'CartQtd', 'CartVal', 'EntrQtd', 'EntrVal'], [
  'Codigo'], [
  CartQtd, CartVal, EntrQtd, EntrVal], [
  Codigo], True);
end;

procedure TDmTedC.AtualizaStepRetorno(Codigo: Integer);
var
  Step: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStepI, Dmod.MyDB, [
  'SELECT COUNT(Linha) Itens, SUM(IF(Step=9, 1, 0) ) Step9 ',
  'FROM tedcretits ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  if QrStepIItens.Value = Trunc(QrStepIStep9.Value) then
  begin
    Step := CO_TEDC_STEP_ARQ_ITENS_PROCESSADOS; // = 10
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcretcab', False, [
    'Step'], ['Codigo'], [Step], [Codigo], True);
  end;
end;

procedure TDmTedC.DataModuleCreate(Sender: TObject);
var
  Compo: TComponent;
  Query: TmySQLQuery;
  I: Integer;
//  Mensagem: WideString;
begin
  for I := 0 to DmTedC.ComponentCount - 1 do
  begin
    Compo := DmTedC.Components[I];
    if Compo is TmySQLQuery then
    begin
      Query := TmySQLQuery(Compo);
      {if Query.Name = 'QrUpdPID1' then
        Query.Database := MyPID_DB
      else}
        Query.Database := Dmod.MyDB;
    end;
  end;
end;

procedure TDmTedC.MostraLoteRetorno(TedCRetCab: Integer);
begin
  MostraLoteRetorno_Abre(Geral.FF0(TedCRetCab));
end;

procedure TDmTedC.MostraLoteRetorno_Abre(TedCRetCabs: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTedCRetCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM tedcretcab ',
  'WHERE Codigo IN (' + TedCRetCabs + ')',
  '']);
  //
  MyObjects.frxMostra(frxTedCRet, 'Arquivo Retorno de TED Cheque');
end;

procedure TDmTedC.QrTedCRetCabAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTedCRetIts, Dmod.MyDB, [
  'SELECT tcri.*, tcos.Nome NO_MyOcorr, ',
  'tinc.Nome NO_Inconsiste ',
  'FROM tedcretits tcri ',
  'LEFT JOIN tedcocosys tcos ON tcos.Codigo=tcri.MyOcorr ',
  'LEFT JOIN tedcretinc tinc ON tinc.Codigo=tcri.Inconsiste ',
  'WHERE tcri.Codigo=' + Geral.FF0(QrTedCRetCabCodigo.Value),
  '']);
end;

procedure TDmTedC.QrTedCRetCabBeforeClose(DataSet: TDataSet);
begin
  QrTedCRetIts.Close;
end;

procedure TDmTedC.QrTedCRetCabCalcFields(DataSet: TDataSet);
begin
  //case QrTedCRetItsRetInscTip.Value
end;

procedure TDmTedC.QrTedCRetItsCalcFields(DataSet: TDataSet);
begin
{
  case QrTedCRetItsStep.Value of
    (*0*)CO_TEDC_STEP_ITEM_ABERTO       : QrTedCRetItsTEXTO_PARA_USR.Value := 'Item de retorno n�o processado';
    (*1*)CO_TEDC_STEP_ITEM_INCONSISTENTE: QrTedCRetItsTEXTO_PARA_USR.Value := QrTedCRetItsNO_Inconsiste.Value;
    (*9*)CO_TEDC_STEP_ITEM_ENCERRADO    : QrTedCRetItsTEXTO_PARA_USR.Value := 'Item OK';
  end;
  if QrTedCRetItsTEXTO_PARA_USR.Value = '' then
    QrTedCRetItsTEXTO_PARA_USR.Value := 'Inconsist�ncia n�o implementada!';
}
  QrTedCRetItsTEXTO_PARA_USR.Value := TedC_Ax.TextoParaUser(
    QrTedCRetItsStep.Value,
    QrTedCRetItsNO_Inconsiste.Value);
end;

procedure TDmTedC.ReopenCHEnvi(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCHEnvi, Dmod.MyDB, [
  'SELECT COUNT(Controle) Itens, SUM(Valor) Valor ',
  'FROM tedcremits ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  // Bruno do Safra(Protocolo 122856) falou que deve ser informado a soma de todos cheques
  //'AND MyOcorr=' + Geral.FF0(CO_MYOCORR_TEDC_ENVIO_AO_BANCO);
  '']);
end;

procedure TDmTedC.ReopenCustodia();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCustodia, Dmod.MyDB, [
  'SELECT COUNT(Controle) Itens, SUM(Credito) Credito ',
  'FROM ' + CO_TabLctA,
  'WHERE TedRem_Sit IN (' + CO_SIT_TEDC_ESTA_NO_BANCO + ') ',
  '']);
end;

procedure TDmTedC.ReopenLastRet(Empresa, Banco: Integer;
(*; Produto, Filial, CNPJCPF*)Cedente: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLastRet, Dmod.MyDB, [
  'SELECT (CartVal + EntrVal) Valor ',
  'FROM tedcretcab ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
(*
  'AND Produto="' + Produto + '" ',
*)
  'AND Cedente="' + Cedente + '" ',
(*  'AND Filial="' + Filial + '" ',
  'AND CNPJCPF="' + CNPJCPF + '" ',
*)
  'AND Banco=' + Geral.FF0(Banco),
  'ORDER BY DataGer DESC ',
  'LIMIT 0,1 ',
  '']);
end;

function TDmTedC.ReopenLocLct1(LctCtrl, LctSub, FatID: Integer; FatNum: Double;
  FatParcela: Integer; TabLctA: String): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocLct1, Dmod.MyDB, [
  'SELECT lct.Controle, lct.TedRem_Sit, lct.TedRem_Its, ',
  'lct.TedRetPrev, lct.TedRetExeC, lct.TedRetExeS, ',
  'lct.DDeposito, lct.Compensado, lct.Sit, ',
  'tcri.Controle TCRI_Lot ',
  'FROM ' + TabLctA + ' lct ',
  'LEFT JOIN tedcretits tcri ON tcri.Controle=lct.TedRetExeC ',
  'WHERE lct.Controle=' + Geral.FF0(LctCtrl),
  'AND lct.Sub=' + Geral.FF0(LctSub),
  'AND lct.FatID=' + Geral.FF0(FatID),
  'AND lct.FatNum=' + Geral.FF0(Trunc(FatNum)),
  'AND lct.FatParcela=' + Geral.FF0(FatParcela),
  '']);
  //
  Result := True;
end;

function TDmTedC.ReopenTCCIts(Layout, Registro, Subreg: Integer;
  Segmento: String; SubSeg: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTCCIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM tedccfgits ',
  'WHERE Codigo=' + Geral.FF0(Layout),
  'AND Registro=' + Geral.FF0(Registro),
  'AND SubReg=' + Geral.FF0(Subreg),
  'AND Segmento="' + Segmento + '" ',
  'AND SubSeg=' + Geral.FF0(SubSeg),
  'ORDER BY Registro, SubReg, PosIni ',
  '']);
  //
  Result := QrTCCIts.RecordCount > 0;
  if not Result then
  begin
    Geral.MensagemBox(
    'Configura��o (layout) TED Cheque sem registro "HEADER" definido! ',
    'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
end;

function TDmTedC.ReopenTCTCab(Produto, Cedente, Filial, CNPJCPF: String; Banco,
  NumSeqGer: Integer; DataGer: TDateTime): Boolean;
begin
  Result := UnDmkDAC_PF.AbreMySQLQuery0(QrTCTCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM tedcretcab ',
  'WHERE  Produto="' + Produto + '" ',
  'AND Cedente="' + Cedente + '" ',
  'AND Filial="' + Filial + '" ',
  'AND CNPJCPF="' + CNPJCPF + '" ',
  'AND Banco=' + Geral.FF0(Banco),
  'AND NumSeqGer=' + Geral.FF0(NumSeqGer),
  'AND DataGer="' + Geral.FDT(DataGer, 1) + '" ',
  '']);
end;


procedure TDmTedC.TedCCfgSys_MostraTexto(CodSis: Integer; TxtPre,
  TxtPos: String; LaAviso1, LaAviso2: TLabel; var NoErro: String);
var
  Aviso: String;
begin
  if CodSis = 0 then
  begin
    NoErro := '';
    Aviso := '...';
  end else
  begin
    NoErro := TedCCfgSys_ObtemNome(CodSis);
    Aviso := TxtPre + 'ERRO EM: ' + NoErro + ' ' + TxtPos;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Aviso);
end;

function TDmTedC.TedCCfgSys_ObtemNome(CodSis: Integer): String;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.DataBase := Dmod.MyDB;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT Nome FROM tedccfgsys WHERE Codigo=' + Geral.FF0(CodSis));
    Qry. Open;
    if Qry.RecordCount = 0 then
      Result := 'N�o existe cadastro do c�digo ' + Geral.FF0(CodSis) +
      ' na tabela "TedCCfgSys"!'
    else
      Result := Qry.FieldByName('Nome').AsString;
  finally
    Qry.Free;
  end;
end;

end.
