unit TedCCfgIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask;

type
  TFmTedCCfgIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    QrEmiSac: TmySQLQuery;
    QrEmiSacNome: TWideStringField;
    QrEmiSacCPF: TWideStringField;
    QrEmiSacTipo: TWideStringField;
    DsEmiSac: TDataSource;
    QrLoc: TmySQLQuery;
    QrLocCNPJ_CPF: TWideStringField;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    EdDescri01: TdmkEdit;
    EdDescri02: TdmkEdit;
    EdDescri03: TdmkEdit;
    EdDescri04: TdmkEdit;
    EdDescri05: TdmkEdit;
    EdCampo: TdmkEdit;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    EdDefaultRem: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmTedCCfgIts: TFmTedCCfgIts;

implementation

uses UnMyObjects, Module, UMySQLModule, TedCCfgCab;

{$R *.DFM}

procedure TFmTedCCfgIts.BtOKClick(Sender: TObject);
var
  Codigo, Registro, SubReg, PosIni: Integer;
  DefaultRem: String;
begin
  Codigo := FmTedCCfgCab.QrTedCCfgItsCodigo.Value;
  Registro := FmTedCCfgCab.QrTedCCfgItsRegistro.Value;
  SubReg := FmTedCCfgCab.QrTedCCfgItsSubReg.Value;
  PosIni := FmTedCCfgCab.QrTedCCfgItsPosIni.Value;
  DefaultRem := EdDefaultRem.Text;
  //
  if MyObjects.FIC(
  (FmTedCCfgCab.QrTedCCfgItsFulSize.Value = 1) and
  (Length(DefaultRem) <> EdDefaultRem.MaxLength), EdDefaultRem,
  'O campo deve ter ' + Geral.FF0(EdDefaultRem.MaxLength) + ' caracteres!') then
    Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'tedccfgits', False, [
  'DefaultRem'], ['Codigo', 'Registro', 'Subreg', 'PosIni'], [
  DefaultRem], [Codigo, Registro, Subreg, PosIni], False) then
  begin
    FmTedCCfgCab.ReopenTedCCfgIts(Registro, SubReg, PosIni);
    Close;
  end;
end;

procedure TFmTedCCfgIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTedCCfgIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTedCCfgIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
