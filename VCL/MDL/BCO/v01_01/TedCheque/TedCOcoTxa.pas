unit TedCOcoTxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, UnDmkEnums;

type
  TFmTedCOcoTxa = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    QrBancos: TmySQLQuery;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    DsBancos: TDataSource;
    Label8: TLabel;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    EdCedente: TdmkEdit;
    Label1: TLabel;
    RGTxaTip: TdmkRadioGroup;
    EdTxaFat: TdmkEdit;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label3: TLabel;
    EdOldBco: TdmkEdit;
    EdOldCeden: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmTedCOcoTxa: TFmTedCOcoTxa;

implementation

uses UnMyObjects, Module, GruSacEmi, PesqCPFCNPJ, UMySQLModule, TedCOcoSys,
  Bancos, MyDBCheck;

{$R *.DFM}

procedure TFmTedCOcoTxa.BtOKClick(Sender: TObject);
var
  Cedente, OldCeden: String;
  Codigo, Banco, TxaTip, OldBco: Integer;
  TxaFat: Double;
  Continua: Boolean;
begin
  Codigo         := EdCodigo.ValueVariant;
  OldBco         := EdOldBco.ValueVariant;
  OldCeden       := EdOldCeden.Text;
  //
  Banco          := EdBanco.ValueVariant;
  Cedente        := EdCedente.Text;
  TxaFat         := EdTxaFat.ValueVariant;
  TxaTip         := RGTxaTip.ItemIndex;
  //
  if ImgTipo.SQLType = stUpd then
  begin
    Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcocotxa', False, [
    'Codigo', 'Banco', 'Cedente',
    'TxaFat', 'TxaTip'], [
    'Codigo', 'Banco', 'Cedente'], [
    Codigo, Banco, Cedente,
    TxaFat, TxaTip], [
    Codigo, OldBco, OldCeden], True);
  end else begin
    Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'tedcocotxa', False, [
    'TxaFat', 'TxaTip'], [
    'Codigo', 'Banco', 'Cedente'], [
    TxaFat, TxaTip], [
    Codigo, Banco, Cedente], True);
  end;
  if Continua then
  begin
    FmTedCOcoSys.ReopenTedCOcoTxa(Banco, Cedente);
    if CkContinuar.Checked then
    begin
      EdOldBco.ValueVariant := 0;
      EdOldCeden.Text := '';

      EdBanco.ValueVariant := 0;
      EdCedente.Text := '';
      EdTxaFat.ValueVariant := 0;
      //RGTxaTip.ItemIndex := ?;
      EdBanco.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmTedCOcoTxa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTedCOcoTxa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTedCOcoTxa.FormCreate(Sender: TObject);
begin
  UMyMod.AbreQuery(QrBancos, Dmod.MyDB);
end;

procedure TFmTedCOcoTxa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTedCOcoTxa.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdBanco, CBBanco, QrBancos, VAR_CADASTRO);
  end;
end;

end.
