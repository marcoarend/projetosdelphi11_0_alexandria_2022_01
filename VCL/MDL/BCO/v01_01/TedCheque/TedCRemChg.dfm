object FmTedCRemChg: TFmTedCRemChg
  Left = 339
  Top = 185
  Caption = 'TED-CHQUE-104 :: Envia Instru'#231#227'o em Lote TED'
  ClientHeight = 477
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 355
        Height = 32
        Caption = 'Envia Instru'#231#227'o em Lote TED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 355
        Height = 32
        Caption = 'Envia Instru'#231#227'o em Lote TED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 355
        Height = 32
        Caption = 'Envia Instru'#231#227'o em Lote TED'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 315
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 315
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 315
        Align = alClient
        Caption = ' Localiza'#231#227'o do cheque pelos seus dados: '
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 54
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object EdTipific: TdmkEdit
            Left = 8
            Top = 24
            Width = 37
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '5'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 5
            ValWarn = False
            OnChange = CkTipoClick
          end
          object EdRegiaoCompe: TdmkEdit
            Left = 52
            Top = 24
            Width = 49
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = CkTipoClick
          end
          object EdBanco: TdmkEdit
            Left = 108
            Top = 24
            Width = 49
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = CkTipoClick
          end
          object EdAgencia: TdmkEdit
            Left = 160
            Top = 24
            Width = 57
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 4
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = CkTipoClick
          end
          object EdConta: TdmkEdit
            Left = 220
            Top = 24
            Width = 97
            Height = 21
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = CkTipoClick
          end
          object EdCheque: TdmkEdit
            Left = 324
            Top = 24
            Width = 57
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = CkTipoClick
          end
          object CkTipo: TCheckBox
            Left = 8
            Top = 4
            Width = 41
            Height = 17
            Caption = 'Tipo'
            TabOrder = 6
            OnClick = CkTipoClick
          end
          object CkPraca: TCheckBox
            Left = 52
            Top = 4
            Width = 49
            Height = 17
            Caption = 'Pra'#231'a'
            TabOrder = 7
            OnClick = CkTipoClick
          end
          object CkBanco: TCheckBox
            Left = 108
            Top = 4
            Width = 61
            Height = 17
            Caption = 'Banco'
            Checked = True
            State = cbChecked
            TabOrder = 8
            OnClick = CkTipoClick
          end
          object CkAgencia: TCheckBox
            Left = 160
            Top = 4
            Width = 57
            Height = 17
            Caption = 'Ag'#234'ncia'
            Checked = True
            State = cbChecked
            TabOrder = 9
            OnClick = CkTipoClick
          end
          object CkContaCorrente: TCheckBox
            Left = 220
            Top = 4
            Width = 97
            Height = 17
            Caption = 'Conta corrente'#185
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 10
            OnClick = CkTipoClick
          end
          object CkCheque: TCheckBox
            Left = 324
            Top = 4
            Width = 61
            Height = 17
            Caption = 'Cheque'
            Checked = True
            State = cbChecked
            TabOrder = 11
            OnClick = CkTipoClick
          end
          object BtPesquisa: TBitBtn
            Tag = 18
            Left = 900
            Top = 3
            Width = 100
            Height = 40
            Cursor = crHandPoint
            Caption = '&Pesquisar'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 12
            OnClick = BtPesquisaClick
          end
          object CkCNPJCPF: TCheckBox
            Left = 384
            Top = 4
            Width = 89
            Height = 17
            Caption = 'CNPJ / CPF'
            TabOrder = 13
            OnClick = CkTipoClick
          end
          object EdCPF: TdmkEdit
            Left = 384
            Top = 24
            Width = 112
            Height = 21
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 14
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = CkTipoClick
          end
          object CkEmitente: TCheckBox
            Left = 500
            Top = 4
            Width = 153
            Height = 17
            Caption = 'Emitente'#185
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 15
            OnClick = CkTipoClick
          end
          object EdEmitente: TdmkEdit
            Left = 500
            Top = 24
            Width = 177
            Height = 21
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 16
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = CkTipoClick
          end
          object CkSoEnviados: TCheckBox
            Left = 684
            Top = 24
            Width = 213
            Height = 17
            Caption = 'Somente cheques j'#225' enviados ao banco.'
            Checked = True
            State = cbChecked
            TabOrder = 17
          end
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 125
          Width = 1004
          Height = 111
          Align = alClient
          DataSource = DsLct
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Tipific'
              Title.Caption = 'Tipo'
              Width = 25
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Praca'
              Title.Caption = 'Pra'#231'a'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco'
              Width = 37
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag'#234'nc.'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ContaCorrente'
              Title.Caption = 'Conta corrente'
              Width = 69
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Title.Caption = 'Cheque'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Valor'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'Dep'#243'sito'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJCPF'
              Title.Caption = 'CNPJ / CPF'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Title.Caption = 'Nome do emitente'
              Width = 203
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TedC_Lot'
              Title.Caption = 'Lote TED '#178
              Title.Font.Charset = ANSI_CHARSET
              Title.Font.Color = clRed
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TedC_Sit'
              Title.Caption = 'Situa'#231#227'o do cheque no banco'
              Width = 600
              Visible = True
            end>
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 236
          Width = 1004
          Height = 77
          ActivePage = TabSheet1
          Align = alBottom
          TabOrder = 2
          OnChanging = PageControl1Changing
          object TabSheet1: TTabSheet
            Caption = ' Altera'#231#227'o da data boa para dep'#243'sito'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 49
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label2: TLabel
                Left = 8
                Top = 20
                Width = 111
                Height = 13
                Caption = 'Nova data de dep'#243'sito:'
              end
              object SpeedButton2: TSpeedButton
                Left = 240
                Top = 16
                Width = 23
                Height = 22
                Caption = '>'
                OnClick = SpeedButton2Click
              end
              object TPDataBoa: TdmkEditDateTimePicker
                Left = 124
                Top = 16
                Width = 112
                Height = 21
                Date = 40922.728802048610000000
                Time = 40922.728802048610000000
                Enabled = False
                TabOrder = 0
                ReadOnly = True
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Instru'#231#227'o de baixa'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 996
              Height = 49
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
            end
          end
        end
        object GB_CMC7: TGroupBox
          Left = 2
          Top = 69
          Width = 1004
          Height = 56
          Align = alTop
          Caption = ' Informa'#231#245'es adicionais necess'#225'rias: '
          TabOrder = 3
          Visible = False
          object Panel8: TPanel
            Left = 2
            Top = 15
            Width = 1000
            Height = 39
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label36: TLabel
              Left = 4
              Top = 12
              Width = 143
              Height = 13
              Caption = 'Leitura pela banda magn'#233'tica:'
              Visible = False
            end
            object SpeedButton1: TSpeedButton
              Left = 440
              Top = 7
              Width = 23
              Height = 22
              Caption = '...'
              Visible = False
              OnClick = SpeedButton1Click
            end
            object Label1: TLabel
              Left = 468
              Top = 12
              Width = 32
              Height = 13
              Caption = 'CMC7:'
              Visible = False
            end
            object EdBanda: TdmkEdit
              Left = 152
              Top = 8
              Width = 285
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 34
              ParentFont = False
              TabOrder = 0
              Visible = False
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdBandaChange
            end
            object EdCMC_7: TdmkEdit
              Left = 507
              Top = 8
              Width = 177
              Height = 21
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 1
              Visible = False
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCMC_7Change
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 363
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 781
        Height = 16
        Caption = 
          #185': O n'#250'mero da conta corrente e o nome do emitente podem ser inf' +
          'ormados parcialmente.   '#178': '#218'ltimo lote que o cheque foi menciona' +
          'do.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 781
        Height = 16
        Caption = 
          #185': O n'#250'mero da conta corrente e o nome do emitente podem ser inf' +
          'ormados parcialmente.   '#178': '#218'ltimo lote que o cheque foi menciona' +
          'do.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 407
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 7
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirmar'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLctAfterOpen
    BeforeClose = QrLctBeforeClose
    OnCalcFields = QrLctCalcFields
    SQL.Strings = (
      'SELECT Data, Tipific, Praca, Banco, Agencia, '
      'ContaCorrente, Documento, Cliente, Vencimento,'
      'CNPJCPF, Emitente, Credito, DDeposito, TedRem_Sit, TedRem_Its,'
      'Controle, Sub, FatID, FatNum, FatParcela, Depositado'
      'FROM lct0001a'
      'WHERE FatID=301'
      'AND Tipific=5'
      'AND Praca=9'
      'AND Banco=1'
      'AND Agencia=891'
      'AND Documento=71025')
    Left = 472
    Top = 12
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lct0001a.Banco'
      DisplayFormat = '000'
    end
    object QrLctAgencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'lct0001a.Agencia'
      DisplayFormat = '0000'
    end
    object QrLctContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'lct0001a.ContaCorrente'
      Size = 15
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lct0001a.Documento'
      DisplayFormat = '000000'
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lct0001a.Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLctData: TDateField
      FieldName = 'Data'
      Origin = 'lct0001a.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDDeposito: TDateField
      FieldName = 'DDeposito'
      Origin = 'lct0001a.DDeposito'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Origin = 'lct0001a.CNPJCPF'
      Size = 15
    end
    object QrLctEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lct0001a.Emitente'
      Size = 30
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lct0001a.Controle'
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      Origin = 'lct0001a.Sub'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lct0001a.FatParcela'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lct0001a.FatID'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'lct0001a.FatNum'
    end
    object QrLctTipific: TSmallintField
      FieldName = 'Tipific'
      Origin = 'lct0001a.Tipific'
    end
    object QrLctPraca: TIntegerField
      FieldName = 'Praca'
      Origin = 'lct0001a.Praca'
    end
    object QrLctNo_TedRem_Sit: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'No_TedRem_Sit'
      Size = 255
      Calculated = True
    end
    object QrLctTedRem_Sit: TSmallintField
      FieldName = 'TedRem_Sit'
    end
    object QrLctTedRem_Its: TIntegerField
      FieldName = 'TedRem_Its'
    end
    object QrLctDepositado: TSmallintField
      FieldName = 'Depositado'
    end
    object QrLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 500
    Top = 12
  end
  object QrTCRI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CMC7'
      'FROM tedcremits'
      'WHERE LctCtrl=123481'
      'AND LctSub=0'
      'AND LctFatID=301'
      'AND LctFatNum=13211'
      'AND LctFatParc=141638')
    Left = 240
    Top = 156
    object QrTCRICMC7: TWideStringField
      FieldName = 'CMC7'
      Size = 40
    end
  end
end
