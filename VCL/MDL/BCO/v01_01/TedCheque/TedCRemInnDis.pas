unit TedCRemInnDis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmTedCRemInnDis = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Qrlct: TmySQLQuery;
    DsLct: TDataSource;
    DBGrid1: TDBGrid;
    QrlctVencimento: TDateField;
    QrlctBanco: TIntegerField;
    QrlctPraca: TIntegerField;
    QrlctAgencia: TIntegerField;
    QrlctContaCorrente: TWideStringField;
    QrlctDocumento: TFloatField;
    QrlctEmitente: TWideStringField;
    QrlctCNPJCPF: TWideStringField;
    QrlctCredito: TFloatField;
    QrlctTipific: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCMC7: String;
  end;

  var
  FmTedCRemInnDis: TFmTedCRemInnDis;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmTedCRemInnDis.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTedCRemInnDis.DBGrid1DblClick(Sender: TObject);
{
var
  Compensacao, Banco, Agencia, Cheque, Tipificacao: Integer;
  Conta: String;
}
begin
  if MLAGeral.GeraCMC7_34(
    QrLctPraca.Value,
    QrLctBanco.Value,
    QrLctAgencia.Value,
    QrLctContaCorrente.Value,
    Trunc(QrLctDocumento.Value),
    QrLctTipific.Value,
    FCMC7) then
      Close;
end;

procedure TFmTedCRemInnDis.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmTedCRemInnDis.FormCreate(Sender: TObject);
begin
  FCMC7 := '';
  QrLct.Open;
end;

procedure TFmTedCRemInnDis.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
