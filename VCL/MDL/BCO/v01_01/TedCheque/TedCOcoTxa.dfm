object FmTedCOcoTxa: TFmTedCOcoTxa
  Left = 339
  Top = 185
  Caption = 'TED-CHQUE-004 :: Tarifa de Ocorr'#234'ncias de TED Cheque'
  ClientHeight = 391
  ClientWidth = 587
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 587
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 539
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 491
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 461
        Height = 32
        Caption = 'Tarifa de Ocorr'#234'ncias de TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 461
        Height = 32
        Caption = 'Tarifa de Ocorr'#234'ncias de TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 461
        Height = 32
        Caption = 'Tarifa de Ocorr'#234'ncias de TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 587
    Height = 229
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 587
      Height = 229
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 101
        Width = 587
        Height = 128
        Align = alClient
        TabOrder = 0
        object Label8: TLabel
          Left = 12
          Top = 12
          Width = 34
          Height = 13
          Caption = 'Banco:'
        end
        object SpeedButton5: TSpeedButton
          Left = 553
          Top = 28
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object Label1: TLabel
          Left = 12
          Top = 52
          Width = 142
          Height = 13
          Caption = 'C'#243'digo da empresa no banco:'
        end
        object Label2: TLabel
          Left = 496
          Top = 52
          Width = 30
          Height = 13
          Caption = 'Valor: '
          Enabled = False
        end
        object CkContinuar: TCheckBox
          Left = 12
          Top = 100
          Width = 213
          Height = 17
          Caption = 'Continuar inserindo ap'#243's a confirma'#231#227'o.'
          Checked = True
          State = cbChecked
          TabOrder = 5
        end
        object EdBanco: TdmkEditCB
          Left = 12
          Top = 28
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'PlaGen'
          UpdCampo = 'PlaGen'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBBanco
          IgnoraDBLookupComboBox = False
        end
        object CBBanco: TdmkDBLookupComboBox
          Left = 68
          Top = 28
          Width = 485
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsBancos
          TabOrder = 1
          dmkEditCB = EdBanco
          QryCampo = 'PlaGen'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCedente: TdmkEdit
          Left = 12
          Top = 68
          Width = 165
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object RGTxaTip: TdmkRadioGroup
          Left = 180
          Top = 52
          Width = 309
          Height = 37
          Caption = ' Tipo de cobran'#231'a: '
          Columns = 2
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'Valor fixo'
            'Percentual')
          TabOrder = 3
          UpdType = utYes
          OldValor = 0
        end
        object EdTxaFat: TdmkEdit
          Left = 496
          Top = 68
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 587
        Height = 101
        Align = alTop
        Caption = ' Informa'#231#245'es para inclus'#227'o / altera'#231#227'o: '
        Enabled = False
        TabOrder = 1
        object Label3: TLabel
          Left = 12
          Top = 56
          Width = 34
          Height = 13
          Caption = 'Banco:'
          Enabled = False
        end
        object Label4: TLabel
          Left = 68
          Top = 56
          Width = 142
          Height = 13
          Caption = 'C'#243'digo da empresa no banco:'
          Enabled = False
        end
        object Label5: TLabel
          Left = 12
          Top = 16
          Width = 55
          Height = 13
          Caption = 'Ocorr'#234'ncia:'
          Enabled = False
        end
        object EdCodigo: TdmkEdit
          Left = 12
          Top = 31
          Width = 53
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdNome: TdmkEdit
          Left = 64
          Top = 31
          Width = 509
          Height = 21
          TabStop = False
          Enabled = False
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdOldBco: TdmkEdit
          Left = 12
          Top = 72
          Width = 53
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdOldCeden: TdmkEdit
          Left = 68
          Top = 72
          Width = 505
          Height = 21
          TabStop = False
          Enabled = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 277
    Width = 587
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 583
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 321
    Width = 587
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 441
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 439
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM Bancos'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 180
    Top = 56
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 208
    Top = 56
  end
end
