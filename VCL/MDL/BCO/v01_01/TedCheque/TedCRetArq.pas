unit TedCRetArq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Menus, UnDmkProcFunc, UnDmkEnums;

type
  TFmTedCRetArq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAvisoR1: TLabel;
    LaAvisoR2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConcilia: TBitBtn;
    LaTitulo1C: TLabel;
    QrCfgCab: TmySQLQuery;
    QrCfgCabCodigo: TIntegerField;
    QrCfgCabBanco: TIntegerField;
    QrCfgCabAnoMes: TIntegerField;
    QrCfgCabVersao: TWideStringField;
    QrCfgCabTamReg: TIntegerField;
    QrCfgCabNome: TWideStringField;
    QrCfgCabSeqLay: TIntegerField;
    QrCfgCabRemRet: TIntegerField;
    QrCfgCabEmpresa: TIntegerField;
    QrCfgCabCartDep: TIntegerField;
    QrCfgCabPathRem: TWideStringField;
    QrCfgCabPathRet: TWideStringField;
    QrCfgCabCNPJCPF: TWideStringField;
    LaAvisoB1: TLabel;
    LaAvisoB2: TLabel;
    LaAvisoG1: TLabel;
    LaAvisoG2: TLabel;
    QrListaTed: TmySQLQuery;
    DsListaTed: TDataSource;
    DBGrid1: TDBGrid;
    QrListaTedDataHora: TDateTimeField;
    QrListaTedLayout: TIntegerField;
    QrListaTedTedCRetCab: TIntegerField;
    QrListaTedNoLayout: TWideStringField;
    QrListaTedNoArquivo: TWideStringField;
    QrListaTedErro: TIntegerField;
    QrListaTedStep: TIntegerField;
    QrListaTedItens: TIntegerField;
    QrListaTedAtivo: TSmallintField;
    QrListaTedNoErro: TWideStringField;
    QrListaRet: TmySQLQuery;
    QrListaRetCodigo: TIntegerField;
    QrListaRetTedC_Cfg: TIntegerField;
    QrListaRetStep: TIntegerField;
    QrListaRetDataGer: TDateField;
    QrListaRetNoTedC_Cfg: TWideStringField;
    QrListaRetNome: TWideStringField;
    QrListaRetItens: TIntegerField;
    QrSemErr: TmySQLQuery;
    QrSemErrDataHora: TDateTimeField;
    QrSemErrLayout: TIntegerField;
    QrSemErrTedCRetCab: TIntegerField;
    QrSemErrNoLayout: TWideStringField;
    QrSemErrNoArquivo: TWideStringField;
    QrSemErrNoErro: TWideStringField;
    QrSemErrErro: TIntegerField;
    QrSemErrStep: TIntegerField;
    QrSemErrItens: TIntegerField;
    QrSemErrAtivo: TSmallintField;
    SbImprime: TBitBtn;
    PMImprime: TPopupMenu;
    Arquivoselecionado1: TMenuItem;
    ArquivosSelecionados1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtConciliaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure ArquivosSelecionados1Click(Sender: TObject);
    procedure Arquivoselecionado1Click(Sender: TObject);
  private
    { Private declarations }
    FJaListou: Boolean;
    F_Lista_TED_: String;
    //
    procedure AnalisaArquivo(Layout, SeqLay, TamReg, Empresa, LctFatID: Integer;
              NoLayout, CNPJEmp, Arquivo, TbTemp: String);
    //
    procedure VerificaItensDeLotesdeRetorno();
  public
    { Public declarations }
  end;

  var
  FmTedCRetArq: TFmTedCRetArq;

implementation

uses UnMyObjects, Module, ModuleGeral, UCreateCredito, UMySQLModule, TedC_Aux,
  ModuleFin, MyListas, UnBancos, ModuleTedC;

{$R *.DFM}

procedure TFmTedCRetArq.AnalisaArquivo(Layout, SeqLay, TamReg, Empresa,
  LctFatID: Integer; NoLayout, CNPJEmp, Arquivo, TbTemp: String);
var
  Linhas: TStrings;
  Erro, TedCRetCab, BancoTED, UltLin, Step, Itens: Integer;
  //
  DestName, DataHora, NoArquivo, NoErro: String;
begin
  Erro := 0;
  Itens := 0;
  Linhas := TStringList.Create;
  try
    Linhas.LoadFromFile(Arquivo);
    if Length(Linhas[0]) <> Tamreg then
      Erro := 1001 // Tamanho do registro (tedccfgsys)
    else
    begin
      case SeqLay of
        (*1*) VAR_TED_U0N1U9:
        begin
          TedC_Ax.AnalisaRetLinhaReg0(
            Layout, Empresa, CNPJEmp, Arquivo, Linhas,
            LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2, LaAvisoR1, LaAvisoR2,
            NoErro, Erro, Step, BancoTED, TedCRetCab);
          if Erro = 0 then
            TedC_Ax.AnalisaRetLinhaReg1(TedCRetCab, Layout, TamReg,
            Empresa, BancoTED, LctFatID, CNPJEmp, Arquivo, Linhas,
            LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2, LaAvisoR1, LaAvisoR2,
            CO_TabLctA, NoErro, Erro, Step, UltLin, Itens);
          if Erro = 0 then
            TedC_Ax.AnalisaRetLinhaReg9(TedCRetCab, Layout, UltLin + 1, Arquivo,
            Linhas, LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2, LaAvisoR1,
            LaAvisoR2, NoErro, Erro, Step);
        end;
      end;
      if (Erro = 0) and (Step = CO_TEDC_STEP_ARQ_ARQUIVO_LIDO) then
      begin
        // Transferir arquivo para pasta "Lidos"
        if FileExists(Arquivo) then
        begin
          DestName := ExtractFilePath(Arquivo);
          DestName := dmkPF.CaminhoArquivo(DestName, 'Lidos', '');
          ForceDirectories(DestName);
          dmkPF.MoveArq(PChar(Arquivo), PChar(DestName));
        end;
        //
        MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True,
          'Leitura do finalizada do arquivo "' + Arquivo + '"');
        MyObjects.Informa2(LaAvisoB1, LaAvisoB2, False, '...');
        MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
      end else
      begin
        DataHora := '0000-00-00 00:00:00';
        //NoLayout := NoLayout;
        NoArquivo := Arquivo;
        if Erro = 0 then
          Erro := 9999; // Desconhecido
        UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, TbTemp, False, [
        'DataHora', 'Layout', 'TedCRetCab',
        'NoLayout', 'NoArquivo', 'NoErro',
        'Erro', 'Step', 'Itens'], [
        ], [
        DataHora, Layout, TedCRetCab,
        NoLayout, NoArquivo, NoErro,
        Erro, Step, Itens], [
        ], False);
      end;
    end;
    //
  finally
    Linhas.Free;
  end;
end;

procedure TFmTedCRetArq.Arquivoselecionado1Click(Sender: TObject);
begin
  DmTedC.MostraLoteRetorno(QrListaTedTedCRetCab.Value);
end;

procedure TFmTedCRetArq.ArquivosSelecionados1Click(Sender: TObject);
var
  TedCRetCabs: String;
  I: Integer;
begin
  TedCRetCabs := '';
  with DBGrid1.DataSource.DataSet do
  for I:= 0 to DBGrid1.SelectedRows.Count-1 do
  begin
    GotoBookmark(DBGrid1.SelectedRows.Items[i]);
    TedCRetCabs := TedCRetCabs + ',' + Geral.FF0(QrListaTedTedCRetCab.Value);
  end;
  if Length(TedCRetCabs) > 0 then
  begin
    TedCRetCabs := Copy(TedCRetCabs, 2);
    DmTedC.MostraLoteRetorno_Abre(TedCRetCabs);
  end else Geral.MensagemBox('Nenhum arquivo foi selecionado!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmTedCRetArq.BtConciliaClick(Sender: TObject);
begin
  VerificaItensDeLotesdeRetorno();
end;

procedure TFmTedCRetArq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTedCRetArq.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
{
  if (Column.FieldName = 'NoErro') then
  begin
}
    if QrListaTedErro.Value = 0 then
      Cor := clBlue
    else
      Cor := clRed;
    with DBGrid1.Canvas do
    begin
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left + 2, Rect.Top  + 2, Column.Field.DisplayText);
    end;
//  end;
end;

procedure TFmTedCRetArq.FormActivate(Sender: TObject);
  function GetAllFiles(Mask: string; Lista: TStrings): Integer;
  var
    Search: TSearchRec;
    Directory: string;
  begin
    Result := 0;
    Application.ProcessMessages;
    //
    Directory := ExtractFilePath(Mask);

    // find all files
    if FindFirst(Mask, $23, search) = 0 then
    begin
      repeat
        // add the files to the listbox
        if Lista <> nil then
          Lista.Add(directory + search.Name);
        Result := Result + 1;
        //Inc(Count);
      until FindNext(search) <> 0;
    end;

  end;
var
  Lista: TStrings;
  Arquivos: String;
  I: Integer;
  //
  Layout, SeqLay, TamReg, Empresa: Integer;
  CNPJEmp: String;
  //
  DataHora, NoLayout, NoArquivo, NoErro: String;
  TedCRetCab, Erro, Step, Itens: Integer;
begin
  Layout := 0;
  MyObjects.CorIniComponente();
  Application.ProcessMessages;
  if not FJaListou then
  begin
    F_Lista_TED_ :=
      UCriarCredito.RecriaTempTableNovo(ntrtt_Lista_TED_, DModG.QrUpdPID1);
    //
    UMyMod.AbreQuery(QrCFgCab, Dmod.MyDB);
    if MyObjects.FIC(QrCFgCab.RecordCount = 0, nil,
    'N�o h� cadastro de configura��o de retorno TED!') then
      Exit;
    QrCFgCab.First;
    while not QrCFgCab.Eof do
    begin
      Arquivos := dmkPF.CaminhoArquivo(QrCfgCabPathRet.Value, '*', '*');
      Lista := TStringList.Create;
      try
        GetAllFiles(Arquivos, Lista);
        for I := 0 to Lista.Count - 1 do
        begin
          Layout := QrCFgCabCodigo.Value;
          SeqLay := QrCFgCabSeqLay.Value;
          TamReg := QrCfgCabTamReg.Value;
          Empresa := QrCfgCabEmpresa.Value;
          CNPJEmp := QrCFgCabCNPJCPF.Value;
          //
          AnalisaArquivo(Layout, SeqLay, TamReg, Empresa, VAR_FATID_0301,
            QrCfgCabNome.Value, CNPJEmp, Lista[I], F_Lista_TED_);
        end;
        MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True,
          'Leitura do finalizada na pasta "' + QrCfgCabPathRet.Value + '"');
        MyObjects.Informa2(LaAvisoB1, LaAvisoB2, False, '...');
        MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
      finally
        Lista.Free;
      end;
      //
      QrCFgCab.Next;
    end;
    MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True,
      'Verificando arquivos carregados e n�o processados');
    MyObjects.Informa2(LaAvisoB1, LaAvisoB2, False, '...');
    MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
    //
    UMyMod.AbreQuery(QrListaRet, Dmod.MyDB);
    while not QrListaRet.Eof do
    begin
      DataHora := Geral.FDT(QrListaRetDataGer.Value, 105);
      NoLayout := QrListaRetNoTedC_Cfg.Value;
      NoArquivo := QrListaRetNome.Value;
      TedCRetCab := QrListaRetCodigo.Value;
      Step := QrListaRetStep.Value;
      Itens := QrListaRetItens.Value;
      Erro := 0;
      NoErro := '';
      //
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True,
        'Verificando arquivo carregados e n�o processado: ' + NoArquivo);
      //
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, F_Lista_TED_, False, [
      'DataHora', 'Layout', 'TedCRetCab',
      'NoLayout', 'NoArquivo', 'NoErro',
      'Erro', 'Step', 'Itens'], [
      ], [
      DataHora, Layout, TedCRetCab,
      NoLayout, NoArquivo, NoErro,
      Erro, Step, Itens], [
      ], False);
      //
      QrListaRet.Next;
    end;
    UMyMod.AbreQuery(QrListaTed, DModG.MyPID_DB);
    //
    Application.ProcessMessages;
    UMyMod.AbreQuery(QrSemErr, DModG.MyPID_DB);
    if QrSemErr.RecordCount > 0 then
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False,
      'Clique no bot�o "Concilia" para conciliar os itens carregados!')
    else
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, '...');
    MyObjects.Informa2(LaAvisoB1, LaAvisoB2, False, '...');
    MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
  end;
end;

procedure TFmTedCRetArq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  QrListaTed.Close;
  QrListaTed.Database := DModG.MyPID_DB;
  QrSemErr.Close;
  QrSemErr.Database := DModG.MyPID_DB;
end;

procedure TFmTedCRetArq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAvisoR1, LaAvisoR2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTedCRetArq.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmTedCRetArq.VerificaItensDeLotesdeRetorno();
begin
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True,
    'Preparando para conciliar itens de retorno');
  //
  UMyMod.AbreQuery(QrSemErr, DModG.MyPID_DB);
  QrSemErr.First;
  while not QrSemErr.Eof do
  begin
    TedC_Ax.VerificaItemDeRetorno(QrSemErrTedCRetCab.Value,
    VAR_FATID_0301, CO_TabLctA,
    LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2);
    //
    QrSemErr.Next;
  end;
  // No final, Reabre QrListaTed
  UMyMod.AbreQuery(QrListaTed, DModG.MyPID_DB);
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, '...');
  MyObjects.Informa2(LaAvisoB1, LaAvisoB2, False, '...');
  MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
end;

end.
