object FmTedCRetCab: TFmTedCRetCab
  Left = 368
  Top = 194
  Caption = 'TED-CHQUE-201 :: Retorno TED Cheque'
  ClientHeight = 514
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 418
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 68
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Caminho:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 32
        Width = 921
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 355
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 898
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object Memo1: TMemo
      Left = 0
      Top = 61
      Width = 1008
      Height = 128
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 2
      WordWrap = False
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 418
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 69
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 12
        Top = 20
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 204
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 112
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 12
        Top = 44
        Width = 40
        Height = 13
        Caption = 'Produto:'
        FocusControl = DBEdit2
      end
      object Label5: TLabel
        Left = 808
        Top = 20
        Width = 43
        Height = 13
        Caption = 'Cedente:'
        FocusControl = DBEdit3
      end
      object Label6: TLabel
        Left = 100
        Top = 44
        Width = 23
        Height = 13
        Caption = 'Filial:'
        FocusControl = DBEdit4
      end
      object Label8: TLabel
        Left = 164
        Top = 44
        Width = 55
        Height = 13
        Caption = 'CNPJ/CPF:'
        FocusControl = DBEdit5
      end
      object Label10: TLabel
        Left = 340
        Top = 44
        Width = 34
        Height = 13
        Caption = 'Banco:'
        FocusControl = DBEdit6
      end
      object Label11: TLabel
        Left = 416
        Top = 44
        Width = 44
        Height = 13
        Caption = 'Gera'#231#227'o:'
        FocusControl = DBEdit7
      end
      object Label12: TLabel
        Left = 524
        Top = 44
        Width = 38
        Height = 13
        Caption = 'N'#186' seq.:'
        FocusControl = DBEdit8
      end
      object Label13: TLabel
        Left = 632
        Top = 44
        Width = 56
        Height = 13
        Caption = 'Depositaria:'
        FocusControl = DBEdit9
      end
      object Label14: TLabel
        Left = 752
        Top = 44
        Width = 42
        Height = 13
        Caption = 'Terceira:'
        FocusControl = DBEdit10
      end
      object Label28: TLabel
        Left = 828
        Top = 44
        Width = 26
        Height = 13
        Caption = 'Itens:'
        FocusControl = DBEdit24
      end
      object Label29: TLabel
        Left = 912
        Top = 44
        Width = 35
        Height = 13
        Caption = 'Layout:'
        FocusControl = DBEdit25
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 52
        Top = 16
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTedCRetCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 256
        Top = 16
        Width = 549
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTedCRetCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 160
        Top = 16
        Width = 37
        Height = 21
        DataField = 'Empresa'
        DataSource = DsTedCRetCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 56
        Top = 40
        Width = 41
        Height = 21
        DataField = 'Produto'
        DataSource = DsTedCRetCab
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 856
        Top = 16
        Width = 145
        Height = 21
        DataField = 'Cedente'
        DataSource = DsTedCRetCab
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 124
        Top = 40
        Width = 37
        Height = 21
        DataField = 'Filial'
        DataSource = DsTedCRetCab
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 224
        Top = 40
        Width = 112
        Height = 21
        DataField = 'CNPJCPF'
        DataSource = DsTedCRetCab
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 376
        Top = 40
        Width = 37
        Height = 21
        DataField = 'Banco'
        DataSource = DsTedCRetCab
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 464
        Top = 40
        Width = 56
        Height = 21
        DataField = 'DataGer'
        DataSource = DsTedCRetCab
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 564
        Top = 40
        Width = 64
        Height = 21
        DataField = 'NumSeqGer'
        DataSource = DsTedCRetCab
        TabOrder = 9
      end
      object DBEdit9: TDBEdit
        Left = 692
        Top = 40
        Width = 57
        Height = 21
        DataField = 'Depositari'
        DataSource = DsTedCRetCab
        TabOrder = 10
      end
      object DBEdit10: TDBEdit
        Left = 796
        Top = 40
        Width = 29
        Height = 21
        DataField = 'Terceira'
        DataSource = DsTedCRetCab
        TabOrder = 11
      end
      object DBEdit24: TDBEdit
        Left = 860
        Top = 40
        Width = 45
        Height = 21
        DataField = 'Itens'
        DataSource = DsTedCRetCab
        TabOrder = 12
      end
      object DBEdit25: TDBEdit
        Left = 948
        Top = 40
        Width = 53
        Height = 21
        DataField = 'TedC_Cfg'
        DataSource = DsTedCRetCab
        TabOrder = 13
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 354
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Arquivo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 161
      Width = 1008
      Height = 193
      Align = alClient
      DataSource = DsTedCRetIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CMC7'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Praca'
          Title.Caption = 'Pr'#231'.'
          Width = 29
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Banco'
          Title.Caption = 'Bco'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Agencia'
          Title.Caption = 'Ag'#234'.'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ContaCorre'
          Title.Caption = 'Conta corr.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cheque'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LctCtrl'
          Title.Caption = 'Lan'#231'to'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TEXTO_PARA_USR'
          Title.Caption = 'Status'
          Width = 152
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetInscTip'
          Title.Caption = 'TD'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetInscNum'
          Title.Caption = 'Documento'
          Width = 112
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetEmitNom'
          Title.Caption = 'Emitente'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RemDataBoa'
          Title.Caption = 'RmDt.Boa'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RemValor'
          Title.Caption = 'Rm. Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetDataBoa'
          Title.Caption = 'Rt.Dt.Boa'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetValor'
          Title.Caption = 'Rt. Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Alinea'
          Title.Caption = 'Al'#237'n.'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BorderoBco'
          Title.Caption = 'Border'#244' Bco'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NossoNum'
          Title.Caption = 'Nosso Num. Bco.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodRejeito'
          Title.Caption = 'Rej.'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtaCaptura'
          Title.Caption = 'DDt.Capt.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LctFatNum'
          Title.Caption = 'Lote'#185
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LctFatParc'
          Title.Caption = 'ID'#185
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Linha'
          Title.Caption = 'Lin.Arq.'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RemItsCtrl'
          Title.Caption = 'ID Rm.'
          Visible = True
        end>
    end
    object Panel6: TPanel
      Left = 0
      Top = 69
      Width = 1008
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 88
        Align = alTop
        Caption = ' Situa'#231#227'o geral da cust'#243'dia: '
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 69
          Top = 15
          Width = 155
          Height = 71
          Align = alLeft
          Caption = ' Em carteira: '
          TabOrder = 0
          object DBEdit11: TDBEdit
            Left = 8
            Top = 20
            Width = 134
            Height = 21
            DataField = 'CartQtd'
            DataSource = DsTedCRetCab
            TabOrder = 0
          end
          object DBEdit12: TDBEdit
            Left = 8
            Top = 44
            Width = 134
            Height = 21
            DataField = 'CartVal'
            DataSource = DsTedCRetCab
            TabOrder = 1
          end
        end
        object GroupBox3: TGroupBox
          Left = 224
          Top = 15
          Width = 155
          Height = 71
          Align = alLeft
          Caption = ' Entrada:'
          TabOrder = 1
          object DBEdit13: TDBEdit
            Left = 8
            Top = 20
            Width = 134
            Height = 21
            DataField = 'EntrQtd'
            DataSource = DsTedCRetCab
            TabOrder = 0
          end
          object DBEdit14: TDBEdit
            Left = 8
            Top = 44
            Width = 134
            Height = 21
            DataField = 'EntrVal'
            DataSource = DsTedCRetCab
            TabOrder = 1
          end
        end
        object GroupBox4: TGroupBox
          Left = 379
          Top = 15
          Width = 155
          Height = 71
          Align = alLeft
          Caption = ' Dep'#243'sito: '
          TabOrder = 2
          object DBEdit16: TDBEdit
            Left = 8
            Top = 44
            Width = 134
            Height = 21
            DataField = 'DepoVal'
            DataSource = DsTedCRetCab
            TabOrder = 0
          end
          object DBEdit15: TDBEdit
            Left = 8
            Top = 20
            Width = 134
            Height = 21
            DataField = 'DepoQtd'
            DataSource = DsTedCRetCab
            TabOrder = 1
          end
        end
        object GroupBox5: TGroupBox
          Left = 534
          Top = 15
          Width = 155
          Height = 71
          Align = alLeft
          Caption = ' Baixa: '
          TabOrder = 3
          object DBEdit18: TDBEdit
            Left = 8
            Top = 44
            Width = 134
            Height = 21
            DataField = 'BaxaVal'
            DataSource = DsTedCRetCab
            TabOrder = 0
          end
          object DBEdit17: TDBEdit
            Left = 8
            Top = 20
            Width = 134
            Height = 21
            DataField = 'BaxaQtd'
            DataSource = DsTedCRetCab
            TabOrder = 1
          end
        end
        object GroupBox6: TGroupBox
          Left = 689
          Top = 15
          Width = 155
          Height = 71
          Align = alLeft
          Caption = ' Devolu'#231#227'o: '
          TabOrder = 4
          object DBEdit19: TDBEdit
            Left = 8
            Top = 20
            Width = 134
            Height = 21
            DataField = 'DevlQtd'
            DataSource = DsTedCRetCab
            TabOrder = 0
          end
          object DBEdit20: TDBEdit
            Left = 8
            Top = 44
            Width = 134
            Height = 21
            DataField = 'DevlVal'
            DataSource = DsTedCRetCab
            TabOrder = 1
          end
        end
        object GroupBox7: TGroupBox
          Left = 844
          Top = 15
          Width = 155
          Height = 71
          Align = alLeft
          Caption = 'Reapresenta'#231#227'o:'
          TabOrder = 5
          object DBEdit22: TDBEdit
            Left = 8
            Top = 44
            Width = 134
            Height = 21
            DataField = 'ReapVal'
            DataSource = DsTedCRetCab
            TabOrder = 0
          end
          object DBEdit21: TDBEdit
            Left = 8
            Top = 20
            Width = 134
            Height = 21
            DataField = 'ReapQtd'
            DataSource = DsTedCRetCab
            TabOrder = 1
          end
        end
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 67
          Height = 71
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 6
          object Label21: TLabel
            Left = 4
            Top = 44
            Width = 50
            Height = 13
            Caption = 'Valor total:'
          end
          object Label15: TLabel
            Left = 4
            Top = 24
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 259
        Height = 32
        Caption = 'Retorno TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 259
        Height = 32
        Caption = 'Retorno TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 259
        Height = 32
        Caption = 'Retorno TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrTedCRetCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTedCRetCabBeforeOpen
    AfterOpen = QrTedCRetCabAfterOpen
    AfterScroll = QrTedCRetCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM tedcretcab'
      'WHERE Codigo > 0')
    Left = 64
    Top = 65
    object QrTedCRetCabCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrTedCRetCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrTedCRetCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTedCRetCabTedC_Cfg: TIntegerField
      FieldName = 'TedC_Cfg'
    end
    object QrTedCRetCabStep: TIntegerField
      FieldName = 'Step'
    end
    object QrTedCRetCabProduto: TWideStringField
      FieldName = 'Produto'
      Size = 10
    end
    object QrTedCRetCabCedente: TWideStringField
      FieldName = 'Cedente'
    end
    object QrTedCRetCabFilial: TWideStringField
      FieldName = 'Filial'
      Size = 10
    end
    object QrTedCRetCabCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrTedCRetCabBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrTedCRetCabDataGer: TDateField
      FieldName = 'DataGer'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTedCRetCabNumSeqGer: TIntegerField
      FieldName = 'NumSeqGer'
      DisplayFormat = '#,###,###,###,##0'
    end
    object QrTedCRetCabDepositari: TWideStringField
      FieldName = 'Depositari'
      Size = 10
    end
    object QrTedCRetCabTerceira: TWideStringField
      FieldName = 'Terceira'
      Size = 10
    end
    object QrTedCRetCabCartQtd: TIntegerField
      FieldName = 'CartQtd'
      DisplayFormat = '#,###,###,###,##0'
    end
    object QrTedCRetCabCartVal: TFloatField
      FieldName = 'CartVal'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrTedCRetCabEntrQtd: TIntegerField
      FieldName = 'EntrQtd'
      DisplayFormat = '#,###,###,###,##0'
    end
    object QrTedCRetCabEntrVal: TFloatField
      FieldName = 'EntrVal'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrTedCRetCabDepoQtd: TIntegerField
      FieldName = 'DepoQtd'
      DisplayFormat = '#,###,###,###,##0'
    end
    object QrTedCRetCabDepoVal: TFloatField
      FieldName = 'DepoVal'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrTedCRetCabBaxaQtd: TIntegerField
      FieldName = 'BaxaQtd'
      DisplayFormat = '#,###,###,###,##0'
    end
    object QrTedCRetCabBaxaVal: TFloatField
      FieldName = 'BaxaVal'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrTedCRetCabDevlQtd: TIntegerField
      FieldName = 'DevlQtd'
      DisplayFormat = '#,###,###,###,##0'
    end
    object QrTedCRetCabDevlVal: TFloatField
      FieldName = 'DevlVal'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrTedCRetCabReapQtd: TIntegerField
      FieldName = 'ReapQtd'
      DisplayFormat = '#,###,###,###,##0'
    end
    object QrTedCRetCabReapVal: TFloatField
      FieldName = 'ReapVal'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrTedCRetCabUltLin: TIntegerField
      FieldName = 'UltLin'
      DisplayFormat = '#,###,###,###,##0'
    end
    object QrTedCRetCabItens: TIntegerField
      FieldName = 'Itens'
      DisplayFormat = '#,###,###,###,##0'
    end
  end
  object DsTedCRetCab: TDataSource
    DataSet = QrTedCRetCab
    Left = 92
    Top = 65
  end
  object QrTedCRetIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTedCRetItsCalcFields
    SQL.Strings = (
      'SELECT tcri.*, tcos.Nome NO_MyOcorr,'
      'tinc.Nome NO_Inconsiste'
      'FROM tedcretits tcri'
      'LEFT JOIN tedcocosys tcos ON tcos.Codigo=tcri.MyOcorr'
      'LEFT JOIN tedcretinc tinc ON tinc.Codigo=tcri.Inconsiste'
      'WHERE tcri.Codigo=:P0'
      ''
      '')
    Left = 148
    Top = 65
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTedCRetItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTedCRetItsLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrTedCRetItsLctCtrl: TIntegerField
      FieldName = 'LctCtrl'
    end
    object QrTedCRetItsLctSub: TIntegerField
      FieldName = 'LctSub'
    end
    object QrTedCRetItsLctFatID: TIntegerField
      FieldName = 'LctFatID'
    end
    object QrTedCRetItsLctFatNum: TLargeintField
      FieldName = 'LctFatNum'
    end
    object QrTedCRetItsLctFatParc: TIntegerField
      FieldName = 'LctFatParc'
    end
    object QrTedCRetItsOcorrBco: TIntegerField
      FieldName = 'OcorrBco'
    end
    object QrTedCRetItsMyOcorr: TIntegerField
      FieldName = 'MyOcorr'
    end
    object QrTedCRetItsStep: TSmallintField
      FieldName = 'Step'
    end
    object QrTedCRetItsCedente: TWideStringField
      FieldName = 'Cedente'
    end
    object QrTedCRetItsFilial: TWideStringField
      FieldName = 'Filial'
      Size = 10
    end
    object QrTedCRetItsUsoEmpresa: TWideStringField
      FieldName = 'UsoEmpresa'
      Size = 25
    end
    object QrTedCRetItsDtaCaptura: TDateField
      FieldName = 'DtaCaptura'
    end
    object QrTedCRetItsCMC7: TWideStringField
      FieldName = 'CMC7'
      Size = 30
    end
    object QrTedCRetItsRemValor: TFloatField
      FieldName = 'RemValor'
    end
    object QrTedCRetItsRemDataBoa: TDateField
      FieldName = 'RemDataBoa'
    end
    object QrTedCRetItsBorderoBco: TIntegerField
      FieldName = 'BorderoBco'
    end
    object QrTedCRetItsSeuNumero: TLargeintField
      FieldName = 'SeuNumero'
    end
    object QrTedCRetItsAlinea: TIntegerField
      FieldName = 'Alinea'
    end
    object QrTedCRetItsCodRejeito: TIntegerField
      FieldName = 'CodRejeito'
    end
    object QrTedCRetItsNossoNum: TLargeintField
      FieldName = 'NossoNum'
    end
    object QrTedCRetItsRetValor: TFloatField
      FieldName = 'RetValor'
    end
    object QrTedCRetItsRetDataBoa: TDateField
      FieldName = 'RetDataBoa'
    end
    object QrTedCRetItsTrfCedente: TWideStringField
      FieldName = 'TrfCedente'
      Size = 18
    end
    object QrTedCRetItsTrfDtaOper: TDateField
      FieldName = 'TrfDtaOper'
    end
    object QrTedCRetItsTrfContrat: TWideStringField
      FieldName = 'TrfContrat'
      Size = 25
    end
    object QrTedCRetItsTrfParcela: TIntegerField
      FieldName = 'TrfParcela'
    end
    object QrTedCRetItsTrfDtaCntr: TDateField
      FieldName = 'TrfDtaCntr'
    end
    object QrTedCRetItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrTedCRetItsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrTedCRetItsContaCorre: TWideStringField
      FieldName = 'ContaCorre'
      Size = 10
    end
    object QrTedCRetItsCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrTedCRetItsPraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrTedCRetItsRetInscTip: TWideStringField
      FieldName = 'RetInscTip'
      Size = 3
    end
    object QrTedCRetItsRetInscNum: TWideStringField
      FieldName = 'RetInscNum'
    end
    object QrTedCRetItsRetEmitNom: TWideStringField
      FieldName = 'RetEmitNom'
      Size = 30
    end
    object QrTedCRetItsRemItsCtrl: TIntegerField
      FieldName = 'RemItsCtrl'
    end
    object QrTedCRetItsInconsiste: TIntegerField
      FieldName = 'Inconsiste'
    end
    object QrTedCRetItsNO_MyOcorr: TWideStringField
      FieldName = 'NO_MyOcorr'
      Size = 100
    end
    object QrTedCRetItsNO_Inconsiste: TWideStringField
      FieldName = 'NO_Inconsiste'
      Size = 60
    end
    object QrTedCRetItsTEXTO_PARA_USR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO_PARA_USR'
      Size = 255
      Calculated = True
    end
  end
  object DsTedCRetIts: TDataSource
    DataSet = QrTedCRetIts
    Left = 176
    Top = 65
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 660
    Top = 368
    object Processa1: TMenuItem
      Caption = '&Processa'
      Enabled = False
      OnClick = Processa1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 536
    Top = 364
    object CabInclui1: TMenuItem
      Caption = '&Inclui (Carrega)'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
end
