object FmTedCCfgCab: TFmTedCCfgCab
  Left = 368
  Top = 194
  Caption = 'TED-CHQUE-001 :: Configura'#231#245'es TED Cheque'
  ClientHeight = 553
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 457
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 157
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit6
      end
      object Label4: TLabel
        Left = 508
        Top = 56
        Width = 176
        Height = 13
        Caption = 'Carteira de dep'#243'sito: (Conta corrente)'
        FocusControl = DBEdit8
      end
      object Label8: TLabel
        Left = 16
        Top = 96
        Width = 244
        Height = 13
        Caption = 'Pasta na qual ser'#227'o salvos os arquivos de remessa:'
        FocusControl = DBEdit10
      end
      object Label10: TLabel
        Left = 508
        Top = 96
        Width = 261
        Height = 13
        Caption = 'Pasta na qual ser'#227'o procurados os arquivos de retorno:'
        FocusControl = DBEdit11
      end
      object Label14: TLabel
        Left = 688
        Top = 16
        Width = 191
        Height = 13
        Caption = 'C'#243'digo da empresa no banco (Cedente):'
        Color = clBtnFace
        ParentColor = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTedCCfgCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 609
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTedCCfgCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object CkCliInfo: TCheckBox
        Left = 16
        Top = 136
        Width = 269
        Height = 17
        Caption = 'Mostra somente itens que devo informar o conte'#250'do.'
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = CkCliInfoClick
      end
      object DBEdit6: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsTedCCfgCab
        TabOrder = 4
      end
      object DBEdit7: TDBEdit
        Left = 72
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_ENT'
        DataSource = DsTedCCfgCab
        TabOrder = 5
      end
      object DBEdit8: TDBEdit
        Left = 508
        Top = 72
        Width = 56
        Height = 21
        DataField = 'CartDep'
        DataSource = DsTedCCfgCab
        TabOrder = 6
      end
      object DBEdit9: TDBEdit
        Left = 564
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_CARTDEP'
        DataSource = DsTedCCfgCab
        TabOrder = 7
      end
      object DBEdit10: TDBEdit
        Left = 16
        Top = 112
        Width = 489
        Height = 21
        DataField = 'PathRem'
        DataSource = DsTedCCfgCab
        TabOrder = 8
      end
      object DBEdit11: TDBEdit
        Left = 508
        Top = 112
        Width = 489
        Height = 21
        DataField = 'PathRet'
        DataSource = DsTedCCfgCab
        TabOrder = 9
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 688
        Top = 32
        Width = 309
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Cedente'
        DataSource = DsTedCCfgCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 393
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 10088
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Layout'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 10089
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item layout'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 157
      Width = 1008
      Height = 48
      Align = alTop
      DataSource = DsTedCCfgIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Registro'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SubReg'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PosIni'
          Width = 37
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Campo'
          Width = 119
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DefaultRem'
          Title.Caption = 'Valor do campo (remessa)'
          Width = 210
          Visible = True
        end>
    end
    object GroupBox1: TGroupBox
      Left = 500
      Top = 205
      Width = 508
      Height = 188
      Align = alRight
      Caption = ' Explica'#231#227'o do campo: '
      TabOrder = 3
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 504
        Height = 171
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object DBEdit1: TDBEdit
          Left = 8
          Top = 8
          Width = 488
          Height = 21
          DataField = 'Descri01'
          DataSource = DsTedCCfgIts
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 8
          Top = 28
          Width = 488
          Height = 21
          DataField = 'Descri02'
          DataSource = DsTedCCfgIts
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 8
          Top = 48
          Width = 488
          Height = 21
          DataField = 'Descri03'
          DataSource = DsTedCCfgIts
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 68
          Width = 488
          Height = 21
          DataField = 'Descri04'
          DataSource = DsTedCCfgIts
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 8
          Top = 88
          Width = 488
          Height = 21
          DataField = 'Descri05'
          DataSource = DsTedCCfgIts
          TabOrder = 4
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 457
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 169
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 16
        Top = 56
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label6: TLabel
        Left = 508
        Top = 56
        Width = 176
        Height = 13
        Caption = 'Carteira de dep'#243'sito: (Conta corrente)'
      end
      object Label11: TLabel
        Left = 16
        Top = 96
        Width = 244
        Height = 13
        Caption = 'Pasta na qual ser'#227'o salvos os arquivos de remessa:'
        FocusControl = DBEdit10
      end
      object Label12: TLabel
        Left = 508
        Top = 96
        Width = 261
        Height = 13
        Caption = 'Pasta na qual ser'#227'o procurados os arquivos de retorno:'
        FocusControl = DBEdit11
      end
      object SpeedButton5: TSpeedButton
        Left = 484
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object SpeedButton6: TSpeedButton
        Left = 976
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object Label13: TLabel
        Left = 688
        Top = 16
        Width = 191
        Height = 13
        Caption = 'C'#243'digo da empresa no banco (Cedente):'
        Color = clBtnFace
        ParentColor = False
      end
      object SpeedButton7: TSpeedButton
        Left = 976
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton7Click
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 609
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object EdCartDep: TdmkEditCB
        Left = 508
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CartDep'
        UpdCampo = 'CartDep'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCartDep
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 433
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 4
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBCartDep: TdmkDBLookupComboBox
        Left = 564
        Top = 72
        Width = 409
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 6
        dmkEditCB = EdCartDep
        QryCampo = 'CartDep'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPathRem: TdmkEdit
        Left = 16
        Top = 112
        Width = 465
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'PathRem'
        UpdCampo = 'PathRem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPathRet: TdmkEdit
        Left = 508
        Top = 112
        Width = 465
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'PathRet'
        UpdCampo = 'PathRet'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCedente: TdmkEdit
        Left = 688
        Top = 32
        Width = 308
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Cedente'
        UpdCampo = 'Cedente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 394
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 898
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 340
        Height = 32
        Caption = 'Configura'#231#245'es TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 340
        Height = 32
        Caption = 'Configura'#231#245'es TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 340
        Height = 32
        Caption = 'Configura'#231#245'es TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrTedCCfgCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTedCCfgCabBeforeOpen
    AfterOpen = QrTedCCfgCabAfterOpen
    AfterScroll = QrTedCCfgCabAfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, '
      'car.Nome NO_CARTDEP, tcc.*'
      'FROM tedccfgcab tcc'
      'LEFT JOIN enticliint eci ON eci.CodCliInt=tcc.Empresa'
      'LEFT JOIN entidades ent ON ent.Codigo=eci.CodEnti'
      'LEFT JOIN carteiras car ON car.Codigo=tcc.CartDep'
      'WHERE tcc.Codigo > 0'
      '')
    Left = 64
    Top = 65
    object QrTedCCfgCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'tedccfgcab.Codigo'
    end
    object QrTedCCfgCabBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'tedccfgcab.Banco'
    end
    object QrTedCCfgCabAnoMes: TIntegerField
      FieldName = 'AnoMes'
      Origin = 'tedccfgcab.AnoMes'
    end
    object QrTedCCfgCabVersao: TWideStringField
      FieldName = 'Versao'
      Origin = 'tedccfgcab.Versao'
    end
    object QrTedCCfgCabTamReg: TIntegerField
      FieldName = 'TamReg'
      Origin = 'tedccfgcab.TamReg'
    end
    object QrTedCCfgCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'tedccfgcab.Nome'
      Size = 50
    end
    object QrTedCCfgCabSeqLay: TIntegerField
      FieldName = 'SeqLay'
      Origin = 'tedccfgcab.SeqLay'
    end
    object QrTedCCfgCabRemRet: TIntegerField
      FieldName = 'RemRet'
      Origin = 'tedccfgcab.RemRet'
    end
    object QrTedCCfgCabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'tedccfgcab.Lk'
    end
    object QrTedCCfgCabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'tedccfgcab.DataCad'
    end
    object QrTedCCfgCabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'tedccfgcab.DataAlt'
    end
    object QrTedCCfgCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'tedccfgcab.UserCad'
    end
    object QrTedCCfgCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'tedccfgcab.UserAlt'
    end
    object QrTedCCfgCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'tedccfgcab.AlterWeb'
    end
    object QrTedCCfgCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'tedccfgcab.Ativo'
    end
    object QrTedCCfgCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'tedccfgcab.Empresa'
    end
    object QrTedCCfgCabCartDep: TIntegerField
      FieldName = 'CartDep'
      Origin = 'tedccfgcab.CartDep'
    end
    object QrTedCCfgCabNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrTedCCfgCabNO_CARTDEP: TWideStringField
      FieldName = 'NO_CARTDEP'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrTedCCfgCabPathRem: TWideStringField
      FieldName = 'PathRem'
      Origin = 'tedccfgcab.PathRem'
      Size = 255
    end
    object QrTedCCfgCabPathRet: TWideStringField
      FieldName = 'PathRet'
      Origin = 'tedccfgcab.PathRet'
      Size = 255
    end
    object QrTedCCfgCabCedente: TWideStringField
      FieldName = 'Cedente'
      Size = 30
    end
  end
  object DsTedCCfgCab: TDataSource
    DataSet = QrTedCCfgCab
    Left = 92
    Top = 65
  end
  object QrTedCCfgIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 148
    Top = 65
    object QrTedCCfgItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTedCCfgItsRegistro: TIntegerField
      FieldName = 'Registro'
    end
    object QrTedCCfgItsSubReg: TIntegerField
      FieldName = 'SubReg'
    end
    object QrTedCCfgItsPosIni: TIntegerField
      FieldName = 'PosIni'
    end
    object QrTedCCfgItsPosFim: TIntegerField
      FieldName = 'PosFim'
    end
    object QrTedCCfgItsPosTam: TIntegerField
      FieldName = 'PosTam'
    end
    object QrTedCCfgItsCampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
    object QrTedCCfgItsRestricao: TWideStringField
      FieldName = 'Restricao'
      Size = 1
    end
    object QrTedCCfgItsTamInt: TIntegerField
      FieldName = 'TamInt'
    end
    object QrTedCCfgItsTamFlu: TIntegerField
      FieldName = 'TamFlu'
    end
    object QrTedCCfgItsFmtDta: TWideStringField
      FieldName = 'FmtDta'
    end
    object QrTedCCfgItsPreenchmto: TIntegerField
      FieldName = 'Preenchmto'
    end
    object QrTedCCfgItsCodSis: TIntegerField
      FieldName = 'CodSis'
    end
    object QrTedCCfgItsDefaultRem: TWideStringField
      FieldName = 'DefaultRem'
      Size = 50
    end
    object QrTedCCfgItsDefaultRet: TWideStringField
      FieldName = 'DefaultRet'
      Size = 50
    end
    object QrTedCCfgItsObrigatori: TSmallintField
      FieldName = 'Obrigatori'
    end
    object QrTedCCfgItsCliPreench: TSmallintField
      FieldName = 'CliPreench'
    end
    object QrTedCCfgItsDescri01: TWideStringField
      FieldName = 'Descri01'
      Size = 80
    end
    object QrTedCCfgItsDescri02: TWideStringField
      FieldName = 'Descri02'
      Size = 80
    end
    object QrTedCCfgItsDescri03: TWideStringField
      FieldName = 'Descri03'
      Size = 80
    end
    object QrTedCCfgItsDescri04: TWideStringField
      FieldName = 'Descri04'
      Size = 80
    end
    object QrTedCCfgItsDescri05: TWideStringField
      FieldName = 'Descri05'
      Size = 80
    end
    object QrTedCCfgItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTedCCfgItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTedCCfgItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTedCCfgItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTedCCfgItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTedCCfgItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTedCCfgItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTedCCfgItsFulSize: TSmallintField
      FieldName = 'FulSize'
    end
  end
  object DsTedCCfgIts: TDataSource
    DataSet = QrTedCCfgIts
    Left = 176
    Top = 65
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 652
    Top = 472
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 536
    Top = 472
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Tipo=1'
      'AND Codigo>0'
      '')
    Left = 792
    Top = 192
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 820
    Top = 192
  end
end
