unit TedCRemInn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmTedCRemInn = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    QrLct: TmySQLQuery;
    QrLctBanco: TIntegerField;
    QrLctAgencia: TIntegerField;
    QrLctContaCorrente: TWideStringField;
    QrLctDocumento: TFloatField;
    QrLctCredito: TFloatField;
    QrLctData: TDateField;
    QrLctDDeposito: TDateField;
    QrLctCNPJCPF: TWideStringField;
    QrLctEmitente: TWideStringField;
    Panel5: TPanel;
    Label36: TLabel;
    EdBanda: TdmkEdit;
    EdCMC_7: TdmkEdit;
    Label210: TLabel;
    EdTipific: TdmkEdit;
    Label37: TLabel;
    EdRegiaoCompe: TdmkEdit;
    Label38: TLabel;
    EdBanco: TdmkEdit;
    Label39: TLabel;
    EdAgencia: TdmkEdit;
    Label40: TLabel;
    EdConta: TdmkEdit;
    Label41: TLabel;
    EdCheque: TdmkEdit;
    DBGrid1: TDBGrid;
    SpeedButton1: TSpeedButton;
    DsLct: TDataSource;
    QrLctControle: TIntegerField;
    QrLctSub: TSmallintField;
    QrLctFatParcela: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatNum: TFloatField;
    Label1: TLabel;
    QrLctTedRem_Sit: TSmallintField;
    QrLctTedRem_Its: TIntegerField;
    QrLctDepositado: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure EdBandaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCMC_7Change(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure Desiste();
    procedure LocalizaCheque();
  public
    FOcorrBco, FMyOcorr: Integer;
    FSQLType: TSQLType;
    { Public declarations }
  end;

  var
  FmTedCRemInn: TFmTedCRemInn;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, MyDBCheck, TedCRemInnDis,
  TedC_Aux, TedCRemCab, MyListas;

{$R *.DFM}

procedure TFmTedCRemInn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTedCRemInn.Desiste();
begin
  Close;
end;

procedure TFmTedCRemInn.EdBandaChange(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //BtOk.Enabled := False;
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
    EdCMC_7.Text := Geral.SoNumero_TT(EdBanda.Text);
end;

procedure TFmTedCRemInn.EdBandaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Desiste();
end;

procedure TFmTedCRemInn.EdCMC_7Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7.Text;
    EdTipific.Text       := Banda.Tipo;
    EdRegiaoCompe.Text   := Banda.Compe;
    EdBanco.Text   := Banda.Banco;
    EdAgencia.Text := Banda.Agencia;
    EdConta.Text   := Banda.Conta;
    EdCheque.Text  := Banda.Numero;
    //
    LocalizaCheque();
    //
  end;
end;

procedure TFmTedCRemInn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmTedCRemInn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTedCRemInn.LocalizaCheque();
const
  Depositado = CO_CH_DEPOSITADO_TED;
var
  CMC7, BACC, BomPara: String;
  LctFatNum, FatNum, Valor: Double;
  TedRem_Sit, TedRem_Its, FatID, FatParcela, Sub, LctSitAnt, LctCtrAnt, OcorrBco,
  MyOcorr, Codigo, Controle, LctCtrl, LctFatID, LctFatParc, LctSub: Integer;
begin
  BACC := EdBanco.Text  + ' - ' + EdAgencia.Text +
          ' - ' + EdConta.Text + ' - ' + EdCheque.Text;
  case  FSQLType of
    stIns: TedRem_Sit := CO_SIT_TEDC_INSTRUCAO_ENTRADA_NO_BANCO; // Adicionado em lote (entrada), aguardando entrada no banco
    else begin
      Geral.MensagemBox(
      'Adi��o a lote concelada!' + sLineBreak +
      '"FSQLType" incorreto!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLct, Dmod.MyDB, [
  'SELECT Banco, Agencia, ContaCorrente, Documento, ',
  'Credito, Data, DDeposito, CNPJCPF, Emitente, ',
  'TedRem_Sit, TedRem_Its, Controle, Sub, ',
  'FatID, FatNum, FatParcela, Depositado ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0301,
  'AND Banco=' + Geral.FF0(EdBanco.ValueVariant),
  'AND Agencia=' + Geral.FF0(EdAgencia.ValueVariant),
  'AND ContaCorrente="' + EdConta.Text + '" ',
  'AND Documento=' + Geral.FF0(EdCheque.ValueVariant),
  'ORDER BY Vencimento DESC ',
  '']);
  //
  if QrLct.RecordCount > 0 then
  begin
    if TedC_Ax.AceitaChequeEmLote(FSQLType, QrLctDepositado.Value,
      QrLctTedRem_Sit.Value) then
    begin
      Codigo         := FmTedCRemCab.QrTedCRemCabCodigo.Value;
      LctCtrl        := QrLctControle.Value;
      LctSub         := QrLctSub.Value;
      LctFatID       := QrLctFatID.Value;
      LctFatNum      := QrLctFatNum.Value;
      LctFatParc     := QrLctFatParcela.Value;
      OcorrBco       := FOcorrBco;
      MyOcorr        := FMyOcorr;
      CMC7           := EdCMC_7.Text;
      LctSitAnt      := QrLctTedRem_Sit.Value;
      LctCtrAnt      := QrLctTedRem_Its.Value;
      BomPara        := Geral.FDT(QrLctDDeposito.Value, 1);
      Valor          := QrLctCredito.Value;
      //
      Controle :=
        UMyMod.BPGS1I32('tedcremits', 'Controle', '', '', tsPos, stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'tedcremits', False, [
      'Codigo', 'LctCtrl', 'LctSub',
      'LctFatID', 'LctFatNum', 'LctFatParc',
      'BomPara', 'Valor', 'OcorrBco',
      'MyOcorr', 'CMC7', 'LctSitAnt',
      'LctCtrAnt'], [
      'Controle'], [
      Codigo, LctCtrl, LctSub,
      LctFatID, LctFatNum, LctFatParc,
      BomPara, Valor, OcorrBco,
      MyOcorr, CMC7, LctSitAnt,
      LctCtrAnt], [
      Controle], True) then
      begin
        // Deve ser antes
        TedRem_Its := Controle;
        // Deve ser depois
        Controle := QrLctControle.Value;
        Sub := QrLctSub.Value;
        FatID := QrLctFatID.Value;
        FatNum := QrLctFatNum.Value;
        FatParcela := QrLctFatParcela.Value;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
        'TedRem_Sit', 'TedRem_Its', 'Depositado'], [
        'Controle', 'Sub',
        'FatID', 'FatNum', 'FatParcela'], [
        TedRem_Sit, TedRem_Its, Depositado], [
        Controle, Sub,
        FatID, FatNum, FatParcela], True);
      end;
      EdBanda.Text := '';
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'O cheque ' + BACC + ' foi adicionado ao lote!');
      //
    end;
  end else
    //BtOK.Enabled := False;
end;

procedure TFmTedCRemInn.SpeedButton1Click(Sender: TObject);
var
  CMC7: String;
  I: Integer;
begin
  if Geral.MensagemBox('A janela que ser� aberta serve somente para teste!' + sLineBreak +
  'N�o selecione nenhum cheque nela para enviar ao banco quando n�o for envio de teste!' + sLineBreak +
  'Confirma que a sele��o de cheques na janela ser� apenas para envio de teste?!',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if DBCheck.CriaFm(TFmTedCRemInnDis, FmTedCRemInnDis, afmoNegarComAviso) then
    begin
      FmTedCRemInnDis.ShowModal;
      CMC7 := FmTedCRemInnDis.FCMC7;
      FmTedCRemInnDis.Destroy;
      Application.ProcessMessages;
      if CMC7 <> '' then
      begin
        EdBanda.Text := '';
        for I := 1 to Length(CMC7) do
        begin
          EdBanda.Perform(WM_CHAR, Ord(CMC7[I]), 1);
          Sleep(25);
        end;
      end;
    end;
  end;
end;

{ TODO :   A  :: URGENTE! Tirar a compensa��o e sit>0 dos cheques enviados! }

end.
