unit TedC_Aux;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, UMySQLModule, dmkGeral,
  StdCtrls, Variants, UnMLAGeral, DmkDAC_PF, UnDmkEnums;

type
  TSitChequeNoBanco = (enbNaoSei, enbLimbo, enbNao, enbSim);
  TTedC_Aux = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    //
    //
    function  TedChequeTipoDeMovimento(const Banco: Integer;
              const Envio: TEnvioCNAB;
              const OcorrBco, TamReg: Integer; var MyOcorr: Integer;
              var Descricao, Aviso: String; var SQLType: TSQLType): Boolean;
    function  TedChequeSelecionaOcorrencia(const Banco: Integer;
              const Envio: TEnvioCNAB; const TamReg: Integer;
              var OcorrBco, MyOcorr: Integer; var SQLType: TSQLType): Boolean;
    function  AceitaChequeEmLote(SQLType: TSQLType; Depositado, TedRem_Sit:
              Integer): Boolean;
    function  DescricaoDeMyOcorrTedC(MyOcorr: Integer): String;
    function  DescricaoDeTedRem_Sit(Situacao: Integer): String;
    //
    // J A N E L A S
    procedure MostraTedCCfgCab(Codigo: Integer);
    procedure MostraTedCOcoSys(Codigo: Integer);
    procedure MostraTedCRemCab(Codigo, FatParcela: Integer);
    procedure MostraTedCRetArq();
    procedure MostraTedCRetCab();
    // R E M E S S A
    function  AddCampoNaLinhaAtual(const Layout, Registro, SubReg: Integer;
              const Segmento: String; const SubSeg: Integer; var Linha: String;
              const DefaultRem, Restricao, FmtDta: String; const Preenchmto,
              CodSis, Obrigatori, CliPreench, FulSize, InvertAlin, CortaStr,
              PosIni, PosFim, PosTam, TamInt, TamFlu: Integer; const Txt1, Txt2:
              String; const Memo: TMemo; var Continua: Boolean; var Reg: TFldTabRec;
              const Lista: TList; var Texto: String): Boolean;
    function  AddFld(var Reg: TFldTabRec; var Lista: TList;
              const Codigo: Integer; const Valor: Variant): Boolean;
    function  AddLinhaAoMemo(const Memo: TMemo; var Linha: String; const TamReg:
              Integer; var Continua: Boolean): Boolean;
    function  ChequeSitNoBanco(TedRem_Its: Integer): TSitChequeNoBanco;
    function  ChequeEstaNoBanco(TedRem_Its: Integer; Avisa: Boolean): Boolean;
    function  ChequeNaoEstaNoBanco(TedRem_Its: Integer; Avisa: Boolean): Boolean;
    function  CriaLista(const Layout, Registro, SubReg: Integer; const Segmento:
              String; const SubSeg: Integer; var Reg: TFldTabRec; var Lista:
              TList; var PosAnt: Integer; var Txt1: String): Boolean;
    function  CriaTabAux(const Layout, Registro, Subreg: Integer;
              const Segmento: String; const SubSeg: Integer;
              const Tabela: String): Boolean;
    function  GeoCampoCorreto(const Campo, Descri01, Descri02, Descri03,
              Descri04, Descri05: String; const PosIni, PosFim, PosTam, TamInt,
              TamFlu: Integer; var FPosIni, FPosFim, FPosTam, FTamInt, FTamFlu:
              Integer; const Txt1: String; var Txt2: String; var FposAnt:
              Integer): Boolean;
    function  GeraLinha(const Layout, Registro, Subreg: Integer;
              const Segmento: String; const SubSeg: Integer; var Linha: String;
              var Continua: Boolean; var PosIni, PosFim, PosTam, TamInt,
              TamFlu: Integer; var Txt1, Txt2: String; var PosAnt: Integer;
              const Memo: TMemo; var Reg: TFldTabRec; const Lista: TList;
              const Tabela: String; var Seq: Integer): Boolean;
    function  MotaNomeTabela(Registro, SubReg: Integer; Segmento: String;
              SubSeg: Integer): String;
    function  NaoContinua(Continua: Boolean): Boolean;
    function  ObtemValorDeCodSis(var Texto: String; const FmtDta: String;
              const CodSis, TamInt, TamFlu: Integer;
              var Reg: TFldTabRec; const Lista: TList;
              const Memo: TMemo; const Linha: String;
              var Continua: Boolean): Boolean;
    function  PreparaExportacao(const Memo: TMemo; var Continua: Boolean):
              Boolean;
    procedure QuebraGeracao(const Memo: TMemo; const Linha: String;
              var Continua: Boolean);
    function  VariavelToTxtArqRem(var Texto: String; const Variavel: Variant;
              const FmtDta: String; const TamInt, TamFlu: Integer): Boolean;

    //
    // R E T O R N O
    function  Nada(): Boolean;
    procedure AnalisaRetLinhaReg0(const Layout, Empresa: Integer; const CNPJEmp,
              Arquivo: String; const Linhas: TStrings;
              const LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2, LaAvisoR1, LaAvisoR2: TLabel;
              var NoErro: String; var Erro, Step, BancoTED, TedCRetCab: Integer);
    procedure AnalisaRetLinhaReg1(const TedCRetCab, Layout, TamReg, Empresa,
              BancoTED, LctFatID: Integer; const CNPJEmp, Arquivo: String;
              const Linhas: TStrings;
              const LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2, LaAvisoR1, LaAvisoR2: TLabel;
              const TabLctA: String;
              var NoErro: String; var Erro, Step, UltLin1, Itens: Integer);
    procedure AnalisaRetLinhaReg9(const TedCRetCab, Layout, Linha: Integer;
              Arquivo: String; const Linhas: TStrings;
              const LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2, LaAvisoR1, LaAvisoR2: TLabel;
              var NoErro: String; var Erro, Step: Integer);
    function  DesmembraUsoEmpresa(const UsoEmpresa: String; var LctCtrl, LctSub:
              Integer; var LctFatNum: Double; var LctFatParc: Integer): Boolean;
    function  TextoParaUser(Step: Integer; TxtInconsiste: String): String;
    procedure VerificaItemDeRetorno(TedCRetCab, FatID: Integer;
              TabLctA: String;
              LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2: TLabel);
    //
    // L A Y O U T S   I M P L E M E N T A D O S
    function  SQL_422_2007_04_300(Codigo: Integer): Boolean;
    //
  end;

const
////////////////////////// TIPOS DE LAYOUTS ////////////////////////////////////
  VAR_TED_U0N1U9 = 1; // Arquivo 0=Header 1=Detail e 9=Trailler
////////////////////// FIM DE TIPOS DE LAYOUTS /////////////////////////////////
//
////////////////////////// STEPS DE ARQUIVO RETORNO ////////////////////////////
///  Step da tabela 'tedcretcab'
  CO_TEDC_STEP_ARQ_CRIADO = 0;
  CO_TEDC_STEP_ARQ_ITENS_CARREGADOS = 1;
  CO_TEDC_STEP_ARQ_ARQUIVO_LIDO = 9;
  CO_TEDC_STEP_ARQ_ITENS_PROCESSADOS = 10;
////////////////////// FIM DE STEPS DE ARQUIVO RETORNO /////////////////////////
//
////////////////////////// STEPS DE ITEM DE RETORNO ////////////////////////////
///  Step da tabela 'tedcretits'
  CO_TEDC_STEP_ITEM_ABERTO = 0;
  CO_TEDC_STEP_ITEM_INCONSISTENTE = 1;
  CO_TEDC_STEP_ITEM_ENCERRADO = 9;
////////////////////// FIM DE STEPS DE ITEM DE RETORNO /////////////////////////
//
////////////////////////// STEPS DE LOG DE ITEM DE RETORNO ////////////////////////////
///  Step da tabela 'tedcretlog'
  CO_TEDC_STEP_LOG_ITEM_ABERTO = 0;
  CO_TEDC_STEP_LOG_ITEM_ENCERRADO = 9;
////////////////////// FIM DE STEPS DE ITEM DE RETORNO /////////////////////////
//
/////////////////////////////// OCORR�NCIAS ////////////////////////////////////
  // Tabela: 'TedCOcoSys'
  CO_MYOCORR_TEDC_NULO = -1;
  CO_MYOCORR_TEDC_ENVIO_AO_BANCO = 1;
  CO_MYOCORR_TEDC_ALTERAR_BOM_PARA = 2;
  CO_MYOCORR_TEDC_INSTRUCAO_BAIXAR = 9;
  //
  CO_MYOCORR_TEDC_ENTRADA_CONFIRMADA = 201;
  CO_MYOCORR_TEDC_TRANSFERENCIA_DE_CEDENTE = 210;
  CO_MYOCORR_TEDC_PRORROGACAO_DE_VECIMENTO = 211;
  CO_MYOCORR_TEDC_ALTERACAO_DE_VECIMENTO = 214;
  CO_MYOCORR_TEDC_CARTEIRA_EM_SER = 220;
  CO_MYOCORR_TEDC_PREVIA_DO_BORDERO = 230;
  CO_MYOCORR_TEDC_ENTRADA_REJEITADA = 231;
  CO_MYOCORR_TEDC_DEPOSITO = 250;
  CO_MYOCORR_TEDC_BAIXA_POR_INSTRUCAO = 261;
  CO_MYOCORR_TEDC_PRIMEIRA_DEVOLUCAO = 295;
  CO_MYOCORR_TEDC_REAPRESENTACAO = 296;
  CO_MYOCORR_TEDC_SEGUNDA_DEVOLUCAO = 297;
  CO_MYOCORR_TEDC_LIQUIDACAO_POR_DEBITO_EM_CONTA = 298;
////////////////////////////  FIM  OCORR�NCIAS /////////////////////////////////
///
//////////////////////////  STATUS /////////////////////////////////////////////
//     Usado no campo TedRem_Sit da tabela lct 0001a
// 0=N�o adicionado em nenhum lote                                             /
// 1=Adicionado em lote (entrada), aguardando entrada no banco                 /
// 2=Adicionado em lote (instru��o), aguardando altera��o no banco             /
// 3=Entrou no banco                                                           /
// 4=Alterou no banco                                                          /
// 9=Baixou por qualquer motivo                                                /
  CO_SIT_TEDC_DESCONHECIDO = -1;
  CO_SIT_TEDC_NADA = 0;
  CO_SIT_TEDC_INSTRUCAO_ENTRADA_NO_BANCO = 1;
  CO_SIT_TEDC_LIMBO_ENTRADA_NO_BANCO = 2;
  CO_SIT_TEDC_ENTROU_NO_BANCO = 3;
  CO_SIT_TEDC_INSTRUCAO_ALTERACAO_NO_BANCO = 4;
  CO_SIT_TEDC_LIMBO_ALTERACAO_NO_BANCO = 5;
  CO_SIT_TEDC_ALTEROU_NO_BANCO = 6;
  CO_SIT_TEDC_BAIXOU_NO_BANCO = 9;
  //  Todos que est�o no banco!
  CO_SIT_TEDC_ESTA_NO_BANCO = '3,4,5,6';
  //
////////////////////////  FIM  STATUS /////////////////////////////////////////////
//
/////////////////////////////// INCONSIST�NCIAS ////////////////////////////////
  //  Tabela:   'TedCCfgSys'
  CO_TEDC_ERR_OCRR_NAO_IMPLEMENTADA = 1;
  CO_TEDC_ERR_DATABOA = 2;
  CO_TEDC_ERR_DOCUMENTO_JA_COMPENSADO = 3;
////////////////////////////  FIM INCONSIST�NCIAS ////////////////////////////////
///
//
/////////////////////////////// DEPOSITADO ////////////////////////////////
  //  Tabela:   Lct... Campo 'Depositado'
  CO_CH_DEPOSITADO_NAO = 0;
  CO_CH_DEPOSITADO_MANUAL = 1;
  CO_CH_DEPOSITADO_TED = 2;
////////////////////////////  FIM INCONSIST�NCIAS ////////////////////////////////
///

var
  TedC_Ax: TTedC_Aux;

implementation

uses UnMyObjects, Module, ModuleTedC, ModuleGeral, UnBancos, TedCCfgCab,
  MyDBCheck, TedCOcoSys, TedCRemCab, TedCRetArq, TedCRetCab;

function TTedC_Aux.AceitaChequeEmLote(SQLType: TSQLType;
  Depositado, TedRem_Sit: Integer): Boolean;
var
  Msg: String;
/////////////////// TecC_Sit ///////////////////////////////////////////////////
// 0=N�o adicionado em nenhum lote
// 1=Adicionado em lote de entrada, aguardando entrada no banco
// 2=Limbo. Banco recebeu a instru��o de entrada mas n�o se posicionou sobre ela.
// 3=Entrou no banco
// 4=Adicionado em lote de instru��o, aguardando altera��o no banco
// 5=Limbo. Banco recebeu a instru��o de altera��o mas n�o se posicionou sobre ela.
// 6=Alterou no banco
// 9=Baixou por qualquer motivo
////////////////////////////////////////////////////////////////////////////////
///
begin
  Result := False;
  Msg := '';
  if Depositado = CO_CH_DEPOSITADO_MANUAL then
  begin
    Msg := 'O cheque selecionado j� foi depositado manualmente!';
  end else
  begin
    case SQLType of
      stIns:
      begin
        case TedRem_Sit of
          0: Result := True;
          1,2: Msg := 'O cheque selecionado j� est� em um lote e aguarda confirma��o de entrada no banco!';
          3,6: Msg := 'O cheque selecionado j� est� no banco em cust�dia!';
          4,5: Msg := 'O cheque selecionado j� est� no banco em cust�dia e aguarda confirma��o de instru��o de altera��o! ';
          9:
          begin
            if Geral.MensagemBox('O cheque selecionado j� foi baixado!' +
            sLineBreak + 'Deseja envi�-lo ao banco novamente?',
            'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
              Result := True;
          end;
        end;
      end;
      stUpd:
      begin
        case TedRem_Sit of
          0:
          begin
            if Geral.MensagemBox(
            'O cheque selecionado ainda n�o foi adicionado a nenhum lote!' + sLineBreak +
            'Portanto n�o pode ter nenhuma instru��o de altera��o!' + sLineBreak +
            'Deseja anviar a instru��o assim mesmo?',
            'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
              Result := True;
          end;
          1,2: Msg := 'O cheque selecionado j� est� em um lote e aguarda confirma��o de entrada no banco!';
          3,6: Result := True;
          4,5: Msg := 'O cheque selecionado j� est� em um lote e aguarda confirma��o de instru��o no banco!';
          9: Msg := 'O cheque selecionado j� foi baixado, portanto n�o pode ter nova instru��o!';
        end;
      end;
      else Msg := 'Forma de envio n�o implementada! AVISE A DERMATEK!';
    end;
  end;
  if (Result = False) and (Msg <> '') then
    Geral.MensagemBox(Msg, 'Aviso', MB_OK+MB_ICONWARNING);
end;

function TTedC_Aux.AddCampoNaLinhaAtual(const Layout, Registro, SubReg: Integer;
  const Segmento: String; const SubSeg: Integer; var Linha: String; const
  DefaultRem, Restricao, FmtDta: String; const Preenchmto, CodSis, Obrigatori,
  CliPreench, FulSize, InvertAlin, CortaStr, PosIni, PosFim, PosTam, TamInt,
  TamFlu: Integer; const Txt1, Txt2: String; const Memo: TMemo; var Continua:
  Boolean; var Reg: TFldTabRec; const Lista: TList; var Texto: String): Boolean;
var
  Dif: Integer;
begin
  Texto := '';
  Dif := PosIni - Length(Linha) - 1;
  if Dif <> 0 then
  begin
    Geral.MensagemBox('Tamanho do texto parcial (pr� adi��o) difere do esperado!'
    + sLineBreak + 'Tamanho do texto parcial: ' + Geral.FF0(Length(Linha)) + sLineBreak +
    Txt1 + Txt2, 'Aviso', MB_OK+MB_ICONWARNING);
    //
    QuebraGeracao(Memo, Linha, Continua);
  end;
  case Preenchmto of
    // 1=Vari�vel (CodSis),
    1:
    begin
      if not ObtemValorDeCodSis(Texto, FmtDta, CodSis, TamInt, TamFlu,
      Reg, Lista, Memo, Linha, Continua) then
      begin
        QuebraGeracao(Memo, Linha, Continua);
        Exit;
      end;
    end;
    // 2=Fixo (DefaultRem/Ret)
    2:
    begin
      if (Obrigatori = 1) and (DefaultRem = '') then
      begin
        if CliPreench = 1 then
          Geral.MensagemBox(Txt1 + Txt2 +
          '"Preenchmto" de valor Default sem valor default definido!' +sLineBreak +
          'OSERVA��O: A informa��o deve ser preenchida pelo usu�rio no cadastro de configura��o de remessa/retorno!',
          'ERRO', MB_OK+MB_ICONERROR)
        else
          Geral.MensagemBox(Txt1 + Txt2 +
          '"Preenchmto" de valor Default sem valor default definido!',
          'ERRO', MB_OK+MB_ICONERROR);
        //
        QuebraGeracao(Memo, Linha, Continua);
        Exit;
      end else
        Texto := DefaultRem;
    end;
    // 3=Brancos,
    3:
    begin
      Texto := ' ';
      while Length(Texto) < PosTam do
        Texto := Texto + ' ';
      if Restricao <> 'X' then
      begin
        Geral.MensagemBox(Txt1 + Txt2 +
        '"Preenchmto" de brancos com "Restri��o" diferente de "X"',
        'ERRO', MB_OK+MB_ICONERROR);
        QuebraGeracao(Memo, Linha, Continua);
        Exit;
      end;
    end;
    // 4=SET(Val1|Explic1|Val2|Explic2|...),
    4:
    begin
      if not ObtemValorDeCodSis(Texto, FmtDta, CodSis, TamInt, TamFlu,
      Reg, Lista, Memo, Linha, Continua) then
      begin
        QuebraGeracao(Memo, Linha, Continua);
        Exit;
      end;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(DmTedC.QrTCCSet, Dmod.MyDB, [
      'SELECT ValBco ',
      'FROM tedccfgset ',
      'WHERE Codigo=' + Geral.FF0(Layout),
      'AND Registro=' + Geral.FF0(Registro),
      'AND SubReg=' + Geral.FF0(SubReg),
      'AND Segmento="' + Segmento + '" ',
      'AND SubSeg=' + Geral.FF0(SubSeg),
      'AND PosIni=' + Geral.FF0(PosIni),
      'AND ValSis="' + Texto + '" ',
      '']);
      Texto := DmTedC.QrTCCSetValBco.Value;
    end;
    // 5=Zeros
    5:
    begin
      Texto := '0';
      while Length(Texto) < PosTam do
        Texto := Texto + '0';
      if (Restricao <> '9') and (Restricao <> 'D') then
      begin
        Geral.MensagemBox(Txt1 + Txt2 +
        '"Preenchmto" de zeros com "Restri��o" diferente de "9" e de "D"',
        'ERRO', MB_OK+MB_ICONERROR);
        QuebraGeracao(Memo, Linha, Continua);
        Exit;
      end;
    end;
    // ?=N�o implementado
    else begin
      Geral.MensagemBox('"Preenchmto" n�o implementado: ' + Geral.FF0(
      Preenchmto), 'ERRO', MB_OK+MB_ICONERROR);
      QuebraGeracao(Memo, Linha, Continua);
      Exit;
    end;
  end;
  // Completa string
  case Ord(Restricao[1]) of
    Ord('X'):
    begin
       if InvertAlin = 1 then
       begin
         while Length(Texto) < PosTam do
           Texto := ' ' + Texto;
       end else begin
         while Length(Texto) < PosTam do
           Texto := Texto + ' ';
       end;
    end;
    Ord('9'):
    begin
       if InvertAlin = 1 then
       begin
         while Length(Texto) < PosTam do
           Texto := Texto + '0'
       end else begin
         while Length(Texto) < PosTam do
           Texto := '0' + Texto;
       end;
    end;
    Ord('D'): (*Nada*);
    else begin
      Geral.MensagemBox('"Restricao" n�o implementada: "' + Restricao + '"',
      'ERRO', MB_OK+MB_ICONERROR);
      QuebraGeracao(Memo, Linha, Continua);
      Exit;
    end;
  end;
  // Ajusta Strings grandes, como nomes e endere�os
  if CortaStr = 1 then
  begin
    if Length(Texto) > PosTam then
      Texto := Copy(Texto, 1, PosTam);
  end;
  Linha := Linha + Texto;
  Dif := PosFim - Length(Linha);
  if Dif <> 0 then
  begin
    Geral.MensagemBox('Tamanho do texto parcial (p�s adi��o) difere do esperado!'
    + sLineBreak + 'Tamanho do texto parcial: ' + Geral.FF0(Length(Linha)) + sLineBreak +
    Txt1 + Txt2, 'Aviso', MB_OK+MB_ICONWARNING);
    //
    QuebraGeracao(Memo, Linha, Continua);
  end;
end;

function TTedC_Aux.AddFld(var Reg: TFldTabRec; var Lista: TList;
  const Codigo: Integer; const Valor: Variant): Boolean;
begin
  //Result := False;
  New(Reg);
  Reg.Codigo := Codigo;
  Reg.Valor  := Valor;
  Lista.Add(Reg);
  //
  Result := True;
end;

function TTedC_Aux.AddLinhaAoMemo(const Memo: TMemo; var Linha: String;
  const TamReg: Integer; var Continua: Boolean): Boolean;
var
  Dif: Integer;
begin
  Result := False;
  Dif := Tamreg - Length(Linha);
  if Dif <> 0 then
  begin
    Geral.MensagemBox('Tamanho da �ltima linha gerada n�o confere com o esperado!'
    + sLineBreak + 'Tamanho da �ltima linha: ' + Geral.FF0(Length(Linha)) + sLineBreak +
    'Tamanho esperado da linha: ' + Geral.FF0(TamReg),
    'Aviso', MB_OK+MB_ICONWARNING);
    //
    QuebraGeracao(Memo, Linha, Continua);
  end else
  begin
    Memo.Lines.Add(Linha);
    Result := True;
  end;
  Linha := '';
end;

procedure TTedC_Aux.AnalisaRetLinhaReg0(const Layout, Empresa: Integer; const
  CNPJEmp, Arquivo: String; const Linhas: TStrings;
  const LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2, LaAvisoR1, LaAvisoR2: TLabel;
  var NoErro: String; var Erro, Step, BancoTED, TedCRetCab: Integer);
const
  Registro = 0;
  Subreg = 0;
  Segmento = '';
  SubSeg = 0;
var
  CodSis, Codigo, PosIni, Itens: Integer;
  TxtLin, ValTxt, ValNum, Nome,
  Produto, Cedente, Filial, CNPJCPF, Depositari, Terceira: String;
  CodReg, RemRet, DefRet, Banco, NumSeqGer, TedC_Cfg: Integer;
  DataGer: TDateTime;
  SQLType: TSQLType;
begin
  CodSis := 0;
  PosIni := 0;
  TedCRetCab := 0;
  Erro := 1003;
  SQLType := stIns;
  Codigo := 0;
  DefRet := 0;
  try
    MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True,
      'Carregando arquivo: "' + Arquivo + '"');
    MyObjects.Informa2(LaAvisoB1, LaAvisoB2, True,
      'Incluindo registro Tipo "' + Geral.FF0(Registro) + '" da Linha n� 1');
    MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
    //
    Produto := '';
    Cedente := '';
    Filial := '';
    CNPJCPF := '';
    CodReg := -1;
    RemRet := 0;
    Banco := 0;
    NumSeqGer := 0;
    DataGer := 0;
    //
    Depositari := '';
    Terceira := '';
    //
    TxtLin := Linhas[0];
    DmTedC.ReopenTCCIts(Layout, Registro, Subreg, Segmento, SubSeg);
    //
    DmTedC.QrTCCIts.First;
    while not DmtedC.QrTCCIts.Eof do
    begin
      CodSis := DmtedC.QrTCCItsCodSis.Value;
      PosIni := DmtedC.QrTCCItsPosIni.Value;
      ValTxt := Trim(Copy(TxtLin, PosIni, DmtedC.QrTCCItsPosTam.Value));
      if ValTxt = '' then
      begin
        ValNum := '0';
      end else begin
        ValNum := ValTxt;
      end;
      case CodSis of
        // C�digo do registro (Header / Detalhe / Trailler)
        001: CodReg := Geral.IMV(ValTxt);
        // C�digo de identifica��o do arquivo (Remessa / Retorno)
        002:
        begin
          RemRet := Geral.IMV(ValTxt);
          DefRet := Geral.IMV(Trim(DmtedC.QrTCCItsDefaultRet.Value));
        end;
        // Literal de identifica�ao do arquivo (Remessa / Retorno)
        003: Nada();//
        // Servi�o - C�digo do produto (informado pelo banco)
        004: Produto := ValTxt;
        // Servi�o - Nome
        005: Nada();
        // C�digo da empresa no banco
        006: Cedente := ValTxt;
        // C�digo da filial
        007: Filial := ValTxt;
        //  Empresa - Tipo de inscri��o
        008: Nada();
        // Empresa - N�mero da inscri��o
        009: CNPJCPF := ValTxt;
        // Empresa - Nome
        010: Nada();
        // C�digo do Banco na compensa��o
        011: Banco := Geral.IMV(ValNum);
        // Nome do Banco por extenso
        012: Nada();
        // C�digo da ag�ncia deposit�ria (Uso do Banco)
        013: Depositari := ValTxt;
        // C�digo da empresa terceirizada
        014: Terceira := ValTxt;
        // Data da grava��o do arquivo (gera��o do lote no sistema)
        090: Geral.MontaDataPorFmt(ValNum, DmtedC.QrTCCItsFmtDta.Value, False, DataGer);
        // N�mero do arquivo (C�digo do lote de envio)
        091: NumSeqGer := Geral.IMV(ValTxt);
        // N�mero sequencial do registro
        099: Nada();//
        // Brancos
        999: Nada();//
        else begin
          Erro := 1901;
          {
          Geral.MensagemBox(
          'C�digo interno de tipo de campo (CodSis) n�o implementado!' + sLineBreak +
          'Local: "0A"' + sLineBreak + 'CodSis: ' + Geral.FF0(CodSis),
          'ERRO', MB_OK+MB_ICONERROR);
          }
        end;
      end;
      DmtedC.QrTCCIts.Next;
    end;
    PosIni := -1;
    CodSis := -1;
    BancoTED := Banco;
    if Erro = 1003 then
      if CodReg <> 0 then
        Erro := 001
    else
    if Erro = 1003 then
      if RemRet <> DefRet then
        Erro := 002
    else
      if CNPJEmp <> CNPJCPF then
        Erro := 009
    else {?};
    //
    if Erro = 1003 then
    begin
      if DmTedC.ReopenTCTCab(Produto, Cedente, Filial, CNPJCPF, Banco, NumSeqGer,
      DataGer) then
      begin
        if DmTedC.QrTCTCab.RecordCount > 0 then
        begin
          if DmTedC.QrTCTCabStep.Value > CO_TEDC_STEP_ARQ_ITENS_CARREGADOS then
            Erro := 1002 // Arquivo j� carregado!
          else begin
            SQLType := stUpd;
            Codigo := DmTedC.QrTCTCabCodigo.Value;
          end
        end else
        begin
            // est� configurado no in�cio da procedure
        end;
        if Erro = 1003 then
        begin
          TedC_Cfg := Layout;
          Step := CO_TEDC_STEP_ARQ_CRIADO; // 0
          Itens := -1;
          Nome := Geral.ReduzNomeDeCaminhoDeArquivo(Arquivo, 255);
          //
          Codigo := UMyMod.BPGS1I32('tedcretcab', 'Codigo', '', '',
            tsPos, SQLType, Codigo);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'tedcretcab', False, [
          'Empresa', 'TedC_Cfg', 'Step',
          'Produto', 'Cedente', 'Filial',
          'CNPJCPF', 'Banco', 'DataGer',
          'NumSeqGer', 'Depositari', 'Terceira',
          (*'CartQtd', 'CartVal', 'EntrQtd',
          'EntrVal', 'DepoQtd', 'DepoVal',
          'BaxaQtd', 'BaxaVal', 'DevlQtd',
          'DevlVal', 'ReapQtd', 'ReapVal',
          'UltLin',*) 'Nome', 'Itens'], [
          'Codigo'], [
          Empresa, TedC_Cfg, Step,
          Produto, Cedente, Filial,
          CNPJCPF, Banco, DataGer,
          NumSeqGer, Depositari, Terceira,
          (*CartQtd, CartVal, EntrQtd,
          EntrVal, DepoQtd, DepoVal,
          BaxaQtd, BaxaVal, DevlQtd,
          DevlVal, ReapQtd, ReapVal,
          UltLin,*) Nome, Itens], [
          Codigo], True) then
          begin
            TedCRetCab := Codigo;
            Erro := 0;
            //
            MyObjects.Informa2(LaAvisoB1, LaAvisoB2, True,
              'Leitura do Registro Tipo "' + Geral.FF0(Registro) + '" conclu�do');
            MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
          end;
          //
        end;
      end;
    end;
  finally
    DmTedC.TedCCfgSys_MostraTexto(Erro, '',
    'PosIni = ' + Geral.FF0(PosIni) + ' - CodSis = ' + Geral.FF0(CodSis),
    LaAvisoR1, LaAvisoR2, NoErro);
  end;
end;

procedure TTedC_Aux.AnalisaRetLinhaReg1(const TedCRetCab, Layout, TamReg,
  Empresa, BancoTED, LctFatID: Integer; const CNPJEmp, Arquivo: String;
  const Linhas: TStrings;
  const LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2, LaAvisoR1, LaAvisoR2: TLabel;
  const TabLctA: String;
  var NoErro: String; var Erro, Step, UltLin1, Itens: Integer);
const
  Registro = 1;
  Subreg = 0;
  Segmento = '';
  SubSeg = 0;
var
  I, CodSis, PosIni, Casas, MyOcorr: Integer;
  TxtLin, ValTxt, ValNum, FmtDta: String;
  //
  CodReg, OcorrBco, BorderoBco, Alinea, CodRejeito, TrfParcela, Banco, Agencia,
  Cheque, Praca, Linha: Integer;
  Cedente, Filial, UsoEmpresa, CMC7, RetInscTip, RetInscNum, RetEmitNom,
  TrfCedente, TrfContrat, ContaCorre: String;
  DtaCaptura, RemDataBoa, RetDataBoa, TrfDtaOper, TrfDtaCntr: TDateTime;
  RemValor, RetValor: Double;
  SeuNumero, NossoNum: Int64;
  //
  Codigo, LctCtrl, LctSub, LctFatParc, Controle: Integer;
  LctFatNum: Double;
  nilDescricao, nilAviso: String;
  nilSQLTyp: TSQLType;
begin
  CodSis := 0;
  PosIni := 0;
  Itens  := 0;
  try
    MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True,
      'Carregando arquivo: "' + Arquivo + '"');
    MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
    //
    Codigo := TedCRetCab;
    // Elimina registros se o arquivo foi lido parcialmente anteriormente!
    UMyMod.ExcluiRegistroInt1('', 'tedcretits', 'Codigo', Codigo, Dmod.MyDB);
    // Fm Exclus�o!
    for I := 1 to Linhas.Count - 2 do
    begin
      Itens := Itens + 1;
      MyObjects.Informa2(LaAvisoB1, LaAvisoB2, True, 'Incluindo registro Tipo "' +
        Geral.FF0(Registro) + '" da Linha n� ' + Geral.FF0(I + 1));
      Erro := 1004;
      {001}CodReg := 0;
      {006}Cedente := '';
      {007}Filial := '';
      {099}Linha := 0;
      {101}OcorrBco := 0;
      {102}UsoEmpresa := '';
      {103}SeuNumero := 0;
      {104}DtaCaptura := 0;
      {105}CMC7 := '';
      {106}RetInscTip := '';
      {107}RetInscNum := '';
      {108}RetEmitNom := '';
      {109}RemValor := 0;
      {110}RemDataBoa := 0;
      {111}BorderoBco := 0;
      {112}Alinea := 0;
      {113}CodRejeito := 0;
      {114}NossoNum := 0;
      {115}RetValor := 0;
      {116}RetDataBoa := 0;
      {117}TrfCedente := '';
      {118}TrfDtaOper := 0;
      {119}TrfContrat := '';
      {120}TrfParcela := 0;
      {121}TrfDtaCntr := 0;
      {122}Banco := 0;
      {123}Agencia := 0;
      {124}ContaCorre := '';
      {125}Cheque := 0;
      {126}Praca := 0;
      {999Nada()}
      //
      TxtLin := Linhas[I];
      DmTedC.ReopenTCCIts(Layout, Registro, Subreg, Segmento, SubSeg);
      //
      DmtedC.QrTCCIts.First;
      while not DmtedC.QrTCCIts.Eof do
      begin
        CodSis := DmtedC.QrTCCItsCodSis.Value;
        PosIni := DmtedC.QrTCCItsPosIni.Value;
        FmtDta := DmtedC.QrTCCItsFmtDta.Value;
        Casas := DmtedC.QrTCCItsTamFlu.Value;
        ValTxt := Trim(Copy(TxtLin, PosIni, DmtedC.QrTCCItsPosTam.Value));
        if ValTxt = '' then
        begin
          ValNum := '0';
        end else begin
          ValNum := ValTxt;
        end;
        case CodSis of
          // C�digo do registro (Header / Detalhe / Trailler)
          001: CodReg := Geral.IMV(ValTxt);
          // C�digo da empresa no banco
          006: Cedente := ValTxt;
          // C�digo da filial
          007: Filial := ValTxt;
          // N�mero sequencial do registro
          099: Linha := Geral.IMV(ValTxt);
          // C�digo da ocorr�ncia (Remessa/Retorno)
          101: OcorrBco := Geral.IMV(ValTxt);
          // Uso da empresa (Hexadecimal: lct.Controle + lct.Sub + lct.FatNum + lct.FatParcela)
          102: UsoEmpresa := ValTxt;
          // Seu Numero > (TedCRemIts.Controle)
          103: SeuNumero := Geral.I64(ValTxt);
          // Data do evento
          104: Geral.MontaDataPorFmt(ValNum, FmtDta, False, DtaCaptura);
          // Identifica��o do cheque (CMC7)
          105: CMC7 := ValTxt;
          // Emitente do cheque - Tipo de inscri��o
          106: RetInscTip := ValTxt;
          // Emitente do cheque - N�mero da inscri��o
          107: RetInscNum := ValTxt;
          // Emitente do cheque - Nome
          108: RetEmitNom := ValTxt;
          // Cheque - Valor (Remessa)
          109: Geral.MontaValDblPorFmt(ValNum, Casas, RemValor);
          // Cheque - Data boa para compensa��o (Remessa)
          110: Geral.MontaDataPorFmt(ValNum, FmtDta, False, RemDataBoa);
          // N�mero do Border� (Retorno - Informado pelo banco
          111: BorderoBco := Geral.IMV(ValTxt);
          // Motivo da devolu��o (Al�nea)
          112: Alinea := Geral.IMV(ValNum);
          // C�digo de rejei��o
          113: CodRejeito := Geral.IMV(ValNum);
          // Nosso N�mero (Banco)
          114: NossoNum := Geral.I64(ValNum);
          // Valor do cheque (Retorno)
          115: Geral.MontaValDblPorFmt(ValNum, Casas, RetValor);
          // Data boa (Retorno)
          116: Geral.MontaDataPorFmt(ValNum, FmtDta, True, RetDataBoa);
          // C�digo do cedente para transfer�ncia
          117: TrfCedente := ValTxt;
          // Data da opera��o cess�o
          118: Geral.MontaDataPorFmt(ValNum, FmtDta, True, TrfDtaOper);
          // N�mero do contrato
          119: TrfContrat := ValTxt;
          // N�mero da parcela
          120: TrfParcela := Geral.IMV(ValNum);
          // Data emiss�o
          121: Geral.MontaDataPorFmt(ValNum, FmtDta, True, TrfDtaCntr);
          // Banco (Retorno)
          122: Banco := Geral.IMV(ValNum);
          // Ag�ncia (Retorno)
          123: Agencia := Geral.IMV(ValNum);
          // Conta (Retorno)
          124: ContaCorre := ValTxt;
          // Cheque (Retorno)
          125: Cheque := Geral.IMV(ValNum);
          // Comp (Retorno)
          126: Praca := Geral.IMV(ValNum);
          // Brancos
          999: Nada();//
          //
          else begin
            Erro := 1901;
            {
            Geral.MensagemBox(
            'C�digo interno de tipo de campo (CodSis) n�o implementado!' + sLineBreak +
            'Local: "1A"' + sLineBreak + 'CodSis: ' + Geral.FF0(CodSis),
            'ERRO', MB_OK+MB_ICONERROR);
            }
          end;
        end;
        DmtedC.QrTCCIts.Next;
      end;
      if CodReg = Registro then
      begin
        UltLin1 := Linha;
        //
        PosIni := -1;
        CodSis := -1;
        if Erro = 1004 then
        begin
          if SeuNumero = 0 then
            Erro := 1903;
        end;
        if not TedChequeTipoDeMovimento(BancoTED, ecnabRetorno, OcorrBco,
        TamReg, MyOcorr, nilDescricao, nilAviso, nilSQLTyp) then
          Erro := 1900;
        if  OcorrBco = 0 then
          Erro := 1904;
        //
        if Erro = 1004 then
        begin
          if not DesmembraUsoEmpresa(
          UsoEmpresa, LctCtrl, LctSub, LctFatNum, LctFatParc) then
            Erro := 1902
          else begin
            DmTedC.ReopenLocLct1(LctCtrl, LctSub, LctFatID, LctFatNum,
              LctFatParc, TabLctA);
            if DmTedC.QrLocLct1Controle.Value = 0 then
              Erro := 1905;
          end;
          if Erro = 1004 then
          begin
            Controle := UMyMod.BPGS1I32(
              'tedcremits', 'Controle', '', '', tsPos, stIns, 0);
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'tedcretits', False, [
            'LctCtrl', 'LctSub', 'LctFatID',
            'LctFatNum', 'LctFatParc', 'OcorrBco',
            'MyOcorr', 'Step', 'Cedente',
            'Filial', 'UsoEmpresa', 'DtaCaptura',
            'CMC7', 'RemValor', 'RemDataBoa',
            'BorderoBco', 'SeuNumero', 'Alinea',
            'CodRejeito', 'NossoNum', 'RetValor',
            'RetDataBoa', 'TrfCedente', 'TrfDtaOper',
            'TrfContrat', 'TrfParcela', 'TrfDtaCntr',
            'Banco', 'Agencia', 'ContaCorre',
            'Cheque', 'Praca', 'RetInscTip',
            'RetInscNum', 'RetEmitNom',
            'Codigo', 'Linha'], [
            'Controle'], [
            LctCtrl, LctSub, LctFatID,
            LctFatNum, LctFatParc, OcorrBco,
            MyOcorr, Step, Cedente,
            Filial, UsoEmpresa, DtaCaptura,
            CMC7, RemValor, RemDataBoa,
            BorderoBco, SeuNumero, Alinea,
            CodRejeito, NossoNum, RetValor,
            RetDataBoa, TrfCedente, TrfDtaOper,
            TrfContrat, TrfParcela, TrfDtaCntr,
            Banco, Agencia, ContaCorre,
            Cheque, Praca, RetInscTip,
            RetInscNum, RetEmitNom,
            Codigo, Linha], [
            Controle], True) then
          end;
        end;
        //  Caso encontre alguma linha com erro!
        if Erro <> 1004 then
          Exit;
      end else
        Erro := 001;
    end;
    //  Mudar o Step para 1: Itens carregados!
    Step := CO_TEDC_STEP_ARQ_ITENS_CARREGADOS; // 1
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcretcab', False, [
    'Step', 'Itens'], ['Codigo'], [1, Itens], [Codigo], True) then
    begin
      Erro := 0;
      //
      MyObjects.Informa2(LaAvisoB1, LaAvisoB2, True,
        'Leitura do Registro Tipo "' + Geral.FF0(Registro) + '" conclu�do');
      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
    end;
  finally
    DmTedC.TedCCfgSys_MostraTexto(Erro, '',
    'PosIni = ' + Geral.FF0(PosIni) + ' - CodSis = ' + Geral.FF0(CodSis),
    LaAvisoR1, LaAvisoR2, NoErro);
  end;
end;

procedure TTedC_Aux.AnalisaRetLinhaReg9(const TedCRetCab, Layout, Linha:
  Integer; Arquivo: String; const Linhas: TStrings;
  const LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2, LaAvisoR1, LaAvisoR2: TLabel;
  var NoErro: String; var Erro, Step: Integer);
const
  Registro = 9;
  Subreg = 0;
  Segmento = '';
  SubSeg = 0;
var
  CodSis, PosIni, Codigo, Casas: Integer;
  TxtLin, ValTxt, ValNum: String;
  CodReg, UltLin, CartQtd, EntrQtd, DepoQtd, BaxaQtd, DevlQtd, ReapQtd: Integer;
  ReapVal, CartVal, EntrVal, DepoVal, BaxaVal, DevlVal: Double;
begin
  CodSis := 0;
  PosIni := 0;
  CodReg := 0;
  try
    Erro := 1005;
    MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True,
      'Carregando arquivo: "' + Arquivo + '"');
    MyObjects.Informa2(LaAvisoB1, LaAvisoB2, True, 'Incluindo registro Tipo "'
    + Geral.FF0(Registro) + '" da Linha n� ' + Geral.FF0(Linha));
    MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
    //
    TxtLin := Linhas[Linha - 1];
    DmTedC.ReopenTCCIts(Layout, Registro, Subreg, Segmento, SubSeg);
    //
    //CodReg,
    UltLin  := 0;
    CartQtd := 0;
    EntrQtd := 0;
    DepoQtd := 0;
    BaxaQtd := 0;
    DevlQtd := 0;
    ReapQtd := 0;
    ReapVal := 0;
    CartVal := 0;
    EntrVal := 0;
    DepoVal := 0;
    BaxaVal := 0;
    DevlVal := 0;
    //
    DmtedC.QrTCCIts.First;
    while not DmtedC.QrTCCIts.Eof do
    begin
      CodSis := DmtedC.QrTCCItsCodSis.Value;
      PosIni := DmtedC.QrTCCItsPosIni.Value;
      ValTxt := Trim(Copy(TxtLin, PosIni, DmtedC.QrTCCItsPosTam.Value));
      if ValTxt = '' then
      begin
        ValNum := '0';
      end else begin
        ValNum := ValTxt;
      end;
      Casas := DmtedC.QrTCCItsTamFlu.Value;
      case CodSis of
        // C�digo do registro (Header / Detalhe / Trailler)
        001: CodReg := Geral.IMV(ValTxt);
        // N�mero sequencial do registro
        099: UltLin := Geral.IMV(ValTxt);
        // POSI��O DA CARTEIRA
        901: CartQtd := Geral.IMV(ValNum);
        // VALOR TOTAL DOS CHEQUES NA CARTEIRA
        902: Geral.MontaValDblPorFmt(ValNum, Casas, CartVal);
        // QUANTIDADE DE CHEQUES - ENTRADA
        903: EntrQtd := Geral.IMV(ValNum);
        // VALOR TOTAL DAS ENTRADAS
        904: Geral.MontaValDblPorFmt(ValNum, Casas, EntrVal);
        // DEP�SITOS QDADE DE CHEQUES
        905: DepoQtd := Geral.IMV(ValNum);
        // VALOR TOTAL DO DEPOSITO
        906: Geral.MontaValDblPorFmt(ValNum, Casas, DepoVal);
        // BAIXAS QDADE DE CHEQUES
        907: BaxaQtd := Geral.IMV(ValNum);
        // VALOR TOTAL DAS BAIXAS
        908: Geral.MontaValDblPorFmt(ValNum, Casas, BaxaVal);
        // DEVOLU��O QDADE DE CHEQUES
        909: DevlQtd := Geral.IMV(ValNum);
        // VALOR TOTAL DAS DEVOLU��ES
        910: Geral.MontaValDblPorFmt(ValNum, Casas, DevlVal);
        // REAPRESENTA��ES QDADE DE CHEQUES
        911: ReapQtd := Geral.IMV(ValNum);
        // VALOR TOTAL DAS REAPRESENTA��ES
        912: Geral.MontaValDblPorFmt(ValNum, Casas, ReapVal);
      end;
      DmtedC.QrTCCIts.Next;
    end;
    if CodReg <> Registro then
      Erro := 001
    else begin
      PosIni := -1;
      CodSis := -1;
      Codigo := TedCRetCab;
      //  Mudar o Step para 9: Arquivo todo lido!
      Step := CO_TEDC_STEP_ARQ_ARQUIVO_LIDO; // = 9
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcretcab', False, [
      (*'Empresa', 'TedC_Cfg',*) 'Step',
      (*'Produto', 'Cedente', 'Filial',
      'CNPJCPF', 'Banco', 'DataGer',
      'NumSeqGer', 'Depositari', 'Terceira',*)
      'CartQtd', 'CartVal', 'EntrQtd',
      'EntrVal', 'DepoQtd', 'DepoVal',
      'BaxaQtd', 'BaxaVal', 'DevlQtd',
      'DevlVal', 'ReapQtd', 'ReapVal',
      'UltLin'(*, 'Nome', 'Itens'*)], [
      'Codigo'], [
      (*Empresa, TedC_Cfg,*) Step,
      (*Produto, Cedente, Filial,
      CNPJCPF, Banco, DataGer,
      NumSeqGer, Depositari, Terceira,*)
      CartQtd, CartVal, EntrQtd,
      EntrVal, DepoQtd, DepoVal,
      BaxaQtd, BaxaVal, DevlQtd,
      DevlVal, ReapQtd, ReapVal,
      UltLin(*, Nome, Itens*)], [
      Codigo], True) then
      begin
        Erro := 0;
        //
        MyObjects.Informa2(LaAvisoB1, LaAvisoB2, True,
          'Leitura do Registro Tipo "' + Geral.FF0(Registro) + '" conclu�do');
        MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
      end;
    end;
  finally
    DmTedC.TedCCfgSys_MostraTexto(Erro, '',
    'PosIni = ' + Geral.FF0(PosIni) + ' - CodSis = ' + Geral.FF0(CodSis),
    LaAvisoR1, LaAvisoR2, NoErro);
  end;
end;

function TTedC_Aux.ChequeEstaNoBanco(TedRem_Its: Integer; Avisa: Boolean): Boolean;
begin
  Result := ChequeSitNoBanco(TedRem_Its) = enbSim;
  //
  if Result and Avisa then
    Geral.MensagemBox('O cheque est� em cust�dia banc�ria!', 'Aviso',
    MB_OK+MB_ICONWARNING);
end;

function TTedC_Aux.ChequeNaoEstaNoBanco(TedRem_Its: Integer; Avisa: Boolean): Boolean;
begin
  Result := ChequeSitNoBanco(TedRem_Its) = enbNao;
  if not Result and Avisa then
    Geral.MensagemBox('O cheque n�o est� em cust�dia banc�ria!', 'Aviso',
    MB_OK+MB_ICONWARNING);
end;

function TTedC_Aux.ChequeSitNoBanco(TedRem_Its: Integer): TSitChequeNoBanco;
begin
  case TedRem_Its of
    (*0*)CO_SIT_TEDC_NADA: Result := enbNao;
    (*1*)CO_SIT_TEDC_INSTRUCAO_ENTRADA_NO_BANCO: Result := enbLimbo;
    (*2*)CO_SIT_TEDC_LIMBO_ENTRADA_NO_BANCO: Result := enbLimbo;
    (*3*)CO_SIT_TEDC_ENTROU_NO_BANCO: Result := enbSim;
    (*4*)CO_SIT_TEDC_INSTRUCAO_ALTERACAO_NO_BANCO: Result := enbLimbo;
    (*5*)CO_SIT_TEDC_LIMBO_ALTERACAO_NO_BANCO: Result := enbLimbo;
    (*6*)CO_SIT_TEDC_ALTEROU_NO_BANCO: Result := enbSim;
    (*9*)CO_SIT_TEDC_BAIXOU_NO_BANCO: Result := enbNao;
    else(*-1CO_SIT_TEDC_DESCONHECIDO:*) Result := enbNaoSei;
  end;
end;

function TTedC_Aux.CriaLista(const Layout, Registro, SubReg: Integer;
  const Segmento: String; const SubSeg: Integer; var Reg: TFldTabRec;
  var Lista: TList; var PosAnt: Integer; var Txt1: String): Boolean;
begin
  if Lista <> nil then
    Lista.Free;
  Lista := TList.Create;
  //
  Result := True;
  Lista.Clear;
  PosAnt := 0;
  Txt1 := 'Registro = ' + Geral.FF0(Registro) + sLineBreak +
           'SubReg = ' + Geral.FF0(SubReg) + sLineBreak +
           'Segmento = "' + Segmento + '"' + sLineBreak +
           'SubSeg = ' + Geral.FF0(SubSeg) + sLineBreak;
end;

function TTedC_Aux.CriaTabAux(const Layout, Registro, Subreg: Integer;
  const Segmento: String; const SubSeg: Integer;
  const Tabela: String): Boolean;
var
  //Temp, Num, Txts, Ordem,
  Flds: String;
  //I: Integer;
begin
{
  FCliente := EdCliente.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtaFat0, Dmod.MyDB, [
  'SELECT FatID, ChkFator ',
  'FROM ctafat0 ',
  'ORDER BY FatID ',
  '']);
  FFatIDs := QrCtaFat0.RecordCount;
  Flds := '';
  SetLength(FArrFatID, FFatIDs);
  SetLength(FArrFator, FFatIDs);
  QrCtaFat0.First;
  while not QrCtaFat0.Eof do
  begin
    FArrFator[QrCtaFat0.RecNo - 1] := QrCtaFat0ChkFator.Value;
    FArrFatID[QrCtaFat0.RecNo - 1] := QrCtaFat0FatID.Value;
    Flds := Flds + 'Fld' + Geral.FFN(QrCtaFat0FatID.Value, 4) +
      '              double(15,2) NOT NULL DEFAULT "0.00"       , ' + sLineBreak;
    //
    QrCtaFat0.Next;
  end;
  //
  FOLC_1 := '_OLC_1_';
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'DROP TABLE IF EXISTS ' + FOLC_1 + ';                          ',
  'CREATE TABLE ' + FOLC_1 + ' (                                 ',
  '  FatNum               double(15,0) NOT NULL DEFAULT "0"          , ',
  '  Lote                 int(11)      NOT NULL DEFAULT "0"          , ',
  '  Data                 date         NOT NULL DEFAULT "0000-00-00" , ',
  '  CodCli               int(11)      NOT NULL DEFAULT "0"          , ',
  '  NomCli               varchar(100) NOT NULL                      , ',
  '  Nivel1               int(11)      NOT NULL DEFAULT "0"          , ',
  '  Nivel2               int(11)      NOT NULL DEFAULT "0"          , ',
  Flds,
  '  Ativo                tinyint(1)   NOT NULL DEFAULT "0"            ',
  ') TYPE=MyISAM                                                       ',
  ';']);
}
  Result := False;
  if not DmTedC.ReopenTCCIts(Layout, Registro, Subreg, Segmento, SubSeg) then
    Exit;
  DmtedC.QrTCCIts.First;
  while not DmtedC.QrTCCIts.Eof do
  begin
    Flds := Flds + '_' + Geral.FFN(DmtedC.QrTCCItsPosIni.Value, 3) + '_' +
    Geral.FFN(DmtedC.QrTCCItsPosTam.Value, 3) + '_' +
    DmtedC.QrTCCItsRestricao.Value[1] + '              varchar(' +
// Fazer tamanho 2 caracteres maior para detectar texto com tamanho superior ao m�ximo
    Geral.FFN(DmtedC.QrTCCItsPosTam.Value + 2, 3) +
    ')                               , ' + sLineBreak;
    //
    DmtedC.QrTCCIts.Next;
  end;
  //
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'DROP TABLE IF EXISTS ' + Tabela + ';                          ',
  'CREATE TABLE ' + Tabela + ' (                                 ',
  Flds,
  '  Linha                int(11)      NOT NULL DEFAULT "0"           ,',
  '  Ativo                tinyint(1)   NOT NULL DEFAULT "0"            ',
  ') TYPE=MyISAM                                                       ',
  ';']);
end;

function TTedC_Aux.DescricaoDeMyOcorrTedC(MyOcorr: Integer): String;
begin
  case MyOcorr of
    CO_MYOCORR_TEDC_NULO: Result := 'Sem ocorr�ncia!';
    CO_MYOCORR_TEDC_ENVIO_AO_BANCO: Result := 'Envio para o banco';
    CO_MYOCORR_TEDC_ALTERAR_BOM_PARA: Result := 'Instru��o para alterar "BOM PARA"';
    CO_MYOCORR_TEDC_INSTRUCAO_BAIXAR: Result := 'Instru��o para baixar o cheque';
    else Result := 'Ocorr�ncia n�o catalogada! AVISE A DERMATEK';
  end;
end;

function TTedC_Aux.DescricaoDeTedRem_Sit(Situacao: Integer): String;
{
// 0=N�o adicionado em nenhum lote
// 1=Adicionado em lote de entrada, aguardando entrada no banco
// 2=Adicionado em lote de instru��o, aguardando altera��o no banco
// 3=Limbo. Banco recebeu a instru��o de entrada mas n�o se posicionou sobre ela.
// 4=Limbo. Banco recebeu a instru��o de altera��o mas n�o se posicionou sobre ela.
// 5=Entrou no banco
// 6=Alterou no banco
// 9=Baixou por qualquer motivo
}
begin
  case Situacao of
  {0}CO_SIT_TEDC_NADA                         : Result := 'N�o ativo em nenhum lote';
  {1}CO_SIT_TEDC_INSTRUCAO_ENTRADA_NO_BANCO   : Result := 'Adicionado em lote (entrada), aguardando entrada no banco';
  {2}CO_SIT_TEDC_LIMBO_ENTRADA_NO_BANCO       : Result := 'Banco recebeu a instru��o de entrada mas n�o se posicionou sobre ela';
  {3}CO_SIT_TEDC_ENTROU_NO_BANCO              : Result := 'Entrou no banco. Est� em cust�dia';
  {4}CO_SIT_TEDC_INSTRUCAO_ALTERACAO_NO_BANCO : Result := 'Adicionado em lote (instru��o), aguardando altera��o no banco';
  {5}CO_SIT_TEDC_LIMBO_ALTERACAO_NO_BANCO     : Result := 'Banco recebeu a instru��o de altera��o mas n�o se posicionou sobre ela';
  {6}CO_SIT_TEDC_ALTEROU_NO_BANCO             : Result := 'Alterou no banco. Est� em cust�dia';
  {9}CO_SIT_TEDC_BAIXOU_NO_BANCO              : Result := 'Baixou no banco por qualquer motivo';
     else Result := 'Situa��o n�o catalogada! AVISE A DERMATEK';
  end;
end;

function TTedC_Aux.DesmembraUsoEmpresa(const UsoEmpresa: String; var LctCtrl,
  LctSub: Integer; var LctFatNum: Double; var LctFatParc: Integer): Boolean;
begin
{    IntToHex(QrTedCRemItsLctCtrl.Value, 8) +
    IntToHex(QrTedCRemItsLctSub.Value, 1) +
    IntToHex(QrTedCRemItsLctFatNum.Value, 8) +
    IntToHex(QrTedCRemItsLctFatParc.Value, 8);
}
  //Result := False;
  try
    LctCtrl    := MLAGeral.HexToInt(Copy(UsoEmpresa, 01, 8));
    LctSub     := MLAGeral.HexToInt(Copy(UsoEmpresa, 09, 1));
    LctFatNum  := MLAGeral.HexToInt(Copy(UsoEmpresa, 10, 8));
    LctFatParc := MLAGeral.HexToInt(Copy(UsoEmpresa, 18, 8));
    //
    Result := True;
  finally
    //
  end;
end;

function TTedC_Aux.GeoCampoCorreto(const Campo, Descri01, Descri02, Descri03,
  Descri04, Descri05: String; const PosIni, PosFim, PosTam, TamInt,
  TamFlu: Integer; var FPosIni, FPosFim, FPosTam, FTamInt, FTamFlu: Integer;
  const Txt1: String; var Txt2: String; var FPosAnt: Integer): Boolean;
var
  Msg: String;
begin
  Txt2 :=  'PosIni = ' + Geral.FF0(PosIni) + ' + ' +
           'PosTam = ' + Geral.FF0(PosTam) + ' > ' +
           'PosFim = ' + Geral.FF0(PosFim) + sLineBreak +
           'Campo: ' + Campo + sLineBreak +
           '=======================================================' + sLineBreak +
           'Descri��o: ' + Descri01 + sLineBreak +
           '                ' + Descri02 + sLineBreak +
           '                ' + Descri03 + sLineBreak +
           '                ' + Descri04 + sLineBreak +
           '                ' + Descri05 + sLineBreak +
           '=======================================================' + sLineBreak;
  Result := False;
  Msg := '';
  FPosIni := PosIni;
  FPosFim := PosFim;
  FPosTam := PosTam;
  FTamInt := TamInt;
  FTamFlu := TamFlu;
  //
  if PosIni - FPosAnt <> 1 then
    Msg := 'A posi��o inicial atual deveria ser ' + Geral.FF0(FPosAnt + 1) +
    ' mas foi informada ' + Geral.FF0(PosIni) + ' !!!';
  if PosFim < PosIni then
    Msg := 'A posi��o Final (' + Geral.FF0(PosFIm) +
    ' � menor que a inicial ' + Geral.FF0(PosIni) + ' !!!';
  if (PosFim - PosIni + 1) <> PosTam then
    Msg := 'O Tamanho do campo deveria ser (' + Geral.FF0(PosFim) + ' - ' +
    Geral.FF0(PosIni) + ' + 1) mas foi informado ' + Geral.FF0(PosTam) + ' !!!';
  if (TamInt + TamFlu) <> PosTam then
    Msg := 'O Tamanho do campo deveria ser (' + Geral.FF0(TamInt) + ' + ' +
    Geral.FF0(TamFlu) + ' + 1) mas foi informado ' + Geral.FF0(PosTam) + ' !!!';
  if Msg <> '' then
    Geral.MensagemBox(Txt1 + Txt2 + Msg, 'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    FPosAnt := FPosFim;
    Result := True;
  end;
end;

function TTedC_Aux.GeraLinha(const Layout, Registro, Subreg: Integer;
  const Segmento: String; const SubSeg: Integer; var Linha: String;
  var Continua: Boolean; var PosIni, PosFim, PosTam, TamInt,
  TamFlu: Integer; var Txt1, Txt2: String; var PosAnt: Integer;
  const Memo: TMemo; var Reg: TFldTabRec; const Lista: TList;
  const Tabela: String; var Seq: Integer):
  Boolean;
var
  //Flds: array of String;
  Texto: String;
  SQL: WideString;
begin
  Result := False;
  Linha := '';
{ N�o precisa! � executado no CriaTabAux()!
  if not DmTedC.ReopenTCCIts(Layout, Registro, Subreg, Segmento, SubSeg) then
    Exit;
}
  //SetLength(Flds, DmtedC.QrTCCIts.RecordCount);
  SQL := '';
  DmtedC.QrTCCIts.First;
  while not DmtedC.QrTCCIts.Eof do
  begin
    if NaoContinua(Continua) then Exit;
    //
    if not GeoCampoCorreto(
    DmtedC.QrTCCItsCampo.Value,
    DmtedC.QrTCCItsDescri01.Value,
    DmtedC.QrTCCItsDescri02.Value,
    DmtedC.QrTCCItsDescri03.Value,
    DmtedC.QrTCCItsDescri04.Value,
    DmtedC.QrTCCItsDescri05.Value,
    DmtedC.QrTCCItsPosIni.Value,
    DmtedC.QrTCCItsPosFim.Value,
    DmtedC.QrTCCItsPosTam.Value,
    DmtedC.QrTCCItsTamInt.Value,
    DmtedC.QrTCCItsTamFlu.Value,
    PosIni, PosFim, PosTam, TamInt, TamFlu, Txt1, Txt2, PosAnt) then
      QuebraGeracao(Memo, Linha, Continua)
    else
      AddCampoNaLinhaAtual(
      DmtedC.QrTCCItsCodigo.Value,
      DmtedC.QrTCCItsRegistro.Value,
      DmtedC.QrTCCItsSubReg.Value,
      DmtedC.QrTCCItsSegmento.Value,
      DmtedC.QrTCCItsSubSeg.Value,
      Linha,
      DmtedC.QrTCCItsDefaultRem.Value,
      DmtedC.QrTCCItsRestricao.Value,
      DmtedC.QrTCCItsFmtDta.Value,
      DmtedC.QrTCCItsPreenchmto.Value,
      DmtedC.QrTCCItsCodSis.Value,
      DmtedC.QrTCCItsObrigatori.Value,
      DmtedC.QrTCCItsCliPreench.Value,
      DmtedC.QrTCCItsFulSize.Value,
      DmtedC.QrTCCItsInvertAlin.Value,
      DmtedC.QrTCCItsCortaStr.Value,
      PosIni, PosFim, PosTam, TamInt, TamFlu,
      Txt1, Txt2, Memo, Continua, Reg,
      Lista, Texto);
      //
      //Flds[DmtedC.QrTCCIts.RecNo-1] := Texto;
      SQL := SQL + '''' + Texto + ''',';
    //
    DmtedC.QrTCCIts.Next;
  end;
  Seq := Seq + 1;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + Tabela + ' VALUES (' + SQL +
  Geral.FF0(Seq) + ',1);');
  DModG.QrUpdPID1.ExecSQL;
  Result := AddLinhaAoMemo(Memo, Linha, DmTedC.QrTCCCabTamReg.Value, Continua);
end;

procedure TTedC_Aux.MostraTedCCfgCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTedCCfgCab, FmTedCCfgCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmTedCCfgCab.LocCod(Codigo, Codigo);
    FmTedCCfgCab.ShowModal;
    FmTedCCfgCab.Destroy;
  end;
end;

procedure TTedC_Aux.MostraTedCOcoSys(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTedCOcoSys, FmTedCOcoSys, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmTedCOcoSys.LocCod(COdigo, Codigo);
    FmTedCOcoSys.ShowModal;
    FmTedCOcoSys.Destroy;
  end;
end;

procedure TTedC_Aux.MostraTedCRemCab(Codigo, FatParcela: Integer);
begin
  DModG.EmpresaAtual_SetaCodigos(Dmod.QrControleDono.Value, tecEntidade, True);
  //
  if DBCheck.CriaFm(TFmTedCRemCab, FmTedCRemCab, afmoNegarComAviso) then
  begin
    if (Codigo > 0) and (FatParcela > 0) then
    begin
      FmTedCRemCab.LocCod(Codigo, Codigo);
      FmTedCRemCab.ReopenTedCRemIts(FatParcela);
    end;
    FmTedCRemCab.ShowModal;
    FmTedCRemCab.Destroy;
  end;
end;

procedure TTedC_Aux.MostraTedCRetArq;
begin
  if DBCheck.CriaFm(TFmTedCRetArq, FmTedCRetArq, afmoNegarComAviso) then
  begin
    FmTedCRetArq.ShowModal;
    FmTedCRetArq.Destroy;
  end;
end;

procedure TTedC_Aux.MostraTedCRetCab;
begin
  if DBCheck.CriaFm(TFmTedCRetCab, FmTedCRetCab, afmoNegarComAviso) then
  begin
    FmTedCRetCab.ShowModal;
    FmTedCRetCab.Destroy;
  end;
end;

function TTedC_Aux.MotaNomeTabela(Registro, SubReg: Integer; Segmento: String;
SubSeg: Integer): String;
begin
  Result := '_TedC_' + Geral.FF0(Registro) +
  '_' + Geral.FF0(SubReg) +
  '_' + Geral.SoNumeroELetra_TT(Segmento) +
  '_' + Geral.FF0(SubSeg) + '_';
end;

function TTedC_Aux.Nada(): Boolean;
begin
  Result := True;
end;

function TTedC_Aux.NaoContinua(Continua: Boolean): Boolean;
begin
  Result := not Continua;
end;

function TTedC_Aux.ObtemValorDeCodSis(var Texto: String; const FmtDta: String;
  const CodSis, TamInt, TamFlu: Integer; var Reg: TFldTabRec; const Lista: TList;
  const Memo: TMemo; const Linha: String; var Continua: Boolean): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Lista.Count - 1 do
  begin
    Reg := Lista[I];
    if Reg.Codigo = CodSis then
    begin
{
      n�o usar Geral.VariavelToString() !
      deve ser visto caso a caso
      - casas decimais de double
      - formata��o de data!
}
      Result := VariavelToTxtArqRem(Texto, Reg.Valor, FmtDta, TamInt, TamFlu);
      Exit;
    end;
  end;
  if not Result then
    Geral.MensagemBox('N�o foi poss�vel definir o valor do "CodSis" n� ' +
    Geral.FF0(CodSis) + ' :' + sLineBreak + DmTedC.TedCCfgSys_ObtemNome(CodSis),
    'Erro', MB_OK+MB_ICONERROR);
end;

function TTedC_Aux.PreparaExportacao(const Memo: TMemo; var Continua: Boolean):
  Boolean;
begin
  Memo.Lines.Clear;
  Continua := True;
  //
  Result := True;
end;

procedure TTedC_Aux.QuebraGeracao(const Memo: TMemo; const Linha: String;
  var Continua: Boolean);
begin
  Memo.Lines.Add(Linha);
  //
  Continua := False;
end;

function TTedC_Aux.SQL_422_2007_04_300(Codigo: Integer): Boolean;
begin
  UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
  'DELETE FROM tedccfgcab WHERE Codigo=-999999999;',
  'DELETE FROM tedccfgits WHERE Codigo=-999999999;',
  'DELETE FROM tedccfgset WHERE Codigo=-999999999;',
  '']);
  //
  UMyMod.InsereRegistrosPreDefinidos(Dmod.QrUpd, 'tedccfgcab', [
'Codigo|Banco|AnoMes|Versao|TamReg|Nome|SeqLay|RemRet|Empresa|CartDep|Lk|DataCad|DataAlt|UserCad|UserAlt|AlterWeb|Ativo|',
'-999999999|422|200704|ABRIL/2007|300|TED Cheques Safra 900|1|3|0|0|0|0|0|0|0|1|1|',
'']);
{ TODO :    AA :: Exportar novamente os dados do "tedccfgits" tirando os preenchimentos de codigo cedente!}
{
SELECT -999999999 Codigo, 
Registro, SubReg, Segmento, SubSeg, 
PosIni, PosFim, PosTam, Campo, 
Restricao, TamInt, TamFlu, 
FmtDta, Preenchmto, CodSis, 
IF(CliPreench=1, "", DefaultRem)
DefaultRem, DefaultRet, Obrigatori, 
CliPreench, Descri01, Descri02, 
Descri03, Descri04, Descri05, 
FulSize, InvertAlin, CortaStr
FROM tedccfgits
WHERE Codigo=(
  SELECT  MAX(Codigo) FROM tedccfgits)
}
  UMyMod.InsereRegistrosPreDefinidos(Dmod.QrUpd, 'tedccfgits', [
'Codigo|Registro|SubReg|Segmento|SubSeg|PosIni|PosFim|PosTam|Campo|Restricao|TamInt|TamFlu|FmtDta|Preenchmto|CodSis|DefaultRem|DefaultRet|Obrigatori|CliPreench|Descri01|Descri02|Descri03|Descri04|Descri05|FulSize|InvertAlin|CortaStr',
'-999999999|0|0||0|1|1|1|COD. REGISTRO|9|1|0||2|1|0|0|0|0|IDENTIFICA��O DO REGISTRO DE ARQUIVO|||||0|0|0',
'-999999999|0|0||0|2|2|1|IDENT. ARQUIVO|9|1|0||2|2|1|2|1|0|C�DIGO DE IDENTIFICA��O DO MOVIMENTO|||||0|0|0',
'-999999999|0|0||0|3|9|7|IDENT. ARQUIVO|X|7|0||2|3|REMESSA|RETORNO|1|0|IDENTIFICA��O DO MOVIMENTO P/ EXTENSO|||||0|0|0',
'-999999999|0|0||0|10|11|2|BRANCOS|X|2|0||3|999|||0|0|BRANCOS|||||0|0|0',
'-999999999|0|0||0|12|18|7|LITERAL DO SERVI�O|X|7|0||2|5|CHEQUES|CHEQUES|1|0|IDENTIFICA��O DO SERVI�O|||||0|0|0',
'-999999999|0|0||0|19|19|1|BRANCO|X|1|0||3|999|||0|0|BRANCO|||||0|0|0',
'-999999999|0|0||0|20|22|3|COD. DO PRODUTO|9|3|0||2|4|900|900|1|0|C�DIGO DO PRODUTO|||||0|0|0',
'-999999999|0|0||0|23|36|14|COD. EMPRESA|9|14|0||2|6|||1|1|IDENTIFICA��O DA EMPRESA NO BANCO|C�digo do Cedente:|5 d�gitos da ag�ncia|9 d�gitos da conta corrente||1|0|0',
'-999999999|0|0||0|37|39|3|COD. FILIAL|9|3|0||2|7|000|000|0|0|CODIGO FILIAL DA EMPRESA|||||0|0|0',
'-999999999|0|0||0|40|40|1|TIPO DE INSCRI��O|9|1|0||4|8|||1|0|TIPO DE INSCRI��O DA EMPRESA|||||0|0|0',
'-999999999|0|0||0|41|54|14|NUM. INSCRI��O|9|14|0||1|9|||1|0|N�MERO DE INSCRI��O DA EMPRESA|||||0|0|0',
'-999999999|0|0||0|55|94|40|NOME EMPRESA|X|40|0||1|10|||1|0|NOME DA EMPRESA CEDENTE|||||0|0|1',
'-999999999|0|0||0|95|97|3|COD. BANCO|9|3|0||2|11|422|422|1|0|C�DIGO DE IDENTIFICA��O DO BANCO NA COMPENSA��O|||||0|0|0',
'-999999999|0|0||0|98|102|5|NOME BANCO|X|5|0||2|12|SAFRA|SAFRA|1|0|NOME DO BANCO POR EXTENSO|||||0|0|0',
'-999999999|0|0||0|103|107|5|AG�NCIA DEPOSIT�RIA|9|5|0||2|13|00000|00000|0|0|C�DIGO DA AG�NCIA DEPOSITARIA (USO BANCO)|||||0|0|0',
'-999999999|0|0||0|108|109|2|C�DIGO DA TERCEIRIZADA|9|2|0||2|14|99|99|1|0|C�DIGO DA EMPRESA TERCEIRIZADA|||||0|0|0',
'-999999999|0|0||0|110|280|171|BRANCOS|X|171|0||2|999|||0|0|BRANCOS|||||0|0|0',
'-999999999|0|0||0|281|288|8|DT. GRAVA��O|D|8|0|DDMMYYYY|1|90|||1|0|DATA DAGERA��O DO ARQUIVO REMESSA|||||0|0|0',
'-999999999|0|0||0|289|294|6|NUM. ARQUIVO|9|6|0||1|91|||1|0|N�MERO SEQ�ENCIAL DE GERA��O DO ARQUIVO|||||0|0|0',
'-999999999|0|0||0|295|300|6|NUM. REGISTRO|9|6|0||2|99|000001|000001|1|0|N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO|||||0|0|0',
'-999999999|1|0||0|1|1|1|COD. REGISTRO|9|1|0||2|1|1|1|1|0|IDENTIFICA��O DO REGISTRO TRANSA��O|||||0|0|0',
'-999999999|1|0||0|2|3|2|C�D. OCORR�NCIA|9|2|0||1|101|||1|0|C�DIGO DE OCORR�NCIA (REMESSA/RETORNO)|||||0|0|0',
'-999999999|1|0||0|4|17|14|COD. EMPRESA|9|14|0||2|6|||1|1|IDENTIFICA��O DA EMPRESA NO BANCO|C�digo do Cedente:|5 d�gitos da ag�ncia|9 d�gitos da conta corrente||1|0|0',
'-999999999|1|0||0|18|20|3|COD. FILIAL|9|3|0||2|7|||0|0|CODIGO DA FILIAL|||||0|0|0',
'-999999999|1|0||0|21|45|25|USO EMPRESA|X|25|0||1|102|||1|0|USO EXCLUSIVO DA EMPRESA|||||0|0|0',
'-999999999|1|0||0|46|48|3|BRANCOS|X|3|0||3|999|||0|0|BRANCOS|||||0|0|0',
'-999999999|1|0||0|49|56|8|DATA DO EVENTO|9|8|0|DDMMYYYY|1|104|||1|0|DATA DA CAPTURA/EVENTO|||||0|0|0',
'-999999999|1|0||0|57|57|1|BRANCO|X|1|0||3|999|||0|0|BRANCO|||||0|0|0',
'-999999999|1|0||0|58|87|30|IDENTIFICA��O DO CHEQUE|9|30|0||1|105|||1|0|IDENTIFICA��O DO CHEQUE NO BANCO - CMC7|(capturar/digitar as informa��es pela banda magn�tica - CMC7)||||0|0|0',
'-999999999|1|0||0|88|89|2|TIPO INSCRI��O|9|2|0||4|106|||0|0|TIPO DE INSCRI��O DO EMITENTE DO CHEQUE|(OPCIONAL)||||0|0|0',
'-999999999|1|0||0|90|103|14|NUM. INSCRI��O DO EMITENTE|9|14|0||1|107|||0|0|N�MERO DE INSCRI��O DO EMITENTE|(OPCIONAL)||||0|0|0',
'-999999999|1|0||0|104|133|30|NOME DO EMITENTE|X|30|0||1|108|||0|0|NOME DO EMITENTE|(OPCIONAL)||||0|0|1',
'-999999999|1|0||0|134|143|10|VALOR DO CHEQUE (REMESSA)|9|8|2||1|109|||1|0|VALOR DO CHEQUE| (informado no arquivo remessa pelo cliente)||||0|0|0',
'-999999999|1|0||0|144|151|8|DATA BOA (REMESSA)|D|8|0|DDMMYYYY|1|110|||1|0|DATA BOA PARA COMPENSA��O| (informado no arquivo remessa pelo cliente)||||0|0|0',
'-999999999|1|0||0|152|156|5|N�MERO BORDERO (RETORNO)|9|5|0||5|111|||0|0|NUMERO BORDERO|(GERADO PELO BANCO NO ARQUIVO RETORNO)||||0|0|0',
'-999999999|1|0||0|157|166|10|SEU N�MERO|X|10|0||1|103|||1|0|N�MERO ATRIBU�DO PELO CLIENTE|(CAMPO OPCIONAL)||||0|0|0',
'-999999999|1|0||0|167|181|15|BRANCOS|X|15|0||3|999|||0|0|BRANCOS|BRANCOS||||0|0|0',
'-999999999|1|0||0|182|183|2|MOTIVO DE VOLU��O|9|2|0||5|112|||0|0|MOTIVO DE DEVOLU��O (ALINEA)|(C�DIGO GERADO PELO BANCO NO ARQUIVO RETORNO)||||0|0|0',
'-999999999|1|0||0|184|185|2|C�D. REJEI��O|9|2|0||5|113|||0|0|CODIGO MOTIVO DE REJEI��O|(C�DIGO GERADO PELO BANCO NO ARQUIVO RETORNO)||||0|0|0',
'-999999999|1|0||0|186|194|9|NOSSO N�MERO|9|9|0||5|114|||0|0|N�MERO SEQUENCIAL DO MOVIMENTO DE ENTRADA|(GERADO PELO BANCO NO ARQUIVO RETORNO)||||0|0|0',
'-999999999|1|0||0|195|204|10|VALOR DO CHEQUE (RETORNO)|9|8|2||5|115|||0|0|VALOR DO CHEQUE REGISTRADO NO BANCO| (CONFIRMADO ATRAV�S DO FISICO)||||0|0|0',
'-999999999|1|0||0|205|212|8|DATA BOA (RETORNO)|D|8|0|DDMMYYYY|5|116|||0|0|DATA BOA REGISTRADA NO BANCO PARA COMPENSA��O|||||0|0|0',
'-999999999|1|0||0|213|226|14|C�D. CEDENTE P/ TRANSFERENCIA|9|14|0||1|117|||0|0|C�DIGO CEDENTE PARA TRANSFERENCIA|||||0|0|0',
'-999999999|1|0||0|227|234|8|DATAO PERA��O|D|8|0|DDMMYYYY|1|118|||0|0|DATA DA OPERA��O/CESS�O|||||0|0|0',
'-999999999|1|0||0|235|249|15|N�MERO DO CONTRATO|X|15|0||1|119|||0|0|N�MERO DO CONTRATO|(OBRIGATORIO P/ OCORR�NCIA 03)||||0|0|0',
'-999999999|1|0||0|250|251|2|NUMERO PARCELA|9|2|0||1|120|||0|0|N�MERO DE PARCELA DO CONTRATO|(OBRIGATORIO P/ OCORR�NCIA 03)||||0|0|0',
'-999999999|1|0||0|252|259|8|DATA EMISS�O|D|8|0|DDMMYYYY|1|121|||0|0|DATA EMISS�O DO CONTRATO (DT. CONTABIL)|(OBRIGATORIO P/ OCORR�NCIA 03)||||0|0|0',
'-999999999|1|0||0|260|262|3|BANCO (RETORNO)|9|3|0||5|122|||0|0|BANCO|||||0|0|0',
'-999999999|1|0||0|263|266|4|AG�NCIA (RETORNO)|9|4|0||5|123|||0|0|AG�NCIA|||||0|0|0',
'-999999999|1|0||0|267|276|10|CONTA (RETORNO)|9|10|0||5|124|||0|0|CONTA|||||0|0|0',
'-999999999|1|0||0|277|282|6|CHEQUE (RETORNO)|9|6|0||5|125|||0|0|CHEQUE|||||0|0|0',
'-999999999|1|0||0|283|285|3|COMP (RETORNO)|9|3|0||5|126|||0|0|COMP|||||0|0|0',
'-999999999|1|0||0|286|294|9|BRANCOS|X|9|0||3|999|||0|0|BRANCOS|||||0|0|0',
'-999999999|1|0||0|295|300|6|NUM. SEQUENCIAL DO REGISTRO|9|6|0||1|99|||1|0|N�MERO SEQUENCIAL DO REGISTRO|||||0|0|0',
'-999999999|9|0||0|1|1|1|COD. REGISTRO|9|1|0||2|1|9|9|1|0|IDENTIFICA��O REGISTRO TRAILLER|||||0|0|0',
'-999999999|9|0||0|2|7|6|BRANCOS|X|6|0||3|999|||0|0|BRANCOS|||||0|0|0',
'-999999999|9|0||0|8|15|8|QUANTIDADE|9|8|0||5|901|||0|0|POSI��O DA CARTEIRA|||||0|0|0',
'-999999999|9|0||0|16|25|10|VALOR TOTAL|9|8|2||1|902|||0|0|VALOR TOTAL DOS CHEQUES NA CARTEIRA|||||0|0|0',
'-999999999|9|0||0|26|33|8|QUANTIDADE|9|8|0||1|903|||0|0|QUANTIDADE DE CHEQUES - ENTRADA|||||0|0|0',
'-999999999|9|0||0|34|43|10|VALOR TOTAL|9|8|2||1|904|||0|0|VALOR TOTAL DAS ENTRADAS|||||0|0|0',
'-999999999|9|0||0|44|51|8|QUANTIDADE|9|8|0||5|905|||0|0|DEP�SITOS QDADE DE CHEQUES|||||0|0|0',
'-999999999|9|0||0|52|61|10|VALOR TOTAL|9|8|2||5|906|||0|0|VALOR TOTAL DO DEPOSITO|||||0|0|0',
'-999999999|9|0||0|62|69|8|QUANTIDADE|9|8|0||5|907|||0|0|BAIXAS QDADE DE CHEQUES|||||0|0|0',
'-999999999|9|0||0|70|79|10|VALOR TOTAL|9|8|2||5|908|||0|0|VALOR TOTAL DAS BAIXAS|||||0|0|0',
'-999999999|9|0||0|80|87|8|QUANTIDADE|9|8|0||5|909|||0|0|DEVOLU��O QDADE DE CHEQUES|||||0|0|0',
'-999999999|9|0||0|88|97|10|VALOR TOTAL|9|8|2||5|910|||0|0|VALOR TOTAL DAS DEVOLU��ES|||||0|0|0',
'-999999999|9|0||0|98|105|8|QUANTIDADE|9|8|0||5|911|||0|0|REAPRESENTA��ES QDADE DE CHEQUES|||||0|0|0',
'-999999999|9|0||0|106|115|10|VALOR TOTAL|9|8|2||5|912|||0|0|VALOR TOTAL DAS REAPRESENTA��ES|||||0|0|0',
'-999999999|9|0||0|116|294|179|BRANCOS|X|179|0||3|999|||0|0|BRANCOS|||||0|0|0',
'-999999999|9|0||0|295|300|6|N�MERO SEQUENCIAL DO REGISTRO|9|6|0||1|99|||1|0|N�MERO SEQUENCIAL DO REGISTRO|||||0|0|0',
'']);

  UMyMod.InsereRegistrosPreDefinidos(Dmod.QrUpd, 'tedccfgset', [
'Codigo|Registro|SubReg|Segmento|SubSeg|PosIni|ValSis|ValBco',
{ N�o precisa! j� vai no DefaultRem / DefaultRet!
'-999999999|0|0||0|2|1|1',
'-999999999|0|0||0|2|2|2',
}
'-999999999|0|0||0|40|0|2',
'-999999999|0|0||0|40|1|1',
'-999999999|1|0||0|88|-1|0',
'-999999999|1|0||0|88|0|2',
'-999999999|1|0||0|88|1|1',
'']);

  UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
  'UPDATE tedccfgcab SET Codigo=' + Geral.FF0(Codigo) + ' WHERE Codigo=-999999999;',
  'UPDATE tedccfgits SET Codigo=' + Geral.FF0(Codigo) + ' WHERE Codigo=-999999999;',
  'UPDATE tedccfgset SET Codigo=' + Geral.FF0(Codigo) + ' WHERE Codigo=-999999999;',
  '']);
  Result := True;
end;

function TTedC_Aux.TedChequeSelecionaOcorrencia(const Banco: Integer;
  const Envio: TEnvioCNAB; const TamReg: Integer; var OcorrBco,
  MyOcorr: Integer; var SQLType: TSQLType): Boolean;
const
  DefaultItemIndex = -1;
var
  I, K, Colunas, MinhaOco: Integer;
  Descricao, Aviso, Valor: String;
  //
  MyOcors: array of Integer;
  Itens: array of String;
  Avisos: array of String;
  Acoes: array of TSQLType;
  Valores: array of String;
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  //
  MyOcorr := CO_MYOCORR_TEDC_NULO;
  OcorrBco := CO_MYOCORR_TEDC_NULO;
  SQLType := stNil;
  //
  K := 0;
  for I := 0 to 999 do
  begin
    TedChequeTipoDeMovimento(Banco, Envio, I, TamReg,
      MinhaOco, Descricao, Aviso, SQLType);
    if Trim(Descricao) <> ''  then
    begin
      K := K + 1;
      //
      SetLength(Itens, K);
      Itens[K-1] := Descricao;
      //
      SetLength(Avisos, K);
      Avisos[K-1] := Aviso;
      //
      SetLength(Acoes, K);
      Acoes[K-1] := SQLType;
      //
      SetLength(Valores, K);
      Valores[K-1] := Geral.FF0(I);
      //
      SetLength(MyOcors, K);
      MyOcors[K-1] := MinhaOco;
      //
    end;
  end;
  Colunas := (K div 30) + 1;
  Screen.Cursor := MyCursor;
  Result := MyObjects.SelRadioGroup2(
    'Sele��o de Ocorr�ncia', 'Selecione a ocorr�ncia',
    Itens, Avisos, Acoes, Valores, MyOcors, Colunas, Valor, MyOcorr, SQLType, DefaultItemIndex);
  if Result then
    OcorrBco := Geral.IMV(Valor);
end;

function TTedC_Aux.TedChequeTipoDeMovimento(const Banco: Integer;
  const Envio: TEnvioCNAB; const OcorrBco, TamReg: Integer;
  var MyOcorr: Integer; var Descricao, Aviso: String;
  var SQLType: TSQLType): Boolean;
begin
  Descricao := '';
  Aviso := '';
  SQLType := stNil;
  MyOcorr := CO_MYOCORR_TEDC_NULO;
  //
  if TamReg = 300 then
  begin
    case Envio of
      ecnabRemessa: // Remessa
      begin
        case Banco of
         422:
         begin
           case OcorrBco of
             01:
             begin
               SQLType := stIns;
               Descricao := 'Entrada de cheques';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_ENVIO_AO_BANCO;
             end;
             14:
             begin
               SQLType := stUpd;
               Descricao := 'Altera��o da data de vencimento (data boa) do cheque';
               Aviso := 'Instru��o v�lida somente para a carteira de Dep�sito Programado';
               MyOcorr := CO_MYOCORR_TEDC_ALTERAR_BOM_PARA;
             end;
             61:
             begin
               SQLType := stUpd;
               Descricao := 'Baixa de cheque';
               Aviso := 'Instru��o v�lida somente para a carteira de Dep�sito Programado';
               MyOcorr := CO_MYOCORR_TEDC_INSTRUCAO_BAIXAR;
             end;
           end;
         end;
        end;
      end;
      ecnabRetorno: // Retorno
      begin
        case Banco of
         422:
         begin
           case OcorrBco of
             01:
             begin
               SQLType := stLok;
               Descricao := 'Entrada confirmada';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_ENTRADA_CONFIRMADA;
             end;
             10:
             begin
               SQLType := stLok;
               Descricao := 'Transfer�ncia de Cedente';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_TRANSFERENCIA_DE_CEDENTE;
             end;
             11:
             begin
               SQLType := stLok;
               Descricao := 'Prorroga��o de vencimento';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_PRORROGACAO_DE_VECIMENTO;
             end;
             14:
             begin
               SQLType := stLok;
               Descricao := 'Altera��o de vencimento';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_ALTERACAO_DE_VECIMENTO;
             end;
             20:
             begin
               SQLType := stLok;
               Descricao := '';
               Aviso := 'Carteira em ser';
               MyOcorr := CO_MYOCORR_TEDC_CARTEIRA_EM_SER;
             end;
             30:
             begin
               SQLType := stLok;
               Descricao := 'Previa de Border�';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_PREVIA_DO_BORDERO;
             end;
             31:
             begin
               SQLType := stLok;
               Descricao := 'Entrada rejeitada';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_ENTRADA_REJEITADA;
             end;
             50:
             begin
               SQLType := stLok;
               Descricao := 'Dep�sito';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_DEPOSITO;
             end;
             61:
             begin
               SQLType := stUpd;
               Descricao := 'Baixa por instrucao';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_BAIXA_POR_INSTRUCAO;
             end;
             95:
             begin
               SQLType := stLok;
               Descricao := 'Primeira devolu��o';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_PRIMEIRA_DEVOLUCAO;
             end;
             96:
             begin
               SQLType := stLok;
               Descricao := 'Reapresenta��o';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_REAPRESENTACAO;
             end;
             97:
             begin
               SQLType := stLok;
               Descricao := 'Segunda Devolu��o';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_SEGUNDA_DEVOLUCAO;
             end;
             98:
             begin
               SQLType := stLok;
               Descricao := 'Liquida��o por D�bito em Conta';
               Aviso := '';
               MyOcorr := CO_MYOCORR_TEDC_LIQUIDACAO_POR_DEBITO_EM_CONTA;
             end;
           end;
         end;
        end;
      end;
    end;
  end;
  Result := SQLType <> stNil;
end;

function TTedC_Aux.TextoParaUser(Step: Integer; TxtInconsiste: String): String;
begin
  case Step of
    (*0*)CO_TEDC_STEP_ITEM_ABERTO       : Result := 'Item de retorno n�o processado';
    (*1*)CO_TEDC_STEP_ITEM_INCONSISTENTE: Result := TxtInconsiste;
    (*9*)CO_TEDC_STEP_ITEM_ENCERRADO    : Result := 'Item OK';
  end;
  if Result = '' then
    Result := 'Inconsist�ncia n�o implementada!';
end;

function TTedC_Aux.VariavelToTxtArqRem(var Texto: String; const Variavel: Variant;
  const FmtDta: String; const TamInt, TamFlu: Integer): Boolean;
  var
    Quebra: Boolean;
    Msg: String;
    //
  function Inteiro(Variavel: Variant): String;
  begin
    if Variavel = Null then
      Result := 'Null'
    else
      Result := Geral.FF0(Integer(Variavel));
  end;
  function Intei64(Variavel: Variant): String;
  begin
    if Variavel = Null then
      Result := 'Null'
    else
      Result := String(Variavel);
  end;
  //
  function Flutuante(Variavel: Variant(*; TamInt, TamFlu: Integer*)): String;
  begin
    if Variavel = Null then
      Result := 'Null'
    else
      Result := Geral.FTX(Variavel, TamInt, TamFlu, siPositivo);
  end;
  //
  function Data(const Variavel: Variant(*; FmtDta: String*); var Erro: Boolean;
  var Mensagem: String): String;
  var
    Fmt: String;
    I: Integer;
  begin
    if Variavel = Null then
      Result := 'Null'
    else
    if Variavel = 0 then
    begin
      Result := Geral.FFN(0, Length(FmtDta))
    end else begin
      Fmt := '';
      for I := 1 to Length(FmtDta) do
      begin
        if FmtDta[I] = 'A' then
          Fmt := Fmt + 'Y'
        else
          Fmt := Fmt + FmtDta[I];
      end;
      if Fmt = '' then
      begin
        Erro := True;
        Mensagem := 'Formata��o de data n�o definida!';
      end;
      Result := FormatDateTime(Fmt, Variavel);
    end;
  end;
begin
  // para ver a variavel sem aspas use
  //MLAGeral.VariantToString
  Result := False;
  try
    Quebra := False;
    case VarType(Variavel) of
      varEmpty    {= $0000;} { vt_empty        0 } : Texto := '';
      varNull     {= $0001;} { vt_null         1 } : Texto := 'Null';
      varSmallint {= $0002;} { vt_i2           2 } : Texto := Inteiro(Variavel);
      varInteger  {= $0003;} { vt_i4           3 } : Texto := Inteiro(Variavel);
      varSingle   {= $0004;} { vt_r4           4 } : Texto := Flutuante(Variavel);
      varDouble   {= $0005;} { vt_r8           5 } : Texto := Flutuante(Variavel);
      varCurrency {= $0006;} { vt_cy           6 } : Texto := Flutuante(Variavel);
      varDate     {= $0007;} { vt_date         7 } : Texto := Data(Variavel, Quebra, Msg);
      varOleStr   {= $0008;} { vt_bstr         8 } : Texto := Variavel;
      varDispatch {= $0009;} { vt_dispatch     9 } : Texto := Variavel;
      varError    {= $000A;} { vt_error       10 } : Texto := Variavel;
      varBoolean  {= $000B;} { vt_bool        11 } : Texto := IntToStr(Geral.BoolToInt(Variavel));
      varVariant  {= $000C;} { vt_variant     12 } : Texto := Variavel;
      varUnknown  {= $000D;} { vt_unknown     13 } : Texto := Variavel;
    //varDecimal  {= $000E;} { vt_decimal     14 } {UNSUPPORTED as of v6.x code base}
    //varUndef0F  {= $000F;} { undefined      15 } {UNSUPPORTED per Microsoft}
      varShortInt {= $0010;} { vt_i1          16 } : Texto := Inteiro(Variavel);
      varByte     {= $0011;} { vt_ui1         17 } : Texto := Inteiro(Variavel);
      varWord     {= $0012;} { vt_ui2         18 } : Texto := Inteiro(Variavel);
      varLongWord {= $0013;} { vt_ui4         19 } : Texto := Inteiro(Variavel);
      varInt64    {= $0014;} { vt_i8          20 } : Texto := Intei64(Variavel);
    //varWord64   {= $0015;} { vt_ui8         21 } {UNSUPPORTED as of v6.x code base}
    {  if adding new items, update Variants' varLast, BaseTypeMap and OpTypeMap }

      varStrArg   {= $0048;} { vt_clsid       72 } : Texto := Variavel;
      varString   {= $0100;} { Pascal string 256 } {not OLE compatible } : Texto := Geral.WideStringToSQLString(Variavel);
      varAny      {= $0101;} { Corba any     257 } {not OLE compatible } : Texto := Variavel;
      // custom types range from $110 (272) to $7FF (2047)
      else begin
        Quebra := True;
        Msg := 'Vari�vel n�o definida!';
        Texto := Geral.VariantToString(Variavel);
      end;
    end;
{   N�o usa aqui?
    if VarType(Variavel) in ([varSingle, varDouble, varCurrency]) then
    begin
        if pos(',', Texto) > 0 then Texto[pos(',', Texto)] := '.';
        // Evitar erro em n�meros muito grandes!
        if Length(Texto) > 21 then Texto := Copy(Texto, 1, 21);
    end;
}   if Quebra then
      Geral.MensagemBox(Msg, 'Aviso', MB_OK+MB_ICONWARNING)
    else
      Result := True;
  except
    Geral.MensagemBox('Erro na function "VariavelToString".' + sLineBreak +
    'Tipo de vari�vel: ' + IntToStr(VarType(Variavel)),
    'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

procedure TTedC_Aux.VerificaItemDeRetorno(TedCRetCab, FatID: Integer;
  TabLctA: String;
  LaAvisoG1, LaAvisoG2, LaAvisoB1, LaAvisoB2: TLabel);
  procedure ReabreTedCRemIts(Controle: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmTedC.QrTCMI, Dmod.MyDB, [
    'SELECT Controle, LoReCaPrev, LoReItPrev, ',
    'LoReCaProc, LoReItProc, LoReMyOcor ',
    'FROM tedcremits ',
    'WHERE Controle=' + Controle,
    '']);
    //
  end;
var
  Msg: String;
  IM: String;
  MyOcorr, LoReCaPrev, LoReItPrev, LoReCaProc, LoReItProc,
  LctSit, LctLot, AtuLot, AtuSit, Controle, RemItsCtrl,
  Codigo, Linha, Sit, Inconsiste: Integer;
  LctPrv,
  Continua, Altera, Maior, DtBoa, JaCom, Encerra: Boolean;
  DDeposito, RetDataBoa, Compensado: TDateTime;
  //
  //Controle, FatID, TedRet_Lot, TedRet_Sit, 
  TedRem_Sit, TedRem_Its,
  Sub, FatParcela, Step, LoReMyOcor, TedRetPrev, TedRetExeC, TedRetExeS: Integer;
  FatNum: Double;
  DataHora: String;
  AchouLct: Boolean;
begin
  Msg := 'Verificando itens de pr�via de border�';
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, Msg);
  UnDmkDAC_PF.AbreMySQLQuery0(DmTedC.QrTCTI, Dmod.MyDB, [
  'SELECT * ',
  'FROM tedcretits ',
  'WHERE Codigo=' + Geral.FF0(TedCRetCab), //QrSemErrTedCRetCab.Value
//  'AND MyOcorr=' + Geral.FF0(MyOcorr),
  'AND Step < ' + Geral.FF0(CO_TEDC_STEP_ARQ_ARQUIVO_LIDO), // que n�o foram totalmente encerrados
  '']);
  DmTedC.QrTCTI.First;
  while not DmTedC.QrTCTI.Eof do
  begin
    DmTedC.ReopenLocLct1(DmTedC.QrTCTILctCtrl.Value, DmTedC.QrTCTILctSub.Value,
    FatID, DmTedC.QrTCTILctFatNum.Value, DmTedC.QrTCTILctFatParc.Value,
    TabLctA);
    if DmTedC.QrLocLct1.RecordCount = 0 then
    begin
      AchouLct := False;
      // Caso n�o foi localizado avisa o usu�rio
      Geral.MensagemBox('ERRO! O lan�amento (Controle = ' + Geral.FF0(
      DmTedC.QrTCTILctCtrl.Value) + ') n�o foi localizado.' + sLineBreak +
      'Certifique-se de que ele n�o tenha sido exclu�do!',
      'ERRO', MB_OK+MB_ICONERROR);
      //
      //Continua := False;
    end else AchouLct := True;
    //
    MyOcorr := DmTedC.QrTCTIMyOcorr.Value;
    Inconsiste := 0;
    IM := Geral.FF0(DmTedC.QrTCTISeuNumero.Value);
    //
    // Procura o item de remessa relacionado
    MyObjects.Informa2(LaAvisoB1, LaAvisoB2, True, 'Item de remessa: ' + IM);
    ReabreTedCRemIts(IM);
    // Caso n�o achou verifica se n�o est� na lixeira
    if DmTedC.QrTCMI.RecordCount = 0 then
    begin
      // caso n�o achou, verifica se o item foi exclu�do ap�s ter sido
      // enviado ao banco e antes que fosse processado o retorno. (Limbo mesmo!)
      if UMyMod.MoveRegistroEntreTabelas(Dmod.QrUpd, 'tedcremdel', 'tedcremits',
      ['Controle'], ['='], [IM], amrInsAndDel) then
        // Procura o item de remessa novamente
        ReabreTedCRemIts(IM);
    end;
    // Caso achou processa o item de retorno
    if DmTedC.QrTCMI.RecordCount > 0 then
    begin
      Continua := True;
      // Cria um relacionamento, se o item de remessa ainda n�o tiver
      // ... se for uma pr�via:
      if MyOcorr = CO_MYOCORR_TEDC_PREVIA_DO_BORDERO then
      begin
        if DmTedC.QrTCMILoReCaPrev.Value = 0 then
        begin
          LoReCaPrev := DmTedC.QrTCTICodigo.Value;
          LoReItPrev := DmTedC.QrTCTILinha.Value;
          Controle := DmTedC.QrTCMIControle.value;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcremits', False, [
          'LoReCaPrev', 'LoReItPrev'], ['Controle'], [
          LoReCaPrev, LoReItPrev], [Controle], True);
        end;
        // tamb�m informa no lancto de que o banco informou que o cheque est� no limbo
        if AchouLct then
        begin
          TedRem_Its := DmTedC.QrLocLct1TedRem_Its.Value;
          if TedRem_Its = DmTedC.QrTCTISeuNumero.Value then
          begin
            case DmTedC.QrLocLct1TedRem_Sit.Value of
              CO_SIT_TEDC_INSTRUCAO_ENTRADA_NO_BANCO: TedRem_Sit := CO_SIT_TEDC_LIMBO_ENTRADA_NO_BANCO;
              CO_SIT_TEDC_INSTRUCAO_ALTERACAO_NO_BANCO:  TedRem_Sit := CO_SIT_TEDC_LIMBO_ALTERACAO_NO_BANCO;
              else TedRem_Sit := CO_SIT_TEDC_NADA;
            end;
            if (TedRem_Sit <> CO_SIT_TEDC_NADA) then
            begin
              Controle :=  DmTedC.QrTCTILctCtrl.Value;
              Sub :=  DmTedC.QrTCTILctSub.Value;
              //FatID := FatID;
              FatNum :=  DmTedC.QrTCTILctFatNum.Value;
              FatParcela := DmTedC.QrTCTILctFatParc.Value;
              //
              TedRetPrev := DmTedC.QrTCTIControle.Value;
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabLctA, False, [
              'TedRem_Sit', 'TedRetPrev'], [
              'TedRem_Its', 'Controle', 'Sub', 'FatID', 'FatNum', 'FatParcela'], [
              TedRem_Sit, TedRetPrev], [
              TedRem_Its, Controle, Sub, FatID, FatNum, FatParcela],  True);
            end else
            begin
            // Aqui � diferente quando MyOcorr n�o for CO_MYOCORR_TEDC_PREVIA_DO_BORDERO
            // pois no teste do banco 422 a entrada veio depois da pr�via!
              if (DmTedC.QrLocLct1TedRetExeC.Value = 0)
              and (DmTedC.QrLocLct1TedRetExeS.Value = 0) then
              begin
                Geral.MensagemBox('ERRO! O item de remessa (Controle = ' + IM +
                ') est� com a situa��o incorreta.',
                'ERRO', MB_OK+MB_ICONERROR);
                //
                Continua := False;
              end else
              begin
                // Nada? N�o � erro?
              end;
            end;
          end else
          begin
            // � erro? pois pode ter mudado para outro lote de instru��o?
            Geral.MensagemBox('ERRO! O item de remessa (Controle = ' + IM +
            ') n�o confere com o esperado.',
            'ERRO', MB_OK+MB_ICONERROR);
            //
            Continua := False;
          end;
        end else Continua := False;
      end else
        // ... se N�O for uma pr�via:
      begin
        // informa o lote de processamento
        if DmTedC.QrTCMILoReCaProc.Value = 0 then
        begin
          LoReCaProc := DmTedC.QrTCTICodigo.Value;
          LoReItProc := DmTedC.QrTCTILinha.Value;
          Controle := DmTedC.QrTCMIControle.value;
          LoReMyOcor := MyOcorr;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcremits', False, [
          'LoReCaProc', 'LoReItProc', 'LoReMyOcor'], ['Controle'], [
          LoReCaProc, LoReItProc, LoReMyOcor], [Controle], True);
        end;
        // tamb�m informa no lancto de que o banco informou que a instru��o foi processada
        if AchouLct then
        begin
          TedRem_Its := DmTedC.QrLocLct1TedRem_Its.Value;
          if TedRem_Its = DmTedC.QrTCTISeuNumero.Value then
          begin
            case DmTedC.QrLocLct1TedRem_Sit.Value of
              CO_SIT_TEDC_NADA: TedRem_Sit := CO_SIT_TEDC_LIMBO_ENTRADA_NO_BANCO;
              CO_SIT_TEDC_INSTRUCAO_ENTRADA_NO_BANCO: TedRem_Sit := CO_SIT_TEDC_ENTROU_NO_BANCO;
              CO_SIT_TEDC_LIMBO_ENTRADA_NO_BANCO: TedRem_Sit := CO_SIT_TEDC_ENTROU_NO_BANCO;
              CO_SIT_TEDC_INSTRUCAO_ALTERACAO_NO_BANCO:  TedRem_Sit := CO_SIT_TEDC_ALTEROU_NO_BANCO;
              CO_SIT_TEDC_LIMBO_ALTERACAO_NO_BANCO:  TedRem_Sit := CO_SIT_TEDC_ALTEROU_NO_BANCO;
              CO_SIT_TEDC_ALTEROU_NO_BANCO:  TedRem_Sit := CO_SIT_TEDC_ALTEROU_NO_BANCO;
              CO_SIT_TEDC_BAIXOU_NO_BANCO:  TedRem_Sit := CO_SIT_TEDC_NADA;
              else TedRem_Sit := CO_SIT_TEDC_NADA;
            end;
            //
            if TedRem_Sit <> CO_SIT_TEDC_NADA then
            begin
              Controle :=  DmTedC.QrTCTILctCtrl.Value;
              Sub :=  DmTedC.QrTCTILctSub.Value;
              //FatID := FatID;
              FatNum :=  DmTedC.QrTCTILctFatNum.Value;
              FatParcela := DmTedC.QrTCTILctFatParc.Value;
              //
              TedRetExeC := DmTedC.QrTCTIControle.Value;
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabLctA, False, [
              'TedRem_Sit'], [
              'TedRem_Its', 'Controle', 'Sub', 'FatID', 'FatNum', 'FatParcela'], [
              TedRem_Sit], [
              TedRem_Its, Controle, Sub, FatID, FatNum, FatParcela],  True);
            end else
            begin
              // Nada? N�o � erro?

              Geral.MensagemBox('ERRO! O item de remessa (Controle = ' + IM +
              ') est� com a situa��o incorreta.',
              'ERRO', MB_OK+MB_ICONERROR);
              //
              Continua := False;
            end;
          end else
          begin
            // Nada? N�o � erro pois pode ter mudado para outro lote de instru��o?
            Geral.MensagemBox('ERRO! O item de remessa (Controle = ' + IM +
            ') n�o confere com o esperado.',
            'ERRO', MB_OK+MB_ICONERROR);
            //
            Continua := False;
          end;
        end else Continua := False;
      end;



      // Informa no item de retorno o item de remessa que foi achado
      if DmTedC.QrTCTIRemItsCtrl.Value = 0 then
      begin
        RemItsCtrl := DmTedC.QrTCMIControle.value;
        Codigo := DmTedC.QrTCTICodigo.Value;
        Linha := DmTedC.QrTCTILinha.Value;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcretits', False, [
        'RemItsCtrl'], ['Codigo', 'Linha'], [
        RemItsCtrl], [Codigo, Linha], True);
      end;
      //Continua := True;
    end else begin
      // caso n�o achou, avisa o usu�rio do problema e n�o continua
      // o processamento do item atual
      Geral.MensagemBox('ERRO! O item de remessa (Controle = ' + IM +
      ') n�o foi localizado.' + sLineBreak +
      'Certifique-se de que ele n�o tenha sido exclu�do!',
      'ERRO', MB_OK+MB_ICONERROR);
      //
      Continua := False;
    end;

    if Continua then
    begin
      //Continua := False;
      // Caso for s� previa j� pode encerrar.
      // Caso contr�rio, � necess�rio mudar o lancto primeiro!
      Encerra := MyOcorr = CO_MYOCORR_TEDC_PREVIA_DO_BORDERO;
      // Atualiza o registro de lan�amento financeiro
      // e devolve ou prorroga caso necess�rio
      if AchouLct then
      begin
        Altera := False;
        LctLot := DmTedC.QrLocLct1TCRI_Lot.Value;
        LctSit := DmTedC.QrLocLct1TedRetExeS.Value;
        AtuLot := DmTedC.QrTCTICodigo.Value;
        //LctPrv := LctSit = CO_MYOCORR_TEDC_PREVIA_DO_BORDERO;
        DDeposito  := DmTedC.QrLocLct1DDeposito.Value;
        Compensado := DmTedC.QrLocLct1Compensado.Value;
        Sit        := DmTedC.QrLocLct1Sit.Value;
        Maior := AtuLot >= LctLot; // coloquei o igual para refazer caso de erro em alguma etapa de uma tentativa anterior!
        DtBoa := Trunc(DDeposito) <> Trunc(RetDataBoa);
        JaCom := (Compensado > 2) or (Sit > 0);
        TedRem_Sit := CO_SIT_TEDC_DESCONHECIDO;
        case MyOcorr of
          // 201: Entrada: Altera s� se "Maior"
          CO_MYOCORR_TEDC_ENTRADA_CONFIRMADA:
          begin
            Altera := Maior;
            TedRem_Sit := CO_SIT_TEDC_ENTROU_NO_BANCO;
          end;
{ TODO : Fazer as ocorr�ncias que faltam }
{ TODO : Setar Depositado=? quando desfazer dep�sito! }

          // 210: Transfer�ncia de cedente
          //Parei aqui! ver com o cliente!
          //CO_MYOCORR_TEDC_TRANSFERENCIA_DE_CEDENTE = 210;

          {Parei aqui! ver com o cliente!
          // 211: Prorroga��o! Altera s� se "Maior"
          CO_MYOCORR_TEDC_PRORROGACAO_DE_VECIMENTO:
          begin
            Altera := Maior;
            TedRem_Sit := CO_SIT_TEDC_ALTEROU_NO_BANCO;
          end;
          }
          {Parei aqui! ver com o cliente!
          // 214: Altera��o de vencimento: Altera s� se "Maior"
          CO_MYOCORR_TEDC_ALTERACAO_DE_VECIMENTO:
          begin
            Altera := Maior;
            TedRem_Sit := CO_SIT_TEDC_ALTEROU_NO_BANCO;
          end;
          }

          {Parei aqui! ver com o cliente!
          // 220: N�o faz nada? O banco apenas est� informando que o cheque continua em cust�dia?
          CO_MYOCORR_TEDC_CARTEIRA_EM_SER:
          begin
            Altera := Maior;
            TedRem_Sit := ?;
          end;
          }

          // 230: N�o faz nada! O banco apenas est� informando que recebeu a instru��o
          CO_MYOCORR_TEDC_PREVIA_DO_BORDERO:
          begin
            // Encerra = true (Acima)
            Altera := False;
            TedRem_Sit := CO_SIT_TEDC_DESCONHECIDO; // N�o deveria alterar o lct (Altera=False), mas se setar vamos saber!. Argh!
          end;

          {Parei aqui! ver o que fazer!
          // 231: Entrada rejeitada: Altera s� se "Maior"
          CO_MYOCORR_TEDC_ENTRADA_REJEITADA:
          begin
            Altera := Maior;
            TedRem_Sit := ?;
          end;
          }

          {Parei aqui! ver com o cliente!
          // 250: Dep�sito. S� se "Maior" porque j� pode ter sido devolvido!
          CO_MYOCORR_TEDC_DEPOSITO:
          begin
            Altera := Maior;
            TedRem_Sit := ?;
          end;
          }

          {Parei aqui! ver com o cliente!
          // 261: Baixa por instru��o. S� se "Maior" porque pode ter sido redepositado?
          CO_MYOCORR_TEDC_BAIXA_POR_INSTRUCAO:
          begin
            Altera := Maior;
            TedRem_Sit := ?;
          end;
          }

          {Parei aqui! ver com o cliente!
          // 295: 1� devolu��o. S� se "Maior", pois j� pode ter sido reapresentado, devolvido pela segunda vez ou baixado?
          CO_MYOCORR_TEDC_PRIMEIRA_DEVOLUCAO:
          begin
            Altera := Maior;
            TedRem_Sit := ?;
          end;
          }

          {Parei aqui! ver com o cliente!
          // 296: Reapresenta��o: S� se "Maior", pois j� pode ter sido devolvido pela 2� vaz ou baixado?
          CO_MYOCORR_TEDC_REAPRESENTACAO:
          begin
            Altera := Maior;
            TedRem_Sit := ?;
          end;
          }

          {Parei aqui! ver com o cliente!
          // 297: 2� devolu��o. S� se "Maior", pois j� pode ter sido baixado?
          CO_MYOCORR_TEDC_SEGUNDA_DEVOLUCAO:
          begin
            Altera := Maior;
            TedRem_Sit := ?;
          end;
          }

{
          // 298: Liquida��o por d�bito em conta! O que relamente aconteceu? O que Fazer?
          CO_MYOCORR_TEDC_LIQUIDACAO_POR_DEBITO_EM_CONTA:
          begin
            Altera := True;
            TedRem_Sit := ?;
          end;
          }

          else (*Geral.MensagemBox('"MyOcor" c�digo: ' + Geral.FF0(MyOcorr) +
          ' sem implementa��o! AVISE A DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);*)
            Inconsiste := CO_TEDC_ERR_OCRR_NAO_IMPLEMENTADA;
        end;
        // Ve se n�o h� inconsist�ncias conhecidas.
        if Altera and (Inconsiste <> 0) then
        begin
          // Verifica se o DDeposito confere com o BOM PARA informado no arquivo!
          if MyOcorr in ([
          CO_MYOCORR_TEDC_ENTRADA_CONFIRMADA,
{ TODO :    AAA :: URGENTE!!! ver se vem data! }
          //CO_MYOCORR_TEDC_CARTEIRA_EM_SER,
          CO_MYOCORR_TEDC_PRORROGACAO_DE_VECIMENTO,
          CO_MYOCORR_TEDC_ALTERACAO_DE_VECIMENTO]) then
            if not DtBoa then
              Inconsiste := CO_TEDC_ERR_DATABOA;
          //ver se o lan�amento j� n�o foi quitado...
          if JaCom then
            Inconsiste := CO_TEDC_ERR_DOCUMENTO_JA_COMPENSADO;
        end;

        // Caso n�o encontrou inconsist�ncia altera...
        if Altera and (Inconsiste = 0) then
        begin
          //TedRetExeC := DmTedC.QrTCTICodigo.Value;
          TedRetExeC := DmTedC.QrTCTIControle.Value;
          TedRetExeS := MyOcorr;
          //
          Controle := DmTedC.QrTCTILctCtrl.Value;
          Sub := DmTedC.QrTCTILctSub.Value;
          //FatID := FatID;
          FatNum := DmTedC.QrTCTILctFatNum.Value;
          FatParcela := DmTedC.QrTCTILctFatParc.Value;

          Encerra := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabLctA, False, [
          'TedRem_Sit', 'TedRetExeC', 'TedRetExeS'], [
          'Controle', 'Sub', 'FatID', 'FatNum', 'FatParcela'], [
          TedRem_Sit, TedRetExeC, TedRetExeS], [
          Controle, Sub, FatID, FatNum, FatParcela],  True);
        end else
        begin
          // ... caso tenha inconsit�ncia informa no item de retorno e gera log.
          if Inconsiste <> 0 then
          begin
            Step := CO_TEDC_STEP_ITEM_INCONSISTENTE; // = 1
            Codigo := DmTedC.QrTCTICodigo.Value;
            Linha := DmTedC.QrTCTILinha.Value;
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcretits', False, [
            'Step', 'Inconsiste'], ['Codigo', 'Linha'], [
            Step, Inconsiste], [Codigo, Linha], True);
            //
  { TODO : Quando processar o LOG atualizar o step do item de retorno e do lote! }
            DataHora := Geral.FDT(DmodG.ObtemAgora(), 109);
            Sit := CO_TEDC_STEP_LOG_ITEM_ABERTO; // = 0; // Aberto
            //Inconsiste := Inconsiste;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'tedcretlog', False, [
            'DataHora', 'Sit'], [
            'Codigo', 'Linha', 'Inconsiste'], [
            DataHora, Sit], [
            Codigo, Linha, Inconsiste], True);
          end;
        end;
      end;
      // Conseguindo informar no item de remessa, no item de retorno e no
      // lan�amento, alterar o item de retorno para Step=9 = encerrado
      if Encerra then
      begin
        Step := CO_TEDC_STEP_ITEM_ENCERRADO; // = 9
        Codigo := DmTedC.QrTCTICodigo.Value;
        Linha := DmTedC.QrTCTILinha.Value;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tedcretits', False, [
        'Step'], ['Codigo', 'Linha'], [
        Step], [Codigo, Linha], True);
      end;
    end;
    //
    DmTedC.QrTCTI.Next;
    //
  end;
  // Atualiza o Step do Lote (tedcretcab) caso todos itens foram
  // processados com sucesso
  DmTedC.AtualizaStepRetorno(TedCRetCab);
end;

end.
