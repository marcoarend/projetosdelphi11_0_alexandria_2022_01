object FmTedCRetArq: TFmTedCRetArq
  Left = 339
  Top = 185
  Caption = 
    'TED-CHQUE-200 :: Listagem de Arquivos Retorno TED Cheque Dispon'#237 +
    'veis'
  ClientHeight = 403
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 677
        Height = 32
        Caption = 'Listagem de Arquivos Retorno TED Cheque Dispon'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 677
        Height = 32
        Caption = 'Listagem de Arquivos Retorno TED Cheque Dispon'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 677
        Height = 32
        Caption = 'Listagem de Arquivos Retorno TED Cheque Dispon'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 204
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 204
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 204
        Align = alClient
        TabOrder = 0
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 1004
          Height = 187
          Align = alClient
          DataSource = DsListaTed
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGrid1DrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / Hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Layout'
              Width = 37
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NoLayout'
              Title.Caption = 'Nome do layout'
              Width = 169
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NoArquivo'
              Title.Caption = 'Nome do arquivo'
              Width = 412
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Itens'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Step'
              Width = 26
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TedCRetCab'
              Title.Caption = 'Lote'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Erro'
              Width = 25
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NoErro'
              Title.Caption = 'Descri'#231#227'o do erro'
              Width = 300
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 256
    Width = 1008
    Height = 77
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 60
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAvisoG2: TLabel
        Left = 13
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAvisoR1: TLabel
        Left = 13
        Top = 36
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAvisoR2: TLabel
        Left = 12
        Top = 35
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAvisoB1: TLabel
        Left = 13
        Top = 18
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAvisoB2: TLabel
        Left = 12
        Top = 17
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAvisoG1: TLabel
        Left = 12
        Top = 0
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 333
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConcilia: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Concilia'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConciliaClick
      end
    end
  end
  object QrCfgCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF, tcc.* '
      'FROM tedccfgcab tcc'
      'LEFT JOIN enticliint eci ON eci.CodCliInt=tcc.Empresa'
      'LEFT JOIN entidades ent ON ent.Codigo=eci.CodEnti'
      'WHERE tcc.RemRet IN (2,3)'
      'ORDER BY tcc.Codigo')
    Left = 496
    Top = 144
    object QrCfgCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCfgCabBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCfgCabAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrCfgCabVersao: TWideStringField
      FieldName = 'Versao'
    end
    object QrCfgCabTamReg: TIntegerField
      FieldName = 'TamReg'
    end
    object QrCfgCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCfgCabSeqLay: TIntegerField
      FieldName = 'SeqLay'
    end
    object QrCfgCabRemRet: TIntegerField
      FieldName = 'RemRet'
    end
    object QrCfgCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCfgCabCartDep: TIntegerField
      FieldName = 'CartDep'
    end
    object QrCfgCabPathRem: TWideStringField
      FieldName = 'PathRem'
      Size = 255
    end
    object QrCfgCabPathRet: TWideStringField
      FieldName = 'PathRet'
      Size = 255
    end
    object QrCfgCabCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
  end
  object QrListaTed: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * FROM _lista_ted_'
      'ORDER BY DataHora')
    Left = 496
    Top = 116
    object QrListaTedDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrListaTedLayout: TIntegerField
      FieldName = 'Layout'
    end
    object QrListaTedTedCRetCab: TIntegerField
      FieldName = 'TedCRetCab'
    end
    object QrListaTedNoLayout: TWideStringField
      FieldName = 'NoLayout'
      Size = 100
    end
    object QrListaTedNoArquivo: TWideStringField
      FieldName = 'NoArquivo'
      Size = 255
    end
    object QrListaTedNoErro: TWideStringField
      FieldName = 'NoErro'
      Size = 255
    end
    object QrListaTedErro: TIntegerField
      FieldName = 'Erro'
    end
    object QrListaTedStep: TIntegerField
      FieldName = 'Step'
    end
    object QrListaTedItens: TIntegerField
      FieldName = 'Itens'
    end
    object QrListaTedAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsListaTed: TDataSource
    DataSet = QrListaTed
    Left = 524
    Top = 116
  end
  object QrListaRet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT trc.Codigo, trc.TedC_Cfg, trc.Step, trc.DataGer,'
      'trc.Itens, trc.Nome, tcc.Nome NoTedC_Cfg'
      'FROM tedcretcab trc'
      'LEFT JOIN tedccfgcab tcc ON tcc.Codigo=trc.TedC_Cfg '
      'WHERE trc.Step=9')
    Left = 524
    Top = 144
    object QrListaRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaRetTedC_Cfg: TIntegerField
      FieldName = 'TedC_Cfg'
    end
    object QrListaRetStep: TIntegerField
      FieldName = 'Step'
    end
    object QrListaRetDataGer: TDateField
      FieldName = 'DataGer'
    end
    object QrListaRetNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrListaRetItens: TIntegerField
      FieldName = 'Itens'
    end
    object QrListaRetNoTedC_Cfg: TWideStringField
      FieldName = 'NoTedC_Cfg'
      Size = 50
    end
  end
  object QrSemErr: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM _lista_ted_'
      'WHERE Erro=0'
      'ORDER BY DataHora')
    Left = 564
    Top = 116
    object QrSemErrDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrSemErrLayout: TIntegerField
      FieldName = 'Layout'
    end
    object QrSemErrTedCRetCab: TIntegerField
      FieldName = 'TedCRetCab'
    end
    object QrSemErrNoLayout: TWideStringField
      FieldName = 'NoLayout'
      Size = 100
    end
    object QrSemErrNoArquivo: TWideStringField
      FieldName = 'NoArquivo'
      Size = 255
    end
    object QrSemErrNoErro: TWideStringField
      FieldName = 'NoErro'
      Size = 255
    end
    object QrSemErrErro: TIntegerField
      FieldName = 'Erro'
    end
    object QrSemErrStep: TIntegerField
      FieldName = 'Step'
    end
    object QrSemErrItens: TIntegerField
      FieldName = 'Itens'
    end
    object QrSemErrAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object PMImprime: TPopupMenu
    Left = 64
    Top = 24
    object Arquivoselecionado1: TMenuItem
      Caption = 'Arquivo &Atual'
      OnClick = Arquivoselecionado1Click
    end
    object ArquivosSelecionados1: TMenuItem
      Caption = 'Arquivos &Selecionados'
      OnClick = ArquivosSelecionados1Click
    end
  end
end
