unit TedCRetCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Menus, Grids, DBGrids, UnDmkProcFunc, UnDmkEnums;

type
  TFmTedCRetCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrTedCRetCab: TmySQLQuery;
    QrTedCRetCabCodigo: TSmallintField;
    QrTedCRetCabNome: TWideStringField;
    DsTedCRetCab: TDataSource;
    QrTedCRetIts: TmySQLQuery;
    DsTedCRetIts: TDataSource;
    PMIts: TPopupMenu;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    Memo1: TMemo;
    QrTedCRetItsCodigo: TIntegerField;
    QrTedCRetItsLinha: TIntegerField;
    QrTedCRetItsLctCtrl: TIntegerField;
    QrTedCRetItsLctSub: TIntegerField;
    QrTedCRetItsLctFatID: TIntegerField;
    QrTedCRetItsLctFatNum: TLargeintField;
    QrTedCRetItsLctFatParc: TIntegerField;
    QrTedCRetItsOcorrBco: TIntegerField;
    QrTedCRetItsMyOcorr: TIntegerField;
    QrTedCRetItsStep: TSmallintField;
    QrTedCRetItsCedente: TWideStringField;
    QrTedCRetItsFilial: TWideStringField;
    QrTedCRetItsUsoEmpresa: TWideStringField;
    QrTedCRetItsDtaCaptura: TDateField;
    QrTedCRetItsCMC7: TWideStringField;
    QrTedCRetItsRemValor: TFloatField;
    QrTedCRetItsRemDataBoa: TDateField;
    QrTedCRetItsBorderoBco: TIntegerField;
    QrTedCRetItsSeuNumero: TLargeintField;
    QrTedCRetItsAlinea: TIntegerField;
    QrTedCRetItsCodRejeito: TIntegerField;
    QrTedCRetItsNossoNum: TLargeintField;
    QrTedCRetItsRetValor: TFloatField;
    QrTedCRetItsRetDataBoa: TDateField;
    QrTedCRetItsTrfCedente: TWideStringField;
    QrTedCRetItsTrfDtaOper: TDateField;
    QrTedCRetItsTrfContrat: TWideStringField;
    QrTedCRetItsTrfParcela: TIntegerField;
    QrTedCRetItsTrfDtaCntr: TDateField;
    QrTedCRetItsBanco: TIntegerField;
    QrTedCRetItsAgencia: TIntegerField;
    QrTedCRetItsContaCorre: TWideStringField;
    QrTedCRetItsCheque: TIntegerField;
    QrTedCRetItsPraca: TIntegerField;
    QrTedCRetItsRetInscTip: TWideStringField;
    QrTedCRetItsRetInscNum: TWideStringField;
    QrTedCRetItsRetEmitNom: TWideStringField;
    QrTedCRetItsRemItsCtrl: TIntegerField;
    QrTedCRetItsInconsiste: TIntegerField;
    QrTedCRetItsNO_MyOcorr: TWideStringField;
    QrTedCRetItsNO_Inconsiste: TWideStringField;
    QrTedCRetItsTEXTO_PARA_USR: TWideStringField;
    Processa1: TMenuItem;
    QrTedCRetCabEmpresa: TIntegerField;
    QrTedCRetCabTedC_Cfg: TIntegerField;
    QrTedCRetCabStep: TIntegerField;
    QrTedCRetCabProduto: TWideStringField;
    QrTedCRetCabCedente: TWideStringField;
    QrTedCRetCabFilial: TWideStringField;
    QrTedCRetCabCNPJCPF: TWideStringField;
    QrTedCRetCabBanco: TIntegerField;
    QrTedCRetCabDataGer: TDateField;
    QrTedCRetCabNumSeqGer: TIntegerField;
    QrTedCRetCabDepositari: TWideStringField;
    QrTedCRetCabTerceira: TWideStringField;
    QrTedCRetCabCartQtd: TIntegerField;
    QrTedCRetCabCartVal: TFloatField;
    QrTedCRetCabEntrQtd: TIntegerField;
    QrTedCRetCabEntrVal: TFloatField;
    QrTedCRetCabDepoQtd: TIntegerField;
    QrTedCRetCabDepoVal: TFloatField;
    QrTedCRetCabBaxaQtd: TIntegerField;
    QrTedCRetCabBaxaVal: TFloatField;
    QrTedCRetCabDevlQtd: TIntegerField;
    QrTedCRetCabDevlVal: TFloatField;
    QrTedCRetCabReapQtd: TIntegerField;
    QrTedCRetCabReapVal: TFloatField;
    QrTedCRetCabUltLin: TIntegerField;
    QrTedCRetCabItens: TIntegerField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    DBEdit6: TDBEdit;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    Label12: TLabel;
    DBEdit8: TDBEdit;
    Label13: TLabel;
    DBEdit9: TDBEdit;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    Label28: TLabel;
    DBEdit24: TDBEdit;
    Label29: TLabel;
    DBEdit25: TDBEdit;
    Panel6: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    GroupBox3: TGroupBox;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GroupBox4: TGroupBox;
    DBEdit16: TDBEdit;
    DBEdit15: TDBEdit;
    GroupBox5: TGroupBox;
    DBEdit18: TDBEdit;
    DBEdit17: TDBEdit;
    GroupBox6: TGroupBox;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    GroupBox7: TGroupBox;
    DBEdit22: TDBEdit;
    DBEdit21: TDBEdit;
    Panel7: TPanel;
    Label21: TLabel;
    Label15: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTedCRetCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTedCRetCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTedCRetCabAfterScroll(DataSet: TDataSet);
    procedure QrTedCRetItsCalcFields(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Processa1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenCadastro_Com_Itens_ITS();

  end;

var
  FmTedCRetCab: TFmTedCRetCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, ModuleTedC, TedC_Aux;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTedCRetCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTedCRetCab.PMCabPopup(Sender: TObject);
begin
{
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTedCRetCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTedCRetCab, QrTedCRetIts);
}
end;

procedure TFmTedCRetCab.PMItsPopup(Sender: TObject);
begin
{
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrTedCRetCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrTedCRetIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrTedCRetIts);
  //
  MyObjects.HabilitaMenuItemItsDel(Nomeiagrupo1, QrTedCRetIts);
}
end;

procedure TFmTedCRetCab.Processa1Click(Sender: TObject);
begin
// ??
end;

procedure TFmTedCRetCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTedCRetCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTedCRetCab.DefParams;
begin
  VAR_GOTOTABELA := 'tedcretcab';
  VAR_GOTOMYSQLTABLE := QrTedCRetCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM tedcretcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTedCRetCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmTedCRetCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTedCRetCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTedCRetCab.ReopenCadastro_Com_Itens_ITS();
begin
  QrTedCRetIts.Close;
  QrTedCRetIts.Params[0].AsInteger := QrTedCRetCabCodigo.Value;
  QrTedCRetIts.Open;
end;


procedure TFmTedCRetCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTedCRetCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTedCRetCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTedCRetCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTedCRetCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTedCRetCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTedCRetCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTedCRetCabCodigo.Value;
  Close;
end;

procedure TFmTedCRetCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTedCRetCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'tedcretcab');
end;

procedure TFmTedCRetCab.BtConfirmaClick(Sender: TObject);
{
var
  Codigo: Integer;
  Nome: String;
}
begin
{
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('tedcretcab', 'Codigo', ImgTipo.SQLType,
    QrTedCRetCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmCadastro_Com_Itens_CAB, PnEdita,
    'tedcretcab', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
}
end;

procedure TFmTedCRetCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'tedcretcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'tedcretcab', 'Codigo');
end;

procedure TFmTedCRetCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTedCRetCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTedCRetCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  Memo1.Align := alClient;
end;

procedure TFmTedCRetCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTedCRetCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTedCRetCab.SbImprimeClick(Sender: TObject);
begin
  DmTedC.MostraLoteRetorno(QrTedCRetCabCodigo.Value);
end;

procedure TFmTedCRetCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTedCRetCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTedCRetCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTedCRetCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTedCRetCab.QrTedCRetCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTedCRetCab.QrTedCRetCabAfterScroll(DataSet: TDataSet);
begin
  ReopenCadastro_Com_Itens_ITS();
end;

procedure TFmTedCRetCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTedCRetCabCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmTedCRetCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTedCRetCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'tedcretcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTedCRetCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTedCRetCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTedCRetCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'tedcretcab');
end;

procedure TFmTedCRetCab.QrTedCRetCabBeforeOpen(DataSet: TDataSet);
begin
  QrTedCRetCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTedCRetCab.QrTedCRetItsCalcFields(DataSet: TDataSet);
begin
  QrTedCRetItsTEXTO_PARA_USR.Value := TedC_Ax.TextoParaUser(
    QrTedCRetItsStep.Value,
    QrTedCRetItsNO_Inconsiste.Value);
end;

end.

