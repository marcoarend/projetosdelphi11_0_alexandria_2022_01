object FmTedCRemCab: TFmTedCRemCab
  Left = 256
  Top = 161
  Caption = 'TED-CHQUE-101 :: Remessa TED Cheque'
  ClientHeight = 664
  ClientWidth = 1016
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1016
    Height = 568
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object GroupBox3: TGroupBox
      Left = 0
      Top = 505
      Width = 1016
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel4: TPanel
        Left = 906
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1016
      Height = 101
      Align = alTop
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 1012
        Height = 42
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Panel8'
        TabOrder = 0
        object Label9: TLabel
          Left = 12
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label3: TLabel
          Left = 88
          Top = 0
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label11: TLabel
          Left = 208
          Top = 0
          Width = 26
          Height = 13
          Caption = 'Hora:'
        end
        object Label20: TLabel
          Left = 280
          Top = 0
          Width = 71
          Height = 13
          Caption = 'Lote Remessa:'
        end
        object Label7: TLabel
          Left = 356
          Top = 0
          Width = 76
          Height = 13
          Caption = 'Layout utilizado:'
        end
        object SpeedButton5: TSpeedButton
          Left = 970
          Top = 16
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object EdCodigo: TdmkEdit
          Left = 12
          Top = 16
          Width = 73
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object TPDataG: TdmkEditDateTimePicker
          Left = 88
          Top = 16
          Width = 115
          Height = 21
          Date = 39067.403862476900000000
          Time = 39067.403862476900000000
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPHoraG: TDateTimePicker
          Left = 208
          Top = 16
          Width = 69
          Height = 21
          Date = 39198.000000000000000000
          Time = 39198.000000000000000000
          Kind = dtkTime
          TabOrder = 2
        end
        object EdCodUsu: TdmkEdit
          Left = 280
          Top = 16
          Width = 72
          Height = 21
          Alignment = taRightJustify
          MaxLength = 8
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdTedC_Cfg: TdmkEditCB
          Left = 356
          Top = 16
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdTedC_CfgChange
          DBLookupComboBox = CBTedC_Cfg
          IgnoraDBLookupComboBox = False
        end
        object CBTedC_Cfg: TdmkDBLookupComboBox
          Left = 424
          Top = 16
          Width = 542
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTedCCfgCab
          TabOrder = 5
          dmkEditCB = EdTedC_Cfg
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object PnIndiretos: TPanel
        Left = 2
        Top = 57
        Width = 1012
        Height = 42
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object Label13: TLabel
          Left = 12
          Top = 1
          Width = 97
          Height = 13
          Caption = 'Carteira de dep'#243'sito:'
          FocusControl = DBEdit5
        end
        object Label14: TLabel
          Left = 508
          Top = 1
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdit10
        end
        object DBEdit5: TDBEdit
          Left = 12
          Top = 17
          Width = 56
          Height = 21
          DataField = 'CartDep'
          DataSource = DsTedCCfgCab
          TabOrder = 0
        end
        object DBEdit6: TDBEdit
          Left = 68
          Top = 17
          Width = 428
          Height = 21
          DataField = 'NO_CARTDEP'
          DataSource = DsTedCCfgCab
          TabOrder = 1
        end
        object DBEdit10: TDBEdit
          Left = 508
          Top = 17
          Width = 56
          Height = 21
          DataField = 'CodCliInt'
          DataSource = DsTedCCfgCab
          TabOrder = 2
        end
        object DBEdit11: TDBEdit
          Left = 564
          Top = 17
          Width = 427
          Height = 21
          DataField = 'NO_EMPRESA'
          DataSource = DsTedCCfgCab
          TabOrder = 3
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1016
    Height = 568
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 0
      Top = 309
      Width = 1016
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      ExplicitLeft = 80
      ExplicitTop = 266
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 504
      Width = 1016
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 1
      end
      object Panel3: TPanel
        Left = 404
        Top = 15
        Width = 610
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 2
        object Label10: TLabel
          Left = 400
          Top = 4
          Width = 31
          Height = 13
          Caption = 'Linhas'
        end
        object BtGera: TBitBtn
          Tag = 410
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'Arquivo TED'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtGeraClick
        end
        object BtCheques: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cheques'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtChequesClick
        end
        object BtLot: TBitBtn
          Tag = 265
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtLotClick
        end
        object Edit1: TEdit
          Left = 400
          Top = 20
          Width = 69
          Height = 21
          TabOrder = 3
          Text = '0'
        end
        object Panel6: TPanel
          Left = 477
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 4
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 1016
      Height = 57
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel9: TPanel
        Left = 668
        Top = 0
        Width = 348
        Height = 57
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 148
          Height = 57
          Align = alLeft
          Caption = ' Cria'#231#227'o: '
          TabOrder = 0
          object Label18: TLabel
            Left = 8
            Top = 15
            Width = 26
            Height = 13
            Caption = 'Data:'
            FocusControl = DBEdit7
          end
          object Label19: TLabel
            Left = 84
            Top = 15
            Width = 26
            Height = 13
            Caption = 'Hora:'
            FocusControl = DBEdit8
          end
          object DBEdit7: TDBEdit
            Left = 8
            Top = 31
            Width = 72
            Height = 21
            DataField = 'MyDATAG'
            DataSource = DsTedCRemCab
            TabOrder = 0
          end
          object DBEdit8: TDBEdit
            Left = 84
            Top = 31
            Width = 56
            Height = 21
            DataField = 'HoraG'
            DataSource = DsTedCRemCab
            TabOrder = 1
          end
        end
        object GroupBox1: TGroupBox
          Left = 148
          Top = 0
          Width = 148
          Height = 57
          Align = alLeft
          Caption = ' '#218'ltimo envio: '
          TabOrder = 1
          object Label2: TLabel
            Left = 8
            Top = 15
            Width = 26
            Height = 13
            Caption = 'Data:'
            FocusControl = DBEdit1
          end
          object Label12: TLabel
            Left = 84
            Top = 15
            Width = 26
            Height = 13
            Caption = 'Hora:'
            FocusControl = DBEdit4
          end
          object DBEdit1: TDBEdit
            Left = 8
            Top = 31
            Width = 72
            Height = 21
            DataField = 'MyDATAS'
            DataSource = DsTedCRemCab
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 84
            Top = 31
            Width = 56
            Height = 21
            DataField = 'HoraS'
            DataSource = DsTedCRemCab
            TabOrder = 1
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 668
        Height = 57
        Align = alLeft
        Caption = ' Informa'#231#245'es do lote: '
        TabOrder = 1
        object Panel1: TPanel
          Left = 2
          Top = 15
          Width = 664
          Height = 40
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 12
            Top = 0
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            FocusControl = DBEdCodigo
          end
          object Label8: TLabel
            Left = 116
            Top = 0
            Width = 76
            Height = 13
            Caption = 'Layout utilizado:'
            FocusControl = DBEdit3
          end
          object Label21: TLabel
            Left = 592
            Top = 0
            Width = 24
            Height = 13
            Caption = 'Lote:'
            FocusControl = DBEdit9
          end
          object DBEdCodigo: TDBEdit
            Left = 12
            Top = 16
            Width = 100
            Height = 21
            Hint = 'N'#186' do banco'
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsTedCRemCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8281908
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
          end
          object DBEdit3: TDBEdit
            Left = 116
            Top = 16
            Width = 473
            Height = 21
            DataField = 'NO_TedCCfgCab'
            DataSource = DsTedCRemCab
            TabOrder = 1
          end
          object DBEdit9: TDBEdit
            Left = 592
            Top = 16
            Width = 64
            Height = 21
            DataField = 'CodUsu'
            DataSource = DsTedCRemCab
            TabOrder = 2
          end
        end
      end
    end
    object GBArqSalvo: TGroupBox
      Left = 0
      Top = 460
      Width = 1016
      Height = 44
      Align = alBottom
      Caption = ' Arquivo Salvo: '
      TabOrder = 2
      Visible = False
      object EdArqSalvo: TEdit
        Left = 8
        Top = 16
        Width = 997
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 312
      Width = 1016
      Height = 148
      ActivePage = TabSheet1
      Align = alBottom
      TabOrder = 3
      object TabSheet1: TTabSheet
        Caption = ' Texto do arquivo '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 1008
          Height = 120
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssBoth
          TabOrder = 0
          WordWrap = False
          OnChange = Memo1Change
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Registros gerados '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object TCRegistros: TTabControl
          Left = 0
          Top = 0
          Width = 1008
          Height = 120
          Align = alClient
          TabOrder = 0
          OnChange = TCRegistrosChange
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Cheque do item Selecionado'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 1008
          Height = 120
          Align = alClient
          DataSource = DsCheque
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Praca'
              Title.Caption = 'Pra'#231'a'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag'#234'nc.'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ContaCorrente'
              Title.Caption = 'Conta corrente'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Title.Caption = 'Cheque'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJCPF'
              Title.Caption = 'CNPJ / CPF'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 480
              Visible = True
            end>
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 57
      Width = 1016
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object GroupBox5: TGroupBox
        Left = 0
        Top = 0
        Width = 584
        Height = 92
        Align = alLeft
        Caption = ' Informa'#231#245'es da empresa: '
        TabOrder = 0
        object Label15: TLabel
          Left = 8
          Top = 53
          Width = 97
          Height = 13
          Caption = 'Carteira de dep'#243'sito:'
          FocusControl = DBEdit12
        end
        object Label16: TLabel
          Left = 8
          Top = 29
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdit14
        end
        object DBEdit12: TDBEdit
          Left = 108
          Top = 49
          Width = 56
          Height = 21
          DataField = 'CartDep'
          DataSource = DsTedCRemCab
          TabOrder = 0
        end
        object DBEdit13: TDBEdit
          Left = 164
          Top = 49
          Width = 409
          Height = 21
          DataField = 'NO_CARTDEP'
          DataSource = DsTedCRemCab
          TabOrder = 1
        end
        object DBEdit14: TDBEdit
          Left = 60
          Top = 25
          Width = 56
          Height = 21
          DataField = 'Empresa'
          DataSource = DsTedCRemCab
          TabOrder = 2
        end
        object DBEdit15: TDBEdit
          Left = 116
          Top = 25
          Width = 457
          Height = 21
          DataField = 'NO_EMPRESA'
          DataSource = DsTedCRemCab
          TabOrder = 3
        end
      end
      object GroupBox6: TGroupBox
        Left = 584
        Top = 0
        Width = 432
        Height = 92
        Align = alClient
        Caption = ' Quantidade e valor total dos cheques:'
        TabOrder = 1
        object Panel12: TPanel
          Left = 2
          Top = 15
          Width = 67
          Height = 75
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label4: TLabel
            Left = 4
            Top = 44
            Width = 50
            Height = 13
            Caption = 'Valor total:'
          end
          object Label5: TLabel
            Left = 4
            Top = 24
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
        end
        object GroupBox7: TGroupBox
          Left = 69
          Top = 15
          Width = 155
          Height = 75
          Align = alLeft
          Caption = ' Em carteira: '
          TabOrder = 1
          object DBEdit2: TDBEdit
            Left = 8
            Top = 20
            Width = 134
            Height = 21
            DataField = 'CartQtd'
            DataSource = DsTedCRemCab
            TabOrder = 0
          end
          object DBEdit16: TDBEdit
            Left = 8
            Top = 44
            Width = 134
            Height = 21
            DataField = 'CartVal'
            DataSource = DsTedCRemCab
            TabOrder = 1
          end
        end
        object GroupBox8: TGroupBox
          Left = 224
          Top = 15
          Width = 155
          Height = 75
          Align = alLeft
          Caption = ' Entrada: '
          TabOrder = 2
          object DBEdit17: TDBEdit
            Left = 8
            Top = 20
            Width = 134
            Height = 21
            DataField = 'EntrQtd'
            DataSource = DsTedCRemCab
            TabOrder = 0
          end
          object DBEdit18: TDBEdit
            Left = 8
            Top = 44
            Width = 134
            Height = 21
            DataField = 'EntrVal'
            DataSource = DsTedCRemCab
            TabOrder = 1
          end
        end
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 149
      Width = 1016
      Height = 132
      ActivePage = TabSheet4
      Align = alTop
      TabOrder = 5
      object TabSheet4: TTabSheet
        Caption = 'Itens'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 1008
          Height = 104
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'CMC7'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BomPara'
              Title.Caption = 'Bom para'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LctCtrl'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LctSub'
              Title.Caption = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LctFatID'
              Title.Caption = 'F.ID'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LctFatNum'
              Title.Caption = 'F.Num'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LctFatParc'
              Title.Caption = 'F.Parc ID'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OcorrBco'
              Title.Caption = 'Oc.Bco'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MyOcorr'
              Title.Caption = 'My.Oc.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OCORR_TXT'
              Title.Caption = 'Descri'#231#227'o da ocorr'#234'ncia'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsTedCRemIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CMC7'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BomPara'
              Title.Caption = 'Bom para'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LctCtrl'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LctSub'
              Title.Caption = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LctFatID'
              Title.Caption = 'F.ID'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LctFatNum'
              Title.Caption = 'F.Num'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LctFatParc'
              Title.Caption = 'F.Parc ID'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OcorrBco'
              Title.Caption = 'Oc.Bco'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MyOcorr'
              Title.Caption = 'My.Oc.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OCORR_TXT'
              Title.Caption = 'Descri'#231#227'o da ocorr'#234'ncia'
              Visible = True
            end>
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Financeiros'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 1008
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object BtExclui: TBitBtn
            Tag = 12
            Left = 96
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Exclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtExcluiClick
          end
          object BtInclui: TBitBtn
            Tag = 10
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Inclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtIncluiClick
          end
          object Panel13: TPanel
            Left = 899
            Top = 0
            Width = 109
            Height = 46
            Align = alRight
            Alignment = taRightJustify
            BevelOuter = bvNone
            TabOrder = 2
          end
        end
        object DBGDespProf: TDBGrid
          Left = 0
          Top = 46
          Width = 1008
          Height = 58
          Align = alClient
          DataSource = DsLcts
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Caption = 'N'#186
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Alignment = taRightJustify
              Title.Caption = 'Cr'#233'dito'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Alignment = taRightJustify
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 175
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECONTA'
              Title.Caption = 'Conta (Plano de contas)'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 968
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 752
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 277
        Height = 32
        Caption = 'Remessa TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 277
        Height = 32
        Caption = 'Remessa TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 277
        Height = 32
        Caption = 'Remessa TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1016
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel10: TPanel
      Left = 2
      Top = 15
      Width = 1012
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsTedCRemCab: TDataSource
    DataSet = QrTedCRemCab
    Left = 560
    Top = 68
  end
  object QrTedCRemCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTedCRemCabBeforeOpen
    AfterOpen = QrTedCRemCabAfterOpen
    BeforeClose = QrTedCRemCabBeforeClose
    AfterScroll = QrTedCRemCabAfterScroll
    OnCalcFields = QrTedCRemCabCalcFields
    SQL.Strings = (
      'SELECT tcc.Nome NO_TedCCfgCab, tcc.Banco, '
      'tcc.TamReg, tcc.PathRem,'
      'IF(ent.Tipo=0, RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOC_EMPRESA,'
      'ent.Tipo TIPO_ENT, car.Nome NO_CARTDEP, eci.CodEnti, trc.*'
      'FROM tedcremcab trc'
      'LEFT JOIN tedccfgcab tcc ON tcc.Codigo=trc.TedC_Cfg'
      'LEFT JOIN carteiras car ON car.Codigo=trc.CartDep'
      'LEFT JOIN enticliint eci ON eci.CodCliInt=trc.Empresa'
      'LEFT JOIN entidades ent ON ent.Codigo=eci.CodEnti'
      ''
      ''
      '')
    Left = 532
    Top = 68
    object QrTedCRemCabMyDATAG: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MyDATAG'
      Size = 10
      Calculated = True
    end
    object QrTedCRemCabMyDATAS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MyDATAS'
      Size = 10
      Calculated = True
    end
    object QrTedCRemCabNO_TedCCfgCab: TWideStringField
      FieldName = 'NO_TedCCfgCab'
      Size = 50
    end
    object QrTedCRemCabBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrTedCRemCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrTedCRemCabNO_CARTDEP: TWideStringField
      FieldName = 'NO_CARTDEP'
      Size = 100
    end
    object QrTedCRemCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTedCRemCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrTedCRemCabDataG: TDateField
      FieldName = 'DataG'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTedCRemCabHoraG: TTimeField
      FieldName = 'HoraG'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrTedCRemCabTedC_Cfg: TIntegerField
      FieldName = 'TedC_Cfg'
    end
    object QrTedCRemCabDataS: TDateField
      FieldName = 'DataS'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTedCRemCabHoraS: TTimeField
      FieldName = 'HoraS'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrTedCRemCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTedCRemCabCartDep: TIntegerField
      FieldName = 'CartDep'
    end
    object QrTedCRemCabSeqArq: TIntegerField
      FieldName = 'SeqArq'
    end
    object QrTedCRemCabTamReg: TIntegerField
      FieldName = 'TamReg'
    end
    object QrTedCRemCabCodEnti: TIntegerField
      FieldName = 'CodEnti'
      Required = True
    end
    object QrTedCRemCabDOC_EMPRESA: TWideStringField
      FieldName = 'DOC_EMPRESA'
      Size = 18
    end
    object QrTedCRemCabTIPO_ENT: TSmallintField
      FieldName = 'TIPO_ENT'
    end
    object QrTedCRemCabEncerrado: TSmallintField
      FieldName = 'Encerrado'
    end
    object QrTedCRemCabPathRem: TWideStringField
      FieldName = 'PathRem'
      Size = 255
    end
    object QrTedCRemCabCedente: TWideStringField
      FieldName = 'Cedente'
      Size = 30
    end
    object QrTedCRemCabCartQtd: TIntegerField
      FieldName = 'CartQtd'
      DisplayFormat = '#,###,###,###,##0'
    end
    object QrTedCRemCabCartVal: TFloatField
      FieldName = 'CartVal'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrTedCRemCabEntrQtd: TIntegerField
      FieldName = 'EntrQtd'
      DisplayFormat = '#,###,###,###,##0'
    end
    object QrTedCRemCabEntrVal: TFloatField
      FieldName = 'EntrVal'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object QrTedCRemIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTedCRemItsBeforeClose
    AfterScroll = QrTedCRemItsAfterScroll
    OnCalcFields = QrTedCRemItsCalcFields
    SQL.Strings = (
      
        'SELECT tri.*, lct.ContaCorrente, lct.Banco, lct.Agencia, lct.Doc' +
        'umento'
      'FROM tedcremits tri'
      'LEFT JOIN lct0001a lct ON lct.Controle = tri.LctCtrl'
      'WHERE tri.Codigo=:P0')
    Left = 592
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTedCRemItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'tedcremits.Codigo'
    end
    object QrTedCRemItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'tedcremits.Controle'
    end
    object QrTedCRemItsLctCtrl: TIntegerField
      FieldName = 'LctCtrl'
      Origin = 'tedcremits.LctCtrl'
    end
    object QrTedCRemItsLctSub: TIntegerField
      FieldName = 'LctSub'
      Origin = 'tedcremits.LctSub'
    end
    object QrTedCRemItsLctFatParc: TIntegerField
      FieldName = 'LctFatParc'
      Origin = 'tedcremits.LctFatParc'
    end
    object QrTedCRemItsBomPara: TDateField
      FieldName = 'BomPara'
      Origin = 'tedcremits.BomPara'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTedCRemItsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'tedcremits.Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTedCRemItsCMC7: TWideStringField
      FieldName = 'CMC7'
      Origin = 'tedcremits.CMC7'
      Size = 40
    end
    object QrTedCRemItsLctSitAnt: TIntegerField
      FieldName = 'LctSitAnt'
      Origin = 'tedcremits.LctSitAnt'
    end
    object QrTedCRemItsLctCtrAnt: TIntegerField
      FieldName = 'LctCtrAnt'
      Origin = 'tedcremits.LctCtrAnt'
    end
    object QrTedCRemItsOCORR_TXT: TWideStringField
      DisplayWidth = 255
      FieldKind = fkCalculated
      FieldName = 'OCORR_TXT'
      Size = 255
      Calculated = True
    end
    object QrTedCRemItsOcorrBco: TIntegerField
      FieldName = 'OcorrBco'
      Origin = 'tedcremits.OcorrBco'
    end
    object QrTedCRemItsMyOcorr: TIntegerField
      FieldName = 'MyOcorr'
      Origin = 'tedcremits.MyOcorr'
    end
    object QrTedCRemItsLctFatID: TIntegerField
      FieldName = 'LctFatID'
      Origin = 'tedcremits.LctFatID'
    end
    object QrTedCRemItsLctFatNum: TLargeintField
      FieldName = 'LctFatNum'
      Origin = 'tedcremits.LctFatNum'
    end
    object QrTedCRemItsLoReCaPrev: TIntegerField
      FieldName = 'LoReCaPrev'
      Origin = 'tedcremits.LoReCaPrev'
    end
    object QrTedCRemItsLoReItPrev: TIntegerField
      FieldName = 'LoReItPrev'
      Origin = 'tedcremits.LoReItPrev'
    end
    object QrTedCRemItsLoReCaProc: TIntegerField
      FieldName = 'LoReCaProc'
      Origin = 'tedcremits.LoReCaProc'
    end
    object QrTedCRemItsLoReItProc: TIntegerField
      FieldName = 'LoReItProc'
      Origin = 'tedcremits.LoReItProc'
    end
    object QrTedCRemItsLoReMyOcor: TIntegerField
      FieldName = 'LoReMyOcor'
      Origin = 'tedcremits.LoReMyOcor'
    end
    object QrTedCRemItsContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrTedCRemItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrTedCRemItsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrTedCRemItsDocumento: TFloatField
      FieldName = 'Documento'
    end
  end
  object DsTedCRemIts: TDataSource
    DataSet = QrTedCRemIts
    Left = 620
    Top = 68
  end
  object PMLot: TPopupMenu
    OnPopup = PMLotPopup
    Left = 456
    Top = 588
    object Crianovolote1: TMenuItem
      Caption = '&Cria novo lote'
      OnClick = Crianovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      OnClick = Excluiloteatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Reabrelote1: TMenuItem
      Caption = 'Reabre lote'
      Enabled = False
      OnClick = Reabrelote1Click
    end
  end
  object PMCheques: TPopupMenu
    OnPopup = PMChequesPopup
    Left = 576
    Top = 588
    object Inclui1: TMenuItem
      Caption = '&Inclui no lote'
      OnClick = Inclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Retira1: TMenuItem
      Caption = '&Remove do lote'
      Enabled = False
      OnClick = Retira1Click
    end
  end
  object QrTedCCfgCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tcc.Nome, tcc.Codigo, tcc.CartDep, '
      'tcc.Empresa, eci.CodCliInt, car.Nome NO_CARTDEP,'
      'IF(ent.Tipo=0, RazaoSocial, ent.Nome) NO_EMPRESA'
      'FROM tedccfgcab tcc'
      'LEFT JOIN carteiras car ON car.Codigo=tcc.CartDep'
      'LEFT JOIN enticliint eci ON eci.CodCliInt=tcc.Empresa'
      'LEFT JOIN entidades ent ON ent.Codigo=eci.CodEnti'
      'ORDER BY tcc.Nome'
      '')
    Left = 372
    Top = 232
    object QrTedCCfgCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrTedCCfgCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTedCCfgCabCartDep: TIntegerField
      FieldName = 'CartDep'
    end
    object QrTedCCfgCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrTedCCfgCabCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
      Required = True
    end
    object QrTedCCfgCabNO_CARTDEP: TWideStringField
      FieldName = 'NO_CARTDEP'
      Size = 100
    end
    object QrTedCCfgCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
  end
  object DsTedCCfgCab: TDataSource
    DataSet = QrTedCCfgCab
    Left = 400
    Top = 232
  end
  object QrLot: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(CodUsu) CodUsu'
      'FROM tedcremcab')
    Left = 396
    Top = 56
    object QrLotCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
  end
  object QrEmit: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmitCalcFields
    SQL.Strings = (
      'SELECT CNPJCPF, Emitente'
      'FROM lct0001a'
      'WHERE FatID=301'
      'AND Controle=1'
      'AND Sub=0'
      'AND FatParcela=2')
    Left = 428
    Top = 56
    object QrEmitCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmitEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmitTIPO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'TIPO'
      Calculated = True
    end
    object QrEmitDOCUM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCUM'
      Size = 14
      Calculated = True
    end
  end
  object QrCheque: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Banco, Agencia, ContaCorrente, '
      'Documento, Praca, Credito, CNPJCPF, Emitente'
      'FROM lct0001a'
      'WHERE Controle=1'
      'AND Sub=0'
      'AND FatID=301'
      'AND FatNum=0'
      'AND FatParcela=0')
    Left = 660
    Top = 68
    object QrChequeBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrChequeAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrChequeContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrChequeDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrChequePraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrChequeCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrChequeCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrChequeEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
  end
  object DsCheque: TDataSource
    DataSet = QrCheque
    Left = 688
    Top = 68
  end
  object frxTED_CHQUE_001_01: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41191.572938252300000000
    ReportOptions.LastChange = 41191.627261238430000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 720
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsTedCRemCab
        DataSetName = 'frxDsTedCRemCab'
      end
      item
        DataSet = frxDsTedCRemIts
        DataSetName = 'frxDsTedCRemIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        Height = 45.354330708661420000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Remessa TED Cheque')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 37.795300000000000000
        Top = 124.724490000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          Width = 30.236178980000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lote:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 120.944960000000000000
          Width = 325.039370078740200000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTedCRemCab."NO_EMPRESA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 445.984540000000000000
          Width = 113.385802360000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora da cria'#231#227'o:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 30.236240000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTedCRemCab."CodUsu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 71.811070000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 555.590910000000000000
          Width = 124.724392360000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTedCRemCab."MyDATAG"] '#224's [frxDsTedCRemCab."HoraG"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 143.622140000000000000
          Top = 18.897650000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cheque')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 18.897650000000000000
          Width = 30.236220470000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 30.236240000000000000
          Top = 18.897650000000000000
          Width = 41.574805590000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ag'#234'nc.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 71.811070000000000000
          Top = 18.897650000000000000
          Width = 71.811023620000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 188.976500000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 257.008040000000000000
          Top = 18.897650000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CMC7')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 491.338900000000000000
          Top = 18.897650000000000000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o da ocorr'#234'ncia')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        Top = 226.771800000000000000
        Width = 680.315400000000000000
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 185.196970000000000000
        Width = 680.315400000000000000
        DataSet = frxDsTedCRemIts
        DataSetName = 'frxDsTedCRemIts'
        RowCount = 0
        object Memo14: TfrxMemoView
          Left = 143.622140000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsTedCRemIts
          DataSetName = 'frxDsTedCRemIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTedCRemIts."Documento"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Width = 30.236220470000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsTedCRemIts
          DataSetName = 'frxDsTedCRemIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTedCRemIts."Banco"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 30.236240000000000000
          Width = 41.574805590000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsTedCRemIts
          DataSetName = 'frxDsTedCRemIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTedCRemIts."Agencia"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 71.811070000000000000
          Width = 71.811023620000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsTedCRemIts
          DataSetName = 'frxDsTedCRemIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTedCRemIts."ContaCorrente"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 188.976500000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsTedCRemIts
          DataSetName = 'frxDsTedCRemIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTedCRemIts."Valor"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 257.008040000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsTedCRemIts
          DataSetName = 'frxDsTedCRemIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTedCRemIts."CMC7"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 491.338900000000000000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsTedCRemIts
          DataSetName = 'frxDsTedCRemIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTedCRemIts."OCORR_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxDsTedCRemCab: TfrxDBDataset
    UserName = 'frxDsTedCRemCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'MyDATAG=MyDATAG'
      'MyDATAS=MyDATAS'
      'NO_TedCCfgCab=NO_TedCCfgCab'
      'Banco=Banco'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_CARTDEP=NO_CARTDEP'
      'Codigo=Codigo'
      'CodUsu=CodUsu'
      'DataG=DataG'
      'HoraG=HoraG'
      'TedC_Cfg=TedC_Cfg'
      'DataS=DataS'
      'HoraS=HoraS'
      'Empresa=Empresa'
      'CartDep=CartDep'
      'SeqArq=SeqArq'
      'TamReg=TamReg'
      'CodEnti=CodEnti'
      'DOC_EMPRESA=DOC_EMPRESA'
      'TIPO_ENT=TIPO_ENT'
      'Encerrado=Encerrado'
      'PathRem=PathRem'
      'Cedente=Cedente'
      'CartQtd=CartQtd'
      'CartVal=CartVal'
      'EntrQtd=EntrQtd'
      'EntrVal=EntrVal')
    DataSet = QrTedCRemCab
    BCDToCurrency = False
    Left = 748
    Top = 8
  end
  object frxDsTedCRemIts: TfrxDBDataset
    UserName = 'frxDsTedCRemIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'LctCtrl=LctCtrl'
      'LctSub=LctSub'
      'LctFatParc=LctFatParc'
      'BomPara=BomPara'
      'Valor=Valor'
      'CMC7=CMC7'
      'LctSitAnt=LctSitAnt'
      'LctCtrAnt=LctCtrAnt'
      'OCORR_TXT=OCORR_TXT'
      'OcorrBco=OcorrBco'
      'MyOcorr=MyOcorr'
      'LctFatID=LctFatID'
      'LctFatNum=LctFatNum'
      'LoReCaPrev=LoReCaPrev'
      'LoReItPrev=LoReItPrev'
      'LoReCaProc=LoReCaProc'
      'LoReItProc=LoReItProc'
      'LoReMyOcor=LoReMyOcor'
      'ContaCorrente=ContaCorrente'
      'Banco=Banco'
      'Agencia=Agencia'
      'Documento=Documento')
    DataSet = QrTedCRemIts
    BCDToCurrency = False
    Left = 777
    Top = 8
  end
  object QrLcts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLctsCalcFields
    SQL.Strings = (
      '')
    Left = 280
    Top = 344
    object QrLctsNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrLctsNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrLctsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctsCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLctsVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctsData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrLctsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctsSub: TIntegerField
      FieldName = 'Sub'
    end
    object QrLctsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctsCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctsSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrLctsID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrLctsCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctsDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsLcts: TDataSource
    DataSet = QrLcts
    Left = 308
    Top = 344
  end
end
