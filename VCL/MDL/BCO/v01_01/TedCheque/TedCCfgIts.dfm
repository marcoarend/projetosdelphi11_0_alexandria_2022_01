object FmTedCCfgIts: TFmTedCCfgIts
  Left = 339
  Top = 185
  Caption = 'TED-CHQUE-002 :: Configura'#231#227'o de Campo TED Cheque'
  ClientHeight = 391
  ClientWidth = 644
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 644
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 596
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 548
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 459
        Height = 32
        Caption = 'Configura'#231#227'o de Campo TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 459
        Height = 32
        Caption = 'Configura'#231#227'o de Campo TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 459
        Height = 32
        Caption = 'Configura'#231#227'o de Campo TED Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 644
    Height = 229
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 337
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 644
      Height = 229
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 337
      object GroupBox1: TGroupBox
        Left = 0
        Top = 48
        Width = 644
        Height = 125
        Align = alTop
        Caption = ' Explica'#231#227'o do campo: '
        Enabled = False
        TabOrder = 1
        ExplicitLeft = 8
        ExplicitTop = 144
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 640
          Height = 108
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object EdDescri01: TdmkEdit
            Left = 72
            Top = 4
            Width = 489
            Height = 21
            Color = clBtnFace
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdDescri02: TdmkEdit
            Left = 72
            Top = 24
            Width = 489
            Height = 21
            Color = clBtnFace
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdDescri03: TdmkEdit
            Left = 72
            Top = 44
            Width = 489
            Height = 21
            Color = clBtnFace
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdDescri04: TdmkEdit
            Left = 72
            Top = 64
            Width = 489
            Height = 21
            Color = clBtnFace
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdDescri05: TdmkEdit
            Left = 72
            Top = 84
            Width = 489
            Height = 21
            Color = clBtnFace
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 644
        Height = 48
        Align = alTop
        Caption = ' Campo: '
        Enabled = False
        TabOrder = 0
        ExplicitTop = 125
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 640
          Height = 31
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 88
          object EdCampo: TdmkEdit
            Left = 168
            Top = 4
            Width = 229
            Height = 21
            Color = clBtnFace
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 173
        Width = 644
        Height = 48
        Align = alTop
        Caption = ' Valor (texto) do campo no arquivo remessa: '
        TabOrder = 2
        ExplicitLeft = 20
        ExplicitTop = 281
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 640
          Height = 31
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object EdDefaultRem: TdmkEdit
            Left = 8
            Top = 4
            Width = 621
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 277
    Width = 644
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 385
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 640
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 321
    Width = 644
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 429
    object PnSaiDesis: TPanel
      Left = 498
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 496
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object QrEmiSac: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Nome, CPF, "Emitente" Tipo FROM emitcpf'
      'WHERE CPF = :P0'
      'UNION'
      'SELECT DISTINCT Nome, CNPJ CPF, "Sacado" Tipo FROM sacados'
      'WHERE CNPJ= :P1'
      ''
      'ORDER BY Nome')
    Left = 236
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmiSacNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrEmiSacCPF: TWideStringField
      FieldName = 'CPF'
      Required = True
      Size = 15
    end
    object QrEmiSacTipo: TWideStringField
      FieldName = 'Tipo'
      Required = True
      Size = 8
    end
  end
  object DsEmiSac: TDataSource
    DataSet = QrEmiSac
    Left = 264
    Top = 64
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CNPJ_CPF'
      'FROM cadastro_com_itens_its'
      'WHERE CNPJ_CPF=:P0'
      'AND Codigo=:P1')
    Left = 297
    Top = 65
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 15
    end
  end
end
