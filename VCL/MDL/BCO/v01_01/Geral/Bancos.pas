unit Bancos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  OleCtrls, SHDocVw, Grids, DBGrids, Menus, IniFiles, ComObj, Variants,
  dmkPermissoes, dmkGeral, dmkEdit, dmkDBLookupComboBox, dmkEditCB,
  dmkCheckGroup, dmkLabel, dmkImage, UnDmkProcFunc, UnDmkEnums, UnBancos;

type
  TFmBancos = class(TForm)
    PainelDados: TPanel;
    DsBancos: TDataSource;
    QrBancos: TmySQLQuery;
    QrBancosLk: TIntegerField;
    QrBancosDataCad: TDateField;
    QrBancosDataAlt: TDateField;
    QrBancosUserCad: TIntegerField;
    QrBancosUserAlt: TIntegerField;
    QrBancosCodigo: TSmallintField;
    PainelEdita: TPanel;
    QrBancosSite: TWideStringField;
    OpenDialog1: TOpenDialog;
    QrBancosNome: TWideStringField;
    QrBancosDVCC: TSmallintField;
    QrBancosDVB: TWideStringField;
    PMOcorrencia: TPopupMenu;
    Novaocorrncia1: TMenuItem;
    Alteraocorrnciaatual1: TMenuItem;
    Excluiocorrnciaatual1: TMenuItem;
    QrEntidade: TmySQLQuery;
    QrEntidadeCodigo: TIntegerField;
    QrEntidadeNOMEENTI: TWideStringField;
    DsEntidade: TDataSource;
    QrBancosEntidade: TIntegerField;
    QrBancosNOMEENTIDADE: TWideStringField;
    PMBanco: TPopupMenu;
    Novobanco1: TMenuItem;
    Alterabancoatual1: TMenuItem;
    Excluibancoatual1: TMenuItem;
    N1: TMenuItem;
    Importadelista1: TMenuItem;
    QrBanCNAB_R: TmySQLQuery;
    DsBanCNAB_R: TDataSource;
    PMComposicao: TPopupMenu;
    PainelRegistro: TPanel;
    Registros1: TMenuItem;
    Campos1: TMenuItem;
    Incluinovocampo1: TMenuItem;
    Alteracampoatual1: TMenuItem;
    ExcluicampoAtual1: TMenuItem;
    Incluinovoregistro1: TMenuItem;
    Alteraregistroatual1: TMenuItem;
    Excluiregistroatual1: TMenuItem;
    QrReg: TmySQLQuery;
    DsReg: TDataSource;
    QrRegCodigo: TIntegerField;
    QrRegNome: TWideStringField;
    QrBanCNAB_RNOMEREGISTRO: TWideStringField;
    QrBanCNAB_RCodigo: TIntegerField;
    QrBanCNAB_RRegistro: TIntegerField;
    QrBanCNAB_RLk: TIntegerField;
    QrBanCNAB_RDataCad: TDateField;
    QrBanCNAB_RDataAlt: TDateField;
    QrBanCNAB_RUserCad: TIntegerField;
    QrBanCNAB_RUserAlt: TIntegerField;
    QrBanCNAB_RSeq1Ini: TIntegerField;
    QrBanCNAB_RSeq1Tam: TIntegerField;
    QrBanCNAB_RSeq1Val: TWideStringField;
    QrBanCNAB_RSeq2Ini: TIntegerField;
    QrBanCNAB_RSeq2Tam: TIntegerField;
    QrBanCNAB_RSeq2Val: TWideStringField;
    QrCNAB_CaD: TmySQLQuery;
    DsCNAB_CaD: TDataSource;
    QrCNAB_CaDNome: TWideStringField;
    QrCNAB_CaDSignific: TWideStringField;
    QrCNAB_CaDPadrIni: TIntegerField;
    QrCNAB_CaDPadrTam: TIntegerField;
    QrCNAB_CaDAlfaNum: TSmallintField;
    QrCNAB_CaDFmtInv: TIntegerField;
    QrCNAB_CaDPadrVal: TWideStringField;
    QrCNAB_CaDAjuda: TWideStringField;
    QrCNAB_CaDLk: TIntegerField;
    QrCNAB_CaDDataCad: TDateField;
    QrCNAB_CaDDataAlt: TDateField;
    QrCNAB_CaDUserCad: TIntegerField;
    QrCNAB_CaDUserAlt: TIntegerField;
    QrCNAB_CaDRegistro: TIntegerField;
    QrCNAB_CaDBcoOrig: TIntegerField;
    QrCNAB_CaDNOME_T: TWideStringField;
    QrCNAB_CaDNOME_I: TWideStringField;
    OpenDialog2: TOpenDialog;
    QrCNAB_CaDEnvio: TSmallintField;
    QrCNAB_CaDID: TWideStringField;
    QrBanCNAB_REnvio: TSmallintField;
    QrBanCNAB_RTamLinha: TIntegerField;
    QrGrupos: TmySQLQuery;
    QrGruposCodigo: TIntegerField;
    QrGruposNome: TWideStringField;
    DsGrupos: TDataSource;
    QrBancoz: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsBancoz: TDataSource;
    QrCNAB_Fld: TmySQLQuery;
    QrCNAB_FldCodigo: TIntegerField;
    QrCNAB_FldNome: TWideStringField;
    DsCNAB_Fld: TDataSource;
    QrCNAB_CaDPOSICOES: TIntegerField;
    QrCNAB_CaDCampo: TIntegerField;
    QrCNAB_CaDT240: TIntegerField;
    QrCNAB_CaDT400: TIntegerField;
    QrCNAB_CaDSecoVal: TWideStringField;
    QrCNAB_CaDNOMECAMPO: TWideStringField;
    QrCNAB_CaDFormato: TWideStringField;
    QrCNAB_CaDCasas: TSmallintField;
    QrBancosID_400i: TIntegerField;
    QrBancosID_400t: TIntegerField;
    QrBancosXlsLinha: TIntegerField;
    QrBancosXlsData: TWideStringField;
    QrBancosXlsHist: TWideStringField;
    QrBancosXlsDocu: TWideStringField;
    QrBancosXlsCred: TWideStringField;
    QrBancosXlsDebi: TWideStringField;
    QrBancosXlsTCDB: TSmallintField;
    QrBancosXlsComp: TWideStringField;
    QrBancosXlsCPMF: TWideStringField;
    QrBancosXlsSldo: TWideStringField;
    QrBancosXlsHiDo: TWideStringField;
    QrBancosXlsCrDb: TWideStringField;
    QrBancosXlsDouC: TWideStringField;
    OpenDialog3: TOpenDialog;
    OpenDialog4: TOpenDialog;
    QrPesq: TmySQLQuery;
    QrBancosDescriCNR: TWideStringField;
    Timer1: TTimer;
    OpenDialog5: TOpenDialog;
    dmkPermissoes1: TdmkPermissoes;
    PainelCampo: TPanel;
    QrBancosCNABDirMul: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel24: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBEdita: TGroupBox;
    Pn0001: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label61: TLabel;
    Label64: TLabel;
    SbCNABDirMul: TSpeedButton;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdSite: TdmkEdit;
    EdDVCC: TdmkEdit;
    EdDVB: TdmkEdit;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Label41: TLabel;
    Label16: TLabel;
    EdID_400i: TdmkEdit;
    EdID_400t: TdmkEdit;
    GroupBox7: TGroupBox;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    EdXlsLinha: TdmkEdit;
    EdXLSData: TdmkEdit;
    EdXLSHist: TdmkEdit;
    EdXLSDocu: TdmkEdit;
    EdXLSCred: TdmkEdit;
    EdXLSDebi: TdmkEdit;
    EdXLSDouC: TdmkEdit;
    EdXLSSldo: TdmkEdit;
    EdXLSHiDo: TdmkEdit;
    EdXLSCrDb: TdmkEdit;
    RGXLSTCDB: TRadioGroup;
    EdXLSComp: TdmkEdit;
    EdXLSCPMF: TdmkEdit;
    EdDescriCNR: TdmkEdit;
    EdCNABDirMul: TdmkEdit;
    GroupBox8: TGroupBox;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    EdCChLinha: TdmkEdit;
    EdCChDtaCust: TdmkEdit;
    EdCChNosNum: TdmkEdit;
    EdCChBanco: TdmkEdit;
    EdCChAgencia: TdmkEdit;
    EdCChConta: TdmkEdit;
    EdCChCheque: TdmkEdit;
    EdCChValor: TdmkEdit;
    EdCChCNPJCPF: TdmkEdit;
    EdCChCodSit: TdmkEdit;
    EdCChDtaEven: TdmkEdit;
    EdCChBordero: TdmkEdit;
    EdCChTxtDep: TdmkEdit;
    EdCChTxtDev: TdmkEdit;
    GroupBox10: TGroupBox;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel7: TPanel;
    BtDesiste: TBitBtn;
    GBCampo: TGroupBox;
    GroupBox11: TGroupBox;
    Panel9: TPanel;
    BitBtn3: TBitBtn;
    Panel10: TPanel;
    BitBtn4: TBitBtn;
    Panel17: TPanel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label34: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label60: TLabel;
    EdGrupo: TdmkEditCB;
    CBGrupo: TdmkDBLookupComboBox;
    EdSignific: TdmkEdit;
    EdPadrIni: TdmkEdit;
    EdPadrTam: TdmkEdit;
    EdPadrVal: TdmkEdit;
    EdNomeFld: TdmkEdit;
    RGAlfaNum: TRadioGroup;
    EdBcoOrig: TdmkEditCB;
    CBBcoOrig: TdmkDBLookupComboBox;
    RGEnvioFld: TRadioGroup;
    EdID: TdmkEdit;
    EdCampo: TdmkEditCB;
    CBCampo: TdmkDBLookupComboBox;
    RGFmtInv: TRadioGroup;
    CGPosicoes: TdmkCheckGroup;
    CkContinuar: TCheckBox;
    EdSecoVal: TdmkEdit;
    EdFormato: TdmkEdit;
    EdAjuda: TdmkEdit;
    EdCasas: TdmkEdit;
    MeAnexos: TMemo;
    GBRegistro: TGroupBox;
    GroupBox12: TGroupBox;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    Panel8: TPanel;
    BitBtn2: TBitBtn;
    Panel4: TPanel;
    Label14: TLabel;
    DBEdit02: TDBEdit;
    DBEdit03: TDBEdit;
    Panel1: TPanel;
    Label20: TLabel;
    Label26: TLabel;
    EdReg: TdmkEditCB;
    CBReg: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Label15: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    EdSeq1Ini: TdmkEdit;
    EdSeq1Tam: TdmkEdit;
    EdSeq1Val: TdmkEdit;
    GroupBox2: TGroupBox;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    EdSeq2Ini: TdmkEdit;
    EdSeq2Tam: TdmkEdit;
    EdSeq2Val: TdmkEdit;
    RGEnvio: TRadioGroup;
    EdTamLinha: TdmkEdit;
    GBDados: TGroupBox;
    GroupBox13: TGroupBox;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtCNAB: TBitBtn;
    BtBanco: TBitBtn;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label12: TLabel;
    Label17: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit01: TDBEdit;
    BtNavegar: TBitBtn;
    Progress: TProgressBar;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    Label18: TLabel;
    Label42: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PnNavega: TPanel;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel12: TPanel;
    DBGrid4: TDBGrid;
    Panel14: TPanel;
    DBGrid2: TDBGrid;
    Panel15: TPanel;
    Panel16: TPanel;
    Label25: TLabel;
    EdPosicoes: TdmkEdit;
    RGEnvi2: TRadioGroup;
    TabSheet4: TTabSheet;
    Panel13: TPanel;
    TabSheet5: TTabSheet;
    Panel11: TPanel;
    Panel18: TPanel;
    Label55: TLabel;
    SpeedButton5: TSpeedButton;
    Edit1: TEdit;
    Editor: TMemo;
    Panel19: TPanel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Panel20: TPanel;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit6: TEdit;
    StatusBar: TStatusBar;
    TabSheet6: TTabSheet;
    Panel21: TPanel;
    Panel22: TPanel;
    RGImporta: TRadioGroup;
    BtImportar: TBitBtn;
    PageControl2: TPageControl;
    TabSheet9: TTabSheet;
    Panel23: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    LaAviso: TLabel;
    EdArqBcoXLS: TdmkEdit;
    PB1: TProgressBar;
    BitBtn5: TBitBtn;
    BtCarrega: TBitBtn;
    BtAbre: TBitBtn;
    Grade1: TStringGrid;
    TabSheet10: TTabSheet;
    TabSheet7: TTabSheet;
    MeImporta: TMemo;
    MeInfo: TMemo;
    MeRegistros: TMemo;
    TabSheet8: TTabSheet;
    Grade: TStringGrid;
    QrBancosCChLinha: TIntegerField;
    QrBancosCChDtaCust: TWideStringField;
    QrBancosCChNosNum: TWideStringField;
    QrBancosCChBanco: TWideStringField;
    QrBancosCChAgencia: TWideStringField;
    QrBancosCChConta: TWideStringField;
    QrBancosCChCheque: TWideStringField;
    QrBancosCChValor: TWideStringField;
    QrBancosCChCNPJCPF: TWideStringField;
    QrBancosCChCodSit: TWideStringField;
    QrBancosCChDtaEven: TWideStringField;
    QrBancosCChBordero: TWideStringField;
    QrBancosCChTxtDev: TWideStringField;
    QrBancosCChTxtDep: TWideStringField;
    SBEntidade: TSpeedButton;
    WebBrowser1: TWebBrowser;
    Label79: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtBancoClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrBancosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrBancosAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrBancosBeforeOpen(DataSet: TDataSet);
    procedure BtImportarClick(Sender: TObject);
    procedure BtNavegarClick(Sender: TObject);
    procedure QrBancosBeforeClose(DataSet: TDataSet);
    procedure Novaocorrncia1Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtCNABClick(Sender: TObject);
    procedure Excluiocorrnciaatual1Click(Sender: TObject);
    procedure Novobanco1Click(Sender: TObject);
    procedure Alterabancoatual1Click(Sender: TObject);
    procedure Excluibancoatual1Click(Sender: TObject);
    procedure Importadelista1Click(Sender: TObject);
    procedure Alteraocorrnciaatual1Click(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure PageControl1Change(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Incluinovoregistro1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Alteraregistroatual1Click(Sender: TObject);
    procedure QrCNAB_CaDCalcFields(DataSet: TDataSet);
    procedure QrBanCNAB_RAfterScroll(DataSet: TDataSet);
    procedure RGEnvi2Click(Sender: TObject);
    procedure QrBanCNAB_RBeforeClose(DataSet: TDataSet);
    procedure EdPosicoesChange(Sender: TObject);
    procedure Incluinovocampo1Click(Sender: TObject);
    procedure Alteracampoatual1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure EdXLSDataExit(Sender: TObject);
    procedure EdXLSHistExit(Sender: TObject);
    procedure EdXLSDocuExit(Sender: TObject);
    procedure EdXLSHiDoExit(Sender: TObject);
    procedure EdXLSCompExit(Sender: TObject);
    procedure EdXLSCPMFExit(Sender: TObject);
    procedure EdXLSCredExit(Sender: TObject);
    procedure EdXLSDebiExit(Sender: TObject);
    procedure EdXLSCrDbExit(Sender: TObject);
    procedure EdXLSDouCExit(Sender: TObject);
    procedure EdXLSSldoExit(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EditorKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EditorMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure EditorMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure RGImportaClick(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure EdArqBcoXLSChange(Sender: TObject);
    procedure SbCNABDirMulClick(Sender: TObject);
    procedure SBEntidadeClick(Sender: TObject);
  private
    FRegAnt: Integer;
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure MostraEdicaoIts(SQLType: TSQLType);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    function RichRow(m: TCustomMemo): Longint;
    function RichCol(m: TCustomMemo): Longint;
    procedure PesquisaTexto;
    procedure ImportaThisCNAB;
  public
    { Public declarations }
    FExecutouAuto, FImportaBancos: Boolean;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenBanCNAB_R(Registro: Integer);
    procedure ReopenCNAB_CaD(ID: String);
    procedure ImportaAllCNAB();
    procedure ImportaCNAB_DeArquivo(Fonte: String);
    procedure ImportaBancos();
    //procedure ImportaCNAB_Bancos(LinhaIni: Integer);
    procedure AtualizaCNAB_Bancos;
    procedure AtualizaCNAB_BanCNAB_R;
    procedure AtualizaCNAB_CNAB_CaD;
    procedure AtualizaCNAB_CNAB_CaG;
    procedure AtualizaCNAB_CNAB_Fld;
    procedure CarregaXLS();
  end;

var
  FmBancos: TFmBancos;

resourcestring
  sSaveChanges = 'Salva as altera��es de %s?';
  sOverWrite = 'OK para sobrescrever %s';
  sUntitled = 'Sem t�tulo';
  sModified = 'Modificado';
  sColRowInfo = 'Linha: %3d   Coluna: %3d';
  sPergunta = 'Pergunta';
  //
  sCaminho = 'C:\Dermatek\Temp\Bancos';
  sArqTemp = 'C:\Dermatek\Temp\Bancos\Importa.Txt';

const
  FFormatFloat = '000';

  RulerAdj = 4/3;
  GutterWid = 6;

  ENGLISH = (SUBLANG_ENGLISH_US shl 10) or LANG_ENGLISH;
  FRENCH  = (SUBLANG_FRENCH shl 10) or LANG_FRENCH;
  GERMAN  = (SUBLANG_GERMAN shl 10) or LANG_GERMAN;


implementation

uses UnMyObjects, Module, MyVCLSkin, ModuleGeral, DmkDAC_PF, UnDmkWeb;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmBancos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmBancos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrBancosCodigo.Value, LaRegistro.Caption[2]);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmBancos.DefParams;
begin
  VAR_GOTOTABELA := 'Bancos';
  VAR_GOTOMYSQLTABLE := QrBancos;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT ban.*, CASE WHEN ent.Tipo=0');
  VAR_SQLx.Add('THEN ent.RazaoSocial ELSE ent.Nome END NOMEENTIDADE');
  VAR_SQLx.Add('FROM bancos ban');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=ban.Entidade');
  VAR_SQLx.Add('WHERE ban.Codigo > -1000');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND ban.Codigo=:P0');
  //
  VAR_SQLa.Add('AND ban.Nome Like :P0');
  //
end;

procedure TFmBancos.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible := True;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PainelRegistro.Visible := False;
      PainelCampo.Visible    := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text       := '';
        EdCodigo.ReadOnly   := False;
        EdNome.Text         := '';
        EdDVCC.Text         := '10';
        EdID_400i.Text      := '';
        EdID_400t.Text      := '';
        EdDVB.Text          := '?';
        EdEntidade.Text     := '';
        CBEntidade.KeyValue := Null;
        //
        EdXLSData.Text      := '';
        EdXLSHist.Text      := '';
        EdXLSDocu.Text      := '';
        EdXLSHiDo.Text      := '';
        EdXLSComp.Text      := '';
        EdXLSCPMF.Text      := '';
        EdXLSCred.Text      := '';
        EdXLSDebi.Text      := '';
        EdXLSCrDb.Text      := '';
        EdXLSDouC.Text      := '';
        EdXLSSldo.Text      := '';
        EdXLSLinha.Text     := '';
        RGXLSTCDB.ItemIndex := 0;
        EdDescriCNR.Text    := 'Tarifa CNR';
        EdCNABDirMul.Text   := '';
        //
        EdCChLinha  .ValueVariant := '';
        EdCChDtaCust.ValueVariant := '';
        EdCChNosNum .ValueVariant := '';
        EdCChBanco  .ValueVariant := '';
        EdCChAgencia.ValueVariant := '';
        EdCChConta  .ValueVariant := '';
        EdCChCheque .ValueVariant := '';
        EdCChValor  .ValueVariant := '';
        EdCChCNPJCPF.ValueVariant := '';
        EdCChCodSit .ValueVariant := '';
        EdCChDtaEven.ValueVariant := '';
        EdCChBordero.ValueVariant := '';
        EdCChTxtDev .ValueVariant := '';
        EdCChTxtDep .ValueVariant := '';
        //
        EdCodigo.SetFocus;
      end else begin
        EdCodigo.ReadOnly   := True;
        EdCodigo.Text       := DBEdCodigo.Text;
        EdNome.Text         := DBEdNome.Text;
        EdDVCC.Text         := IntToStr(QrBancosDVCC.Value);
        EdDVB.Text          := QrBancosDVB.Value;
        EdEntidade.Text     := IntToStr(QrBancosEntidade.Value);
        CBEntidade.KeyValue := QrBancosEntidade.Value;
        EdID_400i.Text      := IntToStr(QrBancosID_400i.Value);
        EdID_400t.Text      := IntToStr(QrBancosID_400t.Value);
        //
        EdXLSData.Text      := QrBancosXlsData.Value;
        EdXLSHist.Text      := QrBancosXlsHist.Value;
        EdXLSDocu.Text      := QrBancosXlsDocu.Value;
        EdXLSHiDo.Text      := QrBancosXlsHiDo.Value;
        EdXLSComp.Text      := QrBancosXlsComp.Value;
        EdXLSCPMF.Text      := QrBancosXlsCPMF.Value;
        EdXLSCred.Text      := QrBancosXlsCred.Value;
        EdXLSDebi.Text      := QrBancosXlsDebi.Value;
        EdXLSCrDb.Text      := QrBancosXlsCrDb.Value;
        EdXLSDouC.Text      := QrBancosXlsDouC.Value;
        EdXLSSldo.Text      := QrBancosXlsSldo.Value;
        EdXLSLinha.Text     := IntToStr(QrBancosXlsLinha.Value);
        RGXLSTCDB.ItemIndex := QrBancosXlsTCDB.Value;
        //
        EdDescriCNR.Text    := QrBancosDescriCNR.Value;
        EdCNABDirMul.Text   := QrBancosCNABDirMul.Value;
        //
        EdCChLinha  .ValueVariant := QrBancosCChLinha  .Value;
        EdCChDtaCust.ValueVariant := QrBancosCChDtaCust.Value;
        EdCChNosNum .ValueVariant := QrBancosCChNosNum .Value;
        EdCChBanco  .ValueVariant := QrBancosCChBanco  .Value;
        EdCChAgencia.ValueVariant := QrBancosCChAgencia.Value;
        EdCChConta  .ValueVariant := QrBancosCChConta  .Value;
        EdCChCheque .ValueVariant := QrBancosCChCheque .Value;
        EdCChValor  .ValueVariant := QrBancosCChValor  .Value;
        EdCChCNPJCPF.ValueVariant := QrBancosCChCNPJCPF.Value;
        EdCChCodSit .ValueVariant := QrBancosCChCodSit .Value;
        EdCChDtaEven.ValueVariant := QrBancosCChDtaEven.Value;
        EdCChBordero.ValueVariant := QrBancosCChBordero.Value;
        EdCChTxtDev .ValueVariant := QrBancosCChTxtDev .Value;
        EdCChTxtDep .ValueVariant := QrBancosCChTxtDep .Value;
        //
        EdNome.SetFocus;
      end;
    end;
    2:
    begin
      QrReg.Close;
      QrReg.SQL.Clear;
      QrReg.SQL.Add('SELECT cag.Codigo, cag.Nome');
      QrReg.SQL.Add('FROM cnab_cag cag');
      if SQLType = stIns then
      begin
        QrReg.SQL.Add('WHERE cag.Codigo NOT IN (');
        QrReg.SQL.Add('  SELECT Registro');
        QrReg.SQL.Add('  FROM bancnab_r');
        QrReg.SQL.Add('  WHERE Codigo=:P0)');
        QrReg.Params[0].AsInteger := QrBancosCodigo.Value;
        //
        EdReg.Text     := '';
        CBReg.KeyValue := Null;
        EdSeq1Val.Text := '';
        EdSeq2Val.Text := '';
        //
        RGEnvio.ItemIndex := 0;
        EdTamLinha.Text   := '';
      end else begin
        EdReg.Text     := IntToStr(QrBanCNAB_RRegistro.Value);
        CBReg.KeyValue := QrBanCNAB_RRegistro.Value;
        EdSeq1Val.Text := QrBanCNAB_RSeq1Val.Value;
        EdSeq2Val.Text := QrBanCNAB_RSeq2Val.Value;
        //
        EdSeq1Ini.Text := IntToStr(QrBanCNAB_RSeq1Ini.Value);
        EdSeq1Tam.Text := IntToStr(QrBanCNAB_RSeq1Tam.Value);
        EdSeq2Ini.Text := IntToStr(QrBanCNAB_RSeq2Ini.Value);
        EdSeq2Tam.Text := IntToStr(QrBanCNAB_RSeq2Tam.Value);
        FRegAnt := QrBanCNAB_RRegistro.Value;
        //
        RGEnvio.ItemIndex := QrBanCNAB_REnvio.Value + 1;
        EdTamLinha.Text   := IntToStr(QrBanCNAB_RTamLinha.Value);
      end;
      QrReg.SQL.Add('ORDER BY cag.Nome');
      UnDmkDAC_PF.AbreQuery(QrReg, Dmod.MyDB);
      //
      PainelRegistro.Visible    := True;
      PainelDados.Visible := False;
      //
      EdReg.SetFocus;
    end;
    3:
    begin
      PainelDados.Visible    := False;
      PainelCampo.Visible    := True;
      QrGrupos.Close;
      QrBancoz.Close;
      QrCNAB_Fld.Close;
      //
      UnDmkDAC_PF.AbreQuery(QrGrupos, Dmod.MyDB);
      UnDmkDAC_PF.AbreQuery(QrBancoz, Dmod.MyDB);
      UnDmkDAC_PF.AbreQuery(QrCNAB_Fld, Dmod.MyDB);
      CkContinuar.Checked := False;
      if SQLType = stIns then
      begin
        EdNomeFld.Text      := '';
        EdSignific.Text     := '';
        EdAjuda.Text        := '';
        EdPadrIni.Text      := '';
        EdPadrTam.Text      := '';
        EdPadrVal.Text      := '';
        EdSecoVal.Text      := '';
        EdID.Text           := '';
        EdFormato.Text      := '';
        EdCasas.Text        := '';
        CGPosicoes.Value    := -1;
        EdGrupo.Text        := '';
        CBGrupo.KeyValue    := Null;
        EdBcoOrig.Text      := IntToStr(QrBancosCodigo.Value);
        CBBcoOrig.KeyValue  := QrBancosCodigo.Value;
        EdCampo.Text        := '';
        CBCampo.KeyValue    := Null;
        RGFmtInv.ItemIndex  := 0;
        RGAlfaNum.ItemIndex := -1;
        RGEnvioFld.ItemIndex := -1;
        //
        CkContinuar.Visible := True;
      end else begin
        EdNomeFld.Text      := QrCNAB_CaDNome.Value;
        EdSignific.Text     := QrCNAB_CaDSignific.Value;
        EdAjuda.Text        := QrCNAB_CaDAjuda.Value;
        EdPadrIni.Text      := IntToStr(QrCNAB_CaDPadrIni.Value);
        EdPadrTam.Text      := IntToStr(QrCNAB_CaDPadrTam.Value);
        EdPadrVal.Text      := QrCNAB_CaDPadrVal.Value;
        EdSecoVal.Text      := QrCNAB_CaDSecoVal.Value;
        EdID.Text           := QrCNAB_CaDID.Value;
        EdFormato.Text      := QrCNAB_CaDFormato.Value;
        EdCasas.Text        := IntToStr(QrCNAB_CaDCasas.Value);
        CGPosicoes.Value    := QrCNAB_CaDPOSICOES.Value;
        EdGrupo.Text        := IntToStr(QrCNAB_CaDRegistro.Value);
        CBGrupo.KeyValue    := QrCNAB_CaDRegistro.Value;
        EdBcoOrig.Text      := IntToStr(QrCNAB_CaDBcoOrig.Value);
        CBBcoOrig.KeyValue  := QrCNAB_CaDBcoOrig.Value;
        EdCampo.Text        := IntToStr(QrCNAB_CaDCampo.Value);
        CBCampo.KeyValue    := QrCNAB_CaDCampo.Value;
        RGFmtInv.ItemIndex  := QrCNAB_CaDFmtInv.Value;
        RGAlfaNum.ItemIndex := QrCNAB_CaDAlfaNum.Value;
        RGEnvioFld.ItemIndex := QrCNAB_CaDEnvio.Value - 1;
        //
        CkContinuar.Visible := False;
      end;
      EdID.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmBancos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
  if Dmod.QrControle.FieldByName('LastBco').AsInteger > 0 then
    GOTOy.LC(QrBancosCodigo.Value, Dmod.QrControle.FieldByName('LastBco').AsInteger);
end;

procedure TFmBancos.AlteraRegistro;
var
  Bancos : Integer;
begin
  Bancos := QrBancosCodigo.Value;
  if not UMyMod.SelLockY(Bancos, Dmod.MyDB, 'Bancos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Bancos, Dmod.MyDB, 'Bancos', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmBancos.IncluiRegistro;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MostraEdicao(1, stIns, 0);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmBancos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmBancos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmBancos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmBancos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmBancos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmBancos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmBancos.BtAbreClick(Sender: TObject);
begin
  MyObjects.Xls_To_StringGrid(Grade1, EdArqBcoXLS.Text, PB1, LaAviso, nil);
  BtCarrega.Enabled := True;
end;

procedure TFmBancos.BtBancoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBanco, BtBanco);
end;

procedure TFmBancos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrBancosCodigo.Value;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE controle SET LastBco=:P0');
  Dmod.QrUpd.Params[0].AsInteger := QrBancosCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  Dmod.ReopenControle();
  Close;
end;

procedure TFmBancos.BtConfirmaClick(Sender: TObject);
var
  DVCC, Entidade, (*ID_240i, ID_240t, ID_240r,*) ID_400i, ID_400t, XlsLinha,
  XlsTCDB, CChLinha, Codigo: Integer;
  //Sigla,
  DVB, (*ID_240s,*) XlsData, XlsHist, XlsDocu, XlsHiDo, XlsCred, XlsDebi, XlsCrDb,
  XlsDouC, XlsComp, XlsCPMF, XlsSldo, DescriCNR, CNABDirMul, CChDtaCust,
  CChNosNum, CChBanco, CChAgencia, CChConta, CChCheque, CChValor, CChCNPJCPF,
  CChCodSit, CChDtaEven, CChBordero, CChTxtDev, CChTxtDep, Nome, Site: String;
begin
  Nome := EdNome.Text;
  Site := EdSite.Text;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o.') then
    Exit;
  Codigo := Geral.IMV(EdCodigo.Text);
(*
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO bancos SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE bancos SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Site=:P1, DVCC=:P2, DVB=:P3, Entidade=:P4, ');
  Dmod.QrUpdU.SQL.Add('ID_400i=:P5, ID_400t=:P6, ');
  Dmod.QrUpdU.SQL.Add('XLSLinha=:P07, XLSData=:P08, XLSHist=:P09, ');
  Dmod.QrUpdU.SQL.Add('XLSDocu=:P10, XLSHiDo=:P11, XLSCred=:P12, ');
  Dmod.QrUpdU.SQL.Add('XLSDebi=:P13, XLSCrDb=:P14, XLSDouC=:P15, ');
  Dmod.QrUpdU.SQL.Add('XLSSldo=:P16, XLSComp=:P17, XLSCPMF=:P18, ');
  Dmod.QrUpdU.SQL.Add('XLSTCDB=:P19, DescriCNR=:P20, CNABDirMul=:P21, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsString  := Site;
  Dmod.QrUpdU.Params[02].AsInteger := Geral.IMV(EdDVCC.Text);
  Dmod.QrUpdU.Params[03].AsString  := EdDVB.Text;
  Dmod.QrUpdU.Params[04].AsInteger := Geral.IMV(EdEntidade.Text);
  Dmod.QrUpdU.Params[05].AsInteger := Geral.IMV(EdID_400i.Text);
  Dmod.QrUpdU.Params[06].AsInteger := Geral.IMV(EdID_400t.Text);
  Dmod.QrUpdU.Params[07].AsInteger := Geral.IMV(EdXlsLinha.Text);
  Dmod.QrUpdU.Params[08].AsString  := EdXLSData.Text;
  Dmod.QrUpdU.Params[09].AsString  := EdXLSHist.Text;
  Dmod.QrUpdU.Params[10].AsString  := EdXLSDocu.Text;
  Dmod.QrUpdU.Params[11].AsString  := EdXLSHiDo.Text;
  Dmod.QrUpdU.Params[12].AsString  := EdXLSCred.Text;
  Dmod.QrUpdU.Params[13].AsString  := EdXLSDebi.Text;
  Dmod.QrUpdU.Params[14].AsString  := EdXLSCrDb.Text;
  Dmod.QrUpdU.Params[15].AsString  := EdXLSDouC.Text;
  Dmod.QrUpdU.Params[16].AsString  := EdXLSSldo.Text;
  Dmod.QrUpdU.Params[17].AsString  := EdXLSComp.Text;
  Dmod.QrUpdU.Params[18].AsString  := EdXLSCPMF.Text;
  Dmod.QrUpdU.Params[19].AsInteger := RGXLSTCDB.ItemIndex;
  Dmod.QrUpdU.Params[20].AsString  := EdDescriCNR.Text;
  Dmod.QrUpdU.Params[21].AsString  := EdCNABDirMul.Text;
  //
  Dmod.QrUpdU.Params[22].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[23].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[24].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
*)
  (*ID_240i, ID_240t, ID_240r, ID_240s,*)
  Site      := EdSite.ValueVariant;
  DVCC      := EdDVCC.ValueVariant;
  DVB       := EdDVB.ValueVariant;
  Entidade  := EdEntidade.ValueVariant;
  ID_400i   := EdID_400i.ValueVariant;
  ID_400t   := EdID_400t.ValueVariant;
  XlsLinha  := EdXlsLinha.ValueVariant;
  XlsData   := EdXlsData.ValueVariant;
  XlsHist   := EdXlsHist.ValueVariant;
  XlsDocu   := EdXlsDocu.ValueVariant;
  XlsHiDo   := EdXlsHiDo.ValueVariant;
  XlsCred   := EdXlsCred.ValueVariant;
  XlsDebi   := EdXlsDebi.ValueVariant;
  XlsCrDb   := EdXlsCrDb.ValueVariant;
  XlsDouC   := EdXlsDouC.ValueVariant;
  XlsTCDB   := RGXlsTCDB.ItemIndex;
  XlsComp   := EdXlsComp.ValueVariant;
  XlsCPMF   := EdXlsCPMF.ValueVariant;
  XlsSldo   := EdXlsSldo.ValueVariant;
  DescriCNR := EdDescriCNR.ValueVariant;
  CNABDirMul:= EdCNABDirMul.ValueVariant;
  CChLinha  := EdCChLinha.ValueVariant;
  CChDtaCust:= EdCChDtaCust.ValueVariant;
  CChNosNum := EdCChNosNum.ValueVariant;
  CChBanco  := EdCChBanco.ValueVariant;
  CChAgencia:= EdCChAgencia.ValueVariant;
  CChConta  := EdCChConta.ValueVariant;
  CChCheque := EdCChCheque.ValueVariant;
  CChValor  := EdCChValor.ValueVariant;
  CChCNPJCPF:= EdCChCNPJCPF.ValueVariant;
  CChCodSit := EdCChCodSit.ValueVariant;
  CChDtaEven:= EdCChDtaEven.ValueVariant;
  CChBordero:= EdCChBordero.ValueVariant;
  CChTxtDev := EdCChTxtDev.ValueVariant;
  CChTxtDep := EdCChTxtDep.ValueVariant;
  //Sigla     := EdSigla.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'bancos', False, [
  'Nome', 'Site', 'DVCC',
  'DVB', 'Entidade', (*'ID_240i',
  'ID_240t', 'ID_240r', 'ID_240s',*)
  'ID_400i', 'ID_400t', 'XlsLinha',
  'XlsData', 'XlsHist', 'XlsDocu',
  'XlsHiDo', 'XlsCred', 'XlsDebi',
  'XlsCrDb', 'XlsDouC', 'XlsTCDB',
  'XlsComp', 'XlsCPMF', 'XlsSldo',
  'DescriCNR', 'CNABDirMul', 'CChLinha',
  'CChDtaCust', 'CChNosNum', 'CChBanco',
  'CChAgencia', 'CChConta', 'CChCheque',
  'CChValor', 'CChCNPJCPF', 'CChCodSit',
  'CChDtaEven', 'CChBordero', 'CChTxtDev',
  'CChTxtDep'], [
  'Codigo'(*, 'Sigla'*)], [
  Nome, Site, DVCC,
  DVB, Entidade, (*ID_240i,
  ID_240t, ID_240r, ID_240s,*)
  ID_400i, ID_400t, XlsLinha,
  XlsData, XlsHist, XlsDocu,
  XlsHiDo, XlsCred, XlsDebi,
  XlsCrDb, XlsDouC, XlsTCDB,
  XlsComp, XlsCPMF, XlsSldo,
  DescriCNR, CNABDirMul, CChLinha,
  CChDtaCust, CChNosNum, CChBanco,
  CChAgencia, CChConta, CChCheque,
  CChValor, CChCNPJCPF, CChCodSit,
  CChDtaEven, CChBordero, CChTxtDev,
  CChTxtDep], [
  Codigo(*, Sigla*)], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Bancos', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmBancos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Bancos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Bancos', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Bancos', 'Codigo');
end;

procedure TFmBancos.FormCreate(Sender: TObject);
begin
  FExecutouAuto  := False;
  FImportaBancos := False;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  //
  PageControl1.Align := alClient;
  //
  GBEdita.Align      := alClient;
  GBCampo.Align      := alClient;
  GBRegistro.Align   := alClient;
  GBDados.Align      := alClient;
  //
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  PnNavega.Align     := alClient;
  Panel1.Align       := alClient;
  Panel4.Align       := alClient;
  //
  UnDmkDAC_PF.AbreQuery(QrEntidade, Dmod.MyDB);
  CriaOForm;
  //
  Grade.ColWidths[00] := 032;
  Grade.ColWidths[01] := 032;
  Grade.ColWidths[02] := 300;
  Grade.ColWidths[03] := 032;
  Grade.ColWidths[04] := 600;
  Grade.ColWidths[05] := 032;
  Grade.ColWidths[06] := 040;
  //
  Grade.Cells[00,00] := 'Linha';
  Grade.Cells[01,00] := 'In�cio';
  Grade.Cells[02,00] := 'Nome';
  Grade.Cells[03,00] := 'Tamanho';
  Grade.Cells[04,00] := 'Descri��o';
  Grade.Cells[05,00] := 'X9';
  Grade.Cells[06,00] := 'Link';
  //
  ImgTipo.SQLType := stLok;
end;

procedure TFmBancos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrBancosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmBancos.SbCNABDirMulClick(Sender: TObject);
var
  Arquivo, Titulo, IniDir: String;
begin
  Arquivo := EdCNABDirMul.Text;
  Titulo  := 'Abrir Arquivo de Concilia��o Banc�ria';
  IniDir  := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, Titulo, '', [], Arquivo) then
    EdCNABDirMul.Text := ExtractFilePath(Arquivo);
end;

procedure TFmBancos.SBEntidadeClick(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdEntidade.ValueVariant;
  //
  DmodG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEntidade, CBEntidade, QrEntidade, VAR_CADASTRO);
    EdEntidade.SetFocus;
  end;
end;

procedure TFmBancos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmBancos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmBancos.QrBancosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmBancos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Bancos', 'Livres', 99) then
  //BtInclui.Enabled := False;
  EdCodigo.MaxLength := Length(FFormatFloat);
  //
  if FImportaBancos then
  begin
    FImportaBancos := False;
    RGImporta.Itemindex := 1;
    Timer1.Enabled := true;
  end;
end;

procedure TFmBancos.QrBancosAfterScroll(DataSet: TDataSet);
begin
  ReopenBanCNAB_R(0);
end;

procedure TFmBancos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrBancosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Bancos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmBancos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBancos.QrBancosBeforeOpen(DataSet: TDataSet);
begin
  QrBancosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmBancos.BtNavegarClick(Sender: TObject);
begin
  if Trim(QrBancosSite.Value) <> '' then
    Webbrowser1.Navigate(QrBancosSite.Value);
end;

procedure TFmBancos.ReopenBanCNAB_R(Registro: Integer);
var
  a, b, p: Integer;
begin
  if RGEnvi2.ItemIndex = 0 then
  begin
    a := -1;
    b := -1;
  end else begin
    a := RGEnvi2.ItemIndex -1;
    b := 2;
  end;
  p := Geral.IMV(EdPosicoes.Text);
  QrBanCNAB_R.Close;
  QrBanCNAB_R.Params[0].AsInteger := QrBancosCodigo.Value;
  QrBanCNAB_R.Params[1].AsInteger :=  a;
  QrBanCNAB_R.Params[2].AsInteger :=  b;
  QrBanCNAB_R.Params[3].AsInteger :=  p;
  UnDmkDAC_PF.AbreQuery(QrBanCNAB_R, Dmod.MyDB);
  //
  QrBanCNAB_R.Locate('Registro', Registro, []);
end;

procedure TFmBancos.ReopenCNAB_CaD(ID: String);
begin
  QrCNAB_CaD.Close;
  QrCNAB_CaD.Params[00].AsInteger := QrBancosCodigo.Value;
  QrCNAB_CaD.Params[01].AsInteger := QrBanCNAB_RRegistro.Value;
  QrCNAB_CaD.Params[02].AsInteger := QrBanCNAB_REnvio.Value;
  UnDmkDAC_PF.AbreQuery(QrCNAB_CaD, Dmod.MyDB);
  //
  QrCNAB_CaD.Locate('ID', ID, []);
end;

procedure TFmBancos.QrBancosBeforeClose(DataSet: TDataSet);
begin
  //QrBancosLey.Close;
end;

procedure TFmBancos.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  {if Column.FieldName = 'Carrega' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, QrBancosLeyCarrega.Value);}
end;

procedure TFmBancos.BtCarregaClick(Sender: TObject);
begin
  CarregaXLS();
end;

procedure TFmBancos.BtCNABClick(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    1: MyObjects.MostraPopUpDeBotao(PMOcorrencia, BtCNAB);
    2: MyObjects.MostraPopUpDeBotao(PMComposicao, BtCNAB);
    else Geral.MB_Aviso('Selecione uma "orelha" v�lida do "PageControl"!');
  end;
end;

procedure TFmBancos.Excluiocorrnciaatual1Click(Sender: TObject);
(*
var
  Controle: Integer;
*)
begin
(*
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'bancostar', 'Controle', QrBancosTarControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrBancosTar,
      QrBancosTarControle, QrBancosTarControle.Value);
    ReopenBancosTar(Controle);
  end;
*)
end;

procedure TFmBancos.Novaocorrncia1Click(Sender: TObject);
begin
  MostraEdicaoIts(stIns);
end;

procedure TFmBancos.MostraEdicaoIts(SQLType: TSQLType);
begin
  // Nao adianta fazer aqui!
  // Depende do Tamanho de registro CNAB (240 ou 400)
  // Fazer na Configuracao CNAB
end;

procedure TFmBancos.Novobanco1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmBancos.Alterabancoatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmBancos.Excluibancoatual1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o deste banco?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM bancos WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrBancosCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    LocCod(QrBancosCodigo.Value, QrBancosCodigo.Value);
  end;
end;

procedure TFmBancos.Importadelista1Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 5;
end;

procedure TFmBancos.Alteraocorrnciaatual1Click(Sender: TObject);
begin
  MostraEdicaoIts(stUpd);
end;

procedure TFmBancos.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  BtCNAB.Enabled := False;
end;

procedure TFmBancos.PageControl1Change(Sender: TObject);
begin
  BtCNAB.Enabled := PageControl1.ActivePageIndex in ([1,2]);

end;

procedure TFmBancos.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmBancos.Incluinovoregistro1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmBancos.Alteraregistroatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stUpd, 0);
end;

procedure TFmBancos.BitBtn1Click(Sender: TObject);
var
  Registro, TamLinha: Integer;
begin
  Registro := Geral.IMV(EdReg.Text);
  if Registro < 1 then
  begin
    Geral.MB_Aviso('Informe o registro banc�rio!');
    EdReg.SetFocus;
    Exit;
  end;

  //

  
  TamLinha := Geral.IMV(EdTamLinha.Text);
  if (TamLinha <> 240) and (TamLinha <> 400) then
  begin
    Geral.MB_Aviso('O valor informado para posi��es fora do esperado!');
  end;
  Dmod.QrUpd.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO bancnab_r SET ');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE bancnab_r SET ');
  end;
  Dmod.QrUpd.SQL.Add('Seq1Ini=:P0, Seq1Tam=:P1, Seq1Val=:P2, ');
  Dmod.QrUpd.SQL.Add('Seq2Ini=:P3, Seq2Tam=:P4, Seq2Val=:P5, ');
  Dmod.QrUpd.SQL.Add('Envio=:P6, TamLinha=:P7, ');
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Add('Registro=:Pa, Codigo=:Pb ');
  end else begin
    Dmod.QrUpd.SQL.Add('Registro=:Pa WHERE Codigo=:Pb AND Registro=:Pc');
  end;
  Dmod.QrUpd.Params[00].AsInteger := Geral.IMV(EdSeq1Ini.Text);
  Dmod.QrUpd.Params[01].AsInteger := Geral.IMV(EdSeq1Tam.Text);
  Dmod.QrUpd.Params[02].AsString  :=              EdSeq1Val.Text ;
  Dmod.QrUpd.Params[03].AsInteger := Geral.IMV(EdSeq2Ini.Text);
  Dmod.QrUpd.Params[04].AsInteger := Geral.IMV(EdSeq2Tam.Text);
  Dmod.QrUpd.Params[05].AsString  :=              EdSeq2Val.Text ;
  Dmod.QrUpd.Params[06].AsInteger := RGEnvio.ItemIndex - 1;
  Dmod.QrUpd.Params[07].AsInteger := TamLinha;
  //
  Dmod.QrUpd.Params[08].AsInteger := Registro;
  Dmod.QrUpd.Params[09].AsInteger := QrBancosCodigo.Value;
  //
  if ImgTipo.SQLType = stUpd then
    Dmod.QrUpd.Params[10].AsInteger := FRegAnt;
  Dmod.QrUpd.ExecSQL;
  MostraEdicao(0, stLok, 0);
  ReopenBanCNAB_R(Registro);
end;

procedure TFmBancos.QrCNAB_CaDCalcFields(DataSet: TDataSet);
begin
  case QrCNAB_CaDAlfaNum.Value of
    0: QrCNAB_CaDNOME_T.Value := 'X';
    1: QrCNAB_CaDNOME_T.Value := '9';
    else QrCNAB_CaDNOME_T.Value := '?';
  end;
  case QrCNAB_CaDFmtInv.Value of
    0: QrCNAB_CaDNOME_I.Value := 'N';
    1: QrCNAB_CaDNOME_I.Value := 'S';
    else QrCNAB_CaDNOME_I.Value := '?';
  end;
  QrCNAB_CaDPOSICOES.Value := UBancos.TamanhoLinhaCNAB_Valor(
    QrCNAB_CaDT240.Value, QrCNAB_CaDT400.Value);
end;

procedure TFmBancos.QrBanCNAB_RAfterScroll(DataSet: TDataSet);
begin
  ReopenCNAB_CaD('');
end;

procedure TFmBancos.RGEnvi2Click(Sender: TObject);
begin
  ReopenBanCNAB_R(0);
end;

procedure TFmBancos.RGImportaClick(Sender: TObject);
begin
  PageControl2.ActivePageIndex := RGImporta.ItemIndex;
end;

procedure TFmBancos.QrBanCNAB_RBeforeClose(DataSet: TDataSet);
begin
  QrCNAB_CaD.Close;
end;

procedure TFmBancos.EdPosicoesChange(Sender: TObject);
begin
  ReopenBanCNAB_R(0);
end;

procedure TFmBancos.Incluinovocampo1Click(Sender: TObject);
begin
  MostraEdicao(3, stIns, 0);
end;

procedure TFmBancos.Alteracampoatual1Click(Sender: TObject);
begin
  MostraEdicao(3, stUpd, 0);
end;

procedure TFmBancos.BitBtn3Click(Sender: TObject);
var
  Campo: Integer;
  NomeFld, ID: String;
begin
  NomeFld := EdNomeFld.Text;
  if Length(NomeFld) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    EdNome.SetFocus;
    Exit;
  end;
  ID := EdID.Text;
  if Length(ID) = 0 then
  begin
    Geral.MB_Aviso('Defina O ID do campo.');
    EdID.SetFocus;
    Exit;
  end;
  if RGAlfaNum.ItemIndex = -1 then
  begin
    Geral.MB_Aviso('Defina o tipo de valor do campo.');
    RGAlfaNum.SetFocus;
    Exit;
  end;

  //


  Campo := Geral.IMV(EdCampo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO cnab_cad SET ');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE cnab_cad SET ');
  end;
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Signific=:P1, PadrVal=:P2, Ajuda=:P3, ');
  Dmod.QrUpdU.SQL.Add('PadrIni=:P4, PadrTam=:P5, AlfaNum=:P6, FmtInv=:P7, ');
  Dmod.QrUpdU.SQL.Add('Registro=:P8, Envio=:P09, Campo=:P10, SecoVal=:P11, ');
  Dmod.QrUpdU.SQL.Add('Formato=:P12, Casas=:P13, T240=:P14, T400=:P15, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb, BcoOrig=:Pc, ID=:Pd ')
  else
  begin
    Dmod.QrUpdU.SQL.Add('ID="' + ID + '", ');
    Dmod.QrUpdU.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE BcoOrig=:Pc AND ID=:Pd');
    ID := QrCNAB_CaDID.Value;
  end;
  //
  Dmod.QrUpdU.Params[00].AsString  := NomeFld;
  Dmod.QrUpdU.Params[01].AsString  := EdSignific.Text;
  Dmod.QrUpdU.Params[02].AsString  := EdPadrVal.Text;
  Dmod.QrUpdU.Params[03].AsString  := EdAjuda.Text;
  Dmod.QrUpdU.Params[04].AsInteger := Geral.IMV(EdPadrIni.Text);
  Dmod.QrUpdU.Params[05].AsInteger := Geral.IMV(EdPadrTam.Text);
  Dmod.QrUpdU.Params[06].AsInteger := RGAlfaNum.ItemIndex;
  Dmod.QrUpdU.Params[07].AsInteger := RGFmtInv.ItemIndex;
  Dmod.QrUpdU.Params[08].AsInteger := Geral.IMV(EdGrupo.Text);
  Dmod.QrUpdU.Params[09].AsInteger := RGEnvioFld.ItemIndex + 1;
  Dmod.QrUpdU.Params[10].AsInteger := Campo;
  Dmod.QrUpdU.Params[11].AsString  := EdSecoVal.Text;
  Dmod.QrUpdU.Params[12].AsString  := EdFormato.Text;
  Dmod.QrUpdU.Params[13].AsInteger := Geral.IMV(EdCasas.Text);
  Dmod.QrUpdU.Params[14].AsInteger := dmkPF.IntInConjunto2Def(1, CGPosicoes.Value, 1, 0);
  Dmod.QrUpdU.Params[15].AsInteger := dmkPF.IntInConjunto2Def(2, CGPosicoes.Value, 1, 0);
  //
  Dmod.QrUpdU.Params[16].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[17].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[18].AsInteger := Geral.IMV(EdBcoOrig.Text);
  Dmod.QrUpdU.Params[19].AsString  := ID;
  Dmod.QrUpdU.ExecSQL;
  ReopenCNAB_CaD(ID);
  if CkContinuar.Checked and (ImgTipo.SQLType = stIns) then
  begin
   Geral.MB_Info('Item inclu�do com sucesso!');
   EdPadrIni.Text   := FormatFloat('0', Geral.IMV(EdPadrIni.Text) +
                       Geral.IMV(EdPadrTam.Text));
   EdPadrTam.Text   := '';
   EdNomeFld.Text   := '';
   EdSignific.Text  := '';
   EdPadrVal.Text   := '';
   EdSecoVal.Text   := '';
   EdAjuda.Text     := '';
   EdCampo.Text     := '';
   EdFormato.Text   := '';
   EdCasas.Text     := '';
   RGAlfaNum.ItemIndex := -1;
   CBCampo.KeyValue := Null;
   //
   EdID.Text := dmkPF.DuplicataIncrementa(EdID.Text, 1);
   EdID.SetFocus;
  end else MostraEdicao(0, stLok, 0);
end;

procedure TFmBancos.BitBtn5Click(Sender: TObject);
const
  DestFile = 'C:\Dermatek\BancosE.xls';
var
  Fonte: String;
begin
  Fonte := EdArqBcoXLS.Text;
  if FileExists(DestFile) then DeleteFile(DestFile);
  LaAviso.Caption := 'Aguarde... Baixando arquivo!';
  Application.ProcessMessages;
  if DmkWeb.DownloadFile(Fonte, DestFile) then
  begin
    if MyObjects.Xls_To_StringGrid(Grade1, DestFile, PB1, LaAviso, nil) then
    begin
      BtCarrega.Enabled := True;
      CarregaXLS();
    end;
  end
  else LaAviso.Caption := 'Erro durante o download de "' + Fonte + '"';
end;

procedure TFmBancos.EdArqBcoXLSChange(Sender: TObject);
begin
  BtAbre.Enabled := Copy(EdArqBcoXLS.Text, 1, 4) <> 'http';
end;

procedure TFmBancos.CarregaXLS();
var
  R, L, Codigo: Integer;
  Site, Nome, Lin, Sigla: String;
begin
  Screen.Cursor := crHourGlass;
  try
    PB1.Position := 0;
    PB1.Max := Grade1.RowCount;
    Lin := '003';
    if not InputQuery('Linha inicial', 'Informe a linha inicial:', Lin) then
    begin
      Screen.Cursor := crHourGlass;
      Exit;
    end;
    L := Geral.IMV(Lin);
    PB1.Position := L;
    for R := L to Grade1.RowCount -1 do
    begin
      PB1.Position := PB1.Position + 1;
      Sigla  := Trim(Grade1.Cells[01,R]);
      Codigo := Geral.IMV(Sigla);
      Nome   := Trim(Grade1.Cells[02,R]);
      Site   := Trim(Grade1.Cells[03,R]);
      if (Trim(Sigla) = '') and (Codigo = 0) then
      begin
        if pos('Banco', Nome) = 1 then
          Sigla := Copy(Nome, 7)
        else
          Sigla := Copy(Nome, 1);
      end;
      if Trim(Nome) <> '' then
      UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'bancos', False, [
        'Nome', 'Site'], ['Codigo', 'Sigla'], ['Ativo'], [
         Nome,   Site ], [ Codigo,   Sigla ], [    1  ], True);
    end;
    PB1.Position := PB1.Max;
    LocCod(1, 1);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBancos.EdXLSDataExit(Sender: TObject);
begin
  EdXLSData.Text := dmkPF.FCE(EdXLSData.Text);
end;

procedure TFmBancos.EdXLSHistExit(Sender: TObject);
begin
  EdXLSHist.Text := dmkPF.FCE(EdXLSHist.Text);
end;

procedure TFmBancos.EdXLSDocuExit(Sender: TObject);
begin
  EdXLSDocu.Text := dmkPF.FCE(EdXLSDocu.Text);
end;

procedure TFmBancos.EdXLSHiDoExit(Sender: TObject);
begin
  EdXLSHiDo.Text := dmkPF.FCE(EdXLSHiDo.Text);
end;

procedure TFmBancos.EdXLSCompExit(Sender: TObject);
begin
  EdXLSComp.Text := dmkPF.FCE(EdXLSComp.Text);
end;

procedure TFmBancos.EdXLSCPMFExit(Sender: TObject);
begin
  EdXLSCPMF.Text := dmkPF.FCE(EdXLSCPMF.Text);
end;

procedure TFmBancos.EdXLSCredExit(Sender: TObject);
begin
  EdXLSCred.Text := dmkPF.FCE(EdXLSCred.Text);
end;

procedure TFmBancos.EdXLSDebiExit(Sender: TObject);
begin
  EdXLSDebi.Text := dmkPF.FCE(EdXLSDebi.Text);
end;

procedure TFmBancos.EdXLSCrDbExit(Sender: TObject);
begin
  EdXLSCrDb.Text := dmkPF.FCE(EdXLSCrDb.Text);
end;

procedure TFmBancos.EdXLSDouCExit(Sender: TObject);
begin
  EdXLSDouC.Text := dmkPF.FCE(EdXLSDouC.Text);
end;

procedure TFmBancos.EdXLSSldoExit(Sender: TObject);
begin
  EdXLSSldo.Text := dmkPF.FCE(EdXLSSldo.Text);
end;

procedure TFmBancos.SpeedButton5Click(Sender: TObject);
begin
  if OpenDialog3.Execute then
  begin
    Editor.Lines.Clear;
    Editor.Lines.LoadFromFile(OpenDialog3.FileName);
    Edit1.Text := OpenDialog3.FileName;
  end;
end;

procedure TFmBancos.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdArqBcoXLS.Text);
  Arquivo := ExtractFileName(EdArqBcoXLS.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
  begin
    EdArqBcoXLS.Text := Arquivo;
  end;
end;

procedure TFmBancos.EditorKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  StatusBar.Panels[0].Text := Format(sColRowInfo, [RichRow(Editor)+1, RichCol(Editor)+1]);
end;

procedure TFmBancos.EditorMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  StatusBar.Panels[0].Text := Format(sColRowInfo, [RichRow(Editor)+1, RichCol(Editor)+1]);
end;

procedure TFmBancos.EditorMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  StatusBar.Panels[0].Text := Format(sColRowInfo, [RichRow(Editor)+1, RichCol(Editor)+1]);
end;

procedure TFmBancos.Edit2Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmBancos.Edit3Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmBancos.Edit4Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmBancos.PesquisaTexto;
var
  Lin, Col, Tam: Integer;
begin
  Lin := Geral.IMV(Edit2.Text);
  Col := Geral.IMV(Edit3.Text);
  Tam := Geral.IMV(Edit4.Text);
  //
  if (Lin > 0) and (Col > 0) and (Tam > 0) then
  begin
    Edit6.Text := Copy(Editor.Lines[Lin-1], Col, Tam);
  end else Edit6.Text := '';
end;

function TFmBancos.RichRow(m: TCustomMemo): Longint;
begin
  Result := SendMessage(m.Handle, EM_LINEFROMchar, m.SelStart, 0);
end;

function TFmBancos.RichCol(m: TCustomMemo): Longint;
begin
  Result := m.SelStart - SendMessage(m.Handle, EM_LINEINDEX, SendMessage(m.Handle,
    EM_LINEFROMchar, m.SelStart, 0), 0);
end;

procedure TFmBancos.BtImportarClick(Sender: TObject);
begin
  case RGImporta.Itemindex of
    0: ImportaBancos;
    1: ImportaAllCNAB;
    2: ImportaThisCNAB;
    else Geral.MB_Erro('A��o de importa��o n�o implementada!');
  end;
  MeRegistros.Lines.Clear;
end;

procedure TFmBancos.ImportaBancos();
var
  Lista: TStringList;
  i, PosNum, Tam: Integer;
  Codigo, Nome, Site: String;
begin
  if OpenDialog1.Execute then
  begin
    Screen.Cursor := crHourGlass;
    //
    Lista := TStringList.Create;
    Lista.LoadFromFile(OpenDialog1.FileName);
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM bancos WHERE Codigo=:P0');
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO bancos SET');
    Dmod.QrUpdU.SQL.Add('Nome=:P0, Site=:P1, Codigo=:P2');
    //
    Progress.Position := 0;
    Progress.Visible := True;
    Progress.Max := Lista.Count;

    for i := 0 to Lista.Count-1 do
    begin
      Progress.Position := Progress.Position + 1;
      Tam := Length(Lista[i]);
      if Tam > 4 then
      begin
        Codigo := Copy(Lista[i], 1, 3);
        PosNum := Pos('#', Lista[i]);
        if PosNum <> 0 then
        begin
          Nome := TrimRight(Copy(Lista[i], 5, PosNum-5));
          Site := Copy(Lista[i], PosNum+1, Tam-PosNum);
        end else begin
          Nome := Copy(Lista[i], 5, Length(Lista[i]));
          Site := '';
        end;
        //ShowMessage(PChar(Codigo+' - '+Nome+' @ '+Site));
        Dmod.QrUpdM.Params[00].AsString := Codigo;
        Dmod.QrUpdM.ExecSQL;
        //
        Dmod.QrUpdU.Params[00].AsString := Nome;
        Dmod.QrUpdU.Params[01].AsString := Site;
        Dmod.QrUpdU.Params[02].AsString := Codigo;
        Dmod.QrUpdU.ExecSQL;
      end;
    end;
    Progress.Visible := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBancos.ImportaAllCNAB();
begin
  if OpenDialog4.Execute then ImportaCNAB_DeArquivo(OpenDialog4.FileName);
end;

procedure TFmBancos.ImportaCNAB_DeArquivo(Fonte: String);
  procedure ImportaTabela(const LinhaIni: Integer; const Arquivo: TStringList;
    var Registros: TStringList);
  var
    k, z: Integer;
    x: String;
  begin
    x := '';
    z := LinhaIni;
    while x <> '<' do
    begin
      inc(z, 1);
      if z >= Arquivo.Count then Break;
      //if z >= MeImporta.Lines.Count then Break;
      x := Arquivo[z][1];
      //x := MeImporta.Lines[z][1];
    end;
    inc(z, -1);
    //MeRegistros.Lines.Clear;
    for k := LinhaIni+1 to z do
      //MeRegistros.Lines.Add(Arquivo[k]);
      Registros.Add(Arquivo[k]);
    if FileExists(sArqTemp) then
      DeleteFile(sArqTemp);
    Registros.SaveToFile(sArqTemp);
  end;
  //
var
  i, n, Versao: Integer;
  Tabela: String;
  Arquivo, Tabelas, Registros: TStringList;
begin
  if not FileExists(Fonte) then
  begin
    Geral.MB_Aviso('O arquivo "' + Fonte +
    '" n�o pode ser localizado!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    ForceDirectories(sCaminho);
    Arquivo := TStringList.Create;
    try
      //MeImporta.Lines.LoadFromFile(Fonte);
      Arquivo.LoadFromFile(Fonte);
      if Uppercase(Copy(Arquivo[0], 1, 6)) <> 'BANCOS' then
      begin
        //Arquivo.Free;
        Geral.MB_Aviso('O arquivo selecionado n�o � v�lido!');
        Screen.Cursor := crDefault;
        Exit;
      end else begin
        Versao := Geral.IMV(Copy(Arquivo[0], 8, Length(Arquivo[0])-7));
        if Versao < Dmod.QrControle.FieldByName('VerBcoTabs').AsInteger then
        begin
          if Geral.MB_Pergunta('A vers�o do arquivo selecionado � ' +
          'anterior ao da instalada! Deseja continuar assim mesmo?') <> ID_YES then
          begin
            Screen.Cursor := crDefault;
            Exit;
          end;
        end;
      end;

      Tabelas := TStringList.Create;
      //MeTabelas.Lines.Clear;
      try
        //for i := 1 to MeImporta.Lines.Count -1 do
        for i := 1 to Arquivo.Count -1 do
        // Arquivo[0] => Versao do arquivo
        begin
          //if MeImporta.Lines[i][1] = '<' then
          if Arquivo[i][1] = '<' then
          begin
            //Tabela := Copy(MeImporta.Lines[i], 2, Length(MeImporta.Lines[i])-2);
            Tabela := Copy(Arquivo[i], 2, Length(Arquivo[i])-2);
            Tabelas.Add(Tabela+'='+FormatFloat('0', i));
          end;
        end;
        if Tabelas.Count > 0 then
        begin
          Registros := TStringList.Create;
          try
            MeInfo.Lines.Clear;
            for i := 0 to Tabelas.Count -1 do
            begin
              n := pos('=', Tabelas[i]);
              Tabela := Copy(Tabelas[i], 1, n-1);
              n := StrToInt(Copy(Tabelas[i], n+1, Length(Tabelas[i])-n));
              //ShowMessage(Tabela + ' - ' +IntToStr(n));
              //
              Registros.Clear;
              ImportaTabela(n, Arquivo, Registros);
              //
              if Uppercase(Tabela) = 'BANCOS'    then AtualizaCNAB_Bancos    else
              if Uppercase(Tabela) = 'BANCNAB_R' then AtualizaCNAB_BanCNAB_R else
              if Uppercase(Tabela) = 'CNAB_FLD'  then AtualizaCNAB_CNAB_Fld  else
              if Uppercase(Tabela) = 'CNAB_CAG'  then AtualizaCNAB_CNAB_CaG  else
              if Uppercase(Tabela) = 'CNAB_CAD'  then AtualizaCNAB_CNAB_CaD  else
              Geral.MB_Aviso('A importa��o da tabela "' + Tabela +
              '" n�o est� implementada para ' + RGImporta.Items[RGImporta.Itemindex]);
              //
            end;
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('UPDATE controle SET VerBcoTabs=:P0');
            Dmod.QrUpd.Params[0].AsInteger := Versao;
            Dmod.QrUpd.ExecSQL;
            Dmod.ReopenControle();
            //
            Geral.MB_Info('Fim da importa��o de dados banc�rios!');
            LocCod(QrBancosCodigo.Value, QrBancosCodigo.Value);
          finally
            Registros.Free;
          end;
        end;
      finally
        Tabelas.Free;
      end;
    finally
      Arquivo.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  if FExecutouAuto then Close;
end;

{procedure TFmBancos.ImportaCNAB_Bancos(LinhaIni: Integer);
var
  i, z: Integer;
  x, y: String;
  a: String;
begin
  a := sCaminho + '/Importa.Txt';
  x := '';
  z := LinhaIni;
  while x <> '<' do
  begin
    inc(z, 1);
    if z >= MeImporta.Lines.Count then Break;
    x := MeImporta.Lines[z][1];
  end;
  inc(z, -1);
  MeRegistros.Lines.Clear;
  for i := LinhaIni+1 to z do
    MeRegistros.Lines.Add(MeImporta.Lines[i]);
  if FileExists(a) then
    DeleteFile(a);
  MeRegistros.Lines.SaveToFile(a);
end;}

procedure TFmBancos.AtualizaCNAB_Bancos;
var
  MemFile: TMemIniFile;
  RegItens: TStringList;
  i: Integer;
  //
  Codigo, DVCC, ID_400i, ID_400t: Integer;
  Nome, Site, DVB: String;
begin
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO bancos SET ');
  Dmod.QrUpdU.SQL.Add('Codigo=:P0, Nome=:P1, Site=:P2');
  //
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE bancos SET ');
  Dmod.QrUpdM.SQL.Add('DVCC=:P0, DVB=:P1, ID_400i=:P2, ID_400t=:P3 ');
  Dmod.QrUpdM.SQL.Add('WHERE Codigo=:Pa');
  //
  {
    'XLSLinha', 'XLSData', 'XLSHist','
    'XLSDocu', 'XLSHiDo', 'XLSCred',
    'XLSDebi', 'XLSCrDb', 'XLSDouC',
    'XLSComp', 'XLSCPMF', 'XLSSldo');
  }
  MemFile := TMemIniFile.Create(sArqTemp);
  try
    RegItens := TStringList.Create;
    try
      MemFile.ReadSections(RegItens);
      //MemFile.ReadSections(MeTeste.Lines);
      for i := 0 to RegItens.Count - 1 do
      begin
        //MemFile.ReadSection(RegItens[i], MeTabelas.Lines);
        MemFile.ReadSectionValues(RegItens[i], MeRegistros.Lines);
        Codigo := MemFile.ReadInteger(RegItens[i], 'Codigo', 0);
        if Codigo > 0 then
        begin
          QrPesq.Close;
          QrPesq.Params[0].AsInteger := Codigo;
          UnDmkDAC_PF.AbreQuery(QrPesq, Dmod.MyDB);
          if QrPesq.RecordCount = 0 then
          begin
            Nome := MemFile.ReadString(RegItens[i], 'Nome', '?');
            Site := MemFile.ReadString(RegItens[i], 'Site', '');
            //
            Dmod.QrUpdU.Params[00].AsInteger := Codigo;
            Dmod.QrUpdU.Params[01].AsString  := Nome;
            Dmod.QrUpdU.Params[02].AsString  := Site;
            //
            Dmod.QrUpdU.ExecSQL;
            //
            MeInfo.Lines.Add('O banco ' + FormatFloat('000', Codigo) +
            ' foi inclu�do com sucesso');
          end;
          DVCC    := MemFile.ReadInteger(RegItens[i], 'DVCC', 10);
          DVB     := MemFile.ReadString(RegItens[i], 'DVB', '?');
          ID_400i := MemFile.ReadInteger(RegItens[i], 'ID_400i', 0);
          ID_400t := MemFile.ReadInteger(RegItens[i], 'ID_400t', 0);
          //
          //if (Codigo = 104) or (Codigo=341) or (Codigo=748) then
            //ShowMessage(IntToStr(Codigo));
          Dmod.QrUpdM.Params[00].AsInteger := DVCC;
          Dmod.QrUpdM.Params[01].AsString  := DVB;
          Dmod.QrUpdM.Params[02].AsInteger := ID_400i;
          Dmod.QrUpdM.Params[03].AsInteger := ID_400t;
          Dmod.QrUpdM.Params[04].AsInteger := Codigo;
          //
          Dmod.QrUpdM.ExecSQL;
          //
          MeInfo.Lines.Add('O banco ' + FormatFloat('000', Codigo) +
          ' foi atualizado com sucesso');
        end;
      end;
    finally
      RegItens.Free;
    end;
  finally
    MemFile.Free;
  end;
end;

procedure TFmBancos.AtualizaCNAB_BanCNAB_R;
var
  MemFile: TMemIniFile;
  RegItens: TStringList;
  i: Integer;
  //
  Codigo, Registro, Seq1Ini, Seq1Tam, Seq2Ini, Seq2Tam, Envio, TamLinha: Integer;
  Seq1Val, Seq2Val: String;
begin
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('DELETE FROM bancnab_r ');
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO bancnab_r SET ');
  Dmod.QrUpdU.SQL.Add('Codigo=:P0, Registro=:P1, ');
  Dmod.QrUpdU.SQL.Add('Seq1Ini=:P2, Seq1Tam=:P3, Seq1Val=:P4, ');
  Dmod.QrUpdU.SQL.Add('Seq2Ini=:P5, Seq2Tam=:P6, Seq2Val=:P7, ');
  Dmod.QrUpdU.SQL.Add('Envio=:P8, TamLinha=:P9 ');
  //
  MemFile := TMemIniFile.Create(sArqTemp);
  try
    RegItens := TStringList.Create;
    try
      MemFile.ReadSections(RegItens);
      //MemFile.ReadSections(MeTeste.Lines);
      for i := 0 to RegItens.Count - 1 do
      begin
        //MemFile.ReadSection(RegItens[i], MeTabelas.Lines);
        MemFile.ReadSectionValues(RegItens[i], MeRegistros.Lines);
        Codigo    := MemFile.ReadInteger(RegItens[i], 'Codigo'   , 0);
        Registro  := MemFile.ReadInteger(RegItens[i], 'Registro' , 0);

        Seq1Ini   := MemFile.ReadInteger(RegItens[i], 'Seq1Ini'  , 0);
        Seq1Tam   := MemFile.ReadInteger(RegItens[i], 'Seq1Tam'  , 0);
        Seq1Val   := MemFile.ReadString (RegItens[i], 'Seq1Val'  , '');

        Seq2Ini   := MemFile.ReadInteger(RegItens[i], 'Seq2Ini'  , 0);
        Seq2Tam   := MemFile.ReadInteger(RegItens[i], 'Seq2Tam'  , 0);
        Seq2Val   := MemFile.ReadString (RegItens[i], 'Seq2Val'  , '');

        Envio     := MemFile.ReadInteger(RegItens[i], 'Envio'    , 0);
        TamLinha  := MemFile.ReadInteger(RegItens[i], 'TamLinha' , 0);
        //
        Dmod.QrUpdU.Params[00].AsInteger := Codigo;
        Dmod.QrUpdU.Params[01].AsInteger := Registro;
        Dmod.QrUpdU.Params[02].AsInteger := Seq1Ini;
        Dmod.QrUpdU.Params[03].AsInteger := Seq1Tam;
        Dmod.QrUpdU.Params[04].AsString  := Seq1Val;
        Dmod.QrUpdU.Params[05].AsInteger := Seq2Ini;
        Dmod.QrUpdU.Params[06].AsInteger := Seq2Tam;
        Dmod.QrUpdU.Params[07].AsString  := Seq2Val;
        Dmod.QrUpdU.Params[08].AsInteger := Envio;
        Dmod.QrUpdU.Params[09].AsInteger := TamLinha;
        //
        Dmod.QrUpdU.ExecSQL;
        //
        MeInfo.Lines.Add('A composi��o de arquivo CNAB  ' +
        FormatFloat('000', Codigo) + '.' + FormatFloat('0', Registro) +
        ' foi inclu�da com sucesso');
      end;
    finally
      RegItens.Free;
    end;
  finally
    MemFile.Free;
  end;
end;

procedure TFmBancos.AtualizaCNAB_CNAB_Fld;
var
  MemFile: TMemIniFile;
  RegItens: TStringList;
  i: Integer;
  //
  Codigo: Integer;
  Nome: String;
begin
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('DELETE FROM cnab_fld ');
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO cnab_fld SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, ');
  Dmod.QrUpdU.SQL.Add('Codigo=:Pa');
  //
  MemFile := TMemIniFile.Create(sArqTemp);
  try
    RegItens := TStringList.Create;
    try
      MemFile.ReadSections(RegItens);
      //MemFile.ReadSections(MeTeste.Lines);
      for i := 0 to RegItens.Count - 1 do
      begin
        //MemFile.ReadSection(RegItens[i], MeTabelas.Lines);
        MemFile.ReadSectionValues(RegItens[i], MeRegistros.Lines);
        Codigo   := MemFile.ReadInteger(RegItens[i], 'Codigo'  , 0);
        //
        Nome     := MemFile.ReadString (RegItens[i], 'Nome'     , '');
        //
        Dmod.QrUpdU.Params[00].AsString  := Nome;
        //
        Dmod.QrUpdU.Params[01].AsInteger := Codigo;
        Dmod.QrUpdU.ExecSQL;
        //
        MeInfo.Lines.Add('O grupo de registros banc�rio de arquivo CNAB  ' +
        FormatFloat('000', Codigo) + '.' + Nome + ' foi inclu�do com sucesso');
      end;
    finally
      RegItens.Free;
    end;
  finally
    MemFile.Free;
  end;
end;

procedure TFmBancos.AtualizaCNAB_CNAB_CaG;
var
  MemFile: TMemIniFile;
  RegItens: TStringList;
  i: Integer;
  //
  Codigo: Integer;
  Nome, ID: String;
begin
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('DELETE FROM cnab_cag ');
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO cnab_cag SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, ID=:P1, ');
  Dmod.QrUpdU.SQL.Add('Codigo=:Pa');
  //
  MemFile := TMemIniFile.Create(sArqTemp);
  try
    RegItens := TStringList.Create;
    try
      MemFile.ReadSections(RegItens);
      //MemFile.ReadSections(MeTeste.Lines);
      for i := 0 to RegItens.Count - 1 do
      begin
        //MemFile.ReadSection(RegItens[i], MeTabelas.Lines);
        MemFile.ReadSectionValues(RegItens[i], MeRegistros.Lines);
        Codigo   := MemFile.ReadInteger(RegItens[i], 'Codigo'  , 0);
        //
        Nome     := MemFile.ReadString (RegItens[i], 'Nome'     , '');
        ID       := MemFile.ReadString (RegItens[i], 'ID'       , '');
        //
        Dmod.QrUpdU.Params[00].AsString  := Nome;
        Dmod.QrUpdU.Params[01].AsString  := ID;
        //
        Dmod.QrUpdU.Params[02].AsInteger := Codigo;
        Dmod.QrUpdU.ExecSQL;
        //
        MeInfo.Lines.Add('O grupo de registros banc�rio de arquivo CNAB  ' +
        FormatFloat('000', Codigo) + '.' + Nome + ' foi inclu�do com sucesso');
      end;
    finally
      RegItens.Free;
    end;
  finally
    MemFile.Free;
  end;
end;

procedure TFmBancos.AtualizaCNAB_CNAB_CaD;
var
  MemFile: TMemIniFile;
  RegItens: TStringList;
  i: Integer;
  //
  PadrIni, PadrTam, AlfaNum, FmtInv, Registro, Envio, Campo, Casas, T240, T400,
  BcoOrig: Integer;
  SecoVal, Formato, PadrVal, Ajuda, Nome, Signific, ID: String;
begin
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('DELETE FROM cnab_cad ');
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO cnab_cad SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Signific=:P1, PadrVal=:P2, Ajuda=:P3, ');
  Dmod.QrUpdU.SQL.Add('PadrIni=:P4, PadrTam=:P5, AlfaNum=:P6, FmtInv=:P7, ');
  Dmod.QrUpdU.SQL.Add('Registro=:P8, Envio=:P09, Campo=:P10, SecoVal=:P11, ');
  Dmod.QrUpdU.SQL.Add('Formato=:P12, Casas=:P13, T240=:P14, T400=:P15, ');
  Dmod.QrUpdU.SQL.Add('BcoOrig=:Pa, ID=:Pb ');
  //
  MemFile := TMemIniFile.Create(sArqTemp);
  try
    RegItens := TStringList.Create;
    try
      MemFile.ReadSections(RegItens);
      //MemFile.ReadSections(MeTeste.Lines);
      for i := 0 to RegItens.Count - 1 do
      begin
        //MemFile.ReadSection(RegItens[i], MeTabelas.Lines);
        MemFile.ReadSectionValues(RegItens[i], MeRegistros.Lines);
        PadrIni  := MemFile.ReadInteger(RegItens[i], 'PadrIni'  , 0);
        PadrTam  := MemFile.ReadInteger(RegItens[i], 'PadrTam'  , 0);
        AlfaNum  := MemFile.ReadInteger(RegItens[i], 'AlfaNum'  , 0);
        FmtInv   := MemFile.ReadInteger(RegItens[i], 'FmtInv'   , 0);
        Registro := MemFile.ReadInteger(RegItens[i], 'Registro' , 0);
        Envio    := MemFile.ReadInteger(RegItens[i], 'Envio'    , 0);
        Campo    := MemFile.ReadInteger(RegItens[i], 'Campo'    , 0);
        Casas    := MemFile.ReadInteger(RegItens[i], 'Casas'    , 0);
        T240     := MemFile.ReadInteger(RegItens[i], 'T240'     , 0);
        T400     := MemFile.ReadInteger(RegItens[i], 'T400'     , 0);
        BcoOrig  := MemFile.ReadInteger(RegItens[i], 'BcoOrig'  , 0);
        //
        SecoVal  := MemFile.ReadString (RegItens[i], 'SecoVal'  , '');
        Formato  := MemFile.ReadString (RegItens[i], 'Formato'  , '');
        PadrVal  := MemFile.ReadString (RegItens[i], 'PadrVal'  , '');
        Ajuda    := MemFile.ReadString (RegItens[i], 'Ajuda'    , '');
        Nome     := MemFile.ReadString (RegItens[i], 'Nome'     , '');
        Signific := MemFile.ReadString (RegItens[i], 'Signific' , '');
        ID       := MemFile.ReadString (RegItens[i], 'ID'       , '');
        //
        Dmod.QrUpdU.Params[00].AsString  := Nome;
        Dmod.QrUpdU.Params[01].AsString  := Signific;
        Dmod.QrUpdU.Params[02].AsString  := PadrVal;
        Dmod.QrUpdU.Params[03].AsString  := Ajuda;
        Dmod.QrUpdU.Params[04].AsInteger := PadrIni;
        Dmod.QrUpdU.Params[05].AsInteger := PadrTam;
        Dmod.QrUpdU.Params[06].AsInteger := AlfaNum;
        Dmod.QrUpdU.Params[07].AsInteger := FmtInv;
        Dmod.QrUpdU.Params[08].AsInteger := Registro;
        Dmod.QrUpdU.Params[09].AsInteger := Envio;
        Dmod.QrUpdU.Params[10].AsInteger := Campo;
        Dmod.QrUpdU.Params[11].AsString  := SecoVal;
        Dmod.QrUpdU.Params[12].AsString  := Formato;
        Dmod.QrUpdU.Params[13].AsInteger := Casas;
        Dmod.QrUpdU.Params[14].AsInteger := T240;
        Dmod.QrUpdU.Params[15].AsInteger := T400;
        //
        Dmod.QrUpdU.Params[16].AsInteger := BcoOrig;
        Dmod.QrUpdU.Params[17].AsString  := ID;
        Dmod.QrUpdU.ExecSQL;
        //
        MeInfo.Lines.Add('O registro banc�rio de arquivo CNAB  ' +
        FormatFloat('000', BcoOrig) + '.' + ID + ' foi inclu�do com sucesso');
      end;
    finally
      RegItens.Free;
    end;
  finally
    MemFile.Free;
  end;
end;

procedure TFmBancos.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FExecutouAuto  := True;
  ImportaCNAB_DeArquivo(ExtractFilePath(Application.ExeName) +'Bancos.txt');
end;

procedure TFmBancos.ImportaThisCNAB;
  procedure AdicionaCampo(Nome, Signific, PadrVal, Ajuda: String; PadrIni,
  PadrTam, AlfaNum, FmtInv, Registro, Envio, Campo: Integer; SecoVal,
  Formato: String; Casas, T240, T400, BcoOrig: Integer; ID: String);
  begin
    Dmod.QrUpdU.Params[00].AsString  := Nome;
    Dmod.QrUpdU.Params[01].AsString  := Signific;
    Dmod.QrUpdU.Params[02].AsString  := PadrVal;
    Dmod.QrUpdU.Params[03].AsString  := Ajuda;
    Dmod.QrUpdU.Params[04].AsInteger := PadrIni;
    Dmod.QrUpdU.Params[05].AsInteger := PadrTam;
    Dmod.QrUpdU.Params[06].AsInteger := AlfaNum;
    Dmod.QrUpdU.Params[07].AsInteger := FmtInv;
    Dmod.QrUpdU.Params[08].AsInteger := Registro;
    Dmod.QrUpdU.Params[09].AsInteger := Envio;
    Dmod.QrUpdU.Params[10].AsInteger := Campo;
    Dmod.QrUpdU.Params[11].AsString  := SecoVal;
    Dmod.QrUpdU.Params[12].AsString  := Formato;
    Dmod.QrUpdU.Params[13].AsInteger := Casas;
    Dmod.QrUpdU.Params[14].AsInteger := T240;
    Dmod.QrUpdU.Params[15].AsInteger := T400;
    Dmod.QrUpdU.Params[16].AsInteger := BcoOrig;
    Dmod.QrUpdU.Params[17].AsString  := ID;
    Dmod.QrUpdU.ExecSQL;
  end;
  //
  procedure AdicionaRegistro(Banco, Registro, Envio, TamLinha: Integer;
  Seq1Val: String);
  begin
    Dmod.QrUpdM.Params[00].AsInteger := 1;
    Dmod.QrUpdM.Params[01].AsInteger := 1;
    Dmod.QrUpdM.Params[02].AsString  := Seq1Val;
    Dmod.QrUpdM.Params[03].AsInteger := 0;
    Dmod.QrUpdM.Params[04].AsInteger := 0;
    Dmod.QrUpdM.Params[05].AsString  := '';
    Dmod.QrUpdM.Params[06].AsInteger := Envio;
    Dmod.QrUpdM.Params[07].AsInteger := TamLinha;
    Dmod.QrUpdM.Params[08].AsInteger := Registro;
    Dmod.QrUpdM.Params[09].AsInteger := Banco;
    Dmod.QrUpdM.ExecSQL;
  end;
  //
var
  Banco, i, PadrIni, PadrTam, Registro, Envio, TamLinha, AlfaNum,
  FmtInv, Casas, Campo, T240, T400, ID_i, ID_f: Integer;
  Seq1Val, Aux, Nome, Signific, Ajuda, PadrVal, Formato, SecoVal, ID: String;
begin
  Registro := 0;
  Envio    := 0;
  if OpenDialog5.Execute then
  begin
    if MyObjects.Xls_To_StringGrid(Grade, OpenDialog5.FileName, PB1, LaAviso, nil) then
    //if Xls_To_StringGrid(Grade, OpenDialog5.FileName) then
    begin
      Banco := Geral.IMV(Grade.Cells[06,01]);
      if Banco <> QrBancosCodigo.Value then
      begin
       Geral.MB_Aviso('Importa��o cancelada! O banco do ' +
       'arquivo (' + FormatFloat('000', Banco) + ') n�o � o mesmo que o ' +
       'selecionado (' + FormatFloat('000', QrBancosCodigo.Value) + ')!');
       Exit;
      end;
      if Geral.MB_Pergunta('Todos os registros do banco ' +
      FormatFloat('000', QrBancosCodigo.Value) + ' ser�o exclu�dos para ' +
      'adicionar os registros carregados do arquivo "' +
      OpenDialog5.FileName + '". Deseja continuar assim mesmo?') = ID_YES then
      begin
        Screen.Cursor := crHourGlass;
        // Exclui registros aqui
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM bancnab_r WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrBancosCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM cnab_cad WHERE BcoOrig=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrBancosCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //

        TamLinha := Geral.IMV(Grade.Cells[05,01]);
        if TamLinha = 400 then
        begin
          ID_i := Geral.IMV(Grade.Cells[01,01]);
          ID_f := Geral.IMV(Grade.Cells[03,01]);
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE bancos SET ID_400i=:P0, ID_400t=:P1');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2');
          Dmod.QrUpd.Params[00].AsInteger := ID_i;
          Dmod.QrUpd.Params[01].AsInteger := ID_f;
          Dmod.QrUpd.Params[02].AsInteger := Banco;
          Dmod.QrUpd.ExecSQL;
        end;
        //

        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('INSERT INTO bancnab_r SET ');
        Dmod.QrUpdM.SQL.Add('Seq1Ini=:P0, Seq1Tam=:P1, Seq1Val=:P2, ');
        Dmod.QrUpdM.SQL.Add('Seq2Ini=:P3, Seq2Tam=:P4, Seq2Val=:P5, ');
        Dmod.QrUpdM.SQL.Add('Envio=:P6, TamLinha=:P7, ');
        Dmod.QrUpdM.SQL.Add('Registro=:Pa, Codigo=:Pb ');
        //
        Dmod.QrUpdU.SQL.Clear;
        Dmod.QrUpdU.SQL.Add('INSERT INTO cnab_cad SET ');
        Dmod.QrUpdU.SQL.Add('Nome=:P0, Signific=:P1, PadrVal=:P2, Ajuda=:P3, ');
        Dmod.QrUpdU.SQL.Add('PadrIni=:P4, PadrTam=:P5, AlfaNum=:P6, FmtInv=:P7, ');
        Dmod.QrUpdU.SQL.Add('Registro=:P8, Envio=:P09, Campo=:P10, SecoVal=:P11, ');
        Dmod.QrUpdU.SQL.Add('Formato=:P12, Casas=:P13, T240=:P14, T400=:P15, ');
        Dmod.QrUpdU.SQL.Add('BcoOrig=:Pa, ID=:Pb ');
        //
        for i := 4 to Grade.RowCount -1 do
        begin
          Aux := Grade.Cells[01, i];
          if Length(Aux) > 0 then
          begin
            if (Aux[1] = '[') then
            begin
              inc(Registro, 1);
              Aux := Copy(Aux, 2, pos(']', Aux)-2);
              Seq1Val := Aux;
              Envio  := Geral.IMV(Grade.Cells[05, i]);
              TamLinha := Geral.IMV(Grade.Cells[03, i]);
              AdicionaRegistro(Banco, Registro, Envio, TamLinha, Seq1Val);
            end else begin
              PadrIni := Geral.IMV(Grade.Cells[01, i]);
              if PadrIni > 0 then
              begin
                Nome     := Grade.Cells[02, i];
                PadrTam  := Geral.IMV(Grade.Cells[03, i]);
                Signific := Grade.Cells[04, i];
                AlfaNum  := Geral.BoolToInt(Grade.Cells[05, i][1] in (['0'..'9']));
                PadrVal  := Grade.Cells[07, i];
                FmtInv   := 0;
                Campo    := Geral.IMV(Grade.Cells[06, i]);
                SecoVal  := '';
                Formato  := Grade.Cells[08, i];
                Casas    := Geral.IMV(Grade.Cells[09, i]);
                T240     := Geral.BoolToInt(TamLinha = 240);
                T400     := Geral.BoolToInt(TamLinha = 400);
                ID       := Seq1Val + '.' + FormatFloat('000', PadrIni);
                Ajuda    := Grade.Cells[10, i]; // ver

                AdicionaCampo(Nome, Signific, PadrVal, Ajuda, PadrIni,
                PadrTam, AlfaNum, FmtInv, Registro, Envio, Campo, SecoVal,
                Formato, Casas, T240, T400, Banco, ID);
              end;
            end;
          end;
        end;
        LocCod(Banco, Banco);
        PageControl1.ActivePageIndex := 2;
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

end.

