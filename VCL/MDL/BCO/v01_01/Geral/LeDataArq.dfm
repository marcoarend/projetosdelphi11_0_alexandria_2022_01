object FmLeDataArq: TFmLeDataArq
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: An'#225'lise de  Arquivo de transfer'#234'ncia de dados'
  ClientHeight = 496
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 428
    Width = 1008
    Height = 68
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 66
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 6
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 336
      Height = 66
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Label6: TLabel
        Left = 244
        Top = 4
        Width = 87
        Height = 13
        Caption = 'Texto pesquisado:'
      end
      object Label4: TLabel
        Left = 200
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Tam.:'
      end
      object Label3: TLabel
        Left = 160
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Coluna:'
      end
      object Label2: TLabel
        Left = 120
        Top = 4
        Width = 29
        Height = 13
        Caption = 'Linha:'
      end
      object Edit4: TdmkEdit
        Left = 200
        Top = 20
        Width = 37
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = Edit4Change
      end
      object Edit3: TdmkEdit
        Left = 160
        Top = 20
        Width = 37
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = Edit3Change
      end
      object Edit2: TdmkEdit
        Left = 120
        Top = 20
        Width = 37
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = Edit2Change
      end
      object BtAcoes: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&A'#231#245'es'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtAcoesClick
      end
      object StatusBar: TStatusBar
        Left = 0
        Top = 47
        Width = 336
        Height = 19
        Panels = <
          item
            Width = 120
          end
          item
            Alignment = taCenter
            Width = 120
          end
          item
            Width = 50
          end>
      end
      object BtSubstitui: TButton
        Left = 244
        Top = 20
        Width = 89
        Height = 25
        Caption = 'Substituir'
        TabOrder = 5
        OnClick = BtSubstituiClick
      end
    end
    object Panel6: TPanel
      Left = 337
      Top = 1
      Width = 559
      Height = 66
      Align = alClient
      TabOrder = 2
      object Label7: TLabel
        Left = 8
        Top = 36
        Width = 48
        Height = 13
        Caption = 'Arquivo 2:'
      end
      object Label8: TLabel
        Left = 8
        Top = 12
        Width = 48
        Height = 13
        Caption = 'Arquivo 1:'
      end
      object EdPesq1: TEdit
        Left = 60
        Top = 8
        Width = 493
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object EdPesq2: TEdit
        Left = 60
        Top = 32
        Width = 493
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 428
    Align = alClient
    TabOrder = 0
    ExplicitTop = 48
    ExplicitHeight = 380
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 1006
      Height = 426
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      OnChange = PageControl1Change
      ExplicitHeight = 378
      object TabSheet1: TTabSheet
        Caption = 'Carrega arquivo'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 350
        object PageControl2: TPageControl
          Left = 0
          Top = 0
          Width = 998
          Height = 398
          ActivePage = TabSheet4
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 350
          object TabSheet4: TTabSheet
            Caption = ' Arquivo 1 '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 322
            object Panel3: TPanel
              Left = 0
              Top = 329
              Width = 990
              Height = 41
              Align = alBottom
              Caption = 'Panel3'
              ParentBackground = False
              TabOrder = 0
              ExplicitTop = 281
              object Label1: TLabel
                Left = 4
                Top = 16
                Width = 39
                Height = 13
                Caption = 'Arquivo:'
              end
              object SBArq1: TSpeedButton
                Left = 752
                Top = 12
                Width = 23
                Height = 22
                OnClick = SBArq1Click
              end
              object EdArq1: TEdit
                Left = 48
                Top = 12
                Width = 697
                Height = 21
                TabOrder = 0
                Text = 'C:\BancoBrasil\bbtransf\Remessa\*.*'
                OnChange = EdArq1Change
              end
              object RGQuebraLinha1: TRadioGroup
                Left = 780
                Top = 1
                Width = 209
                Height = 39
                Caption = ' Quebra de linha: '
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  '10'
                  '13'
                  '1013'
                  '1310')
                TabOrder = 1
              end
            end
            object Editor1: TRichEdit
              Left = 0
              Top = 0
              Width = 990
              Height = 329
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              WordWrap = False
              OnKeyUp = Editor1KeyUp
              OnMouseDown = Editor1MouseDown
              OnMouseUp = Editor1MouseUp
            end
          end
          object TabSheet5: TTabSheet
            Caption = ' Arquivo 2 '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel4: TPanel
              Left = 0
              Top = 329
              Width = 990
              Height = 41
              Align = alBottom
              Caption = 'Panel3'
              ParentBackground = False
              TabOrder = 0
              ExplicitTop = 281
              object Label5: TLabel
                Left = 4
                Top = 16
                Width = 39
                Height = 13
                Caption = 'Arquivo:'
              end
              object SBArq2: TSpeedButton
                Left = 752
                Top = 12
                Width = 23
                Height = 22
                OnClick = SBArq2Click
              end
              object EdArq2: TEdit
                Left = 48
                Top = 12
                Width = 697
                Height = 21
                TabOrder = 0
                Text = 'C:\BancoBrasil\bbtransf\Remessa\*.*'
                OnChange = EdArq2Change
              end
              object RgQuebraLinha2: TRadioGroup
                Left = 780
                Top = 1
                Width = 209
                Height = 39
                Caption = ' Quebra de linha: '
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  '10'
                  '13'
                  '1013'
                  '1310')
                TabOrder = 1
              end
            end
            object Editor2: TRichEdit
              Left = 0
              Top = 0
              Width = 990
              Height = 329
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              WordWrap = False
              OnKeyUp = Editor2KeyUp
              OnMouseDown = Editor2MouseDown
              OnMouseUp = Editor2MouseUp
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Arquivo resultante'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object MeRes: TRichEdit
          Left = 0
          Top = 0
          Width = 998
          Height = 350
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          WordWrap = False
          OnKeyUp = Editor1KeyUp
          OnMouseDown = Editor1MouseDown
          OnMouseUp = Editor1MouseUp
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Comparativo linhas arquivo '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 998
          Height = 61
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Label18: TLabel
            Left = 8
            Top = 12
            Width = 46
            Height = 13
            Caption = 'Aquivo A:'
          end
          object Label19: TLabel
            Left = 8
            Top = 36
            Width = 46
            Height = 13
            Caption = 'Aquivo B:'
          end
          object SB_A: TSpeedButton
            Left = 564
            Top = 8
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SB_AClick
          end
          object SB_B: TSpeedButton
            Left = 564
            Top = 32
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SB_BClick
          end
          object Label20: TLabel
            Left = 592
            Top = 12
            Width = 149
            Height = 13
            Caption = 'Tamanho linha (em caracteres):'
          end
          object Label9: TLabel
            Left = 596
            Top = 36
            Width = 143
            Height = 13
            Caption = 'Qtde de caracteres diferentes:'
          end
          object EditA: TEdit
            Left = 60
            Top = 8
            Width = 500
            Height = 21
            TabOrder = 0
            Text = 'C:\BancoBrasil\bbtransf\Remessa\*.*'
          end
          object Button1: TButton
            Left = 820
            Top = 16
            Width = 75
            Height = 25
            Caption = 'Compara'
            TabOrder = 1
            OnClick = Button1Click
          end
          object EditB: TEdit
            Left = 60
            Top = 32
            Width = 500
            Height = 21
            TabOrder = 2
            Text = 'C:\BancoBrasil\bbtransf\Remessa\*.*'
          end
          object EdTamLin: TdmkEdit
            Left = 748
            Top = 8
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '241'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 241
            ValWarn = False
          end
          object EdQtdLinDif: TdmkEdit
            Left = 744
            Top = 32
            Width = 61
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object MemoA: TRichEdit
          Left = 0
          Top = 61
          Width = 289
          Height = 289
          Align = alLeft
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 1
        end
        object MemoB: TRichEdit
          Left = 289
          Top = 61
          Width = 289
          Height = 337
          Align = alLeft
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 2
        end
        object MemoC: TRichEdit
          Left = 578
          Top = 61
          Width = 420
          Height = 337
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 3
        end
      end
    end
  end
  object PMAcoes: TPopupMenu
    OnPopup = PMAcoesPopup
    Left = 29
    Top = 401
    object EliminarodigitoverificadordoNOSSONMERO1: TMenuItem
      Caption = '&Eliminar o d'#237'gito verificador do NOSSO N'#218'MERO'
      OnClick = EliminarodigitoverificadordoNOSSONMERO1Click
    end
    object Salvararquivogeradocomo1: TMenuItem
      Caption = 'Salvar arquivo gerado &como ...'
      Enabled = False
      OnClick = Salvararquivogeradocomo1Click
    end
  end
  object QrField: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cad.PadrIni, PadrTam'
      'FROM cnab_cad cad'
      'LEFT JOIN cnab_cag cag ON cag.Codigo=cad.Registro'
      'WHERE cag.ID=1'
      'AND cad.Campo=501'
      'AND cad.BcoOrig=:P0')
    Left = 185
    Top = 173
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFieldPadrIni: TIntegerField
      FieldName = 'PadrIni'
      Required = True
    end
    object QrFieldPadrTam: TIntegerField
      FieldName = 'PadrTam'
      Required = True
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 69
    Top = 189
  end
  object PMSubstitui: TPopupMenu
    Left = 272
    Top = 400
    object N121: TMenuItem
      Caption = '1 > 2'
      OnClick = N121Click
    end
    object N211: TMenuItem
      Caption = '2 > 1'
      OnClick = N211Click
    end
  end
end
