unit UnBancos;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, dmkGeral, UnDmkEnums, UnDmkProcFunc,
  UnProjGroup_Consts;

type
  TCNABFormatType = (cftText, cftInte, cftFlot, cftDate, cftTime);
  TCNABResult = (cresNoCNAB, cresNoBank, cresNoField, cresOKField,
    cresNoLayoutName);
  TUBancos = class(TObject)
  private
    { Private declarations }
    function FFV3(const Casas: Integer; const Num: String; var Res: String;
             const Mensagem: String ): Boolean;
  public
    { Public declarations }
    //  Inicio Movidos do  M L A G e r a l
    function  TamanhoLinhaCNAB_Posicoes(Index: Integer): Integer;
    function  TamanhoLinhaCNAB_Index(Posicoes: Integer): Integer;
    function  TamanhoLinhaCNAB_Valor(T240, T400: Integer): Integer;
    function  CNAB240Envio(Envio: Integer): String;
    function  CNAB240TipoArqRetBco(Tipo: String): String;
    function  CNABTipoDeMovimento(Banco: Integer; Envio: TEnvioCNAB;
              Movimento, TamReg: Integer; Registrado: Boolean;
              NomeLayout: String): String;
    function  CNABMotivosDeTipoDeMovimento(Banco, Movimento, Motivo,
              TamReg: Integer; Envio: TEnvioCNAB; NomeLayout: String): String;
    function  CNABMotivosDeTipoDeMovimento28(Banco: Integer;
              Motivos: String): String;
    function  CNABEspecieDeDocumento(Banco: Integer; Especie: String): String;
    function  CNABRetRegistrosOK(Banco, QtdPos: Integer; FLista: TStrings;
              Arquivo: String): Boolean;
    function  CNABRetEhLoteServico(Banco, QtdPos: Integer; FLista: TStrings;
              Arquivo: String): Boolean;
    function  CNABRetEhHeaderArq(Banco, QtdPos: Integer; FLista: TStrings;
              Arquivo: String): Boolean;
    function  CNABRetEhRetorno(Banco, QtdPos: Integer; FLista: TStrings;
              Arquivo: String): Boolean;
    function  CNABRetQtdeLots(Banco, QtdPos: Integer; FLista: TStrings;
              Arquivo: String): String;
    function  CNABRetCNPJ(Banco, QtdPos: Integer; FLista: TStrings;
              Arquivo: String): String;
    function  CNABRetCodCedente(Banco, QtdPos: Integer; FLista: TStrings;
              Arquivo: String): String;
    //  FIM Movidos do  M L A G e r a l
    function CNABEnvio_ecnabToTxt(Envio: TEnvioCNAB): String;
    function BancoImplementado(Banco, CNAB: Integer; Envio: TEnvioCNAB): Boolean;
    function BancoImplemMultiEmp(Banco, CNAB: Integer; Envio: TEnvioCNAB): Boolean;
    function EhCodigoLiquidacao(CodLiq, Banco, TamReg: Integer; NomeLayout: String): Boolean;
    function CodigoTarifa(Banco: Integer; NomeLayout: String): Integer;
    function ContaDaOcorrencia(const Banco, TamReg: Integer;
             const Ocorrencia, LayoutRem: String; var Genero: Integer): Boolean;
    function FatorDeRecebimento(Banco: Integer;
             Pago, Tarifa, ValBloqueto: Double; var Fator: Double): Boolean;
    function DiferencaDeRecebimento(Banco: Integer;
             Pago, Tarifa, SomaVal: Double; var Diferenca: Double): Boolean;
    function FatorMultaDeRecebimento(Banco: Integer;
             Pago, Tarifa, ValMulta: Double; var Multa: Double): Boolean;
    function GeraBloquetoNumero(const Banco: Integer; const BloquetoAtual: Double;
             const Pergunta: Boolean;
             const NosNumFxaU: Boolean; // 2012-08-24 Itau
             const NosNumFxaI, NosNumFxaF: Integer; // 2012-08-24 Itau
             var NumeroBloqueto: Double; NomeLayout: String): Boolean;
    (*function GeraAgenciaCodigoCedente(Banco, Agencia, Posto: Integer;
             Conta, CodigoCedente: String): String;*)
    function DACNossoNumero(const CadTitBco, Banco, Agencia, Posto:
             Integer; const Sequencial: Double; const ContaCorrente, Carteira, IDCobranca,
             CodigoCedente: String; TipoCobranca: Integer;
             EspecieDoc: String;// 2012-04-27 Bco 399
             CNAB: Integer; // 2012-05-01 Bco 033(008/353)
             CtaCooper: String; // 2012-05-10 Bco 237
             Layout: String; // 2012-11-01
             var DACNossoNumero: String): Boolean;
    function GeraNossoNumero(const CadTitBco, Banco, Agencia, Posto: Integer;
             const Sequencial: Double;
             const ContaCorrente, Carteira, IDCobranca, CodigoCedente: String;
             Vencimento: TDateTime; TipoCobranca: Integer;
             EspecieDoc: String; // 2012-04-27 Bco 399
             CNAB: Integer; // 2012-05-01 Bco 033(008/353)
             CtaCooper: String; // 2012-05-10 Bco 237
             NomeLayout: String; //2014-04-23 Bco 756
             var NossoNumero, NossoNumeroRem: String): Boolean;
    function CodigoDeBarra_BoletoDeCobranca(Banco, Agencia, CorresBco,
             CorresAge: Integer; DVAgencia: String; Posto: Integer;
             ContaCorrente, DVContaC, CorresCto: String; Moeda, TipoCobranca,
             TipoCarteira: Integer; NossoNumero, CodigoCedente, Carteira,
             CartImp, IDCobranca, OperCodi: String; Vencto: TDateTime;
             Valor: Double; ParcTit, IOF: Integer; MostraValorEVencto: Boolean;
             ModalCobr: Integer; NomeLayout: String): String;
    function LinhaDigitavel_BoletoDeCobranca(CodigoBarra: String): String;
    function DigitoVerificadorCodigoBanco(Banco: Integer): String;
    function BancoTemEntidade(Banco: Integer): Boolean;
    function SeparaJurosEMultaImplementado(Banco: Integer; NomeLayout: String; var Separa: Boolean): Boolean;
    function InformaTarifaDeCobrancaImplementado(Banco: Integer; var Informa: Boolean): Boolean;
    function LocDado240(const Banco, Campo, Linha: Integer; const Lista:
             TStrings; const Mensagem, NomeLayout: String; var Res: String): Boolean;
    function LocDado400(const Banco, Campo, Linha: Integer; const Lista:
             TStrings; const Mensagem, NomeLayout: String; var Res: String): Boolean;
    function FormataLocDado(const Dado, Formato: String;
             const Tipo, Tam, Cas: Integer; var Res: String): Boolean;
    function ValorPadrao(TamReg, Banco, Campo: Integer; NomeLayout: String = ''): String;
    {
    function CriaSelLista(Lista: MyArrayLista; TituloForm: String;
             TitulosColunas: array of String): Boolean;
    function SelecionaItem(Lista: MyArrayLista; TituloForm: String;
             TitulosColunas: array of String): String;
    }
    function DescricaoDoMeuCodigoCNAB(Codigo: Integer): String;
    procedure RecarregaMesuCodigosDeCamposCNAB();
    function CNABResult_Msg(CNABResult: TCNABResult; CNAB, Banco, Registro:
             Integer; Envio: TEnvioCNAB): String;
    function TipoArqCNAB(const Arquivo: String; const Lista: TStrings;
             var CNAB: Integer): Boolean;
    function OcorrenciaConhecida(Banco: Integer; Envio: TEnvioCNAB;
             Movimento, TamReg: Integer; Registrado: Boolean;
             NomeLayout: String): Boolean;
    procedure MostraFormBancos(Banco: Integer);
    procedure ConfiguraTipoCobranca(RGTipoCobranca: TRadioGroup; Colunas, Default: Integer);
  end;

var
  UBancos: TUBancos;

const
  CO_CNAB_HIGH_COD_OCORRENCIA = 999; // Na averdade atualmente � 99 mas por setado 999 por precau��o!
  CO_399_MODULO_I_2009_10 = 'MODULO I - 2009/10';
  CO_033_RECEBIMENTOS_240_v01_07_2009_06 = 'RECEBIMENTOS CNAB 240 V01.07 - 2009/06';
  CO_033_RECEBIMENTOS_400_v02_00_2009_10 = 'RECEBIMENTOS CNAB 400 V02.00 - 2009/10';
  CO_341_COBRANCA_BANCARIA_MARCO_2012 = 'COBRAN�A BANC�RIA - MAR�O 2012';
  CO_341_COBRANCA_BANCARIA_SETEMBRO_2012 = 'COBRAN�A BANC�RIA - SETEMBRO 2012';
  CO_756_EXCEL_2012_03_14 = 'EXCEL 14/03/2012';
  CO_756_EXCEL_2013_07_18 = 'EXCEL 18/07/2013';
  CO_756_CORRESPONDENTE_BRADESCO_2015 = '756 - CORRESPONDENTE BRADESCO - 2015';
  CO_756_CORRESPONDENTE_BB_2015 = '756 - CORRESPONDENTE BB - 2015';
  CO_756_240_2018 = 'COBRAN�A BANC�RIA - 2018';
  CO_237_MP_4008_0121_01_Data_11_11_2010 = 'MP_4008-0121-01 DATA: 11/11/2010';

implementation

uses UnMyObjects, Module, Bancos, Opcoes, MyDBCheck, MyListas, DmkDAC_PF;

function TUBancos.BancoImplementado(Banco, CNAB: Integer; Envio: TEnvioCNAB): Boolean;
{Sequ�ncia de implementa��es
  1. UBancos.BancoImplementado
  2.}
begin
  Result := False;
  case Envio of
    ecnabRemessa: // Remessa
    begin
      case CNAB of
        240:
        begin
          case Banco of
            001: Result := True;
            756: Result := True;
            else Result := False;
          end;
        end;
        400:
        begin
          case Banco of
            001: Result := True;
            008, 033, 353: Result := True;
            //104: Result := True;
            237: Result := True;
            341: Result := True;
            399: Result := True;
            //748: Result := True;
            756: Result := True;
          end;
        end;
      end;
    end;
    ecnabRetorno:
    begin
      case CNAB of
        240: Result := True; // Todos
        400:
        begin
          case Banco of
            001: Result := True;
            008, 033, 353: Result := True;
            104: Result := True;
            237: Result := True;
            341: Result := True;
            399: Result := True;
            409: Result := True;
            748: Result := True;
            756: Result := True;
          end;
        end;
      end;
    end;
    ecnabBloquet: // Gera��o de bloquetos
    begin
      case Banco of
        001: Result := True;
        008, 033, 353: Result := True;
        104: Result := True;
        237: Result := True;
        341: Result := True;
        399: Result := True;
        409: Result := True;
        748: Result := True;
        756: Result := True;
      end;
    end;
  end;
  //
  if not Result then Geral.MB_Aviso('O banco ' + FormatFloat(
  '000', Banco) + ' n�o est� implementado para ' +
  CNABEnvio_ecnabToTxt(Envio) + FormatFloat('000;-000; ',
  CNAB) + '!' + sLineBreak +
  'Avise a DERMATEK (UBancos.BancoImplementado)!');
  //Result := True;
end;

function TUBancos.EhCodigoLiquidacao(CodLiq, Banco, TamReg: Integer;
  NomeLayout: String): Boolean;
begin
  Result := False;
  if TamReg = 240 then
  begin
      // Igual para todos?
      case Banco of
        001: Result := CodLiq in [06,17];
        008, 033, 353: Result := CodLiq =17;
        756:
        begin
          if NomeLayout = CO_756_CORRESPONDENTE_BB_2015 then
            Result := CodLiq in  [06,17]
          else
            Result := CodLiq = 99;
        end;
        else Result := CodLiq = 06;
      end;
  end else begin
    case banco of
      001: Result := CodLiq = 06;
      008, 033, 353: Result := CodLiq = 06;
      104: Result := CodLiq = 21;
      237: Result := CodLiq in  [06,15,17];
      341: Result := CodLiq = 06;
      399: Result := CodLiq = 06;
      409: Result := CodLiq = 06;
      748: Result := CodLiq = 06;
      756:
      begin
        if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
          Result := CodLiq in  [06,15,17]
        else
          Result := CodLiq = 99;
      end
      else Geral.MB_Erro('Banco sem c�digo de liquida��o ' +
      'implementado para 400 posi��es ou posi��es n�o informadas!' + sLineBreak +
      'ID = "EhCodigoLiquidacao".' + sLineBreak +
      'Avise a DERMATEK!');
    end;
  end;
  //
end;

function TUBancos.CodigoTarifa(Banco: Integer; NomeLayout: String): Integer;   // Liquida��o??
begin
  Result := -1;
  case banco of
    001: Result := 06; // e 17
    008,033,353: Result := 17; //???
    104: Result := 06;
    237: Result := 06; // e 17
    341: Result := 06;
    748: Result := 28;
    756:
    begin
      if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
        Result := 06;
    end;
    else Geral.MB_Erro('Banco sem c�digo de cobran�a de tarifa. ' +
  'Avise a DERMATEK!');
  end;
  //
end;

procedure TUBancos.ConfiguraTipoCobranca(RGTipoCobranca: TRadioGroup; Colunas,
  Default: Integer);
begin
  RGTipoCobranca.Columns := Colunas;
  RGTipoCobranca.ItemIndex := Default;
  //
  RGTipoCobranca.Items.Clear;
  RGTipoCobranca.Items.Add((*[0] :=*) 'Padr�o');
  RGTipoCobranca.Items.Add((*[1] :=*) 'SIGCB - CEF');
end;

function TUBancos.ContaDaOcorrencia(const Banco, TamReg: Integer;
  const Ocorrencia, LayoutRem: String; var Genero: Integer): Boolean;
var
  OcorCod, CtaTar, ResOco, ResBco: Integer;
begin
  Result  := False;
  CtaTar  := Dmod.QrControle.FieldByName('CNABCtaTar').AsInteger;
  //
  if CtaTar = 0 then
  begin
    if Geral.MB_Pergunta('A conta de tarifa de cobran�a ainda n�o ' +
    'foi definada! Deseja defin�-la agora?') = ID_YES then
    begin
      Application.CreateForm(TFmOpcoes, FmOpcoes);
      FmOpcoes.FAba := 3;
      FmOpcoes.ShowModal;
      FmOpcoes.destroy;
      //
      Dmod.ReopenControle;
      //
      CtaTar  := Dmod.QrControle.FieldByName('CNABCtaTar').AsInteger;
    end;
  end;
  OcorCod := Geral.IMV(Ocorrencia);
  Genero  := 0;
  ResBco  := 0;
  ResOco  := 0;
  //
  if TamReg = 240 then
  begin
    // Igual para todos???
    case Banco of
      001:
      begin
        case OcorCod of
          06: Genero := CtaTar;
          17: Genero := CtaTar;
          else ResOco := -1;
        end;
      end;
      008, 033, 353:
      begin
        case OcorCod of
          17: Genero := CtaTar;
          else ResOco := -1;
        end;
      end;
      //104:
      else
      begin
        case OcorCod of
          06: Genero := CtaTar;
          else ResOco := -1;
        end;
      end;
    end;
  end else
  if TamReg = 400 then
  begin
    case banco of
      // Santander
      008, 033, 353:
      begin
        case OcorCod of
          06: Genero := CtaTar;
          else ResOco := -1;
        end;
      end;
      104:
      begin
        case OcorCod of
          21: Genero := CtaTar;
          else ResOco := -1;
        end;
      end;
      237:
      begin
        case OcorCod of
          06: Genero := CtaTar;
          else ResOco := -1;
        end;
      end;
      //237: Result := ;
      341:
      begin
        case OcorCod of
          6: Genero := CtaTar;
          else ResOco := -1;
        end;
      end;
      399:
      begin
        case OcorCod of
          6: Genero := CtaTar;
          else ResOco := -1;
        end;
      end;
      409:
      begin
        case OcorCod of
          6: Genero := CtaTar;
          else ResOco := -1;
        end;
      end;
      748:
      begin
        case OcorCod of
          //6: Result := CtaTar;
          28: Genero := CtaTar;
          else ResOco := -1;
        end;
      end;
      756: //N�o tem
      begin
        if LayoutRem = CO_756_CORRESPONDENTE_BRADESCO_2015 then
        begin
          case OcorCod of
            6: Genero := CtaTar;
            else ResOco := -1;
          end;
        end else
        begin
          case OcorCod of
            0: Genero := CtaTar;
           99: Genero := CtaTar;
            else ResOco := -1;
          end;
        end;
      end;
      else ResBco := -1;
    end;
  end else ResBco := -1;
  if ResBco = -1 then Geral.MB_Erro('Banco sem nenhuma ' +
  'defini��o de contas para ocorr�ncias. Ocorr�ncia: "' + Ocorrencia +
  '" Banco: ' + FormatFloat('000', Banco) + '. ' + sLineBreak +
  'ID = "ContaDaOcorrencia". Avise a DERMATEK!')
  else
  if ResOco = -1 then Geral.MB_Erro('Banco sem defini��o ' +
  'da conta para a ocorr�ncia "' + Ocorrencia + '" do banco ' +
  FormatFloat('000', Banco) + '. ' + sLineBreak +
  'ID = "ContaDaOcorrencia". Avise a DERMATEK!');
  //
  if Genero = 0 then Geral.MB_Erro('Conta n�o definida ' +
  'para a ocorr�ncia "' + Ocorrencia + '" do banco ' +
  FormatFloat('000', Banco) + '. ' + sLineBreak +
  'ID = "ContaDaOcorrencia". Avise a DERMATEK!')
  else Result := True;
  //
end;

function TUBancos.FatorDeRecebimento(Banco: Integer;
  Pago, Tarifa, ValBloqueto: Double; var Fator: Double): Boolean;
begin
  Result := True;
  if ValBloqueto > 0 then
  begin
    case banco of
      001: Fator := Pago / ValBloqueto * 100;
      008,
      033,
      353: Fator := Pago / ValBloqueto * 100;
      104: Fator := Pago / ValBloqueto * 100;
      237: Fator := Pago / ValBloqueto * 100;
      341: Fator := (Pago + Tarifa) / ValBloqueto * 100;
      399: Fator := Pago / ValBloqueto * 100;  // ??? Est� correto????
      409: Fator := Pago / ValBloqueto * 100;
      748: Fator := (Pago + Tarifa) / ValBloqueto * 100;
      756: Fator := (Pago + Tarifa) / ValBloqueto * 100;
      else begin
        Result := False;
        Geral.MB_Aviso('Banco sem implementa��o para obten��o ' +
        'de fator de recebimento' + sLineBreak + 'ID ="FatorDeRecebimento"');
        Fator := (Pago + Tarifa) / ValBloqueto * 100;
      end;
    end;
  end else Fator := 0;
end;

function TUBancos.DiferencaDeRecebimento(Banco: Integer;
  Pago, Tarifa, SomaVal: Double; var Diferenca: Double): Boolean;
begin
  Result := True;
  case banco of
    001: Diferenca := Pago - SomaVal;
    008,
    033,
    353: Diferenca := Pago - SomaVal;
    104: Diferenca := Pago - SomaVal;
    237: Diferenca := Pago - SomaVal;
    341: Diferenca := Pago + Tarifa - SomaVal;
    399: Diferenca := Pago - SomaVal;
    409: Diferenca := Pago - SomaVal;
    748: Diferenca := Pago + Tarifa - SomaVal;
    756: Diferenca := Pago + Tarifa - SomaVal;
    else begin
      Result := False;
      Geral.MB_Aviso('Banco sem implementa��o para obten��o ' +
      'da diferen�a de recebimento' + sLineBreak + 'ID ="DiferencaDeRecebimento"');
      Diferenca := Pago + Tarifa - SomaVal;
    end;
  end;
end;

function TUBancos.FatorMultaDeRecebimento(Banco: Integer;
  Pago, Tarifa, ValMulta: Double; var Multa: Double): Boolean;
var
  Divisor: Double;
begin
  Result := true;
  case banco of
    001: Divisor := Pago;
    008,
    033,
    353: Divisor := Pago;
    104: Divisor := Pago;
    237: Divisor := Pago;
    341: Divisor := Pago + Tarifa;
    399: Divisor := Pago;
    409: Divisor := Pago;
    748: Divisor := Pago + Tarifa;
    756: Divisor := Pago + Tarifa;
    else begin
      Result := False;
      Geral.MB_Aviso('Banco sem implementa��o para obten��o ' +
      'de fator da multa de recebimento' + sLineBreak + 'ID ="FatorMultaDeRecebimento"');
      Divisor := Pago + Tarifa;
    end;
  end;
  Multa := ValMulta / Divisor;
end;

function TUBancos.FFV3(const Casas: Integer; const Num: String; var Res: String;
  const Mensagem: String): Boolean;
begin
  Result := True;
  Res := Num;
  while Length(Res) < Casas do
    Res := '0' + Res;
  if Length(Res) <> Casas then
  begin
    Result := False;
    Geral.MB_Aviso('N�mero inv�lido: '+Res + sLineBreak +
    'Formata��o inv�lida na "procedure FFV3" na gera��o do NOSSO N�MERO!' +
    sLineBreak + sLineBreak + 'Campo: '+ Mensagem);
  end;
end;

function TUBancos.SeparaJurosEMultaImplementado(Banco: Integer; NomeLayout: String;
  var Separa: Boolean): Boolean;
begin
  //LocDado400:
  //555: Valor de juros de mora pago
  //556: Valor da multa pago
  //557: Valor da multa + juros de mora pagos   237,341,409
  Result := True;
  case Banco of
    001: Separa := False;
    008, 033, 353: Separa := True;
    104: Separa := True;
    237: Separa := False;
    341: Separa := True; //??
    399: Separa := False;
    409: Separa := False;
    748: Separa := True;
    756:
    begin
      if NomeLayout = CO_756_CORRESPONDENTE_BB_2015 then
        Separa := True
      else
        Separa := False;
    end
    else begin
      Result := False;
      Separa := True;
      Geral.MB_Aviso('Banco sem implementa��o para defini��o ' +
      'de valores de multa e juros pagos' + sLineBreak +
      'ID ="SeparaJurosEMultaImplementado"');
    end;
  end;
end;

function TUBancos.InformaTarifaDeCobrancaImplementado(Banco: Integer; var Informa: Boolean): Boolean;
begin
  Result := True;
  case Banco of
    001: Informa := True;
    008,
    033,
    353: Informa := True;
    104: Informa := True;
    237: Informa := True;
    341: Informa := True;
    399: Informa := True;
    409: Informa := True;
    748: Informa := True;
    756: Informa := False;
    else begin
      Informa := True;
      Result := False;
      Geral.MB_Aviso('Banco sem implementa��o para defini��o ' +
      'informa��o de tarifa de cobran�a' + sLineBreak +
      'ID ="InformaTarifaDeCobrancaImplementado"');
    end;
  end;
end;

function TUBancos.GeraBloquetoNumero(const Banco: Integer;
  const BloquetoAtual: Double; const Pergunta: Boolean;
  const NosNumFxaU: Boolean; // 2012-08-24 Itau
  const NosNumFxaI, NosNumFxaF: Integer; // 2012-08-24 Itau
  var NumeroBloqueto: Double; NomeLayout: String): Boolean;

  function GeraBloquetoNoAno(Pergunta: Boolean): Double;
  var
    Resp, AnoCad, Max, Min: Integer;
    Ano, Mes, Dia: Word;
    Continua: Boolean;
    BloqStr, AnoCtaCooper: String;
    Boleto: Double;
  begin
    Boleto := 0;
    DecodeDate(Date, Ano, Mes, Dia);
    //
    if (Banco = 756) and ((NomeLayout = CO_756_EXCEL_2013_07_18) or
      (NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015))
    then
      AnoCad := StrToInt(Copy(FormatFloat('0000000', BloquetoAtual), 1, 2))
    else
      AnoCad := StrToInt(Copy(FormatFloat('00000000', BloquetoAtual), 1, 2));
    //
    if AnoCad <> (Ano - 2000) then
    begin
      if BloquetoAtual = 0 then
      begin
        if Geral.MB_Pergunta('Confirma que este ano � o de ' + IntToStr(Ano) + '?') =
        ID_YES then Continua := True else Continua := False;
      end else begin
        if Pergunta then Continua :=
          Geral.MB_Pergunta('O �ltimo bloqueto para este cliente ' +
          'foi emitido no ano de ' + IntToStr(Ano-1) + '. Confirma que o ano ' +
          'mudou para ' + IntToStr(Ano) + '?') = ID_YES
        else
          Continua := False;
      end;
    end else Continua := True;
    if Continua and (BloquetoAtual = 0) then
    begin
      BloqStr := '0';
      if InputQuery('N�mero de Bloqueto',
      'Informe o n�mero do �ltimo bloqueto:', BloqStr) then
      begin
        try
          Boleto := Geral.IMV(BloqStr); // Mostrar erro caso ocorra
        except
          Continua := False;
        end;
      end;
    end else
      Boleto := BloquetoAtual;
    //
    Min := 0;
    Max := 0;
    //
    if Continua then
    begin
      case Banco of
        748:
        begin
          Max := ((Ano-2000 + 1) * 1000000) - 000001;
          Min := ((Ano-2000    ) * 1000000) + 200001; // n�o pode ser menor
        end;
        756:
        begin
          if (NomeLayout = CO_756_EXCEL_2013_07_18) or
            (NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015) then
          begin
            Max := ((Ano-2000 + 1) * 100000) - 1;
            Min := ((Ano-2000    ) * 100000) + 1;
          end else
          begin
            Max := ((Ano-2000 + 1) * 1000000) - 1;
            Min := ((Ano-2000    ) * 1000000) + 1;
          end;
        end;
        else
        begin
          Geral.MB_Aviso('O banco ' + FormatFloat('000', Banco)
          + 'n�o tem defini��o de limites de numera��o de n�mero de bloqueto!' +
          sLineBreak + 'Avise a Dermatek');
          //
          Max := ((Ano-2000 + 1) * 1000000) - 1;
          Min := ((Ano-2000    ) * 1000000) + 1;
        end;
      end;
      if Boleto < Min then
      begin
        Resp := Geral.MB_Pergunta('O n�mero de bloqueto ' +
        FormatFloat('0', Boleto+1) + ' n�o � v�lido. O banco ' +
        FormatFloat('000', Banco) +
        ' exige o rein�cio da numera��o do bloqueto a cada ano. ' +
        'O novo n�mero pode ser '+ IntToStr(Min)+'?');
        case Resp of
          ID_NO:    {Boleto := nada};
          ID_CANCEL: Boleto := 0;
          ID_YES:    Boleto := Min - 1;
        end;
      end;
      if Boleto > Max then
      begin
        BloqStr := '0';
        if InputQuery('N�mero de Bloqueto',
        'Informe o n�mero do primeiro bloqueto:', BloqStr) then
        begin
          Boleto := Geral.IMV(BloqStr); // Mostrar erro caso ocorra
          if Boleto > 0 then Boleto := Boleto -1;
        end;
      end;
    end;
    if Boleto > 0 then Boleto := Boleto + 1;
    if (Boleto > Max) or (Boleto < Min) then
    begin
      if Pergunta then
        if Geral.MB_Pergunta('O n�mero de bloqueto ' +
        FormatFloat('0', Boleto) + ' est� em desacordo com o que o banco solicita.' +
        sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
          Boleto := 0;
    end;
    Result := Boleto;
  end;
var
  Boleto: Double;
begin
  Boleto := 0;
  case Banco of
    001: Boleto := BloquetoAtual + 1;
    008,
    033,
    353:
    begin
      Boleto := BloquetoAtual + 1;
      // Fazer o mesmo com os outros bancos
      // ver o n�mero m�ximo
      case Banco of
        008: if Boleto >=     100000000 then Boleto := 1;
        033: if Boleto >= 1000000000000 then Boleto := 1;
        353: if Boleto >=      10000000 then Boleto := 1;
      end;
    end;
    104: Boleto := BloquetoAtual + 1;
    237: Boleto := BloquetoAtual + 1;
    341:
    begin
      Boleto := BloquetoAtual + 1;
      if NosNumFxaU then
      begin
        if (Boleto < NosNumFxaI) or (Boleto > NosNumFxaF) then
        begin
          if NosNumFxaF > NosNumFxaI then
            Boleto := NosNumFxaI;
        end;
      end;
    end;
    399: Boleto := BloquetoAtual + 1;
    409: Boleto := BloquetoAtual + 1;
    748: Boleto := GeraBloquetoNoAno(Pergunta);
    756:
    begin
      if (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) or
        (NomeLayout = CO_756_240_2018)
      then
        Boleto := BloquetoAtual + 1
      else
        Boleto := GeraBloquetoNoAno(Pergunta);
    end
    else begin
      if Banco = 0 then
        Geral.MB_Aviso('N�o h� banco definido para ' +
        'este cliente em seu cadastro!')
      else
        Geral.MB_Aviso('O banco ' + FormatFloat('000', Banco) +
        ' n�o possui gera��o de n�mero de bloqueto neste aplicativo!' +
        sLineBreak + 'Contate a DERMATEK. - "GeraBloquetoNumero"');
    end;
  end;
  if Boleto = 0 then Geral.MB_Erro('N�o foi poss�vel gerar o ' +
  'n�mero do bloqueto para o banco ' + FormatFloat('000', Banco) +
  '! "GeraBloquetoNumero"');
  //
  NumeroBloqueto := Boleto;
  Result := NumeroBloqueto > 0;
end;

function TUBancos.DACNossoNumero(const CadTitBco, Banco, Agencia, Posto:
  Integer; const Sequencial: Double; const ContaCorrente, Carteira, IDCobranca,
  CodigoCedente: String; TipoCobranca: Integer;
  EspecieDoc: String;// 2012-04-27 Bco 399
  CNAB: Integer; // 2012-05-01 Bco 033(008/353)
  CtaCooper: String; // 2012-05-10 Bco 237
  Layout: String; // 2012-11-01
  var DACNossoNumero: String): Boolean;
var
  NS, NS_Rem: String;
  SomaDVs: Integer;
  Vencimento: TDateTime;
begin
  // N�o precisa (usa no banco 399)
  Vencimento := -1;
  DACNossoNumero := '';
  Result := GeraNossoNumero(CadTitBco, Banco, Agencia, Posto, Sequencial,
            ContaCorrente, Carteira, IDCobranca, CodigoCedente, Vencimento,
            TipoCobranca, EspecieDoc, CNAB, CtaCooper, Layout, NS, NS_Rem);
  //
  if Result and (Banco <> 756) and (NS <> '') then
  begin
    DACNossoNumero := Copy(NS, Length(NS), 1);
    //ShowMessage(DACNossoNumero);
  end else
  if Result and (Banco = 756) and (Layout = CO_756_CORRESPONDENTE_BRADESCO_2015)
    and (NS <> '') then
  begin
    DACNossoNumero := Geral.Modulo11_2a7_Back(Carteira + NS_Rem, 0);
  end else
  if Result and (Layout = CO_756_EXCEL_2012_03_14) or (Layout = CO_756_EXCEL_2013_07_18) and (NS <> '') then
  begin
    // Calcular m�dulo 11 (qual m�dulo 11? vou usar o back 2 a 9 Tipo 0)
    DACNossoNumero := Geral.Modulo11_2a9_Back(NS, 0);
  end;
end;

function TUBancos.GeraNossoNumero(const CadTitBco, Banco, Agencia, Posto:
  Integer; const Sequencial: Double; const ContaCorrente, Carteira, IDCobranca,
  CodigoCedente: String; Vencimento: TDateTime; TipoCobranca: Integer;
  EspecieDoc: String; // 2012-04-27 Bco 399
  CNAB: Integer; // 2012-05-01 Bco 033(008/353)
  CtaCooper: String; // 2012-05-10 Bco 237
  NomeLayout: String; //2014-04-23 Bco 756
  {
  UsaFxaNosNum: Boolean; // 2012-08-24 Bco 341 Registrada
  FxaNosNumI, FxaNosNumF: Integer; // 2012-08-24 Bco 341 Registrada
  }
  var NossoNumero, NossoNumeroRem: String): Boolean;
  //
  function FFV(const Fmt: String; const Num: Double; var Res: String;
  const Mensagem: String ): Boolean;
  begin
    Result := True;
    Res := FormatFloat(Fmt, Num);
    if Length(Res) <> Length(Fmt) then
    begin
      Result := False;
      Geral.MB_Aviso('N�mero inv�lido: '+Res + sLineBreak +
      'Formata��o inv�lida na "procedure GeraNossoNumero.FFV" na gera��o do NOSSO N�MERO!' +
      sLineBreak + sLineBreak + 'Campo: '+ Mensagem);
    end;
  end;
  //
  function FFF(const Fmt: Integer; const Num: Double; var Res: String;
  const Mensagem: String ): Boolean;
  begin
    Result := True;
    Res := Geral.FFF(Num, Fmt);
    if Length(Res) <> Fmt then
    begin
      Result := False;
      Geral.MB_Aviso('N�mero inv�lido: '+Res + sLineBreak +
      'Formata��o inv�lida na "procedure GeraNossoNumero.FFF" na gera��o do NOSSO N�MERO!' +
      sLineBreak + sLineBreak + 'Campo: '+ Mensagem);
    end;
  end;
  //
const
  ConstSicoob = '319731973197319731973';
var
  Txt, Txt2: String;
  Ag, Po, NS, AA, Ce, CC, Ct, Co, Ano: String;
  Boleto, AAno, Casas, Posicoes, MaxCasas, I: Integer;
  Seq: Double;
  DV1, DV2, DV3, DVx, DVNS: String;
  SomaDVs: Int64;
  Tam, Val1, Val2: Integer;
begin
  Result := False;

  if CadTitBco = 1 then
  begin
    case Banco of
      001,
      008, 033, 353,
      104,
      237,
      341, // Ver se pode!
      399, // Segue adiante!
      756: ;
      else begin
        Geral.MB_Aviso(
        'Modalidade de cobran�a inv�lida na gera��o do Nosso N�mero! [A]');
        Exit;
      end;
    end;
  end;
  NossoNumero    := '';
  NossoNumeroRem := '';
  Seq            := Sequencial;
  //
  case Banco of
    001:
    begin
      if Seq = -1 then Seq := 0;
      //
      case CadTitBco of
        0,2: // Sem registro (Sem Cadastro)
        begin
          Posicoes := Length(FormatFloat('0000',
            Geral.IMV(Geral.SoNumero_TT(CodigoCedente))));
          case Posicoes of
            4: // 4 posi��es
            begin
              // N�o testado!
              if not FFV3(4, Geral.SoNumero_TT(CodigoCedente),
                Ce, 'C�digo cedente') then Exit;
              if not FFV3(7, FormatFloat('0', Seq), NS, 'N�mero sequencial') then Exit;
              Txt := Ce + NS;
              Txt := Txt + '-' + Geral.Modulo11_2a9_Back(Txt, 1);
            end;
            6: // 6 posi��es
            begin
              // N�o testado!
              if not FFV3(6, Geral.SoNumero_TT(CodigoCedente),
                Ce, 'C�digo cedente') then Exit;
              if not FFV3(5, FormatFloat('0', Seq), NS, 'N�mero sequencial') then Exit;
              Txt := Ce + NS;
              Txt := Txt + '-' + Geral.Modulo11_2a9_Back(Txt, 1);
            end;
            7: // 7 posi��es
            begin
              if not FFV3(7, Geral.SoNumero_TT(CodigoCedente),
                Ce, 'C�digo cedente') then Exit;
              if not FFV3(10, FormatFloat('0', Seq), NS, 'N�mero sequencial') then Exit;
              Txt := Ce + NS;
            end;
            else begin
              Geral.MB_Aviso(
              'Modalidade de cobran�a inv�lida na gera��o do Nosso N�mero! [B]' +
              'Posi��es: ' + IntToStr(Posicoes));
              Exit;
            end;
          end;
        end;
        1: // Com Registro
        begin
          if not FFV3(20, '0', Txt, 'Nosso N�mero') then Exit; // 12 zeros (+8 brancos?)
        end;
        else begin
          Geral.MB_Aviso(
          'Modalidade de cobran�a inv�lida na gera��o do Nosso N�mero! [C]' +
          sLineBreak + ' Tipo de Cadastro no Banco = ' + IntToStr(CadTitBco));
          Exit;
        end;
      end;
      NossoNumero    := Txt;// + '-' + Geral.Modulo11_2a9_Back(Txt, 1);
      NossoNumeroRem := Txt;
    end;
    008, 033, 353:
    begin
      if (CadTitBco = 1) and (CNAB = 400) then // CNAB 400 Com Registro
      begin
        if Seq = -1 then Seq := 0;
        if not FFV('0000000', Seq, Ce, 'N�mero sequencial') then Exit;
        Txt := Ct+Ce;
        NossoNumero    := Ce + '-' + Geral.Modulo11_2a9_Back(Txt, 0);
        NossoNumeroRem := Ce + '-' + Geral.Modulo11_2a9_Back(Txt, 0);
      end else
      begin
        // A principio � CNAB 240 com e sem registro!
        if Seq = -1 then Seq := 0;
        if not FFV('000000000000', Seq, Ce, 'N�mero sequencial') then Exit;
        Txt := Ct+Ce;
        NossoNumero    := Ce + '-' + Geral.Modulo11_2a9_Back(Txt, 0);
        NossoNumeroRem := Ce + '-' + Geral.Modulo11_2a9_Back(Txt, 0);
      end;
    end;
    104:
    begin
      if Seq = -1 then Seq := 0;
      Co := Geral.SoNumero_TT(IDCobranca);
      if TipoCobranca <> 1 then
      begin
        MaxCasas := 10;
        NossoNumero    := '0000000000-0';
        NossoNumeroRem := '0000000000-0';
      end else begin
        MaxCasas := 17;
        NossoNumero    := '00000000000000000-0';
        NossoNumeroRem := '00000000000000000-0';
      end;
      Casas := MaxCasas - Length(Co);
      //
      if Casas = MaxCasas then
      begin
        Geral.MB_Aviso('O banco 104 necessita que se informe o ' +
        'identificador do tipo de cobran�a no cadastro do cliente!');
      end else
      begin
        Txt := FormatFloat('0', Seq);
        while Length(Txt) < casas do
          Txt := '0' + Txt;
        Txt := Co + Txt;
        NossoNumero    := Txt + '-' + Geral.Modulo11_2a9_Back(Txt, 0);
        NossoNumeroRem := Txt + '-' + Geral.Modulo11_2a9_Back(Txt, 0);
      end;
    end;
    237:
    begin
      if Seq = -1 then Seq := 0;
      if not FFV('00', Geral.IMV(Geral.SoNumero_TT(Carteira)), Ct,
        'Carteira') then Exit;
      // 2012-05-10
      //if not FFV('00000000000', Seq, Ce, 'N�mero sequencial') then Exit;
      if not FFF(11-Length(CtaCooper), Seq, Ce, 'N�mero sequencial') then Exit;
      Ce := CtaCooper + Ce;
      // Fim 2012-05-10
      //
      Txt := Ct+Ce;
      NossoNumero    := Ct + '/' + Ce + '-' + Geral.Modulo11_2a7_Back(Txt, 0);
      NossoNumeroRem := Ce;
    end;
    341:
    begin
      if Seq = -1 then Seq := 0;
      if not FFV('0000', Agencia, Ag, 'Ag�ncia') then Exit;
      if not FFV('00000', Geral.IMV(Geral.SoNumero_TT(ContaCorrente)), CC,
        'Conta corrente') then Exit;
      if not FFV('000', Geral.IMV(Geral.SoNumero_TT(Carteira)), Ct,
        'Carteira') then Exit;
      if not FFV('00000000', Seq, Ce, 'N�mero sequencial') then Exit;
      Txt := Ag+CC+Ct+Ce;
      NossoNumero    := Ct + '-' + Ce + '-' + Geral.Modulo10_2e1_X_Back(Txt, 0);
      NossoNumeroRem := Ct + '-' + Ce + '-' + Geral.Modulo10_2e1_X_Back(Txt, 0);
    end;
    399:
    begin
      if CadTitBco = 1 then  // 1 = registrado
      begin
        if (EspecieDoc = 'SD') // Cobran�a Expressa (M�dulo II)  ????
        // precisa ver!!!
        or (EspecieDoc = 'PD') // Cobran�a Diretiva (M�dulo II)
        then begin
          if Seq = -1 then Seq := 0;
          if not FFV3(5, Geral.SoNumero_TT(CodigoCedente), CC,
          'C�digo do Cliente (Range) {C�digo cedente!}') then Exit;
          if not FFV('00000', Seq, Ce, 'N�mero sequencial') then Exit;
          //
          Txt := CC + Ce;
          DV1 := Geral.Modulo11_2a7_Back(Txt, 1);
          NossoNumero    := Txt + DV1;
          NossoNumeroRem := Txt + DV1;
        end else
        begin
          Geral.MB_Aviso(
          'Esp�cie de T�tulo inv�lido na gera��o do Nosso N�mero!');
          Exit;
        end;
      end else
      begin
        if Seq = -1 then Seq := 0;
        if not FFV('0000000000000', Seq, Ce, 'N�mero sequencial') then Exit;
        // Calcular os 3 d�gitos verificadores
        DV1 := Geral.Modulo11_2a9_Back(Ce, 0);
        if Vencimento > 2 then
          DV2 := '4'
        else
          DV2 := '5';
        DVx := Ce + DV1 + DV2;
        SomaDVS := StrToInt64(DVx) + Geral.IMV(Geral.SoNumero_TT(CodigoCedente));
        // Se houver vencimento, adicionar vencimento � soma
        if DV2 = '4'  then
          SomaDVS := SomaDVS + Geral.IMV(FormatDateTime('DDMMYY', Vencimento));
        Txt := IntToStr(SomaDVS);
        DV3 := Geral.Modulo11_2a9_Back(Txt, 0);
        NossoNumero    := Ce + DV1 + DV2 + DV3;
        NossoNumeroRem := Ce + DV1 + DV2 + DV3;
      end;
    end;
    409:
    begin
      if Seq = -1 then Seq := 0;
      if not FFV('00000000000000', Seq, Ce, 'N�mero sequencial') then Exit;
      Txt := Ce;
      NossoNumero    := Ce + '/' + Geral.Modulo11_2a9_Back(Txt, 0);
      NossoNumeroRem := Ce + '/' + Geral.Modulo11_2a9_Back(Txt, 0);
    end;
    748:
    begin
      if Seq = -1 then Seq := 200000;
      if Seq < 200000 then
      begin
        Geral.MB_Aviso('N�mero Seq inv�lido para o banco 748!');
        Exit;
      end;
      if not FFV('0000', Agencia, Ag, 'Ag�ncia') then Exit;
      if not FFV('00', Posto, Po, 'Posto') then Exit;
      if not FFV('00000', Geral.IMV(Geral.SoNumero_TT(CodigoCedente)),
        Ce, 'C�digo cedente') then Exit;
      AAno   := Trunc(Seq) div 1000000;
      Boleto := Trunc(Seq) mod 1000000;
      if not FFV('00', AAno, AA, 'Ano') then Exit;
      if not FFV('000000', Boleto, NS, 'N�mero sequencial "NS"') then Exit;
      Txt := Ag + Po + Ce + AA + NS;
      NossoNumero    := AA + '/' + NS + '-' + Geral.Modulo11_2a9_Back(Txt, 0);
      NossoNumeroRem := AA + '/' + NS + '-' + Geral.Modulo11_2a9_Back(Txt, 0);
    end;
    756:
    begin
      if (NomeLayout = CO_756_EXCEL_2013_07_18) or (NomeLayout = CO_756_240_2018) then
      begin
        Seq := Geral.IMV(Copy(Geral.FFI(Seq), 1, 7));

        if not FFV('0000', Agencia, Ag, 'Ag�ncia') then Exit;
        if not FFV3(10, Geral.SoNumero_TT(CodigoCedente), Ce, 'C�digo cedente') then Exit;
        if not FFV('0000000', Seq, NS, 'N�mero sequencial "NS"') then Exit;

        DVNS    := Ag + Ce + NS;
        SomaDVs := 0;
        Tam     := Length(DVNS);

        for I := 1 to Tam do
        begin
          Val1    := Geral.IMV(DVNS[I]);
          Val2    := Geral.IMV(ConstSicoob[I]);
          SomaDVs := SomaDVs + (Val1 * Val2);
        end;

        SomaDVs := SomaDVs mod 11;

        if SomaDVs in [0, 1] then
          DVx := '0'
        else
          DVx := Geral.FF0((11 - SomaDVs));

        if (NomeLayout = CO_756_240_2018) then
        begin
          NossoNumero    := NS + '-' + DVx;
          NossoNumeroRem := NS + DVx;
        end else
        begin
          NS := NS + DVx;
        end;
      end else
      if (NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015) and (CadTitBco = 1) then
      begin
        if Seq = -1 then Seq := 0;

        Ano := Copy(Geral.FFI(Seq), 1, 2);
        Seq := StrToFloat(Copy(Geral.FFI(Seq), 3, Length(Geral.FFI(Seq)) - 2));

        if not FFF(11 - Length(CtaCooper) - 2, Seq, NS, 'N�mero sequencial') then Exit;

        NS := Ano + CtaCooper + NS;
      end else
      if (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) and (CadTitBco = 1) then
      begin
        if not FFV3(7, CodigoCedente, Txt, 'C�digo cedente') then Exit;
        if not FFV3(3, CtaCooper, Txt2, 'Conta corrente cooperado') then Exit;

        Txt := Txt + Txt2;

        if not FFV3(7, FormatFloat('0', Seq), NS, 'N�mero sequencial') then Exit;
      end else
      begin
        if Seq = -1 then Seq := 0;
        if not FFV('00000000', Seq, NS, 'N�mero sequencial "NS"') then Exit;
      end;

      if (NomeLayout  <> CO_756_240_2018) then
      begin
        if (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) and (CadTitBco = 1) then
        begin
          NossoNumero    := Txt + NS;
          NossoNumeroRem := Txt + NS;
        end else
        begin
          NossoNumero    := NS + Geral.Modulo11_2a7_Back(Carteira + NS, 0);
          NossoNumeroRem := NS;
        end;
      end;
    end;
    else begin
      //Result := False;
      Geral.MB_Aviso('O banco ' + FormatFloat('000', Banco) +
      ' n�o possui gera��o do NOSSO N�MERO neste aplicativo!' +
      sLineBreak + 'Contate a DERMATEK. "GeraNossoNumero"');
    end;
  end;
  Result := NossoNumero <> '';
end;

{$WARNINGS OFF}
function TUBancos.CodigoDeBarra_BoletoDeCobranca(Banco, Agencia, CorresBco,
             CorresAge: Integer; DVAgencia: String; Posto: Integer;
             ContaCorrente, DVContaC, CorresCto: String; Moeda, TipoCobranca,
             TipoCarteira: Integer; NossoNumero, CodigoCedente, Carteira,
             CartImp, IDCobranca, OperCodi: String; Vencto: TDateTime;
             Valor: Double; ParcTit, IOF: Integer; MostraValorEVencto: Boolean;
             ModalCobr: Integer; NomeLayout: String): String;
  //
  function FFV(const Fmt: String; const Num: Integer; var Res: String;
  const Mensagem: String ): Boolean;
  begin
    Result := True;
    Res := FormatFloat(Fmt, Num);
    if Length(Res) <> Length(Fmt) then
    begin
      Result := False;
      Geral.MB_Aviso('N�mero inv�lido: '+Res + sLineBreak +
      'Formata��o inv�lida na "procedure CodigoDeBarra_BoletoDeCobranca.FFV" na gera��o do NOSSO N�MERO!' +
      sLineBreak + sLineBreak + 'Campo: '+ Mensagem);
    end;
  end;
  //
  function FFV2(const Fmt: String; const Num: Double; var Res: String;
  const Mensagem: String ): Boolean;
  begin
    Result := True;
    Res := FormatFloat(Fmt, Num);
    {
    Res := Num;
    while Length(Res) < Length(Fmt) do
      Res := '0' + Res;
    }
    if Length(Res) <> Length(Fmt) then
    begin
      Result := False;
      Geral.MB_Aviso('N�mero inv�lido: '+Res + sLineBreak +
      'Formata��o inv�lida na "procedure FFV2" na gera��o do NOSSO N�MERO!' +
      sLineBreak + sLineBreak + 'Campo: '+ Mensagem);
    end;
  end;
  //
{
  function FFV3(const Casas: Integer; const Num: String; var Res: String;
  const Mensagem: String ): Boolean;
  begin
    Result := True;
    Res := Num;
    while Length(Res) < Casas do
      Res := '0' + Res;
    if Length(Res) <> Casas then
    begin
      Result := False;
      Geral.MB_('N�mero inv�lido: '+Res + sLineBreak +
      'Formata��o inv�lida na "procedure FFV3" na gera��o do NOSSO N�MERO!' +
      sLineBreak + sLineBreak + 'Campo: '+ Mensagem));
    end;
  end;
}
  //
  function Substitui(var Source, SubStr: String; Pos, Tam: Integer): Boolean;
  begin
    Delete(Source, Pos, Tam);
    Insert(SubStr, Source, Pos);
    if Length(Source) <> 44 then
    begin
      Geral.MB_Erro('Erro ao criar C�digo de Barras!' + sLineBreak +
      '[' + Source + ']' + sLineBreak + 'Tamanho do texto ' +
      'deveria ser de 44 letras mas tem ' + IntToStr(Length(Source)) + '!' +
      sLineBreak + 'Este erro ocorreu ao inserir o texto "' + SubStr +
      '" na posi��o ' + IntToStr(Pos) + '.');
      Result := False;
    end else Result := True;
    //ShowMessage(Source + ' - ' + FormatFloat('0000000000', StrToInt(SubStr)));
  end;
var
  CodBarra, Txt(*, Sequencial*): String;
  i: Integer;
  xBanco, xMoeda, xVenct, xValor, xTCobr, xTCart, xNosso, xNcart, (*xIDCar,*)
  xAgenc, xPosto, xCeden, xCobra, xFill1, xDV00G, xDV00L, xConta, xOpCod,
  xModCo, xNParc, xFixo9, xFixo0, xJulia, xFix00, xDVNos,
  (*xCamp1, xCamp2, xCamp3, xCamp4, xCamp5,*) xDV001, xDV002, xDV003,
  Seq1, Seq2, Seq3: String;
  //
  Parcela: Integer;
begin
  Parcela := ParcTit;
  // Exig�ncia Banco 756 - SICOOB
  if Parcela = 0 then
    Parcela := 1;
  //Result := '00000000000000000000000000000000000000000000';
  CodBarra := '';
  for i := 1 to 44 do CodBarra := CodBarra + '0';
  Result := CodBarra;
  if Moeda <> 9 then
    Geral.MB_Aviso('O c�digo da moeda difere de 9');
  // Banco
  if ((Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015)) or
    ((Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015)) then
  begin
    FFV('000', CorresBco, xBanco, 'Banco') ;
    Substitui(CodBarra, xBanco, 1, 3);
  end else
  begin
    FFV('000', Banco, xBanco, 'Banco') ;
    Substitui(CodBarra, xBanco, 1, 3);
  end;
  // Moeda
  FFV('0', Moeda, xMoeda, 'Moeda') ;
  Substitui(CodBarra, xMoeda, 4, 1);
  // Posi��o 5: Digito verificador geral do c�digo de barras
  xVenct := '';
  xValor := '';
  if not MostraValorEVencto then
  begin
    // Fator de vencoimento
    for i := 1 to 04 do xVenct := xVenct + '0';
    // Valor
    for i := 1 to 10 do xValor := xValor + '0';
  end else begin
    // Fator de Vencimento
    FFV('0000', Trunc(Vencto - EncodeDate(1997, 10, 7)), xVenct, 'Fator de vencimento');
    Substitui(CodBarra, xVenct, 6, 4);
    // Valor
    xValor := Geral.SoNumero_TT(Geral.FFT(Valor, 2, siPositivo));
    while Length(xValor) < 10 do  xValor := '0' + xValor;
  end;
  Substitui(CodBarra, xValor, 10, 10);
  //
  case Banco of
    001:
    begin
      //  20 a 25 Zeros
      xFixo0 := '000000';
      Substitui(CodBarra, xFixo0, 20, 06);
      // Retirar n�mero do controle sequencial do NossoNumero
      FFV3(17, Copy(Geral.SoNumero_TT(NossoNumero), 1, 17),
        xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
      //FFV3('00000000000000000', Geral.DMV(Copy(Geral.SoNumero_TT(NossoNumero), 1, 17)),
        //xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
      //
      Substitui(CodBarra, xNosso, 26, 17);
      // 43 a 44 Carteira
      FFV('00', Geral.IMV(Carteira), xNCart, 'N�mero da carteira de cobran�a') ;
      Substitui(CodBarra, xNCart, 43, 02);
      //
      Txt := xFixo0 + xNosso + xNCart;
    end;
    008, 033, 353: // Santander
    begin
      // 20 a 20 Fixo "9"
      FFV('0', 9, xFixo9, 'Fixo "9"') ;
      Substitui(CodBarra, xFixo9, 20, 01);
      //
      // 21 a 27 N�mero no PSK (C�digo do cliente)
      FFV('0000000', Geral.IMV(CodigoCedente), xCeden, 'C�digo do cedente (cliente)') ;
      Substitui(CodBarra, xCeden, 21, 07);
      //
      // 28 a 40 Nosso N�mero
      FFV2('0000000000000', Geral.DMV(Geral.SoNumero_TT(NossoNumero)), xNosso, 'Nosso n�mero') ;
      Substitui(CodBarra, xNosso, 28, 13);
      //
      // 41 a 41 IOF - Seguradoras - Demais Clientes Zero
      FFV('0', 0, xFixo0, 'IOF - Seguradoras - Demais Clientes Zero') ;
      Substitui(CodBarra, xFixo0, 41, 01);
      //
      // 42 a 44 Carteira ( C�digo de cobran�a (Sem registro = 102) (Com registro = 101))
      FFV('000', Geral.IMV(Carteira), xNCart, 'N�mero da carteira de cobran�a') ;
      Substitui(CodBarra, xNCart, 42, 03);
      //
      Txt := xFixo9 + xCeden + xNosso + xFixo0 + xNCart;
    end;
    104: // CEF Caixa Econ�mica Federal
    begin
      if TipoCobranca <> 1 then
      begin
        // Retirar n�mero do controle sequencial do NossoNumero
        FFV2('0000000000', Geral.DMV(Copy(Geral.SoNumero_TT(NossoNumero), 1, 10)),
          xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
        Substitui(CodBarra, xNosso, 20, 10);

        // Ag�ncia
        FFV('0000', Agencia, xAgenc, 'Ag�ncia') ;
        Substitui(CodBarra, xAgenc, 30, 04);

        //  Carteira (Opera��o C�digo)
        FFV('000', Geral.IMV(Geral.SoNumero_TT(OperCodi)), xOpCod,
        'Opera��o c�digo');
        Substitui(CodBarra, xOpCod, 34, 03);

        // C�digo do cedente (C�digo fornecido pela Ag�ncia)
        FFV('00000000', Geral.IMV(Geral.SoNumero_TT(CodigoCedente)), xCeden,
        'C�digo cedente');
        Substitui(CodBarra, xCeden, 37, 08);
        //

        Txt := xNosso + xAgenc + xOpCod + xCeden;
      end else
      begin
        // C�digo do cedente + DV (C�digo fornecido pela Ag�ncia)
        if Length(CodigoCedente) <> 7 then
          Geral.MB_Aviso('CUIDADO!!! O c�digo do cedente deve conter 7 d�gitos!' +
          sLineBreak+
          'Ou seja, deve ser informado os 6 d�gitos do codigo do cedente ' +
          'acrescido do seu d�gito verificador!');
        FFV('0000000', Geral.IMV(Geral.SoNumero_TT(CodigoCedente)), xCeden,
        'C�digo cedente');
        Substitui(CodBarra, xCeden, 20, 07);
        //

        // Retirar n�mero do controle sequencial do NossoNumero
        xNosso := Copy(Geral.SoNumero_TT(NossoNumero), 1, 17);
        // Erro! N�mero muito grande
        //FFV2('00000000000000000', Geral.DMV(xNosso),
          //xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
        if Length(xNosso) <> 17 then
        begin
          Geral.MB_Aviso(
          'H� erro no tamanho do Nosso N�mero do banco ' +
          FormatFloat('000', Banco) + ', pois a quandidade de n�meros (' +
          IntToStr(Length(xNosso)) + ') difere do esperado (17)!' + sLineBreak +
          'Contate a DERMATEK. "CodigoDeBarra_BoletoDeCobranca"');
        end;
        while Length(xNosso) < 17 do
          xNosso := '0' + xNosso;
        Seq1 := Copy(xNosso, 03, 03);
        Seq2 := Copy(xNosso, 06, 03);
        Seq3 := Copy(xNosso, 09, 09);
        // Seq 1 do Nosso Numero
        Substitui(CodBarra, Seq1, 27, 03);
        // Seq 2 do Nosso Numero
        Substitui(CodBarra, Seq2, 31, 03);
        // Seq 3 do Nosso Numero
        Substitui(CodBarra, Seq3, 35, 09);

        // Constante1: ModalCobr (Com ou sem registro)
        FFV('0', ModalCobr, xModCo, 'Modalidade de cobran�a') ;
        Substitui(CodBarra, xModCo, 30, 01);

        // Constante 2: Bloqueto Cedente
        xOpCod := '4';
        Substitui(CodBarra, xOpCod, 34, 01);

        if IDCobranca <>  (xModCo + xOpCod) then
        begin
          Geral.MB_Aviso(
          'H� diverg�ncia nos dados informados para:' + sLineBreak +
          'Tipo de Cobran�a = "SIGCB - CEF" para o Banco ' + sLineBreak +
          FormatFloat('000', Banco) + sLineBreak +
          'Contate a DERMATEK. "CodigoDeBarra_BoletoDeCobranca"');
        end;

        // D�gito verificador do campo livre
        Txt := xCeden + Seq1 + xModCo + Seq2 + xOpCod + Seq3;
        xDV001 := Geral.Modulo11_2a9_Back(Txt, 0);
        Substitui(CodBarra, xDV001, 44, 01);

        Txt := Txt + xDV001;

      end;
    end;
    237: // Bradesco
    begin
      // 20 a 23 4 Ag�ncia Cedente (Sem o digito verificador,
      //completar com zeros a esquerda quando necess�rio)
      FFV('0000', Agencia, xAgenc, 'Ag�ncia') ;
      Substitui(CodBarra, xAgenc, 20, 04);

      //24 a 25 2 Carteira
      FFV('00', Geral.IMV(Geral.SoNumero_TT(Carteira)), xNCart,
        'N�mero da carteira') ;
      Substitui(CodBarra, xNCart, 24, 02);

      //26 a 36 11 N�mero do Nosso N�mero(Sem o digito verificador)
      FFV2('00000000000', Geral.DMV(Copy(Geral.SoNumero_TT(NossoNumero), 3, 11)),
        xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
      Substitui(CodBarra, xNosso, 26, 11);

      //37 a 43 7 Conta do Cedente (Sem o digito verificador,
      //completar com zeros a esquerda quando necess�rio)
      FFV('0000000', Geral.IMV(Geral.SoNumero_TT(ContaCorrente)),
        xConta, 'Conta corrente sem o d�gito verificador');
      Substitui(CodBarra, xConta, 37, 07);

      //44 a 44 1 Zero
      //Para o c�lculo do D�gito verificador
      xDV001 := '0';
      Substitui(CodBarra, xDV001, 44, 01);

      Txt := xAgenc + xNCart + xNosso + xConta + xDV001;
    end;
    341: // Ita�
    begin
      // Carteira
      FFV('000', Geral.IMV(Geral.SoNumero_TT(Carteira)), xNCart,
        'N�mero da carteira') ;
      Substitui(CodBarra, xNCart, 20, 03);

      // Retirar n�mero do controle sequencial do NossoNumero
      FFV('00000000', Geral.IMV(Copy(Geral.SoNumero_TT(NossoNumero), 4, 8)),
        xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
      Substitui(CodBarra, xNosso, 23, 08);

      // D�gito verificador [Ag�ncia/Conta/Carteira/NossoNumero]
      // Ag�ncia
      FFV('0000', Agencia, xAgenc, 'Ag�ncia') ;
      //Conta sem o DV
      FFV('00000', Geral.IMV(Geral.SoNumero_TT(ContaCorrente)),
        xConta, 'Conta corrente sem o d�gito verificador');
      xDV001 := Geral.Modulo10_1e2_9_Back(xAgenc + xConta + xNCart + xNosso);
      Substitui(CodBarra, xDV001, 31, 01);
      Substitui(CodBarra, xAgenc, 32, 04);
      Substitui(CodBarra, xConta, 36, 05);

      // D�gito verificador [Ag�ncia/Conta corrente]
      xDV002 := Geral.Modulo10_2e1_X_Back(xAgenc + xConta, 0);
      Substitui(CodBarra, xDV002, 41, 01);

      // Zeros
      xDV003 := '000';
      Substitui(CodBarra, xDV003, 42, 03);

      Txt := xNCart + xNosso + xDV001 + xAgenc + xConta + xDV002 + xDV003;

      {FFV('000000000', Geral.IMV(xBanco + xMoeda + xNcart +
      Copy(Sequencial, 1, 2)), xCamp1, 'Campo 1 sem o d�gito verificador');
      xDV001 := Geral.Modulo10_2e1_X_Back(xCamp1, 0);
      //ShowMessage(xCamp1+xDV001);}
      //

    end;
    399: // HSBC
    begin
      if ModalCobr = 1 then  // Com registro
      begin
        // 20 a 30 (11) - N�mero Banc�rio (NossoNumero)
        FFV3(11, Geral.SoNumero_TT(NossoNumero),
          xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
        Substitui(CodBarra, xNosso, 20, 11);
        // 31 a 41 (11) - Ag�ncia a Conta Corrente
        // Ag�ncia (31 a 34)
        FFV('0000', Agencia, xAgenc, 'Ag�ncia') ;
        Substitui(CodBarra, xAgenc, 31, 04);
        // Cedente (Conta de cobran�a) (35 a 41)
        FFV('0000000', Geral.IMV(ContaCorrente), xConta, 'Conta de cobran�a (Conta corrente?)') ;
        Substitui(CodBarra, xConta, 35, 07);
        // 42 a 43 C�digo da Carteira
        xNCart := '00'; // -> FmCNAB_Cfg.EdCartCod
        Substitui(CodBarra, xNCart, 42, 02);
        //
        // 44 a 44 (01) - C�digo do Produto COB, igual a 1.  (Cobranca registrada)
        xTCobr := '1';
        Substitui(CodBarra, xTCobr, 44, 01);

        Txt := xNosso + xAgenc + xConta + xNcart + xTCobr;

      end
      else // if ModalCobr = 0 then  // sem registro
      begin
        // 20 a 26 (07) - C�digo do Cedente
        FFV('0000000', Geral.IMV(Geral.SoNumero_TT(CodigoCedente)), xCeden,
        'C�digo cedente');
        Substitui(CodBarra, xCeden, 20, 07);
        //
        // 27 a 39 (13) - N�mero Banc�rio, igual ao C�digo do Documento, sem os
        //d�gitos verificadores e tipo identificador.
        FFV2('0000000000000', Geral.DMV(Copy(Geral.SoNumero_TT(NossoNumero), 1, 13)),
          xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
        Substitui(CodBarra, xNosso, 27, 13);
        //
        // 40 a 43 (04) - Data de Vencimento no Formato Juliano.
        FFV2('0000', Geral.IMV(dmkPF.DataJuliana_HSBC(Vencto)), xJulia, 'Data juliana');
        Substitui(CodBarra, xJulia, 40, 04);
        //
        // 44 a 44 (01) - C�digo do Produto CNR, igual a 2.  (Cobranca n�o registrada)
        xTCobr := '2';
        Substitui(CodBarra, xTCobr, 44, 01);

        Txt := xCeden + xNosso + xJulia + xTCobr;
      end;
    end;
    409: // Unibanco
    begin
      // 20 a 20 (01) C�digo para transa��o CVT = 5 (n�mero fixo)
      xTCobr := '5';
      Substitui(CodBarra, xTCobr, 20, 1);
      //
      // 21 a 27 (07) n�mero do cliente no c�digo de barras(consultar junto ao banco)
      FFV('0000000', Geral.IMV(Geral.SoNumero_TT(CodigoCedente)), xCeden,
      'C�digo cedente');
      Substitui(CodBarra, xCeden, 21, 07);
      //
      // 28 a 29 (02) Vago. Usar 00 (n�mero fixo)
      xFix00 := '00';
      Substitui(CodBarra, xFix00, 28, 02);
      //
      // 30 a 43 (14) n�mero de refer�ncia > Nosso N�mero
      FFV2('00000000000000', Geral.DMV(Copy(Geral.SoNumero_TT(NossoNumero), 1, 14)),
        xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
      Substitui(CodBarra, xNosso, 30, 14);
      //
      // 44 a 44 (01) D�gito verificador do n�mero de refer�ncia (Nosso N�mero) calculado pelo m�dulo 11
      FFV2('0', Geral.DMV(Copy(Geral.SoNumero_TT(NossoNumero), 15, 01)),
        xDVNos, 'DV do NOSSO N�MERO');
      Substitui(CodBarra, xDVNos, 44, 1);
      //

      Txt := xTCobr + xCeden + xFix00 + xNosso + xDVNos;
    end;
    748: // Sicredi
    begin
      // Tipo de cobran�a
      xTCobr := '3';
      Substitui(CodBarra, xTCobr, 20, 1);
      // Tipo de carteira
      xTCart := '1';
      Substitui(CodBarra, xTCart, 21, 1);
      // Nosso Numero
      xNosso := Geral.SoNumero_TT(NossoNumero);
      while Length(xNosso) < 9 do xNosso := '0' + xNosso;
      Substitui(CodBarra, xNosso, 22, 9);
      // Agencia cedente
      FFV('0000', Agencia, xAgenc, 'Ag�ncia');
      Substitui(CodBarra, xAgenc, 31, 4);
      // Posto de atendimento
      FFV('00', Posto, xPosto, 'Posto (Banc�rio)');
      Substitui(CodBarra, xPosto, 35, 2);
      // C�digo do cedente
      FFV('00000', Geral.IMV(Geral.SoNumero_TT(CodigoCedente)), xCeden,
      'C�digo cedente');
      Substitui(CodBarra, xCeden, 37, 5);
      // Tem valor na cobranca
      xCobra := IntToStr(Geral.BoolToInt(MostraValorEVencto and (Valor >= 0.01)));
      Substitui(CodBarra, xCobra, 42, 1);
      // Filler
      xFill1 := '0';
      Substitui(CodBarra, xFill1, 43, 1);
      // DV - Digito verificador do campo livre
      Txt := xTCobr + xTCart + xNosso + xAgenc + xPosto + xCeden + xCobra + xFill1;
      xDV00L := Geral.Modulo11_2a9_Back(Txt, 0);
      Substitui(CodBarra, xDV00L, 44, 1);
      Txt := Txt + xDV00L;
    end;
    756: // SICOOB
    begin
      if NomeLayout = CO_756_CORRESPONDENTE_BB_2015 then
      begin
        //  20 a 25 Zeros
        xFixo0 := '000000';
        Substitui(CodBarra, xFixo0, 20, 06);
        // Retirar n�mero do controle sequencial do NossoNumero
        FFV3(17, Copy(Geral.SoNumero_TT(NossoNumero), 1, 17),
          xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
        //FFV3('00000000000000000', Geral.DMV(Copy(Geral.SoNumero_TT(NossoNumero), 1, 17)),
          //xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
        //
        Substitui(CodBarra, xNosso, 26, 17);
        // 43 a 44 Carteira
        FFV('00', Geral.IMV(CartImp), xNCart, 'N�mero da carteira de cobran�a') ;
        Substitui(CodBarra, xNCart, 43, 02);
        //
        Txt := xFixo0 + xNosso + xNCart;
      end else
      if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
      begin
        // 20 a 23 4 Ag�ncia Cedente (Sem o digito verificador,
        //completar com zeros a esquerda quando necess�rio)
        FFV('0000', CorresAge, xAgenc, 'Ag�ncia') ;
        Substitui(CodBarra, xAgenc, 20, 04);

        //24 a 25 2 Carteira
        FFV('00', Geral.IMV(Geral.SoNumero_TT(Carteira)), xNCart,
          'N�mero da carteira') ;
        Substitui(CodBarra, xNCart, 24, 02);

        //26 a 36 11 N�mero do Nosso N�mero(Sem o digito verificador)
        FFV2('00000000000', Geral.DMV(Geral.SoNumero_TT(NossoNumero)),
          xNosso, 'Sequencial do NOSSO N�MERO (Documento)');
        Substitui(CodBarra, xNosso, 26, 11);

        //37 a 43 7 Conta do Cedente (Sem o digito verificador,
        //completar com zeros a esquerda quando necess�rio)
        FFV('0000000', Geral.IMV(Geral.SoNumero_TT(CorresCto)),
          xConta, 'Conta corrente sem o d�gito verificador');
        Substitui(CodBarra, xConta, 37, 07);

        //44 a 44 1 Zero
        //Para o c�lculo do D�gito verificador
        xDV001 := '0';
        Substitui(CodBarra, xDV001, 44, 01);

        Txt := xAgenc + xNCart + xNosso + xConta + xDV001;
      end else
      begin
        //20 a 20	1	C�digo da carteira
        if Carteira = '02' then //Sem registro
          Carteira := '01'; //Com e sem registro � na carteira 1
        //
        FFV('0', Geral.IMV(Geral.SoNumero_TT(Carteira)), xNCart,
          'N�mero da carteira') ;
        Substitui(CodBarra, xNCart, 20, 01);

        //21 a 24	4	C�digo da ag�ncia
        FFV('0000', Agencia, xAgenc, 'Ag�ncia') ;
        Substitui(CodBarra, xAgenc, 21, 04);

        //25 a 44	20	Campo Livre
        //1 a 2	2	C�digo da modalidade de cobran�a (01)
        //Mudado em 09/04/2014 novo layout Ini

        //Mudado em 28/04/2015 o Sicoob voltou a usar o xModCo = 01 fixo
        (*
        if NomeLayout = CO_756_EXCEL_2013_07_18 then
        begin
          if ModalCobr = 0 then
            xModCo := '02'
          else
           xModCo := '01';
        end else
          xModCo := '01';
        *)
        xModCo := '01';


        //Mudado em 09/04/2014 novo layout Fim
        Substitui(CodBarra, xModCo, 25, 2);

        //3 a 9	7	C�digo do Cedente
        FFV('0000000', Geral.IMV(Geral.SoNumero_TT(CodigoCedente)),
          xCeden, 'C�digo cedente');
        Substitui(CodBarra, xCeden, 27, 07);

        //10 a 17	8	Nosso N�mero do t�tulo
        FFV2('00000000', Geral.DMV(NossoNumero), xNosso,
          'Sequencial do NOSSO N�MERO (Documento)');
        Substitui(CodBarra, xNosso, 34, 08);


        //18 a 20	3	N�mero da Parcela do T�tulo
        // 2011-09-19
  {
        FFV2('000', ParcTit(*Unica*), xNParc,
          'Parcela do t�tulo');
        Substitui(CodBarra, xNParc, 42, 03);
  }
        FFV2('000', Parcela(*Unica*), xNParc,
          'Parcela do t�tulo');
        Substitui(CodBarra, xNParc, 42, 03);
        // fim 2011-09-19

        Txt := xNCart + xAgenc + xModCo + xCeden + xNosso + xNParc;
      end;
    end;
    else Geral.MB_Aviso('O banco ' + FormatFloat('000', Banco) +
    ' n�o possui gera��o do C�DIGO DE BARRAS neste aplicativo!' +
    sLineBreak + 'Contate a DERMATEK.. "CodigoDeBarra_BoletoDeCobranca"');
  end;
  // Digito Verificador Geral
  Txt := xBanco + xMoeda + xVenct + xValor + Txt;
  if Length(Txt) <> 43 then
  begin
    Geral.MB_Aviso(
    'H� erro na gera��o do d�gito verificador geral do banco ' +
    FormatFloat('000', Banco) + ', pois a quandidade de n�meros (' +
    IntToStr(Length(Txt)) + ') difere do esperado (43)!' +
    sLineBreak + 'Contate a DERMATEK. "CodigoDeBarra_BoletoDeCobranca"');
  end;
  xDV00G := Geral.Modulo11_2a9_Back(Txt, 2);
  Substitui(CodBarra, xDV00G, 5, 1);
  //
  Result := CodBarra;
end;
{$WARNINGS ON}

function TUBancos.LinhaDigitavel_BoletoDeCobranca(CodigoBarra: String): String;
var
  Campo1, Campo2, Campo3, Campo4, Campo5: String;
begin
  Campo1 := Copy(CodigoBarra, 1, 4) + Copy(CodigoBarra, 20, 5);
  Campo1 := Campo1 + Geral.Modulo10_1e2_9_Back(Campo1);
  //
  Campo2 := Copy(CodigoBarra, 25, 10);
  Campo2 := Campo2 + Geral.Modulo10_1e2_9_Back(Campo2);
  //
  Campo3 := Copy(CodigoBarra, 35, 10);
  Campo3 := Campo3 + Geral.Modulo10_1e2_9_Back(Campo3);
  //
  Campo4 := CodigoBarra[5];
  //
  Campo5 := Copy(CodigoBarra, 6, 14);
  //
  Insert('.', Campo1, 6);
  Insert('.', Campo2, 6);
  Insert('.', Campo3, 6);
  Result := Concat(Campo1, ' ', Campo2, ' ', Campo3, ' ', Campo4, ' ', Campo5);
end;

function TUBancos.DigitoVerificadorCodigoBanco(Banco: Integer): String;
begin
  case Banco of
    001: Result := '9';
    033: Result := '7';
    104: Result := '0';
    237: Result := '2';
    341: Result := '7';
    399: Result := '9';
    409: Result := '9';
    748: Result := 'X';
    756: Result := '0';
    else Result := '?'
  end;
end;

function TUBancos.BancoImplemMultiEmp(Banco, CNAB: Integer;
  Envio: TEnvioCNAB): Boolean;
begin
  Result := False;
  //
  if Envio = ecnabRetorno then
  begin
    // 341 = Ita�
    if (Banco = 341) then Result := True
    // 008, 033 e 353 = Santander
    else if (Banco = 008) or (Banco = 033) or (Banco = 353) then Result := True
    else
      Result := False;
    if not Result then
      Geral.MB_Aviso('O banco ' + Geral.FFN(Banco, 3) +
      ' n�o est� implementado para ' + CNABEnvio_ecnabToTxt(Envio) +
      Geral.FFN(CNAB, 3) + '!' + sLineBreak +
      'Avise a DERMATEK (UBancos.BancoImplemMultiEmp)!');
  end;
end;

function TUBancos.BancoTemEntidade(Banco: Integer): Boolean;
  function Achou(Banco: Integer): Boolean;
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Entidade ');
    Dmod.QrAux.SQL.Add('FROM bancos ');
    Dmod.QrAux.SQL.Add('WHERE Codigo=:P0 ');
    Dmod.QrAux.Params[0].AsInteger := Banco;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    Result := Dmod.QrAux.FieldByName('Entidade').AsInteger <> 0;
  end;
begin
  Result := False;
  if not Achou(Banco) then
  begin
    if Geral.MB_Pergunta('O banco ' + FormatFloat('000', Banco) +
    ' n�o possui entidade banc�ria associada!. Deseja associar agora?') = ID_YES then
    begin
      Application.CreateForm(TFmBancos, FmBancos);
      FmBancos.LocCod(Banco, Banco);
      FmBancos.ShowModal;
      FmBancos.Destroy;
      //
      Result := Achou(Banco);
    end;
  end else Result := True;
  //
  if not Result then Geral.MB_Aviso('O banco ' +
  FormatFloat('000', Banco) + ' n�o possui entidade banc�ria associada!');
end;

function TUBancos.LocDado240(const Banco, Campo, Linha: Integer; const Lista:
TStrings; const Mensagem, NomeLayout: String; var Res: String): Boolean;
{
function TUBancos.LocDado240(Banco, Campo, Linha: Integer; Lista: TStrings;
const Mensagem: String; var Res: String): Boolean;
}
var
  Ini, Tam, Cas, Typ: Integer;
  Fmt, Aux: String;
begin
  Ini := 0;
  Tam := 0;
  Cas := 0;
  Typ := -1;
  Fmt := '';
  case Campo of
    //C�digo do banco
    001: begin Ini := 001; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
    //Nome do banco
    002:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 103; Tam := 030; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Codigo do servi�o
    003:
    begin
      Ini := 004; Tam := 004; Cas := 0; Typ := 1; Fmt := '';
    end;
    //Codigo de retorno
    007:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 143; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //C�digo de remessa ou retorno
    009:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 143; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Identifica��o do registro header de arquivo
    010: begin Ini := 008; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
    //Identifica��o do registro detalhe
    011: begin Ini := 008; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
    //C�digo de movimento
    019:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 016; Tam := 002; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //C�digo da Ag�ncia da Empresa
    020:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 019; Tam := 004; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 053; Tam := 005; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //N�mero da Conta Corrente da Empresa
    021:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 030; Tam := 011; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 059; Tam := 012; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //D�gito verificador Ag�ncia Empresa
    022:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 058; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //D�gito verificador Ag�ncia e Conta Empresa
    024:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 072; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Tipo do Opera��o
    031:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 009; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //
    // de 040 a 043 > Santander! 008, 033, 353 em 2013-04-01
    // v�lidos para o registro "T" !!!
    //C�digo da Ag�ncia do Cliente
    040:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 018; Tam := 004; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //N�mero da Conta Corrente do Cliente
    041:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 023; Tam := 009; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //D�gito verificador Ag�ncia do Cliente
    042:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 022; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //D�gito verificador da Conta Corrente do Cliente
    043:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 032; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //D�gito verificador da Agencia e Conta Corrente do Cliente
    //044: N�o tem !!!
    // Fim 2013-04-01

{F} //Complemento da ocorr�ncia do sacado
{E} 193:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 181; Tam := 030; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
{B} //Valor da ocorr�ncia do sacado
{R} 194:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 166; Tam := 015; Cas := 2; Typ := 1; Fmt := '';
      end;
    end;
{A} //Data da ocorr�ncia do sacado
{B} 195:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 158; Tam := 008; Cas := 0; Typ := 1; Fmt := 'DDMMAAAA';
      end;
    end;
{A} //C�digo de ocorr�ncias do sacado
{N} 196:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 154; Tam := 004; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;

    //Identifica��o do tipo de inscri��o da Empresa (Cedente)
    400:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 018; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //CNPJ/CPF Empresa (Cedente)
    401:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 019; Tam := 014; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Nome Empresa M�e (Cedente)
    402:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 017; Tam := 030; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 073; Tam := 030; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //C�digo do Cedente
    410:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 023; Tam := 007; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 033; Tam := 016; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //D�gito do cedente
    411:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 105; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //C�digo do Cedente multiempresa ( nos registros das treansa��es, n�o no header!)
    //420: ???  2011-01-13
    //Uso reservado da empresa (header)
    //430: ???  2011-01-13
    //Uso reservado da empresa (detalhe)
    500:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 192; Tam := 020; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Nosso n�mero  (ID_Link > 1000:)
    501:
    begin
      case banco of
        008, 033, 353:
        begin
          Ini := 041; Tam := 013; Cas := 0; Typ := 1; Fmt := '';
        end;
        else begin
          if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
          begin
            Ini := 038; Tam := 020; Cas := 0; Typ := 1; Fmt := '';
          end else
          begin
            Ini := 047; Tam := 011; Cas := 0; Typ := 1; Fmt := '';
          end;
        end;
      end;
    end;
    //N�mero do documento
    502:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 060; Tam := 015; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 059; Tam := 015; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //C�digo de ocorr�ncia / movimento do servi�o
    504: begin Ini := 016; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
    //Data de ocorr�ncia
    505: begin Ini := 138; Tam := 008; Cas := 0; Typ := 2; Fmt := 'DDMMAAAA'; end;
    //C�digo da carteira
    508:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 058; Tam := 002; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 058; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Motivos da ocorrencia / Movimento
    530:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 215; Tam := 010; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 214; Tam := 010; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //C�digo da moeda
    549:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 132; Tam := 002; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 131; Tam := 002; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Valor (nominal) do t�tulo
    550:
    begin
      case banco of
        008, 033, 353:
        begin
         Ini := 082; Tam := 011; Cas := 2; Typ := 1; Fmt := '';
        end;
        else begin
          if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
          begin
            Ini := 083; Tam := 015; Cas := 2; Typ := 1; Fmt := '';
          end else
          begin
            Ini := 082; Tam := 015; Cas := 2; Typ := 1; Fmt := '';
          end;
        end;
      end;
    end;
    //Valor do abatimento concedido
    551: begin Ini := 048; Tam := 015; Cas := 2; Typ := 1; Fmt := ''; end;
    //Valor do desconto concedido
    552: begin Ini := 033; Tam := 015; Cas := 2; Typ := 1; Fmt := ''; end;
    //Valor do outros cr�ditos (FEBRABAN v. 8.4)
    554: begin Ini := 123; Tam := 015; Cas := 2; Typ := 1; Fmt := ''; end;
    //Valor da multa + juros de mora pagos
    557: begin Ini := 018; Tam := 015; Cas := 2; Typ := 1; Fmt := ''; end;
    //Valor do IOF
    569:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 063; Tam := 015; Cas := 2; Typ := 1; Fmt := '';
      end;
    end;
    //Despesas de cobran�a (Tarifa)
    570:
    begin
      case banco of
        008, 033, 353:
        begin
          Ini := 194; Tam := 015; Cas := 2; Typ := 1; Fmt := '';
        end else
        begin
          if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
          begin
            Ini := 200; Tam := 015; Cas := 2; Typ := 1; Fmt := '';
          end else
          begin
            Ini := 199; Tam := 015; Cas := 2; Typ := 1; Fmt := '';
          end;
        end;
      end;
    end;
    //Valor total pago
    578:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 063; Tam := 015; Cas := 2; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 078; Tam := 015; Cas := 2; Typ := 1; Fmt := '';
      end;
    end;
    //Valor cr�dito (Bruto - l�quido)
    579:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 093; Tam := 015; Cas := 2; Typ := 1; Fmt := '';
      end;
    end; // 15??
    //Data do vencimento
    580:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 075; Tam := 008; Cas := 0; Typ := 2; Fmt := 'DDMMAAAA';
      end else
      begin
        Ini := 074; Tam := 008; Cas := 0; Typ := 2; Fmt := 'DDMMAAAA';
      end;
    end;
    //Data de cr�dito na conta corrente
    581: begin Ini := 146; Tam := 008; Cas := 0; Typ := 2; Fmt := 'DDMMAAAA'; end;
    //Valor de outras despesas
    585: begin Ini := 108; Tam := 015; Cas := 2; Typ := 1; Fmt := ''; end;

{F} //Banco cobrador / recebedor
{E} 600:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 098; Tam := 003; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 097; Tam := 003; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
{B} //Ag�ncia cobradora / recebedora
{R} 601:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 101; Tam := 005; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 100; Tam := 005; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
{A} //DV da Ag�ncia cobradora / recebedora
{B} 602:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 106; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 105; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
{A} //Contrato de opera��o de cr�dito
{N} 654: begin Ini := 189; Tam := 010; Cas := 0; Typ := 1; Fmt := ''; end;

    // PERC. CR�DITO ENT. RECEBEDORA
    660:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 123; Tam := 005; Cas := 2; Typ := 1; Fmt := '';
      end;
    end;
    //Identifica��o do tipo de registro (CNAB)
    670:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 009; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 008; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Identifica��o do tipo de segmento (CNAB 240)
    671:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 014; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Tipo de inscri��o do sacado
    801:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 134; Tam := 002; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 133; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //N�mero da inscri��o (CPF/CNPJ) do sacado
    802:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 136; Tam := 014; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 134; Tam := 015; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Nome do Sacado
    803:
    begin
      if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 150; Tam := 040; Cas := 0; Typ := 1; Fmt := '';
      end else
      begin
        Ini := 149; Tam := 040; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Nome do sacador / Avalista
    853:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 106; Tam := 025; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //UF do sacador / avalista
    858:
    begin
      if (Banco <> 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Ini := 097; Tam := 002; Cas := 0; Typ := 1; Fmt := '';
      end;
    end;
    //Layout do lote
    887: begin Ini := 014; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
    //Densidade do arquivo
    888: begin Ini := 167; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
    //Layout do arquivo
    889: begin Ini := 164; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
    //C�digo da entidade recebedora
    900: begin Ini := 104; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
    //Percentual de cr�dito da entidade recebedora
    901: begin Ini := 109; Tam := 005; Cas := 2; Typ := 1; Fmt := ''; end;
    //C�digo da entidade benefici�ria 1
    902: begin Ini := 114; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
    //Percentual de cr�dito da entidade benefici�ria 1
    903: begin Ini := 119; Tam := 005; Cas := 2; Typ := 1; Fmt := ''; end;
    //C�digo da entidade benefici�ria 2
    904: begin Ini := 124; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
    //Percentual de cr�dito da entidade benefici�ria 2
    905: begin Ini := 129; Tam := 005; Cas := 2; Typ := 1; Fmt := ''; end;
    //C�digo da entidade benefici�ria 3
    906: begin Ini := 134; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
    //Percentual de cr�dito da entidade benefici�ria 3
    907: begin Ini := 139; Tam := 005; Cas := 2; Typ := 1; Fmt := ''; end;
    //C�digo da entidade benefici�ria 4
    908: begin Ini := 144; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
    //Percentual de cr�dito da entidade benefici�ria 4
    909: begin Ini := 149; Tam := 005; Cas := 2; Typ := 1; Fmt := ''; end;
    //C�digo da entidade benefici�ria 5
    910: begin Ini := 154; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
    //Percentual de cr�dito da entidade benefici�ria 5
    911: begin Ini := 159; Tam := 005; Cas := 2; Typ := 1; Fmt := ''; end;
    //C�digo da entidade benefici�ria 6
    912: begin Ini := 164; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
    //Percentual de cr�dito da entidade benefici�ria 6
    913: begin Ini := 169; Tam := 005; Cas := 2; Typ := 1; Fmt := ''; end;
    //Data da gera��o do arquivo
    990: begin Ini := 144; Tam := 008; Cas := 0; Typ := 2; Fmt := 'DDMMAAAA'; end;
    //Hora da gera��o do arquivo
    991: begin Ini := 152; Tam := 006; Cas := 0; Typ := 2; Fmt := 'HHMMSS'; end;
    //N�mero do lote no arquivo
    996: begin Ini := 004; Tam := 004; Cas := 0; Typ := 1; Fmt := ''; end;
    //N�mero sequencial do registro no lote
    997: begin Ini := 009; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
    //N�mero sequencial do arquivo
    998: begin Ini := 158; Tam := 006; Cas := 0; Typ := 1; Fmt := ''; end;
    // ID do documento no banco (ID_Link)
    1000:
    begin
      case banco of
        001:
        begin
          //Modificado em 06/02/2013 Ini O Creditor � diferente do Syndi2
          if (CO_DMKID_APP = 3) or (CO_DMKID_APP = 22) then //Creditor e Credito2
          begin
            Ini := 106;
            Tam := 025;
            Cas := 0;
            Typ := 1;
            Fmt := '';
          end else
          begin
            Ini := 048;
            Tam := 010;
            Cas := 0;
            Typ := 1;
            Fmt := '';
          end;
          //Modificado em 06/02/2013 Fim
        end;
        //Modificado em 30/07/2013 Ini
        008,
        033,
        353: begin Ini := 041; Tam := 012; Cas := 0; Typ := 1; Fmt := ''; end;
        //353: begin Ini := 041; Tam := 013; Cas := 0; Typ := 1; Fmt := ''; end;
        //Modificado em 30/07/2013 Fim
        104: begin Ini := 049; Tam := 008; Cas := 0; Typ := 1; Fmt := ''; end;
        756:
        begin
          if (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
          begin
            Ini := 060; Tam := 015; Cas := 0; Typ := 1; Fmt := '';
          end;
        end;
      end;
    end;
  end;
  Aux := Copy(Lista[Linha], Ini, Tam);
  Result := FormataLocDado(Aux, Fmt, Typ, Tam, Cas, Res);
  if not Result and (Mensagem <> '') then
    Geral.MB_Aviso(Mensagem);
end;

function TUBancos.LocDado400(const Banco, Campo, Linha: Integer;
const Lista: TStrings; const Mensagem, NomeLayout: String; var Res: String): Boolean;
var
  Ini, Tam, Cas, Typ: Integer;
  Fmt, Aux: String;
begin
  Ini := 0;
  Tam := 0;
  Cas := 0;
  Typ := -1;
  Fmt := '';
  case Campo of
    //C�digo do banco
    001:
    begin
      Ini := 077; Tam := 003; Cas := 0; Typ := 1; Fmt := '';
      {
      case Banco of
        001: begin Ini := 077; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 077; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 077; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 077; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 077; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 077; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 077; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        756: begin Ini := 077; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
      }
    end;
    //Nome do banco
    002:
    begin
      case Banco of
        001: begin Ini := 080; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 080; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 080; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 080; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 080; Tam := 012; Cas := 0; Typ := 1; Fmt := ''; end;
        756: begin Ini := 080; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 080; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //Codigo do servi�o
    003:
    begin
      Ini := 010; Tam := 002; Cas := 0; Typ := 1; Fmt := '';
      {
      case Banco of
        001: begin Ini := 010; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 010; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 010; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 010; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 010; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 010; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 010; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        756: begin Ini := 010; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
      }
    end;
    //Nome do servi�o
    004:
    begin
      case Banco of
        001: begin Ini := 012; Tam := 008; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 012; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 012; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 012; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 012; Tam := 008; Cas := 0; Typ := 1; Fmt := ''; end;
        756: begin Ini := 012; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //C�digo de retorno
    007:
    begin
      Ini := 002; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
      {
      case Banco of
        001: begin Ini := 002; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 002; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 002; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 002; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 002; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 002; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 002; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        756: begin Ini := 002; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
      }
    end;
    //Literal de retorno
    008:
    begin
      case Banco of
        001: begin Ini := 003; Tam := 007; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 003; Tam := 007; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 003; Tam := 007; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 003; Tam := 007; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 003; Tam := 007; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 003; Tam := 007; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 003; Tam := 007; Cas := 0; Typ := 1; Fmt := ''; end;
        756: begin Ini := 003; Tam := 007; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //C�digo de remessa ou retorno
    009:
    begin
      case Banco of
        756:
        begin
          if NomeLayout = CO_756_EXCEL_2013_07_18 then
            Ini := 021; Tam := 004; Cas := 0; Typ := 1; Fmt := '';
        end;
      end;
    end;
    //Identifica��o do registro header de arquivo
    010:
    begin
      case Banco of
        001: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        756: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //Identifica��o do registro detalhe
    011:
    begin
      case Banco of
        001: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        756: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //Identifica��o do registro trailler
    012:
    begin
      case Banco of
        001: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        756: begin Ini := 001; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //
    { Conta corrente com o d�gito
    015:
    begin
      case Banco of
        756: begin Ini := 025; Tam := 010; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    }
    //Quantidade de registros de transa��o
    301:
    begin
      case Banco of
        // 001: Ver
        341: begin Ini := 213; Tam := 008; Cas := 0; Typ := 1; Fmt := ''; end;
        //409: n�o tem
      end;
    end;
    //CNPJ/CPF Empresa (Cedente)
    401:
    begin
      // Para c�digo cedente ver 410 (ou 420 para multiempresa) !!!
      case Banco of
        //001: n�o tem
        104: begin Ini := 004; Tam := 014; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 004; Tam := 014; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 004; Tam := 014; Cas := 0; Typ := 1; Fmt := ''; end;
        // 399: n�o tem
        // 409: N�o tem
        748: begin Ini := 032; Tam := 014; Cas := 0; Typ := 1; Fmt := ''; end;
        756:
        begin
          if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
          begin
            Ini := 004; Tam := 014; Cas := 0; Typ := 1; Fmt := '';
          end else
            ; //756: // N�o informa
        end;
      end;
    end;
    //Nome Empresa M�e (Cedente)
    402:
    begin
      case Banco of
        001: begin Ini := 047; Tam := 030; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 047; Tam := 030; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 047; Tam := 030; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 047; Tam := 030; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 047; Tam := 030; Cas := 0; Typ := 1; Fmt := ''; end; // No HEADER !!!
        409: begin Ini := 047; Tam := 030; Cas := 0; Typ := 1; Fmt := ''; end;
        //748: // N�o informa
        //756: // N�o informa
      end;
    end;
    //C�digo do Cedente
    410:
    begin
      case Banco of
        001: begin Ini := 027; Tam := 014; Cas := 0; Typ := 1; Fmt := ''; end;  //Ag�ncia + DV + Conta + DV
        104: begin Ini := 027; Tam := 016; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 021; Tam := 017; Cas := 0; Typ := 1; Fmt := ''; end; // zero + Carteira + Ag�ncia + Conta corrente
        //341: // N�o informa,
        399: begin Ini := 027; Tam := 018; Cas := 0; Typ := 1; Fmt := ''; end;  // Ag�ncia + 00 + Conta + DV Conta
        409: begin Ini := 027; Tam := 011; Cas := 0; Typ := 1; Fmt := ''; end; // Ag�ncia + Conta + DV Conta
        748: begin Ini := 027; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
        756:
        begin
          if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
          begin
            Ini := 031; Tam := 007; Cas := 0; Typ := 1; Fmt := '';
          end else
          begin
            Ini := 038; Tam := 007; Cas := 0; Typ := 1; Fmt := '';
          end;
        end;
      end;
    end;
    //C�digo do Cedente multiempresa (no header!)
    420: // 2011-01-13
    begin
      case Banco of
        //341 N�o informa, vou usar ag�ncia(27 a 30) + zeros (31,32) + Conta (33 a 37) + DAC (38)
        341: begin Ini := 027; Tam := 012; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //C�digo do Cedente multiempresa (no detalhe!)
    430: // 2011-01-13
    begin
      case Banco of
        //341 N�o informa, vou usar ag�ncia(18 a 21) + zeros (22,23) + Conta (24 a 28) + DAC (29)
        341: begin Ini := 018; Tam := 012; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //Nosso n�mero
    501:
    begin
      case Banco of
        001: begin Ini := 064; Tam := 017; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 063; Tam := 011; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 071; Tam := 012; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 063; Tam := 008; Cas := 0; Typ := 1; Fmt := ''; end;  // 512 ???
        399: begin Ini := 063; Tam := 016; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 063; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 048; Tam := 015; Cas := 0; Typ := 1; Fmt := ''; end;
        756:
        begin
          if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
          begin
            Ini := 071; Tam := 011; Cas := 0; Typ := 1; Fmt := '';
          end else
          begin
            Ini := 071; Tam := 008; Cas := 0; Typ := 1; Fmt := '';
          end;
        end;
      end;
    end;
    //N�mero do documento  (n�mero duplicata � no c�digo 506)
    502:
    begin
      case Banco of
        001: begin Ini := 039; Tam := 025; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 038; Tam := 025; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 038; Tam := 025; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 038; Tam := 025; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 038; Tam := 016; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 038; Tam := 025; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 117; Tam := 010; Cas := 0; Typ := 1; Fmt := ''; end;
        756:
        begin
          if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
          begin
            Ini := 117; Tam := 010; Cas := 0; Typ := 1; Fmt := '';
          end else
            ;//756: n�o informa
        end;
      end;
    end;
    //Tipo de cobran�a (com ou sem registro) - Carteira
    503:
    begin
      case Banco of
        001: begin Ini := 081; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 014; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        756:
        begin
          if NomeLayout = CO_756_EXCEL_2013_07_18 then
          begin
            Ini := 108; Tam := 001; Cas := 0; Typ := 1; Fmt := '';
          end else
            ;//756: n�o informa
        end;
      end;
    end;
    //C�digo de ocorr�ncia / movimento do servi�o / Comando
    504:
    begin
      case Banco of
        001: begin Ini := 109; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 109; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 109; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 109; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 109; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 109; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 109; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        756: begin Ini := 109; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //Data de ocorr�ncia (na verdade � a data real que o cliente pagou, mas o banco demora para repassar para a c/c)
    505:
    begin
      case Banco of
        001: begin Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        104: begin Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        237: begin Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        341: begin Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        399: begin Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        409: begin Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        748: begin Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        756: begin Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
      end;
    end;
    //Seu n�mero (uso da empresa)
    506:
    begin
      case Banco of
        001: begin Ini := 117; Tam := 010; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 117; Tam := 010; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 117; Tam := 010; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 117; Tam := 010; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 117; Tam := 006; Cas := 0; Typ := 1; Fmt := ''; end; //PPPTTT  onde: PPP = Parcela e TTT = Total de Parcelas
        //409:  N�o informa
        748: begin Ini := 117; Tam := 010; Cas := 0; Typ := 1; Fmt := ''; end;
        //756: N�o informa
      end;
    end;
    //Esp�cie de documento
    507:
    begin
      case Banco of
        001: begin Ini := 174; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 174; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 174; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 174; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 174; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;  // default "99"
        409: begin Ini := 174; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;  // default "07"
        748: begin Ini := 175; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        //  N�o informa - tem o PAC no lugar  (???)
        //756: begin Ini := 175; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //C�digo da carteira
    508:
    begin
      case Banco of
        001: begin Ini := 107; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 107; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        // � mesmo ???
        237: begin Ini := 108; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 108; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 108; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 108; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;  // default "1"
        // 748: N�o informa. Ini=014 e Tam=001 > "A" = Cobran�a com registro
        756: begin Ini := 108; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //Motivos da ocorrencia / Movimento
    530:
    begin
      case Banco of
        001: begin Ini := 087; Tam := 002; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 080; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 319; Tam := 010; Cas := 0; Typ := 1; Fmt := ''; end;
        //outras informa��es ERROS
        341: begin Ini := 378; Tam := 008; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 123; Tam := 009; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 378; Tam := 008; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 319; Tam := 010; Cas := 0; Typ := 1; Fmt := ''; end;
        756:
        begin
          if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
          begin
            Ini := 319; Tam := 010; Cas := 0; Typ := 1; Fmt := '';
          end else
            //756: N�o informa
        end;
      end;
    end;
    //Valor (nominal) do t�tulo
    550:
    begin
      case Banco of
        001: begin Ini := 153; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        104: begin Ini := 153; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        237: begin Ini := 153; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        341: begin Ini := 153; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        399: begin Ini := 153; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        409: begin Ini := 153; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        748: begin Ini := 153; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        756: begin Ini := 153; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
      end;
    end;
    //Valor do abatimento concedido
    551:
    begin
      case Banco of
        001: begin Ini := 228; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        104: begin Ini := 228; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        237: begin Ini := 228; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        341: begin Ini := 228; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        // 399: N�o informa
        409: begin Ini := 228; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        748: begin Ini := 228; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //756: N�o informa
      end;
    end;
    //Valor do desconto concedido
    552:
    begin
      case Banco of
        001: begin Ini := 241; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        104: begin Ini := 241; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        237: begin Ini := 241; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        341: begin Ini := 241; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        399: begin Ini := 241; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        409: begin Ini := 241; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        748: begin Ini := 241; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //756: N�o informa
      end;
    end;
    //Valor efetivo (pago) lan�ado na conta
    553:
    begin
      case Banco of
        001: begin Ini := 254; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        104: begin Ini := 254; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        237: begin Ini := 254; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        341: begin Ini := 254; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        399: begin Ini := 254; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        409: begin Ini := 254; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        748: begin Ini := 254; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        756: begin Ini := 254; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
      end;
    end;
    //Valor de outros cr�ditos
    554:
    begin
      case Banco of
        001: begin Ini := 280; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //104: N�o informa
        237: begin Ini := 280; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        341: begin Ini := 280; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //399: N�o informa
        //409: N�o informa
        //748: N�o informa
        //756: N�o informa
      end;
    end;
    //Valor de juros de mora pago
    555:
    begin
      case Banco of
        001: begin Ini := 267; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        104: begin Ini := 267; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //237: Juros + Multa
        //341: Juros + Multa
        //399: Juros + Multa
        //409: Juros + Multa
        748: begin Ini := 267; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //756: N�o informa
      end;
    end;
    //Valor da multa pago
    556:
    begin
      case Banco of
        001: begin Ini := 280; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        104: begin Ini := 280; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //237: Juros + Multa
        //341: Juros + Multa
        //399: Juros + Multa
        //409: Juros + Multa
        748: begin Ini := 280; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //756: N�o informa
      end;
    end;
    //Valor da multa + juros de mora pagos
    557:
    begin
      case Banco of
        //001: juros e multa separados
        //104: juros e multa separados
        237: begin Ini := 267; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        341: begin Ini := 267; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        399: begin Ini := 267; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        409: begin Ini := 267; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //104: juros e multa separados
        //756: N�o informa
      end;
    end;
    //Valor do IOF
    569:
    begin
      case Banco of
        001: begin Ini := 215; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        104: begin Ini := 215; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        237: begin Ini := 215; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        399: begin Ini := 176; Tam := 011; Cas := 2; Typ := 1; Fmt := ''; end;
        409: begin Ini := 215; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //756: N�o informa
      end;
    end;
    //Despesas de cobran�a (Tarifa)
    570:
    begin
      case Banco of
        001: begin Ini := 182; Tam := 007; Cas := 2; Typ := 1; Fmt := ''; end;
        104: begin Ini := 176; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        237: begin Ini := 176; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        341: begin Ini := 176; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //399: N�o informa 
        409: begin Ini := 176; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        748: begin Ini := 176; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        756:
        begin
          if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
          begin
            Ini := 176; Tam := 013; Cas := 2; Typ := 1; Fmt := '';
          end else
            ;//756: N�o informa
        end;
      end;
    end;
    //Despesas de custas de protesto
    571:
    begin
      case Banco of
        //001: n�o informa
        //104: n�o informa
        237: begin Ini := 189; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        //341: n�o informa
        //399: n�o informa
        //409: n�o informa
        748: begin Ini := 189; Tam := 013; Cas := 2; Typ := 1; Fmt := ''; end;
        756:
        begin
          if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
          begin
            Ini := 189; Tam := 013; Cas := 2; Typ := 1; Fmt := '';
          end else
            ;//756: n�o informa
        end;
      end;
    end;
    //Data do vencimento
    580:
    begin
      case Banco of
        001: begin Ini := 147; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        104: begin Ini := 147; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        237: begin Ini := 147; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        341: begin Ini := 147; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        399: begin Ini := 147; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        409: begin Ini := 147; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        748: begin Ini := 147; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        756: begin Ini := 147; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
      end;
    end;
    //Data de cr�dito na conta corrente
    581:
    begin
      case Banco of
        001: begin Ini := 176; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'  ; end;
        104: begin Ini := 294; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMYY'  ; end;
        237: begin Ini := 296; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMYY'  ; end;
        341: begin Ini := 296; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMYY'  ; end;
        399: begin Ini := 083; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'  ; end;
        409: begin Ini := 293; Tam := 006; Cas := 0; Typ := 2; Fmt := 'YYMMDD'  ; end; // C U I D D A D O ao copiar !!!!
        748: begin Ini := 329; Tam := 008; Cas := 0; Typ := 2; Fmt := 'AAAAMMDD'; end; // C U I D D A D O ao copiar !!!!!
        756: begin Ini := 296; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'  ; end;
      end;
    end;
    //Data de d�bito da tarifa de cobran�a
    582:
    begin
      case Banco of
        001: begin Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'; end;
        104: begin Ini := 195; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMYY'; end;
        //237: N�o informa! Usar : "Data Ocorr�ncia no Banco" ???
        237: begin Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMYY'; end;
        //341: N�o informa
        399: begin Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMYY'; end; // Data da ocorr�ncia!
        //409: N�o informa
        //748: N�o informa
        756:
        begin
          if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
          begin
            Ini := 111; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMYY';
          end else
            ;//756: N�o informa
        end;
      end;
    end;
    //C�digo do Banco cobrador / recebedor
    600:
    begin
      case Banco of
        001: begin Ini := 166; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 166; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 166; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 166; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 166; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 166; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
        //748: N�o informa
        756: begin Ini := 166; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //C�digo da Ag�ncia recebedora / cobradora
    601:
    begin
      case Banco of
        001: begin Ini := 169; Tam := 004; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 169; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 169; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 169; Tam := 004; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 169; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 169; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
        //748: N�o informa
        756: begin Ini := 169; Tam := 005; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    //DV da Ag�ncia recebedora
    602:
    begin
      case Banco of
        001: begin Ini := 173; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        //104: j� est� no C�digo da ag�ncia recebedora / cobradora?
        //237: j� est� no C�digo da ag�ncia recebedora / cobradora?
        341: begin Ini := 173; Tam := 001; Cas := 0; Typ := 1; Fmt := ''; end;
        //399: j� est� no C�digo da ag�ncia recebedora / cobradora?
        //409: j� est� no C�digo da ag�ncia recebedora / cobradora?
        //748: N�o informa
        //756: j� est� no C�digo da ag�ncia recebedora / cobradora?
      end;
    end;
    //Data da gera��o do arquivo
    990:
    begin
      case Banco of
        001: begin Ini := 095; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'  ; end;
        104: begin Ini := 095; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'  ; end;
        237: begin Ini := 095; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'  ; end;
        341: begin Ini := 095; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'  ; end;
        399: begin Ini := 095; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'  ; end;
        409: begin Ini := 095; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'  ; end;
        748: begin Ini := 095; Tam := 008; Cas := 0; Typ := 2; Fmt := 'AAAAMMDD'; end;
        756: begin Ini := 095; Tam := 006; Cas := 0; Typ := 2; Fmt := 'DDMMAA'  ; end;
      end;
    end;
    (*
    //Hora da gera��o do arquivo
    991:
    begin
      case Banco of
        756: begin Ini := 309; Tam := 003; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    *)
    (*
    992:
    begin
      case Banco of
        756: begin Ini := 312; Tam := 006; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    *)
    //N�mero sequencial do registro
    999:
    begin
      case Banco of
        001: begin Ini := 395; Tam := 006; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 395; Tam := 006; Cas := 0; Typ := 1; Fmt := ''; end;
        237: begin Ini := 395; Tam := 006; Cas := 0; Typ := 1; Fmt := ''; end;
        341: begin Ini := 395; Tam := 006; Cas := 0; Typ := 1; Fmt := ''; end;
        395: begin Ini := 395; Tam := 006; Cas := 0; Typ := 1; Fmt := ''; end;
        409: begin Ini := 395; Tam := 006; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 395; Tam := 006; Cas := 0; Typ := 1; Fmt := ''; end;
        756: begin Ini := 395; Tam := 006; Cas := 0; Typ := 1; Fmt := ''; end;
      end;
    end;
    1000: // parte do "nosso n�mero" (501)
    //ver como foi feito na function GeraNossoNumero para saber quais casas copiar
    begin
      case Banco of
        //001: begin Ini := 064; Tam := 017; Cas := 0; Typ := 1; Fmt := ''; end;
        104: begin Ini := 065; Tam := 008; Cas := 0; Typ := 1; Fmt := ''; end;
        //Mudado em 05/06/2014 Ini => No Creditor (no Credito2 n�o) a linkagem do retorno � diferente dos demais aplicativos
        237:
        begin
          if (CO_DMKID_APP = 3) then //Creditor
          begin
            Ini := 038;
            Tam := 025;
            Cas := 0;
            Typ := 1;
            Fmt := '';
          end else
          begin
            Ini := 071;
            Tam := 011;
            Cas := 0;
            Typ := 1;
            Fmt := '';
          end;
        end;
        //Mudado em 05/06/2014 Fim
        341: begin Ini := 086; Tam := 008; Cas := 0; Typ := 1; Fmt := ''; end;
        399: begin Ini := 063; Tam := 013; Cas := 0; Typ := 1; Fmt := ''; end; // + 3 de verifica��o = 16 d�gitos
        409: begin Ini := 063; Tam := 014; Cas := 0; Typ := 1; Fmt := ''; end;
        748: begin Ini := 048; Tam := 008; Cas := 0; Typ := 1; Fmt := ''; end;
        756:
        begin
          if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
          begin
            Ini := 071; Tam := 011; Cas := 0; Typ := 1; Fmt := '';
          end else
          begin
            Ini := 071; Tam := 008; Cas := 0; Typ := 1; Fmt := '';
          end;
        end;
      end;
    end;
  end;
  {
  if (Ini = 0) or (Tam = 0) then
    Geral.MB_('O campo ' + IntToStr(Campo) + ' n�o est� ' +
    'definido para o banco ' + FormatFloat('000', Banco) + '.+ sLineBreak +
    'Avise a DERMATEK!'))
  else begin
  }
  Aux    := Copy(Lista[Linha], Ini, Tam);
  Result := FormataLocDado(Aux, Fmt, Typ, Tam, Cas, Res);
  if not Result and (Mensagem <> '') then
    Geral.MB_Aviso(Mensagem);
end;

procedure TUBancos.MostraFormBancos(Banco: Integer);
begin
  Application.CreateForm(TFmBancos, FmBancos);
  if Banco <> 0 then
    FmBancos.LocCod(Banco, Banco);
  FmBancos.ShowModal;
  FmBancos.Destroy;
end;

function TUBancos.OcorrenciaConhecida(Banco: Integer; Envio: TEnvioCNAB;
Movimento, TamReg: Integer; Registrado: Boolean; NomeLayout: String): Boolean;
var
  Txt: String;
begin
  Txt := CNABTipoDeMovimento(Banco, Envio, Movimento, TamReg, Registrado, NomeLayout);
  //
  if Length(Trim(Txt)) > 1 then
    Result := Copy(Txt, 1, 2) <> '**'
  else
    Result := False;
end;

function TUBancos.FormataLocDado(const Dado, Formato: String;
const Tipo, Tam, Cas: Integer; var Res: String): Boolean;
begin
  Result := Length(Dado) = Tam;
  case Tipo of
    -1: Res := '';
    1:
    begin
     if Cas > 0 then
       Res := dmkPF.XFT(Dado, Cas, siPositivo)
     else
       Res := Dado;
    end;
    2:
    begin
      if Dado = '' then
        Res := ''
      else
        Res := dmkPF.CDS2(Dado, Formato, 3);
    end else
    begin
      Res := Dado;
      Geral.MB_Aviso('Formato n�o definido em "FormataLocDado"');
    end;
  end;
end;

function TUBancos.ValorPadrao(TamReg, Banco, Campo: Integer;
  NomeLayout: String = ''): String;
begin
  case Campo of
    // C�digo do servi�o
    003:
    case TamReg of
      240: Result := '0000';
      400: Result := '01';
      else Result := '';
    end;
    // C�digo de retorno
    007: Result := '2';
    010: Result := '0';
    031:
    begin
      if NomeLayout = CO_756_CORRESPONDENTE_BB_2015 then
        Result := 'T'
      else
        Result := '';
    end
    else Result := '';
  end;
end;

{
function TUBancos.DescricaoDeArrStrStr(const Codigo: String; const ArrTextos:
MyArrayLista; var Texto: String; ColLoc, ColSel: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  Texto  := '';
  for i := Low(ArrTextos) to High(ArrTextos) do
    if Uppercase(ArrTextos[i][ColLoc]) = Uppercase(Codigo) then
    begin
      Result := True;
      Texto := ArrTextos[i][ColSel];
      Break;
    end;
end;

function TUBancos.DescricaoDeArrStrStr2(const Codigo: String; const ArrTextos:
MyArrayLista; var Texto1, Texto2: String; ColLoc, ColSel1, ColSel2: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  Texto1 := '';
  Texto2 := '';
  for i := Low(ArrTextos) to High(ArrTextos) do
    if Uppercase(ArrTextos[i][ColLoc]) = Uppercase(Codigo) then
    begin
      Result := True;
      Texto1 := ArrTextos[i][ColSel1];
      Texto2 := ArrTextos[i][ColSel2];
      Break;
    end;
end;

function TUBancos.DescricaoEAvisosDeArrStrStr(const Codigo: String; const ArrTextos,
ArrAvisos: MyArrayLista; var Texto, Aviso: String; ColLocTexto, ColSelTexto,
ColSelLocAv, ColLocAviso, ColSelAviso: Integer): Integer;
  procedure AvisoDeArrStr(const CodLoc: String; const ColLoc, ColSel: Integer;
  var Aviso: String; var Res: Integer);
  var
    i: Integer;
  begin
    for i := Low(ArrAvisos) to High(ArrAvisos) do
    if ArrAvisos[i][ColLoc] = CodLoc then
    begin
      Res   := 3;
      if Length(Aviso) = 0 then
        Aviso := ArrAvisos[i][ColSel]
      else
        Aviso := Aviso + sLineBreak + ArrAvisos[i][ColSel];
      Break;
    end;
  end;
var
  i, pI, pF: Integer;
  Codes, SubCod: String;
begin
  Result := 0;
  Texto  := '';
  Aviso  := '';
  for i := Low(ArrTextos) to High(ArrTextos) do
  if ArrTextos[i][ColLocTexto] = Codigo then
  begin
    Result := 1;
    Texto  := Trim(ArrTextos[i][ColSelTexto]);
    Codes  := Trim(ArrTextos[i][ColSelLocAv]);
    Break;
  end;
  //
  if Result = 1 then
  begin
    if Codes  <> '' then
    begin
      Result := 2;
      pI := 1;
      while Length(Codes) > 0 do
      begin
        pF := Pos(',', Codes);
        if pF > 0 then
        begin
          SubCod := Copy(Codes, pI, pF-1);
          Codes  := Copy(Codes, pF+1, Length(Codes));
          AvisoDeArrStr(SubCod, ColLocAviso, ColSelAviso, Aviso, Result);
        end else
        begin
          AvisoDeArrStr(Codes, ColLocAviso, ColSelAviso, Aviso, Result);
          Codes := '';
        end;
      end;
    end;
  end;
end;

function TUBancos.DescricaoEAvisosEItensDeArrStrStr(const Codigo: String;
const ArrTextos, ArrAvisos: MyArrayLista; var Texto, Aviso, Item1, Item2: String;
ColLocTexto, ColSelTexto, ColSelLocAv, ColLocAviso, ColSelAviso, CodItem1,
CodItem2: Integer): Integer;
  procedure AvisoDeArrStr(const CodLoc: String; const ColLoc, ColSel: Integer;
  var Aviso: String; var Res: Integer);
  var
    i: Integer;
  begin
    for i := Low(ArrAvisos) to High(ArrAvisos) do
    if ArrAvisos[i][ColLoc] = CodLoc then
    begin
      Res   := 3;
      if Length(Aviso) = 0 then
        Aviso := ArrAvisos[i][ColSel]
      else
        Aviso := Aviso + sLineBreak + ArrAvisos[i][ColSel];
      Break;
    end;
  end;
var
  i, pI, pF: Integer;
  Codes, SubCod: String;
begin
  Result := 0;
  Texto  := '';
  Aviso  := '';
  for i := Low(ArrTextos) to High(ArrTextos) do
  if ArrTextos[i][ColLocTexto] = Codigo then
  begin
    Result := 1;
    Texto  := Trim(ArrTextos[i][ColSelTexto]);
    Item1  := Trim(ArrTextos[i][CodItem1]);
    Item2  := Trim(ArrTextos[i][CodItem2]);
    Codes  := Trim(ArrTextos[i][ColSelLocAv]);
    Break;
  end;
  //
  if Result = 1 then
  begin
    if Codes  <> '' then
    begin
      Result := 2;
      pI := 1;
      while Length(Codes) > 0 do
      begin
        pF := Pos(',', Codes);
        if pF > 0 then
        begin
          SubCod := Copy(Codes, pI, pF-1);
          Codes  := Copy(Codes, pF+1, Length(Codes));
          AvisoDeArrStr(SubCod, ColLocAviso, ColSelAviso, Aviso, Result);
        end else
        begin
          AvisoDeArrStr(Codes, ColLocAviso, ColSelAviso, Aviso, Result);
          Codes := '';
        end;
      end;
    end;
  end;
end;
}

{ Geral.CriaSelLista
function TUBancos.CriaSelLista(Lista: MyArrayLista; TituloForm: String;
TitulosColunas: array of String): Boolean;
var
  n, i, k, j: Integer;
  t: array of Integer;
begin
  Result := False;
  n := High(Lista);
  if n = -1 then
  begin
    Geral.MB_('Lista n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  k := High(lista[0]);
  if k = -1 then
  begin
    Geral.MB_('Lista sem itens!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  SetLength(t, k + 1);
  // Largura m�nima das colunas
  for i := 0 to k do
    t[i] := 28;
  if n = 0 then
  begin
    Geral.MB_('N�o h� itens a escolher para a op��o selecionada!',
    'Aviso', MB_OK+MB_ICONINFORMATION);
    Exit;
  end;
  if DBCheck.CriaFm(TFmSelLista, FmSelLista, afmoNegarComAviso) then
  begin
    FmSelLista.Caption := TituloForm;
    FmSelLista.Grade.ColCount := k + 1;
    FmSelLista.Grade.RowCount := n + 2;
    for i := Low(TitulosColunas) to High(TitulosColunas) do
      FmSelLista.Grade.Cells[i, 0] := TitulosColunas[i];
    for i := Low(Lista)  to n do
    begin
      for j := Low(lista[i]) to k do
      begin
        if Length(Lista[i][j]) * 7 > t[j] then
          t[j] := Length(Lista[i][j]) * 7;
        //if Length(Lista[i][1]) * 7 > t1 then
          //t1 := Length(Lista[i][1]) * 7;
        FmSelLista.Grade.Cells[j, i + 1] := Lista[i][j];
        //FmSelLista.Grade.Cells[1, i + 1] := Lista[i][1];
      end;
    end;
    n := 0;
    for i := 0 to k do
    begin
      FmSelLista.Grade.ColWidths[i] := t[i];
      n := n + t[i];
    end;
    n := n + 36;
    (*
    FmSelLista.Grade.ColWidths[0] := t0;
    FmSelLista.Grade.ColWidths[1] := t1;
    *)
    if n > Screen.Width - 16 then n := Screen.Width - 16;
    FmSelLista.Width := n;
    FmSelLista.ShowModal;
    Result := True;
  end;
end;
}

{  Geral.SelecionaItem
function TUBancos.SelecionaItem(Lista: MyArrayLista; TituloForm: String;
TitulosColunas: array of String): String;
begin
  if CriaSelLista(Lista, TituloForm, TitulosColunas) then
  begin
    if FmSelLista.FResult > -1 then
      Result := Lista[FmSelLista.FResult][0];
    FmSelLista.Destroy;
  end;
end;
}

function TUBancos.DescricaoDoMeuCodigoCNAB(Codigo: Integer): String;
begin
  case Codigo of
    -02: Result := 'Preenchimento com espa�os brancos';
    -01: Result := 'Preenchimento com zeros';
    000: Result := 'In�cuo';
    001: Result := 'C�digo do banco';
    002: Result := 'Nome do banco';
    003: Result := 'Codigo do servi�o (Lote de Servi�o)';
    004: Result := 'Nome do servi�o';
    005: Result := 'C�digo de remessa';
    006: Result := 'Literal de remessa';
    007: Result := 'C�digo de retorno';
    008: Result := 'Literal de retorno';
    009: Result := 'C�digo de remessa ou retorno';
    010: Result := 'Identifica��o do registro header de arquivo';
    011: Result := 'Identifica��o do registro detalhe (1,2,3,4,5,7)';
    012: Result := 'Identifica��o do registro trailler';
    013: Result := 'Identifica��o do registro header de lote';
    // 014 a 018 N�o usa porque tem valor default pelo campo 011
    014: Result := 'Identifica��o do registro'; // 0 a 9
    015: Result := 'Identifica��o do segmento'; // A a Z
    016: Result := 'Identifica��o do sub-segmento'; // Ex: Y01, Y04, Y05, Y50 e Y51
    {
    014: Result := 'Identifica��o do registro detalhe 2 (Mensagens)';
    015: Result := 'Identifica��o do registro detalhe 3 (Rateio de cr�dito)';
    //015: Result := 'Identifica��o do registro detalhe 4 (Existe????)';
    016: Result := 'Identifica��o do registro detalhe 5 (email + Sacado - Ita�)';
    //017: Result := 'Identifica��o do registro detalhe 6 (Existe????)';
    //018: Result := 'Identifica��o do registro detalhe 7 (Existe????)';
    //???: Result := 'Identifica��o do registro detalhe 8 (Existe????)';
    }
    019: Result := 'C�digo de movimento';
    020: Result := 'C�digo da Ag�ncia da Empresa';
    021: Result := 'N�mero da Conta Corrente da Empresa';
    022: Result := 'D�gito verificador Ag�ncia Empresa';
    023: Result := 'D�gito verificador Conta Empresa';
    024: Result := 'D�gito verificador Ag�ncia e Conta Empresa';
    025: Result := 'C�digo da Ag�ncia cobradora (o banco � que define)';
    026: Result := 'DV da Ag�ncia cobradora (o banco � que define)';
    027: Result := 'C�digo da banco cobrador';
    028: Result := 'Tipo do Servi�o - Header de Arquivo'; // CNAB240 02=Cobran�a sem papel
    029: Result := 'Tipo do Servi�o - Header de Lote';
    030: Result := 'C�digos de Ocorr�ncia dos Tipos do Servi�o'; // CNAB240
    031: Result := 'Tipo do Opera��o'; // CNAB240
    032: Result := 'N�mero remessa / retorno';
    033: Result := 'Data de grava��o remessa / retorno';
    034: Result := 'Forma de cadastramento do t�tulo no banco'; 
    035: Result := 'Sub-conta da conta corrente';
    036: Result := 'Identifica��o de cobran�a sem papel "CSP"';
    037: Result := 'C�digo especial para cada banco (Identifica o cliente no arquivo remessa)';
    038: Result := 'Complemento do c�digo especial de cada banco';

    040: Result := 'C�digo da Ag�ncia do Cliente';
    041: Result := 'N�mero da Conta Corrente do Cliente';
    042: Result := 'D�gito verificador Ag�ncia do Cliente';
    043: Result := 'D�gito verificador Conta Corrente do Cliente';
    044: Result := 'D�gito verificador Ag�ncia e Conta Corrente do Cliente';

    080: Result := 'Identifica��o do segmento P';
    081: Result := 'Identifica��o do segmento Q';
    082: Result := 'Identifica��o do segmento R';
    083: Result := 'Identifica��o do segmento S';
    084: Result := 'Identifica��o do segmento T';
    085: Result := 'Identifica��o do segmento U';

    089: Result := 'Identifica��o do segmento Y';

    099: Result := 'Identifica��o do sistema';
    // D�bito autom�tico
    100: Result := 'C�digo do Aviso para D�bito Autom�tico';
    101: Result := 'C�digo do Banco do d�bito';
    102: Result := 'N�mero da Ag�ncia do d�bito';
    103: Result := 'D�gito verificador da Ag�ncia do d�bito';
    104: Result := 'N�mero da Conta Corrente do d�bito';
    105: Result := 'D�gito verificador da Conta do d�bito';
    106: Result := 'D�gito verificador Ag�ncia e Conta do d�bito';

    193: Result := 'Complemento da ocorr�ncia do sacado'; // Retoro FEBRABAN
    194: Result := 'Valor da ocorr�ncia do sacado'; // Retoro FEBRABAN
    195: Result := 'Data da ocorr�ncia do sacado'; // Retoro FEBRABAN
    196: Result := 'C�digo de ocorr�ncias do sacado'; // Remessa/Retoro FEBRABAN

    // Registro "3" Segmento "S"
    197: Result := 'Identifica��o da impress�o';
    198: Result := 'Numero da linha a ser impressa';
    199: Result := 'Mensagem a ser impressa';
    200: Result := 'Tipo de caracter a ser impresso';
    //
    // Registro 2
    201: Result := 'Mensagem 1';
    202: Result := 'Mensagem 2';
    203: Result := 'Mensagem 3';
    204: Result := 'Mensagem 4';
    // CNAB 240: Registro "3" Segmento "S"
    205: Result := 'Mensagem 5';
    206: Result := 'Mensagem 6';
    207: Result := 'Mensagem 7';
    208: Result := 'Mensagem 8';
    209: Result := 'Mensagem 9';
    //210: Result := 'Mensagem 10';
    //
    211: Result := 'Mensagem 1 (CNAB 240 - 40 caracteres - impressa em todos bloquetos do mesmo lote)';
    212: Result := 'Mensagem 2 (CNAB 240 - 40 caracteres - impressa em todos bloquetos do mesmo lote)';
    // NO BB (BANCO 001) as mensagem 3 e 4 prevalecem sobre as mensagens 1 e 2!!!
    213: Result := 'Mensagem 3 (CNAB 240 - 40 caracteres - impressa em todos bloquetos do mesmo lote)';
    214: Result := 'Mensagem 4 (CNAB 240 - 40 caracteres - impressa em todos bloquetos do mesmo lote)';
    //
    301: Result := 'Quantidade de registros de transa��o';
    302: Result := 'Valor dos t�tulos informados no arquivo';

    // FEBRABAN
    // Rateio de cr�dito
    350: Result := 'C�digo do C�lculo de Rateio p/ Benefici�rio'; // 1.Valor Cobrado  2.Valor Registro      3.Rateio p/ Menor Valor
    351: Result := 'Tipo de Valor Informado [Rateio de cr�dito]'; // 1.% (Percentual) 2.Valor ou Quantidade
    352: Result := 'Valor ou % (Percentual) [Rateio de cr�dito]';
    353: Result := 'C�digo do Banco para rateio de cr�dito';
    354: Result := 'N�mero da Ag�ncia para rateio do cr�dito';
    355: Result := 'D�gito verificador da Ag�ncia para rateio do cr�dito';
    356: Result := 'N�mero da Conta Corrente para rateio do cr�dito';
    357: Result := 'D�gito verificador da Conta para rateio do cr�dito';
    358: Result := 'D�gito verificador Ag�ncia e Conta para rateio do cr�dito';
    359: Result := 'Nome do benefici�rio  [Rateio de cr�dito]';
    360: Result := 'Identifica��o da Parcela do Rateio';
    361: Result := 'Qtde Dias p/ Cr�dito do Benefici�rio';
    362: Result := 'Data Cr�dito Benefici�rio';
    363: Result := 'Identifica��o das Rejei��es  [Rateio de cr�dito]';
    // fim - Rateio de cr�dito

    // in�cio NFs
    371: Result := 'N�mero da Nota Fiscal';
    372: Result := 'Valor da Nota Fiscal';
    373: Result := 'Data da Nota Fiscal';
    // fim - NFs -
    // fim - FEBRABAN

    400: Result := 'Identifica��o do tipo de inscri��o da Empresa (Cedente)';
    401: Result := 'CNPJ/CPF Empresa (Cedente)';
    402: Result := 'Nome Empresa M�e (Cedente)';

    408: Result := 'C�digo do Cedente (FEBRABAN)';
    409: Result := 'C�digo do Cedente (Conv�nio L�der)';
    410: Result := 'C�digo do Cedente (Conv�nio Cobran�a)';
    411: Result := 'D�gito do cedente';
    420: Result := 'C�digo do Cedente (Multi-empresas - Empresa m�e)';
    430: Result := 'C�digo do Cedente (Multi-empresas - Empresa cliente)';

    500: Result := 'Uso reservado da empresa (header)';
    501: Result := 'Nosso n�mero';
    502: Result := 'N�mero do documento';
    503: Result := 'Tipo de cobran�a (com ou sem registro)';
    504: Result := 'C�digo de ocorr�ncia / movimento do servi�o';
    505: Result := 'Data de ocorr�ncia';
    506: Result := 'Seu n�mero (uso da empresa)';
    507: Result := 'Esp�cie de documento (t�tulo)';
    508: Result := 'C�digo da carteira';
    509: Result := 'N�mero da carteira';
    //510: Result := 'Nosso n�mero sem o DAC';   mesmo que 501 ???
    511: Result := 'DAC do Nosso n�mero';
    512: Result := 'Seqencial do Nosso Numero'; // Itau Remessa / (retorno ?)
    513: Result := 'N�mero da parcela da fatura (Banco 756)';
    514: Result := 'N�mero do Contrato de Garantia (Banco 756)';
    515: Result := 'DV do N�mero do Contrato de Garantia (Banco 756)';
    516: Result := 'N�mero do Border�';

    520: Result := 'Aceite do t�tulo';

    530: Result := 'Motivos da ocorrencia / Movimento';

    549: Result := 'C�digo da moeda';
    550: Result := 'Valor (nominal) do t�tulo';
    551: Result := 'Valor do abatimento concedido';
    552: Result := 'Valor do desconto concedido';
    553: Result := 'Valor efetivo (pago) lan�ado na conta';
    554: Result := 'Valor de outros cr�ditos';
    555: Result := 'Valor de juros de mora pago';
    556: Result := 'Valor da multa pago';
    557: Result := 'Valor da multa + juros de mora pagos';
    558: Result := 'Valor de desconto bonifica��o por dia (Bradesco)';

    569: Result := 'Valor do IOF';
    570: Result := 'Despesas de cobran�a (Tarifa)';
    571: Result := 'Despesas de custas de protesto';
    572: Result := 'Valor de mora/dia a cobrar ap�s atraso';
    573: Result := 'Valor de multa a cobrar ap�s atraso';
    574: Result := 'Percentual de mora/m�s a cobrar ap�s atraso';
    575: Result := 'Percentual de multa a cobrar ap�s atraso';
    576: Result := 'Como cobrar os juros (valor ou %)';
    577: Result := 'Como cobrar a multa (valor ou %)';
    578: Result := 'Valor total pago';
    579: Result := 'Valor cr�dito bruto';
    580: Result := 'Data do vencimento';
    581: Result := 'Data de cr�dito na conta corrente';
    582: Result := 'Data de d�bito da tarifa de cobran�a';
    583: Result := 'Data de emiss�o do t�tulo';
    584: Result := 'Data do inicio de cobran�a de mora';
    585: Result := 'Valor de outras despesas';
    586: Result := 'Data limite para concess�o de desconto';
    587: Result := 'Data da cobran�a da multa';

    591: Result := 'C�digo do desconto 1';
    592: Result := 'Data do desconto 1';
    593: Result := 'Valor do desconto 1';
    594: Result := 'C�digo do desconto 2';
    595: Result := 'Data do desconto 2';
    596: Result := 'Valor do desconto 2';
    597: Result := 'C�digo do desconto 3';
    598: Result := 'Data do desconto 3';
    599: Result := 'Valor do desconto 3';

    600: Result := 'C�digo do Banco recebedor';
    601: Result := 'C�digo da Ag�ncia recebedora';



//  IN�CIO BRADESCO (CNAB 400)
    610: Result := 'C�digo do Banco na C�mara de Compensa��o (para d�bito em conta do sacado)';
    611: Result := 'C�digo da Ag�ncia de d�bito (d�bito em conta do sacado)';
    612: Result := 'DAC da Ag�ncia de d�bito (d�bito em conta do sacado)';
    613: Result := 'Raz�o da conta (Bradesco ex. 07050) (d�bito em conta do sacado)';
    614: Result := 'N�mero da conta corrente (d�bito em conta do sacado)';
    615: Result := 'DAC da conta corrente (d�bito em conta do sacado)';
    621: Result := 'Quem imprime o bloqueto?';
    622: Result := 'Condi��o de registro para d�bito autom�tico (Bradesco)';
    623: Result := 'Identificador de rateio de cr�dito (Bradesco)';
    624: Result := 'Endere�amento para aviso de d�bito autom�tico';
    625: Result := 'C�digo de c�lculo do rateio';
    626: Result := 'Tipo de valor informado no rateio (% ou valor)';
    627: Result := 'Quem distribui o bloqueto? (Tipo de distribui��o)';

    630: Result := 'C�digo do banco para cr�dito do 1� benefici�rio';
    631: Result := 'C�digo da ag�ncia para cr�dito do 1� benefici�rio';
    632: Result := 'DAC da ag�ncia para cr�dito do 1� benefici�rio';
    633: Result := 'N�mero da conta corrente para cr�dito do 1� benefici�rio';
    634: Result := 'DAC da conta corrente para cr�dito do 1� benefici�rio';
    635: Result := 'Valor ou % para reteio para cr�dito do 1� benefici�rio';
    636: Result := 'Nome do 1� benefici�rio';
    637: Result := 'Parcela do 1� benefici�rio';
    638: Result := 'Floating para o 1� benefici�rio';
    639: Result := 'Mensagem 1 (Bradesco - 12 posi��es)';
    640: Result := 'Mensagem 2 (Bradesco - 60 posi��es)';
//  FIM BRADESCO

//  IN�CIO BANCO DO BRASIL (CNAB 400?)
    643: Result := 'Indicativo de Mensagem ou Sacador/Avalista';
    644: Result := 'Tipo de cobran�a (Forma de registro)';
    645: Result := 'Varia��o da carteira';
    646: Result := 'Comando';
    647: Result := 'N�mero de dias para protesto';
    648: Result := 'DV do Nosso N�mero (BB)';  //?????
    649: Result := 'Prefixo do t�tulo';
    650: Result := 'Mensagem 40 cararcteres';
    651: Result := 'C�digo para protesto';
    652: Result := 'C�digo para baixa / devolu��o';
    653: Result := 'N�mero de dias para baixa / devolu��o';
    654: Result := 'N�mero do contrato da opera��o de cr�dito'; // Tamb�m FEBRABAN 240
    655: Result := 'Carteira de cobran�a - para o caso de cobran�a cedente';
    656: Result := 'Varia��o da carteira de cobran�a - para o caso de cobran�a cedente';
//  FIM BANCO DO BRASIL

    660: Result := 'PERC. CR�DITO ENT. RECEBEDORA'; // ITA�

    665: Result := 'TIPO DE BLOQUETO UTILIZADO'; // HSBC 400 DIRETIVA

    666: Result := 'Subsequencias do registro'; // SANTANDER BANESPA
    667: Result := 'Valor do t�tulo em outra unidade (consultar banco)'; // SANTANDER BANESPA

    670: Result := 'Identifica��o do tipo de registro (CNAB)';
    671: Result := 'Identifica��o do tipo de segmento (CNAB 240)';

    699: Result := 'C�digo oculto do banco';

    701: Result := 'Instru��o 1 de cobran�a';
    702: Result := 'Instru��o 2 de cobran�a';

    // N�o est� usando - usa 950 para o banco 341
    711: Result := 'Dias da instru��o 1';
    712: Result := 'Dias da Instru��o 2';
    //
    720: Result := 'Instru��o de n�o recebimento';
    //
    749: Result := 'Quantidade de moeda vari�vel';
    750: Result := 'C�digo de instru��o / alega��o (a ser cancelada)';

    780: Result := 'CMC7 do cheque';

    790: Result := 'Email para envio de informa��es'; //Segmento Y
    791: Result := 'DDD do CELULAR para envio de informa��es via SMS'; //Segmento Y
    792: Result := 'N�mero do CELULER sem o DDD para envio de informa��es via SMS'; //Segmento Y

    801: Result := 'Tipo de inscri��o do sacado';
    802: Result := 'N�mero da inscri��o (CPF/CNPJ) do sacado';
    803: Result := 'Nome do Sacado';
    804: Result := 'Logradouro completo do sacado (rua, n�mero e complemento)';
    805: Result := 'Bairro do sacado';
    806: Result := 'CEP do sacado';
    807: Result := 'Cidade do sacado';
    808: Result := 'UF do sacado';

    811: Result := 'Tipo de logradouro do sacado';
    812: Result := 'Logradouro do sacado';
    813: Result := 'N�mero do logradouro do sacado';
    814: Result := 'Complemento do logradouro do sacado';
    815: Result := 'E-mail do sacado';

    851: Result := 'Tipo de inscri��o do sacador / avalista';
    852: Result := 'N�mero da inscri��o (CPF/CNPJ) do sacador / avalista';
    853: Result := 'Nome do sacador / Avalista';
    854: Result := 'Logradouro completo do sacador / avalista (rua, n�mero e complemento)';
    855: Result := 'Bairro do sacador / avalista';
    856: Result := 'CEP do sacador / avalista';
    857: Result := 'Cidade do sacador /avalista';
    858: Result := 'UF do sacador / avalista';

    861: Result := 'Tipo de logradouro do sacador / avalista';
    862: Result := 'Logradouro do sacador / avalista';
    863: Result := 'N�mero do logradouro do sacador / avalista';
    864: Result := 'Complemento do logradouro do sacador / avalista';
    //865: Result := 'Emeio do sacador / avalista';  Precisa?

    887: Result := 'Layout do lote';
    888: Result := 'Densidade do arquivo';
    889: Result := 'Layout do arquivo';
    890: Result := 'Unidade da densidade de grava��o do arquivo';
    891: Result := 'Vers�o de remessa (o cliente informa ao banco e o sistema dele controla (Bco 033))';

    900: Result := 'C�digo da entidade recebedora';
    901: Result := 'Percentual de cr�dito da entidade recebedora';
    902: Result := 'C�digo da entidade benefici�ria 1';
    903: Result := 'Percentual de cr�dito da entidade benefici�ria 1';
    904: Result := 'C�digo da entidade benefici�ria 2';
    905: Result := 'Percentual de cr�dito da entidade benefici�ria 2';
    906: Result := 'C�digo da entidade benefici�ria 3';
    907: Result := 'Percentual de cr�dito da entidade benefici�ria 3';
    908: Result := 'C�digo da entidade benefici�ria 4';
    909: Result := 'Percentual de cr�dito da entidade benefici�ria 4';
    910: Result := 'C�digo da entidade benefici�ria 5';
    911: Result := 'Percentual de cr�dito da entidade benefici�ria 5';
    912: Result := 'C�digo da entidade benefici�ria 6';
    913: Result := 'Percentual de cr�dito da entidade benefici�ria 6';

    950: Result := 'Quantidade de dias (341)';

    990: Result := 'Data da gera��o do arquivo';
    991: Result := 'Hora da gera��o do arquivo';
    992: Result := 'Quantidade de registros do lote';
    993: Result := 'Quantidade de lotes do arquivo';
    994: Result := 'Quantidade de registros do arquivo';
    995: Result := 'Quantidade de contas p/ conc. - Lotes';
    996: Result := 'N�mero do lote no arquivo';
    997: Result := 'N�mero sequencial do registro no lote';
    998: Result := 'N�mero sequencial do arquivo';
    999: Result := 'N�mero sequencial do registro no arquivo';
   1000: Result := 'Meu ID (identificador do documento no sistema)';
   // Necess�rio para recarregamento da tabela no BD (CNAB_Fld)
   else  Result := '';
  end;
end;

procedure TUBancos.RecarregaMesuCodigosDeCamposCNAB();
var
  i, n: Integer;
  t: String;
begin
  Screen.Cursor := crHourGlass;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM cnab_fld');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO cnab_fld SET ');
  Dmod.QrUpd.SQL.Add('Codigo=:P0, Nome=:P1 ');
  n := 0;
  for i := -2 to 1000 do
  begin
    t := UBancos.DescricaoDoMeuCodigoCNAB(i);
    if t <> '' then
    begin
      n := n + 1;
      Dmod.QrUpd.Params[0].AsInteger := i;
      Dmod.QrUpd.Params[1].AsString  := t;
      Dmod.QrUpd.ExecSQL;
    end;
  end;
  //
  Geral.MB_Aviso('Foram inclu�dos ' + IntToStr(n) + ' registros!');
  Screen.Cursor := crDefault;
end;

function TUBancos.CNABEnvio_ecnabToTxt(Envio: TEnvioCNAB): String;
begin
  case Envio of
    ecnabRemessa: Result := 'Remessa de arquivo CNAB';
    ecnabRetorno: Result := 'Retorno de arquivo CNAB';
    ecnabBloquet: Result := 'Gera��o de Bloqueto';
    else Result := 'Indefinido';
  end;
end;

function TUBancos.CNABResult_Msg(CNABResult: TCNABResult; CNAB, Banco, Registro:
Integer; Envio: TEnvioCNAB): String;
var
  Msg: String;
  Icone: Word;
begin
  Msg    := 'Mensagem desconhecida em "UBancos.CNABResult_Msg"!' + sLineBreak +
            'CNAB   = ' + IntToStr(CNAB)+sLineBreak+
            'Banco  = ' + FormatFloat('000', Banco)+sLineBreak+
            'A��o   = ' + UBancos.CNABEnvio_ecnabToTxt(Envio);
  Icone  := MB_ICONERROR;
  case CNABResult of
    cresNoCNAB:
    begin
      Msg    := 'Tamanho de registro CNAB n�o habilitado para: ' + sLineBreak +
                UBancos.CNABEnvio_ecnabToTxt(Envio) + '!' +
                sLineBreak + sLineBreak + '"UBancos.CNABResult_Msg()"';
      Icone  := MB_ICONWARNING;
    end;
    cresNoBank:
    begin
      Msg    := 'O banco: ' + FormatFloat('000', Banco) + sLineBreak +
                'N�o est� implementado para: ' + sLineBreak +
                UBancos.CNABEnvio_ecnabToTxt(Envio) + '!' +
                sLineBreak + sLineBreak + '"UBancos.CNABResult_Msg()"';
      Icone  := MB_ICONWARNING;
    end;
    cresNoField:
    begin
      Msg    := 'O banco: ' + FormatFloat('000', Banco) + sLineBreak +
                'N�o possui o tipo de registro: ' + sLineBreak +
                IntToStr(Registro) + sLineBreak + 'Para: ' + sLineBreak +
                UBancos.CNABEnvio_ecnabToTxt(Envio) + '!' +
                sLineBreak + sLineBreak + '"UBancos.CNABResult_Msg()"';
      Icone  := MB_ICONINFORMATION;
    end;
    cresOKField:
    begin
      Msg    := 'O banco: ' + FormatFloat('000', Banco) + sLineBreak +
                'N�o possui o tipo de registro ' + sLineBreak +
                IntToStr(Registro) + sLineBreak + 'Para ' +
                UBancos.CNABEnvio_ecnabToTxt(Envio)  + '!' +
                sLineBreak + sLineBreak + '"UBancos.CNABResult_Msg()"';
      Icone  := MB_ICONINFORMATION;
    end;
    cresNoLayoutName:
    begin
      Msg    := 'Nome do layout n�o definido ou n�o implementado do banco: ' +
                sLineBreak + FormatFloat('000', Banco) + sLineBreak + 'Para: ' + sLineBreak +
                UBancos.CNABEnvio_ecnabToTxt(Envio) + '!' +
                sLineBreak + sLineBreak + '"UBancos.CNABResult_Msg()"';
      Icone  := MB_ICONINFORMATION;
    end;
  end;
  case Icone of
    MB_ICONERROR: Geral.MB_Erro(Msg);
    MB_ICONINFORMATION: Geral.MB_Info(Msg);
    else Geral.MB_Aviso(Msg);
  end;
end;

function TUBancos.TipoArqCNAB(const Arquivo: String; const Lista: TStrings;
var CNAB: Integer): Boolean;
var
  n, i, k: Integer;
begin
  k := 0;
  Result := True;
  CNAB := -1000;
  for i := 0 to Lista.Count -1 do
  begin
    n := Length(Lista[i]);
    if (CNAB = -1000) then
    begin
      if ( (n=240) or (n=400) ) then
        CNAB := n
      else
        inc(k, 1);
    end else
    if (CNAB<>240) and (CNAB<>400) then
      inc(k, 1);
  end;
  if (CNAB<>240) and (CNAB<>400) then
  begin
    Result := False;
    Geral.MB_Aviso('O arquivo "'+Arquivo+'" n�o possui nenhuma '+
    'linha com 240 ou 400 posi��es.');
    Exit;
  end;
  if k > 0 then
  begin
    Result := False;
    Geral.MB_Aviso('O arquivo "'+Arquivo+'" possui '+IntToStr(k)+
    ' linhas que possuem quantidade de caracteres diferente de '+IntToStr(CNAB) +
    '.');
    Exit;
  end;
end;

function TUBancos.CNAB240Envio(Envio: Integer): String;
begin
  case Envio of
    1: Result := 'Remessa';
    2: Result := 'Retorno';
    else Result := 'Indefinido';
  end;
end;

function TUBancos.CNAB240TipoArqRetBco(Tipo: String): String;
var
  Cod: Integer;
begin
  Cod := Geral.IMV(Tipo);
  case Cod of
    0: Result := 'Lote de servi�o';
    else Result := '*** Desconhecido ***';
  end;
end;

function TUBancos.CNABEspecieDeDocumento(Banco: Integer;
  Especie: String): String;
begin
  case Banco of
    748:
    begin
      if Especie = 'A' then Result := 'Duplicata Mercantil ppor Indica��o (DMI)' else
      if Especie = 'B' then Result := 'Duplicata Rural (DR)' else
      if Especie = 'C' then Result := 'Nota Promiss�ria (NP)' else
      if Especie = 'D' then Result := 'Nota Promiss�ria Rural (NR)' else
      if Especie = 'E' then Result := 'Nota de Suguros (NS)' else

      if Especie = 'G' then Result := 'Recibo (RC)' else
      if Especie = 'H' then Result := 'Letra de C�mbio (LC)' else
      if Especie = 'I' then Result := 'Nota de D�bito (ND)' else
      if Especie = 'J' then Result := 'Duplicata de Servi�o por Indica��o (DSI)' else
      if Especie = 'K' then Result := 'Outros (OS)';
    end
    else Result := ' ';
  end;
end;

function TUBancos.CNABMotivosDeTipoDeMovimento(Banco, Movimento, Motivo,
  TamReg: Integer; Envio: TEnvioCNAB; NomeLayout: String): String;
begin
  Result := '';
  //
  if TamReg = 240 then //CNAB 240
  begin
    ;
  end else
  begin //CNAB 400
    case Banco of
      341:
      begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
          end;
          ecnabRetorno: // Retorno
          begin
            case Movimento of
              03:
              begin
                case Motivo of
                  03: Result := 'AG. COBRADORA	N�O FOI POSS�VEL ATRIBUIR A AG�NCIA  PELO CEP OU CEP INV�LIDO';
                  04: Result := 'ESTADO	SIGLA DO ESTADO INV�LIDA';
                  05: Result := 'DATA VENCIMENTO	PRAZO DA OPERA��O MENOR QUE PRAZO M�NIMO OU MAIOR QUE O M�XIMO';
                  07: Result := 'VALOR DO T�TULO	VALOR DO T�TULO MAIOR QUE 10.000.000,00';
                  08: Result := 'NOME DO SACADO 	N�O INFORMADO OU DESLOCADO';
                  09: Result := 'AGENCIA/CONTA 	AG�NCIA ENCERRADA';
                  10: Result := 'LOGRADOURO 	N�O INFORMADO OU DESLOCADO';
                  11: Result := 'CEP	CEP N�O NUM�RICO';
                  12: Result := 'SACADOR / AVALISTA 	NOME N�O INFORMADO OU DESLOCADO (BANCOS CORRESPONDENTES)';
                  13: Result := 'ESTADO/CEP 	CEP INCOMPAT�VEL COM A SIGLA DO ESTADO';
                  14: Result := 'NOSSO N�MERO	NOSSO N�MERO J� REGISTRADO NO CADASTRO DO BANCO OU FORA DA FAIXA';
                  15: Result := 'NOSSO N�MERO	NOSSO N�MERO EM DUPLICIDADE NO MESMO MOVIMENTO';
                  18: Result := 'DATA DE ENTRADA	DATA DE ENTRADA INV�LIDA PARA OPERAR COM ESTA CARTEIRA';
                  19: Result := 'OCORR�NCIA	OCORR�NCIA INV�LIDA';
                  21: Result := 'AG. COBRADORA 	�	CARTEIRA N�O ACEITA DEPOSIT�RIA CORRESPONDENTE';
                  22: Result := 'CARTEIRA 	CARTEIRA N�O PERMITIDA (NECESS�RIO CADASTRAR FAIXA LIVRE)';
                  27: Result := 'CNPJ INAPTO	CNPJ DO CEDENTE INAPTO';
                  29: Result := 'C�DIGO EMPRESA	CATEGORIA DA CONTA INV�LIDA';
                  35: Result := 'VALOR DO IOF	IOF MAIOR QUE 5%';
                  36: Result := 'QTDADE DE MOEDA	QUANTIDADE DE MOEDA INCOMPAT�VEL COM VALOR DO T�TULO';
                  37: Result := 'CNPJ/CPF DO SACADO	N�O NUM�RICO OU IGUAL A ZEROS';
                  42: Result := 'NOSSO N�MERO	NOSSO N�MERO FORA DE FAIXA';
                  52: Result := 'AG. COBRADORA	EMPRESA N�O ACEITA BANCO CORRESPONDENTE';
                  53: Result := 'AG. COBRADORA	EMPRESA N�O ACEITA BANCO CORRESPONDENTE  - COBRAN�A MENSAGEM';
                  54: Result := 'DATA DE VENCTO	BANCO CORRESPONDENTE - T�TULO COM VENCIMENTO INFERIOR A 15 DIAS';
                  55: Result := 'DEP/BCO CORRESP	CEP N�O PERTENCE � DEPOSIT�RIA INFORMADA';
                  56: Result := 'DT VENCTO/BCO CORRESP	VENCTO SUPERIOR A 180 DIAS DA DATA DE ENTRADA';
                  57: Result := 'DATA DE VENCTO 	CEP S� DEPOSIT�RIA BCO DO BRASIL COM VENCTO INFERIOR A 8 DIAS';
                  60: Result := 'ABATIMENTO	VALOR DO ABATIMENTO INV�LIDO';
                  61: Result := 'JUROS DE MORA	JUROS DE MORA MAIOR QUE O PERMITIDO';
                  65: Result := 'TAXA FINANCTO	TAXA INV�LIDA (VENDOR)';
                  66: Result := 'DATA DE VENCTO	INVALIDA/FORA DE PRAZO DE OPERA��O (M�NIMO OU M�XIMO)';
                  67: Result := 'VALOR/QTIDADE	VALOR DO T�TULO/QUANTIDADE DE MOEDA INV�LIDO';
                  68: Result := 'CARTEIRA 	CARTEIRA INV�LIDA';
                  98: Result := 'FLASH INV�LIDO	REGISTRO MENSAGEM SEM FLASH CADASTRADO OU FLASH INFORMADO DIFERENTE DO CADASTRADO';
                  99: Result := 'FLASH INV�LIDO	CONTA DE COBRAN�A COM FLASH CADASTRADO E SEM REGISTRO DE MENSAGEM CORRESPONDENTE';
                end;
              end;
              15:
              begin
                case Motivo of
                  01: Result := 'CARTEIRA/N� N�MERO N�O NUM�RICO';
                  04: Result := 'NOSSO N�MERO EM DUPLICIDADE NUM MESMO MOVIMENTO';
                  05: Result := 'SOLICITA��O DE BAIXA PARA T�TULO J� BAIXADO OU LIQUIDADO';
                  06: Result := 'SOLICITA��O DE BAIXA PARA T�TULO N�O REGISTRADO NO SISTEMA';
                  07: Result := 'COBRAN�A PRAZO CURTO - SOLICITA��O DE BAIXA P/ T�TULO N�O REGISTRADO NO SISTEMA';
                  08: Result := 'SOLICITA��O DE BAIXA PARA T�TULO EM FLOATING';
                end;
              end;
              16:
              begin
                case Motivo of
                  01: Result :=	'INSTRU��O/OCORR�NCIA N�O EXISTENTE';
                  06: Result :=	'NOSSO N�MERO IGUAL A ZEROS';
                  09: Result :=	'CNPJ/CPF  DO SACADOR/AVALISTA INV�LIDO';
                  10: Result :=	'VALOR DO ABATIMENTO IGUAL OU MAIOR QUE O VALOR DO T�TULO';
                  14: Result :=	'REGISTRO EM DUPLICIDADE';
                  15: Result :=	'CNPJ/CPF INFORMADO SEM NOME DO SACADOR/AVALISTA';
                  21: Result :=	'T�TULO N�O REGISTRADO NO SISTEMA';
                  22: Result :=	'T�TULO BAIXADO OU LIQUIDADO';
                  23: Result :=	'INSTRU��O N�O ACEITA POR TER SIDO EMITIDO �LTIMO AVISO AO SACADO';
                  24: Result :=	'INSTRU��O INCOMPAT�VEL - EXISTE INSTRU��O DE PROTESTO PARA O T�TULO';
                  25: Result :=	'INSTRU��O INCOMPAT�VEL � N�O EXISTE INSTRU��O DE PROTESTO PARA O T�TULO';
                  26: Result :=	'INSTRU��O N�O ACEITA POR J� TER SIDO EMITIDA A ORDEM DE PROTESTO AO CART�RIO';
                  27: Result :=	'INSTRU��O N�O ACEITA POR N�O TER SIDO EMITIDA A ORDEM DE PROTESTO AO CART�RIO';
                  28: Result :=	'J� EXISTE UMA MESMA INSTRU��O CADASTRADA ANTERIORMENTE PARA O T�TULO';
                  29: Result :=	'VALOR L�QUIDO + VALOR DO ABATIMENTO DIFERENTE DO VALOR DO T�TULO REGISTRADO, OU VALOR DO ABATIMENTO MAIOR QUE 90% DO VALOR DO T�TULO';
                  30: Result :=	'EXISTE UMA INSTRU��O DE N�O PROTESTAR ATIVA PARA O T�TULO';
                  31: Result :=	'EXISTE UMA OCORR�NCIA DO SACADO QUE BLOQUEIA A INSTRU��O';
                  32: Result :=	'DEPOSIT�RIA DO T�TULO = 9999 OU CARTEIRA N�O ACEITA PROTESTO';
                  33: Result :=	'ALTERA��O DE VENCIMENTO IGUAL � REGISTRADA NO SISTEMA OU QUE TORNA O T�TULO VENCIDO';
                  34: Result :=	'INSTRU��O DE EMISS�O DE AVISO DE COBRAN�A PARA T�TULO VENCIDO ANTES DO VENCIMENTO';
                  35: Result :=	'SOLICITA��O DE CANCELAMENTO DE INSTRU��O INEXISTENTE';
                  36: Result :=	'T�TULO SOFRENDO ALTERA��O DE CONTROLE (AG�NCIA/CONTA/CARTEIRA/NOSSO N�MERO)';
                  37: Result :=	'INSTRU��O N�O PERMITIDA PARA A CARTEIRA';
                end;
              end;
              17:
              begin
                case Motivo of
                  02: Result := 'AG�NCIA COBRADORA INV�LIDA OU COM O MESMO CONTE�DO';
                  04: Result := 'SIGLA DO ESTADO INV�LIDA';
                  05: Result := 'DATA DE VENCIMENTO INV�LIDA OU COM O MESMO CONTE�DO';
                  06: Result := 'VALOR DO T�TULO COM OUTRA ALTERA��O SIMULT�NEA';
                  08: Result := 'NOME DO SACADO COM O MESMO CONTE�DO';
                  09: Result := 'AG�NCIA/CONTA INCORRETA';
                  11: Result := 'CEP INV�LIDO';
                  13: Result := 'SEU N�MERO COM O MESMO CONTE�DO';
                  16: Result := 'ABATIMENTO/ALTERA��O DO VALOR DO T�TULO OU SOLICITA��O DE BAIXA BLOQUEADA';
                  20: Result := 'ESP�CIE INV�LIDA';
                  21: Result := 'AG�NCIA COBRADORA N�O CONSTA NO CADASTRO DE DEPOSIT�RIA OU EM ENCERRAMENTO';
                  23: Result := 'DATA DE EMISS�O DO T�TULO INV�LIDA OU COM MESMO CONTE�DO';
                  41: Result := 'CAMPO ACEITE INV�LIDO OU COM MESMO CONTE�DO';
                  53: Result := 'INSTRU��O COM O MESMO CONTE�DO';
                  54: Result := 'DATA VENCIMENTO PARA BANCOS CORRESPONDENTES INFERIOR AO ACEITO PELO BANCO';
                  55: Result := 'ALTERA��ES IGUAIS PARA O MESMO CONTROLE (AG�NCIA/CONTA/CARTEIRA/NOSSO N�MERO)';
                  56: Result := 'CNPJ/CPF INV�LIDO N�O NUM�RICO OU ZERADO';
                  57: Result := 'PRAZO DE VENCIMENTO INFERIOR A 15 DIAS';
                  60: Result := 'VALOR DE IOF - ALTERA��O N�O PERMITIDA PARA CARTEIRAS DE N.S. - MOEDA VARI�VEL';
                  61: Result := 'T�TULO J� BAIXADO OU LIQUIDADO OU N�O EXISTE T�TULO CORRESPONDENTE NO SISTEMA';
                  66: Result := 'ALTERA��O N�O PERMITIDA PARA CARTEIRAS DE NOTAS DE SEGUROS - MOEDA VARI�VEL';
                  81: Result := 'ALTERA��O BLOQUEADA - T�TULO COM PROTESTO';
                end;
              end;
              18:
              begin
                case Motivo of
                  16: Result := 'ABATIMENTO/ALTERA��O DO VALOR DO T�TULO OU SOLICITA��O DE BAIXA BLOQUEADOS';
                end;
              end;
              24:
              begin
                case Motivo of
                  1610: Result := 'DOCUMENTA��O SOLICITADA AO CEDENTE';
                  3111: Result := 'SUSTA��O SOLICITADA AG. CEDENTE';
                  3160: Result := 'NOME DO SACADO INCOMPLETO/INCORRETO';
                  3186: Result := 'NOME DO SACADOR INCOMPLETO/INCORRETO';
                  3228: Result := 'ATOS DA CORREGEDORIA ESTADUAL';
                  3244: Result := 'PROTESTO SUSTADO / CEDENTE N�O ENTREGOU A DOCUMENTA��O';
                  3269: Result := 'DATA DE EMISS�O DO T�TULO INV�LIDA/IRREGULAR';
                  3285: Result := 'PRA�A N�O ATENDIDA PELA REDE BANC�RIA';
                  3301: Result := 'CNPJ/CPF DO SACADO INV�LIDO/INCORRETO';
                  3319: Result := 'SACADOR/AVALISTA E PESSOA F�SICA';
                  3327: Result := 'CEP DO SACADO INCORRETO';
                  3335: Result := 'DEPOSIT�RIA INCOMPAT�VEL COM CEP DO SACADO';
                  3343: Result := 'CNPJ/CPF SACADOR INVALIDO/INCORRETO';
                  3350: Result := 'ENDERE�O DO SACADO INSUFICIENTE';
                  3368: Result := 'PRA�A PAGTO INCOMPAT�VEL COM ENDERE�O';
                  3376: Result := 'FALTA N�MERO/ESP�CIE DO T�TULO';
                  3384: Result := 'T�TULO ACEITO S/ ASSINATURA DO SACADOR';
                  3392: Result := 'T�TULO ACEITO S/ ENDOSSO CEDENTE OU IRREGULAR';
                  3400: Result := 'T�TULO SEM LOCAL OU DATA DE EMISS�O';
                  3418: Result := 'T�TULO ACEITO COM VALOR EXTENSO DIFERENTE DO NUM�RICO';
                  3822: Result := 'SACADO IR� NEGOCIAR DIRETAMENTE COM O CEDENTE';
                  3426: Result := 'T�TULO ACEITO DEFINIR ESP�CIE DA DUPLICATA';
                  3434: Result := 'DATA EMISS�O POSTERIOR AO VENCIMENTO';
                  3442: Result := 'T�TULO ACEITO DOCUMENTO N�O PROSTEST�VEL';
                  3459: Result := 'T�TULO ACEITO EXTENSO VENCIMENTO IRREGULAR';
                  3467: Result := 'T�TULO ACEITO FALTA NOME FAVORECIDO';
                  3475: Result := 'T�TULO ACEITO FALTA PRA�A DE PAGAMENTO';
                  3483: Result := 'T�TULO ACEITO FALTA CPF ASSINANTE CHEQUE';
                  3491: Result := 'FALTA N�MERO DO T�TULO (SEU N�MERO)';
                  3509: Result := 'CART�RIO DA PRA�A COM ATIVIDADE SUSPENSA';
                end;
              end;
              25:
              begin
                case Motivo of
                  1313: Result := 'SOLICITA A PRORROGA��O DO VENCIMENTO PARA:';
                  1321: Result := 'SOLICITA A DISPENSA DOS JUROS DE MORA';
                  1339: Result := 'N�O RECEBEU A MERCADORIA';
                  1347: Result := 'A MERCADORIA CHEGOU ATRASADA';
                  1354: Result := 'A MERCADORIA CHEGOU AVARIADA';
                  1362: Result := 'A MERCADORIA CHEGOU INCOMPLETA';
                  1370: Result := 'A MERCADORIA N�O CONFERE COM O PEDIDO';
                  1388: Result := 'A MERCADORIA EST� � DISPOSI��O';
                  1396: Result := 'DEVOLVEU A MERCADORIA';
                  1404: Result := 'N�O RECEBEU A FATURA';
                  1412: Result := 'A FATURA EST� EM DESACORDO COM A NOTA FISCAL';
                  1420: Result := 'O PEDIDO DE COMPRA FOI CANCELADO';
                  1438: Result := 'A DUPLICATA FOI CANCELADA';
                  1446: Result := 'QUE NADA DEVE OU COMPROU';
                  1453: Result := 'QUE MANT�M ENTENDIMENTOS COM O SACADOR';
                  1461: Result := 'QUE PAGAR� O T�TULO EM';
                  1479: Result := 'QUE PAGOU O T�TULO DIRETAMENTE AO CEDENTE EM';
                  1487: Result := 'QUE PAGAR� O T�TULO DIRETAMENTE AO CEDENTE EM';
                  1495: Result := 'QUE O VENCIMENTO CORRETO �';
                  1503: Result := 'VALOR	QUE TEM DESCONTO OU ABATIMENTO DE';
                  1719: Result := 'SACADO N�O FOI LOCALIZADO; CONFIRMAR ENDERE�O';
                  1727: Result := 'SACADO EST� EM REGIME DE CONCORDATA';
                  1735: Result := 'SACADO EST� EM REGIME DE FAL�NCIA';
                  1750: Result := 'SACADO SE RECUSA A PAGAR JUROS BANC�RIOS';
                  1768: Result := 'SACADO SE RECUSA A PAGAR COMISS�O DE PERMAN�NCIA';
                  1776: Result := 'N�O FOI POSS�VEL A ENTREGA DO BLOQUETO AO SACADO';
                  1784: Result := 'BLOQUETO N�O ENTREGUE, MUDOU-SE/DESCONHECIDO';
                  1792: Result := 'BLOQUETO N�O ENTREGUE, CEP ERRADO/INCOMPLETO';
                  1800: Result := 'BLOQUETO N�O ENTREGUE, N�MERO N�O EXISTE/ENDERE�O INCOMPLETO';
                  1818: Result := 'BLOQUETO N�O RETIRADO PELO SACADO. REENVIADO PELO CORREIO';
                  1826: Result := 'ENDERE�O DE E-MAIL INV�LIDO. BLOQUETO ENVIADO PELO CORREIO';
                end;
              end;
              57:
              begin
                case Motivo of
                  1156: Result := 'N�O PROTESTAR';
                  2261: Result := 'DISPENSAR JUROS/COMISS�O DE  PERMAN�NCIA';
                end;
              end;
            end;
          end;
        end;
      end;
      756:
      begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
          end;
          ecnabRetorno: // Retorno
          begin
            if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
            begin
              case Movimento of
                02:
                begin
                  case Motivo of
                    00: Result := 'Ocorr�ncia aceita';
                    17: Result := 'Data de vencimento anterior a data de emiss�o';
                    21: Result := 'Esp�cie do T�tulo inv�lido';
                    24: Result := 'Data da emiss�o inv�lida';
                    38: Result := 'Prazo para protesto inv�lido';
                    39: Result := 'Pedido para protesto n�o permitido para t�tulo';
                    43: Result := 'Prazo para baixa e devolu��o inv�lido';
                    45: Result := 'Nome do Pagador inv�lido';
                    46: Result := 'Tipo/num. de inscri��o do Pagador inv�lidos';
                    47: Result := 'Endere�o do Pagador n�o informado';
                    48: Result := 'CEP irregular';
                    50: Result := 'CEP referente a Banco correspondente';
                    53: Result := 'N� de inscri��o do Sacador/avalista inv�lidos (CPF/CGC)';
                    54: Result := 'Sacador/avalista n�o informado';
                    67: Result := 'D�bito autom�tico agendado';
                    68: Result := 'D�bito n�o agendado � erro nos dados de remessa';
                    69: Result := 'D�bito n�o agendado � Pagador n�o consta no cadastro de autorizante';
                    70: Result := 'D�bito n�o agendado � Cedente n�o autorizado pelo Pagador';
                    71: Result := 'D�bito n�o agendado � Cedente n�o participa da modalidade de d�bito autom�tico';
                    72: Result := 'D�bito n�o agendado � C�digo de moeda diferente de R$';
                    73: Result := 'D�bito n�o agendado � Data de vencimento inv�lida';
                    75: Result := 'D�bito n�o agendado � Tipo do n�mero de inscri��o do pagador debitado inv�lido';
                    86: Result := 'Seu n�mero do documento inv�lido';
                  end;
                end;
                03:
                begin
                  case Motivo of
                    02: Result := 'C�digo do registro detalhe inv�lido';
                    03: Result := 'C�digo da ocorr�ncia inv�lida';
                    04: Result := 'C�digo de ocorr�ncia n�o permitida para a carteira';
                    05: Result := 'C�digo de ocorr�ncia n�o num�rico';
                    07: Result := 'Ag�ncia/conta/Digito � |Inv�lido';
                    08: Result := 'Nosso n�mero inv�lido';
                    09: Result := 'Nosso n�mero duplicado';
                    10: Result := 'Carteira inv�lida';
                    16: Result := 'Data de vencimento inv�lida';
                    18: Result := 'Vencimento fora do prazo de opera��o';
                    20: Result := 'Valor do T�tulo inv�lido';
                    21: Result := 'Esp�cie do T�tulo inv�lida';
                    22: Result := 'Esp�cie n�o permitida para a carteira';
                    24: Result := 'Data de emiss�o inv�lida';
                    38: Result := 'Prazo para protesto inv�lido';
                    44: Result := 'Ag�ncia Cedente n�o prevista';
                    50: Result := 'CEP irregular � Banco Correspondente';
                    63: Result := 'Entrada para T�tulo j� cadastrado';
                    68: Result := 'D�bito n�o agendado � erro nos dados de remessa';
                    69: Result := 'D�bito n�o agendado � Pagador n�o consta no cadastro de autorizante';
                    70: Result := 'D�bito n�o agendado � Cedente n�o autorizado pelo Pagador';
                    71: Result := 'D�bito n�o agendado � Cedente n�o participa da modalidade de d�bito autom�tico';
                    72: Result := 'D�bito n�o agendado � C�digo de moeda diferente de R$';
                    73: Result := 'D�bito n�o agendado � Data de vencimento inv�lida';
                    74: Result := 'D�bito n�o agendado � Conforme seu pedido, T�tulo n�o registrado';
                    75: Result := 'D�bito n�o agendado � Tipo de n�mero de inscri��o do debitado inv�lido';
                  end;
                end;
                09:
                begin
                  case Motivo of
                    10: Result := 'Baixa comandada pelo cliente';
                  end;
                end;
                10:
                begin
                  case Motivo of
                    00: Result := 'Baixa Comandada';
                    14: Result := 'T�tulo Protestado';
                    15: Result := 'T�tulo exclu�do';
                  end;
                end;
                24:
                begin
                  case Motivo of
                    48: Result := 'CEP inv�lido';
                  end;
                end;
                27:
                begin
                  case Motivo of
                    04: Result := 'C�digo de ocorr�ncia n�o permitido para a carteira';
                    07: Result := 'Ag�ncia/Conta/d�gito inv�lidos';
                    08: Result := 'Nosso n�mero inv�lido';
                    10: Result := 'Carteira inv�lida';
                    15: Result := 'Carteira/Ag�ncia/Conta/nosso n�mero inv�lidos';
                    40: Result := 'T�tulo com ordem de protesto emitido';
                    42: Result := 'C�digo para baixa/devolu��o via Telebradesco inv�lido';
                    60: Result := 'Movimento para T�tulo n�o cadastrado';
                    77: Result := 'Transfer�ncia para desconto n�o permitido para a carteira';
                    85: Result := 'T�tulo com pagamento vinculado';
                  end;
                end;
                28:
                begin
                  case Motivo of
                    03: Result := 'Tarifa de susta��o';
                    04: Result := 'Tarifa de protesto';
                    08: Result := 'Custas de protesto';
                  end;
                end;
                30:
                begin
                  case Motivo of
                    01: Result := 'C�digo do Banco inv�lido';
                    04: Result := 'C�digo de ocorr�ncia n�o permitido para a carteira';
                    05: Result := 'C�digo da ocorr�ncia n�o num�rico';
                    08: Result := 'Nosso n�mero inv�lido';
                    15: Result := 'Caracter�stica da cobran�a imcop�tivel';
                    16: Result := 'Data de vencimento inv�lido';
                    17: Result := 'Data de vencimento anterior a data de emiss�o';
                    18: Result := 'Vencimento fora do prazo de opera��o';
                    24: Result := 'Data de emiss�o Inv�lida';
                    29: Result := 'Valor do desconto maior/igual ao valor do T�tulo';
                    30: Result := 'Desconto a conceder n�o confere';
                    31: Result := 'Concess�o de desconto j� existente ( Desconto anterior )';
                    33: Result := 'Valor do abatimento inv�lido';
                    34: Result := 'Valor do abatimento maior/igual ao valor do T�tulo';
                    38: Result := 'Prazo para protesto inv�lido';
                    39: Result := 'Pedido de protesto n�o permitido para o T�tulo';
                    40: Result := 'T�tulo com ordem de protesto emitido';
                    42: Result := 'C�digo para baixa/devolu��o inv�lido';
                    60: Result := 'Movimento para T�tulo n�o cadastrado';
                    85: Result := 'T�tulo com Pagamento Vinculado';
                  end;
                end;
                32:
                begin
                  case Motivo of
                    01: Result := 'C�digo do Banco inv�lido';
                    02: Result := 'C�digo do registro detalhe inv�lido';
                    04: Result := 'C�digo de ocorr�ncia n�o permitido para a carteira';
                    05: Result := 'C�digo de ocorr�ncia n�o num�rico';
                    07: Result := 'Ag�ncia/Conta/d�gito inv�lidos';
                    08: Result := 'Nosso n�mero inv�lido';
                    10: Result := 'Carteira inv�lida';
                    15: Result := 'Caracter�sticas da cobran�a incompat�veis';
                    16: Result := 'Data de vencimento inv�lida';
                    17: Result := 'Data de vencimento anterior a data de emiss�o';
                    18: Result := 'Vencimento fora do prazo de opera��o';
                    20: Result := 'Valor do t�tulo inv�lido';
                    21: Result := 'Esp�cie do T�tulo inv�lida';
                    22: Result := 'Esp�cie n�o permitida para a carteira';
                    24: Result := 'Data de emiss�o inv�lida';
                    28: Result := 'C�digo de desconto via Telebradesco inv�lido';
                    29: Result := 'Valor do desconto maior/igual ao valor do T�tulo';
                    30: Result := 'Desconto a conceder n�o confere';
                    31: Result := 'Concess�o de desconto � J� existe desconto anterior';
                    33: Result := 'Valor do abatimento inv�lido';
                    34: Result := 'Valor do abatimento maior/igual ao valor do T�tulo';
                    36: Result := 'Concess�o abatimento � J� existe abatimento anterior';
                    38: Result := 'Prazo para protesto inv�lido';
                    39: Result := 'Pedido de protesto n�o permitido para o T�tulo';
                    40: Result := 'T�tulo com ordem de protesto emitido';
                    41: Result := 'Pedido cancelamento/susta��o para T�tulo sem instru��o de protesto';
                    42: Result := 'C�digo para baixa/devolu��o inv�lido';
                    45: Result := 'Nome do Pagador n�o informado';
                    46: Result := 'Tipo/n�mero de inscri��o do Pagador inv�lidos';
                    47: Result := 'Endere�o do Pagador n�o informado';
                    48: Result := 'CEP Inv�lido';
                    50: Result := 'CEP referente a um Banco correspondente';
                    53: Result := 'Tipo de inscri��o do sacador avalista inv�lidos';
                    60: Result := 'Movimento para T�tulo n�o cadastrado';
                    85: Result := 'T�tulo com pagamento vinculado';
                    86: Result := 'Seu n�mero inv�lido';
                  end;
                end;
                35:
                begin
                  case Motivo of
                    81: Result := 'Tentativas esgotadas, baixado';
                    82: Result := 'Tentativas esgotadas, pendente';
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

function TUBancos.CNABMotivosDeTipoDeMovimento28(Banco: Integer;
  Motivos: String): String;
begin
  Result := ' ';
end;

function TUBancos.CNABRetCNPJ(Banco, QtdPos: Integer; FLista: TStrings;
  Arquivo: String): String;
begin
  Result := '';
  case QtdPos of
    240:
    begin
      (*case Banco of
        001: Result := Copy(FLista[FLista.Count - 1], 018, 006);
      end;*)
    end;
    400:
    begin
      case Banco of
        748: Result := Copy(FLista[0], 032, 014);
      end;
    end;
  end;
  if Result = '' then
  begin
    Geral.MB_Aviso(
    'N�o foi poss�vel verificar o CNPJ / CPF do cabe�alho do arquivo "' +
    Arquivo + '"!');
    Screen.Cursor := crDefault;
  end;
end;

function TUBancos.CNABRetCodCedente(Banco, QtdPos: Integer; FLista: TStrings;
  Arquivo: String): String;
begin
  Result := '??????';
  case QtdPos of
    240:
    begin
      (*case Banco of
        001: Result := Copy(FLista[FLista.Count - 1], 018, 006);
      end;*)
    end;
    400:
    begin
      case Banco of
        748: Result := Copy(FLista[0], 027, 005);
      end;
    end;
  end;
  if Result = '??????' then
  begin
    Geral.MB_Aviso(
    'N�o foi poss�vel verificar o c�digo do cedente do cabe�alho do arquivo "' +
    Arquivo + '"!');
    Screen.Cursor := crDefault;
  end;
end;

function TUBancos.CNABRetEhHeaderArq(Banco, QtdPos: Integer; FLista: TStrings;
  Arquivo: String): Boolean;
begin
  Result := False;
  case QtdPos of
    240:
    begin
      case Banco of
        001: Result := FLista[0][8] = '0';
      end;
    end;
    400:
    begin
      case Banco of
        748: Result := Copy(FLista[0], 001, 001) = '0';
      end;
    end;
  end;
  if Result = False then
  begin
    Geral.MB_Aviso('A linha 1 do arquivo "' + Arquivo +
    '" n�o � cabe�alho de arquivo!');
    Screen.Cursor := crDefault;
  end;
end;

function TUBancos.CNABRetEhLoteServico(Banco, QtdPos: Integer; FLista: TStrings;
  Arquivo: String): Boolean;
begin
  Result := False;
  case QtdPos of
    240:
    begin
      case Banco of
        001: Result := Copy(FLista[0], 4, 4) = '0000';
      end;
    end;
    400:
    begin
      case Banco of
        748: Result := Copy(FLista[0], 010, 002) = '01';
      end;
    end;
  end;
  if Result = False then
  begin
    Geral.MB_Aviso('O arquivo "' + Arquivo + '" n�o � lote de servi�o!');
    Screen.Cursor := crDefault;
  end;
end;

function TUBancos.CNABRetEhRetorno(Banco, QtdPos: Integer; FLista: TStrings;
  Arquivo: String): Boolean;
begin
  Result := False;
  case QtdPos of
    240:
    begin
      case Banco of
        001: Result := FLista[0][143] = '2';
      end;
    end;
    400:
    begin
      case Banco of
        748: Result := Copy(FLista[0], 002, 001) = '2';
      end;
    end;
  end;
  if Result = False then
  begin
    Geral.MB_Aviso('O arquivo "' + Arquivo + '" n�o � lote de retorno');
    Screen.Cursor := crDefault;
  end;
end;

function TUBancos.CNABRetQtdeLots(Banco, QtdPos: Integer; FLista: TStrings;
  Arquivo: String): String;
begin
  Result := '??????';
  case QtdPos of
    240:
    begin
      case Banco of
        001: Result := Copy(FLista[FLista.Count - 1], 018, 006);
      end;
    end;
    400:
    begin
      case Banco of
        748: Result := 'n�o info';
      end;
    end;
  end;
  if Result = '??????' then
  begin
    Geral.MB_Aviso(
    'N�o foi poss�vel verificar a quantidade de registros no arquivo "' +
    Arquivo + '"!');
    Screen.Cursor := crDefault;
  end;
end;

function TUBancos.CNABRetRegistrosOK(Banco, QtdPos: Integer; FLista: TStrings;
  Arquivo: String): Boolean;
var
  k: Integer;
begin
  k := 0;
  Result := False;
  case QtdPos of
    240:
    begin
      case Banco of
        001:
        begin
          k := Geral.IMV(Copy(FLista[FLista.Count -1], 24, 6));
          Result := FLista.Count = k;
        end;
      end;
    end;
    400:
    begin
      case Banco of
        748: Result := True; // N�o tem info
      end;
    end;
  end;
  if Result = False then
  begin
    Geral.MB_Aviso('O arquivo "' + Arquivo + '" possui ' + IntToStr(
    FLista.Count) + ' registros mas seu "trailer" informa ' + IntToStr(k)+'!');
    Screen.Cursor := crDefault;
  end;
end;

function TUBancos.CNABTipoDeMovimento(Banco: Integer; Envio: TEnvioCNAB;
  Movimento, TamReg: Integer; Registrado: Boolean; NomeLayout: String): String;
begin
  if TamReg = 240 then
  begin
    //case Banco of
      //-1:
      //begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
          end;
          ecnabRetorno: // Retorno
          begin
            if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
            begin
              case Movimento of
                02: Result := 'Entrada confirmada.';
                03: Result := 'Entrada rejeitada.';
                04: Result := 'Transferencia de carteira / entrada.';
                05: Result := 'Transferencia de carteira / baixa.';
                06: Result := 'Liquida��o.';
                09: Result := 'Baixa.';
                10: Result := 'Baixa conforme instru��o da ag�ncia.';
                11: Result := 'T�tulos em carteira / em ser.';
                12: Result := 'Confirma��o recebimento instru��o de abatimento.';
                13: Result := 'Confirma��o recebimento instru��o de cancelamento de abatimento.';
                14: Result := 'Confirma��o recebimento instru��o altera��o de vencimento.';
                17: Result := 'Liquida��o ap�s baixa.';
                19: Result := 'Confirma��o recebimento instru��o de protesto.';
                20: Result := 'Confirma��o recebimento instru��o de susta��o / cancelamento de protesto.';
                23: Result := 'Remessa a cart�rio / aponte em cart�rio.';
                24: Result := 'Retirada de cart�rio e manuten��o em carteira.';
                25: Result := 'Protestado e baixado (baixa por ter sido protestado).';
                26: Result := 'Instru��o rejeitada.';
                27: Result := 'Confirma��o do pedido de altera��o de outros dados.';
                28: Result := 'D�bito de tarifas / custas.';
                29: Result := 'Ocorr�ncias do Pagador.';
                30: Result := 'Altera��o de dados rejeitada.';
                (*
                Obs.: Exceto para a corr�ncia                     !
                28 - D�bitos de tarifas e custas, ser�o retornadas!
                somente as ocorr�ncias correspondentes as         !
                instru��es comandas.                              !
                *)
              end;
            end else
            begin
              case Movimento of
                02: Result := 'Entrada confirmada.';
                03: Result := 'Entrada rejeitada.';
                04: Result := 'Transfer�ncia de carteira/entrada.';
                05: Result := 'Transfer�ncia de carteira/baixa.';
                06: Result := 'Liquida��o.';
                09: Result := 'Baixa.';
                11: Result := 'Titulos em carteira (em ser).';
                12: Result := 'Confirma��o recebimento instru��o de abatimento.';
                13: Result := 'Confirma��o recebimento instru��o de cancelamento abatimento.';
                14: Result := 'Confirma��o recebimento instru��o altera��o de vencimento.';
                15: Result := 'Franco de pagamento.';
                17: Result := 'Liquida��o ap�s baixa ou liquida��o de t�tulo n�o registrado.';
                19: Result := 'Confirma��o de recebimento instru��o de protesto.';
                20: Result := 'Confirma��o de recebimento instru��o de susta��o/cancelamento de protesto.';
                23: Result := 'Remessa a cart�rio (aponte em cart�rio).';
                24: Result := 'Retirada de cart�rio e manuten��o em carteira.';
                25: Result := 'Protestado e baixado (baixa por ter sido protestado).';
                26: Result := 'Instru��o rejeitada.';
                27: Result := 'Confirma��o do pedido de altera��o de outros dados.';
                28: Result := 'D�bito de tarifas/custas.';
                29: Result := 'Ocorr�ncias do sacado.';
                30: Result := 'Altera��o de dados rejeitada.';
                36: Result := 'Confirma��o de recebimento de instru��o de desconto';
                37: Result := 'Confirma��o de recebimento de instru��o de cancelamento de desconto';
                43: Result := 'Estorno de protesto/susta��o.';
                44: Result := 'Estorno de baixa / liquida��o';
                45: Result := 'Altera��o de dados';
                (*
                Obs.: Os codigos 03, 26 e 30 estao relacionados!
                com a nota 42-a.                               !
                O codigo 28 esta relacionado com a nota 42-b.  !
                Os codigos 06, 09 e 17 estao relacionados com a!
                nota 42-c.                                     !
                *)
                else Result := '';
              end;
            end;
          end;
        end;
      //end;
    //end;
  end else begin
    case Banco of
      001:
      begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
      (*
      !/20/             !01 - Entrada de Titulos.                       !
      !CODIGOS DE MOVI- !02 - Pedido de Baixa.                          !
      !MENTO PARA       !04 - Concessao de Abatimento.                  !
      !REMESSA          !05 - Cancelamento de Abatimento.               !
      !                 !06 - Alteracao de Vencimento.                  !
      !                 !07 - Concessao de Desconto.                    !
      !                 !08 - Cancelamento de Desconto.                 !
      !                 !09 - Protestar.                                !
      !                 !10 - Cancela/Sustacao da Instrucao de protesto !
      !                 !30 - Recusa da Alegacao do Sacado.             !
      !                 !31 - Alteracao de Outros Dados.                !
      !                 !40 - Alteracao de modalidade.                  !
      !                 !Obs.: Cada banco definira os  campos a serem   !
      !                 !alterados para o codigo de movimento 31        !
      *)
            case Movimento of
              01: Result := 'Entrada de T�tulos.';
              02: Result := 'Pedido de Baixa.';
              04: Result := 'Concess�o de Abatimento.';
              05: Result := 'Cancelamento de Abatimento.';
              06: Result := 'Altera��o de Vencimento.';
              07: Result := 'Concess�o de Desconto.';
              08: Result := 'Cancelamento de Desconto.';
              09: Result := 'Protestar.';
              10: Result := 'Cancela/Susta��o da Instru��o de protesto.';
              30: Result := 'Recusa da Alega��o do Sacado.';
              31: Result := 'Altera��o de Outros Dados.';
              40: Result := 'Altera��o de modalidade.';
              else Result := '';
            end;
          end;
          ecnabRetorno:  // Retorno
          begin
      (*
      !/40/             !02 - Entrada confirmada.                       !
      !CODIGOS DE MOVI- !03 - Entrada rejeitada.                        !
      !MENTO PARA RETOR-!04 - Transferencia de carteira/entrada.        !
      !NO               !05 - Transferencia de carteira/baixa.          !
      !                 !06 - Liquidacao.                               !
      !                 !09 - Baixa.                                    !
      !                 !11 - Titulos em carteira /em ser/.             !
      !                 !12 - Confirmacao recebimento instrucao de      !
      !                 !     abatimento.                               !
      !                 !13 - Confirmacao recebimento instrucao de      !
      !                 !     cancelamento abatimento.                  !
      !                 !14 - Confirmacao recebimento instrucao altera- !
      !                 !     cao de vencimento.                        !
      !                 !15 - Franco de pagamento.                      !
      !                 !17 - Liquidacao apos baixa ou liquidacao titulo!
      !                 !     nao registrado.                           !
      !                 !19 - Confirmacao recebimento instrucao de      !
      !                 !     protesto.                                 !
      !                 !20 - Confirmacao recebimento instrucao de      !
      !                 !     sustacao/cancelamento de protesto.        !
      !                 !23 - Remessa a cartorio /aponte em cartorio/.  !
      !                 !24 - Retirada de cartorio e manutencao em      !
      !                 !     carteira.                                 !
      !                 !25 - Protestado e baixado /baixa por ter sido  !
      !                 !     protestado/.                              !
      !                 !26 - Instrucao rejeitada.                      !
      !                 !27 - Confirmacao do pedido de alteracao de     !
      !                       outros dados.                             !
      !                 !28 - Debito de tarifas/custas.                 !
      !                 !29 - Ocorrencias do sacado.                    !
      !                 !30 - Alteracao de dados rejeitada.             !
      !                 !Obs.: Os codigos 03, 26 e 30 estao relacionados!
      !                 !com a nota 42-a.                               !
      !                 !O codigo 28 esta relacionado com a nota 42-b.  !
      !                 !Os codigos 06, 09 e 17 estao relacionados com a!
      !                 !nota 42-c.                                     !
      *-----------------------------------------------------------------*
      *)
            case Movimento of
              02: Result := 'Entrada confirmada.';
              03: Result := 'Entrada rejeitada.';
              04: Result := 'Transfer�ncia de carteira/entrada.';
              05: Result := 'Transfer�ncia de carteira/baixa.';
              06: Result := 'Liquida��o.';
              09: Result := 'Baixa.';
              11: Result := 'Titulos em carteira (em ser).';
              12: Result := 'Confirma��o recebimento instru��o de abatimento.';
              13: Result := 'Confirma��o recebimento instru��o de cancelamento abatimento.';
              14: Result := 'Confirma��o recebimento instru��o altera��o de vencimento.';
              15: Result := 'Franco de pagamento.';
              17: Result := 'Liquida��o ap�s baixa ou liquida��o de t�tulo n�o registrado.';
              19: Result := 'Confirma��o de recebimento instru��o de protesto.';
              20: Result := 'Confirma��o de recebimento instru��o de susta��o/cancelamento de protesto.';
              23: Result := 'Remessa a cart�rio (aponte em cart�rio).';
              24: Result := 'Retirada de cart�rio e manuten��o em carteira.';
              25: Result := 'Protestado e baixado (baixa por ter sido protestado).';
              26: Result := 'Instru��o rejeitada.';
              27: Result := 'Confirma��o do pedido de altera��o de outros dados.';
              28: Result := 'D�bito de tarifas/custas.';
              29: Result := 'Ocorr�ncias do sacado.';
              30: Result := 'Altera��o de dados rejeitada.';
          (*
              Obs.: Os codigos 03, 26 e 30 estao relacionados!
              com a nota 42-a.                               !
              O codigo 28 esta relacionado com a nota 42-b.  !
              Os codigos 06, 09 e 17 estao relacionados com a!
              nota 42-c.                                     !
          *)
              else Result := '';
            end
          end else Result := '** N�o implementado **';
        end;
      end; // fim Banco 001
      008, 033, 353:
      begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
            Result := '** N�o implementado **';
          end;
          ecnabRetorno: // Retorno
          begin
            case Movimento of
              01 : Result := 'T�TULO N�O EXISTE';
              02 : Result := 'ENTRADA T�T. CONFIRMADA';
              03 : Result := 'ENTRADA T�T. REJEITADA';
              06 : Result := 'LIQUIDA��O';
              07 : Result := 'LIQUIDA��O POR CONTA';
              08 : Result := 'LIQUIDA��O POR SALDO';
              09 : Result := 'BAIXA AUTOM�TICA';
              10 : Result := 'T�T. BAIX. CONF. INSTRU��O';
              11 : Result := 'EM SER';
              12 : Result := 'ABATIMENTO CONCEDIDO';
              13 : Result := 'ABATIMENTO CANCELADO';
              14 : Result := 'PRORROGA��O DE VENCIMENTO';
              15 : Result := 'CONFIRMA��O DE PROTESTO';
              16 : Result := 'T�T. J� BAIXADO/LIQUIDADO';
              17 : Result := 'LIQUIDADO EM CART�RIO';
              21 : Result := 'T�T. ENVIADO A CART�RIO';
              22 : Result := 'T�T. RETIRADO DE CART�RIO';
              24 : Result := 'CUSTAS DE CART�RIO';
              25 : Result := 'PROTESTAR T�TULO';
              26 : Result := 'SUSTAR PROTESTO';
              else Result := '** Movimento n�o implementado';
            end;
          end;
          else Result := '** Tipo de envio n�o implementado';
        end;
      end;
      104:
      begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
            Result := '** N�o implementado **';
          end;
          ecnabRetorno: // Retorno
          begin
            case Movimento of
              01 : Result := 'Entrada Confirmada';
              02 : Result := 'Baixa Confirmada';
              03 : Result := 'Abatimento Concedido';
              04 : Result := 'Abatimento Cancelado';
              05 : Result := 'Vencimento Alterado';
              06 : Result := 'Uso da Empresa Alterado';
              07 : Result := 'Prazo de Protesto Alterado';
              08 : Result := 'Prazo de Devolu��o Alterado';
              09 : Result := 'Altera��o Confirmada';
              10 : Result := 'Altera��o com Reemiss�o de Boquete Confirmada';
              11 : Result := 'Altera��o da Op��o de Protesto para Devolu��o Confirmada';
              12 : Result := 'Altera��o da Op��o de Devolu��o para protesto Confirmada';
              20 : Result := 'Em Ser';
              21 : Result := 'Liquida��o';
              22 : Result := 'Liquida��o em Cart�rio';
              23 : Result := 'Baixa por Devolu��o';
              24 : Result := 'Baixa Franco Pagamento';
              25 : Result := 'Baixa por Protesto';
              26 : Result := 'T�tulo enviado para Cart�rio';
              27 : Result := 'Susta��o de Protesto';
              28 : Result := 'Estorno de Protesto';
              29 : Result := 'Estorno de Susta��o de Protesto';
              30 : Result := 'Altera��o de T�tulo';
              31 : Result := 'Tarifa sobre T�tulo Vencido';
              32 : Result := 'Outras Tarifas de Altera��o';
              33 : Result := 'Estorno de Baixa /Liquida��o';
              99 : Result := 'Rejei��o do T�tulo - C�d. rejei��o informado nas POS 80 a 82';
              else Result := '** Movimento n�o implementado';
            end;
          end;
          else Result := '** Tipo de envio n�o implementado';
        end;
      end; //fim banco 104
      237:
      begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
            case Movimento of
              01: Result := 'Remessa';
              02: Result := 'Pedido de baixa';
              04: Result := 'Concess�o de abatimento';
              05: Result := 'Cancelamento de abatimento concedido';
              06: Result := 'Altera��o de vencimento';
              07: Result := 'Altera��o do controle do participante';
              08: Result := 'Altera��o de seu n�mero';
              09: Result := 'Pedido de protesto';
              18: Result := 'Sustar protesto e baixar T�tulo';
              19: Result := 'Sustar protesto e manter em carteira';
              22: Result := 'Transfer�ncia Cess�o cr�dito ID. Prod. 10';
              23: Result := 'Transfer�ncia entre Carteiras';
              24: Result := 'Dev. Transfer�ncia entre Carteiras';
              31: Result := 'Altera��o de outros dados';
              35: Result := 'Desagendamento do d�bito autom�tico';
              68: Result := 'Acerto nos dados do rateio de Cr�dito';
              69: Result := 'Cancelamento do rateio de cr�dito.';
              else Result := '** N�o implementado **';
            end;
          end;
          ecnabRetorno: // Retorno
          begin
            {if Registrado then
            begin
            }
              case Movimento of
                02: Result := 'Entrada Confirmada (verificar motivo na posi��o 319 a 328 )';
                03: Result := 'Entrada Rejeitada ( verificar motivo na posi��o 319 a 328)';
                06: Result := 'Liquida��o normal (sem motivo)';
                09: Result := 'Baixado Automat. via Arquivo (verificar motivo posi��o 319 a 328)';
                10: Result := 'Baixado conforme instru��es da Ag�ncia(verificar motivo pos.319 a 328)';
                11: Result := 'Em Ser - Arquivo de T�tulos pendentes (sem motivo)';
                12: Result := 'Abatimento Concedido (sem motivo)';
                13: Result := 'Abatimento Cancelado (sem motivo)';
                14: Result := 'Vencimento Alterado (sem motivo)';
                15: Result := 'Liquida��o em Cart�rio (sem motivo)';
                16: Result := 'T�tulo Pago em Cheque � Vinculado';
                17: Result := 'Liquida��o ap�s baixa ou T�tulo n�o registrado (sem motivo)';
                18: Result := 'Acerto de Deposit�ria (sem motivo)';
                19: Result := 'Confirma��o Receb. Inst. de Protesto (verificar motivo pos.295 a 295)';
                20: Result := 'Confirma��o Recebimento Instru��o Susta��o de Protesto (sem motivo)';
                21: Result := 'Acerto do Controle do Participante (sem motivo)';
                22: Result := 'T�tulo Com Pagamento Cancelado';
                23: Result := 'Entrada do T�tulo em Cart�rio (sem motivo)';
                24: Result := 'Entrada rejeitada por CEP Irregular (verificar motivo pos.319 a 328)';
                25: Result := 'Confirma��o Receb.Inst.de Protesto Falimentar';
                27: Result := 'Baixa Rejeitada (verificar motivo posi��o 319 a 328)';
                28: Result := 'D�bito de tarifas/custas (verificar motivo na posi��o 319 a 328)';
                29: Result := 'Ocorr�ncias do Sacado';
                30: Result := 'Altera��o de Outros Dados Rejeitados (verificar motivo pos.319 a 328)';
                32: Result := 'Instru��o Rejeitada (verificar motivo posi��o 319 a 328)';
                33: Result := 'Confirma��o Pedido Altera��o Outros Dados (sem motivo)';
                34: Result := 'Retirado de Cart�rio e Manuten��o Carteira (sem motivo)';
                35: Result := 'Desagendamento do d�bito autom�tico (verificar motivos pos. 319 a 328)';
                40: Result := 'Estorno de pagamento (Novo)';
                55: Result := 'Sustado judicial (Novo)';
                68: Result := 'Acerto dos dados do rateio de Cr�dito (verificar motivo posi��o de status do registro tipo 3)';
                69: Result := 'Cancelamento dos dados do rateio (verificar motivo posi��o de status do registro tipo 3)';
                else Result := '** N�o implementado **';
              end;
            {
            end else begin
              case Movimento of
                02: Result := 'Entrada Confirmada - Quando o registro estiver correto e a papeleta for impressa';
                06: Result := 'Liquida��o Normal - Pagamento Efetuado';
                else Result := '** N�o implementado **';
              end;
            end;
            }
          end;
        end;
      end; //fim banco 237
      341:
      begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
            Result := '** N�o implementado **';
          end;
          ecnabRetorno: // Retorno
          begin
            case Movimento of
              02: Result := 'ENTRADA CONFIRMADA';
              03: Result := 'ENTRADA REJEITADA (NOTA 20 - TABELA 1)';
              04: Result := 'ALTERA��O DE DADOS - NOVA ENTRADA';
              05: Result := 'ALTERA��O DE DADOS - BAIXA';
              06: Result := 'LIQUIDA��O NORMAL';
              08: Result := 'LIQUIDA��O EM CART�RIO';
              09: Result := 'BAIXA SIMPLES';
              10: Result := 'BAIXA POR TER SIDO LIQUIDADO';
              11: Result := 'EM SER (S� NO RETORNO MENSAL)';
              12: Result := 'ABATIMENTO CONCEDIDO';
              13: Result := 'ABATIMENTO CANCELADO';
              14: Result := 'VENCIMENTO ALTERADO';
              15: Result := 'BAIXAS REJEITADAS (NOTA 20 - TABELA 4)';
              16: Result := 'INSTRU��ES REJEITADAS (NOTA 20 - TABELA 3)';
              17: Result := 'ALTERA��O DE DADOS REJEITADOS (NOTA 20 - TABELA 2)';
              18: Result := 'COBRAN�A CONTRATUAL - ABATIMENTO E BAIXA BLOQUEADOS (NOTA 20 - TABELA 5)';
              19: Result := 'CONFIRMA RECEBIMENTO DE INSTRU��O DE PROTESTO';
              20: Result := 'CONFIRMA RECEBIMENTO DE INSTRU��O DE SUSTA��O DE PROTESTO /TARIFA';
              21: Result := 'CONFIRMA RECEBIMENTO DE INSTRU��O DE N�O PROTESTAR';
              23: Result := 'PROTESTO ENVIADO A CART�RIO/TARIFA';
              24: Result := 'INSTRU��O DE PROTESTO REJEITADA / SUSTADA / PENDENTE (NOTA 20 - TABELA 7)';
              25: Result := 'ALEGA��ES DO SACADO (NOTA 20 - TABELA 6)';
              26: Result := 'TARIFA DE AVISO DE COBRAN�A';
              27: Result := 'TARIFA DE EXTRATO POSI��O (B40X)';
              28: Result := 'TARIFA DE RELA��O DAS LIQUIDA��ES';
              29: Result := 'TARIFA DE MANUTEN��O DE T�TULOS VENCIDOS';
              30: Result := 'D�BITO MENSAL DE TARIFAS (PARA ENTRADAS E BAIXAS)';
              32: Result := 'BAIXA POR TER SIDO PROTESTADO';
              33: Result := 'CUSTAS DE PROTESTO';
              34: Result := 'CUSTAS DE SUSTA��O';
              35: Result := 'CUSTAS DE CART�RIO DISTRIBUIDOR';
              36: Result := 'CUSTAS DE EDITAL';
              37: Result := 'TARIFA DE EMISS�O DE BLOQUETO/TARIFA DE ENVIO DE DUPLICATA';
              38: Result := 'TARIFA DE INSTRU��O';
              39: Result := 'TARIFA DE OCORR�NCIAS';
              40: Result := 'TARIFA MENSAL DE EMISS�O DE BLOQUETO/TARIFA MENSAL DE ENVIO DE DUPLICATA';
              41: Result := 'D�BITO MENSAL DE TARIFAS - EXTRATO DE POSI��O (B4EP/B4OX)';
              42: Result := 'D�BITO MENSAL DE TARIFAS - OUTRAS INSTRU��ES';
              43: Result := 'D�BITO MENSAL DE TARIFAS - MANUTEN��O DE T�TULOS VENCIDOS';
              44: Result := 'D�BITO MENSAL DE TARIFAS - OUTRAS OCORR�NCIAS';
              45: Result := 'D�BITO MENSAL DE TARIFAS - PROTESTO';
              46: Result := 'D�BITO MENSAL DE TARIFAS - SUSTA��O DE PROTESTO';
              47: Result := 'BAIXA COM TRANSFER�NCIA PARA DESCONTO';
              48: Result := 'CUSTAS DE SUSTA��O JUDICIAL';
              51: Result := 'TARIFA MENSAL REF A ENTRADAS BANCOS CORRESPONDENTES NA CARTEIRA';
              52: Result := 'TARIFA MENSAL BAIXAS NA CARTEIRA';
              53: Result := 'TARIFA MENSAL BAIXAS EM BANCOS CORRESPONDENTES NA CARTEIRA';
              54: Result := 'TARIFA MENSAL  DE LIQUIDA��ES NA CARTEIRA';
              55: Result := 'TARIFA MENSAL DE LIQUIDA��ES EM BANCOS CORRESPONDENTES NA CARTEIRA';
              56: Result := 'CUSTAS DE IRREGULARIDADE';
              57: Result := 'INSTRU��O CANCELADA (NOTA 20 - TABELA 8)';
              60: Result := 'ENTRADA REJEITADA CARN�  (NOTA 20 - TABELA 1)';
              61: Result := 'TARIFA EMISS�O AVISO DE MOVIMENTA��O DE T�TULOS (2154)';
              62: Result := 'D�BITO MENSAL DE TARIFA - AVISO DE MOVIMENTA��O DE T�TULOS (2154)';
              63: Result := 'T�TULO SUSTADO JUDICIALMENTE';
              else Result := ' ';
            end;
          end;
        end;
      end; // fim banco 341
      399:
      begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
            Result := '** N�o implementado **';
          end;
          ecnabRetorno: // Retorno
          begin
            case Movimento of
              06: Result := 'Retorno da Liquida��o';
              07: Result := 'Emiss�o Confirmada';
              08: Result := 'Parcela Rejeitada';
              else Result := ' ';
            end;
          end;
        end;
      end; // fim banco 409
      409:
      begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
            Result := '** N�o implementado **';
          end;
          ecnabRetorno: // Retorno
          begin
            case Movimento of
              02: Result := 'Remessa aceita';
              03: Result := 'Remessa rejeitada';
              06: Result := 'Liquida��o (T�tulos liquidados)';
              09: Result := 'T�tulo Reclassificado (Tarifa por liquida��o de t�tulo reclassificado)';
              else Result := ' ';
            end;
          end;
        end;
      end; // fim banco 409
      748:
      begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
            Result := '** N�o implementado **';
          end;
          ecnabRetorno: // Retorno
          begin
            case Movimento of
              02: Result := 'Entrada confirmada.';
              03: Result := 'Entrada rejeitada.';
              06: Result := 'Liquida��o normal.';
              28: Result := 'Tarifa.';
              else Result := ' ';
            end;
          end;
        end;
      end; // fim banco 748
      756:
      begin
        case Envio of
          ecnabRemessa: // Remessa
          begin
            Result := '** N�o implementado **';
          end;
          ecnabRetorno: // Retorno
          begin
            if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
            begin
              case Movimento of
                02: Result := 'Entrada Confirmada';
                03: Result := 'Entrada Rejeitada';
                06: Result := 'Liquida��o Normal';
                09: Result := 'Baixado automaticamente via arquivos';
                10: Result := 'Baixado Conforme Instru��es da Ag�ncia';
                11: Result := 'Em Ser � Arquivo de T�tulos Pendentes';
                12: Result := 'Abatimento Concedido';
                13: Result := 'Abatimento Cancelado';
                14: Result := 'Vencimento Alterado';
                15: Result := 'Liquida��o em Cart�rio';
                16: Result := 'T�tulo pago em cheque � vinculado';
                17: Result := 'Liquida��o ap�s baixa ou t�tulo n�o registrado';
                18: Result := 'Acerto de Deposit�ria';
                19: Result := 'Confirma��o de Recebimento Instru��o de Protesto';
                20: Result := 'Confirma��o de Recebimento Instru��o de Susta��o de Protesto';
                22: Result := 'T�tulo com pagamento cancelado';
                21: Result := 'Acerto do Controle do Participante';
                23: Result := 'Entrada do T�tulo em Cart�rio';
                24: Result := 'Entrada Rejeitada por CEP irregular';
                27: Result := 'Baixa Rejeitada';
                28: Result := 'D�bito de Tarifas e Custas';
                30: Result := 'Altera��o de Outros Dados Rejeitados';
                32: Result := 'Instru��o Rejeitada';
                33: Result := 'Confirma��o de Pedido de Altera��o de Outros Dados';
                34: Result := 'Retirado de Cart�rio e Manuten��o de Carteira';
                35: Result := 'Desagendamento do d�bito autom�tico';
                68: Result := 'Acerto do dados de rateio de cr�dito';
                69: Result := 'Cancelamento dos dados do rateio';
                else Result := ' ';
              end;
            end else
            begin
              case Movimento of
                02: Result := 'Entrada confirmada';
                03: Result := 'Entrada Rejeitada';
                10: Result := 'Baixado Conforme Instru��es da Ag�ncia';
                24: Result := 'Entrada Rejeitada por CEP irregular';
                27: Result := 'Baixa Rejeitada';
                28: Result := 'D�bito de Tarifas/Custas';
                30: Result := 'Altera��o de Outros Dados Rejeitados';
                32: Result := 'Instru��o Rejeitada';
                35: Result := 'Desagendamento do D�bito Autom�tico';
                99: Result := 'Liquida��o';
                else Result := ' ';
              end;
            end;
          end;
        end;
      end; // fim banco 756
      else Result := '*** Banco n�o implementado ***';
    end;
  end;
end;

function TUBancos.TamanhoLinhaCNAB_Index(Posicoes: Integer): Integer;
begin
  Result := 0;
  case Posicoes of
    240: Result := 1;
    400: Result := 2;
    else Geral.MB_Aviso('Index de tamanho de registro CNAB indefinido para ' +
    IntToStr(Posicoes) + ' posi��es!');
  end;
end;

function TUBancos.TamanhoLinhaCNAB_Posicoes(Index: Integer): Integer;
begin
  Result := 0;
  case Index of
    1: Result := 240;
    2: Result := 400;
    else Geral.MB_Aviso('Index de tamanho de registro CNAB indefinido!');
  end;
end;

function TUBancos.TamanhoLinhaCNAB_Valor(T240, T400: Integer): Integer;
begin
  Result := 0;
  if T240 = 1 then Result := Result + 1;
  if T400 = 1 then Result := Result + 2;
end;

end.



