object FmBancos: TFmBancos
  Left = 341
  Top = 169
  Caption = 'FIN-BANCO-001 :: Cadastro de Bancos'
  ClientHeight = 723
  ClientWidth = 1016
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1016
    Height = 627
    Align = alClient
    BevelOuter = bvNone
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1016
      Height = 436
      Align = alTop
      Caption = ' Dados: '
      TabOrder = 0
      object PainelData: TPanel
        Left = 2
        Top = 15
        Width = 1012
        Height = 132
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 56
          Top = 8
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label4: TLabel
          Left = 360
          Top = 8
          Width = 71
          Height = 13
          Caption = 'Site na internet'
          FocusControl = DBEdit01
        end
        object Label7: TLabel
          Left = 295
          Top = 8
          Width = 22
          Height = 13
          Caption = 'DVB'
          FocusControl = DBEdit1
        end
        object Label12: TLabel
          Left = 327
          Top = 8
          Width = 29
          Height = 13
          Caption = 'DVCC'
          FocusControl = DBEdit2
        end
        object Label17: TLabel
          Left = 784
          Top = 8
          Width = 89
          Height = 13
          Caption = 'Entidade banc'#225'ria:'
          FocusControl = DBEdit3
        end
        object Label62: TLabel
          Left = 468
          Top = 87
          Width = 256
          Height = 13
          Caption = 'Descri'#231#227'o da cobran'#231'a sem registro nos lan'#231'amentos:'
        end
        object Label63: TLabel
          Left = 468
          Top = 48
          Width = 223
          Height = 13
          Caption = 'Diret'#243'rio dos arquivos de retorno multi empresa:'
          FocusControl = DBEdit7
        end
        object DBEdCodigo: TDBEdit
          Left = 8
          Top = 24
          Width = 44
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsBancos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdNome: TDBEdit
          Left = 56
          Top = 24
          Width = 235
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsBancos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object DBEdit01: TDBEdit
          Left = 360
          Top = 24
          Width = 321
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Site'
          DataSource = DsBancos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtNavegar: TBitBtn
          Tag = 119
          Left = 688
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Navega'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtNavegarClick
        end
        object Progress: TProgressBar
          Left = 444
          Top = 4
          Width = 233
          Height = 17
          TabOrder = 4
          Visible = False
        end
        object DBEdit1: TDBEdit
          Left = 295
          Top = 24
          Width = 29
          Height = 21
          DataField = 'DVB'
          DataSource = DsBancos
          TabOrder = 5
        end
        object DBEdit2: TDBEdit
          Left = 327
          Top = 23
          Width = 29
          Height = 21
          DataField = 'DVCC'
          DataSource = DsBancos
          TabOrder = 6
        end
        object DBEdit3: TDBEdit
          Left = 784
          Top = 24
          Width = 461
          Height = 21
          DataField = 'NOMEENTIDADE'
          DataSource = DsBancos
          TabOrder = 7
        end
        object GroupBox5: TGroupBox
          Left = 8
          Top = 48
          Width = 457
          Height = 77
          Caption = ' Identificador de cobran'#231'as no arquivo CNAB: '
          TabOrder = 8
          object GroupBox6: TGroupBox
            Left = 2
            Top = 15
            Width = 453
            Height = 60
            Align = alClient
            Caption = ' CNAB 400: '
            TabOrder = 0
            object Label18: TLabel
              Left = 12
              Top = 16
              Width = 30
              Height = 13
              Caption = 'In'#237'cio:'
            end
            object Label42: TLabel
              Left = 72
              Top = 16
              Width = 48
              Height = 13
              Caption = 'Tamanho:'
            end
            object DBEdit4: TDBEdit
              Left = 12
              Top = 32
              Width = 57
              Height = 21
              DataField = 'ID_400i'
              DataSource = DsBancos
              TabOrder = 0
            end
            object DBEdit5: TDBEdit
              Left = 72
              Top = 32
              Width = 57
              Height = 21
              DataField = 'ID_400t'
              DataSource = DsBancos
              TabOrder = 1
            end
          end
        end
        object DBEdit6: TDBEdit
          Left = 468
          Top = 104
          Width = 540
          Height = 21
          DataField = 'DescriCNR'
          DataSource = DsBancos
          TabOrder = 9
        end
        object DBEdit7: TDBEdit
          Left = 468
          Top = 64
          Width = 540
          Height = 21
          DataField = 'CNABDirMul'
          DataSource = DsBancos
          TabOrder = 10
        end
      end
      object PageControl1: TPageControl
        Left = 2
        Top = 147
        Width = 1012
        Height = 253
        ActivePage = TabSheet2
        Align = alTop
        TabOrder = 1
        OnChange = PageControl1Change
        OnChanging = PageControl1Changing
        object TabSheet1: TTabSheet
          Caption = 'Navegar na p'#225'gina da internet '
          object PnNavega: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 225
            Align = alClient
            TabOrder = 0
            object WebBrowser1: TWebBrowser
              Left = 1
              Top = 1
              Width = 1002
              Height = 223
              Align = alClient
              TabOrder = 0
              ExplicitLeft = 136
              ExplicitTop = 92
              ExplicitWidth = 300
              ExplicitHeight = 150
              ControlData = {
                4C0000008F6700000C1700000000000000000000000000000000000000000000
                000000004C000000000000000000000001000000E0D057007335CF11AE690800
                2B2E126208000000000000004C0000000114020000000000C000000000000046
                8000000000000000000000000000000000000000000000000000000000000000
                00000000000000000100000000000000000000000000000000000000}
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'CNAB - Ocorr'#234'ncias banc'#225'rias '
          ImageIndex = 1
          object Label79: TLabel
            Left = 7
            Top = 9
            Width = 608
            Height = 32
            Caption = 'Cadastrar nas respectivas Configura'#231#245'es CNAB!!!'
            Color = clBtnFace
            Font.Charset = ANSI_CHARSET
            Font.Color = clGradientActiveCaption
            Font.Height = -27
            Font.Name = 'Arial'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            Visible = False
          end
          object Label80: TLabel
            Left = 9
            Top = 11
            Width = 608
            Height = 32
            Caption = 'Cadastrar nas respectivas Configura'#231#245'es CNAB!!!'
            Color = clBtnFace
            Font.Charset = ANSI_CHARSET
            Font.Color = clSilver
            Font.Height = -27
            Font.Name = 'Arial'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object Label81: TLabel
            Left = 8
            Top = 10
            Width = 608
            Height = 32
            Caption = 'Cadastrar nas respectivas Configura'#231#245'es CNAB!!!'
            Color = clBtnFace
            Font.Charset = ANSI_CHARSET
            Font.Color = clHotLight
            Font.Height = -27
            Font.Name = 'Arial'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'CNAB - Composi'#231#227'o arquivos '
          ImageIndex = 2
          object Panel12: TPanel
            Left = 213
            Top = 0
            Width = 791
            Height = 225
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object DBGrid4: TDBGrid
              Left = 0
              Top = 0
              Width = 791
              Height = 225
              Align = alClient
              DataSource = DsCNAB_CaD
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'ID'
                  Width = 39
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PadrIni'
                  Title.Caption = 'Ini'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PadrTam'
                  Title.Caption = 'Tam'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECAMPO'
                  Title.Caption = 'Funcionalidade do campo'
                  Width = 155
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'NOME_T'
                  Title.Alignment = taCenter
                  Title.Caption = 'T'
                  Width = 18
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'NOME_I'
                  Title.Alignment = taCenter
                  Title.Caption = 'I'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Casas'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Formato'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PadrVal'
                  Title.Caption = 'Valor padr'#227'o'
                  Width = 69
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Signific'
                  Title.Caption = 'Significado'
                  Width = 236
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ajuda'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Visible = True
                end>
            end
          end
          object Panel14: TPanel
            Left = 0
            Top = 0
            Width = 213
            Height = 225
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object DBGrid2: TDBGrid
              Left = 0
              Top = 68
              Width = 213
              Height = 157
              Align = alClient
              DataSource = DsBanCNAB_R
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMEREGISTRO'
                  Title.Caption = 'Tipo de Registro'
                  Width = 89
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Seq1Ini'
                  Title.Caption = 'Ini 1'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Seq1Tam'
                  Title.Caption = 'Tam 1'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Seq1Val'
                  Title.Caption = 'Val 1'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Seq2Ini'
                  Title.Caption = 'Ini 2'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Seq2Tam'
                  Title.Caption = 'Tam 2'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Seq2Val'
                  Title.Caption = 'Val 2'
                  Width = 28
                  Visible = True
                end>
            end
            object Panel15: TPanel
              Left = 0
              Top = 0
              Width = 213
              Height = 68
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object Panel16: TPanel
                Left = 158
                Top = 0
                Width = 55
                Height = 68
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 0
                object Label25: TLabel
                  Left = 4
                  Top = 0
                  Width = 46
                  Height = 13
                  Caption = 'Posi'#231#245'es:'
                end
                object EdPosicoes: TdmkEdit
                  Left = 4
                  Top = 16
                  Width = 45
                  Height = 21
                  Alignment = taCenter
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdPosicoesChange
                end
              end
              object RGEnvi2: TRadioGroup
                Left = 0
                Top = 0
                Width = 158
                Height = 68
                Align = alClient
                Caption = ' Tipo de envio: '
                ItemIndex = 2
                Items.Strings = (
                  'Nenhum'
                  'Remessa'
                  'Retorno')
                TabOrder = 1
                OnClick = RGEnvi2Click
              end
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'CNAB - Vincula'#231#227'o de campos com dados '
          ImageIndex = 3
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 41
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Carrega arquivo remessa / retorno'
          ImageIndex = 4
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 225
            Align = alClient
            TabOrder = 0
            object Panel18: TPanel
              Left = 1
              Top = 1
              Width = 1002
              Height = 41
              Align = alTop
              BevelOuter = bvNone
              Caption = 'Panel3'
              TabOrder = 0
              object Label55: TLabel
                Left = 4
                Top = 16
                Width = 39
                Height = 13
                Caption = 'Arquivo:'
              end
              object SpeedButton5: TSpeedButton
                Left = 752
                Top = 12
                Width = 23
                Height = 22
                OnClick = SpeedButton5Click
              end
              object Edit1: TEdit
                Left = 48
                Top = 12
                Width = 697
                Height = 21
                TabOrder = 0
              end
            end
            object Editor: TMemo
              Left = 1
              Top = 42
              Width = 1002
              Height = 115
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              WordWrap = False
              OnKeyUp = EditorKeyUp
              OnMouseDown = EditorMouseDown
              OnMouseUp = EditorMouseUp
            end
            object Panel19: TPanel
              Left = 1
              Top = 176
              Width = 1002
              Height = 48
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 2
              object Label56: TLabel
                Left = 4
                Top = 4
                Width = 29
                Height = 13
                Caption = 'Linha:'
              end
              object Label57: TLabel
                Left = 44
                Top = 4
                Width = 36
                Height = 13
                Caption = 'Coluna:'
              end
              object Label58: TLabel
                Left = 84
                Top = 4
                Width = 27
                Height = 13
                Caption = 'Tam.:'
              end
              object Label59: TLabel
                Left = 124
                Top = 4
                Width = 87
                Height = 13
                Caption = 'Texto pesquisado:'
              end
              object Panel20: TPanel
                Left = 891
                Top = 0
                Width = 111
                Height = 48
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 0
              end
              object Edit2: TEdit
                Left = 4
                Top = 20
                Width = 37
                Height = 21
                TabOrder = 1
                Text = '0'
                OnChange = Edit2Change
              end
              object Edit3: TEdit
                Left = 44
                Top = 20
                Width = 37
                Height = 21
                TabOrder = 2
                Text = '0'
                OnChange = Edit3Change
              end
              object Edit4: TEdit
                Left = 84
                Top = 20
                Width = 37
                Height = 21
                TabOrder = 3
                Text = '0'
                OnChange = Edit4Change
              end
              object Edit6: TEdit
                Left = 124
                Top = 20
                Width = 873
                Height = 21
                ReadOnly = True
                TabOrder = 4
              end
            end
            object StatusBar: TStatusBar
              Left = 1
              Top = 157
              Width = 1002
              Height = 19
              Panels = <
                item
                  Width = 120
                end
                item
                  Alignment = taCenter
                  Width = 60
                end
                item
                  Width = 50
                end>
            end
          end
        end
        object TabSheet6: TTabSheet
          Caption = 'Importa'#231#227'o de cadastros'
          ImageIndex = 5
          object Panel21: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 225
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel22: TPanel
              Left = 0
              Top = 0
              Width = 185
              Height = 225
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object RGImporta: TRadioGroup
                Left = 0
                Top = 0
                Width = 185
                Height = 108
                Align = alTop
                Caption = ' Tipo de importa'#231#227'o: '
                Items.Strings = (
                  'Lista de Bancos em Excel (2009)'
                  'Lista de Bancos em TXT (2007)'
                  'Cadastros CNAB completo'
                  'Campos CNAB deste banco')
                TabOrder = 0
                OnClick = RGImportaClick
              end
              object BtImportar: TBitBtn
                Tag = 19
                Left = 43
                Top = 116
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Inclui novo banco'
                Caption = 'I&mporta'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtImportarClick
              end
            end
            object PageControl2: TPageControl
              Left = 185
              Top = 0
              Width = 819
              Height = 225
              ActivePage = TabSheet9
              Align = alClient
              TabOrder = 1
              object TabSheet9: TTabSheet
                Caption = ' Lista Excel '
                ImageIndex = 2
                object Panel23: TPanel
                  Left = 0
                  Top = 0
                  Width = 811
                  Height = 116
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label27: TLabel
                    Left = 8
                    Top = 11
                    Width = 116
                    Height = 13
                    Caption = 'Arquivo a ser carregado:'
                  end
                  object SpeedButton8: TSpeedButton
                    Left = 780
                    Top = 7
                    Width = 21
                    Height = 21
                    Caption = '...'
                    OnClick = SpeedButton8Click
                  end
                  object LaAviso: TLabel
                    Left = 8
                    Top = 28
                    Width = 15
                    Height = 22
                    Caption = '...'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -19
                    Font.Name = 'Arial'
                    Font.Style = []
                    ParentFont = False
                  end
                  object EdArqBcoXLS: TdmkEdit
                    Left = 128
                    Top = 7
                    Width = 652
                    Height = 21
                    TabOrder = 0
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = 'http://www.dermatek.com.br/Instaladores/BancosE.xls'
                    QryCampo = 'DirNFeGer'
                    UpdCampo = 'DirNFeGer'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 'http://www.dermatek.com.br/Instaladores/BancosE.xls'
                    ValWarn = False
                    OnChange = EdArqBcoXLSChange
                  end
                  object PB1: TProgressBar
                    Left = 8
                    Top = 52
                    Width = 793
                    Height = 17
                    TabOrder = 1
                  end
                  object BitBtn5: TBitBtn
                    Tag = 19
                    Left = 8
                    Top = 72
                    Width = 90
                    Height = 40
                    Caption = '&Baixar'
                    NumGlyphs = 2
                    TabOrder = 2
                    OnClick = BitBtn5Click
                  end
                  object BtCarrega: TBitBtn
                    Left = 208
                    Top = 72
                    Width = 90
                    Height = 40
                    Caption = '&Carrega xls'
                    Enabled = False
                    NumGlyphs = 2
                    TabOrder = 3
                    OnClick = BtCarregaClick
                  end
                  object BtAbre: TBitBtn
                    Left = 108
                    Top = 72
                    Width = 90
                    Height = 40
                    Caption = '&Abre xls'
                    NumGlyphs = 2
                    TabOrder = 4
                    OnClick = BtAbreClick
                  end
                end
                object Grade1: TStringGrid
                  Left = 0
                  Top = 116
                  Width = 811
                  Height = 81
                  Align = alClient
                  ColCount = 2
                  DefaultColWidth = 44
                  DefaultRowHeight = 18
                  RowCount = 2
                  TabOrder = 1
                end
              end
              object TabSheet10: TTabSheet
                Caption = ' Lista TXT '
                ImageIndex = 3
              end
              object TabSheet7: TTabSheet
                Caption = 'CNAB Completo'
                object MeImporta: TMemo
                  Left = 0
                  Top = 0
                  Width = 441
                  Height = 197
                  Align = alClient
                  TabOrder = 1
                end
                object MeInfo: TMemo
                  Left = 0
                  Top = 0
                  Width = 441
                  Height = 197
                  Align = alClient
                  TabOrder = 0
                end
                object MeRegistros: TMemo
                  Left = 441
                  Top = 0
                  Width = 370
                  Height = 197
                  Align = alRight
                  TabOrder = 2
                end
              end
              object TabSheet8: TTabSheet
                Caption = 'Campos CNAB deste banco'
                ImageIndex = 1
                object Grade: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 811
                  Height = 197
                  Align = alClient
                  ColCount = 10
                  DefaultColWidth = 100
                  DefaultRowHeight = 18
                  TabOrder = 0
                end
              end
            end
          end
        end
      end
    end
    object GroupBox13: TGroupBox
      Left = 0
      Top = 569
      Width = 1016
      Height = 58
      Align = alBottom
      TabOrder = 1
      object PainelControle: TPanel
        Left = 2
        Top = 8
        Width = 1012
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object LaRegistro: TdmkLabel
          Left = 172
          Top = 0
          Width = 371
          Height = 48
          Align = alClient
          Caption = '[N]: 0'
          UpdType = utYes
          SQLType = stNil
          ExplicitWidth = 26
          ExplicitHeight = 13
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 172
          Height = 48
          Align = alLeft
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object SpeedButton4: TBitBtn
            Tag = 4
            Left = 128
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'ltimo registro'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = SpeedButton4Click
          end
          object SpeedButton3: TBitBtn
            Tag = 3
            Left = 88
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'Pr'#243'ximo registro'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = SpeedButton3Click
          end
          object SpeedButton2: TBitBtn
            Tag = 2
            Left = 48
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'Registro anterior'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = SpeedButton2Click
          end
          object SpeedButton1: TBitBtn
            Tag = 1
            Left = 8
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'Primeiro registro'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = SpeedButton1Click
          end
        end
        object Panel3: TPanel
          Left = 543
          Top = 0
          Width = 469
          Height = 48
          Align = alRight
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object BtCNAB: TBitBtn
            Tag = 11
            Left = 96
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&CNAB'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtCNABClick
          end
          object BtBanco: TBitBtn
            Tag = 10012
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Banco'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtBancoClick
          end
          object Panel6: TPanel
            Left = 360
            Top = 0
            Width = 109
            Height = 48
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 2
            object BtSaida: TBitBtn
              Tag = 13
              Left = 8
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
        end
      end
    end
  end
  object PainelRegistro: TPanel
    Left = 0
    Top = 96
    Width = 1016
    Height = 627
    Align = alClient
    BevelOuter = bvNone
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object GBRegistro: TGroupBox
      Left = 0
      Top = 0
      Width = 1016
      Height = 420
      Align = alTop
      Caption = ' Registro: '
      TabOrder = 0
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 1012
        Height = 56
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label14: TLabel
          Left = 8
          Top = 8
          Width = 34
          Height = 13
          Caption = 'Banco:'
        end
        object DBEdit02: TDBEdit
          Left = 8
          Top = 24
          Width = 44
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsBancos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdit03: TDBEdit
          Left = 56
          Top = 24
          Width = 721
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsBancos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
      end
      object Panel1: TPanel
        Left = 2
        Top = 71
        Width = 1012
        Height = 264
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label20: TLabel
          Left = 8
          Top = 8
          Width = 42
          Height = 13
          Caption = 'Registro:'
        end
        object Label26: TLabel
          Left = 448
          Top = 116
          Width = 46
          Height = 13
          Caption = 'Posi'#231#245'es:'
        end
        object EdReg: TdmkEditCB
          Left = 8
          Top = 24
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBReg
          IgnoraDBLookupComboBox = False
        end
        object CBReg: TdmkDBLookupComboBox
          Left = 57
          Top = 24
          Width = 436
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsReg
          TabOrder = 1
          dmkEditCB = EdReg
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object GroupBox1: TGroupBox
          Left = 8
          Top = 48
          Width = 245
          Height = 61
          Caption = ' 1'#186' Campo identificador do registro: '
          TabOrder = 2
          object Label15: TLabel
            Left = 8
            Top = 16
            Width = 70
            Height = 13
            Caption = 'Posi'#231#227'o inicial:'
          end
          object Label19: TLabel
            Left = 84
            Top = 16
            Width = 48
            Height = 13
            Caption = 'Tamanho:'
          end
          object Label21: TLabel
            Left = 160
            Top = 16
            Width = 30
            Height = 13
            Caption = 'Texto:'
          end
          object EdSeq1Ini: TdmkEdit
            Left = 8
            Top = 32
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '400'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
          end
          object EdSeq1Tam: TdmkEdit
            Left = 84
            Top = 32
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '400'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
          end
          object EdSeq1Val: TdmkEdit
            Left = 160
            Top = 32
            Width = 72
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object GroupBox2: TGroupBox
          Left = 248
          Top = 48
          Width = 245
          Height = 61
          Caption = ' 2'#186' Campo identificador do registro: '
          TabOrder = 3
          object Label22: TLabel
            Left = 8
            Top = 16
            Width = 70
            Height = 13
            Caption = 'Posi'#231#227'o inicial:'
          end
          object Label23: TLabel
            Left = 84
            Top = 16
            Width = 48
            Height = 13
            Caption = 'Tamanho:'
          end
          object Label24: TLabel
            Left = 160
            Top = 16
            Width = 30
            Height = 13
            Caption = 'Texto:'
          end
          object EdSeq2Ini: TdmkEdit
            Left = 8
            Top = 32
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '400'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
          end
          object EdSeq2Tam: TdmkEdit
            Left = 84
            Top = 32
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '400'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
          end
          object EdSeq2Val: TdmkEdit
            Left = 160
            Top = 32
            Width = 72
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object RGEnvio: TRadioGroup
          Left = 8
          Top = 112
          Width = 437
          Height = 49
          Caption = ' Tipo de envio: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Nenhum'
            'Remessa'
            'Retorno'
            'Ambos')
          TabOrder = 4
        end
        object EdTamLinha: TdmkEdit
          Left = 448
          Top = 132
          Width = 45
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '400'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 400
          ValWarn = False
        end
      end
    end
    object GroupBox12: TGroupBox
      Left = 0
      Top = 569
      Width = 1016
      Height = 58
      Align = alBottom
      TabOrder = 1
      object Panel2: TPanel
        Left = 2
        Top = 8
        Width = 1012
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn1: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn1Click
        end
        object Panel8: TPanel
          Left = 903
          Top = 0
          Width = 109
          Height = 48
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn2: TBitBtn
            Tag = 15
            Left = 8
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn2Click
          end
        end
      end
    end
  end
  object PainelCampo: TPanel
    Left = 0
    Top = 96
    Width = 1016
    Height = 627
    Align = alClient
    BevelOuter = bvNone
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object GBCampo: TGroupBox
      Left = 0
      Top = 0
      Width = 1016
      Height = 460
      Align = alTop
      Caption = ' Campo: '
      TabOrder = 0
      object Panel17: TPanel
        Left = 2
        Top = 15
        Width = 1012
        Height = 443
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label28: TLabel
          Left = 72
          Top = 4
          Width = 81
          Height = 13
          Caption = 'Nome do campo:'
        end
        object Label29: TLabel
          Left = 424
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Registro:'
        end
        object Label30: TLabel
          Left = 16
          Top = 44
          Width = 149
          Height = 13
          Caption = 'Significado do nome do campo:'
        end
        object Label31: TLabel
          Left = 16
          Top = 84
          Width = 70
          Height = 13
          Caption = 'Posi'#231#227'o inicial:'
        end
        object Label32: TLabel
          Left = 92
          Top = 84
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
        end
        object Label33: TLabel
          Left = 168
          Top = 84
          Width = 213
          Height = 13
          Caption = 'Valores pad'#245'es para o campo (alfanum'#233'rico):'
        end
        object Label35: TLabel
          Left = 424
          Top = 44
          Width = 83
          Height = 13
          Caption = 'Banco de origem:'
        end
        object Label36: TLabel
          Left = 16
          Top = 4
          Width = 49
          Height = 13
          Caption = 'ID campo:'
        end
        object Label37: TLabel
          Left = 424
          Top = 84
          Width = 125
          Height = 13
          Caption = 'Funcionalidade do campo:'
        end
        object Label34: TLabel
          Left = 16
          Top = 124
          Width = 41
          Height = 13
          Caption = 'Formato:'
        end
        object Label38: TLabel
          Left = 168
          Top = 124
          Width = 98
          Height = 13
          Caption = 'Ajuda para o campo:'
        end
        object Label39: TLabel
          Left = 92
          Top = 124
          Width = 36
          Height = 13
          Caption = 'CDNF*:'
        end
        object Label40: TLabel
          Left = 164
          Top = 216
          Width = 329
          Height = 13
          Caption = 
            'CDNF* - Casas decimais para n'#250'meros flutuantes (Valores monet'#225'ri' +
            'os)'
        end
        object Label60: TLabel
          Left = 16
          Top = 236
          Width = 129
          Height = 13
          Caption = 'Explica'#231#227'o mais detalhada:'
          Visible = False
        end
        object EdGrupo: TdmkEditCB
          Left = 424
          Top = 20
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBGrupo
          IgnoraDBLookupComboBox = False
        end
        object CBGrupo: TdmkDBLookupComboBox
          Left = 473
          Top = 20
          Width = 303
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsGrupos
          TabOrder = 3
          dmkEditCB = EdGrupo
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdSignific: TdmkEdit
          Left = 16
          Top = 60
          Width = 405
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPadrIni: TdmkEdit
          Left = 16
          Top = 100
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPadrTam: TdmkEdit
          Left = 92
          Top = 100
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPadrVal: TdmkEdit
          Left = 168
          Top = 100
          Width = 125
          Height = 21
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdNomeFld: TdmkEdit
          Left = 72
          Top = 20
          Width = 349
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGAlfaNum: TRadioGroup
          Left = 16
          Top = 164
          Width = 221
          Height = 45
          Caption = ' Tipo de valor: '
          Columns = 2
          Items.Strings = (
            'Alfanum'#233'rico'
            'Apenas num'#233'rico')
          TabOrder = 16
        end
        object EdBcoOrig: TdmkEditCB
          Left = 424
          Top = 60
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBBcoOrig
          IgnoraDBLookupComboBox = False
        end
        object CBBcoOrig: TdmkDBLookupComboBox
          Left = 473
          Top = 60
          Width = 303
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsBancoz
          TabOrder = 6
          dmkEditCB = EdBcoOrig
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object RGEnvioFld: TRadioGroup
          Left = 240
          Top = 164
          Width = 217
          Height = 45
          Caption = ' Tipo de arquivo: '
          Columns = 3
          Items.Strings = (
            'Remessa'
            'Retorno'
            'Ambos')
          TabOrder = 17
        end
        object EdID: TdmkEdit
          Left = 16
          Top = 20
          Width = 53
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 213
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCampo: TdmkEditCB
          Left = 424
          Top = 100
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCampo
          IgnoraDBLookupComboBox = False
        end
        object CBCampo: TdmkDBLookupComboBox
          Left = 473
          Top = 100
          Width = 303
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCNAB_Fld
          TabOrder = 12
          dmkEditCB = EdCampo
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object RGFmtInv: TRadioGroup
          Left = 620
          Top = 164
          Width = 156
          Height = 45
          Caption = ' Inverte lado de alinhamento: '
          Columns = 2
          Items.Strings = (
            'N'#227'o'
            'Sim')
          TabOrder = 19
        end
        object CGPosicoes: TdmkCheckGroup
          Left = 460
          Top = 164
          Width = 156
          Height = 45
          Caption = ' Posi'#231#245'es: '
          Columns = 2
          Items.Strings = (
            '240'
            '400')
          TabOrder = 18
          TabStop = True
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
        object CkContinuar: TCheckBox
          Left = 16
          Top = 216
          Width = 113
          Height = 17
          Caption = 'Continuar inserindo.'
          TabOrder = 20
        end
        object EdSecoVal: TdmkEdit
          Left = 296
          Top = 100
          Width = 125
          Height = 21
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdFormato: TdmkEdit
          Left = 16
          Top = 140
          Width = 72
          Height = 21
          TabOrder = 13
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdAjuda: TdmkEdit
          Left = 168
          Top = 140
          Width = 608
          Height = 21
          TabOrder = 15
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCasas: TdmkEdit
          Left = 92
          Top = 140
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object MeAnexos: TMemo
          Left = 16
          Top = 252
          Width = 761
          Height = 173
          TabOrder = 21
          Visible = False
        end
      end
    end
    object GroupBox11: TGroupBox
      Left = 0
      Top = 569
      Width = 1016
      Height = 58
      Align = alBottom
      TabOrder = 1
      object Panel9: TPanel
        Left = 2
        Top = 8
        Width = 1012
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn3: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn3Click
        end
        object Panel10: TPanel
          Left = 903
          Top = 0
          Width = 109
          Height = 48
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn4: TBitBtn
            Tag = 15
            Left = 8
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn2Click
          end
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1016
    Height = 627
    Align = alClient
    BevelOuter = bvNone
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1016
      Height = 361
      Align = alTop
      Caption = ' Cadastro do Banco: '
      TabOrder = 0
      object Pn0001: TPanel
        Left = 2
        Top = 15
        Width = 1012
        Height = 344
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 16
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label10: TLabel
          Left = 64
          Top = 8
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label3: TLabel
          Left = 16
          Top = 48
          Width = 74
          Height = 13
          Caption = 'Site na internet:'
        end
        object Label5: TLabel
          Left = 440
          Top = 48
          Width = 32
          Height = 13
          Caption = 'DVCC:'
        end
        object Label6: TLabel
          Left = 16
          Top = 304
          Width = 216
          Height = 13
          Caption = 'DVB = D'#237'gito verificador do c'#243'digo do banco.'
        end
        object Label8: TLabel
          Left = 404
          Top = 48
          Width = 25
          Height = 13
          Caption = 'DVB:'
        end
        object Label11: TLabel
          Left = 16
          Top = 288
          Width = 305
          Height = 13
          Caption = 
            'DVCC = D'#237'gitos v'#225'lidos para conta corrente (de tr'#225's para frente)' +
            '.'
        end
        object Label13: TLabel
          Left = 16
          Top = 88
          Width = 252
          Height = 13
          Caption = 'Entidade banc'#225'ria: (utilizado na concilia'#231#227'o banc'#225'ria)'
        end
        object Label61: TLabel
          Left = 16
          Top = 208
          Width = 256
          Height = 13
          Caption = 'Descri'#231#227'o da cobran'#231'a sem registro nos lan'#231'amentos:'
        end
        object Label64: TLabel
          Left = 16
          Top = 248
          Width = 223
          Height = 13
          Caption = 'Diret'#243'rio dos arquivos de retorno multi empresa:'
        end
        object SbCNABDirMul: TSpeedButton
          Left = 448
          Top = 264
          Width = 23
          Height = 22
          Caption = '...'
          OnClick = SbCNABDirMulClick
        end
        object SBEntidade: TSpeedButton
          Left = 448
          Top = 104
          Width = 23
          Height = 22
          Caption = '...'
          OnClick = SBEntidadeClick
        end
        object EdCodigo: TdmkEdit
          Left = 16
          Top = 24
          Width = 44
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome: TdmkEdit
          Left = 64
          Top = 24
          Width = 409
          Height = 21
          MaxLength = 255
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdSite: TdmkEdit
          Left = 16
          Top = 64
          Width = 385
          Height = 21
          MaxLength = 255
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdDVCC: TdmkEdit
          Left = 440
          Top = 64
          Width = 32
          Height = 21
          Alignment = taRightJustify
          MaxLength = 255
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '1'
          ValMax = '10'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '10'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 10
          ValWarn = False
        end
        object EdDVB: TdmkEdit
          Left = 404
          Top = 64
          Width = 32
          Height = 21
          Alignment = taRightJustify
          MaxLength = 1
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '?'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '?'
          ValWarn = False
        end
        object EdEntidade: TdmkEditCB
          Left = 16
          Top = 104
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEntidade
          IgnoraDBLookupComboBox = False
        end
        object CBEntidade: TdmkDBLookupComboBox
          Left = 84
          Top = 104
          Width = 361
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENTI'
          ListSource = DsEntidade
          TabOrder = 6
          dmkEditCB = EdEntidade
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object GroupBox3: TGroupBox
          Left = 16
          Top = 128
          Width = 457
          Height = 77
          Caption = ' Identificador de cobran'#231'as no arquivo CNAB: '
          TabOrder = 7
          object GroupBox4: TGroupBox
            Left = 2
            Top = 15
            Width = 453
            Height = 60
            Align = alClient
            Caption = ' CNAB 400: '
            TabOrder = 0
            object Label41: TLabel
              Left = 12
              Top = 16
              Width = 30
              Height = 13
              Caption = 'In'#237'cio:'
            end
            object Label16: TLabel
              Left = 72
              Top = 16
              Width = 48
              Height = 13
              Caption = 'Tamanho:'
            end
            object EdID_400i: TdmkEdit
              Left = 12
              Top = 32
              Width = 57
              Height = 21
              Alignment = taRightJustify
              MaxLength = 255
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdID_400t: TdmkEdit
              Left = 72
              Top = 32
              Width = 57
              Height = 21
              Alignment = taRightJustify
              MaxLength = 255
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
        object GroupBox7: TGroupBox
          Left = 476
          Top = 8
          Width = 529
          Height = 165
          Caption = ' Configura'#231#227'o de concilia'#231#227'o banc'#225'ria por arquivos Excel: '
          TabOrder = 8
          object Label43: TLabel
            Left = 12
            Top = 20
            Width = 58
            Height = 13
            Caption = 'Linha inicial:'
          end
          object Label44: TLabel
            Left = 12
            Top = 44
            Width = 60
            Height = 13
            Caption = 'Coluna data:'
          end
          object Label45: TLabel
            Left = 12
            Top = 68
            Width = 92
            Height = 13
            Caption = 'Coluna s'#243' hist'#243'rico:'
          end
          object Label46: TLabel
            Left = 12
            Top = 92
            Width = 106
            Height = 13
            Caption = 'Coluna s'#243' documento:'
          end
          object Label47: TLabel
            Left = 184
            Top = 44
            Width = 85
            Height = 13
            Caption = 'Coluna s'#243' cr'#233'dito:'
          end
          object Label48: TLabel
            Left = 184
            Top = 68
            Width = 82
            Height = 13
            Caption = 'Coluna s'#243' d'#233'bito:'
          end
          object Label49: TLabel
            Left = 184
            Top = 116
            Width = 92
            Height = 13
            Caption = 'Coluna "D" ou "C":'
          end
          object Label50: TLabel
            Left = 184
            Top = 140
            Width = 64
            Height = 13
            Caption = 'Coluna saldo:'
          end
          object Label51: TLabel
            Left = 12
            Top = 116
            Width = 102
            Height = 13
            Caption = 'Coluna hist + docum.:'
          end
          object Label52: TLabel
            Left = 184
            Top = 92
            Width = 99
            Height = 13
            Caption = 'Coluna cr'#233'd ou d'#233'b.:'
          end
          object Label53: TLabel
            Left = 12
            Top = 140
            Width = 104
            Height = 13
            Caption = 'Coluna '#39'a compensar'#39':'
          end
          object Label54: TLabel
            Left = 184
            Top = 20
            Width = 72
            Height = 13
            Caption = 'Coluna '#39'CPMF'#39':'
          end
          object EdXlsLinha: TdmkEdit
            Left = 124
            Top = 16
            Width = 33
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
          end
          object EdXLSData: TdmkEdit
            Left = 124
            Top = 40
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSDataExit
          end
          object EdXLSHist: TdmkEdit
            Left = 124
            Top = 64
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSHistExit
          end
          object EdXLSDocu: TdmkEdit
            Left = 124
            Top = 88
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSDocuExit
          end
          object EdXLSCred: TdmkEdit
            Left = 292
            Top = 40
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSCredExit
          end
          object EdXLSDebi: TdmkEdit
            Left = 292
            Top = 64
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSDebiExit
          end
          object EdXLSDouC: TdmkEdit
            Left = 292
            Top = 112
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 10
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSDouCExit
          end
          object EdXLSSldo: TdmkEdit
            Left = 292
            Top = 136
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSSldoExit
          end
          object EdXLSHiDo: TdmkEdit
            Left = 124
            Top = 112
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSHiDoExit
          end
          object EdXLSCrDb: TdmkEdit
            Left = 292
            Top = 88
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSCrDbExit
          end
          object RGXLSTCDB: TRadioGroup
            Left = 352
            Top = 16
            Width = 165
            Height = 117
            Caption = ' Tipo de contabilidade: '
            ItemIndex = 0
            Items.Strings = (
              'Irrelevante')
            TabOrder = 12
          end
          object EdXLSComp: TdmkEdit
            Left = 124
            Top = 136
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSCompExit
          end
          object EdXLSCPMF: TdmkEdit
            Left = 292
            Top = 16
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSCPMFExit
          end
        end
        object EdDescriCNR: TdmkEdit
          Left = 16
          Top = 224
          Width = 457
          Height = 21
          MaxLength = 255
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCNABDirMul: TdmkEdit
          Left = 16
          Top = 264
          Width = 429
          Height = 21
          MaxLength = 255
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object GroupBox8: TGroupBox
          Left = 476
          Top = 176
          Width = 529
          Height = 165
          Caption = ' Configura'#231#227'o de concilia'#231#227'o de liquida'#231#227'o de cheques: '
          TabOrder = 11
          object Label65: TLabel
            Left = 12
            Top = 20
            Width = 58
            Height = 13
            Caption = 'Linha inicial:'
          end
          object Label66: TLabel
            Left = 12
            Top = 44
            Width = 103
            Height = 13
            Caption = 'Coluna data cust'#243'dia:'
          end
          object Label67: TLabel
            Left = 12
            Top = 68
            Width = 85
            Height = 13
            Caption = 'Coluna n/n'#250'mero:'
          end
          object Label68: TLabel
            Left = 12
            Top = 92
            Width = 82
            Height = 13
            Caption = 'Coluna n'#186' banco:'
          end
          object Label69: TLabel
            Left = 12
            Top = 116
            Width = 90
            Height = 13
            Caption = 'Coluna n'#186' ag'#234'ncia:'
          end
          object Label70: TLabel
            Left = 12
            Top = 140
            Width = 79
            Height = 13
            Caption = 'Coluna n'#186' conta:'
          end
          object Label71: TLabel
            Left = 184
            Top = 20
            Width = 88
            Height = 13
            Caption = 'Coluna n'#186' cheque:'
          end
          object Label72: TLabel
            Left = 184
            Top = 44
            Width = 62
            Height = 13
            Caption = 'Coluna valor:'
          end
          object Label73: TLabel
            Left = 184
            Top = 68
            Width = 91
            Height = 13
            Caption = 'Coluna CNPJ/CPF:'
          end
          object Label74: TLabel
            Left = 184
            Top = 92
            Width = 79
            Height = 13
            Caption = 'Coluna situa'#231#227'o:'
          end
          object Label75: TLabel
            Left = 184
            Top = 116
            Width = 96
            Height = 13
            Caption = 'Coluna data evento:'
          end
          object Label76: TLabel
            Left = 184
            Top = 140
            Width = 75
            Height = 13
            Caption = 'Coluna border'#244':'
          end
          object Label77: TLabel
            Left = 336
            Top = 24
            Width = 102
            Height = 13
            Caption = 'Codifica'#231#227'o dep'#243'sito:'
          end
          object Label78: TLabel
            Left = 336
            Top = 72
            Width = 106
            Height = 13
            Caption = 'C'#243'difica'#231#227'o devul'#231#227'o:'
          end
          object EdCChLinha: TdmkEdit
            Left = 124
            Top = 16
            Width = 33
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
          end
          object EdCChDtaCust: TdmkEdit
            Left = 124
            Top = 40
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSDataExit
          end
          object EdCChNosNum: TdmkEdit
            Left = 124
            Top = 64
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSHistExit
          end
          object EdCChBanco: TdmkEdit
            Left = 124
            Top = 88
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSDocuExit
          end
          object EdCChAgencia: TdmkEdit
            Left = 124
            Top = 112
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSHiDoExit
          end
          object EdCChConta: TdmkEdit
            Left = 124
            Top = 136
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSCompExit
          end
          object EdCChCheque: TdmkEdit
            Left = 292
            Top = 16
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSCPMFExit
          end
          object EdCChValor: TdmkEdit
            Left = 292
            Top = 40
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSCredExit
          end
          object EdCChCNPJCPF: TdmkEdit
            Left = 292
            Top = 64
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSDebiExit
          end
          object EdCChCodSit: TdmkEdit
            Left = 292
            Top = 88
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSCrDbExit
          end
          object EdCChDtaEven: TdmkEdit
            Left = 292
            Top = 112
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 10
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSDouCExit
          end
          object EdCChBordero: TdmkEdit
            Left = 292
            Top = 136
            Width = 33
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'A'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'A'
            ValWarn = False
            OnExit = EdXLSSldoExit
          end
          object EdCChTxtDep: TdmkEdit
            Left = 336
            Top = 40
            Width = 184
            Height = 21
            MaxLength = 255
            TabOrder = 12
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCChTxtDev: TdmkEdit
            Left = 336
            Top = 88
            Width = 184
            Height = 21
            MaxLength = 255
            TabOrder = 13
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
    end
    object GroupBox10: TGroupBox
      Left = 0
      Top = 569
      Width = 1016
      Height = 58
      Align = alBottom
      TabOrder = 1
      object PainelConfirma: TPanel
        Left = 2
        Top = 8
        Width = 1012
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
        object Panel7: TPanel
          Left = 903
          Top = 0
          Width = 109
          Height = 48
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 8
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GB_R: TGroupBox
      Left = 968
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 752
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 248
        Height = 32
        Caption = 'Cadastro de Bancos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 248
        Height = 32
        Caption = 'Cadastro de Bancos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 248
        Height = 32
        Caption = 'Cadastro de Bancos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1016
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 5
    object Panel24: TPanel
      Left = 2
      Top = 15
      Width = 1012
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 520
    Top = 13
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrBancosBeforeOpen
    AfterOpen = QrBancosAfterOpen
    BeforeClose = QrBancosBeforeClose
    AfterScroll = QrBancosAfterScroll
    SQL.Strings = (
      'SELECT ban.*, CASE WHEN ent.Tipo=0 '
      'THEN ent.RazaoSocial ELSE ent.Nome END NOMEENTIDADE'
      'FROM bancos ban'
      'LEFT JOIN entidades ent ON ent.Codigo=ban.Entidade'
      'WHERE ban.Codigo > 0')
    Left = 492
    Top = 13
    object QrBancosLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'bancos.Lk'
    end
    object QrBancosDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'bancos.DataCad'
    end
    object QrBancosDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'bancos.DataAlt'
    end
    object QrBancosUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'bancos.UserCad'
    end
    object QrBancosUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'bancos.UserAlt'
    end
    object QrBancosCodigo: TSmallintField
      FieldName = 'Codigo'
      Origin = 'bancos.Codigo'
      DisplayFormat = '000'
    end
    object QrBancosSite: TWideStringField
      FieldName = 'Site'
      Origin = 'bancos.Site'
      Size = 255
    end
    object QrBancosNome: TWideStringField
      DisplayWidth = 100
      FieldName = 'Nome'
      Origin = 'bancos.Nome'
      Size = 100
    end
    object QrBancosDVCC: TSmallintField
      FieldName = 'DVCC'
      Origin = 'bancos.DVCC'
    end
    object QrBancosDVB: TWideStringField
      FieldName = 'DVB'
      Origin = 'bancos.DVB'
      Size = 1
    end
    object QrBancosEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'bancos.Entidade'
    end
    object QrBancosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrBancosID_400i: TIntegerField
      FieldName = 'ID_400i'
      Origin = 'bancos.ID_400i'
      Required = True
    end
    object QrBancosID_400t: TIntegerField
      FieldName = 'ID_400t'
      Origin = 'bancos.ID_400t'
      Required = True
    end
    object QrBancosXlsLinha: TIntegerField
      FieldName = 'XlsLinha'
      Origin = 'bancos.XlsLinha'
      Required = True
    end
    object QrBancosXlsData: TWideStringField
      FieldName = 'XlsData'
      Origin = 'bancos.XlsData'
      Size = 2
    end
    object QrBancosXlsHist: TWideStringField
      FieldName = 'XlsHist'
      Origin = 'bancos.XlsHist'
      Size = 2
    end
    object QrBancosXlsDocu: TWideStringField
      FieldName = 'XlsDocu'
      Origin = 'bancos.XlsDocu'
      Size = 2
    end
    object QrBancosXlsCred: TWideStringField
      FieldName = 'XlsCred'
      Origin = 'bancos.XlsCred'
      Size = 2
    end
    object QrBancosXlsDebi: TWideStringField
      FieldName = 'XlsDebi'
      Origin = 'bancos.XlsDebi'
      Size = 2
    end
    object QrBancosXlsTCDB: TSmallintField
      FieldName = 'XlsTCDB'
      Origin = 'bancos.XlsTCDB'
      Required = True
    end
    object QrBancosXlsComp: TWideStringField
      FieldName = 'XlsComp'
      Origin = 'bancos.XlsComp'
      Size = 2
    end
    object QrBancosXlsCPMF: TWideStringField
      FieldName = 'XlsCPMF'
      Origin = 'bancos.XlsCPMF'
      Size = 2
    end
    object QrBancosXlsSldo: TWideStringField
      FieldName = 'XlsSldo'
      Origin = 'bancos.XlsSldo'
      Size = 2
    end
    object QrBancosXlsHiDo: TWideStringField
      FieldName = 'XlsHiDo'
      Origin = 'bancos.XlsHiDo'
      Size = 2
    end
    object QrBancosXlsCrDb: TWideStringField
      FieldName = 'XlsCrDb'
      Origin = 'bancos.XlsCrDb'
      Size = 2
    end
    object QrBancosXlsDouC: TWideStringField
      FieldName = 'XlsDouC'
      Origin = 'bancos.XlsDouC'
      Size = 2
    end
    object QrBancosDescriCNR: TWideStringField
      FieldName = 'DescriCNR'
      Required = True
      Size = 100
    end
    object QrBancosCNABDirMul: TWideStringField
      FieldName = 'CNABDirMul'
      Size = 255
    end
    object QrBancosCChLinha: TIntegerField
      FieldName = 'CChLinha'
    end
    object QrBancosCChDtaCust: TWideStringField
      FieldName = 'CChDtaCust'
      Size = 2
    end
    object QrBancosCChNosNum: TWideStringField
      FieldName = 'CChNosNum'
      Size = 2
    end
    object QrBancosCChBanco: TWideStringField
      FieldName = 'CChBanco'
      Size = 2
    end
    object QrBancosCChAgencia: TWideStringField
      FieldName = 'CChAgencia'
      Size = 2
    end
    object QrBancosCChConta: TWideStringField
      FieldName = 'CChConta'
      Size = 2
    end
    object QrBancosCChCheque: TWideStringField
      FieldName = 'CChCheque'
      Size = 2
    end
    object QrBancosCChValor: TWideStringField
      FieldName = 'CChValor'
      Size = 2
    end
    object QrBancosCChCNPJCPF: TWideStringField
      FieldName = 'CChCNPJCPF'
      Size = 2
    end
    object QrBancosCChCodSit: TWideStringField
      FieldName = 'CChCodSit'
      Size = 2
    end
    object QrBancosCChDtaEven: TWideStringField
      FieldName = 'CChDtaEven'
      Size = 2
    end
    object QrBancosCChBordero: TWideStringField
      FieldName = 'CChBordero'
      Size = 2
    end
    object QrBancosCChTxtDev: TWideStringField
      FieldName = 'CChTxtDev'
      Size = 30
    end
    object QrBancosCChTxtDep: TWideStringField
      FieldName = 'CChTxtDep'
      Size = 30
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.txt'
    Filter = 'Arquivos Texto|*.txt'
    Left = 660
    Top = 424
  end
  object PMOcorrencia: TPopupMenu
    Left = 320
    Top = 12
    object Novaocorrncia1: TMenuItem
      Caption = '&Nova ocorr'#234'ncia'
      OnClick = Novaocorrncia1Click
    end
    object Alteraocorrnciaatual1: TMenuItem
      Caption = '&Altera ocorr'#234'ncia atual'
      OnClick = Alteraocorrnciaatual1Click
    end
    object Excluiocorrnciaatual1: TMenuItem
      Caption = '&Exclui ocorr'#234'ncia atual'
      OnClick = Excluiocorrnciaatual1Click
    end
  end
  object QrEntidade: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'ORDER BY NOMEENTI')
    Left = 600
    Top = 426
    object QrEntidadeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadeNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsEntidade: TDataSource
    DataSet = QrEntidade
    Left = 628
    Top = 426
  end
  object PMBanco: TPopupMenu
    Left = 292
    Top = 12
    object Novobanco1: TMenuItem
      Caption = '&Novo banco'
      OnClick = Novobanco1Click
    end
    object Alterabancoatual1: TMenuItem
      Caption = '&Altera banco atual'
      OnClick = Alterabancoatual1Click
    end
    object Excluibancoatual1: TMenuItem
      Caption = '&Exclui banco atual'
      OnClick = Excluibancoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Importadelista1: TMenuItem
      Caption = 'Importa de lista'
      OnClick = Importadelista1Click
    end
  end
  object QrBanCNAB_R: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrBanCNAB_RBeforeClose
    AfterScroll = QrBanCNAB_RAfterScroll
    SQL.Strings = (
      'SELECT cag.Nome NOMEREGISTRO, bcr.* '
      'FROM bancnab_r bcr'
      'LEFT JOIN cnab_cag cag ON cag.Codigo=bcr.Registro'
      'WHERE bcr.Codigo=:P0'
      'AND Envio in (:P1,:P2)'
      'AND TamLinha=:P3')
    Left = 620
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrBanCNAB_RNOMEREGISTRO: TWideStringField
      FieldName = 'NOMEREGISTRO'
      Origin = 'cnab400cag.Nome'
      Size = 50
    end
    object QrBanCNAB_RCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'bancnab400r.Codigo'
      Required = True
    end
    object QrBanCNAB_RRegistro: TIntegerField
      FieldName = 'Registro'
      Origin = 'bancnab400r.Registro'
      Required = True
    end
    object QrBanCNAB_RLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'bancnab400r.Lk'
    end
    object QrBanCNAB_RDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'bancnab400r.DataCad'
    end
    object QrBanCNAB_RDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'bancnab400r.DataAlt'
    end
    object QrBanCNAB_RUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'bancnab400r.UserCad'
    end
    object QrBanCNAB_RUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'bancnab400r.UserAlt'
    end
    object QrBanCNAB_RSeq1Ini: TIntegerField
      FieldName = 'Seq1Ini'
      Origin = 'bancnab400r.Seq1Ini'
      Required = True
      DisplayFormat = '000;-000; '
    end
    object QrBanCNAB_RSeq1Tam: TIntegerField
      FieldName = 'Seq1Tam'
      Origin = 'bancnab400r.Seq1Tam'
      Required = True
      DisplayFormat = '000;-000; '
    end
    object QrBanCNAB_RSeq1Val: TWideStringField
      FieldName = 'Seq1Val'
      Origin = 'bancnab400r.Seq1Val'
      Size = 50
    end
    object QrBanCNAB_RSeq2Ini: TIntegerField
      FieldName = 'Seq2Ini'
      Origin = 'bancnab400r.Seq2Ini'
      Required = True
      DisplayFormat = '000;-000; '
    end
    object QrBanCNAB_RSeq2Tam: TIntegerField
      FieldName = 'Seq2Tam'
      Origin = 'bancnab400r.Seq2Tam'
      Required = True
      DisplayFormat = '000;-000; '
    end
    object QrBanCNAB_RSeq2Val: TWideStringField
      FieldName = 'Seq2Val'
      Origin = 'bancnab400r.Seq2Val'
      Size = 50
    end
    object QrBanCNAB_REnvio: TSmallintField
      FieldName = 'Envio'
      Origin = 'bancnab400r.Envio'
      Required = True
    end
    object QrBanCNAB_RTamLinha: TIntegerField
      FieldName = 'TamLinha'
      Required = True
    end
  end
  object DsBanCNAB_R: TDataSource
    DataSet = QrBanCNAB_R
    Left = 648
    Top = 12
  end
  object PMComposicao: TPopupMenu
    Left = 349
    Top = 12
    object Registros1: TMenuItem
      Caption = '&Registros'
      object Incluinovoregistro1: TMenuItem
        Caption = '&Inclui novo registro'
        OnClick = Incluinovoregistro1Click
      end
      object Alteraregistroatual1: TMenuItem
        Caption = '&Altera registro atual'
        OnClick = Alteraregistroatual1Click
      end
      object Excluiregistroatual1: TMenuItem
        Caption = '&Exclui registro atual'
        Enabled = False
      end
    end
    object Campos1: TMenuItem
      Caption = '&Campos'
      object Incluinovocampo1: TMenuItem
        Caption = '&Inclui novo campo'
        OnClick = Incluinovocampo1Click
      end
      object Alteracampoatual1: TMenuItem
        Caption = '&Altera campo atual'
        OnClick = Alteracampoatual1Click
      end
      object ExcluicampoAtual1: TMenuItem
        Caption = '&Exclui campo Atual'
      end
    end
  end
  object QrReg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cag.Codigo, cag.Nome'
      'FROM cnab_cag cag'
      'WHERE cag.Codigo NOT IN ('
      '  SELECT Registro'
      '  FROM bancnab_r'
      '  WHERE Codigo=:P0)'
      'ORDER BY cag.Nome')
    Left = 540
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRegCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRegNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsReg: TDataSource
    DataSet = QrReg
    Left = 568
    Top = 424
  end
  object QrCNAB_CaD: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCNAB_CaDCalcFields
    SQL.Strings = (
      'SELECT fld.Nome NOMECAMPO, cad.* '
      'FROM cnab_cad cad'
      'LEFT JOIN cnab_cag cag ON cag.Codigo=cad.Registro'
      'LEFT JOIN cnab_fld fld ON fld.Codigo=cad.Campo'
      'WHERE cad.BcoOrig =:P0'
      'AND cad.Registro=:P1'
      'AND cad.Envio=:P2'
      'ORDER BY PadrIni, ID, Nome')
    Left = 692
    Top = 421
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCNAB_CaDPOSICOES: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'POSICOES'
      Calculated = True
    end
    object QrCNAB_CaDNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab400cad.Nome'
      Required = True
      Size = 50
    end
    object QrCNAB_CaDSignific: TWideStringField
      FieldName = 'Signific'
      Origin = 'cnab400cad.Signific'
      Size = 255
    end
    object QrCNAB_CaDPadrIni: TIntegerField
      FieldName = 'PadrIni'
      Origin = 'cnab400cad.PadrIni'
      Required = True
    end
    object QrCNAB_CaDPadrTam: TIntegerField
      FieldName = 'PadrTam'
      Origin = 'cnab400cad.PadrTam'
      Required = True
    end
    object QrCNAB_CaDAlfaNum: TSmallintField
      FieldName = 'AlfaNum'
      Origin = 'cnab400cad.AlfaNum'
      Required = True
    end
    object QrCNAB_CaDFmtInv: TIntegerField
      FieldName = 'FmtInv'
      Origin = 'cnab400cad.FmtInv'
      Required = True
    end
    object QrCNAB_CaDPadrVal: TWideStringField
      FieldName = 'PadrVal'
      Origin = 'cnab400cad.PadrVal'
      Size = 255
    end
    object QrCNAB_CaDAjuda: TWideStringField
      FieldName = 'Ajuda'
      Origin = 'cnab400cad.Ajuda'
      Size = 255
    end
    object QrCNAB_CaDLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cnab400cad.Lk'
    end
    object QrCNAB_CaDDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cnab400cad.DataCad'
    end
    object QrCNAB_CaDDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cnab400cad.DataAlt'
    end
    object QrCNAB_CaDUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cnab400cad.UserCad'
    end
    object QrCNAB_CaDUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cnab400cad.UserAlt'
    end
    object QrCNAB_CaDRegistro: TIntegerField
      FieldName = 'Registro'
      Origin = 'cnab400cad.Registro'
      Required = True
    end
    object QrCNAB_CaDBcoOrig: TIntegerField
      FieldName = 'BcoOrig'
      Origin = 'cnab400cad.BcoOrig'
      Required = True
    end
    object QrCNAB_CaDNOME_T: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_T'
      Size = 1
      Calculated = True
    end
    object QrCNAB_CaDNOME_I: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_I'
      Size = 1
      Calculated = True
    end
    object QrCNAB_CaDEnvio: TSmallintField
      FieldName = 'Envio'
      Origin = 'cnab400cad.Envio'
      Required = True
    end
    object QrCNAB_CaDID: TWideStringField
      FieldName = 'ID'
      Origin = 'cnab400cad.ID'
      Required = True
    end
    object QrCNAB_CaDCampo: TIntegerField
      FieldName = 'Campo'
      Origin = 'cnab400cad.Campo'
      Required = True
    end
    object QrCNAB_CaDT240: TIntegerField
      FieldName = 'T240'
      Origin = 'cnab400cad.T240'
      Required = True
    end
    object QrCNAB_CaDT400: TIntegerField
      FieldName = 'T400'
      Origin = 'cnab400cad.T400'
      Required = True
    end
    object QrCNAB_CaDSecoVal: TWideStringField
      FieldName = 'SecoVal'
      Origin = 'cnab400cad.SecoVal'
      Size = 255
    end
    object QrCNAB_CaDNOMECAMPO: TWideStringField
      FieldName = 'NOMECAMPO'
      Size = 100
    end
    object QrCNAB_CaDFormato: TWideStringField
      FieldName = 'Formato'
    end
    object QrCNAB_CaDCasas: TSmallintField
      FieldName = 'Casas'
      Required = True
    end
  end
  object DsCNAB_CaD: TDataSource
    DataSet = QrCNAB_CaD
    Left = 720
    Top = 421
  end
  object OpenDialog2: TOpenDialog
    Left = 813
    Top = 61
  end
  object QrGrupos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cnab_cag'
      'ORDER BY Nome')
    Left = 741
    Top = 13
    object QrGruposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruposNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 768
    Top = 13
  end
  object QrBancoz: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bancos'
      'ORDER BY Nome')
    Left = 800
    Top = 12
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBancoz: TDataSource
    DataSet = QrBancoz
    Left = 828
    Top = 12
  end
  object QrCNAB_Fld: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cnab_fld'
      'ORDER BY Nome')
    Left = 860
    Top = 12
    object QrCNAB_FldCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_FldNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCNAB_Fld: TDataSource
    DataSet = QrCNAB_Fld
    Left = 888
    Top = 12
  end
  object OpenDialog3: TOpenDialog
    Left = 289
    Top = 197
  end
  object OpenDialog4: TOpenDialog
    Left = 420
    Top = 252
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM bancos '
      'WHERE Codigo=:P0')
    Left = 752
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 632
    Top = 240
  end
  object OpenDialog5: TOpenDialog
    Filter = 'Arquivos excel (*.xls)|*.xls'
    Left = 192
    Top = 252
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 260
    Top = 12
  end
end
