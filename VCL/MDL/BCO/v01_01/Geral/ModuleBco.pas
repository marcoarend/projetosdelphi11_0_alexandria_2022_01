unit ModuleBco;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, dmkGeral, UnDmkEnums, UnInternalConsts, UnDmkProcFunc;

type
  TDmBco = class(TDataModule)
    QrCNAB_Cfg: TmySQLQuery;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    QrCNAB_CfgCedBanco: TIntegerField;
    QrCNAB_CfgCedAgencia: TIntegerField;
    QrCNAB_CfgCedConta: TWideStringField;
    QrCNAB_CfgCedNome: TWideStringField;
    QrCNAB_CfgTipoCart: TSmallintField;
    QrCNAB_CfgCartNum: TWideStringField;
    QrCNAB_CfgCartCod: TWideStringField;
    QrCNAB_CfgEspecieTit: TWideStringField;
    QrCNAB_CfgAceiteTit: TSmallintField;
    QrCNAB_CfgInstrCobr1: TWideStringField;
    QrCNAB_CfgInstrCobr2: TWideStringField;
    QrCNAB_CfgInstrDias: TSmallintField;
    QrCNAB_CfgSacadAvali: TIntegerField;
    QrCNAB_CfgSacAvaNome: TWideStringField;
    QrAddr1: TmySQLQuery;
    QrAddr1Codigo: TIntegerField;
    QrAddr1Cadastro: TDateField;
    QrAddr1CNPJ_CPF: TWideStringField;
    QrAddr1IE_RG: TWideStringField;
    QrAddr1NIRE_: TWideStringField;
    QrAddr1RUA: TWideStringField;
    QrAddr1COMPL: TWideStringField;
    QrAddr1BAIRRO: TWideStringField;
    QrAddr1CIDADE: TWideStringField;
    QrAddr1NOMELOGRAD: TWideStringField;
    QrAddr1NOMEUF: TWideStringField;
    QrAddr1Pais: TWideStringField;
    QrAddr1Tipo: TSmallintField;
    QrAddr1TE1: TWideStringField;
    QrAddr1FAX: TWideStringField;
    QrAddr1ENatal: TDateField;
    QrAddr1PNatal: TDateField;
    QrAddr1Respons1: TWideStringField;
    QrAddr1ECEP_TXT: TWideStringField;
    QrAddr1NUMERO_TXT: TWideStringField;
    QrAddr1E_ALL: TWideStringField;
    QrAddr1CNPJ_TXT: TWideStringField;
    QrAddr1FAX_TXT: TWideStringField;
    QrAddr1TE1_TXT: TWideStringField;
    QrAddr1NATAL_TXT: TWideStringField;
    QrAddr2: TmySQLQuery;
    QrAddr2Codigo: TIntegerField;
    QrAddr2Cadastro: TDateField;
    QrAddr2CNPJ_CPF: TWideStringField;
    QrAddr2IE_RG: TWideStringField;
    QrAddr2NIRE_: TWideStringField;
    QrAddr2RUA: TWideStringField;
    QrAddr2COMPL: TWideStringField;
    QrAddr2BAIRRO: TWideStringField;
    QrAddr2CIDADE: TWideStringField;
    QrAddr2NOMELOGRAD: TWideStringField;
    QrAddr2NOMEUF: TWideStringField;
    QrAddr2Pais: TWideStringField;
    QrAddr2Tipo: TSmallintField;
    QrAddr2TE1: TWideStringField;
    QrAddr2FAX: TWideStringField;
    QrAddr2ENatal: TDateField;
    QrAddr2PNatal: TDateField;
    QrAddr2Respons1: TWideStringField;
    QrAddr2ECEP_TXT: TWideStringField;
    QrAddr2NUMERO_TXT: TWideStringField;
    QrAddr2E_ALL: TWideStringField;
    QrAddr2CNPJ_TXT: TWideStringField;
    QrAddr2FAX_TXT: TWideStringField;
    QrAddr2TE1_TXT: TWideStringField;
    QrAddr2NATAL_TXT: TWideStringField;
    QrAddr3: TmySQLQuery;
    QrAddr3Codigo: TIntegerField;
    QrAddr3Cadastro: TDateField;
    QrAddr3CNPJ_CPF: TWideStringField;
    QrAddr3IE_RG: TWideStringField;
    QrAddr3NIRE_: TWideStringField;
    QrAddr3RUA: TWideStringField;
    QrAddr3COMPL: TWideStringField;
    QrAddr3BAIRRO: TWideStringField;
    QrAddr3CIDADE: TWideStringField;
    QrAddr3NOMELOGRAD: TWideStringField;
    QrAddr3NOMEUF: TWideStringField;
    QrAddr3Pais: TWideStringField;
    QrAddr3Tipo: TSmallintField;
    QrAddr3TE1: TWideStringField;
    QrAddr3FAX: TWideStringField;
    QrAddr3ENatal: TDateField;
    QrAddr3PNatal: TDateField;
    QrAddr3Respons1: TWideStringField;
    QrAddr3ECEP_TXT: TWideStringField;
    QrAddr3NUMERO_TXT: TWideStringField;
    QrAddr3E_ALL: TWideStringField;
    QrAddr3CNPJ_TXT: TWideStringField;
    QrAddr3FAX_TXT: TWideStringField;
    QrAddr3TE1_TXT: TWideStringField;
    QrAddr3NATAL_TXT: TWideStringField;
    QrCNAB_CfgLk: TIntegerField;
    QrCNAB_CfgDataCad: TDateField;
    QrCNAB_CfgDataAlt: TDateField;
    QrCNAB_CfgUserCad: TIntegerField;
    QrCNAB_CfgUserAlt: TIntegerField;
    QrCNAB_CfgAlterWeb: TSmallintField;
    QrCNAB_CfgAtivo: TSmallintField;
    QrCount: TmySQLQuery;
    QrCountItens: TLargeintField;
    QrAddr1NOMEENT: TWideStringField;
    QrAddr2NOMEENT: TWideStringField;
    QrAddr3NOMEENT: TWideStringField;
    QrBco: TmySQLQuery;
    QrBcoCodigo: TIntegerField;
    QrBcoNome: TWideStringField;
    QrCNAB_CfgCodEmprBco: TWideStringField;
    QrCNAB_CfgMultaTipo: TSmallintField;
    QrCNAB_CfgMultaPerc: TFloatField;
    QrCNAB_CfgTexto01: TWideStringField;
    QrCNAB_CfgTexto02: TWideStringField;
    QrCNAB_CfgTexto03: TWideStringField;
    QrCNAB_CfgTexto04: TWideStringField;
    QrCNAB_CfgTexto05: TWideStringField;
    QrCNAB_CfgTexto06: TWideStringField;
    QrCNAB_CfgTexto07: TWideStringField;
    QrCNAB_CfgTexto08: TWideStringField;
    QrCNAB_CfgTexto09: TWideStringField;
    QrCNAB_CfgTexto10: TWideStringField;
    QrCNAB_CfgJurosPerc: TFloatField;
    QrCNAB_CfgJurosDias: TSmallintField;
    QrCNAB_CfgJurosTipo: TSmallintField;
    QrAddr3EMail: TWideStringField;
    QrCNAB_CfgCNAB: TIntegerField;
    QrCNAB_CfgCedDAC_A: TWideStringField;
    QrCNAB_CfgCedDAC_C: TWideStringField;
    QrCNAB_CfgCedDAC_AC: TWideStringField;
    QrCNAB_CfgEnvEmeio: TSmallintField;
    QrCNPJ: TmySQLQuery;
    QrCNPJCodigo: TIntegerField;
    QrCNAB_CfgDiretorio: TWideStringField;
    QrCNAB_Cfg_237Mens1: TWideStringField;
    QrCNAB_Cfg_237Mens2: TWideStringField;
    QrCNAB_CfgCodOculto: TWideStringField;
    QrCNAB_CfgSeqArq: TIntegerField;
    DsCNAB_Cfg: TDataSource;
    QrCNAB_CfgCedPosto: TIntegerField;
    QrCNAB_CfgLastNosNum: TLargeintField;
    QrCNAB_CfgLocalPag: TWideStringField;
    QrCNAB_CfgEspecieVal: TWideStringField;
    QrCNAB_CfgOperCodi: TWideStringField;
    QrCNAB_CfgIDCobranca: TWideStringField;
    QrCNAB_CfgAgContaCed: TWideStringField;
    QrAux: TmySQLQuery;
    QrCNAB_CfgDirRetorno: TWideStringField;
    QrCNAB_CfgCartRetorno: TIntegerField;
    QrCNAB_CfgPosicoesBB: TSmallintField;
    QrCNAB_CfgIndicatBB: TWideStringField;
    QrCNAB_CfgTipoCobrBB: TWideStringField;
    QrCNAB_CfgDdProtesBB: TIntegerField;
    QrCNAB_CfgMsg40posBB: TWideStringField;
    QrCNAB_CfgComando: TIntegerField;
    QrCNAB_CfgQuemPrint: TWideStringField;
    QrCNAB_CfgQuemDistrb: TWideStringField;
    QrCNAB_CfgConvCartCobr: TWideStringField;
    QrCNAB_CfgConvVariCart: TWideStringField;
    QrCNAB_CfgCedente: TIntegerField;
    QrCNAB_CfgCodLidrBco: TWideStringField;
    QrCNAB_CfgVariacao: TIntegerField;
    QrBancos: TmySQLQuery;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    QrCNAB_CfgTipoCobranca: TIntegerField;
    QrCNAB_CfgProtesCod: TSmallintField;
    QrCNAB_CfgProtesDds: TSmallintField;
    QrCNAB_CfgEspecieDoc: TWideStringField;
    QrCNAB_CfgEspecieTxt: TWideStringField;
    QrCNAB_CfgCartTxt: TWideStringField;
    QrCNAB_CfgLayoutRem: TWideStringField;
    QrCNAB_CfgLayoutRet: TWideStringField;
    QrCNAB_CfgMultaDias: TSmallintField;
    QrCNAB_CfgNaoRecebDd: TSmallintField;
    QrCNAB_CfgTipBloqUso: TWideStringField;
    QrCNAB_CfgDesco1Cod: TSmallintField;
    QrCNAB_CfgDesco1Dds: TIntegerField;
    QrCNAB_CfgDesco1Fat: TFloatField;
    QrCNAB_CfgDesco2Cod: TSmallintField;
    QrCNAB_CfgDesco2Dds: TIntegerField;
    QrCNAB_CfgDesco2Fat: TFloatField;
    QrCNAB_CfgDesco3Cod: TSmallintField;
    QrCNAB_CfgDesco3Dds: TIntegerField;
    QrCNAB_CfgDesco3Fat: TFloatField;
    QrCNAB_CfgConcatCod: TWideStringField;
    QrCNAB_CfgComplmCod: TWideStringField;
    QrCNAB_CfgNumVersaoI3: TIntegerField;
    QrCNAB_CfgBxaDevCod: TSmallintField;
    QrCNAB_CfgBxaDevDds: TIntegerField;
    QrCNAB_CfgMoedaCod: TWideStringField;
    QrCNAB_CfgContrato: TFloatField;
    QrCNAB_CfgInfNossoNum: TSmallintField;
    QrCNAB_CfgCartEmiss: TIntegerField;
    QrCNAB_CfgModalCobr: TSmallintField;
    QrCNAB_CfgCtaCooper: TWideStringField;
    QrAddr1NUMERO: TFloatField;
    QrAddr1CEP: TFloatField;
    QrAddr1Lograd: TFloatField;
    QrAddr3NUMERO: TFloatField;
    QrAddr3Lograd: TFloatField;
    QrAddr3CEP: TFloatField;
    QrCNAB_CfgNosNumFxaI: TIntegerField;
    QrCNAB_CfgNosNumFxaF: TIntegerField;
    QrCNAB_CfgNosNumFxaU: TSmallintField;
    QrCNAB_CfgCtrGarantiaNr: TWideStringField;
    QrCNAB_CfgCtrGarantiaDV: TWideStringField;
    QrAddr2Lograd: TFloatField;
    QrAddr2CEP: TFloatField;
    QrAddr2NUMERO: TFloatField;
    procedure QrAddr1CalcFields(DataSet: TDataSet);
    procedure QrAddr2CalcFields(DataSet: TDataSet);
    procedure QrAddr3CalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo

  public
    { Public declarations }
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    function ReopenCNAB_Cfg(const Config, CliInt: Integer; const SQL: String;
             var CedenteTipo: Integer; var CedenteNome, CedenteCNPJ: String;
             var SacadorTipo: Integer; var SacadorNome, SacadorCNPJ: String):
             Boolean;
    function PesquisaEntidadePorCNPJ(const CNPJ: String; var Entidade:
             Integer): Boolean;
    function TraduzInstrucaoBloqueto(Instrucao: String; ValorBloq, PercMulta,
             PercJurosMes, CreditoBloq, PercComiss: Double): String;
    function BancosEstaoCadastrados(MostraFmBancos: Boolean): Boolean;
    //
    function ReopenAddr3(Entidade, ModalCobr: Integer; var Msg: String): Boolean;
  end;

var
  DmBco: TDmBco;

implementation

uses UnMyObjects, Module, Bancos, MyDBCheck, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TDmBco.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

function TDmBco.ReopenAddr3(Entidade, ModalCobr: Integer; var Msg: String): Boolean;
begin
  Result := False;
  Msg    := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAddr3, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1,',
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOMEENT,',
    'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF,',
    'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG,',
    'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_,',
    'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA,',
    'IF(en.Tipo=0, en.ENumero    , en.PNumero) + 0.000 NUMERO,',
    'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL,',
    'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO,',
    'mun.Nome CIDADE,',
    'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD,',
    'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF,',
    'IF(en.Tipo=0, en.EPais      , en.PPais  ) Pais,',
    'IF(en.Tipo=0, en.ELograd    , en.PLograd) + 0.000 Lograd,',
    'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) + 0.000 CEP,',
    'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1,',
    'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX,',
    'IF(en.Tipo=0, en.EEMail       , en.PEMail   ) EMail',
    'FROM entidades en',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici)',
    'WHERE en.Codigo=' + Geral.FF0(Entidade),
    '']);
  if QrAddr3.RecordCount > 0 then
  begin
    if QrAddr3CNPJ_TXT.Value = '' then
    begin
      Msg := 'A entidade "' + Geral.FF0(Entidade) + ' ' + QrAddr3NOMEENT.Value + '" n�o possui n�mero de documento cadastrado!';
    end else
    if (ModalCobr = 1) and (QrAddr3CEP.Value = 0) then //Com registro
    begin
      Msg := 'A entidade "' + Geral.FF0(Entidade) + ' ' + QrAddr3NOMEENT.Value + '" n�o possui CEP cadastrado!';
    end else
      Result := True;
  end else
    Msg := 'N�o foi poss�vel localizar o endere�o da entidade ID n�mero ' + Geral.FF0(Entidade) + '!';
end;

function TDmBco.ReopenCNAB_Cfg(const Config, CliInt: Integer; const SQL: String;
var CedenteTipo: Integer; var CedenteNome, CedenteCNPJ: String;
var SacadorTipo: Integer; var SacadorNome, SacadorCNPJ: String//;
{
var Sacado_Tipo: Integer; var Sacado_Nome, Sacado_CNPJ, Sacado_TipoLograd,
Sacado_Rua, Sacado_Numero, Sacado_ComplEnd, Sacado_Bairro, Sacado_Cidade,
Sacado_CEP, Sacado_UF: String;
}): Boolean;
begin
  CedenteTipo := -1;
  CedenteNome := '';
  CedenteCNPJ := '';
  //
  SacadorTipo := -1;
  SacadorNome := '';
  SacadorCNPJ := '';
  //
  QrCNAB_Cfg.Close;
  QrCNAB_Cfg.Params[0].AsInteger := Config;
  UnDmkDAC_PF.AbreQuery(QrCNAB_Cfg, Dmod.MyDB);
  Result := QrCNAB_Cfg.RecordCount > 0;
  if not Result then
  begin
    Geral.MB_Aviso('A configura��o CNAB sob o n�mero de cadastro '
    + IntToStr(Config) + ' n�o foi localizada!');
    Exit;
  end;
  Result := CliInt <> 0;
  if not Result then
  begin
    Geral.MB_Aviso('Gera��o de arquivo cancelada! ' +
    'N�o h� cliente interno definido!');
    Exit;
  end;
  Result :=
    (CliInt = QrCNAB_CfgCedente.Value) or
    (CliInt = QrCNAB_CfgSacadAvali.Value);
  if not Result then
  begin
    Geral.MB_Aviso('Gera��o de arquivo cancelada! ' +
    'O cliente interno deve ser o cedente ou o sacador / avalista na "Configura��o CNAB"!');
    Exit;
  end;
  //
  QrCount.Close;
  QrCount.SQL.Text := SQL;
  UnDmkDAC_PF.AbreQuery(QrCount, Dmod.MyDB);
  Result := QrCountItens.Value = 0;
  if not Result then
  begin
    Geral.MB_Aviso('Gera��o de arquivo cancelada! H� ' +
    IntToStr(QrCountItens.Value) +
    ' item(s) com o cliente interno diferente do cadastrado para a tarefa!');
    Exit;
  end;

  //  Mudei 2011=02-13 de Dmod para DModG (n�o testado)
  DModG.ReopenEndereco(DMBco.QrCNAB_CfgCedente.Value);
  //
  Result := dmkPF.CheckCPFCNPJ(
  DmodG.QrEnderecoCodigo.Value, DmodG.QrEnderecoTipo.Value,
  DmodG.QrEndereco2NOME_ENT.Value, DmodG.QrEnderecoCNPJ_CPF.Value);
  //  Fim 2011=02-13
  if not Result then Exit;
  {
  begin
    //Screen.Cursor := crDefault;
    Geral.MB_Aviso('A��o abortada!');
    Exit;
  end;
  }
  (*
  QrAddr1.Close;
  QrAddr1.Params[0].AsInteger := QrCNAB_CfgCedente.Value;
  UnDmkDAC_PF.AbreQuery(QrAddr1. O p e n;
  *)
  UnDmkDAC_PF.AbreMySQLQuery0(QrAddr1, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, ',
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOMEENT, ',
    'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, ',
    'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, ',
    'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, ',
    'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, ',
    'IF(en.Tipo=0, en.ENumero    , en.PNumero) + 0.000 NUMERO, ',
    'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, ',
    'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, ',
    'IF(en.Tipo=0, en.ECidade    , en.PCidade) CIDADE, ',
    'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, ',
    'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, ',
    'IF(en.Tipo=0, en.EPais      , en.PPais  ) Pais, ',
    'IF(en.Tipo=0, en.ELograd    , en.PLograd) + 0.000 Lograd, ',
    'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) + 0.000 CEP, ',
    'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, ',
    'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'WHERE en.Codigo=' + Geral.FF0(QrCNAB_CfgCedente.Value),
    '']);
  Result := QrAddr1.RecordCount > 0;
  if not Result then
  begin
    Geral.MB_Aviso('Gera��o de arquivo cancelada! O cadastro do cedente '+
    IntToStr(QrCNAB_CfgCedente.Value) + ' n�o foi localizado nas entidades!');
    Exit;
  end;
  Result := dmkPF.CheckCPFCNPJ( QrAddr1Codigo.Value, QrAddr1Tipo.Value,
  QrAddr1NOMEENT.Value, QrAddr1CNPJ_CPF.Value);
  if not Result then
  begin
    //Screen.Cursor := crDefault;
    Geral.MB_Aviso('A��o abortada!');
    Exit;
  end;
  CedenteTipo := QrAddr1Tipo.Value;
  CedenteNome := QrAddr1NOMEENT.Value;
  CedenteCNPJ := QrAddr1CNPJ_CPF.Value;
  CedenteTipo := QrAddr1Tipo.Value;
  //
  if QrCNAB_CfgSacadAvali.Value <> 0 then
  begin
    QrAddr2.Close;
    QrAddr2.Params[0].AsInteger := QrCNAB_CfgSacadAvali.Value;
    UnDmkDAC_PF.AbreQuery(QrAddr2, Dmod.MyDB);
    Result := QrAddr2.RecordCount > 0;
    if not Result then
    begin
      Geral.MB_Aviso('Gera��o de arquivo cancelada! O cadastro do '
      + 'sacador '+ IntToStr(QrCNAB_CfgSacadAvali.Value) + ' n�o foi localizado '
      + 'nas entidades!');
      Exit;
    end;
    Result := dmkPF.CheckCPFCNPJ(QrAddr1Codigo.Value, QrAddr1Tipo.Value,
    QrAddr1NOMEENT.Value, QrAddr1CNPJ_CPF.Value);
    if not Result then Exit;
    {
    begin
      Geral.MB_Aviso('A��o abortada!');
      Exit;
    end;
    }
    SacadorTipo := QrAddr2Tipo.Value;
    SacadorNome := QrAddr2NOMEENT.Value;
    SacadorCNPJ := QrAddr2CNPJ_CPF.Value;
    //
  end;
end;

procedure TDmBco.QrAddr1CalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrAddr1TE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrAddr1Te1.Value);
  QrAddr1FAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrAddr1Fax.Value);
  QrAddr1CNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrAddr1CNPJ_CPF.Value);
  //
  QrAddr1NUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrAddr1Rua.Value, Trunc(QrAddr1Numero.Value), False);
  QrAddr1E_ALL.Value := QrAddr1NOMELOGRAD.Value;
  if Trim(QrAddr1E_ALL.Value) <> '' then QrAddr1E_ALL.Value :=
    QrAddr1E_ALL.Value + ' ';
  QrAddr1E_ALL.Value := QrAddr1E_ALL.Value + QrAddr1Rua.Value;
  if Trim(QrAddr1Rua.Value) <> '' then QrAddr1E_ALL.Value :=
    QrAddr1E_ALL.Value + ', ' + QrAddr1NUMERO_TXT.Value;
  if Trim(QrAddr1Compl.Value) <>  '' then QrAddr1E_ALL.Value :=
    QrAddr1E_ALL.Value + ' ' + QrAddr1Compl.Value;
  if Trim(QrAddr1Bairro.Value) <>  '' then QrAddr1E_ALL.Value :=
    QrAddr1E_ALL.Value + ' - ' + QrAddr1Bairro.Value;
  if QrAddr1CEP.Value > 0 then QrAddr1E_ALL.Value :=
    QrAddr1E_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrAddr1CEP.Value);
  if Trim(QrAddr1Cidade.Value) <>  '' then QrAddr1E_ALL.Value :=
    QrAddr1E_ALL.Value + ' - ' + QrAddr1Cidade.Value;
  //
  QrAddr1ECEP_TXT.Value :=Geral.FormataCEP_NT(QrAddr1CEP.Value);
  //
  if QrAddr1Tipo.Value = 0 then Natal := QrAddr1ENatal.Value
  else Natal := QrAddr1PNatal.Value;
  if Natal < 2 then QrAddr1NATAL_TXT.Value := ''
  else QrAddr1NATAL_TXT.Value := Geral.FDT(Natal, 2);
end;

procedure TDmBco.QrAddr2CalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrAddr2TE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrAddr2Te1.Value);
  QrAddr2FAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrAddr2Fax.Value);
  QrAddr2CNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrAddr2CNPJ_CPF.Value);
  //
  QrAddr2NUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrAddr2Rua.Value , QrAddr2Numero.Value, False);
  QrAddr2E_ALL.Value := QrAddr2NOMELOGRAD.Value;
  if Trim(QrAddr2E_ALL.Value) <> '' then QrAddr2E_ALL.Value :=
    QrAddr2E_ALL.Value + ' ';
  QrAddr2E_ALL.Value := QrAddr2E_ALL.Value + QrAddr2Rua.Value;
  if Trim(QrAddr2Rua.Value) <> '' then QrAddr2E_ALL.Value :=
    QrAddr2E_ALL.Value + ', ' + QrAddr2NUMERO_TXT.Value;
  if Trim(QrAddr2Compl.Value) <>  '' then QrAddr2E_ALL.Value :=
    QrAddr2E_ALL.Value + ' ' + QrAddr2Compl.Value;
  if Trim(QrAddr2Bairro.Value) <>  '' then QrAddr2E_ALL.Value :=
    QrAddr2E_ALL.Value + ' - ' + QrAddr2Bairro.Value;
  if QrAddr2CEP.Value > 0 then QrAddr2E_ALL.Value :=
    QrAddr2E_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrAddr2CEP.Value);
  if Trim(QrAddr2Cidade.Value) <>  '' then QrAddr2E_ALL.Value :=
    QrAddr2E_ALL.Value + ' - ' + QrAddr2Cidade.Value;
  //
  QrAddr2ECEP_TXT.Value :=Geral.FormataCEP_NT(QrAddr2CEP.Value);
  //
  if QrAddr2Tipo.Value = 0 then Natal := QrAddr2ENatal.Value
  else Natal := QrAddr2PNatal.Value;
  if Natal < 2 then QrAddr2NATAL_TXT.Value := ''
  else QrAddr2NATAL_TXT.Value := Geral.FDT(Natal, 2);
end;

procedure TDmBco.QrAddr3CalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrAddr3TE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrAddr3Te1.Value);
  QrAddr3FAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrAddr3Fax.Value);
  QrAddr3CNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrAddr3CNPJ_CPF.Value);
  //
  QrAddr3NUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrAddr3Rua.Value, Trunc(QrAddr3Numero.Value), False);
  QrAddr3E_ALL.Value := QrAddr3NOMELOGRAD.Value;
  if Trim(QrAddr3E_ALL.Value) <> '' then QrAddr3E_ALL.Value :=
    QrAddr3E_ALL.Value + ' ';
  QrAddr3E_ALL.Value := QrAddr3E_ALL.Value + QrAddr3Rua.Value;
  if Trim(QrAddr3Rua.Value) <> '' then QrAddr3E_ALL.Value :=
    QrAddr3E_ALL.Value + ', ' + QrAddr3NUMERO_TXT.Value;
  if Trim(QrAddr3Compl.Value) <>  '' then QrAddr3E_ALL.Value :=
    QrAddr3E_ALL.Value + ' ' + QrAddr3Compl.Value;
  if Trim(QrAddr3Bairro.Value) <>  '' then QrAddr3E_ALL.Value :=
    QrAddr3E_ALL.Value + ' - ' + QrAddr3Bairro.Value;
  if QrAddr3CEP.Value > 0 then QrAddr3E_ALL.Value :=
    QrAddr3E_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrAddr3CEP.Value);
  if Trim(QrAddr3Cidade.Value) <>  '' then QrAddr3E_ALL.Value :=
    QrAddr3E_ALL.Value + ' - ' + QrAddr3Cidade.Value;
  //
  QrAddr3ECEP_TXT.Value :=Geral.FormataCEP_NT(QrAddr3CEP.Value);
  //
  if QrAddr3Tipo.Value = 0 then Natal := QrAddr3ENatal.Value
  else Natal := QrAddr3PNatal.Value;
  if Natal < 2 then QrAddr3NATAL_TXT.Value := ''
  else QrAddr3NATAL_TXT.Value := Geral.FDT(Natal, 2);
end;

function TDmBco.BancosEstaoCadastrados(MostraFmBancos: Boolean): Boolean;
begin
  //Result := False;
  QrBancos.Close;
  //QrBancos.Params[0].As
  UnDmkDAC_PF.AbreQuery(QrBancos, Dmod.MyDB);
  //
  Result := QrBancos.RecordCount > 0;
  //
  if MostraFmBancos and not Result then
  begin
    if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
    begin
      FmBancos.ShowModal;
      FmBancos.Destroy;
    end;
  end;
end;

procedure TDmBco.DataModuleCreate(Sender: TObject);
{
var
  I: Integer;
}
begin
{ o DMod ainda n�oi foi Criado!
  for I := 0 to ComponentCount - 1 do
  begin
    if (Components[I] is TmySQLQuery) then
    begin
      TmySQLQuery(Components[I]).Close;
      TmySQLQuery(Components[I]).Database := Dmod.MyDB;
    end;
  end;
}
end;

procedure TDmBco.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

function TDmBco.PesquisaEntidadePorCNPJ(const CNPJ: String; var Entidade:
Integer): Boolean;
var
   Txt: String;
begin
  Txt := Geral.SoNumero_TT(CNPJ);
  QrCNPJ.Close;
  QrCNPJ.Params[0].AsString := Txt;
  UnDmkDAC_PF.AbreQuery(QrCNPJ, Dmod.MyDB);
  Entidade := QrCNPJCodigo.Value;
  Result := QrCNPJ.RecordCount > 0;
end;

function TDmBco.TraduzInstrucaoBloqueto(Instrucao: String; ValorBloq, PercMulta,
PercJurosMes, CreditoBloq, PercComiss: Double): String;
  procedure Troca(var Instr: String; const Variavel: String);
  var
    p, t1: Integer;
    Txt: String;
  begin
    p := pos(Variavel, Instr);
    t1 := Length(Variavel);
    if p > 0 then
    begin
      Delete(Instr, p, t1);
      Txt := '';
      if Variavel = '[MULTA_ATRASO]' then
        Txt := Geral.FFT(
          //DCond.QrCondPercMulta.Value * DCond.QrBoletosSUB_TOT.Value / 100, 2, siPositivo)
          PercMulta * ValorBloq / 100, 2, siPositivo)
      else
      if Variavel = '[JUROS_ATRASO]' then
        Txt := Geral.FFT(
          //(DCond.QrCondPercJuros.Value / 30) * DCond.QrBoletosSUB_TOT.Value / 100, 2, siPositivo);
          (PercJurosMes / 30) * ValorBloq / 100, 2, siPositivo)
      else
      if Variavel = '[VALOR_VALOR]' then
        Txt := Geral.FFT(ValorBloq, 2, siPositivo)
      else
      if Variavel = '[VALOR_CREDITO]' then
        Txt := Geral.FFT(CreditoBloq, 2, siPositivo)
      else
      if Variavel = '[VALOR_COMISS]' then
        Txt := Geral.FFT(PercComiss, 6, siPositivo);
      Insert(Txt, Instr, p);
    end;
  end;
var
  Instr: String;
begin
  Instr := Instrucao;
  Troca(Instr, '[MULTA_ATRASO]');
  Troca(Instr, '[JUROS_ATRASO]');
  Troca(Instr, '[VALOR_VALOR]');
  Troca(Instr, '[VALOR_CREDITO]');
  Troca(Instr, '[VALOR_COMISS]');
  Result := Instr;
end;

procedure TDmBco.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

end.
