unit BancosOcor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, dmkGeral,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox;

type
  TFmBancosOcor = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    Label2: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    BtContas: TSpeedButton;
    EdOcorrencia: TEdit;
    Label1: TLabel;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrDupl: TmySQLQuery;
    LaTipo: TLabel;
    Label3: TLabel;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    QrBancos: TmySQLQuery;
    DsBancos: TDataSource;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    CkCarrega: TCheckBox;
    Label4: TLabel;
    EdNome: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmBancosOcor: TFmBancosOcor;

implementation

uses UnMyObjects, Contas, UMySQLModule, UnInternalConsts, Module, Bancos;

{$R *.DFM}

procedure TFmBancosOcor.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBancosOcor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBancosOcor.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmBancosOcor.BtContasClick(Sender: TObject);
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmContas, FmContas);
  finally
    FmContas.LocCod(QrContasCodigo.Value, QrContasCodigo.Value);
    Screen.Cursor := Cursor;
  end;
  FmContas.ShowModal;
  FmContas.Destroy;
  QrContas.Close;
  QrContas.Open;
  EdConta.Text := IntToStr(VAR_CONTA);
  CBConta.KeyValue := VAR_CONTA;
  //Dmod.DefParams;
end;

procedure TFmBancosOcor.FormCreate(Sender: TObject);
begin
  QrContas.Open;
  QrBancos.Open;
end;

procedure TFmBancosOcor.BtOKClick(Sender: TObject);
var
  Banco, Genero: Integer;
  Ocorrencia: String;
begin
  Banco := Geral.IMV(EdBanco.Text);
  if Banco = 0 then
  begin
    Application.MessageBox('Banco n�o informado!', 'Aviso', MB_OK+MB_ICONWARNING);
    if EdBanco.Enabled then EdBanco.SetFocus;
    Exit;
  end;
  Ocorrencia := Trim(EdOcorrencia.Text);
  if Ocorrencia = '' then
  begin
    Application.MessageBox('Ocorr�ncia n�o informada!', 'Aviso', MB_OK+MB_ICONWARNING);
    if EdOcorrencia.Enabled then EdOcorrencia.SetFocus;
    Exit;
  end;
  Genero := Geral.IMV(EdConta.Text);
  if Genero = 0 then
  begin
    if Application.MessageBox(PChar('Conta vinculada n�o informada! Deseja ' +
    'Continuar assim mesmo?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONWARNING) <> ID_YES
    then Exit;
  end;
  Dmod.QrUpd.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    QrDupl.Close;
    QrDupl.Params[0].AsInteger := Banco;
    QrDupl.Params[1].AsString := Ocorrencia;
    QrDupl.Open;
    if QrDupl.RecordCount > 0 then
    begin
      Application.MessageBox('Ocorr�ncia duplicada! Cadastro cancelado', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
    Dmod.QrUpd.SQL.Add('INSERT INTO bancoslei SET ');
    Dmod.QrUpd.SQL.Add('Genero=:P0, Carrega=:P1, Nome=:P2, ');
    Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Ocorrencia=:Pc, Codigo=:Pd');
    Dmod.QrUpd.SQL.Add('');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE bancoslei SET ');
    Dmod.QrUpd.SQL.Add('Genero=:P0, Carrega=:P1, Nome=:P2, ');
    Dmod.QrUpd.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Ocorrencia=:Pc AND Codigo=:Pd');
    Dmod.QrUpd.SQL.Add('');
  end;
  Dmod.QrUpd.Params[00].AsInteger := Genero;
  Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(CkCarrega.Checked);
  Dmod.QrUpd.Params[02].AsString  := EdNome.Text;
  //
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[04].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[05].AsString  := Ocorrencia;
  Dmod.QrUpd.Params[06].AsInteger := Banco;
  Dmod.QrUpd.ExecSQL;
  //FmBancos.ReopenBancosLei(Ocorrencia);
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    EdOcorrencia.Text := '';
    EdConta.Text := '';
    CBConta.KeyValue := '';
    EdOcorrencia.SetFocus;
  end else Close;
end;

end.
