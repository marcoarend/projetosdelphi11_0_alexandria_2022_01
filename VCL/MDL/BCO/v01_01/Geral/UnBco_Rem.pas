unit UnBco_Rem;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, UnBancos, dmkGeral, dmkEdit, Variants;

type
  TUBco_Rem = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  AvisoImplementacao(Banco, CNAB, Registrada: Integer; NomeLayout: String): String;
    function  TipoDeInscricao(const TipoCad, Banco, CNAB, Campo: Integer;
              NomeLayout: String; var Res: String): Boolean;
    function  CodigoDeOcorrenciaRemessa(const Banco, MeuCodOcorrencia: Integer;
              var Codigo: String): Boolean;
    function  CodigoDeAceiteRemessa(const Banco, Aceite: Integer;
              NomeLayout: String; var Cod: String): Boolean;
    function  QuemImprimeOBloqueto(const Banco, Quem: Integer; NomeLayout: String;
              var Cod: String): Boolean;
    function  QuemDistribuiOBloqueto(const Banco, Quem: Integer;
              var Cod: String): Boolean;
    function  LayoutCNAB240(const Banco, Registro: Integer; Segmento: Char;
              var L: MyArrayLista; const SubSegmento: Integer;
              const NomeLayout: String): TCNABResult;
    function  LayoutCNAB400(const Banco, Registro: Integer; var L: MyArrayLista;
              const Posicoes: Integer; const NomeLayout: String): TCNABResult;
    function  FileEnd(const Banco, CNAB: Integer; const NomeLayout: String): Variant;
    // Arrays CNAB_Cfg
    procedure AddLin(var Res: MyArrayLista; var Linha: Integer;
              const Codigo: String; Descricao: array of String);
    function  InstrucaoDeCobranca(const Banco, CNAB, Comando: Integer;
              NomeLayout: String): MyArrayLista;
    function  AvisosDeInstrucaoDeCobranca(const Banco, CNAB: Integer;
              NomeLayout: String): MyArrayLista;
    function  EspecieDoTitulo(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
    function  EspecieDoValor(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
    function  CarteiraDeCobranca(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
    //function CartCodBB(const Banco, CNAB: Integer): MyArrayLista;
    //function CadastroDoTituloNoBanco(const Banco, CNAB: Integer): MyArrayLista;
    function  QuemDistribuiBloqueto(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
    function  QuemImprimeBloqueto(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
    procedure QuemImprimeBloqueto_Verifica(const Banco, CNAB: Integer; ValorInformado: String);
    function  TipoCart(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
    function  AvisosDeCarteiraDeCobranca(const Banco, CNAB: Integer; Layout: String): MyArrayLista;
    function  TipoDeCobrancaBB(const Banco, CNAB: Integer; Carteira: String): MyArrayLista;
    function  Comando(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
    function  DdProtesto(const Banco, CNAB: Integer): MyArrayLista;
    function  MultaTipo(const Banco, CNAB: Integer; Layout: String): MyArrayLista;
    function  JurosTipo(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
    function  DescontosTipos(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
    function  CodProtestoRem(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
    function  CodBaixaDevolRem(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
    function  CodMoeda(const Banco, CNAB: Integer): MyArrayLista;
    function  VariacaoCarteira(const Banco, CNAB: Integer): MyArrayLista;
    function  LayoutsRemessa(const Banco, CNAB: Integer): MyArrayLista;
    function  LayoutsRetorno(const Banco, CNAB: Integer): MyArrayLista;
    function  TipoDeBloqutoUsado(const Banco, CNAB: Integer): MyArrayLista;
    //
    //function SelecionaItensCarteira341(Lista: MyArrayLista): String;
    function  SelecionaItensCarteira(Lista: MyArrayLista; EdTipoCart: TdmkEdit;
              EdCartNum, EdCartCod, EdCartTxt, EdDescri: TEdit): Boolean;
    function  TipoDeCarteiraRemessa(const Banco: Integer; const Tipo:
              String; var TipoCart: Integer): Boolean;
    function  ObtemProximoNossoNumero(CNAB_Cfg: Integer;
              AvisaCNABNaoImplementado: Boolean): Double;
    //function ObtemTipoDeCobrancaBB(const Banco, Carteira: Integer): String;
  end;

var
  UBco_Rem: TUBco_Rem;

implementation

uses dmkSelLista, ModuleBco, Module, ModuleGeral, DmkDAC_PF;


function TUBco_Rem.TipoDeInscricao(const TipoCad, Banco, CNAB, Campo: Integer;
  NomeLayout: String; var Res: String): Boolean;
begin
  Result := True;
  case Banco of
    001: // Banco do Brasil
    begin
      case CNAB of
        240:
        begin
          case TipoCad of
           -1: Res := '0';  // N�o tem
            0: Res := '2';  // CNPJ
            1: Res := '1';  // CPF
          //?: Res := '3';  // PIS/PASEP
          //?: Res := '99';  // Outros
          end;
        end;
        400:
        begin
          case TipoCad of
           -1: Res := '00';  // N�o tem
            0: Res := '02';  // CNPJ
            1: Res := '01';  // CPF
          //?: Res := '3';  // PIS/PASEP
          //?: Res := '99';  // Outros
          end;
        end;
      end;
    end;
    008, 033, 353: // Santander Banespa
    begin
      case TipoCad of
       //-1: Res := '98';  // N�o tem
        0: Res := '02';  // CNPJ
        1: Res := '01';  // CPF
      //?: Res := '03';  // PIS/PASEP
      //?: Res := '99';  // Outros
      end;
    end;
    237: // Bradesco
    begin
      case TipoCad of
       -1: Res := '98';  // N�o tem
        0: Res := '02';  // CNPJ
        1: Res := '01';  // CPF
      //?: Res := '03';  // PIS/PASEP
      //?: Res := '99';  // Outros
      end;
    end;
    341: // Itau
    begin                                      // nenhum
      if (Campo = 851) and (TipoCad = -1) then Res := '00' else
      case TipoCad of
       //-1: Res := '00';  // N�o tem
        0: Res := '02';  // CNPJ
        1: Res := '01';  // CPF
      //?: Res := '03';  // PIS/PASEP
      //?: Res := '99';  // Outros
      end;
    end;
    399: // HSBC CNAB 400 - Cobran�a Com Registro -> Diretiva
    begin
      case TipoCad of
       -1: Res := '98';  // N�o tem
        0: Res := '02';  // CNPJ
        1: Res := '01';  // CPF
      //?: Res := '03';  // PIS/PASEP
        else Res := '99';  // Outros
      end;
    end;
    409: // Unibanco
    begin
      case TipoCad of
       //-1: Res := '00';  // N�o tem
        0: Res := '02';  // CNPJ
        1: Res := '01';  // CPF
      //?: Res := '03';  // PIS/PASEP
      //?: Res := '99';  // Outros
      end;
    end;
    756: // SICOOB
    begin
      if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
      begin
        case TipoCad of
         -1: Res := '00';  // N�o tem
          0: Res := '02';  // CNPJ
          1: Res := '01';  // CPF
        //?: Res := '03';  // PIS/PASEP
        //?: Res := '99';  // Outros
        end;
      end else
      if NomeLayout = CO_756_240_2018 then
      begin
        case TipoCad of
          0: Res := '2';  // CNPJ
          1: Res := '1';  // CPF
        end;
      end else
      begin
        case TipoCad of
         //-1: Res := '00';  // N�o tem
          0: Res := '02';  // CNPJ
          1: Res := '01';  // CPF
        //?: Res := '03';  // PIS/PASEP
        //?: Res := '99';  // Outros
        end;
      end;
    end;
    else begin
      Result := False;
      Geral.MB_Erro(
      'Banco n�o implementado para defini��o de "tipo de inscri��o".');
    end;
  end;
end;

function TUBco_Rem.CodigoDeOcorrenciaRemessa(const Banco, MeuCodOcorrencia: Integer;
  var Codigo: String): Boolean;
var
  Erro: Integer;
begin
  Codigo := '';
  Erro := 0;
  case MeuCodOcorrencia of
  // Remessa
    001: // Entrada de T�tulo
    case Banco of
      008, 033, 356: 
        Codigo := '01';
      237,341: 
        Codigo := '01'; // Nota 6
      399: 
        Codigo := '01'; // Nota 6 no manual da Cobran�a Diretiva
      756: 
        Codigo := '01'; //Remessa
      else Erro := 1;
    end;
    else Erro := 2;
  end;
  Result := Erro = 0;
  case Erro of
    1: Geral.MB_Erro('O banco ' + IntToStr(Banco) +
       ' n�o possui c�digo para a ocorr�ncia ' + FormatFloat('000',
       MeuCodOcorrencia) + ' em "UnBco_Rem.CodigoDeOcorrenciaRemessa".');
    2: Geral.MB_Erro('A ocorr�ncia ' + FormatFloat('000',
       MeuCodOcorrencia) + ' n�o est� implementada em ' +
       '"UnBco_Rem.CodigoDeOcorrenciaRemessa".');
  end;
end;

function TUBco_rem.CodigoDeAceiteRemessa(const Banco, Aceite: Integer;
  NomeLayout: String; var Cod: String): Boolean;
begin
  Result := True;
  Cod := 'N';
  case Banco of
    // Banco do Brasil
    001:
    case Aceite of
      0: Cod := 'N';  // N�o
      1: Cod := 'A';  // Sim
    end;
    008, 033, 356:
    case Aceite of
      0: Cod := 'N';  // N�o
      1: Cod := 'A';  // Sim // N�o comentado no manual CO_033_RECEBIMENTOS_400_v02_00_2009_10
    end;
    // Bradesco
    237:
    case Aceite of
      0: Cod := 'N';  // N�o
      1: Cod := 'A';  // Sim
    end;
    // Itau
    341:
    case Aceite of
      0: Cod := 'N';  // N�o
      1: Cod := 'A';  // Sim
    end;
    // HSBC
    399:
    case Aceite of
      0: Cod := 'N';  // N�o
      1: Cod := 'A';  // Sim
    end;
    // Itau
    409:
    case Aceite of
      0: Cod := 'N';  // N�o
      1: Cod := 'A';  // Sim
    end;
    // Itau
    756:
    begin
      if (NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015) or
        (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) or
        (NomeLayout = CO_756_240_2018) then
      begin
        case Aceite of
          0: Cod := 'N';  // N�o
          1: Cod := 'A';  // Sim
        end;
      end else
      begin
        case Aceite of
          0: Cod := '0';  // N�o
          1: Cod := '1';  // Sim
        end;
      end;
    end
    else begin
      Result := False;
      Geral.MB_Erro('Banco n�o implementado para defini��o de "Aceite".');
    end;
  end;
end;

function TUBco_rem.QuemDistribuiOBloqueto(const Banco, Quem: Integer;
var Cod: String): Boolean;
begin
  Result := False;
  case Banco of
    // Banco do Brasil?
    //001: ???
    // SICOOB // com registro!!!
    756:
    begin
      Result := True;
      case Quem of
        1: Cod := '1';  // A cooperativa
        2: Cod := '2';  // O cliente da Dermatek
        else
        begin
          //Result := False;
          Geral.MB_ERRO('Valor inv�lido para "Quem distribui"!');
        end;
      end;
    end
    else begin
      //Result := False;
      Geral.MB_Aviso('Banco n�o implementado para defini��o de ' +
      '"Quem imprime o bloqueto".');
    end;
  end;
end;

function TUBco_rem.QuemImprimeOBloqueto(const Banco, Quem: Integer;
  NomeLayout: String; var Cod: String): Boolean;
begin
  Result := True;
  case Banco of
    // Banco do Brasil // com registro!!!
    001: ;// diz na carteira?
    // Bradesco // com registro!!!
    237:
    case Quem of
      0: Cod := '2';  // O cliente da Dermatek
      1: Cod := '1';  // O banco
    end;
    // Itau // com registro!!!
    756: // diz na carteira?
    begin
      if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
      begin
        case Quem of
          0: Cod := '2';  // O cliente da Dermatek
          1: Cod := '1';  // O banco
        end;
      end else
        ;
    end;
    341: ;// diz na carteira?
    else begin
      Result := False;
      Geral.MB_Aviso('Banco n�o implementado para defini��o de ' +
      '"Quem imprime o bloqueto".');
    end;
  end;
end;

function TUBco_rem.EspecieDoTitulo(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer;
  const Codigo, Impressao, Descricao, Especie: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 4);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Impressao;
    Res[Linha][2] :=  Descricao;
    Res[Linha][3] :=  Especie;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case Banco of
    001: // com registro!!!
    begin
      case CNAB of
        240:
        begin
          AddLinha(Result, Linha, '01', 'CH ', 'Cheque', '');
          AddLinha(Result, Linha, '02', 'DM ', 'Duplicata Mercantil', '');
          AddLinha(Result, Linha, '03', 'DMI', 'Duplicata Mercantil p/ Indica��o', ''); 
          AddLinha(Result, Linha, '04', 'DS ', 'Duplicata Servi�o', ''); 
          AddLinha(Result, Linha, '05', 'DSI', 'Duplicata Servi�o p/ Indica��o', ''); 
          AddLinha(Result, Linha, '06', 'DR ', 'Duplicata Rural', ''); 
          AddLinha(Result, Linha, '07', 'LC ', 'Letra de C�mbio', ''); 
          AddLinha(Result, Linha, '08', 'NCC', 'Nota de Cr�dito Comercial', ''); 
          AddLinha(Result, Linha, '09', 'NCE', 'Nota de Cr�dito � Exporta��o', ''); 
          AddLinha(Result, Linha, '10', 'NCI', 'Nota de Cr�dito Industrial', ''); 
          AddLinha(Result, Linha, '11', 'NCR', 'Nota de Cr�dito Rural', ''); 
          AddLinha(Result, Linha, '12', 'NP ', 'Nota Promiss�ria', ''); 
          AddLinha(Result, Linha, '13', 'NPR', 'Nota Promiss�ria Rural', ''); 
          AddLinha(Result, Linha, '14', 'TM ', 'Triplicata Mercantil', ''); 
          AddLinha(Result, Linha, '15', 'TS ', 'Triplicata de Servi�o', ''); 
          AddLinha(Result, Linha, '16', 'NS ', 'Nota de Seguro', ''); 
          AddLinha(Result, Linha, '17', 'RC ', 'Recibo', ''); 
          AddLinha(Result, Linha, '18', 'FAT', 'Fatura', ''); 
          AddLinha(Result, Linha, '19', 'ND ', 'Nota de d�bito', ''); 
          AddLinha(Result, Linha, '20', 'AP ', 'Ap�lice de Seguros', ''); 
          AddLinha(Result, Linha, '21', 'ME ', 'Mensalidade Escolar', ''); 
          AddLinha(Result, Linha, '22', 'PC ', 'Parcela de Cons�rcio', ''); 
          AddLinha(Result, Linha, '99', 'Outros', 'Outros', ''); 
        end;
        400:
        begin
          AddLinha(Result, Linha, '01', ' ', 'DUPLICATA MERCANTIL', '');
          AddLinha(Result, Linha, '02', ' ', 'NOTA PROMISS�RIA', '');
          AddLinha(Result, Linha, '03', ' ', 'NOTA DE SEGURO', '');
          AddLinha(Result, Linha, '05', ' ', 'RECIBO', ''); 
          AddLinha(Result, Linha, '08', ' ', 'LETRA DE C�MBIO', ''); 
          AddLinha(Result, Linha, '09', ' ', 'WARRANT', ''); 
          AddLinha(Result, Linha, '10', ' ', 'CHEQUE', ''); 
          AddLinha(Result, Linha, '12', ' ', 'DUPLICATA DE SERVI�O', ''); 
          AddLinha(Result, Linha, '13', ' ', 'NOTA DE D�BITO', ''); 
          AddLinha(Result, Linha, '15', ' ', 'AP�LICE DE SEGURO', ''); 
          AddLinha(Result, Linha, '25', ' ', 'D�VIDA ATIVA DA UNI�O', ''); 
          AddLinha(Result, Linha, '26', ' ', 'DIVIDA ATIVA DE ESTADO', ''); 
          AddLinha(Result, Linha, '27', ' ', 'DIVIDA ATIVA DE MUNIC�PIO', ''); 
        end;
      end;
    end;
    008, 033, 353: // com registro!!!
    begin
      case CNAB of
        240:
        begin
          AddLinha(Result, Linha, '02', 'DM', 'DUPLICATA MERCANTIL', '');
          AddLinha(Result, Linha, '04', 'DS', 'DUPLICATA DE SERVICO', '');
          AddLinha(Result, Linha, '07', 'LC', 'LETRA DE C�MBIO (SOMENTE PARA BANCO 353)', '');
          AddLinha(Result, Linha, '30', 'LC', 'LETRA DE C�MBIO (SOMENTE PARA BANCO 008)', '');
          AddLinha(Result, Linha, '12', 'NP', 'NOTA PROMISSORIA', '');
          AddLinha(Result, Linha, '13', 'NR', 'NOTA PROMISSORIA RURAL', '');
          AddLinha(Result, Linha, '17', 'RC', 'RECIBO', '');
          AddLinha(Result, Linha, '20', 'AP', 'APOLICE DE SEGURO', '');
          AddLinha(Result, Linha, '97', 'CH', 'CHEQUE', '');
          AddLinha(Result, Linha, '98', 'ND', 'NOTA PROMISSORIA DIRETA', '');
        end;
        400:
        begin
          AddLinha(Result, Linha, '01', ' ', 'DUPLICATA', '');
          AddLinha(Result, Linha, '02', ' ', 'NOTA PROMISS�RIA', '');
          AddLinha(Result, Linha, '03', ' ', 'AP�LICE / NOTA DE SEGURO', '');
          AddLinha(Result, Linha, '05', ' ', 'RECIBO', '');
          AddLinha(Result, Linha, '06', ' ', 'DUPLICATA DE SERVI�O', '');
          AddLinha(Result, Linha, '07', ' ', 'LETRA DE CAMBIO', '');
        end;
      end;
    end;
    237: // com registro!!!
    begin
      case CNAB of
        400:
        begin
          {if NomeLayout = CO_237_MP_4008_0121_01_Data_11_11_2010 then}
          begin
            AddLinha(Result, Linha, '01', ' ', 'DUPLICATA MERCANTIL', '');
            AddLinha(Result, Linha, '02', ' ', 'NOTA PROMISS�RIA', '');
            AddLinha(Result, Linha, '03', ' ', 'NOTA DE SEGURO', '');
            AddLinha(Result, Linha, '04', ' ', 'COBRAN�A SERIADA', '');
            AddLinha(Result, Linha, '05', ' ', 'RECIBO', '');
            AddLinha(Result, Linha, '10', ' ', 'LETRA DE C�MBIO', '');
            AddLinha(Result, Linha, '11', ' ', 'NOTA DE D�BITOS', '');
            AddLinha(Result, Linha, '12', ' ', 'DUPLICATA DE SERVI�O', '');
            AddLinha(Result, Linha, '99', ' ', 'DIVERSOS', '');
          end;
        end;
      end;
    end;
    341:  // com registro!!!
    begin
      case CNAB of
        400:
        begin
          if NomeLayout = CO_341_COBRANCA_BANCARIA_MARCO_2012 then
          begin
            AddLinha(Result, Linha, '01', ' ', 'DUPLICATA MERCANTIL', '');
            AddLinha(Result, Linha, '02', ' ', 'NOTA PROMISS�RIA', '');
            AddLinha(Result, Linha, '03', ' ', 'NOTA DE SEGURO', '');
            AddLinha(Result, Linha, '04', ' ', 'MENSALIDADE ESCOLAR', '');
            AddLinha(Result, Linha, '05', ' ', 'RECIBO', '');
            AddLinha(Result, Linha, '06', ' ', 'CONTRATO', '');
            AddLinha(Result, Linha, '07', ' ', 'COSSEGUROS', '');
            AddLinha(Result, Linha, '08', ' ', 'DUPLICATA DE SERVI�O', '');
            AddLinha(Result, Linha, '09', ' ', 'LETRA DE C�MBIO', '');
            AddLinha(Result, Linha, '13', ' ', 'NOTA DE D�BITOS', '');
            AddLinha(Result, Linha, '15', ' ', 'DOCUMENTO DE D�VIDA', '');
            AddLinha(Result, Linha, '99', ' ', 'DIVERSOS', '');
          end else
          if NomeLayout = CO_341_COBRANCA_BANCARIA_SETEMBRO_2012 then
          begin
            AddLinha(Result, Linha, '01', ' ', 'DUPLICATA MERCANTIL', '');
            AddLinha(Result, Linha, '02', ' ', 'NOTA PROMISS�RIA', '');
            AddLinha(Result, Linha, '03', ' ', 'NOTA DE SEGURO', '');
            AddLinha(Result, Linha, '04', ' ', 'MENSALIDADE ESCOLAR', '');
            AddLinha(Result, Linha, '05', ' ', 'RECIBO', '');
            AddLinha(Result, Linha, '06', ' ', 'CONTRATO', '');
            AddLinha(Result, Linha, '07', ' ', 'COSSEGUROS', '');
            AddLinha(Result, Linha, '08', ' ', 'DUPLICATA DE SERVI�O', '');
            AddLinha(Result, Linha, '09', ' ', 'LETRA DE C�MBIO', '');
            AddLinha(Result, Linha, '13', ' ', 'NOTA DE D�BITOS', '');
            AddLinha(Result, Linha, '15', ' ', 'DOCUMENTO DE D�VIDA', '');
            AddLinha(Result, Linha, '16', ' ', 'ENCARGOS CONDOMINIAIS', '');
            AddLinha(Result, Linha, '17', ' ', 'CONTA DE PRESTA��O DE SERVI�OS', '');
            AddLinha(Result, Linha, '99', ' ', 'DIVERSOS', '');
          end;
        end;
      end;
    end;
    399:  // com registro
    begin
      case CNAB of
        400:
        begin
          AddLinha(Result, Linha, '01', 'DP', 'DUPLICATA MERCANTIL', 'COBRAN�A SIMPLIFICADA');
          AddLinha(Result, Linha, '02', 'NP', 'NOTA PROMISS�RIA', 'COBRAN�A SIMPLIFICADA');
          AddLinha(Result, Linha, '03', 'NS', 'NOTA DE SEGURO', 'COBRAN�A SIMPLIFICADA');
          AddLinha(Result, Linha, '05', 'RC', 'RECIBO', 'COBRAN�A SIMPLIFICADA');
          AddLinha(Result, Linha, '10', 'DS', 'DUPLICATA DE SERVI�O', 'COBRAN�A SIMPLIFICADA');
          //
          AddLinha(Result, Linha, '08', 'SD', 'COM COMPLEMENTA��O DO BLOQUETO PELO CLIENTE', 'COBRAN�A EXPRESSA');
          //
          AddLinha(Result, Linha, '09', 'CE', 'COBRAN�A COM EMISS�O TOTAL DO BLOQUETO PELO BANCO', 'COBRAN�A ESCRITURAL');
          //
          AddLinha(Result, Linha, '98', 'PD', 'COBRAN�A COM EMISS�O TOTAL DO BLOQUETO PELO CLIENTE', 'COBRAN�A DIRETIVA');
        end;
      end;
    end;
    409:  // sem registro!!!
    begin
      case CNAB of
        400:
        begin
          AddLinha(Result, Linha, 'Mercan', 'Mercan', 'DUPLICATA MERCANTIL', '');
          AddLinha(Result, Linha, 'Promis', 'Promis', 'NOTA PROMISS�RIA', '');
          AddLinha(Result, Linha, 'Recibo', 'Recibo', 'RECIBO', '');
          AddLinha(Result, Linha, 'Cambio', 'Cambio', 'LETRA DE C�MBIO', '');
          AddLinha(Result, Linha, 'Servic', 'Servic', 'DUPLICATA DE SERVI�O', '');
          AddLinha(Result, Linha, 'Outros', 'Outros', 'OUTROS', '');
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        240:
        begin
          if (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
          begin
            AddLinha(Result, Linha, '02', 'DM', 'Duplicata', '');
            AddLinha(Result, Linha, '04', 'DS', 'Duplicata de Servi�o', '');
            AddLinha(Result, Linha, '07', 'LC', 'Letra de C�mbio', '');
            AddLinha(Result, Linha, '12', 'NP', 'Nota Promiss�ria', '');
            AddLinha(Result, Linha, '17', 'RC', 'Recibo', '');
            AddLinha(Result, Linha, '19', 'ND', 'Nota de D�bito', '');
            AddLinha(Result, Linha, '20', 'NS', 'Nota de Servi�o', '');
            AddLinha(Result, Linha, '99', ' ',  'Outros', '');
          end else
          if (NomeLayout = CO_756_240_2018) then
          begin
            AddLinha(Result, Linha, '01', 'CH',  'Cheque', '');
            AddLinha(Result, Linha, '02', 'DM',  'Duplicata Mercantil', '');
            AddLinha(Result, Linha, '03', 'DMI', 'Duplicata Mercantil p/ Indica��o', '');
            AddLinha(Result, Linha, '04', 'DS',  'Duplicata de Servi�o', '');
            AddLinha(Result, Linha, '05', 'DSI', 'Duplicata de Servi�o p/ Indica��o', '');
            AddLinha(Result, Linha, '06', 'DR',  'Duplicata Rural', '');
            AddLinha(Result, Linha, '07', 'LC',  'Letra de C�mbio', '');
            AddLinha(Result, Linha, '08', 'NCC', 'Nota de Cr�dito Comercial', '');
            AddLinha(Result, Linha, '09', 'NCE', 'Nota de Cr�dito a Exporta��o', '');
            AddLinha(Result, Linha, '10', 'NCI', 'Nota de Cr�dito Industrial', '');
            AddLinha(Result, Linha, '11', 'NCR', 'Nota de Cr�dito Rural', '');
            AddLinha(Result, Linha, '12', 'NP',  'Nota Promiss�ria', '');
            AddLinha(Result, Linha, '13', 'NPR', 'Nota Promiss�ria Rural', '');
            AddLinha(Result, Linha, '14', 'TM',  'Triplicata Mercantil', '');
            AddLinha(Result, Linha, '15', 'TS',  'Triplicata de Servi�o', '');
            AddLinha(Result, Linha, '16', 'NS',  'Nota de Seguro', '');
            AddLinha(Result, Linha, '17', 'RC',  'Recibo', '');
            AddLinha(Result, Linha, '18', 'FAT', 'Fatura', '');
            AddLinha(Result, Linha, '19', 'ND',  'Nota de D�bito', '');
            AddLinha(Result, Linha, '20', 'AP',  'Ap�lice de Seguro', '');
            AddLinha(Result, Linha, '21', 'ME',  'Mensalidade Escolar', '');
            AddLinha(Result, Linha, '22', 'PC',  'Parcela de Cons�rcio', '');
            AddLinha(Result, Linha, '23', 'NF',  'Nota Fiscal', '');
            AddLinha(Result, Linha, '24', 'DD',  'Documento de D�vida', '');
            AddLinha(Result, Linha, '25', '',    'C�dula de Produto Rural', '');
            AddLinha(Result, Linha, '99', '',    'Outros', '');
          end;
        end;
        400:
        begin
          if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then
          begin
            AddLinha(Result, Linha, '01', 'DM',  'Duplicata Mercantil', '');
            AddLinha(Result, Linha, '02', 'NP',  'Nota Promiss�ria', '');
            AddLinha(Result, Linha, '03', 'NS',  'Nota de Seguro', '');
            AddLinha(Result, Linha, '05', 'RC',  'Recibo', '');
            AddLinha(Result, Linha, '06', 'DR',  'Duplicata Rural', '');
            AddLinha(Result, Linha, '08', 'LC',  'Letra de C�mbio', '');
            AddLinha(Result, Linha, '09', ' ',   'Warrant', '');
            AddLinha(Result, Linha, '10', 'CH',  'Cheque', '');
            AddLinha(Result, Linha, '12', 'DS',  'Duplicata de Servi�o', '');
            AddLinha(Result, Linha, '13', 'ND',  'Nota de D�bito', '');
            AddLinha(Result, Linha, '14', 'TM',  'Triplicata Mercantil', '');
            AddLinha(Result, Linha, '15', 'TS',  'Triplicata de Servi�o', '');
            AddLinha(Result, Linha, '18', 'FAT', 'Fatura', '');
            AddLinha(Result, Linha, '20', 'AP',  'Ap�lice de Seguro', '');
            AddLinha(Result, Linha, '21', 'ME',  'Mensalidade Escolar', '');
            AddLinha(Result, Linha, '22', 'PC',  'Parcela de Cons�rcio', '');
            AddLinha(Result, Linha, '99', ' ',   'Outros"', '');
          end else
          if (NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015) then
          begin
            AddLinha(Result, Linha, '01', 'DM', 'Duplicata', '');
            AddLinha(Result, Linha, '02', 'NP', 'Nota Promiss�ria', '');
            AddLinha(Result, Linha, '03', 'NS', 'Nota de Seguro', '');
            AddLinha(Result, Linha, '05', 'RC', 'Recibo', '');
            AddLinha(Result, Linha, '10', 'LC', 'Letra de C�mbio', '');
            AddLinha(Result, Linha, '11', 'ND', 'Nota de D�bito', '');
            AddLinha(Result, Linha, '12', 'DS', 'Duplicata de Servi�o', '');
            AddLinha(Result, Linha, '99', ' ',  'Outros', '');
          end;
        end;
      end;
    end;
    {
    748:  VER EM
           M L A G e r a l.CNABEspecieDeDocumento(Banco: Integer; Especie: String): String;
          COPIAR DE L� E ELIMAN�-LO DE L�
    }
  end;
end;

function TUBco_rem.EspecieDoValor(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo,
  Impressao, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Impressao;
    Res[Linha][2] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case Banco of
    008, 033, 353:  // com registro
    begin
      case CNAB of
        400:
        begin
          AddLinha(Result, Linha, '00', '', 'Moeda corrente');
        end;
      end;
    end;
    237:  // com registro!!!
    begin
      case CNAB of
        400:
        begin
          {if NomeLayout = CO_237_MP_4008_0121_01_Data_11_11_2010 then}
          // N�o menciiona nada no manual, ent�o vou fazer igual aos outros BRL
          begin
            AddLinha(Result, Linha, '', 'R$', 'Real');
          end;
        end;
      end;
    end;
    399:  // com registro
    begin
      case CNAB of
        400:
        begin
          AddLinha(Result, Linha, '2', '', 'D�lar comercial - Venda');
          AddLinha(Result, Linha, '3', '', 'D�lar Turismo - Venda');
          AddLinha(Result, Linha, '9', '', 'Real');
          AddLinha(Result, Linha, 'A', '', 'IGPM/93 - (Base Nova Mensal FGV)');
        end;
      end;
    end;
    409:  // sem registro!!!
    begin
      case CNAB of
        400:
        begin
          AddLinha(Result, Linha, ''     , '', 'Real');
          AddLinha(Result, Linha, 'R$ 14', '', 'Real');
          AddLinha(Result, Linha, 'US$4' , '', 'Dollar');
          AddLinha(Result, Linha, 'TRM5' , '', 'TR');
          AddLinha(Result, Linha, 'IG51' , '', 'IGPM');
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        240:
        begin
          if (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
          begin
            AddLinha(Result, Linha, '09', 'R$', 'Real');
          end else
          if (NomeLayout = CO_756_240_2018) then
          begin
            AddLinha(Result, Linha, '02', '$',  'D�lar Americano Comercial (Venda)');
            AddLinha(Result, Linha, '09', 'R$', 'Real');
          end;
        end;
        400:
        begin
          if (NomeLayout = CO_756_EXCEL_2012_03_14) or
            (NomeLayout = CO_756_EXCEL_2013_07_18) or
            (NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015) then
          begin
            AddLinha(Result, Linha, '09', 'R$', 'Real');
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.FileEnd(const Banco, CNAB: Integer;
  const NomeLayout: String): Variant;
begin
  Result := Null;
  case Banco of
    399:
    begin
      case CNAB of
        400:
        begin
          if NomeLayout = CO_399_MODULO_I_2009_10 then Result := #26;          
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.InstrucaoDeCobranca(const Banco, CNAB, Comando: Integer;
NomeLayout: String): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer;
  const Coment, Codigo, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    Res[Linha][2] :=  Coment;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001:  // com registro
    begin
      case CNAB of
        400:
        begin
          case Comando of
            01:
            begin
              AddLinha(Result, Linha, '', '00', 'Sem de instru��es');
              AddLinha(Result, Linha, '', '01', 'Cobrar juros (Dispens�vel se informado o valor a ser cobrado por dia de atraso).');
              AddLinha(Result, Linha, '', '03', 'Protestar no 3� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '04', 'Protestar no 4� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '05', 'Protestar no 5� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '10', 'Protestar no 10� dia corrido ap�s vencido');
              AddLinha(Result, Linha, '', '15', 'Protestar no 15� dia corrido ap�s vencido');
              AddLinha(Result, Linha, '', '20', 'Protestar no 20� dia corrido ap�s vencido');
              AddLinha(Result, Linha, '', '25', 'Protestar no 25� dia corrido ap�s vencido');
              AddLinha(Result, Linha, '', '30', 'Protestar no 30� dia corrido ap�s vencido');
              AddLinha(Result, Linha, '', '45', 'Protestar no 45� dia corrido ap�s vencido');
              AddLinha(Result, Linha, '', '06', 'Indica Protesto em dias corridos, com prazo de 6 a 29, 35 ou 40 dias Corridos. Obrigat�rio impostar, nas posi��es 392 a 393 o prazo de protesto desejado:  6 a 29, 35 ou 40 dias.');
              AddLinha(Result, Linha, '', '07', 'N�o protestar');
              AddLinha(Result, Linha, '', '22', 'Conceder desconto s� at� a data estipulada');
            end;
            02:
            begin
              AddLinha(Result, Linha, '', '42', 'Devolver');
              AddLinha(Result, Linha, '', '44', 'Baixar');
              AddLinha(Result, Linha, '', '46', 'Entregar ao sacado franco de pagamento');
            end;
            09:
            begin
              AddLinha(Result, Linha, '', '00', 'O Sistema de Cobran�a do Banco assumir� o prazo de protesto de 5 (cinco) dias �teis');
              AddLinha(Result, Linha, '', '03', 'Protestar no 3� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '04', 'Protestar no 4� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '05', 'Protestar no 5� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '06', 'Protestar no 6� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '07', 'Protestar no 7� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '08', 'Protestar no 8� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '09', 'Protestar no 9� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '10', 'Protestar no 10� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '11', 'Protestar no 11� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '12', 'Protestar no 12� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '13', 'Protestar no 13� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '14', 'Protestar no 14� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '15', 'Protestar no 15� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '16', 'Protestar no 16� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '17', 'Protestar no 17� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '18', 'Protestar no 18� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '19', 'Protestar no 19� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '20', 'Protestar no 20� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '21', 'Protestar no 21� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '22', 'Protestar no 22� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '23', 'Protestar no 23� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '24', 'Protestar no 24� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '25', 'Protestar no 25� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '26', 'Protestar no 26� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '27', 'Protestar no 27� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '28', 'Protestar no 28� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '29', 'Protestar no 29� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '30', 'Protestar no 30� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '35', 'Protestar no 35� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '40', 'Protestar no 40� dia �til ap�s vencido');
              AddLinha(Result, Linha, '', '45', 'Protestar no 45� dia �til ap�s vencido');
            end;
          end;
        end;
      end;
    end;
    008, 033, 353:  // com registro
    begin
      case CNAB of
        400:
        begin
          AddLinha(Result, Linha, '', '00', 'N�O H� INSTRU��ES');
          AddLinha(Result, Linha, '', '02', 'BAIXAR AP�S QUINZE DIAS DO VENCIMENTO');
          AddLinha(Result, Linha, '', '03', 'BAIXAR AP�S 30 DIAS DO VENCIMENTO');
          AddLinha(Result, Linha, '', '04', 'N�O BAIXAR');
          AddLinha(Result, Linha, '', '06', 'PROTESTAR (VIDE POSI��O 392/393)');
          AddLinha(Result, Linha, '', '07', 'N�O PROTESTAR');
          AddLinha(Result, Linha, '', '08', 'N�O COBRAR JUROS DE MORA');
        end;
      end;
    end;
    237:  // com registro
    begin
      case CNAB of
        400:
        begin
          {if NomeLayout = CO_237_MP_4008_0121_01_Data_11_11_2010 then}
          begin
            AddLinha(Result, Linha, '', '08', 'N�o cobrar juros de mora');
            AddLinha(Result, Linha, '', '09', 'N�o receber ap�s o vencimento');
            AddLinha(Result, Linha, '', '10', 'Multa de 10% ap�s o 4� dia do Vencimento.');
            AddLinha(Result, Linha, '', '11', 'N�o receber ap�s o 8� dia do vencimento.');
            AddLinha(Result, Linha, '', '12', 'Cobrar encargos ap�s o 5� dia do vencimento.');
            AddLinha(Result, Linha, '', '13', 'Cobrar encargos ap�s o 10� dia do vencimento.');
            AddLinha(Result, Linha, '', '14', 'Cobrar encargos ap�s o 15� dia do vencimento');
            AddLinha(Result, Linha, '', '15', 'Conceder desconto mesmo se pago ap�s o vencimento.');
          end;
        end;
      end;
    end;
    341: // com registro
    begin
      case CNAB of
        400:
        begin
          if NomeLayout = CO_341_COBRANCA_BANCARIA_MARCO_2012 then
          begin
            AddLinha(Result, Linha, '', '02', 'DEVOLVER  AP�S 05 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '03', 'DEVOLVER  AP�S 30 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '05', 'RECEBER CONFORME INSTRU��ES NO PR�PRIO T�TULO');
            AddLinha(Result, Linha, '', '06', 'DEVOLVER  AP�S 10 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '07', 'DEVOLVER  AP�S 15 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '08', 'DEVOLVER  AP�S 20 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, 'A','09', 'PROTESTAR  (emite aviso ao sacado ap�s xx dias do vencimento, e envia ao cart�rio ap�s 5 dias �teis)');
            AddLinha(Result, Linha, 'G','10', 'N�O PROTESTAR (inibe protesto, quando houver instru��o permanente na conta corrente)');
            AddLinha(Result, Linha, '', '11', 'DEVOLVER  AP�S 25 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '12', 'DEVOLVER  AP�S 35 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '13', 'DEVOLVER  AP�S 40 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '14', 'DEVOLVER  AP�S 45 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '15', 'DEVOLVER  AP�S 50 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '16', 'DEVOLVER  AP�S 55 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '17', 'DEVOLVER  AP�S 60 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '18', 'DEVOLVER  AP�S 90 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '19', 'N�O RECEBER AP�S 05 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '20', 'N�O RECEBER AP�S 10 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '21', 'N�O RECEBER AP�S 15 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '22', 'N�O RECEBER AP�S 20 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '23', 'N�O RECEBER AP�S 25 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '24', 'N�O RECEBER AP�S 30 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '25', 'N�O RECEBER AP�S 35 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '26', 'N�O RECEBER AP�S 40 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '27', 'N�O RECEBER AP�S 45 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '28', 'N�O RECEBER AP�S 50 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '29', 'N�O RECEBER AP�S 55 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, 'E','30', 'IMPORT�NCIA DE DESCONTO POR DIA');
            AddLinha(Result, Linha, '', '31', 'N�O RECEBER AP�S 60 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '32', 'N�O RECEBER AP�S 90 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '37', 'RECEBER AT� O �LTIMO DIA DO M�S DE VENCIMENTO');
            AddLinha(Result, Linha, '', '38', 'CONCEDER DESCONTO MESMO AP�S VENCIMENTO');
            AddLinha(Result, Linha, '', '39', 'N�O RECEBER AP�S O VENCIMENTO');
            AddLinha(Result, Linha, '', '40', 'CONCEDER DESCONTO CONFORME NOTA DE CR�DITO');
            AddLinha(Result, Linha, '', '43', 'SUJEITO A PROTESTO SE N�O FOR PAGO NO VENCIMENTO');
            AddLinha(Result, Linha, 'F','44', 'IMPORT�NCIA POR DIA DE ATRASO A PARTIR DE DDMMAA');
            AddLinha(Result, Linha, '', '45', 'TEM DIA DA GRA�A');
            AddLinha(Result, Linha, '', '46', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '47', 'DISPENSAR JUROS/COMISS�O DE PERMAN�NCIA');
            AddLinha(Result, Linha, '', '51', 'RECEBER SOMENTE COM A PARCELA ANTERIOR QUITADA');
            AddLinha(Result, Linha, '', '52', 'FAVOR EFETUAR PGTO  SOMENTE ATRAV�S DESTA COBRAN�A BANC�RIA');
            AddLinha(Result, Linha, '', '53', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '54', 'AP�S VENCIMENTO PAG�VEL SOMENTE NA EMPRESA');
            AddLinha(Result, Linha, '', '56', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '57', 'SOMAR VALOR DO T�TULO AO VALOR DO CAMPO MORA/MULTA CASO EXISTA');
            AddLinha(Result, Linha, '', '58', 'DEVOLVER AP�S 365 DIAS DE VENCIDO');
            AddLinha(Result, Linha, '', '59', 'COBRAN�A NEGOCIADA. PAG�VEL SOMENTE POR ESTE BLOQUETO NA REDE BANC�RIA');
            AddLinha(Result, Linha, '', '61', 'T�TULO ENTREGUE EM PENHOR EM FAVOR DO CEDENTE ACIMA');
            AddLinha(Result, Linha, '', '70', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '71', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '72', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '73', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '74', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '75', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '78', 'VALOR DA IDA ENGLOBA MULTA DE 10% PRO RATA');
            AddLinha(Result, Linha, '', '79', 'COBRAR JUROS AP�S 15 DIAS DA EMISS�O (para t�tulos com vencimento � vista)');
            AddLinha(Result, Linha, '', '80', 'PAGAMENTO EM CHEQUE: SOMENTE RECEBER COM CHEQUE DE EMISS�O DO SACADO');
            AddLinha(Result, Linha, 'A,H','81', 'PROTESTAR AP�S XX DIAS CORRIDOS DO VENCIMENTO');
            AddLinha(Result, Linha, 'A,H','82', 'PROTESTAR AP�S XX DIAS �TEIS DO VENCIMENTO');
            AddLinha(Result, Linha, '', '83', 'OPERA��O REF A VENDOR');
            AddLinha(Result, Linha, '', '84', 'AP�S VENCIMENTO CONSULTAR A AG�NCIA CEDENTE');
            AddLinha(Result, Linha, '', '86', 'ANTES DO VENCIMENTO OU AP�S 15 DIAS, PAG�VEL SOMENTE EM NOSSA SEDE');
            AddLinha(Result, Linha, '', '87', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '88', 'N�O RECEBER ANTES DO VENCIMENTO');
            AddLinha(Result, Linha, '', '89', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '90', 'NO VENCIMENTO PAG�VEL EM QUALQUER AG�NCIA BANC�RIA');
            AddLinha(Result, Linha, 'A','91', 'N�O RECEBER AP�S XX DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, 'A','92', 'DEVOLVER AP�S XX DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, 'B','93', 'MENSAGENS NOS BLOQUETOS COM 30 POSI��ES');
            AddLinha(Result, Linha, 'C','94', 'MENSAGENS NOS BLOQUETOS COM 40 POSI��ES');
            AddLinha(Result, Linha, '', '95', 'USO DO BANCO');
            AddLinha(Result, Linha, '', '96', 'USO DO BANCO');
            AddLinha(Result, Linha, 'A,B,C', '97', 'USO DO BANCO');
            AddLinha(Result, Linha, 'D','98', 'DUPLICATA / FATURA N�');
          end else
          if NomeLayout = CO_341_COBRANCA_BANCARIA_SETEMBRO_2012 then
          begin
            AddLinha(Result, Linha, '',     '02', 'DEVOLVER  AP�S 05 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '03', 'DEVOLVER  AP�S 30 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '05', 'RECEBER CONFORME INSTRU��ES NO PR�PRIO T�TULO');
            AddLinha(Result, Linha, '',     '06', 'DEVOLVER  AP�S 10 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '07', 'DEVOLVER  AP�S 15 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '08', 'DEVOLVER  AP�S 20 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, 'A',    '09', 'PROTESTAR  (emite aviso ao sacado ap�s xx dias do vencimento, e envia ao cart�rio ap�s 5 dias �teis)');
            AddLinha(Result, Linha, 'G',    '10', 'N�O PROTESTAR (inibe protesto, quando houver instru��o permanente na conta corrente)');
            AddLinha(Result, Linha, '',     '11', 'DEVOLVER  AP�S 25 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '12', 'DEVOLVER  AP�S 35 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '13', 'DEVOLVER  AP�S 40 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '14', 'DEVOLVER  AP�S 45 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '15', 'DEVOLVER  AP�S 50 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '16', 'DEVOLVER  AP�S 55 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '17', 'DEVOLVER  AP�S 60 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '18', 'DEVOLVER  AP�S 90 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '19', 'N�O RECEBER AP�S 05 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '20', 'N�O RECEBER AP�S 10 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '21', 'N�O RECEBER AP�S 15 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '22', 'N�O RECEBER AP�S 20 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '23', 'N�O RECEBER AP�S 25 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '24', 'N�O RECEBER AP�S 30 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '25', 'N�O RECEBER AP�S 35 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '26', 'N�O RECEBER AP�S 40 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '27', 'N�O RECEBER AP�S 45 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '28', 'N�O RECEBER AP�S 50 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '29', 'N�O RECEBER AP�S 55 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, 'E',    '30', 'IMPORT�NCIA DE DESCONTO POR DIA');
            AddLinha(Result, Linha, '',     '31', 'N�O RECEBER AP�S 60 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '32', 'N�O RECEBER AP�S 90 DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, 'I',    '33', 'CONCEDER ABATIMENTO REF. � PIS-PASEP/COFIN/CSSL, MESMO AP�S VENCIMENTO');
            AddLinha(Result, Linha, 'A, H', '34', 'PROTESTAR AP�S XX DIAS CORRIDOS DO VENCIMENTO (SEM AVISO AO SACADO)');
            AddLinha(Result, Linha, 'A, H', '35', 'PROTESTAR AP�S XX DIAS �TEIS DO VENCIMENTO (SEM AVISO AO SACADO)'); 
            AddLinha(Result, Linha, '',     '37', 'RECEBER AT� O �LTIMO DIA DO M�S DE VENCIMENTO');
            AddLinha(Result, Linha, '',     '38', 'CONCEDER DESCONTO MESMO AP�S VENCIMENTO');
            AddLinha(Result, Linha, '',     '39', 'N�O RECEBER AP�S O VENCIMENTO');
            AddLinha(Result, Linha, '',     '40', 'CONCEDER DESCONTO CONFORME NOTA DE CR�DITO');
            AddLinha(Result, Linha, 'A',    '42', 'PROTESTO PARA FINS FALIMENTARES');
            AddLinha(Result, Linha, '',     '43', 'SUJEITO A PROTESTO SE N�O FOR PAGO NO VENCIMENTO');
            AddLinha(Result, Linha, 'F',    '44', 'IMPORT�NCIA POR DIA DE ATRASO A PARTIR DE DDMMAA');
            AddLinha(Result, Linha, '',     '45', 'TEM DIA DA GRA�A');
            AddLinha(Result, Linha, '',     '46', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '47', 'DISPENSAR JUROS/COMISS�O DE PERMAN�NCIA');
            AddLinha(Result, Linha, '',     '51', 'RECEBER SOMENTE COM A PARCELA ANTERIOR QUITADA');
            AddLinha(Result, Linha, '',     '52', 'FAVOR EFETUAR PGTO  SOMENTE ATRAV�S DESTA COBRAN�A BANC�RIA');
            AddLinha(Result, Linha, '',     '53', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '54', 'AP�S VENCIMENTO PAG�VEL SOMENTE NA EMPRESA');
            AddLinha(Result, Linha, '',     '56', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '57', 'SOMAR VALOR DO T�TULO AO VALOR DO CAMPO MORA/MULTA CASO EXISTA');
            AddLinha(Result, Linha, '',     '58', 'DEVOLVER AP�S 365 DIAS DE VENCIDO');
            AddLinha(Result, Linha, '',     '59', 'COBRAN�A NEGOCIADA. PAG�VEL SOMENTE POR ESTE BLOQUETO NA REDE BANC�RIA');
            AddLinha(Result, Linha, '',     '61', 'T�TULO ENTREGUE EM PENHOR EM FAVOR DO CEDENTE ACIMA');
            AddLinha(Result, Linha, '',     '62', 'T�TULO TRANSFERIDO A FAVOR DO CEDENTE');
            AddLinha(Result, Linha, '',     '70', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '71', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '72', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '73', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '74', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '75', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '78', 'VALOR DA IDA ENGLOBA MULTA DE 10% PRO RATA');
            AddLinha(Result, Linha, '',     '79', 'COBRAR JUROS AP�S 15 DIAS DA EMISS�O (para t�tulos com vencimento � vista)');
            AddLinha(Result, Linha, '',     '80', 'PAGAMENTO EM CHEQUE: SOMENTE RECEBER COM CHEQUE DE EMISS�O DO SACADO');
            AddLinha(Result, Linha, '',     '83', 'OPERA��O REF A VENDOR');
            AddLinha(Result, Linha, '',     '84', 'AP�S VENCIMENTO CONSULTAR A AG�NCIA CEDENTE');
            AddLinha(Result, Linha, '',     '86', 'ANTES DO VENCIMENTO OU AP�S 15 DIAS, PAG�VEL SOMENTE EM NOSSA SEDE');
            AddLinha(Result, Linha, '',     '87', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '88', 'N�O RECEBER ANTES DO VENCIMENTO');
            AddLinha(Result, Linha, '',     '89', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '90', 'NO VENCIMENTO PAG�VEL EM QUALQUER AG�NCIA BANC�RIA');
            AddLinha(Result, Linha, 'A',    '91', 'N�O RECEBER AP�S XX DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, 'A',    '92', 'DEVOLVER AP�S XX DIAS DO VENCIMENTO');
            AddLinha(Result, Linha, 'B',    '93', 'MENSAGENS NOS BLOQUETOS COM 30 POSI��ES');
            AddLinha(Result, Linha, 'C',    '94', 'MENSAGENS NOS BLOQUETOS COM 40 POSI��ES');
            AddLinha(Result, Linha, '',     '95', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '96', 'USO DO BANCO');
            AddLinha(Result, Linha, '',     '97', 'USO DO BANCO');
            AddLinha(Result, Linha, 'D',    '98', 'DUPLICATA / FATURA N�');
          end;
        end;
      end;
    end;
    399: // com registro
    begin
      case CNAB of
        400:
        begin
          if NomeLayout = CO_399_MODULO_I_2009_10 then
          begin
            // INSTRU��ES que GERAM mensagens nos bloquetos
            AddLinha(Result, Linha, '1,A', '15', 'Multa de ...... por cento ap�s dia .../.../...');
            AddLinha(Result, Linha, '1,B', '16', 'Ap�s .../... multa dia de ............... m�ximo ...............');
            AddLinha(Result, Linha, '1,C', '19', 'Multa de R$ _____ap�s ____ dias corridos do vencimento.');
            AddLinha(Result, Linha, '1',   '20', 'Cobrar juros s� ap�s 07 dias do vencimento');
            AddLinha(Result, Linha, '1,C', '22', 'Multa de R$ _____ap�s ____ dias �teis do vencimento');
            AddLinha(Result, Linha, '1',   '23', 'N�o receber ap�s o vencimento');
            AddLinha(Result, Linha, '1,D', '24', 'Multa de R$ _____ap�s o vencimento');
            AddLinha(Result, Linha, '1,E', '29', 'Juros s� ap�s .../.../..., cobrar desde o vencimento');
            AddLinha(Result, Linha, '1',   '34', 'Conceder abatimento conforme proposto pelo sacado');
            AddLinha(Result, Linha, '1',   '36', 'Ap�s vencimento multa de 10 por cento.');
            AddLinha(Result, Linha, '1',   '40', 'Conceder desconto mesmo se pago ap�s o vencimento');
            AddLinha(Result, Linha, '1',   '42', 'N�o receber antes do vencimento - instru��o do cedente');
            AddLinha(Result, Linha, '1',   '53', 'Ap�s vencimento multa de 20% mais mora de 1% ao m�s');
            AddLinha(Result, Linha, '1',   '56', 'N�o receber antes do vencimento ou 10 dias ap�s');
            AddLinha(Result, Linha, '1',   '65', 'Abatimento/desconto s� com instru��o do cedente.');
            AddLinha(Result, Linha, '1',   '67', 'T�tulo sujeito a protesto ap�s o vencimento');
            AddLinha(Result, Linha, '1',   '68', 'p�s o vencimento multa de 2 por cento.');
            AddLinha(Result, Linha, '1,F', '71', 'N�o receber ap�s ......dias corridos do vencimento.');
            AddLinha(Result, Linha, '1,F', '72', 'N�o receber ap�s ......dias �teis do vencimento.');
            AddLinha(Result, Linha, '1,G', '73', 'Multa de ..... por cento ap�s ...... dias corridos do vencimento.');
            AddLinha(Result, Linha, '1,G', '74', 'Multa de ...... por cento ap�s ...... dias �teis do vencimento.');
            AddLinha(Result, Linha, '1,H', '75', 'Protestar ...... dias corridos ap�s o vencimento se n�o pago.');
            AddLinha(Result, Linha, '1,H', '77', 'Protestar ...... dias �teis ap�s o vencimento se n�o pago.');
            // INSTRU��ES que N�O geram mensagens nos bloquetos (ARQUIVO REMESSA)
            AddLinha(Result, Linha, '2,I', '76', 'Protestar ......dias �teis ap�s o vencimento, se n�o pago.');
            AddLinha(Result, Linha, '2,I', '84', 'Protestar ......dias corridos ap�s o vencimento, se n�o pago.');
          end else
            //Geral.MB_Aviso(
            //'Verifique se o layout de remessa foi definido corretamente!',
        end;
      end;
    end;
    756: // com registro
    begin
      case CNAB of
        400:
        begin
          if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then
          begin
            AddLinha(Result, Linha, '', '00', 'Sem de instru��es');
////////////////////////////////////////////////////////////////////////////////
/// Primeira instru��o codificada:
/// Regras de impress�o de mensagens nos boletos:
/// * Primeira instru��o (SEQ 34) = 00 e segunda (SEQ 35) = 00, n�o imprime nada.
/// * Primeira instru��o (SEQ 34) = 01 e segunda (SEQ 35) = 01, desconsideramos
///   as instru��es CNAB e imprimimos nos boletos as mensagens relatadas no
///   trailler do arquivo.
/// * Primeira e segunda instru��o diferente das situa��es acima, imprimimos o
///   conte�do CNAB:
            AddLinha(Result, Linha, 'dmk', '00', 'AUSENCIA DE INSTRUCOES');
            AddLinha(Result, Linha, 'dmk', '01', 'COBRAR JUROS');
            AddLinha(Result, Linha, 'dmk', '03', 'PROTESTAR 3 DIAS UTEIS APOS VENCIMENTO');
            AddLinha(Result, Linha, 'dmk', '04', 'PROTESTAR 4 DIAS UTEIS APOS VENCIMENTO');
            AddLinha(Result, Linha, 'dmk', '05', 'PROTESTAR 5 DIAS UTEIS APOS VENCIMENTO');
            AddLinha(Result, Linha, 'dmk', '07', 'NAO PROTESTAR');
            AddLinha(Result, Linha, 'dmk', '10', 'PROTESTAR 10 DIAS UTEIS APOS VENCIMENTO');
            AddLinha(Result, Linha, 'dmk', '15', 'PROTESTAR 15 DIAS UTEIS APOS VENCIMENTO');
            AddLinha(Result, Linha, 'dmk', '20', 'PROTESTAR 20 DIAS UTEIS APOS VENCIMENTO');
            AddLinha(Result, Linha, 'dmk', '22', 'CONCEDER DESCONTO SO ATE DATA ESTIPULADA');
            AddLinha(Result, Linha, 'dmk', '42', 'DEVOLVER APOS 15 DIAS VENCIDO');
            AddLinha(Result, Linha, 'dmk', '43', 'DEVOLVER APOS 30 DIAS VENCIDO"');
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.AvisosDeInstrucaoDeCobranca(const Banco, CNAB: Integer;
NomeLayout: String): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer;
  const Codigo: String; Descricao: array of String);
  var
    i: Integer;
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 2);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  '';
    for i := Low(Descricao) to High(Descricao) do
      Res[Linha][1] :=  Res[Linha][1] + Descricao[i];
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    237: // com registro
    begin
      case CNAB of
        400:
        begin
          if NomeLayout = CO_237_MP_4008_0121_01_Data_11_11_2010 then
          begin
            AddLinha(Result, Linha, 'dmk', [
            //'Essas instru��es dever�o ser enviadas no Arquivo-Remessa, ',
            'Essa instru��o dever� ser enviada no Arquivo-Remessa, ',
            'quando da entrada, desde que o c�digo de ocorr�ncia na posi��o',
            ' 109 a 110 do registro de transa��o, seja �01�, para as ',
            'instru��es de protesto, o CNPJ / CPF e o endere�o do Sacado ',
            'dever�o ser informados corretamente.']);
          end;
        end;
      end;
    end;
    341: // com registro
    begin
      case CNAB of
        400:
        begin
          if NomeLayout = CO_341_COBRANCA_BANCARIA_MARCO_2012 then
          begin
            AddLinha(Result, Linha, 'A', ['Informar a quantidade de dias nas posi��es 392 a 393.']);
            AddLinha(Result, Linha, 'B', ['Informar a mensagem nas posi��es 352 a 381;', ' o conte�do da mensagem ser� informado na primeira linha dispon�vel no campo de "instru��es" do bloqueto.', ' Caso todas as linhas deste campo estejam ocupadas, a mensagem ser� impressa acima do campo "Sacador / Avalista".', ' Utilizando-se deste campo para instru��o "93", para indica��o do nome e dados do sacador / avalista, deve-se utilizar do registro tipo "5".']);
            AddLinha(Result, Linha, 'C', ['Informar a mensagem nas posi��es 352 a 391;', ' o conte�do da mensagem ser� informado na primeira linha dispon�vel no campo de "instru��es" do bloqueto.', ' Caso todas as linhas deste campo estejam ocupadas, a mensagem ser� impressa nos campos "Sacador / Avalista" e "data da mora".', ' Utilizando-se deste campo para instru��o "94", para indica��o do nome e dados do sacador / avalista, deve-se utilizar do registro tipo "5".']);
            AddLinha(Result, Linha, 'D', ['Informar o n�mero da Duplicata/Fatura nas posi��es 087 a 106. Se este campo estiver com brancos ou zeros, a mensagem n�o ser� impressa.']);
            AddLinha(Result, Linha, 'E', ['Informar o valor do desconto por dia nas posi��es 180 a 192.']);
            AddLinha(Result, Linha, 'F', ['Informar o valor por dia de atraso nas posi��es 161 a 173 e a data nas posi��es 386 a 391.']);
            AddLinha(Result, Linha, 'G', ['Pode ser cancelada pela ag�ncia, bankline ou atrav�s de arquivo, C�digo de Ocorr�ncia 35,   Nota 6, (utilizando a instru��o 2196). Depois de cancelada, comandar a instru��o de protesto novamente.']);
            AddLinha(Result, Linha, 'H', ['� impressa mensagem no bloqueto informando prazo de protesto. Emite aviso ao sacado ap�s o prazo cadastrado, e envia ao cart�rio ap�s 5 dias �teis.']);
          end else
          if NomeLayout = CO_341_COBRANCA_BANCARIA_SETEMBRO_2012 then
          begin
            AddLinha(Result, Linha, 'A', ['Informar a quantidade de dias nas posi��es 392 a 393.', 'No caso da instru��o �42�, o cedente passa a ter prioridade no recebimento quando o sacado estiver com fal�ncia decretada.', 'No caso da instru��o �39�, se informar �00� ser� impresso no boleto a literal �N�O RECEBER AP�S O VENCIMENTO�']);
            AddLinha(Result, Linha, 'B', ['Informar a mensagem nas posi��es 352 a 381; o conte�do da mensagem ser� informado na primeira linha dispon�vel no campo de �instru��es� do BOLETO.', 'Caso todas as linhas deste campo estejam ocupadas, a mensagem ser� impressa acima do campo "Sacador / Avalista".', 'Utilizando-se deste campo para instru��o �93�, para indica��o do nome e dados do sacador / avalista, deve-se utilizar-se do registro tipo �1� ou do registro tipo �5�.']);
            AddLinha(Result, Linha, 'C', ['Informar a mensagem nas posi��es 352 a 391; o conte�do da mensagem ser� informado na primeira linha dispon�vel no campo de �instru��es� do BOLETO.', 'Caso todas as linhas deste campo estejam ocupadas, a mensagem ser� impressa nos campos "Sacador / Avalista" e �data da mora�.', 'Utilizando-se deste campo para instru��o �94�, para indica��o do nome e dados do sacador / avalista, deve-se utilizar-se do registro tipo �1� ou do registro tipo �5�.']);
            AddLinha(Result, Linha, 'D', ['Informar o n�mero da Duplicata/Fatura nas posi��es 087 a 106. Se este campo estiver com brancos ou zeros, a mensagem n�o ser� impressa.']);
            AddLinha(Result, Linha, 'E', ['Informar o valor do desconto por dia nas posi��es 180 a 192.']);
            AddLinha(Result, Linha, 'F', ['Informar o valor por dia de atraso nas posi��es 161 a 173 e a data nas posi��es 386 a 391.']);
            AddLinha(Result, Linha, 'G', ['Pode ser cancelada pela ag�ncia, bankline ou atrav�s de arquivo, C�digo de Ocorr�ncia 35, Nota 6, (utilizando a instru��o 2196). Depois de cancelada, comandar a instru��o de protesto novamente.']);
            AddLinha(Result, Linha, 'H', ['� impressa mensagem no BOLETO informando prazo de protesto.']);
            AddLinha(Result, Linha, 'I', ['Informar o VALOR do abatimento (nunca em percentual) referente a PIS-PASEP/COFIN/CSSL nas posi��es 206 a 218. A instru��o ser� impressa no BOLETO com a literal: �ABATIMENTO DE XXXX.XXX,XX REF. PIS-PASEP/COFIN/CSSL, MESMO APOS VCTO�.']);
          end;
        end;
      end;
    end;
    399: // com registro
    begin
      case CNAB of
        400:
        begin
          if NomeLayout = CO_399_MODULO_I_2009_10 then
          begin
            AddLinha(Result, Linha, '1', ['ESTA INSTRU��O GERA MENSAGEM NOS BLOQUETOS']);
            AddLinha(Result, Linha, '2', ['ESTA INSTRU��O N�O GERA MENSAGEM NOS BLOQUETOS']);
            //
            AddLinha(Result, Linha, 'A', ['Quando utilizar a instru��o 15, preencher as posi��es 206 a 218 ',
                                      sLineBreak, 'do registro Detalhe (arquivo Remessa), da seguinte forma:',
                                      sLineBreak, '          Posi��es 206 a 211 - Data de in�cio da multa DDMMAA (Dia/M�s/Ano)',
                                      sLineBreak, '          Posi��es 212 a 215 - Taxa (com 02 decimais)',
                                      sLineBreak, '          Posi��es 216 a 218 - Brancos']);
            AddLinha(Result, Linha, 'B', ['Quando utilizar a instru��o 16, preencher as posi��es 193 a 216 ',
                                      sLineBreak, 'do registro Detalhe (arquivo Remessa), da seguinte forma: ',
                                      sLineBreak, '     Posi��es 193 a 205 - Valor di�rio da multa (quando indicado "V" na',
                                      sLineBreak, '                          posi��o 216). Caso contr�rio, deixar em branco.',
                                      sLineBreak, '     Posi��es 206 a 211 - Data in�cio da multa (Dia/M�s/Ano)',
                                      sLineBreak, '     Posi��es 212 a 215 - Se inserido "V" na posi��o 216, deixar em branco.',
                                      sLineBreak, '                          Se branco na posi��o 216 informar taxa mensal com 02 decimais.',
                                      sLineBreak, '     Posi��o 216 -        Deixar em branco caso identificado taxa nas posi��es 212 a 215.',
                                      sLineBreak, '                          Indicar "V" quando informado o valor di�rio da',
                                      sLineBreak, '                          multa nas posi��es 193 a 205.',
                                      sLineBreak, 'Nota -N�o utilizar a instru��o 16 ( multa pr�-rata ), para tipo de moeda diferente de "Real".']);
            AddLinha(Result, Linha, 'C', ['Obs.: Para as instru��es 19 e 22, quando utilizadas, preencher as posi��es 206 a 218 ',
                                      sLineBreak, 'do registro detalhe (arquivo Remessa), da seguinte forma:',
                                      sLineBreak, '     Posi��es 206 a 215 � Valor da Multa, em moeda Real com duas decimais.',
                                      sLineBreak, '     Posi��es 216 a 218 � Quantidade de dias da multa.']);
            AddLinha(Result, Linha, 'D', ['Quando utilizar a instru��o 24, preencher as posi��es 206 a 218 ',
                                      sLineBreak, 'do registro Detalhe (arquivo Remessa), da seguinte forma:',
                                      sLineBreak, '     Posi��es 206 a 215 � Valor da Multa, em moeda Real com duas decimais.',
                                      sLineBreak, '     Posi��es 216 a 218 � Preencher com zeros.',
                                      sLineBreak, 'Obs.: Para a instru��o 24, a data da multa ir� iniciar a partir do vencimento.']);
            AddLinha(Result, Linha, 'E', ['Quando utilizar a instru��o 29, informar a data nas posi��es',
                                      sLineBreak, '206 a 211 do registro Detalhe (arquivo Remessa).']);
            AddLinha(Result, Linha, 'F', ['Obs.: Para as instru��es 71 e 72, preencher nas posi��es 313 a 314',
                                      sLineBreak, 'do registro detalhe remessa, o n�mero de dias. ']);
            AddLinha(Result, Linha, 'G', ['Obs.: Para as instru��es 73 e 74, quando utilizadas, preencher',
                                      sLineBreak, 'as posi��es 206 a 218 do registro detalhe da seguinte forma: ',
                                      sLineBreak, '     Posi��es 206 a 211 � Brancos',
                                      sLineBreak, '     Posi��es 212 a 215 � Taxa (com 02 decimais).',
                                      sLineBreak, '     Posi��es 216 a 218 � N�mero de dias.']);
            AddLinha(Result, Linha, 'H', ['Obs.: Para as instru��es 75 e 77, dever� ser informado nos',
                                      sLineBreak, 'campos 392 a 393 do registro detalhe remessa, a quantidade de',
                                      sLineBreak, 'dias, que tem intervalo de 02 a 45 dias para a instru��o 75 e de',
                                      sLineBreak, '02 a 35 dias para a instru��o 77.']);
            AddLinha(Result, Linha, 'I', ['Obs.: Para as instru��es 76 e 84, dever� ser informado nos ',
                                      sLineBreak, 'campos 392 a 393 do registro detalhe remessa, a quantidade de ',
                                      sLineBreak, 'dias, que tem intervalo de 02 a 35 dias para a instru��o 76 e de ',
                                      sLineBreak, '02 a 45 dias para a instru��o 84.']);
          end;
        end;
      end;
    end;
    756: // com registro
    begin
      case CNAB of
        400:
        begin
          if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then
          begin
            AddLinha(Result, Linha, 'dmk', ['Instru��o codificada (primeira e segunda): ',
                                      sLineBreak, 'Regras de impress�o de mensagens nos boletos:',
                                      sLineBreak, '* Primeira instru��o (SEQ 34) = 00 e segunda (SEQ 35) = 00, n�o imprime nada.',
                                      sLineBreak, '* Primeira instru��o (SEQ 34) = 01 e segunda (SEQ 35) = 01, desconsideramos as instru��es CNAB e imprimimos nos boletos as mensagens relatadas no trailler do arquivo.',
                                      sLineBreak, '* Primeira e segunda instru��o diferente das situa��es acima, imprimimos o conte�do CNAB']);
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.CarteiraDeCobranca(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;

  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer;
    const Coment, Codigo, CartTxt, Tipo, Carteira, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 6);
    Res[Linha][0] := Codigo;
    Res[Linha][1] := Carteira;
    Res[Linha][2] := CartTxt;
    Res[Linha][3] := Tipo; // E-Escritural S-Simples D-Direta
    Res[Linha][4] := Descricao;
    Res[Linha][5] := Coment;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: // com registro!!!
    begin
      case CNAB of
        240:
        begin
          {11}AddLinha(Result, Linha, '     ', '1', ' ', ' ', '1', 'Tradicional/Cobran�a Simples.');
          {12}AddLinha(Result, Linha, '     ', '1', ' ', ' ', '2', 'Tradicional/Cobran�a Vinculada.');
          {13}AddLinha(Result, Linha, '     ', '1', ' ', ' ', '3', 'Tradicional/Cobran�a Caucionada.');
          {14}AddLinha(Result, Linha, '     ', '1', ' ', ' ', '4', 'Tradicional/Cobran�a Descontada.');
          {17}AddLinha(Result, Linha, '     ', '1', ' ', ' ', '7', 'Tradicional/Cobran�a Direta Especial.');
          {21}AddLinha(Result, Linha, '     ', '2', ' ', ' ', '1', 'Escritural/Cobran�a Simples.');
          {22}AddLinha(Result, Linha, '     ', '2', ' ', ' ', '2', 'Escritural/Cobran�a Vinculada.');
          {23}AddLinha(Result, Linha, '     ', '2', ' ', ' ', '3', 'Escritural/Cobran�a Caucionada.');
          {24}AddLinha(Result, Linha, '     ', '2', ' ', ' ', '4', 'Escritural/Cobran�a Descontada.');
          {27}AddLinha(Result, Linha, '     ', '2', ' ', ' ', '7', 'Escritural/Cobran�a Direta Especial.');
       end;
        400:
        begin
          {  }AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '   ', '');
          {11}AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '11' , 'Cobran�a Simples/Descontada/BBVendor');
          {12}AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '12' , 'Cobran�a em Unidade Vari�vel');
          {17}AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '17' , 'Cobran�a Simples/Descontada');
          {31}AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '31' , 'Cobran�a Caucionada/Vinculada');
          {51}AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '51' , 'Cobran�a Descontada');
        end;
      end;
    end;
    008, 033, 353:  // com registro
    begin
      case CNAB of
        240:
        begin
          AddLinha(Result, Linha, '     ', '  ', ' ', ' ', '1', 'Cobran�a Simples (Sem Registro e Eletr�nica com Registro)');
          AddLinha(Result, Linha, '     ', '  ', ' ', ' ', '3', 'Cobran�a Caucionada (Eletr�nica com Registro e Convencional com Registro)');
          AddLinha(Result, Linha, '     ', '  ', ' ', ' ', '4', 'Cobran�a Descontada (Eletr�nica com Registro)');
          AddLinha(Result, Linha, '     ', '  ', ' ', ' ', '5', 'Cobran�a Simples (R�pida com Registro)');
          AddLinha(Result, Linha, '     ', '  ', ' ', ' ', '6', 'Cobran�a Caucionada (R�pida com Registro)');
          //AddLinha(Result, Linha, '     ', '  ', ' ', ' ', '7', ''); Nao tem?
        end;
        400:
        begin
          AddLinha(Result, Linha, '     ', '1', ' ', ' ', '???', 'Eletr�nica Simples');
          AddLinha(Result, Linha, '     ', '3', ' ', ' ', '???', 'Caucionada Eletr�nica');
          AddLinha(Result, Linha, '     ', '4', ' ', ' ', '???', 'Cobran�a sem registro');
          AddLinha(Result, Linha, '     ', '5', ' ', ' ', '101', 'R�pida com registro (Bloquete emitido pelo cliente)');
          AddLinha(Result, Linha, '     ', '6', ' ', ' ', '???', 'Caucionada r�pida');
          AddLinha(Result, Linha, '     ', '7', ' ', ' ', '???', 'Descontada eletr�nica');
        end;
      end;
    end;
    341: // com e sem registro
    begin
      case CNAB of
        400:
        begin
          if NomeLayout = CO_341_COBRANCA_BANCARIA_MARCO_2012 then
          begin
            AddLinha(Result, Linha, '     ', 'I', ' ', 'E', '112', 'ESCRITURAL ELETR�NICA - SIMPLES');
            AddLinha(Result, Linha, 'D    ', 'I', ' ', 'E', '104', 'ESCRITURAL ELETR�NICA - CARN�');
            AddLinha(Result, Linha, '     ', 'E', ' ', 'E', '147', 'ESCRITURAL ELETR�NICA - D�LAR');
            AddLinha(Result, Linha, 'D    ', 'I', ' ', 'E', '105', 'ESCRITURAL ELETR�NICA - D�LAR - CARN�');
            AddLinha(Result, Linha, '     ', 'I', ' ', 'E', '114', 'ESCRITURAL ELETR�NICA - SEGUROS');
            AddLinha(Result, Linha, '     ', 'F', ' ', 'E', '166', 'ESCRITURAL ELETR�NICA - TR');
            AddLinha(Result, Linha, 'D    ', 'I', ' ', 'E', '113', 'ESCRITURAL ELETR�NICA - TR - CARN�');
            AddLinha(Result, Linha, 'D,E  ', 'I', ' ', 'D', '108', 'DIRETA ELETR�NICA EMISS�O INTEGRAL - CARN�');
            AddLinha(Result, Linha, 'E    ', 'I', ' ', 'D', '109', 'DIRETA ELETR�NICA SEM EMISS�O - SIMPLES');
            AddLinha(Result, Linha, 'E    ', 'I', ' ', 'D', '110', 'DIRETA ELETR�NICA SEM EMISS�O - SIMPLES');
            AddLinha(Result, Linha, 'E    ', 'I', ' ', 'D', '111', 'DIRETA ELETR�NICA SEM EMISS�O - SIMPLES');
            AddLinha(Result, Linha, 'E    ', 'U', ' ', 'D', '150', 'DIRETA ELETR�NICA SEM EMISS�O - D�LAR');
            AddLinha(Result, Linha, '     ', 'I', ' ', 'D', '121', 'DIRETA ELETR�NICA EMISS�O PARCIAL - SIMPLES');
            AddLinha(Result, Linha, 'E    ', 'I', ' ', 'D', '180', 'DIRETA ELETR�NICA EMISS�O INTEGRAL SIMPLES');
            AddLinha(Result, Linha, 'A    ', 'I', ' ', 'S', '175', 'SEM REGISTRO SEM EMISS�O');
            AddLinha(Result, Linha, 'C    ', 'I', ' ', 'S', '198', 'SEM REGISTRO SEM EMISS�O 15 D�GITOS');
            AddLinha(Result, Linha, 'C    ', 'I', ' ', 'S', '143', 'SEM REGISTRO SEM EMISS�O 15 D�GITOS COM IOF 7%');
            AddLinha(Result, Linha, 'A    ', 'I', ' ', 'S', '174', 'SEM REGISTRO EMISS�O PARCIAL COM PROTESTO BORDER�');
            AddLinha(Result, Linha, 'A    ', 'I', ' ', 'S', '177', 'SEM REGISTRO EMISS�O PARCIAL COM PROTESTO ELETR�NICO');
            AddLinha(Result, Linha, 'A    ', 'I', ' ', 'S', '120', 'SEM REGISTRO EMISS�O INTEGRAL COM IOF 2% - CARN�');
            AddLinha(Result, Linha, 'A    ', 'I', ' ', 'S', '129', 'SEM REGISTRO EMISS�O PARCIAL SEGUROS COM IOF 2%');
            AddLinha(Result, Linha, 'A    ', 'I', ' ', 'S', '139', 'SEM REGISTRO EMISS�O PARCIAL SEGUROS COM IOF 4%');
            AddLinha(Result, Linha, 'A    ', 'I', ' ', 'S', '169', 'SEM REGISTRO EMISS�O PARCIAL SEGUROS COM IOF 7%');
            AddLinha(Result, Linha, 'A,B  ', 'I', ' ', 'S', '172', 'SEM REGISTRO COM EMISS�O INTEGRAL');
            AddLinha(Result, Linha, 'A,B,D', 'I', ' ', 'S', '102', 'SEM REGISTRO COM EMISS�O INTEGRAL - CARN�');
            AddLinha(Result, Linha, 'B,C  ', 'I', ' ', 'S', '195', 'SEM REGISTRO COM EMISS�O INTEGRAL - 15 POSI��ES');
            AddLinha(Result, Linha, 'B,C,D', 'I', ' ', 'S', '107', 'SEM REGISTRO COM EMISS�O INTEGRAL - 15 POSI��ES - CARN�');
            AddLinha(Result, Linha, 'A,B  ', 'I', ' ', 'S', '173', 'SEM REGISTRO COM EMISS�O E ENTREGA');
            AddLinha(Result, Linha, 'A,B,D', 'I', ' ', 'S', '103', 'SEM REGISTRO COM EMISS�O E ENTREGA - CARN�');
            AddLinha(Result, Linha, 'B,C  ', 'I', ' ', 'S', '196', 'SEM REGISTRO COM EMISS�O E ENTREGA - 15 POSI��ES');
            AddLinha(Result, Linha, 'B,D  ', 'I', ' ', 'S', '106', 'SEM REGISTRO COM EMISS�O E ENTREGA - 15 D�GITOS - CARN�');
          end else
          if NomeLayout = CO_341_COBRANCA_BANCARIA_SETEMBRO_2012 then
          begin
            AddLinha(Result, Linha, 'D,E  ', 'I', ' ', 'D', '108', 'DIRETA ELETR�NICA EMISS�O INTEGRAL � CARN�');
            AddLinha(Result, Linha, 'D    ', 'I', ' ', 'E', '104', 'ESCRITURAL ELETR�NICA � CARN�');
            AddLinha(Result, Linha, '     ', 'I', ' ', 'E', '138', 'ESCRITURAL ELETR�NICA � MENSAGEM COLORIDA');
            AddLinha(Result, Linha, '     ', 'I', ' ', 'E', '112', 'ESCRITURAL ELETR�NICA � SIMPLES');
            AddLinha(Result, Linha, 'A,B  ', 'I', ' ', 'S', '173', 'SEM REGISTRO COM EMISS�O E ENTREGA');
            AddLinha(Result, Linha, 'B,C  ', 'I', ' ', 'S', '196', 'SEM REGISTRO COM EMISS�O E ENTREGA � 15 POSI��ES');
            AddLinha(Result, Linha, 'A,B,D', 'I', ' ', 'S', '103', 'SEM REGISTRO COM EMISS�O E ENTREGA � CARN�');
            AddLinha(Result, Linha, '     ', 'E', ' ', 'E', '147', 'ESCRITURAL ELETR�NICA � D�LAR');
          end;
        end;
      end;
    end;
    399:  // com registro
    begin
      case CNAB of
        400:
        begin
          AddLinha(Result, Linha, '     ', '00', ' ', ' ', '1', 'Cobran�a Simples');
          AddLinha(Result, Linha, '     ', '00', ' ', ' ', '3', 'Garantia de Opera��es');
          AddLinha(Result, Linha, '     ', '00', ' ', ' ', '4', 'Desconto Suspenso (somente para arquivo Retorno)');
        end;
      end;
    end;
    409:  // sem registro!!!
    begin
      case CNAB of
        400:
        begin
          AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '   ', '');
          AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '20' , 'Carteira sem registro (Fixo)');
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        240:
        begin
          if (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
          begin
            AddLinha(Result, Linha, '     ', ' ', '17', '', '9', 'Com Registro');
          end else
          if (NomeLayout = CO_756_240_2018) then
          begin
            AddLinha(Result, Linha, '     ', ' ', '1', '', '01', 'Com Registro');
            AddLinha(Result, Linha, '     ', ' ', '3', '', '03', 'Garantida Caucionada');
          end;
        end;
        400:
        begin
          if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then
          begin
            AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '  ', '');
            AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '01', 'Simples com Registro');
            AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '02', 'Simples sem Registro');
            AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '03', 'Garantida Caucionada');
          end else
          if (NomeLayOut = CO_756_CORRESPONDENTE_BRADESCO_2015) then
          begin
            AddLinha(Result, Linha, '     ', ' ', ' ', ' ', '09', 'Com Registro');
          end;
        end;
      end;
    end;
  end;
end;

procedure TUBco_Rem.AddLin(var Res: MyArrayLista; var Linha: Integer;
  const Codigo: String; Descricao: array of String);
var
  i: Integer;
begin
  SetLength(Res, Linha+1);
  SetLength(Res[Linha], 2);
  Res[Linha][0] :=  Codigo;
  Res[Linha][1] :=  '';
  for i := Low(Descricao) to High(Descricao) do
    Res[Linha][1] :=  Res[Linha][1] + Descricao[i];
  inc(Linha, 1);
  //
end;

function TUBco_Rem.AvisoImplementacao(Banco, CNAB, Registrada: Integer;
  NomeLayout: String): String;
const
  cDesenv = 'Banco em fase de implementa��o!';
  cSimAll = 'Banco implementado!';
  cNaoRem = 'Banco implementado para bloqueto mas N�O implementado para remessa';
  cNaoBlq = 'Banco implementado para remessa mas N�O implementado para bloqueto';
  cNaoAll = 'Banco N�O implementado';
begin
  //Bloqueto
  Result := cNaoAll;
  {
  case TipoCart of
    0: // sem registro
    1,2: // com registro
    begin
  }
      case CNAB of
        240:
        case Banco of
          001:
            Result := cNaoBlq;
          756: //Sicoob
          begin
            if (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) and (Registrada = 1) then
              Result := cSimAll
            else if (NomeLayout = CO_756_240_2018) and (Registrada = 1) then
              Result := cSimAll
            else
              Result := cNaoRem;
          end;
        end;
        400:
        case Banco of
          001,
          237,
          341://Ita�
          begin
             if (NomeLayout = CO_341_COBRANCA_BANCARIA_MARCO_2012) then
               Result := cSimAll
             else
               Result := cNaoRem;
          end;
{ TODO : Mudar quando o banco aprovar! }
          008, 033, 353,
{ TODO : Mudar quando o banco aprovar! }
          399: Result := cDesenv;
          409: Result := cNaoRem;
{ TODO : Mudar quando o banco aprovar! }
          756: //Sicoob
          begin
            if (NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015) and (Registrada = 1) then
              Result := cSimAll
            else
              Result := cNaoRem
          end;
        end;
      end;
    {
    end;
  end;
  }
end;

function TUBco_Rem.AvisosDeCarteiraDeCobranca(const Banco, CNAB: Integer; Layout: String): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer;
  const Codigo: String; Descricao: array of String);
  var
    i: Integer;
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 2);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  '';
    for i := Low(Descricao) to High(Descricao) do
      Res[Linha][1] :=  Res[Linha][1] + Descricao[i];
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    341: // com registro
    begin
      case CNAB of
        400:
        begin
          if Layout = CO_341_COBRANCA_BANCARIA_MARCO_2012 then
          begin
            AddLinha(Result, Linha, 'A', ['No arquivo retorno � informado somente: ag�ncia, conta corrente, carteira, nosso n�mero, data do pagamento, multa, desconto/abatimento, tarifa, valor l�quido.']);
            AddLinha(Result, Linha, 'B', ['Carteiras sem registro, com emiss�o do bloqueto pelo Ita�. S�o as �nicas que utilizam arquivo remessa conforme anexo A.']);
            AddLinha(Result, Linha, 'C', ['No arquivo retorno � informado somente: ag�ncia, conta corrente, carteira, nosso n�mero, data do pagamento, multa, desconto/abatimento, tarifa, valor l�quido e seu n�mero.']);
            AddLinha(Result, Linha, 'D', ['Para carteiras com impress�o e montagem de carn�s pelo Ita�, o arquivo remessa dever� ser ordenado por sacado e vencimento. A cada altera��o no nome do sacado ser� emitido um carn� (limitado a 99 parcelas), obedecendo a ordem do arquivo remessa.', ' Quando a quantidade de parcelas de um carn� for superior a "99", � necess�ria a emiss�o de mais de um carn�.']);
            AddLinha(Result, Linha, 'E', ['Somente utilizar nosso n�mero dentro de faixa num�rica definida pelo Ita�.']);
          end else
          if Layout = CO_341_COBRANCA_BANCARIA_SETEMBRO_2012 then
          begin
            AddLinha(Result, Linha, 'A', ['No arquivo retorno � informado somente: ag�ncia, conta corrente, carteira, nosso n�mero, data do pagamento, multa, desconto/abatimento, tarifa, valor l�quido.']);
            AddLinha(Result, Linha, 'B', ['Carteiras sem registro, com emiss�o do BOLETO pelo Banco Ita�. S�o as �nicas que utilizam arquivo remessa conforme anexo A.']);
            AddLinha(Result, Linha, 'C', ['No arquivo retorno � informado somente: ag�ncia, conta corrente, carteira, nosso n�mero, data do pagamento, multa, desconto/abatimento, tarifa, valor l�quido e seu n�mero.']);
            AddLinha(Result, Linha, 'D', ['Para carteiras com impress�o e montagem de carn�s pelo Banco Ita�, o arquivo remessa dever� ser ordenado por sacado e vencimento.', 'A cada altera��o no nome do sacado ser� emitido um carn� (limitado a 99 parcelas), obedecendo a ordem do arquivo remessa.', 'Quando a quantidade de parcelas de um carn� for superior a �99�, � necess�ria a emiss�o de mais de um carn�.']);
            AddLinha(Result, Linha, 'E', ['Somente utilizar nosso n�mero dentro de faixa num�rica definida pelo Banco Ita�.']);
            AddLinha(Result, Linha, 'F', ['Carteira exclusiva para permitir liquida��o parcial do t�tulo, conforme negocia��o previamente cadastrada pelo cedente no Ita� 30 horas Empresa Plus.', 'N�o permite protesto de t�tulos que tiveram liquida��o parcial e o cliente necessita estar operando com BOLETO digital no site da empresa (B2B).']);
          end;
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        400:
        begin
          {if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then}
          begin
            //
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.TipoDeCobrancaBB(const Banco, CNAB: Integer; Carteira: String): MyArrayLista;
var
  Linha, CartCod: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: // com registro
    begin
      CartCod := Geral.IMV(Carteira);
      case CNAB of
        400:
        begin
          case CartCod of
            11,17:
            begin
              AddLin(Result, Linha, '04DSC', ['Solicita��o de registro na Modalidade Descontada']);
              AddLin(Result, Linha, '08VDR', ['Solicita��o de registro na Modalidade BBVendor']);
              AddLin(Result, Linha, '02VIN', ['Solicita��o de registro na Modalidade Vinculada']);
              AddLin(Result, Linha, '', ['Registro na Modalidade Simples']);
            end;
            (**)0(**),12,31,51:
            begin
              AddLin(Result, Linha, '', ['']);
            end;
            else begin
              if CartCod = 0 then
              Geral.MB_Aviso('A carteira ' + Carteira + ' n�o est� ' +
              'implementada para o banco ' + FormatFloat('000', Banco) + '.');
            end;
          end;
        end;
      end;
    end;
  end;
end;

{
function TUBco_Rem.ObtemTipoDeCobrancaBB(const Banco, Carteira: Integer): String;
var
  Linha: Integer;
begin
  Linha := 0;
  //SetLength(Result, 0);
  case banco of
    001: // com registro
    begin
      case Carteira of
        11,17:
        begin
          Result := '04DSC';
          Result := '08VDR';
          Result := '02VIN';
          Result := '';
        end;
        (**)0(**),12,31,51:
        begin
          Result := '';
        end;
        else begin
          if Carteira = 0 then
          Geral.MB_Aviso('A carteira ' + IntToStr(Carteira) + ' n�o est� ' +
          'implementada para o banco ' + FormatFloat('000', Banco) + '.');
        end;
      end;
    end;
  end;
end;
}

function TUBco_Rem.Comando(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: // com registro
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '01', ['Entrada de t�tulos']);
          AddLin(Result, Linha, '02', ['Pedido de baixa']);
          AddLin(Result, Linha, '04', ['Concess�o de abatimento']);
          AddLin(Result, Linha, '05', ['Cancelamento de abatimento']);
          AddLin(Result, Linha, '06', ['Altera��o de vencimento']);
          AddLin(Result, Linha, '07', ['Concess�o de desconto']);
          AddLin(Result, Linha, '08', ['Cancelamento de desconto']);
          AddLin(Result, Linha, '09', ['Protestar']);
          AddLin(Result, Linha, '10', ['Cancela/sustac�o da instru��o de protesto']);
          AddLin(Result, Linha, '30', ['Recusa da alega��o do sacado']);
          AddLin(Result, Linha, '31', ['Altera��o de outros dados']);
          AddLin(Result, Linha, '40', ['Altera��o de modalidade']);
        end;
        400:
        begin
          AddLin(Result, Linha, '01', ['Registro de t�tulos']);
          AddLin(Result, Linha, '02', ['Solicita��o de baixa']);
          AddLin(Result, Linha, '03', ['Pedido de d�bito em conta']);
          AddLin(Result, Linha, '04', ['Concess�o de abatimento']);
          AddLin(Result, Linha, '05', ['Cancelamento de abatimento']);
          AddLin(Result, Linha, '06', ['Altera��o de vencimento de t�tulo']);
          AddLin(Result, Linha, '07', ['Altera��o do n�mero de controle do participante']);
          AddLin(Result, Linha, '08', ['Altera��o do n�mero do titulo dado pelo cedente']);
          AddLin(Result, Linha, '09', ['Instru��o para protestar (Nota 8)']);
          AddLin(Result, Linha, '10', ['Instru��o para sustar protesto']);
          AddLin(Result, Linha, '11', ['Instru��o para dispensar juros']);
          AddLin(Result, Linha, '12', ['Altera��o de nome e endere�o do Sacado']);
          AddLin(Result, Linha, '16', ['Altera��o de juros de mora']);
          AddLin(Result, Linha, '31', ['Conceder desconto']);
          AddLin(Result, Linha, '32', ['N�o conceder desconto']);
          AddLin(Result, Linha, '33', ['Retificar dados da concess�o de desconto']);
          AddLin(Result, Linha, '34', ['Alterar data para concess�o de desconto']);
          AddLin(Result, Linha, '35', ['Cobrar multa']);
          AddLin(Result, Linha, '36', ['Dispensar multa']);
          AddLin(Result, Linha, '37', ['Dispensar indexador']);
          AddLin(Result, Linha, '38', ['Dispensar prazo limite de recebimento']);
          AddLin(Result, Linha, '39', ['Alterar prazo limite de recebimento']);
          AddLin(Result, Linha, '40', ['Alterar modalidade']);
        end;
      end;
    end;
    008, 033, 353: // com registro
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '01', ['Entrada de t�tulo']);
          AddLin(Result, Linha, '02', ['Pedido de baixa']);
          //AddLin(Result, Linha, '03', ['']);
          AddLin(Result, Linha, '04', ['Concess�o de abatimento']);
          AddLin(Result, Linha, '05', ['Cancelamento de abatimento']);
          AddLin(Result, Linha, '06', ['Altera��o de vencimento']);
          AddLin(Result, Linha, '07', ['Altera��o da identifica��o do t�tulo na empresa']);
          AddLin(Result, Linha, '08', ['Altera��o seu n�mero']);
          AddLin(Result, Linha, '09', ['Pedido de Protesto']);
          AddLin(Result, Linha, '18', ['Pedido de Susta��o de Protesto']);
          AddLin(Result, Linha, '10', ['Concess�o de Desconto']);
          AddLin(Result, Linha, '11', ['Cancelamento de desconto']);
          AddLin(Result, Linha, '31', ['Altera��o de outros dados']);
          AddLin(Result, Linha, '98', ['N�o Protestar']);
        end;
        400:
        begin
          AddLin(Result, Linha, '01', ['ENTRADA DE T�TULO']);
          AddLin(Result, Linha, '02', ['BAIXA DE T�TULO']);
          //AddLin(Result, Linha, '03', ['']);
          AddLin(Result, Linha, '04', ['CONCESS�O DE ABATIMENTO']);
          AddLin(Result, Linha, '05', ['CANCELAMENTO ABATIMENTO']);
          AddLin(Result, Linha, '06', ['PRORROGA��O DE VENCIMENTO']);
          AddLin(Result, Linha, '07', ['ALT. N�MERO CONT.CEDENTE']);
          AddLin(Result, Linha, '08', ['ALTERA��O DO SEU N�MERO']);
          AddLin(Result, Linha, '09', ['PROTESTAR']);
          AddLin(Result, Linha, '18', ['SUSTAR PROTESTO']);
        end;
      end;
    end;
    399: // com registro
    begin
      case CNAB of
        400:
        begin
          AddLin(Result, Linha, '01', ['Remessa']);
          AddLin(Result, Linha, '02', ['Pedido de baixa']);
          AddLin(Result, Linha, '04', ['Concess�o de abatimento (somente para moeda Real)']);
          AddLin(Result, Linha, '05', ['Cancelamento de abatimento concedido']);
          AddLin(Result, Linha, '06', ['Prorroga��o de vencimento']);
          AddLin(Result, Linha, '07', ['Altera��o do "controle do participante"']);
          AddLin(Result, Linha, '08', ['Altera��o do "seu n�mero"']);
          AddLin(Result, Linha, '09', ['Protestar']);
          AddLin(Result, Linha, '10', ['Sustar protesto']);
          AddLin(Result, Linha, '11', ['N�o cobrar juros de mora']);
          AddLin(Result, Linha, '13', ['Conceder desconto R$.... p/pgto at� ..../..../....']);
          AddLin(Result, Linha, '14', ['Cancelamento condi��a de desconto fixo']);
          AddLin(Result, Linha, '15', ['Cancelamento de desconto di�rio']);
          AddLin(Result, Linha, '48', ['Vencimento alterado para ...']);
          AddLin(Result, Linha, '49', ['Altera��o de dias para envio a Cart�rio de Protesto']);
          AddLin(Result, Linha, '50', ['Inclus�o de Sacado no Boleto Eletr�nico.']);
          AddLin(Result, Linha, '51', ['Exclus�o de Sacado no Boleto Eletr�nico.']);
          AddLin(Result, Linha, '57', ['Protesto para fins Falimentares']);
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        240:
        begin
          if NomeLayout = CO_756_CORRESPONDENTE_BB_2015 then
          begin
            AddLin(Result, Linha, '01', ['Entrada de t�tulos']);
            AddLin(Result, Linha, '02', ['Pedido de Baixa']);
            AddLin(Result, Linha, '04', ['Concess�o de Abatimento']);
            AddLin(Result, Linha, '05', ['Cancelamento de Abatimento']);
            AddLin(Result, Linha, '06', ['Altera��o de Vencimento']);
            AddLin(Result, Linha, '07', ['Concess�o de Desconto']);
            AddLin(Result, Linha, '08', ['Cancelamento de Desconto']);
            AddLin(Result, Linha, '09', ['Protestar']);
            AddLin(Result, Linha, '10', ['Cancela / Susta��o da Instru��o de Protesto']);
            AddLin(Result, Linha, '31', ['Altera��o de Outros Dados']);
          end else
          if NomeLayout = CO_756_240_2018 then
          begin
            AddLin(Result, Linha, '01', ['Entrada de T�tulos']);
          end;
        end;
        400:
        begin
          if (NomeLayout = CO_756_EXCEL_2012_03_14) or
            (NomeLayout = CO_756_EXCEL_2013_07_18) then
          begin
            AddLin(Result, Linha, '01', ['Registro de T�tulos']);
            AddLin(Result, Linha, '02', ['Solicita��o de Baixa']);
            AddLin(Result, Linha, '03', ['Pedido de D�bito em Conta']);
            AddLin(Result, Linha, '04', ['Concess�o de Abatimento']);
            AddLin(Result, Linha, '05', ['Cancelamento de Abatimento']);
            AddLin(Result, Linha, '06', ['Altera��o de Vencimento']);
            AddLin(Result, Linha, '07', ['Altera��o do N�mero de Controle']);
            AddLin(Result, Linha, '08', ['Aldera��o de Seu N�mero']);
            AddLin(Result, Linha, '09', ['Instru��o para Protestar']);
            AddLin(Result, Linha, '10', ['Instru��o para Sustar Protesto']);
            AddLin(Result, Linha, '11', ['Instru��o para Dispensar Juros']);
            AddLin(Result, Linha, '12', ['Altera��o de Sacado']);
            AddLin(Result, Linha, '30', ['Recusa de Alega��o do Sacado']);
            AddLin(Result, Linha, '31', ['Altera��o de Outros Dados']);
            AddLin(Result, Linha, '34', ['Baixa - Pagamento Direto ao Cedente']);
          end else
          if NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015 then
          begin
            AddLin(Result, Linha, '01', ['Remessa']);
            AddLin(Result, Linha, '02', ['Pedido de Baixa']);
            AddLin(Result, Linha, '04', ['Concess�o de Abatimento']);
            AddLin(Result, Linha, '05', ['Cancelamento de Abatimento']);
            AddLin(Result, Linha, '06', ['Altera��o de Vencimento']);
            AddLin(Result, Linha, '09', ['Pedido de Protesto']);
            AddLin(Result, Linha, '18', ['Sustar Protesto e Baixar T�tulo']);
            AddLin(Result, Linha, '19', ['Sustar Protesto e Manter em Carteira']);
            AddLin(Result, Linha, '31', ['Altera��o de Outros Dados']);
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.DdProtesto(const Banco, CNAB: Integer): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: // com registro
    begin
      case CNAB of
        400:
        begin
          AddLin(Result, Linha, '06', ['06 dias corridos. "Comando" deve ser "01" e "Instru��o codificada" de ser "06"']);
          AddLin(Result, Linha, '29', ['29 dias corridos. "Comando" deve ser "01" e "Instru��o codificada" de ser "06"']);
          AddLin(Result, Linha, '35', ['35 dias corridos. "Comando" deve ser "01" e "Instru��o codificada" de ser "06"']);
          AddLin(Result, Linha, '40', ['40 dias corridos. "Comando" deve ser "01" e "Instru��o codificada" de ser "06"']);
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        400:
        begin
          {if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then}
          begin
            //
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.CodProtestoRem(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: // com registro
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '1', ['Dias corridos']);
          AddLin(Result, Linha, '2', ['Dias �teis']);
          AddLin(Result, Linha, '3', ['N�o protestar']);
        end;
      end;
    end;
    399: // com registro
    begin
      case CNAB of
        400:  // Diretiva
        begin
          AddLin(Result, Linha, '0' , ['N�o utiliza (Protesto autom�tico?)']);  // Deixar dias em branco
          AddLin(Result, Linha, '1', ['Protestar ap�s ... dias do vencimento (Protesto autom�tico)']); // n�o tem c�digo, ent�o usei o '1'
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        240:
        begin
          if(NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
          begin
            AddLin(Result, Linha, '0' , ['N�o utiliza']);  // Deixar dias em branco
            AddLin(Result, Linha, '1' , ['Protesto autom�tico (5 dias no m�nimo)']);  //5 dias no m�nimo e m�ximo 99
          end else if (NomeLayout = CO_756_240_2018) then
          begin
            AddLin(Result, Linha, '1' , ['Protestar dias corridos']);
            AddLin(Result, Linha, '3' , ['N�o protestar']);
            AddLin(Result, Linha, '9' , ['Cancelar instru��es de protesto']);
          end;
        end;
        400:
        begin
          {if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then}
          if (NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015) then
          begin
            AddLin(Result, Linha, '00' , ['N�o utiliza']);  // Deixar dias em branco
            AddLin(Result, Linha, '06' , ['Protesto autom�tico (5 dias no m�nimo)']);  //5 dias no m�nimo e m�ximo 99
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.CodBaixaDevolRem(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: // com registro
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '1', ['Baixar / devolver']);
          AddLin(Result, Linha, '2', ['N�o baixar / n�o devolver']);
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        240:
        begin
          if NomeLayout = CO_756_240_2018 then
          begin
            AddLin(Result, Linha, '0', ['Baixar / devolver']);
          end;
        end;
        400:
        begin
          {if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then}
          begin
            //
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.CodMoeda(const Banco, CNAB: Integer): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: // com registro
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '01', ['Reservado para uso futuro']);
          AddLin(Result, Linha, '02', ['D�lar Americano Comercial (Venda)']);
          AddLin(Result, Linha, '03', ['D�lar Americano Turismo (Venda)']);
          AddLin(Result, Linha, '04', ['ITRD']);
          AddLin(Result, Linha, '05', ['IDTR']);
          AddLin(Result, Linha, '06', ['UFIR Di�ria']);
          AddLin(Result, Linha, '07', ['UFIR Mensal']);
          AddLin(Result, Linha, '08', ['FAJ-TR']);
          AddLin(Result, Linha, '09', ['REAL']);
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.VariacaoCarteira(const Banco, CNAB: Integer): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: // com registro
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '14', ['Cobran�a cedente']);
          AddLin(Result, Linha, '19', ['Cobran�a sacada']);
          AddLin(Result, Linha, '24', ['Captura de cheques']);
          AddLin(Result, Linha, '126', ['Pagamento fornecedores - PGT']);
        end;
      end;
    end;
  end;
end;

{
function TUBco_Rem.CartCodBB(const Banco, CNAB: Integer): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: //
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '1', ['Cobran�a Simples.']);
          AddLin(Result, Linha, '2', ['Cobran�a Vinculada.']);
          AddLin(Result, Linha, '3', ['Cobran�a Caucionada.']);
          AddLin(Result, Linha, '4', ['Cobran�a Descontada']);
          AddLin(Result, Linha, '7', ['Cobran�a Direta Especial (carteira 17)']);
        end;
        400:
        begin
          // N�o tem !!!
          AddLin(Result, Linha, '', ['']);
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.CadastroDoTituloNoBanco(const Banco, CNAB: Integer): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: //
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '1', ['Com cadastro.']);
          AddLin(Result, Linha, '2', ['Sem cadastro.']);
        end;
      end;
    end;
  end;
end;

}

function TUBco_Rem.QuemDistribuiBloqueto(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: //
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '1', ['Banco']);
          AddLin(Result, Linha, '2', ['Cliente']);
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        240:
        begin
          if (NomeLayout = CO_756_240_2018) then
          begin
            AddLin(Result, Linha, '1', ['Banco emite']);
            AddLin(Result, Linha, '2', ['Benefici�rio emite']);
          end;
        end;
        400:
        begin
          if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then
          begin
            AddLin(Result, Linha, '1', ['Cooperativa']);
            AddLin(Result, Linha, '2', ['Cliente']);
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.QuemImprimeBloqueto(const Banco, CNAB: Integer;
  NomeLayout: String): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: //
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '1', ['Banco emite.']);
          AddLin(Result, Linha, '2', ['Cliente emite.']);
          AddLin(Result, Linha, '3', ['Banco pr�-emite e cliente complementa']);
          AddLin(Result, Linha, '4', ['Banco reemite']);
          AddLin(Result, Linha, '5', ['Banco n�o reemite']);
          AddLin(Result, Linha, '6', ['Cobran�a sem papel']);
        end;
      end;
    end;
    237: //
    begin
      case CNAB of
        400:
        begin
          {if NomeLayout = CO_237_MP_4008_0121_01_Data_11_11_2010 then}
          begin
            AddLin(Result, Linha, '1', ['Banco']);
            AddLin(Result, Linha, '2', ['Cliente']);
          end;
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        240:
        begin
          if (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) or (NomeLayout = CO_756_240_2018) then
          begin
            AddLin(Result, Linha, '1', ['Banco emite']);
            AddLin(Result, Linha, '2', ['Benefici�rio emite']);
          end;
        end;
        400:
        begin
          {if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then}
          begin
            //
          end;
        end;
      end;
    end;
  end;
end;

procedure TUBco_Rem.QuemImprimeBloqueto_Verifica(const Banco, CNAB: Integer;
  ValorInformado: String);
const
  _001_240 = ['1', '2', '3', '4', '5', '6'];
var
  //Linha: Integer;
  Achou, Pode: Boolean;
begin
  Achou := False;
  Pode  := False;
  //Linha := 0;
  //SetLength(Result, 0);
  case banco of
    001: //
    begin
      case CNAB of
        240:
        begin
          {
          AddLin(Result, Linha, '1', ['Banco emite.']);
          AddLin(Result, Linha, '2', ['Cliente emite.']);
          AddLin(Result, Linha, '3', ['Banco pr�-emite e cliente complementa']);
          AddLin(Result, Linha, '4', ['Banco reemite']);
          AddLin(Result, Linha, '5', ['Banco n�o reemite']);
          AddLin(Result, Linha, '6', ['Cobran�a sem papel']);
          }
          if (ValorInformado = '1')
          or (ValorInformado = '6') then
          begin
            Achou := True;
            Pode  := False;
          end else
          if (ValorInformado = '2')
          or (ValorInformado = '3')
          or (ValorInformado = '4')
          or (ValorInformado = '5') then
          begin
            Achou := True;
            Pode  := True;
          end;
        end;
      end;
    end;
    237: //
    begin
      case CNAB of
        400:
        begin
          {if NomeLayout = CO_237_MP_4008_0121_01_Data_11_11_2010 then}
          begin
            {
            AddLin(Result, Linha, '1', ['Banco']);
            AddLin(Result, Linha, '2', ['Cliente']);
            }
            if (ValorInformado = '1') then
            begin
              Achou := True;
              Pode  := False;
            end else
            if (ValorInformado = '2') then
            begin
              Achou := True;
              Pode  := True;
            end;
          end;
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        400:
        begin
          {if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then}
          begin
            //
          end;
        end;
      end;
    end;
  end;
  //
  if Achou then
  begin
    if not Pode then
      Geral.MB_Aviso('CUIDADO! Na configura��o de remessa n�o informa que o boleto � impresso pelo cliente!');
  end else
    Geral.MB_Info('Banco sem informa��o de quem imprime o boileto!')
end;

function TUBco_Rem.MultaTipo(const Banco, CNAB: Integer; Layout: String): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: //
    begin
      case CNAB of
        240:
        begin
          // Banco n�o fala sobre o c�digo zero mas exige se n�o � cobrada multa!
          AddLin(Result, Linha, '0', ['']);
          AddLin(Result, Linha, '1', ['Valor Fixo']);
          AddLin(Result, Linha, '2', ['Percentual']);
        end;
      end;
    end;
    008, 033, 353:
    begin
      case CNAB of
        400:
        begin
          AddLin(Result, Linha, '0', ['N�o cobrar multa']);
          AddLin(Result, Linha, '4', ['Cobrar .... % de multa ']);
        end;
      end;
    end;
    237:  // com registro!!!
    begin
      case CNAB of
        400:
        begin
          if Layout = CO_237_MP_4008_0121_01_Data_11_11_2010 then
          begin
            AddLin(Result, Linha, '0', ['N�o cobrar multa']);
            AddLin(Result, Linha, '2', ['Cobrar .... % de multa ']);
          end;
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        240:
        begin
          if Layout = CO_756_240_2018 then
          begin
            AddLin(Result, Linha, '0', ['Isento']);
            AddLin(Result, Linha, '1', ['Valor fixo']);
            AddLin(Result, Linha, '2', ['Percentual']);
          end;
        end;
        400:
        begin
          {if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then}
          begin
            //
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.DescontosTipos(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: //
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '1', ['Valor fixo at� a data informada']);
          AddLin(Result, Linha, '2', ['Percentual at� a data informada']);
          AddLin(Result, Linha, '3', ['Valor por antecipa��o dia corrido']);
          AddLin(Result, Linha, '4', ['Valor por antecipa��o dia �til']);
          AddLin(Result, Linha, '5', ['Percentual sobre o valor nominal dia']);
        end;
      end;
    end;
    756:
    begin
      case CNAB of
        240:
        begin
          if(NomeLayout = CO_756_240_2018) then
          begin
            AddLin(Result, Linha, '0', ['N�o conceder desconto']);
            AddLin(Result, Linha, '1', ['Valor fixo at� a data informada']);
            AddLin(Result, Linha, '2', ['Percentual at� a data informada']);
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.JurosTipo(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: //
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, '1', ['Valor por dia']);
          AddLin(Result, Linha, '2', ['Taxa mensal']);
          AddLin(Result, Linha, '3', ['Isento']);
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        240:
        begin
          if NomeLayout = CO_756_240_2018 then
          begin
            AddLin(Result, Linha, '0', ['Isento']);
            AddLin(Result, Linha, '1', ['Valor por dia']);
            AddLin(Result, Linha, '2', ['Porcentagem']);
          end else
          begin
            AddLin(Result, Linha, '1', ['Isento']);
            AddLin(Result, Linha, '2', ['Valor por dia']);
            AddLin(Result, Linha, '3', ['Porcentagem']);
          end;
        end;
        400:
        begin
          {if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then}
          begin
            //
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.TipoCart(const Banco, CNAB: Integer; NomeLayout: String): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    001: //
    begin
      case CNAB of
        240:
        begin
          {
          AddLin(Result, Linha, '1', ['Tradicional']);
          AddLin(Result, Linha, '2', ['Escritural']);
          }
          AddLin(Result, Linha, '1', ['Com Cadastro']);
          AddLin(Result, Linha, '2', ['Sem Cadastro']);
        end;
        400:
        begin
          // N�o tem ?
          AddLin(Result, Linha, '', ['']);
        end;
      end;
    end;
    008, 033, 353: //
    begin
      case CNAB of
        400:
        begin
          // N�o tem?
          //AddLin(Result, Linha, '1', ['Com Cadastro']);
          //AddLin(Result, Linha, '2', ['Sem Cadastro']);
        end;
      end;
    end;
    341: //
    begin
      case CNAB of
        400:
        begin
          AddLin(Result, Linha, '0', ['Carteira sem registro']);
          AddLin(Result, Linha, '1', ['Carteira com registro - escritural (o banco gera o n�mero?)']);
          AddLin(Result, Linha, '2', ['Carteira com registro - direta (o banco fornece faixa de n�meros?)']);
        end;
      end;
    end;
    399: //
    begin
      case CNAB of
        400:
        begin
          AddLin(Result, Linha, '1', ['Registrada (forma definida pela esp�cie)']);
        end;
      end;
    end;
    756: //
    begin
      case CNAB of
        400:
        begin
          if (NomeLayout = CO_756_EXCEL_2012_03_14) or
            (NomeLayout = CO_756_EXCEL_2013_07_18) or
            (NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015) then
          // j� est� na carteira de cobran�a!
          begin
            {
            AddLin(Result, Linha, '01', ['Simples com Registro']);
            AddLin(Result, Linha, '02', ['Simples sem Registro']);
            AddLin(Result, Linha, '03', ['Garantia Caucionada']);
            }
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.LayoutCNAB240(const Banco, Registro: Integer;
Segmento: Char; var L: MyArrayLista; const SubSegmento: Integer;
const NomeLayout: String): TCNABResult;

  procedure AL(var Res: MyArrayLista; var Linha: Integer; const IDReg: String; const Ini, Fim,
  Tam, AjustaTamanho, Preenchimento, Casas, Formatacao, MyCodCampo, ConteudoPadrao, NomeDoCampo,
  DescricaoDoCampo: String);
  var
    i, f, t: Integer;
  begin
    i := Geral.IMV(Ini);
    f := Geral.IMV(Fim);
    t := Geral.IMV(Tam);
    if t = 0 then
      t := f - i + 1;
    if t < 1 then
      Geral.MB_Erro('O tamanho do campo "' + NomeDoCampo +
      '" n�o confere na function "UBancos.LayoutCNAB240" !' + sLineBreak +
      'Banco: '+FormatFloat('000', Banco) + sLineBreak +
      'Registro: '+ IntToStr(Registro) + sLineBreak +
      'Segmento: '+ Segmento + sLineBreak +
      'Posi��o inicial do campo: ' + Ini + sLineBreak +
      'Posicao final do campo: ' + Fim);
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 11);
    Res[Linha][00] :=  Ini;
    Res[Linha][01] :=  Fim;
    Res[Linha][02] :=  FormatFloat('000', t);
    Res[Linha][03] :=  Preenchimento;
    Res[Linha][04] :=  Casas;
    Res[Linha][05] :=  Formatacao;
    Res[Linha][06] :=  MyCodCampo;
    Res[Linha][07] :=  ConteudoPadrao;
    Res[Linha][08] :=  NomeDoCampo;
    Res[Linha][09] :=  DescricaoDoCampo;
    Res[Linha][10] :=  AjustaTamanho;
    inc(Linha, 1);
  end;
const
  x = '';
var
  I: Integer;
begin
  {
  AL(L,I, IDReg, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
   'C' = Corrige tamanho? S=sim, N=N�o, X= Sim mas exige um valor(alfanum�rico)
   'P' = Picture (Preenchimento) '0'= Num�rico (Formata � direta) ' '= Alfanum�rico (Formata � esquerda)
   'T' = Tipo de dados:
     'I'=Integer, 'F'=Float, 'X'=Texto, 'Z'=Zero,
     'B'=Branco, 'D'=Data, 'T'=Hora, 'C'=Calculado(j� formatado)
   'FLD' = Ver em : TUBancos.DescricaoDoMeuCodigoCNAB(...
  }
  // Format (usado para data ou quantidade de casas decimais de float)

  I := 0;
  SetLength(L, 0);
  Result := cresNoBank;
  case banco of
    -01: // Padr�o FEBRABAN  8.4
    begin
      Result := cresNoField;
        case Registro of
          0: // Header de arquivo
          begin
//      'IdReg', 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, '01.0 ', '001', '003', '003', 'N', '0', 'I', '        ', '001', '               ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, '02.0 ', '004', '007', '004', 'N', '0', 'I', '        ', '003', '0000           ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O');
AL(L,I, '03.0 ', '008', '008', '001', 'N', '0', 'I', '        ', '014', '0              ', 'REGISTRO HEADER     ', 'REGISTRO HEADER DE ARQUIVO');
AL(L,I, '04.0 ', '009', '017', '009', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '05.0 ', '018', '018', '001', 'N', '0', 'I', '        ', '400', '               ', '1-CPF 2-CNPJ        ', 'TIPO DE INSCRI��O DA EMPRESA');
AL(L,I, '06.0 ', '019', '032', '014', 'N', '0', 'I', '        ', '401', '               ', 'N�MERO DO CPF/CNPJ  ', 'N�MERO DA INCRI��O DA EMPRESA (CPF/CNPJ)');
AL(L,I, '07.0 ', '033', '052', '020', 'N', ' ', 'X', '        ', '408', '               ', 'C�DIGO DO CONV�NIO  ', 'C�DIGO DO CONV�NIO NO BANCO');
AL(L,I, '08.0 ', '053', '057', '005', 'N', '0', 'I', '        ', '020', '               ', 'PREFIXO DA AG�NCIA  ', 'N�MERO DA AG�NCIA ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
AL(L,I, '09.0 ', '058', '058', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
AL(L,I, '10.0 ', '059', '070', '012', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
AL(L,I, '11.0 ', '071', '071', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
AL(L,I, '12.0 ', '072', '072', '001', 'N', ' ', 'X', '        ', '024', '               ', 'DV DA AG�NCIA/CONTA ', 'D�GITO VERIFICADOR DA AG�NCIA / CONTA CORRENTE');
AL(L,I, '13.0 ', '073', '102', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DA EMPRESA     ', 'NOME POR EXTENSO DA "EMPRESA M�E"');
AL(L,I, '14.0 ', '103', '132', '030', 'S', ' ', 'X', '        ', '002', '               ', 'NOME DO BANCO       ', 'NOME POR EXTENSO DO BANCO COBRADOR');
AL(L,I, '15.0 ', '133', '142', '010', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '16.0 ', '143', '143', '001', 'N', '0', 'I', '        ', '009', '               ', '1-REM E 2-RET       ', 'C�DIGO DE REMESSA OU RETORNO');
AL(L,I, '17.0 ', '144', '151', '008', 'N', '0', 'D', 'DDMMAAAA', '990', '               ', 'DATA DE GERA��O     ', 'DATA DE GERA��O DO ARQUIVO');
AL(L,I, '18.0 ', '152', '157', '006', 'N', '0', 'T', 'HHMMSS  ', '991', '               ', 'HORA DE GERA��O     ', 'HORA DE GERA��O DO ARQUIVO');
AL(L,I, '19.0 ', '158', '163', '006', 'N', '0', 'I', '        ', '032', '               ', 'SEQUENCIAL          ', 'SEQUENCIAL DA REMESSA');
AL(L,I, '20.0 ', '164', '166', '003', 'N', '0', 'I', '        ', '889', '084            ', 'VERS�O DO LAYOUT    ', 'N�MERO DA VERS�O DO LAYOUT DO ARQUIVO');
AL(L,I, '21.0 ', '167', '171', '005', 'N', '0', 'I', '        ', '888', '               ', 'DENSIDADE GRAVA��O  ', 'DENSIDADE DE GRAVA��O DO ARQUIVO EM BPI');
AL(L,I, '22.0 ', '172', '191', '020', 'N', ' ', 'X', '        ', '-02', '               ', 'USO DO BANCO        ', 'USO PARA USO RESERVADO DO BANCO');
AL(L,I, '23.0 ', '192', '211', '020', 'N', ' ', 'X', '        ', '-02', '               ', 'USO DA EMPRESA      ', 'USO PARA USO RESERVADO DA EMPRESA');
AL(L,I, '24.0 ', '212', '240', '029', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            //
            Result := cresOKField;
          end;
          1: // Header de Lote
          begin
//      'IdReg'  'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, '01.1 ', '001', '003', '003', 'N', '0', 'I', '        ', '001', '               ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, '02.1 ', '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, '03.1 ', '008', '008', '001', 'N', '0', 'I', '        ', '014', '1              ', 'REGISTRO HEADER LOTE', 'REGISTRO HEADER DE LOTE');
AL(L,I, '04.1 ', '009', '009', '001', 'N', ' ', 'X', '        ', '031', '               ', 'TIPO DE OPERA��O    ', 'TIPO DE OPERA��O');
AL(L,I, '05.1 ', '010', '011', '002', 'N', '0', 'I', '        ', '029', '01             ', 'TIPO DE SERVI�O     ', 'TIPO DE SERVI�O');
AL(L,I, '06.1 ', '012', '013', '002', 'N', '0', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '07.1 ', '014', '016', '003', 'N', '0', 'I', '        ', '889', '043            ', 'VERSAO DO LAYOUT    ', 'N�MERO DA VERS�O DO LAYOUT');
AL(L,I, '08.1 ', '017', '017', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '09.1 ', '018', '018', '001', 'N', '0', 'I', '        ', '400', '               ', '1-CPF 2-CNPJ        ', 'TIPO DE INSCRI��O DA EMPRESA');
AL(L,I, '10.1 ', '019', '033', '015', 'N', '0', 'I', '        ', '401', '               ', 'N�MERO DO CPF/CNPJ  ', 'N�MERO DA INCRI��O DA EMPRESA (CPF/CNPJ)');
AL(L,I, '11.1 ', '034', '053', '020', 'N', ' ', 'X', '        ', '408', '               ', 'C�DIGO DO CONV�NIO  ', 'C�DIGO DO CONV�NIO NO BANCO');
AL(L,I, '12.1 ', '054', '058', '005', 'N', '0', 'I', '        ', '020', '               ', 'PREFIXO DA AG�NCIA  ', 'N�MERO DA AG�NCIA ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
AL(L,I, '13.1 ', '059', '059', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
AL(L,I, '14.1 ', '060', '071', '012', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
AL(L,I, '15.1 ', '072', '072', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
AL(L,I, '16.1 ', '073', '073', '001', 'N', ' ', 'X', '        ', '024', '               ', 'DV DA AG�NCIA/CONTA ', 'D�GITO VERIFICADOR DA AG�NCIA / CONTA CORRENTE');
AL(L,I, '17.1 ', '074', '103', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DA EMPRESA     ', 'NOME POR EXTENSO DA "EMPRESA M�E"');
AL(L,I, '18.1 ', '104', '143', '040', 'N', ' ', 'X', '        ', '211', '               ', 'MENSAGEM 1          ', 'MENSAGEM 1 - 40 CARACTERES');
AL(L,I, '19.1 ', '144', '183', '040', 'N', ' ', 'X', '        ', '212', '               ', 'MENSAGEM 2          ', 'MENSAGEM 2 - 40 CARACTERES');
AL(L,I, '20.1 ', '184', '191', '008', 'N', '0', 'I', '        ', '032', '               ', 'N�M. REMESSA/RETORNO', 'N�MERO REMESSA / RETORNO');
AL(L,I, '21.1 ', '192', '199', '008', 'N', '0', 'D', 'DDMMAAAA', '033', '               ', 'DATA GRAV. REM/RET  ', 'DATA DE GRAVA��O DA REMESSA / RETORNO');
AL(L,I, '22.1 ', '200', '207', '008', 'N', '0', 'D', 'DDMMAAAA', '581', '               ', 'DATA DE CR�DITO     ', 'DATA DO CR�DITO');
AL(L,I, '23.1 ', '208', '240', '033', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            //
            Result := cresOKField;
          end;
          3: // Registro de detalhe ( v�rios segmentos)
          begin
            if Segmento = 'P' then
            begin
//      'IdReg'  'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, '01.3P', '001', '003', '003', 'N', '0', 'I', '        ', '001', '               ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, '02.3P', '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, '03.3P', '008', '008', '001', 'N', '0', 'I', '        ', '014', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
AL(L,I, '04.3P', '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
AL(L,I, '05.3P', '014', '014', '001', 'N', ' ', 'X', '        ', '015', 'P              ', 'IGUAL A "P"         ', 'SEGMENTO DE REGISTRO: IGUAL A "P"');
AL(L,I, '06.3P', '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '07.3P', '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
AL(L,I, '08.3P', '018', '022', '005', 'N', '0', 'I', '        ', '020', '               ', 'AG�NCIA MANTENEDORA ', 'AG�NCIA MANTENEDORA DA CONTA');
AL(L,I, '09.3P', '023', '023', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
AL(L,I, '10.3P', '024', '035', '012', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE');
AL(L,I, '11.3P', '036', '036', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
AL(L,I, '12.3P', '037', '037', '001', 'N', ' ', 'X', '        ', '024', '               ', 'DV DA AG�NCIA/CONTA ', 'D�GITO VERIFICADOR DA AG�NCIA / CONTA CORRENTE');
AL(L,I, '13.3P', '038', '057', '020', 'N', ' ', 'X', '        ', '501', '               ', 'NOSSO N�MERO        ', 'IDENTIFICA��O DO T�TULO NO BANCO');
AL(L,I, '14.3P', '058', '058', '001', 'N', '0', 'I', '        ', '509', '               ', 'C�DIGO DA CARTEIRA  ', 'C�DIGO DA CARTEIRA');
AL(L,I, '15.3P', '059', '059', '001', 'N', '0', 'I', '        ', '034', '               ', 'FORMA CAD T�T NO BCO', 'FORMA DE CADASTRAMENTO DO T�TULO NO BANCO 1=SIM, 2=N�O');
AL(L,I, '16.3P', '060', '060', '001', 'N', '0', 'I', '        ', '508', '               ', 'TIPO DE DOCUMENTO   ', 'TIPO DE DOCUMENTO: 1=TRADICIONAL, 2=ESCRITURAL');
AL(L,I, '17.3P', '061', '061', '001', 'N', '0', 'I', '        ', '621', '               ', 'EMISS�O DO BLOQUETO ', 'IDENTIFICA��O DA EMISS�O DO BLOQUETO');
AL(L,I, '18.3P', '062', '062', '001', 'N', '0', 'I', '        ', '036', '               ', 'IDENT. DISTRIBUI��O ', 'IDENTIFICA��O DA DISTRIBUI��O: 1-BANCO, 2-CLIENTE');
AL(L,I, '19.3P', '063', '077', '015', 'S', ' ', 'X', '        ', '502', '               ', 'NUM DOC COBRANCA    ', 'N�MERO DO DOCUMENTO DE COBRAN�A (EXEMPLO: DUPLICATA)');
AL(L,I, '20.3P', '078', '085', '008', 'N', '0', 'D', 'DDMMAAAA', '580', '               ', 'DATA VENCIM. T�TULO ', 'DATA DO VENCIMENTO DO T�TULO');
AL(L,I, '21.3P', '086', '100', '015', 'N', '0', 'F', '2       ', '550', '               ', 'VALOR DO T�TULO     ', 'VALOR NOMINAL DO T�TULO');
AL(L,I, '22.3P', '101', '105', '005', 'N', '0', 'I', '        ', '025', '00000          ', 'AG�NCIA COBRADORA   ', 'PREFIXO AG�NCIA COBRADORA');
AL(L,I, '23.3P', '106', '106', '001', 'N', '0', 'I', '        ', '026', '0              ', 'DV AG�NCIA COBRADORA', 'D�GITO VERIFICADOR DA AG�NCIA COBRADORA');
AL(L,I, '24.3P', '107', '108', '002', 'N', '0', 'I', '        ', '507', '               ', 'ESP�CIE DO T�TULO   ', 'ESP�CIE DO T�TULO');
AL(L,I, '25.3P', '109', '109', '001', 'S', ' ', 'X', '        ', '520', '               ', 'ACEITE DO T�TULO    ', 'IDENTIFICA��O DO T�TULO ACEITO / N�O ACEITO');
AL(L,I, '26.3P', '110', '117', '008', 'N', '0', 'D', 'DDMMAAAA', '583', '               ', 'DATA EMISS�O T�TULO ', 'DATA DA EMISS�O DO T�TULO');
AL(L,I, '27.3P', '118', '118', '001', 'N', '0', 'I', '        ', '576', '               ', 'C�DIGO JUROS DE MORA', 'C�DIGO DE JUROS DE MORA: 1=VALOR, 2=% MENSAL, 3=ISENTO');
AL(L,I, '28.3P', '119', '126', '008', 'N', '0', 'D', 'DDMMAAAA', '584', '               ', 'DATA JUROS DE MORA  ', 'DATA DO JUROS DE MORA');
AL(L,I, '29.3P', '127', '141', '015', 'N', '0', 'F', '2       ', '572', '               ', 'J. MORA P/DIA - TAXA', 'JUROS DE MORA POR DIA / TAXA');
AL(L,I, '30.3P', '142', '142', '001', 'N', '0', 'I', '        ', '591', '               ', 'CODIGO DO DESCONTO 1', 'C�DIGO DO DESCONTO 1');
AL(L,I, '31.3P', '143', '150', '008', 'N', '0', 'D', 'DDMMAAAA', '592', '               ', 'DATA DO DESCONTO 1  ', 'DATA DO DESCONTO 1');
AL(L,I, '32.3P', '151', '165', '015', 'N', '0', 'F', '2       ', '593', '               ', 'VAL/% DO DESCONTO 1 ', 'VALOR / PERCENTUAL DO DESCONTO 1');
AL(L,I, '33.3P', '166', '180', '015', 'N', '0', 'F', '2       ', '569', '               ', 'VALOR DO IOF A RECOL', 'VALOR DO IOF A SER RECOLHIDO');
AL(L,I, '34.3P', '181', '195', '015', 'N', '0', 'F', '2       ', '551', '               ', 'VALOR DO ABATIMENTO ', 'VALOR DO ABATIMENTO');
AL(L,I, '35.3P', '196', '220', '25 ', 'S', ' ', 'X', '        ', '506', '               ', 'ID T�TULO NA EMPRESA', 'IDENTIFICA��O DO T�TULO NA EMPRESA');
AL(L,I, '36.3P', '221', '221', '001', 'N', '0', 'I', '        ', '651', '               ', 'C�DIGO PARA PROTESTO', 'C�DIGO PARA PROTESTO');
AL(L,I, '37.3P', '222', '223', '002', 'N', '0', 'I', '        ', '647', '               ', 'N� DIAS P/ PROTESTO ', 'N�MERO DE DIAS PARA PROTESTO');
AL(L,I, '38.3P', '224', '224', '001', 'N', '0', 'I', '        ', '652', '               ', 'C�D. BAIXA/DEVOLU��O', 'C�DIGO PARA BAIXA / DEVOLU��O');
AL(L,I, '39.3P', '225', '227', '003', 'N', '0', 'I', '        ', '653', '               ', 'N� DIAS BAIXA/DEVOLU', 'N�MERO DE DIAS PARA BAIXA / DEVOLU��O');
AL(L,I, '40.3P', '228', '229', '002', 'N', '0', 'I', '        ', '549', '               ', 'C�DIGO DA MOEDA     ', 'C�DIGO DA MOEDA');
AL(L,I, '41.3P', '230', '239', '010', 'N', '0', 'I', '        ', '654', '               ', 'N� CONTRATO OPERA��O', 'N�MERO DO CONTRATO DA OPERA��O DE CR�DITO');
AL(L,I, '42.3P', '240', '240', '001', 'S', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
              //
              Result := cresOKField;
            end else
            if Segmento = 'Q' then
            begin
//      'IdReg'         'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, '01.3Q', '001', '003', '003', 'N', '0', 'I', '        ', '001', '               ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, '02.3Q', '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, '03.3Q', '008', '008', '001', 'N', '0', 'I', '        ', '014', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
AL(L,I, '04.3Q', '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
AL(L,I, '05.3Q', '014', '014', '001', 'N', ' ', 'X', '        ', '015', 'Q              ', 'IGUAL A "Q"         ', 'SEGMENTO DE REGISTRO: IGUAL A "Q"');
AL(L,I, '06.3Q', '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '07.3Q', '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
AL(L,I, '08.3Q', '018', '018', '001', 'N', '0', 'I', '        ', '801', '               ', 'TIPO DE INSCRI��O   ', 'TIPO DE INSCRI��O DO SACADO (CNPJ OU CPF)');
AL(L,I, '09.3Q', '019', '033', '015', 'N', '0', 'I', '        ', '802', '               ', 'N�MERO DE INSCRI��O ', 'N�MERO DE INSCRI��O DO SACADO (CNPJ / CPF)');
AL(L,I, '10.3Q', '034', '073', '040', 'S', ' ', 'X', '        ', '803', '               ', 'NOME DO SACADO      ', 'NOME DO SACADO');
AL(L,I, '11.3Q', '074', '113', '040', 'S', ' ', 'X', '        ', '804', '               ', 'ENDERE�O DO SACADO  ', 'ENDERE�O DO SACADO (LOGRADOURO, N�MERO E COMPLEMENTO)');
AL(L,I, '12.3Q', '114', '128', '015', 'S', ' ', 'X', '        ', '805', '               ', 'BAIRRO DO SACADO    ', 'BAIRRO DO SACADO');
AL(L,I, '13.3Q', '129', '136', '008', 'N', '0', 'I', '        ', '806', '               ', 'CEP DO SACADO       ', 'CEP DO SACADO');
AL(L,I, '14.3Q', '137', '151', '015', 'S', ' ', 'X', '        ', '807', '               ', 'CIDADE DO SACADO    ', 'CIDADE DO SACADO');
AL(L,I, '15.3Q', '152', '153', '002', 'S', ' ', 'X', '        ', '808', '               ', 'UF DO SACADO        ', 'UF DO SADCADO');
AL(L,I, '16.3Q', '154', '154', '001', 'N', '0', 'I', '        ', '851', '               ', 'TIPO DE INSCRI��O   ', 'TIPO DE INSCRI��O DO SACADOR / AVALISTA (CNPJ OU CPF)');
AL(L,I, '17.3Q', '155', '169', '015', 'N', '0', 'I', '        ', '852', '               ', 'N�MERO DE INSCRI��O ', 'N�MERO DE INSCRI��O DO SACADOR / AVALISTA (CNPJ / CPF)');
AL(L,I, '18.3Q', '170', '209', '040', 'S', ' ', 'X', '        ', '853', '               ', 'NOME DO SACADOR/AVAL', 'NOME DO SACADOR / AVALISTA');
AL(L,I, '19.3Q', '210', '212', '003', 'N', '0', 'I', '        ', '-01', '               ', 'USO DOS BANCOS      ', 'C�DIGO DO BANCO CORRESPONDENTE NA COMPENSA��O');
AL(L,I, '20.3Q', '213', '232', '20 ', 'S', ' ', 'X', '        ', '-02', '               ', 'USO DOS BANCOS      ', 'NOSSO N�MERO NO BANCO CORRESPONDENTE');
AL(L,I, '21.3Q', '233', '240', '008', 'S', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
              //
              Result := cresOKField;
            end else
            if Segmento = 'R' then
            begin
//      'IdReg'  'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, '01.3R', '001', '003', '003', 'N', '0', 'I', '        ', '001', '               ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, '02.3R', '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, '03.3R', '008', '008', '001', 'N', '0', 'I', '        ', '014', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
AL(L,I, '04.3R', '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
AL(L,I, '05.3R', '014', '014', '001', 'N', ' ', 'X', '        ', '015', 'R              ', 'IGUAL A "R"         ', 'SEGMENTO DE REGISTRO: IGUAL A "R"');
AL(L,I, '06.3R', '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '07.3R', '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
AL(L,I, '08.3R', '018', '018', '001', 'N', '0', 'I', '        ', '594', '               ', 'CODIGO DO DESCONTO 2', 'C�DIGO DO DESCONTO 2');
AL(L,I, '09.3R', '019', '026', '008', 'N', '0', 'D', 'DDMMAAAA', '595', '               ', 'DATA DO DESCONTO 2  ', 'DATA DO DESCONTO 2');
AL(L,I, '10.3R', '027', '041', '015', 'N', '0', 'F', '2       ', '596', '               ', 'VAL/% DO DESCONTO 2 ', 'VALOR / PERCENTUAL DO DESCONTO 2');
AL(L,I, '11.3R', '042', '042', '001', 'N', '0', 'I', '        ', '597', '               ', 'CODIGO DO DESCONTO 3', 'C�DIGO DO DESCONTO 3');
AL(L,I, '12.3R', '043', '050', '008', 'N', '0', 'D', 'DDMMAAAA', '598', '               ', 'DATA DO DESCONTO 3  ', 'DATA DO DESCONTO 3');
AL(L,I, '13.3R', '051', '065', '015', 'N', '0', 'F', '2       ', '599', '               ', 'VAL/% DO DESCONTO 3 ', 'VALOR / PERCENTUAL DO DESCONTO 3');
AL(L,I, '14.3R', '066', '066', '001', 'N', '0', 'I', '        ', '577', '               ', 'C�DIGO DA MULTA     ', 'C�DIGO DA MULTA');
AL(L,I, '15.3R', '067', '074', '008', 'N', '0', 'D', 'DDMMAAAA', '587', '               ', 'DATA COBRA MULTA    ', 'DATA DA COBRAN�A DA MULTA');
AL(L,I, '16.3R', '075', '089', '015', 'N', '0', 'F', '2       ', '573', '               ', 'VALOR/% MULTA       ', 'VALOR / PERCENTUAL DE MULTA A SER APLICADO');
AL(L,I, '17.3R', '090', '099', '010', 'S', ' ', 'X', '        ', '-02', '               ', 'INFO DO BCO AO SACAD', 'INFORMA��O DO BANCO AO SACADO');
AL(L,I, '18.3R', '100', '139', '040', 'N', ' ', 'X', '        ', '213', '               ', 'MENSAGEM 3          ', 'MENSAGEM 3 - 40 CARACTERES');
AL(L,I, '19.3R', '140', '179', '040', 'N', ' ', 'X', '        ', '214', '               ', 'MENSAGEM 4          ', 'MENSAGEM 4 - 40 CARACTERES');
AL(L,I, '20.3R', '180', '199', '013', 'N', '0', 'I', '        ', '-01', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '21.3R', '200', '207', '008', 'N', '0', 'I', '        ', '196', '               ', 'C�DIGOS OCORR. SACAD', 'C�DIGOS DE OCORR�NCIA DO SACADO');
AL(L,I, '22.3R', '208', '210', '003', 'N', '0', 'I', '        ', '101', '               ', 'COD BCO DA CTA D�BIT', 'C�DIGO DO BANCO DA CONTA DO D�BITO');
AL(L,I, '23.3R', '211', '215', '005', 'N', '0', 'I', '        ', '102', '               ', 'COD AGE DA CTA D�BIT', 'C�DIGO DA AGENCIA DO D�BITO');
AL(L,I, '24.3R', '216', '216', '001', 'N', '0', 'I', '        ', '103', '               ', 'DV AGE DA CTA D�BITO', 'D�GITO VERIFICADOR DA AG�NCIA DO D�BITO');
AL(L,I, '25.3R', '217', '228', '012', 'N', '0', 'I', '        ', '104', '               ', 'CONTA CORRENTE D�BIT', 'CONTA CORRENTE DO D�BITO');
AL(L,I, '26.3R', '229', '229', '001', 'N', '0', 'I', '        ', '105', '               ', 'DV CONTA CORR. D�BIT', 'DV CONTA CORRENTE DO D�BITO');
AL(L,I, '27.3R', '230', '230', '001', 'N', '0', 'I', '        ', '106', '               ', 'DV AG�NCIA/CONTA COR', 'DV DA AG�NCIA / CONTA CORRENTE DO D�BITO');
AL(L,I, '28.3R', '231', '231', '001', 'N', '0', 'I', '        ', '100', '               ', 'ID EMISS AVISO D�BIT', 'IDENTIFICA��O DA EMISS�O DO AVISO DE D�BITO');
AL(L,I, '29.3R', '232', '240', '009', 'S', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
              //
              Result := cresOKField;
            end else
            if Segmento = 'S' then
            begin
//      'IdReg'  'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, '01.3S', '001', '003', '003', 'N', '0', 'I', '        ', '001', '               ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, '02.3S', '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, '03.3S', '008', '008', '001', 'N', '0', 'I', '        ', '014', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
AL(L,I, '04.3S', '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
AL(L,I, '05.3S', '014', '014', '001', 'N', ' ', 'X', '        ', '015', 'S              ', 'IGUAL A "S"         ', 'SEGMENTO DE REGISTRO: IGUAL A "S"');
AL(L,I, '06.3S', '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '07.3S', '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
AL(L,I, '08.3S', '018', '018', '001', 'N', '0', 'I', '        ', '197', '               ', 'IDENTIF. DA IMPRESS.', 'IDENTIFICA��O DA IMPRESS�O');
case SubSegmento of
  1,2:
  begin
AL(L,I, '09.3S', '019', '020', '002', 'N', '0', 'I', '        ', '198', '               ', 'N� DA LINHA A IMPRIM', 'N�MERO DA LINHA A SER IMPRESSA');
AL(L,I, '10.3S', '021', '160', '140', 'S', ' ', 'X', '        ', '199', '               ', 'MENSAGEM A IMPRIMIR ', 'MENSAGEM A SER IMPRESSA');
AL(L,I, '11.3S', '161', '162', '002', 'N', '0', 'I', '        ', '200', '               ', 'TIPO DE FONTE IMPRES', 'TIPO DE FONTE A SER IMPRESSA');
AL(L,I, '12.3S', '163', '240', '078', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
  end;
  3:
  begin
AL(L,I, '09.3S', '019', '058', '040', 'S', ' ', 'X', '        ', '205', '               ', 'MENSAGEM 5          ', 'MENSAGEM 5');
AL(L,I, '10.3S', '059', '098', '040', 'S', ' ', 'X', '        ', '206', '               ', 'MENSAGEM 6          ', 'MENSAGEM 6');
AL(L,I, '11.3S', '099', '138', '040', 'S', ' ', 'X', '        ', '207', '               ', 'MENSAGEM 7          ', 'MENSAGEM 7');
AL(L,I, '12.3S', '139', '178', '040', 'S', ' ', 'X', '        ', '208', '               ', 'MENSAGEM 8          ', 'MENSAGEM 8');
AL(L,I, '13.3S', '179', '218', '040', 'S', ' ', 'X', '        ', '209', '               ', 'MENSAGEM 9          ', 'MENSAGEM 9');
AL(L,I, '14.3S', '219', '240', '022', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
  end;
end;
              //
              Result := cresOKField;
            end else
            if Segmento = 'Y' then
            begin
//      'IdReg'  'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, '01.3Y', '001', '003', '003', 'N', '0', 'I', '        ', '001', '               ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, '02.3Y', '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, '03.3Y', '008', '008', '001', 'N', '0', 'I', '        ', '014', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
AL(L,I, '04.3Y', '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
AL(L,I, '05.3Y', '014', '014', '001', 'N', ' ', 'X', '        ', '015', 'Y              ', 'IGUAL A "Y"         ', 'SEGMENTO DE REGISTRO: IGUAL A "Y"');
AL(L,I, '06.3Y', '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'CANAB               ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '07.3Y', '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
              case SubSegmento of
                01:
                begin
AL(L,I, '08.3Y', '018', '019', '002', 'N', '0', 'I', '        ', '016', '01             ', 'C�DIGO SUB-SEGMENTO ', 'C�DIGO DO SUB-SEGMENTO');
AL(L,I, '09.3Y', '020', '020', '001', 'N', '0', 'I', '        ', '851', '               ', 'SACADOR-TIPO INSCRI.', 'TIPO DE INSCRI��O DO SACADOR / AVALISTA');
AL(L,I, '10.3Y', '021', '035', '015', 'N', '0', 'F', '        ', '852', '               ', 'SACADOR-N�M. INSCRI.', 'N�MERO DA INSCRI��O DO SACADOR / AVALISTA');
AL(L,I, '11.3Y', '036', '075', '040', 'S', ' ', 'X', '        ', '853', '               ', 'SACADOR-NOME        ', 'NOME DO SACADOR / AVALISTA');
AL(L,I, '12.3Y', '076', '115', '040', 'S', ' ', 'X', '        ', '854', '               ', 'SACADOR-ENDERE�O    ', 'ENDERE�O DO SACADOR / AVALISTA');
AL(L,I, '13.3Y', '116', '130', '015', 'S', ' ', 'X', '        ', '855', '               ', 'SACADOR-BAIRRO      ', 'BAIRRO DO SACADOR / AVALISTA');
AL(L,I, '14.3Y', '131', '135', '005', 'N', '0', 'I', '        ', '856', '               ', 'SACADOR-CEP PREFIXO ', 'PREFIXO DO CEP DO SACADOR / AVALISTA');
AL(L,I, '15.3Y', '136', '138', '003', 'N', '0', 'I', '        ', '856', '               ', 'SACADOR-CEP SUFIXO  ', 'PREFIXO DO CEP DO SACADOR / AVALISTA');
AL(L,I, '16.3Y', '139', '153', '015', 'S', ' ', 'X', '        ', '857', '               ', 'SACADOR-CIDADE      ', 'CIDADE DO SACADOR AVALISTA');
AL(L,I, '17.3Y', '154', '155', '002', 'N', ' ', 'X', '        ', '858', '               ', 'SACADOR-UF          ', 'UF DO SACADOR / AVALISTA');
AL(L,I, '18.3Y', '156', '240', '085', 'S', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
                end;
                04:
                begin
AL(L,I, '08.4Y', '018', '019', '002', 'N', '0', 'I', '        ', '016', '03             ', 'C�DIGO SUB-SEGMENTO ', 'C�DIGO DO SUB-SEGMENTO');
AL(L,I, '09.4Y', '020', '069', '050', 'N', ' ', 'X', '        ', '790', '               ', 'EMAIL P/ ENVIO INFO ', 'EMAIL PARA ENIO DE INFORMA��ES');
AL(L,I, '10.4Y', '070', '071', '002', 'N', '0', 'I', '        ', '791', '               ', 'DDD CELULAR P/ SMS  ', 'DDD DO CELULAR PARA ENVIO DE INFORMA��ES VIA SMS');
AL(L,I, '11.4Y', '072', '079', '008', 'N', '0', 'I', '        ', '792', '               ', 'N�M CELULAR P/ SMS  ', 'N�MERO DO CELULAR SEM O DDD PARA ENVIO DE INFORMA��ES VIA SMS');
AL(L,I, '12.4Y', '080', '240', '161', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
                end;
                05:
                begin
AL(L,I, '08.5Y', '018', '019', '002', 'N', '0', 'I', '        ', '016', '04             ', 'C�DIGO SUB-SEGMENTO ', 'C�DIGO DO SUB-SEGMENTO');
AL(L,I, '09.5Y', '020', '053', '034', 'N', ' ', 'X', '        ', '780', '               ', 'CMC7 DO CHEQUE 1    ', 'IDENTIFICA��O DO CHEQUE - CMC7');
AL(L,I, '10.5Y', '053', '087', '034', 'N', ' ', 'X', '        ', '780', '               ', 'CMC7 DO CHEQUE 2    ', 'IDENTIFICA��O DO CHEQUE - CMC7');
AL(L,I, '11.5Y', '088', '121', '034', 'N', ' ', 'X', '        ', '780', '               ', 'CMC7 DO CHEQUE 3    ', 'IDENTIFICA��O DO CHEQUE - CMC7');
AL(L,I, '12.5Y', '122', '155', '034', 'N', ' ', 'X', '        ', '780', '               ', 'CMC7 DO CHEQUE 4    ', 'IDENTIFICA��O DO CHEQUE - CMC7');
AL(L,I, '13.5Y', '156', '189', '034', 'N', ' ', 'X', '        ', '780', '               ', 'CMC7 DO CHEQUE 5    ', 'IDENTIFICA��O DO CHEQUE - CMC7');
AL(L,I, '14.5Y', '190', '223', '034', 'N', ' ', 'X', '        ', '780', '               ', 'CMC7 DO CHEQUE 6    ', 'IDENTIFICA��O DO CHEQUE - CMC7');
AL(L,I, '15.5Y', '224', '240', '017', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
                end;
                50:
                begin
AL(L,I, '08.3Y', '018', '019', '002', 'N', '0', 'I', '        ', '016', '50             ', 'C�DIGO SUB-SEGMENTO ', 'C�DIGO DO SUB-SEGMENTO');
AL(L,I, '09.3Y', '020', '024', '005', 'N', '0', 'I', '        ', '020', '               ', 'PREFIXO DA AG�NCIA  ', 'N�MERO DA AG�NCIA ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
AL(L,I, '10.3Y', '025', '025', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
AL(L,I, '11.3Y', '026', '037', '012', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
AL(L,I, '12.3Y', '038', '038', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
AL(L,I, '13.3Y', '039', '039', '001', 'N', ' ', 'X', '        ', '024', '               ', 'DV DA AG�NCIA/CONTA ', 'D�GITO VERIFICADOR DA AG�NCIA / CONTA CORRENTE');
AL(L,I, '14.3Y', '040', '059', '020', 'N', '0', 'I', '        ', '501', '               ', 'NOSSO N�MERO        ', 'IDENTIFICA��O DO T�TULO O BANCO');
AL(L,I, '15.3Y', '060', '060', '001', 'N', '0', 'I', '        ', '350', '               ', 'C�D CALC RATEIO BENE', 'C�DIGO DO C�LCULO DO RATEIO P/ BENEFICI�RIO');
AL(L,I, '16.3Y', '061', '061', '001', 'N', '0', 'I', '        ', '351', '               ', 'TIPO DE VALOR INFORM', 'TIPO DE VALOR INFORMADO ($ OU %)');
AL(L,I, '17.3Y', '062', '076', '015', 'N', '0', 'F', 'A       ', '352', '               ', 'VALOR OU %          ', 'VALOR OU PERCENTUAL DE RETEIO P/ BENEFICI�RIO');
AL(L,I, '18.3Y', '077', '079', '003', 'N', '0', 'I', '        ', '353', '               ', 'C�D BANCO BENEFICIAR', 'C�DIGO DO BANCO DO BENEFICI�RIO');
AL(L,I, '19.3Y', '080', '084', '005', 'N', '0', 'I', '        ', '354', '               ', 'C�D AG�NCIA BENEFICI', 'C�DIGO DA AG�NCIA DO BENEFICI�RIO');
AL(L,I, '20.3Y', '085', '085', '001', 'N', ' ', 'X', '        ', '355', '               ', 'DV AG�NCIA BENEFICIA', 'DV DA AG�NCIA DO BENEFICI�RIO');
AL(L,I, '21.3Y', '086', '097', '012', 'N', '0', 'I', '        ', '356', '               ', 'CONTA CORRENTE BENEF', 'CONTA CORRENTE DO BENEFICI�RIO');
AL(L,I, '22.3Y', '098', '098', '001', 'N', ' ', 'X', '        ', '357', '               ', 'DV DA CONTA DO BENEF', 'DV DA CONTA CORRENTE DO BENEFICI�RIO');
AL(L,I, '23.3Y', '099', '099', '001', 'N', ' ', 'X', '        ', '358', '               ', 'DV AG/CONTA DO BENEF', 'DV DA AG�NCIA / CONTA DO BENEFICI�RIO');
AL(L,I, '24.3Y', '100', '139', '040', 'S', ' ', 'X', '        ', '359', '               ', 'NOME BENEFICI�RIO   ', 'NOME DO BENEFICI�RIO');
AL(L,I, '25.3Y', '140', '145', '006', 'S', ' ', 'X', '        ', '360', '               ', 'IDENT.PARCELA RATEIO', 'IDENTIFICA��O DA PARCELA DO RATEIO');
AL(L,I, '26.3Y', '146', '148', '003', 'N', '0', 'I', '        ', '361', '               ', 'FLOAT               ', 'QUANTIDADE DE DIAS PARA CR�DITO BENEFICI�RIO');
AL(L,I, '27.3Y', '149', '156', '008', 'N', '0', 'D', 'DDMMAAAA', '362', '               ', 'DATA CREDITO BENEFIC', 'DATA DE CR�DITO DO BENEFICI�RIO');
AL(L,I, '28.3Y', '157', '166', '010', 'N', '0', 'I', '        ', '363', '               ', 'MOTVO OCORRIDO      ', 'IDENTIFICA��ES DAS REJEI��ES');
AL(L,I, '29.3Y', '167', '240', '074', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO DA FEBRABAN');
                end;
                51:
                begin
AL(L,I, '08.3Y', '018', '019', '002', 'N', '0', 'I', '        ', '016', '51             ', 'C�DIGO SUB-SEGMENTO ', 'C�DIGO DO SUB-SEGMENTO');
AL(L,I, '09.3Y', '020', '034', '015', 'N', ' ', 'X', '        ', '371', '               ', 'NOTA FISCAL 1       ', 'N�MERO DA NOTA FISCAL');
AL(L,I, '10.3Y', '035', '049', '013', 'N', '0', 'F', '2       ', '372', '               ', 'VALOR DA NOTA FISCAL', 'VALOR DA NOTA FISCAL');
AL(L,I, '11.3Y', '050', '057', '008', 'N', '0', 'D', 'DDMMAAAA', '373', '               ', 'DATA DA NOTA FISCAL ', 'DATA DA NOTA FISCAL');
AL(L,I, '12.3Y', '058', '072', '015', 'N', ' ', 'X', '        ', '371', '               ', 'NOTA FISCAL 2       ', 'N�MERO DA NOTA FISCAL');
AL(L,I, '13.3Y', '073', '087', '013', 'N', '0', 'F', '2       ', '372', '               ', 'VALOR DA NOTA FISCAL', 'VALOR DA NOTA FISCAL');
AL(L,I, '14.3Y', '088', '095', '008', 'N', '0', 'D', 'DDMMAAAA', '373', '               ', 'DATA DA NOTA FISCAL ', 'DATA DA NOTA FISCAL');
AL(L,I, '15.3Y', '096', '110', '015', 'N', ' ', 'X', '        ', '371', '               ', 'NOTA FISCAL 3       ', 'N�MERO DA NOTA FISCAL');
AL(L,I, '16.3Y', '111', '125', '013', 'N', '0', 'F', '2       ', '372', '               ', 'VALOR DA NOTA FISCAL', 'VALOR DA NOTA FISCAL');
AL(L,I, '17.3Y', '126', '133', '008', 'N', '0', 'D', 'DDMMAAAA', '373', '               ', 'DATA DA NOTA FISCAL ', 'DATA DA NOTA FISCAL');
AL(L,I, '18.3Y', '134', '148', '015', 'N', ' ', 'X', '        ', '371', '               ', 'NOTA FISCAL 4       ', 'N�MERO DA NOTA FISCAL');
AL(L,I, '19.3Y', '149', '163', '013', 'N', '0', 'F', '2       ', '372', '               ', 'VALOR DA NOTA FISCAL', 'VALOR DA NOTA FISCAL');
AL(L,I, '20.3Y', '164', '171', '008', 'N', '0', 'D', 'DDMMAAAA', '373', '               ', 'DATA DA NOTA FISCAL ', 'DATA DA NOTA FISCAL');
AL(L,I, '21.3Y', '172', '186', '015', 'N', ' ', 'X', '        ', '371', '               ', 'NOTA FISCAL 5       ', 'N�MERO DA NOTA FISCAL');
AL(L,I, '22.3Y', '187', '201', '013', 'N', '0', 'F', '2       ', '372', '               ', 'VALOR DA NOTA FISCAL', 'VALOR DA NOTA FISCAL');
AL(L,I, '23.3Y', '202', '209', '008', 'N', '0', 'D', 'DDMMAAAA', '373', '               ', 'DATA DA NOTA FISCAL ', 'DATA DA NOTA FISCAL');
AL(L,I, '24.3Y', '210', '240', '031', 'S', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
                end;
              end;
              //
              Result := cresOKField;
            end;
            if Segmento = 'T' then
            begin
//      'IdReg'  'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, '01.3T', '001', '003', '003', 'N', '0', 'I', '        ', '001', '               ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, '02.3T', '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, '03.3T', '008', '008', '001', 'N', '0', 'I', '        ', '014', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
AL(L,I, '04.3T', '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
AL(L,I, '05.3T', '014', '014', '001', 'N', ' ', 'X', '        ', '015', 'T              ', 'IGUAL A "T"         ', 'SEGMENTO DE REGISTRO: IGUAL A "T"');
AL(L,I, '06.3T', '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '07.3T', '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
AL(L,I, '08.3T', '018', '022', '005', 'N', '0', 'I', '        ', '020', '               ', 'AG�NCIA MANTENEDORA ', 'AG�NCIA MANTENEDORA DA CONTA');
AL(L,I, '09.3T', '023', '023', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
AL(L,I, '10.3T', '024', '035', '012', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE');
AL(L,I, '11.3T', '036', '036', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
AL(L,I, '12.3T', '037', '037', '001', 'N', ' ', 'X', '        ', '024', '               ', 'DV DA AG�NCIA/CONTA ', 'D�GITO VERIFICADOR DA AG�NCIA / CONTA CORRENTE');
AL(L,I, '13.3T', '038', '057', '020', 'N', ' ', 'X', '        ', '501', '               ', 'NOSSO N�MERO        ', 'IDENTIFICA��O DO T�TULO NO BANCO');
AL(L,I, '14.3T', '058', '058', '001', 'N', '0', 'I', '        ', '509', '               ', 'C�DIGO DA CARTEIRA  ', 'C�DIGO DA CARTEIRA');
AL(L,I, '15.3T', '059', '073', '015', 'N', ' ', 'X', '        ', '502', '               ', 'N�MERO DO DOCUMENTO ', 'N�MERO DO DOCUMENTO DE COBRAN�A');
AL(L,I, '16.3T', '074', '081', '008', 'N', '0', 'D', 'DDMMAAAA', '580', '               ', 'VENCIMENTO T�TULO   ', 'DATA DO VENCIMENTO DO T�TULO');
AL(L,I, '17.3T', '082', '096', '015', 'N', '0', 'F', '2       ', '550', '               ', 'VALOR DO T�TULO     ', 'VALOR DO T�TULO');
AL(L,I, '18.3T', '097', '099', '003', 'N', '0', 'I', '        ', '600', '               ', 'BANCO COBR/RECEBEDOR', 'C�DIGO DO BANCO COBRADOR/RECEBEDOR');
AL(L,I, '19.3T', '100', '104', '005', 'N', '0', 'I', '        ', '601', '               ', 'AG�NCIA COBR/RECEBED', 'C�DIGO DA AG�NCIA COBRADORA/RECEBEDORA');
AL(L,I, '20.3T', '105', '105', '001', 'N', '0', 'I', '        ', '602', '               ', 'DV AG. COBR/RECEBED.', 'D�GITO VERIFICADOR DA AG�NCIA COBRADORA/RECEBEDORA');
AL(L,I, '21.3T', '106', '130', '025', 'N', ' ', 'X', '        ', '506', '               ', 'USO DA EMPRESA      ', 'IDENTIFICA��O DO T�TULO NA EMPRESA');
AL(L,I, '22.3T', '131', '132', '002', 'N', '0', 'I', '        ', '549', '               ', 'C�DIGO DA MOEDA     ', 'C�DIGO DA MOEDA');
AL(L,I, '23.3T', '133', '133', '001', 'N', '0', 'I', '        ', '801', '               ', 'SACADO-TIPO DE INSCR', 'TIPO DE INSCRI��O DO SACADO');
AL(L,I, '24.3T', '134', '148', '015', 'N', '0', 'I', '        ', '802', '               ', 'SACAD0-N�M.DA INSCR.', 'N�MERO DA INSCRI��O DO SACADO');
AL(L,I, '25.3T', '149', '188', '040', 'S', ' ', 'X', '        ', '803', '               ', 'SACADO-NOME         ', 'NOME DO SACADO');
AL(L,I, '26.3T', '189', '198', '010', 'N', '0', 'I', '        ', '654', '               ', 'N�MERO DO CONTRATO  ', 'N�MERO DO CONTRATO DA OPERA��O DE CR�DITO');
AL(L,I, '27.3T', '199', '213', '015', 'N', '0', 'I', '        ', '570', '               ', 'VALOR TARIFAS/CUSTAS', 'VALOR DAS TARIFAS/CUSTAS');
AL(L,I, '28.3T', '214', '223', '010', 'N', ' ', 'X', '        ', '530', '               ', 'MOTIVO DA OCORR�NCIA', 'IDENTIFICA��O PARA REJEI��ES, TARIFAS, CUSTAS, LIQUIDA��O E BAIXAS');
AL(L,I, '29.3T', '224', '240', '017', 'S', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
              //
              Result := cresOKField;
            end;
            if Segmento = 'U' then
            begin
//      'IdReg'  'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, '01.3U', '001', '003', '003', 'N', '0', 'I', '        ', '001', '               ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, '02.3U', '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, '03.3U', '008', '008', '001', 'N', '0', 'I', '        ', '014', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
AL(L,I, '04.3U', '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
AL(L,I, '05.3U', '014', '014', '001', 'N', ' ', 'X', '        ', '015', 'U              ', 'IGUAL A "U"         ', 'SEGMENTO DE REGISTRO: IGUAL A "U"');
AL(L,I, '06.3U', '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '07.3U', '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
AL(L,I, '08.3U', '018', '032', '015', 'N', '0', 'F', '2       ', '557', '               ', 'ACR�SCIMOS          ', 'VALOR DE JUROS / MULTA / ENCARGOS');
AL(L,I, '09.3U', '033', '047', '015', 'N', '0', 'F', '2       ', '552', '               ', 'DESCONTO            ', 'VALOR DO DESCONTO CONCEDIDO');
AL(L,I, '10.3U', '048', '062', '015', 'N', '0', 'F', '2       ', '551', '               ', 'ABATIMENTO/CANCELAM.', 'VALOR DO ABATIMENTO CONCEDIDO / CANCELAMENTO');
AL(L,I, '11.3U', '063', '077', '015', 'N', '0', 'F', '2       ', '569', '               ', 'IOF RECOLHIDO       ', 'VALOR DO IOF RECOLHIDO');
AL(L,I, '12.3U', '078', '092', '015', 'N', '0', 'F', '2       ', '578', '               ', 'VALOR PAGO PELO SACA', 'VALOR PAGO PELO SACADO');
AL(L,I, '13.3U', '093', '107', '015', 'N', '0', 'F', '2       ', '579', '               ', 'VAL L�Q A CREDITAR  ', 'VALOR L�QUIDO A SER CREDITADO');
AL(L,I, '14.3U', '108', '122', '015', 'N', '0', 'F', '2       ', '585', '               ', 'OUTRAS DESPESAS     ', 'VALOR DE OUTRAS DESPESAS');
AL(L,I, '15.3U', '123', '137', '015', 'N', '0', 'F', '2       ', '554', '               ', 'OUTROS CR�DITOS     ', 'VALOR DE OUTROS CREDITOS');
AL(L,I, '16.3U', '138', '145', '008', 'N', '0', 'D', 'DDMMAAAA', '505', '               ', 'DATA DA OCORR�NCIA  ', 'DATA DA OCORR�NCIA');
AL(L,I, '17.3U', '146', '153', '008', 'N', '0', 'D', 'DDMMAAAA', '581', '               ', 'DATA DO CR�DITO     ', 'DATA DA EFETIVA��O DO CR�DITO');
AL(L,I, '18.3U', '154', '157', '004', 'N', ' ', 'X', '        ', '196', '               ', 'SACADO-C�D.OCORRENC.', 'C�DIGO DA OCORR�NCIA DO SACADO');
AL(L,I, '19.3U', '158', '165', '008', 'N', '0', 'D', 'DDMMAAAA', '195', '               ', 'SACADO-DATA DA OCORR', 'DATA DA OCORR�NCIA DO SACADO');
AL(L,I, '20.3U', '166', '180', '015', 'N', '0', 'F', '2       ', '194', '               ', 'SACADO-VALOR DA OCOR', 'VALOR DA OCORR�NCIA DO SACADO');
AL(L,I, '21.3U', '181', '210', '030', 'N', ' ', 'X', '        ', '193', '               ', 'SACADO-COMPLEM.OCORR', 'COMPLEMENTO DA OCORR�NCIA DO SACADO');
AL(L,I, '22.3U', '211', '213', '003', 'N', '0', 'I', '        ', '-01', '               ', 'C�D.BANCO CORR. COMP', 'C�DIGO DO BANCO CORRESPONDENTE NA COMPENSA��O');
AL(L,I, '23.3U', '214', '233', '020', 'N', '0', 'I', '        ', '-01', '               ', 'NOSSO NUM BCO CORRES', 'NOSSO N�MERO DO BANCO CORRESPONDENTE');
AL(L,I, '24.3U', '234', '240', '007', 'S', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
              //
              Result := cresOKField;
            end;
          end;
          5: // TRAILER DE LOTE - REGISTRO 5
          begin
//      'IdReg', 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, '01.5 ', '001', '003', '003', 'N', '0', 'I', '        ', '001', '               ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, '02.5 ', '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, '03.5 ', '008', '008', '001', 'N', '0', 'I', '        ', '014', '5              ', 'REGISTRO TRAILER LOT', 'REGISTRO TRAILER DE LOTE');
AL(L,I, '04.5 ', '009', '017', '009', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '05.5 ', '018', '023', '006', 'N', '0', 'I', '        ', '992', '               ', 'QTDE REGISTROS LOTE ', 'QUANTIDADE DE REGISTROS DO LOTE');
AL(L,I, '06.5 ', '024', '029', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'TOTALIZA��O DA COBRAN�A SIMPLES - QUANTIDADE DE T�TULOS EM COBRAN�A');
AL(L,I, '07.5 ', '030', '046', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'TOTALIZA��O DA COBRAN�A SIMPLES - VALOR TOTAL DOS T�TULOS EM CARTEIRA');
AL(L,I, '08.5 ', '047', '052', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'TOTALIZA��O DA COBRAN�A VINCULADA - QUANTIDADE DE T�TULOS EM COBRAN�A');
AL(L,I, '09.5 ', '053', '069', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'TOTALIZA��O DA COBRAN�A VINCULADA - VALOR TOTAL DOS T�TULOS EM CARTEIRA');
AL(L,I, '10.5 ', '070', '075', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'TOTALIZA��O DA COBRAN�A CAUCIONADA - QUANTIDADE DE T�TULOS EM COBRAN�A');
AL(L,I, '11.5 ', '076', '092', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'TOTALIZA��O DA COBRAN�A CAUCIONADA - VALOR TOTAL DOS T�TULOS EM CARTEIRA');
AL(L,I, '12.5 ', '093', '098', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'TOTALIZA��O DA COBRAN�A DESCONTADA - QUANTIDADE DE T�TULOS EM COBRAN�A');
AL(L,I, '13.5 ', '099', '115', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'TOTALIZA��O DA COBRAN�A DESCONTADA - VALOR TOTAL DOS T�TULOS EM CARTEIRA');
AL(L,I, '14.5 ', '116', '123', '008', 'S', '0', 'I', '        ', '-01', '               ', 'NUM AVISO LANCTO    ', 'N�MERO DO AVISO DE LAN�AMENTO');
AL(L,I, '15.5 ', '124', '240', '117', 'S', ' ', 'X', '        ', '-01', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            Result := cresOKField;
          end;
          9: // Registro Trailer (6 posi��es)
          begin
//      'IdReg', 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, '01.9 ', '001', '003', '003', 'N', '0', 'I', '        ', '001', '               ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, '02.9 ', '004', '007', '004', 'N', '0', 'I', '        ', '003', '9999           ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, '03.9 ', '008', '008', '001', 'N', '0', 'I', '        ', '014', '9              ', 'REGISTRO TRAILER ARQ', 'REGISTRO TRAILER DE ARQUIVO');
AL(L,I, '04.9 ', '009', '017', '009', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, '05.9 ', '018', '023', '006', 'N', '0', 'I', '        ', '993', '               ', 'QTDE DE LOTES DO ARQ', 'QUANTIDADE DE LOTES DO ARQUIVO');
AL(L,I, '06.9 ', '024', '029', '006', 'N', '0', 'I', '        ', '994', '               ', 'QTDE DE REGIS.DO ARQ', 'QUANTIDADE DE REGISTROS DO ARQUIVO');
AL(L,I, '07.9 ', '030', '035', '006', 'N', ' ', 'X', '        ', '-02', '               ', 'QTD C/C P/CONC LOTES', 'QUANTIDADE DE CONTAS P/ CONC.-LOTES');
AL(L,I, '08.9 ', '036', '240', '205', 'S', ' ', 'X', '        ', '-01', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            Result := cresOKField;
          end;
        end;
      //end;
    end;
    // Banco do Brasil
    001: // com registro
    begin
      Result := cresNoField;
      //if Posicoes = 6 then
      //begin
        case Registro of
          0: // Header de arquivo
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '001            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
            AL(L,I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '0000           ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O');
            AL(L,I, x, '008', '008', '001', 'N', '0', 'I', '        ', '010', '0              ', 'REGISTRO HEADER     ', 'REGISTRO HEADER DE ARQUIVO');
            AL(L,I, x, '009', '017', '009', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            AL(L,I, x, '018', '018', '001', 'N', '0', 'I', '        ', '400', '               ', '1-CPF 2-CNPJ        ', 'TIPO DE INSCRI��O DA EMPRESA');
            AL(L,I, x, '019', '032', '014', 'N', '0', 'I', '        ', '401', '               ', 'N�MERO DO CPF/CNPJ  ', 'N�MERO DA INCRI��O DA EMPRESA (CPF/CNPJ)');
            // CONV�NIO
            //AL(L,I, x, '033', '052', '020', 'N', ' ', 'X', '        ', '409', '               ', 'C�DIGO DO CONV�NIO  ', 'C�DIGO DO CONV�NIO NO BANCO');
{bco deixa }AL(L,I, x, '033', '041', '009', 'N', '0', 'I', '        ', '409', '               ', 'C�DIGO DO CONV�NIO  ', 'C�DIGO DO CONV�NIO L�DER NO BANCO');
{em branco} AL(L,I, x, '042', '045', '004', 'N', '0', 'I', '        ', '645', '               ', 'C�DIGO DO PRODUTO   ', 'C�DIGO DO PRODUTO (VARIA��O DA CARTEIRA?)');
            AL(L,I, x, '046', '047', '002', 'N', ' ', 'X', '        ', '655', '               ', 'CART COBR - COBR CED', 'CARTEIRA DE COBRAN�A (OBRIGAT�RIO PARA O CASO DE COBRAN�A CEDENTE)');
            AL(L,I, x, '048', '050', '003', 'N', ' ', 'X', '        ', '656', '               ', 'VARI CART - COBR CED', 'VARIA��O DA CARTEIRA DE COBRAN�A (OBRIGAT�RIO PARA O CASO DE COBRAN�A CEDENTE)');
            AL(L,I, x, '051', '052', '002', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            // FIM CONV�NIO
            AL(L,I, x, '053', '057', '005', 'N', '0', 'I', '        ', '020', '               ', 'PREFIXO DA AG�NCIA  ', 'N�MERO DA AG�NCIA ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, x, '058', '058', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
            AL(L,I, x, '059', '070', '012', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, x, '071', '071', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
            AL(L,I, x, '072', '072', '001', 'N', ' ', 'X', '        ', '024', '               ', 'DV DA AG�NCIA/CONTA ', 'D�GITO VERIFICADOR DA AG�NCIA / CONTA CORRENTE');
            AL(L,I, x, '073', '102', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DA EMPRESA     ', 'NOME POR EXTENSO DA "EMPRESA M�E"');
            AL(L,I, x, '103', '132', '030', 'S', ' ', 'X', '        ', '002', 'BANCO DO BRASIL S.A.', 'NOME DO BANCO       ', 'NOME POR EXTENSO DO BANCO COBRADOR');
            AL(L,I, x, '133', '142', '010', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            AL(L,I, x, '143', '143', '001', 'N', '0', 'I', '        ', '009', '               ', '1-REM E 2-RET       ', 'C�DIGO DE REMESSA OU RETORNO');
            AL(L,I, x, '144', '151', '008', 'N', '0', 'D', 'DDMMAAAA', '990', '               ', 'DATA DE GERA��O     ', 'DATA DE GERA��O DO ARQUIVO');
            AL(L,I, x, '152', '157', '006', 'N', '0', 'T', 'HHMMSS  ', '991', '               ', 'HORA DE GERA��O     ', 'HORA DE GERA��O DO ARQUIVO');
{998}       AL(L,I, x, '158', '163', '006', 'N', '0', 'I', '        ', '032', '               ', 'SEQUENCIAL          ', 'SEQUENCIAL DA REMESSA');
            AL(L,I, x, '164', '166', '003', 'N', '0', 'I', '        ', '889', '030            ', 'VERS�O DO LAYOUT    ', 'N�MERO DA VERS�O DO LAYOUT DO ARQUIVO');
            AL(L,I, x, '167', '171', '005', 'N', '0', 'I', '        ', '888', '               ', 'DENSIDADE GRAVA��O  ', 'DENSIDADE DE GRAVA��O DO ARQUIVO EM BPI');
            AL(L,I, x, '172', '191', '020', 'N', ' ', 'X', '        ', '-02', '               ', 'USO DO BANCO        ', 'USO PARA USO RESERVADO DO BANCO');
            AL(L,I, x, '192', '211', '020', 'N', ' ', 'X', '        ', '-02', '               ', 'USO DA EMPRESA      ', 'USO PARA USO RESERVADO DA EMPRESA');
            AL(L,I, x, '212', '222', '011', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            AL(L,I, x, '223', '225', '003', 'N', ' ', 'X', '        ', '036', '               ', 'IDENTIFICA��O CSP   ', 'IDENTIFICA��O DE COBRAN�A SEM PAPEL "CSP"');
            AL(L,I, x, '226', '228', '003', 'N', ' ', 'I', '        ', '-01', '               ', 'USO DAS VANS        ', 'USO EXCLUSIVO DAS VANS');
            AL(L,I, x, '229', '230', '002', 'N', ' ', 'X', '        ', '028', '               ', 'TIPO DO SERVI�O     ', 'TIPO DO SERVI�O: "02" = COBRAN�A SEM PAPEL');
            AL(L,I, x, '231', '240', '010', 'N', ' ', 'X', '        ', '030', '               ', 'C�D.OCOR.TIPO SERV. ', 'C�DIGOS DAS OCORR�NCIAS DO TIPO DE SERVI�O "02: COBRAN�A SEM PAPEL"');
            //
            Result := cresOKField;
          end;
          1: // Header de Lote
          begin
          //AL(L,I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '001            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
            AL(L,I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
            AL(L,I, x, '008', '008', '001', 'N', '0', 'I', '        ', '013', '1              ', 'REGISTRO HEADER LOTE', 'REGISTRO HEADER DE LOTE');
            AL(L,I, x, '009', '009', '001', 'N', ' ', 'X', '        ', '031', '               ', 'TIPO DE OPERA��O    ', 'TIPO DE OPERA��O');
            AL(L,I, x, '010', '011', '002', 'N', '0', 'I', '        ', '029', '01             ', 'TIPO DE SERVI�O     ', 'TIPO DE SERVI�O');
            AL(L,I, x, '012', '013', '002', 'N', '0', 'I', '        ', '-01', '00             ', 'FORMA DE LAN�AMENTO ', 'FORMA DE LAN�AMENTO');
            AL(L,I, x, '014', '016', '003', 'N', '0', 'I', '        ', '889', '020            ', 'VERSAO DO LAYOUT    ', 'N�MERO DA VERS�O DO LAYOUT');
            AL(L,I, x, '017', '017', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            AL(L,I, x, '018', '018', '001', 'N', '0', 'I', '        ', '400', '               ', '1-CPF 2-CNPJ        ', 'TIPO DE INSCRI��O DA EMPRESA');
            AL(L,I, x, '019', '033', '015', 'N', '0', 'I', '        ', '401', '               ', 'N�MERO DO CPF/CNPJ  ', 'N�MERO DA INCRI��O DA EMPRESA (CPF/CNPJ)');
            // CONV�NIO
            //AL(L,I, x, '034', '053', '020', 'N', ' ', 'X', '        ', '410', '               ', 'C�DIGO DO CONV�NIO  ', 'C�DIGO DO CONV�NIO NO BANCO');
            AL(L,I, x, '034', '042', '009', 'N', '0', 'I', '        ', '410', '               ', 'C�DIGO DO CONV�NIO  ', 'C�DIGO DO CONV�NIO COBRAN�A CEDENTE NO BANCO');
            AL(L,I, x, '043', '046', '004', 'N', '0', 'I', '        ', '645', '               ', 'C�DIGO DO PRODUTO   ', 'C�DIGO DO PRODUTO (VARIA��O DA CARTEIRA?)');
            AL(L,I, x, '047', '048', '002', 'N', ' ', 'X', '        ', '655', '               ', 'CART COBR - COBR CED', 'CARTEIRA DE COBRAN�A (OBRIGAT�RIO PARA O CASO DE COBRAN�A CEDENTE)');
            AL(L,I, x, '049', '051', '003', 'N', ' ', 'X', '        ', '656', '               ', 'VARI CART - COBR CED', 'VARIA��O DA CARTEIRA DE COBRAN�A (OBRIGAT�RIO PARA O CASO DE COBRAN�A CEDENTE)');
            AL(L,I, x, '052', '053', '002', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            // FIM CONV�NIO
            AL(L,I, x, '054', '058', '005', 'N', '0', 'I', '        ', '020', '               ', 'PREFIXO DA AG�NCIA  ', 'N�MERO DA AG�NCIA ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, x, '059', '059', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
            AL(L,I, x, '060', '071', '012', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, x, '072', '072', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
            AL(L,I, x, '073', '073', '001', 'N', ' ', 'X', '        ', '024', '               ', 'DV DA AG�NCIA/CONTA ', 'D�GITO VERIFICADOR DA AG�NCIA / CONTA CORRENTE');
            AL(L,I, x, '074', '103', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DA EMPRESA     ', 'NOME POR EXTENSO DA "EMPRESA M�E"');
            AL(L,I, x, '104', '143', '040', 'N', ' ', 'X', '        ', '211', '               ', 'MENSAGEM 1          ', 'MENSAGEM 1 - 40 CARACTERES');
            AL(L,I, x, '144', '183', '040', 'N', ' ', 'X', '        ', '212', '               ', 'MENSAGEM 2          ', 'MENSAGEM 2 - 40 CARACTERES');
            AL(L,I, x, '184', '191', '008', 'N', '0', 'I', '        ', '032', '               ', 'N�M. REMESSA/RETORNO', 'N�MERO REMESSA / RETORNO');
            AL(L,I, x, '192', '199', '008', 'N', '0', 'D', 'DDMMAAAA', '033', '               ', 'DATA GRAV. REM/RET  ', 'DATA DE GRAVA��O DA REMESSA / RETORNO');
            AL(L,I, x, '200', '207', '008', 'N', '0', 'D', 'DDMMAAAA', '581', '               ', 'DATA DE CR�DITO     ', 'DATA DO CR�DITO');
            AL(L,I, x, '208', '240', '033', 'N', ' ', 'X', '        ', '-03', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            //
            Result := cresOKField;
          end;
          3: // Registro de detalhe ( v�rios segmentos)
          begin
            if Segmento = 'P' then
            begin
            //AL(L,I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '001            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, x, '008', '008', '001', 'N', '0', 'I', '        ', '011', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
AL(L,I, x, '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
AL(L,I, x, '014', '014', '001', 'N', ' ', 'X', '        ', '080', 'P              ', 'IGUAL A "P"         ', 'SEGMENTO DE REGISTRO: IGUAL A "P"');
AL(L,I, x, '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, x, '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
AL(L,I, x, '018', '022', '005', 'N', '0', 'I', '        ', '020', '               ', 'AG�NCIA MANTENEDORA ', 'AG�NCIA MANTENEDORA DA CONTA');
AL(L,I, x, '023', '023', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
AL(L,I, x, '024', '035', '012', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE');
AL(L,I, x, '036', '036', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
AL(L,I, x, '037', '037', '001', 'N', ' ', 'X', '        ', '024', '               ', 'DV DA AG�NCIA/CONTA ', 'D�GITO VERIFICADOR DA AG�NCIA / CONTA CORRENTE');
AL(L,I, x, '038', '057', '020', 'N', ' ', 'X', '        ', '501', '               ', 'NOSSO N�MERO        ', 'NOSSO N�MERO (6 POSI��ES COM DV E 7 POSI��ES SEM DV)');
AL(L,I, x, '058', '058', '001', 'N', '0', 'I', '        ', '509', '               ', 'C�DIGO DA CARTEIRA  ', 'C�DIGO DA CARTEIRA');
AL(L,I, x, '059', '059', '001', 'N', '0', 'I', '        ', '034', '               ', 'FORMA CAD T�T NO BCO', 'FORMA DE CADASTRAMENTO DO T�TULO NO BANCO 1=SIM, 2=N�O');
AL(L,I, x, '060', '060', '001', 'N', '0', 'I', '        ', '508', '               ', 'TIPO DE DOCUMENTO   ', 'TIPO DE DOCUMENTO: 1=TRADICIONAL, 2=ESCRITURAL');
AL(L,I, x, '061', '061', '001', 'N', '0', 'I', '        ', '621', '               ', 'EMISS�O DO BLOQUETO ', 'IDENTIFICA��O DA EMISS�O DO BLOQUETO');
AL(L,I, x, '062', '062', '001', 'N', '0', 'I', '        ', '036', '               ', 'IDENT. DISTRIBUI��O ', 'IDENTIFICA��O DA DISTRIBUI��O: 1-BANCO, 2-CLIENTE');
AL(L,I, x, '063', '077', '015', 'S', ' ', 'X', '        ', '502', '               ', 'NUM DOC COBRANCA    ', 'N�MERO DO DOCUMENTO DE COBRAN�A (EXEMPLO: DUPLICATA)');
AL(L,I, x, '078', '085', '008', 'N', '0', 'D', 'DDMMAAAA', '580', '               ', 'DATA VENCIM. T�TULO ', 'DATA DO VENCIMENTO DO T�TULO');
AL(L,I, x, '086', '100', '015', 'N', '0', 'F', '2       ', '550', '               ', 'VALOR DO T�TULO     ', 'VALOR NOMINAL DO T�TULO');
AL(L,I, x, '101', '105', '005', 'N', '0', 'I', '        ', '025', '00000          ', 'AG�NCIA COBRADORA   ', 'PREFIXO AG�NCIA COBRADORA');
AL(L,I, x, '106', '106', '001', 'S', ' ', 'X', '        ', '026', '               ', 'DV AG�NCIA COBRADORA', 'D�GITO VERIFICADOR DA AG�NCIA COBRADORA');
AL(L,I, x, '107', '108', '002', 'N', '0', 'I', '        ', '507', '               ', 'ESP�CIE DO T�TULO   ', 'ESP�CIE DO T�TULO');
AL(L,I, x, '109', '109', '001', 'S', ' ', 'X', '        ', '520', '               ', 'ACEITE DO T�TULO    ', 'IDENTIFICA��O DO T�TULO ACEITO / N�O ACEITO');
AL(L,I, x, '110', '117', '008', 'N', '0', 'D', 'DDMMAAAA', '583', '               ', 'DATA EMISS�O T�TULO ', 'DATA DA EMISS�O DO T�TULO');
AL(L,I, x, '118', '118', '001', 'N', '0', 'I', '        ', '576', '               ', 'C�DIGO JUROS DE MORA', 'C�DIGO DE JUROS DE MORA: 1=VALOR, 2=% MENSAL, 3=ISENTO');
AL(L,I, x, '119', '126', '008', 'N', '0', 'D', 'DDMMAAAA', '584', '               ', 'DATA JUROS DE MORA  ', 'DATA DO JUROS DE MORA');
AL(L,I, x, '127', '141', '015', 'N', '0', 'F', '2       ', '572', '               ', 'J. MORA P/DIA - TAXA', 'JUROS DE MORA POR DIA/TAXA');
AL(L,I, x, '142', '142', '001', 'N', '0', 'I', '        ', '591', '               ', 'CODIGO DO DESCONTO 1', 'C�DIGO DO DESCONTO 1');
AL(L,I, x, '143', '150', '008', 'N', '0', 'D', 'DDMMAAAA', '592', '               ', 'DATA DO DESCONTO 1  ', 'DATA DO DESCONTO 1');
AL(L,I, x, '151', '165', '015', 'N', '0', 'F', '2       ', '593', '               ', 'VAL/% DO DESCONTO 1 ', 'VALOR / PERCENTUAL DO DESCONTO 1');
AL(L,I, x, '166', '180', '015', 'N', '0', 'F', '2       ', '569', '               ', 'VALOR DO IOF A RECOL', 'VALOR DO IOF A SER RECOLHIDO');
AL(L,I, x, '181', '195', '015', 'N', '0', 'F', '2       ', '551', '               ', 'VALOR DO ABATIMENTO ', 'VALOR DO ABATIMENTO');
AL(L,I, x, '196', '220', '25 ', 'S', ' ', 'X', '        ', '506', '               ', 'ID T�TULO NA EMPRESA', 'IDENTIFICA��O DO T�TULO NA EMPRESA');
AL(L,I, x, '221', '221', '001', 'N', '0', 'I', '        ', '651', '               ', 'C�DIGO PARA PROTESTO', 'C�DIGO PARA PROTESTO');
AL(L,I, x, '222', '223', '002', 'N', '0', 'I', '        ', '647', '               ', 'N� DIAS P/ PROTESTO ', 'N�MERO DE DIAS PARA PROTESTO');
// // Banco n�o implementou ainda!!
//AL(L,I, x, '224', '224', '001', 'N', '0', 'I', '        ', '652', '               ', 'C�D. BAIXA/DEVOLU��O', 'C�DIGO PARA BAIXA / DEVOLU��O');
AL(L,I, x, '224', '224', '001', 'N', '0', 'I', '        ', '-01', '               ', 'C�D. BAIXA/DEVOLU��O', 'C�DIGO PARA BAIXA / DEVOLU��O');
AL(L,I, x, '225', '227', '003', 'N', '0', 'I', '        ', '653', '               ', 'N� DIAS BAIXA/DEVOLU', 'N�MERO DE DIAS PARA BAIXA / DEVOLU��O');
AL(L,I, x, '228', '229', '002', 'N', '0', 'I', '        ', '549', '               ', 'C�DIGO DA MOEDA     ', 'C�DIGO DA MOEDA');
AL(L,I, x, '230', '239', '010', 'N', '0', 'I', '        ', '654', '               ', 'N� CONTRATO OPERA��O', 'N�MERO DO CONTRATO DA OPERA��O DE CR�DITO');
AL(L,I, x, '240', '240', '001', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
              //
              Result := cresOKField;
            end else
            if Segmento = 'Q' then
            begin
            //AL(L,I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '001            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, x, '008', '008', '001', 'N', '0', 'I', '        ', '011', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
AL(L,I, x, '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
AL(L,I, x, '014', '014', '001', 'N', ' ', 'X', '        ', '081', 'Q              ', 'IGUAL A "Q"         ', 'SEGMENTO DE REGISTRO: IGUAL A "Q"');
AL(L,I, x, '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, x, '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
AL(L,I, x, '018', '018', '001', 'N', '0', 'I', '        ', '801', '               ', 'TIPO DE INSCRI��O   ', 'TIPO DE INSCRI��O DO SACADO (CNPJ OU CPF)');
AL(L,I, x, '019', '033', '015', 'N', '0', 'I', '        ', '802', '               ', 'N�MERO DE INSCRI��O ', 'N�MERO DE INSCRI��O DO SACADO (CNPJ / CPF)');
AL(L,I, x, '034', '073', '040', 'S', ' ', 'X', '        ', '803', '               ', 'NOME DO SACADO      ', 'NOME DO SACADO');
AL(L,I, x, '074', '113', '040', 'S', ' ', 'X', '        ', '804', '               ', 'ENDERE�O DO SACADO  ', 'ENDERE�O DO SACADO (LOGRADOURO, N�MERO E COMPLEMENTO)');
AL(L,I, x, '114', '128', '015', 'S', ' ', 'X', '        ', '805', '               ', 'BAIRRO DO SACADO    ', 'BAIRRO DO SACADO');
AL(L,I, x, '129', '136', '008', 'N', '0', 'I', '        ', '806', '               ', 'CEP DO SACADO       ', 'CEP DO SACADO');
AL(L,I, x, '137', '151', '015', 'S', ' ', 'X', '        ', '807', '               ', 'CIDADE DO SACADO    ', 'CIDADE DO SACADO');
AL(L,I, x, '152', '153', '002', 'S', ' ', 'X', '        ', '808', '               ', 'UF DO SACADO        ', 'UF DO SADCADO');
AL(L,I, x, '154', '154', '001', 'N', '0', 'I', '        ', '851', '               ', 'TIPO DE INSCRI��O   ', 'TIPO DE INSCRI��O DO SACADOR / AVALISTA (CNPJ OU CPF)');
AL(L,I, x, '155', '169', '015', 'N', '0', 'I', '        ', '852', '               ', 'N�MERO DE INSCRI��O ', 'N�MERO DE INSCRI��O DO SACADOR / AVALISTA (CNPJ / CPF)');
AL(L,I, x, '170', '209', '040', 'S', ' ', 'X', '        ', '853', '               ', 'NOME DO SACADOR/AVAL', 'NOME DO SACADOR / AVALISTA');
AL(L,I, x, '210', '212', '003', 'N', '0', 'I', '        ', '-01', '               ', 'USO DOS BANCOS      ', 'C�DIGO DO BANCO CORRESPONDENTE NA COMPENSA��O');
AL(L,I, x, '213', '232', '20 ', 'S', ' ', 'X', '        ', '-02', '               ', 'USO DOS BANCOS      ', 'NOSSO N�MERO NO BANCO CORRESPONDENTE');
AL(L,I, x, '233', '240', '008', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
              //
              Result := cresOKField;
            end else
            if Segmento = 'R' then
            begin
            //AL(L,I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '001            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, x, '008', '008', '001', 'N', '0', 'I', '        ', '011', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
AL(L,I, x, '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
AL(L,I, x, '014', '014', '001', 'N', ' ', 'X', '        ', '082', 'R              ', 'IGUAL A "R"         ', 'SEGMENTO DE REGISTRO: IGUAL A "R"');
AL(L,I, x, '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, x, '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
AL(L,I, x, '018', '018', '001', 'N', '0', 'I', '        ', '594', '               ', 'CODIGO DO DESCONTO 2', 'C�DIGO DO DESCONTO 2');
AL(L,I, x, '019', '026', '008', 'N', '0', 'D', 'DDMMAAAA', '595', '               ', 'DATA DO DESCONTO 2  ', 'DATA DO DESCONTO 2');
AL(L,I, x, '027', '041', '015', 'N', '0', 'F', '2       ', '596', '               ', 'VAL/% DO DESCONTO 2 ', 'VALOR / PERCENTUAL DO DESCONTO 2');
AL(L,I, x, '042', '042', '001', 'N', '0', 'I', '        ', '597', '               ', 'CODIGO DO DESCONTO 3', 'C�DIGO DO DESCONTO 3');
AL(L,I, x, '043', '050', '008', 'N', '0', 'D', 'DDMMAAAA', '598', '               ', 'DATA DO DESCONTO 3  ', 'DATA DO DESCONTO 3');
AL(L,I, x, '051', '065', '015', 'N', '0', 'F', '2       ', '599', '               ', 'VAL/% DO DESCONTO 3 ', 'VALOR / PERCENTUAL DO DESCONTO 3');
AL(L,I, x, '066', '066', '001', 'N', '0', 'I', '        ', '577', '               ', 'C�DIGO DA MULTA     ', 'C�DIGO DA MULTA');
AL(L,I, x, '067', '074', '008', 'N', '0', 'D', 'DDMMAAAA', '587', '               ', 'DATA COBRA MULTA    ', 'DATA DA COBRAN�A DA MULTA');
AL(L,I, x, '075', '089', '015', 'N', '0', 'F', '2       ', '573', '               ', 'VALOR/% MULTA       ', 'VALOR / PERCENTUAL DE MULTA A SER APLICADO');
AL(L,I, x, '090', '099', '010', 'S', ' ', 'X', '        ', '-02', '               ', 'INFO DO BCO AO SACAD', 'INFORMA��O DO BANCO AO SACADO');
AL(L,I, x, '100', '139', '040', 'N', ' ', 'X', '        ', '213', '               ', 'MENSAGEM 3          ', 'MENSAGEM 3 - 40 CARACTERES');
AL(L,I, x, '140', '179', '040', 'N', ' ', 'X', '        ', '214', '               ', 'MENSAGEM 4          ', 'MENSAGEM 4 - 40 CARACTERES');
AL(L,I, x, '180', '182', '003', 'N', '0', 'I', '        ', '-01', '               ', 'COD BCO DA CTA D�BIT', 'C�DIGO DO BANCO DA CONTA DO D�BITO');
AL(L,I, x, '183', '186', '004', 'N', '0', 'I', '        ', '-01', '               ', 'COD AGE DA CTA D�BIT', 'C�DIGO DA AGENCIA DO D�BITO');
AL(L,I, x, '187', '199', '013', 'N', '0', 'I', '        ', '-01', '               ', 'CONTA CORR./DV D�BIT', 'CONTA CORRENTE / DV DO D�BITO');
AL(L,I, x, '200', '207', '008', 'N', '0', 'I', '        ', '-01', '               ', 'C�DIGOS OCORR. SACAD', 'C�DIGOS DE OCORR�NCIA DO SACADO');
AL(L,I, x, '208', '240', '033', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
              //
              Result := cresOKField;
            end else
            if Segmento = 'S' then
            begin
AL(L,I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '001            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, x, '008', '008', '001', 'N', '0', 'I', '        ', '011', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
AL(L,I, x, '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
AL(L,I, x, '014', '014', '001', 'N', ' ', 'X', '        ', '083', 'S              ', 'IGUAL A "S"         ', 'SEGMENTO DE REGISTRO: IGUAL A "S"');
AL(L,I, x, '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, x, '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
AL(L,I, x, '018', '018', '001', 'N', '0', 'I', '        ', '197', '               ', 'IDENTIF. DA IMPRESS.', 'IDENTIFICA��O DA IMPRESS�O');
case SubSegmento of
  1,2:
  begin
AL(L,I, x, '019', '020', '002', 'N', '0', 'I', '        ', '198', '               ', 'N� DA LINHA A IMPRIM', 'N�MERO DA LINHA A SER IMPRESSA');
AL(L,I, x, '021', '160', '140', 'S', ' ', 'X', '        ', '199', '               ', 'MENSAGEM A IMPRIMIR ', 'MENSAGEM A SER IMPRESSA');
AL(L,I, x, '161', '162', '002', 'N', '0', 'I', '        ', '200', '               ', 'TIPO DE CARACTER IMP', 'TIPO DE CARACTER A SER IMPRESSO');
AL(L,I, x, '163', '240', '078', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
  end;
  3:
  begin
AL(L,I, x, '019', '058', '040', 'S', ' ', 'X', '        ', '205', '               ', 'MENSAGEM 5          ', 'MENSAGEM 5');
AL(L,I, x, '059', '098', '040', 'S', ' ', 'X', '        ', '206', '               ', 'MENSAGEM 6          ', 'MENSAGEM 6');
AL(L,I, x, '099', '138', '040', 'S', ' ', 'X', '        ', '207', '               ', 'MENSAGEM 7          ', 'MENSAGEM 7');
AL(L,I, x, '139', '178', '040', 'S', ' ', 'X', '        ', '208', '               ', 'MENSAGEM 8          ', 'MENSAGEM 8');
AL(L,I, x, '179', '218', '040', 'S', ' ', 'X', '        ', '209', '               ', 'MENSAGEM 9          ', 'MENSAGEM 9');
AL(L,I, x, '219', '240', '022', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
  end;
end;              //
              Result := cresOKField;
            end;
          end;
          5: // TRAILER DE LOTE - REGISTRO 5
          begin
          //AL(L,I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '001            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, x, '008', '008', '001', 'N', '0', 'I', '        ', '012', '5              ', 'REGISTRO TRAILER LOT', 'REGISTRO TRAILER DE LOTE');
AL(L,I, x, '009', '017', '009', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, x, '018', '023', '006', 'N', '0', 'I', '        ', '992', '               ', 'QTDE REGISTROS LOTE ', 'QUANTIDADE DE REGISTROS DO LOTE');
AL(L,I, x, '024', '029', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'QUANTIDADE DE T�TULOS EM COBRAN�A');
AL(L,I, x, '030', '046', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'VALOR TOTAL DOS T�TULOS EM CARTEIRA');
AL(L,I, x, '047', '052', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'QUANTIDADE DE T�TULOS EM COBRAN�A');
AL(L,I, x, '053', '069', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'VALOR TOTAL DOS T�TULOS EM CARTEIRA');
AL(L,I, x, '070', '075', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'QUANTIDADE DE T�TULOS EM COBRAN�A');
AL(L,I, x, '076', '092', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'VALOR TOTAL DOS T�TULOS EM CARTEIRA');
AL(L,I, x, '093', '098', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'QUANTIDADE DE T�TULOS EM COBRAN�A');
AL(L,I, x, '099', '115', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'VALOR TOTAL DOS T�TULOS EM CARTEIRA');
AL(L,I, x, '116', '123', '008', 'S', '0', 'I', '        ', '-01', '               ', 'NUM AVISO LANCTO    ', 'N�MERO DO AVISO DE LAN�AMENTO');
AL(L,I, x, '124', '240', '117', 'S', ' ', 'X', '        ', '-01', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            Result := cresOKField;
          end;
          9: // Registro Trailer (6 posi��es)
          begin
          //AL(L,I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
AL(L,I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '001            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
AL(L,I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '9999           ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
AL(L,I, x, '008', '008', '001', 'N', '0', 'I', '        ', '012', '9              ', 'REGISTRO TRAILER ARQ', 'REGISTRO TRAILER DE ARQUIVO');
AL(L,I, x, '009', '017', '009', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, x, '018', '023', '006', 'N', '0', 'I', '        ', '993', '               ', 'QTDE DE LOTES DO ARQ', 'QUANTIDADE DE LOTES DO ARQUIVO');
AL(L,I, x, '024', '029', '006', 'N', '0', 'I', '        ', '994', '               ', 'QTDE DE REGIS.DO ARQ', 'QUANTIDADE DE REGISTROS DO ARQUIVO');
AL(L,I, x, '030', '035', '006', 'N', ' ', 'X', '        ', '-02', '               ', 'QTD C/C P/CONC LOTES', 'QUANTIDADE DE CONTAS P/ CONC.-LOTES');
AL(L,I, x, '036', '036', '001', 'S', ' ', 'X', '        ', '-01', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
AL(L,I, x, '037', '240', '204', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            Result := cresOKField;
          end;
        end;
      //end;
    end;
    //Sicoob
    756: // com registro
    begin
      if (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
      begin
        Result := cresNoField;
        //
        case Registro of
          0: // Header de arquivo
          begin
          //AL(L, I, 'INI', 'FIM', 'TAM',   'C', 'P', 'T',   'Format',      'FLD',       'Conteudo padr�o',         'Nome do campo', 'Descric�o do campo');
            AL(L, I,     x, '001', '003', '003', 'N', '0',        'I', '        ', '001', '756            ', 'C�DIGO DO BANCO     ', 'Identifica��o do Banco: "756"');
            AL(L, I,     x, '004', '007', '004', 'S', '0',        'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
            AL(L, I,     X, '008', '008', '001', 'N', '0',        'I', '        ', '014', '1              ', 'REGISTRO HEADER LOTE', 'REGISTRO HEADER DE LOTE');
            AL(L, I,     x, '009', '009', '001', 'N', ' ',        'X', '        ', '031', 'R              ', 'TIPO DE OPERA��O    ', 'TIPO DE OPERA��O');
            AL(L, I,     x, '010', '016', '007', 'S', '0',        'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
            AL(L, I,     x, '017', '018', '002', 'N', ' ',        'X', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            AL(L, I,     x, '019', '022', '004', 'N', '0',        'I', '        ', '020', '               ', 'COOPERATIVA         ', 'Cooperativa');
            AL(L, I,     x, '023', '029', '007', 'N', '0',        'I', '        ', '410', '               ', 'C�D. COBRAN�A       ', 'C�digo do Cliente/Cedente');
            AL(L, I,     x, '030', '039', '010', 'N', '0',        'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE');
            AL(L, I,     x, '040', '040', '001', 'N', ' ',        'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
            AL(L, I,     x, '041', '070', '030', 'N', ' ',        'X', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            AL(L, I,     x, '071', '100', '030', 'S', ' ',        'X', '        ', '402', '               ', 'NOME DA EMPRESA     ', 'NOME POR EXTENSO DA "EMPRESA M�E"');
            AL(L, I,     x, '101', '180', '080', 'N', ' ',        'X', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
{998}       AL(L, I,     x, '181', '188', '008', 'N', '0',        'I', '        ', '998', '               ', 'SEQUENCIAL          ', 'SEQUENCIAL DA REMESSA');
            AL(L, I,     x, '189', '196', '008', 'N', '0',        'D', 'DDMMAAAA', '990', '               ', 'DATA DE GERA��O     ', 'DATA DE GERA��O DO ARQUIVO');
            AL(L, I,     x, '197', '207', '011', 'S', '0',        'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
            AL(L, I,     x, '208', '240', '033', 'N', ' ',        'B', '        ', '-02', '               ', 'FILLER              ', 'BRANCOS');
            //
            Result := cresOKField;
          end;
          3: // Registro de detalhe ( v�rios segmentos)
          begin
            if Segmento = 'P' then
            begin
            //AL(L,I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
              AL(L,I, x, '001', '007', '007', 'S', '0', 'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
              AL(L,I, x, '008', '008', '001', 'N', '0', 'I', '        ', '011', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
              AL(L,I, x, '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
              AL(L,I, x, '014', '014', '001', 'S', ' ', 'X', '        ', '080', 'P              ', 'IGUAL A "P"         ', 'SEGMENTO DE REGISTRO: IGUAL A "P"');
              AL(L,I, x, '015', '015', '001', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
              AL(L,I, x, '016', '017', '002', 'N', '0', 'I', '        ', '504', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
              AL(L,I, x, '018', '040', '023', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
              AL(L,I, x, '041', '057', '017', 'N', ' ', 'X', '        ', '501', '               ', 'NOSSO N�MERO        ', 'NOSSO N�MERO (6 POSI��ES COM DV E 7 POSI��ES SEM DV)');
              AL(L,I, x, '058', '058', '001', 'S', '0', 'I', '        ', '509', '               ', 'C�DIGO DA CARTEIRA  ', 'C�DIGO DA CARTEIRA');
              AL(L,I, x, '059', '060', '002', 'N', '0', 'I', '        ', '507', '               ', 'TIPO DE DOCUMENTO   ', 'TIPO DE DOCUMENTO: 1=TRADICIONAL, 2=ESCRITURAL');
              AL(L,I, x, '061', '061', '001', 'N', '0', 'I', '        ', '621', '               ', 'EMISS�O DO BLOQUETO ', 'IDENTIFICA��O DA EMISS�O DO BLOQUETO');
              AL(L,I, x, '062', '062', '001', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
              AL(L,I, x, '063', '077', '015', 'S', ' ', 'X', '        ', '502', '               ', 'NUM DOC COBRANCA    ', 'N�MERO DO DOCUMENTO DE COBRAN�A (EXEMPLO: DUPLICATA)');
              AL(L,I, x, '078', '085', '008', 'N', '0', 'D', 'DDMMAAAA', '580', '               ', 'DATA VENCIM. T�TULO ', 'DATA DO VENCIMENTO DO T�TULO');
              AL(L,I, x, '086', '100', '015', 'S', '0', 'F', '2       ', '550', '               ', 'VALOR DO T�TULO     ', 'VALOR NOMINAL DO T�TULO');
              AL(L,I, x, '101', '106', '006', 'S', '0', 'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
              AL(L,I, x, '107', '107', '001', 'S', ' ', 'X', '        ', '520', '               ', 'ACEITE DO T�TULO    ', 'IDENTIFICA��O DO T�TULO ACEITO / N�O ACEITO');
              AL(L,I, x, '108', '109', '002', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
              AL(L,I, x, '110', '117', '008', 'N', '0', 'D', 'DDMMAAAA', '583', '               ', 'DATA EMISS�O T�TULO ', 'DATA DA EMISS�O DO T�TULO');
              AL(L,I, x, '118', '118', '001', 'N', '0', 'I', '        ', '576', '               ', 'C�DIGO JUROS DE MORA', 'C�DIGO DE JUROS DE MORA: 1=ISENTO, 2=VALOR, 3=PORCENTAGEM');
              AL(L,I, x, '119', '133', '015', 'N', '0', 'F', '2       ', '574', '               ', 'J. MORA P/DIA - %   ', 'JUROS DE MORA POR DIA/%');
              AL(L,I, x, '134', '142', '009', 'S', '0', 'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
              AL(L,I, x, '143', '150', '008', 'N', '0', 'D', 'DDMMAAAA', '592', '               ', 'DATA DO DESCONTO 1  ', 'DATA DO DESCONTO 1');
              AL(L,I, x, '151', '165', '015', 'N', '0', 'F', '2       ', '593', '               ', 'VAL/% DO DESCONTO 1 ', 'VALOR / PERCENTUAL DO DESCONTO 1');
              AL(L,I, x, '166', '180', '015', 'N', ' ', 'B', '        ', '-02', '               ', 'FILLER              ', 'BRANCOS');
              AL(L,I, x, '181', '195', '015', 'N', '0', 'F', '2       ', '551', '               ', 'VALOR DO ABATIMENTO ', 'VALOR DO ABATIMENTO');
              AL(L,I, x, '196', '220', '25 ', 'S', ' ', 'X', '        ', '506', '               ', 'ID T�TULO NA EMPRESA', 'IDENTIFICA��O DO T�TULO NA EMPRESA');
              AL(L,I, x, '221', '221', '001', 'N', '0', 'I', '        ', '651', '               ', 'C�DIGO PARA PROTESTO', 'C�DIGO PARA PROTESTO');
              AL(L,I, x, '222', '223', '002', 'S', '0', 'I', '        ', '647', '               ', 'N� DIAS P/ PROTESTO ', 'N�MERO DE DIAS PARA PROTESTO');
              AL(L,I, x, '224', '227', '004', 'S', '0', 'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
              AL(L,I, x, '228', '229', '002', 'N', '0', 'I', '        ', '549', '               ', 'C�DIGO DA MOEDA     ', 'C�DIGO DA MOEDA');
              AL(L,I, x, '230', '239', '010', 'S', '0', 'I', '        ', '654', '0              ', 'N� CONTRATO OPERA��O', 'N�MERO DO CONTRATO DA OPERA��O DE CR�DITO');
              AL(L,I, x, '240', '240', '001', 'S', '0', 'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
              //
              Result := cresOKField;
            end else
            if Segmento = 'Q' then
            begin
            //AL(L,I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
              AL(L,I, x, '001', '007', '007', 'S', '0', 'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
              AL(L,I, x, '008', '008', '001', 'N', '0', 'I', '        ', '011', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
              AL(L,I, x, '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
              AL(L,I, x, '014', '014', '001', 'S', ' ', 'X', '        ', '081', 'Q              ', 'IGUAL A "Q"         ', 'SEGMENTO DE REGISTRO: IGUAL A "Q"');
              AL(L,I, x, '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
              AL(L,I, x, '016', '017', '002', 'N', '0', 'I', '        ', '504', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
              AL(L,I, x, '018', '019', '002', 'S', '0', 'I', '        ', '801', '               ', 'TIPO DE INSCRI��O   ', 'TIPO DE INSCRI��O DO SACADO (CNPJ OU CPF)');
              AL(L,I, x, '020', '033', '014', 'N', '0', 'I', '        ', '802', '               ', 'N�MERO DE INSCRI��O ', 'N�MERO DE INSCRI��O DO SACADO (CNPJ / CPF)');
              AL(L,I, x, '034', '073', '040', 'S', ' ', 'X', '        ', '803', '               ', 'NOME DO SACADO      ', 'NOME DO SACADO');
              AL(L,I, x, '074', '113', '040', 'S', ' ', 'X', '        ', '804', '               ', 'ENDERE�O DO SACADO  ', 'ENDERE�O DO SACADO (LOGRADOURO, N�MERO E COMPLEMENTO)');
              AL(L,I, x, '114', '128', '015', 'S', ' ', 'X', '        ', '805', '               ', 'BAIRRO DO SACADO    ', 'BAIRRO DO SACADO');
              AL(L,I, x, '129', '136', '008', 'N', '0', 'I', '        ', '806', '               ', 'CEP DO SACADO       ', 'CEP DO SACADO');
              AL(L,I, x, '137', '151', '015', 'S', ' ', 'X', '        ', '807', '               ', 'CIDADE DO SACADO    ', 'CIDADE DO SACADO');
              AL(L,I, x, '152', '153', '002', 'S', ' ', 'X', '        ', '808', '               ', 'UF DO SACADO        ', 'UF DO SADCADO');
              AL(L,I, x, '154', '155', '002', 'S', '0', 'I', '        ', '851', '               ', 'TIPO DE INSCRI��O   ', 'TIPO DE INSCRI��O DO SACADOR / AVALISTA (CNPJ OU CPF)');
              AL(L,I, x, '156', '169', '014', 'N', '0', 'I', '        ', '852', '               ', 'N�MERO DE INSCRI��O ', 'N�MERO DE INSCRI��O DO SACADOR / AVALISTA (CNPJ / CPF)');
              AL(L,I, x, '170', '209', '040', 'S', ' ', 'X', '        ', '853', '               ', 'NOME DO SACADOR/AVAL', 'NOME DO SACADOR / AVALISTA');
              AL(L,I, x, '210', '240', '031', 'N', ' ', 'B', '        ', '-02', '               ', 'FILLER              ', 'BRANCOS');
              //
              Result := cresOKField;
            end;
          end;
          9: // Registro Trailer
          begin
          //AL(L,I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, x, '001', '007', '007', 'S', '0', 'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
            AL(L,I, x, '008', '008', '001', 'N', '0', 'I', '        ', '012', '5              ', 'REGISTRO TRAILER LOT', 'REGISTRO TRAILER DE LOTE');
            AL(L,I, x, '009', '017', '009', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            AL(L,I, x, '018', '023', '006', 'N', '0', 'I', '        ', '994', '               ', 'QTDE REGISTROS LOTE ', 'QUANTIDADE DE REGISTROS DO LOTE');
            AL(L,I, x, '024', '040', '017', 'N', '0', 'F', '2       ', '302', '               ', 'VAL TOT TIT CARTEIRA', 'VALOR TOTAL DOS T�TULOS EM CARTEIRA');
            AL(L,I, x, '041', '046', '006', 'S', '0', 'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
            AL(L,I, x, '047', '240', '194', 'N', ' ', 'B', '        ', '-02', '               ', 'FILLER              ', 'BRANCOS');
            //
            Result := cresOKField;
          end;
        end;
      end else
      if (NomeLayout = CO_756_240_2018) then
      begin
        Result := cresNoField;
        //
        case Registro of
          0: // Header de arquivo
          begin
          //AL(L, I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T',   'Format', 'FLD', 'Conteudo padr�o',        'Nome do campo', 'Descric�o do campo');
            AL(L, I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '756            ', 'C�DIGO DO BANCO     ', 'Identifica��o do Banco: "756"');
            AL(L, I, x, '004', '007', '004', 'S', '0', 'I', '        ', '-01', '0              ', 'ZEROS               ', 'ZEROS');
            AL(L, I, x, '008', '008', '001', 'N', '0', 'I', '        ', '014', '0              ', 'REGISTRO HEADER LOTE', 'REGISTRO HEADER DE LOTE');
            AL(L, I, x, '009', '017', '009', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            AL(L, I, x, '018', '018', '001', 'S', '0', 'I', '        ', '801', '               ', 'TIPO DE INSCRI��O   ', 'TIPO DE INSCRI��O DA EMPRESA (CNPJ OU CPF)');
            AL(L, I, x, '019', '032', '014', 'N', '0', 'I', '        ', '802', '               ', 'N�MERO DE INSCRI��O ', 'N�MERO DE INSCRI��O DO SACADO (CNPJ / CPF)');
            AL(L, I, x, '033', '052', '020', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            AL(L, I, x, '053', '057', '005', 'N', '0', 'I', '        ', '020', '               ', 'COOPERATIVA         ', 'COOPERATIVA');
            AL(L, I, x, '058', '058', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA COOPERATIVA   ', 'D�GITO VERIFICADOR DA COOPERATIVA');
            AL(L, I, x, '059', '070', '012', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE');
            AL(L, I, x, '071', '071', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
            AL(L, I, x, '072', '072', '001', 'N', '0', 'X', '        ', '024', '               ', 'DV DA AG�NCIA/CONTA ', 'D�GITO VERIFICADOR DA AG�NCIA / CONTA CORRENTE');
            AL(L, I, x, '073', '102', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DA EMPRESA     ', 'NOME POR EXTENSO DA EMPRESA');
            AL(L, I, x, '103', '132', '030', 'N', ' ', 'X', '        ', '002', 'SICOOB         ', 'NOME DO BANCO       ', 'NOME DO BANCO: SICOOB');
            AL(L, I, x, '133', '142', '010', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            AL(L, I, x, '143', '143', '001', 'N', '0', 'I', '        ', '009', '1              ', '1-REM E 2-RET       ', 'C�DIGO DE REMESSA OU RETORNO');
            AL(L, I, x, '144', '151', '008', 'N', '0', 'D', 'DDMMAAAA', '990', '               ', 'DATA DE GERA��O     ', 'DATA DE GERA��O DO ARQUIVO');
            AL(L, I, x, '152', '157', '006', 'N', '0', 'T', 'HHMMSS  ', '991', '               ', 'HORA DE GERA��O     ', 'HORA DE GERA��O DO ARQUIVO');
            AL(L, I, x, '158', '163', '006', 'N', '0', 'I', '        ', '998', '               ', 'SEQUENCIAL          ', 'SEQUENCIAL DA REMESSA');
            AL(L, I, x, '164', '166', '003', 'N', '0', 'I', '        ', '889', '081            ', 'VERS�O DO LAYOUT    ', 'N�MERO DA VERS�O DO LAYOUT DO ARQUIVO');
            AL(L, I, x, '167', '171', '005', 'N', '0', 'I', '        ', '888', '00000          ', 'DENSIDADE GRAVA��O  ', 'DENSIDADE DE GRAVA��O DO ARQUIVO');
            AL(L, I, x, '172', '191', '020', 'N', ' ', 'X', '        ', '-02', '               ', 'USO DO BANCO        ', 'USO PARA USO RESERVADO DO BANCO');
            AL(L, I, x, '192', '211', '020', 'N', ' ', 'X', '        ', '-02', '               ', 'USO DA EMPRESA      ', 'USO PARA USO RESERVADO DA EMPRESA');
            AL(L, I, x, '212', '240', '029', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            //
            Result := cresOKField;
          end;
          1: // Header de Lote
          begin
          //AL(L, I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L, I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '756            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
            AL(L, I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
            AL(L, I, x, '008', '008', '001', 'N', '0', 'I', '        ', '014', '1              ', 'REGISTRO HEADER LOTE', 'REGISTRO HEADER DE LOTE');
            AL(L, I, x, '009', '009', '001', 'N', ' ', 'X', '        ', '031', 'R              ', 'TIPO DE OPERA��O    ', 'TIPO DE OPERA��O');
            AL(L, I, x, '010', '011', '002', 'N', '0', 'I', '        ', '029', '01             ', 'TIPO DE SERVI�O     ', 'TIPO DE SERVI�O');
            AL(L, I, x, '012', '013', '002', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
            AL(L, I, x, '014', '016', '003', 'N', '0', 'I', '        ', '889', '040            ', 'VERSAO DO LAYOUT    ', 'N�MERO DA VERS�O DO LAYOUT');
            AL(L, I, x, '017', '017', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
            AL(L, I, x, '018', '018', '001', 'N', '0', 'I', '        ', '400', '               ', '1-CPF 2-CNPJ        ', 'TIPO DE INSCRI��O DA EMPRESA');
            AL(L, I, x, '019', '033', '015', 'N', '0', 'I', '        ', '401', '               ', 'N�MERO DO CPF/CNPJ  ', 'N�MERO DA INCRI��O DA EMPRESA (CPF/CNPJ)');
            AL(L, I, x, '034', '053', '020', 'N', ' ', 'X', '        ', '408', '               ', 'C�DIGO DO CONV�NIO  ', 'C�DIGO DO CONV�NIO NO BANCO');
            AL(L, I, x, '054', '058', '005', 'N', '0', 'I', '        ', '020', '               ', 'PREFIXO DA AG�NCIA  ', 'N�MERO DA AG�NCIA ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L, I, x, '059', '059', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
            AL(L, I, x, '060', '071', '012', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L, I, x, '072', '072', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
            AL(L, I, x, '073', '073', '001', 'N', ' ', 'X', '        ', '024', '               ', 'DV DA AG�NCIA/CONTA ', 'D�GITO VERIFICADOR DA AG�NCIA / CONTA CORRENTE');
            AL(L, I, x, '074', '103', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DA EMPRESA     ', 'NOME POR EXTENSO DA "EMPRESA M�E"');
            AL(L, I, x, '104', '143', '040', 'N', ' ', 'X', '        ', '211', '               ', 'MENSAGEM 1          ', 'MENSAGEM 1 - 40 CARACTERES');
            AL(L, I, x, '144', '183', '040', 'N', ' ', 'X', '        ', '212', '               ', 'MENSAGEM 2          ', 'MENSAGEM 2 - 40 CARACTERES');
            AL(L, I, x, '184', '191', '008', 'N', '0', 'I', '        ', '032', '               ', 'N�M. REMESSA/RETORNO', 'N�MERO REMESSA / RETORNO');
            AL(L, I, x, '192', '199', '008', 'N', '0', 'D', 'DDMMAAAA', '033', '               ', 'DATA GRAV. REM/RET  ', 'DATA DE GRAVA��O DA REMESSA / RETORNO');
            AL(L, I, x, '200', '207', '008', 'N', '0', 'D', 'DDMMAAAA', '581', '               ', 'DATA DE CR�DITO     ', 'DATA DO CR�DITO');
            AL(L, I, x, '208', '240', '033', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            //
            Result := cresOKField;
          end;
          3: // Registro de detalhe ( v�rios segmentos)
          begin
            if Segmento = 'P' then
            begin
            //AL(L, I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
              AL(L, I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '756            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
              AL(L, I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
              AL(L, I, x, '008', '008', '001', 'N', '0', 'I', '        ', '014', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
              AL(L, I, x, '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
              AL(L, I, x, '014', '014', '001', 'N', ' ', 'X', '        ', '015', 'P              ', 'IGUAL A "P"         ', 'SEGMENTO DE REGISTRO: IGUAL A "P"');
              AL(L, I, x, '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
              AL(L, I, x, '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
              AL(L, I, x, '018', '022', '005', 'N', '0', 'I', '        ', '020', '               ', 'AG�NCIA MANTENEDORA ', 'AG�NCIA MANTENEDORA DA CONTA');
              AL(L, I, x, '023', '023', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
              AL(L, I, x, '024', '035', '012', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE');
              AL(L, I, x, '036', '036', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
              AL(L, I, x, '037', '037', '001', 'N', ' ', 'X', '        ', '024', '               ', 'DV DA AG�NCIA/CONTA ', 'D�GITO VERIFICADOR DA AG�NCIA / CONTA CORRENTE');
              AL(L, I, x, '038', '047', '010', 'N', '0', 'I', '        ', '501', '               ', 'NOSSO N�MERO        ', 'IDENTIFICA��O DO T�TULO NO BANCO');
              AL(L, I, x, '048', '049', '002', 'N', '0', 'I', '        ', '513', '01             ', 'N�MERO DA PARCELA   ', 'N�MERO DA PARCELA: "01" SE PARCELA �NICA');
              AL(L, I, x, '050', '051', '002', 'N', '0', 'I', '        ', '509', '01             ', 'CARTEIRA/MODALIDADE ', 'CARTEIRA/MODALIDADE');
              AL(L, I, x, '052', '052', '001', 'N', '0', 'I', '        ', '-01', '4              ', 'TIPO DE FORMUL�RIO  ', 'TIPO DE FORMUL�RIO: 4 "A4 SEM EVELOPAMENTO"');
              AL(L, I, x, '053', '057', '005', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
              AL(L, I, x, '058', '058', '001', 'N', '0', 'I', '        ', '509', '               ', 'C�DIGO DA CARTEIRA  ', 'C�DIGO DA CARTEIRA');
              AL(L, I, x, '059', '059', '001', 'N', '0', 'I', '        ', '034', '0              ', 'FORMA CAD T�T NO BCO', 'FORMA DE CADASTRAMENTO DO T�TULO NO BANCO');
              AL(L, I, x, '060', '060', '001', 'N', ' ', 'X', '        ', '508', '               ', 'TIPO DE DOCUMENTO   ', 'TIPO DE DOCUMENTO');
              AL(L, I, x, '061', '061', '001', 'N', '0', 'I', '        ', '621', '               ', 'EMISS�O DO BLOQUETO ', 'IDENTIFICA��O DA EMISS�O DO BLOQUETO: 1-SICOOB EMITE, 2-BENEFICI�RIO EMITE');
              AL(L, I, x, '062', '062', '001', 'N', '0', 'I', '        ', '036', '               ', 'IDENT. DISTRIBUI��O ', 'IDENTIFICA��O DA DISTRIBUI��O: 1-SICOOB DISTRIBUI, 2-BENEFICI�RIO DISTRIBUI');
              AL(L, I, x, '063', '077', '015', 'S', ' ', 'X', '        ', '502', '               ', 'NUM DOC COBRANCA    ', 'N�MERO DO DOCUMENTO DE COBRAN�A (EXEMPLO: DUPLICATA)');
              AL(L, I, x, '078', '085', '008', 'N', '0', 'D', 'DDMMAAAA', '580', '               ', 'DATA VENCIM. T�TULO ', 'DATA DO VENCIMENTO DO T�TULO');
              AL(L, I, x, '086', '100', '015', 'N', '0', 'F', '2       ', '550', '               ', 'VALOR DO T�TULO     ', 'VALOR NOMINAL DO T�TULO');
              AL(L, I, x, '101', '105', '005', 'N', '0', 'I', '        ', '025', '00000          ', 'AG�NCIA COBRADORA   ', 'PREFIXO AG�NCIA COBRADORA');
              AL(L, I, x, '106', '106', '001', 'N', ' ', 'X', '        ', '026', '               ', 'DV AG�NCIA COBRADORA', 'D�GITO VERIFICADOR DA AG�NCIA COBRADORA');
              AL(L, I, x, '107', '108', '002', 'N', '0', 'I', '        ', '507', '               ', 'ESP�CIE DO T�TULO   ', 'ESP�CIE DO T�TULO');
              AL(L, I, x, '109', '109', '001', 'S', ' ', 'X', '        ', '520', '               ', 'ACEITE DO T�TULO    ', 'IDENTIFICA��O DO T�TULO ACEITO / N�O ACEITO');
              AL(L, I, x, '110', '117', '008', 'N', '0', 'D', 'DDMMAAAA', '583', '               ', 'DATA EMISS�O T�TULO ', 'DATA DA EMISS�O DO T�TULO');
              AL(L, I, x, '118', '118', '001', 'N', '0', 'I', '        ', '576', '               ', 'C�DIGO JUROS DE MORA', 'C�DIGO DE JUROS DE MORA: 0=ISENTO, 2=VALOR POR DIA, 3=TAXA MENSAL');
              AL(L, I, x, '119', '126', '008', 'N', '0', 'D', 'DDMMAAAA', '584', '               ', 'DATA JUROS DE MORA  ', 'DATA DO JUROS DE MORA');
              AL(L, I, x, '127', '141', '015', 'N', '0', 'F', '2       ', '572', '               ', 'J. MORA P/DIA - TAXA', 'JUROS DE MORA POR DIA / TAXA');
              AL(L, I, x, '142', '142', '001', 'N', '0', 'I', '        ', '591', '               ', 'CODIGO DO DESCONTO 1', 'C�DIGO DO DESCONTO: 0=N�O CONCEDER DESCONTO, 1=VALOR FIXO AT� A DATA INFORMADA, 2=PERCENTUAL AT� DATA INFORMADA');
              AL(L, I, x, '143', '150', '008', 'N', '0', 'D', 'DDMMAAAA', '592', '               ', 'DATA DO DESCONTO 1  ', 'DATA DO DESCONTO 1');
              AL(L, I, x, '151', '165', '015', 'N', '0', 'F', '2       ', '593', '               ', 'VAL/% DO DESCONTO 1 ', 'VALOR / PERCENTUAL DO DESCONTO 1');
              AL(L, I, x, '166', '180', '015', 'N', '0', 'F', '2       ', '569', '               ', 'VALOR DO IOF A RECOL', 'VALOR DO IOF A SER RECOLHIDO');
              AL(L, I, x, '181', '195', '015', 'N', '0', 'F', '2       ', '551', '               ', 'VALOR DO ABATIMENTO ', 'VALOR DO ABATIMENTO');
              AL(L, I, x, '196', '220', '25 ', 'S', ' ', 'X', '        ', '506', '               ', 'ID T�TULO NA EMPRESA', 'IDENTIFICA��O DO T�TULO NA EMPRESA');
              AL(L, I, x, '221', '221', '001', 'N', '0', 'I', '        ', '651', '               ', 'C�DIGO PARA PROTESTO', 'C�DIGO PARA PROTESTO');
              AL(L, I, x, '222', '223', '002', 'N', '0', 'I', '        ', '647', '               ', 'N� DIAS P/ PROTESTO ', 'N�MERO DE DIAS PARA PROTESTO');
              AL(L, I, x, '224', '224', '001', 'N', '0', 'I', '        ', '652', '0              ', 'C�D. BAIXA/DEVOLU��O', 'C�DIGO PARA BAIXA / DEVOLU��O');
              AL(L, I, x, '225', '227', '003', 'S', ' ', 'I', '        ', '653', '               ', 'N� DIAS BAIXA/DEVOLU', 'N�MERO DE DIAS PARA BAIXA / DEVOLU��O');
              AL(L, I, x, '228', '229', '002', 'N', '0', 'I', '        ', '549', '               ', 'C�DIGO DA MOEDA     ', 'C�DIGO DA MOEDA: 02-D�LAR COMERCIAL AMERICANO (VENDA), 09-REAL');
              AL(L, I, x, '230', '239', '010', 'N', '0', 'I', '        ', '654', '               ', 'N� CONTRATO OPERA��O', 'N�MERO DO CONTRATO DA OPERA��O DE CR�DITO');
              AL(L, I, x, '240', '240', '001', 'S', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
              //
              Result := cresOKField;
            end else
            if Segmento = 'Q' then
            begin
            //AL(L, I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
              AL(L, I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '756            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
              AL(L, I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
              AL(L, I, x, '008', '008', '001', 'N', '0', 'I', '        ', '014', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
              AL(L, I, x, '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
              AL(L, I, x, '014', '014', '001', 'N', ' ', 'X', '        ', '015', 'Q              ', 'IGUAL A "Q"         ', 'SEGMENTO DE REGISTRO: IGUAL A "Q"');
              AL(L, I, x, '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
              AL(L, I, x, '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
              AL(L, I, x, '018', '018', '001', 'N', '0', 'I', '        ', '801', '               ', 'TIPO DE INSCRI��O   ', 'TIPO DE INSCRI��O DO SACADO (CNPJ OU CPF)');
              AL(L, I, x, '019', '033', '015', 'N', '0', 'I', '        ', '802', '               ', 'N�MERO DE INSCRI��O ', 'N�MERO DE INSCRI��O DO SACADO (CNPJ / CPF)');
              AL(L, I, x, '034', '073', '040', 'S', ' ', 'X', '        ', '803', '               ', 'NOME DO SACADO      ', 'NOME DO SACADO');
              AL(L, I, x, '074', '113', '040', 'S', ' ', 'X', '        ', '804', '               ', 'ENDERE�O DO SACADO  ', 'ENDERE�O DO SACADO (LOGRADOURO, N�MERO E COMPLEMENTO)');
              AL(L, I, x, '114', '128', '015', 'S', ' ', 'X', '        ', '805', '               ', 'BAIRRO DO SACADO    ', 'BAIRRO DO SACADO');
              AL(L, I, x, '129', '136', '008', 'N', '0', 'I', '        ', '806', '               ', 'CEP DO SACADO       ', 'CEP DO SACADO');
              AL(L, I, x, '137', '151', '015', 'S', ' ', 'X', '        ', '807', '               ', 'CIDADE DO SACADO    ', 'CIDADE DO SACADO');
              AL(L, I, x, '152', '153', '002', 'S', ' ', 'X', '        ', '808', '               ', 'UF DO SACADO        ', 'UF DO SADCADO');
              AL(L, I, x, '154', '154', '001', 'N', '0', 'I', '        ', '851', '               ', 'TIPO DE INSCRI��O   ', 'TIPO DE INSCRI��O DO SACADOR / AVALISTA (CNPJ OU CPF)');
              AL(L, I, x, '155', '169', '015', 'N', '0', 'I', '        ', '852', '               ', 'N�MERO DE INSCRI��O ', 'N�MERO DE INSCRI��O DO SACADOR / AVALISTA (CNPJ / CPF)');
              AL(L, I, x, '170', '209', '040', 'S', ' ', 'X', '        ', '853', '               ', 'NOME DO SACADOR/AVAL', 'NOME DO SACADOR / AVALISTA');
              AL(L, I, x, '210', '212', '003', 'N', '0', 'I', '        ', '-01', '               ', 'USO DOS BANCOS      ', 'C�DIGO DO BANCO CORRESPONDENTE NA COMPENSA��O');
              AL(L, I, x, '213', '232', '020', 'S', ' ', 'X', '        ', '-02', '               ', 'USO DOS BANCOS      ', 'NOSSO N�MERO NO BANCO CORRESPONDENTE');
              AL(L, I, x, '233', '240', '008', 'S', ' ', 'X', '        ', '-02', '               ', 'CNAB                ', 'USO EXCLUSIVO FEBRABAN');
              //
              Result := cresOKField;
            end else
            if Segmento = 'R' then
            begin
            //AL(L, I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo                 ', 'Descric�o do campo');
              AL(L, I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '756            ', 'C�DIGO DO BANCO               ', 'C�DIGO DO BANCO NA COMPENSA��O');
              AL(L, I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O               ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
              AL(L, I, x, '008', '008', '001', 'N', '0', 'I', '        ', '011', '3              ', 'REGISTRO DETALHE              ', 'REGISTRO DETALHE');
              AL(L, I, x, '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE           ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
              AL(L, I, x, '014', '014', '001', 'N', ' ', 'X', '        ', '082', 'R              ', 'IGUAL A "R"                   ', 'SEGMENTO DE REGISTRO: IGUAL A "R"');
              AL(L, I, x, '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS                       ', 'USO EXCLUSIVO FEBRABAN');
              AL(L, I, x, '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO           ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
              AL(L, I, x, '018', '018', '001', 'N', '0', 'I', '        ', '594', '               ', 'CODIGO DO DESCONTO 2          ', 'C�DIGO DO DESCONTO 2: 0-N�O CONCEDER DESCONTO, 1-VALOR FIXO AT� A DATA INFORMADA, 2-PERCENTUAL AT� A DATA INFORMADA');
              AL(L, I, x, '019', '026', '008', 'N', '0', 'D', 'DDMMAAAA', '595', '               ', 'DATA DO DESCONTO 2            ', 'DATA DO DESCONTO 2');
              AL(L, I, x, '027', '041', '015', 'N', '0', 'F', '2       ', '596', '               ', 'VAL/% DO DESCONTO 2           ', 'VALOR / PERCENTUAL DO DESCONTO 2');
              AL(L, I, x, '042', '042', '001', 'N', '0', 'I', '        ', '597', '               ', 'CODIGO DO DESCONTO 3          ', 'C�DIGO DO DESCONTO 3: 0-N�O CONCEDER DESCONTO, 1-VALOR FIXO AT� A DATA INFORMADA, 2-PERCENTUAL AT� A DATA INFORMADA');
              AL(L, I, x, '043', '050', '008', 'N', '0', 'D', 'DDMMAAAA', '598', '               ', 'DATA DO DESCONTO 3            ', 'DATA DO DESCONTO 3');
              AL(L, I, x, '051', '065', '015', 'N', '0', 'F', '2       ', '599', '               ', 'VAL/% DO DESCONTO 3           ', 'VALOR / PERCENTUAL DO DESCONTO 3');
              AL(L, I, x, '066', '066', '001', 'N', '0', 'I', '        ', '577', '               ', 'C�DIGO DA MULTA               ', 'C�DIGO DA MULTA: 0-ISENTO, 1-VALOR FIXO, 2-PERCENTUAL');
              AL(L, I, x, '067', '074', '008', 'N', '0', 'D', 'DDMMAAAA', '587', '               ', 'DATA COBRA MULTA              ', 'DATA DA COBRAN�A DA MULTA');
              AL(L, I, x, '075', '089', '015', 'N', '0', 'F', '2       ', '573', '               ', 'VALOR/% MULTA                 ', 'VALOR / PERCENTUAL DE MULTA A SER APLICADO');
              AL(L, I, x, '090', '099', '010', 'S', ' ', 'X', '        ', '-02', '               ', 'INFO DO BCO AO SACAD          ', 'INFORMA��O DO BANCO AO SACADO');
              AL(L, I, x, '100', '139', '040', 'N', ' ', 'X', '        ', '213', '               ', 'MENSAGEM 3                    ', 'MENSAGEM 3 - 40 CARACTERES');
              AL(L, I, x, '140', '179', '040', 'N', ' ', 'X', '        ', '214', '               ', 'MENSAGEM 4                    ', 'MENSAGEM 4 - 40 CARACTERES');
              AL(L, I, x, '180', '199', '020', 'S', ' ', 'X', '        ', '-02', '               ', 'CNAB                          ', 'USO EXCLUSIVO FEBRABAN');
              AL(L, I, x, '200', '207', '008', 'S', '0', 'I', '        ', '-01', '00000000       ', 'COD. OCOR. PAGADOR            ', 'C�D. OCOR. PAGADOR: 00000000');
              AL(L, I, x, '208', '210', '003', 'N', '0', 'I', '        ', '-01', '000            ', 'COD BCO DA CTA D�BIT          ', 'C�DIGO DO BANCO DA CONTA DO D�BITO');
              AL(L, I, x, '211', '215', '005', 'N', '0', 'I', '        ', '-01', '00000          ', 'COD AGE DA CTA D�BIT          ', 'C�DIGO DA AGENCIA DO D�BITO');
              AL(L, I, x, '216', '216', '001', 'S', ' ', 'X', '        ', '-02', '               ', 'DV AGE DA CTA D�BIT           ', 'DV DA AGENCIA DO D�BITO');
              AL(L, I, x, '217', '228', '012', 'N', '0', 'I', '        ', '-01', '000000000000   ', 'CONTA CORR. D�BIT             ', 'CONTA CORRENTE / DV DO D�BITO');
              AL(L, I, x, '229', '229', '001', 'S', ' ', 'X', '        ', '-02', '               ', 'DV CONTA CORR. D�BIT          ', 'DV DA CONTA CORRENTE D�BITO');
              AL(L, I, x, '230', '230', '001', 'S', ' ', 'X', '        ', '-02', '               ', 'DV DA AG�NCIA/CONTA D�BIT     ', 'DV DA AGENCIA/CONTA D�BITO');
              AL(L, I, x, '231', '231', '001', 'N', '0', 'I', '        ', '-01', '0              ', 'IDENT. DA EMISS�O DO AVISO D�B.', 'C�DIGOS DE OCORR�NCIA DO SACADO');
              AL(L, I, x, '232', '240', '009', 'S', ' ', 'X', '        ', '-02', '               ', 'CNAB                           ', 'USO EXCLUSIVO FEBRABAN');
              //
              Result := cresOKField;
            end else
            if Segmento = 'S' then
            begin
            //AL(L, I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
              AL(L, I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '756            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
              AL(L, I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
              AL(L, I, x, '008', '008', '001', 'N', '0', 'I', '        ', '011', '3              ', 'REGISTRO DETALHE    ', 'REGISTRO DETALHE');
              AL(L, I, x, '009', '013', '005', 'N', '0', 'I', '        ', '997', '               ', 'N SEQ REGISTRO LOTE ', 'N�MERO SEQUENCIAL DO REGISTRO NO LOTE');
              AL(L, I, x, '014', '014', '001', 'N', ' ', 'X', '        ', '083', 'S              ', 'IGUAL A "S"         ', 'SEGMENTO DE REGISTRO: IGUAL A "S"');
              AL(L, I, x, '015', '015', '001', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
              AL(L, I, x, '016', '017', '002', 'N', '0', 'I', '        ', '019', '               ', 'C�DIGO DE MOVIMENTO ', 'C�DIGO DE MOVIMENTO'); // 01 = ENTRADA DE T�TULOS
              AL(L, I, x, '018', '018', '001', 'N', '0', 'I', '        ', '197', '               ', 'IDENTIF. DA IMPRESS.', 'IDENTIFICA��O DA IMPRESS�O');
              case SubSegmento of
                1,2:
                begin
              AL(L, I, x, '019', '020', '002', 'N', '0', 'I', '        ', '198', '               ', 'N� DA LINHA A IMPRIM', 'N�MERO DA LINHA A SER IMPRESSA');
              AL(L, I, x, '021', '160', '140', 'S', ' ', 'X', '        ', '199', '               ', 'MENSAGEM A IMPRIMIR ', 'MENSAGEM A SER IMPRESSA');
              AL(L, I, x, '161', '162', '002', 'N', '0', 'I', '        ', '200', '               ', 'TIPO DE CARACTER IMP', 'TIPO DE CARACTER A SER IMPRESSO');
              AL(L, I, x, '163', '240', '078', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
                end;
                3:
                begin
              AL(L, I, x, '019', '058', '040', 'S', ' ', 'X', '        ', '205', '               ', 'MENSAGEM 5          ', 'MENSAGEM 5');
              AL(L, I, x, '059', '098', '040', 'S', ' ', 'X', '        ', '206', '               ', 'MENSAGEM 6          ', 'MENSAGEM 6');
              AL(L, I, x, '099', '138', '040', 'S', ' ', 'X', '        ', '207', '               ', 'MENSAGEM 7          ', 'MENSAGEM 7');
              AL(L, I, x, '139', '178', '040', 'S', ' ', 'X', '        ', '208', '               ', 'MENSAGEM 8          ', 'MENSAGEM 8');
              AL(L, I, x, '179', '218', '040', 'S', ' ', 'X', '        ', '209', '               ', 'MENSAGEM 9          ', 'MENSAGEM 9');
              AL(L, I, x, '219', '240', '022', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
                end;
              end;
              //
              Result := cresOKField;
            end;
          end;
          5: // TRAILER DE LOTE - REGISTRO 5
          begin
          //AL(L, I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L, I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '756            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
            AL(L, I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '               ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
            AL(L, I, x, '008', '008', '001', 'N', '0', 'I', '        ', '012', '5              ', 'REGISTRO TRAILER LOT', 'REGISTRO TRAILER DE LOTE');
            AL(L, I, x, '009', '017', '009', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            AL(L, I, x, '018', '023', '006', 'N', '0', 'I', '        ', '992', '               ', 'QTDE REGISTROS LOTE ', 'QUANTIDADE DE REGISTROS DO LOTE');
            AL(L, I, x, '024', '029', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'QUANTIDADE DE T�TULOS EM COBRAN�A');
            AL(L, I, x, '030', '046', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'VALOR TOTAL DOS T�TULOS EM CARTEIRA');
            AL(L, I, x, '047', '052', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'QUANTIDADE DE T�TULOS EM COBRAN�A');
            AL(L, I, x, '053', '069', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'VALOR TOTAL DOS T�TULOS EM CARTEIRA');
            AL(L, I, x, '070', '075', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'QUANTIDADE DE T�TULOS EM COBRAN�A');
            AL(L, I, x, '076', '092', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'VALOR TOTAL DOS T�TULOS EM CARTEIRA');
            AL(L, I, x, '093', '098', '006', 'N', '0', 'I', '        ', '-01', '               ', 'QTDE TITULOS EM COBR', 'QUANTIDADE DE T�TULOS EM COBRAN�A');
            AL(L, I, x, '099', '115', '017', 'N', '0', 'F', '2       ', '-01', '               ', 'VAL TOT TIT CARTEIRA', 'VALOR TOTAL DOS T�TULOS EM CARTEIRA');
            AL(L, I, x, '116', '123', '008', 'S', ' ', 'X', '        ', '-02', '               ', 'NUM AVISO LANCTO    ', 'N�MERO DO AVISO DE LAN�AMENTO');
            AL(L, I, x, '124', '240', '117', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            //
            Result := cresOKField;
          end;
          9: // Registro Trailer (6 posi��es)
          begin
          //AL(L, I, x, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L, I, x, '001', '003', '003', 'N', '0', 'I', '        ', '001', '756            ', 'C�DIGO DO BANCO     ', 'C�DIGO DO BANCO NA COMPENSA��O');
            AL(L, I, x, '004', '007', '004', 'N', '0', 'I', '        ', '003', '9999           ', 'LOTE DE SERVI�O     ', 'LOTE DE SERVI�O - SEQUENCIAL DENTRO DO ARQUIVO - N�O PODE REPETIR DE 001 A 998');
            AL(L, I, x, '008', '008', '001', 'N', '0', 'I', '        ', '012', '9              ', 'REGISTRO TRAILER ARQ', 'REGISTRO TRAILER DE ARQUIVO');
            AL(L, I, x, '009', '017', '009', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            AL(L, I, x, '018', '023', '006', 'N', '0', 'I', '        ', '993', '               ', 'QTDE DE LOTES DO ARQ', 'QUANTIDADE DE LOTES DO ARQUIVO');
            AL(L, I, x, '024', '029', '006', 'N', '0', 'I', '        ', '994', '               ', 'QTDE DE REGIS.DO ARQ', 'QUANTIDADE DE REGISTROS DO ARQUIVO');
            AL(L, I, x, '030', '035', '006', 'N', ' ', 'X', '        ', '-02', '000000         ', 'QTD C/C P/CONC LOTES', 'QUANTIDADE DE CONTAS P/ CONC.-LOTES');
            AL(L, I, x, '036', '240', '205', 'S', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'USO EXCLUSIVO FEBRABAN');
            //
            Result := cresOKField;
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.LayoutCNAB400(const Banco, Registro: Integer; var L:
  MyArrayLista; const Posicoes: Integer; const NomeLayout: String): TCNABResult;

  procedure AL(var Res: MyArrayLista; var Linha: Integer; const Ini, Fim,
  Tam, AjustaTamanho, Preenchimento, Casas, Formatacao, MyCodCampo, ConteudoPadrao, NomeDoCampo,
  DescricaoDoCampo: String);
  var
    i, f, t: Integer;
  begin
    i := Geral.IMV(Ini);
    f := Geral.IMV(Fim);
    t := Geral.IMV(Tam);
    if t = 0 then
      t := f - i + 1;
    if t < 1 then
      Geral.MB_Erro('O tamanho do campo "' + NomeDoCampo +
      '" n�o confere na function "UBancos.LayoutCNAB400" !' + sLineBreak +
      'Banco: '+FormatFloat('000', Banco) + sLineBreak +
      'Registro: '+IntToStr(Registro) + sLineBreak +
      'Posi��o inicial do campo: ' + Ini + sLineBreak +
      'Posicao final do campo: ' + Fim);
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 11);
    Res[Linha][00] :=  Ini;
    Res[Linha][01] :=  Fim;
    Res[Linha][02] :=  FormatFloat('000', t);
    Res[Linha][03] :=  Preenchimento;
    Res[Linha][04] :=  Casas;
    Res[Linha][05] :=  Formatacao;
    Res[Linha][06] :=  MyCodCampo;
    Res[Linha][07] :=  ConteudoPadrao;
    Res[Linha][08] :=  NomeDoCampo;
    Res[Linha][09] :=  DescricaoDoCampo;
    Res[Linha][10] :=  AjustaTamanho;
    inc(Linha, 1);
  end;
var
  I: Integer;
begin
  {
  AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
   'C' = Corrige tamanho? S=sim, N=N�o, X= Sim mas exige um valor(alfanum�rico)
   'P' = Picture (Preenchimento) '0'= Num�rico (Formata � direta) ' '= Alfanum�rico (Formata � esquerda)
   'T' = Tipo de dados:
     'I'=Integer, 'F'=Float, 'X'=Texto, 'Z'=Zero,
     'B'=Branco, 'D'=Data, 'T'=Hora, 'C'=Calculado(j� formatado)
   'FLD' = Ver em : TUBancos.DescricaoDoMeuCodigoCNAB(...
  }
  // Format (usado para data ou quantidade de casas decimais de float)

  I := 0;
  SetLength(L, 0);
  Result := cresNoBank;
  case banco of
    001: // com registro
    begin
      Result := cresNoField;
      if Posicoes = 6 then
      begin
        case Registro of
          0: // Header de arquivo (6 posi��es)
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '010', '0              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO HEADER');
            AL(L,I, '002', '002', '001', 'N', '0', 'I', '        ', '005', '1              ', 'OPERA��O            ', 'TIPO DE OPERA��O - REMESSA');
            AL(L,I, '003', '009', '007', 'N', ' ', 'X', '        ', '006', 'REMESSA        ', 'LITERAL DE REMESSA  ', 'IDENTIFICA��O POR EXTENSO DO MOVIMENTO (REMESSA ou TESTE');
            AL(L,I, '010', '011', '002', 'N', '0', 'I', '        ', '003', '01             ', 'C�DIGO DO SERVI�O   ', 'IDENTIFICA��O DO TIPO DE SERVI�O');
            AL(L,I, '012', '019', '008', 'N', ' ', 'X', '        ', '004', 'COBRANCA       ', 'LITERAL DE SERVI�O  ', 'IDENTIFICA��O POR EXTENSO DO TIPO DE SERVI�O');
            //
            AL(L,I, '020', '026', '007', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '027', '030', '004', 'N', '0', 'I', '        ', '020', '               ', 'PREFIXO DA AG�NCIA  ', 'N�MERO DA AG�NCIA ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, '031', '031', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
            AL(L,I, '032', '039', '008', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, '040', '040', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
            //
            AL(L,I, '041', '046', '006', 'N', '0', 'I', '        ', '641', '               ', 'N� DO CONV�NIO L�DER', 'N�MERO DO CONV�NIO L�DER');
            //
            AL(L,I, '047', '076', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DA EMPRESA     ', 'NOME POR EXTENSO DA "EMPRESA M�E"');
            AL(L,I, '077', '079', '003', 'N', '0', 'I', '        ', '001', '001            ', 'C�DIGO DO BANCO     ', 'N� DO BANCO NA C�MARA DE COMPENSA��O');
            AL(L,I, '080', '094', '015', 'N', ' ', 'X', '        ', '002', 'BANCODOBRASIL  ', 'NOME DO BANCO       ', 'NOME POR EXTENSO DO BANCO COBRADOR');
            AL(L,I, '095', '100', '006', 'N', '0', 'D', 'DDMMAA  ', '990', '               ', 'DATA DE GERA��O     ', 'DATA DE GERA��O DO ARQUIVO');
            //
            AL(L,I, '101', '107', '007', 'N', '0', 'I', '        ', '998', '               ', 'SEQUENCIAL          ', 'SEQUENCIAL DA REMESSA');
            AL(L,I, '108', '394', '287', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '000001         ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          1: // Registro Detalhe (6 posicoes)
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '7              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO DETALHE');
            AL(L,I, '002', '003', '002', 'N', '0', 'I', '        ', '400', '               ', 'TIPO DE INSCRI��O   ', 'TIPO DE INSCRI��O DO CEDENTE: 01 = CPF, 02 = CNPJ');
            AL(L,I, '004', '017', '014', 'N', '0', 'I', '        ', '401', '               ', 'CNPJ/CPF CEDENTE    ', 'N�MERO DO CNPJ/CPF DO CEDENTE');
            AL(L,I, '018', '021', '004', 'N', '0', 'I', '        ', '020', '               ', 'PREFIXO DA AG�NCIA  ', 'N�MERO DA AG�NCIA ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, '022', '022', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
            AL(L,I, '023', '030', '008', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, '031', '031', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
            AL(L,I, '032', '037', '006', 'N', '0', 'I', '        ', '642', '               ', 'N�MERO CONV�NIO CED.', 'N�MERO DO CONV�NIO DE COBRAN�A DO CEDENTE');
            AL(L,I, '038', '062', '025', 'N', ' ', 'X', '        ', '502', '               ', 'N�M. CTRL PARTICIP. ', 'N�MERO DE CONTROLE DO PARTICIPANTE');
            AL(L,I, '063', '073', '011', 'N', '0', 'I', '        ', '-01', '               ', 'NOSSO N�MERO        ', 'NOSSO N�MERO');
{
            AL(L,I, '063', '068', '006', 'N', '0', 'I', '        ', '642', '               ', '1/2 NOSSO N�MERO    ', 'PRIMEIRA PARTE DO NOSSO N�MERO');
            AL(L,I, '069', '073', '005', 'N', '0', 'I', '        ', '501', '               ', '2/2 NOSSO N�MERO    ', 'SEGUNDA PARTE DO NOSSO N�MERO');
}
            AL(L,I, '074', '074', '001', 'N', '0', 'I', '        ', '648', '               ', 'DV DO NOSSO N�MERO  ', 'D�GITO VERIFICADOR DO NOSSO N�MERO');
            AL(L,I, '075', '076', '002', 'N', '0', 'I', '        ', '-01', '00             ', 'N�MERO DA PRESTA��O ', 'N�MERO DA PRESTA��O');
            AL(L,I, '077', '078', '002', 'N', '0', 'I', '        ', '-01', '00             ', 'GRUPO DE VALOR      ', 'GRUPO DE VALOR');
            AL(L,I, '079', '081', '003', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '082', '082', '001', 'N', ' ', 'X', '        ', '643', '               ', 'INDICATIVO          ', 'a) "BRANCOS": PODER� SER INFORMADA NAS POSI��ES 352 A 391 QUALQUER MENSAGEM PARA SER IMPRESSA NO BLOQUETO; B) "A": DEVER� SER INFORMADO NAS POSI��ES 352 A 391 O NOME E O CPF/CNPJ DO SACADOR, DE ACORDO COM O CONTIDO NA NOTA 13 (DO MANUAL T�CNICO)');
            AL(L,I, '083', '085', '003', 'N', ' ', 'X', '        ', '649', '               ', 'PREFIXO DO T�TULO   ', 'a) CARTEIRAS 31 E 51: "SD"; CARTEIRA 12: "AIU"; DEMAIS CARTEIRAS: "AI"');
            AL(L,I, '086', '088', '003', 'N', '0', 'I', '        ', '645', '               ', 'VARIA��O DA CARTEIRA', 'VARIA��O DA CARTEIRA (INFORMADO PELO BANCO)');
            AL(L,I, '089', '089', '001', 'N', '0', 'I', '        ', '-01', '0              ', 'CONTA CAU��O        ', 'CONTA CAU��O: "0"');
            AL(L,I, '090', '094', '005', 'N', '0', 'I', '        ', '-01', '00000          ', 'C�DIGO DE RESPONSAB.', 'C�DIGO DE RESPONSABILIDADE (INFORMAR ZEROS)');
            AL(L,I, '095', '095', '001', 'N', '0', 'I', '        ', '-01', '0              ', 'DV C�D. DE RESPONS. ', 'D�GITO VERIFICADOR DO C�DIGO DE RESPONSABILIDADE (0))');
            AL(L,I, '096', '101', '006', 'N', '0', 'I', '        ', '-01', '0              ', 'N�MERO DO BORDER�   ', 'N�MERO DO BORDER� - (CARTEIRAS 31 E 51)');
            AL(L,I, '102', '106', '005', 'N', ' ', 'X', '        ', '644', '               ', 'TIPO DE COBRAN�A    ', 'TIPO DE COBRAN�A');
            AL(L,I, '107', '108', '002', 'N', '0', 'I', '        ', '509', '               ', 'CARTEIRA DE COBRAN�A', 'CARTEIRA DE COBRAN�A');
            AL(L,I, '109', '110', '002', 'N', '0', 'I', '        ', '646', '               ', 'COMANDO             ', 'COMANDO (INSTRU��O AO BANCO)');
            AL(L,I, '111', '120', '010', 'N', ' ', 'X', '        ', '506', '               ', 'SEU N�MERO          ', 'SEU N�MERO / N�MERO ATRIBU�DO PELO CEDENTE');
            AL(L,I, '121', '126', '006', 'N', '0', 'D', 'DDMMYY  ', '580', '               ', 'DATA DO VENCIMENTO  ', 'DATA DO VENCIMENTO DDMMAA OU 888888 = "� VISTA" OU 999999 = "NA APRESENTA��O"');
            AL(L,I, '127', '139', '013', 'N', '0', 'F', '2       ', '550', '               ', 'VALOR DO T�TULO     ', 'VALOR NOMINAL DO T�TULO');
            AL(L,I, '140', '142', '003', 'N', '0', 'I', '        ', '027', '001            ', 'BANCO COBRADOR      ', 'N� DO BANCO COBRADOR NA C�MARA DE COMPENSA��O');
            AL(L,I, '143', '146', '004', 'N', '0', 'I', '        ', '025', '0000           ', 'AG�NCIA COBRADORA   ', 'PREFIXO AG�NCIA COBRADORA');
            AL(L,I, '146', '147', '001', 'N', '0', 'I', '        ', '025', '0              ', 'DV AG�NCIA COBRADORA', 'D�GITO VERIFICADOR DA AG�NCIA COBRADORA');
            AL(L,I, '148', '149', '002', 'N', '0', 'I', '        ', '507', '               ', 'ESP�CIE             ', 'ESP�CIE DO T�TULO');
            AL(L,I, '150', '150', '001', 'N', ' ', 'X', '        ', '520', '               ', 'ACEITE              ', 'IDENTIFICA��O DE T�TULO ACEITO OU N�O ACEITO');
            AL(L,I, '151', '156', '006', 'N', '0', 'D', 'DDMMYY  ', '583', '               ', 'DATA DE EMISS�O     ', 'DATA DA EMISS�O DO T�TULO');
            AL(L,I, '157', '158', '002', 'N', '0', 'I', '        ', '701', '               ', 'INSTRU��O 1         ', '1� INSTRU��O DE COBRAN�A');
            AL(L,I, '159', '160', '002', 'N', '0', 'I', '        ', '702', '               ', 'INSTRU��O 2         ', '2� INSTRU��O DE COBRAN�A');
            AL(L,I, '161', '173', '013', 'N', '0', 'F', '2       ', '572', '               ', 'JUROS DE 1 DIA      ', 'VALOR DE MORA POR DIA DE ATRASO');
            AL(L,I, '174', '179', '006', 'N', '0', 'D', 'DDMMYY  ', '586', '               ', 'DESCONTO AT�        ', 'DATA LIMITE PARA CONCESS�O DE DESCONTO');
            AL(L,I, '180', '192', '013', 'N', '0', 'F', '2       ', '552', '               ', 'VALOR DO DESCONTO   ', 'VALOR DO DESCONTO A SER CONCEDIDO');
            AL(L,I, '193', '205', '013', 'N', '0', 'F', '2       ', '569', '               ', 'VALOR DO I.O.F.     ', 'VALOR DO I.O.F. RECOLHIDO P/ NOTAS SEGURO');
            AL(L,I, '206', '218', '013', 'N', '0', 'F', '2       ', '551', '               ', 'ABATIMENTO          ', 'VALOR DO ABATIMENTO A SER CONCEDIDO');
            AL(L,I, '219', '220', '002', 'N', '0', 'I', '        ', '801', '               ', 'C�DIGO DE INSCRI��O ', 'IDENTIFICA��O DO TIPO DE INSCRI��O/SACADO');
            AL(L,I, '221', '234', '014', 'N', '0', 'I', '        ', '802', '               ', 'N�MERO DE INSCRI��O ', 'N� DE INSCRI��O DO SACADO  (CPF/CNPJ)');
            AL(L,I, '235', '271', '037', 'S', ' ', 'X', '        ', '803', '               ', 'NOME DO SACADO      ', 'NOME DO SACADO');
            AL(L,I, '272', '274', '003', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '275', '311', '037', 'S', ' ', 'X', '        ', '804', '               ', 'ENDERE�O DO SACADO  ', 'ENDERE�O DO SACADO (LOGRADOURO, N�MERO E COMPLEMENTO)');
            AL(L,I, '312', '326', '015', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '327', '334', '008', 'N', '0', 'I', '        ', '806', '               ', 'CEP                 ', 'CEP DO SACADO');
            AL(L,I, '335', '349', '015', 'S', ' ', 'X', '        ', '807', '               ', 'CIDADE DO SACADO    ', 'CIDADE DO SACADO');
            AL(L,I, '350', '351', '002', 'N', ' ', 'X', '        ', '808', '               ', 'UF DO SACADO        ', 'UF DO SACADO');
            AL(L,I, '352', '359', '008', 'S', ' ', 'X', '        ', '803', '               ', 'NOME DO SACADOR     ', 'NOME DO SACADOR / AVALISTA');
            AL(L,I, '360', '360', '001', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '361', '365', '005', 'S', ' ', 'X', '        ', '801', 'CNPJ           ', 'LITERAL "CNPJ"      ', 'LITERAL "CNPJ"');
            AL(L,I, '366', '391', '026', 'N', '0', 'I', '        ', '802', '               ', 'CPF / CNPJ SACADOR  ', 'CPF / CNPJ SACADOR / AVALISTA');
            AL(L,I, '392', '393', '002', 'N', '0', 'I', '        ', '647', '               ', 'N� DIAS P/ PROTESTO ', 'N�MERO DE DIAS PARA PROTESTO');
            AL(L,I, '394', '394', '001', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N� SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          9: // Registro Trailer (6 posi��es)
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '012', '9              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO TRAILER');
            AL(L,I, '002', '394', '393', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
        end;
      end;
      if Posicoes = 7 then
      begin
        case Registro of
          0: // Header de arquivo (7 posi��es)
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '010', '0              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO HEADER');
            AL(L,I, '002', '002', '001', 'N', '0', 'I', '        ', '005', '1              ', 'OPERA��O            ', 'TIPO DE OPERA��O - REMESSA');
            AL(L,I, '003', '009', '007', 'N', ' ', 'X', '        ', '006', 'REMESSA        ', 'LITERAL DE REMESSA  ', 'IDENTIFICA��O POR EXTENSO DO MOVIMENTO (REMESSA ou TESTE');
            AL(L,I, '010', '011', '002', 'N', '0', 'I', '        ', '003', '01             ', 'C�DIGO DO SERVI�O   ', 'IDENTIFICA��O DO TIPO DE SERVI�O');
            AL(L,I, '012', '019', '008', 'N', ' ', 'X', '        ', '004', 'COBRANCA       ', 'LITERAL DE SERVI�O  ', 'IDENTIFICA��O POR EXTENSO DO TIPO DE SERVI�O');
            //
            AL(L,I, '020', '026', '007', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '027', '030', '004', 'N', '0', 'I', '        ', '020', '               ', 'PREFIXO DA AG�NCIA  ', 'N�MERO DA AG�NCIA ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, '031', '031', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
            AL(L,I, '032', '039', '008', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, '040', '040', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
            //
            AL(L,I, '041', '046', '006', 'N', '0', 'I', '        ', '-01', '000000         ', 'COMPLEM. DE REGISTRO', 'PREENCHER COM "000000"');
            //
            AL(L,I, '047', '076', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DA EMPRESA     ', 'NOME POR EXTENSO DA "EMPRESA M�E"');
            AL(L,I, '077', '079', '003', 'N', '0', 'I', '        ', '001', '001            ', 'C�DIGO DO BANCO     ', 'N� DO BANCO NA C�MARA DE COMPENSA��O');
            AL(L,I, '080', '094', '015', 'N', ' ', 'X', '        ', '002', 'BANCODOBRASIL  ', 'NOME DO BANCO       ', 'NOME POR EXTENSO DO BANCO COBRADOR');
            AL(L,I, '095', '100', '006', 'N', '0', 'D', 'DDMMAA  ', '990', '               ', 'DATA DE GERA��O     ', 'DATA DE GERA��O DO ARQUIVO');
            //
            AL(L,I, '101', '107', '007', 'N', '0', 'I', '        ', '998', '               ', 'SEQUENCIAL          ', 'SEQUENCIAL DA REMESSA');
            AL(L,I, '108', '129', '022', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '130', '136', '007', 'N', '0', 'I', '        ', '641', '               ', 'N� CONV�NIO  L�DER  ', 'N�MERO DO CONV�NIO L�DER');
            AL(L,I, '137', '394', '258', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '000001         ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          7: // Registro Detalhe (7 posi��es)
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '7              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO DETALHE');
            AL(L,I, '002', '003', '002', 'N', '0', 'I', '        ', '400', '               ', 'TIPO DE INSCRI��O   ', 'TIPO DE INSCRI��O DO CEDENTE: 01 = CPF, 02 = CNPJ');
            AL(L,I, '004', '017', '014', 'N', '0', 'I', '        ', '401', '               ', 'CNPJ/CPF CEDENTE    ', 'N�MERO DO CNPJ/CPF DO CEDENTE');
            AL(L,I, '018', '021', '004', 'N', '0', 'I', '        ', '020', '               ', 'PREFIXO DA AG�NCIA  ', 'N�MERO DA AG�NCIA ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, '022', '022', '001', 'N', ' ', 'X', '        ', '022', '               ', 'DV DA AG�NCIA       ', 'D�GITO VERIFICADOR DO N�MERO DA AG�NCIA');
            AL(L,I, '023', '030', '008', 'N', '0', 'I', '        ', '021', '               ', 'N�M. CONTA CORRENTE ', 'N�MERO DA CONTA CORRENTE ONDE EST� CADASTRADO O CONV�NIO L�DER DO CEDENTE');
            AL(L,I, '031', '031', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DV DA CONTA CORRENTE', 'D�GITO VERIFICADOR DA CONTA CORRENTE');
            AL(L,I, '032', '038', '007', 'N', '0', 'I', '        ', '642', '               ', 'N�MERO CONV�NIO CED.', 'N�MERO DO CONV�NIO DE COBRAN�A DO CEDENTE');
            AL(L,I, '039', '063', '025', 'N', ' ', 'X', '        ', '502', '               ', 'N�M. CTRL PARTICIP. ', 'N�MERO DE CONTROLE DO PARTICIPANTE');
            AL(L,I, '064', '080', '017', 'N', '0', 'I', '        ', '501', '               ', 'NOSSO N�MERO        ', 'NOSSO N�MERO');
            AL(L,I, '081', '082', '002', 'N', '0', 'I', '        ', '-01', '00             ', 'N�MERO DA PRESTA��O ', 'N�MERO DA PRESTA��O');
            AL(L,I, '083', '084', '002', 'N', '0', 'I', '        ', '-01', '00             ', 'GRUPO DE VALOR      ', 'GRUPO DE VALOR');
            AL(L,I, '085', '087', '003', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '088', '088', '001', 'N', ' ', 'X', '        ', '643', '               ', 'INDICATIVO          ', 'a) "BRANCOS": PODER� SER INFORMADA NAS POSI��ES 352 A 391 QUALQUER MENSAGEM PARA SER IMPRESSA NO BLOQUETO; B) "A": DEVER� SER INFORMADO NAS POSI��ES 352 A 391 O NOME E O CPF/CNPJ DO SACADOR, DE ACORDO COM O CONTIDO NA NOTA 13 (DO MANUAL T�CNICO)');
            AL(L,I, '089', '091', '003', 'N', ' ', 'B', '        ', '-02', '               ', 'PREFIXO DO T�TULO   ', 'PREFIXO DO T�TULO: "BRANCOS"');
            AL(L,I, '092', '094', '003', 'N', '0', 'I', '        ', '645', '               ', 'VARIA��O DA CARTEIRA', 'VARIA��O DA CARTEIRA (INFORMADO PELO BANCO)');
            AL(L,I, '095', '095', '001', 'N', '0', 'I', '        ', '-01', '0              ', 'CONTA CAU��O        ', 'CONTA CAU��O: "0"');
            AL(L,I, '096', '101', '006', 'N', '0', 'I', '        ', '-01', '000000         ', 'N�MERO DO BORDER�   ', 'N�MERO DO BORDER�: "000000"');
            AL(L,I, '102', '106', '005', 'N', ' ', 'X', '        ', '644', '               ', 'TIPO DE COBRAN�A    ', 'TIPO DE COBRAN�A');
            AL(L,I, '107', '108', '002', 'N', '0', 'I', '        ', '509', '               ', 'CARTEIRA DE COBRAN�A', 'CARTEIRA DE COBRAN�A');
            AL(L,I, '109', '110', '002', 'N', '0', 'I', '        ', '646', '               ', 'COMANDO             ', 'COMANDO (INSTRU��O AO BANCO)');
            AL(L,I, '111', '120', '010', 'N', ' ', 'X', '        ', '506', '               ', 'SEU N�MERO          ', 'SEU N�MERO / N�MERO ATRIBU�DO PELO CEDENTE');
            AL(L,I, '121', '126', '006', 'N', '0', 'D', 'DDMMYY  ', '580', '               ', 'DATA DO VENCIMENTO  ', 'DATA DO VENCIMENTO DDMMAA OU 888888 = "� VISTA" OU 999999 = "NA APRESENTA��O"');
            AL(L,I, '127', '139', '013', 'N', '0', 'F', '2       ', '550', '               ', 'VALOR DO T�TULO     ', 'VALOR NOMINAL DO T�TULO');
            AL(L,I, '140', '142', '003', 'N', '0', 'I', '        ', '027', '001            ', 'BANCO COBRADOR      ', 'N� DO BANCO COBRADOR NA C�MARA DE COMPENSA��O');
            AL(L,I, '143', '146', '004', 'N', '0', 'I', '        ', '025', '0000           ', 'AG�NCIA COBRADORA   ', 'PREFIXO AG�NCIA COBRADORA');
            AL(L,I, '146', '147', '001', 'N', '0', 'I', '        ', '025', '0              ', 'DV AG�NCIA COBRADORA', 'D�GITO VERIFICADOR DA AG�NCIA COBRADORA');
            AL(L,I, '148', '149', '002', 'N', '0', 'I', '        ', '507', '               ', 'ESP�CIE             ', 'ESP�CIE DO T�TULO');
            AL(L,I, '150', '150', '001', 'N', ' ', 'X', '        ', '520', '               ', 'ACEITE              ', 'IDENTIFICA��O DE T�TULO ACEITO OU N�O ACEITO');
            AL(L,I, '151', '156', '006', 'N', '0', 'D', 'DDMMYY  ', '583', '               ', 'DATA DE EMISS�O     ', 'DATA DA EMISS�O DO T�TULO');
            AL(L,I, '157', '158', '002', 'N', '0', 'I', '        ', '701', '               ', 'INSTRU��O 1         ', '1� INSTRU��O DE COBRAN�A');
            AL(L,I, '159', '160', '002', 'N', '0', 'I', '        ', '702', '               ', 'INSTRU��O 2         ', '2� INSTRU��O DE COBRAN�A');
            AL(L,I, '161', '173', '013', 'N', '0', 'F', '2       ', '572', '               ', 'JUROS DE 1 DIA      ', 'VALOR DE MORA POR DIA DE ATRASO');
            AL(L,I, '174', '179', '006', 'N', '0', 'D', 'DDMMYY  ', '586', '               ', 'DESCONTO AT�        ', 'DATA LIMITE PARA CONCESS�O DE DESCONTO');
            AL(L,I, '180', '192', '013', 'N', '0', 'F', '2       ', '552', '               ', 'VALOR DO DESCONTO   ', 'VALOR DO DESCONTO A SER CONCEDIDO');
            AL(L,I, '193', '205', '013', 'N', '0', 'F', '2       ', '569', '               ', 'VALOR DO I.O.F.     ', 'VALOR DO I.O.F. RECOLHIDO P/ NOTAS SEGURO');
            AL(L,I, '206', '218', '013', 'N', '0', 'F', '2       ', '551', '               ', 'ABATIMENTO          ', 'VALOR DO ABATIMENTO A SER CONCEDIDO');
            AL(L,I, '219', '220', '002', 'N', '0', 'I', '        ', '801', '               ', 'C�DIGO DE INSCRI��O ', 'IDENTIFICA��O DO TIPO DE INSCRI��O/SACADO');
            AL(L,I, '221', '234', '014', 'N', '0', 'I', '        ', '802', '               ', 'N�MERO DE INSCRI��O ', 'N� DE INSCRI��O DO SACADO  (CPF/CNPJ)');
            AL(L,I, '235', '271', '037', 'S', ' ', 'X', '        ', '803', '               ', 'NOME DO SACADO      ', 'NOME DO SACADO');
            AL(L,I, '272', '274', '003', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '275', '311', '037', 'S', ' ', 'X', '        ', '804', '               ', 'ENDERE�O DO SACADO  ', 'ENDERE�O DO SACADO (LOGRADOURO, N�MERO E COMPLEMENTO)');
            AL(L,I, '312', '326', '015', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '327', '334', '008', 'N', '0', 'I', '        ', '806', '               ', 'CEP                 ', 'CEP DO SACADO');
            AL(L,I, '335', '349', '015', 'S', ' ', 'X', '        ', '807', '               ', 'CIDADE DO SACADO    ', 'CIDADE DO SACADO');
            AL(L,I, '350', '351', '002', 'N', ' ', 'X', '        ', '808', '               ', 'UF DO SACADO        ', 'UF DO SACADO');
            AL(L,I, '352', '359', '008', 'S', ' ', 'X', '        ', '803', '               ', 'NOME DO SACADOR     ', 'NOME DO SACADOR / AVALISTA');
            AL(L,I, '360', '360', '001', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '361', '365', '005', 'S', ' ', 'X', '        ', '801', 'CNPJ           ', 'LITERAL "CNPJ"      ', 'LITERAL "CNPJ"');
            AL(L,I, '366', '391', '026', 'N', '0', 'I', '        ', '802', '               ', 'CPF / CNPJ SACADOR  ', 'CPF / CNPJ SACADOR / AVALISTA');
            AL(L,I, '392', '393', '002', 'N', '0', 'I', '        ', '647', '               ', 'N� DIAS P/ PROTESTO ', 'N�MERO DE DIAS PARA PROTESTO');
            AL(L,I, '394', '394', '001', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N� SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          9: // Registro Trailer (7 posi��es)
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '012', '9              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO TRAILER');
            AL(L,I, '002', '394', '393', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
        end;
      end;
    end;
    008, 033, 353:  // com registro
    begin
      Result := cresNoLayoutName;
      if NomeLayout = CO_033_RECEBIMENTOS_400_v02_00_2009_10 then
      begin
        Result := cresNoField;
        case Registro of
          0: // Header de arquivo
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '010', '0              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO HEADER');
            AL(L,I, '002', '002', '001', 'N', '0', 'I', '        ', '005', '1              ', 'OPERA��O            ', 'TIPO DE OPERA��O - REMESSA');
            AL(L,I, '003', '009', '007', 'N', ' ', 'X', '        ', '006', 'REMESSA        ', 'LITERAL DE REMESSA  ', 'IDENTIFICA��O POR EXTENSO DO MOVIMENTO');
            AL(L,I, '010', '011', '002', 'N', '0', 'I', '        ', '003', '01             ', 'C�DIGO DO SERVI�O   ', 'IDENTIFICA��O DO TIPO DE SERVI�O');
            AL(L,I, '012', '026', '015', 'N', ' ', 'X', '        ', '004', 'COBRANCA       ', 'LITERAL DE SERVI�O  ', 'IDENTIFICA��O POR EXTENSO DO TIPO DE SERVI�O');
            AL(L,I, '027', '046', '020', 'N', '0', 'I', '        ', '037', '               ', 'C�DIGO TRANSMISSAO  ', 'C�DIGO FORNECIDO PELO BANCO QUE IDENTIFICA O ARQUIVO REMESSA DO CLIENTE');
            AL(L,I, '047', '076', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DO CEDENTE     ', 'RAZ�O SOCIAL / NOME DO CLIENTE POR EXTENSO');
            AL(L,I, '077', '079', '003', 'N', '0', 'I', '        ', '001', '033            ', 'C�DIGO DO BANCO     ', 'N� DO BANCO NA C�MARA DE COMPENSA��O');
            AL(L,I, '080', '094', '015', 'N', ' ', 'X', '        ', '002', 'SANTANDER      ', 'NOME DO BANCO       ', 'NOME POR EXTENSO DO BANCO COBRADOR');
            AL(L,I, '095', '100', '006', 'N', '0', 'D', 'DDMMAA  ', '990', '               ', 'DATA DE GERA��O     ', 'DATA DE GERA��O DO ARQUIVO');
            AL(L,I, '101', '116', '016', 'N', '0', 'Z', '        ', '-01', '               ', 'ZEROS               ', 'ZEROS');
            AL(L,I, '117', '391', '275', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            AL(L,I, '392', '394', '003', 'N', '0', 'I', '        ', '891', '000            ', 'VERS�O DE REMESSA   ', 'N�MERO DA VERS�O DE REMESSA OPCIONAL , SE INFORMADA, SER� CONTROLADA PELO SISTEMA (OPCIONAL = 000)');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '000001         ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          1: // Detalhe
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '1              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO TRANSA��O');
            AL(L,I, '002', '003', '002', 'N', '0', 'I', '        ', '400', '               ', 'C�DIGO DE INSCRI��O ', 'TIPO DE INSCRI��O DA EMPRESA');
            AL(L,I, '004', '017', '014', 'N', '0', 'I', '        ', '401', '               ', 'N�MERO DE INSCRI��O ', 'N� DE INSCRI��O DA EMPRESA (CPF/CNPJ)');
            AL(L,I, '018', '037', '020', 'N', '0', 'I', '        ', '037', '               ', 'C�DIGO TRANSMISSAO  ', 'C�DIGO DE TRANSMISS�O CEDIDO PELO BANCO');
            AL(L,I, '038', '062', '025', 'N', ' ', 'X', '        ', '506', '               ', 'CONTROLE DO PARTICIP', 'IDENTIFICA��O DO T�TULO NO SISTEMA DO CLIENTE');
            AL(L,I, '063', '070', '008', 'N', '0', 'I', '        ', '501', '               ', 'NOSSO N�MERO        ', 'IDENTIFICA��O DO T�TULO NO BANCO');
            AL(L,I, '071', '076', '006', 'N', '0', 'D', 'DDMMAA  ', '595', '               ', 'DATA DO DESCONTO 2  ', 'DATA LIMITE PARA O DESCONTO 2');
            AL(L,I, '077', '077', '001', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCO              ', 'BRANCO');
            AL(L,I, '078', '078', '001', 'N', '0', 'I', '        ', '577', '               ', 'INFORMA��O DA MULTA ', '"4" COBRAR MULTA, "0" N�O COBRAR');
            AL(L,I, '079', '082', '004', 'N', '0', 'F', '2       ', '575', '               ', '% MULTA POR ATRASO  ', 'PERCENTUAL MULTA POR ATRASO');
            AL(L,I, '083', '084', '002', 'N', ' ', 'X', '        ', '549', '00             ', 'TIPO DE MOEDA       ', 'C�DIGO DA MOEDA');
            AL(L,I, '085', '097', '013', 'N', '0', 'F', '2       ', '667', '               ', 'VAL T�T OUTR UNIDADE', 'VALOR DO T�TULO EM OUTRA UNIDADE (CONSULTAR BANCO)');
            AL(L,I, '098', '101', '004', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            AL(L,I, '102', '107', '006', 'N', '0', 'D', 'DDMMAA  ', '587', '               ', 'DATA COBRA MULTA    ', 'DATA DA COBRAN�A DA MULTA');
            // o FLD para a carteira de cobran�a deveria ser 509, mas j� � usado para o bloqueto (101 - Com registro e 102 sem registro!)
            AL(L,I, '108', '108', '001', 'N', '0', 'I', '        ', '508', '               ', 'CARTEIRA DE COBRANCA', 'IDENTIFICA O TIPO DA CARTEIRA DE COBRANCA');
            AL(L,I, '109', '110', '002', 'N', '0', 'I', '        ', '504', '01             ', 'C�D. DA OCORR�NCIA  ', 'IDENTIFICA��O DA OCORR�NCIA');
            AL(L,I, '111', '120', '010', 'N', ' ', 'X', '        ', '502', '               ', 'SEU N�MERO          ', 'N� DO DOCUMENTO DE COBRAN�A (DUPL.,NP ETC.)');
            AL(L,I, '121', '126', '006', 'N', '9', 'D', 'DDMMAA  ', '580', '               ', 'VENCIMENTO          ', 'DATA DE VENCIMENTO DO T�TULO');
            AL(L,I, '127', '139', '013', 'N', '0', 'F', '2       ', '550', '               ', 'VALOR DO T�TULO     ', 'VALOR NOMINAL DO T�TULO');
            AL(L,I, '140', '142', '003', 'N', '0', 'I', '        ', '027', '033            ', 'C�DIGO DO BANCO     ', 'N� DO BANCO NA C�MARA DE COMPENSA��O');
            AL(L,I, '143', '147', '005', 'N', '0', 'I', '        ', '025', '000            ', 'AG�NCIA COBRADORA   ', 'AG�NCIA ONDE O T�TULO SER� COBRADO');
            AL(L,I, '148', '149', '002', 'N', ' ', 'X', '        ', '507', '               ', 'ESP�CIE             ', 'ESP�CIE DO T�TULO');
            AL(L,I, '150', '150', '001', 'N', ' ', 'X', '        ', '520', '               ', 'ACEITE              ', 'IDENTIFICA��O DE T�TULO ACEITO OU N�O ACEITO');
            AL(L,I, '151', '156', '006', 'N', '0', 'D', 'DDMMAA  ', '583', '               ', 'DATA DE EMISS�O     ', 'DATA DA EMISS�O DO T�TULO');
            AL(L,I, '157', '158', '002', 'N', ' ', 'X', '        ', '701', '               ', 'INSTRU��O 1         ', '1� INSTRU��O DE COBRAN�A');
            AL(L,I, '159', '160', '002', 'N', ' ', 'X', '        ', '702', '               ', 'INSTRU��O 2         ', '2� INSTRU��O DE COBRAN�A');
            AL(L,I, '161', '173', '013', 'N', '0', 'F', '2       ', '572', '               ', 'JUROS DE 1 DIA      ', 'VALOR DE MORA POR DIA DE ATRASO');
            AL(L,I, '174', '179', '006', 'N', '0', 'D', 'DDMMAA  ', '592', '               ', 'DESCONTO AT�        ', 'DATA LIMITE PARA CONCESS�O DE DESCONTO');
            AL(L,I, '180', '192', '013', 'N', '0', 'F', '2       ', '593', '               ', 'VALOR DO DESCONTO   ', 'VALOR DO DESCONTO A SER CONCEDIDO');
            AL(L,I, '193', '205', '013', 'N', '0', 'F', '2       ', '569', '               ', 'VALOR DO I.O.F.     ', 'VALOR DO I.O.F. A SER RECOLHIDO PELO BANCO');
            AL(L,I, '206', '218', '013', 'N', '0', 'C', '2       ', '551', '               ', 'ABATIMENTO          ', 'VALOR DO ABATIMENTO A SER CONCEDIDO');
            AL(L,I, '219', '220', '002', 'N', '0', 'I', '        ', '801', '               ', 'C�DIGO DE INSCRI��O ', 'IDENTIFICA��O DO TIPO DE INSCRI��O/SACADO');
            AL(L,I, '221', '234', '014', 'N', '0', 'I', '        ', '802', '               ', 'N�MERO DE INSCRI��O ', 'N� DE INSCRI��O DO SACADO  (CPF/CNPJ)');
            AL(L,I, '235', '274', '040', 'X', ' ', 'X', '        ', '803', '               ', 'NOME                ', 'NOME DO SACADO');
            AL(L,I, '275', '314', '040', 'X', ' ', 'X', '        ', '804', '               ', 'LOGRADOURO          ', 'RUA, N�MERO E COMPLEMENTO DO SACADO');
            AL(L,I, '315', '326', '012', 'X', ' ', 'X', '        ', '805', '               ', 'BAIRRO              ', 'BAIRRO DO SACADO');
            AL(L,I, '327', '334', '008', '9', '0', 'I', '        ', '806', '               ', 'CEP                 ', 'CEP DO SACADO');
            AL(L,I, '335', '349', '015', 'X', ' ', 'X', '        ', '807', '               ', 'CIDADE              ', 'CIDADE DO SACADO');
            AL(L,I, '350', '351', '002', 'X', ' ', 'X', '        ', '808', '               ', 'ESTADO              ', 'UF DO SACADO');
            AL(L,I, '352', '381', '030', 'S', ' ', 'X', '        ', '853', '               ', 'SACADOR/AVALISTA    ', 'NOME DO SACADOR OU AVALISTA');
            AL(L,I, '382', '382', '001', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCO              ', 'BRANCO');
            AL(L,I, '383', '385', '003', 'N', ' ', 'X', '        ', '038', '               ', 'COMPLEMENTO COD ESP ', 'COMPLEMENTO DO C�DIGO DE TRANSMISS�O = LETRA "I" + DOIS N�MEROS');
            AL(L,I, '386', '391', '006', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            AL(L,I, '392', '393', '002', 'N', '0', 'I', '        ', '647', '               ', 'PRAZO DE PROTESTO   ', 'DIAS PARA PROTESTO');
            AL(L,I, '394', '394', '001', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCO              ', 'BRANCO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N� SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          2: // MENSAGEM VARI�VEL POR T�TULO (OPCIONAL)
          begin
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '010', '2              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO DETALHE - MENSAGEM VARI�VEL POR T�TULO (OPCIONAL)');
            AL(L,I, '002', '017', '016', 'N', ' ', 'B', '        ', '-02', '               ', 'USO DO BANCO        ', 'USO DO BANCO (BRANCOS)');
            AL(L,I, '018', '037', '020', 'N', '0', 'I', '        ', '037', '               ', 'C�DIGO TRANSMISSAO  ', 'C�DIGO DE TRANSMISS�O CEDIDO PELO BANCO');
            AL(L,I, '038', '047', '010', 'N', ' ', 'B', '        ', '-02', '               ', 'USO DO BANCO        ', 'USO DO BANCO (BRANCOS)');
            AL(L,I, '048', '049', '002', 'N', '0', 'I', '        ', '666', '01             ', 'SUBSEQUE DO REGISTRO', 'SUBSEQUENCIAS DO REGISTRO = 01');
            AL(L,I, '050', '099', '050', 'S', ' ', 'X', '        ', '201', '               ', 'MENSAGEM VARIAVEL TIT', 'MENSAGEM VARIAVEL POR TITULO');
            AL(L,I, '100', '382', '283', 'N', ' ', 'B', '        ', '-02', '               ', 'USO DO BANCO        ', 'USO DO BANCO (BRANCOS)');
            AL(L,I, '383', '385', '003', 'N', ' ', 'X', '        ', '038', '               ', 'COMPLEMENTO COD ESP ', 'COMPLEMENTO DO C�DIGO DE TRANSMISS�O = LETRA "I" + DOIS N�MEROS');
            AL(L,I, '386', '394', '009', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '000001         ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
          end;
          9: // Registro Trailer
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '012', '9              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO TRAILER');
            AL(L,I, '002', '007', '006', 'N', '0', 'I', '        ', '994', '               ', 'QTD REGISTROS ARQUIV', 'QUANTIDADE DE REGISTROS NO ARQUIVO');
            AL(L,I, '008', '020', '013', 'N', '0', 'F', '2       ', '302', '               ', 'VALOR TOTAL TITULOS ', 'VALOR TOTAL DOS TITULOS');
            AL(L,I, '021', '394', '374', 'N', '0', 'Z', '        ', '-01', '               ', 'ZEROS               ', 'ZEROS');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
        end;
      end;
    end;
    237: // com registro
    begin
      Result := cresNoField;
      {if NomeLayout = CO_237_MP_4008_0121_01_Data_11_11_2010 then}
      begin
        case Registro of
          0: // Header de arquivo
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '010', '0              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO HEADER');
            AL(L,I, '002', '002', '001', 'N', '0', 'I', '        ', '005', '1              ', 'OPERA��O            ', 'TIPO DE OPERA��O - REMESSA');
            AL(L,I, '003', '009', '007', 'N', ' ', 'X', '        ', '006', 'REMESSA        ', 'LITERAL DE REMESSA  ', 'IDENTIFICA��O POR EXTENSO DO MOVIMENTO');
            AL(L,I, '010', '011', '002', 'N', '0', 'I', '        ', '003', '01             ', 'C�DIGO DO SERVI�O   ', 'IDENTIFICA��O DO TIPO DE SERVI�O');
            AL(L,I, '012', '026', '015', 'N', ' ', 'X', '        ', '004', 'COBRANCA       ', 'LITERAL DE SERVI�O  ', 'IDENTIFICA��O POR EXTENSO DO TIPO DE SERVI�O');
            AL(L,I, '027', '046', '020', 'N', '0', 'I', '        ', '410', '               ', 'C�DIGO DA EMPRESA   ', 'SER� FORNECIDO PELO BRADESCO QUANDO DO CADASTRAMENTO. VIDE OBS. P�G. 19 (MESMO QUE O C�DIGO DO CEDENTE PARA OUTROS BANCOS?)');
            AL(L,I, '047', '076', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DA EMPRESA     ', 'NOME POR EXTENSO DA "EMPRESA M�E"');
            AL(L,I, '077', '079', '003', 'N', '0', 'I', '        ', '001', '237            ', 'C�DIGO DO BANCO     ', 'N� DO BANCO NA C�MARA DE COMPENSA��O');
            AL(L,I, '080', '094', '015', 'N', ' ', 'X', '        ', '002', 'BRADESCO       ', 'NOME DO BANCO       ', 'NOME POR EXTENSO DO BANCO COBRADOR');
            AL(L,I, '095', '100', '006', 'N', '0', 'D', 'DDMMAA  ', '990', '               ', 'DATA DE GERA��O     ', 'DATA DE GERA��O DO ARQUIVO');
            AL(L,I, '101', '108', '008', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '109', '110', '002', 'N', ' ', 'X', '        ', '099', 'MX             ', 'IDENTIFIC. SISTEMA  ', 'IDENTIFICA��O DO SISTEMA');
            AL(L,I, '111', '117', '007', 'N', '0', 'I', '        ', '998', '               ', 'SEQ�ENCIAL ARQUIVO  ', 'N�MERO SEQ�ENCIAL DO ARQUIVO. VIDE OBS. P�G. 20');
            AL(L,I, '118', '124', '007', 'N', ' ', 'X', '        ', '699', '               ', 'C�DIGO OCULTO BANCO ', 'C�DIGO QUE � GERADO QUANDO � USADO UM APLICATIVO DO BANCO PARA CRIAR O ARQUIVO REMESSA');
            AL(L,I, '125', '394', '270', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '000001         ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          1: // Detalhe obrigat�rio
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '1              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO TRANSA��O');
            AL(L,I, '002', '006', '005', 'N', '0', 'I', '        ', '611', '               ', 'AG�NCIA DE D�BITO   ', 'C�DIGO DA AG�NCIA DO SACADO EXCLUSIVO PARA D�BITO EM CONTA. VIDE OBD. P�G. 20');
            AL(L,I, '007', '007', '001', 'N', '0', 'X', '        ', '612', '               ', 'DAC AG�NCIA D�BITO  ', 'D�GITO VERIFICADOR DA AG�NCIA DO SACADO. VIDE OBS. P�G. 20');
            AL(L,I, '008', '012', '005', 'N', '0', 'I', '        ', '613', '               ', 'RAZ�O C/C SACADO    ', 'RAZ�O DA CONTA CORRENTE DO SACADO (EX. 07050). VIDE OBS. P�G. 20');
            AL(L,I, '013', '019', '007', 'N', '0', 'I', '        ', '614', '               ', 'CONTA CORRENTE SACAD', 'N�MERO DA CONTA CORRENTE DO SACADO. VIDE OBS. P�G. 20');
            AL(L,I, '020', '020', '001', 'N', '0', 'X', '        ', '615', '               ', 'DAC C/C SACADO      ', 'D�GITO VERIFICADOR DA CONTA CORRENTE DO SACADO. VIDE OBS. P�G. 20');
            AL(L,I, '021', '021', '001', 'N', '0', 'I', '        ', '-01', '0              ', 'ID CEDENTE 1/5)     ', 'IDENTIFICA��O DA EMPRESA CEDENTE - PREENCHER COM ZERO');
            AL(L,I, '022', '024', '003', 'N', '0', 'I', '        ', '508', '               ', 'C�DIGO DA CARTEIRA  ', 'IDENTIFICA��O DA EMPRESA CEDENTE - C�DIGO DA CARTEIRA');
            AL(L,I, '025', '029', '005', 'N', '0', 'I', '        ', '020', '               ', 'C�DIGO AG. CEDENTE  ', 'IDENTIFICA��O DA EMPRESA CEDENTE - C�DIGO DA AG�NCIA SEM O D�GITO');
            AL(L,I, '030', '036', '007', 'N', '0', 'I', '        ', '021', '               ', 'N�MERO C/C CEDENTE  ', 'IDENTIFICA��O DA EMPRESA CEDENTE - N�MERO DA CONTA CORRENTE');
            AL(L,I, '037', '037', '001', 'N', '0', 'X', '        ', '023', '               ', 'DAC C/C CEDENTE     ', 'IDENTIFICA��O DA EMPRESA CEDENTE - DIGITO VERIFICADOR DA CONTA CORRENTE');
            AL(L,I, '038', '062', '025', 'N', ' ', 'X', '        ', '506', '               ', 'USO DA EMPRESA      ', 'IDENTIFICA��O DO T�TULO NA EMPRESA');
            AL(L,I, '063', '065', '003', 'N', '0', 'I', '        ', '610', '               ', 'C�DIGO DO BANCO     ', 'N� DO BANCO NA C�MARA DE COMPENSA��O');
            AL(L,I, '066', '066', '001', 'N', '0', 'I', '        ', '577', '               ', 'CAMPO DE MULTA      ', 'SE CAMPO = 2 INFORMAR PERCENTUAL DE MULTA NOS CAMPOS 067 A 070 SE CAMPO = 0 DESCONSIDERAR MULTA');
            AL(L,I, '067', '070', '004', 'N', '0', 'F', '2       ', '575', '               ', 'PERCENTUAL DE MULTA ', 'PERCENTUAL DE MULTA A SER COBRADO SE O CAMPO 066 = 2');
            AL(L,I, '071', '081', '011', 'N', '0', 'I', '        ', '501', '               ', 'IDENT.TITUL.NO BANCO', 'NOSSO NUMERO (SEM O DAC) PARA COBRAN�A COM E SEM REGISTRO');
            AL(L,I, '082', '082', '001', 'N', '0', 'X', '        ', '511', '               ', 'DAC DO NOSSO N�MERO ', 'DAC DO NOSSO N�MERO');
            AL(L,I, '083', '092', '010', 'N', '0', 'F', '2       ', '558', '               ', 'DESC.BONIFIC.POR DIA', 'VALOR DE DESCONTO BONIFICA��O POR DIA');
            AL(L,I, '093', '093', '001', 'N', '0', 'I', '        ', '621', '               ', 'IMPRESSAO DO BOLETO ', '1 = BANCO EMITE E PROCESSA O REGISTRO; 2 = CLIENTE EMITE E O BANCO SOMENTE PROCESSA O REGISTRO. VIDE OBS P�G. 23');
            AL(L,I, '094', '094', '001', 'N', ' ', 'X', '        ', '622', '               ', 'COND.D�B.AUTOM�TICO ', 'N=N�O REGISTRA NA COBRAN�A E DIFERENTE DE N REGISTRA E EMITE BLOQUETO');
            AL(L,I, '095', '104', '010', 'N', ' ', 'B', '        ', '-02', '               ', 'IDENT.OPER. NO BANCO', 'IDENTIFICA��O DA OPERA��O NO BANCO - PREENCHER COM BRANCOS');
            AL(L,I, '105', '105', '001', 'N', ' ', 'X', '        ', '623', '               ', 'IDENT.RATEIO CR�DITO', 'DEVER� SER PREENCHIDO COM "R" SE A EMPRESA PARTICIPA DA ROTINA DE RATEIO DE CR�DITO. CASO CONTR�RIO DEVER� SER INFORMADO EM BRANCO');
            AL(L,I, '106', '106', '001', 'N', '0', 'I', '        ', '624', '               ', 'ENDER.AVISO D�B.AUTO', 'ENDERE�AMENTO PARA AVISO DE D�BITO AUTOM�TICO: 1 = EMITE AVISO, E ASSUME ENDERE�O DO SACADO CONSTANTE NO ARQUIVO REMESSA; 2 = N�O EMITE AVISO; DIFERENTE DE 1 E 2 EMITE E ASSUME ENDERE�O CONSTANTE NO CADASTRO DO BRADESCO');
            AL(L,I, '107', '108', '002', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'BRANCOS');
            AL(L,I, '109', '110', '002', 'N', '0', 'I', '        ', '504', '               ', 'C�D. DE OCORR�NCIA  ', 'IDENTIFICA��O DA OCORR�NCIA');
            AL(L,I, '111', '120', '010', 'N', ' ', 'X', '        ', '502', '               ', 'N� DO DOCUMENTO     ', 'N� DO DOCUMENTO DE COBRAN�A (DUPL.,NP ETC.)');
            AL(L,I, '121', '126', '006', 'N', '9', 'D', 'DDMMYY  ', '580', '               ', 'VENCIMENTO          ', 'DATA DE VENCIMENTO DO T�TULO');
            AL(L,I, '127', '139', '013', 'N', '0', 'F', '2       ', '550', '               ', 'VALOR DO T�TULO     ', 'VALOR NOMINAL DO T�TULO');
            AL(L,I, '140', '142', '003', 'N', '0', 'I', '        ', '027', '               ', 'BANCO COBRADOR      ', 'N� DO BANCO COBRADOR NA C�MARA DE COMPENSA��O');
            AL(L,I, '143', '147', '005', 'N', '0', 'I', '        ', '025', '               ', 'AG�NCIA COBRADORA   ', 'AG�NCIA ONDE O T�TULO SER� COBRADO');
            AL(L,I, '148', '149', '002', 'N', '0', 'I', '        ', '507', '               ', 'ESP�CIE             ', 'ESP�CIE DO T�TULO');
            AL(L,I, '150', '150', '001', 'N', ' ', 'X', '        ', '520', '               ', 'ACEITE              ', 'IDENTIFICA��O DE T�TULO ACEITO OU N�O ACEITO');
            AL(L,I, '151', '156', '006', 'N', '0', 'D', 'DDMMYY  ', '583', '               ', 'DATA DE EMISS�O     ', 'DATA DA EMISS�O DO T�TULO');
            AL(L,I, '157', '158', '002', 'N', '0', 'I', '        ', '701', '               ', 'INSTRU��O 1         ', '1� INSTRU��O DE COBRAN�A');
            AL(L,I, '159', '160', '002', 'N', '0', 'I', '        ', '702', '               ', 'INSTRU��O 2         ', '2� INSTRU��O DE COBRAN�A');
            AL(L,I, '161', '173', '013', 'N', '0', 'F', '2       ', '572', '               ', 'JUROS DE 1 DIA      ', 'VALOR DE MORA POR DIA DE ATRASO');
            AL(L,I, '174', '179', '006', 'N', '0', 'D', 'DDMMYY  ', '586', '               ', 'DESCONTO AT�        ', 'DATA LIMITE PARA CONCESS�O DE DESCONTO');
            AL(L,I, '180', '192', '013', 'N', '0', 'F', '2       ', '552', '               ', 'VALOR DO DESCONTO   ', 'VALOR DO DESCONTO A SER CONCEDIDO');
            AL(L,I, '193', '205', '013', 'N', '0', 'F', '2       ', '569', '               ', 'VALOR DO I.O.F.     ', 'VALOR DO I.O.F. RECOLHIDO P/ NOTAS SEGURO');
            AL(L,I, '206', '218', '013', 'N', '0', 'F', '2       ', '551', '               ', 'ABATIMENTO          ', 'VALOR DO ABATIMENTO A SER CONCEDIDO');
            AL(L,I, '219', '220', '002', 'N', '0', 'I', '        ', '801', '               ', 'C�DIGO DE INSCRI��O ', 'IDENTIFICA��O DO TIPO DE INSCRI��O/SACADO');
            AL(L,I, '221', '234', '014', 'N', '0', 'I', '        ', '802', '               ', 'N�MERO DE INSCRI��O ', 'N� DE INSCRI��O DO SACADO  (CPF/CNPJ)');
            AL(L,I, '235', '274', '040', 'S', ' ', 'X', '        ', '803', '               ', 'NOME                ', 'NOME DO SACADO');
            AL(L,I, '275', '314', '040', 'S', ' ', 'X', '        ', '804', '               ', 'ENDERE�O DO SACADO  ', 'ENDERE�O COMPLETO DO SACADO');
            AL(L,I, '315', '326', '012', 'S', ' ', 'X', '        ', '639', '               ', '1� MENSAGEM BLOQUETO', '1� MENSAGEM A SER IMPRESSA NO BLOQUETO');
            AL(L,I, '327', '334', '008', 'N', '0', 'I', '        ', '806', '               ', 'CEP                 ', 'CEP DO SACADO');
            AL(L,I, '335', '394', '060', 'N', ' ', 'X', '        ', '640', '               ', 'SACADOR OU 2� MENSAG', 'SACADOR / AVALISTA OU SEGUNDA MENSAGEM');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N� SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          2: // Registro de mensagens (Opcional)
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '2              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO DETALHE - MENSAGEM (OPCIONAL)');
            AL(L,I, '002', '081', '080', 'N', ' ', 'X', '        ', '201', '               ', 'MENSAGEM 1          ', 'MENSAGEM COM 80 POSI��ES (PARA CONSIDERAR UMA LINHA DEVE TER PELO MENOS 41 CARACTERES');
            AL(L,I, '082', '161', '080', 'N', ' ', 'X', '        ', '202', '               ', 'MENSAGEM 2          ', 'MENSAGEM COM 80 POSI��ES (PARA CONSIDERAR UMA LINHA DEVE TER PELO MENOS 41 CARACTERES');
            AL(L,I, '162', '241', '080', 'N', ' ', 'X', '        ', '203', '               ', 'MENSAGEM 3          ', 'MENSAGEM COM 80 POSI��ES (PARA CONSIDERAR UMA LINHA DEVE TER PELO MENOS 41 CARACTERES');
            AL(L,I, '242', '321', '080', 'N', ' ', 'X', '        ', '204', '               ', 'MENSAGEM 4          ', 'MENSAGEM COM 80 POSI��ES (PARA CONSIDERAR UMA LINHA DEVE TER PELO MENOS 41 CARACTERES');
            AL(L,I, '322', '327', '006', 'N', '0', 'D', 'DDMMAA  ', '595', '               ', 'DATA DO DESCONTO 2  ', 'DATA LIMITE PARA O DESCONTO 2');
            AL(L,I, '328', '340', '013', 'N', '0', 'F', '2       ', '596', '               ', 'VALOR DO DESCONTO 2 ', 'VALOR DO DESCONTO 2 (FIXO OU AO DIA)');
            AL(L,I, '341', '327', '006', 'N', '0', 'D', 'DDMMAA  ', '598', '               ', 'DATA DO DESCONTO 3  ', 'DATA LIMITE PARA O DESCONTO 3');
            AL(L,I, '347', '359', '013', 'N', '0', 'F', '2       ', '599', '               ', 'VALOR DO DESCONTO 3 ', 'VALOR DO DESCONTO 3 (FIXO OU AO DIA)');
            AL(L,I, '360', '366', '007', 'N', ' ', 'B', '        ', '-02', '               ', 'RESERVA             ', 'FILLER');
            AL(L,I, '367', '369', '003', 'N', '0', 'I', '        ', '509', '               ', 'CARTEIRA            ', 'N�MERO DA CARTEIRA');
            AL(L,I, '370', '374', '005', 'N', '0', 'I', '        ', '020', '               ', 'AG�NCIA             ', 'C�DIGO DA AG�NCIA CEDENTE');
            AL(L,I, '375', '381', '007', 'N', '0', 'I', '        ', '021', '               ', 'CONTA CORRENTE      ', 'N�MERO DA CONTA CORRENTE DO CEDENTE');
            AL(L,I, '382', '382', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DAC C/C             ', 'D�GITO VERIFICADOR DA CONTA CORRENTE DO CEDENTE');
            AL(L,I, '383', '393', '011', 'N', '0', 'I', '        ', '501', '               ', 'NOSSO N�MERO SEM DAC', 'IDENTIFICA��O DO T�TULO NO BANCO (SEM O D�GITO VERIFICADOR)');
            AL(L,I, '394', '394', '001', 'N', ' ', 'X', '        ', '511', '               ', 'DAC NOSSO N�MERO    ', 'D�GITO VERIFICADOR DO NOSSO N�MERO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N� SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
          end;
          3: // Registro de RATEIO DE CR�DITO
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '3              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO DETALHE (RATEIO DE CR�DITO)');
            AL(L,I, '002', '004', '003', 'N', '0', 'I', '        ', '509', '               ', 'CARTEIRA            ', 'N�MERO DA CARTEIRA');
            AL(L,I, '005', '009', '005', 'N', '0', 'I', '        ', '020', '               ', 'AG�NCIA             ', 'C�DIGO DA AG�NCIA CEDENTE');
            AL(L,I, '010', '016', '007', 'N', '0', 'I', '        ', '021', '               ', 'CONTA CORRENTE      ', 'N�MERO DA CONTA CORRENTE DO CEDENTE');
            AL(L,I, '016', '017', '001', 'N', ' ', 'X', '        ', '023', '               ', 'DAC C/C             ', 'D�GITO VERIFICADOR DA CONTA CORRENTE DO CEDENTE');
            AL(L,I, '018', '028', '011', 'N', '0', 'I', '        ', '501', '               ', 'NOSSO N�MERO SEM DAC', 'IDENTIFICA��O DO T�TULO NO BANCO (SEM O D�GITO VERIFICADOR)');
            AL(L,I, '029', '029', '001', 'N', ' ', 'X', '        ', '511', '               ', 'DAC NOSSO N�MERO    ', 'D�GITO VERIFICADOR DO NOSSO N�MERO');
            AL(L,I, '030', '030', '001', 'N', '0', 'I', '        ', '625', '               ', 'C�D.CALCULO RATEIO  ', 'C�DIGO DE C�LCULO DE RATEIO "1"=VALOR COBRADO "2"=VALOR DO REGISTRO "3"=RATEIO PELO MENOR VALOR');
            AL(L,I, '031', '031', '001', 'N', '0', 'I', '        ', '626', '               ', 'TIPO VALOR RATEIO   ', 'TIPO DE VALOR INFORMADO "1"= % "2"=VALOR');
            AL(L,I, '032', '043', '012', 'N', ' ', 'B', '        ', '-02', '               ', 'FILLER              ', 'BRANCOS');

            AL(L,I, '044', '046', '003', 'N', '0', 'I', '        ', '630', '237            ', 'C�D.BANCO 1� BENEF. ', 'C�DIGO DO BANCO PARA CR�DITO DO 1� BENEFICI�RIO - FIXO "237"');
            AL(L,I, '047', '051', '005', 'N', '0', 'I', '        ', '631', '               ', 'C�D.AGENCIA 1� BENEF', 'C�DIGO DA AG�NCIA PARA CR�DITO DO 1� BENEFICI�RIO');
            AL(L,I, '052', '052', '001', 'N', ' ', 'X', '        ', '632', '               ', 'DAC AGENCIA 1� BENEF', 'DIGITO VERIFICADOR DA AG�NCIA PARA CR�DITO DO 1� BENEFICI�RIO');
            AL(L,I, '053', '064', '012', 'N', '0', 'I', '        ', '633', '               ', 'N� C/C 1� BENEFIC.  ', 'N�MERO DA CONTA CORRENTE PARA CR�DITO DO 1� BENEFICI�RIO');
            AL(L,I, '065', '065', '001', 'N', ' ', 'X', '        ', '634', '               ', 'DAC C/C 1� BENEFIC. ', 'DIGITO VERIFICADOR DA CONTA CORRENTE PARA CR�DITO DO 1� BENEFICI�RIO');
            AL(L,I, '066', '080', '015', 'N', '0', 'F', '        ', '635', '               ', 'VALOR OU $ P/ RATEIO', '%=INFORMADO EM PERCENTUAIS COM REPRESENTA��O NA M�SCARA DE 000000000999999, ONDE TER� 3 DECIMAIS PARA C�LCULO. VALOR=VLR INFORMADO EM VALOR  - VIDE OBS. P�G. 28');
            AL(L,I, '081', '120', '040', 'S', ' ', 'X', '        ', '636', '               ', 'NOME 1� BENEFICIARIO', 'NOME DO 1� BENEFICI�RIO');
            AL(L,I, '121', '151', '031', 'N', ' ', 'X', '        ', '-02', '               ', 'FILLER              ', 'BRANCOS');
            AL(L,I, '152', '157', '006', 'N', ' ', 'X', '        ', '637', '               ', 'PARCELA 1� BENEFIC. ', 'IDENTIFICA��O DA PARCELA DO 1� BENEFICI�RIO');
            AL(L,I, '158', '160', '003', 'N', '0', 'I', '        ', '638', '               ', 'FLOATING 1� BENEFIC.', 'QUANTIDADE DE DIAS PARA CR�DITO DO 1� BENEFICI�RIO');

            AL(L,I, '161', '163', '003', 'N', '0', 'I', '        ', '630', '237            ', 'C�D.BANCO 2� BENEF. ', 'C�DIGO DO BANCO PARA CR�DITO DO 2� BENEFICI�RIO - FIXO "237"');
            AL(L,I, '164', '168', '005', 'N', '0', 'I', '        ', '631', '               ', 'C�D.AGENCIA 2� BENEF', 'C�DIGO DA AG�NCIA PARA CR�DITO DO 2� BENEFICI�RIO');
            AL(L,I, '169', '169', '001', 'N', ' ', 'X', '        ', '632', '               ', 'DAC AGENCIA 2� BENEF', 'DIGITO VERIFICADOR DA AG�NCIA PARA CR�DITO DO 2� BENEFICI�RIO');
            AL(L,I, '170', '181', '012', 'N', '0', 'I', '        ', '633', '               ', 'N� C/C 2� BENEFIC.  ', 'N�MERO DA CONTA CORRENTE PARA CR�DITO DO 2� BENEFICI�RIO');
            AL(L,I, '182', '182', '001', 'N', ' ', 'X', '        ', '634', '               ', 'DAC C/C 2� BENEFIC. ', 'DIGITO VERIFICADOR DA CONTA CORRENTE PARA CR�DITO DO 2� BENEFICI�RIO');
            AL(L,I, '183', '197', '015', 'N', '0', 'F', '        ', '635', '               ', 'VALOR OU $ P/ RATEIO', '%=INFORMADO EM PERCENTUAIS COM REPRESENTA��O NA M�SCARA DE 000000000999999, ONDE TER� 3 DECIMAIS PARA C�LCULO. VALOR=VLR INFORMADO EM VALOR  - VIDE OBS. P�G. 28');
            AL(L,I, '198', '237', '040', 'S', ' ', 'X', '        ', '636', '               ', 'NOME 2� BENEFICIARIO', 'NOME DO 2� BENEFICI�RIO');
            AL(L,I, '238', '268', '031', 'N', ' ', 'X', '        ', '-02', '               ', 'FILLER              ', 'BRANCOS');
            AL(L,I, '269', '274', '006', 'N', ' ', 'X', '        ', '637', '               ', 'PARCELA 2� BENEFIC. ', 'IDENTIFICA��O DA PARCELA DO 2� BENEFICI�RIO');
            AL(L,I, '275', '277', '003', 'N', '0', 'I', '        ', '638', '               ', 'FLOATING 2� BENEFIC.', 'QUANTIDADE DE DIAS PARA CR�DITO DO 2� BENEFICI�RIO');

            AL(L,I, '278', '280', '003', 'N', '0', 'I', '        ', '630', '237            ', 'C�D.BANCO 3� BENEF. ', 'C�DIGO DO BANCO PARA CR�DITO DO 3� BENEFICI�RIO - FIXO "237"');
            AL(L,I, '281', '285', '005', 'N', '0', 'I', '        ', '631', '               ', 'C�D.AGENCIA 3� BENEF', 'C�DIGO DA AG�NCIA PARA CR�DITO DO 3� BENEFICI�RIO');
            AL(L,I, '286', '286', '001', 'N', ' ', 'X', '        ', '632', '               ', 'DAC AGENCIA 3� BENEF', 'DIGITO VERIFICADOR DA AG�NCIA PARA CR�DITO DO 3� BENEFICI�RIO');
            AL(L,I, '287', '298', '012', 'N', '0', 'I', '        ', '633', '               ', 'N� C/C 3� BENEFIC.  ', 'N�MERO DA CONTA CORRENTE PARA CR�DITO DO 3� BENEFICI�RIO');
            AL(L,I, '299', '299', '001', 'N', ' ', 'X', '        ', '634', '               ', 'DAC C/C 3� BENEFIC. ', 'DIGITO VERIFICADOR DA CONTA CORRENTE PARA CR�DITO DO 3� BENEFICI�RIO');
            AL(L,I, '300', '314', '015', 'N', '0', 'F', '        ', '635', '               ', 'VALOR OU $ P/ RATEIO', '%=INFORMADO EM PERCENTUAIS COM REPRESENTA��O NA M�SCARA DE 000000000999999, ONDE TER� 3 DECIMAIS PARA C�LCULO. VALOR=VLR INFORMADO EM VALOR  - VIDE OBS. P�G. 28');
            AL(L,I, '315', '354', '040', 'S', ' ', 'X', '        ', '636', '               ', 'NOME 3� BENEFICIARIO', 'NOME DO 3� BENEFICI�RIO');
            AL(L,I, '355', '385', '031', 'N', ' ', 'X', '        ', '-02', '               ', 'FILLER              ', 'BRANCOS');
            AL(L,I, '386', '391', '006', 'N', ' ', 'X', '        ', '637', '               ', 'PARCELA 3� BENEFIC. ', 'IDENTIFICA��O DA PARCELA DO 3� BENEFICI�RIO');
            AL(L,I, '392', '394', '003', 'N', '0', 'I', '        ', '638', '               ', 'FLOATING 3� BENEFIC.', 'QUANTIDADE DE DIAS PARA CR�DITO DO 3� BENEFICI�RIO');

            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
          end;
          7: // Dados do Sacador Avalista (opcional)
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '012', '7              ', 'TIPO DE REGISTRO    ', '7');
            AL(L,I, '002', '046', '045', 'S', ' ', 'X', '        ', '854', '               ', 'ENDER SACADOR/AVALIS', 'ENDERE�O SACADOR/AVALISTA');
            AL(L,I, '047', '054', '008', 'N', '0', 'I', '        ', '856', '               ', 'CEP                 ', 'CEP SACADOR AVALISTA');
            AL(L,I, '055', '074', '020', 'S', ' ', 'X', '        ', '857', '               ', 'CIDADE              ', 'CIDADE SACADOR AVALISTA');
            AL(L,I, '075', '076', '002', 'N', ' ', 'X', '        ', '858', '               ', 'UF                  ', 'UF SACADOR AVALISTA');
            AL(L,I, '077', '366', '290', 'N', ' ', 'B', '        ', '-02', '               ', 'RESERVA             ', 'FILLER');
            AL(L,I, '367', '369', '003', 'N', '0', 'I', '        ', '509', '               ', 'CARTEIRA            ', 'NUMERO DA ACRTEIRA');
            AL(L,I, '370', '374', '005', 'N', '0', 'I', '        ', '020', '               ', 'AG�NCIA             ', 'C�DIGO DA AG�NCIA CEDENTE');
            AL(L,I, '375', '381', '007', 'N', '0', 'I', '        ', '021', '               ', 'CONTA CORRENTE      ', 'N�MERO DA CONTA CORRENTE');
            AL(L,I, '382', '382', '001', 'N', ' ', 'X', '        ', '023', '               ', 'D�GITO C/C          ', 'DAC C/C');
            AL(L,I, '383', '393', '011', 'N', '0', 'I', '        ', '501', '               ', 'NOSSO N�MERO        ', 'NOSSO N�MERO');
            AL(L,I, '394', '394', '001', 'N', '0', 'I', '        ', '511', '               ', 'DAC DO NOSSO N�MERO ', 'DAC DO NOSSO N�MERO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
          end;
          9: // Registro Trailer
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '012', '9              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO TRAILER');
            AL(L,I, '002', '394', '393', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
        end;
      end;
    end;
    341:  // com registro
    begin
      Result := cresNoLayoutName;
      if (NomeLayout = CO_341_COBRANCA_BANCARIA_MARCO_2012) or (NomeLayout =
        CO_341_COBRANCA_BANCARIA_SETEMBRO_2012) then
      begin
        case Registro of
          0: // Header de arquivo
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '010', '0              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO HEADER');
            AL(L,I, '002', '002', '001', 'N', '0', 'I', '        ', '005', '1              ', 'OPERA��O            ', 'TIPO DE OPERA��O - REMESSA');
            AL(L,I, '003', '009', '007', 'N', ' ', 'X', '        ', '006', 'REMESSA        ', 'LITERAL DE REMESSA  ', 'IDENTIFICA��O POR EXTENSO DO MOVIMENTO');
            AL(L,I, '010', '011', '002', 'N', '0', 'I', '        ', '003', '01             ', 'C�DIGO DO SERVI�O   ', 'IDENTIFICA��O DO TIPO DE SERVI�O');
            AL(L,I, '012', '026', '015', 'N', ' ', 'X', '        ', '004', 'COBRANCA       ', 'LITERAL DE SERVI�O  ', 'IDENTIFICA��O POR EXTENSO DO TIPO DE SERVI�O');
            AL(L,I, '027', '030', '004', 'N', '0', 'I', '        ', '020', '               ', 'AG�NCIA             ', 'AG�NCIA MANTENEDORA DA CONTA');
            AL(L,I, '031', '032', '002', 'N', '0', 'Z', '        ', '-01', '00             ', 'ZEROS               ', 'COMPLEMENTO DE REGISTRO');
            AL(L,I, '033', '037', '005', 'N', '0', 'I', '        ', '021', '               ', 'CONTA               ', 'N�MERO DA CONTA CORRENTE DA EMPRESA');
            AL(L,I, '038', '038', '001', 'N', '0', 'I', '        ', '024', '               ', 'DAC                 ', 'D�GITO DE AUTO CONFER�NCIA AG/CONTA EMPRESA');
            AL(L,I, '039', '046', '008', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '047', '076', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DA EMPRESA     ', 'NOME POR EXTENSO DA "EMPRESA M�E"');
            AL(L,I, '077', '079', '003', 'N', '0', 'I', '        ', '001', '341            ', 'C�DIGO DO BANCO     ', 'N� DO BANCO NA C�MARA DE COMPENSA��O');
            AL(L,I, '080', '094', '015', 'N', ' ', 'X', '        ', '002', 'BANCO ITAU SA  ', 'NOME DO BANCO       ', 'NOME POR EXTENSO DO BANCO COBRADOR');
            AL(L,I, '095', '100', '006', 'N', '0', 'D', 'DDMMAA  ', '990', '               ', 'DATA DE GERA��O     ', 'DATA DE GERA��O DO ARQUIVO');
            AL(L,I, '101', '390', '290', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '391', '394', '004', 'N', ' ', 'X', '        ', '699', '               ', 'C�DIGO OCULTO BANCO ', 'C�DIGO QUE � GERADO QUANDO � USADO UM APLICATIVO DO BANCO PARA CRIAR O ARQUIVO REMESSA');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '000001         ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          1: // Detalhe obrigat�rio
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '1              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO TRANSA��O');
            AL(L,I, '002', '003', '002', 'N', '0', 'I', '        ', '400', '               ', 'C�DIGO DE INSCRI��O ', 'TIPO DE INSCRI��O DA EMPRESA');
            AL(L,I, '004', '017', '014', 'N', '0', 'I', '        ', '401', '               ', 'N�MERO DE INSCRI��O ', 'N� DE INSCRI��O DA EMPRESA (CPF/CNPJ)');
            AL(L,I, '018', '021', '004', 'N', '0', 'I', '        ', '020', '               ', 'AG�NCIA             ', 'AG�NCIA MANTENEDORA DA CONTA');
            AL(L,I, '022', '023', '002', 'N', '0', 'I', '        ', '-01', '               ', 'ZEROS               ', 'COMPLEMENTO DE REGISTRO');
            AL(L,I, '024', '028', '005', 'N', '0', 'I', '        ', '021', '               ', 'CONTA               ', 'N�MERO DA CONTA CORRENTE DA EMPRESA');
            AL(L,I, '029', '029', '001', 'N', '0', 'I', '        ', '024', '               ', 'DAC                 ', 'D�GITO DE AUTO CONFER�NCIA AG/CONTA EMPRESA');
            AL(L,I, '030', '033', '004', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DE REGISTRO');
            AL(L,I, '034', '037', '004', 'N', '0', 'I', '        ', '750', '               ', 'INSTRU��O/ALEGA��O  ', 'C�D.INSTRU��O/ALEGA��O A SER CANCELADA');
            AL(L,I, '038', '062', '025', 'N', ' ', 'X', '        ', '506', '               ', 'USO DA EMPRESA      ', 'IDENTIFICA��O DO T�TULO NA EMPRESA');
            // 2012-08-24
            //AL(L,I, '063', '070', '008', 'N', '0', 'I', '        ', '501', '               ', 'NOSSO N�MERO        ', 'IDENTIFICA��O DO T�TULO NO BANCO');
            AL(L,I, '063', '070', '008', 'N', '0', 'I', '        ', '512', '               ', 'NOSSO N�MERO        ', 'IDENTIFICA��O DO T�TULO NO BANCO');
            // Fim 2012-08-24
            AL(L,I, '071', '083', '013', 'N', '0', 'F', '5       ', '749', '               ', 'QTDE DE MOEDA       ', 'QUANTIDADE DE MOEDA VARI�VEL');
            AL(L,I, '084', '086', '003', 'N', '0', 'I', '        ', '509', '               ', 'N� DA CARTEIRA      ', 'N�MERO DA CARTEIRA NO BANCO');
            AL(L,I, '087', '107', '021', 'N', ' ', 'X', '        ', '-02', '               ', 'USO DO BANCO        ', 'IDENTIFICA��O DA OPERA��O NO BANCO');
            AL(L,I, '108', '108', '001', 'N', ' ', 'X', '        ', '508', '               ', 'CARTEIRA            ', 'C�DIGO DA CARTEIRA');
            AL(L,I, '109', '110', '002', 'N', '0', 'I', '        ', '504', '               ', 'C�D. DE OCORR�NCIA  ', 'IDENTIFICA��O DA OCORR�NCIA');
            AL(L,I, '111', '120', '010', 'N', ' ', 'X', '        ', '502', '               ', 'N� DO DOCUMENTO     ', 'N� DO DOCUMENTO DE COBRAN�A (DUPL.,NP ETC.)');
            AL(L,I, '121', '126', '006', 'N', '9', 'D', 'DDMMYY  ', '580', '               ', 'VENCIMENTO          ', 'DATA DE VENCIMENTO DO T�TULO');
            AL(L,I, '127', '139', '013', 'N', '0', 'F', '2       ', '550', '               ', 'VALOR DO T�TULO     ', 'VALOR NOMINAL DO T�TULO');
            AL(L,I, '140', '142', '003', 'N', '0', 'I', '        ', '027', '341            ', 'C�DIGO DO BANCO     ', 'N� DO BANCO NA C�MARA DE COMPENSA��O');
            AL(L,I, '143', '147', '005', 'N', '0', 'I', '        ', '025', '               ', 'AG�NCIA COBRADORA   ', 'AG�NCIA ONDE O T�TULO SER� COBRADO');
            AL(L,I, '148', '149', '002', 'N', ' ', 'X', '        ', '507', '               ', 'ESP�CIE             ', 'ESP�CIE DO T�TULO');
            AL(L,I, '150', '150', '001', 'N', ' ', 'X', '        ', '520', '               ', 'ACEITE              ', 'IDENTIFICA��O DE T�TULO ACEITO OU N�O ACEITO');
            AL(L,I, '151', '156', '006', 'N', '0', 'D', 'DDMMYY  ', '583', '               ', 'DATA DE EMISS�O     ', 'DATA DA EMISS�O DO T�TULO');
            AL(L,I, '157', '158', '002', 'N', ' ', 'X', '        ', '701', '               ', 'INSTRU��O 1         ', '1� INSTRU��O DE COBRAN�A');
            AL(L,I, '159', '160', '002', 'N', ' ', 'X', '        ', '702', '               ', 'INSTRU��O 2         ', '2� INSTRU��O DE COBRAN�A');
            AL(L,I, '161', '173', '013', 'N', '0', 'F', '2       ', '572', '               ', 'JUROS DE 1 DIA      ', 'VALOR DE MORA POR DIA DE ATRASO');
            AL(L,I, '174', '179', '006', 'N', '0', 'D', 'DDMMYY  ', '586', '               ', 'DESCONTO AT�        ', 'DATA LIMITE PARA CONCESS�O DE DESCONTO');
            AL(L,I, '180', '192', '013', 'N', '0', 'F', '2       ', '552', '               ', 'VALOR DO DESCONTO   ', 'VALOR DO DESCONTO A SER CONCEDIDO');
            AL(L,I, '193', '205', '013', 'N', '0', 'F', '2       ', '569', '               ', 'VALOR DO I.O.F.     ', 'VALOR DO I.O.F. RECOLHIDO P/ NOTAS SEGURO');
            AL(L,I, '206', '218', '013', 'N', '0', 'F', '2       ', '551', '               ', 'ABATIMENTO          ', 'VALOR DO ABATIMENTO A SER CONCEDIDO');
            AL(L,I, '219', '220', '002', 'N', '0', 'I', '        ', '801', '               ', 'C�DIGO DE INSCRI��O ', 'IDENTIFICA��O DO TIPO DE INSCRI��O/SACADO');
            AL(L,I, '221', '234', '014', 'N', '0', 'I', '        ', '802', '               ', 'N�MERO DE INSCRI��O ', 'N� DE INSCRI��O DO SACADO  (CPF/CNPJ)');
            AL(L,I, '235', '264', '030', 'S', ' ', 'X', '        ', '803', '               ', 'NOME                ', 'NOME DO SACADO');
            AL(L,I, '265', '274', '010', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DE REGISTRO');
            AL(L,I, '275', '314', '040', 'S', ' ', 'X', '        ', '804', '               ', 'LOGRADOURO          ', 'RUA, N�MERO E COMPLEMENTO DO SACADO');
            AL(L,I, '315', '326', '012', 'S', ' ', 'X', '        ', '805', '               ', 'BAIRRO              ', 'BAIRRO DO SACADO');
            AL(L,I, '327', '334', '008', 'N', '0', 'I', '        ', '806', '               ', 'CEP                 ', 'CEP DO SACADO');
            AL(L,I, '335', '349', '015', 'S', ' ', 'X', '        ', '807', '               ', 'CIDADE              ', 'CIDADE DO SACADO');
            AL(L,I, '350', '351', '002', 'N', ' ', 'X', '        ', '808', '               ', 'ESTADO              ', 'UF DO SACADO');
            AL(L,I, '352', '381', '030', 'S', ' ', 'X', '        ', '853', '               ', 'SACADOR/AVALISTA    ', 'NOME DO SACADOR OU AVALISTA');
            AL(L,I, '382', '385', '004', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '386', '391', '006', 'N', '0', 'D', 'DDMMYY  ', '584', '               ', 'DATA DE MORA        ', 'DATA DE MORA');
            AL(L,I, '392', '393', '002', 'N', '0', 'I', '        ', '950', '               ', 'PRAZO               ', 'QUANTIDADE DE DIAS');
            AL(L,I, '394', '394', '001', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N� SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          5: // Detalhe opcional (email e/ou Sacador / Avalista)
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '5              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO TRANSA��O');
            AL(L,I, '002', '121', '120', 'N', ' ', 'X', '        ', '815', '               ', 'ENDERE�O DE E-MAIL  ', 'ENDERE�O DE E-MAIL DO SACADO');
            AL(L,I, '122', '123', '002', 'N', '0', 'I', '        ', '851', '               ', 'C�DIGO DE INSCRI��O ', 'IDENT. DO TIPO DE INSCRI��O DO SACADOR/AVALISTA');
            AL(L,I, '124', '137', '014', 'N', '0', 'I', '        ', '852', '               ', 'N�MERO DE INSCRI��O ', 'N�MERO DE INSCRI��O DO SACADOR AVALISTA');
            AL(L,I, '138', '177', '040', 'S', ' ', 'X', '        ', '854', '               ', 'LOGRADOURO          ', 'RUA, N� E COMPLEMENTO DO SACADOR AVALISTA');
            AL(L,I, '178', '189', '012', 'S', ' ', 'X', '        ', '855', '               ', 'BAIRRO              ', 'BAIRRO DO SACADOR AVALISTA');
            AL(L,I, '190', '197', '008', 'N', '0', 'I', '        ', '856', '               ', 'CEP                 ', 'CEP DO SACADOR AVALISTA');
            AL(L,I, '198', '212', '015', 'S', ' ', 'X', '        ', '857', '               ', 'CIDADE              ', 'CIDADE DO SACADOR AVALISTA');
            AL(L,I, '213', '214', '002', 'N', ' ', 'X', '        ', '858', '               ', 'ESTADO              ', 'UF (ESTADO) DO SACADOR AVALISTA');
            AL(L,I, '215', '394', '180', 'N', ' ', 'X', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DE REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQUENCIAL   ', 'N�MERO SEQUENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          9: // Registro Trailer
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '012', '9              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO TRAILER');
            AL(L,I, '002', '394', '393', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
        end;
      end;
    end;
    399:  // com registro
    begin
      Result := cresNoLayoutName;
      if NomeLayout = CO_399_MODULO_I_2009_10 then
      begin
        Result := cresNoField;
        case Registro of
          0: // Header de arquivo
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '010', '0              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO HEADER');
            AL(L,I, '002', '002', '001', 'N', '0', 'I', '        ', '005', '1              ', 'OPERA��O            ', 'TIPO DE OPERA��O - REMESSA');
            AL(L,I, '003', '009', '007', 'N', ' ', 'X', '        ', '006', 'REMESSA        ', 'LITERAL DE REMESSA  ', 'IDENTIFICA��O POR EXTENSO DO MOVIMENTO');
            AL(L,I, '010', '011', '002', 'N', '0', 'I', '        ', '003', '01             ', 'C�DIGO DO SERVI�O   ', 'IDENTIFICA��O DO TIPO DE SERVI�O');
            AL(L,I, '012', '026', '015', 'N', ' ', 'X', '        ', '004', 'COBRANCA       ', 'LITERAL DE SERVI�O  ', 'IDENTIFICA��O POR EXTENSO DO TIPO DE SERVI�O');
            AL(L,I, '027', '027', '001', 'N', '0', 'Z', '        ', '-01', '               ', 'ZEROS               ', 'ZERO');
            AL(L,I, '028', '031', '004', 'N', '0', 'I', '        ', '020', '               ', 'AG�NCIA             ', 'AG�NCIA MANTENEDORA DA CONTA');
            AL(L,I, '032', '033', '002', 'N', '0', 'I', '        ', '035', '55             ', 'SUB-CONTA           ', 'SUB-CONTA DA CONTA CORRENTE DO CLIENTE');
            AL(L,I, '034', '044', '011', 'N', '0', 'I', '        ', '037', '               ', 'CONTA CORRENTE CLIEN', 'Exemplo: Ag�ncia 4321; Conta 56789-00 Campo Conta Corrente ent�o ser�: 43215678900');
            AL(L,I, '045', '046', '002', 'N', ' ', 'B', '        ', '-02', '               ', 'USO DO BANCO        ', 'USO DO BANCO');
            AL(L,I, '047', '076', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DO CLIENTE     ', 'RAZ�O SOCIAL / NOME DO CLIENTE POR EXTENSO');
            AL(L,I, '077', '079', '003', 'N', '0', 'I', '        ', '001', '399            ', 'C�DIGO DO BANCO     ', 'N� DO BANCO NA C�MARA DE COMPENSA��O');
            AL(L,I, '080', '094', '015', 'N', ' ', 'X', '        ', '002', 'HSBC           ', 'NOME DO BANCO       ', 'NOME POR EXTENSO DO BANCO COBRADOR');
            AL(L,I, '095', '100', '006', 'N', '0', 'D', 'DDMMAA  ', '990', '               ', 'DATA DE GERA��O     ', 'DATA DE GERA��O DO ARQUIVO');
            AL(L,I, '101', '105', '005', 'N', '0', 'I', '        ', '888', '01600          ', 'DENSIDADE DE GRAVA�.', 'DENSIDADE DE GRAVA��O DO ARQUIVO');
            AL(L,I, '106', '108', '003', 'N', ' ', 'X', '        ', '890', 'BPI            ', 'LITERAL DENSIDADE   ', 'UNIDADE DE DENSIDADE DE GRAVA��O');
            AL(L,I, '109', '110', '002', 'N', ' ', 'B', '        ', '-02', '               ', 'USO DO BANCO        ', 'USO DO BANCO');
            AL(L,I, '111', '117', '007', 'N', ' ', 'X', '        ', '889', 'LANCV08        ', 'SIGLA LAYOUT        ', 'SIGLA DO LAYOUT T�CNICO');
            AL(L,I, '118', '394', '277', 'N', ' ', 'B', '        ', '-02', '               ', 'USO DO BANCO        ', 'USO DO BANCO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '000001         ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          1: // Detalhe
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '1              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO TRANSA��O');
            AL(L,I, '002', '003', '002', 'N', '0', 'I', '        ', '400', '               ', 'C�DIGO DE INSCRI��O ', 'TIPO DE INSCRI��O DA EMPRESA');
            AL(L,I, '004', '017', '014', 'N', '0', 'I', '        ', '401', '               ', 'N�MERO DE INSCRI��O ', 'N� DE INSCRI��O DA EMPRESA (CPF/CNPJ)');
            AL(L,I, '018', '018', '001', 'N', '0', 'Z', '        ', '-01', '               ', 'ZEROS               ', 'ZERO');
            AL(L,I, '019', '022', '004', 'N', '0', 'I', '        ', '020', '               ', 'AG�NCIA             ', 'C�DIGO DA AG�NCIA ONDE O CLIENTE MANT�M CONTA');
            AL(L,I, '023', '024', '002', 'N', '0', 'I', '        ', '035', '55             ', 'SUB-CONTA           ', 'SUB-CONTA DA CONTA CORRENTE DO CLIENTE');
            AL(L,I, '025', '035', '011', 'N', '0', 'I', '        ', '037', '               ', 'CONTA CORRENTE CLIEN', 'Exemplo: Ag�ncia 4321; Conta 56789-00 Campo Conta Corrente ent�o ser�: 43215678900');
            AL(L,I, '036', '037', '002', 'N', ' ', 'B', '        ', '-02', '               ', 'USO DO BANCO        ', 'USO DO BANCO');
            AL(L,I, '038', '062', '025', 'N', ' ', 'X', '        ', '506', '               ', 'CONTROLE DO PARTICIP', 'IDENTIFICA��O DO T�TULO NO SISTEMA DO CLIENTE');
            AL(L,I, '063', '073', '011', 'N', '0', 'I', '        ', '501', '               ', 'NOSSO N�MERO        ', 'IDENTIFICA��O DO T�TULO NO BANCO');
            AL(L,I, '074', '079', '006', 'N', '0', 'D', 'DDMMAA  ', '595', '               ', 'DATA DO DESCONTO 2  ', 'DATA LIMITE PARA O DESCONTO 2');
            AL(L,I, '080', '090', '011', 'N', '0', 'F', '2       ', '596', '               ', 'VALOR DO DESCONTO 2 ', 'VALOR DO DESCONTO 2 (FIXO OU AO DIA)');
            AL(L,I, '091', '096', '006', 'N', '0', 'D', 'DDMMAA  ', '598', '               ', 'DATA DO DESCONTO 3  ', 'DATA LIMITE PARA O DESCONTO 3');
            AL(L,I, '097', '107', '011', 'N', '0', 'F', '2       ', '599', '               ', 'VALOR DO DESCONTO 3 ', 'VALOR DO DESCONTO 3 (FIXO OU AO DIA)');
            AL(L,I, '108', '108', '001', 'N', '0', 'I', '        ', '509', '               ', 'CARTEIRA DE COBRANCA', 'IDENTIFICA O TIPO DA CARTEIRA DE COBRANCA');
            AL(L,I, '109', '110', '002', 'N', '0', 'I', '        ', '504', '01             ', 'C�D. DA OCORR�NCIA  ', 'IDENTIFICA��O DA OCORR�NCIA');
            AL(L,I, '111', '120', '010', 'N', ' ', 'X', '        ', '502', '               ', 'N� DO DOCUMENTO     ', 'N� DO DOCUMENTO DE COBRAN�A (DUPL.,NP ETC.)');
            AL(L,I, '121', '126', '006', 'N', '9', 'D', 'DDMMAA  ', '580', '               ', 'VENCIMENTO          ', 'DATA DE VENCIMENTO DO T�TULO');
            AL(L,I, '127', '139', '013', 'N', '0', 'F', '2       ', '550', '               ', 'VALOR DO T�TULO     ', 'VALOR NOMINAL DO T�TULO');
            AL(L,I, '140', '142', '003', 'N', '0', 'I', '        ', '027', '399            ', 'C�DIGO DO BANCO     ', 'N� DO BANCO NA C�MARA DE COMPENSA��O');
            AL(L,I, '143', '147', '005', 'N', '0', 'I', '        ', '025', '000            ', 'AG�NCIA COBRADORA   ', 'AG�NCIA ONDE O T�TULO SER� COBRADO');
            AL(L,I, '148', '149', '002', 'N', ' ', 'X', '        ', '507', '               ', 'ESP�CIE             ', 'ESP�CIE DO T�TULO');
            AL(L,I, '150', '150', '001', 'N', ' ', 'X', '        ', '520', '               ', 'ACEITE              ', 'IDENTIFICA��O DE T�TULO ACEITO OU N�O ACEITO');
            AL(L,I, '151', '156', '006', 'N', '0', 'D', 'DDMMAA  ', '583', '               ', 'DATA DE EMISS�O     ', 'DATA DA EMISS�O DO T�TULO');
            AL(L,I, '157', '158', '002', 'N', ' ', 'X', '        ', '701', '               ', 'INSTRU��O 1         ', '1� INSTRU��O DE COBRAN�A');
            AL(L,I, '159', '160', '002', 'N', ' ', 'X', '        ', '702', '               ', 'INSTRU��O 2         ', '2� INSTRU��O DE COBRAN�A');
            AL(L,I, '161', '173', '013', 'N', '0', 'F', '2       ', '572', '               ', 'JUROS DE 1 DIA      ', 'VALOR DE MORA POR DIA DE ATRASO');
            AL(L,I, '174', '179', '006', 'N', '0', 'D', 'DDMMAA  ', '592', '               ', 'DESCONTO AT�        ', 'DATA LIMITE PARA CONCESS�O DE DESCONTO');
            AL(L,I, '180', '192', '013', 'N', '0', 'F', '2       ', '593', '               ', 'VALOR DO DESCONTO   ', 'VALOR DO DESCONTO A SER CONCEDIDO');
            // Campo que o banco burla o CNAB para permitir ao cliente cobrar multa do sacado!
            AL(L,I, '193', '205', '013', 'N', '0', 'F', '2       ', '569', '               ', 'VALOR DO I.O.F.     ', 'VALOR DO I.O.F. A SER RECOLHIDO PELO BANCO');
            // Campo que o banco burla o CNAB para permitir ao cliente cobrar multa do sacado!
            AL(L,I, '206', '218', '013', 'N', '0', 'C', '2       ', '551', '               ', 'ABATIMENTO          ', 'VALOR DO ABATIMENTO A SER CONCEDIDO');
            AL(L,I, '219', '220', '002', 'N', '0', 'I', '        ', '801', '               ', 'C�DIGO DE INSCRI��O ', 'IDENTIFICA��O DO TIPO DE INSCRI��O/SACADO');
            AL(L,I, '221', '234', '014', 'N', '0', 'I', '        ', '802', '               ', 'N�MERO DE INSCRI��O ', 'N� DE INSCRI��O DO SACADO  (CPF/CNPJ)');
            AL(L,I, '235', '274', '040', 'S', ' ', 'X', '        ', '803', '               ', 'NOME                ', 'NOME DO SACADO');
            AL(L,I, '275', '312', '038', 'S', ' ', 'X', '        ', '804', '               ', 'LOGRADOURO          ', 'RUA, N�MERO E COMPLEMENTO DO SACADO');
            AL(L,I, '313', '314', '002', 'N', '0', 'C', '        ', '720', '               ', 'INSTR. N�O RECEBIMEN', 'INSTRU��O DE N�O RECEBIMENTO');
            AL(L,I, '315', '326', '012', 'S', ' ', 'X', '        ', '805', '               ', 'BAIRRO              ', 'BAIRRO DO SACADO');
            AL(L,I, '327', '334', '008', 'N', '0', 'I', '        ', '806', '               ', 'CEP                 ', 'CEP DO SACADO');
            AL(L,I, '335', '349', '015', 'S', ' ', 'X', '        ', '807', '               ', 'CIDADE              ', 'CIDADE DO SACADO');
            AL(L,I, '350', '351', '002', 'N', ' ', 'X', '        ', '808', '               ', 'ESTADO              ', 'UF DO SACADO');
            AL(L,I, '352', '390', '039', 'S', ' ', 'X', '        ', '853', '               ', 'SACADOR/AVALISTA    ', 'NOME DO SACADOR OU AVALISTA');
            AL(L,I, '391', '391', '001', 'N', ' ', 'X', '        ', '665', '               ', 'TIPO DE BLOQUETO    ', 'TIPO DE BLOQUETO UTILIZADO');
            AL(L,I, '392', '393', '002', 'N', ' ', 'X', '        ', '647', '               ', 'PRAZO DE PROTESTO   ', 'DIAS PARA PROTESTO');
            AL(L,I, '394', '394', '001', 'N', ' ', 'X', '        ', '549', '               ', 'TIPO DE MOEDA       ', 'C�DIGO DA MOEDA');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N� SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
          9: // Registro Trailer
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '012', '9              ', 'TIPO DE REGISTRO    ', 'IDENTIFICA��O DO REGISTRO TRAILER');
            AL(L,I, '002', '394', '393', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'COMPLEMENTO DO REGISTRO');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N�MERO SEQ�ENCIAL DO REGISTRO NO ARQUIVO');
            Result := cresOKField;
          end;
        end;
      end;
    end;
    756:  // com registro
    begin
      Result := cresNoLayoutName;
      if (NomeLayout = CO_756_EXCEL_2012_03_14) or (NomeLayout = CO_756_EXCEL_2013_07_18) then
      begin
        Result := cresNoField;
        case Registro of
          0: // Header de arquivo
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '010', '0              ', 'TIPO DE REGISTRO    ', 'Identifica��o do Registro Header: �0� (zero)');
            AL(L,I, '002', '002', '001', 'N', '0', 'I', '        ', '005', '1              ', 'OPERA��O            ', 'Tipo de Opera��o: �1� (um) ');
            AL(L,I, '003', '009', '007', 'N', ' ', 'X', '        ', '006', 'REMESSA        ', 'LITERAL DE REMESSA  ', 'Identifica��o por Extenso do Tipo de Opera��o: "REMESSA"');
            AL(L,I, '010', '011', '002', 'N', '0', 'I', '        ', '003', '01             ', 'C�DIGO DO SERVI�O   ', 'Identifica��o do Tipo de Servi�o: �01� (um) ');
            AL(L,I, '012', '019', '008', 'N', ' ', 'X', '        ', '004', 'COBRANCA       ', 'LITERAL DE SERVI�O  ', 'Identifica��o por Extenso do Tipo de Servi�o: �COBRAN�A�');
            AL(L,I, '020', '026', '007', 'N', '0', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'Complemento do Registro: Brancos');
            AL(L,I, '027', '030', '004', 'N', '0', 'I', '        ', '020', '               ', 'COOPERATIVA         ', 'Prefixo da Cooperativa: vide planilha "Capa" deste arquivo');
            AL(L,I, '031', '031', '001', 'N', '0', 'I', '        ', '022', '               ', 'DV COOPERATIVA      ', 'D�gito Verificador do Prefixo: vide planilha "Capa" deste arquivo');
            AL(L,I, '032', '039', '008', 'N', '0', 'I', '        ', '410', '               ', 'C�D. CLIENTE/CEDENTE', 'C�digo do Cliente/Cedente: vide planilha "Capa" deste arquivo');
            AL(L,I, '040', '040', '001', 'N', '0', 'I', '        ', '411', '               ', 'DV DO CEDENTE       ', 'D�gito Verificador do C�digo: vide planilha "Capa" deste arquivo');
            AL(L,I, '041', '046', '006', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'N�mero do conv�nio l�der: Brancos');
            AL(L,I, '047', '076', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DO CLIENTE     ', 'Nome do Cedente: vide planilha "Capa" deste arquivo');
            AL(L,I, '077', '079', '003', 'N', '0', 'I', '        ', '001', '756            ', 'C�DIGO DO BANCO     ', 'Identifica��o do Banco: "756"');
            AL(L,I, '080', '094', '015', 'N', ' ', 'X', '        ', '002', 'BANCOOBCED     ', 'NOME DO BANCO       ', 'Identifica��o do Banco: "BANCOOBCED"');
            AL(L,I, '095', '100', '006', 'N', '0', 'D', 'DDMMAA  ', '990', '               ', 'DATA DE GERA��O     ', 'Data da Grava��o da Remessa: formato ddmmaa ');
            AL(L,I, '101', '107', '007', 'N', '0', 'I', '        ', '998', '               ', 'SEQUENCIAL REMESSA  ', 'Seq�encial da Remessa: n�mero seq�encial acrescido de 1 a cada remessa. Inicia com "0000001"');
            AL(L,I, '108', '394', '287', 'N', ' ', 'B', '        ', '-02', '               ', 'USO DO BANCO        ', 'Complemento do Registro: Brancos');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '000001         ', 'N�MERO SEQ�ENCIAL   ', 'Seq�encial do Registro:�000001�');
            Result := cresOKField;
          end;
          1: // Detalhe
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '               ', '                    ', 'Identifica��o do Registro Detalhe: 1 (um)');
            AL(L,I, '002', '003', '002', 'N', '0', 'I', '        ', '400', '               ', '                    ', '"Tipo de Inscri��o do Cedente: ""01"" = CPF ""02"" = CNPJ  "');
            AL(L,I, '004', '017', '014', 'N', '0', 'I', '        ', '401', '               ', '                    ', 'N�mero do CPF/CNPJ do Cedente');
            AL(L,I, '018', '021', '004', 'N', '0', 'I', '        ', '020', '               ', '                    ', 'Prefixo da Cooperativa: vide planilha "Capa" deste arquivo');
            AL(L,I, '022', '022', '001', 'N', '0', 'I', '        ', '022', '               ', '                    ', 'D�gito Verificador do Prefixo: vide planilha "Capa" deste arquivo');
            AL(L,I, '023', '030', '008', 'N', '0', 'I', '        ', '021', '               ', '                    ', 'Conta Corrente: vide planilha "Capa" deste arquivo');
            AL(L,I, '031', '031', '001', 'N', '0', 'I', '        ', '023', '               ', '                    ', 'D�gito Verificador da Conta: vide planilha "Capa" deste arquivo');
            AL(L,I, '032', '037', '006', 'N', '0', 'I', '        ', '-01', '               ', '                    ', 'N�mero do Conv�nio de Cobran�a do Cedente: "000000"');
            AL(L,I, '038', '062', '025', 'N', ' ', 'B', '        ', '-02', '               ', '                    ', 'N�mero de Controle do Participante: Brancos');
            AL(L,I, '063', '073', '011', 'N', '0', 'I', '        ', '501', '               ', '                    ', 'Nosso N�mero');
            AL(L,I, '074', '074', '001', 'N', '0', 'I', '        ', '511', '               ', '                    ', 'DV do Nosso N�mero');
            AL(L,I, '075', '076', '002', 'N', '0', 'I', '        ', '513', '01             ', '                    ', 'N�mero da Parcela: "01" se parcela �nica');
            AL(L,I, '077', '078', '002', 'N', '0', 'I', '        ', '-01', '               ', '                    ', 'Grupo de Valor: "00"');
            AL(L,I, '079', '081', '003', 'N', ' ', 'B', '        ', '-02', '               ', '                    ', 'Complemento do Registro: Brancos');
            AL(L,I, '082', '082', '001', 'N', ' ', 'X', '        ', '643', '               ', '                    ', 'Indicativo de Mensagem ou Sacador/Avalista:');
            AL(L,I, '083', '085', '003', 'N', ' ', 'B', '        ', '-02', '               ', '                    ', 'Prefixo do T�tulo: Brancos');
            AL(L,I, '086', '088', '003', 'N', '0', 'I', '        ', '-01', '               ', '                    ', 'Varia��o da Carteira: "000"');
            AL(L,I, '089', '089', '001', 'N', '0', 'I', '        ', '-01', '               ', '                    ', 'Conta Cau��o: "0"');
            AL(L,I, '090', '094', '005', 'N', '0', 'I', '        ', '514', '               ', '                    ', 'N�mero do Contrato Garantia');
            AL(L,I, '095', '095', '001', 'N', ' ', 'I', '        ', '515', '               ', '                    ', 'DV do contrato');
            AL(L,I, '096', '101', '006', 'N', '0', 'I', '        ', '516', '               ', '                    ', 'Numero do border�: preencher em caso de carteira 3');
            AL(L,I, '102', '106', '005', 'N', ' ', 'B', '        ', '-02', '               ', '                    ', 'Complemento do Registro: Brancos');
            AL(L,I, '107', '108', '002', 'N', '0', 'I', '        ', '509', '               ', '                    ', 'Carteira/Modalidade');
            AL(L,I, '109', '110', '002', 'N', '0', 'I', '        ', '034', '               ', '                    ', 'Comando/Movimento');
            AL(L,I, '111', '120', '010', 'N', '0', 'I', '        ', '506', '               ', '                    ', 'Seu N�mero/N�mero atribu�do pela Empresa');
            AL(L,I, '121', '126', '006', 'N', '0', 'D', 'DDMMAA  ', '580', '               ', '                    ', 'Data Vencimento: formato ddmmaa');
            AL(L,I, '127', '139', '013', 'N', '0', 'F', '2       ', '550', '               ', '                    ', 'Valor do Titulo');
            AL(L,I, '140', '142', '003', 'N', '0', 'I', '        ', '001', '756            ', '                    ', 'N�mero Banco: "756"');
            AL(L,I, '143', '146', '004', 'N', '0', 'I', '        ', '020', '               ', '                    ', 'Prefixo da Cooperativa: vide planilha "Capa" deste arquivo');
            AL(L,I, '147', '147', '001', 'N', ' ', 'X', '        ', '022', '               ', '                    ', 'D�gito Verificador do Prefixo: vide planilha "Capa" deste arquivo');
            AL(L,I, '148', '149', '002', 'N', '0', 'I', '        ', '507', '               ', '                    ', 'Esp�cie do T�tulo');
            AL(L,I, '150', '150', '001', 'N', ' ', 'X', '        ', '520', '               ', '                    ', 'Aceite do T�tulo');
            AL(L,I, '151', '156', '006', 'N', '0', 'D', 'DDMMYY  ', '583', '               ', '                    ', 'Data de Emiss�o do T�tulo: formato ddmmaa');
            AL(L,I, '157', '158', '002', 'N', '0', 'I', '        ', '701', '               ', '                    ', 'Primeira instru��o codificada');
            AL(L,I, '159', '160', '002', 'N', '0', 'I', '        ', '702', '               ', '                    ', 'Segunda instru��o codificada');
            AL(L,I, '161', '166', '006', 'N', '0', 'F', '4       ', '574', '               ', '                    ', 'Taxa de mora m�s');
            AL(L,I, '167', '172', '006', 'N', '0', 'F', '4       ', '575', '               ', '                    ', 'Taxa de multa');
            AL(L,I, '173', '173', '001', 'N', '0', 'I', '        ', '627', '               ', '                    ', 'Tipo Distribui��o');
            AL(L,I, '174', '179', '006', 'N', '0', 'D', 'DDMMYY  ', '592', '               ', '                    ', 'Data primeiro desconto');
            AL(L,I, '180', '192', '013', 'N', '0', 'F', '2       ', '593', '               ', '                    ', 'Valor primeiro desconto');
            AL(L,I, '193', '193', '001', 'N', '0', 'I', '        ', '549', '               ', '                    ', 'C�digo da moeda');
            AL(L,I, '194', '205', '012', 'N', '0', 'F', '2       ', '569', '               ', '                    ', 'Valor IOF ou Quantidade Monet�ria');
            AL(L,I, '206', '218', '013', 'N', '0', 'F', '2       ', '551', '               ', '                    ', 'Valor Abatimento');
            AL(L,I, '219', '220', '002', 'N', '0', 'I', '        ', '801', '               ', '                    ', 'Tipo de Inscri��o do Sacado');
            AL(L,I, '221', '234', '014', 'N', '0', 'I', '        ', '802', '               ', '                    ', 'N�mero do CNPJ ou CPF do Sacado');
            AL(L,I, '235', '274', '040', 'S', ' ', 'X', '        ', '803', '               ', '                    ', 'Nome do Sacado');
            AL(L,I, '275', '311', '037', 'S', ' ', 'X', '        ', '804', '               ', '                    ', 'Endere�o do Sacado');
            AL(L,I, '312', '326', '015', 'S', ' ', 'X', '        ', '805', '               ', '                    ', 'Bairro do Sacado');
            AL(L,I, '327', '334', '008', 'N', '0', 'I', '        ', '806', '               ', '                    ', 'CEP do Sacado');
            AL(L,I, '335', '349', '015', 'S', ' ', 'X', '        ', '807', '               ', '                    ', 'Cidade do Sacado');
            AL(L,I, '350', '351', '002', 'N', ' ', 'X', '        ', '808', '               ', '                    ', 'UF do Sacado');
            AL(L,I, '352', '391', '040', 'S', ' ', 'X', '        ', '650', '               ', '                    ', 'Observa��es/Mensagem ou Sacador/Avalista');
            AL(L,I, '392', '393', '002', 'N', ' ', 'I', '        ', '647', '               ', '                    ', 'N�mero de Dias Para Protesto');
            AL(L,I, '394', '394', '001', 'N', ' ', 'B', '        ', '-02', '               ', '                    ', 'Complemento do Registro: Brancos');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', '                    ', 'Seq�encial do Registro: Incrementado em 1 a cada registro');
          end;
          9: // Registro Trailer 
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '012', '9              ', '                    ', 'Identifica��o Registro Trailler: "9"');
            AL(L,I, '002', '194', '193', 'S', ' ', 'B', '        ', '-02', '               ', '                    ', 'Complemento do Registro: Brancos');
            AL(L,I, '195', '234', '040', 'S', ' ', 'X', '        ', '201', '               ', '                    ', 'Mensagem responsabilidade Cedente 1');
            AL(L,I, '235', '274', '040', 'S', ' ', 'X', '        ', '202', '               ', '                    ', 'Mensagem responsabilidade Cedente 2');
            AL(L,I, '275', '314', '040', 'S', ' ', 'X', '        ', '203', '               ', '                    ', 'Mensagem responsabilidade Cedente 3');
            AL(L,I, '315', '354', '040', 'S', ' ', 'X', '        ', '204', '               ', '                    ', 'Mensagem responsabilidade Cedente 4');
            AL(L,I, '355', '394', '040', 'S', ' ', 'X', '        ', '205', '               ', '                    ', 'Mensagem responsabilidade Cedente 5');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'Seq�encial do Registro: Incrementado em 1 a cada registro');
            Result := cresOKField;
          end;
        end;
      end else
      if (NomeLayout = CO_756_CORRESPONDENTE_BRADESCO_2015) then
      begin
        Result := cresNoField;
        case Registro of
          0: // Header de arquivo
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '010', '0              ', 'TIPO DE REGISTRO    ', 'Identifica��o do Registro Header: �0� (zero)');
            AL(L,I, '002', '002', '001', 'N', '0', 'I', '        ', '005', '1              ', 'OPERA��O            ', 'Tipo de Opera��o: �1� (um) ');
            AL(L,I, '003', '009', '007', 'N', ' ', 'X', '        ', '006', 'REMESSA        ', 'LITERAL DE REMESSA  ', 'Identifica��o por Extenso do Tipo de Opera��o: "REMESSA"');
            AL(L,I, '010', '011', '002', 'N', '0', 'I', '        ', '003', '01             ', 'C�DIGO DO SERVI�O   ', 'Identifica��o do Tipo de Servi�o: �01� (um) ');
            AL(L,I, '012', '026', '015', 'N', ' ', 'X', '        ', '004', 'COBRANCA       ', 'LITERAL DE SERVI�O  ', 'Identifica��o por Extenso do Tipo de Servi�o: �COBRAN�A�');
            AL(L,I, '027', '039', '013', 'N', '0', 'I', '        ', '020', '               ', 'COOPERATIVA         ', 'Cooperativa');
            AL(L,I, '040', '046', '007', 'N', '0', 'I', '        ', '410', '               ', 'C�D. COBRAN�A       ', 'C�digo do Cliente/Cedente');
            AL(L,I, '047', '076', '030', 'S', ' ', 'X', '        ', '402', '               ', 'NOME DO CLIENTE     ', 'Nome do Cedente: vide planilha "Capa" deste arquivo');
            AL(L,I, '077', '079', '003', 'N', '0', 'I', '        ', '001', '756            ', 'C�DIGO DO BANCO     ', 'Identifica��o do Banco: "756"');
            AL(L,I, '080', '094', '015', 'N', ' ', 'X', '        ', '002', 'BANCOOB        ', 'NOME DO BANCO       ', 'Identifica��o do Banco: "BANCOOB"');
            AL(L,I, '095', '100', '006', 'N', '0', 'D', 'DDMMAA  ', '990', '               ', 'DATA DE GERA��O     ', 'Data da Grava��o da Remessa: formato ddmmaa ');
            AL(L,I, '101', '108', '008', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'Brancos');
            AL(L,I, '109', '110', '002', 'N', ' ', 'X', '        ', '099', 'SX             ', 'IDENTIFIC. SISTEMA  ', 'IDENTIFICA��O DO SISTEMA');
            AL(L,I, '111', '117', '007', 'N', '0', 'I', '        ', '998', '               ', 'SEQUENCIAL REMESSA  ', 'Seq�encial da Remessa: n�mero seq�encial acrescido de 1 a cada remessa. Inicia com "0000001"');
            AL(L,I, '118', '394', '277', 'N', ' ', 'B', '        ', '-02', '               ', 'USO DO BANCO        ', 'Complemento do Registro: Brancos');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '000001         ', 'N�MERO SEQ�ENCIAL   ', 'Seq�encial do Registro:�000001�');
            Result := cresOKField;
          end;
          1: // Detalhe
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '               ', '                    ', 'Identifica��o do Registro Detalhe: 1 (um)');
            AL(L,I, '002', '020', '019', 'N', '0', 'I', '        ', '-01', '               ', '                    ', 'Zeros: "000000"');
            AL(L,I, '021', '030', '010', 'N', '0', 'I', '        ', '020', '               ', '                    ', 'Cooperativa');
            AL(L,I, '031', '037', '007', 'N', '0', 'I', '        ', '410', '               ', 'C�D. COBRAN�A       ', 'C�digo do Cliente/Cedente');
            AL(L,I, '038', '062', '025', 'N', ' ', 'B', '        ', '-02', '               ', '                    ', 'N�mero de Controle do Participante: Brancos');
            AL(L,I, '063', '070', '008', 'N', '0', 'I', '        ', '-01', '               ', '                    ', 'Zeros: "000000"');
            AL(L,I, '071', '081', '011', 'N', '0', 'I', '        ', '501', '               ', '                    ', 'Nosso N�mero');
            AL(L,I, '082', '082', '001', 'N', '0', 'X', '        ', '511', '               ', '                    ', 'DV do Nosso N�mero');
            AL(L,I, '083', '092', '010', 'N', '0', 'F', '2       ', '558', '               ', 'DESC.BONIFIC.POR DIA', 'Valor de desconto bonifica��o por dia');
            AL(L,I, '093', '093', '001', 'N', '0', 'I', '        ', '621', '               ', 'IMPRESSAO DO BOLETO ', '1 - Banco emite e processa a papeleta; 2 = O banco somente processa');
            AL(L,I, '094', '094', '001', 'N', ' ', 'X', '        ', '622', '              N', 'COND.D�B.AUTOM�TICO ', 'N - N�o emite para d�bito autom�tico');
            AL(L,I, '095', '108', '014', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'Preencher com brancos');
            AL(L,I, '109', '110', '002', 'N', '0', 'I', '        ', '504', '               ', 'C�D. DE OCORR�NCIA  ', 'C�digo da ocorr�ncia');
            AL(L,I, '111', '120', '010', 'N', ' ', 'X', '        ', '502', '               ', 'N� DO DOCUMENTO     ', 'N� do documento de cobran�a (Dupl.,NP, Etc.)');
            AL(L,I, '121', '126', '006', 'N', '0', 'D', 'DDMMAA  ', '580', '               ', '                    ', 'Data Vencimento: formato ddmmaa');
            AL(L,I, '127', '139', '013', 'N', '0', 'F', '2       ', '550', '               ', '                    ', 'Valor do Titulo');
            AL(L,I, '140', '147', '008', 'N', '0', 'I', '        ', '-01', '               ', '                    ', 'Zeros: "000000"');
            AL(L,I, '148', '149', '002', 'N', '0', 'I', '        ', '507', '               ', '                    ', 'Esp�cie do T�tulo');
            AL(L,I, '150', '150', '001', 'N', ' ', 'X', '        ', '520', '               ', '                    ', 'Aceite do T�tulo');
            AL(L,I, '151', '156', '006', 'N', '0', 'D', 'DDMMYY  ', '583', '               ', '                    ', 'Data de Emiss�o do T�tulo: formato ddmmaa');
            AL(L,I, '157', '158', '002', 'N', '0', 'I', '        ', '701', '               ', '                    ', 'Primeira instru��o codificada');
            AL(L,I, '159', '160', '002', 'N', '0', 'I', '        ', '702', '               ', '                    ', 'Segunda instru��o codificada');
            AL(L,I, '161', '173', '013', 'N', '0', 'F', '2       ', '572', '               ', 'JUROS DE 1 DIA      ', 'Valor de mora por dia de atraso');
            AL(L,I, '174', '179', '006', 'N', '0', 'D', 'DDMMYY  ', '586', '               ', 'DESCONTO AT�        ', 'Data limite para concess�o de desconto');
            AL(L,I, '180', '192', '013', 'N', '0', 'F', '2       ', '552', '               ', 'VALOR DO DESCONTO   ', 'Valor do desconto a ser concedido');
            AL(L,I, '193', '205', '013', 'N', '0', 'I', '        ', '-01', '               ', '                    ', 'Zeros: "000000"');
            AL(L,I, '206', '218', '013', 'N', '0', 'F', '2       ', '551', '               ', '                    ', 'Valor Abatimento');
            AL(L,I, '219', '220', '002', 'N', '0', 'I', '        ', '801', '               ', '                    ', 'Tipo de Inscri��o do Sacado');
            AL(L,I, '221', '234', '014', 'N', '0', 'I', '        ', '802', '               ', '                    ', 'N�mero do CNPJ ou CPF do Sacado');
            AL(L,I, '235', '274', '040', 'S', ' ', 'X', '        ', '803', '               ', '                    ', 'Nome do Sacado');
            AL(L,I, '275', '314', '040', 'S', ' ', 'X', '        ', '804', '               ', 'ENDERE�O DO SACADO  ', 'Endere�o completo do sacado');
            AL(L,I, '315', '326', '012', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'Preencher com brancos');
            AL(L,I, '327', '334', '008', 'N', '0', 'I', '        ', '806', '               ', '                    ', 'CEP do Sacado');
            AL(L,I, '335', '335', '001', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'Preencher com brancos');
            AL(L,I, '336', '349', '014', 'S', '0', 'I', '        ', '852', '               ', 'N�MERO DE INSCRI��O ', 'N�mero de inscri��o do sacador avalista');
            AL(L,I, '350', '351', '002', 'S', '0', 'I', '        ', '851', '               ', 'C�DIGO DE INSCRI��O ', 'Ident. do tipo de inscri��o do sacador/avalista');
            AL(L,I, '352', '394', '043', 'S', ' ', 'X', '        ', '853', '               ', 'SACADOR/AVALISTA    ', 'Nome do sacador ou avalista');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', '                    ', 'Seq�encial do Registro: Incrementado em 1 a cada registro');
          end;
          2: // Registro de mensagens (Opcional)
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '011', '2              ', 'TIPO DE REGISTRO    ', 'Identifica��o do registro detalhe - Mensagem (Opcional)');
            AL(L,I, '002', '081', '080', 'S', ' ', 'X', '        ', '201', '               ', 'MENSAGEM 1          ', 'Mensagem com 80 posi��es');
            AL(L,I, '082', '161', '080', 'S', ' ', 'X', '        ', '202', '               ', 'MENSAGEM 2          ', 'Mensagem com 80 posi��es');
            AL(L,I, '162', '241', '080', 'S', ' ', 'X', '        ', '203', '               ', 'MENSAGEM 3          ', 'Mensagem com 80 posi��es');
            AL(L,I, '242', '321', '080', 'S', ' ', 'X', '        ', '204', '               ', 'MENSAGEM 4          ', 'Mensagem com 80 posi��es');
            AL(L,I, '322', '366', '045', 'S', ' ', 'B', '        ', '-02', '               ', '                    ', 'Complemento do Registro: Brancos');
            AL(L,I, '367', '369', '003', 'S', '0', 'I', '        ', '508', '               ', 'C�DIGO DA CARTEIRA  ', 'C�digo da carteira');
            AL(L,I, '370', '394', '025', 'N', '0', 'I', '        ', '-01', '               ', '                    ', 'Zeros: "000000"');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'Seq�encial do Registro: Incrementado em 1 a cada registro');
            Result := cresOKField;
          end;
          9: // Registro Trailer
          begin
          //AL(L,I, 'INI', 'FIM', 'TAM', 'C', 'P', 'T', 'Format  ', 'FLD', 'Conteudo padr�o', 'Nome do campo       ', 'Descric�o do campo');
            AL(L,I, '001', '001', '001', 'N', '0', 'I', '        ', '012', '9              ', 'TIPO DE REGISTRO    ', 'Identifica��o do registro trailer');
            AL(L,I, '002', '394', '393', 'N', ' ', 'B', '        ', '-02', '               ', 'BRANCOS             ', 'Brancos');
            AL(L,I, '395', '400', '006', 'N', '0', 'I', '        ', '999', '               ', 'N�MERO SEQ�ENCIAL   ', 'N�mero sequencial do registro no arquivo');
            Result := cresOKField;
          end;
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.LayoutsRemessa(const Banco, CNAB: Integer): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    008, 033, 353: //
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, CO_033_RECEBIMENTOS_240_v01_07_2009_06, ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_033_RECEBIMENTOS_240_v01_07_2009_06]);
        end;
      end;
      case CNAB of
        400:
        begin
          AddLin(Result, Linha, CO_033_RECEBIMENTOS_400_v02_00_2009_10, ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_033_RECEBIMENTOS_400_v02_00_2009_10]);
        end;
      end;
    end;
    237: //
    begin
      case CNAB of
        400:
        begin
          AddLin(Result, Linha, CO_237_MP_4008_0121_01_Data_11_11_2010, ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_237_MP_4008_0121_01_Data_11_11_2010]);
        end;
      end;
    end;
    341: //
    begin
      case CNAB of
        400:
        begin
          AddLin(Result, Linha, CO_341_COBRANCA_BANCARIA_MARCO_2012,    ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_341_COBRANCA_BANCARIA_MARCO_2012]);
          AddLin(Result, Linha, CO_341_COBRANCA_BANCARIA_SETEMBRO_2012, ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_341_COBRANCA_BANCARIA_SETEMBRO_2012]);
        end;
      end;
    end;
    399: //
    begin
      case CNAB of
        400:
        begin
          AddLin(Result, Linha, CO_399_MODULO_I_2009_10, ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_399_MODULO_I_2009_10]);
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        240:
        begin
          AddLin(Result, Linha, CO_756_CORRESPONDENTE_BB_2015, ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_756_CORRESPONDENTE_BB_2015]);
          AddLin(Result, Linha, CO_756_240_2018,               ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_756_240_2018]);
        end;
        400:
        begin
          AddLin(Result, Linha, CO_756_EXCEL_2012_03_14,             ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_756_EXCEL_2012_03_14]);
          AddLin(Result, Linha, CO_756_EXCEL_2013_07_18,             ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_756_EXCEL_2013_07_18]);
          AddLin(Result, Linha, CO_756_CORRESPONDENTE_BRADESCO_2015, ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_756_CORRESPONDENTE_BRADESCO_2015]);
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.LayoutsRetorno(const Banco, CNAB: Integer): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case banco of
    399: //
    begin
      case CNAB of
        400:
        begin
          AddLin(Result, Linha, CO_399_MODULO_I_2009_10, ['COBRAN�A REGISTRADA: MANUAL T�CNICO � M�DULO I - CNAB 400 - Vers�o: OUTUBRO / 2009']);
        end;
      end;
    end;
    756:  // com registro!!!
    begin
      case CNAB of
        400:
        begin
          AddLin(Result, Linha, CO_756_EXCEL_2012_03_14,             ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_756_EXCEL_2012_03_14]);
          AddLin(Result, Linha, CO_756_EXCEL_2013_07_18,             ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_756_EXCEL_2013_07_18]);
          AddLin(Result, Linha, CO_756_CORRESPONDENTE_BRADESCO_2015, ['COBRAN�A REGISTRADA: MANUAL T�CNICO - ' + CO_756_CORRESPONDENTE_BRADESCO_2015]);
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.ObtemProximoNossoNumero(CNAB_Cfg: Integer;
   AvisaCNABNaoImplementado: Boolean): Double;
var
  //Tam,
  MaxLen, Banco, Agencia, CNAB, TipoCart, ModalCobr: Integer;
  //Num,
  BloqAtual: Double;
  Conta: String;
begin
  Result := 0;
  {
  Result := DModG.BuscaProximoCodigoInt('CNAB_Cfg', 'LastNosNum',
  'WHERE Codigo='+FormatFloat('0', CNAB_Cfg), 0);
  }
  //
  DmBco.QrCNAB_Cfg.Close;
  DmBco.QrCNAB_Cfg.Params[0].AsInteger := CNAB_Cfg;
  UnDmkDAC_PF.AbreQuery(DmBco.QrCNAB_Cfg, Dmod.MyDB);
  if DmBco.QrCNAB_Cfg.RecordCount = 0 then
  begin
    Geral.MB_Aviso('A configura��o CNAB sob o n�mero de cadastro '
      + Geral.FF0(CNAB_Cfg) + ' n�o foi localizada!');
    Exit;
  end;
  Banco     := DmBco.QrCNAB_CfgCedBanco.Value;
  Agencia   := DmBco.QrCNAB_CfgCedAgencia.Value;
  Conta     := DmBco.QrCNAB_CfgCedConta.Value;
  CNAB      := DmBco.QrCNAB_CfgCNAB.Value;
  TipoCart  := DmBco.QrCNAB_CfgTipoCart.Value;
  BloqAtual := DmBco.QrCNAB_CfgLastNosNum.Value;
  ModalCobr := DmBco.QrCNAB_CfgModalCobr.Value;
  //
  DmBco.QrAux.Close;
  DmBco.QrAux.SQL.Clear;
  DmBco.QrAux.SQL.Add('LOCK TABLES cnab_cfg WRITE');
  DmBco.QrAux.ExecSQL;
  //
  UBancos.GeraBloquetoNumero(Banco, BloqAtual, True,
    Geral.IntToBool(DmBco.QrCNAB_CfgNosNumFxaU.Value),
    DmBco.QrCNAB_CfgNosNumFxaI.Value, DmBco.QrCNAB_CfgNosNumFxaF.Value,
    BloqAtual, DmBco.QrCNAB_CfgLayoutRem.Value);
  //
  DmBco.QrAux.SQL.Clear;
  DmBco.QrAux.SQL.Add('UPDATE cnab_cfg SET LastNosNum=:P0');
  DmBco.QrAux.SQL.Add('WHERE CedBanco=:P1 AND CedAgencia=:P2 ');
  DmBco.QrAux.SQL.Add('AND CedConta=:P3 AND ModalCobr=:P4 ');
  DmBco.QrAux.Params[00].AsFloat   := BloqAtual;
  DmBco.QrAux.Params[01].AsInteger := Banco;
  DmBco.QrAux.Params[02].AsInteger := Agencia;
  DmBco.QrAux.Params[03].AsString  := Conta;
  DmBco.QrAux.Params[04].AsInteger := ModalCobr;
  DmBco.QrAux.ExecSQL;
  //
  {
  DmBco.QrAux.SQL.Clear;
  DmBco.QrAux.SQL.Add('SELECT ' + Campo + ' FROM ' + Lowercase(TabNome));
  DmBco.QrAux.SQL.Add(ComplSQL);
  DmBco.QrAux . O p e n ;
  }
  //
  DmBco.QrAux.Close;
  DmBco.QrAux.SQL.Clear;
  DmBco.QrAux.SQL.Add('UNLOCK TABLES ');
  DmBco.QrAux.ExecSQL;
  //
  Result := BloqAtual;
  //
  Maxlen := 0;
  case Banco of
    237:
    case CNAB of
      400:
      case TipoCart of
        0: MaxLen := 11;
      end;
    end;
    341:
    case CNAB of
      400:
      case TipoCart of
        0: MaxLen := 8;
      end;
    end;
    409:
    case CNAB of
      400:
      case TipoCart of
        0: MaxLen := 14;
      end;
    end;
  end;
  if MaxLen = 0 then
  begin
    if AvisaCNABNaoImplementado then
      Geral.MB_Aviso('O banco ' + FormatFloat('000', Banco) +
        ' n�o est� implementado na fun��o "UnBco_Rem.ObtemProximoNossoNumero".');
    // deixa o n�mero igual, mas pode dar problema se for maior que o m�ximo
  end else begin
    //if Tam > MaxLen then Num := 1;
  end;
end;

function TUBco_Rem.TipoDeBloqutoUsado(const Banco, CNAB: Integer): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer;
  const Codigo, Descricao, Explicacao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    Res[Linha][2] :=  Explicacao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  case Banco of
    399: // com registro!!!
    begin
      case CNAB of
        400:
        begin
          AddLinha(Result, Linha, '', '', '');
          AddLinha(Result, Linha, 'S', 'Assume bloqueto "Auto-Envelopado"',  'OBS.: Uso exclusivo para clientes que utilizam Emiss�o Banco');
        end;
      end;
    end;
  end;
end;

function TUBco_Rem.TipoDeCarteiraRemessa(const Banco: Integer; const Tipo:
String; var TipoCart: Integer): Boolean;
{-2 n�o alterar RGTipocart -1: nenhum ou desconhecido 0: Sem registro 1: Escritural 2: Direta }
begin
  Result := True;
  case Banco of
    001: // com registro
    begin
      TipoCart := -2;
    end;
    008, 033, 353: // Com registro
    begin
      TipoCart := -1; // n�o sei ainda
    end;
    237: // com registro
    begin
      // Banco n�o informa no manual
      TipoCart := -1;
      Result := False;
    end;
    341: // com registro
    begin
      TipoCart := -1;
      if Tipo = 'S' then TipoCart := 0 else
      if Tipo = 'E' then TipoCart := 1 else
      if Tipo = 'D' then TipoCart := 2;
    end;
    399: // com registro
    begin
      TipoCart := 1;
    end;
    409: // sem registro
    begin
      //if Tipo = '20' then TipoCart := 0 else
      TipoCart := -2;
    end;
    756: // com registro
    begin
      TipoCart := -1; // ver o que fazer! Tem "Simples" e "Garantida"
    end
    else begin
      TipoCart := -1;
      Result := False;
      Geral.MB_Aviso('O banco ' + FormatFloat('000', Banco) +
      ' n�o est� implementado para "UBco_Rem.TipoDeCarteiraRemessa".');
    end;
  end;
end;

function TUBco_Rem.SelecionaItensCarteira(Lista: MyArrayLista;
EdTipoCart: TdmkEdit; EdCartNum, EdCartCod, EdCartTxt, EdDescri: TEdit): Boolean;
var
  Tipo: Integer;
begin
  Result := False;
{
Res[Linha][0] := Codigo;
Res[Linha][1] := Carteira;
Res[Linha][2] := CartTxt
Res[Linha][3] := Tipo; // E-Escritural S-Simples D-Direta
Res[Linha][4] := Descricao;
Res[Linha][5] := Coment;
}
  if Geral.CriaSelLista(Lista, 'Sele��o de Carteira de Cobran�a', ['C�D.',
  'TIPO', 'CARTEIRAS ID', 'CARTEIRAS', 'DESCRI��O', 'OBS'], Screen.Width) then
  begin
    if FmdmkSelLista.FResult > -1 then
    begin
      if EdCartCod <> nil then
        EdCartCod.Text := Lista[FmdmkSelLista.FResult][0];
      if EdCartNum <> nil then
        EdCartNum.Text := Lista[FmdmkSelLista.FResult][1];
      if EdCartTxt <> nil then
        EdCartTxt.Text := Lista[FmdmkSelLista.FResult][2];
      if EdTipoCart <> nil then
      begin
        UBco_Rem.TipoDeCarteiraRemessa(341, Lista[FmdmkSelLista.FResult][3], Tipo);
        EdTipoCart.ValueVariant := Tipo;
      end;
      if EdDescri <> nil then
        EdDescri.Text := Lista[FmdmkSelLista.FResult][4];
      Result := True;
    end;
    FmdmkSelLista.Destroy;
  end;
end;

end.
