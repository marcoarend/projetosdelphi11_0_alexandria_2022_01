unit LeDataArq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, RichEdit, ComCtrls, Menus, DmkDAC_PF,
  Db, mySQLDbTables, dmkEdit, dmkGeral, Variants, UnDMkEnums;

type
  TFmLeDataArq = class(TForm)
    PainelConfirma: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PMAcoes: TPopupMenu;
    EliminarodigitoverificadordoNOSSONMERO1: TMenuItem;
    QrField: TmySQLQuery;
    TabSheet2: TTabSheet;
    MeRes: TRichEdit;
    QrFieldPadrIni: TIntegerField;
    QrFieldPadrTam: TIntegerField;
    Salvararquivogeradocomo1: TMenuItem;
    SaveDialog1: TSaveDialog;
    TabSheet3: TTabSheet;
    Panel8: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    SB_A: TSpeedButton;
    SB_B: TSpeedButton;
    Label20: TLabel;
    EditA: TEdit;
    Button1: TButton;
    EditB: TEdit;
    EdTamLin: TdmkEdit;
    MemoA: TRichEdit;
    MemoB: TRichEdit;
    MemoC: TRichEdit;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    SBArq1: TSpeedButton;
    EdArq1: TEdit;
    Editor1: TRichEdit;
    TabSheet5: TTabSheet;
    Panel4: TPanel;
    Label5: TLabel;
    SBArq2: TSpeedButton;
    EdArq2: TEdit;
    Editor2: TRichEdit;
    Panel5: TPanel;
    Label6: TLabel;
    Label4: TLabel;
    Edit4: TdmkEdit;
    Edit3: TdmkEdit;
    Label3: TLabel;
    Label2: TLabel;
    Edit2: TdmkEdit;
    BtAcoes: TBitBtn;
    StatusBar: TStatusBar;
    Panel6: TPanel;
    EdPesq1: TEdit;
    EdPesq2: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    BtSubstitui: TButton;
    PMSubstitui: TPopupMenu;
    N121: TMenuItem;
    N211: TMenuItem;
    RgQuebraLinha2: TRadioGroup;
    RGQuebraLinha1: TRadioGroup;
    Label9: TLabel;
    EdQtdLinDif: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SBArq1Click(Sender: TObject);
    procedure Editor1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Editor1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Editor1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure EliminarodigitoverificadordoNOSSONMERO1Click(
      Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAcoesClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure Salvararquivogeradocomo1Click(Sender: TObject);
    procedure PMAcoesPopup(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SB_AClick(Sender: TObject);
    procedure SB_BClick(Sender: TObject);
    procedure EdArq1Change(Sender: TObject);
    procedure EdArq2Change(Sender: TObject);
    procedure SBArq2Click(Sender: TObject);
    procedure Editor2KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Editor2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Editor2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtSubstituiClick(Sender: TObject);
    procedure N121Click(Sender: TObject);
    procedure N211Click(Sender: TObject);
  private
    { Private declarations }
    function RichRow(m: TCustomMemo): Longint;
    function RichCol(m: TCustomMemo): Longint;
    procedure PesquisaTexto();
    procedure SubtituiPesquisa(Source, Dest: Integer);

  public
    { Public declarations }
  end;

  var
  FmLeDataArq: TFmLeDataArq;

implementation

{$R *.DFM}

uses UnMyObjects, Module;

resourcestring
  sSaveChanges = 'Salva as altera��es de %s?';
  sOverWrite = 'OK para sobrescrever %s';
  sUntitled = 'Sem t�tulo';
  sModified = 'Modificado';
  sColRowInfo = 'Linha: %3d   Coluna: %3d';
  sPergunta = 'Pergunta';

const
  RulerAdj = 4/3;
  GutterWid = 6;

  ENGLISH = (SUBLANG_ENGLISH_US shl 10) or LANG_ENGLISH;
  FRENCH  = (SUBLANG_FRENCH shl 10) or LANG_FRENCH;
  GERMAN  = (SUBLANG_GERMAN shl 10) or LANG_GERMAN;


procedure TFmLeDataArq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLeDataArq.Button1Click(Sender: TObject);
var
  fEntrada: file;
  NumRead, i, j, n, l: integer;
  buf, x: char;
begin
  MemoA.Lines.Clear;
  MemoB.Lines.Clear;
  MemoC.Lines.Clear;
  //
  AssignFile(fEntrada, EditA.Text);
  Reset(fEntrada, 1);
  for i := 1 to FileSize(fEntrada) do
  begin
    Blockread(fEntrada, buf, 1, NumRead);
    j := Ord(Buf);
    if J > 31 then
      x := Buf
    else
      x := ' ';
    n  := i;
    l  := 1;
    while n > EdTamLin.ValueVariant do
    begin
      l := L + 1;
      n := n - EdTamLin.ValueVariant;
    end;
    MemoA.Lines.Add('Lin ' + FormatFloat('000', l) + ' Pos ' +
      FormatFloat('000', n) + ' > "' + x + '" = ' + FormatFloat('000', Ord(buf)));
  end;
  CloseFile(fEntrada);
  //
  AssignFile(fEntrada, EditB.Text);
  Reset(fEntrada, 1);
  for i := 1 to FileSize(fEntrada) do
  begin
    Blockread(fEntrada, buf, 1, NumRead);
    j := Ord(Buf);
    if J > 31 then
      x := Buf
    else
      x := ' ';
    n  := i;
    l  := 1;
    while n > EdTamLin.ValueVariant do
    begin
      l := L + 1;
      n := n - EdTamLin.ValueVariant;
    end;
    MemoB.Lines.Add('Lin ' + FormatFloat('000', l) + ' Pos ' +
      FormatFloat('000', n) + ' > "' + x + '" = ' + FormatFloat('000', Ord(buf)));
  end;
  CloseFile(fEntrada);
  //
  if MemoA.Lines.Count > MemoB.Lines.Count then
    i := MemoA.Lines.Count
  else
    i := MemoB.Lines.Count;
  for j := 0 to i do
  begin
    if MemoA.Lines[j] <> MemoB.Lines[j] then
      MemoC.Lines.Add(MemoA.Lines[j] + ' :: ' + MemoB.Lines[j]);
  end;
  EdQtdLinDif.ValueVariant := MemoC.Lines.Count;
  if MemoC.Lines.Count = 0 then
    Geral.MensagemBox('Nenhum caracter divergente foi encontrado!',
    'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmLeDataArq.BtSubstituiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSubstitui, BtSubstitui);
end;

procedure TFmLeDataArq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLeDataArq.N121Click(Sender: TObject);
begin
  SubtituiPesquisa(1, 2);
end;

procedure TFmLeDataArq.N211Click(Sender: TObject);
begin
  SubtituiPesquisa(2, 1);
end;

procedure TFmLeDataArq.SBArq1Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdArq1.Text);
  Arquivo := ExtractFileName(EdArq1.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo "1"', '', [], Arquivo) then
  begin
    Editor1.Lines.Clear;
    Editor1.Lines.LoadFromFile(Arquivo);
    EdArq1.Text := Arquivo;
  end;
end;

procedure TFmLeDataArq.SBArq2Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdArq2.Text);
  Arquivo := ExtractFileName(EdArq2.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo "2"', '', [], Arquivo) then
  begin
    Editor2.Lines.Clear;
    Editor2.Lines.LoadFromFile(Arquivo);
    EdArq2.Text := Arquivo;
  end;
end;

procedure TFmLeDataArq.SubtituiPesquisa(Source, Dest: Integer);
var
  //fEntrada: file;
  //NumRead,
  i(*, j, n, l*): integer;
  //buf, x: char;
 Texto: String;
  //
  nLin, nCol, nTam, Quebra: Integer;
begin
{
  case Dest of
    1: Arquivo := EdArq1.Text;
    2: Arquivo := EdArq2.Text;
    else Arquivo := '';
  end;
  //
  AssignFile(fEntrada, Arquivo);
  Reset(fEntrada, 1);
  for i := 1 to FileSize(fEntrada) do
  begin
    Blockread(fEntrada, buf, 1, NumRead);
  end;
  CloseFile(fEntrada);
  //
}
  nLin := Edit2.ValueVariant;
  nCol := Edit3.ValueVariant;
  nTam := Edit4.ValueVariant;
  case Dest of
    1: Texto := Editor1.Lines[nLin-1];
    2: Texto := Editor2.Lines[nLin-1];
  end;
  for i := nCol to nCol + nTam -1 do
  begin
    case Dest of
      1: Texto[i] := Editor2.Lines[nLin-1][i];
      2: Texto[i] := Editor1.Lines[nLin-1][i] ;
    end;
  end;
  case Dest of
    1:
    begin
      Editor1.Lines[nLin-1] := Texto;
      Quebra := Geral.IMV(RGQuebraLinha1.Items[RGQuebraLinha1.ItemIndex]);
      MyObjects.ExportaMemoToFileExt(TMemo(Editor1), EdArq1.Text, True, False, etxtsaSemAcento, Quebra, Null);
      SBArq1Click(Self);
    end;
    2:
    begin
      Editor2.Lines[nLin-1] := Texto;
      Quebra := Geral.IMV(RGQuebraLinha2.Items[RGQuebraLinha2.ItemIndex]);
      MyObjects.ExportaMemoToFileExt(TMemo(Editor2), EdArq2.Text, True, False, etxtsaSemAcento, Quebra, Null);
      SBArq2Click(Self);
    end;
  end;
end;

procedure TFmLeDataArq.SB_AClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EditA.Text);
  Arquivo := ExtractFileName(EditA.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo "A"', '', [], Arquivo) then
    EditA.Text := Arquivo;
end;

procedure TFmLeDataArq.SB_BClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EditB.Text);
  Arquivo := ExtractFileName(EditB.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo "B"', '', [], Arquivo) then
    EditB.Text := Arquivo;
end;

function TFmLeDataArq.RichRow(m: TCustomMemo): Longint;
begin
  Result := SendMessage(m.Handle, EM_LINEFROMCHAR, m.SelStart, 0);
end;

function TFmLeDataArq.RichCol(m: TCustomMemo): Longint;
begin
  Result := m.SelStart - SendMessage(m.Handle, EM_LINEINDEX, SendMessage(m.Handle,
    EM_LINEFROMCHAR, m.SelStart, 0), 0);
end;

procedure TFmLeDataArq.Editor1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  StatusBar.Panels[0].Text := Format(sColRowInfo, [RichRow(Editor1)+1, RichCol(Editor1)+1]);
end;

procedure TFmLeDataArq.Editor1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  StatusBar.Panels[0].Text := Format(sColRowInfo, [RichRow(Editor1)+1, RichCol(Editor1)+1]);
end;

procedure TFmLeDataArq.Editor1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  StatusBar.Panels[0].Text := Format(sColRowInfo, [RichRow(Editor1)+1, RichCol(Editor1)+1]);
end;

procedure TFmLeDataArq.Editor2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  StatusBar.Panels[1].Text := Format(sColRowInfo, [RichRow(Editor2)+1, RichCol(Editor2)+1]);
end;

procedure TFmLeDataArq.Editor2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  StatusBar.Panels[1].Text := Format(sColRowInfo, [RichRow(Editor2)+1, RichCol(Editor2)+1]);
end;

procedure TFmLeDataArq.Editor2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  StatusBar.Panels[1].Text := Format(sColRowInfo, [RichRow(Editor2)+1, RichCol(Editor2)+1]);
end;

procedure TFmLeDataArq.EdArq1Change(Sender: TObject);
begin
  PesquisaTexto();
end;

procedure TFmLeDataArq.EdArq2Change(Sender: TObject);
begin
  PesquisaTexto();
end;

procedure TFmLeDataArq.Edit2Change(Sender: TObject);
begin
  PesquisaTexto();
end;

procedure TFmLeDataArq.Edit3Change(Sender: TObject);
begin
  PesquisaTexto();
end;

procedure TFmLeDataArq.Edit4Change(Sender: TObject);
begin
  PesquisaTexto();
end;

procedure TFmLeDataArq.PesquisaTexto();
var
  Lin, Col, Tam: Integer;
  Memo1, Memo2: TRichEdit;
  //Texto1, Texto2: WideString;
begin
  Lin := Geral.IMV(Edit2.Text);
  Col := Geral.IMV(Edit3.Text);
  Tam := Geral.IMV(Edit4.Text);
  //
  Memo1 := nil;
  Memo2 := nil;
  case PageControl1.ActivePageIndex of
    0:
    begin
      Memo1 := Editor1;
      Memo2 := Editor2;
    end;
    1:
    begin
      Memo1 := MeRes;
      Memo2 := nil;
    end;
  end;
  EdPesq1.Text := '';
  EdPesq2.Text := '';
  if (Lin > 0) and (Col > 0) and (Tam > 0) then
  begin
    if Memo1 <> nil then EdPesq1.Text := Copy(Memo1.Lines[Lin-1], Col, Tam);
    if Memo2 <> nil then EdPesq2.Text := Copy(Memo2.Lines[Lin-1], Col, Tam);
  end;
end;

procedure TFmLeDataArq.FormCreate(Sender: TObject);
begin
  Pagecontrol1.ActivePageIndex := 0;
  Pagecontrol2.ActivePageIndex := 0;
end;

procedure TFmLeDataArq.EliminarodigitoverificadordoNOSSONMERO1Click(
  Sender: TObject);
var
  Banco, Parte1, Parte2, Parte3, Posicoes: String;
  i, Tam1, Tam2, Tam3, Ini2, Ini3, DetIni, DetTam: Integer;
begin
  Banco := '';
  if InputQuery('Elimina��o de D�gito Verificador',
    'Informe o banco gerador do arquivo:', Banco) then
  begin
    Posicoes := FormatFloat('000', Length(Editor1.Lines[1]));
    QrField.Close;
    QrField.SQL.Clear;
    QrField.SQL.Add('SELECT cad.PadrIni, PadrTam');
    QrField.SQL.Add('FROM cnab_cad cad');
    QrField.SQL.Add('LEFT JOIN cnab_cag cag ON cag.Codigo=cad.Registro');
    QrField.SQL.Add('WHERE cag.ID=1');
    QrField.SQL.Add('AND cad.Campo=11'); // registro detalhe
    QrField.SQL.Add('AND cad.BcoOrig=:P0');
    QrField.SQL.Add('AND cad.T' + Posicoes +'=1');
    QrField.Params[0].AsInteger := StrToInt(Banco); // gerar erro de prop�sito
    UnDmkDAC_PF.AbreQuery(QrField, Dmod.MyDB);
    //
    if QrField.RecordCount > 0 then
    begin
      DetIni := QrFieldPadrIni.Value;
      DetTam := QrFieldPadrTam.Value;
    end else begin
      Geral.MensagemBox('N�o foi localizada nenhuma ' +
      'configura��o v�lida para a identifica��o de registro detalhe para ' +
      'o Banco "' + Banco + '" (' + Posicoes + ' posi��es)!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //

    QrField.Close;
    QrField.SQL.Clear;
    QrField.SQL.Add('SELECT cad.PadrIni, PadrTam');
    QrField.SQL.Add('FROM cnab_cad cad');
    QrField.SQL.Add('LEFT JOIN cnab_cag cag ON cag.Codigo=cad.Registro');
    QrField.SQL.Add('WHERE cag.ID=1');
    QrField.SQL.Add('AND cad.Campo=501');
    QrField.SQL.Add('AND cad.BcoOrig=:P0');
    QrField.SQL.Add('AND cad.T' + Posicoes + '=1');
    QrField.Params[0].AsInteger := StrToInt(Banco); // gerar erro de prop�sito
    UnDmkDAC_PF.AbreQuery(QrField, Dmod.MyDB);
    //
    if QrField.RecordCount > 0 then
    begin
      Screen.Cursor := crHourGlass;
      MeRes.Lines.Clear;
      for i := 0 to Editor1.Lines.Count - 1 do
      begin
        if Length(Editor1.Lines[i]) > 0 then
        begin
          if Geral.IMV(Copy(Editor1.Lines[i], DetIni, DetTam)) = 1 then // 1 = Registro detalhe
          begin
            Tam1 := QrFieldPadrIni.Value-1;
            Tam2 := QrFieldPadrTam.Value-1;
            Ini2 := QrFieldPadrIni.Value;
            Tam3 := Length(Editor1.Lines[i]);
            Ini3 := QrFieldPadrIni.Value + QrFieldPadrTam.Value;
            Parte1 := Copy(Editor1.Lines[i], 1   , Tam1);
            Parte2 := Copy(Editor1.Lines[i], Ini2, Tam2);
            Parte3 := Copy(Editor1.Lines[i], Ini3, Tam3);
            MeRes.Lines.Add(Parte1 + '0' + Parte2 + Parte3);
          end else MeRes.Lines.Add(Editor1.Lines[i]);
        end;
      end;
      Screen.Cursor := crDefault;
    end else Geral.MensagemBox('N�o foi localizada nenhuma ' +
    'configura��o v�lida para o NOSSO N�MERO para o Banco "' + Banco +
    '" (' + Posicoes + ' posi��es)!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmLeDataArq.BtAcoesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcoes, BtAcoes);
end;

procedure TFmLeDataArq.PageControl1Change(Sender: TObject);
begin
  PesquisaTexto();
end;

procedure TFmLeDataArq.PMAcoesPopup(Sender: TObject);
begin
  Salvararquivogeradocomo1.Enabled := PageControl1.ActivePageIndex = 1;
end;

procedure TFmLeDataArq.Salvararquivogeradocomo1Click(Sender: TObject);
begin
  if SaveDialog1.Execute then
    MyObjects.ExportaMemoToFile(TMemo(MeRes), SaveDialog1.FileName, True, False, True);
end;

end.

