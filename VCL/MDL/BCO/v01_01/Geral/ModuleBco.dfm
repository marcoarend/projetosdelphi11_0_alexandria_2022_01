object DmBco: TDmBco
  OnCreate = DataModuleCreate
  Height = 500
  Width = 870
  PixelsPerInch = 96
  object QrCNAB_Cfg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cnab_cfg'
      'WHERE Codigo=:P0'
      '')
    Left = 24
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_CfgLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cnab_cfg.Lk'
    end
    object QrCNAB_CfgDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cnab_cfg.DataCad'
    end
    object QrCNAB_CfgDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cnab_cfg.DataAlt'
    end
    object QrCNAB_CfgUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cnab_cfg.UserCad'
    end
    object QrCNAB_CfgUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cnab_cfg.UserAlt'
    end
    object QrCNAB_CfgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'cnab_cfg.AlterWeb'
    end
    object QrCNAB_CfgAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cnab_cfg.Ativo'
    end
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_cfg.Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrCNAB_CfgCedBanco: TIntegerField
      FieldName = 'CedBanco'
      Origin = 'cnab_cfg.CedBanco'
    end
    object QrCNAB_CfgCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
      Origin = 'cnab_cfg.CedAgencia'
    end
    object QrCNAB_CfgCedConta: TWideStringField
      FieldName = 'CedConta'
      Origin = 'cnab_cfg.CedConta'
      Size = 30
    end
    object QrCNAB_CfgCedNome: TWideStringField
      FieldName = 'CedNome'
      Origin = 'cnab_cfg.CedNome'
      Size = 100
    end
    object QrCNAB_CfgTipoCart: TSmallintField
      FieldName = 'TipoCart'
      Origin = 'cnab_cfg.TipoCart'
    end
    object QrCNAB_CfgCartNum: TWideStringField
      FieldName = 'CartNum'
      Origin = 'cnab_cfg.CartNum'
      Size = 3
    end
    object QrCNAB_CfgCartCod: TWideStringField
      FieldName = 'CartCod'
      Origin = 'cnab_cfg.CartCod'
      Size = 1
    end
    object QrCNAB_CfgEspecieTit: TWideStringField
      FieldName = 'EspecieTit'
      Origin = 'cnab_cfg.EspecieTit'
      Size = 2
    end
    object QrCNAB_CfgAceiteTit: TSmallintField
      FieldName = 'AceiteTit'
      Origin = 'cnab_cfg.AceiteTit'
    end
    object QrCNAB_CfgInstrCobr1: TWideStringField
      FieldName = 'InstrCobr1'
      Origin = 'cnab_cfg.InstrCobr1'
      Size = 2
    end
    object QrCNAB_CfgInstrCobr2: TWideStringField
      FieldName = 'InstrCobr2'
      Origin = 'cnab_cfg.InstrCobr2'
      Size = 2
    end
    object QrCNAB_CfgInstrDias: TSmallintField
      FieldName = 'InstrDias'
      Origin = 'cnab_cfg.InstrDias'
    end
    object QrCNAB_CfgCedente: TIntegerField
      FieldName = 'Cedente'
      Origin = 'cnab_cfg.Cedente'
    end
    object QrCNAB_CfgSacadAvali: TIntegerField
      FieldName = 'SacadAvali'
      Origin = 'cnab_cfg.SacadAvali'
    end
    object QrCNAB_CfgSacAvaNome: TWideStringField
      FieldName = 'SacAvaNome'
      Origin = 'cnab_cfg.SacAvaNome'
      Size = 100
    end
    object QrCNAB_CfgCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
      Origin = 'cnab_cfg.CodEmprBco'
    end
    object QrCNAB_CfgMultaTipo: TSmallintField
      FieldName = 'MultaTipo'
      Origin = 'cnab_cfg.MultaTipo'
    end
    object QrCNAB_CfgMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'cnab_cfg.MultaPerc'
    end
    object QrCNAB_CfgTexto01: TWideStringField
      FieldName = 'Texto01'
      Origin = 'cnab_cfg.Texto01'
      Size = 100
    end
    object QrCNAB_CfgTexto02: TWideStringField
      FieldName = 'Texto02'
      Origin = 'cnab_cfg.Texto02'
      Size = 100
    end
    object QrCNAB_CfgTexto03: TWideStringField
      FieldName = 'Texto03'
      Origin = 'cnab_cfg.Texto03'
      Size = 100
    end
    object QrCNAB_CfgTexto04: TWideStringField
      FieldName = 'Texto04'
      Origin = 'cnab_cfg.Texto04'
      Size = 100
    end
    object QrCNAB_CfgTexto05: TWideStringField
      FieldName = 'Texto05'
      Origin = 'cnab_cfg.Texto05'
      Size = 100
    end
    object QrCNAB_CfgTexto06: TWideStringField
      FieldName = 'Texto06'
      Origin = 'cnab_cfg.Texto06'
      Size = 100
    end
    object QrCNAB_CfgTexto07: TWideStringField
      FieldName = 'Texto07'
      Origin = 'cnab_cfg.Texto07'
      Size = 100
    end
    object QrCNAB_CfgTexto08: TWideStringField
      FieldName = 'Texto08'
      Origin = 'cnab_cfg.Texto08'
      Size = 100
    end
    object QrCNAB_CfgTexto09: TWideStringField
      FieldName = 'Texto09'
      Origin = 'cnab_cfg.Texto09'
      Size = 100
    end
    object QrCNAB_CfgTexto10: TWideStringField
      FieldName = 'Texto10'
      Origin = 'cnab_cfg.Texto10'
      Size = 100
    end
    object QrCNAB_CfgJurosPerc: TFloatField
      FieldName = 'JurosPerc'
      Origin = 'cnab_cfg.JurosPerc'
    end
    object QrCNAB_CfgJurosDias: TSmallintField
      FieldName = 'JurosDias'
      Origin = 'cnab_cfg.JurosDias'
    end
    object QrCNAB_CfgJurosTipo: TSmallintField
      FieldName = 'JurosTipo'
      Origin = 'cnab_cfg.JurosTipo'
    end
    object QrCNAB_CfgCNAB: TIntegerField
      FieldName = 'CNAB'
      Origin = 'cnab_cfg.CNAB'
    end
    object QrCNAB_CfgCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Origin = 'cnab_cfg.CedDAC_A'
      Size = 1
    end
    object QrCNAB_CfgCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Origin = 'cnab_cfg.CedDAC_C'
      Size = 1
    end
    object QrCNAB_CfgCedDAC_AC: TWideStringField
      FieldName = 'CedDAC_AC'
      Origin = 'cnab_cfg.CedDAC_AC'
      Size = 1
    end
    object QrCNAB_CfgEnvEmeio: TSmallintField
      FieldName = 'EnvEmeio'
      Origin = 'cnab_cfg.EnvEmeio'
    end
    object QrCNAB_CfgDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Origin = 'cnab_cfg.Diretorio'
      Size = 255
    end
    object QrCNAB_Cfg_237Mens1: TWideStringField
      FieldName = '_237Mens1'
      Origin = 'cnab_cfg._237Mens1'
      Size = 12
    end
    object QrCNAB_Cfg_237Mens2: TWideStringField
      FieldName = '_237Mens2'
      Origin = 'cnab_cfg._237Mens2'
      Size = 60
    end
    object QrCNAB_CfgCodOculto: TWideStringField
      FieldName = 'CodOculto'
      Origin = 'cnab_cfg.CodOculto'
    end
    object QrCNAB_CfgSeqArq: TIntegerField
      FieldName = 'SeqArq'
      Origin = 'cnab_cfg.SeqArq'
    end
    object QrCNAB_CfgCedPosto: TIntegerField
      FieldName = 'CedPosto'
      Origin = 'cnab_cfg.CedPosto'
    end
    object QrCNAB_CfgLastNosNum: TLargeintField
      FieldName = 'LastNosNum'
      Origin = 'cnab_cfg.LastNosNum'
    end
    object QrCNAB_CfgLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Origin = 'cnab_cfg.LocalPag'
      Size = 127
    end
    object QrCNAB_CfgEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Origin = 'cnab_cfg.EspecieVal'
      Size = 5
    end
    object QrCNAB_CfgOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Origin = 'cnab_cfg.OperCodi'
      Size = 3
    end
    object QrCNAB_CfgIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Origin = 'cnab_cfg.IDCobranca'
      Size = 2
    end
    object QrCNAB_CfgAgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Origin = 'cnab_cfg.AgContaCed'
      Size = 40
    end
    object QrCNAB_CfgDirRetorno: TWideStringField
      FieldName = 'DirRetorno'
      Origin = 'cnab_cfg.DirRetorno'
      Size = 255
    end
    object QrCNAB_CfgCartRetorno: TIntegerField
      FieldName = 'CartRetorno'
      Origin = 'cnab_cfg.CartRetorno'
    end
    object QrCNAB_CfgPosicoesBB: TSmallintField
      FieldName = 'PosicoesBB'
      Origin = 'cnab_cfg.PosicoesBB'
    end
    object QrCNAB_CfgIndicatBB: TWideStringField
      FieldName = 'IndicatBB'
      Origin = 'cnab_cfg.IndicatBB'
      Size = 1
    end
    object QrCNAB_CfgTipoCobrBB: TWideStringField
      FieldName = 'TipoCobrBB'
      Origin = 'cnab_cfg.TipoCobrBB'
      Size = 5
    end
    object QrCNAB_CfgDdProtesBB: TIntegerField
      FieldName = 'DdProtesBB'
      Origin = 'cnab_cfg.DdProtesBB'
    end
    object QrCNAB_CfgMsg40posBB: TWideStringField
      FieldName = 'Msg40posBB'
      Origin = 'cnab_cfg.Msg40posBB'
      Size = 40
    end
    object QrCNAB_CfgComando: TIntegerField
      FieldName = 'Comando'
      Origin = 'cnab_cfg.Comando'
    end
    object QrCNAB_CfgQuemPrint: TWideStringField
      FieldName = 'QuemPrint'
      Origin = 'cnab_cfg.QuemPrint'
      Size = 1
    end
    object QrCNAB_CfgQuemDistrb: TWideStringField
      FieldName = 'QuemDistrb'
      Origin = 'cnab_cfg.QuemDistrb'
      Size = 1
    end
    object QrCNAB_CfgConvCartCobr: TWideStringField
      FieldName = 'ConvCartCobr'
      Origin = 'cnab_cfg.ConvCartCobr'
      Size = 2
    end
    object QrCNAB_CfgConvVariCart: TWideStringField
      FieldName = 'ConvVariCart'
      Origin = 'cnab_cfg.ConvVariCart'
      Size = 3
    end
    object QrCNAB_CfgCodLidrBco: TWideStringField
      FieldName = 'CodLidrBco'
      Origin = 'cnab_cfg.CodLidrBco'
    end
    object QrCNAB_CfgVariacao: TIntegerField
      FieldName = 'Variacao'
      Origin = 'cnab_cfg.Variacao'
    end
    object QrCNAB_CfgTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
      Origin = 'cnab_cfg.TipoCobranca'
    end
    object QrCNAB_CfgProtesCod: TSmallintField
      FieldName = 'ProtesCod'
      Origin = 'cnab_cfg.ProtesCod'
    end
    object QrCNAB_CfgProtesDds: TSmallintField
      FieldName = 'ProtesDds'
      Origin = 'cnab_cfg.ProtesDds'
    end
    object QrCNAB_CfgEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
      Origin = 'cnab_cfg.EspecieDoc'
    end
    object QrCNAB_CfgEspecieTxt: TWideStringField
      FieldName = 'EspecieTxt'
      Origin = 'cnab_cfg.EspecieTxt'
    end
    object QrCNAB_CfgCartTxt: TWideStringField
      FieldName = 'CartTxt'
      Origin = 'cnab_cfg.CartTxt'
      Size = 10
    end
    object QrCNAB_CfgLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Origin = 'cnab_cfg.LayoutRem'
      Size = 50
    end
    object QrCNAB_CfgLayoutRet: TWideStringField
      FieldName = 'LayoutRet'
      Origin = 'cnab_cfg.LayoutRet'
      Size = 50
    end
    object QrCNAB_CfgMultaDias: TSmallintField
      FieldName = 'MultaDias'
      Origin = 'cnab_cfg.MultaDias'
    end
    object QrCNAB_CfgNaoRecebDd: TSmallintField
      FieldName = 'NaoRecebDd'
      Origin = 'cnab_cfg.NaoRecebDd'
    end
    object QrCNAB_CfgTipBloqUso: TWideStringField
      FieldName = 'TipBloqUso'
      Origin = 'cnab_cfg.TipBloqUso'
      Size = 1
    end
    object QrCNAB_CfgDesco1Cod: TSmallintField
      FieldName = 'Desco1Cod'
      Origin = 'cnab_cfg.Desco1Cod'
    end
    object QrCNAB_CfgDesco1Dds: TIntegerField
      FieldName = 'Desco1Dds'
      Origin = 'cnab_cfg.Desco1Dds'
    end
    object QrCNAB_CfgDesco1Fat: TFloatField
      FieldName = 'Desco1Fat'
      Origin = 'cnab_cfg.Desco1Fat'
    end
    object QrCNAB_CfgDesco2Cod: TSmallintField
      FieldName = 'Desco2Cod'
      Origin = 'cnab_cfg.Desco2Cod'
    end
    object QrCNAB_CfgDesco2Dds: TIntegerField
      FieldName = 'Desco2Dds'
      Origin = 'cnab_cfg.Desco2Dds'
    end
    object QrCNAB_CfgDesco2Fat: TFloatField
      FieldName = 'Desco2Fat'
      Origin = 'cnab_cfg.Desco2Fat'
    end
    object QrCNAB_CfgDesco3Cod: TSmallintField
      FieldName = 'Desco3Cod'
      Origin = 'cnab_cfg.Desco3Cod'
    end
    object QrCNAB_CfgDesco3Dds: TIntegerField
      FieldName = 'Desco3Dds'
      Origin = 'cnab_cfg.Desco3Dds'
    end
    object QrCNAB_CfgDesco3Fat: TFloatField
      FieldName = 'Desco3Fat'
      Origin = 'cnab_cfg.Desco3Fat'
    end
    object QrCNAB_CfgConcatCod: TWideStringField
      FieldName = 'ConcatCod'
      Origin = 'cnab_cfg.ConcatCod'
      Size = 50
    end
    object QrCNAB_CfgComplmCod: TWideStringField
      FieldName = 'ComplmCod'
      Origin = 'cnab_cfg.ComplmCod'
      Size = 10
    end
    object QrCNAB_CfgNumVersaoI3: TIntegerField
      FieldName = 'NumVersaoI3'
      Origin = 'cnab_cfg.NumVersaoI3'
    end
    object QrCNAB_CfgBxaDevCod: TSmallintField
      FieldName = 'BxaDevCod'
      Origin = 'cnab_cfg.BxaDevCod'
    end
    object QrCNAB_CfgBxaDevDds: TIntegerField
      FieldName = 'BxaDevDds'
      Origin = 'cnab_cfg.BxaDevDds'
    end
    object QrCNAB_CfgMoedaCod: TWideStringField
      FieldName = 'MoedaCod'
      Origin = 'cnab_cfg.MoedaCod'
    end
    object QrCNAB_CfgContrato: TFloatField
      FieldName = 'Contrato'
      Origin = 'cnab_cfg.Contrato'
    end
    object QrCNAB_CfgInfNossoNum: TSmallintField
      FieldName = 'InfNossoNum'
      Origin = 'cnab_cfg.InfNossoNum'
    end
    object QrCNAB_CfgCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Origin = 'cnab_cfg.CartEmiss'
    end
    object QrCNAB_CfgModalCobr: TSmallintField
      FieldName = 'ModalCobr'
      Origin = 'cnab_cfg.ModalCobr'
    end
    object QrCNAB_CfgCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrCNAB_CfgNosNumFxaI: TIntegerField
      FieldName = 'NosNumFxaI'
    end
    object QrCNAB_CfgNosNumFxaF: TIntegerField
      FieldName = 'NosNumFxaF'
    end
    object QrCNAB_CfgNosNumFxaU: TSmallintField
      FieldName = 'NosNumFxaU'
    end
    object QrCNAB_CfgCtrGarantiaNr: TWideStringField
      FieldName = 'CtrGarantiaNr'
      Size = 30
    end
    object QrCNAB_CfgCtrGarantiaDV: TWideStringField
      FieldName = 'CtrGarantiaDV'
      Size = 1
    end
  end
  object QrAddr1: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrAddr1CalcFields
    SQL.Strings = (
      
        '  SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1,' +
        ' '
      '  IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEENT, '
      '  IF(en.Tipo=0, en.CNPJ       , en.CPF ) CNPJ_CPF, '
      '  IF(en.Tipo=0, en.IE         , en.RG  ) IE_RG, '
      '  IF(en.Tipo=0, en.NIRE       , ""     ) NIRE_, '
      '  IF(en.Tipo=0, en.ERua       , en.PRua) RUA, '
      '  IF(en.Tipo=0, en.ENumero    , en.PNumero) + 0.000 NUMERO, '
      '  IF(en.Tipo=0, en.ECompl     , en.PCompl) COMPL, '
      '  IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, '
      '  IF(en.Tipo=0, en.ECidade    , en.PCidade) CIDADE, '
      '  IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, '
      '  IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, '
      '  IF(en.Tipo=0, en.EPais      , en.PPais  ) Pais, '
      '  IF(en.Tipo=0, en.ELograd    , en.PLograd) + 0.000 Lograd, '
      '  IF(en.Tipo=0, en.ECEP       , en.PCEP   ) + 0.000 CEP, '
      '  IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, '
      '  IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX '
      '  FROM entidades en '
      '  LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF '
      '  LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF '
      '  LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd '
      '  LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd '
      '  WHERE en.Codigo=:P0 '
      '')
    Left = 24
    Top = 50
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAddr1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAddr1NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrAddr1Cadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrAddr1CNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrAddr1IE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrAddr1NIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrAddr1RUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrAddr1COMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrAddr1BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrAddr1CIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrAddr1NOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrAddr1NOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrAddr1Pais: TWideStringField
      FieldName = 'Pais'
    end
    object QrAddr1Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrAddr1TE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrAddr1FAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrAddr1ENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrAddr1PNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrAddr1Respons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrAddr1ECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrAddr1NUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrAddr1E_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrAddr1CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrAddr1FAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrAddr1TE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrAddr1NATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrAddr1NUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrAddr1CEP: TFloatField
      FieldName = 'CEP'
    end
    object QrAddr1Lograd: TFloatField
      FieldName = 'Lograd'
      Required = True
    end
  end
  object QrAddr2: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrAddr2CalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEENT' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END + 0.' +
        '000 NUMERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END + 0.' +
        '000 Lograd, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END + 0.' +
        '000 CEP, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 24
    Top = 94
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAddr2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAddr2Cadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrAddr2NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrAddr2CNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrAddr2IE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrAddr2NIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrAddr2RUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrAddr2COMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrAddr2BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrAddr2CIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrAddr2NOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrAddr2NOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrAddr2Pais: TWideStringField
      FieldName = 'Pais'
    end
    object QrAddr2Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrAddr2TE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrAddr2FAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrAddr2ENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrAddr2PNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrAddr2Respons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrAddr2ECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrAddr2NUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrAddr2E_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrAddr2CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrAddr2FAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrAddr2TE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrAddr2NATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrAddr2Lograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrAddr2CEP: TFloatField
      FieldName = 'CEP'
    end
    object QrAddr2NUMERO: TFloatField
      FieldName = 'NUMERO'
    end
  end
  object QrAddr3: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrAddr3CalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOMEENT,'
      'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF,'
      'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG,'
      'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_,'
      'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA,'
      'IF(en.Tipo=0, en.ENumero    , en.PNumero) + 0.000 NUMERO,'
      'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL,'
      'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO,'
      'IF(en.Tipo=0, en.ECidade    , en.PCidade) CIDADE,'
      'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD,'
      'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF,'
      'IF(en.Tipo=0, en.EPais      , en.PPais  ) Pais,'
      'IF(en.Tipo=0, en.ELograd    , en.PLograd) + 0.000 Lograd,'
      'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) + 0.000 CEP,'
      'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1,'
      'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX,'
      'IF(en.Tipo=0, en.EEMail       , en.PEMail   ) EMail'
      'FROM entidades en'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0'
      '')
    Left = 24
    Top = 138
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAddr3Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAddr3NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrAddr3Cadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrAddr3CNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrAddr3IE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrAddr3NIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrAddr3RUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrAddr3COMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrAddr3BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrAddr3CIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrAddr3NOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrAddr3NOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrAddr3Pais: TWideStringField
      FieldName = 'Pais'
    end
    object QrAddr3Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrAddr3TE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrAddr3FAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrAddr3ENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrAddr3PNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrAddr3Respons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrAddr3ECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrAddr3NUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrAddr3E_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrAddr3CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrAddr3FAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrAddr3TE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrAddr3NATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrAddr3EMail: TWideStringField
      FieldName = 'EMail'
      Size = 100
    end
    object QrAddr3NUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrAddr3Lograd: TFloatField
      FieldName = 'Lograd'
      Required = True
    end
    object QrAddr3CEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object QrCount: TMySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 140
    object QrCountItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrBco: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bancos'
      'WHERE Codigo=:P0')
    Left = 92
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBcoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBcoNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrCNPJ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM entidades'
      'WHERE CNPJ=:P0')
    Left = 92
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNPJCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 93
    Top = 5
  end
  object QrAux: TMySQLQuery
    Database = Dmod.MyDB
    Left = 24
    Top = 188
  end
  object QrBancos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM Bancos'
      'WHERE Codigo > 0')
    Left = 92
    Top = 188
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
end
