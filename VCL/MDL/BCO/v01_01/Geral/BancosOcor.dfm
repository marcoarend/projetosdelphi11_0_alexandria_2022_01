object FmBancosOcor: TFmBancosOcor
  Left = 449
  Top = 251
  Caption = 'Ocorr'#234'ncia Banc'#225'ria'
  ClientHeight = 251
  ClientWidth = 526
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 203
    Width = 526
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 414
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 526
    Height = 48
    Align = alTop
    Caption = 'Ocorr'#234'ncia Banc'#225'ria'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 442
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 448
      ExplicitHeight = 44
    end
    object LaTipo: TLabel
      Left = 443
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 450
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 526
    Height = 155
    Align = alClient
    TabOrder = 0
    object Label2: TLabel
      Left = 84
      Top = 8
      Width = 80
      Height = 13
      Caption = 'Conta vinculada:'
    end
    object BtContas: TSpeedButton
      Left = 496
      Top = 24
      Width = 23
      Height = 22
      Hint = 'Inclui conta'
      Caption = '...'
      OnClick = BtContasClick
    end
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label3: TLabel
      Left = 84
      Top = 56
      Width = 34
      Height = 13
      Caption = 'Banco:'
    end
    object Label4: TLabel
      Left = 16
      Top = 100
      Width = 110
      Height = 13
      Caption = 'Descri'#231#227'o para extrato:'
    end
    object EdConta: TdmkEditCB
      Left = 84
      Top = 24
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBConta
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 164
      Top = 24
      Width = 329
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 2
      dmkEditCB = EdConta
      UpdType = utYes
    end
    object EdOcorrencia: TEdit
      Left = 16
      Top = 24
      Width = 65
      Height = 21
      TabOrder = 0
    end
    object EdBanco: TdmkEditCB
      Left = 84
      Top = 72
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBBanco
    end
    object CBBanco: TdmkDBLookupComboBox
      Left = 164
      Top = 72
      Width = 329
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBancos
      TabOrder = 5
      dmkEditCB = EdBanco
      UpdType = utYes
    end
    object CkCarrega: TCheckBox
      Left = 16
      Top = 76
      Width = 61
      Height = 17
      Caption = 'Ativo.'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object EdNome: TEdit
      Left = 16
      Top = 116
      Width = 477
      Height = 21
      TabOrder = 6
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome'
      '')
    Left = 376
    Top = 60
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 404
    Top = 60
  end
  object QrDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM bancoslei'
      'WHERE Codigo=:P0'
      'AND Ocorrencia=:P1')
    Left = 348
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bancos'
      'WHERE Codigo>0'
      'ORDER BY Nome'
      '')
    Left = 384
    Top = 108
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 412
    Top = 108
  end
end
