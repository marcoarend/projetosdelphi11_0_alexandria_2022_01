unit SomaDiasCred;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, Grids, UnMLAGeral, dmkGeral, dmkEdit,
  dmkEditDateTimePicker, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmSomaDiasCred = class(TForm)
    PainelDados: TPanel;
    Grade: TStringGrid;
    Timer1: TTimer;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    TPDataF: TdmkEditDateTimePicker;
    EdDMais: TdmkEdit;
    EdComp: TdmkEdit;
    EdDias: TdmkEdit;
    EdDataReal: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel2: TPanel;
    BtRefresh: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure Timer1Timer(Sender: TObject);
    //procedure GradeDrawCell(Sender: TObject; ACol, ARow: Integer;
      //Rect: TRect; State: TGridDrawState);
    procedure RecriaCalendario;
    procedure TPDataIClick(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure TPDataFChange(Sender: TObject);
    procedure TPDataFClick(Sender: TObject);
    procedure EdDMaisChange(Sender: TObject);
    procedure EdCompChange(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
  private
    { Private declarations }
    //FItens: array[1..2048] of integer;
    FDiasEx: MyArrayI1k;
    FDescoDia: Integer;
    //function QueDiaEh(Index: Integer): String;
  public
    { Public declarations }
  end;

  var
  FmSomaDiasCred: TFmSomaDiasCred;

implementation

uses UnMyObjects, UMySQLModule, UnInternalConsts, Module;

{$R *.DFM}

procedure TFmSomaDiasCred.RecriaCalendario;
var
  i, c, j, z, Total, Col, Row: Integer;
  Ano, Mes, Dia: Word;
  DataI, DataF: TDateTime;
begin
  FDiasEx := UMyMod.CalculaDiasArray(Int(TPDataI.Date), Int(TPDataF.Date),
    EdDMais.ValueVariant, EdComp.ValueVariant, Dmod.QrControleTipoPrazoDesc.Value);
  Total := FDiasEx[0];
  //
  for i := 1 to Grade.ColCount-1 do
    for j := 1 to Grade.RowCount-1 do Grade.Cells[i,j] := '';
  //
  i := DayOfWeek(TPDataI.Date);
  FDescoDia := -i+1;
  DataI := TPDataI.Date+FDescoDia;
  DataF := TPDataI.Date+Total;
  Col := 1;
  Row := 1;
  c := Trunc(DataI);
  //i := Trunc(TPDataI.Date);
  //f := Trunc(TPDataF.Date);
  z := Trunc(DataF);
  j := Trunc((z-c)/7) +2;
  Grade.RowCount := j;
  for j := c to z do
  begin
    DecodeDate(j, Ano, Mes, Dia);
    //Grade.Cells[Col, Row] := IntToStr(Dia);
    Grade.Cells[Col, Row] := FormatDateTime(VAR_FORMATDATE2, j);
    inc(Col, 1);
    if Col = 8 then
    begin
      Col := 1;
      inc(Row, 1);
    end;
  end;
  EdDataReal.Text := FormatdateTime(VAR_FORMATDATE2, DataF);
  EdDias.ValueVariant := Total;
  (*Grad2.RowCount := FDiasEx[0]+1;
  for i := 1 to FDiasEx[0] do
  begin
    Grad2.Cells[0,i] := FormatDateTime(VAR_FORMATDATE2, DataI+i);
    Grad2.Cells[1,i] := IntToStr(FDiasEx[i]);
    Grad2.Cells[2,i] := QueDiaEh(FDiasEx[i]);
  end;*)
end;

(*function TFmSomaDiasCred.QueDiaEh(Index: Integer): String;
begin
  case Index of
    -1: Result := '';
    0: Result := 'No prazo';
    1..100: Result := '';
    101: Result := 'Compensando';
    102: Result := 'D+';
    201: Result := 'Domingo';
    207: Result := 'Sabado';
    else Result := '?????'
  end;
end;*)

procedure TFmSomaDiasCred.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSomaDiasCred.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  RecriaCalendario;
end;

procedure TFmSomaDiasCred.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSomaDiasCred.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  TPDataI.Date := Date;
  TPDataF.Date := Date+20;
  //
  Grade.ColWidths[00] := 050;
  (*Grade.ColWidths[01] := 028;
  Grade.ColWidths[02] := 028;
  Grade.ColWidths[03] := 028;
  Grade.ColWidths[04] := 028;
  Grade.ColWidths[05] := 028;
  Grade.ColWidths[06] := 028;
  Grade.ColWidths[07] := 028;*)
  Grade.ColWidths[01] := 070;
  Grade.ColWidths[02] := 070;
  Grade.ColWidths[03] := 070;
  Grade.ColWidths[04] := 070;
  Grade.ColWidths[05] := 070;
  Grade.ColWidths[06] := 070;
  Grade.ColWidths[07] := 070;
  Grade.ColWidths[08] := 100;
  Grade.ColWidths[09] := 036;
  //
  Grade.Cells[00,00] := 'Semana';
  Grade.Cells[01,00] := 'Dom';
  Grade.Cells[02,00] := 'Seg';
  Grade.Cells[03,00] := 'Ter';
  Grade.Cells[04,00] := 'Qua';
  Grade.Cells[05,00] := 'Qui';
  Grade.Cells[06,00] := 'Sex';
  Grade.Cells[07,00] := 'S�b';
  Grade.Cells[08,00] := 'M�s';
  Grade.Cells[09,00] := 'Ano';
  //
end;

procedure TFmSomaDiasCred.GradeDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  OldAlign, MyColor, ddd, Dia: Integer;
  Data: TDateTime;
begin
  if ARow = 0 then MyColor := clSilver else
  if ACol in ([0,8]) then MyColor := clSilver else
  begin
    Data := Geral.ValidaDataSimples(Grade.Cells[ACol, ARow], True);
    ddd := Trunc(Data);
    Dia := ddd-Trunc(TPDataI.Date(*+FDescoDia*));
    //ddd := DecodeDate(Data, Ano, Mes, Dia);
    if dia < 1 then Dia := -1 else Dia := FDiasEx[dia];
    case Dia of
      -1: MyColor := clWhite;
      //0: MyColor := clBlue;
      1..100: MyColor := $00D2FFFE; // creme
      101: MyColor := $00FF8080; // Azul
      102: MyColor := $0093DD6A;//clGreen;
      103..200: MyColor := clFuchsia;
      201: MyColor := $003737FF;//clRed;
      207: MyColor := $003737FF;//clRed;
      208: MyColor := clMaroon;
      else MyColor := clPurple;
    end;
    if Dia = 0 then
    begin
      case ACol of
        0: MyColor := clGray;
        1: MyColor := clSilver;
        7: MyColor := clSilver;
        else MyColor := $000080FF;
      end;
    end;
  end;
  SetBkColor(Grade.Canvas.Handle, MyColor);
  OldAlign := SetTextAlign(Grade.Canvas.Handle, TA_CENTER);
  Grade.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
    Grade.Cells[ACol, ARow]);
  SetTextAlign(Grade.Canvas.Handle, OldAlign);
end;

procedure TFmSomaDiasCred.Timer1Timer(Sender: TObject);
begin
  Grade.Invalidate;
end;

procedure TFmSomaDiasCred.TPDataIClick(Sender: TObject);
begin
  RecriaCalendario;
end;

procedure TFmSomaDiasCred.TPDataIChange(Sender: TObject);
begin
  RecriaCalendario;
end;

procedure TFmSomaDiasCred.TPDataFChange(Sender: TObject);
begin
  RecriaCalendario
end;

procedure TFmSomaDiasCred.TPDataFClick(Sender: TObject);
begin
  RecriaCalendario
end;

procedure TFmSomaDiasCred.EdDMaisChange(Sender: TObject);
begin
  RecriaCalendario;
end;

procedure TFmSomaDiasCred.EdCompChange(Sender: TObject);
begin
  RecriaCalendario;
end;

procedure TFmSomaDiasCred.BtRefreshClick(Sender: TObject);
begin
  RecriaCalendario;
end;

end.
