object FmSomaDiasCred: TFmSomaDiasCred
  Left = 342
  Top = 178
  Caption = 'CDR-EXTRA-001 :: C'#225'lculo de Dias'
  ClientHeight = 520
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 520
    Align = alClient
    TabOrder = 0
    ExplicitTop = 48
    ExplicitHeight = 336
    object Grade: TStringGrid
      Left = 1
      Top = 109
      Width = 782
      Height = 296
      Align = alClient
      ColCount = 10
      DefaultColWidth = 30
      DefaultRowHeight = 21
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
      ScrollBars = ssVertical
      TabOrder = 0
      OnDrawCell = GradeDrawCell
      ExplicitTop = 53
      ExplicitHeight = 282
    end
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 630
      object GB_R: TGroupBox
        Left = 734
        Top = 0
        Width = 48
        Height = 48
        Align = alRight
        TabOrder = 0
        object ImgTipo: TdmkImage
          Left = 8
          Top = 11
          Width = 32
          Height = 32
          Transparent = True
          SQLType = stNil
        end
      end
      object GB_L: TGroupBox
        Left = 0
        Top = 0
        Width = 48
        Height = 48
        Align = alLeft
        TabOrder = 1
      end
      object GB_M: TGroupBox
        Left = 48
        Top = 0
        Width = 686
        Height = 48
        Align = alClient
        TabOrder = 2
        object LaTitulo1A: TLabel
          Left = 7
          Top = 9
          Width = 192
          Height = 32
          Caption = 'C'#225'lculo de Dias'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clGradientActiveCaption
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Visible = False
        end
        object LaTitulo1B: TLabel
          Left = 9
          Top = 11
          Width = 192
          Height = 32
          Caption = 'C'#225'lculo de Dias'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clSilver
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object LaTitulo1C: TLabel
          Left = 8
          Top = 10
          Width = 192
          Height = 32
          Caption = 'C'#225'lculo de Dias'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clHotLight
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 49
      Width = 782
      Height = 60
      Align = alTop
      TabOrder = 2
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 778
        Height = 43
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitHeight = 51
        object Label2: TLabel
          Left = 252
          Top = 0
          Width = 30
          Height = 13
          Caption = 'Comp:'
        end
        object Label1: TLabel
          Left = 212
          Top = 0
          Width = 17
          Height = 13
          Caption = 'D+:'
        end
        object Label3: TLabel
          Left = 352
          Top = 0
          Width = 27
          Height = 13
          Caption = 'Total:'
        end
        object Label4: TLabel
          Left = 8
          Top = 0
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label5: TLabel
          Left = 108
          Top = 0
          Width = 59
          Height = 13
          Caption = 'Vencimento:'
        end
        object Label6: TLabel
          Left = 412
          Top = 0
          Width = 46
          Height = 13
          Caption = 'Data real:'
        end
        object TPDataI: TdmkEditDateTimePicker
          Left = 8
          Top = 16
          Width = 97
          Height = 21
          Date = 38675.754029942100000000
          Time = 38675.754029942100000000
          TabOrder = 0
          OnClick = TPDataIClick
          OnChange = TPDataIChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPDataF: TdmkEditDateTimePicker
          Left = 108
          Top = 16
          Width = 97
          Height = 21
          Date = 38675.754131400500000000
          Time = 38675.754131400500000000
          TabOrder = 1
          OnClick = TPDataFClick
          OnChange = TPDataFChange
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdDMais: TdmkEdit
          Left = 212
          Top = 16
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '3'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 3
          OnChange = EdDMaisChange
        end
        object EdComp: TdmkEdit
          Left = 252
          Top = 16
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '3'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 3
          OnChange = EdCompChange
        end
        object EdDias: TdmkEdit
          Left = 352
          Top = 16
          Width = 53
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdDataReal: TdmkEdit
          Left = 412
          Top = 16
          Width = 81
          Height = 21
          Alignment = taCenter
          ReadOnly = True
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00/00/0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '00/00/0000'
        end
      end
    end
    object GBAvisos1: TGroupBox
      Left = 1
      Top = 405
      Width = 782
      Height = 44
      Align = alBottom
      Caption = ' Avisos: '
      TabOrder = 3
      ExplicitLeft = 13
      ExplicitTop = 467
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 778
        Height = 27
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 1
      Top = 449
      Width = 782
      Height = 70
      Align = alBottom
      TabOrder = 4
      ExplicitLeft = 0
      ExplicitTop = 333
      ExplicitWidth = 630
      object PnSaiDesis: TPanel
        Left = 636
        Top = 15
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn1: TBitBtn
          Tag = 13
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 634
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtRefresh: TBitBtn
          Tag = 18
          Left = 10
          Top = 4
          Width = 120
          Height = 40
          Caption = '&Refresh'
          TabOrder = 0
          OnClick = BtRefreshClick
          NumGlyphs = 2
        end
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = Timer1Timer
    Left = 525
    Top = 61
  end
end
