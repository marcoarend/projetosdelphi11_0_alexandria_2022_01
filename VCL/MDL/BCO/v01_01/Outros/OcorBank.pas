unit OcorBank;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkRadioGroup, ComCtrls,
  dmkImage, dmkDBLookupComboBox, dmkEditCB, UnDmkProcFunc, DmkDAC_PF,
  dmkCheckBox, UnDmkEnums;

type
  TFmOcorBank = class(TForm)
    PainelDados: TPanel;
    DsOcorBank: TDataSource;
    QrOcorBank: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    QrOcorBankBase: TFloatField;
    QrOcorBankEnvio: TIntegerField;
    QrOcorBankMovimento: TIntegerField;
    QrOcorBankFormaCNAB: TSmallintField;
    QrOcorBankAlterWeb: TSmallintField;
    QrOcorBankAtivo: TSmallintField;
    QrOcorBankNOMEMOVIMENTO: TWideStringField;
    OpenDialog1: TOpenDialog;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdBase: TdmkEdit;
    RGFormaCNAB: TdmkRadioGroup;
    RGEnvio: TdmkRadioGroup;
    EdMovimento: TdmkEdit;
    EdMovim_TXT: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBRadioGroup2: TDBRadioGroup;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtImportar: TBitBtn;
    BitBtn1: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    Label8: TLabel;
    EdPlaGen: TdmkEditCB;
    CBPlaGen: TdmkDBLookupComboBox;
    SBPlaGen: TSpeedButton;
    QrOcorBankPlaGen: TIntegerField;
    QrOcorBankNO_PLAGEN: TWideStringField;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    BtContas: TBitBtn;
    CkAtencao: TdmkCheckBox;
    DBCheckBox1: TDBCheckBox;
    QrOcorBankAtencao: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOcorBankAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOcorBankBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure EdMovimentoChange(Sender: TObject);
    procedure QrOcorBankCalcFields(DataSet: TDataSet);
    procedure BtImportarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EdCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbImprimeClick(Sender: TObject);
    procedure SBPlaGenClick(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmOcorBank: TFmOcorBank;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, Contas, MyListas, UnBancos;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOcorBank.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOcorBank.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOcorBankCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOcorBank.DefParams;
begin
  VAR_GOTOTABELA := 'ocorbank';
  VAR_GOTOMYSQLTABLE := QrOcorBank;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cnt.Nome NO_PLAGEN, ocb.*');
  VAR_SQLx.Add('FROM ocorbank ocb');
  VAR_SQLx.Add('LEFT JOIN contas cnt ON cnt.Codigo=ocb.PlaGen');
  VAR_SQLx.Add('WHERE ocb.Codigo <> 0');
  //
  VAR_SQL1.Add('AND ocb.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND ocb.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ocb.Nome Like :P0');
  //
end;

procedure TFmOcorBank.EdCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'ocorbank', 'Codigo', [], [],
    stIns, 0, siPositivo, EdCodigo);
end;

procedure TFmOcorBank.EdMovimentoChange(Sender: TObject);
const
  BancoDoBrasil = 1;
var
  Envio: TEnvioCNAB;
begin
  Envio := ecnabIndefinido;
  //
  case RGEnvio.ItemIndex of
    1: Envio := ecnabRemessa;
    2: Envio := ecnabRetorno;
  end;
  EdMovim_TXT.Text := UBancos.CNABTipoDeMovimento(
    BancoDoBrasil, Envio, EdMovimento.ValueVariant, 240, False, '');
end;

procedure TFmOcorBank.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant    := FormatFloat(FFormatFloat, Codigo);
        EdNome.ValueVariant      := '';
        EdBase.ValueVariant      := 0;
        RGFormaCNAB.ItemIndex    := -1;
        RGEnvio.ItemIndex        := -1;
        EdMovimento.ValueVariant := 0;
        EdMovim_TXT.ValueVariant := '';
        //
      end else begin
        EdCodigo.ValueVariant    := QrOcorBankCodigo.Value;
        EdNome.ValueVariant      := QrOcorBankNome.Value;
        EdBase.ValueVariant      := QrOcorBankBase.Value;
        RGFormaCNAB.ItemIndex    := QrOcorBankFormaCNAB.Value;
        RGEnvio.ItemIndex        := QrOcorBankEnvio.Value;
        EdMovimento.ValueVariant := QrOcorBankMovimento.Value;
        //
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmOcorBank.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOcorBank.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOcorBank.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOcorBank.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOcorBank.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOcorBank.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOcorBank.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOcorBank.SBPlaGenClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.ShowModal;
    FmContas.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdPlaGen, CBPlaGen, QrContas, VAR_CADASTRO);
  end;
end;

procedure TFmOcorBank.BitBtn1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'OcorBank', 'OcorBank',
    nil, False, True, LaAviso1, LaAviso2, nil, nil);
  Geral.MensagemBox('Registros recriados com sucesso!', 'Informa��o',
    MB_OK+MB_ICONINFORMATION);
  Screen.Cursor := crDefault;
end;

procedure TFmOcorBank.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrOcorBank, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'ocorbank');
end;

procedure TFmOcorBank.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOcorBankCodigo.Value;
  Close;
end;

procedure TFmOcorBank.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Codigo := EdCodigo.ValueVariant;
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Codigo = 0, EdCodigo, 'Defina o ID!') then Exit;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if CO_DMKID_APP <> 3 then //Creditor
  begin
    if MyObjects.FIC(EdPlaGen.ValueVariant = 0, EdPlaGen,
      'Defina uma conta (do plano de contas)!') then Exit;
  end;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmOcorBank, PainelEdita,
    'ocorbank', EdCodigo.ValueVariant, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmOcorBank.BtContasClick(Sender: TObject);
var
  Codigo, PlaGen, SubGrupo: Integer;
  Nome, Nome2, Credito, Debito: String;
begin
  if Geral.MensagemBox(
  'A a��o a seguir ir� criar contas novas no plano de contas, ' + sLineBreak +
  'sendo criada uma conta para cada ocorr�ncia sem conta definida!' + sLineBreak +
  '' + sLineBreak +
  'Antes de executar esta a��o tenha certeza que n�o existe cadastro ' + sLineBreak +
  'de contas que poderia ser usada para alguma ocorr�ncia!' + sLineBreak +
  '' + sLineBreak +
  'Somente ocorr�ncias j� utilizadas ter�o contas criadas e atreladas!' + sLineBreak +
  '' + sLineBreak +
  'Deseja realmente criar as contas e atrel�-las �s ocorr�ncias sem contas?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM ocorbank ',
    'WHERE PlaGen=0 ',
    'AND Codigo <> 0 ',
    'AND Codigo IN ',
    '     ( ',
    '     SELECT DISTINCT Ocorrencia ',
    '     FROM ocorreu ',
    '     ) ',
    '']);
    //
    if Dmod.QrAux.RecordCount > 0 then
    begin
      if Geral.MensagemBox(
      'Foram localizadas ' + Geral.FF0(Dmod.QrAux.RecordCount) +
      ' ocorr�ncias usadas e sem conta atrelada!' + sLineBreak +
      '' + sLineBreak +
      'Deseja realmente criar as contas e atrel�-las a estas ocorr�ncias?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Dmod.QrAux.First;
        while not Dmod.QrAux.Eof do
        begin
          Nome := Dmod.QrAux.FieldByName('Nome').AsString;
          Nome2 := Nome;
          Credito := 'V';
          Debito := 'F';
          Subgrupo := 0;
          //
          Codigo := UMyMod.BuscaEmLivreY_Def('contas', 'Codigo', stIns, 0);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'contas', False, [
          'Nome', 'Nome2',
          'Subgrupo', 'Credito', 'Debito'], [
          'Codigo'], [
          Nome, Nome2,
          Subgrupo, Credito, Debito], [
          Codigo], False) then
          begin
            PlaGen := Codigo;
            Codigo := Dmod.QrAux.FieldByName('Codigo').AsInteger;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ocorbank', False, [
            'PlaGen'], ['Codigo'], [PlaGen], [Codigo], False);
          end;
          //
          Dmod.QrAux.Next;
        end;
        //
        LocCod(QrOcorBankCodigo.Value, QrOcorBankCodigo.Value);
      end;
    end else Geral.MensagemBox(
      'N�o foram localizadas ocorr�ncias usadas sem conta atrelada!', 'Mensagem',
      MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TFmOcorBank.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ocorbank', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ocorbank', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ocorbank', 'Codigo');
end;

procedure TFmOcorBank.BtImportarClick(Sender: TObject);
var
  Lista: TStringList;
  i, PosNum, Tam: Integer;
  Codigo, Nome: String;
begin
  if OpenDialog1.Execute then
  begin
    Screen.Cursor := crHourGlass;
    //
    PosNum := 0;
    Lista := TStringList.Create;
    Lista.LoadFromFile(OpenDialog1.FileName);
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM ocorbank WHERE Codigo=:P0');
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO ocorbank SET AlterWeb=1, ');
    Dmod.QrUpdU.SQL.Add('Nome=:P0, Codigo=:P1');
    //
    PB1.Position := 0;
    PB1.Max      := Lista.Count;

    for i := 0 to Lista.Count-1 do
    begin
      PB1.Position := PB1.Position + 1;
      Tam := Length(Lista[i]);
      if Tam > 4 then
      begin
        if (Lista[i][1] in (['0'..'9'])) then
        begin
          Codigo := Copy(Lista[i], 1, 3);
          if PosNum <> 0 then
            Nome := TrimRight(Copy(Lista[i], 5, Length(Lista[i])))
          else
            Nome := Copy(Lista[i], 5, Length(Lista[i]));
          //ShowMessage(PChar(Codigo+' - '+Nome+' @ '+Motivo));
          Dmod.QrUpdM.Params[00].AsString := Codigo;
          Dmod.QrUpdM.ExecSQL;
          //
          Dmod.QrUpdU.Params[00].AsString := Nome;
          Dmod.QrUpdU.Params[01].AsString := Codigo;
          Dmod.QrUpdU.ExecSQL;
        end;
      end;
    end;
    PB1.Position := 0;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOcorBank.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrOcorBank, [PainelDados],
  [PainelEdita], EdCodigo, ImgTipo, 'ocorbank');
end;

procedure TFmOcorBank.FormCreate(Sender: TObject);
var
  Enab: Boolean;
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
  CriaOForm;
  //
  Enab := (CO_DMKID_APP <> 3); //Creditor
  //
  EdPlaGen.Enabled := Enab;
  CBPlaGen.Enabled := Enab;
  SBPlaGen.Enabled := Enab;
end;

procedure TFmOcorBank.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOcorBankCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOcorBank.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmOcorBank.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOcorBank.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOcorBankCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOcorBank.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOcorBank.QrOcorBankAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOcorBank.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOcorBank.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOcorBankCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ocorbank', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOcorBank.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOcorBank.QrOcorBankBeforeOpen(DataSet: TDataSet);
begin
  QrOcorBankCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmOcorBank.QrOcorBankCalcFields(DataSet: TDataSet);
var
  BancoDoBrasil: Integer;
  Envio: TEnvioCNAB;
begin
  Envio         := ecnabIndefinido;
  BancoDoBrasil := 1;
  case QrOcorBankEnvio.Value of
    1: Envio := ecnabRemessa;
    2: Envio := ecnabRetorno;
  end;
  QrOcorBankNOMEMOVIMENTO.Value := UBancos.CNABTipoDeMovimento(
    BancoDoBrasil, Envio, QrOcorBankMovimento.Value, 240, False, '');
end;

end.

