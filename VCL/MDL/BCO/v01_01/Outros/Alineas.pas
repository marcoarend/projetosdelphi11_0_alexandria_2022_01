unit Alineas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, ComCtrls, dmkImage,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmAlineas = class(TForm)
    PainelDados: TPanel;
    DsAlineas: TDataSource;
    QrAlineas: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrAlineasCodigo: TIntegerField;
    QrAlineasNome: TWideStringField;
    QrAlineasMotivo: TWideStringField;
    QrAlineasLk: TIntegerField;
    QrAlineasDataCad: TDateField;
    QrAlineasDataAlt: TDateField;
    QrAlineasUserCad: TIntegerField;
    QrAlineasUserAlt: TIntegerField;
    QrAlineasAlterWeb: TSmallintField;
    QrAlineasAtivo: TSmallintField;
    OpenDialog1: TOpenDialog;
    GBDados: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtImportar: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    EdCodigo: TdmkEdit;
    EdMotivo: TdmkEdit;
    EdNome: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Progress: TProgressBar;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrAlineasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrAlineasBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
    procedure EdCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmAlineas: TFmAlineas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmAlineas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmAlineas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrAlineasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmAlineas.DefParams;
begin
  VAR_GOTOTABELA := 'alineas';
  VAR_GOTOMYSQLTABLE := QrAlineas;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM alineas');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmAlineas.EdCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'alineas', 'Codigo', [], [],
    stIns, 0, siPositivo, EdCodigo);
end;

procedure TFmAlineas.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant := FormatFloat(FFormatFloat, Codigo);
        EdNome.ValueVariant   := '';
        EdMotivo.ValueVariant := '';
        //
      end else begin
        EdCodigo.ValueVariant := QrAlineasCodigo.Value;
        EdNome.ValueVariant   := QrAlineasNome.Value;
        EdMotivo.ValueVariant := QrAlineasMotivo.Value;
        //
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmAlineas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmAlineas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmAlineas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmAlineas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmAlineas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmAlineas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmAlineas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmAlineas.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrAlineas, [PainelDados],
  [PainelEdita], EdMotivo, ImgTipo, 'alineas');
end;

procedure TFmAlineas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrAlineasCodigo.Value;
  Close;
end;

procedure TFmAlineas.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome, Motivo: String;
begin
  Codigo := EdCodigo.ValueVariant;
  Nome   := EdNome.ValueVariant;
  Motivo := EdMotivo.ValueVariant;
  //
  if MyObjects.FIC(Codigo = 0, EdCodigo, 'Defina o ID!') then Exit;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(Motivo) = 0, EdMotivo, 'Defina o motivo!') then Exit;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmAlineas, PainelEdita,
    'alineas', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmAlineas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'alineas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'alineas', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'alineas', 'Codigo');
end;

procedure TFmAlineas.BtImportarClick(Sender: TObject);
var
  Lista: TStringList;
  i, PosNum, Tam: Integer;
  Codigo, Nome, Motivo: String;
begin
  if OpenDialog1.Execute then
  begin
    Screen.Cursor := crHourGlass;
    //
    PosNum := 0;
    Lista := TStringList.Create;
    Lista.LoadFromFile(OpenDialog1.FileName);
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM alineas WHERE Codigo=:P0');
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO alineas SET');
    Dmod.QrUpdU.SQL.Add('Nome=:P0, Motivo=:P1, Codigo=:P2');
    //
    Progress.Position := 0;
    Progress.Visible := True;
    Progress.Max := Lista.Count;

    for i := 0 to Lista.Count-1 do
    begin
      Progress.Position := Progress.Position + 1;
      Tam := Length(Lista[i]);
      if Tam > 3 then
      begin
        if not (Lista[i][1] in (['0'..'9'])) then Motivo := Lista[i] else
        begin
          Codigo := Copy(Lista[i], 1, 2);
          if PosNum <> 0 then
            Nome := TrimRight(Copy(Lista[i], 4, Length(Lista[i])))
          else
            Nome := Copy(Lista[i], 4, Length(Lista[i]));
          //ShowMessage(PChar(Codigo+' - '+Nome+' @ '+Motivo));
          Dmod.QrUpdM.Params[00].AsString := Codigo;
          Dmod.QrUpdM.ExecSQL;
          //
          Dmod.QrUpdU.Params[00].AsString := Nome;
          Dmod.QrUpdU.Params[01].AsString := Motivo;
          Dmod.QrUpdU.Params[02].AsString := Codigo;
          Dmod.QrUpdU.ExecSQL;
        end;
      end;
    end;
    Progress.Visible := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAlineas.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrAlineas, [PainelDados],
  [PainelEdita], EdCodigo, ImgTipo, 'alineas');
end;

procedure TFmAlineas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmAlineas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrAlineasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAlineas.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmAlineas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmAlineas.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrAlineasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAlineas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmAlineas.QrAlineasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmAlineas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAlineas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrAlineasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'alineas', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmAlineas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAlineas.QrAlineasBeforeOpen(DataSet: TDataSet);
begin
  QrAlineasCodigo.DisplayFormat := FFormatFloat;
end;

end.

