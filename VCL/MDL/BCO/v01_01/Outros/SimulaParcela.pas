unit SimulaParcela;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, UnMLAGeral, ComCtrls, UnGOTOy, Buttons, Db, (*DBTables,*)
  UnInternalConsts, (*DBIProcs,*) ExtCtrls, ZCF2, UnMsgInt, UMySQLModule,
  mySQLDbTables, Grids, DBGrids, math, dmkGeral, dmkEdit, dmkEditDateTimePicker,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmSimulaParcela = class(TForm)
    QrSimula0: TmySQLQuery;
    DsSimula0: TDataSource;
    QrSimula0Parcela: TIntegerField;
    QrSimula0DataPg: TDateField;
    QrSimula0ValParc: TFloatField;
    QrSimula0ValOrig: TFloatField;
    QrSimula0PerJuro: TFloatField;
    QrSimula0ValPrin: TFloatField;
    QrSimula0ValJuro: TFloatField;
    QrSum0: TmySQLQuery;
    QrSum0ValParc: TFloatField;
    QrSum0ValOrig: TFloatField;
    QrSum0ValPrin: TFloatField;
    QrSum0ValJuro: TFloatField;
    QrSum0Parcelas: TLargeintField;
    DsSum0: TDataSource;
    Qr2: TmySQLQuery;
    Qr2Parcela: TIntegerField;
    Qr2DataPg: TDateField;
    Qr2PerJuro: TFloatField;
    Qr2ValParc: TFloatField;
    Qr2ValOrig: TFloatField;
    Qr2ValPrin: TFloatField;
    Qr2ValJuro: TFloatField;
    QrMedia: TmySQLQuery;
    QrMediaDias: TFloatField;
    DsMedia: TDataSource;
    QrPesq: TmySQLQuery;
    DsSimula1: TDataSource;
    QrSum1: TmySQLQuery;
    LargeintField1: TLargeintField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    DsSum1: TDataSource;
    QrSum2: TmySQLQuery;
    DsSum2: TDataSource;
    QrSimula2: TmySQLQuery;
    DsSimula2: TDataSource;
    QrSum2Parcelas: TLargeintField;
    QrSum2ValParc: TFloatField;
    QrSum2ValOrig: TFloatField;
    QrSum2ValPrin: TFloatField;
    QrSum2ValJuro: TFloatField;
    QrSimula2Parcela: TIntegerField;
    QrSimula2DataPg: TDateField;
    QrSimula2ValParc: TFloatField;
    QrSimula2ValOrig: TFloatField;
    QrSimula2PerJuro: TFloatField;
    QrSimula2ValPrin: TFloatField;
    QrSimula2ValJuro: TFloatField;
    TbSimula1: TmySQLTable;
    TbSimula1Tipo: TIntegerField;
    TbSimula1Parcela: TIntegerField;
    TbSimula1DataPg: TDateField;
    TbSimula1PerJuro: TFloatField;
    TbSimula1ValParc: TFloatField;
    TbSimula1ValOrig: TFloatField;
    TbSimula1ValPrin: TFloatField;
    TbSimula1ValJuro: TFloatField;
    Qr2Tipo: TIntegerField;
    Qr2PercIOF: TFloatField;
    Qr2ValrIOF: TFloatField;
    Qr2Dias: TIntegerField;
    QrSimula0Tipo: TIntegerField;
    QrSimula0PercIOF: TFloatField;
    QrSimula0ValrIOF: TFloatField;
    QrSimula0Dias: TIntegerField;
    TbSimula1PercIOF: TFloatField;
    TbSimula1ValrIOF: TFloatField;
    TbSimula1Dias: TIntegerField;
    QrSimula2Tipo: TIntegerField;
    QrSimula2PercIOF: TFloatField;
    QrSimula2ValrIOF: TFloatField;
    QrSimula2Dias: TIntegerField;
    QrSum0ValrIOF: TFloatField;
    QrSum1ValrIOF: TFloatField;
    QrSum2ValrIOF: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel6: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel7: TPanel;
    BtCalcula: TBitBtn;
    BtReabre: TBitBtn;
    Panel1: TPanel;
    RGTipo: TRadioGroup;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    LaDeb: TLabel;
    Label14: TLabel;
    Label10: TLabel;
    LaNF: TLabel;
    Label15: TLabel;
    LaCred: TLabel;
    TPData: TdmkEditDateTimePicker;
    TPPrimeiroDia: TdmkEditDateTimePicker;
    EdPrincipal: TdmkEdit;
    EdOutros: TdmkEdit;
    EdTaxa: TdmkEdit;
    EdParcelas: TdmkEdit;
    EdValor: TdmkEdit;
    EdValorParcela: TdmkEdit;
    GroupBox3: TGroupBox;
    EdIOF_F: TdmkEdit;
    CkIOF_M: TCheckBox;
    EdIOF_M: TdmkEdit;
    LaIOFVal: TLabel;
    EdIOFVal: TdmkEdit;
    EdIOF_D: TdmkEdit;
    Label9: TLabel;
    Label8: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel4: TPanel;
    CkDiasUteis: TCheckBox;
    Label52: TLabel;
    PnCalc: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    Button1: TButton;
    EdJuros: TdmkEdit;
    EdTotal: TdmkEdit;
    GroupBox1: TGroupBox;
    EdPraca: TdmkEdit;
    Label4: TLabel;
    EdDMais: TdmkEdit;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    Label7: TLabel;
    EdCETaa: TdmkEdit;
    Label17: TLabel;
    EdCETam: TdmkEdit;
    Label16: TLabel;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    LaCalculo1: TLabel;
    LaCalculo2: TLabel;
    QrLast: TmySQLQuery;
    QrLastParcela: TIntegerField;
    QrLastBaseCalc: TFloatField;
    QrLastVencimento: TDateField;
    QrLastDiasParcel: TIntegerField;
    QrLastDiasCorrid: TIntegerField;
    QrLastMesesPerio: TFloatField;
    QrLastFatorJuros: TFloatField;
    QrLastValCorrigi: TFloatField;
    QrLastValPrincip: TFloatField;
    QrLastValJuros: TFloatField;
    QrLastIOF_Percen: TFloatField;
    QrLastIOF_Valor: TFloatField;
    QrLastAtivo: TIntegerField;
    QrIOF: TmySQLQuery;
    QrIOFParcela: TIntegerField;
    QrIOFBaseCalc: TFloatField;
    QrIOFVencimento: TDateField;
    QrIOFDiasParcel: TIntegerField;
    QrIOFDiasCorrid: TIntegerField;
    QrIOFMesesPerio: TFloatField;
    QrIOFFatorJuros: TFloatField;
    QrIOFValCorrigi: TFloatField;
    QrIOFValPrincip: TFloatField;
    QrIOFValJuros: TFloatField;
    QrIOFIOF_Percen: TFloatField;
    QrIOFIOF_Valor: TFloatField;
    QrIOFAtivo: TIntegerField;
    QrSoma: TmySQLQuery;
    QrSomaIOF_Valor: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    PnBanco: TPanel;
    DBGrid1: TDBGrid;
    DBGrid4: TDBGrid;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DsIOF: TDataSource;
    QrMediaJUROS: TFloatField;
    TabSheet5: TTabSheet;
    Panel9: TPanel;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    Panel10: TPanel;
    DBGrid8: TDBGrid;
    DBGrid9: TDBGrid;
    Panel11: TPanel;
    DBGrid5: TDBGrid;
    DBGrid6: TDBGrid;
    QrIOFPrestacao: TFloatField;
    QrIOFVCTO_TXT: TWideStringField;
    QrIOFJUROS: TFloatField;
    QrSumI: TmySQLQuery;
    DsSumI: TDataSource;
    QrSumIPrestacao: TFloatField;
    QrSumIValOrig: TFloatField;
    QrSumIValPrincip: TFloatField;
    QrSumIValJuros: TFloatField;
    QrSumIIOF_Valor: TFloatField;
    QrSumIParcelas: TLargeintField;
    Panel12: TPanel;
    La001: TLabel;
    La002: TLabel;
    La003: TLabel;
    La004: TLabel;
    La005: TLabel;
    La006: TLabel;
    Label2: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    GroupBox5: TGroupBox;
    Panel13: TPanel;
    LaTempo1: TLabel;
    LaTempo2: TLabel;
    Label26: TLabel;
    La000: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrMediaCalcFields(DataSet: TDataSet);
    procedure TbSimula1NewRecord(DataSet: TDataSet);
    procedure TbSimula1DataPgValidate(Sender: TField);
    procedure TbSimula1DataPgChange(Sender: TField);
    procedure BtReabreClick(Sender: TObject);
    procedure TbSimula1BeforePost(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtCalculaClick(Sender: TObject);
    procedure EdValorParcelaChange(Sender: TObject);
    procedure EdTaxaChange(Sender: TObject);
    procedure HabilitaCalcula(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrIOFCalcFields(DataSet: TDataSet);
    procedure EdPrincipalChange(Sender: TObject);
    procedure EdOutrosChange(Sender: TObject);
    procedure EdIOFValChange(Sender: TObject);
  private
    { Private declarations }
    FIOF, FSimulaPG: String;
    FCalculos, FDias, FCompensacao, FPrazo: Integer;
    FJuro, FValr, FValF: Double;
    FTempoIni: TDateTime;
    //
    function CalculaCET_Meu(): Double;
    function CalculaParcelas(): Double;
    function ValorParcela(DataExe, DataI: TDateTime; Principal, JurosM: Double;
             Parcelas: Integer; SoDiaUtil: Boolean): Double;
    //procedure CalculaValoresChequeAlterado(Valor: Double; Dias: Integer);
    procedure CalculaDiasCH(Data, Vcto: TDateTime; Valor: Double; Parcelas: Integer);
    procedure AtualizaItem();
    procedure SimulaBordero(Tipo: Integer);
    procedure ReopenSimulaPg(Reopen: Boolean);
    procedure CriaBordero1();
    procedure CalculaParcelaBancaria();
    procedure CalculaParcelaInfinita();
    //
    //Altera��o da data
    procedure CriaBordero1Data;
    function  ValorParcelaData(DataExe, DataI: TDateTime; Principal, JurosM: Double;
              Parcelas: Integer; SoDiaUtil: Boolean): Double;
    function  ReabreQr2(Tipo: Integer): Boolean;
    procedure CalculaIOF(Parcelas: Integer; SoDiaUtil: Boolean);
    procedure SetaDadosIOF(PJ: Boolean);
    procedure IncrementaCalculo(Incremento, Tipo: Integer);
    function ValorPrestacao_Aproximado(const Emprestado,
              Taxa, MediaDias: Double): Double;
    function  RecalculaIOF(Parcelas: Integer; Base, Taxa,
              Prestacao: Double): Double;
    procedure DefineValorFinanciado();
    function  DescricaoDeTipoDeSimulacao(Tipo: Integer): String;
    procedure InformaTempo();

  public
    { Public declarations }
  end;

var
  FmSimulaParcela: TFmSimulaParcela;

implementation

uses UnMyObjects, Module, Principal, UCreate, ModuleGeral;

{$R *.DFM}

procedure TFmSimulaParcela.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSimulaParcela.BtReabreClick(Sender: TObject);
var
  Valor, Juros, Parcela: Double;
  Parcelas: Integer;
begin
  IncrementaCalculo(- FCalculos, 0);
  Valor := EdValor.ValueVariant;
  Juros := Geral.DMV(EdTaxa.Text);
  Parcelas := EdParcelas.ValueVariant;
  //
  case RGTipo.ItemIndex of
    0: Exit; // Nada
    1: // Saber Taxa
    begin
      Juros := CalculaParcelas();
      EdTaxa.Text := Geral.FFT(Juros, 6, siNegativo);
    end;
    2: //Saber valor da parcela
    begin
      Parcela := ValorParcelaData(Int(TPData.Date), Int(TPPrimeiroDia.Date),
        Valor, Juros, Parcelas, CkDiasUteis.Checked);
      EdValorParcela.Text := Geral.FFT(Parcela, 2, siNegativo);
    end;
  end;
  SimulaBordero(0);
  CriaBordero1Data;
  CalculaParcelaBancaria();
  CalculaParcelaInfinita();
  SimulaBordero(1);
  SimulaBordero(2);
  ReopenSimulaPg(True);
end;

procedure TFmSimulaParcela.BitBtn1Click(Sender: TObject);
begin
  SetaDadosIOF(True);
end;

procedure TFmSimulaParcela.BitBtn2Click(Sender: TObject);
begin
  SetaDadosIOF(False);
end;

procedure TFmSimulaParcela.BtCalculaClick(Sender: TObject);
var
  Valor, Taxa, Parcela: Double;
  //DiasUteis, PrimeiroDia: String;
  Parcelas: Integer;
begin
  La000.Caption := '0';
  La001.Caption := '0';
  La002.Caption := '0';
  La003.Caption := '0';
  La004.Caption := '0';
  La005.Caption := '0';
  La006.Caption := '0';
  FTempoIni := Now();
  //
  IncrementaCalculo(- FCalculos, 0);
  if MyObjects.FIC(RGTipo.ItemIndex < 1, RGTIpo,  'Defina o tipo de c�lculo') then
    Exit;
  Valor := Geral.DMV(EdPrincipal.Text + EdOutros.ValueVariant);
  Taxa := Geral.DMV(EdTaxa.Text);
  Parcelas := EdParcelas.ValueVariant;
  if MyObjects.FIC(Valor = 0, EdPrincipal, 'Defina o valor do empr�stimo') then
    Exit;
  Screen.Cursor := crHourGlass;
  (*
  if (TPPrimeiroDia.Enabled = False) then PrimeiroDia := CO_VAZIO
  else PrimeiroDia := FormatDateTime(VAR_FORMATDATE, Int(TPPrimeiroDia.Date));
  if CkDiasUteis.Checked = True then DiasUteis := 'V' else DiasUteis := 'F';
  *)
  //
  case RGTipo.ItemIndex of
    0: Exit; // Nada
    1: // Saber Taxa
    begin
      //CalculaIOF(Parcelas, CkDiasUteis.Checked);
      Taxa := CalculaParcelas();
      EdTaxa.Text := Geral.FFT(Taxa, 6, siNegativo);
    end;
    2: //Saber valor da parcela
    begin
      if Parcelas = 0 then
      begin
        Geral.MensagemBox('Defina a quantidade de parcelas!',
        'Aviso', MB_OK+MB_ICONWARNING);
        EdValor.SetFocus;
        Exit;
      end;
      //
      CalculaIOF(Parcelas, CkDiasUteis.Checked);
      //
      Valor := EdValor.ValueVariant;
      Parcela := ValorParcela(Int(TPData.Date), Int(TPPrimeiroDia.Date),
        Valor, Taxa, Parcelas, CkDiasUteis.Checked);
      EdValorParcela.Text := Geral.FFT(Parcela, 2, siNegativo);
    end;
  end;
  SimulaBordero(0);
  CriaBordero1();
  CalculaParcelaBancaria();
  CalculaParcelaInfinita();
  SimulaBordero(1);
  SimulaBordero(2);
  ReopenSimulaPg(True);
  //
  CalculaCET_Meu();
  //
  InformaTempo();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  Screen.Cursor := crDefault;
end;

procedure TFmSimulaParcela.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSimulaParcela.RGTipoClick(Sender: TObject);
begin
  HabilitaCalcula(Sender);
  case RGTipo.ItemIndex of
    0: ;// Nada
    1: // Saber Taxa
    begin
      EdTaxa.Enabled := False;
      //
      LaIOFVal.Enabled := True;
      EdIOFVal.Enabled := True;
      EdValorParcela.Enabled   := True;
      TbSimula1DataPg.ReadOnly := True;
    end;
    2: //Saber valor da parcela
    begin
      EdTaxa.Enabled := True;
      //
      LaIOFVal.Enabled := False;
      EdIOFVal.Enabled := False;
      EdValorParcela.Enabled   := False;
      TbSimula1DataPg.ReadOnly := False;
    end;
  end;
end;

function TFmSimulaParcela.CalculaCET_Meu(): Double;
type
  Registro = ^AList;
  AList = record
    Data : TDate;
    Juros : Double;
  end;
var
  //Valor: Double; 
  //i,
  DataExe, Parcelas : Integer;
  Prestacao, ValorTotal, JuroM : Double;
  Data, Datai : TDate;
  SoDiaUtil : Boolean;
  Cursor : TCursor;
begin
  Result := -1;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    /////////////////////////////////////////
    Data := Int(TPData.Date);
    Datai := Int(TPPrimeiroDia.Date);
    Parcelas := EdParcelas.ValueVariant;
    Prestacao := Geral.DMV(EdValorParcela.Text);
    //ValorTotal := EdValor.ValueVariant;
    ValorTotal := Geral.DMV(EdPrincipal.Text);
    if CkDiasUteis.Checked = False then
       SoDiaUtil := False else SoDiaUtil := True;

    /////////////////////////////////////////
    if (Datai <= Data) then
    begin
      Application.MessageBox('Data do primeiro pagamento inv�lida.', 'Erro', MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    Screen.Cursor := crHourGlass;
    Refresh;

    DataExe := Trunc(Data);
    //Valor := Trunc((ValorTotal / Parcelas*100)+0.9)/100;
    //Data := Datai;
    JuroM := MLAGeral.JuroMensal(DataExe, Datai, Parcelas,
      ValorTotal, Prestacao, SoDiaUtil);
    if JuroM > -1 then
    begin
      Result := JuroM;
      ///
      EdCETam.ValueVariant := Result;
      //
      /// C�lculo CET anual  /////////////////////////////////////////////////////
      ///
      EdCETaa.ValueVariant := ((Power(1 + (Result / 100), 12) -1) * 100);
      ///
      //
    end;
  finally
    Screen.Cursor := Cursor;
  end;
end;

function TFmSimulaParcela.CalculaParcelas(): Double;
type
  Registro = ^AList;
  AList = record
    Data : TDate;
    Juros : Double;
  end;
var
  i, DataExe, Parcelas : Integer;
  //Valor, 
  Prestacao, ValorTotal, JuroM : Double;
  Data, Datai : TDate;
  SoDiaUtil : Boolean;
  Cursor : TCursor;
begin
  Result := -1;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    /////////////////////////////////////////
    Data := Int(TPData.Date);
    Datai := Int(TPPrimeiroDia.Date);
    Parcelas := EdParcelas.ValueVariant;
    Prestacao := EdValorParcela.ValueVariant;
    ValorTotal := EdValor.ValueVariant;
    //Principal := EdValor.ValueVariant - EdIOFVal.ValueVariant;
    if CkDiasUteis.Checked = False then
       SoDiaUtil := False else SoDiaUtil := True;

    /////////////////////////////////////////
    if (Datai <= Data) then
    begin
      Application.MessageBox('Data do primeiro pagamento inv�lida.', 'Erro', MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    Screen.Cursor := crHourGlass;
    Refresh;

    DataExe := Trunc(Data);
    //Data := Datai;
    //Valor := Trunc((ValorTotal / Parcelas * 100) + 0.9) / 100;
    JuroM := MLAGeral.JuroMensal(DataExe, Datai, Parcelas,
      ValorTotal, Prestacao, SoDiaUtil);
    {
    Valor := Trunc((Principal / Parcelas * 100) + 0.9) / 100;
    JuroM := MLAGeral.JuroMensal(DataExe, Datai, Parcelas,
      Principal, Prestacao, SoDiaUtil);
    }
    if JuroM > -1 then
    begin
      try
        Result := JuroM;
        //
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FSimulaPg + ' SET DataPg=:P0, ');
        DModG.QrUpdPID1.SQL.Add('Parcela=:P1, ValParc=:P2, ValOrig=:P3, ');
        DModG.QrUpdPID1.SQL.Add('ValPrin=:P4, ValJuro=:P5, PerJuro=:P6, Tipo=0');
        for i := 1 to Parcelas do
        begin
          if SoDiaUtil then
          begin
            //if (DayOfWeek(Data) = 1) then Data := Data + 1;
            //if (DayOfWeek(Data) = 7) then Data := Data + 2;
          end;
          //
          //Data := IncMonth(Datai,i);
        end;
      except
        Result := -1;
      end;
    end;
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmSimulaParcela.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;

  (*
  TPData.Date := Int(Date);
  TPPrimeiroDia.Date := IncMonth(Date, 1);
  *)
  TPData.Date := EncodeDate(2011, 08, 29);
  TPPrimeiroDia.Date := EncodeDate(2011, 10, 01);

  //UCriar.GerenciaTabelaLocal('SimulaPg', acCreate);
  FSimulaPG := UCriar.RecriaTempTableNovo(ntrttSimulaPg, DModG.QrUpdPID1, False);
  BtReabre.Visible   := False;
  HabilitaCalcula(Sender);
  //
  QrMedia.Database := DModG.MyPID_DB;
  QrPesq.Database := DModG.MyPID_DB;
  Qr2.Database := DModG.MyPID_DB;
  //
  QrSimula0.Database := DModG.MyPID_DB;
  TbSimula1.Database := DModG.MyPID_DB;
  QrSimula2.Database := DModG.MyPID_DB;
  //
  QrSum0.Database := DModG.MyPID_DB;
  QrSum1.Database := DModG.MyPID_DB;
  QrSum2.Database := DModG.MyPID_DB;
end;

procedure TFmSimulaParcela.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSimulaParcela.HabilitaCalcula(Sender: TObject);
begin
  BtCalcula.Enabled := True;
  ReopenSimulaPg(False);
end;

procedure TFmSimulaParcela.IncrementaCalculo(Incremento, Tipo: Integer);
begin
  InformaTempo();
  //
  FCalculos := FCalculos + Incremento;
  if FCalculos < 1 then
    FCalculos := 0;
  MyObjects.Informa2(LaCalculo1, LaCalculo2, False, FormatFloat('00000000',
    FCalculos));
  case Tipo of
    0: La000.Caption := FormatFloat('0', Geral.IMV(La000.Caption) + 1);
    1: La001.Caption := FormatFloat('0', Geral.IMV(La001.Caption) + 1);
    2: La002.Caption := FormatFloat('0', Geral.IMV(La002.Caption) + 1);
    3: La003.Caption := FormatFloat('0', Geral.IMV(La003.Caption) + 1);
    4: La004.Caption := FormatFloat('0', Geral.IMV(La004.Caption) + 1);
    5: La005.Caption := FormatFloat('0', Geral.IMV(La005.Caption) + 1);
    6: La006.Caption := FormatFloat('0', Geral.IMV(La006.Caption) + 1);
  end;
end;

procedure TFmSimulaParcela.InformaTempo();
var
  Tempo: String;
begin
  Tempo := FormatDateTime('hh:nn:ss', Now() - FTempoIni);
  //
  LaTempo1.Caption := Tempo;
  LaTempo2.Caption := Tempo;
end;

function TFmSimulaParcela.ValorPrestacao_Aproximado(const Emprestado,
  Taxa, MediaDias: Double): Double;
const
  Euler =  2.718281828;
  DiasAno = 365;
var
  C0, C1, C2, C3, C4, C5, C6, C7, C8, C9: Double;
  //Principal, Outros, ValIOF, Parcelas: Double;
begin
(*
  APPLICATIONS: MODELING WITH FIRST ORDER EQUATIONS

      10% x 8000
  k = -----------
           -10% x 3
      1 - e
*)
  //
  C0 := EdParcelas.ValueVariant;
  //
  C1 := Taxa / 100;
  C2 := C1 * C0;
  C3 := Power(euler, - C2);
  C4 := 1 - C3;
  //
  C5 := Emprestado;
  C6 := C5 * C1;
  //
  C7 := MediaDias / (DiasAno / 12) * 2;
  //
  //
  if C4 = 0 then
    C8 := 0
  else
    C8 := C6 / C4;
  if C0 = 0 then
    C9 := 0
  else
    C9 := C7 / C0;
  //
  Result := C8 * C9;
end;

{
procedure TFmSimulaParcela.JurosEValorTotalAPagar_SemPrestacoes(const Princ,
  TaxaM, Meses: Double; var Total, Juros: Double);
begin
(*
  Princ := EdValor.ValueVariant;
  TaxaM := EdTaxa.ValueVariant;
  Meses := EdParcelas.ValueVariant;
  //
*)
(*
S = P (1 + i)n

onde S = montante,
P = principal,
i = taxa de juros e
n = n�mero de per�odos que o principal P (capital inicial) foi aplicado.

Montante = Principal * (1 + (TaxaM / 100))n
*)
  Juros := (Power(1 + (TaxaM / 100), Meses));
  Total := Princ * Juros;
end;
}

function TFmSimulaParcela.ValorParcela(DataExe, DataI: TDateTime;
  Principal, JurosM: Double; Parcelas: Integer; SoDiaUtil: Boolean): Double;
const
  Tipo = 0;
var
  ValParc, Juros, ValI, ValOrig: Double;
  I, Casas: Integer;
  //Data: TDateTime;
  Teste: Double;
  Parcela: Integer;
begin
  ValParc := Principal / Parcelas;
  if JurosM <= 0 then
  begin
    Result := Geral.RoundC(ValParc, 2);
    Exit;
  end;
  //Juros := 0;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Casas := Length(IntToStr(Trunc(ValParc)));
  //////////////////////////////////////////////////////////////////////////////
  // Maior valor abaixo da parcela para dar in�cio ao c�lculo
  ValI := Power(10, Casas-1);
  ValParc := 0;
  //Teste :=  Geral.RoundC(Principal / Parcelas, 2);
  //Teste :=  Trunc((Principal / Parcelas * 100) + 0.9) / 100;
  Teste :=  Principal / Parcelas;
  while ValParc < Teste do
    ValParc := ValParc + ValI;
  ValParc := ValParc - ValI;
  //
  for i := Casas-1 downto -4 do
  begin
    ValI := Power(10, i);
    Juros := 0;
    while Juros < JurosM do
    begin
      ValParc := ValParc + ValI;
      Juros := MLAGeral.JuroMensal(DataExe, DataI, Parcelas, Principal, ValParc, SoDiaUtil);
      IncrementaCalculo(1, 4); // 4
    end;
    ValParc := ValParc - ValI;
  end;
  //////////////////////////////////////////////////////////////////////////////
  //Data := DataI;
  for Parcela := 1 to Parcelas do
  begin
    ValOrig := Trunc((Principal / Parcelas * 100) + 0.9) / 100;

    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, 'simulapg', False, [
      'ValParc', 'ValOrig'
    ], [
      'Tipo', 'Parcela'
    ], [
      ValParc, ValOrig
    ], [
      Tipo, Parcela], False);
  end;
  Result := ValParc;
end;

function TFmSimulaParcela.ValorParcelaData(DataExe, DataI: TDateTime;
  Principal, JurosM: Double; Parcelas: Integer; SoDiaUtil: Boolean): Double;
var
  ValParc, Juros, ValI: Double;
  i, Casas, Parcela: Integer;
begin
  ValParc := Trunc((Principal / Parcelas*100) + 0.9) / 100;
  if JurosM <= 0 then
  begin
    Result := ValParc;
    Exit;
  end;
  //Juros := 0;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Casas := Length(IntToStr(Trunc(ValParc)));
  //////////////////////////////////////////////////////////////////////////////
  // Maior valor abaixo da parcela para dar in�cio ao c�lculo
  ValI := Power(10, Casas-1);
  ValParc := 0;
  while ValParc < Trunc((Principal / Parcelas*100)+0.9)/100 do
    ValParc := ValParc + ValI;
  ValParc := ValParc - ValI;
  //
  for i := Casas-1 downto -4 do
  begin
    ValI := Power(10, i);
    Juros := 0;
    while Juros < JurosM do
    begin
      ValParc := ValParc + ValI;
      Juros := MLAGeral.JuroMensal(DataExe, DataI, Parcelas, Principal, ValParc, SoDiaUtil);
      IncrementaCalculo(1, 0);
    end;
    ValParc := ValParc - ValI;
  end;
  //////////////////////////////////////////////////////////////////////////////
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE ' + FSimulaPg + ' SET DataPg=:P0, ');
  Dmod.QrUpdL.SQL.Add('ValParc=:P1, ValOrig=:P2, ValPrin=:P3, ');
  Dmod.QrUpdL.SQL.Add('ValJuro=:P4, PerJuro=:P5 ');
  Dmod.QrUpdL.SQL.Add('WHERE Tipo=0 AND Parcela=:P6');
  //
  Parcela := 0;
  //
  ReabreQr2(1);
  Qr2.First;
  while not Qr2.Eof do
  begin
    Parcela := Parcela + 1;
    Dmod.QrUpdL.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, Qr2DataPg.Value);
    Dmod.QrUpdL.Params[1].AsFloat   := ValParc;
    Dmod.QrUpdL.Params[2].AsFloat   := Trunc((Principal / Parcelas*100)+0.9)/100;
    Dmod.QrUpdL.Params[3].AsFloat   := 0;
    Dmod.QrUpdL.Params[4].AsFloat   := 0;
    Dmod.QrUpdL.Params[5].AsFloat   := 0;
    Dmod.QrUpdL.Params[6].AsInteger := Parcela;
    Dmod.QrUpdL.ExecSQL;
    //
    Qr2.Next;
  end;
  Result := ValParc;
end;

procedure TFmSimulaParcela.CalculaDiasCH(Data, Vcto: TDateTime; Valor: Double;
  Parcelas: Integer);
var
  Comp, DMai: Integer;
  TaxaT, JuroT: Double;
begin
  Vcto := Int(Vcto);
  Data := Int(Data);
  DMai := EdDMais.ValueVariant;
  Comp := FmPrincipal.VerificaDiasCompensacao(EdPraca.ValueVariant,
          0(*CBE*), Valor, Data, Vcto, 0 (*SCB*));
  FDias := UMyMod.CalculaDias(Int(Data), Int(Vcto), DMai, Comp,
          Dmod.QrControleTipoPrazoDesc.Value, 0(*CBE*));
  FCompensacao := Comp;
  FPrazo := Trunc(Int(Vcto)-Int(Data));
  //Parcelas := EdParcelas.ValueVariant;
  //
  TaxaT := Geral.DMV(EdTaxa.Text);
  JuroT := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(TaxaT, FDias);
  FJuro := JuroT;
  FValr := JuroT * Qr2ValParc.Value / 100;
  FValF := Qr2ValParc.Value / (1 + (JuroT / 100));
end;

procedure TFmSimulaParcela.CalculaIOF(Parcelas: Integer; SoDiaUtil: Boolean);
  function ResidualPositivo(const Fator: Integer; var Ant, New, R: Double): Boolean;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLast, DModG.MyPID_DB, [
    'SELECT *',
    'FROM _iof_',
    'WHERE Parcela=' + FormatFloat('0', Parcelas),
    '']);
    //
    Result := QrLastBaseCalc.Value >= 0;
    //
    if Result then
    begin
      Ant := New;
      New := New + Power(10, Fator);
    end else
    begin
      // nada?
    end;
  end;
{
  procedure InsereNaTabela(Hoje, DtaPag: TDateTime; Parcela, Tipo: Integer);
  var
    Dias: Integer;
    PercIOF: Double;
    DataPg: String;
  begin
    DataPg := Geral.FDT(DtaPag, 1);
    Dias := Trunc(DtaPag) - Trunc(Hoje) - 1;
    if Dias = -1 then
      Dias := 0;
    PercIOF := Geral.RoundC(EdIOF_D.ValueVariant * Dias, 2) + EdIOF_F.ValueVariant;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSimulaPg, False, [
    'Tipo', 'Parcela', 'DataPg',
    'PercIOF','Dias'], [
    ], [
    Tipo, Parcela, DataPg,
    PercIOF, Dias], [
    ], False);
  end;
}
var
  ValParc, Total, Juros, ValI, ValOrig: Double;
  I, Casas, J, N: Integer;
  Data, DataI, Hoje: TDateTime;
  Teste: Double;
  Parcela, Dias, Util, DiasParcel, DiasCorrid: Integer;
  BaseCalc, MesesPerio, FatorTaxa, FatorJuros, ValCorrigi, ValPrincip, Taxa,
  ValJuros, IOF_Percen, IOF_Valor, IOF_Total, temp, PrestacaoAnt, PrestacaoNew,
  NovoIOF: Double;
  Vencimento: String;
  //
  Tipo, C: Integer;
  DataPg: String;
  PercIOF, ValrIOF, Prestacao, Residual, A, R: Double;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Calculando valor do IOF');

  //////////////////////////////////////////////////////////////////////////////
  ///                                                                        ///
  ///  Baseado em simula��o no site do HSBC em 29/08/2011                    ///
  ///  Arquivo teste em excel:                                               ///
  ///  C:\Projetos_Aux\_Manuais e Tutoriais\Juros\Teste HSBC 2011_08_29.xlsx ///
  ///                                                                        ///
  //////////////////////////////////////////////////////////////////////////////

  FIOF := UCriar.RecriaTempTableNovo(ntrttIOF, DmodG.QrUpdPID1, False);
  UMyMod.ExcluiRegistrosDeTodaTabela(DModG.QrUpdPID1, '', FSimulaPg);
  ///
  Hoje := Int(TPData.Date);
  DataI := Int(TPPrimeiroDia.Date);
  Data := DataI;
  //

  DiasCorrid := 0;
  Taxa := EdTaxa.ValueVariant;
  FatorTaxa := 1 + (Taxa / 100);

  //
  // Parcela zero
  Parcela := 0;
  Total := EdPrincipal.ValueVariant + EdOutros.ValueVariant;
  BaseCalc := Total;
  IOF_Percen := EdIOF_F.ValueVariant;
  IOF_Valor := BaseCalc * IOF_Percen / 100;
  IOF_Total := IOF_Valor;
  BaseCalc := BaseCalc + IOF_Valor;
  Prestacao := 0;
  //
  IncrementaCalculo(1, 1);  //1
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FIOF, False, [
  'Parcela', 'BaseCalc', (*'Vencimento',
  'DiasParcel', 'DiasCorrid', 'MesesPerio',
  'FatorJuros', 'ValCorrigi', 'ValPrincip',
  'ValJuros',*) 'IOF_Percen', 'IOF_Valor',
  'Prestacao'], [
  ], [
  Parcela, BaseCalc, (*Vencimento,
  DiasParcel, DiasCorrid, MesesPerio,
  FatorJuros, ValCorrigi, ValPrincip,
  ValJuros,*) IOF_Percen, IOF_Valor,
  Prestacao], [
  ], False);

  // Ainda n�o tem a m�dia de dias
  PrestacaoAnt := ValorPrestacao_Aproximado(BaseCalc, Taxa, Parcelas * 30 / 2);
  //PrestacaoAnt := 0;

  N := Length(FormatFloat('0', Trunc(PrestacaoAnt)));
  for I := 1 to Parcelas do
  begin
    IncrementaCalculo(1, 2);    //2
    Parcela := I;
    if SoDiaUtil then
    begin
      Util := UMyMod.DiaInutil(Data);
      while Util > 0 do
      begin
        Data := Data + 1;
        Util := UMyMod.DiaInutil(Data);
      end;
    end;
    // Predefine Parcela
    Vencimento := Geral.FDT(Data, 1);
    Dias := Trunc(Data) - Trunc(Hoje);
    if Dias = -1 then
      Dias := 0;
    DiasParcel := Dias - DiasCorrid;
    DiasCorrid := Dias;
    MesesPerio := Geral.RoundC(DiasParcel / 30, 4);
    FatorJuros := Power(FatorTaxa, MesesPerio);
    ValCorrigi := BaseCalc * FatorJuros;
    temp := BaseCalc;
    BaseCalc := ValCorrigi - PrestacaoAnt;

    ValPrincip := temp - BaseCalc;
    ValJuros   := ValCorrigi - temp;
    // IOF
    IOF_Percen := DiasCorrid * EdIOF_D.ValueVariant;
    if CkIOF_M.Checked and (IOF_Percen > EdIOF_M.ValueVariant) then
      IOF_Percen := EdIOF_M.ValueVariant;
    IOF_Valor := Geral.RoundC(IOF_Percen * ValPrincip / 100, 2);
    IOF_Total := IOF_Total + IOF_Valor;
    Prestacao := PrestacaoAnt;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FIOF, False, [
    'Parcela', 'BaseCalc', 'Vencimento',
    'DiasParcel', 'DiasCorrid', 'MesesPerio',
    'FatorJuros', 'ValCorrigi', 'ValPrincip',
    'ValJuros', 'IOF_Percen', 'IOF_Valor',
    'Prestacao'], [
    ], [
    Parcela, BaseCalc, Vencimento,
    DiasParcel, DiasCorrid, MesesPerio,
    FatorJuros, ValCorrigi, ValPrincip,
    ValJuros, IOF_Percen, IOF_Valor,
    Prestacao], [
    ], False);
    //
    Data := IncMonth(DataI, I);
  end;
  //
  Total := Total + IOF_Total;
  QrMedia.Close;
  QrMedia.Params[0].AsString := Geral.FDT(Int(TPData.Date), 1);
  QrMedia.Open;
  //
  PrestacaoAnt := ValorPrestacao_Aproximado(Total, Taxa, QrMediaDias.Value);
  Residual := RecalculaIOF(Parcelas, Total, Taxa, PrestacaoAnt);
  //

  PrestacaoNew := PrestacaoAnt;
  QrLast.Close;
  QrLast.Database := DModG.MyPID_DB;
  for J := N downto -4 do
  begin
    R := 0;
    A := 0;
    C := 0;
    while ResidualPositivo(J, PrestacaoAnt, PrestacaoNew, R) do
    //while Residual > 0 do
    begin
{
      PrestacaoAnt := PrestacaoNew;
      PrestacaoNew := PrestacaoNew + Power(10, J);
      Residual := RecalculaIOF(Parcelas, Total, Taxa, PrestacaoNew);
}
      RecalculaIOF(Parcelas, Total, Taxa, PrestacaoNew);
      if (C > 0) and (A > R) then
      begin
        ShowMessage('Erro!');
      end;
      A := R;
    end;
    PrestacaoNew := PrestacaoAnt;
    RecalculaIOF(Parcelas, Total, Taxa, PrestacaoNew);
  end;
  //ShowMessage('Nova presta��o: ' + FormatFloat('#,###,###,##0.00', PrestacaoNew));
  QrSoma.Close;
  QrSoma.Open;
  //NovoIOF := QrSomaIOF_Valor.Value;
  while QrSomaIOF_Valor.Value > IOF_Total do
  begin
    Total := Total - IOF_Total + QrSomaIOF_Valor.Value;
    IOF_Total := QrSomaIOF_Valor.Value;
    for J := N downto -4 do
    begin
      while ResidualPositivo(J, PrestacaoAnt, PrestacaoNew, R) do
      begin
        RecalculaIOF(Parcelas, Total, Taxa, PrestacaoNew);
      end;
      PrestacaoNew := PrestacaoAnt;
      RecalculaIOF(Parcelas, Total, Taxa, PrestacaoNew);
    end;
    QrSoma.Close;
    QrSoma.Open;
  end;
  EdIOFVal.ValueVariant := IOF_Total;
  DefineValorFinanciado();
  UnDmkDAC_PF.AbreMySQLQuery0(QrIOF, DModG.MyPID_DB, [
  'SELECT *',
  'FROM _iof_',
  'ORDER BY Parcela',
  '']);
  QrIOF.First;
  while not QrIOF.Eof do
  begin
    if QrIOFParcela.Value > 0 then
    begin
      //
      Tipo := 0;
      Parcela := QrIOFParcela.Value;
      DataPg := Geral.FDT(QrIOFVencimento.Value, 1);
      Dias := QrIOFDiasCorrid.Value;
      PercIOF := QrIOFIOF_Percen.Value;
      ValrIOF := QrIOFIOF_Valor.Value;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSimulaPG, False, [
      'Tipo', 'Parcela', 'DataPg',
      (*'PerJuro', 'ValParc', 'ValOrig',
      'ValPrin', 'ValJuro',*) 'PercIOF',
      'ValrIOF', 'Dias'], [
      ], [
      Tipo, Parcela, DataPg,
      (*PerJuro, ValParc, ValOrig,
      ValPrin, ValJuro,*) PercIOF,
      ValrIOF, Dias], [
      ], False);
    end;
    //
    QrIOF.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Valor do IOF calculado!');
end;

function TFmSimulaParcela.RecalculaIOF(Parcelas: Integer; Base, Taxa,
Prestacao: Double): Double;
var
  Parcela: Integer;
  BaseCalc, ValCorrigi, temp, ValPrincip, ValJuros, IOF_Valor,
  IOF_Total: Double;
begin
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FIOF, False, [
  'BaseCalc'], ['Parcela'], [Base], [0], False);
  //
  QrIOF.Close;
  QrIOF.Database := DModG.MyPID_DB;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrIOF, DModG.MyPID_DB, [
  'SELECT *',
  'FROM _iof_',
  'WHERE Parcela > 0',
  'ORDER BY Parcela',
  '']);
  //
  BaseCalc := Base;
  //
  IOF_Total := 0;
  QrIOF.First;
  while not QrIOF.Eof do
  begin
    Parcela := QrIOFParcela.Value;
    //
    ValCorrigi := BaseCalc * QrIOFFatorJuros.Value;
    temp := BaseCalc;
    BaseCalc := ValCorrigi - Prestacao;

    ValPrincip := temp - BaseCalc;
    ValJuros   := ValCorrigi - temp;
    // IOF
    IOF_Valor := Geral.RoundC(QrIOFIOF_Percen.Value * ValPrincip / 100, 2);
    IOF_Total := IOF_Total + IOF_Valor;
    //
    IncrementaCalculo(1, 3);  //3
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FIOF, False, [
    'BaseCalc', 'ValCorrigi', 'ValPrincip',
    'ValJuros', 'IOF_Valor', 'Prestacao'], [
    'Parcela'], [
    BaseCalc, ValCorrigi, ValPrincip,
    ValJuros, IOF_Valor, Prestacao], [
    Parcela], False);
    //
    QrIOF.Next;
  end;
  Result := BaseCalc;
end;


procedure TFmSimulaParcela.SetaDadosIOF(PJ: Boolean);
begin
  if PJ then
  begin
    EdIOF_F.ValueVariant := Dmod.QrControleIOF_F_PJ.Value;
    EdIOF_D.ValueVariant := Dmod.QrControleIOF_D_PJ.Value;
    EdIOF_M.ValueVariant := Dmod.QrControleIOF_Max_Per_PJ.Value;
    CkIOF_M.Checked := Dmod.QrControleIOF_Max_Usa_PJ.Value = 1;
  end else begin
    EdIOF_F.ValueVariant := Dmod.QrControleIOF_F_PF.Value;
    EdIOF_D.ValueVariant := Dmod.QrControleIOF_D_PF.Value;
    EdIOF_M.ValueVariant := Dmod.QrControleIOF_Max_Per_PF.Value;
    CkIOF_M.Checked := Dmod.QrControleIOF_Max_Usa_PF.Value = 1;
  end;
end;

procedure TFmSimulaParcela.SimulaBordero(Tipo: Integer);
var
  ValPrin, PerJuro, ValJuro(*, PercIOF, ValrIOF, Total*): Double;
  Parcela: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando parcelas "' +
  DescricaoDeTipoDeSimulacao(Tipo) + '"!');
  if ReabreQr2(Tipo) then
  begin
    //Total := EdPrincipal.ValueVariant + EdOutros.ValueVariant + EdIOFVal.ValueVariant;
    while not Qr2.Eof do
    begin
      AtualizaItem();
      PerJuro := FJuro;
      if Tipo = 1 then
      begin
        ValJuro := FValr;
        ValPrin := Geral.RoundC(Qr2ValParc.Value - FValr, 2);
      end else begin
        ValJuro := Qr2ValParc.Value - FValF;
        ValPrin := Geral.RoundC(FValF, 2);
      end;
      //
      Parcela := Qr2Parcela.Value;
      (*
      PercIOF := Qr2PercIOF.Value;
      ValrIOF := Geral.RoundC(ValPrin / Total * EdIOFVal.ValueVariant, 2);
      *)
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, 'simulapg', False, [
      'ValPrin', 'PerJuro', 'ValJuro'], [
      'Tipo', 'Parcela'], [
      ValPrin, PerJuro, ValJuro
      ], [Tipo, Parcela], False);
      //
      Qr2.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Parcelas de ' +
    DescricaoDeTipoDeSimulacao(Tipo) + ' geradas!');
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Parcelas "' +
    DescricaoDeTipoDeSimulacao(Tipo) + '" N�O geradas!');
  //
  ReopenSimulaPg(True);
end;

procedure TFmSimulaParcela.SpeedButton1Click(Sender: TObject);
begin
//
end;

procedure TFmSimulaParcela.TbSimula1BeforePost(DataSet: TDataSet);
var
  Data: TDateTime;
begin
  Data := TbSimula1DataPg.Value;
  if (TbSimula1Tipo.Value = 1) and (TbSimula1Parcela.Value = 1) then
    TPPrimeiroDia.Date := Data;
  if CkDiasUteis.Checked = True then
  begin
    if (DayOfWeek(Data) = 1) then TbSimula1DataPg.Value := Data + 1;
    if (DayOfWeek(Data) = 7) then TbSimula1DataPg.Value := Data + 2;
  end;
end;

procedure TFmSimulaParcela.TbSimula1DataPgChange(Sender: TField);
begin
  BtCalcula.Enabled  := False;
  BtReabre.Visible   := True;
end;

procedure TFmSimulaParcela.TbSimula1DataPgValidate(Sender: TField);
begin
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Parcela, DataPg');
  QrPesq.SQL.Add('FROM ' + FSimulaPg);
  QrPesq.SQL.Add('WHERE Tipo=1');
  QrPesq.SQL.Add('AND (');
  QrPesq.SQL.Add('     (Parcela > :P0 AND DataPg < :P1)');
  QrPesq.SQL.Add('  OR');
  QrPesq.SQL.Add('     (Parcela < :P2 AND DataPg > :P3)');
  QrPesq.SQL.Add(')');
  QrPesq.Params[0].AsInteger := TbSimula1Parcela.Value;
  QrPesq.Params[1].AsDate    := TField(Sender).Value;
  QrPesq.Params[2].AsInteger := TbSimula1Parcela.Value;
  QrPesq.Params[3].AsDate    := TField(Sender).Value;
  QrPesq.Open;
  //
  if QrPesq.RecordCount > 0 then
  begin
    raise Exception.Create('Data atual deve ser superior a data anterior!');
    Exit;
  end;    
end;

procedure TFmSimulaParcela.TbSimula1NewRecord(DataSet: TDataSet);
begin
  TbSimula1.Cancel;
end;

procedure TFmSimulaParcela.AtualizaItem();
var
  Vence, Data_: TDateTime;
  Parcelas: Integer;
  Valor: Double;
  DDeposito, DCompra, Vencto: String;
begin
  Screen.Cursor := crHourGlass;
  try
    //Erros   := 0;
    Parcelas := 0;
    Valor   := EdValor.ValueVariant;
    Vence   := Qr2DataPg.Value;
    Vencto  := FormatDateTime(VAR_FORMATDATE, Vence);
    Data_   := Int(TPData.Date);
    DCompra := FormatDateTime(VAR_FORMATDATE, Data_);
    //Praca   := EdPraca.ValueVariant;
    //Comp    := FmPrincipal.VerificaDiasCompensacao(Praca, 0 (*CBE*),
      //         Valor, Data_, Vence, 0 (*SCB*));
    CalculaDiasCH(Data_, Vence, Valor, Parcelas);
    //DMais := EdDMais.ValueVariant;
    DDeposito := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
    //
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

function TFmSimulaParcela.ReabreQr2(Tipo: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM simulapg ',
  'WHERE Tipo=' + FormatFloat('0', Tipo),
  '']);
  //
  Result := Qr2.RecordCount > 0;
end;

procedure TFmSimulaParcela.ReopenSimulaPg(Reopen: Boolean);
begin
  QrSimula0.Close;
  TbSimula1.Close;
  QrSimula2.Close;
  QrSum0.Close;
  QrSum1.Close;
  QrSum2.Close;
  QrMedia.Close;
  if Reopen then
  begin
    QrMedia.Params[0].AsString := Geral.FDT(Int(TPData.Date), 1);
    QrMedia.Open;
    QrSimula0.Open;
    TbSimula1.Open;
    QrSimula2.Open;
    QrSum0.Open;
    QrSum1.Open;
    QrSum2.Open;
  end;
end;

procedure TFmSimulaParcela.QrIOFCalcFields(DataSet: TDataSet);
begin
  QrIOFVCTO_TXT.Value := Geral.FDT(QrIOFVencimento.Value, 3, True);
end;

procedure TFmSimulaParcela.QrMediaCalcFields(DataSet: TDataSet);
var
  Taxa: Double;
begin
  Taxa := Geral.DMV(EdTaxa.Text);
  //Valor := EdValor.ValueVariant;
  //Parcelas := EdParcelas.ValueVariant;
  QrMediaJUROS.Value := Geral.RoundC(Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa,
    QrMediaDias.Value), 4);
  //QrMediaPARCELA.Value := (Valor / Parcelas) / (1 - (QrMediaJUROS.Value/100));
end;

procedure TFmSimulaParcela.CriaBordero1();
const
  ValPrin = 0.00;
  ValJuro = 0.00;
var
  Tipo: Integer;
  DataPg: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando parcelas de Border�!');
  if ReabreQr2(0) then
  begin
    Qr2.First;
  {
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO ' + FSimulaPg + ' SET DataPg=:P0, Parcela=:P1, ');
    Dmod.QrUpdL.SQL.Add('ValParc=:P2, ValOrig=:P3, ValPrin=:P4, ');
    Dmod.QrUpdL.SQL.Add('ValJuro=:P5, PerJuro=:P6, Tipo=:P7');
    Dmod.QrUpdL.SQL.Add('');
  }
    while not Qr2.Eof do
    begin
  {
      Dmod.QrUpdL.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, Qr2DataPg.Value);
      Dmod.QrUpdL.Params[1].AsInteger := Qr2Parcela.Value;
      Dmod.QrUpdL.Params[2].AsFloat   := Qr2ValParc.Value;
      Dmod.QrUpdL.Params[3].AsFloat   := Qr2ValOrig.Value;
      Dmod.QrUpdL.Params[4].AsFloat   := 0;
      Dmod.QrUpdL.Params[5].AsFloat   := 0;
      Dmod.QrUpdL.Params[6].AsFloat   := Qr2Perjuro.Value;
      Dmod.QrUpdL.Params[7].AsInteger := 1;
      Dmod.QrUpdL.ExecSQL;
      //
      Dmod.QrUpdL.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, Qr2DataPg.Value);
      Dmod.QrUpdL.Params[1].AsInteger := Qr2Parcela.Value;
      Dmod.QrUpdL.Params[2].AsFloat   := Qr2ValParc.Value;
      Dmod.QrUpdL.Params[3].AsFloat   := Qr2ValOrig.Value;
      Dmod.QrUpdL.Params[4].AsFloat   := 0;
      Dmod.QrUpdL.Params[5].AsFloat   := 0;
      Dmod.QrUpdL.Params[6].AsFloat   := Qr2Perjuro.Value;
      Dmod.QrUpdL.Params[7].AsInteger := 2;
      Dmod.QrUpdL.ExecSQL;
      //
  }
      for Tipo := 1 to 2 do
      begin
        DataPg := Geral.FDT(Qr2DataPg.Value, 1);
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'simulapg', False, [
        'Tipo', 'Parcela', 'DataPg',
        'PerJuro', 'ValParc', 'ValOrig',
        'ValPrin', 'ValJuro', 'PercIOF',
        'ValrIOF', 'Dias'], [
        ], [
        Tipo, Qr2Parcela.Value, DataPg,
        Qr2PerJuro.Value, Qr2ValParc.Value, Qr2ValOrig.Value,
        ValPrin, ValJuro, Qr2PercIOF.Value,
        Qr2ValrIOF.Value, Qr2Dias.Value], [
        ], False);
      end;
      Qr2.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Parcelas de Border� geradas!');
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Parcelas de Border� N�O geradas!');
end;

procedure TFmSimulaParcela.CriaBordero1Data;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando parcelas de Border� (1 Data)!');
  if ReabreQr2(0) then
  begin
    Qr2.First;
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('UPDATE ' + FSimulaPg + ' SET DataPg=:P0, Parcela=:P1, ');
    Dmod.QrUpdL.SQL.Add('ValParc=:P2, ValOrig=:P3, ValPrin=:P4, ');
    Dmod.QrUpdL.SQL.Add('ValJuro=:P5, PerJuro=:P6 ');
    Dmod.QrUpdL.SQL.Add('WHERE Tipo=:P7 AND Parcela=:P8');
    Dmod.QrUpdL.SQL.Add('');
    while not Qr2.Eof do
    begin
      Dmod.QrUpdL.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, Qr2DataPg.Value);
      Dmod.QrUpdL.Params[1].AsInteger := Qr2Parcela.Value;
      Dmod.QrUpdL.Params[2].AsFloat   := Qr2ValParc.Value;
      Dmod.QrUpdL.Params[3].AsFloat   := Qr2ValOrig.Value;
      Dmod.QrUpdL.Params[4].AsFloat   := 0;
      Dmod.QrUpdL.Params[5].AsFloat   := 0;
      Dmod.QrUpdL.Params[6].AsFloat   := Qr2Perjuro.Value;
      Dmod.QrUpdL.Params[7].AsInteger := 1;
      Dmod.QrUpdL.Params[8].AsInteger := Qr2Parcela.Value;
      Dmod.QrUpdL.ExecSQL;
      //
      //
      Dmod.QrUpdL.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, Qr2DataPg.Value);
      Dmod.QrUpdL.Params[1].AsInteger := Qr2Parcela.Value;
      Dmod.QrUpdL.Params[2].AsFloat   := Qr2ValParc.Value;
      Dmod.QrUpdL.Params[3].AsFloat   := Qr2ValOrig.Value;
      Dmod.QrUpdL.Params[4].AsFloat   := 0;
      Dmod.QrUpdL.Params[5].AsFloat   := 0;
      Dmod.QrUpdL.Params[6].AsFloat   := Qr2Perjuro.Value;
      Dmod.QrUpdL.Params[7].AsInteger := 2;
      Dmod.QrUpdL.Params[8].AsInteger := Qr2Parcela.Value;
      Dmod.QrUpdL.ExecSQL;
      //
      Qr2.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Parcelas de Border� (1 Data) geradas!');
  end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Parcelas de Border� (1 Data) N�O geradas!');
end;

procedure TFmSimulaParcela.DefineValorFinanciado;
begin
  EdValor.ValueVariant :=
  EdPrincipal.ValueVariant +
  EdOutros.ValueVariant +
  EdIOFVal.ValueVariant;
end;

function TFmSimulaParcela.DescricaoDeTipoDeSimulacao(Tipo: Integer): String;
begin
  case Tipo of
    0: Result := 'Comercial';
    1: Result := 'Banc�rio progressivo';
    2: Result := 'Banc�rio regressivo';
    else Result := '(tipo desconhecido)';
  end;
end;

procedure TFmSimulaParcela.EdIOFValChange(Sender: TObject);
begin
  DefineValorFinanciado();
end;

procedure TFmSimulaParcela.EdOutrosChange(Sender: TObject);
begin
  DefineValorFinanciado();
  HabilitaCalcula(Sender);
end;

procedure TFmSimulaParcela.EdPrincipalChange(Sender: TObject);
begin
  DefineValorFinanciado();
  HabilitaCalcula(Sender);
end;

procedure TFmSimulaParcela.EdTaxaChange(Sender: TObject);
begin
  if RGTipo.ItemIndex <> 1 then
    HabilitaCalcula(Sender);
end;

procedure TFmSimulaParcela.EdValorParcelaChange(Sender: TObject);
begin
  if RGTipo.ItemIndex <> 2 then
    HabilitaCalcula(Sender);
end;

procedure TFmSimulaParcela.CalculaParcelaBancaria();
var
  ValPar, ValLiq, ValIni, ValI, ValJur: Double;
  i, Casas, Parcelas: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando parcelas banc�rias!');
  if ReabreQr2(1) then
  begin
    Qr2.Last;
    if Qr2PerJuro.Value > 100 then
    begin
      PnBanco.Visible := False;
      while not Qr2.Bof do
      begin
        if Qr2PerJuro.Value <= 100 then
        begin
          Panel3.Caption := 'N�mero de parcelas excedeu o m�ximo! ('+
          IntToStr(Qr2Parcela.Value)+')';
          Break;
        end;
        Qr2.Prior;
      end;
      Exit;
    end else begin
      PnBanco.Visible := True;
    end;
    ValLiq := 0;
    Parcelas := EdParcelas.ValueVariant;
    ValIni := EdValor.ValueVariant;
    //ValIni := ValTot / Parcelas;
    ValPar := Trunc((ValIni / Parcelas*100)+0.9)/100;
    //JurosM := Geral.DMV(EdTaxa.Text);
    Casas := Length(IntToStr(Trunc(ValPar)));
    //DtE := Trunc(Int(TPData.Date));
    //DtI := Trunc(Int(TPPrimeiroDia.Date));
    //
    Qr2.First;
    for i := Casas-1 downto -4 do
    begin
      ValI := Power(10, i);
      while ValLiq < ValIni do
      begin
        if ValLiq < 0 then
        begin
          Application.MessageBox('Valor l�quido negativo!', 'Erro', MB_OK+MB_ICONERROR);
          DModG.QrUpdPID1.SQL.Clear;
          DModG.QrUpdPID1.SQL.Add('DELETE FROM ' + FSimulaPg + ' WHERE Tipo=1');
          DModG.QrUpdPID1.ExecSQL;
          Exit;
        end;
        ValLiq := 0;
        ValPar := ValPar + ValI;
        Qr2.First;
        //LiqSub := 0;
        while not Qr2.Eof do
        begin
          IncrementaCalculo(1, 6); // 6
          // PerJuro Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Prazo);
          ValJur := ValPar * Qr2PerJuro.Value / 100;
          ValLiq := ValLiq + (ValPar - ValJur);
          Qr2.Next;
        end;
      end;
      ValLiq := 0;
      ValPar := ValPar - ValI;
    end;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE ' + FSimulaPg + ' SET ValParc=:P0 WHERE Tipo=1');
    DModG.QrUpdPID1.Params[0].AsFloat   := ValPar;
    DModG.QrUpdPID1.ExecSQL;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Parcelas banc�rias geradas!');
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Parcelas banc�rias N�O geradas!');
end;

procedure TFmSimulaParcela.CalculaParcelaInfinita();
var
  ValPar, ValLiq, ValIni, ValI, ValJur: Double;
  i, Casas, Parcelas: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando parcelas infinitas!');
  if ReabreQr2(2) then
  begin
    Qr2.Last;
    ValLiq   := 0;
    Parcelas := EdParcelas.ValueVariant;
    ValIni   := EdValor.ValueVariant;
    ValPar   := Trunc((ValIni / Parcelas*100)+0.9)/100;
    //JurosM   := Geral.DMV(EdTaxa.Text);
    Casas    := Length(IntToStr(Trunc(ValPar)));
    //DtE      := Trunc(Int(TPData.Date));
    //DtI      := Trunc(Int(TPPrimeiroDia.Date));
    //
    Qr2.First;
    for i := Casas-1 downto -4 do
    begin
      ValI := Power(10, i);
      while ValLiq < ValIni do
      begin
        if ValLiq < 0 then
        begin
          Application.MessageBox('Valor l�quido negativo!', 'Erro', MB_OK+MB_ICONERROR);
          DModG.QrUpdPID1.SQL.Clear;
          DModG.QrUpdPID1.SQL.Add('DELETE FROM ' + FSimulaPg + ' WHERE Tipo=2');
          DModG.QrUpdPID1.ExecSQL;
          Exit;
        end;
        ValLiq := 0;
        ValPar := ValPar + ValI;
        Qr2.First;
        //LiqSub := 0;
        while not Qr2.Eof do
        begin
          IncrementaCalculo(1, 5);  // 5
          // PerJuro Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Prazo);
          ValJur := ValPar / (1+ (Qr2PerJuro.Value / 100));
          ValLiq := ValLiq + ValJur;
          Qr2.Next;
        end;
      end;
      ValLiq := 0;
      ValPar := ValPar - ValI;
    end;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE ' + FSimulaPg + ' SET ValParc=:P0 WHERE Tipo=2');
    DModG.QrUpdPID1.Params[0].AsFloat   := ValPar;
    DModG.QrUpdPID1.ExecSQL;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Parcelas infinitas geradas!');
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Parcelas infinitas N�O geradas!');
end;

(*
Esque�a a taxa de juros e fixe no CET. Como voc� bem disse, o CET era de 20,12.
Vamos compar�-lo com a sua tabela price, que eu acho que foi calculada com base nas suas presta��es.
Trazendo o CET para um m�s, calcula-se assim: 1 + 20,12/100 = 1,2012
Eleva-se 1,2012 a (1/12), que pode ser feito no Excel com a seguinte f�rmula: =1,2012^(1/12)
O resultado � 1,015394, isto �, 1,54%
Portanto, bate certinho com sua apura��o.
O banco cobrou conforme declarou.
Moral da hist�ria: esque�a os juros e foque sempre no CET.
Abra�o do Beto

*)

end.

