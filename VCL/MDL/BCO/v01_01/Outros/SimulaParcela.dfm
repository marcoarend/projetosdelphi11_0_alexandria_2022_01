object FmSimulaParcela: TFmSimulaParcela
  Left = 404
  Top = 200
  Caption = 'CDT-SIMUL-001 :: Simula'#231#227'o de Parcela'
  ClientHeight = 692
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 263
        Height = 32
        Caption = 'Simula'#231#227'o de Parcela'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 263
        Height = 32
        Caption = 'Simula'#231#227'o de Parcela'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 263
        Height = 32
        Caption = 'Simula'#231#227'o de Parcela'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 530
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 1
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 108
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object RGTipo: TRadioGroup
        Left = 0
        Top = 0
        Width = 76
        Height = 108
        Align = alLeft
        Caption = '  C'#225'lculo: '
        ItemIndex = 2
        Items.Strings = (
          '?'
          'Taxa'
          '$ Parcela')
        TabOrder = 0
        TabStop = True
        OnClick = RGTipoClick
      end
      object GroupBox2: TGroupBox
        Left = 76
        Top = 0
        Width = 392
        Height = 108
        Align = alLeft
        Caption = ' Dados para a simula'#231#227'o: '
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 20
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label3: TLabel
          Left = 8
          Top = 60
          Width = 96
          Height = 13
          Caption = 'Primeiro pagamento:'
        end
        object LaDeb: TLabel
          Left = 116
          Top = 20
          Width = 43
          Height = 13
          Caption = 'Principal:'
        end
        object Label14: TLabel
          Left = 116
          Top = 60
          Width = 34
          Height = 13
          Caption = 'Outros:'
        end
        object Label10: TLabel
          Left = 200
          Top = 20
          Width = 63
          Height = 13
          Caption = '% Juros/m'#234's:'
        end
        object LaNF: TLabel
          Left = 200
          Top = 60
          Width = 57
          Height = 13
          Caption = 'N'#186' de parc.:'
        end
        object Label15: TLabel
          Left = 304
          Top = 20
          Width = 64
          Height = 13
          Caption = '$ Financiado:'
          Enabled = False
        end
        object LaCred: TLabel
          Left = 304
          Top = 60
          Width = 47
          Height = 13
          Caption = '$ parcela:'
        end
        object Label4: TLabel
          Left = 268
          Top = 20
          Width = 31
          Height = 13
          Caption = 'Pra'#231'a:'
        end
        object Label5: TLabel
          Left = 268
          Top = 60
          Width = 17
          Height = 13
          Caption = 'D+:'
        end
        object TPData: TdmkEditDateTimePicker
          Left = 8
          Top = 36
          Width = 104
          Height = 21
          Date = 37617.480364108800000000
          Time = 37617.480364108800000000
          Color = clWhite
          TabOrder = 0
          OnClick = HabilitaCalcula
          OnChange = HabilitaCalcula
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPPrimeiroDia: TdmkEditDateTimePicker
          Left = 8
          Top = 76
          Width = 104
          Height = 21
          Date = 37617.480364108800000000
          Time = 37617.480364108800000000
          Color = clWhite
          TabOrder = 1
          OnClick = HabilitaCalcula
          OnChange = HabilitaCalcula
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdPrincipal: TdmkEdit
          Left = 116
          Top = 36
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1.000,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1000.000000000000000000
          OnChange = EdPrincipalChange
        end
        object EdOutros: TdmkEdit
          Left = 116
          Top = 76
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '142,85'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 142.850000000000000000
          OnChange = EdOutrosChange
        end
        object EdTaxa: TdmkEdit
          Left = 200
          Top = 36
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '4,4900'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 4.490000000000000000
          OnChange = EdTaxaChange
        end
        object EdParcelas: TdmkEdit
          Left = 200
          Top = 76
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '24'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 24
          OnChange = HabilitaCalcula
        end
        object EdValor: TdmkEdit
          Left = 304
          Top = 36
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdValorParcela: TdmkEdit
          Left = 304
          Top = 76
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdValorParcelaChange
        end
        object EdPraca: TdmkEdit
          Left = 268
          Top = 36
          Width = 32
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = HabilitaCalcula
        end
        object EdDMais: TdmkEdit
          Left = 268
          Top = 76
          Width = 32
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = HabilitaCalcula
        end
      end
      object GroupBox3: TGroupBox
        Left = 468
        Top = 0
        Width = 175
        Height = 108
        Align = alLeft
        Caption = ' IOF: '
        TabOrder = 2
        object LaIOFVal: TLabel
          Left = 104
          Top = 60
          Width = 47
          Height = 13
          Caption = 'Valor IOF:'
          Enabled = False
        end
        object Label9: TLabel
          Left = 36
          Top = 60
          Width = 48
          Height = 13
          Caption = '% IOF dia:'
        end
        object Label8: TLabel
          Left = 36
          Top = 20
          Width = 50
          Height = 13
          Caption = '% IOF fixo:'
        end
        object EdIOF_F: TdmkEdit
          Left = 36
          Top = 36
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,3800'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.380000000000000000
          OnChange = HabilitaCalcula
        end
        object CkIOF_M: TCheckBox
          Left = 104
          Top = 18
          Width = 64
          Height = 17
          Caption = '% M'#225'x*:'
          Checked = True
          State = cbChecked
          TabOrder = 1
          OnClick = HabilitaCalcula
        end
        object EdIOF_M: TdmkEdit
          Left = 104
          Top = 36
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '3,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 3.000000000000000000
          OnChange = HabilitaCalcula
        end
        object EdIOFVal: TdmkEdit
          Left = 104
          Top = 76
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdIOFValChange
        end
        object EdIOF_D: TdmkEdit
          Left = 36
          Top = 76
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0082'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.008200000000000001
          OnChange = HabilitaCalcula
        end
        object BitBtn1: TBitBtn
          Left = 4
          Top = 42
          Width = 21
          Height = 21
          Caption = 'PJ'
          TabOrder = 5
          OnClick = BitBtn1Click
        end
        object BitBtn2: TBitBtn
          Left = 4
          Top = 66
          Width = 21
          Height = 21
          Caption = 'PF'
          TabOrder = 6
          OnClick = BitBtn2Click
        end
      end
      object GroupBox1: TGroupBox
        Left = 643
        Top = 0
        Width = 141
        Height = 108
        Align = alClient
        Caption = 'GroupBox1'
        TabOrder = 3
        object Label6: TLabel
          Left = 8
          Top = 20
          Width = 54
          Height = 13
          Caption = 'M'#233'dia dias:'
          FocusControl = DBEdit1
        end
        object Label7: TLabel
          Left = 8
          Top = 60
          Width = 63
          Height = 13
          Caption = 'M'#233'dia juros?:'
          FocusControl = DBEdit2
        end
        object Label17: TLabel
          Left = 76
          Top = 60
          Width = 45
          Height = 13
          Caption = 'CET a.a.:'
        end
        object Label16: TLabel
          Left = 76
          Top = 20
          Width = 47
          Height = 13
          Caption = 'CET a.m.:'
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 36
          Width = 64
          Height = 21
          DataField = 'Dias'
          DataSource = DsMedia
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 8
          Top = 76
          Width = 64
          Height = 21
          DataField = 'JUROS'
          DataSource = DsMedia
          TabOrder = 1
        end
        object EdCETaa: TdmkEdit
          Left = 76
          Top = 76
          Width = 56
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdCETam: TdmkEdit
          Left = 76
          Top = 36
          Width = 56
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 108
      Width = 784
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object Label52: TLabel
        Left = 476
        Top = 4
        Width = 157
        Height = 13
        Caption = '*: % m'#225'ximo de cobran'#231'a de IOF.'
      end
      object CkDiasUteis: TCheckBox
        Left = 80
        Top = 0
        Width = 269
        Height = 21
        Caption = 'Calcular para os vencimentos ca'#237'rem em dias '#250'teis.'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
    end
    object PnCalc: TPanel
      Left = 0
      Top = 133
      Width = 784
      Height = 52
      Align = alTop
      ParentBackground = False
      TabOrder = 2
      Visible = False
      object Label11: TLabel
        Left = 88
        Top = 4
        Width = 28
        Height = 13
        Caption = 'Juros:'
      end
      object Label12: TLabel
        Left = 172
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object Button1: TButton
        Left = 8
        Top = 12
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 0
      end
      object EdJuros: TdmkEdit
        Left = 88
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdTotal: TdmkEdit
        Left = 172
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 185
      Width = 784
      Height = 345
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 3
      object TabSheet1: TTabSheet
        Caption = ' Parcelas '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl2: TPageControl
          Left = 0
          Top = 0
          Width = 776
          Height = 317
          ActivePage = TabSheet2
          Align = alClient
          TabOrder = 0
          object TabSheet2: TTabSheet
            Caption = ' Progressivo '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel3: TPanel
              Left = 0
              Top = 0
              Width = 768
              Height = 289
              Align = alClient
              BevelOuter = bvNone
              Caption = 'N'#250'mero de parcelas excedeu o m'#225'ximo!'
              TabOrder = 0
              object PnBanco: TPanel
                Left = 0
                Top = 0
                Width = 708
                Height = 289
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object DBGrid1: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 708
                  Height = 249
                  Align = alClient
                  DataSource = DsSimula1
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Parcela'
                      Width = 44
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DataPg'
                      Title.Caption = 'Vence'
                      Width = 62
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValParc'
                      Title.Caption = 'Valor parcela'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PerJuro'
                      Title.Caption = '% Juros'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValJuro'
                      Title.Caption = '$ Juros'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValPrin'
                      Title.Caption = 'Principal'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValOrig'
                      Title.Caption = 'Valor original'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Dias'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PercIOF'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValrIOF'
                      Visible = True
                    end>
                end
                object DBGrid4: TDBGrid
                  Left = 0
                  Top = 249
                  Width = 708
                  Height = 40
                  Align = alBottom
                  DataSource = DsSum1
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Parcelas'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValParc'
                      Title.Caption = '$ Parcela'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValJuro'
                      Title.Caption = '$ Juros'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValPrin'
                      Title.Caption = '$ Principal'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValOrig'
                      Title.Caption = '$ Original'
                      Width = 100
                      Visible = True
                    end>
                end
              end
              object Panel12: TPanel
                Left = 708
                Top = 0
                Width = 60
                Height = 289
                Align = alRight
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object La001: TLabel
                  Left = 16
                  Top = 8
                  Width = 42
                  Height = 13
                  Caption = '0000000'
                end
                object La002: TLabel
                  Left = 16
                  Top = 24
                  Width = 42
                  Height = 13
                  Caption = '0000000'
                end
                object La003: TLabel
                  Left = 16
                  Top = 40
                  Width = 42
                  Height = 13
                  Caption = '0000000'
                end
                object La004: TLabel
                  Left = 16
                  Top = 56
                  Width = 42
                  Height = 13
                  Caption = '0000000'
                end
                object La005: TLabel
                  Left = 16
                  Top = 72
                  Width = 42
                  Height = 13
                  Caption = '0000000'
                end
                object La006: TLabel
                  Left = 16
                  Top = 88
                  Width = 42
                  Height = 13
                  Caption = '0000000'
                end
                object Label2: TLabel
                  Left = 4
                  Top = 8
                  Width = 9
                  Height = 13
                  Caption = '1:'
                end
                object Label18: TLabel
                  Left = 4
                  Top = 24
                  Width = 9
                  Height = 13
                  Caption = '2:'
                end
                object Label19: TLabel
                  Left = 4
                  Top = 40
                  Width = 9
                  Height = 13
                  Caption = '3:'
                end
                object Label23: TLabel
                  Left = 4
                  Top = 56
                  Width = 9
                  Height = 13
                  Caption = '4:'
                end
                object Label24: TLabel
                  Left = 4
                  Top = 72
                  Width = 9
                  Height = 13
                  Caption = '5:'
                end
                object Label25: TLabel
                  Left = 4
                  Top = 88
                  Width = 9
                  Height = 13
                  Caption = '6:'
                end
                object Label26: TLabel
                  Left = 4
                  Top = 104
                  Width = 9
                  Height = 13
                  Caption = '?:'
                end
                object La000: TLabel
                  Left = 16
                  Top = 104
                  Width = 42
                  Height = 13
                  Caption = '0000000'
                end
              end
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Regressivo'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel11: TPanel
              Left = 0
              Top = 0
              Width = 768
              Height = 289
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object DBGrid5: TDBGrid
                Left = 0
                Top = 0
                Width = 768
                Height = 249
                Align = alClient
                DataSource = DsSimula2
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Parcela'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataPg'
                    Title.Caption = 'Vence'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValParc'
                    Title.Caption = 'Valor parcela'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PerJuro'
                    Title.Caption = '% Juros'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValJuro'
                    Title.Caption = '$ Juros'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValPrin'
                    Title.Caption = 'Principal'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValOrig'
                    Title.Caption = 'Valor original'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Dias'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PercIOF'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValrIOF'
                    Visible = True
                  end>
              end
              object DBGrid6: TDBGrid
                Left = 0
                Top = 249
                Width = 768
                Height = 40
                Align = alBottom
                DataSource = DsSum2
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Parcelas'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValParc'
                    Title.Caption = '$ Parcela'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValJuro'
                    Title.Caption = '$ Juros'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValPrin'
                    Title.Caption = '$ Principal'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValOrig'
                    Title.Caption = '$ Original'
                    Width = 100
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet4: TTabSheet
            Caption = ' Banc'#225'rio '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 768
              Height = 289
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object DBGrid8: TDBGrid
                Left = 0
                Top = 249
                Width = 768
                Height = 40
                Align = alBottom
                DataSource = DsSumI
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Parcelas'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Prestacao'
                    Title.Caption = '$ Parcelas'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValJuros'
                    Title.Caption = '$ Juros'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValPrincip'
                    Title.Caption = '$ Principal'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'IOF_Valor'
                    Title.Caption = '$ IOF'
                    Width = 72
                    Visible = True
                  end>
              end
              object DBGrid9: TDBGrid
                Left = 0
                Top = 0
                Width = 768
                Height = 249
                Align = alClient
                DataSource = DsIOF
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Parcela'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VCTO_TXT'
                    Title.Caption = 'Vence'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Prestacao'
                    Title.Caption = 'Valor parcela'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'JUROS'
                    Title.Caption = '% Juros'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValJuros'
                    Title.Caption = '$ Juros'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValPrincip'
                    Title.Caption = 'Principal'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValCorrigi'
                    Title.Caption = 'Valor corrigido'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DiasParcel'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'IOF_Percen'
                    Title.Caption = '% IOF'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'IOF_Valor'
                    Title.Caption = '$ IOF'
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet5: TTabSheet
            Caption = ' Comercial '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 768
              Height = 289
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object DBGrid2: TDBGrid
                Left = 0
                Top = 249
                Width = 768
                Height = 40
                Align = alBottom
                DataSource = DsSum0
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Parcelas'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValParc'
                    Title.Caption = '$ Parcela'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValJuro'
                    Title.Caption = '$ Juros'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValPrin'
                    Title.Caption = '$ Principal'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValOrig'
                    Title.Caption = '$ Original'
                    Width = 100
                    Visible = True
                  end>
              end
              object DBGrid3: TDBGrid
                Left = 0
                Top = 0
                Width = 768
                Height = 249
                Align = alClient
                DataSource = DsSimula0
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Parcela'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataPg'
                    Title.Caption = 'Vence'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValParc'
                    Title.Caption = 'Valor parcela'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PerJuro'
                    Title.Caption = '% Juros'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValJuro'
                    Title.Caption = '$ Juros'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValPrin'
                    Title.Caption = 'Principal'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValOrig'
                    Title.Caption = 'Valor original'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Dias'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PercIOF'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValrIOF'
                    Visible = True
                  end>
              end
            end
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 622
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtCalcula: TBitBtn
        Tag = 10086
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Calcula'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCalculaClick
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 135
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Recalcula'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Visible = False
        OnClick = BtReabreClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 578
    Width = 784
    Height = 44
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object GBAvisos1: TGroupBox
      Left = 0
      Top = 0
      Width = 648
      Height = 44
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 644
        Height = 27
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 712
      Top = 0
      Width = 72
      Height = 44
      Align = alRight
      Caption = ' C'#225'lculos: '
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 68
        Height = 27
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaCalculo1: TLabel
          Left = 7
          Top = 4
          Width = 56
          Height = 16
          Caption = '00000000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaCalculo2: TLabel
          Left = 6
          Top = 3
          Width = 56
          Height = 16
          Caption = '00000000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object GroupBox5: TGroupBox
      Left = 648
      Top = 0
      Width = 64
      Height = 44
      Align = alRight
      Caption = ' Tempo: '
      TabOrder = 2
      object Panel13: TPanel
        Left = 2
        Top = 15
        Width = 60
        Height = 27
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaTempo1: TLabel
          Left = 7
          Top = 4
          Width = 50
          Height = 16
          Caption = '00:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaTempo2: TLabel
          Left = 6
          Top = 3
          Width = 50
          Height = 16
          Caption = '00:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object QrSimula0: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM simulapg'
      'WHERE Tipo=0')
    Left = 452
    Top = 364
    object QrSimula0Parcela: TIntegerField
      FieldName = 'Parcela'
    end
    object QrSimula0DataPg: TDateField
      FieldName = 'DataPg'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSimula0ValParc: TFloatField
      FieldName = 'ValParc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSimula0ValOrig: TFloatField
      FieldName = 'ValOrig'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSimula0PerJuro: TFloatField
      FieldName = 'PerJuro'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrSimula0ValPrin: TFloatField
      FieldName = 'ValPrin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSimula0ValJuro: TFloatField
      FieldName = 'ValJuro'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSimula0Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSimula0PercIOF: TFloatField
      FieldName = 'PercIOF'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrSimula0ValrIOF: TFloatField
      FieldName = 'ValrIOF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSimula0Dias: TIntegerField
      FieldName = 'Dias'
    end
  end
  object DsSimula0: TDataSource
    DataSet = QrSimula0
    Left = 480
    Top = 364
  end
  object QrSum0: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(ValParc) ValParc, SUM(ValOrig) ValOrig,'
      'SUM(ValPrin) ValPrin, SUM(ValJuro) ValJuro,'
      'SUM(ValrIOF) ValrIOF, COUNT(Parcela) Parcelas'
      'FROM simulapg'
      'WHERE Tipo=0')
    Left = 508
    Top = 364
    object QrSum0Parcelas: TLargeintField
      FieldName = 'Parcelas'
      Required = True
    end
    object QrSum0ValParc: TFloatField
      FieldName = 'ValParc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum0ValOrig: TFloatField
      FieldName = 'ValOrig'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum0ValPrin: TFloatField
      FieldName = 'ValPrin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum0ValJuro: TFloatField
      FieldName = 'ValJuro'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum0ValrIOF: TFloatField
      FieldName = 'ValrIOF'
    end
  end
  object DsSum0: TDataSource
    DataSet = QrSum0
    Left = 536
    Top = 364
  end
  object Qr2: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM simulapg'
      'WHERE Tipo=:P0')
    Left = 592
    Top = 392
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object Qr2Parcela: TIntegerField
      FieldName = 'Parcela'
    end
    object Qr2DataPg: TDateField
      FieldName = 'DataPg'
    end
    object Qr2PerJuro: TFloatField
      FieldName = 'PerJuro'
    end
    object Qr2ValParc: TFloatField
      FieldName = 'ValParc'
    end
    object Qr2ValOrig: TFloatField
      FieldName = 'ValOrig'
    end
    object Qr2ValPrin: TFloatField
      FieldName = 'ValPrin'
    end
    object Qr2ValJuro: TFloatField
      FieldName = 'ValJuro'
    end
    object Qr2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object Qr2PercIOF: TFloatField
      FieldName = 'PercIOF'
    end
    object Qr2ValrIOF: TFloatField
      FieldName = 'ValrIOF'
    end
    object Qr2Dias: TIntegerField
      FieldName = 'Dias'
    end
  end
  object QrMedia: TmySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrMediaCalcFields
    SQL.Strings = (
      'SELECT '
      'SUM(TO_DAYS(Vencimento) - TO_DAYS(:P0))/COUNT(Parcela) Dias'
      'FROM _iof_')
    Left = 592
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMediaDias: TFloatField
      FieldName = 'Dias'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrMediaJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      Calculated = True
    end
  end
  object DsMedia: TDataSource
    DataSet = QrMedia
    Left = 620
    Top = 364
  end
  object QrPesq: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT Parcela, DataPg'
      'FROM simulapg'
      'WHERE Tipo=1'
      'AND ('
      '     (Parcela > :P0 AND DataPg < :P1)'
      '  OR '
      '     (Parcela < :P2 AND DataPg > :P3)'
      ')')
    Left = 592
    Top = 420
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
  end
  object DsSimula1: TDataSource
    DataSet = TbSimula1
    Left = 480
    Top = 392
  end
  object QrSum1: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(ValParc) ValParc, SUM(ValOrig) ValOrig,'
      'SUM(ValPrin) ValPrin, SUM(ValJuro) ValJuro,'
      'SUM(ValrIOF) ValrIOF, COUNT(Parcela) Parcelas'
      'FROM simulapg'
      'WHERE Tipo=1')
    Left = 508
    Top = 392
    object LargeintField1: TLargeintField
      FieldName = 'Parcelas'
      Required = True
    end
    object FloatField1: TFloatField
      FieldName = 'ValParc'
      DisplayFormat = '#,###,##0.00'
    end
    object FloatField2: TFloatField
      FieldName = 'ValOrig'
      DisplayFormat = '#,###,##0.00'
    end
    object FloatField3: TFloatField
      FieldName = 'ValPrin'
      DisplayFormat = '#,###,##0.00'
    end
    object FloatField4: TFloatField
      FieldName = 'ValJuro'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum1ValrIOF: TFloatField
      FieldName = 'ValrIOF'
    end
  end
  object DsSum1: TDataSource
    DataSet = QrSum1
    Left = 536
    Top = 392
  end
  object QrSum2: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(ValParc) ValParc, SUM(ValOrig) ValOrig,'
      'SUM(ValPrin) ValPrin, SUM(ValJuro) ValJuro,'
      'SUM(ValrIOF) ValrIOF, COUNT(Parcela) Parcelas'
      'FROM simulapg'
      'WHERE Tipo=2')
    Left = 508
    Top = 420
    object QrSum2Parcelas: TLargeintField
      FieldName = 'Parcelas'
      Required = True
    end
    object QrSum2ValParc: TFloatField
      FieldName = 'ValParc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum2ValOrig: TFloatField
      FieldName = 'ValOrig'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum2ValPrin: TFloatField
      FieldName = 'ValPrin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum2ValJuro: TFloatField
      FieldName = 'ValJuro'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSum2ValrIOF: TFloatField
      FieldName = 'ValrIOF'
    end
  end
  object DsSum2: TDataSource
    DataSet = QrSum2
    Left = 536
    Top = 420
  end
  object QrSimula2: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM simulapg'
      'WHERE Tipo=2')
    Left = 452
    Top = 420
    object QrSimula2Parcela: TIntegerField
      FieldName = 'Parcela'
    end
    object QrSimula2DataPg: TDateField
      FieldName = 'DataPg'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSimula2ValParc: TFloatField
      FieldName = 'ValParc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSimula2ValOrig: TFloatField
      FieldName = 'ValOrig'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSimula2PerJuro: TFloatField
      FieldName = 'PerJuro'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrSimula2ValPrin: TFloatField
      FieldName = 'ValPrin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSimula2ValJuro: TFloatField
      FieldName = 'ValJuro'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSimula2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSimula2PercIOF: TFloatField
      FieldName = 'PercIOF'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrSimula2ValrIOF: TFloatField
      FieldName = 'ValrIOF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSimula2Dias: TIntegerField
      FieldName = 'Dias'
    end
  end
  object DsSimula2: TDataSource
    DataSet = QrSimula2
    Left = 480
    Top = 420
  end
  object TbSimula1: TmySQLTable
    Database = DModG.MyPID_DB
    Filter = 'Tipo=1'
    Filtered = True
    BeforePost = TbSimula1BeforePost
    OnNewRecord = TbSimula1NewRecord
    TableName = 'simulapg'
    Left = 452
    Top = 392
    object TbSimula1Tipo: TIntegerField
      FieldName = 'Tipo'
      ReadOnly = True
    end
    object TbSimula1Parcela: TIntegerField
      FieldName = 'Parcela'
      ReadOnly = True
    end
    object TbSimula1DataPg: TDateField
      FieldName = 'DataPg'
      OnChange = TbSimula1DataPgChange
      OnValidate = TbSimula1DataPgValidate
      EditMask = '99/99/9999;1;'
    end
    object TbSimula1PerJuro: TFloatField
      FieldName = 'PerJuro'
      ReadOnly = True
    end
    object TbSimula1ValParc: TFloatField
      FieldName = 'ValParc'
      ReadOnly = True
    end
    object TbSimula1ValOrig: TFloatField
      FieldName = 'ValOrig'
      ReadOnly = True
    end
    object TbSimula1ValPrin: TFloatField
      FieldName = 'ValPrin'
      ReadOnly = True
    end
    object TbSimula1ValJuro: TFloatField
      FieldName = 'ValJuro'
      ReadOnly = True
    end
    object TbSimula1PercIOF: TFloatField
      FieldName = 'PercIOF'
      DisplayFormat = '#,###,##0.0000'
    end
    object TbSimula1ValrIOF: TFloatField
      FieldName = 'ValrIOF'
      DisplayFormat = '#,###,##0.00'
    end
    object TbSimula1Dias: TIntegerField
      FieldName = 'Dias'
    end
  end
  object QrLast: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM _iof_'
      'WHERE Parcela=24')
    Left = 620
    Top = 392
    object QrLastParcela: TIntegerField
      FieldName = 'Parcela'
    end
    object QrLastBaseCalc: TFloatField
      FieldName = 'BaseCalc'
    end
    object QrLastVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLastDiasParcel: TIntegerField
      FieldName = 'DiasParcel'
    end
    object QrLastDiasCorrid: TIntegerField
      FieldName = 'DiasCorrid'
    end
    object QrLastMesesPerio: TFloatField
      FieldName = 'MesesPerio'
    end
    object QrLastFatorJuros: TFloatField
      FieldName = 'FatorJuros'
    end
    object QrLastValCorrigi: TFloatField
      FieldName = 'ValCorrigi'
    end
    object QrLastValPrincip: TFloatField
      FieldName = 'ValPrincip'
    end
    object QrLastValJuros: TFloatField
      FieldName = 'ValJuros'
    end
    object QrLastIOF_Percen: TFloatField
      FieldName = 'IOF_Percen'
    end
    object QrLastIOF_Valor: TFloatField
      FieldName = 'IOF_Valor'
    end
    object QrLastAtivo: TIntegerField
      FieldName = 'Ativo'
    end
  end
  object QrIOF: TmySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrIOFCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM _iof_'
      'WHERE Parcela=24')
    Left = 452
    Top = 448
    object QrIOFParcela: TIntegerField
      FieldName = 'Parcela'
    end
    object QrIOFBaseCalc: TFloatField
      FieldName = 'BaseCalc'
    end
    object QrIOFVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrIOFDiasParcel: TIntegerField
      FieldName = 'DiasParcel'
    end
    object QrIOFDiasCorrid: TIntegerField
      FieldName = 'DiasCorrid'
    end
    object QrIOFMesesPerio: TFloatField
      FieldName = 'MesesPerio'
    end
    object QrIOFFatorJuros: TFloatField
      FieldName = 'FatorJuros'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrIOFValCorrigi: TFloatField
      FieldName = 'ValCorrigi'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIOFValPrincip: TFloatField
      FieldName = 'ValPrincip'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIOFValJuros: TFloatField
      FieldName = 'ValJuros'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIOFIOF_Percen: TFloatField
      FieldName = 'IOF_Percen'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrIOFIOF_Valor: TFloatField
      FieldName = 'IOF_Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIOFAtivo: TIntegerField
      FieldName = 'Ativo'
    end
    object QrIOFPrestacao: TFloatField
      FieldName = 'Prestacao'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIOFVCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VCTO_TXT'
      Size = 8
      Calculated = True
    end
    object QrIOFJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Calculated = True
    end
  end
  object QrSoma: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(IOF_Valor) IOF_Valor'
      'FROM _iof_')
    Left = 620
    Top = 420
    object QrSomaIOF_Valor: TFloatField
      FieldName = 'IOF_Valor'
    end
  end
  object DsIOF: TDataSource
    DataSet = QrIOF
    Left = 480
    Top = 448
  end
  object QrSumI: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(Prestacao) Prestacao, SUM(0) ValOrig,'
      'SUM(ValPrincip) ValPrincip, SUM(ValJuros) ValJuros,'
      'SUM(IOF_Valor) IOF_Valor, COUNT(Parcela) Parcelas'
      'FROM _iof_')
    Left = 508
    Top = 448
    object QrSumIPrestacao: TFloatField
      FieldName = 'Prestacao'
    end
    object QrSumIValOrig: TFloatField
      FieldName = 'ValOrig'
    end
    object QrSumIValPrincip: TFloatField
      FieldName = 'ValPrincip'
    end
    object QrSumIValJuros: TFloatField
      FieldName = 'ValJuros'
    end
    object QrSumIIOF_Valor: TFloatField
      FieldName = 'IOF_Valor'
    end
    object QrSumIParcelas: TLargeintField
      FieldName = 'Parcelas'
      Required = True
    end
  end
  object DsSumI: TDataSource
    DataSet = QrSumI
    Left = 536
    Top = 448
  end
end
