object FmOcorBank: TFmOcorBank
  Left = 368
  Top = 194
  Caption = 'FIN-BANCO-003 :: Ocorr'#234'ncias Banc'#225'rias'
  ClientHeight = 470
  ClientWidth = 954
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 113
    Width = 954
    Height = 357
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 954
      Height = 235
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 12
        Top = 20
        Width = 35
        Height = 13
        Caption = 'ID [F4]:'
      end
      object Label9: TLabel
        Left = 73
        Top = 20
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 628
        Top = 20
        Width = 53
        Height = 13
        Caption = 'Valor base:'
      end
      object Label4: TLabel
        Left = 273
        Top = 160
        Width = 55
        Height = 13
        Caption = 'Movimento:'
      end
      object Label8: TLabel
        Left = 12
        Top = 60
        Width = 131
        Height = 13
        Caption = 'Conta (do plano de contas):'
      end
      object SBPlaGen: TSpeedButton
        Left = 687
        Top = 76
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBPlaGenClick
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 36
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodigoKeyDown
      end
      object EdNome: TdmkEdit
        Left = 73
        Top = 36
        Width = 550
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdBase: TdmkEdit
        Left = 628
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'Base'
        UpdCampo = 'Base'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object RGFormaCNAB: TdmkRadioGroup
        Left = 12
        Top = 103
        Width = 696
        Height = 45
        Caption = 'Forma de cobran'#231'a em arquivos remessa / retorno (CNAB240)'
        Columns = 3
        Items.Strings = (
          'N'#227'o cobrar'
          'Cobrar o mesmo valor do banco'
          'Cobrar o valor base deste cadastro')
        TabOrder = 5
        QryCampo = 'FormaCNAB'
        UpdCampo = 'FormaCNAB'
        UpdType = utYes
        OldValor = 0
      end
      object RGEnvio: TdmkRadioGroup
        Left = 12
        Top = 155
        Width = 255
        Height = 45
        Caption = 'Tipo de envio'
        Columns = 3
        Items.Strings = (
          'Nenhum'
          'Remessa'
          'Retorno')
        TabOrder = 6
        QryCampo = 'Envio'
        UpdCampo = 'Envio'
        UpdType = utYes
        OldValor = 0
      end
      object EdMovimento: TdmkEdit
        Left = 273
        Top = 177
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Movimento'
        UpdCampo = 'Movimento'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdMovimentoChange
      end
      object EdMovim_TXT: TdmkEdit
        Left = 318
        Top = 177
        Width = 389
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPlaGen: TdmkEditCB
        Left = 12
        Top = 76
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PlaGen'
        UpdCampo = 'PlaGen'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPlaGen
        IgnoraDBLookupComboBox = False
      end
      object CBPlaGen: TdmkDBLookupComboBox
        Left = 68
        Top = 76
        Width = 617
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 4
        dmkEditCB = EdPlaGen
        QryCampo = 'PlaGen'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CkAtencao: TdmkCheckBox
        Left = 12
        Top = 210
        Width = 450
        Height = 17
        Caption = 
          'Informar na inclus'#227'o da duplicata sobre sacados que possuam dupl' +
          'icatas com esta al'#237'nea '
        TabOrder = 9
        QryCampo = 'Atencao'
        UpdCampo = 'Atencao'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 294
      Width = 954
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 816
        Top = 15
        Width = 136
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 113
    Width = 954
    Height = 357
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 954
      Height = 230
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 73
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 609
        Top = 16
        Width = 53
        Height = 13
        Caption = 'Valor base:'
        FocusControl = dmkDBEdit1
      end
      object Label6: TLabel
        Left = 372
        Top = 157
        Width = 55
        Height = 13
        Caption = 'Movimento:'
        FocusControl = DBEdit2
      end
      object Label10: TLabel
        Left = 12
        Top = 60
        Width = 131
        Height = 13
        Caption = 'Conta (do plano de contas):'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOcorBank
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 73
        Top = 32
        Width = 530
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOcorBank
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 609
        Top = 32
        Width = 80
        Height = 21
        Color = clWhite
        DataField = 'Base'
        DataSource = DsOcorBank
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 12
        Top = 99
        Width = 677
        Height = 45
        Caption = 'Forma de cobran'#231'a em arquivos remessa / retorno (CNAB240)'
        Columns = 3
        DataField = 'FormaCNAB'
        DataSource = DsOcorBank
        Items.Strings = (
          'N'#227'o cobrar'
          'Cobrar o mesmo valor do banco'
          'Cobrar o valor base acima')
        ParentBackground = True
        TabOrder = 5
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object DBEdit2: TDBEdit
        Left = 401
        Top = 173
        Width = 288
        Height = 21
        DataField = 'NOMEMOVIMENTO'
        DataSource = DsOcorBank
        TabOrder = 8
      end
      object DBEdit4: TDBEdit
        Left = 372
        Top = 173
        Width = 29
        Height = 21
        DataField = 'Movimento'
        DataSource = DsOcorBank
        TabOrder = 7
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 12
        Top = 150
        Width = 355
        Height = 45
        Caption = 'Tipo de envio'
        Columns = 3
        DataField = 'Envio'
        DataSource = DsOcorBank
        Items.Strings = (
          'Nenhum'
          'Remessa'
          'Retorno')
        ParentBackground = True
        TabOrder = 6
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object DBEdit1: TDBEdit
        Left = 68
        Top = 76
        Width = 621
        Height = 21
        DataField = 'NO_PLAGEN'
        DataSource = DsOcorBank
        TabOrder = 4
      end
      object DBEdit3: TDBEdit
        Left = 12
        Top = 76
        Width = 56
        Height = 21
        DataField = 'PlaGen'
        DataSource = DsOcorBank
        TabOrder = 3
      end
      object DBCheckBox1: TDBCheckBox
        Left = 12
        Top = 202
        Width = 450
        Height = 17
        Caption = 
          'Informar na inclus'#227'o da duplicata sobre sacados que possuam dupl' +
          'icatas com esta al'#237'nea '
        DataField = 'Atencao'
        DataSource = DsOcorBank
        TabOrder = 9
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 293
      Width = 954
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 240
        Top = 15
        Width = 712
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 603
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
        object BtImportar: TBitBtn
          Tag = 19
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = 'I&mporta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtImportarClick
        end
        object BitBtn1: TBitBtn
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Recria'#13#10'Obrigat'#243'rios'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BitBtn1Click
        end
        object BtContas: TBitBtn
          Tag = 10087
          Left = 464
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Contas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtContasClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 954
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 906
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 690
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 276
        Height = 32
        Caption = 'Ocorr'#234'ncias Banc'#225'rias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 276
        Height = 32
        Caption = 'Ocorr'#234'ncias Banc'#225'rias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 276
        Height = 32
        Caption = 'Ocorr'#234'ncias Banc'#225'rias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 954
    Height = 61
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 950
      Height = 44
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 27
        Width = 950
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrOcorBank
    Left = 640
    Top = 12
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrOcorBankBeforeOpen
    AfterOpen = QrOcorBankAfterOpen
    OnCalcFields = QrOcorBankCalcFields
    SQL.Strings = (
      'SELECT cnt.Nome NO_PLAGEN, ocb.* '
      'FROM ocorbank ocb'
      'LEFT JOIN contas cnt ON cnt.Codigo=ocb.PlaGen')
    Left = 612
    Top = 12
    object QrOcorBankNOMEMOVIMENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMOVIMENTO'
      Size = 100
      Calculated = True
    end
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorBankEnvio: TIntegerField
      FieldName = 'Envio'
    end
    object QrOcorBankMovimento: TIntegerField
      FieldName = 'Movimento'
    end
    object QrOcorBankFormaCNAB: TSmallintField
      FieldName = 'FormaCNAB'
    end
    object QrOcorBankAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOcorBankAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOcorBankPlaGen: TIntegerField
      FieldName = 'PlaGen'
    end
    object QrOcorBankNO_PLAGEN: TWideStringField
      FieldName = 'NO_PLAGEN'
      Size = 50
    end
    object QrOcorBankAtencao: TIntegerField
      FieldName = 'Atencao'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Panel2
    CanUpd01 = BtInclui
    CanDel01 = BtAltera
    Left = 668
    Top = 12
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.txt'
    Filter = 'Arquivos Texto|*.txt'
    Left = 584
    Top = 12
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 608
    Top = 48
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 636
    Top = 48
  end
end
