object FmLocImprVend: TFmLocImprVend
  Left = 339
  Top = 185
  Caption = 'LOC-IMPRI-002 :: Impress'#227'o de Vendas de Mercadorias'
  ClientHeight = 540
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 467
        Height = 32
        Caption = ' Impress'#227'o de Vendas de Mercadorias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 467
        Height = 32
        Caption = ' Impress'#227'o de Vendas de Mercadorias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 467
        Height = 32
        Caption = ' Impress'#227'o de Vendas de Mercadorias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 426
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 470
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 20
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 378
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object DBGVendas: TdmkDBGridZTO
      Left = 0
      Top = 277
      Width = 1008
      Height = 101
      Align = alClient
      DataSource = DsVendas
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = DBGVendasDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'DtHrSai'
          Title.Caption = 'Sa'#237'da'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Referencia'
          Title.Caption = 'Refer'#234'ncia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD'
          Title.Caption = 'Mercadoria'
          Width = 448
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SIGLA'
          Title.Caption = 'Sigla'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrcUni'
          Title.Caption = '$ Unt.'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Quantidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PercDesco'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorBruto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorTotal'
          Title.Caption = '$ Total'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Atendimento'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PGT'
          Width = 120
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 277
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object PnAtndCanais: TPanel
        Left = 613
        Top = 0
        Width = 220
        Height = 277
        Align = alLeft
        TabOrder = 0
        object DBGrid2: TDBGrid
          Left = 1
          Top = 70
          Width = 218
          Height = 206
          Align = alClient
          DataSource = DsAtndCanalSel
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGrid2CellClick
          OnDrawColumnCell = DBGrid2DrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = ' X'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'd'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Canal de Atendimento'
              Width = 129
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 218
          Height = 69
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label8: TLabel
            Left = 0
            Top = 0
            Width = 218
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Canais de Atendimento'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ExplicitWidth = 109
          end
          object BtTodosAtndCanais: TBitBtn
            Tag = 127
            Left = 5
            Top = 24
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Todos'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtTodosAtndCanaisClick
          end
          object BtNenhumAtndCanal: TBitBtn
            Tag = 128
            Left = 117
            Top = 24
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Nenhum'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtNenhumAtndCanalClick
          end
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 613
        Height = 277
        Align = alLeft
        TabOrder = 1
        object Panel8: TPanel
          Left = 1
          Top = 1
          Width = 611
          Height = 252
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 137
            Height = 252
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox6: TGroupBox
              Left = 0
              Top = 0
              Width = 137
              Height = 100
              Align = alTop
              Caption = ' Per'#237'odo: '
              TabOrder = 0
              object Label6: TLabel
                Left = 8
                Top = 16
                Width = 55
                Height = 13
                Caption = 'Data inicial:'
              end
              object Label19: TLabel
                Left = 8
                Top = 56
                Width = 48
                Height = 13
                Caption = 'Data final:'
              end
              object TPDataIni: TdmkEditDateTimePicker
                Left = 8
                Top = 32
                Width = 113
                Height = 21
                Time = 0.499141331019927700
                TabOrder = 0
                OnChange = TPDataIniChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPDataFim: TdmkEditDateTimePicker
                Left = 8
                Top = 72
                Width = 113
                Height = 21
                Date = 401768.000000000000000000
                Time = 0.499141331005375800
                TabOrder = 1
                OnChange = TPDataFimChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
            end
            object RGStatus: TRadioGroup
              Left = 0
              Top = 100
              Width = 137
              Height = 152
              Align = alClient
              Caption = ' Status: '
              ItemIndex = 2
              Items.Strings = (
                'Indefinido'
                'Or'#231'amento'
                'Pedido'
                'Qualquer')
              TabOrder = 1
            end
          end
          object Panel9: TPanel
            Left = 137
            Top = 0
            Width = 474
            Height = 252
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Label4: TLabel
              Left = 8
              Top = 4
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label3: TLabel
              Left = 8
              Top = 88
              Width = 79
              Height = 13
              Caption = 'Tipo de Produto:'
            end
            object Label5: TLabel
              Left = 8
              Top = 208
              Width = 90
              Height = 13
              Caption = 'Produto [F3 todos]:'
            end
            object Label16: TLabel
              Left = 8
              Top = 168
              Width = 36
              Height = 13
              Caption = 'Nivel 2:'
            end
            object Label17: TLabel
              Left = 8
              Top = 128
              Width = 36
              Height = 13
              Caption = 'Nivel 3:'
            end
            object dmkLabel3: TdmkLabel
              Left = 8
              Top = 44
              Width = 90
              Height = 13
              Caption = 'Centro de estoque:'
              UpdType = utYes
              SQLType = stNil
            end
            object EdEmpresa: TdmkEditCB
              Left = 8
              Top = 20
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdEmpresaChange
              DBLookupComboBox = CBEmpresa
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEmpresa: TdmkDBLookupComboBox
              Left = 64
              Top = 20
              Width = 400
              Height = 21
              KeyField = 'Filial'
              ListField = 'NOMEFILIAL'
              TabOrder = 1
              dmkEditCB = EdEmpresa
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdPrdGrupTip: TdmkEditCB
              Left = 8
              Top = 104
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdPrdGrupTipRedefinido
              DBLookupComboBox = CBPrdGrupTip
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBPrdGrupTip: TdmkDBLookupComboBox
              Left = 64
              Top = 104
              Width = 400
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsPrdGrupTip
              TabOrder = 5
              dmkEditCB = EdPrdGrupTip
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdGraGru1: TdmkEditCB
              Left = 8
              Top = 224
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 10
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnKeyDown = EdGraGru1KeyDown
              OnRedefinido = EdGraGru1Redefinido
              DBLookupComboBox = CBGraGru1
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGraGru1: TdmkDBLookupComboBox
              Left = 64
              Top = 224
              Width = 400
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsGraGru1
              TabOrder = 11
              dmkEditCB = EdGraGru1
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdGraGru2: TdmkEditCB
              Left = 8
              Top = 184
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdGraGru2Redefinido
              DBLookupComboBox = CBGraGru2
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGraGru2: TdmkDBLookupComboBox
              Left = 64
              Top = 184
              Width = 400
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsGraGru2
              TabOrder = 9
              dmkEditCB = EdGraGru2
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdGraGru3: TdmkEditCB
              Left = 8
              Top = 144
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdGraGru3Redefinido
              DBLookupComboBox = CBGraGru3
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGraGru3: TdmkDBLookupComboBox
              Left = 64
              Top = 144
              Width = 400
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsGraGru3
              TabOrder = 7
              dmkEditCB = EdGraGru3
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdStqCenCad: TdmkEditCB
              Left = 8
              Top = 60
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBStqCenCad
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBStqCenCad: TdmkDBLookupComboBox
              Left = 64
              Top = 60
              Width = 400
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsStqCenCadPsq
              TabOrder = 3
              dmkEditCB = EdStqCenCad
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
        end
        object Panel10: TPanel
          Left = 1
          Top = 253
          Width = 611
          Height = 23
          Align = alClient
          TabOrder = 1
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrStqCenCadPsq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 204
    Top = 312
    object QrStqCenCadPsqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadPsqCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadPsqNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCadPsq: TDataSource
    DataSet = QrStqCenCadPsq
    Left = 204
    Top = 360
  end
  object QrPrdGrupTip: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPrdGrupTipAfterScroll
    SQL.Strings = (
      'SELECT pgt.Codigo, pgt.CodUsu, pgt.Nome, pgt.MadeBy, '
      'pgt.Fracio, pgt.TipPrd, pgt.NivCad, pgt.FaixaIni, pgt.FaixaFim,'
      'pgt.TitNiv1, pgt.TitNiv2, pgt.TitNiv3,'
      'ELT(pgt.MadeBy, "Pr'#243'prio","Terceiros") NOME_MADEBY,'
      'ELT(pgt.Fracio+1, '#39'N'#227'o'#39', '#39'Sim'#39') NOME_FRACIO,'
      'ELT(pgt.TipPrd, '#39'Produto'#39', '#39'Mat'#233'ria-prima'#39', '#39'Uso e consumo'#39') '
      'NOME_TIPPRD, pgt.ImpedeCad, pgt.PerComissF, pgt.PerComissR'
      'FROM prdgruptip pgt'
      'ORDER BY pgt.Nome'
      '')
    Left = 40
    Top = 312
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPrdGrupTipMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrPrdGrupTipFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrPrdGrupTipTipPrd: TSmallintField
      FieldName = 'TipPrd'
    end
    object QrPrdGrupTipNivCad: TSmallintField
      FieldName = 'NivCad'
    end
    object QrPrdGrupTipFaixaIni: TIntegerField
      FieldName = 'FaixaIni'
    end
    object QrPrdGrupTipFaixaFim: TIntegerField
      FieldName = 'FaixaFim'
    end
    object QrPrdGrupTipTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Size = 15
    end
    object QrPrdGrupTipTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 15
    end
    object QrPrdGrupTipTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 15
    end
    object QrPrdGrupTipNOME_MADEBY: TWideStringField
      FieldName = 'NOME_MADEBY'
      Size = 9
    end
    object QrPrdGrupTipNOME_FRACIO: TWideStringField
      FieldName = 'NOME_FRACIO'
      Size = 3
    end
    object QrPrdGrupTipNOME_TIPPRD: TWideStringField
      FieldName = 'NOME_TIPPRD'
      Size = 13
    end
    object QrPrdGrupTipImpedeCad: TSmallintField
      FieldName = 'ImpedeCad'
    end
    object QrPrdGrupTipPerComissF: TFloatField
      FieldName = 'PerComissF'
    end
    object QrPrdGrupTipPerComissR: TFloatField
      FieldName = 'PerComissR'
    end
  end
  object QrGraGru3: TMySQLQuery
    Database = Dmod.MyDB
    AfterClose = QrGraGru3AfterClose
    AfterScroll = QrGraGru3AfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gg1.PrdGrupTip, gg3.Codusu, gg3.Nivel3, '
      'gg3.Nome'
      'FROM gragru1 gg1'
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3'
      'WHERE gg1.PrdGrupTip=:P0'
      '/*AND gg3.Nome LIKE :P1*/'
      'ORDER BY gg3.Nome')
    Left = 40
    Top = 404
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru3Codusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrGraGru3Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru3Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGru3PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object QrGraGru2: TMySQLQuery
    Database = Dmod.MyDB
    AfterClose = QrGraGru2AfterClose
    AfterScroll = QrGraGru2AfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gg2.Codusu, gg2.Nivel3, gg2.Nivel2, '
      'gg2.Nome'
      'FROM gragru2 gg2'
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg2.Nivel3'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel2=gg2.Nivel2'
      'WHERE gg1.PrdGrupTip=:P0'
      'AND gg2.Nivel3=:P1'
      '/*AND gg2.Nome LIKE :P2*/'
      'ORDER BY gg2.Nome')
    Left = 116
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraGru2Codusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrGraGru2Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru2Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGru2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    AfterClose = QrGraGru1AfterClose
    SQL.Strings = (
      'SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,'
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad,'
      'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD,'
      'gg1.CST_A,gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso,'
      'gg1.IPI_Alq, gg1.IPI_CST, gg1.IPI_cEnq, gg1.TipDimens,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED,'
      'unm.Nome NOMEUNIDMED, gg1.PartePrinc, '
      'gg1.PerCuztMin, gg1.PerCuztMax, gg1.MedOrdem, gg1.FatorClas,'
      'mor.Nome NO_MedOrdem, mor.Medida1, mor.Medida2,'
      'mor.Medida3, mor.Medida4, mpc.Nome NO_PartePrinc,'
      'InfAdProd, SiglaCustm, HowBxaEstq, GerBxaEstq,'
      'PIS_CST, PIS_AlqP, PIS_AlqV, PISST_AlqP, PISST_AlqV,'
      'COFINS_CST, COFINS_AlqP, COFINS_AlqV,COFINSST_AlqP, '
      'COFINSST_AlqV, ICMS_modBC, ICMS_modBCST, ICMS_pRedBC,'
      'ICMS_pRedBCST, ICMS_pMVAST, ICMS_pICMSST,'
      'IPI_pIPI, IPI_TpTrib, IPI_vUnid, EX_TIPI,'
      'ICMS_Pauta, ICMS_MaxTab, cGTIN_EAN,'
      'PIS_pRedBC, PISST_pRedBCST,'
      'COFINS_pRedBC, COFINSST_pRedBCST,'
      ''
      'ICMSRec_pRedBC, IPIRec_pRedBC,'
      'PISRec_pRedBC, COFINSRec_pRedBC,'
      'ICMSRec_pAliq, IPIRec_pAliq,'
      'PISRec_pAliq, COFINSRec_pAliq,'
      'ICMSRec_tCalc, IPIRec_tCalc,'
      'PISRec_tCalc, COFINSRec_tCalc,'
      'ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA,'
      'gg1.PerComissF, gg1.PerComissR, gg1.PerComissZ'
      ''
      'FROM gragru1 gg1'
      'LEFT JOIN gragru2   gg2 ON gg1.Nivel2=gg2.Nivel2'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN medordem mor ON mor.Codigo= gg1.MedOrdem'
      'LEFT JOIN matpartcad mpc ON mpc.Codigo=gg1.PartePrinc'
      'WHERE gg1.PrdGrupTip=:P0'
      'AND gg1.Nivel2=:P1'
      '/*AND gg1.Nome LIKE :P2*/'
      'ORDER BY gg1.Nome')
    Left = 116
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraGru1Codusu: TIntegerField
      FieldName = 'Codusu'
      Origin = 'gragru1.CodUsu'
      Required = True
    end
    object QrGraGru1Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Origin = 'gragru1.Nivel3'
      Required = True
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Origin = 'gragru1.Nivel2'
      Required = True
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
      Required = True
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru1.Nome'
      Required = True
      Size = 30
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
      Required = True
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Origin = 'gragru1.GraTamCad'
      Required = True
    end
    object QrGraGru1NOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Origin = 'gratamcad.Nome'
      Size = 50
    end
    object QrGraGru1CODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Origin = 'gratamcad.CodUsu'
      Required = True
    end
    object QrGraGru1CST_A: TSmallintField
      FieldName = 'CST_A'
      Origin = 'gragru1.CST_A'
      Required = True
      DisplayFormat = '0'
    end
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Origin = 'gragru1.CST_B'
      Required = True
      DisplayFormat = '00'
    end
    object QrGraGru1UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = 'gragru1.UnidMed'
      Required = True
    end
    object QrGraGru1Peso: TFloatField
      FieldName = 'Peso'
      Origin = 'gragru1.Peso'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrGraGru1NCM: TWideStringField
      FieldName = 'NCM'
      Origin = 'gragru1.NCM'
      Required = True
      Size = 10
    end
    object QrGraGru1SIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Origin = 'unidmed.Sigla'
      Size = 3
    end
    object QrGraGru1NOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Origin = 'unidmed.Nome'
      Size = 30
    end
    object QrGraGru1CODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Origin = 'unidmed.CodUsu'
      Required = True
    end
    object QrGraGru1IPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrGraGru1IPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
      Origin = 'gragru1.IPI_Alq'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Origin = 'gragru1.IPI_cEnq'
      Size = 3
    end
    object QrGraGru1TipDimens: TSmallintField
      FieldName = 'TipDimens'
      Origin = 'gragru1.TipDimens'
    end
    object QrGraGru1PerCuztMin: TFloatField
      FieldName = 'PerCuztMin'
      Origin = 'gragru1.PerCuztMin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrGraGru1PerCuztMax: TFloatField
      FieldName = 'PerCuztMax'
      Origin = 'gragru1.PerCuztMax'
      DisplayFormat = '#,###,##0.00'
    end
    object QrGraGru1MedOrdem: TIntegerField
      FieldName = 'MedOrdem'
      Origin = 'gragru1.MedOrdem'
    end
    object QrGraGru1PartePrinc: TIntegerField
      FieldName = 'PartePrinc'
      Origin = 'gragru1.PartePrinc'
    end
    object QrGraGru1NO_MedOrdem: TWideStringField
      FieldName = 'NO_MedOrdem'
      Origin = 'medordem.Nome'
      Size = 50
    end
    object QrGraGru1Medida1: TWideStringField
      FieldName = 'Medida1'
      Origin = 'medordem.Medida1'
    end
    object QrGraGru1Medida2: TWideStringField
      FieldName = 'Medida2'
      Origin = 'medordem.Medida2'
    end
    object QrGraGru1Medida3: TWideStringField
      FieldName = 'Medida3'
      Origin = 'medordem.Medida3'
    end
    object QrGraGru1Medida4: TWideStringField
      FieldName = 'Medida4'
      Origin = 'medordem.Medida4'
    end
    object QrGraGru1NO_PartePrinc: TWideStringField
      FieldName = 'NO_PartePrinc'
      Origin = 'matpartcad.Nome'
      Size = 50
    end
    object QrGraGru1InfAdProd: TWideStringField
      FieldName = 'InfAdProd'
      Origin = 'gragru1.InfAdProd'
      Size = 255
    end
    object QrGraGru1SiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Origin = 'gragru1.SiglaCustm'
      Size = 15
    end
    object QrGraGru1HowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
      Origin = 'gragru1.HowBxaEstq'
    end
    object QrGraGru1GerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
      Origin = 'gragru1.GerBxaEstq'
    end
    object QrGraGru1PIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrGraGru1PIS_AlqP: TFloatField
      FieldName = 'PIS_AlqP'
      Origin = 'gragru1.PIS_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PIS_AlqV: TFloatField
      FieldName = 'PIS_AlqV'
      Origin = 'gragru1.PIS_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISST_AlqP: TFloatField
      FieldName = 'PISST_AlqP'
      Origin = 'gragru1.PISST_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISST_AlqV: TFloatField
      FieldName = 'PISST_AlqV'
      Origin = 'gragru1.PISST_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrGraGru1COFINS_AlqP: TFloatField
      FieldName = 'COFINS_AlqP'
      Origin = 'gragru1.COFINS_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINS_AlqV: TFloatField
      FieldName = 'COFINS_AlqV'
      Origin = 'gragru1.COFINS_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSST_AlqP: TFloatField
      FieldName = 'COFINSST_AlqP'
      Origin = 'gragru1.COFINSST_AlqP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSST_AlqV: TFloatField
      FieldName = 'COFINSST_AlqV'
      Origin = 'gragru1.COFINSST_AlqV'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
      Origin = 'gragru1.ICMS_modBC'
    end
    object QrGraGru1ICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
      Origin = 'gragru1.ICMS_modBCST'
    end
    object QrGraGru1ICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      Origin = 'gragru1.ICMS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      Origin = 'gragru1.ICMS_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      Origin = 'gragru1.ICMS_pMVAST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      Origin = 'gragru1.ICMS_pICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPI_TpTrib: TSmallintField
      FieldName = 'IPI_TpTrib'
    end
    object QrGraGru1IPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      DisplayFormat = '#,###,###0.0000'
    end
    object QrGraGru1ICMS_Pauta: TFloatField
      FieldName = 'ICMS_Pauta'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMS_MaxTab: TFloatField
      FieldName = 'ICMS_MaxTab'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1cGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Size = 14
    end
    object QrGraGru1EX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrGraGru1PIS_pRedBC: TFloatField
      FieldName = 'PIS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISST_pRedBCST: TFloatField
      FieldName = 'PISST_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINS_pRedBC: TFloatField
      FieldName = 'COFINS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSST_pRedBCST: TFloatField
      FieldName = 'COFINSST_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1IPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrGraGru1ICMSRec_tCalc: TSmallintField
      FieldName = 'ICMSRec_tCalc'
    end
    object QrGraGru1IPIRec_tCalc: TSmallintField
      FieldName = 'IPIRec_tCalc'
    end
    object QrGraGru1PISRec_tCalc: TSmallintField
      FieldName = 'PISRec_tCalc'
    end
    object QrGraGru1COFINSRec_tCalc: TSmallintField
      FieldName = 'COFINSRec_tCalc'
    end
    object QrGraGru1ICMSAliqSINTEGRA: TFloatField
      FieldName = 'ICMSAliqSINTEGRA'
    end
    object QrGraGru1ICMSST_BaseSINTEGRA: TFloatField
      FieldName = 'ICMSST_BaseSINTEGRA'
    end
    object QrGraGru1FatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
    object QrGraGru1PerComissF: TFloatField
      FieldName = 'PerComissF'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrGraGru1PerComissR: TFloatField
      FieldName = 'PerComissR'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrGraGru1PerComissZ: TSmallintField
      FieldName = 'PerComissZ'
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 40
    Top = 356
  end
  object DsGraGru3: TDataSource
    DataSet = QrGraGru3
    Left = 40
    Top = 452
  end
  object DsGraGru2: TDataSource
    DataSet = QrGraGru2
    Left = 116
    Top = 360
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 116
    Top = 456
  end
  object QrEstq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ind.Tipo=0 THEN ind.RazaoSocial'
      'ELSE ind.Nome END NOMEFO, '
      'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECI, pq_.Nome NOMEPQ, pcl.*,'
      'lse.Nome NOMESE'
      'FROM pqcli pcl'
      'LEFT JOIN entidades    cli ON cli.Codigo=pcl.CI'
      'LEFT JOIN pq           pq_ ON pq_.Codigo=pcl.PQ'
      'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor'
      'LEFT JOIN entidades    ind ON ind.Codigo=pq_.IQ'
      'WHERE pq_.Ativo=2'
      'AND pq_.Setor in (5,2,3,4)'
      'ORDER BY NOMECI, NOMESE, NOMEPQ')
    Left = 12
    Top = 8
    object QrEstqNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEstqNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrEstqControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEstqPQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrEstqCI: TIntegerField
      FieldName = 'CI'
    end
    object QrEstqMinEstq: TFloatField
      FieldName = 'MinEstq'
    end
    object QrEstqLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEstqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEstqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEstqUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEstqUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEstqCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
    end
    object QrEstqMoedaPadrao: TSmallintField
      FieldName = 'MoedaPadrao'
      Required = True
    end
    object QrEstqEstSegur: TIntegerField
      FieldName = 'EstSegur'
    end
    object QrEstqPeso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrEstqValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrEstqCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrEstqNOMESE: TWideStringField
      FieldName = 'NOMESE'
    end
    object QrEstqNOMEFO: TWideStringField
      FieldName = 'NOMEFO'
      Size = 100
    end
    object QrEstqCodProprio: TWideStringField
      FieldName = 'CodProprio'
    end
    object QrEstqCustoEstoquePadrao: TFloatField
      FieldName = 'CustoEstoquePadrao'
    end
  end
  object DsEstq: TDataSource
    DataSet = QrEstq
    Left = 40
    Top = 8
  end
  object frxEstq: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MePesoOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Band4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsEstq."Peso"> < 0 then'
      '  begin              '
      '    MePeso.Font.Color       := clRed;'
      '    MeCodProprio.Font.Color := clRed;  '
      '    MePQ.Font.Color         := clRed;  '
      '    MeNomePQ.Font.Color     := clRed;                        '
      '  end else'
      '  begin              '
      '    MePeso.Font.Color       := clBlack;'
      '    MeCodProprio.Font.Color := clBlack;  '
      '    MePQ.Font.Color         := clBlack;  '
      '    MeNomePQ.Font.Color     := clBlack;                        '
      '  end;            '
      'end;'
      ''
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      '  //'
      'end.')
    OnGetValue = frxEstqGetValue
    Left = 68
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 548.031850000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 158.740250240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 113.385734020000000000
          Width = 613.763760000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 0.928880000000000000
          Top = 98.267604250000000000
          Width = 678.834880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 143.621974020000000000
          Width = 355.275556380000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188971500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897691500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606411500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 83.149506220000000000
          Width = 614.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DE USO E CONSUMO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_HORA]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 143.622140000000000000
          Width = 52.913385830000000000
          Height = 15.118110236220470000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd. CI')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527559054998000
          Width = 381.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188956850000000000
          Top = 3.779527559054998000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Top = 3.779527559054998000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Top = 3.779527559054998000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'Band4OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
        RowCount = 0
        object MeNomePQ: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 302.362170550000000000
          Height = 15.118110240000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstq."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188976380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MePesoOnBeforePrint'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Peso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Custo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePQ: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstq."PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCodProprio: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CodProprio'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstq."CodProprio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188956850000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 3.779530000000022000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 54.456710000000000000
          Top = 3.779530000000022000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMECI"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstq."NOMEPQ"'
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 54.220470000000000000
          Top = 3.779527560000020000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188956850000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Peso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstq."Peso">)=0,0,(SUM(<frxDsEstq."Valor">) / SUM' +
              '(<frxDsEstq."Peso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsEstq: TfrxDBDataset
    UserName = 'frxDsEstq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECI=NOMECI'
      'NOMEPQ=NOMEPQ'
      'Controle=Controle'
      'PQ=PQ'
      'CI=CI'
      'MinEstq=MinEstq'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'CustoPadrao=CustoPadrao'
      'MoedaPadrao=MoedaPadrao'
      'EstSegur=EstSegur'
      'Peso=Peso'
      'Valor=Valor'
      'Custo=Custo'
      'NOMESE=NOMESE'
      'NOMEFO=NOMEFO'
      'CodProprio=CodProprio'
      'CustoEstoquePadrao=CustoEstoquePadrao')
    DataSet = QrEstq
    BCDToCurrency = False
    DataSetOptions = []
    Left = 96
    Top = 8
  end
  object QrAtndCanalSel: TMySQLQuery
    Database = Dmod.DBAnt
    AfterOpen = QrAtndCanalSelAfterOpen
    Left = 204
    Top = 408
    object QrAtndCanalSelCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtndCanalSelNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrAtndCanalSelAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsAtndCanalSel: TDataSource
    DataSet = QrAtndCanalSel
    Left = 204
    Top = 456
  end
  object QrVendas: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVendasAfterOpen
    BeforeClose = QrVendasBeforeClose
    SQL.Strings = (
      'SELECT ven.*,  '
      'gg1.Referencia, gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,   '
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,   '
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,   '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI  '
      '   '
      'FROM loccitsven ven  '
      'LEFT JOIN loccconits its ON its.CtrID=ven.CtrID  '
      'LEFT JOIN loccconcab cab ON cab.Codigo=its.Codigo  '
      '  '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=ven.GraGruX   '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip   '
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed   '
      '  '
      'WHERE cab.Empresa=-11'
      'AND cab.DtHrEmi  BETWEEN "0000-00-00" AND "2021-06-28 23:59:59"'
      'AND its.AtndCanal IN (0,1,2)'
      'AND its.Status=2'
      'ORDER BY NO_PRD')
    Left = 288
    Top = 312
    object QrVendasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVendasCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrVendasItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrVendasManejoLca: TSmallintField
      FieldName = 'ManejoLca'
      Required = True
    end
    object QrVendasGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVendasUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrVendasQuantidade: TFloatField
      FieldName = 'Quantidade'
      Required = True
    end
    object QrVendasPrcUni: TFloatField
      FieldName = 'PrcUni'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVendasValorTotal: TFloatField
      FieldName = 'ValorTotal'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVendasQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrVendasValorProduto: TFloatField
      FieldName = 'ValorProduto'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVendasQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrVendasValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVendasQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
    end
    object QrVendasValDevolParci: TFloatField
      FieldName = 'ValDevolParci'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVendasTroca: TSmallintField
      FieldName = 'Troca'
      Required = True
    end
    object QrVendasCategoria: TWideStringField
      FieldName = 'Categoria'
      Required = True
      Size = 1
    end
    object QrVendasCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 40
    end
    object QrVendasCobrancaConsumo: TFloatField
      FieldName = 'CobrancaConsumo'
      Required = True
    end
    object QrVendasCobrancaRealizadaVenda: TWideStringField
      FieldName = 'CobrancaRealizadaVenda'
      Size = 1
    end
    object QrVendasCobranIniDtHr: TDateTimeField
      FieldName = 'CobranIniDtHr'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrVendasSaiDtHr: TDateTimeField
      FieldName = 'SaiDtHr'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrVendasSMIA_OriCtrl: TIntegerField
      FieldName = 'SMIA_OriCtrl'
      Required = True
    end
    object QrVendasSMIA_IDCtrl: TIntegerField
      FieldName = 'SMIA_IDCtrl'
      Required = True
    end
    object QrVendasLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrVendasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVendasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVendasUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrVendasUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrVendasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrVendasAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrVendasAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrVendasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrVendasprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrVendasprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrVendasprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      Required = True
    end
    object QrVendasprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrVendasStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Required = True
    end
    object QrVendasNF_Stat: TIntegerField
      FieldName = 'NF_Stat'
      Required = True
    end
    object QrVendasPercDesco: TFloatField
      FieldName = 'PercDesco'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVendasValorBruto: TFloatField
      FieldName = 'ValorBruto'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVendasReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrVendasNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrVendasNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrVendasPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrVendasUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrVendasNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 60
    end
    object QrVendasSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrVendasNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrVendasNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrVendasGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrVendasGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrVendasGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVendasGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrVendasDtHrSai: TDateTimeField
      FieldName = 'DtHrSai'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
  end
  object DsVendas: TDataSource
    DataSet = QrVendas
    Left = 288
    Top = 360
  end
  object QrAux: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 353
    Top = 216
  end
  object frxLOC_IMPRI_002_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxLOC_IMPRI_002_AGetValue
    Left = 472
    Top = 316
    Datasets = <
      item
        DataSet = frxDsVendas
        DataSetName = 'frxDsVendas'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 88.818946460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VENDAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[PERIODO_TXT]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Top = 75.590600000000000000
          Width = 15.118110240000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 75.590600000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde vendida')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 75.590600000000000000
          Width = 37.795260940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'N'#186' atend.')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Top = 75.590600000000000000
          Width = 52.913380940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo de Produto')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 75.590600000000000000
          Width = 294.803320470000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 75.590600000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 75.590600000000000000
          Width = 71.811030940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 52.913390710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 75.590600000000000000
          Width = 37.795260940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 75.590600000000000000
          Width = 56.692910940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor venda')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de Grupo: [VFR_NO_PGT]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 37.795300000000000000
          Width = 340.157480310000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Produto: [VFR_NO_PRD]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 13.228346460000000000
        ParentFont = False
        Top = 170.078850000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVendas
        DataSetName = 'frxDsVendas'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Width = 15.118110240000000000
          Height = 13.228346460000000000
          DataField = 'SIGLA'
          DataSet = frxDsVendas
          DataSetName = 'frxDsVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVendas."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DataField = 'Quantidade'
          DataSet = frxDsVendas
          DataSetName = 'frxDsVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendas."Quantidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 37.795260940000000000
          Height = 13.228346460000000000
          DataField = 'Codigo'
          DataSet = frxDsVendas
          DataSetName = 'frxDsVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendas."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Width = 52.913380940000000000
          Height = 13.228346460000000000
          DataField = 'NO_PGT'
          DataSet = frxDsVendas
          DataSetName = 'frxDsVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVendas."NO_PGT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Width = 294.803300940000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD'
          DataSet = frxDsVendas
          DataSetName = 'frxDsVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVendas."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 30.236200940000000000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsVendas
          DataSetName = 'frxDsVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVendas."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 71.811030940000000000
          Height = 13.228346460000000000
          DataField = 'Referencia'
          DataSet = frxDsVendas
          DataSetName = 'frxDsVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVendas."Referencia"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913390710000000000
          Height = 13.228346460000000000
          DataField = 'DtHrSai'
          DataSet = frxDsVendas
          DataSetName = 'frxDsVendas'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVendas."DtHrSai"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Width = 37.795260940000000000
          Height = 13.228346460000000000
          DataField = 'PrcUni'
          DataSet = frxDsVendas
          DataSetName = 'frxDsVendas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendas."PrcUni"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Width = 56.692910940000000000
          Height = 13.228346460000000000
          DataField = 'ValorTotal'
          DataSet = frxDsVendas
          DataSetName = 'frxDsVendas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendas."ValorTotal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsVendas: TfrxDBDataset
    UserName = 'frxDsVendas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'CtrID=CtrID'
      'Item=Item'
      'ManejoLca=ManejoLca'
      'GraGruX=GraGruX'
      'Unidade=Unidade'
      'Quantidade=Quantidade'
      'PrcUni=PrcUni'
      'ValorTotal=ValorTotal'
      'QtdeProduto=QtdeProduto'
      'ValorProduto=ValorProduto'
      'QtdeLocacao=QtdeLocacao'
      'ValorLocacao=ValorLocacao'
      'QtdeDevolucao=QtdeDevolucao'
      'ValDevolParci=ValDevolParci'
      'Troca=Troca'
      'Categoria=Categoria'
      'COBRANCALOCACAO=COBRANCALOCACAO'
      'CobrancaConsumo=CobrancaConsumo'
      'CobrancaRealizadaVenda=CobrancaRealizadaVenda'
      'CobranIniDtHr=CobranIniDtHr'
      'SaiDtHr=SaiDtHr'
      'SMIA_OriCtrl=SMIA_OriCtrl'
      'SMIA_IDCtrl=SMIA_IDCtrl'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'prod_vFrete=prod_vFrete'
      'prod_vSeg=prod_vSeg'
      'prod_vDesc=prod_vDesc'
      'prod_vOutro=prod_vOutro'
      'StqCenCad=StqCenCad'
      'NF_Stat=NF_Stat'
      'PercDesco=PercDesco'
      'ValorBruto=ValorBruto'
      'Referencia=Referencia'
      'Nivel1=Nivel1'
      'NO_PRD=NO_PRD'
      'PrdGrupTip=PrdGrupTip'
      'UnidMed=UnidMed'
      'NO_PGT=NO_PGT'
      'SIGLA=SIGLA'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'GraCorCad=GraCorCad'
      'GraGruC=GraGruC'
      'GraGru1=GraGru1'
      'GraTamI=GraTamI'
      'DtHrSai=DtHrSai')
    DataSet = QrVendas
    BCDToCurrency = False
    DataSetOptions = []
    Left = 285
    Top = 409
  end
end
