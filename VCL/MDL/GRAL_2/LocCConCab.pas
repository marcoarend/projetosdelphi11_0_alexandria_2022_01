unit LocCConCab;

interface

uses
  System.TypInfo,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, dmkEditDateTimePicker,
  dmkValUsu, dmkMemo, NFSe_PF_0000, UnDmkEnums, frxClass, dmkCompoStore,
  Vcl.ComCtrls, Data.DB, Variants, UnAppEnums, UnAppPF, DateUtils, AppListas,
  dmkDBGridZTO, NFe_PF, UnGrl_Consts, UnProjGroup_Vars, UnitWin;

type
{
  TColorDBEdit = Class (Vcl.DBCtrls.TDBEdit);

  TEditStyleHookColor = class(TEditStyleHook)
  private
    procedure UpdateColors;
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AControl: TWinControl); override;
  end;

  TMemoStyleHookColor = class(TMemoStyleHook)
  private
    procedure UpdateColors;
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AControl: TWinControl); override;
  end;
}

  //
  THackDBGrid = class(TDBGrid);
  //
  TAvaliacao = (avalIni, avalFim);
  //
  TFmLocCConCab = class(TForm)
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrLocCConCab: TMySQLQuery;
    DsLocCConCab: TDataSource;
    QrLocIPatPri: TMySQLQuery;
    DsLocIPatPri: TDataSource;
    PMLocIItsLca: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    QrLocCConCabNO_COMPRADOR: TWideStringField;
    QrLocCConCabNO_RECEBEU: TWideStringField;
    QrLocCConCabCodigo: TIntegerField;
    QrLocCConCabContrato: TIntegerField;
    QrLocCConCabCliente: TIntegerField;
    QrLocCConCabDtHrEmi: TDateTimeField;
    QrLocCConCabDtHrBxa: TDateTimeField;
    QrLocCConCabVendedor: TIntegerField;
    QrLocCConCabECComprou: TIntegerField;
    QrLocCConCabECRetirou: TIntegerField;
    QrLocCConCabNumOC: TWideStringField;
    QrLocCConCabValorTot: TFloatField;
    QrLocCConCabLocalObra: TWideStringField;
    QrLocCConCabObs0: TWideStringField;
    QrLocCConCabObs1: TWideStringField;
    QrLocCConCabObs2: TWideStringField;
    QrLocCConCabNO_LOGIN: TWideStringField;
    QrLocCConCabNO_CLIENTE: TWideStringField;
    QrLocIPatPriCodigo: TIntegerField;
    QrLocIPatPriCtrID: TIntegerField;
    QrLocIPatPriGraGruX: TIntegerField;
    QrLocIPatPriValorDia: TFloatField;
    QrLocIPatPriValorSem: TFloatField;
    QrLocIPatPriValorQui: TFloatField;
    QrLocIPatPriValorMes: TFloatField;
    QrLocIPatPriDtHrRetorn: TDateTimeField;
    QrLocIPatPriLibFunci: TIntegerField;
    QrLocIPatPriLibDtHr: TDateTimeField;
    QrLocIPatPriRELIB: TWideStringField;
    QrLocIPatPriHOLIB: TWideStringField;
    QrLocIPatPriNO_GGX: TWideStringField;
    QrLocIPatPriLOGIN: TWideStringField;
    QrLocIPatPriREFERENCIA: TWideStringField;
    QrLocIPatPriTXT_DTA_DEVOL: TWideStringField;
    QrLocIPatPriTXT_DTA_LIBER: TWideStringField;
    QrLocIPatPriTXT_QEM_LIBER: TWideStringField;
    QrLocIPatSec: TMySQLQuery;
    DsLocIPatSec: TDataSource;
    QrLocIPatSecCodigo: TIntegerField;
    QrLocIPatSecCtrID: TIntegerField;
    QrLocIPatSecItem: TIntegerField;
    QrLocIPatSecGraGruX: TIntegerField;
    QrLocIPatSecNO_GGX: TWideStringField;
    QrLocIPatSecREFERENCIA: TWideStringField;
    QrLocIPatAce: TMySQLQuery;
    DsLocIPatAce: TDataSource;
    QrLocIPatCns: TMySQLQuery;
    DsLocIPatCns: TDataSource;
    QrLocIPatUso: TMySQLQuery;
    DsLocIPatUso: TDataSource;
    QrLocIPatAceCodigo: TIntegerField;
    QrLocIPatAceCtrID: TIntegerField;
    QrLocIPatAceItem: TIntegerField;
    QrLocIPatAceGraGruX: TIntegerField;
    QrLocIPatAceNO_GGX: TWideStringField;
    QrLocIPatAceREFERENCIA: TWideStringField;
    QrLocIPatCnsCodigo: TIntegerField;
    QrLocIPatCnsCtrID: TIntegerField;
    QrLocIPatCnsItem: TIntegerField;
    QrLocIPatCnsGraGruX: TIntegerField;
    QrLocIPatCnsNO_GGX: TWideStringField;
    QrLocIPatCnsREFERENCIA: TWideStringField;
    QrLocIPatUsoCodigo: TIntegerField;
    QrLocIPatUsoCtrID: TIntegerField;
    QrLocIPatUsoItem: TIntegerField;
    QrLocIPatUsoGraGruX: TIntegerField;
    QrLocIPatUsoNO_GGX: TWideStringField;
    QrLocIPatUsoREFERENCIA: TWideStringField;
    QrLocFCab: TmySQLQuery;
    DsLocFCab: TDataSource;
    DsLctFatRef: TDataSource;
    QrLctFatRef: TmySQLQuery;
    QrLctFatRefPARCELA: TIntegerField;
    QrLctFatRefCodigo: TIntegerField;
    QrLctFatRefControle: TIntegerField;
    QrLctFatRefConta: TIntegerField;
    QrLctFatRefLancto: TLargeintField;
    QrLctFatRefValor: TFloatField;
    QrLctFatRefVencto: TDateField;
    QrLocIPatCnsUnidade: TIntegerField;
    QrLocIPatUsoAvalIni: TIntegerField;
    QrLocIPatUsoValUso: TFloatField;
    QrLocIPatCnsQtdIni: TFloatField;
    QrLocIPatCnsQtdFim: TFloatField;
    QrLocIPatCnsPrcUni: TFloatField;
    QrLocIPatCnsValUso: TFloatField;
    Panel11: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    DsClientes: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrECComprou: TmySQLQuery;
    QrECComprouControle: TIntegerField;
    QrECComprouNome: TWideStringField;
    DsECComprou: TDataSource;
    QrECRetirou: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsECRetirou: TDataSource;
    EdECComprou: TdmkEditCB;
    CBECComprou: TdmkDBLookupComboBox;
    CBECRetirou: TdmkDBLookupComboBox;
    EdECRetirou: TdmkEditCB;
    EdVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    QrSenhas: TmySQLQuery;
    QrSenhasNumero: TIntegerField;
    QrSenhaslogin: TWideStringField;
    DsSenhas: TDataSource;
    EdNumOC: TdmkEdit;
    EdLocalObra: TdmkEdit;
    QrContratos: TmySQLQuery;
    DsContratos: TDataSource;
    QrContratosCodigo: TIntegerField;
    QrContratosNome: TWideStringField;
    EdObs0: TdmkEdit;
    EdObs1: TdmkEdit;
    EdObs2: TdmkEdit;
    QrLocCConCabDataEmi: TDateField;
    QrLocCConCabHoraEmi: TTimeField;
    QrLocIPatUsoUnidade: TIntegerField;
    QrLocIPatUsoSIGLA: TWideStringField;
    QrLocIPatCnsSIGLA: TWideStringField;
    QrLocIPatUsoNO_AVALINI: TWideStringField;
    QrLocIPatUsoNO_AVALFIM: TWideStringField;
    QrLocIPatUsoAvalFim: TIntegerField;
    QrLocIPatCnsQtdUso: TFloatField;
    N1: TMenuItem;
    Libera1: TMenuItem;
    Retorna1: TMenuItem;
    N2: TMenuItem;
    QrLocIPatPriDtHrLocado: TDateTimeField;
    QrLocIPatUsoPrcUni: TFloatField;
    QrLocCConCabDtHrBxa_TXT: TWideStringField;
    Panel7: TPanel;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label21: TLabel;
    TPDataEmi: TdmkEditDateTimePicker;
    EdHoraEmi: TdmkEdit;
    QrLocCConCabEmpresa: TIntegerField;
    QrLocCConCabNO_EMP: TWideStringField;
    QrLcts: TmySQLQuery;
    QrLocFCabCodigo: TIntegerField;
    QrLocFCabControle: TIntegerField;
    QrLocFCabDtHrFat: TDateTimeField;
    QrLocFCabValLocad: TFloatField;
    QrLocFCabValConsu: TFloatField;
    QrLocFCabValUsado: TFloatField;
    QrLocFCabValDesco: TFloatField;
    QrLocFCabValTotal: TFloatField;
    QrLocFCabSerNF: TWideStringField;
    QrLocFCabNumNF: TIntegerField;
    QrLocFCabCondicaoPG: TIntegerField;
    QrLocFCabCartEmis: TIntegerField;
    PMFat: TPopupMenu;
    Faturamentoparcial1: TMenuItem;
    Faturaeencerracontrato1: TMenuItem;
    N3: TMenuItem;
    Excluifaturamento1: TMenuItem;
    Gerabloqueto1: TMenuItem;
    N4: TMenuItem;
    Label7: TLabel;
    EdContrato: TdmkEditCB;
    CBContrato: TdmkDBLookupComboBox;
    Label24: TLabel;
    EdLocalCntat: TdmkEdit;
    Label29: TLabel;
    EdLocalFone: TdmkEdit;
    Label30: TLabel;
    EdEndCobra: TdmkEdit;
    QrLocCConCabEndCobra: TWideStringField;
    QrLocCConCabLocalCntat: TWideStringField;
    QrLocCConCabLocalFone: TWideStringField;
    VUEmpresa: TdmkValUsu;
    SbCliente: TSpeedButton;
    QrLocCConCabFilial: TIntegerField;
    PMLocIPatAce: TPopupMenu;
    Adicionaacessrio1: TMenuItem;
    Removeacessrio1: TMenuItem;
    PMLocIPatUso: TPopupMenu;
    Adicionamaterialuso1: TMenuItem;
    Removematerialuso1: TMenuItem;
    PMLocIPatCns: TPopupMenu;
    Adicionamaterialdeconsumo1: TMenuItem;
    Removematerialdeconsumo1: TMenuItem;
    Encerracontrato1: TMenuItem;
    Reabrelocao1: TMenuItem;
    EdEndCliente: TdmkEdit;
    Label34: TLabel;
    QrLocIPatPriLibExUsr: TIntegerField;
    ItsExclui2: TMenuItem;
    ItsExclui3: TMenuItem;
    SbImagens: TBitBtn;
    PMImagens: TPopupMenu;
    Visualizarimagemdocliente1: TMenuItem;
    Visualizarimagemdorequisitante1: TMenuItem;
    Visualizarimagemdorecebedor1: TMenuItem;
    N5: TMenuItem;
    BtNFSe: TBitBtn;
    Visualizarbloquetos1: TMenuItem;
    PMImprime: TPopupMenu;
    Contrato1: TMenuItem;
    Devoluodelocao1: TMenuItem;
    CSTabSheetChamou: TdmkCompoStore;
    Selecionados1: TMenuItem;
    odos1: TMenuItem;
    QrLoc: TmySQLQuery;
    RGFormaCobrLoca: TdmkRadioGroup;
    GroupBox8: TGroupBox;
    QrLocCConCabTipoAluguel: TWideStringField;
    QrLocCConCabHistNaoImpr: TWideMemoField;
    QrLocCConCabDtHrSai: TDateTimeField;
    QrLocCConCabValorAdicional: TFloatField;
    QrLocCConCabValorAdiantamento: TFloatField;
    QrLocCConCabValorDesconto: TFloatField;
    QrLocCConCabStatus: TIntegerField;
    QrLocCConCabDtUltCobMens: TDateField;
    QrLocCConCabImportado: TSmallintField;
    QrLocCConCabDtHrDevolver: TDateTimeField;
    QrLocCConCabValorEquipamentos: TFloatField;
    QrLocCConCabValorLocacao: TFloatField;
    QrLocCConCabFormaCobrLoca: TSmallintField;
    LaDtHrSai: TLabel;
    EDDtHrSai: TdmkEdit;
    TPDtHrDevolver: TdmkEditDateTimePicker;
    Label36: TLabel;
    EdDtHrDevolver: TdmkEdit;
    QrLocIPatPriRetFunci: TIntegerField;
    QrLocIPatPriRetExUsr: TIntegerField;
    QrLocIPatPriValorProduto: TFloatField;
    QrLocIPatPriQtdeLocacao: TIntegerField;
    QrLocIPatPriValorLocacao: TFloatField;
    QrLocIPatPriQtdeDevolucao: TIntegerField;
    QrLocIPatPriTroca: TSmallintField;
    QrLocIPatPriCategoria: TWideStringField;
    QrLocIPatPriCobrancaConsumo: TFloatField;
    QrLocIPatPriCobrancaRealizadaVenda: TWideStringField;
    QrLocIPatPriSaiDtHr: TDateTimeField;
    QrLocIPatPriQtdeProduto: TIntegerField;
    QrLocIPatPriCOBRANCALOCACAO: TWideStringField;
    QrLocIPatPriCobranIniDtHr: TDateTimeField;
    QrLocCConIts: TMySQLQuery;
    DsLocCConIts: TDataSource;
    QrLocCConItsNO_IDTab: TWideStringField;
    QrLocCConItsCodigo: TIntegerField;
    QrLocCConItsCtrID: TIntegerField;
    QrLocCConItsIDTab: TIntegerField;
    QrLocIPatPriItem: TIntegerField;
    QrLocIPatApo: TMySQLQuery;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    WideStringField1: TWideStringField;
    WideStringField2: TWideStringField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    IntegerField8: TIntegerField;
    WideStringField3: TWideStringField;
    DsLocIPatApo: TDataSource;
    QrLocCItsSvc: TMySQLQuery;
    DsLocCItsSvc: TDataSource;
    QrLocCItsSvcCodigo: TIntegerField;
    QrLocCItsSvcCtrID: TIntegerField;
    QrLocCItsSvcItem: TIntegerField;
    QrLocCItsSvcManejoLca: TSmallintField;
    QrLocCItsSvcGraGruX: TIntegerField;
    QrLocCItsSvcQuantidade: TFloatField;
    QrLocCItsSvcPrcUni: TFloatField;
    QrLocCItsSvcValorTotal: TFloatField;
    QrLocCItsSvcQtdeProduto: TIntegerField;
    QrLocCItsSvcValorProduto: TFloatField;
    QrLocCItsSvcQtdeLocacao: TIntegerField;
    QrLocCItsSvcValorLocacao: TFloatField;
    QrLocCItsSvcQtdeDevolucao: TIntegerField;
    QrLocCItsSvcTroca: TSmallintField;
    QrLocCItsSvcCategoria: TWideStringField;
    QrLocCItsSvcCOBRANCALOCACAO: TWideStringField;
    QrLocCItsSvcCobrancaConsumo: TFloatField;
    QrLocCItsSvcCobrancaRealizadaVenda: TWideStringField;
    QrLocCItsSvcCobranIniDtHr: TDateTimeField;
    QrLocCItsSvcSaiDtHr: TDateTimeField;
    QrLocCItsSvcLk: TIntegerField;
    QrLocCItsSvcDataCad: TDateField;
    QrLocCItsSvcDataAlt: TDateField;
    QrLocCItsSvcUserCad: TIntegerField;
    QrLocCItsSvcUserAlt: TIntegerField;
    QrLocCItsSvcAlterWeb: TSmallintField;
    QrLocCItsSvcAWServerID: TIntegerField;
    QrLocCItsSvcAWStatSinc: TSmallintField;
    QrLocCItsSvcAtivo: TSmallintField;
    QrLocCItsSvcNO_GGX: TWideStringField;
    QrLocCItsSvcREFERENCIA: TWideStringField;
    QrLocCItsVen: TMySQLQuery;
    DsLocCItsVen: TDataSource;
    QrLocCItsVenCodigo: TIntegerField;
    QrLocCItsVenCtrID: TIntegerField;
    QrLocCItsVenItem: TIntegerField;
    QrLocCItsVenManejoLca: TSmallintField;
    QrLocCItsVenGraGruX: TIntegerField;
    QrLocCItsVenUnidade: TIntegerField;
    QrLocCItsVenQuantidade: TFloatField;
    QrLocCItsVenPrcUni: TFloatField;
    QrLocCItsVenValorTotal: TFloatField;
    QrLocCItsVenQtdeProduto: TIntegerField;
    QrLocCItsVenValorProduto: TFloatField;
    QrLocCItsVenQtdeLocacao: TIntegerField;
    QrLocCItsVenValorLocacao: TFloatField;
    QrLocCItsVenQtdeDevolucao: TIntegerField;
    QrLocCItsVenTroca: TSmallintField;
    QrLocCItsVenCategoria: TWideStringField;
    QrLocCItsVenCOBRANCALOCACAO: TWideStringField;
    QrLocCItsVenCobrancaConsumo: TFloatField;
    QrLocCItsVenCobrancaRealizadaVenda: TWideStringField;
    QrLocCItsVenCobranIniDtHr: TDateTimeField;
    QrLocCItsVenSaiDtHr: TDateTimeField;
    QrLocCItsVenLk: TIntegerField;
    QrLocCItsVenDataCad: TDateField;
    QrLocCItsVenDataAlt: TDateField;
    QrLocCItsVenUserCad: TIntegerField;
    QrLocCItsVenUserAlt: TIntegerField;
    QrLocCItsVenAlterWeb: TSmallintField;
    QrLocCItsVenAWServerID: TIntegerField;
    QrLocCItsVenAWStatSinc: TSmallintField;
    QrLocCItsVenAtivo: TSmallintField;
    QrLocCItsVenNO_GGX: TWideStringField;
    QrLocCItsVenREFERENCIA: TWideStringField;
    QrLocCItsVenSIGLA: TWideStringField;
    PnTop: TPanel;
    PnInfoTop: TPanel;
    QrLocCConCabNO_STATUS: TWideStringField;
    QrLocCConCabNO_TipoAluguel: TWideStringField;
    Label37: TLabel;
    DBEdit17: TDBEdit;
    Label38: TLabel;
    DBEdNO_STATUS: TDBEdit;
    //DBEdNO_STATUS: TColorDBEdit;
    PnDados: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCab: TBitBtn;
    BtLocCItsLca: TBitBtn;
    BtFat: TBitBtn;
    GBDados: TPanel;
    Panel6: TPanel;
    Panel12: TPanel;
    Label20: TLabel;
    Label5: TLabel;
    Label31: TLabel;
    DBEdit3: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    Panel14: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label10: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    DBEdNome: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBGLocIConIts: TDBGrid;
    PCMovimento: TPageControl;
    TabSheet1: TTabSheet;
    Splitter1: TSplitter;
    GroupBox1: TGroupBox;
    DBGLocIPatPri: TDBGrid;
    TabSheet4: TTabSheet;
    DBLocCItsSvc: TDBGrid;
    TabSheet5: TTabSheet;
    DBGLocCItsVen: TDBGrid;
    QrLocCConCabDtHrSai_TXT: TWideStringField;
    QrLocCConCabDtHrDevolver_TXT: TWideStringField;
    Label39: TLabel;
    DBEdit19: TDBEdit;
    Label40: TLabel;
    DBEdit20: TDBEdit;
    TPDtHrSai: TdmkEditDateTimePicker;
    RGTipoAluguel: TdmkRadioGroup;
    BtForma: TBitBtn;
    BtData: TBitBtn;
    Mudastatus1: TMenuItem;
    BtLocCConIts: TBitBtn;
    PMLocIConIts: TPopupMenu;
    ExcluiMovimento1: TMenuItem;
    BtLocCItsSvc: TBitBtn;
    BtLocCItsVen: TBitBtn;
    PMLocIPatApo: TPopupMenu;
    AdicionamaterialdeApoio1: TMenuItem;
    RemovematerialdeApoio1: TMenuItem;
    PMLocIPatSec: TPopupMenu;
    AdicionaequipamentoSecundrio1: TMenuItem;
    RemoveequipamentoSecundrio1: TMenuItem;
    QrLocIPatAceQtdeProduto: TIntegerField;
    QrLocIPatAceValorProduto: TFloatField;
    QrLocIPatAceQtdeLocacao: TIntegerField;
    QrLocIPatAceValorLocacao: TFloatField;
    QrLocIPatAceQtdeDevolucao: TIntegerField;
    QrLocIPatSecValorDia: TFloatField;
    QrLocIPatSecValorSem: TFloatField;
    QrLocIPatSecValorQui: TFloatField;
    QrLocIPatSecValorMes: TFloatField;
    QrLocIPatSecDtHrLocado: TDateTimeField;
    QrLocIPatSecDtHrRetorn: TDateTimeField;
    QrLocIPatSecQtdeProduto: TIntegerField;
    QrLocIPatSecValorProduto: TFloatField;
    QrLocIPatSecQtdeLocacao: TIntegerField;
    QrLocIPatSecValorLocacao: TFloatField;
    QrLocIPatSecQtdeDevolucao: TIntegerField;
    QrLocIPatAceDtHrLocado: TDateTimeField;
    QrLocIPatAceDtHrRetorn: TDateTimeField;
    QrLocCConCabDtUltAtzPend: TDateField;
    QrItsLca: TMySQLQuery;
    QrItsLcaCodigo: TIntegerField;
    QrItsLcaCtrID: TIntegerField;
    QrItsLcaItem: TIntegerField;
    QrItsLcaManejoLca: TSmallintField;
    QrItsLcaGraGruX: TIntegerField;
    QrItsLcaValorDia: TFloatField;
    QrItsLcaValorSem: TFloatField;
    QrItsLcaValorQui: TFloatField;
    QrItsLcaValorMes: TFloatField;
    QrItsLcaRetFunci: TIntegerField;
    QrItsLcaRetExUsr: TIntegerField;
    QrItsLcaDtHrLocado: TDateTimeField;
    QrItsLcaDtHrRetorn: TDateTimeField;
    QrItsLcaLibExUsr: TIntegerField;
    QrItsLcaLibFunci: TIntegerField;
    QrItsLcaLibDtHr: TDateTimeField;
    QrItsLcaRELIB: TWideStringField;
    QrItsLcaHOLIB: TWideStringField;
    QrItsLcaQtdeProduto: TIntegerField;
    QrItsLcaValorProduto: TFloatField;
    QrItsLcaQtdeLocacao: TIntegerField;
    QrItsLcaValorLocacao: TFloatField;
    QrItsLcaQtdeDevolucao: TIntegerField;
    QrItsLcaTroca: TSmallintField;
    QrItsLcaCategoria: TWideStringField;
    QrItsLcaCOBRANCALOCACAO: TWideStringField;
    QrItsLcaCobrancaConsumo: TFloatField;
    QrItsLcaCobrancaRealizadaVenda: TWideStringField;
    QrItsLcaCobranIniDtHr: TDateTimeField;
    QrItsLcaSaiDtHr: TDateTimeField;
    QrItsLcaDMenos: TIntegerField;
    QrItsLcaValorFDS: TFloatField;
    MudastatusparaLocado1: TMenuItem;
    MudastatusparaAberto1: TMenuItem;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    QrLocCConCabValorASerAdiantado: TFloatField;
    QrLocCConCabValorProdutos: TFloatField;
    QrLocCConCabValorLocAAdiantar: TFloatField;
    QrLocCConCabValorLocAtualizado: TFloatField;
    DBEdit21: TDBEdit;
    Label41: TLabel;
    Label42: TLabel;
    DBEdit22: TDBEdit;
    Label43: TLabel;
    DBEdit23: TDBEdit;
    ExcluiMovimentoetodosseusitens1: TMenuItem;
    PMLocIItsSvc: TPopupMenu;
    AdicionaServico1: TMenuItem;
    RemoveServico1: TMenuItem;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    QrLocCConItsStatus: TSmallintField;
    QrLocCConItsNO_STATUS: TWideStringField;
    PMLocIItsVen: TPopupMenu;
    AdicionaVenda1: TMenuItem;
    Removevenda1: TMenuItem;
    QrLocCConItsValorOrca: TFloatField;
    QrLocCConItsValorPedi: TFloatField;
    QrLocCConItsValorFatu: TFloatField;
    QrLocCConItsValorStat: TFloatField;
    IncluiMovimento1: TMenuItem;
    QrLocCConCabValorServicos: TFloatField;
    QrLocCConCabValorPago: TFloatField;
    QrLocCConCabValorVendas: TFloatField;
    QrLocCConCabValorOrca: TFloatField;
    QrLocCConCabValorPedi: TFloatField;
    QrLocCConCabValorFatu: TFloatField;
    Label44: TLabel;
    DBEdit18: TDBEdit;
    Label45: TLabel;
    DBEdit24: TDBEdit;
    Label46: TLabel;
    DBEdit25: TDBEdit;
    N7: TMenuItem;
    Recalculatudo1: TMenuItem;
    QrLocCItsVenSMIA_OriCtrl: TIntegerField;
    QrLocCItsVenSMIA_IDCtrl: TIntegerField;
    QrItsLcaValorLocAtualizado: TFloatField;
    QrItsLcaValDevolParci: TFloatField;
    N8: TMenuItem;
    PMLocIPatPri: TPopupMenu;
    Devolues1: TMenuItem;
    QrLocIPatPriManejoLca: TSmallintField;
    Devolues2: TMenuItem;
    QrLocIPatSecManejoLca: TSmallintField;
    QrLocIPatAceManejoLca: TSmallintField;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    Panel8: TPanel;
    Splitter5: TSplitter;
    GroupBox4: TGroupBox;
    DBGrid3: TDBGrid;
    GroupBox7: TGroupBox;
    DBGLctFatRef: TDBGrid;
    Panel15: TPanel;
    Splitter2: TSplitter;
    Panel13: TPanel;
    Panel9: TPanel;
    Splitter3: TSplitter;
    Panel10: TPanel;
    GroupBox3: TGroupBox;
    DBGLocIPatSec: TDBGrid;
    GroupBox5: TGroupBox;
    DBGLocIPatAce: TDBGrid;
    QrLocIPatUsoValorRucAAdiantar: TFloatField;
    QrLocIPatCnsValorRucAAdiantar: TFloatField;
    Panel16: TPanel;
    ImgCliente: TImage;
    ImgRequisitante: TImage;
    ImgRetirada: TImage;
    PnEntiStatus: TPanel;
    QrEntiStatus: TMySQLQuery;
    QrEntiStatusSELECI: TWideStringField;
    QrEntiStatusStatus: TIntegerField;
    QrEntiStatusNO_Status: TWideStringField;
    DsEntiStatus: TDataSource;
    DBGEntiStatus: TdmkDBGridZTO;
    QrLocCConCabHistImprime: TWideMemoField;
    DBMeHistImprime: TDBMemo;
    Splitter7: TSplitter;
    DBMemo2: TDBMemo;
    Cancelado1: TMenuItem;
    Oramentodevenda1: TMenuItem;
    QrLocFCabValVenda: TFloatField;
    QrLocFCabValFrete: TFloatField;
    QrLocFCabValServi: TFloatField;
    DBEdit26: TDBEdit;
    Label47: TLabel;
    Pedidodevenda1: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    Imprimerecibo1: TMenuItem;
    QrLocFCabCodHist: TIntegerField;
    Devolues3: TMenuItem;
    Devolues4: TMenuItem;
    DBEdit27: TDBEdit;
    AlteraStatusdoMovimento1: TMenuItem;
    N6: TMenuItem;
    Oramento2: TMenuItem;
    Pedido2: TMenuItem;
    OramentodeLocao1: TMenuItem;
    N11: TMenuItem;
    QrLocCItsSvcNO_Atrelamento: TWideStringField;
    QrPsqSum: TMySQLQuery;
    QrEndContratante: TMySQLQuery;
    QrEndContratanteCodigo: TIntegerField;
    QrEndContratanteTipo: TSmallintField;
    QrEndContratanteNOMELOGRAD: TWideStringField;
    QrEndContratanteNOMERUA: TWideStringField;
    QrEndContratanteCOMPL: TWideStringField;
    QrEndContratanteBAIRRO: TWideStringField;
    QrEndContratanteNOMEUF: TWideStringField;
    QrEndContratanteCNPJ_CPF: TWideStringField;
    QrEndContratanteIE_RG: TWideStringField;
    QrEndContratanteTEL: TWideStringField;
    QrEndContratanteFAX: TWideStringField;
    QrEndContratanteCEP: TFloatField;
    QrEndContratanteTEL_TXT: TWideStringField;
    QrEndContratanteCONTRATANTE: TWideStringField;
    QrEndContratanteFAX_TXT: TWideStringField;
    QrEndContratanteCNPJ_TXT: TWideStringField;
    QrEndContratanteCEP_TXT: TWideStringField;
    QrEndContratanteNUMERO: TFloatField;
    QrEndContratanteNO_CIDADE: TWideStringField;
    QrEndContratanteNO_PAIS: TWideStringField;
    QrLocCItsVenGraGru1: TIntegerField;
    QrLocCItsVenprod_vFrete: TFloatField;
    QrLocCItsVenprod_vSeg: TFloatField;
    QrLocCItsVenprod_vDesc: TFloatField;
    QrLocCItsVenprod_vOutro: TFloatField;
    QrLocCItsVenStqCenCad: TIntegerField;
    PCMateriais: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet6: TTabSheet;
    DBGLocIPatUso: TDBGrid;
    DBGLocIPatCns: TDBGrid;
    DBGLocIPatApo: TDBGrid;
    EdCodigo: TdmkEdit;
    Label16: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label1: TLabel;
    Label48: TLabel;
    QrLocIPatPriPATRIMONIO: TWideStringField;
    Devolvertodosequipamentos1: TMenuItem;
    N12: TMenuItem;
    QrAux: TMySQLQuery;
    Suspenso1: TMenuItem;
    QrLocIts: TMySQLQuery;
    QrLocItsCtrID: TIntegerField;
    QrLocItsFatPedCab: TIntegerField;
    QrLocItsStatus: TIntegerField;
    QrLocItsNF_Stat: TIntegerField;
    Atualizatotaidomovimento1: TMenuItem;
    QrLocCItsVenPercDesco: TFloatField;
    QrLocCItsVenValorBruto: TFloatField;
    QrLocCConItsValorBrto: TFloatField;
    QrLocCConCabValorBrto: TFloatField;
    Label49: TLabel;
    DBEdit28: TDBEdit;
    EmiteNFCe2: TMenuItem;
    QrLocCConItsCondiOrca: TIntegerField;
    QrLocCConItsNO_CondiOrca: TWideStringField;
    Alteracondiodepagamentodooramento1: TMenuItem;
    Cancelado2: TMenuItem;
    N13: TMenuItem;
    Devolues5: TMenuItem;
    QrLocCConCabJaLocou: TSmallintField;
    Finalizado1: TMenuItem;
    QrValAberto: TMySQLQuery;
    QrValAbertoAberto: TFloatField;
    QrValAbertoVencido: TFloatField;
    DBText1: TDBText;
    DBText2: TDBText;
    Label35: TLabel;
    Label50: TLabel;
    DsValAberto: TDataSource;
    Timer1: TTimer;
    DBEdit29: TDBEdit;
    Label51: TLabel;
    QrLocCConCabValorPndt: TFloatField;
    Fichadetroca1: TMenuItem;
    Equipamentoprincipal1: TMenuItem;
    Equipamentosecundrio1: TMenuItem;
    Acessrio1: TMenuItem;
    Indefinido1: TMenuItem;
    QrLocIPatPriTROCA_TXT: TWideStringField;
    QrLocIPatPriSubstituidoLca: TIntegerField;
    QrLocIPatPriSubstitutoLca: TIntegerField;
    QrLocIPatSecSubstituidoLca: TIntegerField;
    QrLocIPatSecSubstitutoLca: TIntegerField;
    QrLocIPatAceSubstituidoLca: TIntegerField;
    QrLocIPatAceSubstitutoLca: TIntegerField;
    EmiteNFe1: TMenuItem;
    QrLocCConItsLk: TIntegerField;
    QrLocCConItsDataCad: TDateField;
    QrLocCConItsDataAlt: TDateField;
    QrLocCConItsUserCad: TIntegerField;
    QrLocCConItsUserAlt: TIntegerField;
    QrLocCConItsAlterWeb: TSmallintField;
    QrLocCConItsAWServerID: TIntegerField;
    QrLocCConItsAWStatSinc: TSmallintField;
    QrLocCConItsAtivo: TSmallintField;
    QrLocCConItsFatID: TIntegerField;
    QrLocCConItsFatNum: TIntegerField;
    QrLocCConItside_mod: TIntegerField;
    QrLocCConItside_nNF: TIntegerField;
    QrLocCConItsNF_Stat: TIntegerField;
    QrLocCConItsPediVda: TIntegerField;
    QrLocCConItsFatPedCab: TIntegerField;
    QrLocCConItsFatPedVolCnta: TIntegerField;
    QrLocCConItsNO_ide_mod: TWideStringField;
    SpeedButton6: TSpeedButton;
    Label52: TLabel;
    DBText3: TDBText;
    QrClientesLimiCred: TFloatField;
    QrLocFCabNO_CartEmis: TWideStringField;
    QrLocFCabNO_PediPrzCab: TWideStringField;
    Agora1: TMenuItem;
    Agoramostrandoclculo1: TMenuItem;
    Outradata1: TMenuItem;
    Outradatamostrandoclculo1: TMenuItem;
    QrLocFCabReciboImp: TIntegerField;
    QrItsLcaOriSubstSaiDH: TDateTimeField;
    QrLocIPatPriBAIXA_TXT: TWideStringField;
    Renovar1: TMenuItem;
    Contratofonte71: TMenuItem;
    Contratofonte101: TMenuItem;
    Alteratolerncia1: TMenuItem;
    QrLocIPatPriHrTolerancia: TTimeField;
    QrLocIPatPriHrTolerancia_TXT: TWideStringField;
    olerncia1: TMenuItem;
    QrItsLcaHrTolerancia: TTimeField;
    Fonte101: TMenuItem;
    Fonte71: TMenuItem;
    Fonte102: TMenuItem;
    Fonte72: TMenuItem;
    PMNFs: TPopupMenu;
    EmiteNFCe1: TMenuItem;
    EmiteNFe2: TMenuItem;
    N14: TMenuItem;
    MMCSnapIn1: TMenuItem;
    N15: TMenuItem;
    MMCSnapIn2: TMenuItem;
    N16: TMenuItem;
    Redefinir1: TMenuItem;
    QrLocCConItsNO_AtndCanal: TWideStringField;
    QrAtndCanal: TMySQLQuery;
    QrAtndCanalCodigo: TIntegerField;
    QrAtndCanalNome: TWideStringField;
    DsAtndCanal: TDataSource;
    Label53: TLabel;
    EdAtndCanal: TdmkEditCB;
    CBAtndCanal: TdmkDBLookupComboBox;
    SbAtndCanal: TSpeedButton;
    QrLocCConCabAtndCanal: TIntegerField;
    QrLocCConItsAtndCanal: TIntegerField;
    QrLocFCabValVeMaq: TFloatField;
    QrLocIPatAceQTDE: TFloatField;
    QrLocIPatSecQTDE: TFloatField;
    DBGrid1: TDBGrid;
    QrSmiaVen: TMySQLQuery;
    DsSmiaVen: TDataSource;
    QrSmiaVenDataHora: TDateTimeField;
    QrSmiaVenIDCtrl: TIntegerField;
    QrSmiaVenTipo: TIntegerField;
    QrSmiaVenOriCodi: TIntegerField;
    QrSmiaVenOriCtrl: TIntegerField;
    QrSmiaVenOriCnta: TIntegerField;
    QrSmiaVenEmpresa: TIntegerField;
    QrSmiaVenStqCencad: TIntegerField;
    QrSmiaVenGraGruX: TIntegerField;
    QrSmiaVenQtde: TFloatField;
    QrSmiaVenBaixa: TSmallintField;
    QrSmiaVenvDesc: TFloatField;
    QrSmiaVenBaixa_TXT: TWideStringField;
    Splitter4: TSplitter;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrLocCConCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrLocCConCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrLocCConCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtLocCItsLcaClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMLocIItsLcaPopup(Sender: TObject);
    procedure QrLocIPatPriCalcFields(DataSet: TDataSet);
    procedure QrLocCConCabBeforeClose(DataSet: TDataSet);
    procedure QrLocFCabAfterScroll(DataSet: TDataSet);
    procedure QrLocFCabBeforeClose(DataSet: TDataSet);
    procedure QrLocIPatCnsCalcFields(DataSet: TDataSet);
    procedure QrLctFatRefCalcFields(DataSet: TDataSet);
    procedure EdClienteChange(Sender: TObject);
    procedure QrLocCConCabCalcFields(DataSet: TDataSet);
    procedure DBGLocIPatCnsDblClick(Sender: TObject);
    procedure Libera1Click(Sender: TObject);
    procedure Retorna1Click(Sender: TObject);
    procedure DBGLocIPatUsoDblClick(Sender: TObject);
    procedure BtFatClick(Sender: TObject);
    procedure PMFatPopup(Sender: TObject);
    procedure Faturamentoparcial1Click(Sender: TObject);
    procedure Faturaeencerracontrato1Click(Sender: TObject);
    procedure Excluifaturamento1Click(Sender: TObject);
    procedure Gerabloqueto1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure DBGLocIPatAceMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Adicionaacessrio1Click(Sender: TObject);
    procedure Adicionamaterialuso1Click(Sender: TObject);
    procedure Adicionamaterialdeconsumo1Click(Sender: TObject);
    procedure DBGLocIPatUsoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGLocIPatCnsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PMLocIPatAcePopup(Sender: TObject);
    procedure PMLocIPatUsoPopup(Sender: TObject);
    procedure PMLocIPatCnsPopup(Sender: TObject);
    procedure Removeacessrio1Click(Sender: TObject);
    procedure Removematerialuso1Click(Sender: TObject);
    procedure Removematerialdeconsumo1Click(Sender: TObject);
    procedure Encerracontrato1Click(Sender: TObject);
    procedure Reabrelocao1Click(Sender: TObject);
    procedure ItsExclui2Click(Sender: TObject);
    procedure ItsExclui3Click(Sender: TObject);
    procedure Visualizarimagemdorequisitante1Click(Sender: TObject);
    procedure Visualizarimagemdorecebedor1Click(Sender: TObject);
    procedure Visualizarimagemdocliente1Click(Sender: TObject);
    procedure SbImagensClick(Sender: TObject);
    procedure PMImagensPopup(Sender: TObject);
    procedure EdECComprouChange(Sender: TObject);
    procedure EdECRetirouChange(Sender: TObject);
    procedure ImgClienteClick(Sender: TObject);
    procedure ImgRequisitanteClick(Sender: TObject);
    procedure ImgRetiradaClick(Sender: TObject);
    procedure BtNFSeClick(Sender: TObject);
    procedure Visualizarbloquetos1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EDDtHrSaiRedefinido(Sender: TObject);
    procedure TPDtHrDevolverClick(Sender: TObject);
    procedure EdDtHrDevolverRedefinido(Sender: TObject);
    procedure RGTipoAluguelClick(Sender: TObject);
    procedure QrLocCConItsAfterScroll(DataSet: TDataSet);
    procedure QrLocCConItsBeforeClose(DataSet: TDataSet);
    procedure ImgTipoChange(Sender: TObject);
    procedure BtDataClick(Sender: TObject);
    procedure BtFormaClick(Sender: TObject);
    procedure Loca1Click(Sender: TObject);
    procedure BtLocCConItsClick(Sender: TObject);
    procedure PMLocIConItsPopup(Sender: TObject);
    procedure PCMovimentoChanging(Sender: TObject; var AllowChange: Boolean);
    procedure PCMovimentoChange(Sender: TObject);
    procedure DBGLocIPatSecMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGLocIPatApoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AdicionamaterialdeApoio1Click(Sender: TObject);
    procedure RemovematerialdeApoio1Click(Sender: TObject);
    procedure AdicionaequipamentoSecundrio1Click(Sender: TObject);
    procedure RemoveequipamentoSecundrio1Click(Sender: TObject);
    procedure PMLocIPatSecPopup(Sender: TObject);
    procedure PMLocIPatApoPopup(Sender: TObject);
    procedure ExcluiMovimento1Click(Sender: TObject);
    procedure MudastatusparaLocado1Click(Sender: TObject);
    procedure MudastatusparaAberto1Click(Sender: TObject);
    procedure DBGLocIPatAceDblClick(Sender: TObject);
    procedure DBGLocIPatApoDblClick(Sender: TObject);
    procedure ExcluiMovimentoetodosseusitens1Click(Sender: TObject);
    procedure DBEdNO_STATUSChange(Sender: TObject);
    procedure BtLocCItsSvcClick(Sender: TObject);
    procedure PMLocIItsSvcPopup(Sender: TObject);
    procedure AdicionaServico1Click(Sender: TObject);
    procedure RemoveServico1Click(Sender: TObject);
    procedure BtLocCItsVenClick(Sender: TObject);
    procedure PMLocIItsVenPopup(Sender: TObject);
    procedure AdicionaVenda1Click(Sender: TObject);
    procedure Removevenda1Click(Sender: TObject);
    procedure IncluiMovimento1Click(Sender: TObject);
    procedure MeHistoricoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGLocIPatPriMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Devolues1Click(Sender: TObject);
    procedure Devolues2Click(Sender: TObject);
    procedure EdClienteRedefinido(Sender: TObject);
    procedure DBMeHistImprimeDblClick(Sender: TObject);
    procedure DBMemo2DblClick(Sender: TObject);
    procedure Cancelado1Click(Sender: TObject);
    procedure PMImprimePopup(Sender: TObject);
    procedure Selecionado1Click(Sender: TObject);
    procedure Imprimerecibo1Click(Sender: TObject);
    procedure Devolues3Click(Sender: TObject);
    procedure Devolues4Click(Sender: TObject);
    procedure DBEdit27DblClick(Sender: TObject);
    procedure Oramento2Click(Sender: TObject);
    procedure OramentodeLocao1Click(Sender: TObject);
    procedure Pedido2Click(Sender: TObject);
    procedure QrLocIPatApoAfterOpen(DataSet: TDataSet);
    procedure QrLocIPatCnsAfterOpen(DataSet: TDataSet);
    procedure QrLocIPatUsoAfterOpen(DataSet: TDataSet);
    procedure DBGLocIPatPriDblClick(Sender: TObject);
    procedure DBGLocIConItsDblClick(Sender: TObject);
    procedure DBGLocIPatSecDblClick(Sender: TObject);
    procedure Devolvertodosequipamentos1Click(Sender: TObject);
    procedure DBGLocCItsVenDblClick(Sender: TObject);
    procedure Suspenso1Click(Sender: TObject);
    procedure Atualizatotaidomovimento1Click(Sender: TObject);
    procedure OrcamentoSelecionadoVias1Click(Sender: TObject);
    procedure OrcamentoSelecionadoVias2Click(Sender: TObject);
    procedure OrcamentoTodosVias1Click(Sender: TObject);
    procedure OrcamentoTodosVias2Click(Sender: TObject);
    procedure PedidoTodosVias1Click(Sender: TObject);
    procedure PedidoTodosVias2Click(Sender: TObject);
    procedure PedidoSelecionadoVias1Click(Sender: TObject);
    procedure PedidoSelecionadoVias2Click(Sender: TObject);
    procedure Oramentodevenda1Click(Sender: TObject);
    procedure EmiteNFCe2Click(Sender: TObject);
    procedure Pedidodevenda1Click(Sender: TObject);
    procedure Alteracondiodepagamentodooramento1Click(Sender: TObject);
    procedure Cancelado2Click(Sender: TObject);
    procedure Devolues5Click(Sender: TObject);
    procedure Finalizado1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure QrValAbertoAfterOpen(DataSet: TDataSet);
    procedure Equipamentoprincipal1Click(Sender: TObject);
    procedure Equipamentosecundrio1Click(Sender: TObject);
    procedure Acessrio1Click(Sender: TObject);
    procedure Indefinido1Click(Sender: TObject);
    procedure EmiteNFe1Click(Sender: TObject);
    procedure SbClienteClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure Agora1Click(Sender: TObject);
    procedure Agoramostrandoclculo1Click(Sender: TObject);
    procedure Outradata1Click(Sender: TObject);
    procedure Outradatamostrandoclculo1Click(Sender: TObject);
    procedure Renovar1Click(Sender: TObject);
    procedure Contratofonte71Click(Sender: TObject);
    procedure Contratofonte101Click(Sender: TObject);
    procedure Alteratolerncia1Click(Sender: TObject);
    procedure olerncia1Click(Sender: TObject);
    procedure Fonte101Click(Sender: TObject);
    procedure Fonte71Click(Sender: TObject);
    procedure Fonte102Click(Sender: TObject);
    procedure Fonte72Click(Sender: TObject);
    procedure DBGLocIConItsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PMNFsPopup(Sender: TObject);
    procedure EmiteNFCe1Click(Sender: TObject);
    procedure EmiteNFe2Click(Sender: TObject);
    procedure MMCSnapIn1Click(Sender: TObject);
    procedure MMCSnapIn2Click(Sender: TObject);
    procedure Redefinir1Click(Sender: TObject);
    procedure QrLocCItsVenAfterScroll(DataSet: TDataSet);
    procedure QrLocCItsVenBeforeClose(DataSet: TDataSet);
    procedure QrSmiaVenAfterOpen(DataSet: TDataSet);
  private
    FAtualizando, FApenasLocCConCab, FFechando, FCalculandoManual: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ImprimeDevolucao(Todos: Boolean; Fonte: Integer);
    //
    procedure DefineAvaliacao(Parte: TAvaliacao; Item, Atual: Integer; Reabre:
              Boolean = False);
    procedure MostraFormLocFCab(Encerra: Boolean);
    //
    procedure AtualizaImagens(Codigo, TipImg: Integer; Image: TImage);
    //
    procedure PesquisaPorCliente();
    procedure VisualizaFoto(Codigo, Entidade, Contato: Integer);
    function  VerificaSeTodosOsItensRetornaram((*QueryLoc: TmySQLQuery;*) Codigo,
                Controle: Integer): Boolean;


    // T i s o l i n
    procedure AlteraStatusMovimento(NewStatusMovimento: TStatusMovimento);
    procedure RecalculaDataProgramadaRetorno();
    //procedure InsereLocCConIts(TabLocIts: TTabLocIts);
    //
    procedure DesabilitaBotoesMovimento();
    procedure HabilitaBotaoMovimentoAtual();
    //
    function  DoubleClick_GradeItens(Sender: TDBGrid): Boolean;

    function  ExcluiItemVendaDeMuitos(): Boolean;

    procedure MostraFormLocCConIts(SQLType: TSQLType; IDTab: TTabConLocIts);
    procedure MostraFormLocCItsLca(SQLType: TSQLType);
    procedure MostraFormLocCItsMat(Codigo, CtrID: Integer; Tipo: TGraGXToolRnt;
              GraGruY: Integer);
    procedure MostraFormLocIPatCns();
    procedure MostraFormLocCItsSvc(SQLType: TSQLType; Codigo, CtrID, Item: Integer);
    procedure MostraFormLocCItsVen(SQLType: TSQLType; Codigo, CtrID, Item: Integer);
    procedure MostraFormLocCRedef();

    function  StatusAtendimentoAberto(): Boolean;
    function  StatusAtendimentoCancelado(): Boolean;
    function  StatusAtendimentoLocado(): Boolean;
    function  StatusAtendimentoFinalizado(): Boolean;
    function  TipoDeMovimentoEhLocacao(): Boolean;
    function  TipoDeMovimentoEhServico(): Boolean;
    function  TipoDeMovimentoEhVenda(): Boolean;
    function  StatusMovimentoEhIndefinido(): Boolean;
    function  StatusMovimentoEhOrcamento(): Boolean;
    function  StatusMovimentoEhPedido(): Boolean;
    function  StatusMovimentoEhFaturado(): Boolean;
    function  StatusMovimentoPodeCancelar(): Boolean;
    procedure Info_Fazer(Txt: String);
    procedure ReopenPsqSum_LocCConIts((*const FatID: Integer; *)var Achou: Boolean);
    //procedure ImprimeOrcamentoSelecionado(Vias: Integer);
    //procedure ImprimeOrcamentoTodos(Vias: Integer);
    //procedure ImprimePedidoTodos(Vias: Integer);
    procedure DefineImprOrcaPedi(StatusMovimento: TStatusMovimento);
    procedure ImprimeOrcaPedi(CtrID: Integer; StatusMovimento: TStatusMovimento;
              Vias: Integer; ImpDesco: Boolean = False);
    function  MovimentoPossuiNFAutorizada(AvisaQueTem: Boolean): Boolean;
    function  MovimentoPossuiNFCeAutorizada(AvisaQueTem: Boolean): Boolean;
    function  MovimentoPossuiNFIniciadaENaoExcluida(ide_mod: Integer; AvisaQueTem: Boolean): Boolean;
    //function  MovimentoPossuiNFeAutorizada(AvisaQueTem: Boolean): Boolean;
    procedure ImprimeContrato(Fonte: Integer);

    procedure EmiteNFe();
    procedure EmiteNFCe();
    function  PermiteEmitirNF(): Boolean;
    procedure ReopenSmiaVen();
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    FTipoAluguelAnt: String;
    //

    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenLocFCab(Controle: Integer);
    procedure ReopenLctFatRef(Conta: Integer);
    //
    procedure ReopenECComprou(Controle: Integer);
    procedure ReopenECRetirou(Controle: Integer);
    function  LiberaItensLocacao(QueryLocIPatPri(*, QueryLoc*): TmySQLQuery): Boolean;

    // T i s o l i n
    function  FormaAluguelPelaDataDeRetono(): Integer;
    //procedure QuantidadeDeDiasFormaAluguel();
    procedure QuantidadeDeDiasFormaAluguel(var DataDev, HoraDev: TDateTime);
    procedure DefineDataHoraDevolver();
    function  ConfereDataHoraDevolverComPeriodicidade(): Boolean;
(*
    function  RecalculaItens_Datas(QrItsLca, Codigo, StatusLocacao: Integer; QualID: Variant; GeraLog:
              Boolean): Boolean;
*)
    procedure RecalculaTudo(GeraLog, OutraData: Boolean; Redefine: Boolean = False);
    procedure ReopenEntiStatus(Entidade: Integer);
    procedure ReopenLocCConIts(CtrID: Integer);
    procedure ReopenLocIPatPri(QueryLocIPatPri: TmySQLQuery; Codigo, CtrID, Item: Integer);
    procedure ReopenLocIPatSec(QueryLocIPatSec: TmySQLQuery; Codigo, CtrID, Item: Integer);
    procedure ReopenLocIPatAce(QueryLocIPatAce: TmySQLQuery; Codigo, CtrID, Item: Integer);
    procedure ReopenLocIPatUso(QueryLocIPatUso: TmySQLQuery; Codigo, CtrID, Item: Integer);
    procedure ReopenLocIPatCns(QueryLocIPatCns: TmySQLQuery; Codigo, CtrID, Item: Integer);
    procedure ReopenLocIPatApo(QueryLocIPatApo: TmySQLQuery; Codigo, CtrID, Item: Integer);
    procedure ReopenLocCItsSvc(QueryLocCItsSvc: TmySQLQuery; Codigo, CtrID, Item: Integer);
    procedure ReopenLocCItsVen(QueryLocCItsVen: TmySQLQuery; Codigo, CtrID, Item: Integer);
    //
    procedure AlteraTextoMemoWideStringField(Titulo, Campo: String);
    procedure RecalculaEstoquesPatr(var ItensNegativos: Integer);
    procedure RecalculaEstoquesVend(var ItensNegativos: Integer);
    procedure ReopenEndContratante();
    procedure ImprimeFichaDeTroca(Tool: TGraGXToolRnt);
    function  NotLocado(): Boolean;
    function  ErroStatusNFe(): Boolean;
  end;

var
  FmLocCConCab: TFmLocCConCab;

const
  FFormatFloat = '00000';
  FTabLctA = 'lct0001a';

implementation

uses //Vcl.Styles, Vcl.Themes,
  UnMyObjects, Module, MyDBCheck, DmkDAC_PF, SelCod, ModuleFatura, LocFCab2,
  ModuleGeral, UnBloquetos, ModuleLct2, ModuleFin, ContratApp2, MyListas,
  EntiImagens, Principal, MyGlyfs, UnEntities, LocCItsLca, LocCItsMat, ModProd,
  UnGraL_Jan, UnGrade_Jan, LocIPatCns, LocCItsSvc, LocCItsVen, LocCConIts,
  GetTexto, NFCe_PF, Entidade2, ModuleNFe_0000, UnXXe_PF, UnFinanceiroJan,
  LocCRedef;

{$R *.DFM}

const
  PCMov_PageIndex_Loca = 0;
  PCMov_PageIndex_Serv = 1;
  PCMov_PageIndex_Vend = 2;
  PCMov_PageIndex_Obse = 3;
  PCMov_PageIndex_Fatu = 4;

(*
type
 TWinControlH= class(TWinControl);

constructor TEditStyleHookColor.Create(AControl: TWinControl);
begin
  inherited;
  UpdateColors;
end;

procedure TEditStyleHookColor.UpdateColors;
var
  LStyle: TCustomStyleServices;
begin
 if Control.Enabled then
 begin
  Brush.Color := TWinControlH(Control).Color;
  FontColor   := TWinControlH(Control).Font.Color;
 end
 else
 begin
  LStyle := StyleServices;
  Brush.Color := LStyle.GetStyleColor(scEditDisabled);
  FontColor := LStyle.GetStyleFontColor(sfEditBoxTextDisabled);
 end;
end;

procedure TEditStyleHookColor.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    CN_CTLCOLORMSGBOX..CN_CTLCOLORSTATIC:
      begin
        UpdateColors;
        SetTextColor(Message.WParam, ColorToRGB(FontColor));
        SetBkColor(Message.WParam, ColorToRGB(Brush.Color));
        Message.Result := LRESULT(Brush.Handle);
        Handled := True;
      end;
    CM_ENABLEDCHANGED:
      begin
        UpdateColors;
        Handled := False;
      end
  else
    inherited WndProc(Message);
  end;
end;

{ TMemoStyleHookColor }

constructor TMemoStyleHookColor.Create(AControl: TWinControl);
begin
  inherited;
  UpdateColors;
end;

procedure TMemoStyleHookColor.UpdateColors;
var
  LStyle: TCustomStyleServices;
begin
 if Control.Enabled then
 begin
  Brush.Color := TWinControlH(Control).Color;
  FontColor   := TWinControlH(Control).Font.Color;
 end
 else
 begin
  LStyle := StyleServices;
  Brush.Color := LStyle.GetStyleColor(scEditDisabled);
  FontColor := LStyle.GetStyleFontColor(sfEditBoxTextDisabled);
 end;
end;

procedure TMemoStyleHookColor.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    CN_CTLCOLORMSGBOX..CN_CTLCOLORSTATIC:
      begin
        UpdateColors;
        SetTextColor(Message.WParam, ColorToRGB(FontColor));
        SetBkColor(Message.WParam, ColorToRGB(Brush.Color));
        Message.Result := LRESULT(Brush.Handle);
        Handled := True;
      end;

    CM_COLORCHANGED,
    CM_ENABLEDCHANGED:
      begin
        UpdateColors;
        Handled := False;
      end
  else
    inherited WndProc(Message);
  end;
end;
*)



/////////////////////////////////////////////////////////////////////////////////////
procedure TFmLocCConCab.Libera1Click(Sender: TObject);
{
var
  DataDef, DataSel, DataMin: TDateTime;
  LibDtHr: String;
  CtrID, Item, LibFunci, UserSelect, LibExUsr, i: Integer;
}
begin
{
  //Info_Fazer('1');
  if (QrLocIPatPri.State = dsInactive) and (QrLocIPatPri.RecordCount = 0) then Exit;
  //
  DataDef := Now();
  DataMin := Trunc(QrLocCConCabDtHrEmi.Value);
  DataSel := 0;
  LibExUsr := VAR_USUARIO;
  if not DBCheck.ObtemDataEUser(VAR_USUARIO, UserSelect,
    DataDef, DataSel,dataMin, Time, True) then Exit;
  //
  LibDtHr  := Geral.FDT(DataSel, 109);
  LibFunci := UserSelect;
  //
  CtrID := QrLocCConItsCtrID.Value;
  //
  if DBGLocIPatPri.SelectedRows.Count > 1 then
  begin
    with DBGLocIPatPri.DataSource.DataSet do
    for i:= 0 to DBGLocIPatPri.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGLocIPatPri.SelectedRows.Items[i]));
      GotoBookmark(DBGLocIPatPri.SelectedRows.Items[i]);
      //
      Item  := QrLocIPatPriItem.Value;
      //
      if not VerificaSeTodosOsItensRetornaram((*QrLoc,*) QrLocIPatPriCodigo.Value, CtrID) then
      begin
        Geral.MB_Info('O patrim�nio ' + QrLocIPatPriNO_GGX.Value +
          ' n�o retornou e por isso n�o pode ser liberado!');
        Exit;
      end;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitslca', False, ['LibFunci',
        'LibDtHr', 'LibExUsr'], ['CtrID', 'Item'], [LibFunci, LibDtHr, LibExUsr],
        [CtrID, Item], True)
      then
        Dmod.VerificaSituacaoPatrimonio(QrLocIPatPriGraGruX.Value);
    end;
  end else
  begin
    Item := QrLocIPatPriItem.Value;
    //
    if not VerificaSeTodosOsItensRetornaram((*QrLoc,*) QrLocIPatPriCodigo.Value, CtrID) then
    begin
      Geral.MB_Info('O patrim�nio ' + QrLocIPatPriNO_GGX.Value +
        ' n�o retornou e por isso n�o pode ser liberado!');
      Exit;
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitslca', False, ['LibFunci',
      'LibDtHr', 'LibExUsr'], ['CtrID', 'Item'], [LibFunci, LibDtHr, LibExUsr],
      [CtrID, Item], True)
    then
      Dmod.VerificaSituacaoPatrimonio(QrLocIPatPriGraGruX.Value);
  end;
  ReopenLocIPatPri(QrLocIPatPri, QrLocCConCabCodigo.Value, CtrID, Item);
}
end;

function TFmLocCConCab.LiberaItensLocacao(QueryLocIPatPri(*, QueryLoc*): TmySQLQuery): Boolean;
var
  LibDtHr: String;
  CtrID, Item, LibFunci, LibExUsr: Integer;
  DataMi: TDateTime;  
begin
   Info_Fazer('2');
  Result := False;

  CtrID := QueryLocIPatPri.FieldByName('CtrID').AsInteger;
  DataMi := EncodeDate(1900, 1, 1);
  //
  if (QueryLocIPatPri.State <> dsInactive) and (QueryLocIPatPri.RecordCount > 0) then
  begin
    QueryLocIPatPri.First;
    while not QueryLocIPatPri.Eof do
    begin
      if not VerificaSeTodosOsItensRetornaram((*QueryLoc,*) QueryLocIPatPri.FieldByName('Codigo').AsInteger, CtrID) then
      begin
        Geral.MB_Info('Encerramento cancelado!' + sLineBreak +
          'Motivo: O patrim�nio ' + QueryLocIPatPri.FieldByName('NO_GGX').AsString +
          ' n�o retornou e por isso n�o pode ser liberado!');
        Exit;
      end;
      Item := QueryLocIPatPri.FieldByName('Item').AsInteger;
      //
      if QueryLocIPatPri.FieldByName('LibDtHr').AsDateTime < DataMi then
      begin
        LibFunci := VAR_USUARIO;
        LibDtHr  := Geral.FDT(Now(), 109);
        LibExUsr := VAR_USUARIO;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitslca', False, [
          'LibFunci', 'LibDtHr', 'LibExUsr'], ['Item'], [LibFunci, LibDtHr,
          LibExUsr], [Item], True)
        then
          Dmod.VerificaSituacaoPatrimonio(QueryLocIPatPri.FieldByName('GraGruX').AsInteger);
      end;
      QueryLocIPatPri.Next;
    end;
    ReopenLocIPatPri(QueryLocIPatPri, QueryLocIPatPri.FieldByName('Codigo').AsInteger, CtrID, Item);
    //
    Result := True;
  end;
end;

procedure TFmLocCConCab.VisualizaFoto(Codigo, Entidade, Contato: Integer);
begin
  LocCod(Codigo, Codigo);
  //
  if Contato = 0  then
    Entities.MostraEntiImagens(Entidade, 0, 0, True)
  else
    Entities.MostraEntiImagens(Entidade, Contato, 1, True);
end;

procedure TFmLocCConCab.Loca1Click(Sender: TObject);
begin
   Info_Fazer('26');
end;

procedure TFmLocCConCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  if not FFechando then
    GOTOy.LC(Atual, Codigo);
end;

procedure TFmLocCConCab.MostraFormLocIPatCns();
var
  SQLType: TSQLType;
  Codigo: Integer;
begin
  SQLType := stUpd;
  Codigo  := QrLocCConCabCodigo.Value;
  //
  if DBCheck.CriaFm(TFmLocIPatCns, FmLocIPatCns, afmoNegarComAviso) then
  begin
    FmLocIPatCns.ImgTipo.SQLType := SQLType;
    FmLocIPatCns.FQrCab  := QrLocIPatCns;
    FmLocIPatCns.FDsCab  := DsLocIPatCns;
    FmLocIPatCns.FQrIts  := QrLocIPatCns;
    FmLocIPatCns.FCodigo := Codigo;
    if SQLType = stIns then
      //?
    else
    begin
      //FmLocIPatCns.EdItem.ValueVariant := QrLocIPatCnsItem.Value;
      FmLocIPatCns.EdQtdIni.ValueVariant := QrLocIPatCnsQtdIni.Value;
      FmLocIPatCns.EdQtdFim.ValueVariant := QrLocIPatCnsQtdFim.Value;
      FmLocIPatCns.EdPrcUni.ValueVariant := QrLocIPatCnsPrcUni.Value;
      FmLocIPatCns.EdValorRucAAdiantar.ValueVariant := QrLocIPatCnsValorRucAAdiantar.Value;
    end;
    FmLocIPatCns.ShowModal;
    FmLocIPatCns.Destroy;
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmLocCConCab.MeHistoricoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    if ssCtrl in shift then
      //
    else
    begin
      key := 0;
      TPDtHrSai.SetFocus;
    end;
  end;
end;

procedure TFmLocCConCab.MMCSnapIn1Click(Sender: TObject);
begin
  UnWin.MostraMMC_SnapIn();
end;

procedure TFmLocCConCab.MMCSnapIn2Click(Sender: TObject);
begin
  UnWin.MostraMMC_SnapIn();
end;

function TFmLocCConCab.MovimentoPossuiNFAutorizada(
  AvisaQueTem: Boolean): Boolean;
begin
  Result :=
    (QrLocCConItsNF_Stat.Value = CO_100_NFeAutorizada)
    (*
    or
    (QrLocCConItsNFeNF_Stat.Value = CO_100_NFeAutorizada)*);
  if Result and AvisaQueTem then
    Geral.MB_Aviso('A��o cancelada! Movimento atual possui emiss�o de NFC-e (ou NF-e) autorizada!')
end;

function TFmLocCConCab.MovimentoPossuiNFCeAutorizada(AvisaQueTem: Boolean): Boolean;
begin
  Result := QrLocCConItsNF_Stat.Value = CO_100_NFeAutorizada;
  if AvisaQueTem then
    if Result then
      Geral.MB_Aviso('A��o cancelada! Movimento atual possui emiss�o de NFC-e autorizada!')
  else
  begin
{
    if QrLocCConItsNFeNF_Stat.Value = CO_100_NFeAutorizada then
      Result := Geral.MB_Aviso(
      'O movimento atual j� possui emiss�o de NF-e autorizada!' +
      sLineBreak + 'Deseja mesmo assim emitir uma NFC-e?') <> ID_YES;
}
  end;
end;

function TFmLocCConCab.MovimentoPossuiNFIniciadaENaoExcluida(ide_mod: Integer;
  AvisaQueTem: Boolean): Boolean;
const
  InvalidaDI = False;
  InvalisaGA = False;
var
  Status, FatID, FatNum, Empresa: Integer;
begin
  if QrLocCConItside_mod.Value = ide_mod then
  begin
    Result  := True;
    Status  := QrLocCConItsNF_Stat.Value;
    FatID   := QrLocCConItsFatID.Value;
    FatNum  := QrLocCConItsFatNum.Value;
    Empresa := QrLocCConCabEmpresa.Value;
    if Status in ([
      //CO_005_StepNFePedido
      //CO_010_StepNFeDados
      //CO_020_StepNFeGerada
      //CO_030_StepNFeAssinada
      CO_035_StepNFCeOffLine      ,
      //CO_040_StepLoteRejeitado
      //CO_050_StepNFeAdedLote
      CO_060_StepLoteEnvEnviado   ,
      CO_070_StepLoteEnvConsulta  ,
      CO_090_StepNFeCabecalhoDwnl ,
      CO_100_NFeAutorizada        ,
      CO_101_NFeCancelada
    ]) then
    begin
      Geral.MB_Aviso(
      'Movimento atual possui NFC-e em andamento com Status que impede o seu desfazimento!');
      Exit;
    end else
    begin
      if Geral.MB_Pergunta(
      'Movimento atual possui NFC-e em andamento. Deseja desfaz�-lo?') = ID_YES then
      begin
        if DBCheck.LiberaPelaSenhaBoss() then
        begin
          if DmNFe_0000.InvalidaNfe(Status, FatID, FatNum, Empresa, InvalidaDI,
          InvalisaGA) then
          begin
            Result := False;
          end;
        end;
      end;
    end;
  end else Result := False;
end;

{
function TFmLocCConCab.MovimentoPossuiNFeAutorizada(
  AvisaQueTem: Boolean): Boolean;
begin
  Result := QrLocCConItsNFeNF_Stat.Value = CO_100_NFeAutorizada;
  if AvisaQueTem then
    if Result then
      Geral.MB_Aviso('A��o cancelada! Movimento atual possui emiss�o de NF-e autorizada!')
  else
  begin
    if QrLocCConItsNF_Stat.Value = CO_100_NFeAutorizada then
      Result := Geral.MB_Aviso(
      'O movimento atual j� possui emiss�o de NFC-e autorizada!' +
      sLineBreak + 'Deseja mesmo assim emitir uma NF-e?') <> ID_YES;
  end;
end;
}

procedure TFmLocCConCab.MostraFormLocCConIts(SQLType: TSQLType; IDTab: TTabConLocIts);
const
  sProcName = 'TFmLocCConCab.MostraFormLocCConIts()';
  GeraLog = False;
var
  Inseriu: Boolean;
  CtrID, Codigo, Status: Integer;
begin
  Codigo := QrLocCConCabCodigo.Value;
  if DBCheck.CriaFm(TFmLocCConIts, FmLocCConIts, afmoNegarComAviso) then
  begin
    FmLocCConIts.ImgTipo.SQLType := SQLType;
    //FmLocCConIts.FQrIts := QrLocCConIts;
    //FmLocCConIts.FDsCab := DsLocCConCab;
    FmLocCConIts.FCodigo  := QrLocCConCabCodigo.Value;
    FmLocCConIts.FCtrID   := QrLocCConItsCtrID.Value;
    FmLocCConIts.FEmpresa := QrLocCConCabEmpresa.Value;
    FmLocCConIts.FCliente := QrLocCConCabCliente.Value;
    FmLocCConIts.FDtHrEmi := QrLocCConCabDtHrEmi.Value;
    //
    if SQLType = stIns then
    begin
      FmLocCConIts.RGIDTab. ItemIndex := Integer(IDTab);
      FmLocCConIts.EdAtndCanal.ValueVariant := QrLocCConCabAtndCanal.Value;
      FmLocCConIts.CBAtndCanal.KeyValue     := QrLocCConCabAtndCanal.Value;
    end else
    begin
      FmLocCConIts.RGIDTab.Enabled          := False;
      FmLocCConIts.RGStatus.Enabled         := False;
      FmLocCConIts.RGIDTab.ItemIndex        := QrLocCConItsIDTab.Value;
      FmLocCConIts.RGStatus.ItemIndex       := QrLocCConItsStatus.Value;
      FmLocCConIts.EdCondiOrca.ValueVariant := QrLocCConItsCondiOrca.Value;
      FmLocCConIts.CBCondiOrca.KeyValue     := QrLocCConItsCondiOrca.Value;
      FmLocCConIts.EdAtndCanal.ValueVariant := QrLocCConItsAtndCanal.Value;
      FmLocCConIts.CBAtndCanal.KeyValue     := QrLocCConItsAtndCanal.Value;
    end;
    FmLocCConIts.ShowModal;
    Inseriu := FmLocCConIts.FInseriu;
    CtrID := FmLocCConIts.FCtrID;
    FmLocCConIts.Destroy;
    ReopenLocCConIts(CtrID);
    //
    if Inseriu then
    begin
      if QrLocCConItsCtrID.Value = CtrID then
      begin
        Status := QrLocCConCabStatus.Value;
        case TTabConLocIts(QrLocCConItsIDTab.Value) of
          TTabConLocIts.tcliIndefinido: ;
          TTabConLocIts.tcliLocacao:
          begin
            MostraFormLocCItsLca(stIns);
(*
            if AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, CtrID,
            QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
            QrLocCConCabDtHrDevolver.Value, GeraLog) then
              AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
*)
          end;
          TTabConLocIts.tcliOutroServico:
          begin
            MostraFormLocCItsSvc(stIns, Codigo, CtrID, (*Item*)0);
          end;
          TTabConLocIts.tcliVenda:
          begin
            MostraFormLocCItsVen(stIns, Codigo, CTrID, (*Item*)0);
          end;
          else Geral.MB_Erro('Movimento n�o implementado em ' + sProcName);
        end;
      end else
        Geral.MB_Erro('ERRO ao localizar movimento criado!');
      //
    end;
  end;
  //LocCod(Codigo, Codigo);
end;

procedure TFmLocCConCab.MostraFormLocCItsLca(SQLType: TSQLType);
const
  AvisaEstqNeg = True;
  GeraLog = False;
var
  Codigo, Status, CtrID, Item(*, Status, GraGruX*): Integer;
  //SQLType: TSQLType;
  DtFimCobra: TDateTime;
begin
  //Info_Fazer('Fazer 3');
  Codigo := QrLocCConCabCodigo.Value;
  Status := QrLocCConCabStatus.Value;
  if DBCheck.CriaFm(TFmLocCItsLca, FmLocCItsLca, afmoNegarComAviso) then
  begin
    FmLocCItsLca.ImgTipo.SQLType := SQLType;
    FmLocCItsLca.FQrCab := QrLocCConCab;
    FmLocCItsLca.FDsCab := DsLocCConCab;
    //FmLocCItsLca.FQrIts := QrLocCConIts;
    FmLocCItsLca.FCtrID := QrLocCConItsCtrID.Value;
    if SQLType = stIns then
      FmLocCItsLca.FItem := 0
    else
      FmLocCItsLca.FItem := QrLocIPatPriItem.Value;
    //FmLocCItsLca.FCliente := QrLocCConCabCliente.Value;
    FmLocCItsLca.ReopenTabelas();
    if SQLType = stIns then
    begin

(*
      FmLocCItsLca.TPSaiDtHr.Date               := QrLocCConCabDtHrSai.Value;
      FmLocCItsLca.EdSaiDtHr.ValueVariant       := QrLocCConCabDtHrSai.Value;
      FmLocCItsLca.TPCobranIniDtHr.Date         := QrLocCConCabDtHrSai.Value;
      FmLocCItsLca.EdCobranIniDtHr.ValueVariant := QrLocCConCabDtHrSai.Value;
*)
      FmLocCItsLca.FQtdeLocacaoAntes            := 0;
      FmLocCItsLca.FTipoAluguel                 := QrLocCConCabTipoAluguel.Value;

      if TStatusMovimento(QrLocCConItsStatus.Value) = TStatusMovimento.statmovPedido then
      begin
        FmLocCItsLca.RGQtdeLocacao.ItemIndex := 1;
        FmLocCItsLca.RGQtdeLocacao.Enabled := False;
      end else
      begin
        FmLocCItsLca.RGQtdeLocacao.ItemIndex := 2;
        FmLocCItsLca.RGQtdeLocacao.Enabled := True;
      end;
    end else
    begin
      FmLocCItsLca.FOldGGX                      := QrLocIPatPriGraGruX.Value;
      //
      FmLocCItsLca.EdCtrID.ValueVariant         := QrLocCConItsCtrID.Value;
      FmLocCItsLca.EdGraGruX.ValueVariant       := QrLocIPatPriGraGruX.Value;
      FmLocCItsLca.CBGraGruX.KeyValue           := QrLocIPatPriGraGruX.Value;
      FmLocCItsLca.TPDataLoc.Date               := Trunc(QrLocIPatPriDtHrLocado.Value);
      FmLocCItsLca.EdHoraLoc.Text               := FormatDateTime('hh:nn:ss', QrLocIPatPriDtHrLocado.Value);
      //
      //FmLocCItsLca.CkQtdeLocacao.Checked        := QrLocIPatPriQtdeLocacao.Value > 0;
      if TStatusMovimento(QrLocCConItsStatus.Value) = TStatusMovimento.statmovPedido then
      begin
        FmLocCItsLca.RGQtdeLocacao.ItemIndex := 1;
        FmLocCItsLca.RGQtdeLocacao.Enabled := False;
      end else
      begin
        FmLocCItsLca.RGQtdeLocacao.ItemIndex := QrLocIPatPriQtdeLocacao.Value;
        FmLocCItsLca.RGQtdeLocacao.Enabled := True;
      end;
      //
      FmLocCItsLca.EdQtdeProduto.ValueVariant   := QrLocIPatPriQtdeProduto.Value;
      FmLocCItsLca.EdValorLocacao.ValueVariant  := QrLocIPatPriValorLocacao.Value;
      //FmLocCItsLca.FDtHrSai                     := QrLocCConCabDtHrSai.Value;
      //
(*
      FmLocCItsLca.TPSaiDtHr.Date               := QrLocIPatPriSaiDtHr.Value;
      FmLocCItsLca.EdSaiDtHr.ValueVariant       := QrLocIPatPriSaiDtHr.Value;
      FmLocCItsLca.TPCobranIniDtHr.Date         := QrLocIPatPriCobranIniDtHr.Value;
      FmLocCItsLca.EdCobranIniDtHr.ValueVariant := QrLocIPatPriCobranIniDtHr.Value;
*)
      //
      FmLocCItsLca.FQtdeLocacaoAntes            := QrLocIPatPriQtdeLocacao.Value;
    end;
    FmLocCItsLca.ShowModal;
    if FmLocCItsLca.FAlterou then
    begin
      CtrID := FmLocCItsLca.FCtrID;
      Item  := FmLocCItsLca.FItem;
      //
      DtFimCobra := DMOdG.ObtemAgora();
      if AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, CtrID,
      QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
      QrLocCConCabDtHrDevolver.Value, DtFimCobra, GeraLog) then
        AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);

    end;
    FmLocCItsLca.Destroy;
  end;
  LocCod(Codigo, Codigo);
  if QrLocCConIts.State <> dsInactive then
    QrLocCConIts.Locate('CtrID', CtrID, []);
  if QrLocIPatPri.State <> dsInactive then
    QrLocIPatPri.Locate('Item', Item, []);
end;

procedure TFmLocCConCab.MostraFormLocFCab(Encerra: Boolean);
const
  SQLType = stIns;
var
  Codigo, Controle, ItensOrca: Integer;
begin
   //Info_Fazer('36');
  Codigo := QrLocCConCabCodigo.Value;
  ///////////////////////////////////
  ///  A V I S O S
  if QrLocCConCabValorPndt.Value <= 0.00 then
  begin
    Geral.MB_Info('N�o h� valor pendente para faturamento!');
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT COUNT(Status) ITENS  ',
  'FROM loccconits ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND Status=' + Geral.FF0(Integer(TStatusMovimento.statmovOrcamento)),
  '']);
  ItensOrca := Dmod.QrAux.FieldByName('ITENS').AsInteger;
  if ItensOrca > 0 then
  begin
    Geral.MB_Info('Existem ' + IntToStr(ItensOrca) +
      ' movimentos de or�amento n�o transformados em pedido!');
  end;
  ///  F I M   A V I S O S
  ///////////////////////////////////
  //

  if DBCheck.CriaFm(TFmLocFCab2, FmLocFCab2, afmoNegarComAviso) then
  begin
    FmLocFCab2.ImgTipo.SQLType        := SQLType;
    FmLocFCab2.DBEdCodigo.DataSource  := DsLocCConCab;
    FmLocFCab2.DBEdCliente.DataSource := DsLocCConCab;
    FmLocFCab2.DBEdNome.DataSource    := DsLocCConCab;
    FmLocFCab2.FQrCab                 := QrLocCConCab;
    FmLocFCab2.FDsCab                 := DsLocCConCab;
    FmLocFCab2.FQrIts                 := QrLocFCab;
    FmLocFCab2.FQrLocCPatPri          := QrLocIPatPri;
    //FmLocFCab2.FQrLoc                := QrLocCConCabEmpresa.Value;
    FmLocFCab2.FEmpresa               := QrLocCConCabEMpresa.Value;
    FmLocFCab2.FFilial                := QrLocCConCabFilial.Value;
    FmLocFCab2.FCliente               := QrLocCConCabCliente.Value;
    FmLocFCab2.FDtHrEmi               := QrLocCConCabDtHrEmi.Value;
    FmLocFCab2.FEncerra               := Encerra;
    FmLocFCab2.FFmLocCConCab          := Self;
    FmLocFCab2.FTabLcta               := FTabLcta;

    //
    if SQLType = stIns then
    begin
      FmLocFCab2.TPDataFat.Date := Trunc(Date);
      FmLocFCab2.EdHoraFat.Text := FormatDateTime('hh:nn:ss', Now());
    end else
    begin
      FmLocFCab2.EdControle.ValueVariant   := QrLocFCabControle.Value;
      FmLocFCab2.EdCondicaoPG.ValueVariant := QrLocFCabCondicaoPG.Value;
      FmLocFCab2.CBCondicaoPG.KeyValue     := QrLocFCabCondicaoPG.Value;
      FmLocFCab2.EdCartEmis.ValueVariant   := QrLocFCabCartEmis.Value;
      FmLocFCab2.CBCartEmis.KeyValue       := QrLocFCabCartEmis.Value;
      FmLocFCab2.EdValLocad.ValueVariant   := QrLocFCabValLocad.Value;
      FmLocFCab2.EdValConsu.ValueVariant   := QrLocFCabValConsu.Value;
      FmLocFCab2.EdValUsado.ValueVariant   := QrLocFCabValUsado.Value;
      FmLocFCab2.EdValVenda.ValueVariant   := QrLocFCabValVenda.Value;
      FmLocFCab2.EdValVeMaq.ValueVariant   := QrLocFCabValVeMaq.Value;
      FmLocFCab2.EdValServi.ValueVariant   := QrLocFCabValServi.Value;
      FmLocFCab2.EdValDesco.ValueVariant   := QrLocFCabValDesco.Value;
      FmLocFCab2.EdValTotal.ValueVariant   := QrLocFCabValTotal.Value;
      FmLocFCab2.EdSerNF.ValueVariant      := QrLocFCabSerNF.Value;
      FmLocFCab2.EdNumNF.ValueVariant      := QrLocFCabNumNF.Value;
      FmLocFCab2.TPDataFat.Date            := Trunc(QrLocFCabDtHrFat.Value);
      FmLocFCab2.EdHoraFat.Text            := FormatDateTime('hh:nn:ss', QrLocFCabDtHrFat.Value);
      FmLocFCab2.CGCodHist.Value           := QrLocFCabCodHist.Value;
    end;
    FmLocFCab2.ShowModal;
    Controle := FmLocFCab2.FControle;
    FmLocFCab2.Destroy;
    //
    LocCod(Codigo, Codigo);
    QrLocFCab.Locate('Controle', Controle, []);
    PCMovimento.ActivePageIndex := PCMov_PageIndex_Fatu;
  end;
end;

procedure TFmLocCConCab.MostraFormLocCItsMat(Codigo, CtrID: Integer; Tipo:
  TGraGXToolRnt; GraGruY: Integer);
var
  Habilita: Boolean;
begin
  //Info_Fazer(' 4');
  if Tipo = TGraGXToolRnt.ggxoIndef then Exit;
  //
  if DBCheck.CriaFm(TFmLocCItsMat, FmLocCItsMat, afmoNegarComAviso) then
  begin
    FmLocCItsMat.ImgTipo.SQLType := stIns;
    FmLocCItsMat.FTipo           := Tipo;
    case Tipo of
      TGraGXToolRnt.ggxo1gAcessorio:  FmLocCItsMat.FCaption := 'Acess�rio';
      TGraGXToolRnt.ggxo1gUso:        FmLocCItsMat.FCaption := 'Material de Uso';
      TGraGXToolRnt.ggxo1gConsumo:    FmLocCItsMat.FCaption := 'Material de Consumo';
      TGraGXToolRnt.ggxo2gSecundario: FmLocCItsMat.FCaption := 'Patrim�nio Secund�rio';
      TGraGXToolRnt.ggxo2gAcessorio:  FmLocCItsMat.FCaption := 'Acess�rio';
      TGraGXToolRnt.ggxo2gApoio:      FmLocCItsMat.FCaption := 'Material de Apoio';
      TGraGXToolRnt.ggxo2gUso:        FmLocCItsMat.FCaption := 'Material de Uso';
      TGraGXToolRnt.ggxo2gConsumo:    FmLocCItsMat.FCaption := 'Material de Consumo';
      else FmLocCItsMat.FCaption := '?????????????????????';
    end;
    case Tipo of
      TGraGXToolRnt.ggxo1gAcessorio:  FmLocCItsMat.CBGraGruX.LocF7TableName := ' ??? ';
      TGraGXToolRnt.ggxo1gUso:        FmLocCItsMat.CBGraGruX.LocF7TableName := ' ??? ';
      TGraGXToolRnt.ggxo1gConsumo:    FmLocCItsMat.CBGraGruX.LocF7TableName := ' ??? ';
      TGraGXToolRnt.ggxo2gSecundario: FmLocCItsMat.CBGraGruX.LocF7TableName := 'gragxpatr';
      TGraGXToolRnt.ggxo2gAcessorio:  FmLocCItsMat.CBGraGruX.LocF7TableName := 'gragxpatr';
      TGraGXToolRnt.ggxo2gApoio:      FmLocCItsMat.CBGraGruX.LocF7TableName := 'gragxoutr';
      TGraGXToolRnt.ggxo2gUso:        FmLocCItsMat.CBGraGruX.LocF7TableName := 'gragxoutr';
      TGraGXToolRnt.ggxo2gConsumo:    FmLocCItsMat.CBGraGruX.LocF7TableName := 'gragxoutr';
      else FmLocCItsMat.CBGraGruX.LocF7TableName := ' ????? ';
    end;
    case Tipo of
      TGraGXToolRnt.ggxo1gAcessorio:  FmLocCItsMat.CBGraGruX.LocF7NameFldName := ' ??? ';
      TGraGXToolRnt.ggxo1gUso:        FmLocCItsMat.CBGraGruX.LocF7NameFldName := ' ??? ';
      TGraGXToolRnt.ggxo1gConsumo:    FmLocCItsMat.CBGraGruX.LocF7NameFldName := ' ??? ';
      TGraGXToolRnt.ggxo2gSecundario: FmLocCItsMat.CBGraGruX.LocF7NameFldName := 'GraGruX';
      TGraGXToolRnt.ggxo2gAcessorio:  FmLocCItsMat.CBGraGruX.LocF7NameFldName := 'GraGruX';
      TGraGXToolRnt.ggxo2gApoio:      FmLocCItsMat.CBGraGruX.LocF7NameFldName := 'GraGruX';
      TGraGXToolRnt.ggxo2gUso:        FmLocCItsMat.CBGraGruX.LocF7NameFldName := 'GraGruX';
      TGraGXToolRnt.ggxo2gConsumo:    FmLocCItsMat.CBGraGruX.LocF7NameFldName := 'GraGruX';
      else FmLocCItsMat.CBGraGruX.LocF7NameFldName := ' ????? ';
    end;
    //
    FmLocCItsMat.CBGraGruX.LocF7SQLText.Text :=  'AND its.Aplicacao=' + Geral.FF0(AppPF.ObtemTipoDeTGraGXOutr(Tipo));
    //
    FmLocCItsMat.FTipoAluguel  := QrLocCConCabTipoAluguel.Value;
    FmLocCItsMat.FEmpresa      := QrLocCConCabEmpresa.Value;
    FmLocCItsMat.FGraGruY      := GraGruY;
    FmLocCItsMat.FCodigo       := Codigo;
    FmLocCItsMat.FCtrID        := CtrID;
    FmLocCItsMat.FQrLocIPatSec := QrLocIPatSec;
    FmLocCItsMat.FQrLocIPatApo := QrLocIPatApo;
    FmLocCItsMat.FQrLocIPatCns := QrLocIPatCns;
    FmLocCItsMat.FQrLocIPatUso := QrLocIPatUso;
    FmLocCItsMat.FQrLocIPatAce := QrLocIPatAce;
    //
    Habilita := (GraGruY = CO_GraGruY_2048_GXPatSec) or (GraGruY = CO_GraGruY_3072_GXPatAce);
{
    FmLocCItsMat.LaQtdeLocacao.Enabled := Habilita;
    FmLocCItsMat.EdQtdeLocacao.Enabled := Habilita;
}
    FmLocCItsMat.LaQtdeProduto.Enabled := Habilita;
    FmLocCItsMat.EdQtdeProduto.Enabled := Habilita;
    //
    FmLocCItsMat.ShowModal;
    FmLocCItsMat.Destroy;
  end;
end;

procedure TFmLocCConCab.MostraFormLocCItsSvc(SQLType: TSQLType; Codigo, CtrID, Item: Integer);
var
  NewCtrID, NewItem: Integer;
begin
  //Info_Fazer(' 33');
  if DBCheck.CriaFm(TFmLocCItsSvc, FmLocCItsSvc, afmoNegarComAviso) then
  begin
    FmLocCItsSvc.ImgTipo.SQLType := SQLType;
    (*
    FmLocCItsSvc.FTipo         := Tipo;
    FmLocCItsSvc.FGraGruY      := GraGruY;
    *)
    FmLocCItsSvc.FCodigo       := Codigo;
    FmLocCItsSvc.FCtrID        := CtrID;
    FmLocCItsSvc.FItem         := QrLocCItsSvcItem.Value;
    FmLocCItsSvc.FQrLocCItsSvc := QrLocCItsSvc;
    //
    FmLocCItsSvc.FNewCtrID     := CtrID;
    FmLocCItsSvc.FNewItem      := QrLocCItsSvcItem.Value;
    //
    FmLocCItsSvc.ShowModal;

    if FmLocCItsSvc.FAlterou then
    begin
      NewCtrID := FmLocCItsSvc.FNewCtrID;
      NewItem  := FmLocCItsSvc.FNewItem;
      //
      AppPF.AtualizaTotaisLocCConCab_Servicos(Codigo);
      //
      LocCod(Codigo, Codigo);
      QrLocCConIts.Locate('CtrID', NewCtrID, []);
      QrLocCItsSvc.Locate('Item', NewItem, []);
    end;
    FmLocCItsSvc.Destroy;
  end;
end;

procedure TFmLocCConCab.MostraFormLocCItsVen(SQLType: TSQLType; Codigo, CtrID,
  Item: Integer);
var
  NewCtrID, NewItem, RegrFiscal: Integer;
begin
  //Info_Fazer('Fazer 35');
  //
  RegrFiscal := Dmod.QrOpcoesTRenLocRegrFisNFCe.Value;
  if MyObjects.FIC(RegrFiscal = 0, nil,
  'Regra fiscal n�o definida nas op��es espec�ficas!') then
    Exit;
  //
  if DBCheck.CriaFm(TFmLocCItsVen, FmLocCItsVen, afmoNegarComAviso) then
  begin
    FmLocCItsVen.ImgTipo.SQLType := SQLType;
    (*
    FmLocCItsVen.FTipo            := Tipo;
    FmLocCItsVen.FGraGruY         := GraGruY;
    *)
    FmLocCItsVen.FCodigo          := Codigo;
    FmLocCItsVen.FCtrID           := CtrID;
    FmLocCItsVen.FItem            := QrLocCItsVenItem.Value;
    FmLocCItsVen.FQrLocCItsVen    := QrLocCItsVen;
    //
    FmLocCItsVen.FNewCtrID        := CtrID;
    FmLocCItsVen.FNewItem         := Item;
    //
    FmLocCItsVen.FFatID           := VAR_FATID_0002;
    FmLocCItsVen.FEmpresa         := QrLocCConCabEmpresa.Value;
    FmLocCItsVen.FNomeEmp         := QrLocCConCabNO_EMP.Value;
    FmLocCItsVen.FRegrFiscal      := Dmod.QrOpcoesTRenLocRegrFisNFCe.Value;
    FmLocCItsVen.FNOMEFISREGCAD   := Dmod.QrOpcoesTRenNO_FisRegCad.Value;
    FmLocCItsVen.FFatPedCab       := QrLocCConItsFatPedCab.Value;
    FmLocCItsVen.FFatPedVolCnta   := QrLocCConItsFatPedVolCnta.Value;
    FmLocCItsVen.FEhPedido        := StatusMovimentoEhPedido();
    FmLocCItsVen.FStatusLocacao   := TStatusLocacao(QrLocCConCabStatus.Value);
    FmLocCItsVen.FStatusMovimento := TStatusMovimento(QrLocCConItsStatus.Value);
    //
    FmLocCItsVen.ReopenEstqCenCad();
   //
    FmLocCItsVen.ShowModal;
    //
    if FmLocCItsVen.FAlterou then
    begin
      NewCtrID := FmLocCItsVen.FNewCtrID;
      NewItem  := FmLocCItsVen.FNewItem;
      //
      //AppPF.AtualizaTotaisLocCConCab_Vendas(Codigo);
      LocCod(Codigo, Codigo);
      QrLocCConIts.Locate('CtrID', NewCtrID, []);
      QrLocCItsVen.Locate('Item', NewItem, []);
    end;
    FmLocCItsVen.Destroy;
  end;
end;

procedure TFmLocCConCab.MostraFormLocCRedef();
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if DBCheck.CriaFm(TFmLocCRedef, FmLocCRedef, afmoNegarComAviso) then
  begin
    FmLocCRedef.ImgTipo.SQLType := TSQLType.stUpd;
    FmLocCRedef.FGtrTab := TGraToolRent.gbsLocar;
    FmLocCRedef.FCodigo := QrLocCConCabCodigo.Value;
    FmLocCRedef.FTipoAluguel := QrLocCConCabTipoAluguel.Value;
    FmLocCRedef.ReopenLocCMovAll();

    //FmLocCRedef.FManejoLca :=  Todos!;
    //
    FmLocCRedef.ShowModal;

    FmLocCRedef.Destroy;
  end;
end;

procedure TFmLocCConCab.MudastatusparaAberto1Click(Sender: TObject);
var
  Codigo, Status, ItensNegativosPatr, ItensNegativosVend: Integer;
  SQLType: TSQLType;
  SQL_CtrID: String;
begin
  if TStatusLocacao(QrLocCConCabStatus.Value) = TStatusLocacao.statlocLocado then
    if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
      Exit;
  SQLType           := stUpd;
  Codigo            := QrLocCConCabCodigo.Value;
  Status            := Integer(TStatusLocacao.statlocAberto);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconcab', False, [
  'Status'], ['Codigo'], [
  Status], [Codigo], True) then
  begin
    // N�o desfazer estoque para n�o liberar para outros
    // ou para a pr�pria loca��o!
    // ... 2020-11-14
    // Karoline da T i s o l i n  quer que volte para o estoque!
    SQL_CtrID := '';
    AppPF.ReopenItsLca(QrItsLca, Codigo, SQL_CtrID);
    RecalculaEstoquesPatr(ItensNegativosPatr);
    RecalculaEstoquesVend(ItensNegativosVend);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmLocCConCab.MudastatusparaLocado1Click(Sender: TObject);
const
  AvisaEstqNeg = True;
  GeraLog = False;
  JaLocou = 1;
var
  Codigo, Status, StaAtu, GraGruX, ItensNegativosPatr, ItensNegativosVend: Integer;
  SQLType: TSQLType;
  DtFimCobra: TDateTime;
begin
  if StatusAtendimentoFinalizado() then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
  end;
  //
  SQLType           := stUpd;
  Codigo            := QrLocCConCabCodigo.Value;
  Status            := Integer(TStatusLocacao.statlocLocado);
  StaAtu            := QrLocCConCabStatus.Value;
  DtFimCobra        := DModG.ObtemAgora();
  //
  if AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, Null,
  QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
  QrLocCConCabDtHrDevolver.Value, DtFimCobra, GeraLog) then
  begin
    if QrItsLca.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT * FROM loccconits',
      'WHERE Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
      'AND IDTab=' + Geral.FF0(Integer(TTabConLocIts.tcliLocacao)),
      'AND Status>' + Geral.FF0(Integer(TStatusMovimento.statmovOrcamento)),
      '']);
      if Dmod.QrAux.RecordCount > 0 then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconcab', False, [
        'Status'], ['Codigo'], [
        Status], [Codigo], True) then
        begin
          // Recalcula estoques de Patrim�nios!
          RecalculaEstoquesPatr(ItensNegativosPatr);
          // Recalcula estoques de mercadorias
          RecalculaEstoquesVend(ItensNegativosVend);
          //
          if (ItensNegativosPatr > 0) or (ItensNegativosVend > 0) then
          begin
            if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconcab', False, [
            'Status'], ['Codigo'], [
            StaAtu], [Codigo], True) then
            begin
              //LocCod(Codigo, Codigo);
            end;
          end else
          begin
            if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconcab', False, [
            'JaLocou'], ['Codigo'], [
            JaLocou], [Codigo], True) then
            begin
              //LocCod(Codigo, Codigo);
            end;
          end;
          //
          LocCod(Codigo, Codigo);
        end;
      end else
      Geral.MB_Aviso('Status N�O alterado!' + sLineBreak +
      'Este atendimento n�o possui nenhuma loca��o v�lida (B)!');
    end else
    begin
      LocCod(Codigo, Codigo);
      Geral.MB_Aviso('Status N�O alterado!' + sLineBreak +
      'Este atendimento n�o possui nenhuma loca��o (A)!');
    end;
  end else
    LocCod(Codigo, Codigo);
end;

function TFmLocCConCab.NotLocado(): Boolean;
begin
  Result := not StatusAtendimentoLocado();
  if Result then
    Geral.MB_Aviso('O status deve estar como locado para devolu��o!');
end;

procedure TFmLocCConCab.olerncia1Click(Sender: TObject);
var
  Hora: TTime;
  HrTolerancia: String;
  Item, Codigo: Integer;
begin
  Hora := 0;
  Item := QrLocIPatPriItem.Value;
  Codigo := QrLocCConCabCodigo.Value;
  if DBCheck.ObtemHora(Hora, Hora) then
  begin
    HrTolerancia := Geral.FDT(Hora, 100);
    if HrTolerancia = '' then
      HrTolerancia := '00:00:00';
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitslca', False, [
    'HrTolerancia'], ['Item'], [HrTolerancia], [Item], True) then
    begin
      RecalculaTudo(False, False);
      LocCod(Codigo, Codigo);
    end;
  end
end;

procedure TFmLocCConCab.Oramento2Click(Sender: TObject);
begin
  AlteraStatusMovimento(TStatusMovimento.statmovOrcamento);
end;

procedure TFmLocCConCab.OramentodeLocao1Click(Sender: TObject);
begin
  Application.CreateForm(TFmContratApp2, FmContratApp2);
  //
  FmContratApp2.FLocCConCab := QrLocCConCabCodigo.Value;
  //
  FmContratApp2.ImprimeOrcamentoLocacao();
  //
  FmContratApp2.Destroy;
end;

procedure TFmLocCConCab.Oramentodevenda1Click(Sender: TObject);
begin
  DefineImprOrcaPedi(TStatusMovimento.statmovOrcamento);
end;

procedure TFmLocCConCab.OrcamentoSelecionadoVias1Click(Sender: TObject);
const
  StatusMovimento = TStatusMovimento.statmovOrcamento;
  Vias = 1;
begin
  //ImprimeOrcamentoSelecionado(1);
  ImprimeOrcaPedi(QrLocCConItsCtrID.Value, StatusMovimento, Vias);
end;

procedure TFmLocCConCab.OrcamentoSelecionadoVias2Click(Sender: TObject);
const
  StatusMovimento = TStatusMovimento.statmovOrcamento;
  Vias = 2;
begin
  //ImprimeOrcamentoSelecionado(2);
  ImprimeOrcaPedi(QrLocCConItsCtrID.Value, StatusMovimento, Vias);
end;

procedure TFmLocCConCab.OrcamentoTodosVias1Click(Sender: TObject);
const
  StatusMovimento = TStatusMovimento.statmovOrcamento;
  Vias = 1;
  Todos = 0;
begin
  //ImprimeOrcamentoTodos(1);
  ImprimeOrcaPedi(Todos, StatusMovimento, Vias);
end;

procedure TFmLocCConCab.OrcamentoTodosVias2Click(Sender: TObject);
const
  StatusMovimento = TStatusMovimento.statmovOrcamento;
  Vias = 2;
  Todos = 0;
begin
  //ImprimeOrcamentoTodos(2);
  ImprimeOrcaPedi(Todos, StatusMovimento, Vias);
end;

procedure TFmLocCConCab.Outradata1Click(Sender: TObject);
begin
  RecalculaTudo(False, True);
end;

procedure TFmLocCConCab.Outradatamostrandoclculo1Click(Sender: TObject);
begin
  RecalculaTudo(True, True);
end;

procedure TFmLocCConCab.PCMovimentoChange(Sender: TObject);
begin
  HabilitaBotaoMovimentoAtual();
end;

procedure TFmLocCConCab.PCMovimentoChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  DesabilitaBotoesMovimento();
end;

procedure TFmLocCConCab.Pedido2Click(Sender: TObject);
begin
  AlteraStatusMovimento(TStatusMovimento.statmovPedido);
end;

procedure TFmLocCConCab.Pedidodevenda1Click(Sender: TObject);
begin
  DefineImprOrcaPedi(TStatusMovimento.statmovPedido);
end;

procedure TFmLocCConCab.PedidoSelecionadoVias2Click(Sender: TObject);
const
  StatusMovimento = TStatusMovimento.statmovPedido;
  Vias = 2;
begin
  ImprimeOrcaPedi(QrLocCConItsCtrID.Value, StatusMovimento, Vias);
end;

procedure TFmLocCConCab.PedidoSelecionadoVias1Click(Sender: TObject);
const
  StatusMovimento = TStatusMovimento.statmovPedido;
  Vias = 1;
begin
  ImprimeOrcaPedi(QrLocCConItsCtrID.Value, StatusMovimento, Vias);
end;

procedure TFmLocCConCab.PedidoTodosVias1Click(Sender: TObject);
const
  StatusMovimento = TStatusMovimento.statmovPedido;
  Vias = 1;
  Todos = 0;
begin
  //ImprimePedidoTodos(1);
  ImprimeOrcaPedi(Todos, StatusMovimento, Vias);
end;

procedure TFmLocCConCab.PedidoTodosVias2Click(Sender: TObject);
const
  StatusMovimento = TStatusMovimento.statmovPedido;
  Vias = 2;
  Todos = 0;
begin
  //ImprimePedidoTodos(2);
  ImprimeOrcaPedi(Todos, StatusMovimento, Vias);
end;

function TFmLocCConCab.PermiteEmitirNF(): Boolean;
begin
  Result :=
    TipoDeMovimentoEhVenda() and StatusMovimentoEhPedido()
    and (
      (QrLocCConItside_nNF.Value = 0)
      or
      (QrLocCConItsNF_Stat.Value <> CO_100_NFeAutorizada)
    )
    and (
      (QrLocCItsVen.State <> dsInactive)
      and
      (QrLocCItsVen.RecordCount > 0)
    );
end;

procedure TFmLocCConCab.PesquisaPorCliente;
var
  Cliente, Codigo, GraGruX: Integer;
begin
{
  Codigo := CuringaLoc.CriaForm('lcc.Codigo',
              'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome)', 'loccconcab lcc',
              Dmod.MyDB, '', False, 'LEFT JOIN entidades cli ON cli.Codigo=lcc.Cliente');
  //
  LocCod(QrLocCConCabCodigo.Value, Codigo);
}
  Cliente := 0; //QrLocCConCabCliente.Value;
  Codigo  := 0;
  GraGruX := 0;
  GraL_Jan.MostraFormLocCConPsq(Cliente, Codigo, GraGruX);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmLocCConCab.PMLocIPatAcePopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  if not StatusAtendimentoAberto() then
  begin
    Adicionaacessrio1.Enabled := False;
    Removeacessrio1.Enabled   := False;
  end else
  begin
    Enab  := (QrLocCConIts.State <> dsInactive) and (QrLocCConIts.RecordCount > 0);
    Enab2 := (QrLocIPatAce.State <> dsInactive) and (QrLocIPatAce.RecordCount > 0);
    //
    Adicionaacessrio1.Enabled := Enab;
    Removeacessrio1.Enabled   := Enab and Enab2;
  end;
end;

procedure TFmLocCConCab.PMCabPopup(Sender: TObject);
begin
  if not StatusAtendimentoAberto() then
  begin
    CabAltera1.Enabled := False;
    CabExclui1.Enabled := False;
  end else
  begin
    MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrLocCConCab);
    MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrLocCConCab, QrLocIPatPri);
  end;
  MudastatusparaAberto1.Enabled := StatusAtendimentoLocado()
                                   or
                                   StatusAtendimentoCancelado();
  MudastatusparaLocado1.Enabled := StatusAtendimentoAberto()
                                   or
                                   StatusAtendimentoFinalizado();
  Devolvertodosequipamentos1.Enabled := StatusAtendimentoLocado();
  Renovar1.Enabled :=
    TStatusLocacao(QrLocCConCabStatus.Value) = TStatusLocacao.statlocLocado;
end;

procedure TFmLocCConCab.PMFatPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  //Info_Fazer(' 5');
  if not StatusAtendimentoAberto() then
  begin
    //Faturamentoparcial1.Enabled     := False;
    //Faturaeencerracontrato1.Enabled := False;
    //Encerracontrato1.Enabled        := False;
    //
    Excluifaturamento1.Enabled := True;
    Reabrelocao1.Enabled       := True;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT COUNT(CtrID) Itens, ',
    'SUM(IF(YEAR(DtHrRetorn) < 1901, 1, 0)) Abertos ',
    'FROM loccitslca ',
    'WHERE Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
    '']);
    Faturamentoparcial1.Enabled     := Dmod.QrAux.FieldByName('Abertos').AsFloat > 0.01;
    Faturaeencerracontrato1.Enabled := Dmod.QrAux.FieldByName('Itens').AsFloat > 0.01;
    Encerracontrato1.Enabled        := Dmod.QrAux.FieldByName('Itens').AsFloat > 0.01;
    (*
    Gerabloqueto1.Enabled := (QrLocFCab.State <> dsInactive) and
      (QrLocFCab.RecordCount > 0);
    *)
    //
    Enab := (QrLocFCab.State <> dsInactive) and (QrLocFCab.RecordCount > 0);
    //
    Excluifaturamento1.Enabled := Enab;
    Reabrelocao1.Enabled       := Enab;
  end;
  Gerabloqueto1.Enabled := (QrLctFatRef.State <> dsInactive) and
    (QrLctFatRef.RecordCount > 0) and
    (not UBloquetos.VerificaSeBoletoExiste(QrLctFatRefLancto.Value));


////////////////////////////////////////////////////////////////////////////////


// T i s o l i n
  Faturaeencerracontrato1.Enabled := StatusAtendimentoLocado();
  Faturamentoparcial1.Enabled     := True; // sempre ???
  Encerracontrato1.Enabled        := StatusAtendimentoLocado();

end;

procedure TFmLocCConCab.PMImagensPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := StatusAtendimentoAberto();
  //
  if Enab then
  begin
    Visualizarimagemdocliente1.Enabled      := Enab and (QrLocCConCabCliente.Value <> 0);
    Visualizarimagemdorequisitante1.Enabled := Enab and (QrLocCConCabECComprou.Value <> 0);
    Visualizarimagemdorecebedor1.Enabled    := Enab and (QrLocCConCabECRetirou.Value <> 0);
  end else
  begin
    Visualizarimagemdocliente1.Enabled      := Enab;
    Visualizarimagemdorequisitante1.Enabled := Enab;
    Visualizarimagemdorecebedor1.Enabled    := Enab;
  end;
end;

procedure TFmLocCConCab.PMImprimePopup(Sender: TObject);
begin
  Oramentodevenda1.Enabled :=
    TipoDeMovimentoEhVenda() and StatusMovimentoEhOrcamento();
  MyObjects.HabilitaMenuItemItsUpd(Equipamentoprincipal1,  QrLocIPatPri);
  MyObjects.HabilitaMenuItemItsUpd(Equipamentosecundrio1,  QrLocIPatPri);
  MyObjects.HabilitaMenuItemItsUpd(Acessrio1,  QrLocIPatPri);
end;

procedure TFmLocCConCab.PMLocIItsLcaPopup(Sender: TObject);
begin
  ItsInclui1.Enabled := False;
  ItsAltera1.Enabled := False;
  //
  Devolues4.Enabled := False;
  Retorna1.Enabled := False;
  Libera1.Enabled := False;
  //
  ItsExclui1.Enabled := False;
  //
  ItsExclui2.Enabled := False;
  ItsExclui3.Enabled := False;
  //
  if TipoDeMovimentoEhLocacao() then
  begin
    if StatusAtendimentoLocado() then
    begin
      ItsInclui1.Enabled := False;
      ItsAltera1.Enabled := False;
      //
      MyObjects.HabilitaMenuItemItsUpd(Devolues4, QrLocIPatPri);
      MyObjects.HabilitaMenuItemItsUpd(Retorna1, QrLocIPatPri);
      MyObjects.HabilitaMenuItemItsUpd(Libera1, QrLocIPatPri);
      //
      ItsExclui1.Enabled := False;
      //
      MyObjects.HabilitaMenuItemItsUpd(ItsExclui2, QrLocIPatPri);
      MyObjects.HabilitaMenuItemItsUpd(ItsExclui3, QrLocIPatPri);
      //
    end else
    if StatusAtendimentoAberto() then
    begin
      MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrLocCConCab);
      MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrLocIPatPri);
      //
      MyObjects.HabilitaMenuItemItsUpd(Retorna1, QrLocIPatPri);
      MyObjects.HabilitaMenuItemItsUpd(Libera1, QrLocIPatPri);
      //
      MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrLocIPatPri);
      //
      MyObjects.HabilitaMenuItemItsDel(ItsExclui2, QrLocIPatPri);
      MyObjects.HabilitaMenuItemItsDel(ItsExclui3, QrLocIPatPri);
    end;
  end else
    Geral.MB_Aviso('O movimento selecionado n�o � de loca��o!');
  //
end;

procedure TFmLocCConCab.PMLocIItsSvcPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  if not StatusAtendimentoAberto() then
  begin
    AdicionaServico1.Enabled := False;
    RemoveServico1.Enabled   := False;
  end else
  begin
    Enab  := (QrLocCConIts.State <> dsInactive) and (QrLocCConIts.RecordCount > 0);
    Enab2 := (QrLocCItsSvc.State <> dsInactive) and (QrLocCItsSvc.RecordCount > 0);
    //
    AdicionaServico1.Enabled := Enab;
    RemoveServico1.Enabled   := Enab and Enab2;
  end;
end;

procedure TFmLocCConCab.PMLocIItsVenPopup(Sender: TObject);
var
  Enab1, Enab2, Enab3, Enab4: Boolean;
begin
  if not StatusAtendimentoAberto() then
  begin
    AdicionaVenda1.Enabled := False;
    RemoveVenda1.Enabled   := False;
  end else
  begin
    Enab1 := (QrLocCConIts.State <> dsInactive) and (QrLocCConIts.RecordCount > 0);
    Enab2 := (QrLocCItsVen.State <> dsInactive) and (QrLocCItsVen.RecordCount > 0);
    Enab3 := QrLocCConItsNF_Stat.Value < 100;
    Enab4 := StatusMovimentoPodeCancelar();
    //
    AdicionaVenda1.Enabled := Enab1 and Enab3 and Enab4;
    RemoveVenda1.Enabled   := Enab1 and Enab2 and Enab3 and Enab4;
  end;
  Enab1 := PermiteEmitirNF();
  EmiteNFCe2.Enabled := Enab1;
  EmiteNFe1.Enabled := Enab1;
end;

procedure TFmLocCConCab.PMLocIConItsPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := StatusAtendimentoAberto();
  MyObjects.HabilitaMenuItemItsIns(IncluiMovimento1, QrLocCConCab, Enab1);
  Enab2 := (
            TipoDeMovimentoEhLocacao()
            and (QrLocIPatPri.RecordCount +
                 QrLocIPatSec.RecordCount +
                 QrLocIPatAce.RecordCount = 0)
           )
           or
           (
              TipoDeMovimentoEhServico()
            and
             (QrLocCItsSvc.RecordCount = 0)
           )
           or
           (
              TipoDeMovimentoEhVenda()
            and
             (QrLocCItsVen.RecordCount = 0)
           );
  //MyObjects.HabilitaMenuItemItsDel(ExcluiMovimento1, QrLocCConIts, Habilita);
  ExcluiMovimento1.Enabled := Enab2;
  MyObjects.HabilitaMenuItemItsDel(ExcluiMovimentoetodosseusitens1, QrLocCConIts, Enab1);
  Alteracondiodepagamentodooramento1.Enabled :=
    TipoDeMovimentoEhVenda() and StatusMovimentoEhOrcamento();
  Oramento2.Enabled := StatusMovimentoEhIndefinido();
  Pedido2.Enabled := StatusMovimentoEhOrcamento();
  Cancelado2.Enabled := StatusMovimentoPodeCancelar();
end;

procedure TFmLocCConCab.PMLocIPatApoPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  if not StatusAtendimentoAberto() then
  begin
    AdicionamaterialdeApoio1.Enabled := False;
    RemovematerialdeApoio1.Enabled   := False;
  end else
  begin
    Enab  := (QrLocCConIts.State <> dsInactive) and (QrLocCConIts.RecordCount > 0);
    Enab2 := (QrLocIPatApo.State <> dsInactive) and (QrLocIPatApo.RecordCount > 0);
    //
    AdicionamaterialdeApoio1.Enabled := Enab;
    RemovematerialdeApoio1.Enabled   := Enab and Enab2;
  end;
end;

procedure TFmLocCConCab.PMLocIPatCnsPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  if not StatusAtendimentoAberto() then
  begin
    Adicionamaterialdeconsumo1.Enabled := False;
    Removematerialdeconsumo1.Enabled   := False;
  end else
  begin
    Enab  := (QrLocCConIts.State <> dsInactive) and (QrLocCConIts.RecordCount > 0);
    Enab2 := (QrLocIPatCns.State <> dsInactive) and (QrLocIPatCns.RecordCount > 0);
    //
    Adicionamaterialdeconsumo1.Enabled := Enab;
    Removematerialdeconsumo1.Enabled   := Enab and Enab2;
  end;
end;

procedure TFmLocCConCab.PMLocIPatSecPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  if not StatusAtendimentoAberto() then
  begin
    AdicionaequipamentoSecundrio1.Enabled := False;
    RemoveequipamentoSecundrio1.Enabled   := False;
  end else
  begin
    Enab  := (QrLocCConIts.State <> dsInactive) and (QrLocCConIts.RecordCount > 0);
    Enab2 := (QrLocIPatSec.State <> dsInactive) and (QrLocIPatSec.RecordCount > 0);
    //
    AdicionaequipamentoSecundrio1.Enabled := Enab;
    RemoveequipamentoSecundrio1.Enabled   := Enab and Enab2;
  end;
end;

procedure TFmLocCConCab.PMLocIPatUsoPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  if not StatusAtendimentoAberto() then
  begin
    Adicionamaterialuso1.Enabled := False;
    Removematerialuso1.Enabled   := False;
  end else
  begin
    Enab  := (QrLocCConIts.State <> dsInactive) and (QrLocCConIts.RecordCount > 0);
    Enab2 := (QrLocIPatUso.State <> dsInactive) and (QrLocIPatUso.RecordCount > 0);
    //
    Adicionamaterialuso1.Enabled := Enab;
    Removematerialuso1.Enabled   := Enab and Enab2;
  end;
end;

procedure TFmLocCConCab.PMNFsPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := PermiteEmitirNF();
  //
  EmiteNFCe1.Enabled := Enab;
  EmiteNFe2.Enabled := Enab;
end;

procedure TFmLocCConCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrLocCConCabCodigo.Value, LaRegistro.Caption[2]);
end;

function TFmLocCConCab.VerificaSeTodosOsItensRetornaram((*QueryLoc: TmySQLQuery;*)
  Codigo, Controle: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  //Info_Fazer(' 6');
  //False = Existem itens que n�o retornaram
  //True  = Todos os itens retornaram
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT lpp.CtrID');
    Qry.SQL.Add('FROM loccitslca lpp');
    Qry.SQL.Add('WHERE lpp.Codigo=' + Geral.FF0(Codigo));
    if Controle <> 0 then
      Qry.SQL.Add('AND lpp.CtrID=' + Geral.FF0(Controle));
    Qry.SQL.Add('AND lpp.DtHrRetorn < "1900-01-01"');
    UMyMod.AbreQuery(Qry, Dmod.MyDB);
    if Qry.RecordCount > 0 then
      Result := False
    else
      Result := True;
  finally
    Qry.Free;
  end;
end;

procedure TFmLocCConCab.Visualizarbloquetos1Click(Sender: TObject);
begin
  UBloquetos.MostraBloGeren(0, VAR_FATID_3001, 0, QrLocCConCabCodigo.Value,
    FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPagerNovo);
end;

procedure TFmLocCConCab.Visualizarimagemdocliente1Click(Sender: TObject);
begin
  VisualizaFoto(QrLocCConCabCodigo.Value, QrLocCConCabCliente.Value, 0);
end;

procedure TFmLocCConCab.Visualizarimagemdorecebedor1Click(Sender: TObject);
begin
  VisualizaFoto(QrLocCConCabCodigo.Value, QrLocCConCabCliente.Value, QrLocCConCabECRetirou.Value);
end;

procedure TFmLocCConCab.Visualizarimagemdorequisitante1Click(Sender: TObject);
begin
  VisualizaFoto(QrLocCConCabCodigo.Value, QrLocCConCabCliente.Value, QrLocCConCabECComprou.Value);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmLocCConCab.DefParams;
begin
  VAR_GOTOTABELA := 'loccconcab';
  VAR_GOTOMYSQLTABLE := QrLocCConCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT CONCAT(IF(Status=1 AND JaLocou=1, "RE", ""), ');
  VAR_SQLx.Add('  ELT(lcc.Status + 1, "N�O DEFINIDO", "ABERTO", ');
  VAR_SQLx.Add('  "LOCADO", "FINALIZADO", "CANCELADO", "SUSPENSO", ');
  VAR_SQLx.Add('  "N�O IMPLEMENTADO")) NO_STATUS, ');

  //VAR_SQLx.Add('SELECT ELT(lcc.Status + 1, "N�O DEFINIDO", "ABERTO", ');
  //VAR_SQLx.Add('"LOCADO", "FINALIZADO", "CANCELADO", "SUSPENSO", ');
  //VAR_SQLx.Add('"N�O IMPLEMENTADO") NO_STATUS, ');

  VAR_SQLx.Add('CASE lcc.TipoAluguel');
  VAR_SQLx.Add('  WHEN "D" THEN "DIARIA" ');
  VAR_SQLx.Add('  WHEN "S" THEN "SEMANAL" ');
  VAR_SQLx.Add('  WHEN "Q" THEN "QUINZENAL" ');
  VAR_SQLx.Add('  WHEN "M" THEN "MENSAL" ');
  VAR_SQLx.Add('ELSE "INDEFINIDO" END NO_TipoAluguel,');
  VAR_SQLx.Add('IF(DtHrSai < "1900-01-010", "", ');
  VAR_SQLx.Add('  DATE_FORMAT(DtHrSai, "%d/%m/%Y %H:%i")) DtHrSai_TXT,');
  VAR_SQLx.Add('IF(DtHrDevolver < "1900-01-010", "", ');
  VAR_SQLx.Add('  DATE_FORMAT(DtHrDevolver, "%d/%m/%Y %H:%i")) ');
  VAR_SQLx.Add('  DtHrDevolver_TXT,');
  VAR_SQLx.Add('com.Nome NO_COMPRADOR, ret.Nome NO_RECEBEU, ');
  VAR_SQLx.Add('lcc.*, sen.Login NO_LOGIN, cin.CodCliInt Filial, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE ');
  VAR_SQLx.Add('FROM loccconcab lcc ');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=lcc.Empresa ');
  VAR_SQLx.Add('LEFT JOIN enticliint cin ON cin.CodEnti=emp.Codigo ');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=lcc.Cliente ');
  VAR_SQLx.Add('LEFT JOIN senhas sen ON sen.Numero=lcc.Vendedor ');
  VAR_SQLx.Add('LEFT JOIN enticontat com ON com.Controle=lcc.ECComprou ');
  VAR_SQLx.Add('LEFT JOIN enticontat ret ON ret.Controle=lcc.ECRetirou ');
  VAR_SQLx.Add(' ');
  VAR_SQLx.Add('WHERE lcc.Codigo > 0');
  //
  VAR_SQL1.Add('AND lcc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmLocCConCab.DesabilitaBotoesMovimento();
begin
  BtLocCItsLca.Enabled := False;
  BtLocCItsSvc.Enabled := False;
  BtLocCItsVen.Enabled := False;
end;

procedure TFmLocCConCab.Devolues1Click(Sender: TObject);
const
  GeraLog = False;
var
  GTRTab: TGraToolRent;
  Codigo, Status, ManejoLca, CtrID, Item, GraGruX, Empresa: Integer;
  NO_ManejoLca, NO_GraGruX: String;
  Recalcular: Boolean;
  DtFimCobra, DtHrDevolver: TDateTime;
begin
  //Info_Fazer('Fazer 17');
  //
  if NotLocado() then Exit;
  //
  Codigo       := QrLocIPatPriCodigo.Value;
  Empresa      := QrLocCConCabEmpresa.Value;
  Status       := QrLocCConCabStatus.Value;
  GTRTab       := TGraToolRent.gbsLocar;
  ManejoLca    := QrLocIPatPriManejoLca.Value;
  CtrID        := QrLocIPatPriCtrID.Value;
  Item         := QrLocIPatPriItem.Value;
  NO_ManejoLca := sManejoLca[QrLocIPatPriManejoLca.Value];
  NO_GraGruX   := QrLocIPatPriNO_GGX.Value;
  DtHrDevolver := QrLocCConCabDtHrDevolver.Value;
  //

  //GraL_Jan.MostraFormLocCMovAll(GTRTab, Codigo, ManejoLca, CtrID, Item, GraGruX,
  //Empresa, NO_ManejoLca, NO_GraGruX, Recalcular);
  GraL_Jan.MostraFormLocCMovAll(GTRTab, Codigo, ManejoLca, CtrID, Item, GraGruX,
  Empresa, NO_ManejoLca, NO_GraGruX,

  TGraGXToolRnt.ggxo2gPrincipal, CO_GraGruY_1024_GXPatPri, QrLocCConCabTipoAluguel.Value,
  QrLocIPatPri, QrLocIPatSec, QrLocIPatAce,
  QrLocIPatApo, QrLocIPatCns, QrLocIPatUso,
  DtHrDevolver,
  Recalcular);
  if Recalcular then
  begin
    DtFimCobra := DModG.ObtemAgora();
    if AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, (*CtrID*)0,
    QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
    QrLocCConCabDtHrDevolver.Value, DtFimCobra, GeraLog) then
      AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmLocCConCab.Devolues2Click(Sender: TObject);
const
  GeraLog = False;
var
  GTRTab: TGraToolRent;
  Codigo, Status, ManejoLca, CtrID, Item, GraGruX, Empresa: Integer;
  NO_ManejoLca, NO_GraGruX: String;
  Recalcular: Boolean;
  DtFimCobra, DtHrDevolver: TDateTime;
begin
  //Info_Fazer('Fazer 37');
  //
  if NotLocado() then Exit;
  //
  Codigo       := QrLocCConCabCodigo.Value;
  Empresa      := QrLocCConCabEmpresa.Value;
  Status       := QrLocCConCabStatus.Value;
  GTRTab       := TGraToolRent.gbsLocar;
  ManejoLca    := QrLocIPatSecManejoLca.Value;
  CtrID        := QrLocIPatSecCtrID.Value;
  Item         := QrLocIPatSecItem.Value;
  NO_ManejoLca := sManejoLca[QrLocIPatSecManejoLca.Value];
  NO_GraGruX   := QrLocIPatSecNO_GGX.Value;
  DtHrDevolver := QrLocCConCabDtHrDevolver.Value;
  //
  GraL_Jan.MostraFormLocCMovAll(GTRTab, Codigo, ManejoLca, CtrID, Item, GraGruX,
  Empresa, NO_ManejoLca, NO_GraGruX,

  TGraGXToolRnt.ggxo2gSecundario, CO_GraGruY_2048_GXPatSec, QrLocCConCabTipoAluguel.Value,
  QrLocIPatPri, QrLocIPatSec, QrLocIPatAce,
  QrLocIPatApo, QrLocIPatCns, QrLocIPatUso,
  DtHrDevolver,
  Recalcular);
  if Recalcular then
  begin
    DtFimCobra := DModG.ObtemAgora();
    if AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, (*CtrID*)0,
    QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
    QrLocCConCabDtHrDevolver.Value, DtFimCobra, GeraLog) then
      AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmLocCConCab.Devolues3Click(Sender: TObject);
const
  GeraLog = False;
var
  GTRTab: TGraToolRent;
  Codigo, Status, ManejoLca, CtrID, Item, GraGruX, Empresa: Integer;
  NO_ManejoLca, NO_GraGruX: String;
  Recalcular: Boolean;
  DtFimCobra, DtHrDevolver: TDateTime;
begin
  //Info_Fazer('Fazer 37 A');
  //
  if NotLocado() then Exit;
  //
  Codigo       := QrLocCConCabCodigo.Value;
  Empresa      := QrLocCConCabEmpresa.Value;
  Status       := QrLocCConCabStatus.Value;
  GTRTab       := TGraToolRent.gbsLocar;
  ManejoLca    := QrLocIPatAceManejoLca.Value;
  CtrID        := QrLocIPatAceCtrID.Value;
  Item         := QrLocIPatAceItem.Value;
  NO_ManejoLca := sManejoLca[QrLocIPatAceManejoLca.Value];
  NO_GraGruX   := QrLocIPatAceNO_GGX.Value;
  DtHrDevolver := QrLocCConCabDtHrDevolver.Value;
  //
  GraL_Jan.MostraFormLocCMovAll(GTRTab, Codigo, ManejoLca, CtrID, Item, GraGruX,
  Empresa, NO_ManejoLca, NO_GraGruX,

  TGraGXToolRnt.ggxo2gAcessorio, CO_GraGruY_3072_GXPatAce, QrLocCConCabTipoAluguel.Value,
  QrLocIPatPri, QrLocIPatSec, QrLocIPatAce,
  QrLocIPatApo, QrLocIPatCns, QrLocIPatUso,
  DtHrDevolver,
  Recalcular);
  if Recalcular then
  begin
    DtFimCobra := DModG.ObtemAgora();
    if AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, (*CtrID*)0,
    QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
    QrLocCConCabDtHrDevolver.Value, DtFimCobra, GeraLog) then
      AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmLocCConCab.Devolues4Click(Sender: TObject);
const
  GeraLog = False;
var
  Codigo, Status, CtrID, AtuControle, Item, NewControle, Empresa: Integer;
  TipoMotiv: TTipoMotivLocMov;
  Recalcular: Boolean;
  DtFimCobra, DtHrDevolver: TDateTime;
begin
  Codigo       := QrLocCConItsCodigo.Value;
  Empresa      := QrLocCConCabEmpresa.Value;
  Status       := QrLocCConCabStatus.Value;
  CtrID        := QrLocCConItsCtrID.Value;
  Item         := 0;
  TipoMotiv    := TTipoMotivLocMov.tmlmLocarVolta;
  AtuControle  := 0;
  DtHrDevolver := QrLocCConCabDtHrDevolver.Value;
  //
  GraL_Jan.MostraFormLocCItsRet(stIns, Codigo, CtrID, Item, AtuControle,
  Empresa, TipoMotiv, DtHrDevolver, NewControle, Recalcular);
  if Recalcular then
  begin
    DtFimCobra := DModG.ObtemAgora();
    if AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, (*CtrID*)0,
    QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
    QrLocCConCabDtHrDevolver.Value, DtFimCobra, GeraLog) then
      AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmLocCConCab.Devolues5Click(Sender: TObject);
begin
  GraL_Jan.MostraFormLocCMovGer(QrLocCConCabEmpresa.Value,
    QrLocCConCabCodigo.Value, QrLocCConCabCLiente.Value, (*GraGruX*)0);
end;

procedure TFmLocCConCab.Devolvertodosequipamentos1Click(Sender: TObject);
const
  Finaliza = True;
var
  ItensNegativosVend: Integer;
  BxaDtHr: TDateTime;
begin
  BxaDtHr := DModG.ObtemAgora();
  AppPF.DevolveTodosEquipamentosLocacao(QrLocCConCabCodigo.Value,
    QrLocCConCabEmpresa.Value, QrItsLca, Finaliza, BxaDtHr,
    QrLocCConCabDtHrDevolver.Value, QrLocCConCabValorPndt.Value);
  //
  RecalculaEstoquesVend(ItensNegativosVend);
  //
  LocCod(QrLocCConCabCodigo.Value, QrLocCConCabCodigo.Value);
end;

procedure TFmLocCConCab.DBGLocIPatPriDblClick(Sender: TObject);
var
  Campo: String;
begin
  if not DoubleClick_GradeItens(TDBGrid(Sender)) then
  begin
(*
    if (QrLocIPatPri.State = dsInactive) or (QrLocIPatPri.RecordCount = 0) then
      Exit;
    //
    Campo := Uppercase(DBGLocIPatPri.Columns[THackDBGrid(DBGLocIPatPri).Col -1].FieldName);
    if (Uppercase(Campo) = Uppercase('GraGruX'))
    or (Uppercase(Campo) = Uppercase('Patrimonio'))
    or (Uppercase(Campo) = Uppercase('REFERENCIA')) then
        GraL_Jan.MostraFormGraGXPatr(QrLocIPatPriGraGruX.Value);
*)
  end;
end;

procedure TFmLocCConCab.DBGLocIPatPriMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMLocIPatPri, DBGLocIPatPri, X,Y);

end;

function TFmLocCConCab.DoubleClick_GradeItens(Sender: TDBGrid): Boolean;
const
  sProcName = 'TFmLocCConCab.DoubleClick_GradeItens()';
var
  Campo: String;
  Qry: TmySQLQuery;
  GraGruX, GraGruY, Nivel1: Integer;
begin
  Result := False;
  //
  Qry := TmySQLQuery(Sender.DataSource.DataSet);
  if (Qry <> nil) and (Qry.State <> dsInactive) and (Qry.RecordCount > 0) then
  begin
    Campo := Uppercase(Sender.Columns[THackDBGrid(Sender).Col -1].FieldName);
    if (Uppercase(Campo) = Uppercase('GRAGRUX'))
    or (Uppercase(Campo) = Uppercase('PATRIMONIO'))
    or (Uppercase(Campo) = Uppercase('REFERENCIA')) then
    begin
      GraGruX := Qry.FieldByName('GraGruX').AsInteger;
      GraGruY := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
      //
      case GraGruY of
        CO_GraGruY_1024_GXPatPri, // = Primario
        CO_GraGruY_2048_GXPatSec, // = Secund�rio
        CO_GraGruY_3072_GXPatAce: // = Acess�rio
        begin
          Result := True;
          GraL_Jan.MostraFormGraGXPatr(GraGruX);
        end;
        CO_GraGruY_4096_GXPatApo, // = Apoio
        CO_GraGruY_5120_GXPatUso,  // = Uso (desgaste)
        CO_GraGruY_6144_GXPrdCns:  // = Uso (consumo)
        begin
          GraL_Jan.MostraFormGraGXOutr(GraGruX);
          Result := True;
        end;
        else
          Geral.MB_Erro('GraGruY (' + Geral.FF0(GraGruY) +
          ') n�o implementado em ' + sProcName);
          Nivel1 := DmProd.ObtemGraGru1DeGraGruX(GraGruX);
          Grade_Jan.MostraFormGraGruN(Nivel1);
      end;
    end;
  end;
end;

procedure TFmLocCConCab.EDDtHrSaiRedefinido(Sender: TObject);
begin
  RecalculaDataProgramadaRetorno();
end;

procedure TFmLocCConCab.EdDtHrDevolverRedefinido(Sender: TObject);
begin
  //RecalculaDataProgramadaRetorno();
end;

procedure TFmLocCConCab.Timer1Timer(Sender: TObject);
begin
  DBText1.Visible := not DBText1.Visible;
  if DBText1.Visible then
  begin
    DBText1.Font.Color := clRed;
    DBText1.Invalidate;
  end;
end;

function TFmLocCConCab.TipoDeMovimentoEhLocacao(): Boolean;
begin
  Result :=
  (
    (QrLocCConIts.State <> dsInactive)
    and
    (QrLocCConIts.RecordCount > 0)
    and
    (TTabConLocIts(QrLocCConItsIDTab.Value) = TTabConLocIts.tcliLocacao)
  );
end;

function TFmLocCConCab.TipoDeMovimentoEhServico(): Boolean;
begin
  Result :=
  (
    (QrLocCConIts.State <> dsInactive)
    and
    (QrLocCConIts.RecordCount > 0)
    and
    (TTabConLocIts(QrLocCConItsIDTab.Value) = TTabConLocIts.tcliOutroServico)
  );
end;

function TFmLocCConCab.TipoDeMovimentoEhVenda(): Boolean;
begin
  Result :=
  (
    (QrLocCConIts.State <> dsInactive)
    and
    (QrLocCConIts.RecordCount > 0)
    and
    (TTabConLocIts(QrLocCConItsIDTab.Value) = TTabConLocIts.tcliVenda)
  );
end;

procedure TFmLocCConCab.TPDtHrDevolverClick(Sender: TObject);
begin
  RecalculaDataProgramadaRetorno();
end;

procedure TFmLocCConCab.EdClienteChange(Sender: TObject);
begin
  ReopenECComprou(0);
  ReopenECRetirou(0);
  //
  if EdCliente.ValueVariant <> 0 then
    EdEndCliente.Text := GOTOy.EnderecoDeEntidade(EdCliente.ValueVariant, 0);
  //
  AtualizaImagens(EdCliente.ValueVariant, 0, ImgCliente);
end;

procedure TFmLocCConCab.EdClienteRedefinido(Sender: TObject);
var
  Cliente: Integer;
begin
  Cliente := EdCliente.ValueVariant;
  ReopenEntiStatus(Cliente);
  //
  if Cliente <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrValAberto, Dmod.MyDB, [
    'SELECT Cliente, SUM(Credito) Aberto, ',
    'SUM(IF(Vencimento < SYSDATE(), Credito, 0)) Vencido ',
    'FROM ' +  FTabLctA,
    'WHERE Cliente=' + Geral.FF0(Cliente),
    'AND Compensado < "1900-01-01" ',
    '']);
  end else
    QrValAberto.Close;
end;

procedure TFmLocCConCab.EdECComprouChange(Sender: TObject);
begin
  AtualizaImagens(EdECComprou.ValueVariant, 1, ImgRequisitante);
end;

procedure TFmLocCConCab.EdECRetirouChange(Sender: TObject);
begin
  AtualizaImagens(EdECRetirou.ValueVariant, 1, ImgRetirada);
end;

procedure TFmLocCConCab.EmiteNFCe();
{$IfNDef SemNFe_0000}
const
  OriTipo = 1;
  OEstoqueJaFoiBaixado = True;
var
  Especie, UFCli: String;
  Quantidade, FatSemEstq, FatPedCab, VolCnta, RegrFiscal, PediVda, CtrID,
  EntidadeTipo: Integer;
  ValFrete, prod_vFrete, ValorTotal: Double;
  IDCtrl, Status: Integer;
  Achou: Boolean;
  //
  Empresa, Tipo, OriCodi, ValiStq,
  Codigo,  FatID, FatNum, ide_mod, ide_nNF, NF_Stat, Baixa, Cliente: Integer;
  CodStatus: Integer;
  TxtStatus: String;
begin










  try
    //UnNFe_PF.NFe_StatusServicoCodDef(Self, False, CodStatus, TxtStatus);
    UnNFe_PF.NFe_StatusServicoCodDef(Self, CodStatus, TxtStatus);
    if CodStatus <> 107 then
    begin
      Geral.MB_Info(TxtStatus + ' (C�digo ' + Geral.FF0(CodStatus) + ')');
      Exit;
    end;
  except
     UnWin.MostraMMC_SnapIn();
     Exit;
  end;












  ValFrete := 0;
  if MovimentoPossuiNFAutorizada
  (True) then Exit;
  //
  ReopenPsqSum_LocCConIts((*VAR_FATID_0002,*) Achou);
(*
  IDCtrl := QrPsqSum.FieldByName('IDCtrl').AsInteger;
  if IDCtrl > 0 then
  begin
    Status := QrPsqSum.FieldByName('Status').AsInteger;
    //
    if Status = 35 then
    begin
      if Geral.MB_Pergunta('J� existe uma NFC-e emitida OFF-LINE para este movimento!' +
      sLineBreak + 'S�rie: ' + Geral.FF0(QrPsqSum.FieldByName('ide_Serie').AsInteger) +
      sLineBreak + 'N� NF: ' + Geral.FF0(QrPsqSum.FieldByName('ide_nNF').AsInteger) +
      sLineBreak + 'Deseja visualiz�-la?') = ID_Yes then
      begin
        UnNFe_PF.MostraFormNFePesq(True, FMPrincipal.PageControl1,
        FmPrincipal.AdvToolBarPagerNovo, 0);
        //
        Exit;
      end;
      //else vai permitir editar!!!
    end else
    if Status = 100 then
    begin
      if Geral.MB_Pergunta('J� existe uma NFC-e emitida para este movimento!' +
      sLineBreak + 'S�rie: ' + Geral.FF0(QrPsqSum.FieldByName('ide_Serie').AsInteger) +
      sLineBreak + 'N� NF: ' + Geral.FF0(QrPsqSum.FieldByName('ide_nNF').AsInteger) +
      sLineBreak + 'Deseja visualiz�-la?') = ID_Yes then
        UnNFe_PF.MostraFormNFePesq(True, FMPrincipal.PageControl1,
        FmPrincipal.AdvToolBarPagerNovo, 0);
      //
      Exit;
    end;
  end;
*)
///////////////////////////////////////////////////////////////////////////////
///  Se achou n�o pode emitir de novo!
  if Achou then Exit;

////////////////////////////////////////////////////////////////////////////////
  //Fazer NFCe com presencial e frete
  // Precisa ser por item de NF!
(**)
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqSum, Dmod.MyDB, [
  'SELECT ',
  'IF(its.Status<2, svc.ValorTotal, 0) ValOrca, ',
  'IF(its.Status<2, 0, svc.ValorTotal) ValPedi ',
  'FROM loccitssvc svc ',
  'LEFT JOIN loccconits its ON its.CtrID=svc.CtrID ',
  'WHERE svc.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  'AND svc.AtrelCtrID=' + Geral.FF0(QrLocCItsVenCtrID.Value),
  '']);
  //
  if QrPsqSum.FieldByName('ValOrca').AsFloat > 0 then
  begin
    if Geral.MB_Pergunta('H� o valor de frete de $ ' + Geral.FFT(
    QrPsqSum.FieldByName('ValOrca').AsFloat, 2, siPositivo) + ' que n�o ser� considerado!' +
    sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  end;
  ValFrete := QrPsqSum.FieldByName('ValPedi').AsFloat;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqSum, Dmod.MyDB, [
  'SELECT SUM(ValorTotal) ValorTotal, SUM(ven.prod_vFrete) prod_vFrete',
  'FROM loccitsven ven ',
  'LEFT JOIN loccconits its ON its.CtrID=ven.CtrID ',
  'WHERE ven.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  'AND ven.CtrID=' + Geral.FF0(QrLocCConItsCtrID.Value),
  '']);
  prod_vFrete := QrPsqSum.FieldByName('prod_vFrete').AsFloat;
  ValorTotal  := QrPsqSum.FieldByName('ValorTotal').AsFloat;
  //
  if prod_vFrete <> ValFrete then
  begin
    Geral.MB_Aviso('Valor do frete dos itens n�o confere com o valor do frete de servi�o!'
    + sLineBreak + 'Frete dos itens: $ ' + Geral.FFT(prod_vFrete, 2, siNegativo)
    + sLineBreak + 'Frete dos servi�os: $ ' + Geral.FFT(ValFrete, 2, siNegativo)
    + '');
    //
    Exit;
  end;
*)

  //UnNFe_PF.MostraFormStepsNFe_StatusServico();

  ValorTotal := QrLocCConItsValorPedi.Value;

  CtrID := QrLocCConItsCtrID.Value;
  //DModG.ReopenParamsEmp();
  //FatSemEstq := DModG.QrParamsEmpFatSemEstq.Value;
  FatSemEstq := 1; // Permite sem avisar pois o estoque j� foi baixado!
  FatPedCab  := QrLoCCConItsFatPedCab.Value;
  Cliente    := QrLoCCConCabCliente.Value;
  PediVda    := QrLoCCConItsPediVda.Value;
  VolCnta    := QrLoCCConItsFatPedVolCnta.Value;
  RegrFiscal := Dmod.QrOpcoesTRenLocRegrFisNFCe.Value;
  //
  Empresa    := QrLoCCConCabEmpresa.Value;
  Codigo     := QrLoCCConCabCodigo.Value;
  //
  ReopenEndContratante();
  UFCli := (*Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(*)QrEndContratanteNOMEUF.Value;
  EntidadeTipo := QrEndContratanteTipo.Value;
  //
  XXe_PF.AlteraPedido_TipoNF(Empresa, FatPedCab, Cliente, RegrFiscal,
    (*De*) VAR_FATID_0001, (*Para*) VAR_FATID_0002);
  //
  UnNFCe_PF.EncerraFaturamento(FatSemEstq, FatPedCab, PediVda, VolCnta, OriTipo,
    RegrFiscal, 'fatpedcab', ValFrete, UFCli, EntidadeTipo, ValorTotal,
    (* OEstoqueJaFoiBaixado*)True);
  //
  // Fazer aqui somas e atualizad��es!!!!!!!!!!!!
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT FatID, FatNum, ide_mod, ide_nNF, Status  ',
  'FROM nfecaba ',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_0002), // NFCe
  'AND FatNum=' + Geral.FF0(FatPedCab),
  'AND Empresa=' + Geral.FF0(Empresa),
  'ORDER BY Status DESC ',
  ' ']);
  //Geral.MB_Teste(Dmod.QrAux.SQL.Text);
  NF_Stat := 0;
  if Dmod.QrAux.RecordCount > 0 then
  begin
    FatID    := DMod.QrAux.FieldByName('FatID').AsInteger;
    FatNum   := DMod.QrAux.FieldByName('FatNum').AsInteger;
    ide_mod  := DMod.QrAux.FieldByName('ide_mod').AsInteger;
    ide_nNF  := DMod.QrAux.FieldByName('ide_nNF').AsInteger;
    NF_Stat  := DMod.QrAux.FieldByName('Status').AsInteger;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconits', False, [
    'Codigo', 'FatID', 'FatNum',
    'ide_mod', 'ide_nNF', 'NF_Stat'], [
    'CtrID'], [
    Codigo, FatID, FatNum,
    ide_mod, ide_nNF, NF_Stat], [
    CtrID], True) then
    begin
    end;
  end;
  //
  AppPF.VerificaEstoqueVenda(Empresa, FatPedCab, NF_Stat, QrLocCConCabStatus.Value,
  QrLocCConItsStatus.Value);
  //
  LocCod(QrLocCConCabCodigo.Value, QrLocCConCabCodigo.Value);
  QrLocCConIts.Locate('CtrID', CtrID, []);
  //
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappNFe);
{$EndIf}
end;

procedure TFmLocCConCab.EmiteNFCe1Click(Sender: TObject);
begin
  EmiteNFCe();
end;

procedure TFmLocCConCab.EmiteNFCe2Click(Sender: TObject);
begin
  EmiteNFCe();
end;

procedure TFmLocCConCab.EmiteNFe();
{$IfNDef SemNFe_0000}
const
  OriTipo = 1;
  OEstoqueJaFoiBaixado = True;
  SemFinanceiro = 0;
var
  Especie, UFCli: String;
  Quantidade, FatSemEstq, FatPedCab, VolCnta, RegrFiscal, PediVda, CtrID,
  EntidadeTipo: Integer;
  ValFrete, prod_vFrete, ValorTotal: Double;
  IDCtrl, Status: Integer;
  Achou: Boolean;
  //
  Cliente, Empresa, Tipo, OriCodi, ValiStq,
  Codigo,  FatID, FatNum, ide_mod, ide_nNF, NF_Stat, Baixa: Integer;
  CodStatus: Integer;
  TxtStatus: String;
begin
  ValFrete := 0;
  if MovimentoPossuiNFAutorizada(True) then Exit;
  //
  if MovimentoPossuiNFIniciadaENaoExcluida(65, True) then Exit;
///////////////////////////////////////////////////////////////////////////////
  ReopenPsqSum_LocCConIts((*VAR_FATID_0001,*) Achou);
///  Se achou n�o pode emitir de novo!
  if Achou then Exit;

////////////////////////////////////////////////////////////////////////////////
  //Fazer NFCe com presencial e frete
  // Precisa ser por item de NF!
  ValorTotal := QrLocCConItsValorPedi.Value;
  //
  CtrID := QrLocCConItsCtrID.Value;
  //DModG.ReopenParamsEmp();
  //FatSemEstq := DModG.QrParamsEmpFatSemEstq.Value;
  FatSemEstq := 1; // Permite sem avisar pois o estoque j� foi baixado!
  Cliente    := QrLoCCConCabCliente.Value;
  FatPedCab  := QrLoCCConItsFatPedCab.Value;
  PediVda    := QrLoCCConItsPediVda.Value;
  VolCnta    := QrLoCCConItsFatPedVolCnta.Value;
  RegrFiscal := Dmod.QrOpcoesTRenLocRegrFisNFe.Value;
  //
  Empresa    := QrLoCCConCabEmpresa.Value;
  Codigo     := QrLoCCConCabCodigo.Value;
  //
  ReopenEndContratante();
  UFCli := (*Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(*)QrEndContratanteNOMEUF.Value;
  EntidadeTipo := QrEndContratanteTipo.Value;
  //
  // For�a convers�o de registros pr�-cadastrados de pedido e faturamento de NFCe
  // para NFe
  XXe_PF.AlteraPedido_TipoNF(Empresa, FatPedCab, Cliente, RegrFiscal,
  (*De*) VAR_FATID_0002, (*Para*) VAR_FATID_0001);
//

   try
    //UnNFe_PF.NFe_StatusServicoCodDef(Self, False, CodStatus, TxtStatus);
    UnNFe_PF.NFe_StatusServicoCodDef(Self, CodStatus, TxtStatus);
    if CodStatus <> 107 then
    begin
      Geral.MB_Info(TxtStatus + ' (C�digo ' + Geral.FF0(CodStatus) + ')');
      Exit;
    end;
  except
     UnWin.MostraMMC_SnapIn();
     Exit;
  end;

  // ini 2021-11-03
{
  UnNFe_PF.EncerraFaturamento_Quiet(FatSemEstq, FatPedCab, PediVda, (*IDVolume*)VolCnta, (*Tipo*)OriTipo,
  RegrFiscal, (*Tabela*)'fatpedcab' (*, OEstoqueJaFoiBaixado: Boolean*),
  SemFinanceiro, ValorTotal, ValFrete);
}

  UnNFe_PF.EncerraFaturamento_Quiet(FatSemEstq, FatPedCab, PediVda, (*IDVolume*)VolCnta, (*Tipo*)OriTipo,
  RegrFiscal, (*Tabela*)'fatpedcab',
  SemFinanceiro, ValorTotal, ValFrete,
  (*FretePor*)0, (*Transporta*)0);

  // fim 2021-11-03

  //UnNFCe_PF.EncerraFaturamento(FatSemEstq, FatPedCab, PediVda, VolCnta, OriTipo,
  //  RegrFiscal, 'fatpedcab', ValFrete, UFCli, EntidadeTipo, ValorTotal,
  //  (* OEstoqueJaFoiBaixado*)True);
  //
  // Fazer aqui somas e atualizad��es!!!!!!!!!!!!
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT FatID, FatNum, ide_mod, ide_nNF, Status  ',
  'FROM nfecaba ',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_0001), // NFe
  'AND FatNum=' + Geral.FF0(FatPedCab),
  'AND Empresa=' + Geral.FF0(Empresa),
  'ORDER BY Status DESC ',
  ' ']);
  //Geral.MB_Teste(Dmod.QrAux.SQL.Text);
  NF_Stat := 0;
  if Dmod.QrAux.RecordCount > 0 then
  begin
    FatID    := DMod.QrAux.FieldByName('FatID').AsInteger;
    FatNum   := DMod.QrAux.FieldByName('FatNum').AsInteger;
    ide_mod  := DMod.QrAux.FieldByName('ide_mod').AsInteger;
    ide_nNF  := DMod.QrAux.FieldByName('ide_nNF').AsInteger;
    NF_Stat  := DMod.QrAux.FieldByName('Status').AsInteger;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconits', False, [
    'Codigo', 'FatID', 'FatNum',
    'ide_mod', 'ide_nNF', 'NF_Stat'], [
    'CtrID'], [
    Codigo, FatID, FatNum,
    ide_mod, ide_nNF, NF_Stat], [
    CtrID], True) then
    begin
    end;
  end;
  //
  AppPF.VerificaEstoqueVenda(Empresa, FatPedCab, NF_Stat, QrLocCConCabStatus.Value,
  QrLocCConItsStatus.Value);
  //
  LocCod(QrLocCConCabCodigo.Value, QrLocCConCabCodigo.Value);
  QrLocCConIts.Locate('CtrID', CtrID, []);
  //
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappNFe);
{$EndIf}
end;

procedure TFmLocCConCab.EmiteNFe1Click(Sender: TObject);
begin
  EmiteNFe();
end;

procedure TFmLocCConCab.EmiteNFe2Click(Sender: TObject);
begin
  EmiteNFe();
end;

procedure TFmLocCConCab.Encerracontrato1Click(Sender: TObject);
var
  DataDef, DataSel, DataMin: TDateTime;
  DtHrBxa: String;
  ValTotalFat: Double;
  Codigo: Integer;
  Qry: TmySQLQuery;
begin
  if not LiberaItensLocacao(QrLocIPatPri(*, QrLoc*)) then Exit;
  //
  Codigo  := QrLocCConCabCodigo.Value;
  DataDef := Now();
  DataMin := 0;
  DataSel := 0;
  if not DBCheck.ObtemData(DataDef, DataSel,dataMin, Time, True) then Exit;
  //
  DtHrBxa := Geral.FDT(DataSel, 109);
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT SUM(lfc.ValTotal) ValTotal');
    Qry.SQL.Add('FROM locfcab lfc');
    Qry.SQL.Add('WHERE lfc.Codigo=' + Geral.FF0(Codigo));
    UMyMod.AbreQuery(Qry, Dmod.MyDB);
    if Qry.RecordCount > 0 then
      ValTotalFat := Qry.FieldByName('ValTotal').AsFloat
    else
      ValTotalFat := 0;
  finally
    Qry.Free;
  end;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconcab', False, ['ValorTot',
    'DtHrBxa'], ['Codigo'], [ValTotalFat, DtHrBxa], [Codigo], True);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmLocCConCab.Equipamentoprincipal1Click(Sender: TObject);
begin
  ImprimeFichaDeTroca(TGraGXToolRnt.ggxo2gPrincipal);
end;

procedure TFmLocCConCab.Equipamentosecundrio1Click(Sender: TObject);
begin
  ImprimeFichaDeTroca(TGraGXToolRnt.ggxo2gSecundario);
end;

function TFmLocCConCab.ErroStatusNFe: Boolean;
const
  MostraForm = False;
var
  CodStatus: Integer;
  TxtStatus: String;
begin
  //UnNFe_PF.NFe_StatusServicoCodDef(Self, MostraForm, CodStatus, TxtStatus);
  UnNFe_PF.NFe_StatusServicoCodDef(Self, CodStatus, TxtStatus);
  Result := CodStatus = 107;
  if not Result then
  begin
    if CodStatus < 100 then
    begin
      UnWin.MostraMMC_SnapIn();
    end else
      Geral.MB_Info(TxtStatus + ' (C�digo ' + Geral.FF0(CodStatus) + ')');
  end;
end;

procedure TFmLocCConCab.Gerabloqueto1Click(Sender: TObject);
var
  Entidade, Filial: Integer;
  TabLctA: String;
begin
  try
    Screen.Cursor         := crHourGlass;
    Gerabloqueto1.Enabled := False;
    //
    Entidade := QrLocCConCabEmpresa.Value;
    Filial   := DmFatura.ObtemFilialDeEntidade(Entidade);
    TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
    //
    UBloquetos.BloquetosParcelasByFatNum(Entidade, VAR_FatID_3001,
      QrLocFCabControle.Value, QrLocCConCabCodigo.Value, TabLctA, True, PB1);
  finally
    Gerabloqueto1.Enabled := True;
    Screen.Cursor         := crDefault;
  end;
end;

procedure TFmLocCConCab.HabilitaBotaoMovimentoAtual();
var
  IdTab, Mov, Qual: Integer;
begin
  Qual := 0;
  if StatusAtendimentoAberto() and (QrLocCConIts.State <> dsInactive) then
  begin
    IdTab := PCMovimento.ActivePageIndex + 1;
    Mov := QrLocCConItsIDTab.Value;
    if IdTab = Mov then
      Qual := IdTab;
    //
    BtLocCItsLca.Enabled := Qual = 1; // Loca��o
    BtLocCItsSvc.Enabled := Qual = 2; // Servi�o
    BtLocCItsVen.Enabled := Qual = 3; // Venda
  end else
  if StatusAtendimentoLocado() and (QrLocCConIts.State <> dsInactive) then
  begin
    BtLocCItsLca.Enabled := True; // Loca��o pode retornar itens!
    BtLocCItsSvc.Enabled := False; // Servi�o
    BtLocCItsVen.Enabled := False; // Venda
  end else
  begin
    DesabilitaBotoesMovimento();
  end;
end;

procedure TFmLocCConCab.Excluifaturamento1Click(Sender: TObject);
var
  Codigo, Filial, Quitados: Integer;
  TabLctA: String;
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then Exit;
  //
  if MyObjects.FIC(QrLocFCabSerNF.Value <> '', nil, 'Exclus�o abortada!' + sLineBreak +
    'Motivo: J� foi emitida um Nota Fiscal para este faturamento!') then Exit;
  //
  if MyObjects.FIC((QrLocFCab.State = dsInactive) or (QrLocFCab.RecordCount = 0),
    nil, 'N�o h� nenhum faturamento para ser exclu�do!') then Exit;
  //
  Filial  := DmFatura.ObtemFilialDeEntidade(QrLocCConCabEmpresa.Value);
  TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  Codigo  := QrLocCConCabCodigo.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, Dmod.MyDB, [
  'SELECT COUNT(Controle) Itens ',
  'FROM ' + TabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_3001),
  'AND FatNum=' + Geral.FF0(QrLocFCabControle.Value),
  'AND (Sit > 0 OR Compensado > 2) ',
  '']);
  //
  Quitados := DModFin.FaturamentosQuitados(
    TabLctA, VAR_FATID_3001, QrLocFCabControle.Value);
  //
  if Quitados > 0 then
  begin
    Geral.MensagemBox('Existem ' + Geral.FF0(Quitados) +
    ' itens j� quitados que impedem a exclus�o deste faturamento',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Geral.MensagemBox(
  'Confirma a exclus�o do faturamento e de TODOS seus lan�amentos financeiros atrelados?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UBloquetos.DesfazerBoletosFatID(QrLoc, Dmod.MyDB, VAR_FatID_3001,
      QrLocCConCabCodigo.Value, TabLctA) then
    begin
      Screen.Cursor := crHourGlass;
      try
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
          'DELETE FROM ' + TabLctA,
          'WHERE FatID=' + Geral.FF0(VAR_FATID_3001),
          'AND FatNum=' + Geral.FF0(QrLocFCabControle.Value),
          '']);
        //
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
          'DELETE FROM lctfatref',
          'WHERE Controle=' + Geral.FF0(QrLocFCabControle.Value),
          '']);
        //
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
          'DELETE FROM locfcab ',
          'WHERE Controle=' + Geral.FF0(QrLocFCabControle.Value),
          '']);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconcab', False, [
          'ValorTot', 'DtHrBxa'], ['Codigo'], [0, '0000-00-00 00:00:00'], [Codigo], True);
        //
        ReopenLocFCab(0);
        LocCod(Codigo, Codigo);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
    //
    AppPF.AtualizaTotaisLocCConCab_Faturados(Codigo, False(*Encerra*), (*DtHrFat*)'');
    LocCod(Codigo, Codigo);
    //QrLocFCab.Locate('Controle', Controle, []);
    PCMovimento.ActivePageIndex := PCMov_PageIndex_Fatu;
  end;
end;

function TFmLocCConCab.ExcluiItemVendaDeMuitos: Boolean;
var
  SMIA_IDCtrl, Item: Integer;
  sIDCtrl: String;
begin
  Result := False;
  Item        := QrLocCItsVenItem.Value;
  SMIA_IDCtrl := QrLocCItsVenSMIA_IDCtrl.Value;
  //
  if SMIA_IDCtrl <> 0 then
  begin
    sIDCtrl := Geral.FF0(SMIA_IDCtrl);
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovitsa WHERE IDCtrl=' + sIDCtrl);
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovitsb WHERE IDCtrl=' + sIDCtrl);
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovvala WHERE IDCtrl=' + sIDCtrl);
  end;
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM loccitsven WHERE Item=' + Geral.FF0(Item));
  //
  Result := True;
end;

procedure TFmLocCConCab.ExcluiMovimento1Click(Sender: TObject);
var
  CtrID: Integer;
begin
  //Info_Fazer('Fazer 31');
  //Verifica se tem patrim�nios primarios
  if (QrLocIPatPri.State <> dsInactive) and (QrLocIPatPri.RecordCount > 0) then
  begin
    Geral.MB_Aviso(
      'Existe(m) patrim�nio(s) prim�rios cadastradado(s) para esta loca��o!' + sLineBreak +
      'Exclua-o(s) e tente novamente.');
    Exit;
  end;
  //Verifica se tem patrim�nios secundarios
  if (QrLocIPatSec.State <> dsInactive) and (QrLocIPatSec.RecordCount > 0) then
  begin
    Geral.MB_Aviso(
      'Existe(m) patrim�nio(s) secund�rios cadastradado(s) para esta loca��o!' + sLineBreak +
      'Exclua-o(s) e tente novamente.');
    Exit;
  end;
  //Verifica se tem acess�rios
  if (QrLocIPatAce.State <> dsInactive) and (QrLocIPatAce.RecordCount > 0) then
  begin
    Geral.MB_Aviso(
      'Existe(m) acess�rio(s) cadastradado(s) para esta loca��o!' + sLineBreak +
      'Exclua-o(s) e tente novamente.');
    Exit;
  end;
  //Verifica se tem materiais de apoio
  if (QrLocIPatApo.State <> dsInactive) and (QrLocIPatApo.RecordCount > 0) then
  begin
    Geral.MB_Aviso(
      'Existe(m) material(is) de apoio cadastradado(s) para esta loca��o!' +
      sLineBreak + 'Exclua-o(s) e tente novamente.');
    Exit;
  end;
  //Verifica se tem materiais de uso
  if (QrLocIPatUso.State <> dsInactive) and (QrLocIPatUso.RecordCount > 0) then
  begin
    Geral.MB_Aviso(
      'Existe(m) material(is) de uso cadastradado(s) para esta loca��o!' +
      sLineBreak + 'Exclua-o(s) e tente novamente.');
    Exit;
  end;
  //Verifica se tem materiais de consumo
  if (QrLocIPatCns.State <> dsInactive) and (QrLocIPatCns.RecordCount > 0) then
  begin
    Geral.MB_Aviso(
      'Existe(m) material(is) de consumo cadastradado(s) para esta loca��o!' +
      sLineBreak + 'Exclua-o(s) e tente novamente.');
    Exit;
  end;
  // Exclui Movimento
  CtrID := QrLocCConItsCtrID.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do movimento atual?',
  'loccconits', 'CtrID', CtrID, Dmod.MyDB) = ID_YES then
  begin
    RecalculaTudo(False, False);
    ReopenLocCConIts(0);
  end;
end;

procedure TFmLocCConCab.ExcluiMovimentoetodosseusitens1Click(Sender: TObject);
const
  GeraLog = False;
var
  sCtrID: String;
  GGXsToAtz: array of Integer;
  I, GraGruX, Codigo, Status: Integer;
  DtFimCobra, DtHrDevolver: TDateTime;
begin
  if Geral.MB_Pergunta('Confirma a exclus�o de TODOS itens deste movimento?') =
  ID_YES then
  begin
    DtFimCobra := DModG.ObtemAgora();
    DtHrDevolver := QrLocCConCabDtHrDevolver.Value;
    Screen.Cursor := crHourGlass;
    try
      sCtrID := Geral.FF0(QrLocCConItsCtrID.Value);
      Codigo := QrLocCConCabCodigo.Value;
      Status := QrLocCConCabStatus.Value;
      //
      case TTabConLocIts(QrLocCConItsIDTab.Value) of
        //TTabConLocIts.tcliIndefinido: ;
        TTabConLocIts.tcliLocacao:
        begin
          //
          SetLength(GGXsToAtz,
            QrLocIPatPri.RecordCount +
            QrLocIPatSec.RecordCount +
            QrLocIPatAce.RecordCount);
          I := -1;
          //
          //
          QrLocIPatPri.DisableControls;
          try
            QrLocIPatPri.First;
            while not QrLocIPatPri.Eof do
            begin
              I := I + 1;
              GGXsToAtz[I] := QrLocIPatPriGraGruX.Value;
              //
              QrLocIPatPri.Next;
            end;
          finally
            QrLocIPatPri.EnableControls;
          end;
          //
          QrLocIPatSec.DisableControls;
          try
            QrLocIPatSec.First;
            while not QrLocIPatSec.Eof do
            begin
              I := I + 1;
              GGXsToAtz[I] := QrLocIPatSecGraGruX.Value;
              //
              QrLocIPatSec.Next;
            end;
          finally
            QrLocIPatSec.EnableControls;
          end;
          //
          QrLocIPatAce.DisableControls;
          try
            QrLocIPatAce.First;
            while not QrLocIPatAce.Eof do
            begin
              I := I + 1;
              GGXsToAtz[I] := QrLocIPatAceGraGruX.Value;
              //
              QrLocIPatAce.Next;
            end;
          finally
            QrLocIPatAce.EnableControls;
          end;
          //
          try
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM loccitslca WHERE CtrID=' + sCtrID);
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM loccitsruc WHERE CtrID=' + sCtrID);
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM loccitssvc WHERE CtrID=' + sCtrID);
          finally
            for I := 0 to Length(GGXsToAtz) - 1 do
            begin
              GraGruX := GGXsToAtz[I];
              if GraGruX <> 0 then
                AppPF.AtualizaEstoqueGraGXPatr(GraGruX, QrLocCConCabEmpresa.Value, False);
            end;
          end;
          //
          AppPF.AtualizaPendenciasLocacao(QrItsLca, Codigo, Status,
            QrLocCConCabDtHrEmi.Value, DtFimCobra, DtHrDevolver, GeraLog);
        end;
        TTabConLocIts.tcliOutroServico:
        begin
          UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM loccitssvc WHERE CtrID=' + sCtrID);
          AppPF.AtualizaTotaisLocCConCab_Servicos(Codigo);
        end;
        TTabConLocIts.tcliVenda:
        begin
          QrLocCItsVen.DisableControls;
          try
            QrLocCItsVen.First;
            while not QrLocCItsVen.Eof do
            begin
              ExcluiItemVendaDeMuitos();
              //
              QrLocCItsVen.Next;
            end;
          finally
            QrLocCItsVen.EnableControls;
          end;
          AppPF.AtualizaTotaisLocCConCab_Vendas(Codigo);
        end;
      end;
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM loccconits WHERE CtrID=' + sCtrID);
      //excluir stqmovitsa
      if QrLocCConItsFatPedCab.Value <> 0 then
      begin
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovitsa WHERE ' +
        ' Tipo=' + Geral.FF0(VAR_FATID_0002) +
        ' AND OriCodi=' + Geral.FF0(QrLocCConItsFatPedCab.Value) +
        //' AND OriCnta=' + Geral.FF0(QrLocCConItsFatPedVolCnta.Value) +
        ' AND Empresa=' + Geral.FF0(QrLocCConCabEmpresa.Value));
      end;
      //
      LocCod(Codigo, Codigo);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmLocCConCab.ItsExclui2Click(Sender: TObject);
var
  CtrID: Integer;
begin
  Info_Fazer(' 7');
{ Fazer

  CtrID := QrLocCConItsCtrID.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccpatpri, False, ['RetFunci',
    'DtHrRetorn', 'RetExUsr'], ['CtrID'], [0, '', 0], [CtrID], True) then
  begin
    Dmod.VerificaSituacaoPatrimonio(QrLocIPatPriGraGruX.Value);
    ReopenLocIPatPri(QrLocIPatPri, QrLocCConCabCodigo.Value, CtrID);
    //
    UMyMod.ProximoRegistro(QrLocIPatPri, 'CtrID', QrLocCConItsCtrID.Value);
  end;
}
end;

procedure TFmLocCConCab.ItsExclui3Click(Sender: TObject);
var
  CtrID: Integer;
begin
  Info_Fazer(' 8');
{ Fazer

  CtrID := QrLocCConItsCtrID.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccpatpri, False, ['LibFunci',
    'LibDtHr', 'LibExUsr'], ['CtrID'], [0, '', 0], [CtrID], True) then
  begin
    Dmod.VerificaSituacaoPatrimonio(QrLocIPatPriGraGruX.Value);
    ReopenLocIPatPri(QrLocIPatPri, QrLocCConCabCodigo.Value, CtrID);
    //
    UMyMod.ProximoRegistro(QrLocIPatPri, 'CtrID', QrLocCConItsCtrID.Value);
  end;
}
end;

procedure TFmLocCConCab.ImgClienteClick(Sender: TObject);
begin
  Entities.MostraEntiImagens(EdCliente.ValueVariant, EdCliente.ValueVariant, 0, True);
  //
  AtualizaImagens(EdCliente.ValueVariant, 0, ImgCliente);
  AtualizaImagens(EdECComprou.ValueVariant, 1, ImgRequisitante);
  AtualizaImagens(EdECRetirou.ValueVariant, 1, ImgRetirada);
end;

procedure TFmLocCConCab.ImgRequisitanteClick(Sender: TObject);
begin
  Entities.MostraEntiImagens(EdCliente.ValueVariant, EdECComprou.ValueVariant, 1, True);
  //
  AtualizaImagens(EdCliente.ValueVariant, 0, ImgCliente);
  AtualizaImagens(EdECComprou.ValueVariant, 1, ImgRequisitante);
  AtualizaImagens(EdECRetirou.ValueVariant, 1, ImgRetirada);
end;

procedure TFmLocCConCab.ImgRetiradaClick(Sender: TObject);
begin
  Entities.MostraEntiImagens(EdCliente.ValueVariant, EdECRetirou.ValueVariant, 1, True);
  //
  AtualizaImagens(EdCliente.ValueVariant, 0, ImgCliente);
  AtualizaImagens(EdECComprou.ValueVariant, 1, ImgRequisitante);
  AtualizaImagens(EdECRetirou.ValueVariant, 1, ImgRetirada);
end;

procedure TFmLocCConCab.ImgTipoChange(Sender: TObject);
begin
  PnInfoTop.Visible := ImgTipo.SQLType <> stIns;
end;

procedure TFmLocCConCab.ImprimeContrato(Fonte: Integer);
begin
  Application.CreateForm(TFmContratApp2, FmContratApp2);
  //
  FmContratApp2.FLocCConCab := QrLocCConCabCodigo.Value;
  //
  FmContratApp2.ImprimeContrato(Fonte);
  //
  FmContratApp2.Destroy;
end;

procedure TFmLocCConCab.ImprimeDevolucao(Todos: Boolean; Fonte: Integer);
var
  CtrID_Txt: String;
  I, CtrID: Integer;
begin
  //Info_Fazer('Fazer 9');
  if not Todos then
  begin
    if DBGLocIPatPri.SelectedRows.Count > 1 then
    begin
      CtrID_Txt := '';
      //
      with DBGLocIPatPri.DataSource.DataSet do
      begin
        for I := 0 to DBGLocIPatPri.SelectedRows.Count - 1 do
        begin
          //GotoBookmark(pointer(DBGLocIPatPri.SelectedRows.Items[i]));
          GotoBookmark(DBGLocIPatPri.SelectedRows.Items[i]);
          //
          CtrID := QrLocCConItsCtrID.Value;
          //
          if I = DBGLocIPatPri.SelectedRows.Count - 1 then
            CtrID_Txt := CtrID_Txt + Geral.FF0(CtrID)
          else
            CtrID_Txt := CtrID_Txt + Geral.FF0(CtrID) + ', ';
        end;
      end;
    end else
      CtrID_Txt := Geral.FF0(QrLocCConItsCtrID.Value);
  end else
    CtrID_Txt := '';
  //
  Application.CreateForm(TFmContratApp2, FmContratApp2);
  //
  FmContratApp2.FLocCConCab := QrLocCConCabCodigo.Value;
  //
  FmContratApp2.ImprimeDevolucao((*CtrID_Txt*)Todos, Fonte);
  //
  FmContratApp2.Destroy;
end;

procedure TFmLocCConCab.ImprimeFichaDeTroca(Tool: TGraGXToolRnt);
var
  Nome: String;
  //
  procedure DeNo(Patrimonio, Referencia, Descricao: String);
  begin
    if Patrimonio <> '' then
      Nome := Nome + Patrimonio + ' - ';
    if Referencia <> '' then
      Nome := Nome + Referencia + ' - ';
    Nome := Nome + Descricao;
  end;
begin
  Application.CreateForm(TFmContratApp2, FmContratApp2);
  //
  Nome := '';
  FmContratApp2.FLocCConCab := QrLocCConCabCodigo.Value;
  case Tool of
    TGraGXToolRnt.ggxo2gPrincipal:
      DeNo(QrLocIPatPriPATRIMONIO.Value, QrLocIPatPriReferencia.Value, QrLocIPatPriNO_GGX.Value);
    TGraGXToolRnt.ggxo2gSecundario:
      DeNo('', QrLocIPatSecReferencia.Value, QrLocIPatSecNO_GGX.Value);
    TGraGXToolRnt.ggxo2gAcessorio:
      DeNo('', QrLocIPatAceReferencia.Value, QrLocIPatAceNO_GGX.Value);
  end;
  FmContratApp2.FPatrimonioAtual_TXT := Nome;
  //
  FmContratApp2.ImprimeFichaDeTroca();
  //
  FmContratApp2.Destroy;
end;

{
procedure TFmLocCConCab.ImprimeOrcamentoSelecionado(Vias: Integer);
begin
  //
  Application.CreateForm(TFmContratApp2, FmContratApp2);
  //
  FmContratApp2.FLocCConCab := QrLocCConCabCodigo.Value;
  //
  FmContratApp2.ImprimeOrcaPedi(QrLocCConItsCtrID.Value,
    TStatusMovimento.statmovOrcamento, Vias);
  //
  FmContratApp2.Destroy;
end;
}

{
procedure TFmLocCConCab.ImprimeOrcamentoTodos(Vias: Integer);
const
  Todos = 0;
begin
  //
  Application.CreateForm(TFmContratApp2, FmContratApp2);
  //
  FmContratApp2.FLocCConCab := QrLocCConCabCodigo.Value;
  //
  //FmContratApp2.ImprimeOrcaPediTodos(TStatusMovimento.statmovOrcamento, Vias);
  FmContratApp2.ImprimeOrcaPedi(Todos, TStatusMovimento.statmovOrcamento, Vias);
  //
  FmContratApp2.Destroy;
end;
}
procedure TFmLocCConCab.ImprimeOrcaPedi(CtrID: Integer;
  StatusMovimento: TStatusMovimento; Vias: Integer; ImpDesco: Boolean);
begin
  Application.CreateForm(TFmContratApp2, FmContratApp2);
  FmContratApp2.FLocCConCab := QrLocCConCabCodigo.Value;
  FmContratApp2.ImprimeOrcaPedi(CtrID, StatusMovimento, Vias, ImpDesco);
  FmContratApp2.Destroy;
end;

{
procedure TFmLocCConCab.ImprimePedidoTodos(Vias: Integer);
const
  Todos = 0;
begin
  //
  Application.CreateForm(TFmContratApp2, FmContratApp2);
  //
  FmContratApp2.FLocCConCab := QrLocCConCabCodigo.Value;
  //
  //FmContratApp2.ImprimeOrcaPediTodos(TStatusMovimento.statmovPedido, Vias);
  FmContratApp2.ImprimeOrcaPedi(Todos, TStatusMovimento.statmovPedido, Vias);
  //
  FmContratApp2.Destroy;
end;
}

procedure TFmLocCConCab.Imprimerecibo1Click(Sender: TObject);
const
  DBGLct = nil;
  ModuleLctX = nil;
var
  FatID, LocFCab, Emitente, Beneficiario: Integer;
  ValorPago: Double;
  TxtHist: String;
begin
{$IFNDEF NAO_USA_RECIBO}
{
  //if TDmLct2(FModuleLctX).QrLctCredito.Value > 0 then
    GOTOy.EmiteRecibo(QrLocCConCabCliente.Value,
    QrLocCConCabEmpresa.Value,
    QrLocFCabValTotal.Value, 0, 0, 'ALSV#' +
    Geral.FF0(QrLocCConCabCodigo.Value) + '-' +
    IntToStr(QrLocFCabControle.Value),
    '? ? ? ? ? ? ?', '', '',
    QrLocFCabDtHrFat.Value,
    (*Sit*)4)
}
  if QrLocFCabReciboImp.Value > 0 then
    FinanceiroJan.MostraFormReciboImpCab(QrLocFCabReciboImp.Value)
  else
  begin
    FatID        := VAR_FATID_3001;
    LocFCab      := QrLocFCabControle.Value;
    Emitente     := QrLocCConCabCliente.Value;
    Beneficiario := QrLocCConCabEmpresa.Value;
    ValorPago    := QrLocFCabValTotal.Value;
    //
    TxtHist        := Trim(AppPF.TextoDeCodHist_Descricao(QrLocFCabCodHist.Value));
    //
    DModFin.GeraReciboImpCabLocacao(
      FatID, LocFCab, Emitente, Beneficiario, ValorPago, TxtHist);
    //
    ReopenLocFCab(QrLocFCabControle.Value);
  end;
{$Else}
  Geral.MB_Aviso('M�dulo de Recibo desabilitado!');
{$EndIf}

end;

procedure TFmLocCConCab.IncluiMovimento1Click(Sender: TObject);
begin
  MostraFormLocCConIts(stIns, TTabConLocIts.tcliIndefinido);
end;

procedure TFmLocCConCab.Indefinido1Click(Sender: TObject);
begin
  ImprimeFichaDeTroca(TGraGXToolRnt.ggxoIndef);
end;

procedure TFmLocCConCab.Info_Fazer(Txt: String);
const
  sProcFunc = 'TFmLocCConCab.Info_Fazer()';
begin
  Geral.MB_Info('Procedimento/funcionalidade em fase de desenvolvimento ID: ' +
  Txt + sLineBreak + sProcFunc);
end;

{
procedure TFmLocCConCab.InsereLocCConIts(TabLocIts: TTabLocIts);
const
  sProcName = 'TFmLocCConCab.InsereLocCConIts()';
var
  Codigo, CtrID, IDTab, Selecionado: Integer;
  SQLType: TSQLType;
  MyTabLocIts: TTabLocIts;
begin
  if TTabLocIts(TabLocIts) = TTabLocIts.tliIndefinido then
  begin
    Selecionado := MyObjects.SelRadioGroup('Movimento', 'Informe o tipo de movimento',
    ['Loca��o', 'Servi�o', 'Venda'], 1,  -1, (*SelOnClick*)True);
    case Selecionado of
      0: MyTabLocIts := TTabLocIts.tliLocacaoComEstq;
      1: MyTabLocIts := TTabLocIts.tliOutroServico;
      2: MyTabLocIts := TTabLocIts.tliVenda;
    end;
  end else
    MyTabLocIts := TabLocIts;
  //
  if TTabLocIts(TabLocIts) = TTabLocIts.tliIndefinido then
    Exit;
  //
  SQLType        := stIns;
  Codigo         := QrLocCConCabCodigo.Value;
  CtrID          := 0;
  IDTab          := Integer(TabLocIts);
  // Fazer 27 ver aqui se j� n�o h� em sem movimento!
  //
  //
  //
  CtrID := UMyMod.BPGS1I32('loccconits', 'CtrID', '', '', tsPos, SQLType, CtrID);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconits', False, [
  'Codigo', 'IDTab'], [
  'CtrID'], [
  Codigo, IDTab], [
  CtrID], True) then
  begin
    ReopenLocCConIts(CtrID);
    if QrLocCConItsCtrID.Value = CtrID then
    begin
      case TTabLocIts(TabLocIts) of
        TTabLocIts.tliIndefinido: ;
        TTabLocIts.tliLocacaoComEstq,
        TTabLocIts.tliLocacaoSemEstq:
        begin
          MostraFormLocCItsLca(stIns);
        end;
        TTabLocIts.tliOutroServico:
        begin
          MostraFormLocCItsSvc(stIns, Codigo, CTrID, (*Item*)0);
        end;
        TTabLocIts.tliVenda:
        begin
          MostraFormLocCItsVen(stIns, Codigo, CTrID, (*Item*)0);
        end;
        else Geral.MB_Erro('Movimento n�o implementado em ' + sProcName);
      end;
    end else
      Geral.MB_Erro('ERRO ao localizar movimento criado!');
  end;
end;
}

procedure TFmLocCConCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormLocCItsLca(stUpd);
end;

procedure TFmLocCConCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  //Verifica se tem Movimento
  if (QrLocCConIts.State <> dsInactive) and (QrLocCConIts.RecordCount > 0) then
  begin
    Geral.MB_Aviso(
      'Existe(m) movimento(para esta loca��o!' + sLineBreak +
      'Exclua-o(s) e tente novamente.');
    Exit;
  end;
  //Verifica se tem faturamento
  if (QrLocFCab.State <> dsInactive) and (QrLocFCab.RecordCount > 0) then
  begin
    if Geral.MensagemBox('Existe faturamento cadastradado para esta loca��o!' +
      sLineBreak + 'Deseja excluir este item assim mesmo?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES
    then
      Exit;
  end;
  Codigo := QrLocCConCabCodigo.Value;
  //
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da loca��o atual?',
  'loccconcab', 'Codigo', Codigo, Dmod.MyDB) = ID_YES then
  begin
    Va(vpLast);
  end;
end;

procedure TFmLocCConCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

{
procedure TFmLocCConCab.QuantidadeDeDiasFormaAluguel();
const
  sProcName = 'QuantidadeDeDiasFormaAluguel()';
var
  DataDev: TDateTime;
  //
  //DIni, DFim, DiasA, DiaSem,
  Partes: Integer;
  DiaInutil: Integer;
  UsarHorarioPreDefinido: Boolean;
  Hora, Parte, Min15: TDateTime;
begin
  DataDev := Int(TPDtHrSai.Date);
  case RGTipoAluguel.ItemIndex of
    (* *)0: ; // nada
    (*D*)1: DataDev := DataDev + 1;
    (*S*)2: DataDev := DataDev + 7;
    (*Q*)3: DataDev := DataDev + 15;
    (*M*)4:
    begin
      DataDev := IncMonth(DataDev);
    end;
    else
    begin
      Geral.MB_Erro('Periodicidade n�o implementada! ' + sProcName);
      DataDev := DataDev + 1;
    end;
  end;
  UsarHorarioPreDefinido := False;
  DiaInutil := UMyMod.DiaInutil(DataDev);
  while DiaInutil <> 0 do
  begin
    UsarHorarioPreDefinido := True;
    case DiaInutil of
      201: (*Domingo*) DataDev := DataDev + 1;
      207: (*S�bado *) DataDev := DataDev + 2;
      208: (*Feriado*) DataDev := DataDev + 1;
    end;
    DiaInutil := UMyMod.DiaInutil(DataDev);
  end;
  //
  TPDtHrDevolver.Date := DataDev;
  if UsarHorarioPreDefinido then
  begin
    EdDtHrDevolver.ValueVariant := DMod.QrOpcoesTRenHrLimPosDiaNaoUtil.Value;
  end else
  begin
    Hora := EDDtHrSai.ValueVariant;
(*     Precisa fazer?
    Parte := (Dmod.QrOpcoesTRenLocArredMinut.Value /60/60/24);
    Min15 := (15/60/60/24);
    Hora := Hora + Min15;
    Partes := Trunc(Hora / Parte);
    Hora := Partes * 60 * 60 * 24;
    if Hora > Dmod.QrOpcoesTRenLocArredHrIniFim.Value then
      Hora := Dmod.QrOpcoesTRenLocArredHrIniFim.Value ;
*)
    EdDtHrDevolver.ValueVariant := Hora;
  end;
end;
}

procedure TFmLocCConCab.QuantidadeDeDiasFormaAluguel(var DataDev, HoraDev: TDateTime);
const
  sProcName = 'QuantidadeDeDiasFormaAluguel()';
var
  //DataDev: TDateTime;
  //
  Partes: Integer;
  DiaInutil: Integer;
  UsarHorarioPreDefinido: Boolean;
  //Hora,
  Parte, Min15: TDateTime;
begin
  HoraDev := EdDtHrSai.ValueVariant;
  DataDev := Int(TPDtHrSai.Date);
  //
  case RGTipoAluguel.ItemIndex of
    (* *)0: ; // nada
    (*D*)1: DataDev := DataDev + 1;
    (*S*)2: DataDev := DataDev + 7;
    (*Q*)3: DataDev := DataDev + 15;
    (*M*)4:
    begin
      DataDev := IncMonth(DataDev);
    end;
    else
    begin
      Geral.MB_Erro('Periodicidade n�o implementada! ' + sProcName);
      DataDev := DataDev + 1;
    end;
  end;
  UsarHorarioPreDefinido := False;
  DiaInutil := UMyMod.DiaInutil(DataDev);
  while DiaInutil <> 0 do
  begin
    UsarHorarioPreDefinido := True;
    case DiaInutil of
      201: (*Domingo*) DataDev := DataDev + 1;
      207: (*S�bado *) DataDev := DataDev + 2;
      208: (*Feriado*) DataDev := DataDev + 1;
    end;
    DiaInutil := UMyMod.DiaInutil(DataDev);
  end;
  //
  //TPDtHrDevolver.Date := DataDev;
  if UsarHorarioPreDefinido then
  begin
    //EdDtHrDevolver.ValueVariant := DMod.QrOpcoesTRenHrLimPosDiaNaoUtil.Value;
    HoraDev := DMod.QrOpcoesTRenHrLimPosDiaNaoUtil.Value;
  end else
  begin
(*     Precisa fazer?
    Parte := (Dmod.QrOpcoesTRenLocArredMinut.Value /60/60/24);
    Min15 := (15/60/60/24);
    Hora := Hora + Min15;
    Partes := Trunc(Hora / Parte);
    Hora := Partes * 60 * 60 * 24;
    if Hora > Dmod.QrOpcoesTRenLocArredHrIniFim.Value then
      Hora := Dmod.QrOpcoesTRenLocArredHrIniFim.Value ;
*)
    //EdDtHrDevolver.ValueVariant := Hora;
  end;
end;

procedure TFmLocCConCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmLocCConCab.ItsExclui1Click(Sender: TObject);
var
  Item, GraGruX: Integer;
begin
   //Info_Fazer(' 10');
(*
  //Verifica se tem secund�rios
  if (QrLocIPatSec.State <> dsInactive) and (QrLocIPatSec.RecordCount > 0) then
  begin
    Geral.MB_Aviso(
      'Existe(m) equipamentos secund�rio(s) cadastradado(s) para esta loca��o!' + sLineBreak +
      'Exclua-o(s) e tente novamente.');
    Exit;
  end;
  //Verifica se tem acess�rios
  if (QrLocIPatAce.State <> dsInactive) and (QrLocIPatAce.RecordCount > 0) then
  begin
    Geral.MB_Aviso(
      'Existe(m) acess�rio(s) cadastradado(s) para esta loca��o!' + sLineBreak +
      'Exclua-o(s) e tente novamente.');
    Exit;
  end;
  //Verifica se tem materiais de uso
  if (QrLocIPatUso.State <> dsInactive) and (QrLocIPatUso.RecordCount > 0) then
  begin
    Geral.MB_Aviso(
      'Existe(m) material(is) de uso cadastradado(s) para esta loca��o!' +
      sLineBreak + 'Exclua-o(s) e tente novamente.');
    Exit;
  end;
  //Verifica se tem materiais de consumo
  if (QrLocIPatCns.State <> dsInactive) and (QrLocIPatCns.RecordCount > 0) then
  begin
    Geral.MB_Aviso(
      'Existe(m) material(is) de consumo cadastradado(s) para esta loca��o!' +
      sLineBreak + 'Exclua-o(s) e tente novamente.');
    Exit;
  end;
  //
*)
  if (QrLocFCab.State <> dsInactive) and (QrLocFCab.RecordCount > 0) then
  begin
    if Geral.MB_Pergunta('Existe faturamento cadastradado para esta loca��o!' +
      sLineBreak + 'Deseja excluir este item assim mesmo?') <> ID_YES
    then
      Exit;
  end;
  GraGruX := QrLocIPatPriGraGruX.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  //'LocCPatPri', 'CtrID', QrLocCConItsCtrID.Value, Dmod.MyDB) = ID_YES then
  'LocCItsLca', 'Item', QrLocIPatPriItem.Value, Dmod.MyDB) = ID_YES then
  begin
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, QrLocCConCabEmpresa.Value, True);
    ///
    ///  Deprecado!!!
{
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragxpatr', False, [
    'Situacao'], ['GraGruX'], [CO_SIT_PATR_068_D_Disponivel],
    [GraGruX], True);
}
    //
    Item := GOTOy.LocalizaPriorNextIntQr(QrLocIPatPri,
      QrLocIPatPriItem, QrLocIPatPriItem.Value);
    ReopenLocIPatPri(QrLocIPatPri, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, Item);
  end;
end;

procedure TFmLocCConCab.Reabrelocao1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrLocCConCabCodigo.Value;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconcab', False, [
  'DtHrBxa'], ['Codigo'], [''], [Codigo], True);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmLocCConCab.RecalculaDataProgramadaRetorno();
var
  DIni, DFim, DiasA, DiaSem, Partes: Integer;
  DiaInutil: Integer;
  UsarHorarioPreDefinido: Boolean;
  Hora, Parte, Min15: TDateTime;
begin
{ Ver o que fazer!!
  DIni    := Trunc(TPDtHrSai.Date);

  if RGTipoAluguel.ItemIndex = 0 then
  begin
    0:
  case RGTipoAluguel.ItemIndex of
    0:
  end;
  DFim    := Trunc(TPDtHrDevolver.Date);
  DiasA   := DFim - DIni;
  UsarHorarioPreDefinido := False;
  DiaInutil := UMyMod.DiaInutil(DFim);
  while DiaInutil <> 0 do
  begin
    UsarHorarioPreDefinido := True;
    case DiaInutil of
      201: (*Domingo*) DFim := DFim + 1;
      207: (*S�bado *) DFim := DFim + 2;
      208: (*Feriado*) DFim := DFim + 1;
    end;
    DiaInutil := UMyMod.DiaInutil(DFim);
  end;
  //
  TPDtHrDevolver.Date := DFim;
  if UsarHorarioPreDefinido then
  begin
    EdDtHrDevolver.ValueVariant := DMod.QrOpcoesTRenHrLimPosDiaNaoUtil.Value;
  end else
  begin
    Hora := EDDtHrSai.ValueVariant;
    Parte := (Dmod.QrOpcoesTRenLocArredMinut.Value /60/60/24);
    Min15 := (15/60/60/24);
    Hora := Hora + Min15;
    Partes := Trunc(Hora / Parte);
    Hora := Partes * 60 * 60 * 24;
    if Hora > Dmod.QrOpcoesTRenLocArredHrIniFim.Value then
      Hora := Dmod.QrOpcoesTRenLocArredHrIniFim.Value ;
    EdDtHrDevolver.ValueVariant := Hora;
  end;
}
end;

procedure TFmLocCConCab.RecalculaEstoquesPatr(var ItensNegativos: Integer);
var
  Codigo, GraGruX: Integer;
  GGXs: array of Integer;
  sGGXs: String;
begin
  ItensNegativos := 0;
  Codigo := QrLocCConCabCodigo.Value;
  //
  SetLength(GGXs, QrItsLca.RecordCount);
  QrItsLca.First;
  while not QrItsLca.Eof do
  begin
    GraGruX := QrItsLcaGraGruX.Value;
    GGXs[QrItsLca.RecNo -1] := GraGruX;
    if not
      AppPF.AtualizaEstoqueGraGXPatr(GraGruX, QrLocCConCabEmpresa.Value, True)
    then
      ItensNegativos := ItensNegativos + 1;
    //
    QrItsLca.Next;
  end;
  AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
  //
  //Info_Fazer('Ver aqui estoques ItensNegativos!');
  sGGXs := MyObjects.CordaDeArrayInt(GGXs);
  if sGGXs <> EmptyStr then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrGGXEPatNeg, Dmod.MyDB, [
    'SELECT gg1.Nome, gg1.Patrimonio, gg1.Referencia, ',
    'gep.*  ',
    'FROM gragruepat gep ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=gep.GraGrux ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE GraGruX IN ( ',
    sGGXs,
    ') ',
    'AND Empresa=' + Geral.FF0(QrLocCConCabEmpresa.Value),
    'AND EstqSdo < 0 ',
    '']);
    if Dmod.QrGGXEPatNeg.RecordCount > 0 then
      GraL_Jan.MostraFormGraGruEPatNeg();
  end;
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmLocCConCab.RecalculaEstoquesVend(var ItensNegativos: Integer);
var
  Codigo, Empresa, FatPedCab, NF_Stat, CabStat, ItsStat: Integer;
begin
  ItensNegativos := 0;
  //
  Codigo    := QrLocCConCabCodigo.Value;
  Empresa   := QrLocCConCabEmpresa.Value;
  CabStat   := QrLocCConCabStatus.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocIts, Dmod.MyDB, [
  'SELECT CtrID, FatPedCab, NF_Stat, Status ',
  'FROM loccconits ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND IDTab=' + Geral.FF0(Integer(TTabConLocIts.tcliVenda)),
  '']);
  QrLocIts.First;
  while not QrLocIts.Eof do
  begin
    FatPedCab := QrLocItsFatPedCab.Value;
    NF_Stat   := QrLocItsNF_Stat.Value;
    ItsStat   := QrLocItsStatus.Value;
    //
    AppPF.VerificaEstoqueVenda(Empresa, FatPedCab, NF_STat, CabStat, ItsStat);
    //
    QrLocIts.Next;
  end;
end;

{
function TFmLocCConCab.AppPF.RecalculaItens_Datas(QrItsLca, Codigo, StatusLocacao: Integer; QualID: Variant;
  GeraLog: Boolean): Boolean;
var
  DtHrDevolver: Integer;
  //
  Item: Integer;
  DMenos, DiasFDS, DiasUteis: Integer;
  SaiDtHr, CobranIniDtHr, CalcAdiLOCACAO, sCodigo, sValorAtz: String;
  Saida, Volta: TDateTime;
  IniCobranca: TDateTime;
  Log: String;
  ValorFDS, ValorLocAntigo, ValorUnitario, ValDevolParci, ValorLocAAdiantar,
  SOMA_ValLocAtz: Double;
  SQLType: TSQLType;
  SQL_CtrID: String;
  CtrID: Integer;
  Calculou: Boolean;
  Troca: Byte;
  //
  procedure CalculaDadosAtual();
  //const
    //GeraLog = True;
  var
    ValorDia, ValorSem, ValorQui, ValorMes, QtdeLocacao, QtdeProduto,
    QtdeDevolucao: Double;
  begin
    Screen.Cursor := crHourGlass;
    try
      SQLType  := stUpd;
      //Volta    := QrLocCConCabDtHrDevolver;
      //
      DMenos         := QrItsLcaDMenos.Value;
      //
      ValorDia       := QrItsLcaValorDia.Value;
      ValorSem       := QrItsLcaValorSem.Value;
      ValorQui       := QrItsLcaValorQui.Value;
      ValorMes       := QrItsLcaValorMes.Value;
      ValorFDS       := QrItsLcaValorFDS.Value;
      QtdeLocacao    := QrItsLcaQtdeLocacao.Value;
      QtdeProduto    := QrItsLcaQtdeProduto.Value;
      QtdeDevolucao  := QrItsLcaQtdeDevolucao.Value;
      ValorLocAntigo := QrItsLcaValorLocAtualizado.Value;
      ValDevolParci  := QrItsLcaValDevolParci.Value;
      Troca          := QrItsLcaTroca.Value;
      //
      //ValorFDS := 0;
      // For�ar defini��o da data inicial de cobran�a, pois ainda n�o foi definida!
      IniCobranca := 0;
      //
      DiasFDS            := 0;
      DiasUteis          := 0;
      CalcAdiLOCACAO     := '';
      ValorLocAAdiantar  := 0.00;
          //
      AppPF.CalculaDadosEntreDatasLocacao_COM_CobrarExtraFDS((*const*) GeraLog,
      (*const*) Saida, Volta, (*const*) DMenos, (*cons*)ValorDia, ValorSem,
      ValorQui, ValorMes, ValorFDS, QtdeLocacao, QtdeProduto, QtdeDevolucao,
      ValDevolParci, Troca, (*var*) Log, (*var*)IniCobranca, (*var*)DiasFDS,
      DiasUteis, (*var*)CalcAdiLOCACAO,
      ValorUnitario, ValorLocAAdiantar, Calculou);

      //
      Geral.MB_Info(Log);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
var
  Continua: Boolean;
begin
  Result := False;
  //
  case TStatusLocacao(StatusLocacao) of
    TStatusLocacao.statlocIndefinido,
    TStatusLocacao.statlocFinalizado,
    TStatusLocacao.statlocCancelado: Continua := False;
    TStatusLocacao.statlocLocado,
    TStatusLocacao.statlocAberto: Continua := True;
  end;
  if not Continua then
    Exit;
  SQLType           := stUpd;
  Screen.Cursor     := crHourGlass;
  try
    SOMA_ValLocAtz  := 0;
    //
    if QualID <> Null then
    begin
      CtrID     := Integer(QualID);
      SQL_CtrID := 'AND CtrID=' + Geral.FF0(CtrID);
    end else
      SQL_CtrID := '';

    UnDmkDAC_PF.AbreMySQLQuery0(QrItsLca, Dmod.MyDB, [
    'SELECT *  ',
    'FROM loccitslca ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    SQL_CtrID,
    'ORDER BY CtrID, Item ',
    '']);
    //
    if QrItsLca.RecordCount > 0 then
    begin
      Saida := QrLocCConCabDtHrSai.Value;
      Volta := Trunc(QrLOcCConCabDtHrDevolver.Value);
      //
      QrItsLca.First;
      while not QrItsLca.Eof do
      begin
        //manelcaPrincipal=1,
        //manelcaSecundario=2,
        //manelcaAcessorio=3
        if QrItsLcaManejoLca.Value in ([1,2,3]) then
        begin
          Item := QrItsLcaItem.Value;
          //
          CalculaDadosAtual();
          //
          if Calculou then
          begin
            CobranIniDtHr := Geral.FDT(IniCobranca, 109);
            SaiDtHr       := Geral.FDT(Saida, 109);

            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitslca', False, [
            'CobranIniDtHr', 'SaiDtHr',
            'ValorFDS', 'DMenos', 'ValorLocAAdiantar',
            'CalcAdiLOCACAO', 'DiasFDS', 'DiasUteis'], [
            'Item'], [
            CobranIniDtHr, SaiDtHr,
            ValorFDS, DMenos, ValorLocAAdiantar,
            CalcAdiLOCACAO, DiasFDS, DiasUteis], [
            Item], True) then
              SOMA_ValLocAtz := SOMA_ValLocAtz + ValorLocAAdiantar;
          end else
            SOMA_ValLocAtz := SOMA_ValLocAtz + ValorLocAntigo;
        end;
        //
        QrItsLca.Next;
      end;
    end;
    //
    //
    if Trunc(DModG.ObtemAgora()) <= Trunc(QrLocCConCabDtHrSai.Value) then
    begin
      sCodigo   := Geral.FF0(Codigo);
      sValorAtz := Geral.FFt_Dot(SOMA_ValLocAtz, 2, siNegativo);
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'UPDATE loccconcab SET ValorLocAAtualizar=' + sValorAtz +
      ', ValorLocAtualizado=' + sValorAtz +
      //', DtUltAtzPend="'+ Geral.FDT(FimCobra, 1) + '" ' + precisa ???
      ' WHERE Codigo=' + sCodigo + '; ');
    end else
    begin
      AppPF.AtualizaPendenciasLocacao(QrItsLca, Codigo, StatusLocacao, QrLocCConCabDtHrEmi.Value,
        GeraLog);
    end;
  //
    Result := True;
    //
    // N�o fechar QrItsLca! Poder� usado para recalcular estoque
    //
    LocCod(Codigo, Codigo);
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmLocCConCab.RecalculaTudo(GeraLog, OutraData: Boolean; Redefine: Boolean);
var
  Codigo, Status: Integer;
  DtFimCobra: TDateTime;
begin
  FCalculandoManual := True;
  try
    DtFimCobra := DModG.ObtemAgora();
    if OutraData then
    begin
      if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
        Exit;
      DBCheck.ObtemData(DtFimCobra, DtFimCobra, DtFimCobra,
      QrLocCConCabDtHrSai.Value, True, 'Outra Data');
    end;
    //
    Codigo := QrLocCConCabCodigo.Value;
    Status := QrLocCConCabStatus.Value;
    if AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, (*CtrID*)0,
    QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
    QrLocCConCabDtHrDevolver.Value, DtFimCobra, GeraLog, Redefine) then
      AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
    AppPF.AtualizaTotaisLocCConCab_Servicos(Codigo);
    AppPF.AtualizaTotaisLocCConCab_Vendas(Codigo);
    //
    LocCod(Codigo, Codigo);
  finally
    FCalculandoManual := False;
  end;
end;

procedure TFmLocCConCab.Redefinir1Click(Sender: TObject);
begin
  MostraFormLocCRedef();
end;

procedure TFmLocCConCab.Removeacessrio1Click(Sender: TObject);
var
  Codigo, Item, GraGruX, CtrID: Integer;
begin
{
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrLocIPatAce, DBGLocIPatAce,
    'loccitslca', ['Item'], ['Item'], istPergunta, '');
  begin
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, QrLocCConCabEmpresa.Value, True);
  end;
}
  // Exclui Acess�rio
  // Fazer um por um! Controla estoque!
  Codigo   := QrLocCConCabCodigo.Value;
  CtrID    := QrLocCConItsCtrID.Value;
  Item     := QrLocIPatAceItem.Value;
  GraGruX  := QrLocIPatAceGraGruX.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do acess�rio atual?',
  'loccitslca', 'Item', Item, Dmod.MyDB) = ID_YES then
  begin
    AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, QrLocCConCabEmpresa.Value, True);
    LocCod(Codigo, Codigo);
    QrLocCConIts.Locate('CtrID', CtrID, []);
    ReopenLocIPatAce(QrLocIPatAce, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
  end;
end;

procedure TFmLocCConCab.RemoveequipamentoSecundrio1Click(Sender: TObject);
var
  Item, GraGruX, CtrID: Integer;
var
  Codigo: Integer;
begin
  Codigo := QRLocCConCabCodigo.Value;
  CtrID := QrLocCConItsCtrID.Value;
  //
  //Info_Fazer(' 30');
{  Fazer um por um! Controla estoque!
  if DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrLocIPatSec, DBGLocIPatSec,
    'loccitslca', ['Item'], ['Item'], istPergunta, '') then
  begin
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, QrLocCConCabEmpresa.Value, True);
  end;
}
  // Exclui Patrim�nio Secund�rio
  Item    := QrLocIPatSecItem.Value;
  GraGruX := QrLocIPatSecGraGruX.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do equipamento secund�rio selecionado?',
  'loccitslca', 'Item', Item, Dmod.MyDB) = ID_YES then
  begin
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, QrLocCConCabEmpresa.Value, True);
    AppPF.AtualizaTotaisLocCConCab_Servicos(Codigo);
    //
    LocCod(Codigo, Codigo);
    QrLocCConIts.Locate('CtrID', CtrID, []);
    ReopenLocIPatSec(QrLocIPatSec, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
  end;
end;

procedure TFmLocCConCab.RemovematerialdeApoio1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrLocIPatApo, DBGLocIPatApo,
    'loccitsruc', ['Item'], ['Item'], istPergunta, '');
end;

procedure TFmLocCConCab.Removematerialdeconsumo1Click(Sender: TObject);
var
  Codigo, CtrID: Integer;
begin
  Codigo := QRLocCConCabCodigo.Value;
  CtrID := QrLocCConItsCtrID.Value;
  //
  if DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrLocIPatCns, DBGLocIPatCns,
  'loccitsruc', ['Item'], ['Item'], istPergunta, '') > 0 then
  begin
    AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
    LocCod(Codigo, Codigo);
    QrLocCConIts.Locate('CtrID', CtrID, []);
  end;
end;

procedure TFmLocCConCab.Removematerialuso1Click(Sender: TObject);
var
  Codigo, CtrID: Integer;
begin
  Codigo := QRLocCConCabCodigo.Value;
  CtrID := QrLocCConItsCtrID.Value;
  //
  if DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrLocIPatUso, DBGLocIPatUso,
  'loccitsruc', ['Item'], ['Item'], istPergunta, '') > 0 then
  begin
    AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
    LocCod(Codigo, Codigo);
    QrLocCConIts.Locate('CtrID', CtrID, []);
  end;
end;

procedure TFmLocCConCab.RemoveServico1Click(Sender: TObject);
var
  Codigo, CtrID: Integer;
begin
  Codigo := QRLocCConCabCodigo.Value;
  CtrID := QrLocCConItsCtrID.Value;
  //
  if DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrLocCItsSvc, DBLocCItsSvc,
  'loccitssvc', ['Item'], ['Item'], istPergunta, '') > 0 then
  begin
    AppPF.AtualizaTotaisLocCConCab_Servicos(Codigo);
    LocCod(Codigo, Codigo);
    QrLocCConIts.Locate('CtrID', CtrID, []);
  end;
end;

procedure TFmLocCConCab.Removevenda1Click(Sender: TObject);
var
  q: TSelType;
  Codigo, CtrID, SMIA_IDCtrl: Integer;
  sIDCtrl: String;
begin
  if MovimentoPossuiNFAutorizada(True) then Exit;
  //
  Codigo      := QrLocCConCabCodigo.Value;
  CtrID       := QrLocCConItsCtrID.Value;
  if DBCheck.Quais_Selecionou(QrLocCItsVen, DBGLocCItsVen, q) then
  begin
    case q of
      TSelType.istAtual,
      TSelType.istSelecionados:
      begin
        ExcluiItemVendaDeMuitos();
      end;
      TSelType.istTodos:
      begin
        QrLocCItsVen.DisableControls;
        try
          QrLocCItsVen.First;
          while not QrLocCItsVen.Eof do
          begin
            ExcluiItemVendaDeMuitos();
            //
            QrLocCItsVen.Next;
          end;
        finally
          QrLocCItsVen.EnableControls;
        end;
      end;
    end;
    AppPF.AtualizaTotaisLocCConCab_Vendas(Codigo);
    LocCod(Codigo, Codigo);
    QrLocCConIts.Locate('CtrID', CtrID, []);
  end;
end;

procedure TFmLocCConCab.Renovar1Click(Sender: TObject);
var
  Codigo: Integer;
  MinData, Data, Hora: TDateTime;
  DtHrDevolver: String;
  SQLType: TSQLType;
begin
  if TStatusLocacao(QrLocCConCabStatus.Value) = TStatusLocacao.statlocLocado then
  begin
    //if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
      //Exit;
    SQLTYpe := stUpd;
    Codigo  := QrLocCConCabCodigo.Value;
    MinData := QrLocCConCabDtHrSai.Value;
    Data    := QrLocCConCabDtHrDevolver.Value;
    Hora    := Data - Trunc(Data);
    //
    if DBCheck.ObtemData(Data, Data, MinData, Hora, True,
    'Nova data prevista para devolu��o') then
    begin
      DtHrDevolver := Geral.FDT(Trunc(Data) + Hora, 109);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconcab', False,[
      'DtHrDevolver'], ['Codigo'], [DtHrDevolver], [Codigo], True) then
      begin
        RecalculaTudo(False, False);
        //
        LocCod(Codigo, Codigo);
      end;
    end;
  end;
end;

procedure TFmLocCConCab.ReopenECComprou(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrECComprou, Dmod.MyDB, [
  'SELECT Controle, Nome ',
  'FROM enticontat ',
  'WHERE Codigo=' + Geral.FF0(EdCliente.ValueVariant),
  'ORDER BY Nome ',
  '']);
  //
  QrECComprou.Locate('Controle', Controle, []);
  //
  EdECComprou.ValueVariant := 0;
  CBECComprou.KeyValue := 0;
end;

procedure TFmLocCConCab.ReopenECRetirou(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrECRetirou, Dmod.MyDB, [
  'SELECT Controle, Nome ',
  'FROM enticontat ',
  'WHERE Codigo=' + Geral.FF0(EdCliente.ValueVariant),
  'ORDER BY Nome ',
  '']);
  //
  QrECRetirou.Locate('Controle', Controle, []);
  //
  EdECRetirou.ValueVariant := 0;
  CBECRetirou.KeyValue := 0;
end;

procedure TFmLocCConCab.ReopenEndContratante();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEndContratante, Dmod.MyDB, [
  'SELECT mun.Nome NO_CIDADE, pai.Nome NO_PAIS, ent.Codigo, ent.Tipo,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATANTE,',
  'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,',
  'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,',
  'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,',
  'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,',
  'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,',
  '/*IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,*/',
  'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,',
  'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,',
  'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,',
  'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,',
  'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX,',
  'IF(ent.Tipo=0, ent.ECep, ent.PCEP) + 0.000 CEP',
  'FROM entidades ent',
  'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF',
  'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF',
  'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd',
  'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici)',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais)',
  'WHERE ent.Codigo=' + Geral.FF0(QrLocCConCabCliente.Value),
  'ORDER BY CONTRATANTE',
  '']);
end;

procedure TFmLocCConCab.ReopenEntiStatus(Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiStatus, Dmod.MyDB, [
  'SELECT IF(est.Status > 0, "SIM", "N�O")',
  'SELECI, ste.Codigo Status, ste.Nome NO_Status',
  'FROM statusenti ste ',
  'LEFT JOIN entiStatus est ON ste.Codigo=est.Status ',
  //'AND est.Codigo=' + Geral.FF0(Entidade),
  'WHERE est.Codigo=' + Geral.FF0(Entidade),
  'ORDER BY NO_Status ',
  ' ']);
end;

procedure TFmLocCConCab.ReopenLocCItsSvc(QueryLocCItsSvc: TmySQLQuery; Codigo,
  CtrID, Item: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocCItsSvc, Dmod.MyDB, [
  'SELECT ELT(lpp.Atrelamento+1, "Indefinido", ',
  '"Nenhum", "Loca��o", "Venda", "? ? ?") ',
  'NO_Atrelamento, ',
  'lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA',
  'FROM loccitssvc lpp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE lpp.Codigo=' + Geral.FF0(Codigo),
  'AND lpp.CtrID=' + Geral.FF0(CtrID),
  'AND lpp.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaServico)), // 7=Servico
  '']);
  //
  QueryLocCItsSvc.Locate('Item', Item, []);
end;

procedure TFmLocCConCab.ReopenLocCItsVen(QueryLocCItsVen: TmySQLQuery; Codigo,
  CtrID, Item: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocCItsVen, Dmod.MyDB, [
  'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,',
  'ggx.GraGru1, med.SIGLA ',
  'FROM loccitsven lpc ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade',
  'WHERE lpc.Codigo=' + Geral.FF0(Codigo),
  'AND lpc.CtrID=' + Geral.FF0(CtrID),
  'AND lpc.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaVenda)), // Venda=8
  '']);
  QueryLocCItsVen.Locate('Item', Item, []);
end;

procedure TFmLocCConCab.ReopenLocCConIts(CtrID: Integer);
begin
  //Info_Fazer(' 11');
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCConIts, Dmod.MyDB, [
  'SELECT ELT(its.IDTab+1, "Indefinido",  ',
  '"Loca��o", "Servi�o", "Venda", "? ? ?") NO_IDTab,  ',
  'ELT(its.Status+1,  "Indefinido", "Or�amento", "Pedido", "Fatura", "Cancelado", "? ? ?") NO_Status,  ',
  'CASE its.Status  ',
  '  WHEN 1 THEN its.ValorOrca  ',
  '  WHEN 2 THEN its.ValorPedi  ',
  '  WHEN 3 THEN its.ValorFatu ',
  'ELSE 0 END ValorStat, ppc.Nome NO_CondiOrca, ',
  'IF(ide_nNF <> 0, ',
  '  CASE its.ide_mod  ',
  '    WHEN 55 THEN "NF-e"  ',
  '    WHEN 65 THEN "NFC-e"  ',
  '  ELSE "?????" END, ',
  '"S/NF") NO_ide_mod, ',
  'aca.Nome NO_AtndCanal, its.*   ',
  'FROM loccconits its  ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=its.CondiOrca ',
  'LEFT JOIN atndcanal aca ON aca.Codigo=its.AtndCanal ',
  'WHERE its.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  ' ']);
  //
  if CtrID > 0 then
    QrLocCConIts.Locate('CtrID', CtrID, []);
end;

procedure TFmLocCConCab.ReopenLocIPatAce(QueryLocIPatAce: TmySQLQuery; Codigo, CtrID, Item: Integer);
begin
  //Info_Fazer('Fazer 12');
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocIPatAce, Dmod.MyDB, [
  'SELECT lpp.*, (QtdeProduto * QtdeLocacao) + 0.000 QTDE, ',
  'gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN ',
  'FROM loccitslca lpp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
  'WHERE lpp.Codigo=' + Geral.FF0(Codigo),
  'AND lpp.CtrID=' + Geral.FF0(CtrID),
  'AND lpp.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaAcessorio)), // Acessorio
  '']);
  //
  QueryLocIPatAce.Locate('Item', Item, []);
{
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocIPatAce, Dmod.MyDB, [
    'SELECT lpa.*, gg1.Nome NO_GGX, gg1.REFERENCIA ',
    'FROM loccpatace lpa ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpa.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE lpa.CtrID=' + Geral.FF0(CtrID),
    '']);
  //
  QueryLocIPatAce.Locate('Item', Item, []);
}
end;

procedure TFmLocCConCab.ReopenLocIPatApo(QueryLocIPatApo: TmySQLQuery; Codigo,
  CtrID, Item: Integer);
begin
{ Fazer
  //Info_Fazer(' 24');

  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocIPatCns, Dmod.MyDB, [
    'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,',
    'med.SIGLA ',
    'FROM
    loccpatcns lpc ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade',
    'WHERE lpc.CtrID=' + Geral.FF0(CtrID),
    '']);
  //
  QueryLocIPatCns.Locate('Item', Item, []);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocIPatApo, Dmod.MyDB, [
  'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,',
  'med.SIGLA ',
  'FROM loccitsruc lpc ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade',
  'WHERE lpc.Codigo=' + Geral.FF0(Codigo),
  'AND lpc.CtrID=' + Geral.FF0(CtrID),
  'AND lpc.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaApoio)), // Apoio
  '']);
  QueryLocIPatApo.Locate('Item', Item, []);
end;

procedure TFmLocCConCab.ReopenLocIPatCns(QueryLocIPatCns: TmySQLQuery; Codigo, CtrID,
  Item: Integer);
begin
{ Fazer
  //Info_Fazer(' 13');
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocIPatCns, Dmod.MyDB, [
    'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,',
    'med.SIGLA ',
    'FROM
    loccpatcns lpc ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade',
    'WHERE lpc.CtrID=' + Geral.FF0(CtrID),
    '']);
  //
  QueryLocIPatCns.Locate('Item', Item, []);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocIPatCns, Dmod.MyDB, [
  'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,',
  'med.SIGLA ',
  'FROM loccitsruc lpc ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade',
  'WHERE lpc.Codigo=' + Geral.FF0(Codigo),
  'AND lpc.CtrID=' + Geral.FF0(CtrID),
  'AND lpc.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaConsumo)), // Consumo
  '']);
  QueryLocIPatCns.Locate('Item', Item, []);
end;

procedure TFmLocCConCab.ReopenLocIPatPri(QueryLocIPatPri: TmySQLQuery;
  Codigo, CtrID, Item: Integer);
begin
  //Info_Fazer('Fazer 14');
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocIPatPri, Dmod.MyDB, [
  'SELECT lpp.*, (QtdeProduto * QtdeLocacao) + 0.000 QTDE, ',
  'gg1.Nome NO_GGX, gg1.Patrimonio, gg1.REFERENCIA, sen.LOGIN, ',
  'IF(lpp.Troca>0, "SIM", "") TROCA_TXT, ',
  'IF(QtdeLocacao=1, "SIM", "N�O") BAIXA_TXT, ',
  'DATE_FORMAT(HrTolerancia, "%H:%i") HrTolerancia_TXT ',
  'FROM loccitslca lpp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
  'WHERE lpp.Codigo=' + Geral.FF0(Codigo),
  'AND lpp.CtrID=' + Geral.FF0(CtrID),
  'AND lpp.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaPrincipal)), // Principal
  '']);
  //
  QueryLocIPatPri.Locate('Item', Item, []);
end;

procedure TFmLocCConCab.ReopenLocIPatSec(QueryLocIPatSec: TmySQLQuery; Codigo, CtrID, Item: Integer);
begin
  //Info_Fazer('Fazer 15');
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocIPatSec, Dmod.MyDB, [
  'SELECT lpp.*, (QtdeProduto * QtdeLocacao) + 0.000 QTDE, ',
  'gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN ',
  'FROM loccitslca lpp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
  'WHERE lpp.Codigo=' + Geral.FF0(Codigo),
  'AND lpp.CtrID=' + Geral.FF0(CtrID),
  'AND lpp.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaSecundario)), // Secundario
  '']);
  //
  QueryLocIPatSec.Locate('Item', Item, []);
end;

procedure TFmLocCConCab.ReopenLocIPatUso(QueryLocIPatUso: TmySQLQuery; Codigo, CtrID, Item: Integer);
begin
{ Fazer
  //Info_Fazer(' 16');

  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocIPatUso, Dmod.MyDB, [
    'SELECT lpu.*, gg1.Nome NO_GGX, gg1.REFERENCIA, ',
    'med.SIGLA, avi.Nome NO_AVALINI, avf.Nome NO_AVALFIM  ',
    'FROM
    loccpatuso lpu  ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpu.GraGruX  ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'LEFT JOIN unidmed med ON med.Codigo=lpu.Unidade ',
    'LEFT JOIN graglaval avi ON avi.Codigo=lpu.AvalIni ',
    'LEFT JOIN graglaval avf ON avf.Codigo=lpu.AvalFim ',
    'WHERE lpu.CtrID=' + Geral.FF0(CtrID),
    '']);
  //
}
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLocIPatUso, Dmod.MyDB, [
  'SELECT lpu.*, gg1.Nome NO_GGX, gg1.REFERENCIA, ',
  'med.SIGLA, avi.Nome NO_AVALINI, avf.Nome NO_AVALFIM  ',
  'FROM loccitsruc lpu  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpu.GraGruX  ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed med ON med.Codigo=lpu.Unidade ',
  'LEFT JOIN graglaval avi ON avi.Codigo=lpu.AvalIni ',
  'LEFT JOIN graglaval avf ON avf.Codigo=lpu.AvalFim ',
  'WHERE lpu.Codigo=' + Geral.FF0(Codigo),
  'AND lpu.CtrID=' + Geral.FF0(CtrID),
  'AND lpu.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaUso)), // Uso
  '']);
  //
  QueryLocIPatUso.Locate('Item', Item, []);
end;

procedure TFmLocCConCab.ReopenPsqSum_LocCConIts((*const FatID: Integer;*) var Achou: Boolean);
const
  sProcName = 'TFmLocCConCab.ReopenPsqSum_LocCConIts()';
var
  IDCtrl, Status, nNF: Integer;
  Msg, sNF: String;
begin
  Achou := False;
  //
{
  case FatID of
    VAR_FATID_0001 : nNF := QrLocCConItsFatPedCab.Value;
    VAR_FATID_0002 : nNF := QrLocCConItsFatPedCab.Value;
    else
    begin
      nNF := -1;
      Geral.MB_Erro('FatID n�o implementado em ' + sProcName);
    end;
  end;
  //case QrLocCConItside_mod.Value
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqSum, Dmod.MyDB, [
  'SELECT IDCtrl, Status, ide_mod, ide_Serie, ide_nNF, Id ',
  'FROM nfecaba ',
  'WHERE FatID=' + Geral.FF0(FatID), // /*NFCe*/ ',
  'AND FatNum=' + Geral.FF0(nNF), // /*FatPedCab*/ ',
  'AND Empresa=' + Geral.FF0(QrLocCConCabEmpresa.Value),
  'AND Status IN (35, 100, 101) ',
  '']);
}
  nNF := QrLocCConItsFatPedCab.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqSum, Dmod.MyDB, [
  'SELECT IDCtrl, Status, ide_mod, ide_Serie, ide_nNF, Id ',
  'FROM nfecaba ',
  'WHERE FatID IN (' +
    Geral.FF0(VAR_FATID_0001) + ', '  +  // NFe
    Geral.FF0(VAR_FATID_0002) + ')',     // NFCe
  'AND FatNum=' + Geral.FF0(nNF), // /*FatPedCab*/ ',
  'AND Empresa=' + Geral.FF0(QrLocCConCabEmpresa.Value),
  'AND Status IN (35, 100, 101) ',
  '']);
  //
  //
  //
  IDCtrl := QrPsqSum.FieldByName('IDCtrl').AsInteger;
  if IDCtrl > 0 then
  begin
    Achou := True;
    Status := QrPsqSum.FieldByName('Status').AsInteger;
    case QrPsqSum.FieldByName('ide_mod').AsInteger of
      55: sNF := 'NFC-e';
      65: sNF := 'NF-e';
      else  sNF := 'NF';
    end;
    //
    case Status of
      35:  Msg := 'J� existe uma ' + sNF + ' emitida OFF-LINE para este movimento!';
      100: Msg := 'J� existe uma ' + sNF + ' emitida para este movimento!';
      101:
      begin
        Msg := 'Existe uma ' + sNF + ' emitida e cancelada para este movimento!';
        //
        if (QrLocCConItsNF_Stat.Value = 100)
        and (QrPsqSum.FieldByName('ide_mod').AsInteger = 65) then
          DmNFe_0000.CancelaFaturamento(QrPsqSum.FieldByName('Id').AsString);
        if (QrLocCConItsNF_Stat.Value = 100)
        and (QrPsqSum.FieldByName('ide_mod').AsInteger = 55) then
          DmNFe_0000.CancelaFaturamento(QrPsqSum.FieldByName('Id').AsString);
      end;
      else Msg := 'Status de ' + sNF + ' n�o implementado em ' + sProcName;
    end;
    if Geral.MB_Pergunta(Msg + sLineBreak +
    sLineBreak + 'S�rie: ' + Geral.FF0(QrPsqSum.FieldByName('ide_Serie').AsInteger) +
    sLineBreak + 'N� ' + sNF + ': ' + Geral.FF0(QrPsqSum.FieldByName('ide_nNF').AsInteger) +
    sLineBreak + 'Deseja visualiz�-la?') = ID_Yes then
    begin
      UnNFe_PF.MostraFormNFePesq_IDCtrl_Unico(True, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPagerNovo, IDCtrl);
      //
      Exit;
    end;
  end;
end;

procedure TFmLocCConCab.ReopenSmiaVen();
begin
  if QrLocCItsVenSMIA_IDCtrl.Value <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSmiaVen, Dmod.MyDB, [
    'SELECT ELT(Baixa+2, "Sim", "N�o", "Erro") Baixa_TXT, ',
    'DataHora, IDCtrl, Tipo, OriCodi, ',
    'OriCtrl, OriCnta, Empresa, StqCencad,',
    'GraGruX, Qtde, Baixa, vDesc',
    '/* FatorClas, UnidMed, ValiStq   */',
    'FROM stqmovitsa',
    'WHERE IDCtrl=' + Geral.FF0(QrLocCItsVenSMIA_IDCtrl.Value),
    '']);
  end else
    QrSmiaVen.Close;
end;

procedure TFmLocCConCab.ReopenLocFCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocFCab, Dmod.MyDB, [
(*
  'SELECT lfc.* ',
  'FROM locfcab lfc ',
  'WHERE lfc.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
*)
  'SELECT car.Nome NO_CartEmis, ',
  'ppc.Nome NO_PediPrzCab, lfc.* ',
  'FROM locfcab lfc ',
  'LEFT JOIN carteiras car ON car.Codigo=lfc.CartEmis',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=lfc.CondicaoPg',
  'WHERE lfc.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  '']);
  //
  QrLocFCab.Locate('Controle', Controle, []);
end;

procedure TFmLocCConCab.ReopenLctFatRef(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLctFatRef, Dmod.MyDB, [
  'SELECT lfr.* ',
  'FROM lctfatref lfr ',
  'WHERE lfr.Controle=' + Geral.FF0(QrLocFCabControle.Value),
  'AND FatID IN(0, '+ Geral.FF0(VAR_FATID_3001) + ') ',
  '']);
  //
  QrLctFatRef.Locate('Conta', Conta, []);
end;

procedure TFmLocCConCab.Retorna1Click(Sender: TObject);
{
var
  DataDef, DataSel, DataMin: TDateTime;
  DtHrRetorn: String;
  RetFunci, UserSelect, RetExUsr, i,
  Codigo, CtrID, Item: Integer;
}
begin
{
  if (QrLocIPatPri.State = dsInactive) and (QrLocIPatPri.RecordCount = 0) then Exit;
  //
  CtrID    := QrLocCConItsCtrID.Value;
  DataDef  := Now();
  DataMin  := Trunc(QrLocCConCabDtHrEmi.Value);
  DataSel  := 0;
  RetExUsr := VAR_USUARIO;
  //
  if not DBCheck.ObtemDataEUser(VAR_USUARIO, UserSelect,
    DataDef, DataSel, dataMin, Time, True) then Exit;
  //
  DtHrRetorn := Geral.FDT(DataSel, 109);
  RetFunci   := UserSelect;
  //
  if DBGLocIPatPri.SelectedRows.Count > 1 then
  begin
    with DBGLocIPatPri.DataSource.DataSet do
    for i:= 0 to DBGLocIPatPri.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGLocIPatPri.SelectedRows.Items[i]));
      GotoBookmark(DBGLocIPatPri.SelectedRows.Items[i]);
      //
      //
      Item := QrLocIPatPriItem.Value;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitslca', False, [
      'RetFunci', 'DtHrRetorn', 'RetExUsr'], ['Item'], [
      RetFunci, DtHrRetorn, RetExUsr], [Item], True) then
      begin
        Dmod.VerificaSituacaoPatrimonio(QrLocIPatPriGraGruX.Value);
      end;
    end;
  end else
  begin
    Item := QrLocIPatPriItem.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitslca', False, [
    'RetFunci', 'DtHrRetorn', 'RetExUsr'], ['Item'], [
    RetFunci, DtHrRetorn, RetExUsr], [Item], True) then
    begin
      Dmod.VerificaSituacaoPatrimonio(QrLocIPatPriGraGruX.Value);
    end;
  end;
  //
  ReopenLocIPatPri(QrLocIPatPri, QrLocCConCabCodigo.Value, CtrID, Item);
}
end;

procedure TFmLocCConCab.RGTipoAluguelClick(Sender: TObject);
var
  DataDev, HoraDev: TDateTime;
begin
  //RecalculaDataProgramadaRetorno();
  if RGTipoAluguel.Focused then
  begin
    DefineDataHoraDevolver();
  end;
end;

procedure TFmLocCConCab.DBGLocIPatCnsDblClick(Sender: TObject);
var
  Campo: String;
begin
  if not DoubleClick_GradeItens(TDBGrid(Sender)) then
  begin
    if (QrLocIPatCns.State = dsInactive) and (QrLocIPatCns.RecordCount = 0) then
      Exit;
    //
    Campo := Uppercase(DBGLocIPatCns.Columns[THackDBGrid(DBGLocIPatCns).Col -1].FieldName);
    if (Uppercase(Campo) = Uppercase('ValorRucAAdiantar'))
    or (Uppercase(Campo) = Uppercase('QTDINI'))
    or (Uppercase(Campo) = Uppercase('QTDFIM')) then
      MostraFormLocIPatCns();
  end;
end;

procedure TFmLocCConCab.DBGLocIPatCnsMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMLocIPatCns, DBGLocIPatCns, X,Y);
end;

procedure TFmLocCConCab.DBGLocIPatUsoDblClick(Sender: TObject);
var
  Campo: String;
  ValorUso, ValorRucAAdiantar: Double;
  Codigo: Integer;
begin
  //Info_Fazer('Fazer 18');
  Codigo := QrLocCConCabCodigo.Value;
  //
  if not DoubleClick_GradeItens(TDBGrid(Sender)) then
  begin
    if (QrLocIPatUso.State = dsInactive) or (QrLocIPatUso.RecordCount = 0) then
      Exit;
    //
    Campo := Uppercase(DBGLocIPatUso.Columns[THackDBGrid(DBGLocIPatUso).Col -1].FieldName);
    //
    if Campo = Uppercase('NO_AVALINI') then DefineAvaliacao(
      avalIni, QrLocIPatUsoItem.Value, QrLocIPatUsoAvalIni.Value, True)
    else
    if Campo = Uppercase('NO_AVALFIM') then DefineAvaliacao(
      avalFim, QrLocIPatUsoItem.Value, QrLocIPatUsoAvalFim.Value, True)
    else
    //if Campo = Uppercase('VALTOT') then
    // T i s o l i n
    if Campo = Uppercase('VALUSO') then
    begin
      if QrLocIPatUsoValUso.Value > 0 then
        ValorUso := QrLocIPatUsoValUso.Value
      else
        ValorUso := QrLocIPatUsoPrcUni.Value;
      if dmkPF.ObtemValorDouble(ValorUso, 2) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitsruc', False, [
        'ValUso'], ['Item'], [ValorUso], [QrLocIPatUsoItem.Value], True) then
        begin
          AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
          LocCod(Codigo, Codigo);
          ReopenLocIPatUso(QrLocIPatUso, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, QrLocIPatUsoItem.Value);
        end;
      end;
    end;
    if Campo = Uppercase('ValorRucAAdiantar') then
    begin
      if QrLocIPatUsoValorRucAAdiantar.Value > 0 then
        ValorRucAAdiantar := QrLocIPatUsoValorRucAAdiantar.Value
      else
        ValorRucAAdiantar := QrLocIPatUsoPrcUni.Value;
      if dmkPF.ObtemValorDouble(ValorRucAAdiantar, 2) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitsruc', False, [
        'ValorRucAAdiantar'], ['Item'], [ValorRucAAdiantar], [QrLocIPatUsoItem.Value], True) then
        begin
          (*
          if  QrLocIPatUsoValUso.Value < ValorRucAAdiantar then
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitsruc', False, [
            'ValUso'], ['Item'], [ValorRucAAdiantar], [QrLocIPatUsoItem.Value], True);
          *)
            //
          AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
          LocCod(Codigo, Codigo);
          ReopenLocIPatUso(QrLocIPatUso, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, QrLocIPatUsoItem.Value);
        end;
      end;
    end;
  end;
end;

procedure TFmLocCConCab.DBGLocIPatUsoMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMLocIPatUso, DBGLocIPatUso, X,Y);
end;

procedure TFmLocCConCab.DBMeHistImprimeDblClick(Sender: TObject);
begin
  AlteraTextoMemoWideStringField('Hist�rico a ser Impresso na Loca��o',
  'HistImprime');
end;

procedure TFmLocCConCab.DBMemo2DblClick(Sender: TObject);
begin
  AlteraTextoMemoWideStringField('Hist�rico N�O imprim�vel', 'HistNaoImpr');
end;

procedure TFmLocCConCab.DBGLocIPatApoDblClick(Sender: TObject);
begin
  DoubleClick_GradeItens(TDBGrid(Sender));
end;

procedure TFmLocCConCab.DBGLocIPatApoMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMLocIPatApo, DBGLocIPatApo, X,Y);
end;

procedure TFmLocCConCab.DBGLocIPatSecDblClick(Sender: TObject);
var
  Campo: String;
begin
  if not DoubleClick_GradeItens(TDBGrid(Sender)) then
  begin
(*
    if (QrLocIPatSec.State = dsInactive) or (QrLocIPatSec.RecordCount = 0) then
      Exit;
    //
    Campo := Uppercase(DBGLocIPatSec.Columns[THackDBGrid(DBGLocIPatSec).Col -1].FieldName);
    if (Uppercase(Campo) = Uppercase('GraGruX'))
    or (Uppercase(Campo) = Uppercase('REFERENCIA')) then
        GraL_Jan.MostraFormGraGXPatr(QrLocIPatSecGraGruX.Value);
*)
  end;
end;

procedure TFmLocCConCab.DBGLocIPatSecMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMLocIPatSec, DBGLocIPatSec, X,Y);
end;

procedure TFmLocCConCab.DBEdit27DblClick(Sender: TObject);
var
  Tab, Entidade: Integer;
  Form: TForm;
begin
  if (QrLocCConCab.State <> dsInactive)
  and (QrLocCConCabCliente.Value <> 0) then
  begin
    Entidade := QrLocCConCabCliente.Value;
    Tab := MyObjects.VerificaSeTabFormExiste(FmPrincipal.PageControl1, TFmEntidade2);
    if Tab < 0 then
    begin
      Entities.CadastroDeEntidade(VAR_ENTIDADE, fmcadEntidade2, fmcadEntidade2,
       (*EditingByMatriz*)False, (*AbrirEmAba*)True, FmPrincipal.PageControl1,
       FmPrincipal.AdvToolBarPagerNovo);
      Tab := MyObjects.VerificaSeTabFormExiste(FmPrincipal.PageControl1, TFmEntidade2);
    end;
  end;
  if Tab > - 1 then
  begin
    Form := TForm(FmPrincipal.Pagecontrol1.Pages[Tab].Components[0]);
    TFmEntidade2(Form).LocCod(Entidade, Entidade);
    FmPrincipal.PageControl1.ActivePageIndex := Tab;
  end;
end;

procedure TFmLocCConCab.DBEdNO_STATUSChange(Sender: TObject);
begin
  case QrLocCConCabStatus.Value of
    (*ABERTO*)     1: DBEdNO_STATUS.Font.Color := clBlue;
    (*LOCADO*)     2: DBEdNO_STATUS.Font.Color := clRed;
    (*FINALIZADO*) 3: DBEdNO_STATUS.Font.Color := clGreen;
    (*CANCELADO*)  4: DBEdNO_STATUS.Font.Color := clPurple;
    (*SUSPENSO*)   5: DBEdNO_STATUS.Font.Color := clBlack;
  end;
  DBEdNO_STATUS.Invalidate;
end;

procedure TFmLocCConCab.DBGLocCItsVenDblClick(Sender: TObject);
var
  Campo: String;
begin
  if (QrLocCItsVen.State = dsInactive) or (QrLocCItsVen.RecordCount = 0) then
    Exit;
  //
  Campo := Uppercase(DBGLocCItsVen.Columns[THackDBGrid(DBGLocCItsVen).Col -1].FieldName);
  if (Uppercase(Campo) = Uppercase('GraGruX'))
  or (Uppercase(Campo) = Uppercase('REFERENCIA')) then
      GraL_Jan.MostraFormGraGXVend(QrLocCItsVenGraGruX.Value);
end;

procedure TFmLocCConCab.DBGLocIConItsDblClick(Sender: TObject);
const
  sProcName = 'TFmLocCConCab.DBGLocIConItsDblClick()';
var
  Campo: String;
  Achou: Boolean;
  FatID, IDCtrl: Integer;
begin
  //if not DoubleClick_GradeItens(TDBGrid(Sender)) then
  begin
    if (QrLocCConIts.State = dsInactive) or (QrLocCConIts.RecordCount = 0) then
      Exit;
    //
    Campo := Uppercase(DBGLocIConIts.Columns[THackDBGrid(DBGLocIConIts).Col -1].FieldName);
    //
    FatID := -999999999;
    if (Uppercase(Campo) = Uppercase('NFCeFatNum'))
    or (Uppercase(Campo) = Uppercase('NFCeide_mod'))
    or (Uppercase(Campo) = Uppercase('NFCeide_nNF'))
    or (Uppercase(Campo) = Uppercase('NFCeNF_Stat')) then
      FatID := VAR_FATID_0002;
    if (Uppercase(Campo) = Uppercase('NFeFatNum'))
    or (Uppercase(Campo) = Uppercase('NFeide_mod'))
    or (Uppercase(Campo) = Uppercase('NFeide_nNF'))
    or (Uppercase(Campo) = Uppercase('NFeNF_Stat')) then
      FatID := VAR_FATID_0001;
    if FatID > 0 then
    begin
      case TTabConLocIts(QrLocCConItsIDTab.Value) of
        TTabConLocIts.tcliIndefinido: ;
        //TTabConLocIts.tcliLocacao:
        //TTabConLocIts.tcliOutroServico
        TTabConLocIts.tcliVenda:
          ReopenPsqSum_LocCConIts((*FatID,*) Achou);
        else
          Geral.MB_Info('Tipo de movimento n�o implementado em ' + sProcName +
          sLineBreak + 'Tipo de Movimento: ' +
          GetEnumName(TypeInfo(TTabConLocIts), QrLocCConItsIDTab.Value));
      end;
    end else
  end;
end;

procedure TFmLocCConCab.DBGLocIConItsMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
  begin
    if (QrLocCConIts.State <> dsInactive) and (QrLocCConIts.RecordCount > 0) then
    begin
        MyObjects.MostraPopOnControlXY(PMNFs, DBGLocIConIts, X,Y);
    end else
      Geral.MB_Aviso('N�o h� venda para ter emiss�o de NF-e ou NFC-e!');
  end;
end;

procedure TFmLocCConCab.DBGLocIPatAceDblClick(Sender: TObject);
var
  Campo: String;
begin
  if not DoubleClick_GradeItens(TDBGrid(Sender)) then
  begin
(*
    if (QrLocIPatAce.State = dsInactive) or (QrLocIPatAce.RecordCount = 0) then
      Exit;
    //
    Campo := Uppercase(DBGLocIPatAce.Columns[THackDBGrid(DBGLocIPatAce).Col -1].FieldName);
    if (Uppercase(Campo) = Uppercase('GraGruX'))
    or (Uppercase(Campo) = Uppercase('REFERENCIA')) then
        GraL_Jan.MostraFormGraGXPatr(QrLocIPatAceGraGruX.Value);
*)
  end;
end;

procedure TFmLocCConCab.DBGLocIPatAceMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMLocIPatAce, DBGLocIPatAce, X,Y);
end;

procedure TFmLocCConCab.DefineAvaliacao(Parte: TAvaliacao; Item, Atual: Integer;
  Reabre: Boolean);
const
  Aviso   = '...';
  Campo   = 'Descricao';
var
  //Avaliacao: Integer;
  C, P: String;
  AvalFld: String;
  Variavel: Variant;
  Codigo: Integer;
begin
  //Info_Fazer(' 19');
  case parte of
    avalIni: C := 'Avalia��o Inicial';
    avalFim: C := 'Avalia��o Final';
  end;
  case parte of
    avalIni: P := 'Informe a avalia��o inicial:';
    avalFim: P := 'Informe a avalia��o final:';
  end;
  case parte of
    avalIni: AvalFld := 'AvalIni';
    avalFim: AvalFld := 'AvalFim';
  end;
  //
  Variavel := DBCheck.EscolheCodigoUnico(Aviso, C, P, nil, nil, Campo, 0, [
  'SELECT Codigo, Nome ' + Campo,
  'FROM graglaval ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, False);
  if Variavel <> Null then
  begin
    Codigo := Variavel;
    if Codigo <> 0 then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitsruc', False, [
      AvalFld], ['Item'], [VAR_SELCOD], [Item], True) then
        if Reabre then
           ReopenLocIPatUso(QrLocIPatUso, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, Item);
    end;
  end;
end;

procedure TFmLocCConCab.DefineDataHoraDevolver();
var
  DataDev, HoraDev: TDateTime;
begin
  QuantidadeDeDiasFormaAluguel(DataDev, HoraDev);
  //
  TPDtHrDevolver.Date := DataDev;
  EdDtHrDevolver.ValueVariant := HoraDev;
end;

procedure TFmLocCConCab.DefineImprOrcaPedi(StatusMovimento: TStatusMovimento);
const
  sProcName = 'TFmLocCConCab.DefineImprOrcaPedi()';
const
  AlturaForm = 150 + (3 * 50 * 1(*linha*));
  CaptionRG1 = ' O que imprime: ';
  CaptionRG2 = ' Imprime os DESCONTOS? ';
  CaptionRG3 = ' Em quantas vias: ';

  ColunasRG1 = 2;
  ColunasRG2 = 2;
  ColunasRG3 = 2;
  DefaultRG1 = -1;
  DefaultRG2 = -1;
  DefaultRG3 = -1;
  SelOnClick = True;
var
  Result1, Result2, Result3: Integer;
var
  Vias, CtrID: Integer;
  ImpDesco: Boolean;
  MovTxt, CaptionFm: String;
begin
  case StatusMovimento of
    TStatusMovimento.statmovOrcamento: MovTxt := 'Or�amento';
    TStatusMovimento.statmovPedido: MovTxt := 'Pedido';
    else MovTxt := '???????';
  end;
  CaptionFm := 'Impress�o de ' + MovTxt;
  if MyObjects.Sel3RadioGroups(AlturaForm, CaptionFm, CaptionRG1, CaptionRG2, CaptionRG3,
  ['O ' + MovTxt + ' selecionado', 'Todos ' + MovTxt + 's '],
  ['SEM os descontos', 'COM os descontos'],
  ['1 via', '2 vias'],
  ColunasRG1, ColunasRG2, ColunasRG3, DefaultRG1, DefaultRG2, DefaultRG3,
  SelOnClick, Result1, Result2, Result3) then
  begin
    case Result1 of
      0: CtrID := QrLocCConItsCtrID.Value;
      1: CtrID := 0;
      else
      begin
        Geral.MB_Erro('"Result1" n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    ImpDesco := Result2 = 1;
    Vias := Result3 + 1;
    ImprimeOrcaPedi(CtrID, StatusMovimento, Vias, ImpDesco);
  end;
end;

procedure TFmLocCConCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmLocCConCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmLocCConCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmLocCConCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmLocCConCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmLocCConCab.SpeedButton6Click(Sender: TObject);
begin
  FinanceiroJan.ExtratosFinanceirosEmpresaUnica3_CLiente(EdCliente.ValueVariant);
end;

function TFmLocCConCab.StatusAtendimentoAberto(): Boolean;
begin
  Result :=
  (
    (QrLocCConCab.State <> dsInactive)
    and
    (QrLocCConCab.RecordCount > 0)
    and
    (TStatusLocacao(QrLocCConCabStatus.Value) = TStatusLocacao.statlocAberto)
  );
end;

function TFmLocCConCab.StatusAtendimentoCancelado: Boolean;
begin
  Result :=
  (
    (QrLocCConCab.State <> dsInactive)
    and
    (QrLocCConCab.RecordCount > 0)
    and
    (TStatusLocacao(QrLocCConCabStatus.Value) = TStatusLocacao.statlocCancelado)
  );
end;

function TFmLocCConCab.StatusAtendimentoFinalizado(): Boolean;
begin
  Result :=
  (
    (QrLocCConCab.State <> dsInactive)
    and
    (QrLocCConCab.RecordCount > 0)
    and
    (
      (TStatusLocacao(QrLocCConCabStatus.Value) = TStatusLocacao.statlocFinalizado)
      or
      (Length(QrLocCConCabDtHrBxa_TXT.Value) > 0)
    )
  );
end;

function TFmLocCConCab.StatusAtendimentoLocado(): Boolean;
begin
  Result :=
  (
    (QrLocCConCab.State <> dsInactive)
    and
    (QrLocCConCab.RecordCount > 0)
    and
    (TStatusLocacao(QrLocCConCabStatus.Value) = TStatusLocacao.statlocLocado)
  );
end;

function TFmLocCConCab.StatusMovimentoEhFaturado(): Boolean;
begin
  Result :=
  (
    (QrLocCConIts.State <> dsInactive)
    and
    (QrLocCConIts.RecordCount > 0)
    and
    (TStatusMovimento(QrLocCConItsStatus.Value) = TStatusMovimento.statmovFaturado)
  );
end;

function TFmLocCConCab.StatusMovimentoEhIndefinido: Boolean;
begin
  Result :=
  (
    (QrLocCConIts.State <> dsInactive)
    and
    (QrLocCConIts.RecordCount > 0)
    and
    (TStatusMovimento(QrLocCConItsStatus.Value) = TStatusMovimento.statmovIndefinido)
  );
end;

function TFmLocCConCab.StatusMovimentoEhOrcamento(): Boolean;
begin
  Result :=
  (
    (QrLocCConIts.State <> dsInactive)
    and
    (QrLocCConIts.RecordCount > 0)
    and
    (TStatusMovimento(QrLocCConItsStatus.Value) = TStatusMovimento.statmovOrcamento)
  );
end;

function TFmLocCConCab.StatusMovimentoEhPedido(): Boolean;
begin
  Result :=
  (
    (QrLocCConIts.State <> dsInactive)
    and
    (QrLocCConIts.RecordCount > 0)
    and
    (TStatusMovimento(QrLocCConItsStatus.Value) = TStatusMovimento.statmovPedido)
  );
end;

function TFmLocCConCab.StatusMovimentoPodeCancelar(): Boolean;
begin
  Result :=
  (
    (QrLocCConIts.State <> dsInactive)
    and
    (QrLocCConIts.RecordCount > 0)
    and
    (QrLocCConItsStatus.Value <  Integer(TStatusMovimento.statmovCancelado))
  );
end;

procedure TFmLocCConCab.Suspenso1Click(Sender: TObject);
const
  AvisaEstqNeg = True;
  GeraLog = False;
  SQL_CtrID = '';
var
  Codigo, Status, GraGruX, ItensNegativosPatr, ItensNegativosVend: Integer;
  SQLType: TSQLType;
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then Exit;
  //
  SQLType           := stUpd;
  Codigo            := QrLocCConCabCodigo.Value;
  Status            := Integer(TStatusLocacao.statlocSuspenso);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconcab', False, [
  'Status'], ['Codigo'], [
  Status], [Codigo], True) then
  begin
    (*
    AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, Null,
    QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
    QrLocCConCabDtHrDevolver.Value, GeraLog);
    *)
    AppPF.ReopenItsLca(QrItsLca, Codigo, SQL_CtrID);
    RecalculaEstoquesPatr(ItensNegativosPatr);
    RecalculaEstoquesVend(ItensNegativosVend);
  end;
end;

procedure TFmLocCConCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCConCab.BtSaidaClick(Sender: TObject);
var
  I: Integer;
begin
  VAR_CADASTRO := QrLocCConCabCodigo.Value;
  //
  if TFmLocCConCab(Self).Owner is TApplication then
    Close
  else
  // ini 2022-01-24 - ver erro!?
  //MyObjects.FormTDIFecha(TFmLocCConCab(Self), TTabSheet(CSTabSheetChamou.Component));
  try
    for I := 0 to TFmLocCConCab(Self).ComponentCount - 1 do
    begin
      if TFmLocCConCab(Self).Components[I] is TmySQLQuery then
        TmySQLQuery(TFmLocCConCab(Self).Components[I]).Close;
    end;
    MyObjects.FormTDIFecha(TFmLocCConCab(Self), TTabSheet(CSTabSheetChamou.Component));
  except
    ; // nada
  end;
  {$WARNINGS OFF}
  // fim 2022-01-24
end;
  {$WARNINGS ON}

procedure TFmLocCConCab.ItsInclui1Click(Sender: TObject);
begin
  if QrLocIPatPri.RecordCount > 0 then
  begin
    MostraFormLocCConIts(stIns, TTabConLocIts.tcliLocacao);
  end else
    MostraFormLocCItsLca(stIns);
end;

procedure TFmLocCConCab.CabAltera1Click(Sender: TObject);
var
  Enab1: Boolean;
begin
  FTipoAluguelAnt := QrLocCConCabTipoAluguel.Value;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrLocCConCab, [PnDados],
  [PnEdita], EdCliente, ImgTipo, 'loccconcab');
  //
  Enab1 := QrLocCConCabJaLocou.Value = 0;
  //
  TPDtHrSai.Enabled := Enab1;
  EdDtHrSai.Enabled := Enab1;
  LaDtHrSai.Enabled := Enab1;
  //
  if QrLocCConCabTipoAluguel.Value = 'D' then
    RGTipoAluguel.ItemIndex := 1
  else
  if QrLocCConCabTipoAluguel.Value = 'S' then
    RGTipoAluguel.ItemIndex := 2
  else
  if QrLocCConCabTipoAluguel.Value = 'M' then
    RGTipoAluguel.ItemIndex := 3
  else
  if QrLocCConCabTipoAluguel.Value = 'A' then
    RGTipoAluguel.ItemIndex := 4
  else
  //if QrLocCConCabTipoAluguel.Value = 'A' then
    RGTipoAluguel.ItemIndex := 0;
end;

procedure TFmLocCConCab.BtConfirmaClick(Sender: TObject);
var
  DtHrEmi, (*DtHrBxa,*) NumOC, LocalObra, Obs0, Obs1, Obs2, EndCobra,
  LocalCntat, LocalFone, (*Historico,*) TipoAluguel: String;
  Empresa, Codigo, Contrato, Cliente, Vendedor, ECComprou, ECRetirou,
  FormaCobrLoca: Integer;
  //ValorTot: Double;
  EhIns: Boolean;
(*
var
  //DtHrEmi, DtHrBxa, NumOC, EndCobra, LocalObra, LocalCntat, LocalFone, Obs0, Obs1, Obs2,
  Historico, TipoAluguel, DtUltCobMens: String;
  Codigo, Empresa, Contrato, Cliente, Vendedor, ECComprou, ECRetirou,
*)
  Status(*, Importado*): Integer;
(*
  //ValorTot,
  ValorEquipamento, ValorAdicional, ValorAdiantamento, ValorDesconto,
  ValorEquipamentos, ValorLocacao: Double;
*)
  SQLType: TSQLType;
  DtHrSai, DtHrDevolver: String;
  PrecisaLiberar: Boolean;
  AtndCanal: Integer;
begin
  Timer1.Enabled := False;
  DBText1.Visible := QrValAberto.State <> dsInactive;
  DBText2.Visible := QrValAberto.State <> dsInactive;
  //
  SQLType        := ImgTipo.SQLType;
  Empresa        := EdEmpresa.ValueVariant;
  //
  Codigo         := EdCodigo.ValueVariant;
  Contrato       := EdContrato.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  DtHrEmi        := Geral.FDT(Trunc(TPDataEmi.Date), 1) + ' ' + EdHoraEmi.Text;
  //DtHrBxa        := ;
  Vendedor       := EdVendedor.ValueVariant;
  ECComprou      := EdECComprou.ValueVariant;
  ECRetirou      := EdECRetirou.ValueVariant;
  NumOC          := EdNumOC.Text;
  EndCobra       := EdEndCobra.Text;
  LocalObra      := EdLocalObra.Text;
  LocalCntat     := EdLocalCntat.Text;
  LocalFone      := EdLocalFone.Text;
  Obs0           := EdObs0.Text;
  Obs1           := EdObs1.Text;
  Obs2           := EdObs2.Text;
  //
  FormaCobrLoca  := RGFormaCobrLoca.ItemIndex;
  //Historico      := MeHistorico.Text;
//begin
  //Historico      := ;
  TipoAluguel        := RGTipoAluguel.Items[RGTipoAluguel.ItemIndex][1];
  if MyObjects.FIC(RGTipoAluguel.ItemIndex < 1, RGTipoAluguel,
  'Informe o tipo de aluguel!') then Exit;

  DtHrSai        := Geral.FDT(Trunc(TPDtHrSai.Date), 1) + ' ' + EdDtHrSai.Text;
  DtHrDevolver        := Geral.FDT(Trunc(TPDtHrDevolver.Date), 1) + ' ' + EdDtHrDevolver.Text;
{
  DtHrSai            := ;
  ValorEquipamento   := ;
  ValorAdicional     := ;
  ValorAdiantamento  := ;
  ValorDesconto      := ;
  Status             := ;
  DtUltCobMens       := ;
  Importado          := ;
  DtHrDevolver       := ;
  ValorEquipamentos  := ;
  ValorLocacao       := ;
}
  AtndCanal := EdAtndCanal.ValueVariant;

  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa!') then
    Exit;
{
  if not UMyMod.ObtemCodigoDeCodUsu(EdEmpresa, Empresa, 'Informe a empresa!',
    'Codigo', 'Filial') then Exit;
}
  //
  if MyObjects.FIC(Contrato = 0, EdContrato, 'Informe o contrato!') then
    Exit;
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Informe o cliente!') then
    Exit;
  if MyObjects.FIC(Vendedor = 0, EdVendedor, 'Informe o vendedor!') then
    Exit;
  if MyObjects.FIC(AtndCanal = 0, EdAtndCanal, 'Informe o canal de atendimento inicial!') then
    Exit;
  //
  //////////////////////////////////////////////////////////////////////////////
  ///
  if SQLType = stIns then
    Status := Integer(TStatusLocacao.statlocAberto)
  else
    Status := QrLocCConCabStatus.Value;

  //////////////////////////////////////////////////////////////////////////////
  PrecisaLiberar := False;
  if ConfereDataHoraDevolverComPeriodicidade() = False then
  begin
    if Geral.MB_Pergunta(
    'A data prevista para devolu��o n�o combina com a periodicidade!' +
    sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
    begin
      //if not DBCheck.LiberaPelaSenhaPwdLibFunc() then Exit;
      // Tisolin n�o quer senha
      //PrecisaLiberar := True;
    end else
      Exit;
  end;
  if not PrecisaLiberar then
  begin
    if (SQLType = stIns) or (Cliente <> QrLocCConCabCliente.Value) then
    begin
      if (QrValAbertoVencido.Value > 0) then PrecisaLiberar := True;
      if (QrValAbertoAberto.Value >= QrClientesLimiCred.Value) then PrecisaLiberar := True;
    end;
  end;
  if PrecisaLiberar then
    if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
      Exit;
  //
  Codigo := UMyMod.BPGS1I32('loccconcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconcab', False, [
  'Contrato', 'Cliente', 'DtHrEmi',
  'Empresa', 'Vendedor', 'ECComprou',
  'ECRetirou', 'NumOC', 'EndCobra',
  'LocalObra', 'LocalCntat', 'LocalFone',
  'Obs0', 'Obs1', 'Obs2',

  //'Historico',
  'TipoAluguel',
  'DtHrSai', (*ValorAdicional,
  ValorAdiantamento, ValorDesconto,*) 'Status',
  (*DtUltCobMens, Importado,*) 'DtHrDevolver',
  (*ValorEquipamentos, ValorLocacao,*) 'FormaCobrLoca',
  'AtndCanal'
  ], [
  'Codigo'], [
  Contrato, Cliente, DtHrEmi,
  VUEmpresa.ValueVariant, Vendedor, ECComprou,
  ECRetirou, NumOC, EndCobra,
  LocalObra, LocalCntat, LocalFone,
  Obs0, Obs1, Obs2,

  //Historico,
  TipoAluguel,
  DtHrSai, (*ValorAdicional,
  ValorAdiantamento, ValorDesconto,*) Status,
  (*DtUltCobMens, Importado,*) DtHrDevolver,
  (*ValorEquipamentos, ValorLocacao,*) FormaCobrLoca,
  AtndCanal
  ], [
  Codigo], True) then
  begin
    EhIns := SQLType = stIns;
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    if SQLType = stUpd then
    begin
      if FTipoAluguelAnt <> TipoAluguel then
      begin
        AppPF.AtualizaValorAluguel_LocCItsLca(Codigo, TipoAluguel);
        AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
        // Precisa recalcular duas vezas
        RecalculaTudo(False, False);
        RecalculaTudo(False, False);
      end;
    end;
    LocCod(Codigo, Codigo);
    if Codigo = QrLocCConCabCodigo.Value then  // Evitar engano
    begin
      if EhIns then
        //InsereLocCConIts(TTabLocIts.tliIndefinido);
        MostraFormLocCConIts(stIns, TTabConLocIts.tcliIndefinido);
    //GOTOy.BotoesSb(ImgTipo.SQLType);
    end;
    GOTOy.BotoesSb(ImgTipo.SQLType, TFmLocCConCab(Self));
  end;
  //
end;

procedure TFmLocCConCab.BtDataClick(Sender: TObject);
begin
  DefineDataHoraDevolver();
end;

procedure TFmLocCConCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
{
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'loccconcab', Codigo);
}
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'loccconcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType, TFmLocCConCab(Self));
end;

procedure TFmLocCConCab.BtFatClick(Sender: TObject);
begin
  PCMovimento.ActivePageIndex := PCMov_PageIndex_Fatu;
  //
  MyObjects.MostraPopUpDeBotao(PMFat, BtFat);
end;

procedure TFmLocCConCab.BtFormaClick(Sender: TObject);
begin
  RGTipoAluguel.ItemIndex := FormaAluguelPelaDataDeRetono();
end;

procedure TFmLocCConCab.BtLocCItsLcaClick(Sender: TObject);
begin
  if TipoDeMovimentoEhLocacao() then
  begin
    PCMovimento.ActivePageIndex := PCMov_PageIndex_Loca;
    //
    MyObjects.MostraPopUpDeBotao(PMLocIItsLca, BtLocCItsLca);
  end else
    Geral.MB_Aviso('O movimento selecionado n�o � de loca��o!');
end;

procedure TFmLocCConCab.BtLocCItsSvcClick(Sender: TObject);
begin
  if TipoDeMovimentoEhServico() then
  begin
    PCMovimento.ActivePageIndex := PCMov_PageIndex_Serv;
    //
    MyObjects.MostraPopUpDeBotao(PMLocIItsSvc, BtLocCItsSvc);
  end else
    Geral.MB_Aviso('O movimento selecionado n�o � de servi�o!');
end;

procedure TFmLocCConCab.BtLocCItsVenClick(Sender: TObject);
begin
  if TipoDeMovimentoEhVenda() then
  begin
    PCMovimento.ActivePageIndex := PCMov_PageIndex_Vend;
    //
    MyObjects.MostraPopUpDeBotao(PMLocIItsVen, BtLocCItsVen);
  end else
    Geral.MB_Aviso('O movimento selecionado n�o � de venda!');
end;

procedure TFmLocCConCab.BtLocCConItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLocIConIts, BtLocCConIts);
end;

procedure TFmLocCConCab.BtNFSeClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(False, nil, nil);
end;

procedure TFmLocCConCab.Acessrio1Click(Sender: TObject);
begin
  ImprimeFichaDeTroca(TGraGXToolRnt.ggxo2gAcessorio);
end;

procedure TFmLocCConCab.Adicionaacessrio1Click(Sender: TObject);
begin
  //Info_Fazer(' 20');
  //MostraFormLocCItsMat(QrLocCConItsCodigo.Value, QrLocCConItsCtrID.Value, 3, CO_GraGruY_3072_GXPatAce); //Acess�rio
  MostraFormLocCItsMat(QrLocCConItsCodigo.Value, QrLocCConItsCtrID.Value,
    TGraGXToolRnt.ggxo2gAcessorio, CO_GraGruY_3072_GXPatAce); //Acess�rio
end;

procedure TFmLocCConCab.AdicionaequipamentoSecundrio1Click(Sender: TObject);
begin
  //Info_Fazer(' 29');
  //MostraLocCApo(QrLocIPatSecCodigo.Value, QrLocCConItsCtrID.Value, 3); //Material secundario
  MostraFormLocCItsMat(QrLocCConItsCodigo.Value, QrLocCConItsCtrID.Value,
  TGraGXToolRnt.ggxo2gSecundario, CO_GraGruY_2048_GXPatSec); //Patrim�nio Secund�rio
end;

procedure TFmLocCConCab.AdicionamaterialdeApoio1Click(Sender: TObject);
begin
  //Info_Fazer(' 28');
  //MostraFormLocCItsMat(QrLocCConItsCodigo.Value, QrLocCConItsCtrID.Value, 1,  CO_GraGruY_4096_GXPatApo); //Material de Apoio
  MostraFormLocCItsMat(QrLocCConItsCodigo.Value, QrLocCConItsCtrID.Value,
  TGraGXToolRnt.ggxo2gApoio, CO_GraGruY_4096_GXPatApo); //Material de Apoio
end;

procedure TFmLocCConCab.Adicionamaterialdeconsumo1Click(Sender: TObject);
begin
  //Info_Fazer(' 21');
  //MostraFormLocCItsMat(QrLocCConItsCodigo.Value, QrLocCConItsCtrID.Value, 3, CO_GraGruY_6144_GXPrdCns); //Material de consumo
  MostraFormLocCItsMat(QrLocCConItsCodigo.Value, QrLocCConItsCtrID.Value,
  TGraGXToolRnt.ggxo2gConsumo, CO_GraGruY_6144_GXPrdCns); //Material de consumo
end;

procedure TFmLocCConCab.Adicionamaterialuso1Click(Sender: TObject);
begin
  //Info_Fazer(' 22');
  //MostraFormLocCItsMat(QrLocCConItsCodigo.Value, QrLocCConItsCtrID.Value, 2, CO_GraGruY_5120_GXPatUso); //Material uso
  MostraFormLocCItsMat(QrLocCConItsCodigo.Value, QrLocCConItsCtrID.Value,
  TGraGXToolRnt.ggxo2gUso, CO_GraGruY_5120_GXPatUso); //Material uso
end;

procedure TFmLocCConCab.AdicionaServico1Click(Sender: TObject);
const
  Item = 0;
begin
  //Info_Fazer(' 32');
  MostraFormLocCItsSvc(stIns, QrLocCConItsCodigo.Value, QrLocCConItsCtrID.Value, Item); //Servi�o (Frete)
end;

procedure TFmLocCConCab.AdicionaVenda1Click(Sender: TObject);
const
  Item = 0;
begin
  if MovimentoPossuiNFAutorizada(True) then Exit;
  //
  MostraFormLocCItsVen(stIns, QrLocCConItsCodigo.Value, QrLocCConItsCtrID.Value, Item); //Venda de Mercadoria
end;

procedure TFmLocCConCab.Agora1Click(Sender: TObject);
begin
  RecalculaTudo(False, False);
end;

procedure TFmLocCConCab.Agoramostrandoclculo1Click(Sender: TObject);
begin
  RecalculaTudo(True, False);
end;

procedure TFmLocCConCab.Alteracondiodepagamentodooramento1Click(
  Sender: TObject);
begin
  MostraFormLocCConIts(stUpd, TTabConLocIts.tcliIndefinido);
end;

procedure TFmLocCConCab.AlteraStatusMovimento(NewStatusMovimento: TStatusMovimento);
const
  sProcName = 'TFmLocCConCab.AlteraStatusMovimento()';
  AvisaEstqNeg = True;
  GeraLog = False;
var
  Codigo, CtrID, Status, IDTab, Empresa, FatPedCab, FatPedVolCnta, GraGruX,
  GraGru1: Integer;
  Quantidade, PrcUni, PercDesco, ValorBruto, ValorTotal, prod_vFrete, prod_vSeg,
  prod_vDesc, prod_vOutro: Double;
  //EraOrcamento, EhPedido: Boolean;
  //
  ManejoLca: TManejoLca;
  Alterou: Boolean;
  NewCtrID, NewItem, StqCenCad, PediVda, NF_Stat, CabStat, ItsStat, Unidade: Integer;
  Tmp_FatPedFis: String;
  OldStatusMovimento: TStatusMovimento;
  Continua, Confirmado: Boolean;
  DtFimCobra: TDateTime;
var
  SMIA_IDCtrl, Item: Integer;
  sIDCtrl: String;
  NaoBaixou, AltItem: Integer;
begin
  //
  OldStatusMovimento := TStatusMovimento(QrLocCConItsStatus.Value);
  if MyObjects.FIC(StatusAtendimentoFinalizado(), nil,
  'Atendimento j� finalizado!') then Exit;
  //
  //verificar orcamento de loca��o
  if (OldStatusMovimento = TStatusMovimento.statmovOrcamento)
  and (NewStatusMovimento = TStatusMovimento.statmovPedido) then
  begin
    case TTabConLocIts(QrLocCConItsIDTab.Value) of
      TTabConLocIts.tcliIndefinido: ;
      TTabConLocIts.tcliLocacao:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
        'SELECT *  ',
        'FROM loccitslca ',
        'WHERE CtrID=' + Geral.FF0(QrLocCConItsCtrID.Value),
        'AND QtdeLocacao = 0 ',
        '']);
        if Dmod.QrAux.RecordCount > 0 then
        begin
          Geral.MB_Aviso('Existem ' + Geral.FF0(Dmod.QrAux.RecordCount)
          + ' itens sem confirma��o de loca��o!');
          Exit;
        end;

(*
        QrLocIPatPri.DisableControls;
        try
          QrLocIPatPri.First;
          while not QrLocIPatPri.Eof do
          begin
              if
            //
            QrLocIPatPri.Next;
          end;
        finally
          QrLocIPatPri.EnableControls;
        end;
*)
      end;
    end;
  end;
  //
  NaoBaixou     := 0;
  Codigo        := QrLocCConCabCodigo.Value;
  CtrID         := QrLocCConItsCtrID.Value;
  Status        := Integer(NewStatusMovimento);
  IDTab         := QrLocCConItsIDTab.Value;
  Empresa       := QrLocCConCabEmpresa.Value;
  FatPedCab     := QrLocCConItsFatPedCab.Value;
  FatPedVolCnta := QrLocCConItsFatPedVolCnta.Value;
  NF_Stat            := QrLocCConItsNF_Stat.Value;
  DtFimCobra         := DModG.ObtemAgora();
  //
  // Impedir voltar para or�amento se NF_Stat = 100
  if NF_Stat = CO_100_NFeAutorizada then
  begin
    Geral.MB_Aviso('Altera��o cancelada! Este movimento j� possui NF emitida!');
    Exit;
  end;
  //EraOrcamento  := TStatusMovimento(QrLocCConItsStatus.Value) = TStatusMovimento.statmovOrcamento;
  //EhPedido      := TStatusMovimento(NewStatusMovimento) = TStatusMovimento.statmovPedido;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconits', False, [
  'Status'], ['CtrID'], [Status], [CtrID], True) then
  begin
    //
    //case TGraToolRent(IDTab) of
    case TTabConLocIts(IDTab) of
      TTabConLocIts.tcliIndefinido: Exit;// Nada
      TTabConLocIts.tcliLocacao:
      begin
        if AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, CtrID,
        QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
        QrLocCConCabDtHrDevolver.Value, DtFimCobra, GeraLog) then
          AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
      end;
      TTabConLocIts.tcliOutroServico: AppPF.AtualizaTotaisLocCConCab_Servicos(Codigo);
      TTabConLocIts.tcliVenda:
      begin
        if NewStatusMovimento = TStatusMovimento.statmovPedido then
        begin
          if FatPedCab = 0 then
          begin
            Confirmado    := False;
            PediVda       := 0;
            FatPedCab     := 0;
            FatPedVolCnta := 0;
            Continua := AppPF.GeraFatPedCabEPediVdaDeLocCItsVen(stIns,
             (*FDtHrEmi*)QrLocCConCabDtHrEmi.Value,
             (*FEmpresa*)QrLocCConCabEmpresa.Value,
             (*FCliente*)QrLocCConCabCliente.Value,
             (*Codigo*)QrLocCConCabCodigo.Value,
              Confirmado, PediVda, FatPedCab, FatPedVolCnta);
              if Continua then
              begin
                Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconits', False, [
                'PediVda', 'FatPedCab', 'FatPedVolCnta'], [
                'CtrID'], [
                PediVda, FatPedCab, FatPedVolCnta], [
                CtrID], True);
              end else
              begin
                NaoBaixou := QrLocCItsVen.RecordCount;
              end;
          end else
            Continua := True;
        end else
          Continua := True;
        //
        if Continua then
        begin
          ManejoLca := TManejoLca.manelcaVenda;
          //
          NF_Stat := QrLocCConItsNF_Stat.Value;
          CabStat := QrLocCConCabStatus.Value;
          ItsStat := QrLocCConItsStatus.Value;
          QrLocCItsVen.DisableControls;
          try
            QrLocCItsVen.First;
            while not QrLocCItsVen.Eof do
            begin
              GraGruX     := QrLocCItsVenGraGruX.Value;
              Unidade     := QrLocCItsVenUnidade.Value;
              StqCencad   := QrLocCItsVenStqCencad.Value;
              GraGru1     := QrLocCItsVenGraGru1.Value;
              Quantidade  := QrLocCItsVenQuantidade.Value;
              PrcUni      := QrLocCItsVenPrcUni.Value;
              ValorTotal  := QrLocCItsVenValorTotal.Value;
              prod_vFrete := QrLocCItsVenprod_vFrete.Value;
              prod_vSeg   := QrLocCItsVenprod_vSeg.Value;
              prod_vDesc  := QrLocCItsVenprod_vDesc.Value;
              prod_vOutro := QrLocCItsVenprod_vOutro.Value;
              PercDesco   := QrLocCItsVenPercDesco.Value;
              ValorBruto  := QrLocCItsVenValorBruto.Value;
              AltItem     := QrLocCItsVenItem.Value;
              //EraOrcamento,
              //FEhPedido, FAlterou, FNewCtrID, FNewItem, FTmp_FatPedFis
              // ini 2023-12-16
              SMIA_IDCtrl   := 0;
              Alterou       := False;
              NewCtrID      := 0;
              NewItem       := 0;
              Tmp_FatPedFis := '';
              // fim 2023-12-16

              //
              if AppPF.InsAltLocCItsVen((*SQLType*)stUpd, Codigo, CtrID,
              (*FatID*)VAR_FATID_0002, Empresa, FatPedCab,
              FatPedVolCnta, ManejoLca, GraGru1, Unidade, GraGruX, StqCencad,
              NF_Stat, Quantidade, PrcUni, PercDesco, ValorBruto,
              ValorTotal, prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro,
              //
              //EraOrcamento, StatusMovimento,
              OldStatusMovimento, NewStatusMovimento,
              //
              // ini 2023-12-16
              AltItem, SMIA_IDCtrl,
              // fim 2023-12-16
              //
              Alterou, NewCtrID, NewItem, Tmp_FatPedFis) then
              begin
                AppPF.VerificaEstoqueVenda(Empresa, FatPedCab, NF_Stat, CabStat,
                // ini 2020-12-11
                //ItsStat);
                Integer(NewStatusMovimento));
                // fim 2020-12-11
              end else
                NaoBaixou := NaoBaixou + 1;
              //
              QrLocCItsVen.Next;
            end;
          finally
            QrLocCItsVen.EnableControls;
            AppPF.AtualizaTotaisLocCConCab_Vendas(Codigo);
          end;
        end;
      end;
      else
      begin
        Geral.MB_Info('"TGraToolRent" n�o implementado em ' + sProcName);
      end;
    end;
  end;
  //
  if NaoBaixou > 0 then
  begin
    if (Status = Integer(TStatusMovimento.statmovPedido))
    and (OldStatusMovimento = TStatusMovimento.statmovOrcamento) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconits', False, [
      'Status'], ['CtrID'], [Integer(OldStatusMovimento)], [CtrID], True) then
      begin
        ReopenLocCItsVen(QrLocCItsVen, Codigo, CtrID, 0);
        QrLocCItsVen.First;
        while not QrLocCItsVen.Eof do
        begin
          SMIA_IDCtrl := QrLocCItsVenSMIA_IDCtrl.Value;
          if SMIA_IDCtrl <> 0 then
          begin
            sIDCtrl := Geral.FF0(SMIA_IDCtrl);
            //
            // ini 2023-12-17
            (*
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovitsa WHERE IDCtrl=' + sIDCtrl);
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovitsb WHERE IDCtrl=' + sIDCtrl);
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovvala WHERE IDCtrl=' + sIDCtrl);
            *)
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE stqmovitsa SET Baixa=0 WHERE IDCtrl=' + sIDCtrl);
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE stqmovitsb SET Baixa=0 WHERE IDCtrl=' + sIDCtrl);
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE stqmovvala SET Ativo=0 WHERE IDCtrl=' + sIDCtrl); // serve para algo?
          end;
          //
          QrLocCItsVen.Next;
          //
        end;
      end;
    end;
  end;
  LocCod(Codigo, Codigo);
  QrLocCConIts.Locate('CtrID', CtrID, []);
  // N�o precisa aqui!
  //QrLocCItsSvc.Locate('Item', NewItem, []);
end;

procedure TFmLocCConCab.AlteraTextoMemoWideStringField(Titulo, Campo: String);
var
  FormCaption, Texto: String;
  Codigo: Integer;
begin
  if (QrLocCConCab.State <> dsInactive ) and (QrLocCConCab.RecordCount > 0) then
  begin
    Codigo := QrLocCConCabCodigo.Value;
    Texto := QrLocCConCab.FieldByName(Campo).AsWideString;
    //
    if MyObjects.GetTexto(TFmGetTexto, FmGetTexto, Titulo, Titulo, Texto) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconcab', False, [
      Campo], [
      'Codigo'], [
      Texto], [
      Codigo], True) then
      begin
        FApenasLocCConCab := True;
        LocCod(Codigo, Codigo);
        PCMovimento.ActivePageIndex := PCMov_PageIndex_Obse;
      end;
    end;
  end;
end;

procedure TFmLocCConCab.Alteratolerncia1Click(Sender: TObject);
var
  Hora: TTime;
  HrTolerancia: String;
  Codigo: Integer;
begin
  Hora := 0;
  Codigo := QrLocCConCabCodigo.Value;
  if DBCheck.ObtemHora(Hora, Hora) then
  begin
    HrTolerancia := Geral.FDT(Hora, 100);
    if HrTolerancia = '' then
      HrTolerancia := '00:00:00';
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitslca', False, [
    'HrTolerancia'], ['Codigo'], [HrTolerancia], [Codigo], True) then
    begin
      RecalculaTudo(False, False);
      LocCod(Codigo, Codigo);
    end;
  end
end;

procedure TFmLocCConCab.AtualizaImagens(Codigo, TipImg: Integer; Image: TImage);
var
  Img: String;
begin
  if Codigo <> 0 then
  begin
    Img := DModG.ObtemCaminhoImagem(TipImg, Codigo);
    //
    if FileExists(Img) then
    begin
      Image.Picture.LoadFromFile(Img);
      Image.Visible := True;
    end;
  end else
    Image.Visible := False;
end;

procedure TFmLocCConCab.Atualizatotaidomovimento1Click(Sender: TObject);
var
  Codigo, CtrID: Integer;
begin
  if (QrLocCConIts.State <> dsInactive) and (QrLocCConIts.RecordCount > 0) then
  begin
    Codigo := QrLocCConItsCodigo.Value;
    CtrID  := QrLocCConItsCtrID.Value;
    //
    case TTabConLocIts(QrLocCConItsIDTab.Value) of
      TTabConLocIts.tcliLocacao: AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
      TTabConLocIts.tcliOutroServico: AppPF.AtualizaTotaisLocCConCab_Servicos(Codigo);
      TTabConLocIts.tcliVenda: AppPF.AtualizaTotaisLocCConCab_Vendas(Codigo);
    end;
    //
    LocCod(Codigo, Codigo);
    QrLocCConIts.Locate('CtrID', CtrID, []);
  end;
end;

procedure TFmLocCConCab.BtCabClick(Sender: TObject);
begin
  DBEdNO_STATUS.Font.Style := [fsBold];
  //
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmLocCConCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType     := stLok;
  FAtualizando        := False;
  FApenasLocCConCab   := False;
  FFechando           := False;
  FCalculandoManual   := False;
  GBEdita.Align       := alTop; //alClient;
  PnEntiStatus.Align  := alClient;
  DBGLocIPatPri.Align := alClient;
  PnDados.Align       := alClient;
  PCMovimento.Align   := alClient;
  // deve ser antes do "CriaOForm"
  PCMovimento.ActivePageIndex := PCMov_PageIndex_Loca;
  CriaOForm;
  FSeq := 0;
  QrLocCConCab.DataBase := Dmod.MyDB;
  //
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrClientes, Dmod.MyDB, [
  'SELECT Codigo, IF(Tipo=0, RazaoSocial, ',
  'Nome) NOMEENTIDADE, LimiCred ',
  'FROM entidades ',
  'WHERE Ativo = 1',
  'ORDER BY NOMEENTIDADE ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSenhas, Dmod.MyDB, [
  'SELECT Numero, Login ',
  'FROM senhas ',
  'WHERE Ativo=1 ',
  'ORDER BY Login ',
  '']);
  //
  {
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartaG, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM cartag ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  }
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratos, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM contratos ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  UnDmkDAC_PF.AbreQuery(QrAtndCanal, Dmod.MyDB);
end;

procedure TFmLocCConCab.FormDestroy(Sender: TObject);
begin
  VAR_FmLocCConCab := nil;
  Application.OnHint := FmPrincipal.MyOnHint;
end;

procedure TFmLocCConCab.SbNumeroClick(Sender: TObject);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Codigo(QrLocCConCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmLocCConCab.SbClienteClick(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdCliente.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdCliente, CBCliente, QrClientes, VAR_CADASTRO, 'Codigo');
  EdCliente.SetFocus;
  //
  AtualizaImagens(EdCliente.ValueVariant, 0, ImgCliente);
  AtualizaImagens(EdECComprou.ValueVariant, 1, ImgRequisitante);
  AtualizaImagens(EdECRetirou.ValueVariant, 1, ImgRetirada);
end;

procedure TFmLocCConCab.SbImagensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImagens, SbImagens);
end;

procedure TFmLocCConCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmLocCConCab.SbNomeClick(Sender: TObject);
begin
  PesquisaPorCliente();
end;

procedure TFmLocCConCab.SbNovoClick(Sender: TObject);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Codigo(QrLocCConCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmLocCConCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FFechando := True;
  //Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmLocCConCab.QrLocCConCabAfterOpen(DataSet: TDataSet);
const
  GeraLog = False;
var
  Texto: String;
  DtFimCobra, DtHrDevolver: TDateTime;
begin
  //Geral.MB_Teste(QrLocCConCab.SQL.Text);
  if FApenasLocCConCab then Exit;
  //
  QueryPrincipalAfterOpen;
  if FAtualizando = False then
  begin
    FAtualizando := True;
    try
      DtFimCobra := DmodG.ObtemAgora();
      DtHrDevolver := QrLocCConCabDtHrDevolver.Value;
      if not FCalculandoManual then
      begin
        if (Trunc(QrLocCConCabDtUltAtzPend.Value) < Trunc(DtFimCobra)) then
        begin
          Texto := LaAviso2.Caption;
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando pend�ncias!');
          //Info_Fazer('Precisa atualizar pend�ncias!');
          AppPF.AtualizaPendenciasLocacao(QrItsLca, QrLocCConCabCodigo.Value, QrLocCConCabStatus.Value, QrLocCConCabDtHrEmi.Value,
            DtFimCobra, DtHrDevolver, GeraLog);
          LocCod(QrLocCConCabCodigo.Value, QrLocCConCabCodigo.Value);
          MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
        end else
          //Info_Fazer('N�O Precisa atualizar pend�ncias!')
          ;
      end;
    finally
      FAtualizando := False;
    end;
  end;
end;

procedure TFmLocCConCab.QrLocCConCabAfterScroll(DataSet: TDataSet);
begin
  BtLocCConIts.Enabled := QrLocCConCab.RecordCount > 0;
  BtFat.Enabled := QrLocCConCab.RecordCount > 0;
  //
  if FApenasLocCConCab then
  begin
    FApenasLocCConCab := False;
    //
    Exit;
  end;
  //
  ReopenLocCConIts(0);
  //
  //ReopenLocIPatPri(QrLocIPatPri, QrLocCConCabCodigo.Value, 0, 0);
  ReopenLocFCab(0);
end;

procedure TFmLocCConCab.Faturaeencerracontrato1Click(Sender: TObject);
begin
  MostraFormLocFCab(True);
end;

procedure TFmLocCConCab.Faturamentoparcial1Click(Sender: TObject);
begin
  MostraFormLocFCab(False);
end;

procedure TFmLocCConCab.Finalizado1Click(Sender: TObject);
var
  Codigo, ItensNegativosPatr, ItensNegativosVend: Integer;
  BxaDtHr: TDateTime;
begin
  Codigo  := QrLocCConCabCodigo.Value;
  BxaDtHr := DModG.ObtemAgora();


  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT * FROM loccitslca ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND ManejoLca IN (1,2,3) ',
  'AND QtdeLocacao > 0 ',
  'AND QtdeDevolucao < QtdeProduto ',
  '']);

  if Dmod.QrAux.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('Existem ' + Geral.FF0(Dmod.QrAux.RecordCount) +
    ' itens a serem devolvidos completamente. Deseja encerrar assim mesmo?') = ID_Yes then
    begin
      if not DBCheck.LiberaPelaSenhaBoss() then
        Exit;
    end else
      Exit;
  end;

  if AppPF.MudaStatus_Finalizado(Codigo, QrItsLca, BxaDtHr,
  QrLocCConCabValorPndt.Value) then
  begin
    RecalculaEstoquesPatr(ItensNegativosPatr);
    RecalculaEstoquesVend(ItensNegativosVend);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmLocCConCab.Fonte101Click(Sender: TObject);
begin
  ImprimeDevolucao(False, 10);
end;

procedure TFmLocCConCab.Fonte102Click(Sender: TObject);
begin
  ImprimeDevolucao(True, 10);
end;

procedure TFmLocCConCab.Fonte71Click(Sender: TObject);
begin
  ImprimeDevolucao(False, 7);
end;

procedure TFmLocCConCab.Fonte72Click(Sender: TObject);
begin
  ImprimeDevolucao(True, 7);
end;

function TFmLocCConCab.FormaAluguelPelaDataDeRetono(): Integer;
var
  Dias: Integer;
begin
  Dias := Trunc(TPDtHrDevolver.Date) - Trunc(TPDtHrSai.Date);
  case Dias of
    0..3:  (*RGTipoAluguel.ItemIndex*)Result := 1;
    4..7:  (*RGTipoAluguel.ItemIndex*)Result := 2;
    8..15: (*RGTipoAluguel.ItemIndex*)Result := 3;
    else   (*RGTipoAluguel.ItemIndex*)Result := 4;
  end;
end;

procedure TFmLocCConCab.FormActivate(Sender: TObject);
begin
  if TFmLocCConCab(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end else
  begin
    // Pode ter mudado para DefParams de outo form em Aba!
    DefParams();
  end;
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrLocCConCabCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmLocCConCab.SbQueryClick(Sender: TObject);
begin
  PesquisaPorCliente();
end;

procedure TFmLocCConCab.Selecionado1Click(Sender: TObject);
begin
{
  //
  Application.CreateForm(TFmContratApp2, FmContratApp2);
  //
  FmContratApp2.FLocCConCab := QrLocCConCabCodigo.Value;
  //
  FmContratApp2.ImprimeOrcaPediSelecionado(QrLocCConItsCtrID.Value);
  //
  FmContratApp2.Destroy;
}
end;

procedure TFmLocCConCab.FormResize(Sender: TObject);
begin
(*
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
*)
end;

procedure TFmLocCConCab.FormShow(Sender: TObject);
begin
  // Ini 2020-09-20
  //FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
  // Fim 2020-09-20
end;

procedure TFmLocCConCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrLocCConCab, [PnDados],
  [PnEdita], EdCliente, ImgTipo, 'loccconcab');
  //
  EdEmpresa.ValueVariant  := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue      := DmodG.QrFiliLogFilial.Value;
  EdContrato.ValueVariant := Dmod.QrOpcoesTRenDefContrat.Value;
  CBContrato.KeyValue := Dmod.QrOpcoesTRenDefContrat.Value;
  TPDataEmi.Date := Trunc(Date);
  EdHoraEmi.Text := FormatDateTime('hh:nn:ss', Now());
  TPDtHrDevolver.Date := Trunc(Date + 1);
  EdDtHrDevolver.Text := FormatDateTime('hh:nn:ss', Now());
  TPDtHrSai.Date := Trunc(Date);
  EdDtHrSai.Text := FormatDateTime('hh:nn:ss', Now());
  EdVendedor.ValueVariant := VAR_USUARIO;
  CBVendedor.KeyValue := VAR_USUARIO;
  RGFormaCobrLoca.ItemIndex := Dmod.QrOpcoesTRenFormaCobrLoca.Value;
  //
  ImgCliente.Visible      := False;
  ImgRequisitante.Visible := False;
  ImgRetirada.Visible     := False;
  //
end;

procedure TFmLocCConCab.Cancelado1Click(Sender: TObject);
const
  AvisaEstqNeg = True;
  GeraLog = False;
  SQL_CtrID = '';
var
  Codigo, Status, GraGruX, ItensNegativosPatr, ItensNegativosVend: Integer;
  SQLType: TSQLType;
begin
  SQLType           := stUpd;
  Codigo            := QrLocCConCabCodigo.Value;
  Status            := Integer(TStatusLocacao.statlocCancelado);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconcab', False, [
  'Status'], ['Codigo'], [
  Status], [Codigo], True) then
  begin
    (*
    AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, Null,
    QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
    QrLocCConCabDtHrDevolver.Value, GeraLog);
    *)
    AppPF.ReopenItsLca(QrItsLca, Codigo, SQL_CtrID);
    RecalculaEstoquesPatr(ItensNegativosPatr);
    RecalculaEstoquesVend(ItensNegativosVend);
  end;
end;

procedure TFmLocCConCab.Cancelado2Click(Sender: TObject);
begin
  AlteraStatusMovimento(TStatusMovimento.statmovCancelado);
end;

function TFmLocCConCab.ConfereDataHoraDevolverComPeriodicidade(): Boolean;
var
  DataDev, HoraDev: TDateTime;
  TipoAluguel: Integer;
begin
  Result := True;
  //
  QuantidadeDeDiasFormaAluguel(DataDev, HoraDev);
  TipoAluguel := FormaAluguelPelaDataDeRetono();
  //
  if (Trunc(DataDev) <> Trunc(TPDtHrDevolver.Date))
  or (TipoAluguel <> RGTipoAluguel.ItemIndex) then
    Result := False;
end;

procedure TFmLocCConCab.Contratofonte101Click(Sender: TObject);
begin
  ImprimeContrato(10)
end;

procedure TFmLocCConCab.Contratofonte71Click(Sender: TObject);
begin
  ImprimeContrato(7)
end;

procedure TFmLocCConCab.QrLocCConCabBeforeClose(DataSet: TDataSet);
begin
  QrLocCConIts.Close;
  //
  QrLocFCab.Close;
  BtLocCConIts.Enabled := False;
  BtFat.Enabled := False;
end;

procedure TFmLocCConCab.QrLocCConCabBeforeOpen(DataSet: TDataSet);
begin
  QrLocCConCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmLocCConCab.QrLocCConCabCalcFields(DataSet: TDataSet);
begin
  QrLocCConCabDataEmi.Value := Trunc(QrLocCConCabDtHrEmi.Value);
  QrLocCConCabHoraEmi.Value := QrLocCConCabDtHrEmi.Value - QrLocCConCabDataEmi.Value;
  QrLocCConCabDtHrBxa_TXT.Value :=  Geral.FDT(QrLocCConCabDtHrBxa.Value, 107);
end;

procedure TFmLocCConCab.QrLocCConItsAfterScroll(DataSet: TDataSet);
const
  sProcName = 'TFmLocCConCab.QrLocCConItsAfterScroll()';
begin
  HabilitaBotaoMovimentoAtual();
  QrLocIPatPri.Close;
  QrLocIPatSec.Close;
  QrLocIPatAce.Close;
  QrLocIPatUso.Close;
  QrLocIPatCns.Close;
  QrLocIPatApo.Close;
  QrLocCItsSvc.Close;
  QrLocCItsVen.Close;
  case TTabConLocIts(QrLocCConItsIDTab.Value) of
    tcliLocacao:
    begin
      PCMovimento.ActivePageIndex := PCMov_PageIndex_Loca;
      //
      //Info_Fazer('Fazer 23 A');
      ReopenLocIPatPri(QrLocIPatPri, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
      ReopenLocIPatSec(QrLocIPatSec, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
      ReopenLocIPatAce(QrLocIPatAce, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
      ReopenLocIPatCns(QrLocIPatCns, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
      ReopenLocIPatUso(QrLocIPatUso, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
      ReopenLocIPatApo(QrLocIPatApo, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
    end;
    tcliOutroServico:
    begin
      PCMovimento.ActivePageIndex := PCMov_PageIndex_Serv;
      //
      //Info_Fazer('Fazer 23 B');
      ReopenLocCItsSvc(QrLocCItsSvc, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
    end;
    tcliVenda:
    begin
      PCMovimento.ActivePageIndex := PCMov_PageIndex_Vend;
      //
      //Info_Fazer('Fazer 23 C');
      ReopenLocCItsVen(QrLocCItsVen, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
    end
    else
      Geral.MB_Erro('"IDTab" n�o implementado em ' + sProcName);
  end;
  HabilitaBotaoMovimentoAtual();
end;

procedure TFmLocCConCab.QrLocCConItsBeforeClose(DataSet: TDataSet);
begin
  DesabilitaBotoesMovimento();
  //
  QrLocIPatPri.Close;
  QrLocIPatSec.Close;
  QrLocIPatAce.Close;
  QrLocIPatUso.Close;
  QrLocIPatCns.Close;
  QrLocIPatApo.Close;
  QrLocCItsSvc.Close;
  QrLocCItsVen.Close;
end;

procedure TFmLocCConCab.QrLocCItsVenAfterScroll(DataSet: TDataSet);
begin
  ReopenSmiaVen();
end;

procedure TFmLocCConCab.QrLocCItsVenBeforeClose(DataSet: TDataSet);
begin
  QrSmiaVen.Close;
end;

procedure TFmLocCConCab.QrLocIPatApoAfterOpen(DataSet: TDataSet);
begin
  if QrLocIPatApo.RecordCount > 0 then
    PCMateriais.ActivePageIndex := 2;
end;

procedure TFmLocCConCab.QrLocIPatCnsAfterOpen(DataSet: TDataSet);
begin
  if QrLocIPatCns.RecordCount > 0 then
    PCMateriais.ActivePageIndex := 1;
end;

procedure TFmLocCConCab.QrLocIPatCnsCalcFields(DataSet: TDataSet);
begin
  QrLocIPatCnsQTDUSO.Value :=
    QrLocIPatCnsQtdFim.Value - QrLocIPatCnsQtdIni.Value;
end;

procedure TFmLocCConCab.QrLocIPatPriCalcFields(DataSet: TDataSet);
begin
  QrLocIPatPriTXT_DTA_DEVOL.Value := Geral.FDT(QrLocIPatPriDtHrRetorn.Value, 107);
  QrLocIPatPriTXT_DTA_LIBER.Value := Geral.FDT(QrLocIPatPriLibDtHr.Value, 107);
  QrLocIPatPriTXT_QEM_LIBER.Value := Trim(QrLocIPatPriLOGIN.Value + ' ' +
    QrLocIPatPriRELIB.Value + ' ' +QrLocIPatPriHOLIB.Value);
end;

procedure TFmLocCConCab.QrLocIPatUsoAfterOpen(DataSet: TDataSet);
begin
  if QrLocIPatUso.RecordCount > 0 then
    PCMateriais.ActivePageIndex := 0;
end;

procedure TFmLocCConCab.QrSmiaVenAfterOpen(DataSet: TDataSet);
var
  DataIni: TDateTime;
begin
  if (QrLocCItsVenSMIA_IDCtrl.Value <> 0) and
  (QrSmiaVen.RecordCount =  0) and
  (QrLocCConCabNO_Status.Value = 'ABERTO') then
  begin
    // data desta atualiza��o
    DataIni := EncodeDate(2023, 12, 18);
    if QrSmiaVenDataHora.Value >= DataIni then
      Geral.MB_Erro('Item de pedido de venda sem baixa!' +  sLineBreak +
      'Avise a Dermatek caso o atendimento tenha sido feito nessa vers�o!');
  end;
end;

procedure TFmLocCConCab.QrValAbertoAfterOpen(DataSet: TDataSet);
begin
  DBText1.Font.Color := clRed;
  DBText1.Invalidate;
  if QrValAbertoVencido.Value > 0 then
  begin
    Geral.MB_Aviso('Valor vencido: ' + Geral.FFT(QrValAbertoVencido.Value, 2, siNegativo));
    Timer1.Enabled := True;
  end;
end;

procedure TFmLocCConCab.QrLocFCabAfterScroll(DataSet: TDataSet);
begin
  ReopenLctFatRef(0);
end;

procedure TFmLocCConCab.QrLocFCabBeforeClose(DataSet: TDataSet);
begin
  QrLctFatRef.Close;
end;

procedure TFmLocCConCab.QrLctFatRefCalcFields(DataSet: TDataSet);
begin
  QrLctFatRefPARCELA.value := QrLctFatRef.RecNo;
end;

{

initialization
(*
 TStyleManager.Engine.RegisterStyleHook(TCustomEdit, TEditStyleHookColor);
 TStyleManager.Engine.RegisterStyleHook(TEdit, TEditStyleHookColor);
 TStyleManager.Engine.RegisterStyleHook(TMemo, TMemoStyleHookColor);
 TStyleManager.Engine.RegisterStyleHook(TCustomMemo, TMemoStyleHookColor);
 TStyleManager.Engine.RegisterStyleHook(TCustomMaskEdit, TEditStyleHookColor);
 TStyleManager.Engine.RegisterStyleHook(TMaskEdit, TEditStyleHookColor);
 TStyleManager.Engine.RegisterStyleHook(TCustomLabeledEdit, TEditStyleHookColor);
 TStyleManager.Engine.RegisterStyleHook(TLabeledEdit, TEditStyleHookColor);
 *)

 TStyleManager.Engine.RegisterStyleHook(TEdit, TEditStyleHookColor);
 TStyleManager.Engine.RegisterStyleHook(TColorDBEdit, TEditStyleHookColor);


 TStyleManager.Engine.RegisterStyleHook(TButtonedEdit, TEditStyleHookColor);

 TStyleManager.Engine.RegisterStyleHook(TMemo, TMemoStyleHookColor);
 TStyleManager.Engine.RegisterStyleHook(TMaskEdit, TEditStyleHookColor);
 TStyleManager.Engine.RegisterStyleHook(TLabeledEdit, TEditStyleHookColor);
}

(* uktimo fazer
Fazer 39
*)



{
object OrcamentoSelecionadoVias1: TMenuItem
  Caption = 'Selecionado - 1 via'
  OnClick = OrcamentoSelecionadoVias1Click
end
object OrcamentoSelecionadoVias2: TMenuItem
  Caption = 'Selecionado - 2 vias'
  OnClick = OrcamentoSelecionadoVias2Click
end
object OrcamentoTodosVias1: TMenuItem
  Caption = 'Todos - 1 via'
  OnClick = OrcamentoTodosVias1Click
end
object OrcamentoTodosVias2: TMenuItem
  Caption = 'Todos - 2 vias'
  OnClick = OrcamentoTodosVias2Click
end
object Selecionado1: TMenuItem
  Caption = '&Selecionado'
  Enabled = False
  Visible = False
  OnClick = Selecionado1Click
end
}

//testar loca��o inteira por causa do ValorBruto e ValorBrto!
{
object PedidoSelecionadoVias1: TMenuItem
  Caption = 'Selecionado - 1 via'
  OnClick = PedidoSelecionadoVias1Click
end
object PedidoSelecionadoVias2: TMenuItem
  Caption = 'Selecionado - 2 vias'
  OnClick = PedidoSelecionadoVias2Click
end
object PedidoTodosVias1: TMenuItem
  Caption = 'Todos - 1 via'
  OnClick = PedidoTodosVias1Click
end
object PedidoTodosVias2: TMenuItem
  Caption = 'Todos - 2 vias'
  OnClick = PedidoTodosVias2Click
end
}










{
object QrLocCConItsNFeFatID: TIntegerField
  FieldName = 'NFeFatID'
  Required = True
end
object QrLocCConItsNFeFatNum: TIntegerField
  FieldName = 'NFeFatNum'
  Required = True
end
object QrLocCConItsNFeide_mod: TIntegerField
  FieldName = 'NFeide_mod'
  Required = True
end
object QrLocCConItsNFeide_nNF: TIntegerField
  FieldName = 'NFeide_nNF'
  Required = True
end
object QrLocCConItsNFeNF_Stat: TIntegerField
  FieldName = 'NFeNF_Stat'
  Required = True
end
object QrLocCConItsNFePediVda: TIntegerField
  FieldName = 'NFePediVda'
  Required = True
end
object QrLocCConItsNFeFatPedCab: TIntegerField
  FieldName = 'NFeFatPedCab'
  Required = True
end
object QrLocCConItsNFeFatPedVolCnta: TIntegerField
  FieldName = 'NFeFatPedVolCnta'
  Required = True
end
}

end.

