object FmGraGruEPatNeg: TFmGraGruEPatNeg
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-112 :: Equipamentos com Estoque Negativo'
  ClientHeight = 467
  ClientWidth = 1018
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1018
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 970
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 922
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 454
        Height = 32
        Caption = 'Equipamentos com Estoque Negativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 454
        Height = 32
        Caption = 'Equipamentos com Estoque Negativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 454
        Height = 32
        Caption = 'Equipamentos com Estoque Negativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 353
    Width = 1018
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1014
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 397
    Width = 1018
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 872
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 870
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1018
    Height = 305
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 812
    ExplicitHeight = 467
    object DBGGraGruEPatNeg: TdmkDBGridZTO
      Left = 0
      Top = 0
      Width = 1018
      Height = 305
      Align = alClient
      DataSource = DsGraGruEPatNeg
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Patrimonio'
          Title.Caption = 'Patrim'#244'nio'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Nome do Equipamento'
          Width = 408
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqAnt'
          Title.Caption = 'Qtd anterior'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqEnt'
          Title.Caption = 'Qtd entrada'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqLoc'
          Title.Caption = 'Qtd locado'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqSai'
          Title.Caption = 'Qtd sa'#237'da'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqSdo'
          Title.Caption = 'Qtd saldo'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Referencia'
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object DsGraGruEPatNeg: TDataSource
    DataSet = Dmod.QrGGXEPatNeg
    Left = 384
    Top = 340
  end
  object QrGGXEPatNeg: TMySQLQuery
    SQL.Strings = (
      'SELECT gg1.Nome, gep.* '
      'FROM gragruepat gep'
      'LEFT JOIN gragrux ggx ON ggx.Controle=gep.GraGrux'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE GraGruX IN ('
      '0,1,2'
      ')'
      'AND EstqSdo >-1')
    Left = 436
    Top = 108
    object QrGGXEPatNegNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrGGXEPatNegGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrGGXEPatNegEstqAnt: TFloatField
      FieldName = 'EstqAnt'
      Required = True
    end
    object QrGGXEPatNegEstqEnt: TFloatField
      FieldName = 'EstqEnt'
      Required = True
    end
    object QrGGXEPatNegEstqSai: TFloatField
      FieldName = 'EstqSai'
      Required = True
    end
    object QrGGXEPatNegEstqLoc: TFloatField
      FieldName = 'EstqLoc'
      Required = True
    end
    object QrGGXEPatNegEstqSdo: TFloatField
      FieldName = 'EstqSdo'
      Required = True
    end
    object QrGGXEPatNegLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrGGXEPatNegDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGGXEPatNegDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGGXEPatNegUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrGGXEPatNegUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrGGXEPatNegAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGGXEPatNegAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrGGXEPatNegAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrGGXEPatNegAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
end
