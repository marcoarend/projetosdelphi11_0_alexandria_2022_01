unit LocCItsVen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkGeral, DmkDAC_PF, dmkImage, UnDmkEnums, AppListas,
  Vcl.ComCtrls, dmkEditDateTimePicker, UnAppENums, UnGrl_Consts, Vcl.Menus;

type
  TFmLocCItsVen = class(TForm)
    PainelDados: TPanel;
    DsGraGXVend: TDataSource;
    QrGraGXVend: TMySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    SBCadastro: TSpeedButton;
    CBGraGruX: TdmkDBLookupComboBox;
    Label7: TLabel;
    Label2: TLabel;
    EdReferencia: TdmkEdit;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    QrPesq2: TmySQLQuery;
    QrPesq2Referencia: TWideStringField;
    QrPesq1: TMySQLQuery;
    QrPesq1Controle: TIntegerField;
    QrGraGXVendControle: TIntegerField;
    QrGraGXVendReferencia: TWideStringField;
    QrGraGXVendNO_GG1: TWideStringField;
    QrGraGXVendGraGruX: TIntegerField;
    QrGraGXVendItemValr: TFloatField;
    QrGraGXVendItemUnid: TIntegerField;
    CkContinuar: TCheckBox;
    Label21: TLabel;
    TPDataLoc: TdmkEditDateTimePicker;
    EdHoraLoc: TdmkEdit;
    QrStqCenCad: TMySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    Label5: TLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    QrFatPedCab: TMySQLQuery;
    QrFatPedCabEmpresa: TIntegerField;
    QrFatPedCabCodigo: TIntegerField;
    QrFatPedCabCodUsu: TIntegerField;
    QrFatPedCabPedido: TIntegerField;
    QrFatPedCabCondicaoPG: TIntegerField;
    QrFatPedCabTabelaPrc: TIntegerField;
    QrFatPedCabMedDDReal: TFloatField;
    QrFatPedCabMedDDSimpl: TFloatField;
    QrFatPedCabRegrFiscal: TIntegerField;
    QrFatPedCabNO_TabelaPrc: TWideStringField;
    QrFatPedCabTipMediaDD: TSmallintField;
    QrFatPedCabAFP_Sit: TSmallintField;
    QrFatPedCabAFP_Per: TFloatField;
    QrFatPedCabAssociada: TIntegerField;
    QrFatPedCabCU_PediVda: TIntegerField;
    QrFatPedCabAbertura: TDateTimeField;
    QrFatPedCabEncerrou: TDateTimeField;
    QrFatPedCabFatSemEstq: TSmallintField;
    QrFatPedCabENCERROU_TXT: TWideStringField;
    QrFatPedCabEMP_CtaProdVen: TIntegerField;
    QrFatPedCabEMP_FaturaSeq: TSmallintField;
    QrFatPedCabEMP_FaturaSep: TWideStringField;
    QrFatPedCabEMP_FaturaDta: TSmallintField;
    QrFatPedCabEMP_TxtProdVen: TWideStringField;
    QrFatPedCabEMP_FILIAL: TIntegerField;
    QrFatPedCabASS_CtaProdVen: TIntegerField;
    QrFatPedCabASS_FaturaSeq: TSmallintField;
    QrFatPedCabASS_FaturaSep: TWideStringField;
    QrFatPedCabASS_FaturaDta: TSmallintField;
    QrFatPedCabASS_TxtProdVen: TWideStringField;
    QrFatPedCabASS_NO_UF: TWideStringField;
    QrFatPedCabASS_CO_UF: TIntegerField;
    QrFatPedCabASS_FILIAL: TIntegerField;
    QrFatPedCabCliente: TIntegerField;
    QrFatPedCabSerieDesfe: TIntegerField;
    QrFatPedCabNFDesfeita: TIntegerField;
    QrFatPedCabPEDIDO_TXT: TWideStringField;
    QrFatPedCabID_Pedido: TIntegerField;
    QrFatPedCabJurosMes: TFloatField;
    QrFatPedCabNOMEFISREGCAD: TWideStringField;
    QrFatPedCabEMP_UF: TIntegerField;
    QrFatPedCabPedidoCli: TWideStringField;
    QrCli: TMySQLQuery;
    QrCliE_ALL: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    QrCliIE: TWideStringField;
    QrCliNUMERO: TFloatField;
    QrCliUF: TFloatField;
    QrCliCEP: TFloatField;
    QrCliLograd: TFloatField;
    QrCliindIEDest: TSmallintField;
    QrCliL_Ativo: TSmallintField;
    QrItem: TMySQLQuery;
    QrItemNOMENIVEL1: TWideStringField;
    QrItemGraCorCad: TIntegerField;
    QrItemNOMECOR: TWideStringField;
    QrItemNOMETAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemGraGru1: TIntegerField;
    QrItemCU_Nivel1: TIntegerField;
    QrItemIPI_Alq: TFloatField;
    QrItemMadeBy: TSmallintField;
    QrItemFracio: TSmallintField;
    QrItemHowBxaEstq: TSmallintField;
    QrItemGerBxaEstq: TSmallintField;
    QrItemprod_indTot: TSmallintField;
    QrGraGXVendGraGru1: TIntegerField;
    QrEstoque: TMySQLQuery;
    QrEstoqueQtde: TFloatField;
    Label6: TLabel;
    DBEdQtde: TDBEdit;
    DsEstoque: TDataSource;
    EdQuantidade: TdmkEdit;
    EdValorTotal: TdmkEdit;
    Label4: TLabel;
    Label3: TLabel;
    EdPrcUni: TdmkEdit;
    LaQtdeLocacao: TLabel;
    QrGraGXVendNCM: TWideStringField;
    Label8: TLabel;
    DBEdNCM: TDBEdit;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Edprod_vFrete: TdmkEdit;
    Label9: TLabel;
    Edprod_vSeg: TdmkEdit;
    Label10: TLabel;
    Edprod_vOutro: TdmkEdit;
    Label12: TLabel;
    SbDesconto: TSpeedButton;
    Edprod_vDesc: TdmkEdit;
    Label11: TLabel;
    EdPercDesco: TdmkEdit;
    Label13: TLabel;
    EdValorBruto: TdmkEdit;
    Label14: TLabel;
    QrGraGXVendMaxPercDesco: TFloatField;
    QrGraGXVendUnidMed: TIntegerField;
    EdEAN13: TdmkEdit;
    Label15: TLabel;
    QrGraGXVendEAN13: TWideStringField;
    QrPesq2EAN13: TWideStringField;
    QrPesq1EAN13: TWideStringField;
    SbPreco: TSpeedButton;
    PMPrcUni: TPopupMenu;
    Aumentar1: TMenuItem;
    Diminuir1: TMenuItem;
    SbPsqPrc: TSpeedButton;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    //procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure SBCadastroClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdReferenciaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdQuantidadeRedefinido(Sender: TObject);
    procedure EdPrcUniRedefinido(Sender: TObject);
    procedure EdStqCenCadRedefinido(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbDescontoClick(Sender: TObject);
    procedure EdEAN13Redefinido(Sender: TObject);
    procedure EdEAN13Change(Sender: TObject);
    procedure SbPrecoClick(Sender: TObject);
    procedure EdPercDescoRedefinido(Sender: TObject);
    procedure Diminuir1Click(Sender: TObject);
    procedure Aumentar1Click(Sender: TObject);
    procedure SbPsqPrcClick(Sender: TObject);
  private
    { Private declarations }
    FCriando: Boolean;
    //
    procedure ReopenGraGXVend();
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorEAN13(Limpa: Boolean);
    procedure PesquisaPorReferencia(Limpa: Boolean);
    procedure AtualizaValorTotal();
    function  InsereItem2(const StqCenCad: Integer; const PrecoR, QuantP,
              DescoP, prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
              var OriCtrl, IDCtrl: Integer): Boolean;
    procedure ReopenFatPedCab();
    function  ReopenItem(GraGruX: Integer): Boolean;
    procedure ReopenCli();
    procedure ReopenEstoque();
    procedure ReopenLocCItsVen(Item: Integer);
    function  ObtemPreco(var Preco: Double): Boolean;
  public
    { Public declarations }
    FQrLocCItsVen: TmySQLQuery;
    //FTipo, FGraGruY,
    FCodigo, FCtrID, FItem, FNewCtrID, FNewItem: Integer;
    FDtHrLocado: String;
    FAlterou, FEhPedido: Boolean;
    // Mov estq
    FRegrFiscal, FFatID, FEmpresa, FFatPedCab, FFatPedVolCnta(*, FOriCtrl*): Integer;
    FNomeEmp, FNOMEFISREGCAD, FTmp_FatPedFis: String;
    FStatusLocacao: TStatusLocacao;
    FStatusMovimento: TStatusMovimento;
    //
    procedure ReopenEstqCenCad();
  end;

var
  FmLocCItsVen: TFmLocCItsVen;

implementation

uses UnMyObjects, Module, Principal, UnGraL_Jan, ModProd, UnAppPF,
  UnGrade_PF, UnGrade_Jan, ModuleFatura, UCreate, ModuleGeral, UnDmkProcFunc,
  MeuDBUses, LocCConCab, GetValor, MyDBCheck;

{$R *.DFM}

var
  FCategoria: String;
  FQtdeProduto: Integer;

procedure TFmLocCItsVen.AtualizaValorTotal();
var
  Quantidade, PrcUni, ValorTotal: Double;
  prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  PercDesco, ValorBruto: Double;
begin
  Quantidade     := EdQuantidade.ValueVariant;
  //PrcUni         := FPrcUni;
  PrcUni         := EdPrcUni.ValueVariant;
  ValorBruto     := Quantidade * PrcUni;
  PercDesco      := EdPercDesco.ValueVariant;
  prod_vDesc     := ValorBruto * (PercDesco / 100);
  if prod_vDesc < 0 then
    prod_vDesc := 0;
  //
  prod_vFrete    := Edprod_vFrete.ValueVariant;
  prod_vSeg      := Edprod_vSeg.ValueVariant;
  prod_vOutro    := Edprod_vOutro.ValueVariant;
  //
  ValorTotal     := ValorBruto + (prod_vFrete + prod_vSeg - prod_vDesc + prod_vOutro);
  ValorBruto     := ValorTotal + prod_vDesc;
  //
  Edprod_vDesc.ValueVariant := prod_vDesc;
  EdValorTotal.ValueVariant := ValorTotal;
  EdValorBruto.ValueVariant := ValorBruto;
end;

procedure TFmLocCItsVen.Aumentar1Click(Sender: TObject);
var
  PrcUniAtu, PrcUniNew: Double;
begin
  PrcUniAtu := EdPrcUni.ValueVariant;
  //if DBCheck.LiberaPelaSenhaPwdLibFunc() then
  begin
    if ObtemPreco(PrcUniNew) then
    begin
      if PrcUniNew > PrcUniAtu then
        EdPrcUni.ValueVariant := PrcUniNew
      else
        Geral.MB_Aviso('Pre�o deve ser maior que o atual!');
    end;
  end;
end;

procedure TFmLocCItsVen.BtOKClick(Sender: TObject);
const
  Categoria = 'S';
  OldStatusMovimento = TStatusMovimento.statmovIndefinido;
  AltItem = 0;
var
  Codigo, CtrID, Item, ManejoLca, GraGruX, StqCencad: Integer;
  Quantidade, PrcUni, PercDesco, ValorBruto, ValorTotal: Double;
  SQLType: TSQLType;
  Continua: Boolean;
  OriCtrl, IDCtrl, SMIA_OriCtrl, SMIA_IDCtrl, GraGru1, Unidade: Integer;
  PrecoR, QuantP, DescoP, Estoque, Saldo: Double;
  sIDCtrl, NCM: String;
  //
  prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  NewStatusMovimento: TStatusMovimento;
  DescoNaoPermitido: Boolean;
begin
  SMIA_IDCtrl    := 0;
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  CtrID          := FCtrID;
  Item           := FItem;
  ManejoLca      := Integer(TManejoLca.manelcaVenda);
  GraGruX        := EdGraGruX.ValueVariant;
  Quantidade     := EdQuantidade.ValueVariant;
  PrcUni         := EdPrcUni.ValueVariant;
  ValorTotal     := EdValorTotal.ValueVariant;
  //Categoria      := 'S';  acima!
  prod_vFrete    := Edprod_vFrete.ValueVariant;
  prod_vSeg      := Edprod_vSeg.ValueVariant;
  prod_vDesc     := Edprod_vDesc.ValueVariant;
  prod_vOutro    := Edprod_vOutro.ValueVariant;
  //
  PercDesco      := EdPercDesco.ValueVariant;
  ValorBruto     := EdValorBruto.ValueVariant;

  DescoNaoPermitido := (PercDesco - QrGraGXVendMaxPercDesco.Value) > 0.000000000099;

  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina a Mercadoria!') then Exit;
  if MyObjects.FIC(Quantidade = 0, EdQuantidade, 'Informe a quantidade!') then Exit;
  if MyObjects.FIC(DescoNaoPermitido, EdPercDesco,
  'Percentual de desconto acima do permitido � necess�rio liberar por senha especial!') then
  begin
    if not DBCheck.LiberaPelaSenhaPwdLibFunc() then Exit;
  end;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCencad,
    'Informe o Centro de estoque!', 'Codigo', 'CodUsu') then Exit;
  //
(*
Obrigat�ria informa��o do NCM completo (8 d�gitos).
Nota: Em caso de item de servi�o ou item que n�o tenham produto
(ex. transfer�ncia de cr�dito, cr�dito do ativo imobilizado, etc.),
informar o valor 00 (dois zeros). (NT 2014/004)
*)
  NCM := Geral.SoNumero_TT(QrGraGXVendNCM.Value);
  GraGru1 := QrGraGXVendGraGru1.Value;
  Unidade := QrGraGXVendUnidMed.Value;
  NewStatusMovimento := TStatusMovimento(FStatusMovimento);
  //
  if (Length(NCM) <> 8) and (NCM <> '00') then
  begin
   if Geral.MB_Pergunta(
   'NCM n�o definido corretamente! Deseja alterar o NCM do produto agora?') =
    ID_YES then
    begin
      Grade_Jan.MostraFormGraGruN_Dados(GraGru1, TSubForm.tsfDadosFiscais);
      //
      ReopenGraGXVend();
      if QrGraGXVend.Locate('GraGruX', GraGruX, []) then
      begin
        EdGraGruX.ValueVariant := GraGruX;
        CBGraGruX.KeyValue     := GraGruX;
      end;
      //
      Exit;
    end;
  end;
  //
  if AppPF.InsAltLocCItsVen(SQLType, Codigo, CtrID, FFatID, FEmpresa,
  FFatPedCab, FFatPedVolCnta, TManejoLca(ManejoLca), GraGru1, Unidade, GraGruX,
  StqCencad, CO_005_StepNFePedido, Quantidade, PrcUni, PercDesco, ValorBruto,
  ValorTotal, prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro,
  OldStatusMovimento, NewStatusMovimento,
  AltItem, SMIA_IDCtrl, // 2023-12-16
  FAlterou, FNewCtrID, FNewItem, FTmp_FatPedFis) then
  begin
    AppPF.VerificaEstoqueVenda(FEmpresa, FFatPedCab, CO_005_StepNFePedido,
    Integer(FStatusLocacao), Integer(NewStatusMovimento));
    if CkContinuar.Checked then
    begin
      ReopenLocCItsVen(Item);
      //
      EdGraGruX.ValueVariant := 0;
      CBGraGruX.KeyValue := 0;
      EdQuantidade.ValueVariant := 0;
      EdPrcUni.ValueVariant := 0;
      Edprod_vFrete.ValueVariant := 0.00;
      Edprod_vSeg.ValueVariant := 0.00;
      Edprod_vDesc.ValueVariant := 0.00;
      Edprod_vOutro.ValueVariant := 0.00;
      //
      EdGraGruX.SetFocus;
    end else
    begin
      AppPF.AtualizaTotaisLocCConCab_Vendas(FCodigo);
      ReopenLocCItsVen(Item);
      Close;
    end;
  end;
end;

procedure TFmLocCItsVen.BtSaidaClick(Sender: TObject);
begin
  VAR_SELCOD := 0;
  AppPF.AtualizaTotaisLocCConCab_Vendas(FCodigo);
  Close;
end;

procedure TFmLocCItsVen.Diminuir1Click(Sender: TObject);
var
  PrcUni: Double;
begin
  if DBCheck.LiberaPelaSenhaPwdLibFunc() then
  begin
    PrcUni := EdValorBruto.ValueVariant;
    if ObtemPreco(PrcUni) then
      EdPrcUni.ValueVariant := PrcUni;
  end;
end;

procedure TFmLocCItsVen.EdEAN13Change(Sender: TObject);
begin
  if EdEAN13.Focused then
    PesquisaPorEAN13(False);
end;

procedure TFmLocCItsVen.EdEAN13Redefinido(Sender: TObject);
var
  EAN13: String;
begin
  EAN13 := Geral.SoNumero_TT(StringReplace(EdEAN13.ValueVariant, #$D#$A, '', [rfReplaceAll]));
  if EAN13 <> EdEAN13.ValueVariant then
    EdEAN13.ValueVariant := EAN13;
end;

procedure TFmLocCItsVen.EdGraGruXChange(Sender: TObject);
(*
var
  EAN13: String;
  Tam: Integer;
begin
  if EdGraGruX.Focused then
  begin
    EAN13 := EdGraGruX.Text;
    Tam := Length(EAN13);
    if Tam >= 9 then
    begin
     // if Tam = 13 then
      begin
        EdGraGruX.Text := '';
        EdEAN13.Text := EAN13;
        EdEAN13.SetFocus;
      end;
      //
      Exit;
    end;
  end;
*)
begin
  if not EdReferencia.Focused then
    PesquisaPorGraGruX();
  if EdGraGruX.ValueVariant <> 0 then
    DBEdNCM.DataField := 'NCM'
  else
    DBEdNCM.DataField := '';
end;

procedure TFmLocCItsVen.EdGraGruXRedefinido(Sender: TObject);
begin
  EdPercDesco.ValueVariant := 0;
  if ImgTipo.SQLType = stIns then
  begin
    EdPrcUni.ValueVariant := QrGraGXVendItemValr.Value;
    //FPrcUni               := QrGraGXVendItemValr.Value;
  end;
  ReopenEstoque();
end;

procedure TFmLocCItsVen.EdPercDescoRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmLocCItsVen.EdPrcUniRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmLocCItsVen.EdQuantidadeRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmLocCItsVen.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
end;

procedure TFmLocCItsVen.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmLocCItsVen.EdStqCenCadRedefinido(Sender: TObject);
begin
  ReopenEstoque();
end;

procedure TFmLocCItsVen.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCItsVen.FormShow(Sender: TObject);
begin
  ReopenGraGXVend;
end;

function TFmLocCItsVen.InsereItem2(const StqCenCad: Integer; const PrecoR,
  QuantP, DescoP, prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  var OriCtrl, IDCtrl: Integer): Boolean;
// copiado de: procedure TFmFatDivGru2.InsereItem2(Col, Row: Integer);
const
  Ativa = True;
var
  NFe_FatID, Cliente, RegrFiscal, GraGruX,
  //StqCenCad,
  FatSemEstq, AFP_Sit: Integer;
  AFP_Per: Double;
  Cli_Tipo: Integer;
  Cli_IE: String;
  Cli_UF, EMP_UF, EMP_FILIAL,
  ASS_CO_UF, ASS_FILIAL, Item_MadeBy: Integer;
  Item_IPI_ALq: Double;
  Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
  Preco_MedidaA, Preco_MedidaE: Double;
  Preco_MedOrdem: Integer;
  SQLType: TSQLType;
  PediVda: Integer;
  TabelaPrc, OriCodi, Empresa, OriCnta, Associada, OriPart, InfAdCuztm: Integer;
  NO_tablaPrc: String;
  //
  Falta, Pecas, AreaM2, AreaP2, Peso: Double;
  Msg: String;
//var
  ErrPreco, AvisoPrc, QtdRed, Casas, Nivel1, Codigo, Controle: Integer;
  //PrecoR, QuantP, DescoP,
  Tot, Qtd, (*PrecoO,*) PrecoF, ValBru, ValLiq, DescoV, DescoI: Double;
  //
// var
  Qtde: Double;
  TipoCalc, prod_indTot, GraGru1: Integer;
begin
  Result := False;
  //
  InfAdCuztm := 0;
(*
  //StqCenCad        := Geral.IMV(EdStqCenCad.Text);
  if not UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCencad,
    'Informe o Centro de estoque!', 'Codigo', 'CodUsu') then Exit;
*)
  //
  NFe_FatID        := FFatID;
  Cliente          := (*FmFatDivGer2.*)QrFatPedCabCliente.Value; //QrCliCodigo.Value;
  RegrFiscal       := (*FmFatDivGer2.*)QrFatPedCabRegrFiscal.Value; //QrPediVdaRegrFiscal.Value;
  GraGruX          := QrItemGraGruX.Value;
  prod_indTot      := QrItemprod_indTot.Value;
  FatSemEstq       := (*FmFatDivGer2.*)QrFatPedCabFatSemEstq.Value;
  AFP_Sit          := (*FmFatDivGer2.*)QrFatPedCabAFP_Sit.Value;
  AFP_Per          := (*FmFatDivGer2.*)QrFatPedCabAFP_Per.Value;
  Cli_Tipo         := (*FmFatDivGer2.*)QrCliTipo.Value;
  Cli_IE           := (*FmFatDivGer2.*)QrCliIE.Value;
  Cli_UF           := Trunc((*FmFatDivGer2.*)QrCliUF.Value);
  EMP_UF           := (*FmFatDivGer2.*)QrFatPedCabEMP_UF.Value;
  EMP_FILIAL       := (*FmFatDivGer2.*)QrFatPedCabEMP_FILIAL.Value;
  ASS_CO_UF        := (*FmFatDivGer2.*)QrFatPedCabASS_CO_UF.Value;
  ASS_FILIAL       := (*FmFatDivGer2.*)QrFatPedCabASS_FILIAL.Value;
  Item_MadeBy      := QrItemMadeBy.Value;
  ITEM_IPI_Alq     := QrItemIPI_Alq.Value;
  Empresa          := (*FmFatDivGer2.*)QrFatPedCabEmpresa.Value;
  //
  //TipoCalc := Geral.IMV(Grade0.Cells[Col, Row]);
  TipoCalc := DmProd.DefineTipoCalc((*FmFatDivGer2.*)QrFatPedCabRegrFiscal.Value,
                StqCenCad, Empresa);
  //
  if TipoCalc = -1 then
    Exit;
  //
  Casas    := Dmod.QrControleCasasProd.Value;
  //PrecoO   := Geral.DMV(GradeL.Cells[Col, Row]);
  //PrecoR   := Geral.DMV(GradeF.Cells[Col, Row]);
  //QuantP   := Geral.DMV(GradeQ.Cells[Col, Row]);
  //DescoP   := Geral.DMV(GradeD.Cells[Col, Row]);
  DescoI   := dmkPF.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
  PrecoF   := PrecoR - DescoI;
  DescoV   := DescoI * QuantP;
  ValBru   := PrecoR * QuantP;
  ValLiq   := ValBru - DescoV;
//
  Preco_PrecoF     := (*QrPreco*)PrecoF(*.Value*);
  Preco_PercCustom := 0;//(*QrPreco*)PercCustom(*.Value*);
  Preco_MedidaC    := 0;//(*QrPreco*)MedidaC(*.Value*);
  Preco_MedidaL    := 0;//(*QrPreco*)MedidaL(*.Value*);
  Preco_MedidaA    := 0;//(*QrPreco*)MedidaA(*.Value*);
  Preco_MedidaE    := 0;//(*QrPreco*)MedidaE(*.Value*);
  Preco_MedOrdem   := 0;//(*QrPreco*)MedOrdem(*.Value*);
//
  //
  SQLType          := ImgTipo.SQLType;
  PediVda          := (*FmFatDivGer2.*)QrFatPedCabPedido.Value;
  TabelaPrc        := (*FmFatDivGer2.*)QrFatPedCabTabelaPrc.Value;
  NO_tablaPrc      := (*FmFatDivGer2.*)QrFatPedCabNO_TabelaPrc.Value;
  OriCodi          := (*FmFatDivGer2.*)QrFatPedCabCodigo.Value;
  OriCnta          := FFatPedVolCnta; //(*FmFatDivGer2.*)QrFatPedVolCnta.Value;
  Associada        := (*FmFatDivGer2.*)QrFatPedCabAssociada.Value;
  //
  if (QrItem.State = dsInactive) or (QrItem.RecordCount = 0) then
  begin
    Geral.MB_Aviso('Reduzido n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  Qtde := QuantP;
(*
  if FMultiGrandeza then
  begin
    Pecas  := - Geral.DMV(Grade1.Cells[Col, Row]);
    Peso   := - Geral.DMV(Grade2.Cells[Col, Row]);
    AreaM2 := - Geral.DMV(Grade3.Cells[Col, Row]);
    AreaP2 := - Geral.DMV(Grade4.Cells[Col, Row]);
    //
    if not Grade_PF.ValidaGrandeza(TipoCalc, QrItemHowBxaEstq.Value, Pecas,
      AreaM2, AreaP2, Peso, Msg) then
    begin
      Geral.MB_Aviso(Msg);
      Exit;
    end;
  end else*) begin
    Pecas  := 0;
    AreaM2 := 0;
    AreaP2 := 0;
    Peso   := 0;
  end;
  IDCtrl := 0;
  OriPart := 0;
  //
  if DmFatura.InsereItemStqMov(FFatID, OriCodi, OriCnta, Empresa, Cliente,
    Associada, RegrFiscal, GraGruX, NO_tablaPrc, FTmp_FatPedFis, StqCenCad,
    FatSemEstq, AFP_Sit, AFP_Per, Qtde, Cli_Tipo, Cli_IE, Cli_UF, EMP_UF,
    EMP_FILIAL, ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq, Preco_PrecoF,
    Preco_PercCustom, Preco_MedidaC, Preco_MedidaL, Preco_MedidaA, Preco_MedidaE,
    Preco_MedOrdem, SQLType, PediVda, OriPart, InfAdCuztm, (*TipoNF*)0,
    (*modNF*)0, (*Serie*)0, (*nNF*)0, (*SitDevolu*)0, (*Servico*)0, (*refNFe*)'',
    0, OriCtrl, Pecas, AreaM2, AreaP2, Peso, TipoCalc, prod_indTot,
    prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro,
    IDCtrl, Ativa) then
  begin
    GraGru1 := QrGraGXVendGraGru1.Value;
    //
    Grade_PF.InsereFatPedFis(FTmp_FatPedFis, GraGru1, IDCtrl, FFatID,
      OriCodi, OriCtrl, OriCnta, OriPart, RegrFiscal);
    //
    Result := True;
  end;
end;

function TFmLocCItsVen.ObtemPreco(var Preco: Double): Boolean;
var
  ResVar: Variant;
  CasasDecimais: Integer;
begin
  Result        := False;
  CasasDecimais := 4;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  0, CasasDecimais, 0, '', '', True, 'Pre�o', 'Informe o pre�o: ',
  0, ResVar) then
  begin
    Preco := Geral.DMV(ResVar);
    Result := True;
  end;
end;

procedure TFmLocCItsVen.PesquisaPorEAN13(Limpa: Boolean);
var
  EAN13: String;
begin
  EAN13 := EdEAN13.Text;
  if Length(EAN13) = 13 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
    'SELECT ggx.Controle, ggx.EAN13 ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
    'WHERE ggx.EAN13="' + EAN13 + '"' ,
    'AND NOT (ggo.GraGruX IS NULL)',
    'AND ggo.Aplicacao <> 0 ',
    //
    'AND ggx.Ativo=1 ',
    '']);
    //
    //Geral.MB_Teste(QrPesq1.SQL.Text);
    //
    QrPesq1.Open;
    if QrPesq1.RecordCount > 0 then
    begin
      if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
      begin
        EdGraGruX.ValueVariant := QrPesq1Controle.Value;
      end;
      if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
        CBGraGruX.KeyValue := QrPesq1Controle.Value;
      //
      if QrEstoqueQtde.Value > 0 then
        DBEdQtde.SetFocus
      else
        Geral.MB_Info('Mercadoria sem estoque positivo!');
    end else if Limpa then
    begin
      EdEAN13.ValueVariant := '';
      EdReferencia.ValueVariant := '';
    end;
  end;
end;

procedure TFmLocCItsVen.PesquisaPorGraGruX;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia, ggx.EAN13 ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  ggo.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (ggo.GraGruX IS NULL) ',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
    //
    if EdEAN13.ValueVariant <> QrPesq2EAN13.Value then
      EdEAN13.ValueVariant := QrPesq2EAN13.Value;
    //
  end else
  begin
    EdReferencia.ValueVariant := '';
    EdEAN13.ValueVariant := '';
  end;
end;

procedure TFmLocCItsVen.PesquisaPorReferencia(Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle, ggx.EAN13 ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
  'AND NOT (ggo.GraGruX IS NULL)',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdEAN13.ValueVariant <> QrPesq1EAN13.Value then
      EdEAN13.ValueVariant := QrPesq1EAN13.Value;
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
  begin
    EdReferencia.ValueVariant := '';
    EdEAN13.ValueVariant := '';
  end;
end;

procedure TFmLocCItsVen.ReopenCli();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCli, Dmod.MyDB, [
  'SELECT en.Codigo, Tipo, CodUsu, IE, indIEDest, ',
  'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOME_ENT,  ',
  'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNPJ_CPF,  ',
  'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_RG,  ',
  'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA,  ',
  'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0.000 NUMERO, ',
  'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COMPL, ',
  'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAIRRO, ',
  'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CIDADE, ',
  'CASE WHEN en.Tipo=0 THEN en.EUF         ELSE en.PUF      END + 0.000 UF, ',
  'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOMELOGRAD, ',
  'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOMEUF, ',
  'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pais, ',
  'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0.000 Lograd, ',
  'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0.000 CEP, ',
  'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END ENDEREF, ',
  'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1, ',
  'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX, ',
  'L_Ativo  ',
  'FROM entidades en  ',
  'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
  'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
  'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
  'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
  'WHERE en.Codigo=' + Geral.FF0(QrFatPedCabCliente.Value),
  '']);
end;

procedure TFmLocCItsVen.ReopenEstoque;
var
  GraGruX, StqCenCad: Integer;
begin
  GraGruX    := EdGraGruX.ValueVariant;
  StqCenCad  := EdStqCenCad.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstoque, Dmod.MyDB, [
  'SELECT SUM(Qtde * Baixa) Qtde ',
  'FROM stqmovitsa ',
  'WHERE Ativo=1 ',
  'AND ValiStq <> 101',
  'AND GraGruX=' + Geral.FF0(GraGruX),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND StqCenCad=' + Geral.FF0(StqCenCad),
  '']);
end;

procedure TFmLocCItsVen.ReopenEstqCenCad();
begin
  if not Grade_PF.ReopenFatStqCenCad(FRegrFiscal, FEmpresa, FNomeEmp,
    FNOMEFISREGCAD, QrStqCenCad) then
  begin
    if Geral.MB_Pergunta('Deseja configurar a Regra Fiscal agora?') = ID_YES then
    begin
      Grade_Jan.MostraFormFisRegCad(FRegrFiscal);
      UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
    end;
  end;
  //
end;

procedure TFmLocCItsVen.ReopenFatPedCab();
begin
  if FFatPedCab <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrFatPedCab, Dmod.MyDB, [
    'SELECT pvd.CodUsu CU_PediVda, pvd.Empresa,  ',
    'pvd.TabelaPrc, pvd.CondicaoPG, pvd.RegrFiscal, ',
    'pvd.AFP_Sit, pvd.AFP_Per, pvd.Cliente, ',
    'pvd.Codigo ID_Pedido, pvd.PedidoCli,  ',
    ' ',
    'pvd.idDest, pvd.indFinal, pvd.indPres, pvd.indSinc,  ',
    ' ',
    'ppc.JurosMes, frc.Nome NOMEFISREGCAD, ',
    'ppc.MedDDReal, ppc.MedDDSimpl, ',
    'par.TipMediaDD, par.Associada, par.FatSemEstq, ',
    ' ',
    'par.CtaProdVen EMP_CtaProdVen, ',
    'par.FaturaSeq EMP_FaturaSeq, ',
    'par.FaturaSep EMP_FaturaSep, ',
    'par.FaturaDta EMP_FaturaDta, ',
    'par.TxtProdVen EMP_TxtProdVen, ',
    'emp.Filial EMP_FILIAL, ',
    'ufe.Codigo EMP_UF, ',
    ' ',
    'ass.CtaProdVen ASS_CtaProdVen, ',
    'ass.FaturaSeq ASS_FaturaSeq, ',
    'ass.FaturaSep ASS_FaturaSep, ',
    'ass.FaturaDta ASS_FaturaDta, ',
    'ass.TxtProdVen ASS_TxtProdVen, ',
    'ufa.Nome ASS_NO_UF, ',
    'ufa.Codigo ASS_CO_UF, ',
    'ase.Filial ASS_FILIAL, ',
    ' ',
    'tpc.Nome NO_TabelaPrc, fpc.* ',
    'FROM fatpedcab fpc ',
    'LEFT JOIN pedivda    pvd ON pvd.Codigo=fpc.Pedido ',
    'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG ',
    'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc ',
    'LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa ',
    'LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada ',
    'LEFT JOIN entidades  ase ON ase.Codigo=ass.Codigo ',
    'LEFT JOIN entidades  emp ON emp.Codigo=par.Codigo ',
    'LEFT JOIN ufs        ufe ON ufe.Codigo=IF(emp.Tipo=0, emp.EUF, emp.PUF) ',
    'LEFT JOIN ufs        ufa ON ufa.Codigo=IF(ase.Tipo=0, ase.EUF, ase.PUF) ',
    'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal ',
    'WHERE fpc.Codigo=' + Geral.FF0(FFatPedCab),
    //'AND pvd.CodUsu < 0  ',
    '']);
    //
    ReopenCli();
    FTmp_FatPedFis := UCriar.RecriaTempTableNovo(ntrttFatPedFis, DmodG.QrUpdPID1, False);
    //
  end;
end;

procedure TFmLocCItsVen.ReopenGraGXVend();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXVend, Dmod.MyDB, [
  'SELECT ggx.Controle, ggx.GraGru1, ggx.EAN13, ',
  'gg1.Referencia, gg1.Nome NO_GG1, gg1.NCM, gg1.UnidMed, ',
  'ggo.GraGruX, ggo.ItemValr, ggo.ItemUnid, ggo.MaxPercDesco ',
  'FROM gragxvend ggo  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE ggx.Ativo=1 ',
  'ORDER BY NO_GG1',
  '']);
end;

function TFmLocCItsVen.ReopenItem(GraGruX: Integer): Boolean;
const
  sProcName = 'TFmLocCItsVen.ReopenItem()';
begin
  Result := False;
  QrItem.Close;
  if GraGruX > 0 then
  begin
    QrItem.Params[0].AsInteger := GraGrux;
    UMyMod.AbreQuery(QrItem, Dmod.MyDB, sProcName);
    //
    if QrItem.RecordCount > 0 then
    begin
      Result := True;
    end else
      Geral.MB_Aviso('Reduzido n�o localizado!');
  end;
end;

procedure TFmLocCItsVen.ReopenLocCItsVen(Item: Integer);
begin
  if (FQrLocCItsVen <> nil) and (FQrLocCItsVen.State <> dsInactive) then
  begin
    UnDmkDAC_PF.AbreQuery(FQrLocCItsVen, Dmod.MyDB);
    FQrLocCItsVen.Locate('Item', Item, []);
  end;
end;

procedure TFmLocCItsVen.SBCadastroClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  GraL_Jan.MostraFormGraGXVend(EdGraGruX.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrGraGXVend, VAR_CADASTRO, 'Controle');
  PesquisaPorGraGruX();
  EdReferencia.SetFocus;
end;

procedure TFmLocCItsVen.SbDescontoClick(Sender: TObject);
  function ObtemDesconto(var Desco: Double): Boolean;
  var
    ResVar: Variant;
    CasasDecimais: Integer;
  begin
    Desco         := 0;
    Result        := False;
    CasasDecimais := 2;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    0, CasasDecimais, 0, '', '', True, 'Desconto', 'Informe o valor de  desconto: ',
    0, ResVar) then
    begin
      Desco := Geral.DMV(ResVar);
      Result := True;
    end;
  end;
var
  prod_vDesc, PercDesco, ValorBruto: Double;
begin
  ValorBruto := EdValorBruto.ValueVariant;
  if ValorBruto > 0 then
  begin
    if ObtemDesconto(prod_vDesc) then
    begin
      PercDesco := (prod_vDesc / ValorBruto) * 100;
      if PercDesco > 100 then
        PercDesco := 100;
      EdPercDesco.ValueVariant := PercDesco;
    end;
  end else
    Geral.MB_Aviso('Valor bruto n�o definido!');
end;

procedure TFmLocCItsVen.SbPrecoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPrcUni, SbPreco);
end;

procedure TFmLocCItsVen.SbPsqPrcClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  if AppPF.PesquisaGraGXVend(GraGruX) then
  begin
    EdGraGruX.ValueVariant := GraGruX;
    CBGraGruX.KeyValue     := GraGruX;
  end;
end;

procedure TFmLocCItsVen.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FCriando then
  begin
    FCriando := False;
    if QrStqCenCad.RecordCount = 1 then
    begin
      EdStqCenCad.ValueVariant := QrStqCenCadCodigo.Value;
      CBStqCenCad.KeyValue     := QrStqCenCadCodigo.Value;
      //
      try
        //EdGraGruX.SetFocus;
        EdEAN13.SetFocus;
      except
        ; // nada
      end;
    end;
  end;
end;

procedure TFmLocCItsVen.FormCreate(Sender: TObject);
begin
  FCriando        := True;
  ImgTipo.SQLType := stLok;
  //
  FEhPedido    := False;
  FAlterou     := False;
  FCategoria   := 'V';
  FQtdeProduto := -1;
  TPDataLoc.Date := Trunc(Date);
  EdHoraLoc.Text := FormatDateTime('hh:nn', Now());
  //
end;

end.
