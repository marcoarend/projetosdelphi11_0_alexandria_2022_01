object FmLocados: TFmLocados
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-115 :: Gerenciamento de Loca'#231#245'es Abertas'
  ClientHeight = 629
  ClientWidth = 995
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 995
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 947
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 899
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 457
        Height = 32
        Caption = ' Gerenciamento de Loca'#231#245'es Abertas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 457
        Height = 32
        Caption = ' Gerenciamento de Loca'#231#245'es Abertas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 457
        Height = 32
        Caption = ' Gerenciamento de Loca'#231#245'es Abertas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 995
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 991
      Height = 21
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PB1: TProgressBar
      Left = 2
      Top = 36
      Width = 991
      Height = 17
      Align = alBottom
      TabOrder = 1
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 995
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 849
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 847
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 995
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 41
      Width = 995
      Height = 415
      Align = alClient
      DataSource = DsLocados
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = dmkDBGridZTO1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Contrato'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TipoAluguel'
          Title.Caption = 'Periodicidade'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtHrSai_TXT'
          Title.Caption = 'Retirada'
          Width = 95
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtHrDevolver_TXT'
          Title.Caption = 'Devolu'#231#227'o'
          Width = 61
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtHrBxa_TXT'
          Title.Caption = 'Baixa'
          Width = 61
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cliente'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CLIENTE'
          Title.Caption = 'Nome do cliente'
          Width = 400
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtUltAtzPend'
          Title.Caption = #218'lt.atualiz.pend'
          Width = 61
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorLocAtualizado'
          Title.Caption = '$ Loc.atualizado'
          Width = 80
          Visible = True
        end>
    end
    object RGStatus: TRadioGroup
      Left = 0
      Top = 0
      Width = 995
      Height = 41
      Align = alTop
      Caption = ' Status: '
      Columns = 10
      ItemIndex = 0
      Items.Strings = (
        '0 - Indefinido'
        '1 - Aberto'
        '2 - Locado'
        '3 - Encerrado'
        '4 - Cancelado'
        '5 - Suspenso')
      TabOrder = 1
      OnClick = RGStatusClick
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object Qr_: TMySQLQuery
    Database = Dmod.MyDB
    Left = 384
    Top = 284
  end
  object Ds_: TDataSource
    DataSet = Qr_
    Left = 384
    Top = 340
  end
  object QrLocados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT com.Nome NO_COMPRADOR, ret.Nome NO_RECEBEU,'
      'lcc.*, sen.Login NO_LOGIN, cin.CodCliInt Filial,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE'
      'FROM loccconcab lcc'
      'LEFT JOIN entidades emp ON emp.Codigo=lcc.Empresa'
      'LEFT JOIN enticliint cin ON cin.CodEnti=emp.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=lcc.Cliente'
      'LEFT JOIN senhas sen ON sen.Numero=lcc.Vendedor'
      'LEFT JOIN enticontat com ON com.Controle=lcc.ECComprou'
      'LEFT JOIN enticontat ret ON ret.Controle=lcc.ECRetirou'
      'WHERE lcc.Codigo > 0')
    Left = 32
    Top = 248
    object QrLocadosEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrLocadosNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrLocadosNO_COMPRADOR: TWideStringField
      FieldName = 'NO_COMPRADOR'
      Size = 30
    end
    object QrLocadosNO_RECEBEU: TWideStringField
      FieldName = 'NO_RECEBEU'
      Size = 30
    end
    object QrLocadosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocadosContrato: TIntegerField
      FieldName = 'Contrato'
    end
    object QrLocadosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLocadosDtHrEmi: TDateTimeField
      FieldName = 'DtHrEmi'
      DisplayFormat = 'dd/mm/yyyy hh:nn'
    end
    object QrLocadosDtHrBxa: TDateTimeField
      FieldName = 'DtHrBxa'
      DisplayFormat = 'dd/mm/yyyy hh:nn'
    end
    object QrLocadosVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLocadosECComprou: TIntegerField
      FieldName = 'ECComprou'
    end
    object QrLocadosECRetirou: TIntegerField
      FieldName = 'ECRetirou'
    end
    object QrLocadosNumOC: TWideStringField
      FieldName = 'NumOC'
      Size = 60
    end
    object QrLocadosValorTot: TFloatField
      FieldName = 'ValorTot'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosLocalObra: TWideStringField
      DisplayWidth = 100
      FieldName = 'LocalObra'
      Size = 50
    end
    object QrLocadosObs0: TWideStringField
      FieldName = 'Obs0'
      Size = 50
    end
    object QrLocadosObs1: TWideStringField
      FieldName = 'Obs1'
      Size = 50
    end
    object QrLocadosObs2: TWideStringField
      FieldName = 'Obs2'
      Size = 50
    end
    object QrLocadosNO_LOGIN: TWideStringField
      FieldName = 'NO_LOGIN'
      Required = True
      Size = 30
    end
    object QrLocadosNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrLocadosEndCobra: TWideStringField
      FieldName = 'EndCobra'
      Size = 100
    end
    object QrLocadosLocalCntat: TWideStringField
      FieldName = 'LocalCntat'
      Size = 50
    end
    object QrLocadosLocalFone: TWideStringField
      FieldName = 'LocalFone'
    end
    object QrLocadosFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrLocadosTipoAluguel: TWideStringField
      FieldName = 'TipoAluguel'
      Size = 1
    end
    object QrLocadosHistImprime: TWideMemoField
      FieldName = 'HistImprime'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocadosHistNaoImpr: TWideMemoField
      FieldName = 'HistNaoImpr'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocadosDtHrSai: TDateTimeField
      FieldName = 'DtHrSai'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn'
    end
    object QrLocadosValorAdicional: TFloatField
      FieldName = 'ValorAdicional'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorAdiantamento: TFloatField
      FieldName = 'ValorAdiantamento'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorDesconto: TFloatField
      FieldName = 'ValorDesconto'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrLocadosDtUltCobMens: TDateField
      FieldName = 'DtUltCobMens'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn'
    end
    object QrLocadosImportado: TSmallintField
      FieldName = 'Importado'
      Required = True
    end
    object QrLocadosDtHrDevolver: TDateTimeField
      FieldName = 'DtHrDevolver'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn'
    end
    object QrLocadosValorEquipamentos: TFloatField
      FieldName = 'ValorEquipamentos'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosFormaCobrLoca: TSmallintField
      FieldName = 'FormaCobrLoca'
      Required = True
    end
    object QrLocadosNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 30
    end
    object QrLocadosNO_TipoAluguel: TWideStringField
      FieldName = 'NO_TipoAluguel'
      Size = 30
    end
    object QrLocadosDtHrSai_TXT: TWideStringField
      FieldName = 'DtHrSai_TXT'
    end
    object QrLocadosDtHrDevolver_TXT: TWideStringField
      FieldName = 'DtHrDevolver_TXT'
    end
    object QrLocadosDtUltAtzPend: TDateField
      FieldName = 'DtUltAtzPend'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrLocadosValorASerAdiantado: TFloatField
      FieldName = 'ValorASerAdiantado'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorProdutos: TFloatField
      FieldName = 'ValorProdutos'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorLocAAdiantar: TFloatField
      FieldName = 'ValorLocAAdiantar'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorLocAtualizado: TFloatField
      FieldName = 'ValorLocAtualizado'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorServicos: TFloatField
      FieldName = 'ValorServicos'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorPago: TFloatField
      FieldName = 'ValorPago'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorVendas: TFloatField
      FieldName = 'ValorVendas'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorOrca: TFloatField
      FieldName = 'ValorOrca'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorPedi: TFloatField
      FieldName = 'ValorPedi'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorFatu: TFloatField
      FieldName = 'ValorFatu'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosValorBrto: TFloatField
      FieldName = 'ValorBrto'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocadosJaLocou: TSmallintField
      FieldName = 'JaLocou'
    end
    object QrLocadosDtHrBxa_TXT: TWideStringField
      FieldName = 'DtHrBxa_TXT'
    end
  end
  object DsLocados: TDataSource
    DataSet = QrLocados
    Left = 32
    Top = 296
  end
  object QrItsLca: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM loccitslca'
      'WHERE Codigo>0')
    Left = 432
    Top = 360
    object QrItsLcaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrItsLcaCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrItsLcaItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrItsLcaManejoLca: TSmallintField
      FieldName = 'ManejoLca'
      Required = True
    end
    object QrItsLcaGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrItsLcaValorDia: TFloatField
      FieldName = 'ValorDia'
      Required = True
    end
    object QrItsLcaValorSem: TFloatField
      FieldName = 'ValorSem'
      Required = True
    end
    object QrItsLcaValorQui: TFloatField
      FieldName = 'ValorQui'
      Required = True
    end
    object QrItsLcaValorMes: TFloatField
      FieldName = 'ValorMes'
      Required = True
    end
    object QrItsLcaRetFunci: TIntegerField
      FieldName = 'RetFunci'
      Required = True
    end
    object QrItsLcaRetExUsr: TIntegerField
      FieldName = 'RetExUsr'
      Required = True
    end
    object QrItsLcaDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
      Required = True
    end
    object QrItsLcaDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
      Required = True
    end
    object QrItsLcaLibExUsr: TIntegerField
      FieldName = 'LibExUsr'
      Required = True
    end
    object QrItsLcaLibFunci: TIntegerField
      FieldName = 'LibFunci'
      Required = True
    end
    object QrItsLcaLibDtHr: TDateTimeField
      FieldName = 'LibDtHr'
      Required = True
    end
    object QrItsLcaRELIB: TWideStringField
      FieldName = 'RELIB'
    end
    object QrItsLcaHOLIB: TWideStringField
      FieldName = 'HOLIB'
      Size = 5
    end
    object QrItsLcaQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrItsLcaValorProduto: TFloatField
      FieldName = 'ValorProduto'
      Required = True
    end
    object QrItsLcaQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrItsLcaValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
    end
    object QrItsLcaQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
    end
    object QrItsLcaTroca: TSmallintField
      FieldName = 'Troca'
      Required = True
    end
    object QrItsLcaCategoria: TWideStringField
      FieldName = 'Categoria'
      Required = True
      Size = 1
    end
    object QrItsLcaCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 40
    end
    object QrItsLcaCobrancaConsumo: TFloatField
      FieldName = 'CobrancaConsumo'
      Required = True
    end
    object QrItsLcaCobrancaRealizadaVenda: TWideStringField
      FieldName = 'CobrancaRealizadaVenda'
      Required = True
      Size = 1
    end
    object QrItsLcaCobranIniDtHr: TDateTimeField
      FieldName = 'CobranIniDtHr'
      Required = True
    end
    object QrItsLcaSaiDtHr: TDateTimeField
      FieldName = 'SaiDtHr'
      Required = True
    end
    object QrItsLcaDMenos: TIntegerField
      FieldName = 'DMenos'
    end
    object QrItsLcaValorFDS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ValorFDS'
      Calculated = True
    end
    object QrItsLcaValorLocAtualizado: TFloatField
      FieldName = 'ValorLocAtualizado'
    end
    object QrItsLcaValDevolParci: TFloatField
      FieldName = 'ValDevolParci'
    end
    object QrItsLcaOriSubstSaiDH: TDateTimeField
      FieldName = 'OriSubstSaiDH'
    end
    object QrItsLcaHrTolerancia: TTimeField
      FieldName = 'HrTolerancia'
    end
  end
end
