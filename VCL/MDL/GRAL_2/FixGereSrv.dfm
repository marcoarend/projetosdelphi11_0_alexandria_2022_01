object FmFixGereSrv: TFmFixGereSrv
  Left = 339
  Top = 185
  Caption = 'FIX-GEREN-004 :: Equipamento - Servi'#231'o'
  ClientHeight = 440
  ClientWidth = 617
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 293
    Width = 617
    Height = 33
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 617
    Height = 64
    Align = alTop
    Caption = ' Dados do equipamento:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 24
      Height = 13
      Caption = 'O.S.:'
      FocusControl = DBEdCodigo
    end
    object Label4: TLabel
      Left = 72
      Top = 20
      Width = 58
      Height = 13
      Caption = 'ID conserto:'
      FocusControl = DBEdControle
    end
    object Label3: TLabel
      Left = 156
      Top = 20
      Width = 65
      Height = 13
      Caption = 'Equipamento:'
      FocusControl = DBEdNO_EQUI
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdControle: TDBEdit
      Left = 72
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'Controle'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DBEdNO_EQUI: TDBEdit
      Left = 156
      Top = 36
      Width = 449
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'NO_EQUI'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 617
    Height = 181
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 51
      Height = 13
      Caption = 'ID do item:'
    end
    object Label7: TLabel
      Left = 12
      Top = 96
      Width = 67
      Height = 13
      Caption = 'Complemento:'
    end
    object Label1: TLabel
      Left = 12
      Top = 56
      Width = 23
      Height = 13
      Caption = 'Item:'
    end
    object SbCodItem: TSpeedButton
      Left = 584
      Top = 72
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbCodItemClick
    end
    object Label2: TLabel
      Left = 100
      Top = 136
      Width = 68
      Height = 13
      Caption = 'Pre'#231'o unit'#225'rio:'
    end
    object Label8: TLabel
      Left = 228
      Top = 136
      Width = 86
      Height = 13
      Caption = 'Desconto unit'#225'rio:'
    end
    object Label9: TLabel
      Left = 356
      Top = 136
      Width = 67
      Height = 13
      Caption = 'Custo unit'#225'rio:'
      Enabled = False
    end
    object Label11: TLabel
      Left = 484
      Top = 136
      Width = 55
      Height = 13
      Caption = 'Custo itens:'
      Enabled = False
    end
    object Label10: TLabel
      Left = 12
      Top = 136
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object EdConta: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Conta'
      UpdCampo = 'Conta'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdDescri: TdmkEdit
      Left = 12
      Top = 112
      Width = 593
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Descri'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object CBCodItem: TdmkDBLookupComboBox
      Left = 68
      Top = 72
      Width = 513
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsFixServCad
      TabOrder = 2
      dmkEditCB = EdCodItem
      QryCampo = 'CodItem'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCodItem: TdmkEditCB
      Left = 12
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'CodItem'
      UpdCampo = 'CodItem'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdCodItemChange
      DBLookupComboBox = CBCodItem
      IgnoraDBLookupComboBox = False
    end
    object EdPreco: TdmkEdit
      Left = 100
      Top = 152
      Width = 120
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'Preco'
      UpdCampo = 'Preco'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnChange = EdPrecoChange
    end
    object EdDesco: TdmkEdit
      Left = 228
      Top = 152
      Width = 120
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'Desco'
      UpdCampo = 'Desco'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnChange = EdDescoChange
    end
    object EdCusto: TdmkEdit
      Left = 356
      Top = 152
      Width = 120
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'Custo'
      UpdCampo = 'Custo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdValor: TdmkEdit
      Left = 484
      Top = 152
      Width = 120
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'Valor'
      UpdCampo = 'Valor'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdQtde: TdmkEdit
      Left = 12
      Top = 152
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'Qtde'
      UpdCampo = 'Qtde'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnChange = EdQtdeChange
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 617
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 569
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 521
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 274
        Height = 32
        Caption = 'Equipamento - Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 274
        Height = 32
        Caption = 'Equipamento - Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 274
        Height = 32
        Caption = 'Equipamento - Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 326
    Width = 617
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 613
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 370
    Width = 617
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 471
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 469
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrFixServCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Preco'
      'FROM fixservcad'
      'ORDER BY Nome')
    Left = 8
    Top = 12
    object QrFixServCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFixServCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFixServCadPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object DsFixServCad: TDataSource
    DataSet = QrFixServCad
    Left = 36
    Top = 12
  end
end
