unit LocCItsLcAd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw;

type
  TFmLocCItsLcAd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrLocCItsLcAd: TMySQLQuery;
    DsLocCItsLcAd: TDataSource;
    Panel2: TPanel;
    QrLocCItsLcAdSubItm: TIntegerField;
    QrLocCItsLcAdData: TDateField;
    QrLocCItsLcAdNovVal: TFloatField;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FItem: Integer;
    procedure ReopenLocCItsLcAd();
  end;

  var
  FmLocCItsLcAd: TFmLocCItsLcAd;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmLocCItsLcAd.BtOKClick(Sender: TObject);
var
  DtHrAbert, DtHrFecha, ZtatusDtH: String;
  Codigo, Local, NrOP, SeqGrupo, NrReduzidoOP, OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed, ZtatusIsp, ZtatusMot: Integer;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType?;
{
  Codigo         := ;
  Local          := ;
  NrOP           := ;
  SeqGrupo       := ;
  NrReduzidoOP   := ;
  DtHrAbert      := ;
  DtHrFecha      := ;
  OVcYnsMed      := ;
  OVcYnsChk      := ;
  LimiteChk      := ;
  LimiteMed      := ;
  ZtatusIsp      := ;
  ZtatusDtH      := ;
  ZtatusMot      := ;

  //
? := UMyMod.BuscaEmLivreY_Def('ovgispgercab', 'Codigo', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('ovgispgercab', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovgispgercab', auto_increment?[
'Local', 'NrOP', 'SeqGrupo',
'NrReduzidoOP', 'DtHrAbert', 'DtHrFecha',
'OVcYnsMed', 'OVcYnsChk', 'LimiteChk',
'LimiteMed', 'ZtatusIsp', 'ZtatusDtH',
'ZtatusMot'], [
'Codigo'], [
Local, NrOP, SeqGrupo,
NrReduzidoOP, DtHrAbert, DtHrFecha,
OVcYnsMed, OVcYnsChk, LimiteChk,
LimiteMed, ZtatusIsp, ZtatusDtH,
ZtatusMot], [
Codigo], UserDataAlterweb?, IGNORE?
}
end;

procedure TFmLocCItsLcAd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCItsLcAd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCItsLcAd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmLocCItsLcAd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCItsLcAd.ReopenLocCItsLcAd();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCItsLcAd, Dmod.MyDB, [
  'SELECT SubItm, Data, NovVal ',
  'FROM loccitslcad ',
  'WHERE Item=',
  'ORDER BY Data DESC, SubItm ',
  '']);
end;

end.
