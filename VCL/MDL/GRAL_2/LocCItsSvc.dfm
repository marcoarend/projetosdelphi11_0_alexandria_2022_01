object FmLocCItsSvc: TFmLocCItsSvc
  Left = 450
  Top = 286
  Caption = 'LOC-PATRI-106 :: Adi'#231#227'o de Servi'#231'o '#224' Loca'#231#227'o'
  ClientHeight = 350
  ClientWidth = 749
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 749
    Height = 188
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object SBCadastro: TSpeedButton
      Left = 719
      Top = 90
      Width = 23
      Height = 21
      Caption = '...'
      OnClick = SBCadastroClick
    end
    object Label7: TLabel
      Left = 167
      Top = 74
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label2: TLabel
      Left = 70
      Top = 74
      Width = 55
      Height = 13
      Caption = 'Refer'#234'ncia:'
    end
    object Label1: TLabel
      Left = 14
      Top = 74
      Width = 48
      Height = 13
      Caption = 'Reduzido:'
    end
    object Label21: TLabel
      Left = 14
      Top = 31
      Width = 134
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data / hora loca'#231#227'o / troca:'
      Enabled = False
    end
    object LaQtdeLocacao: TLabel
      Left = 15
      Top = 118
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label3: TLabel
      Left = 91
      Top = 118
      Width = 54
      Height = 13
      Caption = 'Pre'#231'o unit.:'
    end
    object Label4: TLabel
      Left = 175
      Top = 118
      Width = 49
      Height = 13
      Caption = 'Valor item:'
      Enabled = False
    end
    object Label5: TLabel
      Left = 571
      Top = 118
      Width = 55
      Height = 13
      Caption = 'Movimento:'
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 167
      Top = 90
      Width = 550
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_GG1'
      ListSource = DsGraGXServ
      TabOrder = 4
      dmkEditCB = EdGraGruX
      UpdType = utYes
      LocF7SQLText.Strings = (
        'SELECT lpc.GraGruX _CODIGO, gg1.REFERENCIA, gg1.Nome _NOME'
        'FROM gragxserv lpc'
        'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX'
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
        'WHERE gg1.Nome LIKE "%$#%"'
        'OR gg1.Referencia LIKE "%$#%"'
        'OR gg1.Patrimonio LIKE "%$#%"'
        'AND ggx.Ativo=1')
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdReferencia: TdmkEdit
      Left = 70
      Top = 90
      Width = 97
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdReferenciaChange
    end
    object EdGraGruX: TdmkEditCB
      Left = 14
      Top = 90
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraGruXChange
      OnRedefinido = EdGraGruXRedefinido
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CkContinuar: TCheckBox
      Left = 18
      Top = 163
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo'
      TabOrder = 8
    end
    object TPDataLoc: TdmkEditDateTimePicker
      Left = 14
      Top = 47
      Width = 99
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 41131.724786689820000000
      Time = 41131.724786689820000000
      Enabled = False
      TabOrder = 0
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdHoraLoc: TdmkEdit
      Left = 116
      Top = 47
      Width = 41
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Enabled = False
      TabOrder = 1
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfLong
      HoraFormat = dmkhfShort
      Texto = '00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdQuantidade: TdmkEdit
      Left = 16
      Top = 134
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdQuantidadeRedefinido
    end
    object EdPrcUni: TdmkEdit
      Left = 92
      Top = 134
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdPrcUniRedefinido
    end
    object EdValorTotal: TdmkEdit
      Left = 176
      Top = 134
      Width = 92
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object RGAtrelamento: TdmkRadioGroup
      Left = 272
      Top = 112
      Width = 293
      Height = 45
      Caption = ' Atrelamento de frete: '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'Indefinido'
        'Nenhum'
        'Loca'#231#227'o'
        'Venda')
      TabOrder = 9
      OnClick = RGAtrelamentoClick
      UpdType = utYes
      OldValor = 0
    end
    object EdAtrelCtrID: TdmkEditCB
      Left = 572
      Top = 136
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBAtrelCtrID
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBAtrelCtrID: TdmkDBLookupComboBox
      Left = 644
      Top = 136
      Width = 97
      Height = 21
      KeyField = 'CtrID'
      ListField = 'CtrID'
      ListSource = DsLCI
      TabOrder = 11
      dmkEditCB = EdAtrelCtrID
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 749
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 701
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 9
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 653
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 353
        Height = 32
        Caption = 'Adi'#231#227'o de Servi'#231'o '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 353
        Height = 32
        Caption = 'Adi'#231#227'o de Servi'#231'o '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 353
        Height = 32
        Caption = 'Adi'#231#227'o de Servi'#231'o '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 236
    Width = 749
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 745
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 280
    Width = 749
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 603
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 601
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DsGraGXServ: TDataSource
    DataSet = QrGraGXServ
    Left = 488
    Top = 54
  end
  object QrGraGXServ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Referencia, gg1.Nome NO_GG1, '
      'ggo.GraGruX, ggo.ItemValr, ggo.ItemUnid'
      'FROM gragxoutr ggo '
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggo.Aplicacao=:P0')
    Left = 488
    Top = 6
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGXServControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXServReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXServNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXServGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGXServItemValr: TFloatField
      FieldName = 'ItemValr'
    end
    object QrGraGXServItemUnid: TIntegerField
      FieldName = 'ItemUnid'
    end
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  cpl.GraGruX=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 600
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  gg1.Referencia=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 572
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrLCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lci.CtrID'
      'FROM loccconits lci'
      'WHERE lci.Codigo=27719'
      'AND IDTab=2'
      'ORDER BY CtrID')
    Left = 412
    Top = 28
    object QrLCICtrID: TIntegerField
      FieldName = 'CtrID'
    end
  end
  object DsLCI: TDataSource
    DataSet = QrLCI
    Left = 412
    Top = 76
  end
end
