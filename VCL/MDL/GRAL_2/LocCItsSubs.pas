unit LocCItsSubs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, UnAppEnums, Vcl.Mask, dmkEditDateTimePicker, dmkCheckBox,
  dmkDBLookupComboBox, dmkEditCB, AppListas, dmkMemo;

type
  TFmLocCItsSubs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    DsItsLca: TDataSource;
    QrItsLca: TMySQLQuery;
    Memo1: TMemo;
    QrItsLcaCodigo: TIntegerField;
    QrItsLcaCtrID: TIntegerField;
    QrItsLcaItem: TIntegerField;
    QrItsLcaManejoLca: TSmallintField;
    QrItsLcaGraGruX: TIntegerField;
    QrItsLcaValorDia: TFloatField;
    QrItsLcaValorSem: TFloatField;
    QrItsLcaValorQui: TFloatField;
    QrItsLcaValorMes: TFloatField;
    QrItsLcaRetFunci: TIntegerField;
    QrItsLcaRetExUsr: TIntegerField;
    QrItsLcaDtHrLocado: TDateTimeField;
    QrItsLcaDtHrRetorn: TDateTimeField;
    QrItsLcaLibExUsr: TIntegerField;
    QrItsLcaLibFunci: TIntegerField;
    QrItsLcaLibDtHr: TDateTimeField;
    QrItsLcaRELIB: TWideStringField;
    QrItsLcaHOLIB: TWideStringField;
    QrItsLcaQtdeProduto: TIntegerField;
    QrItsLcaValorProduto: TFloatField;
    QrItsLcaQtdeLocacao: TIntegerField;
    QrItsLcaValorLocacao: TFloatField;
    QrItsLcaQtdeDevolucao: TIntegerField;
    QrItsLcaTroca: TSmallintField;
    QrItsLcaCategoria: TWideStringField;
    QrItsLcaCOBRANCALOCACAO: TWideStringField;
    QrItsLcaCobrancaConsumo: TFloatField;
    QrItsLcaCobrancaRealizadaVenda: TWideStringField;
    QrItsLcaCobranIniDtHr: TDateTimeField;
    QrItsLcaSaiDtHr: TDateTimeField;
    QrItsLcaValorFDS: TFloatField;
    QrItsLcaDMenos: TSmallintField;
    QrItsLcaValorLocAAdiantar: TFloatField;
    QrItsLcaValorLocAtualizado: TFloatField;
    QrItsLcaCalcAdiLOCACAO: TWideStringField;
    QrItsLcaDiasUteis: TIntegerField;
    QrItsLcaDiasFDS: TIntegerField;
    QrItsLcaLk: TIntegerField;
    QrItsLcaDataCad: TDateField;
    QrItsLcaDataAlt: TDateField;
    QrItsLcaUserCad: TIntegerField;
    QrItsLcaUserAlt: TIntegerField;
    QrItsLcaAlterWeb: TSmallintField;
    QrItsLcaAWServerID: TIntegerField;
    QrItsLcaAWStatSinc: TSmallintField;
    QrItsLcaAtivo: TSmallintField;
    QrItsLcaValDevolParci: TFloatField;
    QrItsLcaNO_GGX: TWideStringField;
    QrItsLcaREFERENCIA: TWideStringField;
    QrItsLcaLOGIN: TWideStringField;
    QrItsLcaPatrimonio: TWideStringField;
    Label18: TLabel;
    QrGraGXPatr: TMySQLQuery;
    QrGraGXPatrEstqSdo: TFloatField;
    QrGraGXPatrControle: TIntegerField;
    QrGraGXPatrReferencia: TWideStringField;
    QrGraGXPatrCOD_GG1: TIntegerField;
    QrGraGXPatrNO_GG1: TWideStringField;
    QrGraGXPatrCPL_EXISTE: TFloatField;
    QrGraGXPatrSituacao: TWordField;
    QrGraGXPatrAtualValr: TFloatField;
    QrGraGXPatrValorMes: TFloatField;
    QrGraGXPatrValorQui: TFloatField;
    QrGraGXPatrValorSem: TFloatField;
    QrGraGXPatrValorDia: TFloatField;
    QrGraGXPatrAgrupador: TIntegerField;
    QrGraGXPatrMarca: TIntegerField;
    QrGraGXPatrModelo: TWideStringField;
    QrGraGXPatrSerie: TWideStringField;
    QrGraGXPatrVoltagem: TWideStringField;
    QrGraGXPatrPotencia: TWideStringField;
    QrGraGXPatrCapacid: TWideStringField;
    QrGraGXPatrNO_SITAPL: TIntegerField;
    DsGraGXPatr: TDataSource;
    QrGraGXPatrValorFDS: TFloatField;
    QrLocCMovAll: TMySQLQuery;
    QrLocCMovAllCodigo: TIntegerField;
    QrLocCMovAllGTRTab: TIntegerField;
    QrLocCMovAllCtrID: TIntegerField;
    QrLocCMovAllItem: TIntegerField;
    QrLocCMovAllControle: TIntegerField;
    QrLocCMovAllSEQUENCIA: TIntegerField;
    QrLocCMovAllDataHora: TDateTimeField;
    QrLocCMovAllUSUARIO: TWideStringField;
    QrLocCMovAllGraGruX: TIntegerField;
    QrLocCMovAllTipoES: TWideStringField;
    QrLocCMovAllQuantidade: TIntegerField;
    QrLocCMovAllTipoMotiv: TSmallintField;
    QrLocCMovAllCOBRANCALOCACAO: TWideStringField;
    QrLocCMovAllVALORLOCACAO: TFloatField;
    QrLocCMovAllLIMPO: TWideStringField;
    QrLocCMovAllSUJO: TWideStringField;
    QrLocCMovAllQUEBRADO: TWideStringField;
    QrLocCMovAllTESTADODEVOLUCAO: TWideStringField;
    QrLocCMovAllGGXEntrada: TLargeintField;
    QrLocCMovAllMotivoTroca: TIntegerField;
    QrLocCMovAllObservacaoTroca: TWideStringField;
    QrLocCMovAllQuantidadeLocada: TIntegerField;
    QrLocCMovAllQuantidadeJaDevolvida: TIntegerField;
    QrLocCMovAllLk: TIntegerField;
    QrLocCMovAllDataCad: TDateField;
    QrLocCMovAllDataAlt: TDateField;
    QrLocCMovAllUserCad: TIntegerField;
    QrLocCMovAllUserAlt: TIntegerField;
    QrLocCMovAllAlterWeb: TSmallintField;
    QrLocCMovAllAWServerID: TIntegerField;
    QrLocCMovAllAWStatSinc: TSmallintField;
    QrLocCMovAllAtivo: TSmallintField;
    QrLocCMovAllNO_ManejoLca: TWideStringField;
    Panel5: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit17: TDBEdit;
    Label17: TLabel;
    SbPrecosContrato: TSpeedButton;
    Label19: TLabel;
    DBEdit18: TDBEdit;
    Label20: TLabel;
    DBEdit19: TDBEdit;
    Label22: TLabel;
    DBEdit20: TDBEdit;
    Label23: TLabel;
    DBEdit21: TDBEdit;
    Label24: TLabel;
    DBEdit22: TDBEdit;
    SBPrecosGerais: TSpeedButton;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    Label28: TLabel;
    EdControle: TdmkEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBEdit13: TDBEdit;
    Label13: TLabel;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label21: TLabel;
    TPVolta: TdmkEditDateTimePicker;
    EdVolta: TdmkEdit;
    SbVolta: TSpeedButton;
    EdValorLocAtualizado: TdmkEdit;
    Label25: TLabel;
    CkLimpo: TdmkCheckBox;
    CkSujo: TdmkCheckBox;
    CkQuebrado: TdmkCheckBox;
    CkTestadoDevolucao: TdmkCheckBox;
    Label29: TLabel;
    EdCOBRANCALOCACAO: TdmkEdit;
    EdQtdDevol: TdmkEdit;
    LaQtdeDevol: TLabel;
    Label27: TLabel;
    EdValorLocacaoDev: TdmkEdit;
    Panel6: TPanel;
    QrLocTroMotCad: TMySQLQuery;
    DsLocTroMotCad: TDataSource;
    Label30: TLabel;
    EdMotivoTroca: TdmkEditCB;
    CBMotivoTroca: TdmkDBLookupComboBox;
    QrLocTroMotCadCodigo: TIntegerField;
    QrLocTroMotCadNome: TWideStringField;
    QrGraGXToll: TMySQLQuery;
    QrGraGXTollControle: TIntegerField;
    QrGraGXTollReferencia: TWideStringField;
    QrGraGXTollNO_GG1: TWideStringField;
    QrGraGXTollGraGruX: TIntegerField;
    QrGraGXTollItemValr: TFloatField;
    QrGraGXTollItemUnid: TIntegerField;
    DsGraGXToll: TDataSource;
    PainelDados: TPanel;
    QrGraGXPatrNivel2: TIntegerField;
    Panel7: TPanel;
    Label34: TLabel;
    TPDataLoc: TdmkEditDateTimePicker;
    EdHoraLoc: TdmkEdit;
    Label33: TLabel;
    EdGraGruX: TdmkEditCB;
    EdPatrimonio: TdmkEdit;
    Label37: TLabel;
    Label32: TLabel;
    EdReferencia: TdmkEdit;
    Label31: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    SBCadastro: TSpeedButton;
    SpeedButton1: TSpeedButton;
    Label38: TLabel;
    EdValorLocacaoNew: TdmkEdit;
    EdQtdeProduto: TdmkEdit;
    LaQtdeLocacao: TLabel;
    Label35: TLabel;
    EdQtdeLocacao: TdmkEdit;
    DbEdEstqSdo: TDBEdit;
    Label36: TLabel;
    Label39: TLabel;
    EdValorTotalLocacaoItem: TdmkEdit;
    Panel8: TPanel;
    MeObservacaoTroca: TdmkMemo;
    Label26: TLabel;
    SbValorLocacaoDev: TSpeedButton;
    QrItsLcaOriSubstItem: TIntegerField;
    QrItsLcaOriSubstSaiDH: TDateTimeField;
    SBDataLoc: TSpeedButton;
    QrItsLcaHrTolerancia: TTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtPulaClick(Sender: TObject);
    procedure SBPrecosGeraisClick(Sender: TObject);
    procedure QrItsLcaAfterScroll(DataSet: TDataSet);
    procedure SbVoltaClick(Sender: TObject);
    procedure SbPrecosContratoClick(Sender: TObject);
    procedure EdQtdDevolRedefinido(Sender: TObject);
    procedure EdValorLocAtualizadoRedefinido(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdPatrimonioChange(Sender: TObject);
    procedure EdPatrimonioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdQtdeLocacaoRedefinido(Sender: TObject);
    procedure EdValorLocacaoNewRedefinido(Sender: TObject);
    procedure SBCadastroClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SbValorLocacaoDevClick(Sender: TObject);
    procedure SBDataLocClick(Sender: TObject);
  private
    { Private declarations }
    FDtHrLocado: String;
    //
    procedure CalculaDadosAtual();
    procedure CalculaValorLocacao();
    //procedure MostraProximoItem();
    function IncluiLocCItsMat(var Item, GraGruX: Integer): Boolean;
    procedure CalculaValorTotalLocacaoItem();
  public
    { Public declarations }
    FCriando: Boolean;
    FCodigo, FCtrID, FItem, FControleAlt, FNewControle, FEmpresa: Integer;
    FTipoMotiv: TTipoMotivLocMov;
    FQuantidadeAlt: Double;
    FRecalcular: Boolean;
    //
    FDtHrDevolver: TDateTime;
    //
    FTipoTool: TGraGXToolRnt;
    FTipoAluguel: String;
    FGraGruY: Integer;
    FQrLocIPatSec,
    FQrLocIPatApo,
    FQrLocIPatCns,
    FQrLocIPatUso,
    FQrLocIPatAce: TmySQLQuery;
    //
    procedure ReopenItsLca();
    procedure ReopenLocCMovAll();
  end;

  var
  FmLocCItsSubs: TFmLocCItsSubs;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnAppPF, ModuleGeral, UMySQLModule,
  MyDBCheck, UndmkProcFunc, ModProd, MeuDBUses, UnGraL_Jan;

{$R *.DFM}

procedure TFmLocCItsSubs.BtConfirmaClick(Sender: TObject);
var
  SQLType: TSQLType;
  DataHora, USUARIO, TipoES, COBRANCALOCACAO, LIMPO, SUJO, QUEBRADO,
  TESTADODEVOLUCAO, ObservacaoTroca, Tabela: String;

  Codigo, (*GTRTab,*) CtrID, OriItem, DstItem, Controle, SEQUENCIA, OriGGX,
  DstGGX, Quantidade, TipoMotiv, MotivoTroca, QuantidadeLocada,
  QuantidadeJaDevolvida: Integer;

  VALORLOCACAO, GGXEntrada: Double;
  //
  ManejoLca, GTRTab: Integer;
  QtdsIguais: Boolean;
  //
  sValor: String;
begin
  SQLType               := ImgTipo.SQLType;
  Codigo                := FCodigo;
  CtrID                 := FCtrID;
  OriItem               := QrItsLcaItem.Value;
  Controle              := EdControle.ValueVariant;
  SEQUENCIA             := -1;
  DataHora              := Geral.FDT_TP_Ed(TPVolta.Date, EdVolta.Text);
  USUARIO               := '';
  OriGGX                := QrItsLcaGraGruX.Value;
  TipoES                := 'E'; // Entrada
  Quantidade            := EdQtdDevol.ValueVariant;
  TipoMotiv             := Integer(FTipoMotiv);
  COBRANCALOCACAO       := EdCOBRANCALOCACAO.Text;
  VALORLOCACAO          := EdValorLocAtualizado.ValueVariant;
  LIMPO                 := dmkPF.EscolhaDe2Str(CkLimpo.Checked, 'S', 'N');
  SUJO                  := dmkPF.EscolhaDe2Str(CkSujo.Checked, 'S', 'N');
  QUEBRADO              := dmkPF.EscolhaDe2Str(CkQuebrado.Checked, 'S', 'N');
  TESTADODEVOLUCAO      := dmkPF.EscolhaDe2Str(CkTestadoDevolucao.Checked, 'S', 'N');
  GGXEntrada            := 0;
  MotivoTroca           := EdMotivoTroca.ValueVariant;
  ObservacaoTroca       := MeObservacaoTroca.Text;
  QuantidadeLocada      := QrItsLcaQtdeProduto.Value;
  QuantidadeJaDevolvida := QrItsLcaQtdeDevolucao.Value;
  ManejoLca             := QrItsLcaManejoLca.Value;
  //
  if not AppPF.LocCItsRet_DadosDevolucaoOK(EdQtdDevol,
  FQuantidadeAlt, QuantidadeLocada, QuantidadeJaDevolvida) then Exit;
  //
  QtdsIguais := EdQtdDevol.ValueVariant = EdQtdeProduto.ValueVariant;
  if MyObjects.FIC(QtdsIguais = False, EdQtdDevol, 'Quantidade "' +
  LaQtdeDevol.Caption + '" e "' + LaQtdeLocacao.Caption +'" deve ser igual') then Exit;
  //
  if MyObjects.FIC(MotivoTroca = 0, EdMotivoTroca, 'Informe o motivo') then Exit;
  //
  if AppPF.ObtemTabelaDeGraGruY_Movimento(FGraGruY, Tabela) then
  begin
    if IncluiLocCItsMat(DstItem, DstGGX) then
    begin
      if AppPF.LocCItsRet_InsereLocCMovAll(SQLType, DataHora, USUARIO, TipoES,
      COBRANCALOCACAO, LIMPO, SUJO, QUEBRADO, TESTADODEVOLUCAO, ObservacaoTroca,
      Codigo, ManejoLca, CtrID, OriItem, SEQUENCIA, OriGGX, Quantidade, TipoMotiv,
      MotivoTroca, QuantidadeLocada, QuantidadeJaDevolvida, VALORLOCACAO,
      GGXEntrada, DstItem, Controle, GTRTab) then
      begin
        Dmod.MyDB.Execute('UPDATE ' + Tabela + ' SET SubstituidoLca=' +
          Geral.FF0(OriItem) + ', SubstituidoGGX=' +
          Geral.FF0(OriGGX) + ' WHERE Item=' + Geral.FF0(DstItem) + ';');
        //
        Dmod.MyDB.Execute('UPDATE ' + Tabela + ' SET SubstitutoLca=' +
          Geral.FF0(DstItem) + ', SubstitutoGGX=' +
          Geral.FF0(DstGGX) + ', Troca=1 WHERE Item=' + Geral.FF0(OriItem) + ';');
        //
        // S� funciona para Um! Cuidado!
        sValor := Geral.FFT_Dot(EdValorLocacaoDev.ValueVariant, 2, siNegativo);
        Dmod.MyDB.Execute('UPDATE ' + Tabela +
        ' SET SubstitutoLca=' + Geral.FF0(DstItem) +
        ', SubstitutoGGX=' + Geral.FF0(DstGGX) +
        ', ValorLocAAdiantar=' + sValor +
        ', ValorLocAtualizado=' + sValor +
        ', Troca=1 WHERE Item=' + Geral.FF0(OriItem) + ';');
        //
        FRecalcular  := True;
        FNewControle := Controle;
        AppPF.AtualizaValDevolParci(Codigo, GTRTab, CtrID, OriItem);
        AppPF.AtualizaEstoqueGraGXPatr(OriGGX, FEmpresa, True);
        //MostraProximoItem();
        Close;
      end;
    end;
  end;
end;

procedure TFmLocCItsSubs.BtPulaClick(Sender: TObject);
begin
{
object BtPula: TBitBtn
  Tag = 3
  Left = 136
  Top = 4
  Width = 120
  Height = 40
  Caption = '&Pula'
  NumGlyphs = 2
  TabOrder = 1
  Visible = False
  OnClick = BtPulaClick
end
}
  //MostraProximoItem();
end;

procedure TFmLocCItsSubs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCItsSubs.CalculaDadosAtual();
const
  GeraLog = True;
var
  ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS, QtdeLocacao, QtdeProduto,
  QtdeDevolucao, ValorLocAntigo, ValDevolParci, ValorUnitario,
  ValorLocAtualizado: Double;
  DMenos, Troca, DiasFDS, DiasUteis: Integer;
  CalcAdiLOCACAO, Log: String;
  Calculou: Boolean;
  Saida, Volta, IniCobranca, OriSubstSaiDH, HrTolerancia: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  try
    //Volta    := QrLocCConCabDtHrDevolver;
    //
    DMenos         := QrItsLcaDMenos.Value;
    //
    if QrGraGXPatr.State <> dsInactive then
    begin
      ValorDia       := QrGraGXPatrValorDia.Value;
      ValorSem       := QrGraGXPatrValorSem.Value;
      ValorQui       := QrGraGXPatrValorQui.Value;
      ValorMes       := QrGraGXPatrValorMes.Value;
      ValorFDS       := QrGraGXPatrValorFDS.Value;
    end else
    begin
      ValorDia       := QrItsLcaValorDia.Value;
      ValorSem       := QrItsLcaValorSem.Value;
      ValorQui       := QrItsLcaValorQui.Value;
      ValorMes       := QrItsLcaValorMes.Value;
      ValorFDS       := QrItsLcaValorFDS.Value;
    end;
    //
    QtdeLocacao    := QrItsLcaQtdeLocacao.Value;
    QtdeProduto    := QrItsLcaQtdeProduto.Value;
    QtdeDevolucao  := QrItsLcaQtdeDevolucao.Value;
    ValorLocAntigo := QrItsLcaValorLocAtualizado.Value;
    ValDevolParci  := QrItsLcaValDevolParci.Value;
    Troca          := QrItsLcaTroca.Value;
    IniCobranca    := QrItsLcaCobranIniDtHr.Value;
    OriSubstSaiDH  := QrItsLcaOriSubstSaiDH.Value;
    HrTolerancia   := QrItsLcaHrTolerancia.Value;
    //
    DiasFDS            := 0;
    DiasUteis          := 0;
    CalcAdiLOCACAO     := '';
    ValorUnitario      := 0;
    ValorLocAtualizado := 0.00;
    //
    Calculou           := False;
    Saida              := QrItsLcaSaiDtHr.Value;
    Volta              := Trunc(TPVolta.Date);
    //
    AppPF.CalculaDadosEntreDatasLocacao_COM_CobrarExtraFDS((*const*) GeraLog,
    (*const*) Saida, Volta, HrTolerancia, OriSubstSaiDH, (*const*) DMenos, (*cons*)ValorDia, ValorSem,
    ValorQui, ValorMes, ValorFDS, QtdeLocacao, QtdeProduto, QtdeDevolucao,
    ValDevolParci, Troca, FDtHrDevolver, (*var*) Log, (*var*)IniCobranca, (*var*)DiasFDS,
    DiasUteis, (*var*)CalcAdiLOCACAO,
    ValorUnitario, ValorLocAtualizado, Calculou);
    //
    EdValorLocAtualizado.ValueVariant := ValorLocAtualizado;
    EdCOBRANCALOCACAO.Text := CalcAdiLOCACAO;
    //
    Memo1.Text := Log;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocCItsSubs.CalculaValorLocacao();
var
  ValorLocAtualizado, Quantidade, ValorLocacao: Double;
begin
  ValorLocAtualizado := EdValorLocAtualizado.ValueVariant;
  Quantidade := EdQtdDevol.ValueVariant;
  //
  ValorLocacao := Quantidade * ValorLocAtualizado;
  //
  EdValorLocacaoDev.ValueVariant := ValorLocacao;
end;

procedure TFmLocCItsSubs.CalculaValorTotalLocacaoItem;
var
  QtdeLocacao, ValorLocacao, ValorTotalLocacaoItem: Double;
begin
  QtdeLocacao  := EdQtdeLocacao.ValueVariant;
  ValorLocacao := EdValorLocacaoNew.ValueVariant;
  ValorTotalLocacaoItem := QtdeLocacao * ValorLocacao;
  EdValorTotalLocacaoItem.ValueVariant := ValorTotalLocacaoItem;
end;

procedure TFmLocCItsSubs.EdGraGruXChange(Sender: TObject);
begin
  if (not EdPatrimonio.Focused) and
     (not EdReferencia.Focused) then
    //PesquisaPorGraGruX();
    AppPF.LocCItsRet_PesquisaPorGraGruX(EdGraGruX, CBGraGruX, EdPatrimonio,
    EdReferencia, FTipoTool, (*Limpa*)True);
end;

procedure TFmLocCItsSubs.EdGraGruXRedefinido(Sender: TObject);
var
  ValorLocacao: Double;
begin
  AppPF.LocCItsRet_ReopnGraGXPatr(QrGraGXPatr, FEmpresa, EdGraGruX.ValueVariant);
  //if ImgTipo.SQLType = stIns then
  begin
    if FTipoAluguel = 'D' then
      ValorLocacao := QrGraGXPatrValorDia.Value
    else
    if FTipoAluguel = 'S' then
      ValorLocacao := QrGraGXPatrValorSem.Value
    else
    if FTipoAluguel = 'Q' then
      ValorLocacao := QrGraGXPatrValorQui.Value
    else
    if FTipoAluguel = 'M' then
      ValorLocacao := QrGraGXPatrValorMes.Value
    else
    begin
      Geral.MB_Erro('"Tipo de Aluguel" n�o definido!');
      ValorLocacao := 0;
    end;
    //
    EdValorLocacaoNew.ValueVariant := ValorLocacao;
  end;
end;

procedure TFmLocCItsSubs.EdPatrimonioChange(Sender: TObject);
begin
  if EdPatrimonio.Focused then
    //PesquisaPorPatrimonio(False);
    AppPF.LocCItsRet_PesquisaPOrPatrimonio(EdGraGruX, CBGraGruX, EdPatrimonio,
    EdReferencia, FTipoTool, (*Limpa*)False);
end;

procedure TFmLocCItsSubs.EdPatrimonioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  PesquisaPorNome(Key);
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmLocCItsSubs.EdQtdeLocacaoRedefinido(Sender: TObject);
begin
  CalculaValorTotalLocacaoItem();
end;

procedure TFmLocCItsSubs.EdQtdDevolRedefinido(Sender: TObject);
begin
  BtConfirma.Enabled := EdQtdDevol.ValueVariant <> 0;
  CalculaValorLocacao();
end;

procedure TFmLocCItsSubs.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    //PesquisaPorReferencia(False);
    AppPF.LocCItsRet_PesquisaPorReferencia(EdGraGruX, CBGraGruX, EdPatrimonio,
    EdReferencia, FTipoTool, (*Limpa*)False);
end;

procedure TFmLocCItsSubs.EdValorLocacaoNewRedefinido(Sender: TObject);
begin
  CalculaValorTotalLocacaoItem();
end;

procedure TFmLocCItsSubs.EdValorLocAtualizadoRedefinido(Sender: TObject);
begin
  CalculaValorLocacao();
end;

procedure TFmLocCItsSubs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCItsSubs.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  FRecalcular            := False;
  FNewControle           := 0;
  FCriando               := True;
  FQuantidadeAlt         := 0;
  ImgTipo.SQLType        := stLok;
  //
  Agora                  := DModG.ObtemAgora();
  TPVolta.Date           := Trunc(Agora);
  EdVolta.ValueVariant   := Agora;
  TPDataLoc.Date         := Trunc(Agora);
  EdHoraLoc.ValueVariant := Agora;
  //
  UnDmkDAC_PF.AbreQuery(QrLocTroMotCad, Dmod.MyDB);
end;

procedure TFmLocCItsSubs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCItsSubs.FormShow(Sender: TObject);
begin
  AppPF.ReopenGraGXToll(FGraGruY, FTipoTool, QrGraGXToll);
  if FCriando then
  begin
    FCriando := False;
    //
    if ImgTipo.SQLType = stIns then
      CalculaDadosAtual();
  end;
end;

function TFmLocCItsSubs.IncluiLocCItsMat(var Item, GraGruX: Integer): Boolean;
const
  QtdeLocacaoAntes = 0;
var
  GraGruY, ManejoLca: Integer;
var
  Codigo, CtrID, Unidade: Integer;
  QtdeLocacao, QtdeProduto, ValorProduto, ValorLocacao, PrcUni: Double;
  Categoria: String;
  OriSubstItem: Integer;
  OriSubstSaiDH: String;
begin
  Result := False;
  Item   := 0;
  Codigo := FCodigo;
  CtrID  := FCtrID;
  Categoria     := 'L';
  //
  GraGruX       := EdGraGruX.ValueVariant;
  GraGruY       := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
  ManejoLca     := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);
  FDtHrLocado   := Geral.FDT(Trunc(TPDataLoc.Date), 1) + ' ' + EdHoraLoc.Text;
  QtdeLocacao   := EdQtdeLocacao.ValueVariant;
  QtdeProduto   := EdQtdeProduto.ValueVariant;
  ValorProduto  := QrGraGXTollItemValr.Value;
  ValorLocacao  := EdValorLocacaoNew.ValueVariant;
  PrcUni        := QrGraGXTollItemValr.Value;
  Unidade       := QrGraGXTollItemUnid.Value;
  //
  if QrItsLcaOriSubstItem.Value > 0 then
  begin
    OriSubstItem          := QrItsLcaOriSubstItem.Value;
    //OriSubstSaiDH         := Geral.FDT(QrItsLcaOriSubstSaiDH.Value, 109);
  end else
  begin
    OriSubstItem          := QrItsLcaItem.Value;
    //OriSubstSaiDH         := Geral.FDT(QrItsLcaSaiDtHr.Value, 109);
  end;
  OriSubstSaiDH         := FDtHrLocado;
  //
  //
  //case FTipo of
  case FGraGruY of
    CO_GraGruY_1024_GXPatPri:
      Result := AppPF.LocCItsMat_IncluiPri(stIns, Codigo, CtrID, GraGruX, FEmpresa,
      QtdeLocacao, QtdeProduto, ValorLocacao, QtdeLocacaoAntes, FDtHrLocado,
      Categoria, OriSubstItem, OriSubstSaiDH, Item);
    //
    CO_GraGruY_2048_GXPatSec: ///IncluiSec(Codigo, CtrID); // = Secund�rio
      Result := AppPF.LocCItsMat_IncluiSec(stIns, Codigo, CtrID, GraGruX, FEmpresa,
      QtdeLocacao, QtdeProduto, FTipoAluguel, FDtHrLocado, FQrLocIPatSec,
      OriSubstItem, OriSubstSaiDH, Item);
    //
    CO_GraGruY_3072_GXPatAce: //IncluiAce(Codigo, CtrID); // = Acess�rio
      Result := AppPF.LocCItsMat_IncluiAce(stIns, Codigo, CtrID, GraGruX, FEmpresa,
      ManejoLca, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
      FQrLocIPatAce, OriSubstItem, OriSubstSaiDH, Item);
    //
    CO_GraGruY_4096_GXPatApo: //IncluiApo(Codigo, CtrID); // = Apoio
      Result := AppPF.LocCItsMat_IncluiApo(stIns, Codigo, CtrID, GraGruX, ManejoLca,
      PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
      FQrLocIPatApo, Item);
    //
    CO_GraGruY_5120_GXPatUso: //IncluiUso(Codigo, CtrID); // = Uso (desgaste)
      Result := AppPF.LocCItsMat_IncluiUso(stIns, Codigo, CtrID, GraGruX, ManejoLca, Unidade,
      PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
      FQrLocIPatUso, Item);
    //
    CO_GraGruY_6144_GXPrdCns: //IncluiCns(Codigo, CtrID); // = Uso (consumo)
      Result := AppPF.LocCItsMat_IncluiCns(stIns, Codigo, CtrID, GraGruX, ManejoLca, Unidade,
      PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
      FQrLocIPatCns, Item);
    else
  end;
(*
  if CkContinuar.Checked then
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
    EdGraGruX.ValueVariant := 0;
    CBGraGruX.KeyValue := Null;
    ///
    EdGraGruX.SetFocus;
  end else
    Close;
*)
end;

{
procedure TFmLocCItsSubs.MostraProximoItem();
begin
  if QrItsLca.RecNo < QrItsLca.RecordCount then
  begin
    QrItsLca.Next;
    CalculaDadosAtual();
    EdQtdDevol.SetFocus;
  end else
    Close;
end;
}

procedure TFmLocCItsSubs.QrItsLcaAfterScroll(DataSet: TDataSet);
begin
  QrGraGXPatr.Close;
end;

procedure TFmLocCItsSubs.ReopenItsLca();
begin
  AppPF.LocCItsRet_ReopenItsLca(QrItsLca, nil(*BtPula*), FItem, FCodigo, FCtrID);
end;

procedure TFmLocCItsSubs.ReopenLocCMovAll();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCMovAll, Dmod.MyDB, [
  'SELECT lma.*',
  //SQL_ManejoLca,
  'FROM loccmovall lma ',
  'WHERE lma.Controle=' + Geral.FF0(EdControle.ValueVariant),
  '']);
  FQuantidadeAlt              := QrLocCMovAllQuantidade.Value;
  //
  TPVolta.Date                := Trunc(QrLocCMovAllDataHora.Value);
  EdVolta.ValueVariant        := QrLocCMovAllDataHora.Value;
  CkLimpo.Checked             := QrLocCMovAllLimpo.Value = 'S';
  CkSujo.Checked              := QrLocCMovAllSujo.Value = 'S';
  CkQuebrado.Checked          := QrLocCMovAllQuebrado.Value = 'S';
  CkTestadoDevolucao.Checked  := QrLocCMovAllTestadoDevolucao.Value = 'S';
  EdCOBRANCALOCACAO.Text      := QrLocCMovAllCOBRANCALOCACAO.Value;
  EdQtdDevol.ValueVariant   := QrLocCMovAllQuantidade.Value;
  EdValorLocacaoDev.ValueVariant := QrLocCMovAllVALORLOCACAO.Value;
end;

procedure TFmLocCItsSubs.SBCadastroClick(Sender: TObject);
const
  sProcName = 'TFmLocCItsSubs.SBCadastroClick()';
begin
  VAR_CADASTRO := 0;
  case FGraGruY of
    CO_GraGruY_1024_GXPatPri,
    CO_GraGruY_2048_GXPatSec,
    CO_GraGruY_3072_GXPatAce:
    begin
      GraL_Jan.MostraFormGraGXPatr(EdGraGruX.ValueVariant);
      UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrGraGXPatr, VAR_CADASTRO, 'Controle');
      AppPF.LocCItsRet_PesquisaPorGraGruX(EdGraGruX, CBGraGruX, EdPatrimonio,
        EdReferencia, FTipoTool, (*Limpa*)True);
      EdPatrimonio.SetFocus;
    end;
    CO_GraGruY_4096_GXPatApo,
    CO_GraGruY_5120_GXPatUso,
    CO_GraGruY_6144_GXPrdCns:
    begin
      GraL_Jan.MostraFormGraGXOutr(EdGraGruX.ValueVariant);
      UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrGraGXPatr, VAR_CADASTRO, 'Controle');
      AppPF.LocCItsRet_PesquisaPorGraGruX(EdGraGruX, CBGraGruX, EdPatrimonio,
        EdReferencia, FTipoTool, (*Limpa*)True);
      EdPatrimonio.SetFocus;
    end;
    else
      Geral.MB_Erro('"FGraGruY" n�o implementado em ' + sProcName);
  end;
  //
end;

procedure TFmLocCItsSubs.SBDataLocClick(Sender: TObject);
var
  MinData, Volta, NovaVolta: TDateTime;
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
    Exit;
  MinData := QrItsLcaCobranIniDtHr.Value;
  Volta   := Trunc(TPVolta.DateTime) + EdVolta.ValueVariant;
  //
  if DBCheck.ObtemData(Volta, NovaVolta, MinData, EdVolta.ValueVariant, True, 'In�cio da cobran�a') then
  begin
    TPDataLoc.Date         := Trunc(NovaVolta);
    EdHoraLoc.ValueVariant := NovaVolta;
    //
    CalculaDadosAtual();
  end;
end;

procedure TFmLocCItsSubs.SbPrecosContratoClick(Sender: TObject);
begin
  QrGraGXPatr.Close;
  //
  CalculaDadosAtual();
  //
  EdQtdDevol.SetFocus;
end;

procedure TFmLocCItsSubs.SBPrecosGeraisClick(Sender: TObject);
begin
  AppPF.LocCItsRet_ReopnGraGXPatr(QrGraGXPatr, FEmpresa, QrItsLcaGraGruX.Value);
  //
  CalculaDadosAtual();
  //
  EdQtdDevol.SetFocus;
end;

procedure TFmLocCItsSubs.SbValorLocacaoDevClick(Sender: TObject);
var
  Valor: Double;
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
    Exit;
  Valor := EdValorLocacaoDev.ValueVariant;
  //
  if dmkPF.ObtemValorDouble(Valor, 2) then
  begin
    EdValorLocacaoDev.ValueVariant := Valor;
  end;
end;

procedure TFmLocCItsSubs.SbVoltaClick(Sender: TObject);
var
  MinData, Volta, NovaVolta: TDateTime;
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
    Exit;
  MinData := QrItsLcaCobranIniDtHr.Value;
  Volta   := Trunc(TPVolta.DateTime) + EdVolta.ValueVariant;
  //
  if DBCheck.ObtemData(Volta, NovaVolta, MinData, EdVolta.ValueVariant, True, 'Retorno do equipamento') then
  begin
    TPVolta.Date         := Trunc(NovaVolta);
    EdVolta.ValueVariant := NovaVolta;
    //
    TPDataLoc.Date         := Trunc(NovaVolta);
    EdHoraLoc.ValueVariant := NovaVolta;
    //
    CalculaDadosAtual();
  end;
end;

procedure TFmLocCItsSubs.SpeedButton1Click(Sender: TObject);
var
  GraGruX, Nivel2: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  Nivel2  := QrGraGXPatrNivel2.Value;
  if GraGruX = 0 then
    Geral.MB_Aviso('Selecione o equipamento!')
  else
    GraL_Jan.MostraFormLocCItsLcLocados(FEmpresa, GraGruX, Nivel2);
end;

end.
