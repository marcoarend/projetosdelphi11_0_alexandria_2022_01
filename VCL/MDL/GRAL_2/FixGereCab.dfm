object FmFixGereCab: TFmFixGereCab
  Left = 368
  Top = 194
  Caption = 'FIX-GEREN-001 :: Conserto - Ordem de Servi'#231'o'
  ClientHeight = 724
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnEdita: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 606
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1241
      Height = 223
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 10
        Top = 25
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 162
        Top = 25
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object Label3: TLabel
        Left = 167
        Top = 54
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
      end
      object Label4: TLabel
        Left = 10
        Top = 84
        Width = 234
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Observa'#231#245'es (m'#225'ximo 255 caracteres):'
        Color = clBtnFace
        ParentColor = False
      end
      object SpeedButton5: TSpeedButton
        Left = 1195
        Top = 49
        Width = 26
        Height = 26
        Hint = 'Inclui item de carteira'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 59
        Top = 20
        Width = 99
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 220
        Top = 20
        Width = 54
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 276
        Top = 20
        Width = 945
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliente: TdmkEditCB
        Left = 220
        Top = 49
        Width = 54
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 276
        Top = 49
        Width = 917
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientes
        TabOrder = 4
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object MeObservacao: TdmkMemo
        Left = 10
        Top = 103
        Width = 1212
        Height = 100
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 5
        QryCampo = 'Observacao'
        UpdCampo = 'Observacao'
        UpdType = utYes
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 528
      Width = 1241
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1069
        Top = 18
        Width = 170
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 147
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 606
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 527
      Width = 1241
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 35
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 410
        Top = 18
        Width = 829
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 665
          Top = 0
          Width = 164
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 10090
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&O.S.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtEqu: TBitBtn
          Tag = 497
          Left = 158
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Equip.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtEquClick
        end
        object BtSrv: TBitBtn
          Tag = 530
          Left = 463
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Servi'#231'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtSrvClick
        end
        object BtPca: TBitBtn
          Tag = 498
          Left = 310
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Pe'#231'a'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtPcaClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 110
      Width = 1241
      Height = 216
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      DataSource = DsFixGereEqu
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID conserto'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtHrRece'
          Title.Caption = 'Recebido'
          Width = 96
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'ID Equip.'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Tabela'
          Title.Caption = 'D'
          Width = 12
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_EQUI'
          Title.Caption = 'Nome equipamento'
          Width = 260
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DefeitoTxt'
          Title.Caption = 'Defeito relatado'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoPeca'
          Title.Caption = '$ Pe'#231'as'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoServ'
          Title.Caption = '$ Servi'#231'os'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoTota'
          Title.Caption = '$ Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TXT_DtHrLibe'
          Title.Caption = 'Liberado'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_LIBE'
          Title.Caption = 'Quem liberou'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_RECE'
          Title.Caption = 'Quem Recebeu'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TABELA_TXT'
          Title.Caption = 'Origem'
          Width = 150
          Visible = True
        end>
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1241
      Height = 110
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object Label5: TLabel
        Left = 10
        Top = 25
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 159
        Top = 25
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Caption = 'Empresa:'
      end
      object Label8: TLabel
        Left = 171
        Top = 54
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Caption = 'Cliente:'
      end
      object Label10: TLabel
        Left = 132
        Top = 84
        Width = 85
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Caption = 'Observa'#231#245'es:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label1: TLabel
        Left = 1041
        Top = 25
        Width = 52
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Caption = '$ Pe'#231'as:'
      end
      object Label2: TLabel
        Left = 1030
        Top = 54
        Width = 63
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Caption = '$Servi'#231'os:'
      end
      object Label11: TLabel
        Left = 1040
        Top = 84
        Width = 53
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Caption = '$ Total:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object DBEdit1: TDBEdit
        Left = 59
        Top = 20
        Width = 100
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Codigo'
        DataSource = DsFixGereCab
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 222
        Top = 20
        Width = 50
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Empresa'
        DataSource = DsFixGereCab
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 276
        Top = 20
        Width = 714
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_EMP'
        DataSource = DsFixGereCab
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 222
        Top = 49
        Width = 50
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Cliente'
        DataSource = DsFixGereCab
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 276
        Top = 49
        Width = 714
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_CLI'
        DataSource = DsFixGereCab
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 222
        Top = 79
        Width = 768
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Observacao'
        DataSource = DsFixGereCab
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 1098
        Top = 20
        Width = 128
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CustoPeca'
        DataSource = DsFixGereCab
        TabOrder = 6
      end
      object DBEdit8: TDBEdit
        Left = 1098
        Top = 49
        Width = 128
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CustoServ'
        DataSource = DsFixGereCab
        TabOrder = 7
      end
      object DBEdit9: TDBEdit
        Left = 1098
        Top = 79
        Width = 128
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CustoTota'
        DataSource = DsFixGereCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
      end
    end
    object GBPecServ: TGroupBox
      Left = 0
      Top = 326
      Width = 1241
      Height = 129
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = 'Despesas com pe'#231'as e servi'#231'os:'
      TabOrder = 3
      object DBGrid1: TDBGrid
        Left = 2
        Top = 18
        Width = 618
        Height = 109
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        DataSource = DsFixGerePca
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -15
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Conta'
            Title.Caption = 'ID pe'#231'a'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodItem'
            Title.Caption = 'C'#243'digo'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_ITEM'
            Title.Caption = 'Nome'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descri'
            Title.Caption = 'Complemento'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Caption = 'Quantidade'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Custo'
            Title.Caption = 'Custo un.'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Caption = 'Valor itens'
            Width = 60
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 620
        Top = 18
        Width = 619
        Height = 109
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsFixGereSrv
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -15
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Conta'
            Title.Caption = 'ID pe'#231'a'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodItem'
            Title.Caption = 'C'#243'digo'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_ITEM'
            Title.Caption = 'Nome'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descri'
            Title.Caption = 'Complemento'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Caption = 'Quantidade'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Custo'
            Title.Caption = 'Custo un.'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Caption = 'Valor itens'
            Width = 60
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 916
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 417
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Conserto - Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 417
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Conserto - Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 417
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Conserto - Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 116
    Top = 52
  end
  object QrFixGereCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFixGereCabBeforeOpen
    AfterOpen = QrFixGereCabAfterOpen
    BeforeClose = QrFixGereCabBeforeClose
    AfterScroll = QrFixGereCabAfterScroll
    SQL.Strings = (
      'SELECT '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI,'
      'fgc.*'
      'FROM fixgerecab fgc'
      'LEFT JOIN entidades emp ON emp.Codigo=fgc.Empresa'
      'LEFT JOIN entidades cli ON cli.Codigo=fgc.Cliente')
    Left = 60
    Top = 53
    object QrFixGereCabNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrFixGereCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrFixGereCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fixgerecab.Codigo'
    end
    object QrFixGereCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'fixgerecab.Empresa'
    end
    object QrFixGereCabCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'fixgerecab.Cliente'
    end
    object QrFixGereCabObservacao: TWideStringField
      FieldName = 'Observacao'
      Origin = 'fixgerecab.Observacao'
      Size = 255
    end
    object QrFixGereCabCustoTota: TFloatField
      FieldName = 'CustoTota'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFixGereCabCustoServ: TFloatField
      FieldName = 'CustoServ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFixGereCabCustoPeca: TFloatField
      FieldName = 'CustoPeca'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsFixGereCab: TDataSource
    DataSet = QrFixGereCab
    Left = 88
    Top = 53
  end
  object QrFixGereEqu: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFixGereEquBeforeClose
    AfterScroll = QrFixGereEquAfterScroll
    OnCalcFields = QrFixGereEquCalcFields
    SQL.Strings = (
      'SELECT ELT(Tabela, "E", "T", "?") SIGLA_TAB, '
      'qrc.Login NO_RECE,'
      'qrs.Login NO_RESP,'
      'qlb.Login NO_LIBE,'
      ''
      'if(fge.Tabela=1, gg1.Nome, gxp.DescrTerc) NO_EQUI,'
      ''
      'CASE fge.Tabela'
      'WHEN 1 THEN "Patrim'#244'nio pr'#243'prio"'
      'WHEN 2 THEN "Equipamento de terceiro"'
      'ELSE "???" END TABELA_TXT,'
      ''
      'fge.*'
      ''
      'FROM fixgereequ fge'
      'LEFT JOIN senhas qrc ON qrc.Numero=fge.QuemRece'
      'LEFT JOIN senhas qrs ON qrs.Numero=fge.QuemResp'
      'LEFT JOIN senhas qlb ON qlb.Numero=fge.QuemLibe'
      ''
      'LEFT JOIN gragrux ggx ON ggx.Controle=fge.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      ''
      'LEFT JOIN fixgxpatr gxp ON gxp.GraGrux=fge.GraGruX')
    Left = 144
    Top = 53
    object QrFixGereEquNO_RECE: TWideStringField
      FieldName = 'NO_RECE'
      Size = 100
    end
    object QrFixGereEquNO_RESP: TWideStringField
      FieldName = 'NO_RESP'
      Size = 100
    end
    object QrFixGereEquNO_LIBE: TWideStringField
      FieldName = 'NO_LIBE'
      Size = 100
    end
    object QrFixGereEquNO_EQUI: TWideStringField
      FieldName = 'NO_EQUI'
      Size = 120
    end
    object QrFixGereEquCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFixGereEquControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFixGereEquTabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrFixGereEquGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrFixGereEquQuemRece: TIntegerField
      FieldName = 'QuemRece'
    end
    object QrFixGereEquQuemResp: TIntegerField
      FieldName = 'QuemResp'
    end
    object QrFixGereEquQuemLibe: TIntegerField
      FieldName = 'QuemLibe'
    end
    object QrFixGereEquDtHrRece: TDateTimeField
      FieldName = 'DtHrRece'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFixGereEquDtHrLibe: TDateTimeField
      FieldName = 'DtHrLibe'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFixGereEquCustoPeca: TFloatField
      FieldName = 'CustoPeca'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFixGereEquCustoServ: TFloatField
      FieldName = 'CustoServ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFixGereEquCustoTota: TFloatField
      FieldName = 'CustoTota'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFixGereEquDefeitoTxt: TWideStringField
      FieldName = 'DefeitoTxt'
      Size = 60
    end
    object QrFixGereEquSIGLA_TAB: TWideStringField
      FieldName = 'SIGLA_TAB'
      Size = 1
    end
    object QrFixGereEquTXT_DtHrLibe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DtHrLibe'
      Size = 30
      Calculated = True
    end
    object QrFixGereEquTABELA_TXT: TWideStringField
      FieldName = 'TABELA_TXT'
      Size = 30
    end
  end
  object DsFixGereEqu: TDataSource
    DataSet = QrFixGereEqu
    Left = 172
    Top = 53
  end
  object PMEqu: TPopupMenu
    OnPopup = PMEquPopup
    Left = 508
    Top = 496
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 384
    Top = 496
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Ativo = 1'
      'ORDER BY NOMEENTIDADE')
    Left = 404
    Top = 68
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 432
    Top = 68
  end
  object PMPca: TPopupMenu
    OnPopup = PMPcaPopup
    Left = 628
    Top = 492
    object ItsInclui2: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui2Click
    end
    object ItsAltera2: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera2Click
    end
    object ItsExclui2: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui2Click
    end
  end
  object QrFixGerePca: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fpc.Nome NO_ITEM, fgp.*'
      'FROM fixgerepca fgp'
      'LEFT JOIN fixpecacad fpc ON fpc.Codigo=fgp.CodItem'
      'WHERE fgp.Controle=0')
    Left = 200
    Top = 53
    object QrFixGerePcaNO_ITEM: TWideStringField
      FieldName = 'NO_ITEM'
      Size = 60
    end
    object QrFixGerePcaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFixGerePcaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFixGerePcaConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFixGerePcaCodItem: TIntegerField
      FieldName = 'CodItem'
    end
    object QrFixGerePcaQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrFixGerePcaPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGerePcaDesco: TFloatField
      FieldName = 'Desco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGerePcaCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGerePcaValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGerePcaDescri: TWideStringField
      FieldName = 'Descri'
      Size = 60
    end
  end
  object DsFixGerePca: TDataSource
    DataSet = QrFixGerePca
    Left = 228
    Top = 53
  end
  object QrFixGereSrv: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fpc.Nome NO_ITEM, fgp.*'
      'FROM fixgerepca fgp'
      'LEFT JOIN fixpecacad fpc ON fpc.Codigo=fgp.CodItem'
      'WHERE fgp.Controle=0')
    Left = 256
    Top = 53
    object QrFixGereSrvNO_ITEM: TWideStringField
      FieldName = 'NO_ITEM'
      Size = 60
    end
    object QrFixGereSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFixGereSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFixGereSrvConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFixGereSrvCodItem: TIntegerField
      FieldName = 'CodItem'
    end
    object QrFixGereSrvQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrFixGereSrvPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGereSrvDesco: TFloatField
      FieldName = 'Desco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGereSrvCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGereSrvValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFixGereSrvDescri: TWideStringField
      FieldName = 'Descri'
      Size = 60
    end
  end
  object DsFixGereSrv: TDataSource
    DataSet = QrFixGereSrv
    Left = 284
    Top = 53
  end
  object PMSrv: TPopupMenu
    OnPopup = PMSrvPopup
    Left = 748
    Top = 496
    object ItsInclui3: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui3Click
    end
    object ItsAltera3: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera3Click
    end
    object ItsExclui3: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui3Click
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 380
    Top = 243
  end
end
