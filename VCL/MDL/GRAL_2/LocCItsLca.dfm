object FmLocCItsLca: TFmLocCItsLca
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-103 :: Adi'#231#227'o de Patrim'#244'nio '#224' Loca'#231#227'o'
  ClientHeight = 682
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 540
    Width = 1008
    Height = 24
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object LaSitA1: TLabel
      Left = 351
      Top = 9
      Width = 27
      Height = 19
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '???'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clSilver
      Font.Height = -17
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object LaSitA2: TLabel
      Left = 350
      Top = 8
      Width = 27
      Height = 19
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '???'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -17
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label14: TLabel
      Left = 231
      Top = 12
      Width = 111
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Situa'#231#227'o do patrim'#244'nio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object CkContinuar: TCheckBox
      Left = 20
      Top = 5
      Width = 144
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Continuar inserindo.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 58
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label5: TLabel
      Left = 15
      Top = 17
      Width = 14
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID:'
      FocusControl = DBEdCodigo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 73
      Top = 17
      Width = 35
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cliente:'
      FocusControl = DBEdCodUso
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 140
      Top = 17
      Width = 81
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome do Cliente:'
      FocusControl = DBEdNome
    end
    object Label21: TLabel
      Left = 734
      Top = 15
      Width = 134
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data / hora loca'#231#227'o / troca:'
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 15
      Top = 32
      Width = 56
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmLocCCon.DsLocCCon
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdCodUso: TDBEdit
      Left = 73
      Top = 32
      Width = 64
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      DataField = 'Cliente'
      DataSource = FmLocCCon.DsLocCCon
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DBEdNome: TDBEdit
      Left = 140
      Top = 32
      Width = 589
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clWhite
      DataField = 'NO_CLIENTE'
      DataSource = FmLocCCon.DsLocCCon
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object TPDataLoc: TdmkEditDateTimePicker
      Left = 734
      Top = 31
      Width = 99
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 41131.724786689820000000
      Time = 41131.724786689820000000
      TabOrder = 3
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdHoraLoc: TdmkEdit
      Left = 836
      Top = 31
      Width = 41
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 4
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfLong
      HoraFormat = dmkhfShort
      Texto = '00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 110
    Width = 1008
    Height = 187
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Dados do item (Patrim'#244'nio principal): '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 51
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID do item:'
    end
    object Label1: TLabel
      Left = 70
      Top = 16
      Width = 48
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Reduzido:'
    end
    object SbGraGruX: TSpeedButton
      Left = 784
      Top = 31
      Width = 21
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SbGraGruXClick
    end
    object Label2: TLabel
      Left = 128
      Top = 16
      Width = 52
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Patrim'#244'nio:'
    end
    object Label7: TLabel
      Left = 341
      Top = 16
      Width = 51
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o:'
    end
    object Label15: TLabel
      Left = 76
      Top = 73
      Width = 58
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Quantidade:'
    end
    object Label16: TLabel
      Left = 12
      Top = 73
      Width = 42
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Estoque:'
      Enabled = False
    end
    object Label17: TLabel
      Left = 148
      Top = 73
      Width = 86
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Valor un. loca'#231#227'o:'
    end
    object Label18: TLabel
      Left = 240
      Top = 73
      Width = 91
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Valor total loca'#231#227'o:'
      Enabled = False
    end
    object SbRecalculaSaldo: TSpeedButton
      Left = 860
      Top = 31
      Width = 137
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Recalcula saldo'
      OnClick = SbRecalculaSaldoClick
    end
    object Label19: TLabel
      Left = 216
      Top = 16
      Width = 55
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Refer'#234'ncia:'
    end
    object SbGraGXPatr: TSpeedButton
      Left = 808
      Top = 31
      Width = 21
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '?'
      OnClick = SbGraGXPatrClick
    end
    object SpeedButton1: TSpeedButton
      Left = 832
      Top = 31
      Width = 23
      Height = 22
      Caption = '!'
      OnClick = SpeedButton1Click
    end
    object EdCtrID: TdmkEdit
      Left = 12
      Top = 31
      Width = 56
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 341
      Top = 31
      Width = 440
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Controle'
      ListField = 'NO_GG1'
      ListSource = DsGraGXPatr
      TabOrder = 4
      dmkEditCB = EdGraGruX
      UpdType = utYes
      LocF7SQLText.Strings = (
        'SELECT lpc.GraGruX _CODIGO, gg1.REFERENCIA, gg1.Nome _NOME'
        'FROM gragxpatr lpc'
        'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX'
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
        'WHERE (gg1.Nome LIKE "%$#%"'
        'OR gg1.Referencia LIKE "%$#%"'
        'OR gg1.Patrimonio LIKE "%$#%")'
        'AND ggx.Ativo=1'
        '')
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdGraGruX: TdmkEditCB
      Left = 70
      Top = 31
      Width = 56
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraGruXChange
      OnKeyDown = EdGraGruXKeyDown
      OnRedefinido = EdGraGruXRedefinido
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdPatrimonio: TdmkEdit
      Left = 128
      Top = 31
      Width = 85
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdPatrimonioChange
      OnExit = EdPatrimonioExit
      OnKeyDown = EdPatrimonioKeyDown
    end
    object GroupBox3: TGroupBox
      Left = 344
      Top = 57
      Width = 397
      Height = 58
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Valores de aluguel: '
      Enabled = False
      TabOrder = 9
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 393
        Height = 41
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label11: TLabel
          Left = 235
          Top = 0
          Width = 30
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Di'#225'rio:'
          FocusControl = DBEdit4
        end
        object Label10: TLabel
          Left = 158
          Top = 0
          Width = 41
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Semanal'
          FocusControl = DBEdit3
        end
        object Label9: TLabel
          Left = 82
          Top = 0
          Width = 47
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Quinzenal'
          FocusControl = DBEdit2
        end
        object Label8: TLabel
          Left = 5
          Top = 0
          Width = 37
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Mensal:'
          FocusControl = DBEdit1
        end
        object Label20: TLabel
          Left = 311
          Top = 0
          Width = 59
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Fim-de-sem.:'
          FocusControl = DBEdit7
        end
        object DBEdit3: TDBEdit
          Left = 158
          Top = 16
          Width = 72
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'ValorSem'
          DataSource = DsGraGXPatr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 235
          Top = 16
          Width = 72
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'ValorDia'
          DataSource = DsGraGXPatr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object DBEdit2: TDBEdit
          Left = 82
          Top = 16
          Width = 72
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'ValorQui'
          DataSource = DsGraGXPatr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object DBEdit1: TDBEdit
          Left = 5
          Top = 16
          Width = 72
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'ValorMes'
          DataSource = DsGraGXPatr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object DBEdit7: TDBEdit
          Left = 311
          Top = 16
          Width = 72
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'ValorFDS'
          DataSource = DsGraGXPatr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 12
      Top = 121
      Width = 665
      Height = 58
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Patrim'#244'nio Secund'#225'rio: '
      Enabled = False
      TabOrder = 10
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 661
        Height = 41
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label12: TLabel
          Left = 10
          Top = 0
          Width = 55
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Refer'#234'ncia:'
          FocusControl = DBEdit5
        end
        object Label13: TLabel
          Left = 123
          Top = 0
          Width = 51
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdit6
        end
        object DBEdit5: TDBEdit
          Left = 10
          Top = 16
          Width = 108
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'Referencia'
          DataSource = DsAgrupado
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object DBEdit6: TDBEdit
          Left = 123
          Top = 16
          Width = 534
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clInactiveCaption
          DataField = 'Nome'
          DataSource = DsAgrupado
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
    end
    object EdQtdeProduto: TdmkEdit
      Left = 76
      Top = 88
      Width = 68
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdQtdeProdutoRedefinido
    end
    object DbEdEstqSdo: TDBEdit
      Left = 12
      Top = 88
      Width = 61
      Height = 21
      TabStop = False
      DataField = 'EstqSdo'
      DataSource = DsGraGXPatr
      TabOrder = 5
    end
    object EdValorLocacao: TdmkEdit
      Left = 148
      Top = 88
      Width = 88
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdValorLocacaoRedefinido
    end
    object EdValorTotalLocacaoItem: TdmkEdit
      Left = 240
      Top = 88
      Width = 93
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object CkQtdeLocacao_: TdmkCheckBox
      Left = 780
      Top = 132
      Width = 169
      Height = 17
      Caption = 'Confirmar loca'#231#227'o.'
      Checked = True
      State = cbChecked
      TabOrder = 11
      UpdType = utYes
      ValCheck = '1'
      ValUncheck = '0'
      OldValor = #0
    end
    object EdReferencia: TdmkEdit
      Left = 216
      Top = 31
      Width = 122
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdReferenciaChange
      OnExit = EdReferenciaExit
      OnKeyDown = EdReferenciaKeyDown
    end
    object RGQtdeLocacao: TdmkRadioGroup
      Left = 752
      Top = 56
      Width = 245
      Height = 57
      Caption = ' Baixa do estoque: (Reserva)'
      Columns = 3
      ItemIndex = 2
      Items.Strings = (
        'N'#227'o'
        'Sim'
        '???')
      TabOrder = 12
      UpdType = utYes
      OldValor = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 949
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 14
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 890
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 391
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Adi'#231#227'o de Patrim'#244'nio '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 391
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Adi'#231#227'o de Patrim'#244'nio '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 391
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Adi'#231#227'o de Patrim'#244'nio '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 564
    Width = 1008
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 37
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 618
    Width = 1008
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 829
      Top = 15
      Width = 177
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 2
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 827
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 2
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 297
    Width = 1008
    Height = 243
    Align = alClient
    Caption = 'Panel6'
    TabOrder = 6
    object Splitter1: TSplitter
      Left = 635
      Top = 1
      Width = 5
      Height = 241
      Align = alRight
      ExplicitLeft = 585
    end
    object DBGGraGXComEstq: TdmkDBGridZTO
      Left = 1
      Top = 1
      Width = 634
      Height = 241
      Align = alClient
      DataSource = DsGraGXComEstq
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 34
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 400
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Quantidade'
          Title.Caption = 'Qtde'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AtualValr'
          Title.Caption = '$ Venda'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruY'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorDia'
          Title.Caption = '$ Dia'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorSem'
          Title.Caption = '$ Sem'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorQui'
          Title.Caption = '$ Qui'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorMes'
          Title.Caption = '$ M'#234's'
          Width = 66
          Visible = True
        end>
    end
    object DBGGraGXSemEstq: TdmkDBGridZTO
      Left = 640
      Top = 1
      Width = 367
      Height = 241
      Align = alRight
      DataSource = DsGraGXSemEstq
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Nome'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ItemUnid'
          Title.Caption = 'Un'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Quantidade'
          Title.Caption = 'Qtde'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ItemValr'
          Title.Caption = '$ Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGXOutr'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruY'
          Visible = True
        end>
    end
  end
  object DsGraGXPatr: TDataSource
    DataSet = QrGraGXPatr
    Left = 340
    Top = 384
  end
  object VU_Sel_: TdmkValUsu
    dmkEditCB = EdGraGruX
    QryCampo = '_Sel_'
    UpdCampo = '_Sel_'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 76
    Top = 12
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  cpl.GraGruX=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 704
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrPesq2Patrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 255
    end
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  gg1.Referencia=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 676
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrPesq1Patrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 255
    end
    object QrPesq1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGraGXPatrAfterOpen
    BeforeClose = QrGraGXPatrBeforeClose
    SQL.Strings = (
      'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL,'
      'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1,'
      
        'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXIS' +
        'TE,'
      'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,'
      'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,'
      'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,'
      'cpl.Voltagem, cpl.Potencia, cpl.Capacid'
      ''
      'FROM gragrux ggx'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle'
      'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle'
      'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo'
      'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao'
      'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle'
      'WHERE  gg1.PrdGrupTip=1 AND (gg1.Nivel1=ggx.GraGru1)'
      'AND cpl.Aplicacao <> 0'
      ''
      'ORDER BY NO_GG1'
      '')
    Left = 340
    Top = 336
    object QrGraGXPatrEstqSdo: TFloatField
      FieldName = 'EstqSdo'
    end
    object QrGraGXPatrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXPatrReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXPatrCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
    end
    object QrGraGXPatrNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXPatrCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
      Required = True
    end
    object QrGraGXPatrSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrGraGXPatrAtualValr: TFloatField
      FieldName = 'AtualValr'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorMes: TFloatField
      FieldName = 'ValorMes'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorQui: TFloatField
      FieldName = 'ValorQui'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorSem: TFloatField
      FieldName = 'ValorSem'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorDia: TFloatField
      FieldName = 'ValorDia'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrAgrupador: TIntegerField
      FieldName = 'Agrupador'
    end
    object QrGraGXPatrMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraGXPatrModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 60
    end
    object QrGraGXPatrSerie: TWideStringField
      FieldName = 'Serie'
      Size = 60
    end
    object QrGraGXPatrVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Size = 30
    end
    object QrGraGXPatrPotencia: TWideStringField
      FieldName = 'Potencia'
      Size = 30
    end
    object QrGraGXPatrCapacid: TWideStringField
      FieldName = 'Capacid'
      Size = 30
    end
    object QrGraGXPatrTXT_MES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_MES'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_QUI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_QUI'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_SEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_SEM'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_DIA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DIA'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrNO_SIT: TWideStringField
      FieldName = 'NO_SIT'
      Size = 30
    end
    object QrGraGXPatrNO_SITAPL: TIntegerField
      FieldName = 'NO_SITAPL'
    end
    object QrGraGXPatrValorFDS: TFloatField
      FieldName = 'ValorFDS'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrDMenos: TIntegerField
      FieldName = 'DMenos'
    end
    object QrGraGXPatrPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 60
    end
    object QrGraGXPatrNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
  end
  object QrAgrupado: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia, gg1.Nome'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE  ggx.Controle=:P0')
    Left = 444
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAgrupadoNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrAgrupadoReferencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
  end
  object DsAgrupado: TDataSource
    DataSet = QrAgrupado
    Left = 444
    Top = 384
  end
  object QrGraGXSemEstq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gpi.GraGXOutr, gpi.Quantidade, ggx.GraGruY,'
      'otr.ItemValr, otr.ItemUnid'
      'FROM gragxpits gpi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=gpi.GraGXOutr'
      'LEFT JOIN gragxoutr otr ON otr.GraGruX=gpi.GraGXOutr'
      'WHERE gpi.GraGXPatr>0'
      'AND NOT otr.GraGruX IS NULL'
      '')
    Left = 532
    Top = 336
    object QrGraGXSemEstqGraGXOutr: TIntegerField
      FieldName = 'GraGXOutr'
      Origin = 'gragxpits.GraGXOutr'
    end
    object QrGraGXSemEstqQuantidade: TIntegerField
      FieldName = 'Quantidade'
    end
    object QrGraGXSemEstqGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrGraGXSemEstqItemValr: TFloatField
      FieldName = 'ItemValr'
      Origin = 'gragxoutr.ItemValr'
    end
    object QrGraGXSemEstqItemUnid: TIntegerField
      FieldName = 'ItemUnid'
    end
    object QrGraGXSemEstqNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object QrEstq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 444
    Top = 89
    object QrEstqEstqSdo: TFloatField
      FieldName = 'EstqSdo'
    end
  end
  object QrGraGXComEstq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gpi.GraGXOutr, gpi.Quantidade, ggx.GraGruY,'
      'ptr.*'
      'FROM gragxpits gpi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=gpi.GraGXOutr'
      'LEFT JOIN gragxpatr ptr ON ptr.GraGruX=gpi.GraGXOutr'
      'WHERE gpi.GraGXPatr>0'
      'AND NOT ptr.GraGruX IS NULL')
    Left = 634
    Top = 336
    object QrGraGXComEstqNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrGraGXComEstqGraGXOutr: TIntegerField
      FieldName = 'GraGXOutr'
      Required = True
    end
    object QrGraGXComEstqQuantidade: TIntegerField
      FieldName = 'Quantidade'
      Required = True
    end
    object QrGraGXComEstqGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrGraGXComEstqGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGXComEstqComplem: TWideStringField
      FieldName = 'Complem'
      Size = 60
    end
    object QrGraGXComEstqAquisData: TDateField
      FieldName = 'AquisData'
    end
    object QrGraGXComEstqAquisDocu: TWideStringField
      FieldName = 'AquisDocu'
      Size = 60
    end
    object QrGraGXComEstqAquisValr: TFloatField
      FieldName = 'AquisValr'
    end
    object QrGraGXComEstqSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrGraGXComEstqAtualValr: TFloatField
      FieldName = 'AtualValr'
    end
    object QrGraGXComEstqValorMes: TFloatField
      FieldName = 'ValorMes'
    end
    object QrGraGXComEstqValorQui: TFloatField
      FieldName = 'ValorQui'
    end
    object QrGraGXComEstqValorSem: TFloatField
      FieldName = 'ValorSem'
    end
    object QrGraGXComEstqValorDia: TFloatField
      FieldName = 'ValorDia'
    end
    object QrGraGXComEstqAgrupador: TIntegerField
      FieldName = 'Agrupador'
    end
    object QrGraGXComEstqMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraGXComEstqModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 60
    end
    object QrGraGXComEstqSerie: TWideStringField
      FieldName = 'Serie'
      Size = 60
    end
    object QrGraGXComEstqVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Size = 30
    end
    object QrGraGXComEstqPotencia: TWideStringField
      FieldName = 'Potencia'
      Size = 30
    end
    object QrGraGXComEstqCapacid: TWideStringField
      FieldName = 'Capacid'
      Size = 30
    end
    object QrGraGXComEstqVendaData: TDateField
      FieldName = 'VendaData'
    end
    object QrGraGXComEstqVendaDocu: TWideStringField
      FieldName = 'VendaDocu'
      Size = 60
    end
    object QrGraGXComEstqVendaValr: TFloatField
      FieldName = 'VendaValr'
    end
    object QrGraGXComEstqVendaEnti: TIntegerField
      FieldName = 'VendaEnti'
    end
    object QrGraGXComEstqObserva: TWideStringField
      FieldName = 'Observa'
      Size = 510
    end
    object QrGraGXComEstqAGRPAT: TWideStringField
      FieldName = 'AGRPAT'
      Size = 25
    end
    object QrGraGXComEstqCLVPAT: TWideStringField
      FieldName = 'CLVPAT'
      Size = 25
    end
    object QrGraGXComEstqMARPAT: TWideStringField
      FieldName = 'MARPAT'
      Size = 60
    end
    object QrGraGXComEstqAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrGraGXComEstqDescrTerc: TWideStringField
      FieldName = 'DescrTerc'
      Size = 60
    end
    object QrGraGXComEstqLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGXComEstqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGXComEstqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGXComEstqUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGXComEstqUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGXComEstqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGXComEstqAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrGraGXComEstqAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrGraGXComEstqAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraGXComEstqValorFDS: TFloatField
      FieldName = 'ValorFDS'
    end
    object QrGraGXComEstqDMenos: TIntegerField
      FieldName = 'DMenos'
    end
  end
  object DsGraGXSemEstq: TDataSource
    DataSet = QrGraGXSemEstq
    Left = 532
    Top = 384
  end
  object DsGraGXComEstq: TDataSource
    DataSet = QrGraGXComEstq
    Left = 636
    Top = 384
  end
end
