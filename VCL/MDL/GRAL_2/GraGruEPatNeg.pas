unit GraGruEPatNeg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmGraGruEPatNeg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    DsGraGruEPatNeg: TDataSource;
    Panel2: TPanel;
    DBGGraGruEPatNeg: TdmkDBGridZTO;
    QrGGXEPatNeg: TMySQLQuery;
    QrGGXEPatNegNome: TWideStringField;
    QrGGXEPatNegGraGruX: TIntegerField;
    QrGGXEPatNegEstqAnt: TFloatField;
    QrGGXEPatNegEstqEnt: TFloatField;
    QrGGXEPatNegEstqSai: TFloatField;
    QrGGXEPatNegEstqLoc: TFloatField;
    QrGGXEPatNegEstqSdo: TFloatField;
    QrGGXEPatNegLk: TIntegerField;
    QrGGXEPatNegDataCad: TDateField;
    QrGGXEPatNegDataAlt: TDateField;
    QrGGXEPatNegUserCad: TIntegerField;
    QrGGXEPatNegUserAlt: TIntegerField;
    QrGGXEPatNegAlterWeb: TSmallintField;
    QrGGXEPatNegAWServerID: TIntegerField;
    QrGGXEPatNegAWStatSinc: TSmallintField;
    QrGGXEPatNegAtivo: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmGraGruEPatNeg: TFmGraGruEPatNeg;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmGraGruEPatNeg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruEPatNeg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruEPatNeg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmGraGruEPatNeg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
