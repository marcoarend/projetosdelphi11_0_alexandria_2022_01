object FmStqAlocGer: TFmStqAlocGer
  Left = 339
  Top = 185
  Caption = 'ALO-CACAO-001 :: Gerenciamento de Aloca'#231#227'o'
  ClientHeight = 547
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 342
        Height = 32
        Caption = 'Gerenciamento de Aloca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 342
        Height = 32
        Caption = 'Gerenciamento de Aloca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 342
        Height = 32
        Caption = 'Gerenciamento de Aloca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 433
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 477
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 385
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 40
      Width = 1008
      Height = 288
      Align = alClient
      DataSource = DsStqAlocIts
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = dmkDBGridZTO1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TpMov_TXT'
          Title.Caption = 'Movimento'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Width = 35
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GGX_Aloc'
          Title.Caption = 'Red.Aloc'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'REFERENCIA_A'
          Title.Caption = 'Refer'#234'ncia Aloc.'
          Width = 86
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome_A'
          Title.Caption = 'Nome Alocado'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GGX_SMIA'
          Title.Caption = 'Red.Desal.'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'REFERENCIA_S'
          Title.Caption = 'Refer'#234'ncia desaloca'#231#227'o'
          Width = 86
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome_S'
          Title.Caption = 'Nome desalocado'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoAll'
          Title.Caption = '$ total'
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 40
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 0
        Width = 85
        Height = 13
        Caption = 'Descri'#231#227'o parcial:'
      end
      object dmkLabel3: TdmkLabel
        Left = 548
        Top = 0
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel1: TdmkLabel
        Left = 432
        Top = 0
        Width = 87
        Height = 13
        Caption = 'Limite de registros:'
        UpdType = utYes
        SQLType = stNil
      end
      object EdPesq: TdmkEdit
        Left = 8
        Top = 16
        Width = 421
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPesqChange
      end
      object EdStqCenCad: TdmkEditCB
        Left = 548
        Top = 16
        Width = 53
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenCad: TdmkDBLookupComboBox
        Left = 604
        Top = 16
        Width = 400
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsStqCenCadPsq
        TabOrder = 2
        TabStop = False
        dmkEditCB = EdStqCenCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdLimReg: TdmkEdit
        Left = 432
        Top = 16
        Width = 89
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMax = '100000'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1000
        ValWarn = False
        OnRedefinido = EdLimRegRedefinido
      end
    end
    object DBMemo1: TDBMemo
      Left = 0
      Top = 328
      Width = 1008
      Height = 57
      Align = alBottom
      DataField = 'Historico'
      DataSource = DsStqAlocIts
      TabOrder = 2
      ExplicitLeft = 72
      ExplicitTop = 248
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrStqAlocIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      
        'gg1a.REFERENCIA REFERENCIA_A, ggxa.EAN13 EAN13_A, gg1a.Nome Nome' +
        '_A,'
      
        'gg1s.REFERENCIA REFERENCIA_S, ggxs.EAN13 EAN13_S, gg1s.Nome Nome' +
        '_S,'
      'sai.*'
      'FROM stqalocits sai '
      'LEFT JOIN gragrux ggxa ON ggxa.Controle=sai.GGX_Aloc'
      'LEFT JOIN gragru1 gg1a ON gg1a.Nivel1=ggxa.GraGru1 '
      'LEFT JOIN gragrux ggxs ON ggxs.Controle=sai.GGX_SMIA'
      'LEFT JOIN gragru1 gg1s ON gg1s.Nivel1=ggxs.GraGru1 ')
    Left = 384
    Top = 284
    object QrStqAlocItsREFERENCIA_A: TWideStringField
      FieldName = 'REFERENCIA_A'
      Size = 25
    end
    object QrStqAlocItsEAN13_A: TWideStringField
      FieldName = 'EAN13_A'
      Size = 13
    end
    object QrStqAlocItsNome_A: TWideStringField
      FieldName = 'Nome_A'
      Size = 120
    end
    object QrStqAlocItsREFERENCIA_S: TWideStringField
      FieldName = 'REFERENCIA_S'
      Size = 25
    end
    object QrStqAlocItsEAN13_S: TWideStringField
      FieldName = 'EAN13_S'
      Size = 13
    end
    object QrStqAlocItsNome_S: TWideStringField
      FieldName = 'Nome_S'
      Size = 120
    end
    object QrStqAlocItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqAlocItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqAlocItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrStqAlocItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrStqAlocItsFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrStqAlocItsQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrStqAlocItsCustoAll: TFloatField
      FieldName = 'CustoAll'
      Required = True
    end
    object QrStqAlocItsBaixa: TSmallintField
      FieldName = 'Baixa'
      Required = True
    end
    object QrStqAlocItsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrStqAlocItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrStqAlocItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrStqAlocItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrStqAlocItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrStqAlocItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrStqAlocItsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrStqAlocItsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrStqAlocItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrStqAlocItsGGX_Aloc: TIntegerField
      FieldName = 'GGX_Aloc'
      Required = True
    end
    object QrStqAlocItsGGX_SMIA: TIntegerField
      FieldName = 'GGX_SMIA'
      Required = True
    end
    object QrStqAlocItsTpMov_TXT: TWideStringField
      FieldName = 'TpMov_TXT'
    end
    object QrStqAlocItsHistorico: TWideStringField
      FieldName = 'Historico'
      Size = 511
    end
  end
  object DsStqAlocIts: TDataSource
    DataSet = QrStqAlocIts
    Left = 384
    Top = 340
  end
  object QrStqCenCadPsq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 48
    Top = 152
    object QrStqCenCadPsqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadPsqNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCadPsq: TDataSource
    DataSet = QrStqCenCadPsq
    Left = 76
    Top = 152
  end
end
