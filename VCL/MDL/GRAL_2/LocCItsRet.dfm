object FmLocCItsRet: TFmLocCItsRet
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-108 :: Retorno de Equipamento Locado'
  ClientHeight = 632
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 399
        Height = 32
        Caption = 'Retorno de Equipamento Locado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 399
        Height = 32
        Caption = 'Retorno de Equipamento Locado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 399
        Height = 32
        Caption = 'Retorno de Equipamento Locado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 470
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 470
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 453
        Height = 470
        Align = alLeft
        TabOrder = 0
        object Label18: TLabel
          Left = 16
          Top = 212
          Width = 3
          Height = 13
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 449
          Height = 378
          Align = alTop
          TabOrder = 0
          object Label1: TLabel
            Left = 12
            Top = 12
            Width = 23
            Height = 13
            Caption = 'Item:'
            Enabled = False
            FocusControl = DBEdit1
          end
          object Label2: TLabel
            Left = 84
            Top = 12
            Width = 52
            Height = 13
            Caption = 'Patrim'#244'nio:'
            Enabled = False
            FocusControl = DBEdit2
          end
          object Label3: TLabel
            Left = 188
            Top = 12
            Width = 55
            Height = 13
            Caption = 'Refer'#234'ncia:'
            Enabled = False
            FocusControl = DBEdit3
          end
          object Label8: TLabel
            Left = 12
            Top = 52
            Width = 110
            Height = 13
            Caption = 'Nome do equipamento:'
            Enabled = False
            FocusControl = DBEdit8
          end
          object Label4: TLabel
            Left = 12
            Top = 92
            Width = 46
            Height = 13
            Caption = 'Valor Dia:'
            Enabled = False
            FocusControl = DBEdit4
          end
          object Label5: TLabel
            Left = 88
            Top = 92
            Width = 51
            Height = 13
            Caption = 'Valor Sem:'
            Enabled = False
            FocusControl = DBEdit5
          end
          object Label6: TLabel
            Left = 164
            Top = 92
            Width = 46
            Height = 13
            Caption = 'Valor Qui:'
            Enabled = False
            FocusControl = DBEdit6
          end
          object Label7: TLabel
            Left = 240
            Top = 92
            Width = 50
            Height = 13
            Caption = 'Valor M'#234's:'
            Enabled = False
            FocusControl = DBEdit7
          end
          object Label17: TLabel
            Left = 316
            Top = 92
            Width = 51
            Height = 13
            Caption = 'Valor FDS:'
            Enabled = False
            FocusControl = DBEdit17
          end
          object SbPrecosContrato: TSpeedButton
            Left = 392
            Top = 107
            Width = 53
            Height = 22
            Caption = '>>>'
            OnClick = SbPrecosContratoClick
          end
          object Label19: TLabel
            Left = 12
            Top = 132
            Width = 46
            Height = 13
            Caption = 'Valor Dia:'
            Enabled = False
            FocusControl = DBEdit4
          end
          object Label20: TLabel
            Left = 88
            Top = 132
            Width = 51
            Height = 13
            Caption = 'Valor Sem:'
            Enabled = False
            FocusControl = DBEdit5
          end
          object Label22: TLabel
            Left = 164
            Top = 132
            Width = 46
            Height = 13
            Caption = 'Valor Qui:'
            Enabled = False
            FocusControl = DBEdit6
          end
          object Label23: TLabel
            Left = 240
            Top = 132
            Width = 50
            Height = 13
            Caption = 'Valor M'#234's:'
            Enabled = False
            FocusControl = DBEdit7
          end
          object Label24: TLabel
            Left = 316
            Top = 132
            Width = 51
            Height = 13
            Caption = 'Valor FDS:'
            Enabled = False
            FocusControl = DBEdit17
          end
          object SBPrecosGerais: TSpeedButton
            Left = 392
            Top = 147
            Width = 53
            Height = 22
            Caption = '>>>'
            OnClick = SBPrecosGeraisClick
          end
          object Label9: TLabel
            Left = 12
            Top = 172
            Width = 82
            Height = 13
            Caption = 'Valor do Produto:'
            Enabled = False
            FocusControl = DBEdit9
          end
          object Label16: TLabel
            Left = 100
            Top = 172
            Width = 93
            Height = 13
            Caption = 'Data / hora Sa'#237'da :'
            Enabled = False
            FocusControl = DBEdit16
          end
          object Label15: TLabel
            Left = 216
            Top = 172
            Width = 93
            Height = 13
            Caption = 'In'#237'cio da cobran'#231'a:'
            Enabled = False
            FocusControl = DBEdit15
          end
          object Label28: TLabel
            Left = 380
            Top = 12
            Width = 42
            Height = 13
            Caption = 'Controle:'
            Enabled = False
          end
          object Label10: TLabel
            Left = 12
            Top = 212
            Width = 39
            Height = 13
            Caption = 'Locado:'
            Enabled = False
            FocusControl = DBEdit10
          end
          object Label13: TLabel
            Left = 52
            Top = 212
            Width = 46
            Height = 13
            Caption = 'Trocado?'
            Enabled = False
            FocusControl = DBEdit13
          end
          object Label12: TLabel
            Left = 108
            Top = 212
            Width = 62
            Height = 13
            Caption = 'Qtd. Locado:'
            Enabled = False
            FocusControl = DBEdit12
          end
          object Label11: TLabel
            Left = 192
            Top = 212
            Width = 74
            Height = 13
            Caption = 'Qtd. Devolvido:'
            Enabled = False
            FocusControl = DBEdit11
          end
          object Label14: TLabel
            Left = 276
            Top = 212
            Width = 153
            Height = 13
            Caption = 'Per'#237'odo calculado de cobran'#231'a:'
            Enabled = False
            FocusControl = DBEdit14
          end
          object Label21: TLabel
            Left = 12
            Top = 268
            Width = 143
            Height = 13
            Caption = 'Data / hora final da cobran'#231'a:'
          end
          object SbVolta: TSpeedButton
            Left = 200
            Top = 283
            Width = 23
            Height = 23
            Caption = '...'
            OnClick = SbVoltaClick
          end
          object Label25: TLabel
            Left = 232
            Top = 268
            Width = 83
            Height = 13
            Caption = 'Valor do Per'#237'odo:'
          end
          object Label29: TLabel
            Left = 12
            Top = 336
            Width = 105
            Height = 13
            Caption = 'Per'#237'odo de Cobran'#231'a:'
          end
          object Label26: TLabel
            Left = 240
            Top = 336
            Width = 73
            Height = 13
            Caption = 'Qtd devolu'#231#227'o:'
          end
          object Label27: TLabel
            Left = 344
            Top = 336
            Width = 72
            Height = 13
            Caption = 'Valor Loca'#231#227'o:'
          end
          object Label30: TLabel
            Left = 320
            Top = 268
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label31: TLabel
            Left = 332
            Top = 172
            Width = 100
            Height = 13
            Caption = 'Previs'#227'o devolu'#231#227'o::'
            Enabled = False
          end
          object DBEdit1: TDBEdit
            Left = 12
            Top = 28
            Width = 68
            Height = 21
            TabStop = False
            DataField = 'Item'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 84
            Top = 28
            Width = 101
            Height = 21
            TabStop = False
            DataField = 'Patrimonio'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 188
            Top = 28
            Width = 189
            Height = 21
            TabStop = False
            DataField = 'REFERENCIA'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 2
          end
          object DBEdit8: TDBEdit
            Left = 12
            Top = 68
            Width = 433
            Height = 21
            TabStop = False
            DataField = 'NO_GGX'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 3
          end
          object DBEdit4: TDBEdit
            Left = 12
            Top = 108
            Width = 72
            Height = 21
            TabStop = False
            DataField = 'ValorDia'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 4
          end
          object DBEdit5: TDBEdit
            Left = 88
            Top = 108
            Width = 72
            Height = 21
            TabStop = False
            DataField = 'ValorSem'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 5
          end
          object DBEdit6: TDBEdit
            Left = 164
            Top = 108
            Width = 72
            Height = 21
            TabStop = False
            DataField = 'ValorQui'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 6
          end
          object DBEdit7: TDBEdit
            Left = 240
            Top = 108
            Width = 72
            Height = 21
            TabStop = False
            DataField = 'ValorMes'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 7
          end
          object DBEdit17: TDBEdit
            Left = 316
            Top = 108
            Width = 72
            Height = 21
            TabStop = False
            DataField = 'ValorFDS'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 8
          end
          object DBEdit18: TDBEdit
            Left = 12
            Top = 148
            Width = 72
            Height = 21
            DataField = 'ValorDia'
            DataSource = DsGraGXPatr
            Enabled = False
            TabOrder = 9
          end
          object DBEdit19: TDBEdit
            Left = 88
            Top = 148
            Width = 72
            Height = 21
            DataField = 'ValorSem'
            DataSource = DsGraGXPatr
            Enabled = False
            TabOrder = 10
          end
          object DBEdit20: TDBEdit
            Left = 164
            Top = 148
            Width = 72
            Height = 21
            DataField = 'ValorQui'
            DataSource = DsGraGXPatr
            Enabled = False
            TabOrder = 11
          end
          object DBEdit21: TDBEdit
            Left = 240
            Top = 148
            Width = 72
            Height = 21
            DataField = 'ValorMes'
            DataSource = DsGraGXPatr
            Enabled = False
            TabOrder = 12
          end
          object DBEdit22: TDBEdit
            Left = 316
            Top = 148
            Width = 72
            Height = 21
            DataField = 'ValorFDS'
            DataSource = DsGraGXPatr
            Enabled = False
            TabOrder = 13
          end
          object DBEdit9: TDBEdit
            Left = 12
            Top = 188
            Width = 84
            Height = 21
            TabStop = False
            DataField = 'ValorProduto'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 14
          end
          object DBEdit16: TDBEdit
            Left = 100
            Top = 188
            Width = 112
            Height = 21
            TabStop = False
            DataField = 'SaiDtHr'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 15
          end
          object DBEdit15: TDBEdit
            Left = 216
            Top = 188
            Width = 112
            Height = 21
            TabStop = False
            DataField = 'CobranIniDtHr'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 16
          end
          object EdControle: TdmkEdit
            Left = 380
            Top = 28
            Width = 64
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            ReadOnly = True
            TabOrder = 17
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object DBEdit10: TDBEdit
            Left = 12
            Top = 228
            Width = 37
            Height = 21
            TabStop = False
            DataField = 'QtdeLocacao'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 18
          end
          object DBEdit13: TDBEdit
            Left = 52
            Top = 228
            Width = 53
            Height = 21
            TabStop = False
            DataField = 'Troca'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 19
          end
          object DBEdit12: TDBEdit
            Left = 108
            Top = 228
            Width = 80
            Height = 21
            TabStop = False
            DataField = 'QtdeProduto'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 20
          end
          object DBEdit11: TDBEdit
            Left = 192
            Top = 228
            Width = 80
            Height = 21
            TabStop = False
            DataField = 'QtdeDevolucao'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 21
          end
          object DBEdit14: TDBEdit
            Left = 276
            Top = 228
            Width = 169
            Height = 21
            TabStop = False
            DataField = 'COBRANCALOCACAO'
            DataSource = DsItsLca
            Enabled = False
            TabOrder = 22
          end
          object TPVolta: TdmkEditDateTimePicker
            Left = 12
            Top = 284
            Width = 133
            Height = 21
            Date = 41131.724786689820000000
            Time = 41131.724786689820000000
            Enabled = False
            TabOrder = 23
            TabStop = False
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DataEmi'
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object EdVolta: TdmkEdit
            Left = 150
            Top = 284
            Width = 47
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 24
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfLong
            HoraFormat = dmkhfShort
            Texto = '00:00'
            QryCampo = 'HoraEmi'
            UpdCampo = 'HoraIni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdValorLocAtualizado: TdmkEdit
            Left = 232
            Top = 284
            Width = 85
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 25
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdValorLocAtualizadoRedefinido
          end
          object CkLimpo: TdmkCheckBox
            Left = 12
            Top = 312
            Width = 45
            Height = 17
            Caption = 'Limpo'
            TabOrder = 26
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkSujo: TdmkCheckBox
            Left = 92
            Top = 312
            Width = 45
            Height = 17
            Caption = 'Sujo'
            TabOrder = 27
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkQuebrado: TdmkCheckBox
            Left = 192
            Top = 312
            Width = 77
            Height = 17
            Caption = 'Quebrado'
            TabOrder = 28
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkTestadoDevolucao: TdmkCheckBox
            Left = 296
            Top = 312
            Width = 133
            Height = 17
            Caption = 'Testado na devolu'#231#227'o'
            TabOrder = 29
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object EdCOBRANCALOCACAO: TdmkEdit
            Left = 12
            Top = 352
            Width = 225
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 30
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdQuantidade: TdmkEdit
            Left = 240
            Top = 352
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 31
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdQuantidadeRedefinido
          end
          object EdVALORLOCACAO: TdmkEdit
            Left = 344
            Top = 352
            Width = 100
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 32
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdQtdPeriodo: TdmkEdit
            Left = 320
            Top = 284
            Width = 85
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 33
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdValorLocAtualizadoRedefinido
          end
          object EdDtHrDevolver_TXT: TdmkEdit
            Left = 332
            Top = 188
            Width = 112
            Height = 21
            TabOrder = 34
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 393
          Width = 449
          Height = 75
          Align = alClient
          TabOrder = 1
          object BtRecalcula: TBitBtn
            Tag = 3
            Left = 16
            Top = 16
            Width = 120
            Height = 40
            Caption = '&Recalcula'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtRecalculaClick
          end
        end
      end
      object Memo1: TMemo
        Left = 453
        Top = 0
        Width = 555
        Height = 470
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Lucida Console'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 518
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 562
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BtPula: TBitBtn
        Tag = 3
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pula'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtPulaClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 480
    Top = 3
  end
  object DsItsLca: TDataSource
    DataSet = QrItsLca
    Left = 620
    Top = 140
  end
  object QrItsLca: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrItsLcaAfterScroll
    SQL.Strings = (
      'SELECT lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN '
      'FROM loccitslca lpp '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ')
    Left = 620
    Top = 92
    object QrItsLcaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrItsLcaCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrItsLcaItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrItsLcaManejoLca: TSmallintField
      FieldName = 'ManejoLca'
      Required = True
    end
    object QrItsLcaGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrItsLcaValorDia: TFloatField
      FieldName = 'ValorDia'
      Required = True
    end
    object QrItsLcaValorSem: TFloatField
      FieldName = 'ValorSem'
      Required = True
    end
    object QrItsLcaValorQui: TFloatField
      FieldName = 'ValorQui'
      Required = True
    end
    object QrItsLcaValorMes: TFloatField
      FieldName = 'ValorMes'
      Required = True
    end
    object QrItsLcaRetFunci: TIntegerField
      FieldName = 'RetFunci'
      Required = True
    end
    object QrItsLcaRetExUsr: TIntegerField
      FieldName = 'RetExUsr'
      Required = True
    end
    object QrItsLcaDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
      Required = True
    end
    object QrItsLcaDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
      Required = True
    end
    object QrItsLcaLibExUsr: TIntegerField
      FieldName = 'LibExUsr'
      Required = True
    end
    object QrItsLcaLibFunci: TIntegerField
      FieldName = 'LibFunci'
      Required = True
    end
    object QrItsLcaLibDtHr: TDateTimeField
      FieldName = 'LibDtHr'
      Required = True
    end
    object QrItsLcaRELIB: TWideStringField
      FieldName = 'RELIB'
    end
    object QrItsLcaHOLIB: TWideStringField
      FieldName = 'HOLIB'
      Size = 5
    end
    object QrItsLcaQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrItsLcaValorProduto: TFloatField
      FieldName = 'ValorProduto'
      Required = True
    end
    object QrItsLcaQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrItsLcaValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
    end
    object QrItsLcaQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
    end
    object QrItsLcaTroca: TSmallintField
      FieldName = 'Troca'
      Required = True
    end
    object QrItsLcaCategoria: TWideStringField
      FieldName = 'Categoria'
      Required = True
      Size = 1
    end
    object QrItsLcaCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 40
    end
    object QrItsLcaCobrancaConsumo: TFloatField
      FieldName = 'CobrancaConsumo'
      Required = True
    end
    object QrItsLcaCobrancaRealizadaVenda: TWideStringField
      FieldName = 'CobrancaRealizadaVenda'
      Size = 1
    end
    object QrItsLcaCobranIniDtHr: TDateTimeField
      FieldName = 'CobranIniDtHr'
      Required = True
    end
    object QrItsLcaSaiDtHr: TDateTimeField
      FieldName = 'SaiDtHr'
      Required = True
    end
    object QrItsLcaValorFDS: TFloatField
      FieldName = 'ValorFDS'
      Required = True
    end
    object QrItsLcaDMenos: TSmallintField
      FieldName = 'DMenos'
      Required = True
    end
    object QrItsLcaValorLocAAdiantar: TFloatField
      FieldName = 'ValorLocAAdiantar'
      Required = True
    end
    object QrItsLcaValorLocAtualizado: TFloatField
      FieldName = 'ValorLocAtualizado'
      Required = True
    end
    object QrItsLcaCalcAdiLOCACAO: TWideStringField
      FieldName = 'CalcAdiLOCACAO'
      Size = 40
    end
    object QrItsLcaDiasUteis: TIntegerField
      FieldName = 'DiasUteis'
      Required = True
    end
    object QrItsLcaDiasFDS: TIntegerField
      FieldName = 'DiasFDS'
      Required = True
    end
    object QrItsLcaLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrItsLcaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrItsLcaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrItsLcaUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrItsLcaUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrItsLcaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrItsLcaAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrItsLcaAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrItsLcaAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrItsLcaValDevolParci: TFloatField
      FieldName = 'ValDevolParci'
      Required = True
    end
    object QrItsLcaNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrItsLcaREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Size = 25
    end
    object QrItsLcaLOGIN: TWideStringField
      FieldName = 'LOGIN'
      Size = 30
    end
    object QrItsLcaPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 60
    end
    object QrItsLcaOriSubstSaiDH: TDateTimeField
      FieldName = 'OriSubstSaiDH'
    end
    object QrItsLcaOriSubstItem: TIntegerField
      FieldName = 'OriSubstItem'
    end
    object QrItsLcaHrTolerancia: TTimeField
      FieldName = 'HrTolerancia'
    end
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL, '
      'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1, '
      
        'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXIS' +
        'TE,  '
      'cpl.Situacao, cpl.AtualValr, cpl.ValorMes, '
      'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia, '
      'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie, '
      'cpl.Voltagem, cpl.Potencia, cpl.Capacid'
      '   '
      'FROM gragrux ggx  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle  '
      'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle  '
      'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo  '
      'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao'
      'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle'
      'WHERE cpl.GraGrux=38'
      '')
    Left = 520
    Top = 92
    object QrGraGXPatrEstqSdo: TFloatField
      FieldName = 'EstqSdo'
    end
    object QrGraGXPatrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXPatrReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXPatrCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
    end
    object QrGraGXPatrNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXPatrCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
      Required = True
    end
    object QrGraGXPatrSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrGraGXPatrAtualValr: TFloatField
      FieldName = 'AtualValr'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorMes: TFloatField
      FieldName = 'ValorMes'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorQui: TFloatField
      FieldName = 'ValorQui'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorSem: TFloatField
      FieldName = 'ValorSem'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorDia: TFloatField
      FieldName = 'ValorDia'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorFDS: TFloatField
      FieldName = 'ValorFDS'
    end
    object QrGraGXPatrAgrupador: TIntegerField
      FieldName = 'Agrupador'
    end
    object QrGraGXPatrMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraGXPatrModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 60
    end
    object QrGraGXPatrSerie: TWideStringField
      FieldName = 'Serie'
      Size = 60
    end
    object QrGraGXPatrVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Size = 30
    end
    object QrGraGXPatrPotencia: TWideStringField
      FieldName = 'Potencia'
      Size = 30
    end
    object QrGraGXPatrCapacid: TWideStringField
      FieldName = 'Capacid'
      Size = 30
    end
    object QrGraGXPatrNO_SITAPL: TIntegerField
      FieldName = 'NO_SITAPL'
    end
  end
  object DsGraGXPatr: TDataSource
    DataSet = QrGraGXPatr
    Left = 520
    Top = 140
  end
  object QrLocCMovAll: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lma.* '
      'FROM loccmovall lma'
      'WHERE lma.Codigo=27352'
      'AND lma.GTRTab=1 '
      'AND lma.CtrID=34556 '
      'AND lma.Item=67331')
    Left = 408
    Top = 340
    object QrLocCMovAllCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocCMovAllGTRTab: TIntegerField
      FieldName = 'GTRTab'
      Required = True
    end
    object QrLocCMovAllCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrLocCMovAllItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrLocCMovAllControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLocCMovAllSEQUENCIA: TIntegerField
      FieldName = 'SEQUENCIA'
      Required = True
    end
    object QrLocCMovAllDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrLocCMovAllUSUARIO: TWideStringField
      FieldName = 'USUARIO'
      Size = 10
    end
    object QrLocCMovAllGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrLocCMovAllTipoES: TWideStringField
      FieldName = 'TipoES'
      Size = 1
    end
    object QrLocCMovAllQuantidade: TIntegerField
      FieldName = 'Quantidade'
    end
    object QrLocCMovAllTipoMotiv: TSmallintField
      FieldName = 'TipoMotiv'
    end
    object QrLocCMovAllCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 50
    end
    object QrLocCMovAllVALORLOCACAO: TFloatField
      FieldName = 'VALORLOCACAO'
      Required = True
    end
    object QrLocCMovAllLIMPO: TWideStringField
      FieldName = 'LIMPO'
      Size = 1
    end
    object QrLocCMovAllSUJO: TWideStringField
      FieldName = 'SUJO'
      Size = 1
    end
    object QrLocCMovAllQUEBRADO: TWideStringField
      FieldName = 'QUEBRADO'
      Size = 1
    end
    object QrLocCMovAllTESTADODEVOLUCAO: TWideStringField
      FieldName = 'TESTADODEVOLUCAO'
      Size = 1
    end
    object QrLocCMovAllGGXEntrada: TLargeintField
      FieldName = 'GGXEntrada'
    end
    object QrLocCMovAllMotivoTroca: TIntegerField
      FieldName = 'MotivoTroca'
      Required = True
    end
    object QrLocCMovAllObservacaoTroca: TWideStringField
      FieldName = 'ObservacaoTroca'
      Size = 512
    end
    object QrLocCMovAllQuantidadeLocada: TIntegerField
      FieldName = 'QuantidadeLocada'
    end
    object QrLocCMovAllQuantidadeJaDevolvida: TIntegerField
      FieldName = 'QuantidadeJaDevolvida'
    end
    object QrLocCMovAllLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrLocCMovAllDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocCMovAllDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocCMovAllUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrLocCMovAllUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrLocCMovAllAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLocCMovAllAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrLocCMovAllAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrLocCMovAllAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLocCMovAllNO_ManejoLca: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ManejoLca'
      Size = 60
      Calculated = True
    end
  end
end
