unit ImpGXPatr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, frxClass, frxDBSet, UnDmkEnums;

type
  TFmImpGXPatr = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrGPA: TmySQLQuery;
    DsGPA: TDataSource;
    DBGrid1: TDBGrid;
    QrGPASigla: TWideStringField;
    QrGPANO_GLS: TWideStringField;
    QrGPAReferencia: TWideStringField;
    QrGPANO_GG1: TWideStringField;
    QrGPAGraGruX: TIntegerField;
    frxDsGPA: TfrxDBDataset;
    frxIMP_PATRI_001_001: TfrxReport;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGPA();
  public
    { Public declarations }
  end;

  var
  FmImpGXPatr: TFmImpGXPatr;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmImpGXPatr.BtOKClick(Sender: TObject);
begin
  ReopenGPA();
  MyObjects.frxMostra(frxIMP_PATRI_001_001,
    'Lista de Patrim�nio Principal por Situa��o');
end;

procedure TFmImpGXPatr.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmImpGXPatr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmImpGXPatr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmImpGXPatr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImpGXPatr.ReopenGPA;
begin
  UMyMod.AbreQuery(QrGPA, Dmod.MyDB);
end;

end.
