object FmLocCItsMat: TFmLocCItsMat
  Left = 450
  Top = 286
  Caption = 'LOC-PATRI-105 :: Adi'#231#227'o de Item '#224' Loca'#231#227'o'
  ClientHeight = 324
  ClientWidth = 876
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 876
    Height = 162
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object SBCadastro: TSpeedButton
      Left = 615
      Top = 90
      Width = 23
      Height = 21
      Caption = '...'
      OnClick = SBCadastroClick
    end
    object Label7: TLabel
      Left = 257
      Top = 74
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label2: TLabel
      Left = 160
      Top = 74
      Width = 55
      Height = 13
      Caption = 'Refer'#234'ncia:'
    end
    object Label1: TLabel
      Left = 14
      Top = 74
      Width = 48
      Height = 13
      Caption = 'Reduzido:'
    end
    object Label21: TLabel
      Left = 14
      Top = 31
      Width = 134
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data / hora loca'#231#227'o / troca:'
      Enabled = False
    end
    object LaQtdeProduto: TLabel
      Left = 14
      Top = 114
      Width = 66
      Height = 13
      Caption = 'Qtde Produto:'
      Enabled = False
    end
    object LaQtdeLocacao: TLabel
      Left = 98
      Top = 114
      Width = 71
      Height = 13
      Caption = 'Qtde Loca'#231#227'o:'
      Enabled = False
    end
    object Label16: TLabel
      Left = 183
      Top = 114
      Width = 42
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Estoque:'
      Enabled = False
    end
    object Label4: TLabel
      Left = 72
      Top = 75
      Width = 52
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Patrim'#244'nio:'
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 257
      Top = 90
      Width = 604
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_GG1'
      ListSource = DsGraGXToll
      TabOrder = 3
      dmkEditCB = EdGraGruX
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pPrdTamCor
    end
    object EdReferencia: TdmkEdit
      Left = 160
      Top = 90
      Width = 97
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdReferenciaChange
    end
    object EdGraGruX: TdmkEditCB
      Left = 14
      Top = 90
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraGruXChange
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CkContinuar: TCheckBox
      Left = 254
      Top = 131
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo'
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
    object TPDataLoc: TdmkEditDateTimePicker
      Left = 14
      Top = 47
      Width = 99
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 41131.724786689820000000
      Time = 41131.724786689820000000
      Enabled = False
      TabOrder = 6
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdHoraLoc: TdmkEdit
      Left = 116
      Top = 47
      Width = 41
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Enabled = False
      TabOrder = 7
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfLong
      HoraFormat = dmkhfShort
      Texto = '00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdQtdeProduto: TdmkEdit
      Left = 15
      Top = 130
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1.000000000000000000
      ValWarn = False
    end
    object EdQtdeLocacao: TdmkEdit
      Left = 99
      Top = 130
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1.000000000000000000
      ValWarn = False
    end
    object DbEdEstqSdo: TDBEdit
      Left = 183
      Top = 130
      Width = 61
      Height = 21
      TabStop = False
      DataField = 'EstqSdo'
      DataSource = DsGraGXToll
      TabOrder = 9
    end
    object EdPatrimonio: TdmkEdit
      Left = 72
      Top = 90
      Width = 85
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdPatrimonioChange
      OnKeyDown = EdPatrimonioKeyDown
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 876
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 828
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 9
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 780
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 316
        Height = 32
        Caption = 'Adi'#231#227'o de Item '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 316
        Height = 32
        Caption = 'Adi'#231#227'o de Item '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 316
        Height = 32
        Caption = 'Adi'#231#227'o de Item '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 210
    Width = 876
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 872
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 254
    Width = 876
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 730
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 728
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DsGraGXToll: TDataSource
    DataSet = QrGraGXToll
    Left = 360
    Top = 66
  end
  object QrGraGXToll: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Referencia, gg1.Nome NO_GG1, '
      'ggo.GraGruX, ggo.ItemValr, ggo.ItemUnid'
      'FROM gragxoutr ggo '
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggo.Aplicacao=:P0')
    Left = 332
    Top = 66
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGXTollControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXTollReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXTollNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXTollGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGXTollItemValr: TFloatField
      FieldName = 'ItemValr'
    end
    object QrGraGXTollItemUnid: TIntegerField
      FieldName = 'ItemUnid'
    end
    object QrGraGXTollEstqSdo: TFloatField
      FieldName = 'EstqSdo'
    end
  end
end
