unit LocCItsRet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, UnAppEnums, Vcl.Mask, dmkEditDateTimePicker, dmkCheckBox;

type
  TFmLocCItsRet = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    DsItsLca: TDataSource;
    BtPula: TBitBtn;
    QrItsLca: TMySQLQuery;
    Memo1: TMemo;
    QrItsLcaCodigo: TIntegerField;
    QrItsLcaCtrID: TIntegerField;
    QrItsLcaItem: TIntegerField;
    QrItsLcaManejoLca: TSmallintField;
    QrItsLcaGraGruX: TIntegerField;
    QrItsLcaValorDia: TFloatField;
    QrItsLcaValorSem: TFloatField;
    QrItsLcaValorQui: TFloatField;
    QrItsLcaValorMes: TFloatField;
    QrItsLcaRetFunci: TIntegerField;
    QrItsLcaRetExUsr: TIntegerField;
    QrItsLcaDtHrLocado: TDateTimeField;
    QrItsLcaDtHrRetorn: TDateTimeField;
    QrItsLcaLibExUsr: TIntegerField;
    QrItsLcaLibFunci: TIntegerField;
    QrItsLcaLibDtHr: TDateTimeField;
    QrItsLcaRELIB: TWideStringField;
    QrItsLcaHOLIB: TWideStringField;
    QrItsLcaQtdeProduto: TIntegerField;
    QrItsLcaValorProduto: TFloatField;
    QrItsLcaQtdeLocacao: TIntegerField;
    QrItsLcaValorLocacao: TFloatField;
    QrItsLcaQtdeDevolucao: TIntegerField;
    QrItsLcaTroca: TSmallintField;
    QrItsLcaCategoria: TWideStringField;
    QrItsLcaCOBRANCALOCACAO: TWideStringField;
    QrItsLcaCobrancaConsumo: TFloatField;
    QrItsLcaCobrancaRealizadaVenda: TWideStringField;
    QrItsLcaCobranIniDtHr: TDateTimeField;
    QrItsLcaSaiDtHr: TDateTimeField;
    QrItsLcaValorFDS: TFloatField;
    QrItsLcaDMenos: TSmallintField;
    QrItsLcaValorLocAAdiantar: TFloatField;
    QrItsLcaValorLocAtualizado: TFloatField;
    QrItsLcaCalcAdiLOCACAO: TWideStringField;
    QrItsLcaDiasUteis: TIntegerField;
    QrItsLcaDiasFDS: TIntegerField;
    QrItsLcaLk: TIntegerField;
    QrItsLcaDataCad: TDateField;
    QrItsLcaDataAlt: TDateField;
    QrItsLcaUserCad: TIntegerField;
    QrItsLcaUserAlt: TIntegerField;
    QrItsLcaAlterWeb: TSmallintField;
    QrItsLcaAWServerID: TIntegerField;
    QrItsLcaAWStatSinc: TSmallintField;
    QrItsLcaAtivo: TSmallintField;
    QrItsLcaValDevolParci: TFloatField;
    QrItsLcaNO_GGX: TWideStringField;
    QrItsLcaREFERENCIA: TWideStringField;
    QrItsLcaLOGIN: TWideStringField;
    QrItsLcaPatrimonio: TWideStringField;
    Label18: TLabel;
    QrGraGXPatr: TMySQLQuery;
    QrGraGXPatrEstqSdo: TFloatField;
    QrGraGXPatrControle: TIntegerField;
    QrGraGXPatrReferencia: TWideStringField;
    QrGraGXPatrCOD_GG1: TIntegerField;
    QrGraGXPatrNO_GG1: TWideStringField;
    QrGraGXPatrCPL_EXISTE: TFloatField;
    QrGraGXPatrSituacao: TWordField;
    QrGraGXPatrAtualValr: TFloatField;
    QrGraGXPatrValorMes: TFloatField;
    QrGraGXPatrValorQui: TFloatField;
    QrGraGXPatrValorSem: TFloatField;
    QrGraGXPatrValorDia: TFloatField;
    QrGraGXPatrAgrupador: TIntegerField;
    QrGraGXPatrMarca: TIntegerField;
    QrGraGXPatrModelo: TWideStringField;
    QrGraGXPatrSerie: TWideStringField;
    QrGraGXPatrVoltagem: TWideStringField;
    QrGraGXPatrPotencia: TWideStringField;
    QrGraGXPatrCapacid: TWideStringField;
    QrGraGXPatrNO_SITAPL: TIntegerField;
    DsGraGXPatr: TDataSource;
    QrGraGXPatrValorFDS: TFloatField;
    QrLocCMovAll: TMySQLQuery;
    QrLocCMovAllCodigo: TIntegerField;
    QrLocCMovAllGTRTab: TIntegerField;
    QrLocCMovAllCtrID: TIntegerField;
    QrLocCMovAllItem: TIntegerField;
    QrLocCMovAllControle: TIntegerField;
    QrLocCMovAllSEQUENCIA: TIntegerField;
    QrLocCMovAllDataHora: TDateTimeField;
    QrLocCMovAllUSUARIO: TWideStringField;
    QrLocCMovAllGraGruX: TIntegerField;
    QrLocCMovAllTipoES: TWideStringField;
    QrLocCMovAllQuantidade: TIntegerField;
    QrLocCMovAllTipoMotiv: TSmallintField;
    QrLocCMovAllCOBRANCALOCACAO: TWideStringField;
    QrLocCMovAllVALORLOCACAO: TFloatField;
    QrLocCMovAllLIMPO: TWideStringField;
    QrLocCMovAllSUJO: TWideStringField;
    QrLocCMovAllQUEBRADO: TWideStringField;
    QrLocCMovAllTESTADODEVOLUCAO: TWideStringField;
    QrLocCMovAllGGXEntrada: TLargeintField;
    QrLocCMovAllMotivoTroca: TIntegerField;
    QrLocCMovAllObservacaoTroca: TWideStringField;
    QrLocCMovAllQuantidadeLocada: TIntegerField;
    QrLocCMovAllQuantidadeJaDevolvida: TIntegerField;
    QrLocCMovAllLk: TIntegerField;
    QrLocCMovAllDataCad: TDateField;
    QrLocCMovAllDataAlt: TDateField;
    QrLocCMovAllUserCad: TIntegerField;
    QrLocCMovAllUserAlt: TIntegerField;
    QrLocCMovAllAlterWeb: TSmallintField;
    QrLocCMovAllAWServerID: TIntegerField;
    QrLocCMovAllAWStatSinc: TSmallintField;
    QrLocCMovAllAtivo: TSmallintField;
    QrLocCMovAllNO_ManejoLca: TWideStringField;
    Panel5: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit17: TDBEdit;
    Label17: TLabel;
    SbPrecosContrato: TSpeedButton;
    Label19: TLabel;
    DBEdit18: TDBEdit;
    Label20: TLabel;
    DBEdit19: TDBEdit;
    Label22: TLabel;
    DBEdit20: TDBEdit;
    Label23: TLabel;
    DBEdit21: TDBEdit;
    Label24: TLabel;
    DBEdit22: TDBEdit;
    SBPrecosGerais: TSpeedButton;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    Label28: TLabel;
    EdControle: TdmkEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBEdit13: TDBEdit;
    Label13: TLabel;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label21: TLabel;
    TPVolta: TdmkEditDateTimePicker;
    EdVolta: TdmkEdit;
    SbVolta: TSpeedButton;
    EdValorLocAtualizado: TdmkEdit;
    Label25: TLabel;
    CkLimpo: TdmkCheckBox;
    CkSujo: TdmkCheckBox;
    CkQuebrado: TdmkCheckBox;
    CkTestadoDevolucao: TdmkCheckBox;
    Label29: TLabel;
    EdCOBRANCALOCACAO: TdmkEdit;
    EdQuantidade: TdmkEdit;
    Label26: TLabel;
    Label27: TLabel;
    EdVALORLOCACAO: TdmkEdit;
    Panel6: TPanel;
    EdQtdPeriodo: TdmkEdit;
    Label30: TLabel;
    QrItsLcaOriSubstSaiDH: TDateTimeField;
    QrItsLcaOriSubstItem: TIntegerField;
    BtRecalcula: TBitBtn;
    Label31: TLabel;
    EdDtHrDevolver_TXT: TdmkEdit;
    QrItsLcaHrTolerancia: TTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtPulaClick(Sender: TObject);
    procedure SBPrecosGeraisClick(Sender: TObject);
    procedure QrItsLcaAfterScroll(DataSet: TDataSet);
    procedure SbVoltaClick(Sender: TObject);
    procedure SbPrecosContratoClick(Sender: TObject);
    procedure EdQuantidadeRedefinido(Sender: TObject);
    procedure EdValorLocAtualizadoRedefinido(Sender: TObject);
    procedure BtRecalculaClick(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaDadosAtual();
    procedure CalculaValorLocacao();
    procedure MostraProximoItem();
  public
    { Public declarations }
    FCriando: Boolean;
    FCodigo, FCtrID, FItem, FControleAlt, FNewControle, FEmpresa: Integer;
    FTipoMotiv: TTipoMotivLocMov;
    FQuantidadeAlt: Double;
    FRecalcular: Boolean;
    FDtHrDevolver: TDateTime;
    //
    procedure ReopenItsLca();
    procedure ReopenLocCMovAll();
  end;

  var
  FmLocCItsRet: TFmLocCItsRet;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnAppPF, ModuleGeral, UMySQLModule,
  MyDBCheck, UndmkProcFunc;

{$R *.DFM}

procedure TFmLocCItsRet.BtConfirmaClick(Sender: TObject);
var
  SQLType: TSQLType;
  DataHora, USUARIO, TipoES, COBRANCALOCACAO, LIMPO, SUJO, QUEBRADO,
  TESTADODEVOLUCAO, ObservacaoTroca: String;
  Codigo, (*GTRTab,*) CtrID, Item, Controle, SEQUENCIA, GraGruX, Quantidade,
  TipoMotiv, MotivoTroca, QuantidadeLocada, QuantidadeJaDevolvida: Integer;
  VALORLOCACAO, GGXEntrada: Double;
  //
  ManejoLca, GTRTab: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  CtrID                 := FCtrID;
  Item                  := QrItsLcaItem.Value;
  Controle              := EdControle.ValueVariant;
  SEQUENCIA             := -1;
  DataHora              := Geral.FDT_TP_Ed(TPVolta.Date, EdVolta.Text);
  USUARIO               := '';
  GraGruX               := QrItsLcaGraGruX.Value;
  TipoES                := 'E'; // Entrada
  Quantidade            := EdQuantidade.ValueVariant;
  TipoMotiv             := Integer(FTipoMotiv);
  COBRANCALOCACAO       := EdCOBRANCALOCACAO.Text;
  // ini 2021-04-26
  //VALORLOCACAO          := EdValorLocAtualizado.ValueVariant;
  VALORLOCACAO          := EdVALORLOCACAO.ValueVariant;
  // fim 2021-04-26
  LIMPO                 := dmkPF.EscolhaDe2Str(CkLimpo.Checked, 'S', 'N');
  SUJO                  := dmkPF.EscolhaDe2Str(CkSujo.Checked, 'S', 'N');
  QUEBRADO              := dmkPF.EscolhaDe2Str(CkQuebrado.Checked, 'S', 'N');
  TESTADODEVOLUCAO      := dmkPF.EscolhaDe2Str(CkTestadoDevolucao.Checked, 'S', 'N');
  GGXEntrada            := 0;
  MotivoTroca           := 0;
  ObservacaoTroca       := '';
  QuantidadeLocada      := QrItsLcaQtdeProduto.Value;
  QuantidadeJaDevolvida := QrItsLcaQtdeDevolucao.Value;
  ManejoLca             := QrItsLcaManejoLca.Value;
  //
  if not AppPF.LocCItsRet_DadosDevolucaoOK(EdQuantidade,
  FQuantidadeAlt, QuantidadeLocada, QuantidadeJaDevolvida) then Exit;
  //
  if AppPF.LocCItsRet_InsereLocCMovAll(SQLType, DataHora, USUARIO, TipoES,
  COBRANCALOCACAO, LIMPO, SUJO, QUEBRADO, TESTADODEVOLUCAO, ObservacaoTroca,
  Codigo, ManejoLca, CtrID, Item, SEQUENCIA, GraGruX, Quantidade, TipoMotiv,
  MotivoTroca, QuantidadeLocada, QuantidadeJaDevolvida, VALORLOCACAO,
  GGXEntrada, 0(*SubstitutoLca*), Controle, GTRTab) then
  begin
    FRecalcular  := True;
    FNewControle := Controle;
    AppPF.AtualizaValDevolParci(Codigo, GTRTab, CtrID, Item);
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, FEmpresa, True);
    MostraProximoItem();
    //
  end;
end;

procedure TFmLocCItsRet.BtPulaClick(Sender: TObject);
begin
  MostraProximoItem();
end;

procedure TFmLocCItsRet.BtRecalculaClick(Sender: TObject);
begin
  CalculaDadosAtual();
end;

procedure TFmLocCItsRet.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCItsRet.CalculaDadosAtual();
const
  GeraLog = True;
var
  ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS, QtdeLocacao, QtdeProduto,
  QtdeDevolucao, ValorLocAntigo, ValDevolParci, ValorUnitario,
  ValorLocAtualizado: Double;
  DMenos, Troca, DiasFDS, DiasUteis: Integer;
  CalcAdiLOCACAO, Log: String;
  Calculou: Boolean;
  Saida, Volta, IniCobranca, OriSubstSaiDH, HrTolerancia: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  try
    //Volta    := QrLocCConCabDtHrDevolver;
    //
    DMenos         := QrItsLcaDMenos.Value;
    //
    if QrGraGXPatr.State <> dsInactive then
    begin
      ValorDia       := QrGraGXPatrValorDia.Value;
      ValorSem       := QrGraGXPatrValorSem.Value;
      ValorQui       := QrGraGXPatrValorQui.Value;
      ValorMes       := QrGraGXPatrValorMes.Value;
      ValorFDS       := QrGraGXPatrValorFDS.Value;
    end else
    begin
      ValorDia       := QrItsLcaValorDia.Value;
      ValorSem       := QrItsLcaValorSem.Value;
      ValorQui       := QrItsLcaValorQui.Value;
      ValorMes       := QrItsLcaValorMes.Value;
      ValorFDS       := QrItsLcaValorFDS.Value;
    end;
    //
    QtdeLocacao    := QrItsLcaQtdeLocacao.Value;
    QtdeProduto    := QrItsLcaQtdeProduto.Value;
    QtdeDevolucao  := QrItsLcaQtdeDevolucao.Value;
    ValorLocAntigo := QrItsLcaValorLocAtualizado.Value;
    ValDevolParci  := QrItsLcaValDevolParci.Value;
    Troca          := QrItsLcaTroca.Value;
    IniCobranca    := QrItsLcaCobranIniDtHr.Value;
    OriSubstSaiDH  := QrItsLcaOriSubstSaiDH.Value;
    HrTolerancia   := QrItsLcaHrTolerancia.Value;
    //
    DiasFDS            := 0;
    DiasUteis          := 0;
    CalcAdiLOCACAO     := '';
    ValorUnitario      := 0;
    ValorLocAtualizado := 0.00;
    //
    Calculou           := False;
    Saida              := QrItsLcaSaiDtHr.Value;
    Volta              := Trunc(TPVolta.Date) + EdVolta.ValueVariant;
    //
    AppPF.CalculaDadosEntreDatasLocacao_COM_CobrarExtraFDS((*const*) GeraLog,
    (*const*) Saida, Volta, HrTolerancia, OriSubstSaiDH, (*const*) DMenos, (*cons*)ValorDia, ValorSem,
    ValorQui, ValorMes, ValorFDS, QtdeLocacao, QtdeProduto, QtdeDevolucao,
    ValDevolParci, Troca, FDtHrDevolver, (*var*) Log, (*var*)IniCobranca, (*var*)DiasFDS,
    DiasUteis, (*var*)CalcAdiLOCACAO,
    ValorUnitario, ValorLocAtualizado, Calculou);
    //
    EdQtdPeriodo.ValueVariant := QtdeLocacao * (QtdeProduto -  QtdeDevolucao);
    EdValorLocAtualizado.ValueVariant := ValorLocAtualizado;
    //EdValorLocAtualizado.ValueVariant := ValorUnitario;
    EdCOBRANCALOCACAO.Text := CalcAdiLOCACAO;
    //
    Memo1.Text := Log;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocCItsRet.CalculaValorLocacao();
var
  ValorLocAtualizado, Quantidade, QtdPeriodo, QtdInicial, ValorLocacao,
  ValorRestante: Double;
begin
  ValorLocAtualizado := EdValorLocAtualizado.ValueVariant;
  Quantidade := EdQuantidade.ValueVariant;
  //
  QtdPeriodo := EdQtdPeriodo.ValueVariant;
  if (QtdPeriodo > 0) and (Quantidade > 0) then
  begin
    //ValorLocacao := Quantidade * ValorLocAtualizado;
    ValorRestante := ValorLocAtualizado - QrItsLcaValDevolParci.Value;
    ValorLocacao := Quantidade * (ValorRestante / QtdPeriodo);
  end else
    ValorLocacao := 0;
  //
{
  QtdInicial := QrItsLcaQtdeProduto.Value;
  if (QtdInicial > 0) and (Quantidade > 0) then
  begin
    ValorLocacao := Quantidade * ValorLocAtualizado / QtdInicial;
    ValorLocacao := ValorLocacao - QrItsLcaValDevolParci.Value;
  end else
    ValorLocacao := 0;
}
  //
  EdVALORLOCACAO.ValueVariant := ValorLocacao;
end;

procedure TFmLocCItsRet.EdQuantidadeRedefinido(Sender: TObject);
begin
  BtConfirma.Enabled := EdQuantidade.ValueVariant <> 0;
  CalculaValorLocacao();
end;

procedure TFmLocCItsRet.EdValorLocAtualizadoRedefinido(Sender: TObject);
begin
  CalculaValorLocacao();
end;

procedure TFmLocCItsRet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCItsRet.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  FRecalcular          := False;
  FNewControle         := 0;
  FCriando             := True;
  FQuantidadeAlt       := 0;
  ImgTipo.SQLType      := stLok;
  //
  Agora                := DModG.ObtemAgora();
  TPVolta.Date         := Trunc(Agora);
  EdVolta.ValueVariant := Agora;
end;

procedure TFmLocCItsRet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCItsRet.FormShow(Sender: TObject);
begin
  if FCriando then
  begin
    FCriando := False;
    //
    if ImgTipo.SQLType = stIns then
      CalculaDadosAtual();
  end;
end;

procedure TFmLocCItsRet.MostraProximoItem();
begin
  if QrItsLca.RecNo < QrItsLca.RecordCount then
  begin
    QrItsLca.Next;
    CalculaDadosAtual();
    EdQuantidade.SetFocus;
  end else
    Close;
end;

procedure TFmLocCItsRet.QrItsLcaAfterScroll(DataSet: TDataSet);
begin
  QrGraGXPatr.Close;
end;

procedure TFmLocCItsRet.ReopenItsLca();
begin
  AppPF.LocCItsRet_ReopenItsLca(QrItsLca, BtPula, FItem, FCodigo, FCtrID);
end;

procedure TFmLocCItsRet.ReopenLocCMovAll();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCMovAll, Dmod.MyDB, [
  'SELECT lma.*',
  //SQL_ManejoLca,
  'FROM loccmovall lma ',
  'WHERE lma.Controle=' + Geral.FF0(EdControle.ValueVariant),
  '']);
  FQuantidadeAlt              := QrLocCMovAllQuantidade.Value;
  //
  TPVolta.Date                := Trunc(QrLocCMovAllDataHora.Value);
  EdVolta.ValueVariant        := QrLocCMovAllDataHora.Value;
  CkLimpo.Checked             := QrLocCMovAllLimpo.Value = 'S';
  CkSujo.Checked              := QrLocCMovAllSujo.Value = 'S';
  CkQuebrado.Checked          := QrLocCMovAllQuebrado.Value = 'S';
  CkTestadoDevolucao.Checked  := QrLocCMovAllTestadoDevolucao.Value = 'S';
  EdCOBRANCALOCACAO.Text      := QrLocCMovAllCOBRANCALOCACAO.Value;
  EdQuantidade.ValueVariant   := QrLocCMovAllQuantidade.Value;
  EdVALORLOCACAO.ValueVariant := QrLocCMovAllVALORLOCACAO.Value;
end;

procedure TFmLocCItsRet.SbPrecosContratoClick(Sender: TObject);
begin
  QrGraGXPatr.Close;
  //
  CalculaDadosAtual();
  //
  EdQuantidade.SetFocus;
end;

procedure TFmLocCItsRet.SBPrecosGeraisClick(Sender: TObject);
begin
  AppPF.LocCItsRet_ReopnGraGXPatr(QrGraGXPatr, FEmpresa, QrItsLcaGraGruX.Value);
  //
  CalculaDadosAtual();
  //
  EdQuantidade.SetFocus;
end;

procedure TFmLocCItsRet.SbVoltaClick(Sender: TObject);
var
  MinData, Volta, NovaVolta: TDateTime;
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
    Exit;
  MinData := QrItsLcaCobranIniDtHr.Value;
  Volta   := Trunc(TPVolta.DateTime) + EdVolta.ValueVariant;
  //
  if DBCheck.ObtemData(Volta, NovaVolta, MinData, EdVolta.ValueVariant, True, 'Retorno do equipamento') then
  begin
    TPVolta.Date         := Trunc(NovaVolta);
    EdVolta.ValueVariant := NovaVolta;
    //
    CalculaDadosAtual();
  end;
end;

end.
