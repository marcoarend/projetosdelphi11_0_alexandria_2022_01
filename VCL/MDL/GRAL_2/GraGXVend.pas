unit GraGXVend;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, Module, Grids, DBGrids,
  ComCtrls, dmkEditDateTimePicker, dmkMemo, Menus, UnDmkEnums, AppListas,
  dmkCheckBox, UnAppEnums, dmkCompoStore;

type
  TFmGraGXVend = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrGraGXVend: TMySQLQuery;
    DsGraGXVend: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrMarcas: TmySQLQuery;
    DsMarcas: TDataSource;
    QrMarcasControle: TIntegerField;
    QrMarcasNO_MARCA_FABR: TWideStringField;
    QrGraGXVendGraGruX: TIntegerField;
    QrGraGXVendNO_GG1: TWideStringField;
    QrGraGXVendItemValr: TFloatField;
    Label25: TLabel;
    Label7: TLabel;
    EdGraGruX: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrAgrupador: TmySQLQuery;
    QrAgrupadorControle: TIntegerField;
    QrAgrupadorNome: TWideStringField;
    DsAgrupador: TDataSource;
    QrGraGXVendControle: TIntegerField;
    QrGraGXVendCOD_GG1: TIntegerField;
    Label39: TLabel;
    EdReferencia: TdmkEdit;
    Label46: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    QrGraGXVendAplicacao: TIntegerField;
    QrGraGXVendReferencia: TWideStringField;
    QrUnidMed: TmySQLQuery;
    DsUnidMed: TDataSource;
    QrGraGXVendItemUnid: TIntegerField;
    QrGraGXVendSIGLA: TWideStringField;
    QrGraGXVendCPL_EXISTE: TFloatField;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label37: TLabel;
    EdItemValr: TdmkEdit;
    Label3: TLabel;
    EdItemUnid: TdmkEditCB;
    CBItemUnid: TdmkDBLookupComboBox;
    RGAplicacao: TdmkRadioGroup;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBRGAplicacao: TDBRadioGroup;
    QrLocod: TmySQLQuery;
    SBItemUnid: TSpeedButton;
    PMAltera: TPopupMenu;
    Alteraprodutoatual1: TMenuItem;
    N1: TMenuItem;
    Editanveis1: TMenuItem;
    Editadadosdoproduto1: TMenuItem;
    QrGraGXVendNivel1: TIntegerField;
    QrGraGXVendNivel2: TIntegerField;
    QrGraGXVendNivel3: TIntegerField;
    QrGraGXVendNivel4: TIntegerField;
    QrGraGXVendNivel5: TIntegerField;
    QrGraGXVendCodUsu: TIntegerField;
    QrGraGXVendPrdGrupTip: TIntegerField;
    GroupBox8: TGroupBox;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    Label47: TLabel;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    QrGraGXVendNO_GG2: TWideStringField;
    QrGraGXVendNO_GG3: TWideStringField;
    QrGraGXVendNO_GG4: TWideStringField;
    QrGraGXVendNO_GG5: TWideStringField;
    QrGraGXVendNO_GGT: TWideStringField;
    QrGraGXVendTitNiv1: TWideStringField;
    QrGraGXVendTitNiv2: TWideStringField;
    QrGraGXVendTitNiv3: TWideStringField;
    QrGraGXVendTitNiv4: TWideStringField;
    QrGraGXVendTitNiv5: TWideStringField;
    Label49: TLabel;
    EdNCM: TdmkEdit;
    SpeedButton6: TSpeedButton;
    QrGraGXVendNCM: TWideStringField;
    QrGraGXVendCUNivel5: TIntegerField;
    QrGraGXVendCUNivel4: TIntegerField;
    QrGraGXVendCUNivel3: TIntegerField;
    QrGraGXVendCUNivel2: TIntegerField;
    QrPesq1: TMySQLQuery;
    QrPesq1Controle: TIntegerField;
    DBCkAtivo: TDBCheckBox;
    CkAtivo: TdmkCheckBox;
    QrGraGXVendAtivo: TSmallintField;
    QrEstq: TMySQLQuery;
    DsEstq: TDataSource;
    QrEstqEmpresa: TIntegerField;
    QrEstqStqCenCad: TIntegerField;
    QrEstqNO_StqCenCad: TWideStringField;
    QrEstqQtde: TFloatField;
    QrGraGruVal: TMySQLQuery;
    DsGraGruVal: TDataSource;
    QrGraGruValNO_GraCusPrc: TWideStringField;
    QrGraGruValCustoPreco: TFloatField;
    PnValores: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    Label5: TLabel;
    Label6: TLabel;
    Splitter1: TSplitter;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    EdMaxPercDesco: TdmkEdit;
    Label10: TLabel;
    QrGraGXVendMaxPercDesco: TFloatField;
    Label11: TLabel;
    DBEdit5: TDBEdit;
    QrGraGXVendEAN13: TWideStringField;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    EdEAN13: TdmkEdit;
    Label13: TLabel;
    SbDescoMin: TBitBtn;
    QrExec: TMySQLQuery;
    BtPsqEAN13: TBitBtn;
    CSTabSheetChamou: TdmkCompoStore;
    Panel10: TPanel;
    BtIncVal: TBitBtn;
    BtAltVal: TBitBtn;
    BtExcVal: TBitBtn;
    QrGraGruValControle: TIntegerField;
    QrGraGruValGraGruX: TIntegerField;
    QrGraGruValGraCusPrc: TIntegerField;
    QrGraGruValEntidade: TIntegerField;
    QrGraGruValDataIni: TDateField;
    Label15: TLabel;
    Label16: TLabel;
    Label14: TLabel;
    EdMarca: TdmkEditCB;
    CBMarca: TdmkDBLookupComboBox;
    SbMarcas: TSpeedButton;
    Label40: TLabel;
    EdModelo: TdmkEdit;
    QrGraGXVendNO_MARCA: TWideStringField;
    QrGraGXVendNO_FABR: TWideStringField;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    QrGraGXVendModelo: TWideStringField;
    DBEdit9: TDBEdit;
    QrGraGXVendMarca: TIntegerField;
    DBEdit10: TDBEdit;
    PnCompras: TPanel;
    GBCompras: TDBGrid;
    QrStqInn: TMySQLQuery;
    QrStqInnEmpresa: TIntegerField;
    QrStqInnCodigo: TIntegerField;
    QrStqInnFornece: TIntegerField;
    QrStqInnTransporta: TIntegerField;
    QrStqInnnfe_nNF: TIntegerField;
    QrStqInnDataHora: TDateTimeField;
    QrStqInnIDCtrl: TIntegerField;
    QrStqInnOriCodi: TIntegerField;
    QrStqInnOriCtrl: TIntegerField;
    QrStqInnQtde: TFloatField;
    QrStqInnCustoBuy: TFloatField;
    QrStqInnCustoFrt: TFloatField;
    QrStqInnCustoAll: TFloatField;
    QrStqInnNO_FRN: TWideStringField;
    QrStqInnNO_TRANSP: TWideStringField;
    DsStqInn: TDataSource;
    QrStqInnCustoUni: TFloatField;
    QrStqInnPackGGX: TIntegerField;
    QrStqInnUnidMed: TIntegerField;
    QrStqInnNO_GG1: TWideStringField;
    QrStqInnNO_COR: TWideStringField;
    QrStqInnNO_TAM: TWideStringField;
    QrStqInnNo_SIGLA: TWideStringField;
    QrStqInnSigla: TWideStringField;
    QrStqInnGrandeza: TSmallintField;
    QrStqInnPackQtde: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGXVendAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGXVendBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SBItemUnidClick(Sender: TObject);
    procedure Alteraprodutoatual1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure Editanveis1Click(Sender: TObject);
    procedure Editadadosdoproduto1Click(Sender: TObject);
    procedure PMAlteraPopup(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure QrGraGXVendAfterScroll(DataSet: TDataSet);
    procedure QrGraGXVendBeforeClose(DataSet: TDataSet);
    procedure EdNCMRedefinido(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbDescoMinClick(Sender: TObject);
    procedure BtPsqEAN13Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtAltValClick(Sender: TObject);
  private
    FGraToolRent: TGraToolRent;
    FTabNome,
    FFrmNome: String;
    procedure FiltraItensAExibir(Material: TGraToolRent);
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure IncluiProduto();
    procedure AlteraProduto();
    procedure MostraFormPrdGruNew(SQLTipo: TSQLType; Nivel5, Nivel4, Nivel3,
              Nivel2, Nivel1: Integer);
    procedure ReopenEstq();
    procedure ReopenGraGruVal(Controle: Integer);
    procedure ReopenStqInn();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure PesquisaNomeMateriais();
  end;

var
  FmGraGXVend: TFmGraGXVend;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, DmkDAC_PF, Principal, MyDBCheck, GraGru1, ModuleGeral,
  MeuDBUses, Curinga, UnGrade_Jan, ClasFisc, ModProd, GetCodigoBarras,
  ChangeValDmk, MyGlyfs, GetValor;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraGXVend.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraGXVend.PesquisaNomeMateriais();
var
  Codigo: Integer;
  Filtro: String;
  SQL: String;
begin
  // Sem Filtro: T i s o l i n
  //if Dmod.QrOpcoesTRenGraNivVend.Value = 7 then
  begin
(*
    Codigo := CuringaLoc.CriaForm('ggx.Controle', 'gg1.Nome',
      'gragrux ggx', Dmod.MyDB, 'AND ' + Filtro + ' AND gg1.Nivel1>0',
      False,
      'LEFT JOIN gragxvend cpl ON cpl.GraGruX=ggx.Controle '+ sLineBreak +
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '+ sLineBreak +
      'LEFT JOIN gragruy ggy ON ggy.COdigo=ggx.GraGruY');
*)
    SQL := Geral.ATS([
    'SELECT lpc.GraGruX _CODIGO, gg1.REFERENCIA, gg1.Nome _NOME ',
    'FROM gragxvend lpc ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE gg1.Nome LIKE "%' + CO_JOKE_SQL + '%" ',
    'OR gg1.Referencia LIKE "%' + CO_JOKE_SQL + '%" ',
    'OR gg1.Patrimonio LIKE "%' + CO_JOKE_SQL + '%" ',
    'OR ggx.EAN13 LIKE "%' + CO_JOKE_SQL + '%" ',
    '']);
    FmMeuDBUses.PesquisaNomeSQLeJoke(SQL, CO_JOKE_SQL);
    if VAR_CADASTRO <> 0 then
      LocCod(QrGraGXVendControle.Value, VAR_CADASTRO);
(*
  end else
    // Com Filtro: AAPA
  begin
    Dmod.FiltroGrade(FGraToolRent, Filtro);
    //
    Codigo := CuringaLoc.CriaForm('ggx.Controle', 'gg1.Nome',
      'gragrux ggx', Dmod.MyDB, 'AND ' + Filtro + ' AND gg1.Nivel1>0',
      False, 'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
    //
    LocCod(QrGraGXVendControle.Value, Codigo);
*)
  end;
end;

procedure TFmGraGXVend.PMAlteraPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(Alteraprodutoatual1, QrGraGXVend);
  MyObjects.HabilitaMenuItemCabUpd(Editanveis1, QrGraGXVend);
  MyObjects.HabilitaMenuItemCabUpd(Editadadosdoproduto1, QrGraGXVend);
end;

procedure TFmGraGXVend.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraGXVendControle.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraGXVend.DefParams;
var
  //GraNiv: Integer;
  Filtro: String;
begin
  if FGraToolRent <> gbsIndef then
  begin
    // Sem Filtro: T i s o l i n
    //if Dmod.QrOpcoesTRenGraNivVend.Value = ? then
    begin
      VAR_GOTOTABELA := 'gragxvend cpl, gragrux ggx, gragru1 gg1';
      VAR_GOTOMYSQLTABLE := QrGraGXVend;
      VAR_GOTONEG := gotoPos;
      VAR_GOTOCAMPO := 'cpl.GraGruX';
      VAR_GOTONOME := 'gg1.Nome';
      VAR_GOTOMySQLDBNAME := Dmod.MyDB;
      VAR_GOTOVAR := 1;

      GOTOy.LimpaVAR_SQL;

      Filtro := ' cpl.GraGrux=ggx.Controle AND ggx.GraGru1=gg1.Nivel1 ';

      VAR_SQLx.Add('SELECT ');
      VAR_SQLx.Add('ggx.Controle, ggx.EAN13, gg1.Referencia, ');
      VAR_SQLx.Add('gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, ');
      VAR_SQLx.Add('gfm.Nome NO_MARCA, gfc.Nome NO_FABR, ');
      VAR_SQLx.Add('IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, ');
      VAR_SQLx.Add('med.SIGLA, gg1.Nivel1, gg1.Nivel2, gg1.Nivel3, gg1.Nivel4, ');
      VAR_SQLx.Add('gg2.Nome NO_GG2, gg3.Nome NO_GG3, gg4.Nome NO_GG4, ');
      VAR_SQLx.Add('gg5.Nome NO_GG5, ggt.Nome NO_GGT, gg1.Nivel5, gg1.CodUsu, ');
      VAR_SQLx.Add('gg1.PrdGrupTip, ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3, ');
      VAR_SQLx.Add('ggt.TitNiv4, ggt.TitNiv5, ');
      VAR_SQLx.Add('gg5.CodUsu CUNivel5, gg4.CodUsu CUNivel4, ');
      VAR_SQLx.Add('gg3.CodUsu CUNivel3, gg2.CodUsu CUNivel2, ');
      VAR_SQLx.Add('gg1.NCM, cpl.* ');
      VAR_SQLx.Add('FROM gragxvend cpl ');
      VAR_SQLx.Add('LEFT JOIN gragrux ggx ON cpl.GraGruX=ggx.Controle ');
      VAR_SQLx.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ');
      VAR_SQLx.Add('LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2 ');
      VAR_SQLx.Add('LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3 ');
      VAR_SQLx.Add('LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg1.Nivel4 ');
      VAR_SQLx.Add('LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5 ');
      VAR_SQLx.Add('LEFT JOIN prdgruptip ggt ON ggt.Codigo=gg1.PrdGrupTip ');
      VAR_SQLx.Add('LEFT JOIN unidmed med ON med.Codigo=cpl.ItemUnid ');
      //
      VAR_SQLx.Add('LEFT JOIN gragruy ggy ON ggy.Codigo=ggx.Controle ');
      //
      VAR_SQLx.Add('LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle ');
      VAR_SQLx.Add('LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo ');
{
    end else
    // Com Filtro: AAPA
    begin
      VAR_GOTOTABELA := 'gragrux ggx, gragru1 gg1';
      VAR_GOTOMYSQLTABLE := QrGraGXVend;
      VAR_GOTONEG := gotoPos;
      VAR_GOTOCAMPO := 'ggx.Controle';
      VAR_GOTONOME := 'gg1.Nome';
      VAR_GOTOMySQLDBNAME := Dmod.MyDB;
      VAR_GOTOVAR := 1;

      GOTOy.LimpaVAR_SQL;

      Dmod.FiltroGrade(FGraToolRent, Filtro);
      Filtro := Filtro + ' AND (gg1.Nivel1=ggx.GraGru1)';
      //
      VAR_SQLx.Add('SELECT ');
      VAR_SQLx.Add('ggx.Controle, gg1.Referencia, ');
      VAR_SQLx.Add('gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, ');
      VAR_SQLx.Add('IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, ');
      VAR_SQLx.Add('med.SIGLA, gg1.Nivel1, gg1.Nivel2, gg1.Nivel3, gg1.Nivel4, ');
      VAR_SQLx.Add('gg2.Nome NO_GG2, gg3.Nome NO_GG3, gg4.Nome NO_GG4, ');
      VAR_SQLx.Add('gg5.Nome NO_GG5, ggt.Nome NO_GGT, gg1.Nivel5, gg1.CodUsu, ');
      VAR_SQLx.Add('gg1.PrdGrupTip, ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3, ');
      VAR_SQLx.Add('ggt.TitNiv4, ggt.TitNiv5, ');
      VAR_SQLx.Add('gg5.CodUsu CUNivel5, gg4.CodUsu CUNivel4, ');
      VAR_SQLx.Add('gg3.CodUsu CUNivel3, gg2.CodUsu CUNivel2, ');
      VAR_SQLx.Add('gg1.NCM, cpl.* ');
      VAR_SQLx.Add('FROM gragrux ggx ');
      VAR_SQLx.Add('LEFT JOIN ' + FTabNome + ' cpl ON cpl.GraGruX=ggx.Controle ');
      VAR_SQLx.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ');
      VAR_SQLx.Add('LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2 ');
      VAR_SQLx.Add('LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3 ');
      VAR_SQLx.Add('LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg1.Nivel4 ');
      VAR_SQLx.Add('LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5 ');
      VAR_SQLx.Add('LEFT JOIN prdgruptip ggt ON ggt.Codigo=gg1.PrdGrupTip ');
      VAR_SQLx.Add('LEFT JOIN unidmed med ON med.Codigo=cpl.ItemUnid ');
}
    end;
    VAR_SQLx.Add('');
    VAR_SQLx.Add('WHERE ' + Filtro);
    //
    VAR_SQL1.Add('AND ggx.Controle=:P0');
    //
    //VAR_SQL2.Add('AND gg1.CodUsu=:P0');
    //
    VAR_SQLa.Add('AND gg1.Nome LIKE :P0');
    //
    //
    VAR_GOTOVAR1 := Filtro;
    //
  end;
end;

procedure TFmGraGXVend.Editadadosdoproduto1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrGraGXVendControle.Value;
  //
  Grade_Jan.MostraFormGraGruN(QrGraGXVendNivel1.Value);
  LocCod(Controle, Controle);
end;

procedure TFmGraGXVend.Editanveis1Click(Sender: TObject);
begin
  MostraFormPrdGruNew(stUpd, QrGraGXVendCUNivel5.Value, QrGraGXVendCUNivel4.Value,
    QrGraGXVendCUNivel3.Value, QrGraGXVendCUNivel2.Value, QrGraGXVendCodUsu.Value);
end;

procedure TFmGraGXVend.EdNCMRedefinido(Sender: TObject);
begin
  EdNCM.Text := Geral.FormataNCM(EdNCM.Text);
end;

procedure TFmGraGXVend.CriaOForm;
begin
  DefineONomeDoForm;
end;

procedure TFmGraGXVend.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraGXVend.ReopenEstq();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstq, Dmod.MyDB, [
  'SELECT smia.Empresa, smia.StqCenCad,  ',
  'scc.NOme NO_StqCenCad, ',
  'SUM(smia.Qtde * smia.Baixa) Qtde  ',
  'FROM stqmovitsa smia ',
  'LEFT JOIN stqcencad scc ON scc.Codigo=smia.StqCenCad ',
  'WHERE smia.Ativo=1  ',
  'AND smia.ValiStq <> 101', // 2020-11-22
  'AND smia.GraGruX=' + Geral.FF0(QrGraGXVendGraGruX.Value),
  'GROUP BY smia.GraGruX, smia.Empresa, smia.StqCenCad ',
  ' ']);
end;

procedure TFmGraGXVend.ReopenGraGruVal(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruVal, Dmod.MyDB, [
  'SELECT ggv.Controle, ggv.GraGruX, ',
  'ggv.GraCusPrc, ggv.Entidade, ggv.DataIni, ',
  'gcp.Nome NO_GraCusPrc, ggv.CustoPreco  ',
  'FROM gragruval ggv ',
  'LEFT JOIN gracusprc gcp ON gcp.Codigo=ggv.GraCusPrc ',
  'WHERE ggv.GraGruX=' + Geral.FF0(QrGraGXVendGraGruX.Value),
  ' ']);
  //
  if Controle <> 0 then
    QrGraGruVal.Locate('Controle', Controle, []);
end;

procedure TFmGraGXVend.ReopenStqInn();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqInn, Dmod.MyDB, [
  'SELECT sic.Empresa, sic.Codigo, sic.Fornece,  ',
  'sic.Transporta, sic.nfe_nNF, ',
  'smia.DataHora, smia.IDCtrl, smia.OriCodi,  ',
  'smia.OriCtrl, smia.Qtde, smia.CustoBuy,  ',
  'smia.CustoFrt, smia.CustoAll, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN, ',
  'IF(tsp.Tipo=0, tsp.RazaoSocial, tsp.Nome) NO_TRANSP,  ',
  'IF(smia.Qtde > 0, smia.CustoAll/smia.Qtde, 0.00) CustoUni, ',
  'smia.PackGGX, smia.PackQtde, gg1.UnidMed, gg1.Nome NO_GG1,   ',
  'gcc.Nome NO_COR,  gti.Nome NO_TAM,  ',
  'med.Nome No_SIGLA, med.Sigla, med.Grandeza  ',
  'FROM stqmovitsa smia ',
  'LEFT JOIN stqinncad sic ON sic.Codigo=smia.OriCodi ',
  'LEFT JOIN entidades frn ON frn.Codigo=sic.Fornece ',
  'LEFT JOIN entidades tsp ON tsp.Codigo=sic.Transporta ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=smia.PackGGX ',
  'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip  ',
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  ',
  'WHERE smia.Tipo=' + Geral.FF0(VAR_FATID_0005),
//  'WHERE (smia.Tipo=' + Geral.FF0(VAR_FATID_0005) +
//    ' OR (smia.Tipo=' + Geral.FF0(VAR_FATID_0000) + ' AND Baixa=1) ) ',
  'AND smia.GraGruX=' + Geral.FF0(QrGraGXVendGraGruX.Value),
  'ORDER BY smia.DataHora DESC ',
  '']);
  //Geral.MB_Teste(QrStqInn.SQL.Text);
end;

procedure TFmGraGXVend.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraGXVend.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraGXVend.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraGXVend.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraGXVend.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraGXVend.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmClasFisc, FmClasFisc, afmoNegarComAviso) then
  begin
    FmClasFisc.BtSeleciona.Visible := True;
    FmClasFisc.ShowModal;
    if FmClasFisc.FNCMSelecio <> '' then
      EdNCM.Text := FmClasFisc.FNCMSelecio;
    FmClasFisc.Destroy;
  end;
end;

procedure TFmGraGXVend.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGXVend.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraGXVendGraGruX.Value;
  if TFmGraGXVend(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmGraGXVend.Alteraprodutoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGXVend, [PnDados],
    [PnEdita], EdReferencia, ImgTipo, 'Controle');
  //
  EdGraGruX.ValueVariant      := QrGraGXVendControle.Value;
  EdReferencia.Text           := QrGraGXVendReferencia.Value;
  EdNome.Text                 := QrGraGXVendNO_GG1.Value;
  RGAplicacao.ItemIndex       := QrGraGXVendAplicacao.Value;
  EdItemValr.ValueVariant     := QrGraGXVendItemValr.Value;
  EdItemUnid.ValueVariant     := QrGraGXVendItemUnid.Value;
  CBItemUnid.KeyValue         := QrGraGXVendItemUnid.Value;
  EdMaxPercDesco.ValueVariant := QrGraGXVendMaxPercDesco.Value;
  EdNCM.ValueVariant          := QrGraGXVendNCM.Value;
  EdEAN13.ValueVariant        := QrGraGXVendEAN13.Value;
  CkAtivo.Checked             := Geral.IntToBool(QrGraGXVendAtivo.Value);
  EdMarca.ValueVariant        := QrGraGXVendMarca.Value;
  CBMarca.KeyValue            := QrGraGXVendMarca.Value;
  EdMOdelo.ValueVariant       := QrGraGXVendModelo.Value;
end;

procedure TFmGraGXVend.BtAlteraClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
    Exit;
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmGraGXVend.BtAltValClick(Sender: TObject);
  function ObtemCusto(var CusPrc: Double): Boolean;
  var
    ResVar: Variant;
    CasasDecimais: Integer;
  begin
    CusPrc          := 0;
    Result        := False;
    CasasDecimais := 2;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    0, CasasDecimais, 0, '', '', True, 'Itens', 'Informe a quantidade de itens: ',
    0, ResVar) then
    begin
      CusPrc := Geral.DMV(ResVar);
      Result := True;
    end;
  end;
var
  CustoPreco: Double;
  Controle, GraGruX, GraCusPrc, Entidade: Integer;
  DataIni: String;
  SQLType: TSQLType;
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
    Exit;
  if ObtemCusto(CustoPreco) then
  begin
    SQLType   := stUpd;
    //
    Controle  := QrGraGruValControle.Value;
    GraGruX   := QrGraGruValGraGruX.Value;
    GraCusPrc := QrGraGruValGraCusPrc.Value;
    Entidade  := QrGraGruValEntidade.Value;
    DataIni   := Geral.FDT(QrGraGruValDataIni.Value, 1);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
    'GraGruX', 'GraCusPrc',
    'CustoPreco', 'Entidade', 'DataIni'], [
    'Controle'], [
    GraGruX, GraCusPrc,
    CustoPreco, Entidade, DataIni], [
    Controle], True) then
    begin
      ReopenGraGruVal(Controle);
    end;
  end;
end;

procedure TFmGraGXVend.BtConfirmaClick(Sender: TObject);
const
  sProcName = 'TFmGraGXVend.BtConfirmaClick()';
var
  GraGruX, Nivel1, UnidMed, GraGruY, Marca: Integer;
  //MARPAT,
  Modelo, Nome, Referencia, NCM, EAN13: String;
  SQLType: TSQLType;
  Aplicacao, ItemUnid, Ativo: Integer;
  ItemValr, MaxPercDesco: Double;
  //
  function InsUpdG1PrAp(SQLType: TSQLType; GraGruX: Integer): Boolean;
  begin
    ItemValr       := EdItemValr.ValueVariant;
    ItemUnid       := EdItemUnid.ValueVariant;
    MaxPercDesco   := EdMaxPercDesco.ValueVariant;
    Marca          := EdMarca.ValueVariant;
    Modelo         := EdModelo.ValueVariant;
    //MARPAT         := EdMARPAT.ValueVariant;
    //
(*  ini 2021-07-06
    erro!
    case RGAplicacao.ItemIndex of
      0: GraGruY := CO_GraGruY_4096_GXPatApo;
      2: GraGruY := CO_GraGruY_5120_GXPatUso;
      3: GraGruY := CO_GraGruY_6144_GXPrdCns;
      else
      begin
        Geral.MB_ERRO('ERRO ao definir o GrGruY! ' + sProcName);
      end;
    end;
*)
    // fim 2021-07-06
    GraGruY := CO_GraGruY_8192_GXPrdVen;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragxvend', False, [
    'Aplicacao', 'ItemValr', 'ItemUnid',
    'Marca', 'Modelo', //'MARPAT',
    'MaxPercDesco', 'Ativo'
    ], ['GraGruX'
    ], [Aplicacao, ItemValr, ItemUnid,
    Marca, Modelo, //MARPAT,
    MaxPercDesco, Ativo
    ], [GraGruX], True);
    //
    if Result then
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragrux', False, [
      'GraGruY', 'Ativo', 'EAN13'], [
      'Controle'], [
      GraGruY, Ativo, EAN13], [
      GraGruX], True);
    end;
  end;
begin
  Nome       := EdNome.ValueVariant;
  Referencia := EdReferencia.ValueVariant;
  UnidMed    := EdItemUnid.ValueVariant;
  NCM        := EdNCM.ValueVariant;
  EAN13      := Geral.SoNumero_TT(StringReplace(EdEAN13.ValueVariant, #$D#$A, '', [rfReplaceAll]));
  Aplicacao  := RGAplicacao.ItemIndex;
        //

  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(Referencia) = 0, EdReferencia, 'Defina uma refer�ncia!') then Exit;
  if MyObjects.FIC(UnidMed = 0, EdItemUnid, 'Defina a unidade de medida!') then Exit;
  if MyObjects.FIC(not Aplicacao in ([1,2,3]), RGAplicacao,
    'Defina a aplica��o!') then Exit;
  //
  case Dmod.QrOpcoesTRenFormaCobrLoca.Value of
    0,1: // Modo simplificado - AAPA
    begin

    end;
    2: // Modo completo - T i s o l i n
    begin
      if MyObjects.FIC(Aplicacao < 1, RGAplicacao,
        'Defina a aplica��o!') then Exit;
      //
(*  ini 2021-07-06
    erro!
    case RGAplicacao.ItemIndex of
      0: GraGruY := CO_GraGruY_4096_GXPatApo;
      2: GraGruY := CO_GraGruY_5120_GXPatUso;
      3: GraGruY := CO_GraGruY_6144_GXPrdCns;
      else
      begin
        Geral.MB_ERRO('ERRO ao definir o GrGruY! ' + sProcName);
      end;
    end;
*)
    GraGruY := CO_GraGruY_8192_GXPrdVen;
    // fim 2021-07-06
    end;
  end;
  //
  GraGruX := EdGraGruX.ValueVariant;
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT GraGru1 ',
  'FROM gragrux ',
  'WHERE controle=' + Geral.FF0(GraGruX),
  '']);
  Nivel1 := Dmod.QrAux.FieldByName('GraGru1').AsInteger;
  Ativo  := Geral.BoolToInt(CkAtivo.Checked);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False,
    ['Nome', 'Referencia', 'UnidMed', 'NCM'], ['Nivel1'],
    [Nome, Referencia, UnidMed, NCM], [Nivel1], True) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT GraGruX ',
    'FROM ' + FTabNome,
    'WHERE GraGruX=' + Geral.FF0(GraGruX),
    '']);
    if GraGruX = Dmod.QrAux.FieldByName('GraGruX').AsInteger then
      SQLType := stUpd
    else
      SQLType := stIns;
    if InsUpdG1PrAp(SQLType, GraGruX) then
    begin
      LocCod(GraGruX,GraGruX);
      ImgTipo.SQLType := stlok;
      PnDados.Visible := True;
      PnEdita.Visible := False;
      GOTOy.BotoesSb(ImgTipo.SQLType);
    end;
  end;
end;

procedure TFmGraGXVend.BtDesisteClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(GraGruX, Dmod.MyDB, FTabNome, 'GraGruX');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraGXVend.BtIncluiClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
    Exit;
  MostraFormPrdGruNew(stIns, 0, 0, 0, 0, 0);
end;

procedure TFmGraGXVend.BtPsqEAN13Click(Sender: TObject);
var
  Ean13: String;
begin
  if MyObjects.GetCodigoDeBarras(TFmGetCodigoBarras, FmGetCodigoBarras,
  'C�digo da Barras', 'C�digo da Barras', Ean13) then
  //if InputQuery('EAN13', 'C�digo de barras', Ean13) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
    'SELECT ggx.Controle, ggx.EAN13 ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
    'WHERE ggx.EAN13="' + EAN13 + '"' ,
    'AND NOT (ggo.GraGruX IS NULL)',
    //'AND ggo.Aplicacao <> 0 ',
    //
    //'AND ggx.Ativo=1 ',
    '']);
    //
    //Geral.MB_Teste(QrPesq1.SQL.Text);
    //
    QrPesq1.Open;
    if QrPesq1.RecordCount > 0 then
      LocCod(QrPesq1Controle.Value, QrPesq1Controle.Value);
  end;
end;

procedure TFmGraGXVend.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnCompras.Align := alClient;
  RGAplicacao.Items.Clear;
  DBRGAplicacao.Items.Clear;
  DBRGAplicacao.Values.Clear;
  case Dmod.QrOpcoesTRenFormaCobrLoca.Value of
    0,1,2:
    // Modo simplificado - AAPA
    // Modo completo - T i s o l i n
    begin
      RGAplicacao.Items.Add('Inativo');
      RGAplicacao.Items.Add('Venda');
      //
      DBRGAplicacao.Items.Add('Inativo');
      DBRGAplicacao.Items.Add('Venda');
      //
      DBRGAplicacao.Values.Add('0');
      DBRGAplicacao.Values.Add('1');
      //
    end;
  end;
  FiltraItensAExibir(gbsVends);
  //
  CriaOForm;
  //
  UMyMod.AbreQuery(QrMarcas, Dmod.MyDB);
  UMyMod.AbreQuery(QrAgrupador, Dmod.MyDB);
  UMyMod.AbreQuery(QrUnidMed, Dmod.MyDB);
end;

procedure TFmGraGXVend.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraGXVendGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmGraGXVend.SbDescoMinClick(Sender: TObject);
const
  DefaultAtu    = 0.00;
  DefaultNew    = 0.00;
  CasasAtu      = 2;
  CasasNew      = 2;
  LeftZerosAtu  = 0;
  LeftZerosNew  = 0;
  ValMinAtu     = '0';
  ValMaxAtu     = '100';
  ValMinNew     = '0';
  ValMaxNew     = '100';
  Obrigatorio   = False;
  FormCaption   = 'Desconto Padr�o';
  ValCaptionAtu = 'Desc.Padr�o atual:';
  ValCaptionNew = 'Desc.Padr�o novo:';
  WidthVal      = 132;
var
  ResultadoAtu, ResultadoNew: Variant;
  DescoAtu, DescoNew: Double;
  sDescoAtu, sDescoNew: String;
  Itens: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if MyObjects.ChangeValorDmk(TFmChangeValDmk, FmChangeValDmk,
  TAllFormat.dmktfDouble, DefaultAtu, DefaultNew, CasasAtu, CasasNew,
  LeftZerosAtu, LeftZerosNew, ValMinAtu, ValMaxAtu, ValMinNew, ValMaxNew,
  Obrigatorio, FormCaption, ValCaptionAtu, ValCaptionNew, WidthVal,
  ResultadoAtu, ResultadoNew) then
  begin
    DescoAtu := ResultadoAtu;
    DescoNew := ResultadoNew;
    sDescoAtu    := Geral.FFT(DescoAtu, 2, siPositivo);
    sDescoNew    := Geral.FFT(DescoNew, 2, siPositivo);
    if Geral.MB_Pergunta('Deseja alterar todos cadastros que tem desconto de ' +
    sDescoAtu + '% para ' + sDescoNew + '%?') = ID_Yes then
    begin
      sDescoAtu    := Geral.FFT_Dot(DescoAtu, 2, siPositivo);
      sDescoNew    := Geral.FFT_Dot(DescoNew, 2, siPositivo);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, Dmod.MyDB, [
      //Dmod.MyDB.Execute(
      'UPDATE gragxvend ',
      'SET MaxPercDesco=' + sDescoNew,
      'WHERE MaxPercDesco=' + sDescoAtu,
      '']);
      Itens := QrExec.RowsAffected;
      //
      Geral.MB_Info(Geral.FF0(Itens) + ' cadastros foram alterados!');
    end;
  end;
end;

procedure TFmGraGXVend.SbImprimeClick(Sender: TObject);
var
  PrdGrupTip, GraGru1: Integer;
begin
  PrdGrupTip := QrGraGXVendPrdGrupTip.Value;
  GraGru1    := QrGraGXVendNivel1.Value;
  //
  Grade_Jan.MostraFormGraImpLista(PrdGrupTip, GraGru1);
end;

procedure TFmGraGXVend.SBItemUnidClick(Sender: TObject);
var
  UnidMed: Integer;
begin
  VAR_CADASTRO := 0;
  UnidMed      := EdItemUnid.ValueVariant;
  //
  FmPrincipal.MostraUnidMed(EdItemUnid.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdItemUnid, CBItemUnid, QrUnidMed, VAR_CADASTRO, 'Codigo');
    EdItemUnid.SetFocus;
  end;
end;

procedure TFmGraGXVend.SbNomeClick(Sender: TObject);
begin
  PesquisaNomeMateriais();
end;

procedure TFmGraGXVend.SbNovoClick(Sender: TObject);
var
  Referencia: String;
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrGraGXVendGraGruX.Value, LaRegistro.Caption);
  //
  Referencia := '';
  if InputQuery('Pesquisa por Refer�ncia', 'Informe a refer�ncia', Referencia) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocod, Dmod.MyDB, [
    'SELECT ggx.Controle ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE gg1.Referencia="' + Referencia + '"',
    '']);
    LocCod(QrLocod.FieldByName('Controle').AsInteger, QrLocod.FieldByName('Controle').AsInteger);
  end;
end;

procedure TFmGraGXVend.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraGXVend.QrGraGXVendAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraGXVend.QrGraGXVendAfterScroll(DataSet: TDataSet);
begin
  ReopenEstq();
  ReopenGraGruVal(0);
  ReopenStqInn();
end;

procedure TFmGraGXVend.FiltraItensAExibir(Material: TGraToolRent);
begin
  FGraToolRent := Material;
  case FGraToolRent of
    gbsVends: FTabNome := 'gragxvend';
    else FTabNome := 'gragx????';
  end;
  //
  case FGraToolRent of
    gbsVends  : FFrmNome := 'Material';
    else FFrmNome := '? ? ? ? ?';
  end;
  //
  DefParams;
  Va(vpLast);
end;

procedure TFmGraGXVend.FormActivate(Sender: TObject);
begin
  if TFmGraGXVend(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end else
  begin
    // Pode ter mudado para DefParams de outo form em Aba!
    DefParams();
  end;
  //
end;

procedure TFmGraGXVend.SbQueryClick(Sender: TObject);
begin
  PesquisaNomeMateriais();
end;

procedure TFmGraGXVend.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGXVend.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
end;

procedure TFmGraGXVend.MostraFormPrdGruNew(SQLTipo: TSQLType; Nivel5, Nivel4,
  Nivel3, Nivel2, Nivel1: Integer);
const
  ForcaCadastrGGX_SemCorTam = True;
var
  TipCodVend: Integer;
  InseridoAgora: Boolean;
begin
  InseridoAgora := False;
  VAR_CADASTRO  := 0;
  VAR_CADASTRO2 := 0;
  //
  Dmod.ReopenOpcoesTRen;
  //
  TipCodVend := Dmod.QrOpcoesTRenTipCodVend.Value;
  //
  DmProd.InsereItemPrdGruNew(cggnFmGraGXPatr, TipCodVend, 0, '', '',
  ForcaCadastrGGX_SemCorTam, Nivel5, Nivel4, Nivel3, Nivel2, Nivel1, True, 0, '');
  //
(*
  if VAR_CADASTRO2 <> 0 then
  begin
    LocCod(VAR_CADASTRO2, VAR_CADASTRO2);
    //
    if (QrGraGXVendCPL_EXISTE.Value = 0) and (SQLTipo = stIns) then
    begin
      Alteraprodutoatual1Click(PMAltera);
    end;
  end;
*)
  if VAR_CADASTRO2 <> 0 then // VAR_CADASTRO2 = GraGruX.Controle
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
    'SELECT GraGruX Controle ',
    'FROM gragxvend ',
    'WHERE GraGruX=' + Geral.FF0(VAR_CADASTRO2),
    '']);
    //QrPesq1. Open;
    if QrPesq1.RecordCount = 0 then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragxvend', False,
      ['Aplicacao'], ['GraGruX'], [0], [VAR_CADASTRO2], True) then
          InseridoAgora := True;
    end;
    LocCod(VAR_CADASTRO2, VAR_CADASTRO2);
    //
    if ( (QrGraGXVendCPL_EXISTE.Value = 0) or (InseridoAgora))
    and (SQLTipo = stIns) then
    begin
      Alteraprodutoatual1Click(PMAltera);
    end;
  end;
end;

procedure TFmGraGXVend.IncluiProduto;
var
  TipCodVend: Integer;
begin
  VAR_CADASTRO  := 0;
  VAR_CADASTRO2 := 0;
  //
  Dmod.ReopenOpcoesTRen;
  //
  TipCodVend := Dmod.QrOpcoesTRenTipCodVend.Value;
  //
  if DBCheck.CriaFm(TFmGraGru1, FmGraGru1, afmoNegarComAviso) then
  begin
    FmGraGru1.ImgTipo.SQLType := stIns;
    FmGraGru1.FQrGraGru1 := nil;
    FmGraGru1.FQrGraGruX := QrGraGXVend;
    //
    if TipCodVend <> 0 then
    begin
      FmGraGru1.EdPrdGrupTip.ValueVariant := TipCodVend;
      FmGraGru1.CBPrdGrupTip.KeyValue     := TipCodVend;
      FmGraGru1.EdPrdGrupTip.Enabled      := False;
      FmGraGru1.CBPrdGrupTip.Enabled      := False;
    end else
    begin
      FmGraGru1.EdPrdGrupTip.Enabled := True;
      FmGraGru1.CBPrdGrupTip.Enabled := True;
    end;
    //
    FmGraGru1.ShowModal;
    FmGraGru1.Destroy;
  end;
  if VAR_CADASTRO2 <> 0 then
  begin
    LocCod(VAR_CADASTRO2, VAR_CADASTRO2);
    if (VAR_CADASTRO2 = QrGraGXVendGraGruX.Value) and
    (QrGraGXVendCPL_EXISTE.Value = 0) then
    begin
      BtAlteraClick(BtAltera);
    end;
  end;
end;

procedure TFmGraGXVend.AlteraProduto;
var
  TipCodVend: Integer;
begin
  Dmod.ReopenOpcoesTRen;
  TipCodVend := Dmod.QrOpcoesTRenTipCodVend.Value;
  //
  if DBCheck.CriaFm(TFmGraGru1, FmGraGru1, afmoNegarComAviso) then
  begin
    FmGraGru1.ImgTipo.SQLType := stUpd;
    FmGraGru1.FQrGraGru1 := QrGraGXVend;
    //
    if TipCodVend <> 0 then
    begin
      FmGraGru1.EdPrdGrupTip.ValueVariant := TipCodVend;
      FmGraGru1.CBPrdGrupTip.KeyValue     := TipCodVend;
      FmGraGru1.EdPrdGrupTip.Enabled      := False;
      FmGraGru1.CBPrdGrupTip.Enabled      := False;
    end else
    begin
      FmGraGru1.EdPrdGrupTip.Enabled := True;
      FmGraGru1.CBPrdGrupTip.Enabled := True;
    end;
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdPrdGrupTip, FmGraGru1.CBPrdGrupTip,
      FmGraGru1.QrPrdGrupTip, QrGraGXVendPrdGrupTip.Value);
    //
    FmGraGru1.HabilitaComponentes();
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel5, FmGraGru1.CBNivel5,
      FmGraGru1.QrGraGru5, QrGraGXVendNivel5.Value, 'Nivel5', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel4, FmGraGru1.CBNivel4,
      FmGraGru1.QrGraGru4, QrGraGXVendNivel4.Value, 'Nivel4', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel3, FmGraGru1.CBNivel3,
      FmGraGru1.QrGraGru3, QrGraGXVendNivel3.Value, 'Nivel3', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel2, FmGraGru1.CBNivel2,
      FmGraGru1.QrGraGru2, QrGraGXVendNivel2.Value, 'Nivel2', 'CodUsu');
    //
    FmGraGru1.EdNivel1.ValueVariant     := QrGraGXVendNivel1.Value;
    FmGraGru1.EdCodUsu_Ant.ValueVariant := QrGraGXVendCodUsu.Value;
    FmGraGru1.EdCodUsu_New.ValueVariant := QrGraGXVendCodUsu.Value;
    FmGraGru1.EdNome_Ant.ValueVariant   := QrGraGXVendNO_GG1.Value;
    FmGraGru1.EdNome_New.ValueVariant   := QrGraGXVendNO_GG1.Value;
    //
    FmGraGru1.ShowModal;
    FmGraGru1.Destroy;
  end;
end;

procedure TFmGraGXVend.QrGraGXVendBeforeClose(DataSet: TDataSet);
begin
  QrEstq.Close;
  QrGraGruVal.Close;
  QrStqInn.Close;
end;

procedure TFmGraGXVend.QrGraGXVendBeforeOpen(DataSet: TDataSet);
begin
  QrGraGXVendGraGruX.DisplayFormat := FFormatFloat;
end;

end.

