unit LocCRedef;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, UnProjGroup_Vars,
  UnAppEnums, Vcl.Menus;

type
  TFmLocCRedef = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrLocCMovAll: TMySQLQuery;
    QrLocCMovAllCodigo: TIntegerField;
    QrLocCMovAllGTRTab: TIntegerField;
    QrLocCMovAllCtrID: TIntegerField;
    QrLocCMovAllItem: TIntegerField;
    QrLocCMovAllControle: TIntegerField;
    QrLocCMovAllSEQUENCIA: TIntegerField;
    QrLocCMovAllDataHora: TDateTimeField;
    QrLocCMovAllUSUARIO: TWideStringField;
    QrLocCMovAllGraGruX: TIntegerField;
    QrLocCMovAllTipoES: TWideStringField;
    QrLocCMovAllQuantidade: TIntegerField;
    QrLocCMovAllTipoMotiv: TSmallintField;
    QrLocCMovAllCOBRANCALOCACAO: TWideStringField;
    QrLocCMovAllVALORLOCACAO: TFloatField;
    QrLocCMovAllLIMPO: TWideStringField;
    QrLocCMovAllSUJO: TWideStringField;
    QrLocCMovAllQUEBRADO: TWideStringField;
    QrLocCMovAllTESTADODEVOLUCAO: TWideStringField;
    QrLocCMovAllGGXEntrada: TLargeintField;
    QrLocCMovAllMotivoTroca: TIntegerField;
    QrLocCMovAllObservacaoTroca: TWideStringField;
    QrLocCMovAllQuantidadeLocada: TIntegerField;
    QrLocCMovAllQuantidadeJaDevolvida: TIntegerField;
    QrLocCMovAllLk: TIntegerField;
    QrLocCMovAllDataCad: TDateField;
    QrLocCMovAllDataAlt: TDateField;
    QrLocCMovAllUserCad: TIntegerField;
    QrLocCMovAllUserAlt: TIntegerField;
    QrLocCMovAllAlterWeb: TSmallintField;
    QrLocCMovAllAWServerID: TIntegerField;
    QrLocCMovAllAWStatSinc: TSmallintField;
    QrLocCMovAllAtivo: TSmallintField;
    QrLocCMovAllNO_ManejoLca: TWideStringField;
    QrLocCMovAllNO_TipoMotiv: TWideStringField;
    QrLocCMovAllSubstitutoLca: TIntegerField;
    QrLocCMovAllNO_MotivoTroca: TWideStringField;
    DsLocCMovAll: TDataSource;
    BtAcoes: TBitBtn;
    PMAcoes: TPopupMenu;
    Alteravalortotaldalocao1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtAcoesClick(Sender: TObject);
    procedure Alteravalortotaldalocao1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FGtrTab: TGraToolRent;
    FCodigo, FManejoLca: Integer;
    FTipoAluguel: String;
    //
    procedure ReopenLocCMovAll();
  end;

  var
  FmLocCRedef: TFmLocCRedef;

implementation

uses UnMyObjects, Module, DmkDAC_PF, LocCConCab, UnDmkProcFunc, UMySQLModule,
  GetValor, UnAppPF;

{$R *.DFM}

procedure TFmLocCRedef.Alteravalortotaldalocao1Click(Sender: TObject);
const
  FormCaption  = 'XXX-XXXXX-001 :: Valor Total do Item';
  ValCaption   = 'Valor total loca��o item:';
  WidthCaption = Length(ValCaption) * 7;
var
  ValVar: Variant;
  VALORLOCACAO: Double;
  Controle, CtrID, Item, GTRTab: Integer;
begin
  VALORLOCACAO := QrLocCMovAllVALORLOCACAO.Value;
  Controle := QrLocCMovAllControle.Value;
  CtrID := QrLocCMovAllCtrID.Value;
  Item := QrLocCMovAllItem.Value;
  GTRTab := QrLocCMovAllGTRTab.Value;
  //
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, VALORLOCACAO,
  2, 0, '0,00', '', True, FormCaption, ValCaption, WidthCaption, ValVar) then
  begin
    Screen.Cursor := crHourGlass;
    try
      VALORLOCACAO := ValVar;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccmovall', False, [
      'VALORLOCACAO'], ['Controle'], [
      VALORLOCACAO], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitslca', False, [
        'ValorLocAtualizado', 'ValorBruto'], ['Item'], [
        0.00, 0.00], [Item], True) then
        begin
          AppPF.AtualizaValDevolParci(FCodigo, GTRTab, CtrID, Item);
          //
          AppPF.AtualizaValorAluguel_LocCItsLca(FCodigo, FTipoAluguel);
          AppPF.AtualizaTotaisLocCConCab_Locacao(FCodigo);
          ReopenLocCMovAll();
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmLocCRedef.BtAcoesClick(Sender: TObject);
begin
  MyObjects.MostraPopupdeBotao(PMAcoes, BtAcoes);
end;

procedure TFmLocCRedef.BtOKClick(Sender: TObject);
begin
  if VAR_FmLocCConCab <> nil then
    TFmLocCConCab(VAR_FmLocCConCab).RecalculaTudo(True, True, True);
end;

procedure TFmLocCRedef.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCRedef.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCRedef.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmLocCRedef.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCRedef.ReopenLocCMovAll();
var
  SQL_TipoMotivLocMov: String;
begin
  SQL_TipoMotivLocMov := dmkPF.SQL_ELT('lma.TipoMotiv', 'NO_TipoMotiv',
    TPosVirgula.pvNo, True, sTipoMotivLocMov);
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCMovAll, Dmod.MyDB, [
  'SELECT tmc.Nome NO_MotivoTroca, lma.*, ',
  SQL_TipoMotivLocMov,
  //SQL_ManejoLca,
  'FROM loccmovall lma ',
  'LEFT JOIN loctromotcad tmc ON tmc.Codigo=lma.MotivoTroca ',
  'WHERE lma.Codigo=' + Geral.FF0(FCodigo),
  'AND lma.GTRTab=' + Geral.FF0(Integer(FGtrTab)),
  //'AND lma.CtrID=' + Geral.FF0(FCtrID),
  //'AND lma.Item=' + Geral.FF0(FItem),
  '']);
end;

end.
