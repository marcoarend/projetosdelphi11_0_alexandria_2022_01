unit GraGXVePsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB;

type
  TFmGraGXVePsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel3: TPanel;
    QrStqCenCadPsq: TMySQLQuery;
    QrStqCenCadPsqCodigo: TIntegerField;
    QrStqCenCadPsqNome: TWideStringField;
    DsStqCenCadPsq: TDataSource;
    QrGraCusPrc: TMySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcCodUsu: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    QrGraCusPrcCusPrc: TWideStringField;
    QrGraCusPrcNomeTC: TWideStringField;
    QrGraCusPrcNOMEMOEDA: TWideStringField;
    QrGraCusPrcSIGLAMOEDA: TWideStringField;
    DsGraCusPrc: TDataSource;
    Label1: TLabel;
    EdPesq: TdmkEdit;
    dmkLabel3: TdmkLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    QrPesq: TMySQLQuery;
    DsPesq: TDataSource;
    QrPesqGraGruX: TIntegerField;
    QrPesqREFERENCIA: TWideStringField;
    QrPesqEAN13: TWideStringField;
    QrPesqItemValr: TFloatField;
    QrPesqNO_FABR: TWideStringField;
    QrPesqNO_MARCA: TWideStringField;
    QrPesqNome: TWideStringField;
    QrEstq: TMySQLQuery;
    QrEstqGraGruX: TIntegerField;
    QrEstqQtde: TFloatField;
    QrPesqEstq: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdStqCenCadRedefinido(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure dmkDBGridZTO1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisa();
    procedure Seleciona();
  public
    { Public declarations }
    FGraGruX: Integer;
  end;

  var
  FmGraGXVePsq: TFmGraGXVePsq;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModProd;

{$R *.DFM}

procedure TFmGraGXVePsq.BtOKClick(Sender: TObject);
begin
  Seleciona();
end;

procedure TFmGraGXVePsq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGXVePsq.dmkDBGridZTO1DblClick(Sender: TObject);
begin
  Seleciona();
end;

procedure TFmGraGXVePsq.EdPesqChange(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmGraGXVePsq.EdStqCenCadRedefinido(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstq, Dmod.MyDB, [
  'SELECT smia.GraGruX, ',
  'SUM(smia.Qtde * smia.Baixa) Qtde  ',
  'FROM stqmovitsa smia ',
  'WHERE smia.Ativo=1  ',
  'AND smia.StqCenCad=' + Geral.FF0(EdStqCenCad.ValueVariant),
  'GROUP BY smia.GraGruX',
  '']);
  Pesquisa();
end;

procedure TFmGraGXVePsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGXVePsq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FGraGruX := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrStqCenCadPsq, Dmod.MyDB);
  DmProd.ReopenGraCusPrcU(QrGraCusPrc, 0);
  //
  EdStqCenCad.ValueVariant := 1;
  CBStqCenCad.KeyValue     := 1;
end;

procedure TFmGraGXVePsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGXVePsq.Pesquisa();
var
  Texto, Doc, SQL_Text, Masc: String;
begin
  QrPesq.Close;
  //
  Texto := EdPesq.ValueVariant;
  //
  if Texto <> '' then
    Texto := StringReplace(Texto, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
  //
  Texto := '%' + Texto + '%';
  if (Length(Texto) >= 2) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    'SELECT lpc.GraGruX, gg1.REFERENCIA, ggx.EAN13, ',
    'lpc.ItemValr, gfc.Nome NO_FABR, gfm.Nome NO_MARCA, gg1.Nome',
    'FROM gragxvend lpc ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN grafabmar gfm ON lpc.Marca=gfm.Controle',
    'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo',
    'WHERE gg1.Nome LIKE "' + Texto + '" ',
    'OR gg1.Referencia LIKE "' + Texto + '" ',
    'OR gg1.Patrimonio LIKE "' + Texto + '" ',
    'OR ggx.EAN13 LIKE "' + Texto + '" ',
    '']);
  end;
end;

procedure TFmGraGXVePsq.Seleciona;
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    FGraGruX := QrPesqGraGruX.Value;
    //
    Close;
  end;
end;

end.
