object FmLocCMovGer: TFmLocCMovGer
  Left = 339
  Top = 185
  Caption = 
    'LOC-PATRI-114 :: Gerenciamento de Movimenta'#231#245'es de Equipamentos ' +
    'Locados'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 751
        Height = 32
        Caption = 'Gerenciamento de Movimenta'#231#245'es de Equipamentos Locados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 751
        Height = 32
        Caption = 'Gerenciamento de Movimenta'#231#245'es de Equipamentos Locados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 751
        Height = 32
        Caption = 'Gerenciamento de Movimenta'#231#245'es de Equipamentos Locados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 217
          Width = 1004
          Height = 5
          Cursor = crVSplit
          Align = alTop
          ExplicitTop = 225
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 2
          Top = 222
          Width = 1004
          Height = 243
          Align = alClient
          DataSource = DsLocCMovAll
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TipoES'
              Title.Caption = 'E/S'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LIMPO'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUJO'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUEBRADO'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TESTADODEVOLUCAO'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COBRANCALOCACAO'
              Width = 116
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Quantidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALORLOCACAO'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GGXEntrada'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MotivoTroca'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ObservacaoTroca'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QuantidadeJaDevolvida'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QuantidadeLocada'
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 110
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label3: TLabel
            Left = 952
            Top = 3
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            Enabled = False
          end
          object Label1: TLabel
            Left = 12
            Top = 4
            Width = 62
            Height = 13
            Caption = 'Atendimento:'
          end
          object Label2: TLabel
            Left = 98
            Top = 4
            Width = 48
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Reduzido:'
          end
          object Label4: TLabel
            Left = 156
            Top = 4
            Width = 52
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Patrim'#244'nio:'
          end
          object Label19: TLabel
            Left = 244
            Top = 4
            Width = 55
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Refer'#234'ncia:'
          end
          object Label7: TLabel
            Left = 369
            Top = 4
            Width = 51
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o:'
          end
          object Label17: TLabel
            Left = 12
            Top = 60
            Width = 35
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object EdEmpresa: TdmkEdit
            Left = 952
            Top = 19
            Width = 45
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-11'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -11
            ValWarn = False
            OnRedefinido = EdEmpresaRedefinido
          end
          object EdLocCConCab: TdmkEdit
            Left = 12
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdLocCConCabRedefinido
          end
          object EdGraGruX: TdmkEditCB
            Left = 98
            Top = 19
            Width = 56
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdGraGruXChange
            OnRedefinido = EdGraGruXRedefinido
            DBLookupComboBox = CBGraGruX
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdPatrimonio: TdmkEdit
            Left = 156
            Top = 19
            Width = 85
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdPatrimonioChange
            OnKeyDown = EdPatrimonioKeyDown
          end
          object EdReferencia: TdmkEdit
            Left = 244
            Top = 19
            Width = 122
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdReferenciaChange
            OnKeyDown = EdReferenciaKeyDown
          end
          object CBGraGruX: TdmkDBLookupComboBox
            Left = 369
            Top = 19
            Width = 580
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Controle'
            ListField = 'NO_GG1'
            ListSource = DsGraGXPatr
            TabOrder = 4
            dmkEditCB = EdGraGruX
            UpdType = utYes
            LocF7SQLText.Strings = (
              'SELECT lpc.GraGruX _CODIGO, gg1.REFERENCIA, gg1.Nome _NOME'
              'FROM gragxpatr lpc'
              'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX'
              'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
              'WHERE gg1.Nome LIKE "%$#%"'
              'OR gg1.Referencia LIKE "%$#%"'
              'OR gg1.Patrimonio LIKE "%$#%"'
              '')
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCliente: TdmkEditCB
            Left = 12
            Top = 76
            Width = 52
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdClienteRedefinido
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 68
            Top = 76
            Width = 437
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsClientes
            ParentFont = False
            TabOrder = 7
            dmkEditCB = EdCliente
            QryCampo = 'Cliente'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object GroupBox2: TGroupBox
            Left = 516
            Top = 44
            Width = 233
            Height = 65
            Caption = ' Per'#237'odo de retirada: '
            TabOrder = 8
            object Panel6: TPanel
              Left = 2
              Top = 15
              Width = 229
              Height = 48
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object CkSaiDtHrIni: TCheckBox
                Left = 8
                Top = 4
                Width = 97
                Height = 17
                Caption = 'Inicial:'
                TabOrder = 0
                OnClick = CkSaiDtHrIniClick
              end
              object CkSaiDtHrFim: TCheckBox
                Left = 120
                Top = 4
                Width = 97
                Height = 17
                Caption = 'Final:'
                TabOrder = 1
                OnClick = CkSaiDtHrFimClick
              end
              object TPSaiDtHrIni: TdmkEditDateTimePicker
                Left = 8
                Top = 24
                Width = 104
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 41131.724786689820000000
                Time = 41131.724786689820000000
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                OnClick = TPSaiDtHrIniClick
                OnChange = TPSaiDtHrIniChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                QryCampo = 'DtHrSai'
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPSaiDtHrFim: TdmkEditDateTimePicker
                Left = 120
                Top = 24
                Width = 104
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 41131.724786689820000000
                Time = 41131.724786689820000000
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
                OnClick = TPSaiDtHrFimClick
                OnChange = TPSaiDtHrFimChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                QryCampo = 'DtHrDevolver'
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
            end
          end
          object BrReabrePsq: TBitBtn
            Tag = 18
            Left = 772
            Top = 64
            Width = 120
            Height = 40
            Caption = '&Pesquisa'
            NumGlyphs = 2
            TabOrder = 9
            OnClick = BrReabrePsqClick
          end
        end
        object dmkDBGridZTO2: TdmkDBGridZTO
          Left = 2
          Top = 125
          Width = 1004
          Height = 92
          Align = alTop
          DataSource = DsLocCItsLca
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 2
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'REFERENCIA'
              Title.Caption = 'Refer'#234'ncia'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_GGX'
              Title.Caption = 'Nome'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeProduto'
              Title.Caption = 'Qtde'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorDia'
              Title.Caption = 'Valor Dia'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorSem'
              Title.Caption = 'Valor Sem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorQui'
              Title.Caption = 'Valor Qui'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMes'
              Title.Caption = 'Valor M'#234's'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrLocado'
              Title.Caption = 'Data loca'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SaiDtHr_TXT'
              Title.Caption = 'Sa'#237'da'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Item'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CtrID'
              Title.Caption = 'ID mov.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Atendimento'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PATRIMONIO'
              Title.Caption = 'Patrim'#244'nio'
              Width = 120
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 328
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Inclui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BitBtn1: TBitBtn
        Tag = 11
        Left = 452
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn1Click
      end
      object BitBtn2: TBitBtn
        Tag = 12
        Left = 576
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BitBtn2Click
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtImprimeClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 856
    Top = 65531
  end
  object QrLocCMovAll: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lma.* '
      'FROM loccmovall lma'
      'WHERE lma.Codigo=27352'
      'AND lma.GTRTab=1 '
      'AND lma.CtrID=34556 '
      'AND lma.Item=67331')
    Left = 268
    Top = 392
    object QrLocCMovAllCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocCMovAllGTRTab: TIntegerField
      FieldName = 'GTRTab'
      Required = True
    end
    object QrLocCMovAllCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrLocCMovAllItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrLocCMovAllControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLocCMovAllSEQUENCIA: TIntegerField
      FieldName = 'SEQUENCIA'
      Required = True
    end
    object QrLocCMovAllDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrLocCMovAllUSUARIO: TWideStringField
      FieldName = 'USUARIO'
      Size = 10
    end
    object QrLocCMovAllGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrLocCMovAllTipoES: TWideStringField
      FieldName = 'TipoES'
      Size = 1
    end
    object QrLocCMovAllQuantidade: TIntegerField
      FieldName = 'Quantidade'
    end
    object QrLocCMovAllTipoMotiv: TSmallintField
      FieldName = 'TipoMotiv'
    end
    object QrLocCMovAllCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 50
    end
    object QrLocCMovAllVALORLOCACAO: TFloatField
      FieldName = 'VALORLOCACAO'
      Required = True
    end
    object QrLocCMovAllLIMPO: TWideStringField
      FieldName = 'LIMPO'
      Size = 1
    end
    object QrLocCMovAllSUJO: TWideStringField
      FieldName = 'SUJO'
      Size = 1
    end
    object QrLocCMovAllQUEBRADO: TWideStringField
      FieldName = 'QUEBRADO'
      Size = 1
    end
    object QrLocCMovAllTESTADODEVOLUCAO: TWideStringField
      FieldName = 'TESTADODEVOLUCAO'
      Size = 1
    end
    object QrLocCMovAllGGXEntrada: TLargeintField
      FieldName = 'GGXEntrada'
    end
    object QrLocCMovAllMotivoTroca: TIntegerField
      FieldName = 'MotivoTroca'
      Required = True
    end
    object QrLocCMovAllObservacaoTroca: TWideStringField
      FieldName = 'ObservacaoTroca'
      Size = 512
    end
    object QrLocCMovAllQuantidadeLocada: TIntegerField
      FieldName = 'QuantidadeLocada'
    end
    object QrLocCMovAllQuantidadeJaDevolvida: TIntegerField
      FieldName = 'QuantidadeJaDevolvida'
    end
    object QrLocCMovAllLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrLocCMovAllDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocCMovAllDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocCMovAllUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrLocCMovAllUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrLocCMovAllAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLocCMovAllAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrLocCMovAllAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrLocCMovAllAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLocCMovAllNO_ManejoLca: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ManejoLca'
      Size = 60
      Calculated = True
    end
  end
  object DsLocCMovAll: TDataSource
    DataSet = QrLocCMovAll
    Left = 268
    Top = 440
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL, '
      'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1, '
      
        'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXIS' +
        'TE,  '
      'cpl.Situacao, cpl.AtualValr, cpl.ValorMes, '
      'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia, '
      'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie, '
      'cpl.Voltagem, cpl.Potencia, cpl.Capacid'
      '   '
      'FROM gragrux ggx  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle  '
      'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle  '
      'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo  '
      'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao'
      'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle'
      'WHERE  gg1.PrdGrupTip=1 AND (gg1.Nivel1=ggx.GraGru1) '
      'AND cpl.Aplicacao <> 0'
      ''
      'ORDER BY NO_GG1 '
      '')
    Left = 392
    Top = 308
    object QrGraGXPatrEstqSdo: TFloatField
      FieldName = 'EstqSdo'
    end
    object QrGraGXPatrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXPatrReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXPatrCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
    end
    object QrGraGXPatrNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXPatrCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
      Required = True
    end
    object QrGraGXPatrSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrGraGXPatrAtualValr: TFloatField
      FieldName = 'AtualValr'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorMes: TFloatField
      FieldName = 'ValorMes'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorQui: TFloatField
      FieldName = 'ValorQui'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorSem: TFloatField
      FieldName = 'ValorSem'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorDia: TFloatField
      FieldName = 'ValorDia'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrAgrupador: TIntegerField
      FieldName = 'Agrupador'
    end
    object QrGraGXPatrMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraGXPatrModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 60
    end
    object QrGraGXPatrSerie: TWideStringField
      FieldName = 'Serie'
      Size = 60
    end
    object QrGraGXPatrVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Size = 30
    end
    object QrGraGXPatrPotencia: TWideStringField
      FieldName = 'Potencia'
      Size = 30
    end
    object QrGraGXPatrCapacid: TWideStringField
      FieldName = 'Capacid'
      Size = 30
    end
    object QrGraGXPatrTXT_MES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_MES'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_QUI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_QUI'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_SEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_SEM'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_DIA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DIA'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrNO_SIT: TWideStringField
      FieldName = 'NO_SIT'
      Size = 30
    end
    object QrGraGXPatrNO_SITAPL: TIntegerField
      FieldName = 'NO_SITAPL'
    end
    object QrGraGXPatrValorFDS: TFloatField
      FieldName = 'ValorFDS'
    end
    object QrGraGXPatrDMenos: TIntegerField
      FieldName = 'DMenos'
    end
    object QrGraGXPatrPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 60
    end
    object QrGraGXPatrNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
  end
  object DsGraGXPatr: TDataSource
    DataSet = QrGraGXPatr
    Left = 392
    Top = 356
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  gg1.Referencia=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 560
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrPesq1Patrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 255
    end
    object QrPesq1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  cpl.GraGruX=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 624
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrPesq2Patrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 255
    end
  end
  object QrLocCItsLca: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLocCItsLcaAfterOpen
    BeforeClose = QrLocCItsLcaBeforeClose
    AfterScroll = QrLocCItsLcaAfterScroll
    SQL.Strings = (
      'SELECT lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN '
      'FROM loccitslca lpp '
      'LEFT JOIN loccconcab cab ON cab.Codigo=lpp.Codigo '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ')
    Left = 272
    Top = 248
    object QrLocCItsLcaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCItsLcaCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocCItsLcaItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocCItsLcaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocCItsLcaNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocCItsLcaREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
    object QrLocCItsLcaValorDia: TFloatField
      FieldName = 'ValorDia'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCItsLcaValorSem: TFloatField
      FieldName = 'ValorSem'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCItsLcaValorQui: TFloatField
      FieldName = 'ValorQui'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCItsLcaValorMes: TFloatField
      FieldName = 'ValorMes'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCItsLcaDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrLocCItsLcaDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrLocCItsLcaQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrLocCItsLcaValorProduto: TFloatField
      FieldName = 'ValorProduto'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCItsLcaQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrLocCItsLcaValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCItsLcaQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
    end
    object QrLocCItsLcaManejoLca: TSmallintField
      FieldName = 'ManejoLca'
    end
    object QrLocCItsLcaPATRIMONIO: TWideStringField
      FieldName = 'PATRIMONIO'
      Size = 25
    end
    object QrLocCItsLcaSaiDtHr: TDateTimeField
      FieldName = 'SaiDtHr'
    end
    object QrLocCItsLcaRetFunci: TIntegerField
      FieldName = 'RetFunci'
      Required = True
    end
    object QrLocCItsLcaRetExUsr: TIntegerField
      FieldName = 'RetExUsr'
      Required = True
    end
    object QrLocCItsLcaLibExUsr: TIntegerField
      FieldName = 'LibExUsr'
      Required = True
    end
    object QrLocCItsLcaLibFunci: TIntegerField
      FieldName = 'LibFunci'
      Required = True
    end
    object QrLocCItsLcaLibDtHr: TDateTimeField
      FieldName = 'LibDtHr'
      Required = True
    end
    object QrLocCItsLcaRELIB: TWideStringField
      FieldName = 'RELIB'
    end
    object QrLocCItsLcaHOLIB: TWideStringField
      FieldName = 'HOLIB'
      Size = 5
    end
    object QrLocCItsLcaValDevolParci: TFloatField
      FieldName = 'ValDevolParci'
      Required = True
    end
    object QrLocCItsLcaTroca: TSmallintField
      FieldName = 'Troca'
      Required = True
    end
    object QrLocCItsLcaCategoria: TWideStringField
      FieldName = 'Categoria'
      Required = True
      Size = 1
    end
    object QrLocCItsLcaCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 40
    end
    object QrLocCItsLcaCobrancaConsumo: TFloatField
      FieldName = 'CobrancaConsumo'
      Required = True
    end
    object QrLocCItsLcaCobrancaRealizadaVenda: TWideStringField
      FieldName = 'CobrancaRealizadaVenda'
      Size = 1
    end
    object QrLocCItsLcaCobranIniDtHr: TDateTimeField
      FieldName = 'CobranIniDtHr'
      Required = True
    end
    object QrLocCItsLcaValorFDS: TFloatField
      FieldName = 'ValorFDS'
      Required = True
    end
    object QrLocCItsLcaDMenos: TSmallintField
      FieldName = 'DMenos'
      Required = True
    end
    object QrLocCItsLcaValorLocAAdiantar: TFloatField
      FieldName = 'ValorLocAAdiantar'
      Required = True
    end
    object QrLocCItsLcaValorLocAtualizado: TFloatField
      FieldName = 'ValorLocAtualizado'
      Required = True
    end
    object QrLocCItsLcaCalcAdiLOCACAO: TWideStringField
      FieldName = 'CalcAdiLOCACAO'
      Size = 40
    end
    object QrLocCItsLcaDiasUteis: TIntegerField
      FieldName = 'DiasUteis'
      Required = True
    end
    object QrLocCItsLcaDiasFDS: TIntegerField
      FieldName = 'DiasFDS'
      Required = True
    end
    object QrLocCItsLcaPercDesco: TFloatField
      FieldName = 'PercDesco'
      Required = True
    end
    object QrLocCItsLcaValorBruto: TFloatField
      FieldName = 'ValorBruto'
      Required = True
    end
    object QrLocCItsLcaLOGIN: TWideStringField
      FieldName = 'LOGIN'
      Size = 30
    end
    object QrLocCItsLcaSaiDtHr_TXT: TWideStringField
      FieldName = 'SaiDtHr_TXT'
    end
  end
  object DsLocCItsLca: TDataSource
    DataSet = QrLocCItsLca
    Left = 272
    Top = 296
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 404
    Top = 112
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Ativo = 1'
      'ORDER BY NOMEENTIDADE')
    Left = 404
    Top = 68
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object frxLOC_PATRI_114_01: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41802.645199189800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxLOC_PATRI_114_01GetValue
    Left = 272
    Top = 200
    Datasets = <
      item
        DataSet = frxDsLocCItsLca
        DataSetName = 'frxDsLocCItsLca'
      end
      item
        DataSet = frxDsLocCMovAll
        DataSetName = 'frxDsLocCMovAll'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MD2: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 238.110390000000000000
        Width = 718.110700000000000000
        DataSet = frxDsLocCItsLca
        DataSetName = 'frxDsLocCItsLca'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo21: TfrxMemoView
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DataField = 'REFERENCIA'
          DataSet = frxDsLocCItsLca
          DataSetName = 'frxDsLocCItsLca'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCItsLca."REFERENCIA"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 90.708720000000000000
          Width = 321.260050000000000000
          Height = 15.118110240000000000
          DataField = 'NO_GGX'
          DataSet = frxDsLocCItsLca
          DataSetName = 'frxDsLocCItsLca'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCItsLca."NO_GGX"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsLocCItsLca
          DataSetName = 'frxDsLocCItsLca'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCItsLca."SaiDtHr_TXT"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDsLocCItsLca
          DataSetName = 'frxDsLocCItsLca'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCItsLca."Codigo"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 650.079160000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Item'
          DataSet = frxDsLocCItsLca
          DataSetName = 'frxDsLocCItsLca'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCItsLca."Item"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 411.968770000000000000
          Width = 34.015726060000000000
          Height = 15.118110240000000000
          DataField = 'QtdeProduto'
          DataSet = frxDsLocCItsLca
          DataSetName = 'frxDsLocCItsLca'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCItsLca."QtdeProduto"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 445.984540000000000000
          Width = 34.015726060000000000
          Height = 15.118110240000000000
          DataField = 'QtdeDevolucao'
          DataSet = frxDsLocCItsLca
          DataSetName = 'frxDsLocCItsLca'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCItsLca."QtdeDevolucao"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 480.000310000000000000
          Width = 34.015726060000000000
          Height = 15.118110240000000000
          DataSet = frxDsLocCItsLca
          DataSetName = 'frxDsLocCItsLca'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[IIF(<frxDsLocCItsLca."QtdeDevolucao"> > <frxDsLocCItsLca."QtdeP' +
              'roduto">, 0, <frxDsLocCItsLca."QtdeProduto"> - <frxDsLocCItsLca.' +
              '"QtdeDevolucao">)]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 49.133890000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Width = 718.110700000000000000
          Height = 45.354330710000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo217: TfrxMemoView
          Left = 136.063080000000000000
          Top = 22.677180000000000000
          Width = 445.984251970000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE DEVOLU'#199#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo220: TfrxMemoView
          Left = 3.779530000000000000
          Width = 132.283464566929100000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy  hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo221: TfrxMemoView
          Left = 582.047620000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo63: TfrxMemoView
          Left = 136.063080000000000000
          Width = 445.984540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_EMPRESANOME]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD1: TfrxMasterData
        FillType = ftBrush
        Height = 49.133890000000000000
        Top = 128.504020000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo4: TfrxMemoView
          Top = 15.118120000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#176' CT:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Top = 30.236240000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_LocCConCab]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 64.252010000000000000
          Top = 15.118120000000000000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Equipamento:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 64.252010000000000000
          Top = 30.236240000000000000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_Equipamento]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 64.252010000000000000
          Width = 653.858690000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_Cliente]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 563.149970000000000000
          Top = 15.118120000000000000
          Width = 154.960730000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 563.149970000000000000
          Top = 30.236240000000000000
          Width = 154.960730000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_Periodo]')
          ParentFont = False
        end
      end
      object HE1: TfrxHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 200.315090000000000000
        Width = 718.110700000000000000
        object Memo23: TfrxMemoView
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 90.708720000000000000
          Width = 321.260050000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 411.968770000000000000
          Width = 34.015726060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data/hora Sa'#237'da')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#176' CT')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 650.079160000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Item')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 445.984540000000000000
          Width = 34.015726060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Devolv.')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 480.000310000000000000
          Width = 34.015726060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
      end
      object FO1: TfrxFooter
        FillType = ftBrush
        Height = 7.559060000000000000
        Top = 275.905690000000000000
        Width = 718.110700000000000000
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 343.937230000000000000
        Width = 718.110700000000000000
        DataSet = frxDsLocCMovAll
        DataSetName = 'frxDsLocCMovAll'
        RowCount = 0
        object Memo19: TfrxMemoView
          Left = 11.338590000000000000
          Width = 15.118120000000000000
          Height = 15.118110240000000000
          DataField = 'TipoES'
          DataSet = frxDsLocCMovAll
          DataSetName = 'frxDsLocCMovAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCMovAll."TipoES"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsLocCMovAll
          DataSetName = 'frxDsLocCMovAll'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCMovAll."DataHora"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 94.488250000000000000
          Width = 15.118120000000000000
          Height = 15.118110240000000000
          DataField = 'LIMPO'
          DataSet = frxDsLocCMovAll
          DataSetName = 'frxDsLocCMovAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCMovAll."LIMPO"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 109.606370000000000000
          Width = 15.118120000000000000
          Height = 15.118110240000000000
          DataField = 'SUJO'
          DataSet = frxDsLocCMovAll
          DataSetName = 'frxDsLocCMovAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCMovAll."SUJO"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 124.724490000000000000
          Width = 15.118120000000000000
          Height = 15.118110240000000000
          DataField = 'QUEBRADO'
          DataSet = frxDsLocCMovAll
          DataSetName = 'frxDsLocCMovAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCMovAll."QUEBRADO"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 139.842610000000000000
          Width = 15.118120000000000000
          Height = 15.118110240000000000
          DataField = 'TESTADODEVOLUCAO'
          DataSet = frxDsLocCMovAll
          DataSetName = 'frxDsLocCMovAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCMovAll."TESTADODEVOLUCAO"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 154.960730000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'COBRANCALOCACAO'
          DataSet = frxDsLocCMovAll
          DataSetName = 'frxDsLocCMovAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCMovAll."COBRANCALOCACAO"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 222.992270000000000000
          Width = 26.456666060000000000
          Height = 15.118110240000000000
          DataField = 'Quantidade'
          DataSet = frxDsLocCMovAll
          DataSetName = 'frxDsLocCMovAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCMovAll."Quantidade"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 249.448980000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'ValorLocacao'
          DataSet = frxDsLocCMovAll
          DataSetName = 'frxDsLocCMovAll'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCMovAll."ValorLocacao"]')
          ParentFont = False
        end
      end
      object HE2: TfrxHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        object Memo7: TfrxMemoView
          Left = 11.338590000000000000
          Width = 15.118120000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 94.488250000000000000
          Width = 15.118120000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'L')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 109.606370000000000000
          Width = 15.118120000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'S')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 124.724490000000000000
          Width = 15.118120000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Q')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 139.842610000000000000
          Width = 15.118120000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TD')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 154.960730000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cobran'#231'a')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 222.992270000000000000
          Width = 26.456666060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 249.448980000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Loca'#231#227'o')
          ParentFont = False
        end
      end
      object FO2: TfrxFooter
        FillType = ftBrush
        Top = 381.732530000000000000
        Width = 718.110700000000000000
      end
    end
  end
  object frxDsLocCItsLca: TfrxDBDataset
    UserName = 'frxDsLocCItsLca'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'CtrID=CtrID'
      'Item=Item'
      'GraGruX=GraGruX'
      'NO_GGX=NO_GGX'
      'REFERENCIA=REFERENCIA'
      'ValorDia=ValorDia'
      'ValorSem=ValorSem'
      'ValorQui=ValorQui'
      'ValorMes=ValorMes'
      'DtHrLocado=DtHrLocado'
      'DtHrRetorn=DtHrRetorn'
      'QtdeProduto=QtdeProduto'
      'ValorProduto=ValorProduto'
      'QtdeLocacao=QtdeLocacao'
      'ValorLocacao=ValorLocacao'
      'QtdeDevolucao=QtdeDevolucao'
      'ManejoLca=ManejoLca'
      'PATRIMONIO=PATRIMONIO'
      'SaiDtHr=SaiDtHr'
      'RetFunci=RetFunci'
      'RetExUsr=RetExUsr'
      'LibExUsr=LibExUsr'
      'LibFunci=LibFunci'
      'LibDtHr=LibDtHr'
      'RELIB=RELIB'
      'HOLIB=HOLIB'
      'ValDevolParci=ValDevolParci'
      'Troca=Troca'
      'Categoria=Categoria'
      'COBRANCALOCACAO=COBRANCALOCACAO'
      'CobrancaConsumo=CobrancaConsumo'
      'CobrancaRealizadaVenda=CobrancaRealizadaVenda'
      'CobranIniDtHr=CobranIniDtHr'
      'ValorFDS=ValorFDS'
      'DMenos=DMenos'
      'ValorLocAAdiantar=ValorLocAAdiantar'
      'ValorLocAtualizado=ValorLocAtualizado'
      'CalcAdiLOCACAO=CalcAdiLOCACAO'
      'DiasUteis=DiasUteis'
      'DiasFDS=DiasFDS'
      'PercDesco=PercDesco'
      'ValorBruto=ValorBruto'
      'LOGIN=LOGIN'
      'SaiDtHr_TXT=SaiDtHr_TXT')
    DataSet = QrLocCItsLca
    BCDToCurrency = False
    Left = 272
    Top = 344
  end
  object frxDsLocCMovAll: TfrxDBDataset
    UserName = 'frxDsLocCMovAll'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'GTRTab=GTRTab'
      'CtrID=CtrID'
      'Item=Item'
      'Controle=Controle'
      'SEQUENCIA=SEQUENCIA'
      'DataHora=DataHora'
      'USUARIO=USUARIO'
      'GraGruX=GraGruX'
      'TipoES=TipoES'
      'Quantidade=Quantidade'
      'TipoMotiv=TipoMotiv'
      'COBRANCALOCACAO=COBRANCALOCACAO'
      'VALORLOCACAO=ValorLocacao'
      'LIMPO=LIMPO'
      'SUJO=SUJO'
      'QUEBRADO=QUEBRADO'
      'TESTADODEVOLUCAO=TESTADODEVOLUCAO'
      'GGXEntrada=GGXEntrada'
      'MotivoTroca=MotivoTroca'
      'ObservacaoTroca=ObservacaoTroca'
      'QuantidadeLocada=QuantidadeLocada'
      'QuantidadeJaDevolvida=QuantidadeJaDevolvida'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'NO_ManejoLca=NO_ManejoLca')
    DataSet = QrLocCMovAll
    BCDToCurrency = False
    Left = 268
    Top = 488
  end
end
