unit LocCItsLca;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, Grids, DBGrids, ComCtrls, dmkEditDateTimePicker,
  UnDmkEnums, AppListas, UnAppPF, UnAppEnums, dmkDBGridZTO, dmkCheckBox,
  dmkRadioGroup;

type
  TFmLocCItsLca = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdCodUso: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdCtrID: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    SbGraGruX: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    DsGraGXPatr: TDataSource;
    VU_Sel_: TdmkValUsu;
    EdPatrimonio: TdmkEdit;
    QrPesq2: TmySQLQuery;
    QrPesq2Referencia: TWideStringField;
    QrPesq1: TmySQLQuery;
    QrPesq1Controle: TIntegerField;
    Label2: TLabel;
    Label7: TLabel;
    QrGraGXPatr: TmySQLQuery;
    QrGraGXPatrControle: TIntegerField;
    QrGraGXPatrReferencia: TWideStringField;
    QrGraGXPatrCOD_GG1: TIntegerField;
    QrGraGXPatrNO_GG1: TWideStringField;
    QrGraGXPatrCPL_EXISTE: TFloatField;
    QrGraGXPatrSituacao: TWordField;
    QrGraGXPatrAtualValr: TFloatField;
    QrGraGXPatrValorMes: TFloatField;
    QrGraGXPatrValorQui: TFloatField;
    QrGraGXPatrValorSem: TFloatField;
    QrGraGXPatrValorDia: TFloatField;
    QrGraGXPatrAgrupador: TIntegerField;
    QrGraGXPatrMarca: TIntegerField;
    QrGraGXPatrModelo: TWideStringField;
    QrGraGXPatrSerie: TWideStringField;
    QrGraGXPatrVoltagem: TWideStringField;
    QrGraGXPatrPotencia: TWideStringField;
    QrGraGXPatrCapacid: TWideStringField;
    QrGraGXPatrTXT_MES: TWideStringField;
    QrGraGXPatrTXT_QUI: TWideStringField;
    QrGraGXPatrTXT_SEM: TWideStringField;
    QrGraGXPatrTXT_DIA: TWideStringField;
    QrAgrupado: TmySQLQuery;
    QrAgrupadoReferencia: TWideStringField;
    QrAgrupadoNome: TWideStringField;
    GroupBox3: TGroupBox;
    Panel3: TPanel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label11: TLabel;
    Label10: TLabel;
    DBEdit2: TDBEdit;
    Label9: TLabel;
    DBEdit1: TDBEdit;
    Label8: TLabel;
    GroupBox4: TGroupBox;
    Panel5: TPanel;
    Label12: TLabel;
    DBEdit5: TDBEdit;
    DsAgrupado: TDataSource;
    Label13: TLabel;
    DBEdit6: TDBEdit;
    QrGraGXSemEstq: TMySQLQuery;
    QrGraGXSemEstqGraGXOutr: TIntegerField;
    QrGraGXSemEstqQuantidade: TIntegerField;
    QrGraGXSemEstqItemValr: TFloatField;
    QrGraGXSemEstqItemUnid: TIntegerField;
    QrGraGXPatrNO_SIT: TWideStringField;
    LaSitA1: TLabel;
    LaSitA2: TLabel;
    Label14: TLabel;
    QrGraGXPatrNO_SITAPL: TIntegerField;
    EdQtdeProduto: TdmkEdit;
    Label15: TLabel;
    Label16: TLabel;
    QrGraGXPatrEstqSdo: TFloatField;
    DbEdEstqSdo: TDBEdit;
    EdValorLocacao: TdmkEdit;
    Label17: TLabel;
    Label18: TLabel;
    EdValorTotalLocacaoItem: TdmkEdit;
    QrEstq: TMySQLQuery;
    QrEstqEstqSdo: TFloatField;
    QrGraGXSemEstqGraGruY: TIntegerField;
    QrGraGXComEstq: TMySQLQuery;
    QrGraGXComEstqGraGXOutr: TIntegerField;
    QrGraGXComEstqQuantidade: TIntegerField;
    QrGraGXComEstqGraGruY: TIntegerField;
    QrGraGXComEstqGraGruX: TIntegerField;
    QrGraGXComEstqComplem: TWideStringField;
    QrGraGXComEstqAquisData: TDateField;
    QrGraGXComEstqAquisDocu: TWideStringField;
    QrGraGXComEstqAquisValr: TFloatField;
    QrGraGXComEstqSituacao: TWordField;
    QrGraGXComEstqAtualValr: TFloatField;
    QrGraGXComEstqValorMes: TFloatField;
    QrGraGXComEstqValorQui: TFloatField;
    QrGraGXComEstqValorSem: TFloatField;
    QrGraGXComEstqValorDia: TFloatField;
    QrGraGXComEstqAgrupador: TIntegerField;
    QrGraGXComEstqMarca: TIntegerField;
    QrGraGXComEstqModelo: TWideStringField;
    QrGraGXComEstqSerie: TWideStringField;
    QrGraGXComEstqVoltagem: TWideStringField;
    QrGraGXComEstqPotencia: TWideStringField;
    QrGraGXComEstqCapacid: TWideStringField;
    QrGraGXComEstqVendaData: TDateField;
    QrGraGXComEstqVendaDocu: TWideStringField;
    QrGraGXComEstqVendaValr: TFloatField;
    QrGraGXComEstqVendaEnti: TIntegerField;
    QrGraGXComEstqObserva: TWideStringField;
    QrGraGXComEstqAGRPAT: TWideStringField;
    QrGraGXComEstqCLVPAT: TWideStringField;
    QrGraGXComEstqMARPAT: TWideStringField;
    QrGraGXComEstqAplicacao: TIntegerField;
    QrGraGXComEstqDescrTerc: TWideStringField;
    QrGraGXComEstqLk: TIntegerField;
    QrGraGXComEstqDataCad: TDateField;
    QrGraGXComEstqDataAlt: TDateField;
    QrGraGXComEstqUserCad: TIntegerField;
    QrGraGXComEstqUserAlt: TIntegerField;
    QrGraGXComEstqAlterWeb: TSmallintField;
    QrGraGXComEstqAWServerID: TIntegerField;
    QrGraGXComEstqAWStatSinc: TSmallintField;
    QrGraGXComEstqAtivo: TSmallintField;
    Panel6: TPanel;
    DBGGraGXComEstq: TdmkDBGridZTO;
    DBGGraGXSemEstq: TdmkDBGridZTO;
    DsGraGXSemEstq: TDataSource;
    DsGraGXComEstq: TDataSource;
    Splitter1: TSplitter;
    QrGraGXPatrValorFDS: TFloatField;
    QrGraGXPatrDMenos: TIntegerField;
    QrGraGXComEstqValorFDS: TFloatField;
    QrGraGXComEstqDMenos: TIntegerField;
    Label21: TLabel;
    TPDataLoc: TdmkEditDateTimePicker;
    EdHoraLoc: TdmkEdit;
    SbRecalculaSaldo: TSpeedButton;
    CkQtdeLocacao_: TdmkCheckBox;
    EdReferencia: TdmkEdit;
    Label19: TLabel;
    QrPesq2Patrimonio: TWideStringField;
    QrPesq1Referencia: TWideStringField;
    QrPesq1Patrimonio: TWideStringField;
    QrGraGXPatrPatrimonio: TWideStringField;
    SbGraGXPatr: TSpeedButton;
    QrGraGXSemEstqNome: TWideStringField;
    QrGraGXComEstqNome: TWideStringField;
    SpeedButton1: TSpeedButton;
    QrGraGXPatrNivel2: TIntegerField;
    RGQtdeLocacao: TdmkRadioGroup;
    Label20: TLabel;
    DBEdit7: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbGraGruXClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPatrimonioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure QrGraGXPatrAfterOpen(DataSet: TDataSet);
    procedure QrGraGXPatrBeforeClose(DataSet: TDataSet);
    procedure EdQtdeProdutoRedefinido(Sender: TObject);
    procedure EdValorLocacaoRedefinido(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure SbRecalculaSaldoClick(Sender: TObject);
    procedure SbGraGXPatrClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdPatrimonioChange(Sender: TObject);
    procedure EdPatrimonioExit(Sender: TObject);
    procedure EdReferenciaExit(Sender: TObject);
  private
    { Private declarations }
    //procedure Reopen_SorceSel_(Controle: Integer);
    FCategoria: String;
    //procedure ReopenLocCConIts(CtrID: Integer);
    procedure ReopenAgrupado();
    procedure PesquisaPorPatrimonio(Limpa: Boolean);
    procedure PesquisaPorReferencia(Limpa: Boolean);
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorNome(Key: Integer);
    //
    procedure IncluiOutros(Codigo, CtrID: Integer);
    procedure MostraSituacaoPatrimonio();
    procedure CalculaValorTotalLocacaoItem();
    procedure ReopenGraGXComEstq();
    procedure ReopenGraGXSemEstq();
    function  ObtemQtdeLocacao(): Integer;

  public
    { Public declarations }

    FQrCab(*, FQrIts*): TmySQLQuery;
    FDsCab: TDataSource;
    FOldGGX: Integer;
    FDtHrSai: TDateTime;
    FQtdeLocacaoAntes: Integer;
    FTipoAluguel, FDtHrLocado: String;
    FCtrID, FItem: Integer;
    FAlterou: Boolean;
    //FCliente: Integer;
    procedure ReopenTabelas();
  end;

  var
  FmLocCItsLca: TFmLocCItsLca;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnDmkProcFunc, UnMySQLCuringa, Principal, ModProd, GraGruY, UnGraL_Jan,
  MeuDBUses, GraGruX;

{$R *.DFM}

procedure TFmLocCItsLca.BtOKClick(Sender: TObject);
const
  OriSubstItem = 0;
  OriSubstSaiDH = '0000-00-00 00:00:00';
var
  Codigo, CtrID, GraGruX, (*DMenos,*) Empresa, NO_SITAPL: Integer;
  //ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS, ValorBruto: Double;
  QtdeLocacao, ManejoLca, GraGruY, Item, QtdeProduto: Integer;
  ValorProduto, ValorLocacao(*, CobrancaConsumo*)(*,
  ValorLocAAdiantar, ValorLocAtualizado*): Double;
  //SdoFuturo, SaldoMudado: Double;
  SQLType: TSQLType;
begin
  SQLTYpe := ImgTipo.SQLType;
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
  begin
    if MyObjects.FIC(QrGraGXPatrNO_SITAPL.Value = 0, nil,
      'A situa��o atual deste patrim�nio n�o permite sua loca��o!')
    then
      Exit;
  end;
  FCategoria     := 'L';
  Codigo         := FQrCab.FieldByName('Codigo').AsInteger;
  //CtrID          := EdCtrID.ValueVariant;
  CtrID          := FCtrID;
  GraGruX        := EdGraGruX.ValueVariant;
  Empresa        := FQrCab.FieldByName('Empresa').AsInteger;
  ValorProduto   := QrGraGXPatrAtualValr.Value;    // Pre�o de venda para indeniza��o
  QtdeProduto    := EdQtdeProduto.ValueVariant;
  //QtdeLocacao    := Geral.BoolToInt(CkQtdeLocacao.Checked);
  QtdeLocacao    := ObtemQtdeLocacao();
  ValorLocacao   := EdValorLocacao.ValueVariant;
  FDtHrLocado     := Geral.FDT(Trunc(TPDataLoc.Date), 1) + ' ' + EdHoraLoc.Text;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe um patrim�nio principal!') then
    Exit;
  if MyObjects.FIC(QtdeProduto <= 0, EdQtdeProduto, 'Informe a quantidade!') then
    Exit;
  if MyObjects.FIC(not (RGQtdeLocacao.ItemIndex in ([0, 1])), RGQtdeLocacao,
    'Informe a baixa do estoque!') then Exit;
  //
  //if AppPF.LocCItsMat_IncluiPri(SQLType, Codigo, CtrID) then
  if SQLType = stIns then
    Item := 0
  else
    Item := FItem;
  if AppPF.LocCItsMat_IncluiPri(SQLType, Codigo, CtrID, GraGruX, Empresa,
  QtdeLocacao, QtdeProduto, ValorLocacao, FQtdeLocacaoAntes, FDtHrLocado,
  FCategoria, OriSubstItem, OriSubstSaiDH, Item) then
  begin
    FAlterou := True;
    FItem := Item;
    //AppPF.AtualizaEstoqueGraGXPatr(GraGruX, FQrCab.FieldByName('Empresa').AsInteger, True);
    if SQLType = stIns then
      IncluiOutros(Codigo, CtrID);
    EdCtrID.ValueVariant := 0;
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
    EdGraGruX.ValueVariant := 0;
    CBGraGruX.KeyValue := Null;
    EdQtdeProduto.ValueVariant := 0.00;
    EdValorLocacao.ValueVariant := 0.00;
    //
    Dmod.VerificaSituacaoPatrimonio(GraGruX);
    if (FOldGGX <> 0) and (FOldGGX <> GraGruX) then
      Dmod.VerificaSituacaoPatrimonio(FOldGGX);
    //
{
    ReopenLocCConIts(CtrID);
    if FQrIts <> nil then
    begin
      FQrIts.Close;
      FQrIts.Open;
      FQrIts.Locate('CtrID', CtrID, []);
    end;
}
    if SQLType = stUpd then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE loccitslca ',
      'SET QtdeLocacao = ' + Geral.FF0(QtdeLocacao),
      'WHERE CtrID=' + Geral.FF0(CtrID),
      '']);
    end;
(* /////////////////////////////////////////////////////////////////////////////
================================================================================
   ******** N�o desmarcar! pode dar erro na qquantiade locada dos equipamentos
            secund�rios e dos acess�rios! do patrim�nio atual!!! **************
================================================================================
*)
    if (CkContinuar.Checked) and (CkContinuar.Visible) then
    begin
      UMyMod.AbreQuery(QrGraGXPatr, Dmod.MyDB);
      EdGraGruX.SetFocus;
    end else

///////////////////////////////////////////////////////////////////////////////
      Close;
  end;
end;

{  Movido para TUnAppPF.LocCItsMat_IncluiPri(...
procedure TFmLocCItsLca.BtOKClick(Sender: TObject);
var
  Codigo, CtrID, GraGruX, DMenos: Integer;
  ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS, ValorBruto: Double;
  // Fazer no Cab?  DtHrLocado: String;
  (*
var
  DtHrRetorn, LibDtHr, RELIB, HOLIB,*)
  //CobrancaRealizadaVenda,
  //COBRANCALOCACAO,
  //Categoria: String;
  // Fazer no Cab? SaiDtHr,
  //CobranIniDtHr: String;
  //Codigo, CtrID, GraGruX,
  //RetFunci, RetExUsr, LibExUsr, LibFunci,
  //QtdeProduto,
  (*, QtdeDevolucao, Troca,*)
  QtdeLocacao, ManejoLca, GraGruY, Item, QtdeProduto: Integer;
  ValorProduto, ValorLocacao(*, CobrancaConsumo*),
  ValorLocAAdiantar, ValorLocAtualizado: Double;
  SQLType: TSQLType;
  SdoFuturo, SaldoMudado: Double;
begin
  FAlterou     := True;
  FCategoria   := 'L';
  SQLType := ImgTipo.SQLType;
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
  begin
    if MyObjects.FIC(QrGraGXPatrNO_SITAPL.Value = 0, nil,
      'A situa��o atual deste patrim�nio n�o permite sua loca��o!')
    then
      Exit;
  end;
  //
  Codigo         := FQrCab.FieldByName('Codigo').AsInteger;
  CtrID          := EdCtrID.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  ValorDia       := QrGraGXPatrValorDia.Value;
  ValorSem       := QrGraGXPatrValorSem.Value;
  ValorQui       := QrGraGXPatrValorQui.Value;
  ValorMes       := QrGraGXPatrValorMes.Value;
  ValorFDS       := QrGraGXPatrValorFDS.Value;
  DMenos         := QrGraGXPatrDMenos.Value;
  // Fazer no Cab?
  FDtHrLocado     := Geral.FDT(Trunc(TPDataLoc.Date), 1) + ' ' + EdHoraLoc.Text;
(*  Dados somente ap�s inclus�o (Versao AAPA)
  RetFunci       := ;
  RetExUsr       := ;
  DtHrRetorn     := ;
  LibExUsr       := ;
  LibFunci       := ;
  LibDtHr        := ;
  RELIB          := ;
  HOLIB          := ;
*)
  ValorProduto   := QrGraGXPatrAtualValr.Value;    // Pre�o de venda para indeniza��o
  QtdeProduto    := EdQtdeProduto.ValueVariant;
  QtdeLocacao    := Geral.BoolToInt(CkQtdeLocacao.Checked);
  ValorLocacao   := EdValorLocacao.ValueVariant;
  ValorLocAAdiantar  := QtdeProduto * QtdeLocacao * ValorLocacao;
  ValorLocAtualizado := ValorLocAAdiantar;
  ValorBruto         := ValorLocAAdiantar;
  //QtdeDevolucao  := ;
  //Troca          := ;
  //Categoria      := 'L'; // Loca��o
  //CobrancaConsumo:= ; ??? valor quando definido igual ao ValorLocacao
(*
 SELECT its.CATEGORIA,
  CASE
  WHEN VALORPRODUTO = 0 THEN "VP=0"
  WHEN VALORPRODUTO=COBRANCACONSUMO THEN "VP=CC"
  ELSE "???" END VPxCC,
COUNT(its.PRODUTO) ITENS
FROM locacaoitens its
WHERE its.COBRANCACONSUMO <> 0
GROUP BY CATEGORIA, VPxCC
*)
  //CobrancaRealizadaVenda:= ; "S" quando cobrou venda!
  // Fazer no Cab? SaiDtHr        := Geral.FDT(Trunc(TPSaiDtHr.Date), 1) + ' ' + EdSaiDtHr.Text; // := Geral.FDT(FDtHrSai, 109);
  //COBRANCALOCACAO:= '2 M - 1 Q - 1 S - 1 D';
  // Fazer no Cab? CobranIniDtHr  := Geral.FDT(Trunc(TPCobranIniDtHr.Date), 1) + ' ' + EdCobranIniDtHr.Text; Fazer calculo de DMenos e Sabado + feriados!
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe um patrim�nio principal!') then
    Exit;
  if MyObjects.FIC(QtdeProduto <= 0, EdQtdeProduto, 'Informe a quantidade!') then
    Exit;
  if Dmod.QrOpcoesTRenPermLocSemEstq.Value = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEstq, Dmod.MyDB, [
    'SELECT EstqSdo  ',
    'FROM GraGruEPat ',
    'WHERE GraGruX=' + Geral.FF0(GraGruX),
    'AND Empresa=' + Geral.FF0(FQrCab.FieldByName('Empresa').AsInteger),
    '']);
    SdoFuturo := QrEstqEstqSdo.Value + FQtdeLocacaoAntes - (QtdeLocacao * QtdeProduto);
    //
    if SdoFuturo < 0 then
    begin
      Geral.MB_Info('Saldo insuficiente para loca��o!');
      SaldoMudado := QrGraGXPatrEstqSdo.Value - QrEstqEstqSdo.Value;
      if SaldoMudado <> 0 then
      begin
        Geral.MB_Info(
        'ATEN��O: o saldo deste patrim�nio mudou durante esta inclus�o!' +
        'Outro usu�rio pode ter liberado ou locada o item!' +
        sLineBreak + 'Diferen�a: ' + Geral.FFT(SaldoMudado, 2, siNegativo));
      end;
      Exit;
    end;
  end;
  //
  GraGruY   := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
  ManejoLca := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);
  //
  CtrID := FCtrID; //UMyMod.BPGS1I32('loccitslca', 'CtrID', '', '', tsPos, SQLType, CtrID);
  Item := UMyMod.BPGS1I32('loccitslca', 'Item', '', '', tsPos, SQLType, Item);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitslca', False, [
    'Codigo', 'GraGruX', 'ValorDia',
    'ValorSem', 'ValorQui', 'ValorMes',
    // Fazer no Cab?
    'DtHrLocado',
    'ValorProduto', 'QtdeLocacao', 'ValorLocacao',
    // Fazer no Cab? 'SaiDtHr',
    //'CobranIniDtHr',
    'Categoria', 'QtdeProduto', 'ManejoLca',
    'CtrID', 'ValorFDS', 'DMenos',
    'ValorLocAAdiantar', 'ValorLocAtualizado', 'ValorBruto'
  ], [
    'Item'
  ], [
    Codigo, GraGruX, ValorDia,
    ValorSem, ValorQui, ValorMes,
    // Fazer no Cab?
    FDtHrLocado,
    ValorProduto, QtdeLocacao , ValorLocacao,
    // Fazer no Cab? SaiDtHr,
    //CobranIniDtHr,
    FCategoria, QtdeProduto, ManejoLca,
    CtrID, ValorFDS, DMenos,
    ValorLocAAdiantar, ValorLocAtualizado, ValorBruto
  ], [
    Item], True) then
  begin
    FItem := Item;
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, FQrCab.FieldByName('Empresa').AsInteger, True);
    if SQLType = stIns then
      IncluiOutros(Codigo, CtrID);
    EdCtrID.ValueVariant := 0;
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
    EdGraGruX.ValueVariant := 0;
    CBGraGruX.KeyValue := Null;
    //
    Dmod.VerificaSituacaoPatrimonio(GraGruX);
    if (FOldGGX <> 0) and (FOldGGX <> GraGruX) then
      Dmod.VerificaSituacaoPatrimonio(FOldGGX);
    //
(*
    ReopenLocCConIts(CtrID);
    if FQrIts <> nil then
    begin
      FQrIts.Close;
      FQrIts.Open;
      FQrIts.Locate('CtrID', CtrID, []);
    end;
*)
    if (CkContinuar.Checked) and (CkContinuar.Visible) then
    begin
      UMyMod.AbreQuery(QrGraGXPatr, Dmod.MyDB);
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;
}

procedure TFmLocCItsLca.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCItsLca.CalculaValorTotalLocacaoItem;
var
  QtdeLocacao: Integer;
  QtdeProduto, ValorLocacao, ValorTotalLocacaoItem: Double;
begin
  QtdeLocacao  := ObtemQtdeLocacao();
  QtdeProduto  := EdQtdeProduto.ValueVariant;
  ValorLocacao := EdValorLocacao.ValueVariant;
  ValorTotalLocacaoItem := QtdeLocacao * QtdeProduto * ValorLocacao;
  EdValorTotalLocacaoItem.ValueVariant := ValorTotalLocacaoItem;
end;

procedure TFmLocCItsLca.EdGraGruXChange(Sender: TObject);
begin
  if (not EdPatrimonio.Focused) and
     (not EdReferencia.Focused) then
    PesquisaPorGraGruX();
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmLocCItsLca.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //PesquisaPorNome(Key);
end;

procedure TFmLocCItsLca.EdGraGruXRedefinido(Sender: TObject);
var
  ValorLocacao: Double;
begin
if ImgTipo.SQLType = stIns then
  begin
    if FTipoAluguel = 'D' then
      ValorLocacao := QrGraGXPatrValorDia.Value
    else
    if FTipoAluguel = 'S' then
      ValorLocacao := QrGraGXPatrValorSem.Value
    else
    if FTipoAluguel = 'Q' then
      ValorLocacao := QrGraGXPatrValorQui.Value
    else
    if FTipoAluguel = 'M' then
      ValorLocacao := QrGraGXPatrValorMes.Value
    else
    begin
      Geral.MB_Erro('"Tipo de Aluguel" n�o definido! (Principal)');
      ValorLocacao := 0;
    end;
    //
    EdValorLocacao.ValueVariant := ValorLocacao;
    //
    ReopenGraGXComEstq();
    ReopenGraGXSemEstq();
  end;
end;

procedure TFmLocCItsLca.EdQtdeProdutoRedefinido(Sender: TObject);
begin
  CalculaValorTotalLocacaoItem();
end;

procedure TFmLocCItsLca.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmLocCItsLca.EdReferenciaExit(Sender: TObject);
begin
  if EdReferencia.Text <> '' then
    PesquisaPorReferencia(True);
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmLocCItsLca.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmLocCItsLca.EdPatrimonioChange(Sender: TObject);
begin
  if EdPatrimonio.Focused then
    PesquisaPorPatrimonio(False);
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;

end;

procedure TFmLocCItsLca.EdPatrimonioExit(Sender: TObject);
begin
  if EdPatrimonio.Text <> '' then
    PesquisaPorPatrimonio(True);
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmLocCItsLca.EdPatrimonioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  PesquisaPorNome(Key);
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmLocCItsLca.EdValorLocacaoRedefinido(Sender: TObject);
begin
  CalculaValorTotalLocacaoItem();
end;

procedure TFmLocCItsLca.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmLocCItsLca.FormCreate(Sender: TObject);
begin
  FAlterou := False;
  FOldGGx  := 0;
  FQtdeLocacaoAntes := 0;
  ImgTipo.SQLType := stLok;
  //
  TPDataLoc.Date := Trunc(Date);
  EdHoraLoc.Text := FormatDateTime('hh:nn', Now());
  //
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPatr, Dmod.MyDB, [
  'SELECT ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL, ggx.Controle, ',
  'gg1.Referencia, gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1,   ',
  'IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE,   ',
  'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,  ',
  'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,  ',
  'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,  ',
  'cpl.Voltagem, cpl.Potencia, cpl.Capacid    ',
  'FROM gragrux ggx   ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle   ',
  'LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle   ',
  'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo   ',
  'LEFT JOIN graglsitu ggs ON ggs.Codigo=cpl.Situacao ',
  'WHERE ' + Filtro,
  'ORDER BY NO_GG1 ',
  '']);
}
end;

procedure TFmLocCItsLca.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCItsLca.FormShow(Sender: TObject);
begin
  CkContinuar.Visible := ImgTipo.SQLType = stIns;
end;

procedure TFmLocCItsLca.IncluiOutros(Codigo, CtrID: Integer);
const
  OriSubstItem = 0;
  OriSubstSaiDH = '0000-00-00 00:00:00';

  procedure IncluiAgrupado();
  var
    Item, GraGruX, GraGruY, ManejoLca: Integer;
    Tabela: String;
    ValorProduto: Double;
  begin
    if QrGraGXPatrAgrupador.Value <> EdGraGruX.ValueVariant then
    begin
      //
      GraGruX := QrGraGXPatrAgrupador.Value;
      if GraGruX <> 0 then
      begin
        GraGruY   := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
        ManejoLca := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);
        //
        if AppPF.ObtemValorProdutoDeGraGruXEGraGruY(GraGruX, GraGruY, ValorProduto) then
        begin
          if AppPF.ObtemTabelaDeGraGruY_Movimento(GraGruY, Tabela) then
          begin
            Item := UMyMod.BPGS1I32(Tabela, 'Item', '', '', tsPos, stIns, 0);
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, Tabela, False, [
              'Codigo', 'CtrID', 'GraGruX',
              'ValorProduto', 'ManejoLca'], ['Item'], [
              Codigo, CtrID, GraGruX,
              ValorProduto, ManejoLca], [Item], True);
          end;
        end;
      end;
    end;
  end;

{
  procedure IncluiSec();
  var
    Codigo, GraGruX, Item, ManejoLca, GraGruY, DMenos: Integer;
    ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS: Double;
    Categoria: String;
    QtdeLocacao, QtdeProduto: Integer;
    ValorProduto, ValorLocacao: Double;
    SQLType: TSQLType;
    SdoFuturo, SaldoMudado: Double;
  begin
    SQLType := ImgTipo.SQLType;
    if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    begin
      if MyObjects.FIC(QrGraGXPatrNO_SITAPL.Value = 0, nil,
        'A situa��o atual deste patrim�nio n�o permite sua loca��o!')
      then
        Exit;
    end;
    //

    Codigo         := FQrCab.FieldByName('Codigo').AsInteger;
    //CtrID          := EdCtrID.ValueVariant;
    GraGruX        := QrGraGXComEstqGraGruX.Value;
    ValorDia       := QrGraGXComEstqValorDia.Value;
    ValorSem       := QrGraGXComEstqValorSem.Value;
    ValorQui       := QrGraGXComEstqValorQui.Value;
    ValorMes       := QrGraGXComEstqValorMes.Value;
    ValorFDS       := QrGraGXComEstqValorFDS.Value;
    DMenos         := QrGraGXComEstqDMenos.Value;
    // Fazer no Cab? DtHrLocado     := Geral.FDT(Trunc(TPDataLoc.Date), 1) + ' ' + EdHoraLoc.Text;
  (*  Dados somente ap�s inclus�o (Versao AAPA)
    RetFunci       := ;
    RetExUsr       := ;
    DtHrRetorn     := ;
    LibExUsr       := ;
    LibFunci       := ;
    LibDtHr        := ;
    RELIB          := ;
    HOLIB          := ;
  *)
    ValorProduto   := QrGraGXComEstqAtualValr.Value;    // Pre�o de venda para indeniza��o
    QtdeLocacao    := Geral.BoolToInt(CkQtdeLocacao.Checked);
    QtdeProduto    := QrGraGXComEstqQuantidade.Value;
    //ValorLocacao   := / EdValorLocacao.ValueVariant;
    if FTipoAluguel = 'D' then
      ValorLocacao := ValorDia
    else
    if FTipoAluguel = 'S' then
      ValorLocacao := ValorSem
    else
    if FTipoAluguel = 'Q' then
      ValorLocacao := ValorQui
    else
    if FTipoAluguel = 'M' then
      ValorLocacao := ValorMes
    else
    begin
      Geral.MB_Erro('"Tipo de Aluguel" n�o definido! (2048)');
      ValorLocacao := 0;
    end;

    //QtdeDevolucao  := ;
    //Troca          := ;
    Categoria      := 'L'; // Loca��o
    //CobrancaConsumo:= ; ??? valor quando definido igual ao ValorLocacao
  (*
   SELECT its.CATEGORIA,
    CASE
    WHEN VALORPRODUTO = 0 THEN "VP=0"
    WHEN VALORPRODUTO=COBRANCACONSUMO THEN "VP=CC"
    ELSE "???" END VPxCC,
  COUNT(its.PRODUTO) ITENS
  FROM locacaoitens its
  WHERE its.COBRANCACONSUMO <> 0
  GROUP BY CATEGORIA, VPxCC
  *)
    //CobrancaRealizadaVenda:= ; "S" quando cobrou venda!
    // Fazer no Cab? SaiDtHr        := Geral.FDT(Trunc(TPSaiDtHr.Date), 1) + ' ' + EdSaiDtHr.Text; // := Geral.FDT(FDtHrSai, 109);
    //COBRANCALOCACAO:= '2 M - 1 Q - 1 S - 1 D';
    // Fazer no Cab? CobranIniDtHr  := Geral.FDT(Trunc(TPCobranIniDtHr.Date), 1) + ' ' + EdCobranIniDtHr.Text; ???
    //
(*
    if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe um patrim�nio principal!') then
      Exit;
*)
    if Dmod.QrOpcoesTRenPermLocSemEstq.Value = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrEstq, Dmod.MyDB, [
      'SELECT EstqSdo  ',
      'FROM GraGruEPat ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      'AND Empresa=' + Geral.FF0(FQrCab.FieldByName('Empresa').AsInteger),
      '']);
      SdoFuturo := QrEstqEstqSdo.Value - (QtdeLocacao * QtdeProduto);
      //
      if SdoFuturo < 0 then
      begin
        Geral.MB_Info('Saldo insuficiente para loca��o! Produto ' + Geral.FF0(GraGruX));
        //Exit;  impedir na confirma��o?
      end;
    end;
    //
    GraGruY        := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
    ManejoLca      := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);
    //
    //CtrID := UMyMod.BPGS1I32('loccitslca', 'CtrID', '', '', tsPos, SQLType, CtrID);
    Item := UMyMod.BPGS1I32('loccitslca', 'Item', '', '', tsPos, SQLType, CtrID);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitslca', False, [
      'Codigo', 'GraGruX', 'ValorDia',
      'ValorSem', 'ValorQui', 'ValorMes',
      // Fazer no Cab?
      'DtHrLocado',
      'ValorProduto', 'QtdeLocacao', 'ValorLocacao',
      // Fazer no Cab? 'SaiDtHr',
      //'CobranIniDtHr',
      'Categoria', 'QtdeProduto', 'ManejoLca',
      'CtrID', 'ValorFDS', 'DMenos'
    ], [
    'Item'], [
      Codigo, GraGruX, ValorDia,
      ValorSem, ValorQui, ValorMes,
      // Fazer no Cab?
      FDtHrLocado,
      ValorProduto, QtdeLocacao , ValorLocacao,
      // Fazer no Cab? SaiDtHr,
      //CobranIniDtHr,
      Categoria, QtdeProduto, ManejoLca,
      CtrID, ValorFDS, DMenos
    ], [
    Item], True) then
    begin
      AppPF.AtualizaEstoqueGraGXPatr(GraGruX, FQrCab.FieldByName('Empresa').AsInteger, True);
    end;
  end;
}
  //
{
  procedure IncluiApo();
  var
    Item, GraGruX, GraGruY, ManejoLca, QtdeLocacao: Integer;
    QtdIni, QtdeProduto, ValorProduto, PrcUni: Double;
  begin
    //Item           := 0;
    GraGruX        := QrGraGXSemEstqGraGXOutr.Value;
    //ValBem         := QrGraGXSemEstqItemValr.Value;
    PrcUni         := QrGraGXSemEstqItemValr.Value;
    QtdIni         := QrGraGXSemEstqQuantidade.Value;
    //
    GraGruY     := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
    ManejoLca   := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);
    QtdeLocacao := 0;
    QtdeProduto := 0;
    //QtdeProduto := QrGraGXSemEstqQuantidade.Value;

    //Item := UMyMod.BPGS1I32('loccpatace', 'Item', '', '', tsPos, stIns, 0);
    Item := UMyMod.BPGS1I32('loccitsruc', 'Item', '', '', tsPos, stIns, 0);

    //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatace', False, [
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccitsruc', False, [
    'Codigo', 'CtrID', 'GraGruX',
    //'DtHrLocado',
    'QtdeLocacao',
    'Categoria', 'QtdeProduto', 'ManejoLca',

    'PrcUni', 'QtdIni'], ['Item'], [
    Codigo, CtrID, GraGruX,
    //FDtHrLocado,
    QtdeLocacao,
    FCategoria, QtdeProduto, ManejoLca,
    PrcUni, QtdIni], [Item], True);
  end;
  //
}
{
  procedure IncluiAce();
  var
    Item, GraGruX, GraGruY, ManejoLca, QtdeLocacao: Integer;
    QtdeProduto, ValorProduto: Double;
  begin
    //Item           := 0;
    GraGruX        := QrGraGXComEstqGraGXOutr.Value;
    ValorProduto   := QrGraGXComEstqAtualValr.Value;
    QtdeLocacao    := Geral.BoolToInt(CkQtdeLocacao.Checked);
    QtdeProduto    := QrGraGXComEstqQuantidade.Value;
    //
    GraGruY   := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
    ManejoLca := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);

    //Item := UMyMod.BPGS1I32('loccpatace', 'Item', '', '', tsPos, stIns, 0);
    Item := UMyMod.BPGS1I32('loccitslca', 'Item', '', '', tsPos, stIns, 0);
    //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatace', False, [
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccitslca', False, [
    'Codigo', 'GraGruX', 'ValorDia',
    'ValorSem', 'ValorQui', 'ValorMes',
    'DtHrLocado',
    'Categoria', 'QtdeProduto',
    'ValorProduto', 'QtdeLocacao', 'ManejoLca',
    'CtrID', 'ValorFDS', 'DMenos'], [
    'Item'], [
    Codigo, GraGruX, (*ValorDia*)0,
    (*ValorSem*)0, (*ValorQui*)0, (*ValorMes*)0,
    FDtHrLocado,
    FCategoria, QtdeProduto,
    ValorProduto, QtdeLocacao, ManejoLca,
    CtrID, (*ValorFDS*)0, (*DMenos*)0], [
    Item], True) then
    begin
      AppPF.AtualizaEstoqueGraGXPatr(GraGruX, FQrCab.FieldByName('Empresa').AsInteger, True);
    end;
  end;
}
  //
{
  procedure IncluiCns();
  var
    Item, GraGruX, Unidade, GraGruY, ManejoLca, QtdeLocacao: Integer;
    QtdIni, QtdFim, PrcUni, ValUso, QtdeProduto: Double;
  begin
    //Item           := 0;
    GraGruX        := QrGraGXSemEstqGraGXOutr.Value;
    Unidade        := QrGraGXSemEstqItemUnid.Value;
    QtdIni         := 0;
    QtdFim         := 0;
    PrcUni         := QrGraGXSemEstqItemValr.Value;;
    ValUso         := 0;
    //
    GraGruY        := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
    ManejoLca      := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);
    QtdeLocacao    := 0;
    QtdeProduto    := 0;

    //Item := UMyMod.BPGS1I32('loccpatcns', 'Item', '', '', tsPos, stIns, 0);
    Item := UMyMod.BPGS1I32('loccitsruc', 'Item', '', '', tsPos, stIns, 0);
    //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatcns', False, [
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccitsruc', False, [
    'Codigo', 'CtrID', 'GraGruX',
    //'DtHrLocado',
    'QtdeLocacao',
    'Categoria', 'QtdeProduto', 'ManejoLca',
    'Unidade', 'QtdIni', 'QtdFim',
    'PrcUni', 'ValUso'], [
    'Item'], [
    Codigo, CtrID, GraGruX,
    //FDtHrLocado,
    QtdeLocacao,
    FCategoria, QtdeProduto, ManejoLca,
    Unidade, QtdIni, QtdFim,
    PrcUni, ValUso], [
    Item], True);
  end;
}
  //
{
  procedure IncluiUso();
  var
    Item, GraGruX, Unidade, AvalIni, GraGruY, ManejoLca, QtdeLocacao: Integer;
    AvalFim, PrcUni, ValUso, QtdeProduto: Double;
  begin
    //Item           := 0;
    GraGruX        := QrGraGXSemEstqGraGXOutr.Value;
    Unidade        := QrGraGXSemEstqItemUnid.Value;
    AvalIni        := 0;
    AvalFim        := 0;
    PrcUni         := QrGraGXSemEstqItemValr.Value;
    ValUso         := 0;
    //
    QtdeLocacao    := 0;
    QtdeProduto    := 0;
    GraGruY        := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
    ManejoLca      := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);

    //Item := UMyMod.BPGS1I32('loccpatuso', 'Item', '', '', tsPos, stIns, 0);
    Item := UMyMod.BPGS1I32('loccitsruc', 'Item', '', '', tsPos, stIns, 0);
    //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatuso', False, [
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccitsruc', False, [
    'Codigo', 'CtrID', 'GraGruX',
    //'DtHrLocado',
    'QtdeLocacao',
    'Categoria', 'QtdeProduto', 'ManejoLca',
    'Unidade', 'AvalIni', 'AvalFim',
    'PrcUni', 'ValUso'], [
    'Item'], [
    Codigo, CtrID, GraGruX,
    //FDtHrLocado,
    QtdeLocacao,
    FCategoria, QtdeProduto, ManejoLca,
    Unidade, AvalIni, AvalFim,
    PrcUni, ValUso], [
    Item], True);
  end;
}
var
  I, GraGruX, Empresa, Item, ManejoLca, Unidade: Integer;
  ValorProduto, QtdeLocacao, QtdeProduto, PrcUni: Double;
  Categoria: String;
begin
  Categoria := 'L';
  Empresa := FQrCab.FieldByName('Empresa').AsInteger;
  IncluiAgrupado();
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXSemEstq, Dmod.MyDB, [
  'SELECT gpi.GraGXOutr, ggo.Aplicacao, ggo.ItemValr, ggo.ItemUnid ',
  'FROM gragxpits gpi ',
  'LEFT JOIN gragxoutr ggo ON ggo.GraGruX=gpi.GraGXOutr ',
  'WHERE  gpi.GraGXPatr=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND ggo.Aplicacao <> 0 ',
  '']);
*)


  //Fazer separado para as duas tabelas!



  //Aqui: SQL para gragxpatr!
  //ReopenGraGXComEstq();  N�o pode abrir aqui! perde os SelectedRows!
  QrGraGXComEstq.First;
  if DBGGraGXComEstq.SelectedRows.Count > 0 then
  begin
    with DBGGraGXComEstq.DataSource.DataSet do
    for I := 0 to DBGGraGXComEstq.SelectedRows.Count-1 do
    begin
      Item := 0;
      //GotoBookmark(pointer(DBGGraGXComEstq.SelectedRows.Items[i]));
      GotoBookmark(DBGGraGXComEstq.SelectedRows.Items[i]);
      //
      GraGruX        := QrGraGXComEstqGraGruX.Value;
      ValorProduto   := QrGraGXComEstqAtualValr.Value;    // Pre�o de venda para indeniza��o
      //QtdeLocacao    := Geral.BoolToInt(CkQtdeLocacao.Checked);
      QtdeLocacao    := ObtemQtdeLocacao();
      QtdeProduto    := QrGraGXComEstqQuantidade.Value;
      ManejoLca      := AppPF.ObtemManejoLocaDeGraGruY(QrGraGXComEstqGraGruY.Value);
      //
      case QrGraGXComEstqGraGruY.Value of
        //CO_GraGruY_1024_GXPatPri: IncluiPri();
        CO_GraGruY_2048_GXPatSec: //IncluiSec();
          AppPF.LocCItsMat_IncluiSec(stIns, Codigo, CtrID, GraGruX, Empresa,
          QtdeLocacao, QtdeProduto, FTipoAluguel, FDtHrLocado, nil(*QrLocIPatSec*),
          OriSubstItem, OriSubstSaiDH, Item);
        //
        CO_GraGruY_3072_GXPatAce: //IncluiAce();
          AppPF.LocCItsMat_IncluiAce(stIns, Codigo, CtrID, GraGruX, Empresa,
          ManejoLca, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
          nil(*QrLocIPatAce*), OriSubstItem, OriSubstSaiDH, Item);
        //CO_GraGruY_4096_GXPatApo: IncluiApo();
        //CO_GraGruY_5120_GXPatUso: IncluiUso();
        //CO_GraGruY_6144_GXPrdCns: IncluiCns();
        else Geral.MensagemBox('Aplica��o n�o definida em inclus�o de materiais! (1)',
        'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
  end else
  begin
    while not QrGraGXComEstq.Eof do
    begin
      //
      GraGruX        := QrGraGXComEstqGraGruX.Value;
      ValorProduto   := QrGraGXComEstqAtualValr.Value;    // Pre�o de venda para indeniza��o
      //QtdeLocacao    := Geral.BoolToInt(CkQtdeLocacao.Checked);
      QtdeLocacao    := ObtemQtdeLocacao();
      QtdeProduto    := QrGraGXComEstqQuantidade.Value;
      ManejoLca      := AppPF.ObtemManejoLocaDeGraGruY(QrGraGXComEstqGraGruY.Value);
      //
      case QrGraGXComEstqGraGruY.Value of
        //CO_GraGruY_1024_GXPatPri: IncluiPri();
        CO_GraGruY_2048_GXPatSec: //IncluiSec();
          AppPF.LocCItsMat_IncluiSec(stIns, Codigo, CtrID, GraGruX, Empresa,
          QtdeLocacao, QtdeProduto, FTipoAluguel, FDtHrLocado, nil(*QrLocIPatSec*),
          OriSubstItem, OriSubstSaiDH, Item);
        //
        //CO_GraGruY_3072_GXPatAce: IncluiAce();
        CO_GraGruY_3072_GXPatAce: //IncluiAce();
          AppPF.LocCItsMat_IncluiAce(stIns, Codigo, CtrID, GraGruX, Empresa,
          ManejoLca, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
          nil(*QrLocIPatAce*), OriSubstItem, OriSubstSaiDH, Item);
        //CO_GraGruY_4096_GXPatApo: IncluiApo();
        //CO_GraGruY_5120_GXPatUso: IncluiUso();
        //CO_GraGruY_6144_GXPrdCns: IncluiCns();
        else Geral.MensagemBox('Aplica��o n�o definida em inclus�o de materiais! (1)',
        'Aviso', MB_OK+MB_ICONWARNING);
      end;
      //
      QrGraGXComEstq.Next;
    end;
  end;



  //SQL para gragxoutr!
  //ReopenGraGXSemEstq(); N�o pode abrir aqui! perde os SelectedRows!
  QrGraGXSemEstq.First;
  if DBGGraGXSemEstq.SelectedRows.Count > 0 then
  begin
    with DBGGraGXSemEstq.DataSource.DataSet do
    for I := 0 to DBGGraGXSemEstq.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(DBGGraGXSemEstq.SelectedRows.Items[i]));
      GotoBookmark(DBGGraGXSemEstq.SelectedRows.Items[i]);
      //
      GraGruX        := QrGraGXSemEstqGraGXOutr.Value;
      PrcUni         := QrGraGXSemEstqItemValr.Value;
      ValorProduto   := QrGraGXSemEstqItemValr.Value;    // Pre�o de venda para indeniza��o
      //QtdeLocacao    := Geral.BoolToInt(CkQtdeLocacao.Checked);
      QtdeLocacao    := ObtemQtdeLocacao();
      QtdeProduto    := QrGraGXSemEstqQuantidade.Value;
      ManejoLca      := AppPF.ObtemManejoLocaDeGraGruY(QrGraGXSemEstqGraGruY.Value);
      Unidade        := QrGraGXSemEstqItemUnid.Value;
      //
      case QrGraGXSemEstqGraGruY.Value of
        //CO_GraGruY_1024_GXPatPri: IncluiPri();
        //CO_GraGruY_2048_GXPatSec: IncluiSec();
        //CO_GraGruY_3072_GXPatAce: IncluiAce();
        CO_GraGruY_4096_GXPatApo: //IncluiApo(Codigo, CtrID); // = Apoio
          AppPF.LocCItsMat_IncluiApo(stIns, Codigo, CtrID, GraGruX, ManejoLca,
          PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
          nil(*QrLocIPatApo*), Item);
        //
        CO_GraGruY_5120_GXPatUso: //IncluiUso(Codigo, CtrID); // = Uso (desgaste)
          AppPF.LocCItsMat_IncluiUso(stIns, Codigo, CtrID, GraGruX, ManejoLca, Unidade,
          PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
          nil(*FQrLocIPatUso*), Item);
        //
        CO_GraGruY_6144_GXPrdCns: //IncluiCns(Codigo, CtrID); // = Uso (consumo)
          AppPF.LocCItsMat_IncluiCns(stIns, Codigo, CtrID, GraGruX, ManejoLca, Unidade,
          PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
          nil(*FQrLocIPatCns*), Item);
        else Geral.MensagemBox('Aplica��o n�o definida em inclus�o de materiais (2)!',
        'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
  end else
  begin
    while not QrGraGXSemEstq.Eof do
    begin
      //
      GraGruX        := QrGraGXSemEstqGraGXOutr.Value;
      PrcUni         := QrGraGXSemEstqItemValr.Value;
      ValorProduto   := QrGraGXSemEstqItemValr.Value;    // Pre�o de venda para indeniza��o
      //QtdeLocacao    := Geral.BoolToInt(CkQtdeLocacao.Checked);
      QtdeLocacao    := ObtemQtdeLocacao();
      QtdeProduto    := QrGraGXSemEstqQuantidade.Value;
      ManejoLca      := AppPF.ObtemManejoLocaDeGraGruY(QrGraGXSemEstqGraGruY.Value);
      Unidade        := QrGraGXSemEstqItemUnid.Value;
      //
      case QrGraGXSemEstqGraGruY.Value of
        //CO_GraGruY_1024_GXPatPri: IncluiPri();
        //CO_GraGruY_2048_GXPatSec: IncluiSec();
        //CO_GraGruY_3072_GXPatAce: IncluiAce();
        CO_GraGruY_4096_GXPatApo: //IncluiApo(Codigo, CtrID); // = Apoio
          AppPF.LocCItsMat_IncluiApo(stIns, Codigo, CtrID, GraGruX, ManejoLca,
          PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
          nil(*QrLocIPatApo*), Item);
        //
        CO_GraGruY_5120_GXPatUso: //IncluiUso(Codigo, CtrID); // = Uso (desgaste)
          AppPF.LocCItsMat_IncluiUso(stIns, Codigo, CtrID, GraGruX, ManejoLca, Unidade,
          PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
          nil(*FQrLocIPatUso*), Item);
        //
        CO_GraGruY_6144_GXPrdCns: //IncluiCns(Codigo, CtrID); // = Uso (consumo)
          AppPF.LocCItsMat_IncluiCns(stIns, Codigo, CtrID, GraGruX, ManejoLca, Unidade,
          PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
          nil(*FQrLocIPatCns*), Item);
        else Geral.MensagemBox('Aplica��o n�o definida em inclus�o de materiais (2)!',
        'Aviso', MB_OK+MB_ICONWARNING);
      end;
      //
      QrGraGXSemEstq.Next;
    end;
  end;
end;

procedure TFmLocCItsLca.MostraSituacaoPatrimonio();
var
  Cor: TColor;
begin
  if EdGraGruX.ValueVariant = 0 then
    MyObjects.Informa2(LaSitA1, LaSitA2, False, '? ? ?')
  else
    MyObjects.Informa2(LaSitA1, LaSitA2, False, QrGraGXPatrNO_SIT.Value);
  case QrGraGXPatrSituacao.Value of
    68{D}: Cor := clGreen;
    70{F}: Cor := clBlack;
    73{I}: Cor := clpurple;
    76{L}: Cor := clRed;
    79{O}: Cor := clLaranja;
    82{R}: Cor := clLaranja;
    86{V}: Cor := clGray;
    else Cor := clFuchsia;
  end;
  LaSitA2.Font.Color := Cor;
end;

function TFmLocCItsLca.ObtemQtdeLocacao(): Integer;
begin
  if RGQtdeLocacao.ItemIndex <> 1 then
    ObtemQtdeLocacao := 0
  else
    ObtemQtdeLocacao := 1;
end;

procedure TFmLocCItsLca.PesquisaPorGraGruX();
var
  Reabre: Boolean;
begin
  Reabre := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia, gg1.Patrimonio ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  cpl.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (cpl.GraGruX IS NULL) ',
  'AND ggx.Ativo=1 ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdPatrimonio.ValueVariant <> QrPesq2Patrimonio.Value then
    begin
      EdPatrimonio.ValueVariant := QrPesq2Patrimonio.Value;
      Reabre := True;
    end;
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
    begin
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
      Reabre := True;
    end;
    if Reabre then
      ReopenAgrupado();
  end else
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
  end;
  //
  MostraSituacaoPatrimonio();
end;

procedure TFmLocCItsLca.PesquisaPorNome(Key: Integer);
var
  //Controle
  Nivel1: Integer;
begin
  if Key = VK_F3 then
  begin
    Nivel1 := CuringaLoc.CriaForm('Nivel1', CO_NOME, 'gragru1', Dmod.MyDB, CO_VAZIO);
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT ggx.Controle ',
    'FROM gragrux ggx',
    'WHERE ggx.GraGru1=' + Geral.FF0(Nivel1),
    'AND ggx.Ativo=1 ',
    '']);
    EdGraGruX.ValueVariant := Dmod.QrAux.FieldByName('Controle').AsInteger;
    ReopenAgrupado();
  end;
  //
  MostraSituacaoPatrimonio();
end;

procedure TFmLocCItsLca.PesquisaPorPatrimonio(Limpa: Boolean);
var
  Reabre: Boolean;
begin
  if EdPatrimonio.Text = EmptyStr then
    Exit;
  Reabre := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle, gg1.Referencia, gg1.Patrimonio ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Patrimonio="' + EdPatrimonio.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  'AND ggx.Ativo=1 ',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
      Reabre := True;
    end;
    if EdReferencia.ValueVariant <> QrPesq1Referencia.Value then
    begin
      EdReferencia.ValueVariant := QrPesq1Referencia.Value;
      Reabre := True;
    end;
    if Reabre then
      ReopenAgrupado();
    //  Precisa ?
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
    EdGraGruX.ValueVariant := 0;
    CBGraGruX.KeyValue := 0;
  end else
  begin
    //EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
    EdGraGruX.ValueVariant := 0;
    CBGraGruX.KeyValue := 0;
  end;
  //
  MostraSituacaoPatrimonio();
end;

procedure TFmLocCItsLca.PesquisaPorReferencia(Limpa: Boolean);
var
  Reabre: Boolean;
begin
  if EdReferencia.Text = EmptyStr then
    Exit;
  Reabre := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle, gg1.Referencia, gg1.Patrimonio ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  'AND ggx.Ativo=1 ',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdPatrimonio.ValueVariant <> QrPesq1Patrimonio.Value then
    begin
      EdPatrimonio.ValueVariant := QrPesq1Patrimonio.Value;
      Reabre := True;
    end;
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
      Reabre := True;
    end;
    if Reabre then
      ReopenAgrupado();
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
    EdGraGruX.ValueVariant := 0;
    CBGraGruX.KeyValue := 0;
  end else
  begin
    EdPatrimonio.ValueVariant := '';
    //EdReferencia.ValueVariant := '';
    EdGraGruX.ValueVariant := 0;
    CBGraGruX.KeyValue := 0;
 end;
  //
  MostraSituacaoPatrimonio();
end;

procedure TFmLocCItsLca.QrGraGXPatrAfterOpen(DataSet: TDataSet);
begin
  if QrGraGXPatr.RecordCount > 0 then
    DbEdEstqSdo.DataSource := DsGraGXPatr
  else
    DbEdEstqSdo.DataSource := nil;
end;

procedure TFmLocCItsLca.QrGraGXPatrBeforeClose(DataSet: TDataSet);
begin
  if QrGraGXPatr.RecordCount > 0 then
    DbEdEstqSdo.DataSource := nil;
end;

procedure TFmLocCItsLca.ReopenAgrupado();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgrupado, Dmod.MyDB, [
  'SELECT gg1.Referencia, gg1.Nome ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE ggx.Controle=' + Geral.FF0(QrGraGXPatrAgrupador.Value),
  'AND ggx.Ativo=1 ',
  '']);
  //
end;

procedure TFmLocCItsLca.ReopenGraGXComEstq;
begin
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXComEstq, Dmod.MyDB, [
    'SELECT gg1.Nome, gpi.GraGXOutr, gpi.Quantidade, ggx.GraGruY,',
    'ptr.*',
    'FROM gragxpits gpi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=gpi.GraGXOutr',
    'LEFT JOIN gragxpatr ptr ON ptr.GraGruX=gpi.GraGXOutr',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE gpi.GraGXPatr=' + Geral.FF0(EdGraGruX.ValueVariant),
    'AND NOT ptr.GraGruX IS NULL',
    'AND ggx.Ativo=1 ',
    'ORDER BY ggx.GraGruY ',
    '']);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocCItsLca.ReopenGraGXSemEstq();
begin
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXSemEstq, Dmod.MyDB, [
    'SELECT gg1.Nome, gpi.GraGXOutr, gpi.Quantidade,  ',
    'ggx.GraGruY, otr.ItemValr, otr.ItemUnid ',
    'FROM gragxpits gpi  ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=gpi.GraGXOutr ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragxoutr otr ON otr.GraGruX=gpi.GraGXOutr ',
    'WHERE gpi.GraGXPatr=' + Geral.FF0(EdGraGruX.ValueVariant),
    'AND NOT otr.GraGruX IS NULL',
    'AND ggx.Ativo=1 ',
    'ORDER BY ggx.GraGruY ',
    '']);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocCItsLca.ReopenTabelas();
var
  Filtro: String;
begin
  Dmod.FiltroGrade(gbsLocar, Filtro);
  Filtro := Filtro + ' AND (gg1.Nivel1=ggx.GraGru1)';
  //
  // Sem Filtro: T i s o l i n
  if Dmod.QrOpcoesTRenGraNivOutr.Value = 7 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPatr, Dmod.MyDB, [
    'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL,  ',
    'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1,  ',
    'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE,   ',
    'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,  ',
    'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,  ',
    'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,  ',
    'cpl.Voltagem, cpl.Potencia, cpl.Capacid, ',
    'cpl.ValorFDS, cpl.DMenos, gg1.Patrimonio, gg1.Nivel2 ',
    '    ',
    'FROM gragrux ggx   ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
    'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle   ',
    'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle   ',
    'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo   ',
    'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao ',
    'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle AND gep.Empresa=' + Geral.FF0(FQrCab.FieldByName('Empresa').AsInteger),
    'LEFT JOIN gragruY    ggy ON ggy.Codigo=ggx.GraGruY ',
    'WHERE ggy.Codigo=' + Geral.FF0(CO_GraGruY_1024_GXPatPri),
    'AND cpl.Aplicacao <> 0 ',
    'AND ggx.Ativo = 1',
    ' ',
    'ORDER BY NO_GG1  ',
    '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPatr, Dmod.MyDB, [
    'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL,  ',
    'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1,  ',
    'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE,   ',
    'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,  ',
    'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,  ',
    'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,  ',
    'cpl.Voltagem, cpl.Potencia, cpl.Capacid, ',
    'cpl.ValorFDS, cpl.DMenos, gg1.Patrimonio ',
    '    ',
    'FROM gragrux ggx   ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
    'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle   ',
    'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle   ',
    'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo   ',
    'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao ',
    'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle  AND gep.Empresa=' + Geral.FF0(FQrCab.FieldByName('Empresa').AsInteger),
    'WHERE ' + Filtro,
    'AND cpl.Aplicacao <> 0 ',
    ' ',
    'ORDER BY NO_GG1  ',
    '']);
  end;
  //
  Dmod.ReopenOpcoesTRen;
end;

{
procedure TFmLocCItsLca.ReopenLocCConIts(CtrID: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if CtrID <> 0 then
      FQrIts.Locate('CtrID', CtrID, []);
  end;
end;
}

procedure TFmLocCItsLca.SbRecalculaSaldoClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX > 0 then
  begin
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, FQrCab.FieldByName('Empresa').AsInteger, True);
    UMyMod.AbreQuery(QrGraGXPatr, Dmod.MyDB);
    EdGraGruX.ValueVariant := GraGruX;
    CBGraGruX.KeyValue := GraGruX;
    EdGraGruX.SetFocus;
  end else
    Geral.MB_Aviso('Selecione o equipamento!');
end;

procedure TFmLocCItsLca.SpeedButton1Click(Sender: TObject);
var
  Empresa, GraGruX, Nivel2: Integer;
begin
  Empresa := FQrCab.FieldByName('Empresa').AsInteger;
  GraGruX := EdGraGruX.ValueVariant;
  Nivel2  := QrGraGXPatrNivel2.Value;
  if GraGruX = 0 then
    Geral.MB_Aviso('Selecione o equipamento!')
  else
    GraL_Jan.MostraFormLocCItsLcLocados(Empresa, GraGruX, Nivel2);
end;

procedure TFmLocCItsLca.SbGraGruXClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  GraL_Jan.MostraFormGraGXPatr(EdGraGruX.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrGraGXPatr, VAR_CADASTRO, 'Controle');
  PesquisaPorGraGruX();
  EdPatrimonio.SetFocus;
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmLocCItsLca.SbGraGXPatrClick(Sender: TObject);
var
  Codigo, Cliente, GraGruX: Integer;
begin
  // const
  Cliente := FQrCab.FieldByName('Cliente').AsInteger;
  // var
  Codigo  := 0;
  GraGruX := 0;
  //
  GraL_Jan.MostraFormLocCConPsq(Cliente, Codigo, GraGruX);
  //
  if GraGruX <> 0 then
  begin
    EdGraGruX.ValueVariant := GraGruX;
    CBGraGruX.KeyValue     := GraGruX;
  end;
end;

{
object Label19: TLabel
  Left = 482
  Top = 56
  Width = 88
  Height = 13
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Caption = 'Data / hora sa'#237'da:'
end
object TPSaiDtHr: TdmkEditDateTimePicker
  Left = 482
  Top = 71
  Width = 99
  Height = 21
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Date = 41131.724786689820000000
  Time = 41131.724786689820000000
  TabOrder = 8
  ReadOnly = False
  DefaultEditMask = '!99/99/99;1;_'
  AutoApplyEditMask = True
  UpdType = utYes
  DatePurpose = dmkdpNone
end
object EdSaiDtHr: TdmkEdit
  Left = 584
  Top = 71
  Width = 41
  Height = 21
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  TabOrder = 9
  FormatType = dmktfTime
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfLong
  HoraFormat = dmkhfShort
  Texto = '00:00'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
end
}

{
object Label20: TLabel
  Left = 666
  Top = 72
  Width = 135
  Height = 13
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Caption = 'Data / hora in'#237'cio cobran'#231'a:'
end
object TPCobranIniDtHr: TdmkEditDateTimePicker
  Left = 666
  Top = 87
  Width = 99
  Height = 21
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Date = 41131.724786689820000000
  Time = 41131.724786689820000000
  TabOrder = 10
  ReadOnly = False
  DefaultEditMask = '!99/99/99;1;_'
  AutoApplyEditMask = True
  UpdType = utYes
  DatePurpose = dmkdpNone
end
object EdCobranIniDtHr: TdmkEdit
  Left = 768
  Top = 87
  Width = 41
  Height = 21
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  TabOrder = 11
  FormatType = dmktfTime
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfLong
  HoraFormat = dmkhfShort
  Texto = '00:00'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
end
}
end.
