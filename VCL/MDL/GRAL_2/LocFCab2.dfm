object FmLocFCab2: TFmLocFCab2
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-110 :: Faturamento de Loca'#231#227'o'
  ClientHeight = 448
  ClientWidth = 814
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 814
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 766
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 718
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 303
        Height = 32
        Caption = 'Faturamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 303
        Height = 32
        Caption = 'Faturamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 303
        Height = 32
        Caption = 'Faturamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 48
    Width = 814
    Height = 286
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 814
      Height = 286
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 814
        Height = 64
        Align = alTop
        Caption = ' Dados do cabe'#231'alho:'
        Enabled = False
        TabOrder = 0
        object Label5: TLabel
          Left = 12
          Top = 20
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label4: TLabel
          Left = 72
          Top = 20
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          FocusControl = DBEdCliente
        end
        object Label3: TLabel
          Left = 156
          Top = 20
          Width = 81
          Height = 13
          Caption = 'Nome do Cliente:'
          FocusControl = DBEdNome
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 12
          Top = 36
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = FmLocCConCab.DsLocCConCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdCliente: TDBEdit
          Left = 72
          Top = 36
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'Cliente'
          DataSource = FmLocCConCab.DsLocCConCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object DBEdNome: TDBEdit
          Left = 156
          Top = 36
          Width = 645
          Height = 21
          TabStop = False
          Color = clWhite
          DataField = 'NO_CLIENTE'
          DataSource = FmLocCConCab.DsLocCConCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 64
        Width = 814
        Height = 341
        Align = alTop
        Caption = ' Dados do item (Patrim'#244'nio principal): '
        TabOrder = 1
        object Label6: TLabel
          Left = 12
          Top = 16
          Width = 51
          Height = 13
          Caption = 'ID do item:'
        end
        object Label1: TLabel
          Left = 288
          Top = 16
          Width = 119
          Height = 13
          Caption = 'Condi'#231#227'o de pagamento:'
        end
        object Label21: TLabel
          Left = 92
          Top = 16
          Width = 149
          Height = 13
          Caption = 'Data / hora da loca'#231#227'o / troca:'
        end
        object Label2: TLabel
          Left = 12
          Top = 102
          Width = 54
          Height = 13
          Caption = '$ Loca'#231#227'o:'
        end
        object Label8: TLabel
          Left = 86
          Top = 102
          Width = 56
          Height = 13
          Caption = '$ Consumo:'
        end
        object Label9: TLabel
          Left = 166
          Top = 102
          Width = 31
          Height = 13
          Caption = '$ Uso:'
        end
        object Label7: TLabel
          Left = 12
          Top = 58
          Width = 39
          Height = 13
          Caption = 'Carteira:'
        end
        object Label13: TLabel
          Left = 674
          Top = 16
          Width = 27
          Height = 13
          Caption = 'S'#233'rie:'
        end
        object Label14: TLabel
          Left = 716
          Top = 16
          Width = 57
          Height = 13
          Caption = 'N'#250'mero NF:'
        end
        object SBCondicaoPG: TSpeedButton
          Left = 648
          Top = 32
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBCondicaoPGClick
        end
        object SBCarteira: TSpeedButton
          Left = 387
          Top = 74
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBCarteiraClick
        end
        object SbNFSe: TSpeedButton
          Left = 779
          Top = 32
          Width = 21
          Height = 21
          Caption = '?'
          OnClick = SbNFSeClick
        end
        object Label15: TLabel
          Left = 246
          Top = 102
          Width = 73
          Height = 13
          Caption = '$ Venda Aces.:'
        end
        object Label10: TLabel
          Left = 406
          Top = 102
          Width = 48
          Height = 13
          Caption = '$ Servi'#231'o:'
        end
        object Label11: TLabel
          Left = 566
          Top = 102
          Width = 58
          Height = 13
          Caption = '$ Desconto:'
        end
        object Label12: TLabel
          Left = 726
          Top = 102
          Width = 36
          Height = 13
          Caption = '$ Total:'
        end
        object Label16: TLabel
          Left = 486
          Top = 102
          Width = 37
          Height = 13
          Caption = '$ Bruto:'
        end
        object Label17: TLabel
          Left = 412
          Top = 58
          Width = 127
          Height = 13
          Caption = 'Conta do Plano de Contas:'
        end
        object SbDataHoraFat: TSpeedButton
          Left = 264
          Top = 32
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbDataHoraFatClick
        end
        object SbValDesco: TSpeedButton
          Left = 700
          Top = 118
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbValDescoClick
        end
        object Label18: TLabel
          Left = 326
          Top = 102
          Width = 70
          Height = 13
          Caption = '$ Venda Maq.:'
        end
        object Label19: TLabel
          Left = 646
          Top = 102
          Width = 63
          Height = 13
          Caption = 'M'#225'x % desc.:'
        end
        object EdControle: TdmkEdit
          Left = 12
          Top = 32
          Width = 77
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBCondicaoPG: TdmkDBLookupComboBox
          Left = 346
          Top = 32
          Width = 295
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DmFatura.DsPediPrzCab
          TabOrder = 4
          dmkEditCB = EdCondicaoPG
          QryCampo = 'Cargo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCondicaoPG: TdmkEditCB
          Left = 288
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCondicaoPG
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object TPDataFat: TdmkEditDateTimePicker
          Left = 92
          Top = 32
          Width = 112
          Height = 21
          Date = 41131.000000000000000000
          Time = 0.724786689817847200
          Enabled = False
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataEmi'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdHoraFat: TdmkEdit
          Left = 205
          Top = 32
          Width = 59
          Height = 21
          Enabled = False
          TabOrder = 2
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          QryCampo = 'HoraEmi'
          UpdCampo = 'HoraIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdValLocad: TdmkEdit
          Left = 12
          Top = 118
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValLocadChange
        end
        object EdValConsu: TdmkEdit
          Left = 86
          Top = 118
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValConsuChange
        end
        object EdValUsado: TdmkEdit
          Left = 166
          Top = 118
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValUsadoChange
        end
        object EdCartEmis: TdmkEditCB
          Left = 12
          Top = 74
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCartEmis
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCartEmis: TdmkDBLookupComboBox
          Left = 70
          Top = 74
          Width = 315
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DmFatura.DsCartEmis
          TabOrder = 8
          dmkEditCB = EdCartEmis
          QryCampo = 'Cargo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdSerNF: TdmkEdit
          Left = 674
          Top = 32
          Width = 36
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdNumNF: TdmkEdit
          Left = 716
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdNumNFChange
        end
        object EdValVenda: TdmkEdit
          Left = 246
          Top = 118
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValVendaChange
        end
        object EdValServi: TdmkEdit
          Left = 406
          Top = 118
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 16
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValServiChange
        end
        object EdValDesco: TdmkEdit
          Left = 566
          Top = 118
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 18
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValDescoChange
        end
        object EdValTotal: TdmkEdit
          Left = 726
          Top = 118
          Width = 76
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 19
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdValBruto: TdmkEdit
          Left = 486
          Top = 118
          Width = 76
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 17
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object CGCodHist: TdmkCheckGroup
          Left = 12
          Top = 144
          Width = 785
          Height = 69
          Caption = ' Hist'#243'rico para financeiro e recibo: '
          Columns = 4
          Items.Strings = (
            'Adiantamento de loca'#231#227'o'
            'Valor Parcial de loca'#231#227'o'
            'Renova'#231#227'o de loca'#231#227'o'
            'Quita'#231#227'o de loca'#231#227'o'
            'Frete'
            'Venda de Mercadorias'
            'Servi'#231'os'
            'Outros')
          TabOrder = 20
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
        object EdConta: TdmkEditCB
          Left = 412
          Top = 74
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBConta
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBConta: TdmkDBLookupComboBox
          Left = 470
          Top = 74
          Width = 331
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContas
          TabOrder = 10
          dmkEditCB = EdConta
          QryCampo = 'Cargo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdValVeMaq: TdmkEdit
          Left = 326
          Top = 118
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 15
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValVeMaqChange
        end
        object EdMaxPercDesco: TdmkEdit
          Left = 646
          Top = 118
          Width = 51
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 21
          Visible = False
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValDescoChange
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 334
    Width = 814
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 810
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 378
    Width = 814
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 668
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 666
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object VuCondicaoPG: TdmkValUsu
    dmkEditCB = EdCondicaoPG
    Panel = PnEdita
    QryCampo = 'CondicaoPG'
    UpdCampo = 'CondicaoPG'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 12
    Top = 12
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 396
    Top = 12
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    Left = 496
    Top = 52
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 500
    Top = 104
  end
  object QrValAberto: TMySQLQuery
    Database = Dmod.MyDB
    Left = 608
    Top = 14
    object QrValAbertoAberto: TFloatField
      FieldName = 'Aberto'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrValAbertoVencido: TFloatField
      FieldName = 'Vencido'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object QrCliente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Ativo = 1'
      'ORDER BY NOMEENTIDADE')
    Left = 404
    Top = 68
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrClienteLimiCred: TFloatField
      FieldName = 'LimiCred'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
end
