unit LocFCab2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkDBEdit, dmkValUsu, NFSe_PF_0201,
  UnDmkEnums, UnAppPF, DmkDAC_PF, dmkRadioGroup, UnAppEnums;

type
  TFmLocFCab2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    PnEdita: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdCliente: TDBEdit;
    DBEdNome: TDBEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    Label21: TLabel;
    TPDataFat: TdmkEditDateTimePicker;
    EdHoraFat: TdmkEdit;
    EdValLocad: TdmkEdit;
    Label2: TLabel;
    Label8: TLabel;
    EdValConsu: TdmkEdit;
    EdValUsado: TdmkEdit;
    Label9: TLabel;
    Label7: TLabel;
    EdCartEmis: TdmkEditCB;
    CBCartEmis: TdmkDBLookupComboBox;
    VuCondicaoPG: TdmkValUsu;
    EdNumNF: TdmkEdit;
    Label13: TLabel;
    EdSerNF: TdmkEdit;
    Label14: TLabel;
    SBCondicaoPG: TSpeedButton;
    SBCarteira: TSpeedButton;
    QrLoc: TmySQLQuery;
    SbNFSe: TSpeedButton;
    EdValVenda: TdmkEdit;
    Label15: TLabel;
    EdValServi: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    EdValDesco: TdmkEdit;
    EdValTotal: TdmkEdit;
    Label12: TLabel;
    EdValBruto: TdmkEdit;
    Label16: TLabel;
    CGCodHist: TdmkCheckGroup;
    QrContas: TMySQLQuery;
    DsContas: TDataSource;
    Label17: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    SbDataHoraFat: TSpeedButton;
    QrValAberto: TMySQLQuery;
    QrValAbertoAberto: TFloatField;
    QrValAbertoVencido: TFloatField;
    QrCliente: TMySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    QrClienteLimiCred: TFloatField;
    SbValDesco: TSpeedButton;
    EdValVeMaq: TdmkEdit;
    Label18: TLabel;
    EdMaxPercDesco: TdmkEdit;
    Label19: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdValLocadChange(Sender: TObject);
    procedure EdValConsuChange(Sender: TObject);
    procedure EdValUsadoChange(Sender: TObject);
    procedure EdValServiChange(Sender: TObject);
    procedure EdValDescoChange(Sender: TObject);
    procedure SBCondicaoPGClick(Sender: TObject);
    procedure SBCarteiraClick(Sender: TObject);
    procedure SbNFSeClick(Sender: TObject);
    procedure EdNumNFChange(Sender: TObject);
    procedure EdValVendaChange(Sender: TObject);
    procedure SbDataHoraFatClick(Sender: TObject);
    procedure SbValDescoClick(Sender: TObject);
    procedure EdValVeMaqChange(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValores();
    function  LiberadoFaturar(): Boolean;
  public
    { Public declarations }
    FControle: Integer;
    FTabLcta: String;
    //
    //FQrLoc,
    FQrCab, FQrIts, FQrLocCPatPri: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FFilial, FCliente: Integer;
    FDtHrEmi: TDateTime;
    FEncerra: Boolean;
    FFmLocCConCab: TForm;
    //FDesconsideraMaxDesco: Boolean;
  end;

  var
  FmLocFCab2: TFmLocFCab2;

implementation

uses UnMyObjects, Module, ModuleFatura, UMySQLModule, Principal,
  ModuleGeral, UnFinanceiroJan, UnGFat_Jan, MyDBCheck, LocCConCab, GetValor;

{$R *.DFM}

procedure TFmLocFCab2.BtOKClick(Sender: TObject);
const
  TipoFat = tfatServico;
  FatID = VAR_FATID_3001;
  TipoFatura = TTipoFiscal.tfaMultiSV; // tfatServico; {TTipoFatura}
  FaturaDta = dfEncerramento; {TDataFatura}
  Financeiro = tfinCred; {TTipoFinanceiro}
var
  DtHrFat, DtHrBxa, SerNF: String;
  Codigo, Controle, NumNF, CondicaoPG, CartEmis, Conta: Integer;
  ValLocad, ValConsu, ValUsado, ValVenda, ValServi, ValDesco, ValBruto, ValTotal,
  //ValTotalFat: Double;
  ValorPago, ValVeMaq: Double;
var
  Entidade, FatNum, Cliente, IDDuplicata, NumeroNF, TipoCart, Represen: Integer;
  DataAbriu, DataEncer: TDateTime;
  SerieNF: String;
  CodHist: Integer;
begin
  if not LiberadoFaturar() then
    Exit;
  if FEncerra then
  begin
    if not TFmLocCConCab(FFmLocCConCab).LiberaItensLocacao(FQrLocCPatPri(*, FQrLoc*)) then
      Exit;
  end;
  //
  Codigo     := Geral.IMV(Geral.SoNumero_TT(DBEdCodigo.Text));
  Controle   := EdControle.ValueVariant;
  DtHrFat    := Geral.FDT(Trunc(TPDataFat.Date), 1) + ' ' + EdHoraFat.Text;
  ValLocad   := EdValLocad.ValueVariant;
  ValConsu   := EdValConsu.ValueVariant;
  ValUsado   := EdValUsado.ValueVariant;
  ValVenda   := EdValVenda.ValueVariant;
  ValVeMaq   := EdValVeMaq.ValueVariant;
  ValServi   := EdValServi.ValueVariant;
  ValDesco   := EdValDesco.ValueVariant;
  ValBruto   := EdValBruto.ValueVariant;
  ValTotal   := EdValTotal.ValueVariant;
  SerNF      := EdSerNF.Text;
  NumNF      := EdNumNF.ValueVariant;
  CondicaoPG := EdCondicaoPG.ValueVariant;
  CartEmis   := EdCartEmis.ValueVariant;
  //Conta      := DModG.QrParamsEmpCtaServico.Value;
  Conta      := EdConta.ValueVariant;
  CodHist    := CGCodHist.Value;
  //
  if MyObjects.FIC(CondicaoPG = 0, EdCondicaoPG,
    'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(CartEmis = 0, EdCartEmis,
    'Informe a carteira!') then Exit;
  if MyObjects.FIC(Conta = 0, EdConta,
    'Informe a conta do plano de contas!') then Exit;
  if ValTotal < 0.01 then
  begin
    if FEncerra then
    begin
      if Geral.MB_Pergunta('Valor inv�lido para faturamento!' + sLineBreak +
      'Deseja encerrar assim mesmo?') <> ID_YES then
        Exit;
    end else
    begin
      Geral.MB_Aviso('Valor inv�lido para faturamento!');
      Exit;
    end;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    if ValTotal >= 0.01 then
    begin
      Controle := UMyMod.BPGS1I32('locfcab', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'locfcab', False, [
      'Codigo', 'DtHrFat', 'ValLocad',
      'ValConsu', 'ValUsado', 'ValServi',
      'ValDesco', 'ValTotal', 'SerNF',
      'NumNF', 'CondicaoPG', 'CartEmis',
      'ValVenda', 'ValBruto', 'CodHist',
      'ValVeMaq',
      'Genero'], [
      'Controle'], [
      Codigo, DtHrFat, ValLocad,
      ValConsu, ValUsado, ValServi,
      ValDesco, ValTotal, SerNF,
      NumNF, CondicaoPG, CartEmis,
      ValVenda, ValBruto, CodHist,
      ValVeMaq,
      Conta], [
      Controle], True) then
      begin
        Entidade     := FEmpresa;
        FatNum       := Controle;
        Cliente      := FCliente;
        IDDuplicata  := Controle;
        NumeroNF     := NumNF;
        CartEmis     := CartEmis;
        TipoCart     := DmFatura.QrCartEmisTipo.Value;
        CondicaoPG   := CondicaoPG;
        Represen     := 0;
        DataAbriu    := FDtHrEmi;
        DataEncer    := TPDataFat.Date;
        SerieNF      := SerNF;
        //
        DmFatura.EmiteFaturas(Codigo, FatID, Entidade, FatID, FatNum, Cliente,
          DataAbriu, DataEncer, IDDuplicata, NumeroNF, SerieNF, CartEmis,
          TipoCart,  Conta, CondicaoPG, Represen, TipoFatura, FaturaDta,
          Financeiro, ValTotal,
          // 2020-10-27
          (*Associada*) 0, (*AFP_Sit*)0, (*AFP_Per*)0.00, (*ASS_CtaFaturas*)0,
          (*ASS_IDDuplicata*)0, (*DataFatPreDef*)0, (*AskIns*)False,
          (*ExcluiLct*)True, CodHist);
        end;
    end;
(*
    UnDmkDAC_PF.AbreMySQLQUery0(QrLoc, Dmod.MyDB, [
    'SELECT SUM(lfc.ValBruto) ValBruto ',
    'FROM locfcab lfc ',
    'WHERE lfc.Codigo=' + Geral.FF0(Codigo),
    '']);
    if QrLoc.RecordCount > 0 then
      ValorPago := QrLoc.FieldByName('ValBruto').AsFloat
    else
      ValorPago := 0;
    //

    if FEncerra then
    begin
      DtHrBxa := DtHrFat;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconcab', False, ['ValorPago',
        'DtHrBxa'], ['Codigo'], [ValorPago, DtHrBxa], [Codigo], True);
      //
    end  else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconcab', False, ['ValorPago'
        ], ['Codigo'], [ValorPago], [Codigo], True);
      //
    end;
    //
*)
    //
    AppPF.AtualizaTotaisLocCConCab_Faturados(Codigo, FEncerra, DtHrFat);
    //
    if FQrIts <> nil then
    begin
      UMyMod.AbreQuery(FQrIts, Dmod.MyDB);
      FQrIts.Locate('Controle', Controle, []);
    end;
    //
    FControle := Controle;
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocFCab2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocFCab2.CalculaValores();
var
  ValLocad, ValConsu, ValUsado, ValVenda, ValVeMaq, ValServi, ValDesco, ValBruto,
  ValTotal, MaxValDesco, MaxPercDesco: Double;
begin
  ValLocad := EdValLocad.ValueVariant;
  ValConsu := EdValConsu.ValueVariant;
  ValUsado := EdValUsado.ValueVariant;
  ValVenda := EdValVenda.ValueVariant;
  ValVeMaq := EdValVeMaq.ValueVariant;
  ValServi := EdValServi.ValueVariant;
  ValDesco := EdValDesco.ValueVariant;
  ValBruto := ValLocad + ValConsu + ValUsado + ValVenda + ValVeMaq + ValServi;
  MaxPercDesco := EdMaxPercDesco.ValueVariant;
  MaxValDesco := ValBruto * MaxPercDesco / 100;
  if MaxValDesco >= ValDesco then
  begin
    ValTotal := ValBruto - ValDesco;
    //
    EdValBruto.ValueVariant := ValBruto;
    EdValTotal.ValueVariant := ValTotal;
  end else
  begin
    ValTotal := ValBruto;
    //
    EdValBruto.ValueVariant := ValBruto;
    EdValTotal.ValueVariant := ValTotal;
    EdValDesco.ValueVariant := 0.00;
    ///
    Geral.MB_Aviso('Desconto n�o permitido!')
  end;
end;

procedure TFmLocFCab2.EdNumNFChange(Sender: TObject);
begin
  SbNFSe.Enabled := EdNumNF.ValueVariant = 0;
end;

procedure TFmLocFCab2.EdValConsuChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab2.EdValDescoChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab2.EdValServiChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab2.EdValLocadChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab2.EdValUsadoChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab2.EdValVeMaqChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab2.EdValVendaChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmLocFCab2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocFCab2.FormCreate(Sender: TObject);
const
  Colunas = 4;
  Default = 0;
  FirstAll = False;
begin
  ImgTipo.SQLType := stLok;
  //FDesconsideraMaxDesco = False;
  //
  MyObjects.ConfiguraCheckGroup(CGCodHist, sCodHistTextos, Colunas, Default, FirstAll);
  CGCodHist.Value := 0;
  DmFatura.ReopenPediPrzCab();
  DmFatura.ReopenCartEmis(0, True);
  CBCondicaoPG.ListSource := DmFatura.DsPediPrzCab;
  CBCartEmis.ListSource := DmFatura.DsCartEmis;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrContas, Dmod.MyDB, [
  'SELECT con.Codigo, con.Nome  ',
  'FROM contas con ',
  'WHERE con.Codigo>0 ',
  'AND con.Ativo = 1 ',
  'AND con.Credito = "V" ',
  'AND  ',
  '( ',
  '  Codigo=' + Geral.FF0(DModG.QrParamsEmpCtaProdVen.Value),
  '  OR ',
  '  Codigo=' + Geral.FF0(DModG.QrParamsEmpCtaServico.Value),
  '  OR ',
  '  Codigo=' + Geral.FF0(DModG.QrParamsEmpCtaFretPrest.Value),
  '  OR ',
  '  UsoNoERP=1',
  ') ',
  'ORDER BY con.Nome ',
  '']);
  //
  //EdValDesco.ValMax := Geral.FFT(Dmod.QrOpcoesTRenMaxDescFatAtnd.Value, 2, siNegativo);
  EdMaxPercDesco.ValueVariant := Dmod.QrOpcoesTRenMaxDescFatAtnd.Value;
  //
end;

procedure TFmLocFCab2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmLocFCab2.LiberadoFaturar(): Boolean;
var
  Cliente(*, CliInt*): Integer;
  TabLctA: String;
  AbertoFuturo: Double;
begin
  // se escolheu prazo ent�o verifica se deve
  if DmFatura.QrPediPrzCabMedDDSimpl.Value >= 1 then
  begin
    Cliente := TFmLocCConCab(FFmLocCConCab).QrLocCConCabCliente.Value;
    //CliInt  := FmLocCConCab.QrLocCConCab.Value;
    //
    TabLctA := FTabLcta;
    //TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, 0);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrValAberto, Dmod.MyDB, [
    'SELECT Cliente, SUM(Credito) Aberto, ',
    'SUM(IF(Vencimento < SYSDATE(), Credito, 0)) Vencido ',
    'FROM ' +  FTabLctA,
    'WHERE Cliente=' + Geral.FF0(Cliente),
    'AND Compensado < "1900-01-01" ',
    '']);
    if QrValAbertoVencido.Value > 0 then
    begin
      Result := DBCheck.LiberaPelaSenhaPwdLibFunc('Cliente com valor vencido: ' +
        Geral.FFT(QrValAbertoVencido.Value, 2, siNegativo));
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCliente, Dmod.MyDB, [
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, ',
      'Nome) NOMEENTIDADE, LimiCred ',
      'FROM entidades ',
      'WHERE Codigo=' + Geral.FF0(Cliente),
      'ORDER BY NOMEENTIDADE ',
      '']);
      AbertoFuturo := QrValAbertoAberto.Value + EdValTotal.ValueVariant;
      if (AbertoFuturo > QrClienteLimiCred.Value) then
        Result := DBCheck.LiberaPelaSenhaPwdLibFunc(
        'Cliente ficar� com valor em aberto: ' +
        Geral.FFT(AbertoFuturo, 2, siNegativo) +
        ' maior que o limite de cr�dito: ' +
        Geral.FFT(QrClienteLimiCred.Value, 2, siNegativo))
      else
        Result := True;
    end;
  end else
    Result := True;
end;

procedure TFmLocFCab2.SBCarteiraClick(Sender: TObject);
begin
  FinanceiroJan.InsereEDefineCarteira(EdCartEmis.ValueVariant, EdCartEmis,
  CBCartEmis, DmFatura.QrCartEmis);
end;

procedure TFmLocFCab2.SBCondicaoPGClick(Sender: TObject);
begin
  GFat_Jan.InsereEDefinePediPrzCab(EdCondicaoPG.ValueVariant, EdCondicaoPG,
  CBCondicaoPG, DmFatura.QrPediPrzCab);
end;

procedure TFmLocFCab2.SbDataHoraFatClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then Exit;
  //
  TPDataFat.Enabled := True;
  EdHoraFat.Enabled := True;
end;

procedure TFmLocFCab2.SbNFSeClick(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  //Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
var
  Prestador, Tomador: Integer;
  Valor: Double;
  SerieNF: String;
  NumNF: Integer;
begin
  Prestador := FFilial; //DmodG.QrFiliLogFilial.Value;
  Tomador   := FCliente;
  Valor     := EdValTotal.ValueVariant;
  //
  UnNFSe_PF_0201.MostraFormNFSe(SQLType, Prestador, Tomador, Intermediario,
    MeuServico, ItemListSrv, Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico,
    nil, Valor, SerieNF, NumNF, nil);
  //
  if NumNF <> 0  then
  begin
    EdSerNF.ValueVariant := SerieNF;
    EdNumNF.ValueVariant := NumNF;
  end;
end;

procedure TFmLocFCab2.SbValDescoClick(Sender: TObject);
  function ObtemDesconto(var Preco: Double): Boolean;
  var
    ResVar: Variant;
    CasasDecimais: Integer;
  begin
    Result        := False;
    CasasDecimais := 4;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    0, CasasDecimais, 0, '', '', True, 'Desconto', 'Informe o desconto: ',
    0, ResVar) then
    begin
      Preco := Geral.DMV(ResVar);
      Result := True;
    end;
  end;
var
  MaxPercDesco: Double;
begin
  if DBCheck.LiberaPelaSenhaPwdLibFunc() then
  begin
    MaxPercDesco := EdMaxPercDesco.ValueVariant;
    if ObtemDesconto(MaxPercDesco) then
    begin
      //FDesconsideraMaxDesco = False;
      EdMaxPercDesco.Visible := True;
      EdMaxPercDesco.ValueVariant := MaxPercDesco;
    end;
  end;
end;

end.
