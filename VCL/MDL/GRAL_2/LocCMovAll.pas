unit LocCMovAll;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEdit, dmkPermissoes, dmkDBGridZTO,
  mySQLDbTables, UnDmkEnums, UnAppEnums, Vcl.Menus, MyDBCheck;

type
  TFmLocCMovAll = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtDevolucao: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel5: TPanel;
    EdNO_ManejoLca: TdmkEdit;
    Label1: TLabel;
    EdGraGruX: TdmkEdit;
    Label2: TLabel;
    EdNO_GraGruX: TdmkEdit;
    QrLocCMovAll: TMySQLQuery;
    QrLocCMovAllCodigo: TIntegerField;
    QrLocCMovAllGTRTab: TIntegerField;
    QrLocCMovAllCtrID: TIntegerField;
    QrLocCMovAllItem: TIntegerField;
    QrLocCMovAllControle: TIntegerField;
    QrLocCMovAllSEQUENCIA: TIntegerField;
    QrLocCMovAllDataHora: TDateTimeField;
    QrLocCMovAllUSUARIO: TWideStringField;
    QrLocCMovAllGraGruX: TIntegerField;
    QrLocCMovAllTipoES: TWideStringField;
    QrLocCMovAllQuantidade: TIntegerField;
    QrLocCMovAllTipoMotiv: TSmallintField;
    QrLocCMovAllCOBRANCALOCACAO: TWideStringField;
    QrLocCMovAllVALORLOCACAO: TFloatField;
    QrLocCMovAllLIMPO: TWideStringField;
    QrLocCMovAllSUJO: TWideStringField;
    QrLocCMovAllQUEBRADO: TWideStringField;
    QrLocCMovAllTESTADODEVOLUCAO: TWideStringField;
    QrLocCMovAllGGXEntrada: TLargeintField;
    QrLocCMovAllMotivoTroca: TIntegerField;
    QrLocCMovAllObservacaoTroca: TWideStringField;
    QrLocCMovAllQuantidadeLocada: TIntegerField;
    QrLocCMovAllQuantidadeJaDevolvida: TIntegerField;
    QrLocCMovAllLk: TIntegerField;
    QrLocCMovAllDataCad: TDateField;
    QrLocCMovAllDataAlt: TDateField;
    QrLocCMovAllUserCad: TIntegerField;
    QrLocCMovAllUserAlt: TIntegerField;
    QrLocCMovAllAlterWeb: TSmallintField;
    QrLocCMovAllAWServerID: TIntegerField;
    QrLocCMovAllAWStatSinc: TSmallintField;
    QrLocCMovAllAtivo: TSmallintField;
    QrLocCMovAllNO_ManejoLca: TWideStringField;
    DsLocCMovAll: TDataSource;
    BtSubstituicao: TBitBtn;
    Label3: TLabel;
    EdEmpresa: TdmkEdit;
    PMDevolucao: TPopupMenu;
    IncluiDevoluo1: TMenuItem;
    AlteraDevoluo1: TMenuItem;
    ExcluiDevoluo1: TMenuItem;
    PMSubstituicao: TPopupMenu;
    IncluiSubstituio1: TMenuItem;
    AlteraSubstituio1: TMenuItem;
    ExcluiSubstituio1: TMenuItem;
    QrLocCMovAllNO_TipoMotiv: TWideStringField;
    QrLocCMovAllSubstitutoLca: TIntegerField;
    QrLocCMovAllNO_MotivoTroca: TWideStringField;
    N1: TMenuItem;
    Recalcula1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure IncluiDevoluo1Click(Sender: TObject);
    procedure AlteraDevoluo1Click(Sender: TObject);
    procedure ExcluiDevoluo1Click(Sender: TObject);
    procedure BtDevolucaoClick(Sender: TObject);
    procedure PMDevolucaoPopup(Sender: TObject);
    procedure BtSubstituicaoClick(Sender: TObject);
    procedure IncluiSubstituio1Click(Sender: TObject);
    procedure AlteraSubstituio1Click(Sender: TObject);
    procedure PMSubstituicaoPopup(Sender: TObject);
    procedure ExcluiSubstituio1Click(Sender: TObject);
    procedure Recalcula1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FGtrTab: TGraToolRent;
    FCodigo, FCtrID, FItem, FManejoLca: Integer;
    FRecalcular: Boolean;
    //
    FTipoTool: TGraGXToolRnt;
    FGraGruY: Integer;
    FTipoAluguel: String;
    FQrLocIPatPri, FQrLocIPatSec, FQrLocIPatAce,
    FQrLocIPatApo, FQrLocIPatCns, FQrLocIPatUso: TmySQLQuery;
    //
    FDtHrDevolver: TDateTime;
    //
    procedure ReopenLocCMovAll();
    procedure InsAltMovimento(SQLType: TSQLType; TipoMotiv: TTipoMotivLocMov);
    procedure InsAltSubstituicao(SQLType: TSQLType; TipoMotiv: TTipoMotivLocMov);
  end;

  var
  FmLocCMovAll: TFmLocCMovAll;

implementation

uses UnMyObjects, UnDmkProcFunc, Module, DmkDAC_PF, UnGraL_Jan, UMySQLModule,
  UnAppPF, UnGOTOy;

{$R *.DFM}

procedure TFmLocCMovAll.AlteraDevoluo1Click(Sender: TObject);
begin
  InsAltMovimento(stUpd, TTipoMotivLocMov(QrLocCMovAllTipoMotiv.Value));
end;

procedure TFmLocCMovAll.AlteraSubstituio1Click(Sender: TObject);
begin
//  InsAltSubstituicao(stIns, TTipoMotivLocMov.tmlmTrocaSaida);
end;

procedure TFmLocCMovAll.BtSubstituicaoClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMSubstituicao, BtSubstituicao);
end;

procedure TFmLocCMovAll.BtDevolucaoClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMDevolucao, BtDevolucao);
end;

procedure TFmLocCMovAll.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCMovAll.ExcluiDevoluo1Click(Sender: TObject);
var
  GraGruX, Controle: Integer;
begin
  if not DBCheck.LiberaPelaSenhaPwdLibFunc() then Exit;
  //
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do movimento selecionado?',
  'loccmovall', 'Controle', QrLocCMovAllControle.Value, Dmod.MyDB) = ID_YES then
  begin
    FRecalcular := True;
    GraGruX     := QrLocCMovAllGraGruX.Value;
    AppPF.AtualizaValDevolParci(FCodigo, Integer(FGTRTab), FCtrID, FItem);
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, EdEmpresa.ValueVariant, True);
    Controle := GOTOy.LocalizaPriorNextIntQr(QrLocCMovAll, QrLocCMovAllControle,
    QrLocCMovAllControle.Value);
    ReopenLocCMovAll();
    QrLocCMovAll.Locate('Controle', Controle, []);
  end;
end;

procedure TFmLocCMovAll.ExcluiSubstituio1Click(Sender: TObject);
var
  GGXOri, GGXDst, Controle, SubstitutoLca: Integer;
  Tabela: String;
begin
  GGXOri := QrLocCMovAllGraGruX.Value;
  if Geral.MB_Pergunta('Confirma a exclus�o da substitui��o selecionada?') =
  IDYES then
  begin
    if AppPF.ObtemTabelaDeGraGruY_Movimento(FGraGruY, Tabela) then
    begin
      SubstitutoLca := QrLocCMovAllSubstitutoLca.Value;
      UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT QtdeDevolucao, GraGruX ',
      'FROM loccitslca ',
      'WHERE Item=' + Geral.FF0(SubstitutoLca),
      '']);
      if Dmod.QrAux.FieldByName('QtdeDevolucao').AsInteger > 0 then
      begin
        Geral.MB_Aviso(
        'Esta substitui��o n�o pode ser desfeita pois o item substituto j� tem devolu��o!');
        Exit;
      end;
      GGXDst := Dmod.QrAux.FieldByName('GraGruX').AsInteger;
      //
      //Al�m de excluir par, fazer update parazerar o substituto no substitu�do!
      Dmod.MyDB.Execute(Geral.ATS([
      'DELETE FROM ' + Tabela + ' WHERE Item=' + Geral.FF0(SubstitutoLca) + ';',
      'DELETE FROM loccmovall WHERE Controle=' + Geral.FF0(QrLocCMovAllControle.Value) + ';',
      'UPDATE ' + Tabela + ' SET SubstitutoLca=0, Troca=0 WHERE Item=' + Geral.FF0(QrLocCMovAllItem.Value) + ';',
      '']));
      //
      FRecalcular := True;
      AppPF.AtualizaValDevolParci(FCodigo, Integer(FGTRTab), FCtrID, FItem);
      AppPF.AtualizaEstoqueGraGXPatr(GGXOri, EdEmpresa.ValueVariant, True);
      AppPF.AtualizaEstoqueGraGXPatr(GGXDst, EdEmpresa.ValueVariant, True);
      Controle := GOTOy.LocalizaPriorNextIntQr(QrLocCMovAll, QrLocCMovAllControle,
      QrLocCMovAllControle.Value);
      ReopenLocCMovAll();
      QrLocCMovAll.Locate('Controle', Controle, []);
    end;
  end;
end;

procedure TFmLocCMovAll.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCMovAll.FormCreate(Sender: TObject);
begin
  FRecalcular := False;
  ImgTipo.SQLType := stLok;
end;

procedure TFmLocCMovAll.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCMovAll.IncluiDevoluo1Click(Sender: TObject);
begin
  InsAltMovimento(stIns, TTipoMotivLocMov.tmlmLocarVolta);
end;

procedure TFmLocCMovAll.IncluiSubstituio1Click(Sender: TObject);
begin
  InsAltSubstituicao(stIns, TTipoMotivLocMov.tmlmTrocaEntra);
end;

procedure TFmLocCMovAll.InsAltMovimento(SQLType: TSQLType; TipoMotiv: TTipoMotivLocMov);
var
  Codigo, CtrID, Item, AtuControle, NewControle, Empresa: Integer;
  Recalcular: Boolean;
begin
  Codigo       := FCodigo;
  Empresa      := EdEmpresa.ValueVariant;
  CtrID        := FCtrID;
  Item         := FItem;
  if SQLType = stIns then
    AtuControle := 0
  else
    AtuControle := QrLocCMovAllControle.Value;
  NewControle := 0;

  //
  GraL_Jan.MostraFormLocCItsRet(SQLType, Codigo, CtrID, Item, AtuControle,
  Empresa, TipoMotiv, FDtHrDevolver, NewControle, Recalcular);
  if Recalcular then
    FRecalcular := True;
  //
  ReopenLocCMovAll();
  QrLocCMovAll.Locate('Controle', NewControle, []);
end;

procedure TFmLocCMovAll.InsAltSubstituicao(SQLType: TSQLType;
  TipoMotiv: TTipoMotivLocMov);
var
  Codigo, CtrID, Item, AtuControle, NewControle, Empresa: Integer;
  Recalcular: Boolean;
begin
  Codigo     := FCodigo;
  Empresa    := EdEmpresa.ValueVariant;
  CtrID      := FCtrID;
  Item       := FItem;
  if SQLType = stIns then
    AtuControle := 0
  else
    AtuControle := QrLocCMovAllControle.Value;
  NewControle := 0;
  //
  GraL_Jan.MostraFormLocCItsSubs(SQLType, Codigo, CtrID, Item, AtuControle,
  Empresa, TipoMotiv,

  FTipoTool, FGraGruY, FTipoAluguel,
  FQrLocIPatPri, FQrLocIPatSec, FQrLocIPatAce,
  FQrLocIPatApo, FQrLocIPatCns, FQrLocIPatUso,

  FDtHrDevolver,

  NewControle, Recalcular);
  if Recalcular then
    FRecalcular := True;
  //
  ReopenLocCMovAll();
  QrLocCMovAll.Locate('Controle', NewControle, []);
end;

procedure TFmLocCMovAll.PMDevolucaoPopup(Sender: TObject);
begin
  IncluiDevoluo1.Enabled := True;
  MyObjects.HabilitaMenuItemItsUpd(AlteraDevoluo1, QrLocCMovAll,
    TTipoMotivLocMov(QrLocCMovAllTipoMotiv.Value) = TTipoMotivLocMov.tmlmLocarVolta);
  MyObjects.HabilitaMenuItemItsDel(ExcluiDevoluo1, QrLocCMovAll,
    TTipoMotivLocMov(QrLocCMovAllTipoMotiv.Value) = TTipoMotivLocMov.tmlmLocarVolta);
end;

procedure TFmLocCMovAll.PMSubstituicaoPopup(Sender: TObject);
begin
  IncluiSubstituio1.Enabled := True;
  MyObjects.HabilitaMenuItemItsUpd(AlteraSubstituio1, QrLocCMovAll,
    TTipoMotivLocMov(QrLocCMovAllTipoMotiv.Value) = TTipoMotivLocMov.tmlmTrocaEntra);
  MyObjects.HabilitaMenuItemItsDel(ExcluiSubstituio1, QrLocCMovAll,
    TTipoMotivLocMov(QrLocCMovAllTipoMotiv.Value) = TTipoMotivLocMov.tmlmTrocaEntra);

end;

procedure TFmLocCMovAll.Recalcula1Click(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := QrLocCMovAllGraGruX.Value;
  //
  AppPF.AtualizaValDevolParci(FCodigo, Integer(FGTRTab), FCtrID, FItem);
  AppPF.AtualizaEstoqueGraGXPatr(GraGruX, EdEmpresa.ValueVariant, True);
  //
  FRecalcular  := True;
end;

procedure TFmLocCMovAll.ReopenLocCMovAll();

var
  //SQL_ManejoLca,
  SQL_TipoMotivLocMov: String;
begin
(*  SQL_ManejoLca := dmkPF.ArrayToTexto('lma.ManejoLca', 'NO_ManejoLca',
    TPosVirgula.pv?, True, sManejoLca);
*)
  SQL_TipoMotivLocMov := dmkPF.SQL_ELT('lma.TipoMotiv', 'NO_TipoMotiv',
    TPosVirgula.pvNo, True, sTipoMotivLocMov);
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCMovAll, Dmod.MyDB, [
  'SELECT tmc.Nome NO_MotivoTroca, lma.*, ',
  SQL_TipoMotivLocMov,
  //SQL_ManejoLca,
  'FROM loccmovall lma ',
  'LEFT JOIN loctromotcad tmc ON tmc.Codigo=lma.MotivoTroca ',
  'WHERE lma.Codigo=' + Geral.FF0(FCodigo),
  'AND lma.GTRTab=' + Geral.FF0(Integer(FGtrTab)),
  'AND lma.CtrID=' + Geral.FF0(FCtrID),
  'AND lma.Item=' + Geral.FF0(FItem),
  '']);
  //
(*
SELECT tmc.Nome NO_MotivoTroca, lma.*,
 ELT(lma.TipoMotiv + 1,"Indefinido!","Retirado por Loca��o","Devolvido e substitu�do","Substituiu devolvido","Devolu��o de Loca��o") NO_TipoMotiv
FROM loccmovall lma
LEFT JOIN loctromotcad tmc ON tmc.Codigo=lma.MotivoTroca
*)
  //Geral.MB_Teste(QrLocCMovAll.SQL.Text);
end;

end.
