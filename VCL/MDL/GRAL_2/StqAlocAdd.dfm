object FmStqAlocAdd: TFmStqAlocAdd
  Left = 339
  Top = 185
  Caption = 'ALO-CACAO-002 :: Aloca'#231#227'o para Loca'#231#227'o'
  ClientHeight = 547
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 285
        Height = 32
        Caption = 'Aloca'#231#227'o para Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 285
        Height = 32
        Caption = 'Aloca'#231#227'o para Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 285
        Height = 32
        Caption = 'Aloca'#231#227'o para Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 433
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 477
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 385
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object GBDados: TGroupBox
      Left = 0
      Top = 69
      Width = 1008
      Height = 316
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = ' Dados do item (Patrim'#244'nio principal): '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Visible = False
      object Label6: TLabel
        Left = 156
        Top = 16
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID do item:'
        Enabled = False
      end
      object Label15: TLabel
        Left = 16
        Top = 248
        Width = 58
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Quantidade:'
      end
      object Label16: TLabel
        Left = 12
        Top = 292
        Width = 159
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Estoque dispon'#237'vel para loca'#231#227'o:'
        Enabled = False
      end
      object SbRecalculaSaldo: TSpeedButton
        Left = 444
        Top = 27
        Width = 85
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Recalcula saldo'
        Enabled = False
        OnClick = SbRecalculaSaldoClick
      end
      object Label3: TLabel
        Left = 12
        Top = 16
        Width = 44
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
        Enabled = False
      end
      object Label4: TLabel
        Left = 292
        Top = 292
        Width = 150
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Estoque dispon'#237'vel para alocar:'
        Enabled = False
      end
      object Label5: TLabel
        Left = 72
        Top = 16
        Width = 63
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Centro estq.::'
        Enabled = False
      end
      object Label8: TLabel
        Left = 216
        Top = 16
        Width = 40
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID estq,:'
        Enabled = False
      end
      object Label9: TLabel
        Left = 116
        Top = 248
        Width = 53
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Custo total:'
      end
      object EdControle: TdmkEdit
        Left = 156
        Top = 31
        Width = 56
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdQtde: TdmkEdit
        Left = 16
        Top = 263
        Width = 96
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdQtdeRedefinido
      end
      object DbEdEstqSdo: TDBEdit
        Left = 184
        Top = 287
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'EstqSdo'
        DataSource = DsGraGXPatr
        TabOrder = 4
      end
      object EdEmpresa: TdmkEdit
        Left = 12
        Top = 31
        Width = 56
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '-11'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = -11
        ValWarn = False
        OnRedefinido = EdEmpresaRedefinido
      end
      object DBEdQtde: TDBEdit
        Left = 449
        Top = 287
        Width = 80
        Height = 21
        TabStop = False
        DataField = 'Qtde'
        DataSource = DsEstoque
        TabOrder = 6
      end
      object EdStqCenCad: TdmkEdit
        Left = 72
        Top = 31
        Width = 81
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
        OnRedefinido = EdStqCenCadRedefinido
      end
      object EdIDCtrl: TdmkEdit
        Left = 216
        Top = 31
        Width = 56
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCustoAll: TdmkEdit
        Left = 116
        Top = 263
        Width = 96
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object GroupBox1: TGroupBox
        Left = 12
        Top = 56
        Width = 521
        Height = 95
        Caption = '  Patrim'#244'nio de loca'#231#227'o: '
        TabOrder = 8
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 517
          Height = 78
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label7: TLabel
            Left = 13
            Top = 40
            Width = 51
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o:'
          end
          object SbGGX_Aloc: TSpeedButton
            Left = 488
            Top = 15
            Width = 21
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SbGGX_AlocClick
          end
          object Label19: TLabel
            Left = 208
            Top = 0
            Width = 55
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Refer'#234'ncia:'
          end
          object Label2: TLabel
            Left = 76
            Top = 0
            Width = 52
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Patrim'#244'nio:'
          end
          object Label1: TLabel
            Left = 14
            Top = 0
            Width = 48
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Reduzido:'
          end
          object CbGGX_Aloc: TdmkDBLookupComboBox
            Left = 13
            Top = 55
            Width = 496
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Controle'
            ListField = 'NO_GG1'
            ListSource = DsGraGXPatr
            TabOrder = 3
            dmkEditCB = EdGGX_Aloc
            UpdType = utYes
            LocF7SQLText.Strings = (
              'SELECT lpc.GraGruX _CODIGO, gg1.REFERENCIA, gg1.Nome _NOME'
              'FROM gragxpatr lpc'
              'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX'
              'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
              'WHERE gg1.Nome LIKE "%$#%"'
              'OR gg1.Referencia LIKE "%$#%"'
              'OR gg1.Patrimonio LIKE "%$#%"'
              '')
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdRefe_Aloc: TdmkEdit
            Left = 208
            Top = 15
            Width = 277
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdRefe_AlocChange
            OnKeyDown = EdRefe_AlocKeyDown
          end
          object EdGGX_Aloc: TdmkEditCB
            Left = 14
            Top = 15
            Width = 56
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdGGX_AlocChange
            OnRedefinido = EdGGX_AlocRedefinido
            DBLookupComboBox = CbGGX_Aloc
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdPatrimonio: TdmkEdit
            Left = 76
            Top = 15
            Width = 129
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdPatrimonioChange
            OnKeyDown = EdPatrimonioKeyDown
          end
        end
      end
      object GBVenda: TGroupBox
        Left = 12
        Top = 152
        Width = 521
        Height = 95
        Caption = ' Mercadoria de venda: '
        TabOrder = 9
        Visible = False
        object Panel00A: TPanel
          Left = 2
          Top = 15
          Width = 517
          Height = 78
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label10: TLabel
            Left = 13
            Top = 40
            Width = 51
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o:'
          end
          object SbGGX_SMIA: TSpeedButton
            Left = 488
            Top = 15
            Width = 21
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SbGGX_SMIAClick
          end
          object Label11: TLabel
            Left = 276
            Top = 0
            Width = 55
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Refer'#234'ncia:'
          end
          object Label13: TLabel
            Left = 14
            Top = 0
            Width = 48
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Reduzido:'
          end
          object Label12: TLabel
            Left = 76
            Top = -1
            Width = 40
            Height = 13
            Caption = 'EAN 13:'
          end
          object CBGGX_SMIA: TdmkDBLookupComboBox
            Left = 13
            Top = 55
            Width = 496
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Controle'
            ListField = 'NO_GG1'
            ListSource = DsGraGXVend
            TabOrder = 3
            dmkEditCB = EdGGX_SMIA
            UpdType = utYes
            LocF7SQLText.Strings = (
              'SELECT lpc.GraGruX _CODIGO, gg1.REFERENCIA, gg1.Nome _NOME'
              'FROM gragxpatr lpc'
              'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX'
              'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
              'WHERE gg1.Nome LIKE "%$#%"'
              'OR gg1.Referencia LIKE "%$#%"'
              'OR gg1.Patrimonio LIKE "%$#%"'
              '')
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdRefe_SMIA: TdmkEdit
            Left = 276
            Top = 15
            Width = 209
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdRefe_SMIAChange
            OnKeyDown = EdRefe_SMIAKeyDown
          end
          object EdGGX_SMIA: TdmkEditCB
            Left = 14
            Top = 15
            Width = 56
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdGGX_SMIAChange
            OnRedefinido = EdGGX_SMIARedefinido
            DBLookupComboBox = CBGGX_SMIA
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdEAN13: TdmkEdit
            Left = 76
            Top = 15
            Width = 195
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdEAN13Change
            OnKeyDown = EdEAN13KeyDown
            OnRedefinido = EdEAN13Redefinido
          end
        end
      end
      object MeHistorico: TdmkMemo
        Left = 536
        Top = 12
        Width = 461
        Height = 297
        TabOrder = 10
        QryCampo = 'Historico'
        UpdCampo = 'Historico'
        UpdType = utYes
      end
    end
    object RGFatID: TRadioGroup
      Left = 0
      Top = 0
      Width = 1008
      Height = 69
      Align = alTop
      Caption = ' Movimento de estoque: '
      Columns = 2
      Items.Strings = (
        'Transfer'#234'ncia do estoque geral para o estoque de loca'#231#227'o'
        'Devolu'#231#227'o da aloca'#231#227'o para o estoque geral'
        'Entrada para loca'#231#227'o sem baixa do estoque geral'
        'Baixa da loca'#231#227'o sem devolu'#231#227'o ao estoque geral')
      TabOrder = 1
      OnClick = RGFatIDClick
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 16
    Top = 65515
  end
  object QrStqaLocIts: TMySQLQuery
    Database = Dmod.DBAnt
    SQL.Strings = (
      'SELECT * '
      'FROM stqalocits')
    Left = 368
    Top = 32
  end
  object DsStqaLocIts: TDataSource
    DataSet = QrStqaLocIts
    Left = 368
    Top = 88
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL,'
      'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1,'
      
        'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXIS' +
        'TE,'
      'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,'
      'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,'
      'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,'
      'cpl.Voltagem, cpl.Potencia, cpl.Capacid'
      ''
      'FROM gragrux ggx'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle'
      'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle'
      'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo'
      'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao'
      'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle'
      'WHERE  gg1.PrdGrupTip=1 AND (gg1.Nivel1=ggx.GraGru1)'
      'AND cpl.Aplicacao <> 0'
      ''
      'ORDER BY NO_GG1'
      '')
    Left = 296
    Top = 108
    object QrGraGXPatrEstqSdo: TFloatField
      FieldName = 'EstqSdo'
    end
    object QrGraGXPatrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXPatrReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXPatrCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
    end
    object QrGraGXPatrNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXPatrCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
      Required = True
    end
    object QrGraGXPatrSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrGraGXPatrAtualValr: TFloatField
      FieldName = 'AtualValr'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorMes: TFloatField
      FieldName = 'ValorMes'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorQui: TFloatField
      FieldName = 'ValorQui'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorSem: TFloatField
      FieldName = 'ValorSem'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorDia: TFloatField
      FieldName = 'ValorDia'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrAgrupador: TIntegerField
      FieldName = 'Agrupador'
    end
    object QrGraGXPatrMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraGXPatrModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 60
    end
    object QrGraGXPatrSerie: TWideStringField
      FieldName = 'Serie'
      Size = 60
    end
    object QrGraGXPatrVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Size = 30
    end
    object QrGraGXPatrPotencia: TWideStringField
      FieldName = 'Potencia'
      Size = 30
    end
    object QrGraGXPatrCapacid: TWideStringField
      FieldName = 'Capacid'
      Size = 30
    end
    object QrGraGXPatrTXT_MES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_MES'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_QUI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_QUI'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_SEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_SEM'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_DIA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DIA'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrNO_SIT: TWideStringField
      FieldName = 'NO_SIT'
      Size = 30
    end
    object QrGraGXPatrNO_SITAPL: TIntegerField
      FieldName = 'NO_SITAPL'
    end
    object QrGraGXPatrValorFDS: TFloatField
      FieldName = 'ValorFDS'
    end
    object QrGraGXPatrDMenos: TIntegerField
      FieldName = 'DMenos'
    end
    object QrGraGXPatrPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 60
    end
    object QrGraGXPatrNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
  end
  object DsGraGXPatr: TDataSource
    DataSet = QrGraGXPatr
    Left = 296
    Top = 156
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  gg1.Referencia=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 445
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrPesq1Patrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 255
    end
    object QrPesq1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  cpl.GraGruX=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 441
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrPesq2Patrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 255
    end
  end
  object QrCus: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smia.Qtde * smia.Baixa) Qtde '
      'FROM stqmovitsa smia'
      'WHERE smia.Ativo=1 '
      'AND smia.ValiStq <> 101'
      'AND smia.GraGruX>0'
      'AND smia.Empresa=-11'
      'AND smia.StqCenCad=1'
      'AND smia.GraGruX IN ('
      '  SELECT GROUP_CONCAT(vnd.GraGruX) FROM '
      '  gragxpatr vnd'
      ')')
    Left = 417
    Top = 252
    object QrCusQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrCusCustoAll: TFloatField
      FieldName = 'CustoAll'
    end
    object QrCusCustoUni: TFloatField
      FieldName = 'CustoUni'
    end
  end
  object QrGraGXVend: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, ggx.GraGru1,'
      'gg1.Referencia, gg1.Nome NO_GG1,  gg1.NCM, gg1.UnidMed,'
      'ggo.GraGruX, ggo.ItemValr, ggo.ItemUnid'
      'FROM gragxvend ggo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggo.Aplicacao=:P0'
      'ORDER BY NO_GG1'
      '')
    Left = 300
    Top = 214
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGXVendControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXVendReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXVendNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXVendGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGXVendItemValr: TFloatField
      FieldName = 'ItemValr'
    end
    object QrGraGXVendItemUnid: TIntegerField
      FieldName = 'ItemUnid'
    end
    object QrGraGXVendGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGXVendNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGXVendMaxPercDesco: TFloatField
      FieldName = 'MaxPercDesco'
    end
    object QrGraGXVendUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGXVendEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
  end
  object DsGraGXVend: TDataSource
    DataSet = QrGraGXVend
    Left = 300
    Top = 262
  end
  object QrEstoque: TMySQLQuery
    Database = Dmod.MyDB
    Left = 176
    Top = 168
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
  end
  object DsEstoque: TDataSource
    DataSet = QrEstoque
    Left = 176
    Top = 216
  end
  object QrPesq4: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  cpl.GraGruX=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 440
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq4Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrPesq4EAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
  end
  object QrPesq3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  gg1.Referencia=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 444
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq3Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesq3EAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
  end
  object QrSum: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smia.Qtde * smia.Baixa) Qtde '
      'FROM stqmovitsa smia'
      'WHERE smia.Ativo=1 '
      'AND smia.ValiStq <> 101'
      'AND smia.GraGruX>0'
      'AND smia.Empresa=-11'
      'AND smia.StqCenCad=1'
      'AND smia.GraGruX IN ('
      '  SELECT GROUP_CONCAT(vnd.GraGruX) FROM '
      '  gragxpatr vnd'
      ')')
    Left = 357
    Top = 360
    object QrSumQtdeEnt: TFloatField
      FieldName = 'QtdeEnt'
    end
    object QrSumQtdeSai: TFloatField
      FieldName = 'QtdeSai'
    end
  end
end
