unit LocIPatCns;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, UnAppPF;

type
  TFmLocIPatCns = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdCodUso: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label1: TLabel;
    EdQtdIni: TdmkEdit;
    EdQtdFim: TdmkEdit;
    Label2: TLabel;
    EdQtdUso: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    EdPrcUni: TdmkEdit;
    Label8: TLabel;
    EdValorRucAAdiantar: TdmkEdit;
    EdValUso: TdmkEdit;
    Label9: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdQtdIniChange(Sender: TObject);
    procedure EdQtdFimChange(Sender: TObject);
    procedure EdPrcUniChange(Sender: TObject);
    procedure EdValorRucAAdiantarChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenLocCPatCns(Item: Integer);
    procedure Calcula_Uso_e_Val();

  public
    { Public declarations }
    FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmLocIPatCns: TFmLocIPatCns;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck;

{$R *.DFM}

procedure TFmLocIPatCns.BtOKClick(Sender: TObject);
var
  //Codigo, CtrID, GraGruX, Unidade
  Item: Integer;
  QtdIni, QtdFim, QtdUso, PrcUni, ValUso, ValorRucAAdiantar: Double;
begin
  Item              := Geral.IMV(Geral.SoNumero_TT(DBEdCodigo.Text));
  QtdIni            := EdQtdIni.ValueVariant;
  QtdFim            := EdQtdFim.ValueVariant;
  QtdUso            := EdQtdUso.ValueVariant;
  PrcUni            := EdPrcUni.ValueVariant;
  ValUso            := EdValUso.ValueVariant;
  ValorRucAAdiantar := EdValorRucAAdiantar.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'loccitsruc', False, [
  'QtdIni', 'QtdFim', 'QtdUso',
  'PrcUni', 'ValUso', 'ValorRucAAdiantar'], [
  'Item'], [
  QtdIni, QtdFim, QtdUso,
  PrcUni, ValUso, ValorRucAAdiantar], [
  Item], True) then
  begin
    AppPF.AtualizaTotaisLocCConCab_Locacao(FCodigo);
    ReopenLocCPatCns(Item);
    if FQrIts <> nil then
    begin
      FQrIts.Close;
      FQrIts.Open;
      FQrIts.Locate('Item', Item, []);
    end;
    Close;
  end;
end;

procedure TFmLocIPatCns.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocIPatCns.Calcula_Uso_e_Val;
var
  //ValorRucAAdiantar,
  QtdIni, QtdFim, QtdUso, PrcUni, ValUso: Double;
begin
  QtdIni := EdQtdIni.ValueVariant;
  QtdFim := EdQtdFim.ValueVariant;
  //QtdUso := QtdFim - QtdIni;
  // invertido de negativo para positivo em 2020-10-16
  QtdUso := QtdIni - QtdFim;
  PrcUni := EdPrcUni.ValueVariant;
  ValUso := QtdUso * PrcUni;
(*
  // valor adiantamento 2020-10-16
  ValorRucAAdiantar := EdValorRucAAdiantar.ValueVariant;
  if ValUso < ValorRucAAdiantar then
    ValUso := ValorRucAAdiantar;
*)
  //
  EdQtdUso.ValueVariant := QtdUso;
  EdValUso.ValueVariant := ValUso;
end;

procedure TFmLocIPatCns.EdPrcUniChange(Sender: TObject);
begin
  Calcula_Uso_e_Val();
end;

procedure TFmLocIPatCns.EdQtdFimChange(Sender: TObject);
begin
  Calcula_Uso_e_Val();
end;

procedure TFmLocIPatCns.EdQtdIniChange(Sender: TObject);
begin
  Calcula_Uso_e_Val();
end;

procedure TFmLocIPatCns.EdValorRucAAdiantarChange(Sender: TObject);
begin
  Calcula_Uso_e_Val();
end;

procedure TFmLocIPatCns.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmLocIPatCns.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmLocIPatCns.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocIPatCns.ReopenLocCPatCns(Item: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('CtrID').AsInteger;
    FQrIts.Open;
    //
    if Item <> 0 then
      FQrIts.Locate('Item', Item, []);
  end;
end;

end.
