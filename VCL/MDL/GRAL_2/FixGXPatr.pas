unit FixGXPatr;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, Module, Grids, DBGrids,
  ComCtrls, dmkEditDateTimePicker, dmkMemo, Menus, UnDmkEnums, UnAppEnums;

type
  TFmFixGXPatr = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtCab: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrFixGXPatr: TmySQLQuery;
    DsFixGXPatr: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrMarcas: TmySQLQuery;
    DsMarcas: TDataSource;
    QrMarcasControle: TIntegerField;
    QrMarcasNO_MARCA_FABR: TWideStringField;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    QrFixGXPatrGraGruX: TIntegerField;
    QrFixGXPatrDescrTerc: TWideStringField;
    QrFixGXPatrNO_MARCA: TWideStringField;
    QrFixGXPatrNO_FABR: TWideStringField;
    QrFixGXPatrComplem: TWideStringField;
    QrFixGXPatrAquisData: TDateField;
    QrFixGXPatrAquisDocu: TWideStringField;
    QrFixGXPatrAquisValr: TFloatField;
    QrFixGXPatrSituacao: TWordField;
    QrFixGXPatrAtualValr: TFloatField;
    QrFixGXPatrValorMes: TFloatField;
    QrFixGXPatrValorQui: TFloatField;
    QrFixGXPatrValorSem: TFloatField;
    QrFixGXPatrValorDia: TFloatField;
    QrFixGXPatrAgrupador: TIntegerField;
    QrFixGXPatrModelo: TWideStringField;
    QrFixGXPatrSerie: TWideStringField;
    QrFixGXPatrVoltagem: TWideStringField;
    QrFixGXPatrPotencia: TWideStringField;
    QrFixGXPatrCapacid: TWideStringField;
    QrFixGXPatrVendaData: TDateField;
    QrFixGXPatrVendaDocu: TWideStringField;
    QrFixGXPatrVendaValr: TFloatField;
    QrFixGXPatrVendaEnti: TIntegerField;
    QrFixGXPatrObserva: TWideStringField;
    QrFixGXPatrAGRPAT: TWideStringField;
    QrFixGXPatrCLVPAT: TWideStringField;
    Panel6: TPanel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    Panel9: TPanel;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label5: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    GroupBox4: TGroupBox;
    Panel11: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit22: TDBEdit;
    Label27: TLabel;
    GroupBox2: TGroupBox;
    Panel8: TPanel;
    Label17: TLabel;
    Label16: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label13: TLabel;
    DBEdit8: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit9: TDBEdit;
    GroupBox3: TGroupBox;
    Panel10: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    QrFixGXPatrMarca: TIntegerField;
    QrFixGXPatrVENDADATA_TXT: TWideStringField;
    QrFixGXPatrSIT_CHR: TWideStringField;
    QrFixGXPatrMARPAT: TWideStringField;
    Label25: TLabel;
    Label7: TLabel;
    EdGraGruX: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    EdMarca: TdmkEditCB;
    CBMarca: TdmkDBLookupComboBox;
    SbMarcas: TSpeedButton;
    Label38: TLabel;
    EdComplem: TdmkEdit;
    Panel12: TPanel;
    GroupBox5: TGroupBox;
    Panel13: TPanel;
    Label26: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    TPAquisData: TdmkEditDateTimePicker;
    EdAquisDocu: TdmkEdit;
    EdAquisValr: TdmkEdit;
    Panel14: TPanel;
    GroupBox6: TGroupBox;
    Panel15: TPanel;
    Label30: TLabel;
    Label31: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    GroupBox7: TGroupBox;
    Panel16: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    EdAtualValr: TdmkEdit;
    EdValorMes: TdmkEdit;
    EdValorQui: TdmkEdit;
    EdValorSem: TdmkEdit;
    EdValorDia: TdmkEdit;
    EdModelo: TdmkEdit;
    EdSerie: TdmkEdit;
    EdVoltagem: TdmkEdit;
    EdPotencia: TdmkEdit;
    EdCapacid: TdmkEdit;
    Label46: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Panel17: TPanel;
    Panel18: TPanel;
    DBMemo1: TDBMemo;
    Panel19: TPanel;
    Panel20: TPanel;
    MeObserva: TdmkMemo;
    QrFixGXPatrAplicacao: TIntegerField;
    QrFixGXPatrCPL_EXISTE: TFloatField;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    QrLocod: TmySQLQuery;
    QrFixGXPatrCONTROLE: TIntegerField;
    QrFixGXPatrNO_GG1: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFixGXPatrAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFixGXPatrBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbMarcasClick(Sender: TObject);
    procedure QrFixGXPatrCalcFields(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    FGraToolRent: TGraToolRent;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    (* N�o usa
    function PesquisaNomePatrimonio(): String;
    *)
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmFixGXPatr: TFmFixGXPatr;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, DmkDAC_PF, Principal, MyDBCheck, GraGru1, (*GraGXPIts,*) Curinga,
  ModuleGeral, GraGruN;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFixGXPatr.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

(* N�o usa
function TFmFixGXPatr.PesquisaNomePatrimonio: String;
var
  Filtro, Nome: String;
begin
  Dmod.FiltroGrade(FGraToolRent, Filtro);
  //
  Nome   := '';
  Result := '';
  //
  if InputQuery('Procura e Lista', 'Digite parte do nome:', Nome) then
  begin
    DmodG.QrSB4.Close;
    DmodG.QrSB4.SQL.Clear;
    DmodG.QrSB4.SQL.Add('SELECT gg1.Referencia CodUsu, gg1.Referencia Codigo, gg1.Nome');
    DmodG.QrSB4.SQL.Add('FROM gragru1 gg1');
    DmodG.QrSB4.SQL.Add('WHERE ' + Filtro);
    DmodG.QrSB4.SQL.Add('AND gg1.Nivel1>0');
    DmodG.QrSB4.SQL.Add('AND gg1.Nome LIKE "%' + Nome + '%"');
    DModG.QrSB4.SQL.Add('ORDER BY gg1.Nome');
    DmodG.QrSB4.Open;
    //
    Application.CreateForm(TFmCuringa, FmCuringa);
    FmCuringa.DBGrid1.DataSource := DmodG.DsSB4;
    FmCuringa.ShowModal;
    FmCuringa.Destroy;
    //
    Result := VAR_CODIGO4;    
  end;
end;
*)

procedure TFmFixGXPatr.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrFixGXPatr);
  MyObjects.HabilitaMenuItemItsDel(CabExclui1, QrFixGXPatr);
end;

procedure TFmFixGXPatr.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFixGXPatrControle.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFixGXPatr.DefParams;
begin
  VAR_GOTOTABELA := 'fixgxpatr';
  VAR_GOTOMYSQLTABLE := QrFixGXPatr;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := 'GraGruX';
  VAR_GOTONOME := 'DescrTerc';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;

  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT CHAR(cpl.Situacao) SIT_CHR, ');
  VAR_SQLx.Add('/*');
  VAR_SQLx.Add('ggx.Controle, gg1.Referencia, ');
  VAR_SQLx.Add('gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, ');
  VAR_SQLx.Add('*/');
  VAR_SQLx.Add('gfm.Nome NO_MARCA, gfc.Nome NO_FABR, ');
  VAR_SQLx.Add('IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, ');
  VAR_SQLx.Add('/*');
  VAR_SQLx.Add('gg2.Nome NO_GG2, gg3.Nome NO_GG3, ');
  VAR_SQLx.Add('gg4.Nome NO_GG4, gg5.Nome NO_GG5, ggt.Nome NO_GGT, ');
  VAR_SQLx.Add('ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3, ggt.TitNiv4, ');
  VAR_SQLx.Add('ggt.TitNiv5, gg1.PrdGrupTip, gg1.Nivel1, gg1.Nivel2, ');
  VAR_SQLx.Add('gg1.Nivel3, gg1.Nivel4, gg1.Nivel5, gg1.CodUsu, ');
  VAR_SQLx.Add('*/');
  VAR_SQLx.Add('cpl.* ');
  VAR_SQLx.Add('FROM  fixgxpatr cpl ');
  VAR_SQLx.Add('LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle ');
  VAR_SQLx.Add('LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo ');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE cpl.GraGruX <> 0');
  //
  VAR_SQL1.Add('AND cpl.GraGruX=:P0');
  //
  //VAR_SQL2.Add('AND ???.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cpl.DescrTerc LIKE :P0');
end;

procedure TFmFixGXPatr.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFixGXPatr, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'Controle');
  //
  EdGraGruX.ValueVariant   := QrFixGXPatrControle.Value;
  EdNome.Text              := QrFixGXPatrNO_GG1.Value;
  EdComplem.Text           := QrFixGXPatrComplem.Value;
  TPAquisData.Date         := QrFixGXPatrAquisData.Value;
  EdAquisDocu.Text         := QrFixGXPatrAquisDocu.Value;
  EdAquisValr.ValueVariant := QrFixGXPatrAquisValr.Value;
  EdAtualValr.ValueVariant := QrFixGXPatrAtualValr.Value;
  EdValorMes.ValueVariant  := QrFixGXPatrValorMes.Value;
  EdValorQui.ValueVariant  := QrFixGXPatrValorQui.Value;
  EdValorSem.ValueVariant  := QrFixGXPatrValorSem.Value;
  EdValorDia.ValueVariant  := QrFixGXPatrValorDia.Value;
  EdMarca.ValueVariant     := QrFixGXPatrMarca.Value;
  CBMarca.KeyValue         := QrFixGXPatrMarca.Value;
  EdModelo.Text            := QrFixGXPatrModelo.Value;
  EdSerie.Text             := QrFixGXPatrSerie.Value;
  EdVoltagem.Text          := QrFixGXPatrVoltagem.Value;
  EdPotencia.Text          := QrFixGXPatrPotencia.Value;
  EdCapacid.Text           := QrFixGXPatrCapacid.Value;
  MeObserva.Text           := QrFixGXPatrObserva.Value;
end;

procedure TFmFixGXPatr.CabExclui1Click(Sender: TObject);
var
  GraGruX: Integer;
begin
  if (QrFixGXPatr.State = dsInactive) or (QrFixGXPatr.RecordCount = 0) then
    Exit;
  //
  GraGruX := QrFixGXPatrGraGruX.Value;
  //Verifica Manuten��es
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM fixgereequ ',
  'WHERE GraGruX=' + Geral.FF0(GraGruX),
  '']);
  if Dmod.QrAux.RecordCount = 0 then
  begin
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do registro atual?',
    'fixgxpatr', 'GraGruX', GraGruX, Dmod.MyDB) = ID_YES then
    begin
      Va(vpLast);
    end;
  end else
    Geral.MB_Info('Este item n�o pode ser exclu�do!' + #13#10 +
      'Motivo: Ele foi utilizado no conserto ID n�mero ' +
      Geral.FF0(Dmod.QrAux.FieldByName('Codigo').AsInteger));
end;

procedure TFmFixGXPatr.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFixGXPatr, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'Controle');
end;

procedure TFmFixGXPatr.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFixGXPatr.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFixGXPatr.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFixGXPatr.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFixGXPatr.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFixGXPatr.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFixGXPatr.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFixGXPatr.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFixGXPatr.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFixGXPatrGraGruX.Value;
  Close;
end;

procedure TFmFixGXPatr.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmFixGXPatr.BtConfirmaClick(Sender: TObject);
  function InsUpdG1PrAp(SQLType: TSQLType; GraGruX: Integer): Boolean;
  var
    Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia, Capacid,
    Observa, DescrTerc: String;
    Marca: Integer;
    AquisValr, AtualValr, ValorMes, ValorQui, ValorSem, ValorDia: Double;
  begin
    Complem   := EdComplem.Text;
    AquisData := Geral.FDT(TPAquisData.Date, 1);
    AquisDocu := EdAquisDocu.Text;
    AquisValr := EdAquisValr.ValueVariant;
    AtualValr := EdAtualValr.ValueVariant;
    ValorMes  := EdValorMes.ValueVariant;
    ValorQui  := EdValorQui.ValueVariant;
    ValorSem  := EdValorSem.ValueVariant;
    ValorDia  := EdValorDia.ValueVariant;
    Marca     := EdMarca.ValueVariant;
    Modelo    := EdModelo.Text;
    Serie     := EdSerie.Text;
    Voltagem  := EdVoltagem.Text;
    Potencia  := EdPotencia.Text;
    Capacid   := EdCapacid.Text;
    Observa   := MeObserva.Text;
    DescrTerc := EdNome.Text;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'fixgxpatr', False, [
    'Complem', 'AquisData', 'AquisDocu',
    'AquisValr', 'AtualValr', 'ValorMes',
    'ValorQui', 'ValorSem', 'ValorDia',
    'Marca', 'Modelo', 'Serie',
    'Voltagem', 'Potencia', 'Capacid',
    'Observa', 'DescrTerc'],
    ['GraGruX'], [
    Complem, AquisData, AquisDocu,
    AquisValr, AtualValr, ValorMes,
    ValorQui, ValorSem, ValorDia,
    Marca, Modelo, Serie,
    Voltagem, Potencia, Capacid,
    Observa, DescrTerc], [
    GraGruX], True);
  end;
var
  GraGruX: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  GraGruX := EdGraGruX.ValueVariant;
  GraGruX := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', ImgTipo.SQLType, GraGruX);
  //
  if InsUpdG1PrAp(ImgTipo.SQLType, GraGruX) then
  begin
    LocCod(GraGruX, GraGruX);
    ImgTipo.SQLType := stlok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
  end;
end;

procedure TFmFixGXPatr.BtDesisteClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(GraGruX, Dmod.MyDB, 'fixgxpatr', 'GraGruX');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFixGXPatr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CriaOForm;
  //
  UMyMod.AbreQuery(QrMarcas, Dmod.MyDB);
end;

procedure TFmFixGXPatr.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFixGXPatrGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmFixGXPatr.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Info('Impress�o n�o implementada nesta janela!');
end;

procedure TFmFixGXPatr.SbMarcasClick(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  FmPrincipal.MostraFormGraFabCad();
  UMyMod.SetaCodigoPesquisado(EdMarca, CBMarca, QrMarcas, VAR_CADASTRO2, 'Controle');
end;

procedure TFmFixGXPatr.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  LocCod(QrFixGXPatrGraGruX.Value,
    CuringaLoc.CriaForm('GraGruX', 'DescrTerc', 'fixgxpatr', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFixGXPatr.SbNovoClick(Sender: TObject);
(*
var
  Referencia: String;
*)
begin
(*
  Referencia := '';
  if InputQuery('Pesquisa por Refer�ncia', 'Informe a refer�ncia', Referencia) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocod, Dmod.MyDB, [
    'SELECT ggx.Controle ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE gg1.Referencia="' + Referencia + '"',
    '']);
    LocCod(QrLocod.FieldByName('Controle').AsInteger, QrLocod.FieldByName('Controle').AsInteger);
  end;
*)
  LaRegistro.Caption := GOTOy.Codigo(QrFixGXPatrGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmFixGXPatr.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFixGXPatr.QrFixGXPatrAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFixGXPatr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFixGXPatr.SbQueryClick(Sender: TObject);
(*
var
  Referencia: String;
*)
begin
(*
  Referencia := PesquisaNomePatrimonio();
  //
  if Length(Referencia) > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT ggx.Controle',
    'FROM gragrux ggx',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
    'WHERE gg1.Referencia="' + Referencia + '"',
   '']);
    LocCod(QrFixGXPatrControle.Value, Dmod.QrAux.FieldByName('Controle').AsInteger);
  end;
*)
  LocCod(QrFixGXPatrGraGruX.Value,
    CuringaLoc.CriaForm('GraGruX', 'DescrTerc', 'fixgxpatr', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFixGXPatr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFixGXPatr.QrFixGXPatrBeforeOpen(DataSet: TDataSet);
begin
  QrFixGXPatrGraGruX.DisplayFormat := FFormatFloat;
end;

procedure TFmFixGXPatr.QrFixGXPatrCalcFields(DataSet: TDataSet);
begin
  QrFixGXPatrCONTROLE.Value := QrFixGXPatrGraGruX.Value;
  QrFixGXPatrNO_GG1.Value := QrFixGXPatrDescrTerc.Value;
end;

end.

