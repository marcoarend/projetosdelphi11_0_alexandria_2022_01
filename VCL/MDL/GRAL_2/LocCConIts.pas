unit LocCConIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  UnAppPF, UnAppEnums, UnGrl_Consts, dmkDBLookupComboBox, dmkEditCB,
  mySQLDbTables, UnGraL_Jan;

type
  TFmLocCConIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    RGIDTab: TRadioGroup;
    RGStatus: TRadioGroup;
    Panel6: TPanel;
    Label1: TLabel;
    SbAtndCanal: TSpeedButton;
    EdAtndCanal: TdmkEditCB;
    CBAtndCanal: TdmkDBLookupComboBox;
    Panel5: TPanel;
    LaCondiOrca: TLabel;
    SBCondicaoPG: TSpeedButton;
    EdCondiOrca: TdmkEditCB;
    CBCondiOrca: TdmkDBLookupComboBox;
    QrAtndCanal: TMySQLQuery;
    QrAtndCanalCodigo: TIntegerField;
    QrAtndCanalNome: TWideStringField;
    DsAtndCanal: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGIDTabClick(Sender: TObject);
    procedure RGStatusClick(Sender: TObject);
    procedure SBCondicaoPGClick(Sender: TObject);
    procedure SbAtndCanalClick(Sender: TObject);
  private
    { Private declarations }
    FConfirmado: Boolean;
    procedure VerificaSeConfirma();
    procedure Confirma();
  public
    { Public declarations }
    FInseriu: Boolean;
    FCodigo, FCtrID, FEmpresa, FCliente: Integer;
    FDtHrEmi: TDateTime;
  end;

  var
  FmLocCConIts: TFmLocCConIts;

implementation

uses UnMyObjects, UMySQLModule, Module, ModuleFatura, UnGFat_Jan, DmkDAC_PF;

{$R *.DFM}

procedure TFmLocCConIts.BtOKClick(Sender: TObject);
begin
  Confirma();
end;

procedure TFmLocCConIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCConIts.Confirma();
var
  Codigo, CtrID, IDTab, Status, Cnta, UnidMed, FatPedVolCnta: Integer;
  SQLType: TSQLType;
  DtaInclu, DtaPrevi: TDateTime;
  RegrFiscal, PediVda, FatPedCab: Integer;
  Continua: Boolean;
  FretePor, IndFinal, IndPres, NF_Stat, CondiOrca, AtndCanal: Integer;
  Executou: Boolean;
begin
  FConfirmado    := True;
  SQLType        := ImgTipo.SQLType;
  Continua       := False;
  Codigo         := FCodigo;
  CtrID          := FCtrID;
  IDTab          := RGIDTab.ItemIndex;
  Status         := RGStatus.ItemIndex;
  CondiOrca      := EdCondiOrca.ValueVariant;
  AtndCanal      := EDAtndCanal.ValueVariant;
(*
  FatID          := ;
  FatNum         := ;
  ValorOrca      := ;
  ValorPedi      := ;
  ValorFatu      := ;
*)
  //
  if MyObjects.FIC(IDTab = 0, RGIDTab, 'Selecione o Tipo de Movimento!') then Exit;
  if MyObjects.FIC(Status = 0, RGStatus, 'Selecione o Status Inicial!') then Exit;
  if MyObjects.FIC(AtndCanal = 0, EdAtndCanal, 'Defina o canal de atendimento!') then Exit;
  //
  if (TTabConLocIts(IDTab) = TTabConLocIts.tcliVenda)
  and (TStatusMovimento(Status) = TStatusMovimento.statmovOrcamento) then
  begin
    if MyObjects.FIC(CondiOrca = 0, EdCondiOrca, 'Selecione a condi��o de pagamento!') then Exit;
  end;
  //
  if (TTabConLocIts(IDTab) = TTabConLocIts.tcliVenda)
  and (TStatusMovimento(Status) = TStatusMovimento.statmovPedido)
  and (SQLType = stIns) then
  begin
    Continua := AppPF.GeraFatPedCabEPediVdaDeLocCItsVen(ImgTipo.SQLType, FDtHrEmi,
      FEmpresa, FCliente, Codigo,
      FConfirmado, PediVda, FatPedCab, FatPedVolCnta);
    //
    NF_Stat := CO_005_StepNFePedido;
{
    DtaInclu   := FDtHrEmi;
    DtaPrevi   := FDtHrEmi;
    RegrFiscal := Dmod.QrOpcoesTRenLocRegrFisNFe.Value;
    //
    PediVda    := 0;
    FatPedCab  := 0;
    //
    if MyObjects.FIC(RegrFiscal = 0, nil,
    'Regra fiscal n�o definida nas op��es espec�ficas!') then
    begin
      FConfirmado := False;
      Exit;
    end;
    //
    FretePor       := 9; //Sem Frete! // Presencial (indPres<>4) n�o pode ter frete (modFrete<>9)
    indFinal       := 1; // Consumidor final
    indPres        := 1; // Presencial
    //
    if AppPF.InserePedidoVda(FEmpresa, FCliente, DtaInclu, DtaPrevi, RegrFiscal,
    (*CondicaoPG*)0, (*CartEmis*)0, (*TabelaPrc*)0, indFinal,
    indPres, (*finNFe**)1, (*idDest*)1, FretePor, (*EntregaEnti*)0,
    (*RetiradaEnti*)0, (*DOCENT*)'', (*PedidoCli*)'', (*Observa*)'',
    PediVda, FatPedCab) then
    begin
      UnidMed := 0;
      Cnta := UMyMod.BuscaEmLivreY_Def('fatpedvol', 'Cnta', ImgTipo.SQLType, Cnta);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'fatpedvol', False, [
      'Codigo', 'UnidMed'], ['Cnta'], [Codigo, UnidMed], [Cnta], True) then
      begin
        FatPedVolCnta := Cnta;
        //
        Continua := True;

        (*
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconits', False, [
        'PediVda', 'FatPedCab', 'FatPedVolCnta'], ['CtrID'], [
        PediVda, FatPedCab, FatPedVolCnta], [CtrID], True)
        then ;
        *)

      end;
    end;
}
  end else
  begin
    Continua      := True;
(*
    PediVda       := 0;
    FatPedCab     := 0;
    FatPedVolCnta := 0;
    NF_Stat       := 0;
*)
  end;
  if Continua then
  begin
    CtrID := UMyMod.BPGS1I32('loccconits', 'CtrID', '', '', tsPos, SQLType, CtrID);
    if SQLType = stIns then
    begin
      Executou := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconits', False, [
      'Codigo', 'IDTab', 'Status',
      (*'FatID', 'FatNum',
      'ValorOrca', 'ValorPedi', 'ValorFatu',*)
      'PediVda', 'FatPedCab', 'FatPedVolCnta',
      'NF_Stat', 'CondiOrca', 'AtndCanal'], [
      'CtrID'], [
      Codigo, IDTab, Status,
      (*FatID, FatNum,
      ValorOrca, ValorPedi, ValorFatu*)
      PediVda, FatPedCab, FatPedVolCnta,
      NF_Stat, CondiOrca, AtndCanal], [
      CtrID], True);
    end else
    begin
      Executou := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconits', False, [
      'Codigo', 'CondiOrca'], [
      'CtrID'], [
      Codigo, CondiOrca], [
      CtrID], True);
    end;
    //
    if Executou then
    begin
      FInseriu := SQLType = stIns;
      FCtrID := CtrID;
      //
      //
      Close;
    end;
  end else
    FConfirmado := False;
end;

procedure TFmLocCConIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCConIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FConfirmado := False;
  FInseriu := False;
  //
  DmFatura.ReopenPediPrzCab();
  CBCondiOrca.ListSource := DmFatura.DsPediPrzCab;
  UnDmkDAC_PF.AbreQuery(QrAtndCanal, Dmod.MyDB);
end;

procedure TFmLocCConIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCConIts.RGIDTabClick(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    if RGStatus.ItemIndex  = 0 then
    begin
      case RGIDTab.ItemIndex of
        0: ;
        (*LOC*)1: RGStatus.ItemIndex := Dmod.QrOpcoesTRenStatIniPadrLoc.Value;
        (*SVC*)2: RGStatus.ItemIndex := Dmod.QrOpcoesTRenStatIniPadrSvc.Value;
        (*VEN*)3: RGStatus.ItemIndex := Dmod.QrOpcoesTRenStatIniPadrVen.Value;
      end;
    end;
  end;
  VerificaSeConfirma();
end;

procedure TFmLocCConIts.RGStatusClick(Sender: TObject);
begin
  VerificaSeConfirma()
end;

procedure TFmLocCConIts.SBCondicaoPGClick(Sender: TObject);
begin
  GFat_Jan.InsereEDefinePediPrzCab(EdCondiOrca.ValueVariant, EdCondiOrca,
  CBCondiOrca, DmFatura.QrPediPrzCab);
end;

procedure TFmLocCConIts.SbAtndCanalClick(Sender: TObject);
begin
  GraL_Jan.MostraFormAtndCanal();
end;

procedure TFmLocCConIts.VerificaSeConfirma();
begin
{
  if (ImgTipo.SQLType = stIns) and
  (RGIDTab.ItemIndex > 0) and
  (RGStatus.ItemIndex > 0) then
  begin

    if not FConfirmado then
      Confirma();
  end;
}
end;

end.
