object FmGraGXPatr: TFmGraGXPatr
  Left = 368
  Top = 194
  Caption = 'PRD-GRUPO-027 :: Cadastro de Patrim'#244'nio'
  ClientHeight = 649
  ClientWidth = 993
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 90
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 89
    Width = 993
    Height = 560
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 993
      Height = 101
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Identifica'#231#227'o: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label2: TLabel
        Left = 62
        Top = 16
        Width = 55
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Refer'#234'ncia:'
        FocusControl = DBEdNome
      end
      object Label6: TLabel
        Left = 294
        Top = 58
        Width = 33
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Marca:'
        FocusControl = DBEdit4
      end
      object Label8: TLabel
        Left = 489
        Top = 58
        Width = 53
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fabricante:'
        FocusControl = DBEdit5
      end
      object Label3: TLabel
        Left = 8
        Top = 58
        Width = 67
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Complemento:'
        FocusControl = DBEdit1
      end
      object Label12: TLabel
        Left = 738
        Top = 16
        Width = 45
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Situa'#231#227'o:'
        FocusControl = DBEdit7
      end
      object Label25: TLabel
        Left = 8
        Top = 16
        Width = 48
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Reduzido:'
      end
      object Label1: TLabel
        Left = 684
        Top = 16
        Width = 35
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#237'vel1:'
      end
      object Label46: TLabel
        Left = 271
        Top = 16
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = dmkDBEdit1
      end
      object Label18: TLabel
        Left = 8
        Top = 80
        Width = 52
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Agrupador:'
        FocusControl = DBEdit13
      end
      object Label51: TLabel
        Left = 175
        Top = 16
        Width = 64
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Patrim'#244'nio:'
        FocusControl = dmkDBEdit2
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 31
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Controle'
        DataSource = DsGraGXPatr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 62
        Top = 31
        Width = 111
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Referencia'
        DataSource = DsGraGXPatr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit4: TDBEdit
        Left = 334
        Top = 53
        Width = 151
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_MARCA'
        DataSource = DsGraGXPatr
        TabOrder = 2
      end
      object DBEdit5: TDBEdit
        Left = 546
        Top = 53
        Width = 207
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_FABR'
        DataSource = DsGraGXPatr
        TabOrder = 3
      end
      object DBEdit1: TDBEdit
        Left = 80
        Top = 53
        Width = 211
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Complem'
        DataSource = DsGraGXPatr
        TabOrder = 4
      end
      object DBEdit7: TDBEdit
        Left = 738
        Top = 31
        Width = 36
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Situacao'
        DataSource = DsGraGXPatr
        TabOrder = 5
      end
      object DBEdit19: TDBEdit
        Left = 773
        Top = 31
        Width = 30
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'SIT_CHR'
        DataSource = DsGraGXPatr
        TabOrder = 6
      end
      object DBEdit20: TDBEdit
        Left = 684
        Top = 31
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'COD_GG1'
        DataSource = DsGraGXPatr
        TabOrder = 7
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 271
        Top = 31
        Width = 410
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NO_GG1'
        DataSource = DsGraGXPatr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBRGAplicacao: TDBRadioGroup
        Left = 808
        Top = 24
        Width = 177
        Height = 73
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Aplica'#231#227'o: '
        Columns = 2
        DataField = 'Aplicacao'
        DataSource = DsGraGXPatr
        Items.Strings = (
          'Inativo'
          'Principal'
          'Secund'#225'rio')
        TabOrder = 9
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object DBEdit13: TDBEdit
        Left = 64
        Top = 75
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Agrupador'
        DataSource = DsGraGXPatr
        TabOrder = 10
      end
      object DBEdit21: TDBEdit
        Left = 246
        Top = 75
        Width = 555
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Nome'
        DataSource = DsAgrup
        TabOrder = 11
      end
      object DBEdit23: TDBEdit
        Left = 118
        Top = 75
        Width = 127
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Referencia'
        DataSource = DsAgrup
        TabOrder = 12
      end
      object DBCkAtivo: TDBCheckBox
        Left = 756
        Top = 56
        Width = 49
        Height = 17
        Caption = 'Ativo?'
        DataField = 'Ativo'
        DataSource = DsGraGXPatr
        TabOrder = 13
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 175
        Top = 31
        Width = 94
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Patrimonio'
        DataSource = DsGraGXPatr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 14
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 496
      Width = 993
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 183
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 134
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 92
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 51
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 185
        Top = 15
        Width = 165
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 350
        Top = 15
        Width = 641
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtIts: TBitBtn
          Tag = 498
          Left = 130
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Materiais'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtItsClick
        end
        object BtCab: TBitBtn
          Tag = 497
          Left = 5
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Patrim'#244'nio'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object Panel2: TPanel
          Left = 507
          Top = 0
          Width = 134
          Height = 47
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 157
      Width = 993
      Height = 56
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 489
        Height = 56
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Valores atuais: '
        TabOrder = 0
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 485
          Height = 39
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label17: TLabel
            Left = 321
            Top = 0
            Width = 19
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Dia:'
            FocusControl = DBEdit12
          end
          object Label16: TLabel
            Left = 248
            Top = 0
            Width = 42
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Semana:'
            FocusControl = DBEdit11
          end
          object Label15: TLabel
            Left = 175
            Top = 0
            Width = 48
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quinzena:'
            FocusControl = DBEdit10
          end
          object Label14: TLabel
            Left = 103
            Top = 0
            Width = 23
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'M'#234's:'
            FocusControl = DBEdit9
          end
          object Label13: TLabel
            Left = 7
            Top = 0
            Width = 37
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Venda: '
            FocusControl = DBEdit8
          end
          object Label54: TLabel
            Left = 397
            Top = 0
            Width = 59
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fim-de-sem.:'
          end
          object DBEdit8: TDBEdit
            Left = 7
            Top = 16
            Width = 92
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'AtualValr'
            DataSource = DsGraGXPatr
            TabOrder = 0
          end
          object DBEdit12: TDBEdit
            Left = 321
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ValorDia'
            DataSource = DsGraGXPatr
            TabOrder = 1
          end
          object DBEdit11: TDBEdit
            Left = 248
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ValorSem'
            DataSource = DsGraGXPatr
            TabOrder = 2
          end
          object DBEdit10: TDBEdit
            Left = 175
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ValorQui'
            DataSource = DsGraGXPatr
            TabOrder = 3
          end
          object DBEdit9: TDBEdit
            Left = 103
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ValorMes'
            DataSource = DsGraGXPatr
            TabOrder = 4
          end
          object DBEdit34: TDBEdit
            Left = 395
            Top = 16
            Width = 68
            Height = 21
            DataField = 'ValorFDS'
            DataSource = DsGraGXPatr
            TabOrder = 5
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 489
        Top = 0
        Width = 504
        Height = 56
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = 'Caracter'#237'sticas: '
        TabOrder = 1
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 500
          Height = 39
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label19: TLabel
            Left = 9
            Top = 0
            Width = 38
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Modelo:'
            FocusControl = DBEdit14
          end
          object Label20: TLabel
            Left = 125
            Top = 0
            Width = 27
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'S'#233'rie:'
            FocusControl = DBEdit15
          end
          object Label21: TLabel
            Left = 238
            Top = 0
            Width = 47
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Voltagem:'
            FocusControl = DBEdit16
          end
          object Label22: TLabel
            Left = 311
            Top = 0
            Width = 45
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pot'#234'ncia:'
            FocusControl = DBEdit17
          end
          object Label23: TLabel
            Left = 380
            Top = 0
            Width = 60
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Capacidade:'
            FocusControl = DBEdit18
          end
          object DBEdit14: TDBEdit
            Left = 9
            Top = 16
            Width = 110
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Modelo'
            DataSource = DsGraGXPatr
            TabOrder = 0
          end
          object DBEdit15: TDBEdit
            Left = 125
            Top = 16
            Width = 110
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Serie'
            DataSource = DsGraGXPatr
            TabOrder = 1
          end
          object DBEdit16: TDBEdit
            Left = 238
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Voltagem'
            DataSource = DsGraGXPatr
            TabOrder = 2
          end
          object DBEdit17: TDBEdit
            Left = 311
            Top = 16
            Width = 64
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Potencia'
            DataSource = DsGraGXPatr
            TabOrder = 3
          end
          object DBEdit18: TDBEdit
            Left = 380
            Top = 16
            Width = 111
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Capacid'
            DataSource = DsGraGXPatr
            TabOrder = 4
          end
        end
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 101
      Width = 993
      Height = 56
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 417
        Height = 56
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Aquisi'#231#227'o:  '
        TabOrder = 0
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 413
          Height = 39
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label5: TLabel
            Left = 7
            Top = 0
            Width = 26
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data:'
            FocusControl = DBEdit2
          end
          object Label10: TLabel
            Left = 65
            Top = 0
            Width = 58
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Documento:'
            FocusControl = DBEdit3
          end
          object Label11: TLabel
            Left = 316
            Top = 0
            Width = 27
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor:'
            FocusControl = DBEdit6
          end
          object DBEdit2: TDBEdit
            Left = 7
            Top = 16
            Width = 56
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'AquisData'
            DataSource = DsGraGXPatr
            TabOrder = 0
          end
          object DBEdit3: TDBEdit
            Left = 65
            Top = 16
            Width = 250
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'AquisDocu'
            DataSource = DsGraGXPatr
            TabOrder = 1
          end
          object DBEdit6: TDBEdit
            Left = 316
            Top = 16
            Width = 92
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'AquisValr'
            DataSource = DsGraGXPatr
            TabOrder = 2
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 417
        Top = 0
        Width = 576
        Height = 56
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Venda:  '
        TabOrder = 1
        object Panel11: TPanel
          Left = 2
          Top = 15
          Width = 572
          Height = 39
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label32: TLabel
            Left = 7
            Top = 0
            Width = 26
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data:'
            FocusControl = DBEdit27
          end
          object Label33: TLabel
            Left = 65
            Top = 0
            Width = 58
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Documento:'
            FocusControl = DBEdit28
          end
          object Label34: TLabel
            Left = 320
            Top = 0
            Width = 27
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor:'
            FocusControl = DBEdit29
          end
          object Label27: TLabel
            Left = 416
            Top = 0
            Width = 35
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente:'
            FocusControl = DBEdit22
          end
          object DBEdit27: TDBEdit
            Left = 7
            Top = 16
            Width = 56
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'VENDADATA_TXT'
            DataSource = DsGraGXPatr
            TabOrder = 0
          end
          object DBEdit28: TDBEdit
            Left = 65
            Top = 16
            Width = 250
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'VendaDocu'
            DataSource = DsGraGXPatr
            TabOrder = 1
          end
          object DBEdit29: TDBEdit
            Left = 320
            Top = 16
            Width = 92
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'VendaValr'
            DataSource = DsGraGXPatr
            TabOrder = 2
          end
          object DBEdit22: TDBEdit
            Left = 416
            Top = 16
            Width = 82
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'VendaEnti'
            DataSource = DsGraGXPatr
            TabOrder = 3
          end
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 269
      Width = 993
      Height = 106
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      DataSource = DsGraGXPIts
      TabOrder = 4
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_APLICACAO'
          Title.Caption = 'Aplica'#231#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Referencia'
          Title.Caption = 'Refer'#234'ncia'
          Width = 79
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_GG1'
          Title.Caption = 'Descri'#231#227'o'
          Width = 400
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ItemValr'
          Title.Caption = '$ Unidade'
          Visible = True
        end>
    end
    object Panel17: TPanel
      Left = 0
      Top = 375
      Width = 228
      Height = 121
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 5
      object Panel18: TPanel
        Left = 0
        Top = 0
        Width = 228
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Observa'#231#245'es:'
        TabOrder = 0
      end
      object DBMemo1: TDBMemo
        Left = 0
        Top = 17
        Width = 228
        Height = 104
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataField = 'Observa'
        DataSource = DsGraGXPatr
        TabOrder = 1
      end
    end
    object GroupBox8: TGroupBox
      Left = 0
      Top = 213
      Width = 993
      Height = 56
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = 'Dados do produto:'
      TabOrder = 6
      object DBText2: TDBText
        Left = 405
        Top = 14
        Width = 140
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TitNiv5'
        DataSource = DsGraGXPatr
      end
      object DBText3: TDBText
        Left = 550
        Top = 14
        Width = 140
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TitNiv4'
        DataSource = DsGraGXPatr
      end
      object DBText4: TDBText
        Left = 695
        Top = 14
        Width = 140
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TitNiv3'
        DataSource = DsGraGXPatr
      end
      object DBText5: TDBText
        Left = 840
        Top = 14
        Width = 140
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TitNiv2'
        DataSource = DsGraGXPatr
      end
      object Label47: TLabel
        Left = 243
        Top = 14
        Width = 78
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tipo de produto:'
        FocusControl = DBEdit8
      end
      object Label52: TLabel
        Left = 8
        Top = 13
        Width = 51
        Height = 13
        Caption = 'Estoque:'
        FocusControl = DBEdit32
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label53: TLabel
        Left = 164
        Top = 13
        Width = 24
        Height = 13
        Caption = 'NCM'
        FocusControl = DBEdit33
      end
      object Label56: TLabel
        Left = 84
        Top = 13
        Width = 71
        Height = 13
        Caption = 'Dias livres (D-):'
        FocusControl = DBEdit35
      end
      object DBEdit24: TDBEdit
        Left = 243
        Top = 29
        Width = 158
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GGT'
        DataSource = DsGraGXPatr
        TabOrder = 0
      end
      object DBEdit25: TDBEdit
        Left = 405
        Top = 29
        Width = 140
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GG5'
        DataSource = DsGraGXPatr
        TabOrder = 1
      end
      object DBEdit26: TDBEdit
        Left = 695
        Top = 29
        Width = 140
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GG3'
        DataSource = DsGraGXPatr
        TabOrder = 2
      end
      object DBEdit30: TDBEdit
        Left = 550
        Top = 29
        Width = 140
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GG4'
        DataSource = DsGraGXPatr
        TabOrder = 3
      end
      object DBEdit31: TDBEdit
        Left = 840
        Top = 29
        Width = 140
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GG2'
        DataSource = DsGraGXPatr
        TabOrder = 4
      end
      object DBEdit32: TDBEdit
        Left = 8
        Top = 29
        Width = 73
        Height = 21
        DataField = 'EstqSdo'
        DataSource = DsGraGXPatr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
      end
      object DBEdit33: TDBEdit
        Left = 164
        Top = 29
        Width = 73
        Height = 21
        DataField = 'NCM'
        DataSource = DsGraGXPatr
        TabOrder = 6
      end
      object DBEdit35: TDBEdit
        Left = 84
        Top = 29
        Width = 73
        Height = 21
        DataField = 'DMenos'
        DataSource = DsGraGXPatr
        TabOrder = 7
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 89
    Width = 993
    Height = 560
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 993
      Height = 101
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Identifica'#231#227'o: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 16
        Width = 14
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 279
        Top = 16
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 395
        Top = 58
        Width = 94
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fabricante / Marca:'
      end
      object SbMarcas: TSpeedButton
        Left = 724
        Top = 53
        Width = 21
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SbMarcasClick
      end
      object Label38: TLabel
        Left = 8
        Top = 58
        Width = 67
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Complemento:'
      end
      object Label39: TLabel
        Left = 62
        Top = 16
        Width = 55
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Refer'#234'ncia:'
      end
      object Label24: TLabel
        Left = 573
        Top = 80
        Width = 45
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Situa'#231#227'o:'
      end
      object Label45: TLabel
        Left = 8
        Top = 80
        Width = 52
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Agrupador:'
      end
      object SBSituacao: TSpeedButton
        Left = 784
        Top = 75
        Width = 21
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SBSituacaoClick
      end
      object Label50: TLabel
        Left = 194
        Top = 16
        Width = 52
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Patrim'#244'nio:'
      end
      object EdGraGruX: TdmkEdit
        Left = 8
        Top = 31
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 279
        Top = 31
        Width = 522
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMarca: TdmkEditCB
        Left = 494
        Top = 53
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMarca
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMarca: TdmkDBLookupComboBox
        Left = 546
        Top = 53
        Width = 175
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Controle'
        ListField = 'NO_MARCA_FABR'
        ListSource = DsMarcas
        TabOrder = 9
        dmkEditCB = EdMarca
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdComplem: TdmkEdit
        Left = 79
        Top = 53
        Width = 310
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Complem'
        UpdCampo = 'Complem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdReferencia: TdmkEdit
        Left = 62
        Top = 31
        Width = 130
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGAplicacao: TdmkRadioGroup
        Left = 808
        Top = 24
        Width = 177
        Height = 73
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Aplica'#231#227'o: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Inativo'
          'Principal'
          'Secund'#225'rio')
        TabOrder = 13
        UpdType = utYes
        OldValor = 0
      end
      object EdSIT_CHR: TdmkEdit
        Left = 654
        Top = 75
        Width = 23
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CharCase = ecUpperCase
        MaxLength = 1
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdSIT_CHRChange
      end
      object EdAgrupador: TdmkEditCB
        Left = 63
        Top = 75
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdAgrupadorChange
        OnKeyDown = EdAgrupadorKeyDown
        DBLookupComboBox = CBAgrupador
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBAgrupador: TdmkDBLookupComboBox
        Left = 205
        Top = 75
        Width = 364
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Controle'
        ListField = 'NOMEREF_TXT'
        ListSource = DsAgrupador
        TabOrder = 12
        OnKeyDown = CBAgrupadorKeyDown
        dmkEditCB = EdAgrupador
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCBSituacao: TdmkEditCB
        Left = 625
        Top = 75
        Width = 28
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCBSituacaoChange
        DBLookupComboBox = CBSituacao
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSituacao: TdmkDBLookupComboBox
        Left = 676
        Top = 75
        Width = 105
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGraGLSitu
        TabOrder = 6
        dmkEditCB = EdCBSituacao
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdAgrupRef: TdmkEdit
        Left = 115
        Top = 75
        Width = 89
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdAgrupRefChange
        OnKeyDown = EdAgrupRefKeyDown
      end
      object CkAtivo: TdmkCheckBox
        Left = 752
        Top = 56
        Width = 57
        Height = 17
        Caption = 'Ativo?'
        Checked = True
        State = cbChecked
        TabOrder = 14
        QryCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdPatrimonio: TdmkEdit
        Left = 194
        Top = 31
        Width = 83
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 496
      Width = 993
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 5
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 12
        Width = 111
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 864
        Top = 15
        Width = 127
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 5
          Top = 0
          Width = 110
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object Panel12: TPanel
      Left = 0
      Top = 101
      Width = 993
      Height = 56
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox5: TGroupBox
        Left = 0
        Top = 0
        Width = 993
        Height = 56
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Aquisi'#231#227'o:  '
        TabOrder = 0
        object Panel13: TPanel
          Left = 2
          Top = 15
          Width = 989
          Height = 39
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label26: TLabel
            Left = 5
            Top = 0
            Width = 26
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data:'
          end
          object Label28: TLabel
            Left = 112
            Top = 0
            Width = 58
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Documento:'
          end
          object Label29: TLabel
            Left = 367
            Top = 0
            Width = 27
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor:'
          end
          object TPAquisData: TdmkEditDateTimePicker
            Left = 5
            Top = 16
            Width = 104
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 41133.774077233790000000
            Time = 41133.774077233790000000
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object EdAquisDocu: TdmkEdit
            Left = 112
            Top = 16
            Width = 251
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdAquisValr: TdmkEdit
            Left = 367
            Top = 16
            Width = 92
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
    object Panel14: TPanel
      Left = 0
      Top = 157
      Width = 993
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox6: TGroupBox
        Left = 0
        Top = 0
        Width = 469
        Height = 79
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Valores atuais: '
        TabOrder = 0
        object Panel15: TPanel
          Left = 2
          Top = 15
          Width = 465
          Height = 62
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label30: TLabel
            Left = 321
            Top = 0
            Width = 19
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Dia:'
          end
          object Label31: TLabel
            Left = 248
            Top = 0
            Width = 42
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Semana:'
          end
          object Label35: TLabel
            Left = 175
            Top = 0
            Width = 48
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quinzena:'
          end
          object Label36: TLabel
            Left = 103
            Top = 0
            Width = 23
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'M'#234's:'
          end
          object Label37: TLabel
            Left = 7
            Top = 0
            Width = 37
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Venda: '
          end
          object Label55: TLabel
            Left = 393
            Top = 0
            Width = 59
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fim-de-sem.:'
          end
          object EdAtualValr: TdmkEdit
            Left = 7
            Top = 16
            Width = 92
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdValorMes: TdmkEdit
            Left = 103
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdValorQui: TdmkEdit
            Left = 175
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdValorSem: TdmkEdit
            Left = 248
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdValorDia: TdmkEdit
            Left = 321
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdValorFDS: TdmkEdit
            Left = 393
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object GroupBox7: TGroupBox
        Left = 469
        Top = 0
        Width = 524
        Height = 79
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = 'Caracter'#237'sticas: '
        TabOrder = 1
        object Panel16: TPanel
          Left = 2
          Top = 15
          Width = 520
          Height = 62
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label40: TLabel
            Left = 5
            Top = 0
            Width = 38
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Modelo:'
          end
          object Label41: TLabel
            Left = 121
            Top = 0
            Width = 27
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'S'#233'rie:'
          end
          object Label42: TLabel
            Left = 238
            Top = 0
            Width = 47
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Voltagem:'
          end
          object Label43: TLabel
            Left = 311
            Top = 0
            Width = 45
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pot'#234'ncia:'
          end
          object Label44: TLabel
            Left = 384
            Top = 0
            Width = 60
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Capacidade:'
          end
          object EdModelo: TdmkEdit
            Left = 5
            Top = 16
            Width = 112
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSerie: TdmkEdit
            Left = 121
            Top = 16
            Width = 112
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdVoltagem: TdmkEdit
            Left = 238
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdPotencia: TdmkEdit
            Left = 311
            Top = 16
            Width = 68
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCapacid: TdmkEdit
            Left = 384
            Top = 16
            Width = 112
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
    end
    object Panel19: TPanel
      Left = 0
      Top = 301
      Width = 228
      Height = 195
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 4
      object Panel20: TPanel
        Left = 0
        Top = 0
        Width = 228
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Observa'#231#245'es:'
        TabOrder = 0
      end
      object MeObserva: TdmkMemo
        Left = 0
        Top = 25
        Width = 228
        Height = 170
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Align = alClient
        TabOrder = 1
        UpdType = utYes
      end
    end
    object GroupBox9: TGroupBox
      Left = 0
      Top = 236
      Width = 993
      Height = 65
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Dados complementares:  '
      TabOrder = 3
      object Panel21: TPanel
        Left = 2
        Top = 15
        Width = 989
        Height = 48
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label48: TLabel
          Left = 8
          Top = 2
          Width = 117
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Unidade de Medida [F3]:'
        end
        object SpeedButton5: TSpeedButton
          Left = 463
          Top = 18
          Width = 21
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object Label49: TLabel
          Left = 488
          Top = 2
          Width = 27
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'NCM:'
        end
        object SpeedButton6: TSpeedButton
          Left = 583
          Top = 18
          Width = 21
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '?'
          OnClick = SpeedButton6Click
        end
        object Label57: TLabel
          Left = 612
          Top = 2
          Width = 71
          Height = 13
          Caption = 'Dias livres (D-):'
          FocusControl = DBEdit35
        end
        object EdUnidMed: TdmkEditCB
          Left = 8
          Top = 18
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdUnidMedChange
          OnKeyDown = EdUnidMedKeyDown
          DBLookupComboBox = CBUnidMed
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdSigla: TdmkEdit
          Left = 64
          Top = 18
          Width = 71
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSiglaChange
          OnExit = EdSiglaExit
          OnKeyDown = EdSiglaKeyDown
        end
        object CBUnidMed: TdmkDBLookupComboBox
          Left = 137
          Top = 18
          Width = 325
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsUnidMed
          TabOrder = 2
          OnKeyDown = CBUnidMedKeyDown
          dmkEditCB = EdUnidMed
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNCM: TdmkEdit
          Left = 488
          Top = 18
          Width = 92
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdDMenos: TdmkEdit
          Left = 612
          Top = 18
          Width = 73
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 940
      Top = 0
      Width = 53
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 293
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 129
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtConserto: TBitBtn
        Tag = 10090
        Left = 211
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtConsertoClick
      end
      object BtPanSit: TBitBtn
        Tag = 1000155
        Left = 251
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtPanSitClick
      end
    end
    object GB_M: TGroupBox
      Left = 293
      Top = 0
      Width = 647
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 285
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Patrim'#244'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 285
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Patrim'#244'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 285
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Patrim'#244'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 993
    Height = 37
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 989
      Height = 20
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 944
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Para pesquisar o campo Agrupador pela Refer'#234'ncia + Nome precione' +
          ' F5 e para pesquisar somente pelo Nome precione F6 (Dispon'#237'vel a' +
          'penas no modo de edi'#231#227'o).'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 944
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Para pesquisar o campo Agrupador pela Refer'#234'ncia + Nome precione' +
          ' F5 e para pesquisar somente pelo Nome precione F6 (Dispon'#237'vel a' +
          'penas no modo de edi'#231#227'o).'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGraGXPatrBeforeOpen
    AfterOpen = QrGraGXPatrAfterOpen
    BeforeClose = QrGraGXPatrBeforeClose
    AfterScroll = QrGraGXPatrAfterScroll
    OnCalcFields = QrGraGXPatrCalcFields
    SQL.Strings = (
      'SELECT CHAR(cpl.Situacao) SIT_CHR,'
      'ggx.Controle, gg1.Referencia,'
      'gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, '
      'gfm.Nome NO_MARCA, gfc.Nome NO_FABR, '
      'IF(cpl.GraGruX IS NULL, 0, 1) CPL_EXISTE, '
      'gg2.Nome NO_GG2, gg3.Nome NO_GG3, '
      'gg4.Nome NO_GG4, gg5.Nome NO_GG5, '
      'ggt.Nome NO_GGT, gg1.PrdGrupTip, '
      'gg1.Nivel1, gg1.Nivel2, gg1.Nivel3, '
      'gg1.Nivel4, gg1.Nivel5, '
      'gg5.CodUsu CUNivel5, gg4.CodUsu CUNivel4,'
      'gg3.CodUsu CUNivel3, gg2.CodUsu CUNivel2,'
      'gg1.CodUsu, cpl.* '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2'
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3'
      'LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg1.Nivel4'
      'LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5'
      'LEFT JOIN prdgruptip ggt ON ggt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle'
      'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo'
      ''
      'WHERE  gg1.PrdGrupTip=1'
      'AND gg1.Nivel1>0'
      '')
    Left = 64
    Top = 64
    object QrGraGXPatrGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragxpatr.GraGruX'
    end
    object QrGraGXPatrNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrGraGXPatrMarca: TIntegerField
      FieldName = 'Marca'
      Origin = 'gragxpatr.Marca'
    end
    object QrGraGXPatrNO_MARCA: TWideStringField
      FieldName = 'NO_MARCA'
      Origin = 'grafabmar.Nome'
      Size = 60
    end
    object QrGraGXPatrNO_FABR: TWideStringField
      FieldName = 'NO_FABR'
      Origin = 'grafabcad.Nome'
      Size = 60
    end
    object QrGraGXPatrComplem: TWideStringField
      FieldName = 'Complem'
      Origin = 'gragxpatr.Complem'
      Size = 60
    end
    object QrGraGXPatrAquisData: TDateField
      FieldName = 'AquisData'
      Origin = 'gragxpatr.AquisData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrGraGXPatrAquisDocu: TWideStringField
      FieldName = 'AquisDocu'
      Origin = 'gragxpatr.AquisDocu'
      Size = 60
    end
    object QrGraGXPatrAquisValr: TFloatField
      FieldName = 'AquisValr'
      Origin = 'gragxpatr.AquisValr'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGraGXPatrSituacao: TWordField
      FieldName = 'Situacao'
      Origin = 'gragxpatr.Situacao'
    end
    object QrGraGXPatrAtualValr: TFloatField
      FieldName = 'AtualValr'
      Origin = 'gragxpatr.AtualValr'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGraGXPatrValorMes: TFloatField
      FieldName = 'ValorMes'
      Origin = 'gragxpatr.ValorMes'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGraGXPatrValorQui: TFloatField
      FieldName = 'ValorQui'
      Origin = 'gragxpatr.ValorQui'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGraGXPatrValorSem: TFloatField
      FieldName = 'ValorSem'
      Origin = 'gragxpatr.ValorSem'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGraGXPatrValorDia: TFloatField
      FieldName = 'ValorDia'
      Origin = 'gragxpatr.ValorDia'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGraGXPatrAgrupador: TIntegerField
      FieldName = 'Agrupador'
      Origin = 'gragxpatr.Agrupador'
    end
    object QrGraGXPatrModelo: TWideStringField
      FieldName = 'Modelo'
      Origin = 'gragxpatr.Modelo'
      Size = 60
    end
    object QrGraGXPatrSerie: TWideStringField
      FieldName = 'Serie'
      Origin = 'gragxpatr.Serie'
      Size = 60
    end
    object QrGraGXPatrVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Origin = 'gragxpatr.Voltagem'
      Size = 30
    end
    object QrGraGXPatrPotencia: TWideStringField
      FieldName = 'Potencia'
      Origin = 'gragxpatr.Potencia'
      Size = 30
    end
    object QrGraGXPatrCapacid: TWideStringField
      FieldName = 'Capacid'
      Origin = 'gragxpatr.Capacid'
      Size = 30
    end
    object QrGraGXPatrVendaData: TDateField
      FieldName = 'VendaData'
      Origin = 'gragxpatr.VendaData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrGraGXPatrVendaDocu: TWideStringField
      FieldName = 'VendaDocu'
      Origin = 'gragxpatr.VendaDocu'
      Size = 60
    end
    object QrGraGXPatrVendaValr: TFloatField
      FieldName = 'VendaValr'
      Origin = 'gragxpatr.VendaValr'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGraGXPatrVendaEnti: TIntegerField
      FieldName = 'VendaEnti'
      Origin = 'gragxpatr.VendaEnti'
    end
    object QrGraGXPatrObserva: TWideStringField
      FieldName = 'Observa'
      Origin = 'gragxpatr.Observa'
      Size = 255
    end
    object QrGraGXPatrAGRPAT: TWideStringField
      FieldName = 'AGRPAT'
      Origin = 'gragxpatr.AGRPAT'
      Size = 25
    end
    object QrGraGXPatrCLVPAT: TWideStringField
      FieldName = 'CLVPAT'
      Origin = 'gragxpatr.CLVPAT'
      Size = 25
    end
    object QrGraGXPatrVENDADATA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENDADATA_TXT'
      Size = 8
      Calculated = True
    end
    object QrGraGXPatrSIT_CHR: TWideStringField
      FieldName = 'SIT_CHR'
      Size = 4
    end
    object QrGraGXPatrMARPAT: TWideStringField
      FieldName = 'MARPAT'
      Origin = 'gragxpatr.MARPAT'
      Size = 10
    end
    object QrGraGXPatrControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gragrux.Controle'
    end
    object QrGraGXPatrCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
      Required = True
    end
    object QrGraGXPatrReferencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrGraGXPatrAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrGraGXPatrCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
    end
    object QrGraGXPatrNO_GG2: TWideStringField
      FieldName = 'NO_GG2'
      Size = 30
    end
    object QrGraGXPatrNO_GG3: TWideStringField
      FieldName = 'NO_GG3'
      Size = 30
    end
    object QrGraGXPatrNO_GG4: TWideStringField
      FieldName = 'NO_GG4'
      Size = 30
    end
    object QrGraGXPatrNO_GG5: TWideStringField
      FieldName = 'NO_GG5'
      Size = 30
    end
    object QrGraGXPatrNO_GGT: TWideStringField
      FieldName = 'NO_GGT'
      Size = 30
    end
    object QrGraGXPatrTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Size = 15
    end
    object QrGraGXPatrTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 15
    end
    object QrGraGXPatrTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 15
    end
    object QrGraGXPatrTitNiv4: TWideStringField
      FieldName = 'TitNiv4'
      Size = 15
    end
    object QrGraGXPatrTitNiv5: TWideStringField
      FieldName = 'TitNiv5'
      Size = 15
    end
    object QrGraGXPatrPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGXPatrNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGXPatrNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGXPatrNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGXPatrNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrGraGXPatrNivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrGraGXPatrCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGXPatrNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGXPatrUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGXPatrCUNivel5: TIntegerField
      FieldName = 'CUNivel5'
    end
    object QrGraGXPatrCUNivel4: TIntegerField
      FieldName = 'CUNivel4'
    end
    object QrGraGXPatrCUNivel3: TIntegerField
      FieldName = 'CUNivel3'
    end
    object QrGraGXPatrCUNivel2: TIntegerField
      FieldName = 'CUNivel2'
    end
    object QrGraGXPatrAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraGXPatrPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 25
    end
    object QrGraGXPatrEstqSdo: TFloatField
      FieldName = 'EstqSdo'
    end
    object QrGraGXPatrValorFDS: TFloatField
      FieldName = 'ValorFDS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGraGXPatrDMenos: TIntegerField
      FieldName = 'DMenos'
    end
  end
  object DsGraGXPatr: TDataSource
    DataSet = QrGraGXPatr
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    CanUpd01 = BtIts
    Left = 176
    Top = 64
  end
  object QrMarcas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gfm.Controle, CONCAT(gfm.Nome, " [", '
      'gfc.Nome, "]") NO_MARCA_FABR'
      'FROM grafabmar gfm '
      'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo'
      'ORDER BY NO_MARCA_FABR')
    Left = 204
    Top = 64
    object QrMarcasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMarcasNO_MARCA_FABR: TWideStringField
      FieldName = 'NO_MARCA_FABR'
      Size = 123
    end
  end
  object DsMarcas: TDataSource
    DataSet = QrMarcas
    Left = 232
    Top = 64
  end
  object QrAgrupador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Nome, gg1.Referencia,'
      'CONCAT(gg1.Referencia, " - ", gg1.Nome) NOMEREF_TXT'
      'FROM gragxpatr gxp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=gxp.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'ORDER BY gg1.Nome')
    Left = 264
    Top = 64
    object QrAgrupadorControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gragrux.Controle'
    end
    object QrAgrupadorNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrAgrupadorReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrAgrupadorNOMEREF_TXT: TWideStringField
      FieldName = 'NOMEREF_TXT'
      Size = 148
    end
  end
  object DsAgrupador: TDataSource
    DataSet = QrAgrupador
    Left = 292
    Top = 64
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 604
    Top = 532
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 520
    Top = 532
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Editaniveis1: TMenuItem
      Caption = 'Edita &n'#237'veis'
      OnClick = Editaniveis1Click
    end
    object Editadadosdoproduto1: TMenuItem
      Caption = 'Edita dados do &produto'
      OnClick = Editadadosdoproduto1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Recalculaestoque1: TMenuItem
      Caption = 'Recalcula estoque'
      OnClick = Recalculaestoque1Click
    end
  end
  object QrGraGXPIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggo.GraGruX, ggo.Aplicacao, ggo.ItemValr, '
      'gg1.Referencia, gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1,'
      'ELT(ggo.Aplicacao, "Acess'#243'rio", "Uso", "Consumo",'
      '"? ? ?") NO_APLICACAO, gxi.*'
      'FROM gragxpits gxi'
      'LEFT JOIN gragxoutr ggo ON ggo.GraGruX=gxi.GraGXOutr'
      'LEFT JOIN gragrux ggx ON ggx.Controle=gxi.GraGXOutr'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE gxi.GraGXPatr=:P0'
      'ORDER BY NO_APLICACAO, NO_GG1'
      '')
    Left = 120
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGXPItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGXPItsAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrGraGXPItsItemValr: TFloatField
      FieldName = 'ItemValr'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGraGXPItsReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXPItsCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
    end
    object QrGraGXPItsNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXPItsNO_APLICACAO: TWideStringField
      FieldName = 'NO_APLICACAO'
      Size = 9
    end
    object QrGraGXPItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrGraGXPItsGraGXPatr: TIntegerField
      FieldName = 'GraGXPatr'
    end
    object QrGraGXPItsGraGXOutr: TIntegerField
      FieldName = 'GraGXOutr'
    end
    object QrGraGXPItsQuantidade: TIntegerField
      FieldName = 'Quantidade'
    end
  end
  object DsGraGXPIts: TDataSource
    DataSet = QrGraGXPIts
    Left = 148
    Top = 64
  end
  object QrAgrup: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Nome, gg1.Referencia'
      'FROM gragxpatr gxp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=gxp.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 324
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAgrupControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gragrux.Controle'
    end
    object QrAgrupNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrAgrupReferencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
  end
  object DsAgrup: TDataSource
    DataSet = QrAgrup
    Left = 352
    Top = 64
  end
  object QrLocod: TMySQLQuery
    Database = Dmod.MyDB
    Left = 804
    Top = 12
  end
  object QrGraGLSitu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Sigla'
      'FROM graglsitu'
      'ORDER BY Nome')
    Left = 380
    Top = 64
    object QrGraGLSituCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraGLSituNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGLSituSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 1
    end
  end
  object DsGraGLSitu: TDataSource
    DataSet = QrGraGLSitu
    Left = 408
    Top = 64
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  cpl.GraGruX=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 704
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  gg1.Referencia=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 676
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 732
    Top = 416
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 760
    Top = 416
  end
  object VUUnidMed: TdmkValUsu
    dmkEditCB = EdUnidMed
    Panel = Panel21
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 705
    Top = 416
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 460
    Top = 443
  end
end
