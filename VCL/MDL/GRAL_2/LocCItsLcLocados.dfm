object FmLocCItsLcLocados: TFmLocCItsLcLocados
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-112 :: Loca'#231#245'es Ativas de Equipamento'
  ClientHeight = 629
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1168
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 403
        Height = 32
        Caption = 'Loca'#231#245'es Ativas de Equipamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 403
        Height = 32
        Caption = 'Loca'#231#245'es Ativas de Equipamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 403
        Height = 32
        Caption = 'Loca'#231#245'es Ativas de Equipamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1264
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1260
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1264
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 1118
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1116
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1264
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1264
      Height = 81
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Identifica'#231#227'o: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label2: TLabel
        Left = 62
        Top = 16
        Width = 55
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Refer'#234'ncia:'
        FocusControl = DBEdNome
      end
      object Label6: TLabel
        Left = 294
        Top = 58
        Width = 33
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Marca:'
        FocusControl = DBEdit4
      end
      object Label8: TLabel
        Left = 489
        Top = 58
        Width = 53
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fabricante:'
        FocusControl = DBEdit5
      end
      object Label3: TLabel
        Left = 8
        Top = 58
        Width = 67
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Complemento:'
        FocusControl = DBEdit1
      end
      object Label12: TLabel
        Left = 738
        Top = 16
        Width = 45
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Situa'#231#227'o:'
        FocusControl = DBEdit7
      end
      object Label25: TLabel
        Left = 8
        Top = 16
        Width = 48
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Reduzido:'
      end
      object Label1: TLabel
        Left = 684
        Top = 16
        Width = 35
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#237'vel1:'
      end
      object Label46: TLabel
        Left = 271
        Top = 16
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = dmkDBEdit1
      end
      object Label51: TLabel
        Left = 195
        Top = 16
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = dmkDBEdit2
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 31
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Controle'
        DataSource = DsGraGXPatr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 62
        Top = 31
        Width = 130
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Referencia'
        DataSource = DsGraGXPatr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit4: TDBEdit
        Left = 334
        Top = 53
        Width = 151
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_MARCA'
        DataSource = DsGraGXPatr
        TabOrder = 2
      end
      object DBEdit5: TDBEdit
        Left = 546
        Top = 53
        Width = 207
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_FABR'
        DataSource = DsGraGXPatr
        TabOrder = 3
      end
      object DBEdit1: TDBEdit
        Left = 80
        Top = 53
        Width = 211
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Complem'
        DataSource = DsGraGXPatr
        TabOrder = 4
      end
      object DBEdit7: TDBEdit
        Left = 738
        Top = 31
        Width = 36
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Situacao'
        DataSource = DsGraGXPatr
        TabOrder = 5
      end
      object DBEdit19: TDBEdit
        Left = 773
        Top = 31
        Width = 30
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'SIT_CHR'
        DataSource = DsGraGXPatr
        TabOrder = 6
      end
      object DBEdit20: TDBEdit
        Left = 684
        Top = 31
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'COD_GG1'
        DataSource = DsGraGXPatr
        TabOrder = 7
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 271
        Top = 31
        Width = 410
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NO_GG1'
        DataSource = DsGraGXPatr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBRGAplicacao: TDBRadioGroup
        Left = 808
        Top = 16
        Width = 293
        Height = 60
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Aplica'#231#227'o: '
        Columns = 2
        DataField = 'Aplicacao'
        DataSource = DsGraGXPatr
        Items.Strings = (
          'Inativo'
          'Principal'
          'Secund'#225'rio')
        TabOrder = 9
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object DBCkAtivo: TDBCheckBox
        Left = 756
        Top = 56
        Width = 49
        Height = 17
        Caption = 'Ativo?'
        DataField = 'Ativo'
        DataSource = DsGraGXPatr
        TabOrder = 10
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 195
        Top = 31
        Width = 74
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Patrimonio'
        DataSource = DsGraGXPatr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 11
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 81
      Width = 1264
      Height = 386
      ActivePage = TabSheet4
      Align = alClient
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = ' Equipamento selecionado '
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1256
          Height = 358
          Align = alClient
          DataSource = DsSelecionado
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Loca'#231#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Nome do equipamento'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeProduto'
              Title.Caption = 'Qtd loca'#231#227'o'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeDevolucao'
              Title.Caption = 'Qtd devolvido'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdLocado'
              Title.Caption = 'Qtd a devolver'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOENT'
              Title.Caption = 'Nome do cliente'
              Width = 252
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrLocado'
              Title.Caption = 'Locado em'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrRetorn'
              Title.Caption = 'Previs'#227'o devolu'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CalcAdiLOCACAO'
              Title.Caption = 'Cobran'#231'a adiantada'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COBRANCALOCACAO'
              Title.Caption = 'Cobran'#231'a p'#243's loca'#231#227'o'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CtrID'
              Title.Caption = 'ID Movim.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Item'
              Title.Caption = 'ID item'
              Width = 56
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Equipamentos DISPON'#205'VEIS '
        ImageIndex = 1
        object DBGrid3: TDBGrid
          Left = 0
          Top = 0
          Width = 1256
          Height = 358
          Align = alClient
          DataSource = DsDisponiveis
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Nome do equipamento'
              Width = 475
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Patrimonio'
              Title.Caption = 'Patrim'#244'nio'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Referencia'
              Title.Caption = 'Refer'#234'ncia'
              Width = 180
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Equipamentos LOCADOS '
        ImageIndex = 2
        object DBGLocados: TDBGrid
          Left = 0
          Top = 0
          Width = 1256
          Height = 358
          Align = alClient
          DataSource = DsLocados
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGLocadosDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Loca'#231#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Patrimonio'
              Title.Caption = 'Patrim'#244'nio'
              Width = 57
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Nome do equipamento'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeProduto'
              Title.Caption = 'Qtd loca'#231#227'o'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeDevolucao'
              Title.Caption = 'Qtd devolvido'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdLocado'
              Title.Caption = 'Qtd a devolver'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOENT'
              Title.Caption = 'Nome do cliente'
              Width = 252
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrLocado'
              Title.Caption = 'Locado em'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrRetorn'
              Title.Caption = 'Previs'#227'o devolu'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CalcAdiLOCACAO'
              Title.Caption = 'Cobran'#231'a adiantada'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COBRANCALOCACAO'
              Title.Caption = 'Cobran'#231'a p'#243's loca'#231#227'o'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CtrID'
              Title.Caption = 'ID Movim.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Item'
              Title.Caption = 'ID item'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Referencia'
              Visible = True
            end>
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Aloca'#231#245'es e desaloca'#231#245'es'
        ImageIndex = 3
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 1256
          Height = 358
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitTop = 48
          ExplicitWidth = 1008
          ExplicitHeight = 385
          object Splitter1: TSplitter
            Left = 847
            Top = 0
            Width = 5
            Height = 358
            Align = alRight
            ExplicitLeft = 849
          end
          object dmkDBGridZTO1: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 847
            Height = 358
            Align = alClient
            DataSource = DsStqAlocIts
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 47
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TpMov_TXT'
                Title.Caption = 'Movimento'
                Width = 76
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GGX_SMIA'
                Title.Caption = 'Red.Desal.'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'REFERENCIA_S'
                Title.Caption = 'Refer'#234'ncia desaloca'#231#227'o'
                Width = 86
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome_S'
                Title.Caption = 'Nome desalocado'
                Width = 180
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CustoAll'
                Title.Caption = '$ total'
                Visible = True
              end>
          end
          object DBMemo1: TDBMemo
            Left = 852
            Top = 0
            Width = 404
            Height = 358
            Align = alRight
            DataField = 'Historico'
            DataSource = DsStqAlocIts
            TabOrder = 1
          end
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 28
    Top = 65527
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CHAR(cpl.Situacao) SIT_CHR, ggx.Controle, '
      'gg1.Patrimonio, gg1.Referencia, gg1.NCM, gg1.UnidMed, '
      'gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, '
      'gfm.Nome NO_MARCA, gfc.Nome NO_FABR, '
      'IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, '
      'gg2.Nome NO_GG2, gg3.Nome NO_GG3, '
      'gg4.Nome NO_GG4, gg5.Nome NO_GG5, ggt.Nome NO_GGT, '
      'ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3, ggt.TitNiv4, '
      'ggt.TitNiv5, gg1.PrdGrupTip, gg1.Nivel1, gg1.Nivel2, '
      'gg1.Nivel3, gg1.Nivel4, gg1.Nivel5, '
      'gg5.CodUsu CUNivel5, gg4.CodUsu CUNivel4, '
      'gg3.CodUsu CUNivel3, gg2.CodUsu CUNivel2, '
      'gg1.CodUsu, cpl.* '
      'FROM gragxpatr cpl  '
      'LEFT JOIN gragrux ggx ON cpl.GraGruX=ggx.Controle'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2 '
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3 '
      'LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg1.Nivel4 '
      'LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5 '
      'LEFT JOIN prdgruptip ggt ON ggt.Codigo=gg1.PrdGrupTip '
      'LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle '
      'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo '
      'LEFT JOIN gragruy ggy ON ggy.Codigo=ggx.Controle '
      'WHERE ggx.Controle=14')
    Left = 408
    Top = 44
    object QrGraGXPatrSIT_CHR: TWideStringField
      FieldName = 'SIT_CHR'
      Size = 4
    end
    object QrGraGXPatrControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGraGXPatrPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 25
    end
    object QrGraGXPatrReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXPatrNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGXPatrUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrGraGXPatrCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
      Required = True
    end
    object QrGraGXPatrNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Required = True
      Size = 120
    end
    object QrGraGXPatrNO_MARCA: TWideStringField
      FieldName = 'NO_MARCA'
      Required = True
      Size = 60
    end
    object QrGraGXPatrNO_FABR: TWideStringField
      FieldName = 'NO_FABR'
      Required = True
      Size = 60
    end
    object QrGraGXPatrCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
      Required = True
    end
    object QrGraGXPatrNO_GG2: TWideStringField
      FieldName = 'NO_GG2'
      Required = True
      Size = 30
    end
    object QrGraGXPatrNO_GG3: TWideStringField
      FieldName = 'NO_GG3'
      Required = True
      Size = 30
    end
    object QrGraGXPatrNO_GG4: TWideStringField
      FieldName = 'NO_GG4'
      Required = True
      Size = 30
    end
    object QrGraGXPatrNO_GG5: TWideStringField
      FieldName = 'NO_GG5'
      Required = True
      Size = 30
    end
    object QrGraGXPatrNO_GGT: TWideStringField
      FieldName = 'NO_GGT'
      Size = 60
    end
    object QrGraGXPatrTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Size = 30
    end
    object QrGraGXPatrTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 30
    end
    object QrGraGXPatrTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 30
    end
    object QrGraGXPatrTitNiv4: TWideStringField
      FieldName = 'TitNiv4'
      Size = 30
    end
    object QrGraGXPatrTitNiv5: TWideStringField
      FieldName = 'TitNiv5'
      Size = 30
    end
    object QrGraGXPatrPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGXPatrNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGXPatrNivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGXPatrNivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGXPatrNivel4: TIntegerField
      FieldName = 'Nivel4'
      Required = True
    end
    object QrGraGXPatrNivel5: TIntegerField
      FieldName = 'Nivel5'
      Required = True
    end
    object QrGraGXPatrCUNivel5: TIntegerField
      FieldName = 'CUNivel5'
      Required = True
    end
    object QrGraGXPatrCUNivel4: TIntegerField
      FieldName = 'CUNivel4'
      Required = True
    end
    object QrGraGXPatrCUNivel3: TIntegerField
      FieldName = 'CUNivel3'
      Required = True
    end
    object QrGraGXPatrCUNivel2: TIntegerField
      FieldName = 'CUNivel2'
      Required = True
    end
    object QrGraGXPatrCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrGraGXPatrGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrGraGXPatrComplem: TWideStringField
      FieldName = 'Complem'
      Size = 60
    end
    object QrGraGXPatrAquisData: TDateField
      FieldName = 'AquisData'
      Required = True
    end
    object QrGraGXPatrAquisDocu: TWideStringField
      FieldName = 'AquisDocu'
      Size = 60
    end
    object QrGraGXPatrAquisValr: TFloatField
      FieldName = 'AquisValr'
      Required = True
    end
    object QrGraGXPatrSituacao: TWordField
      FieldName = 'Situacao'
      Required = True
    end
    object QrGraGXPatrAtualValr: TFloatField
      FieldName = 'AtualValr'
      Required = True
    end
    object QrGraGXPatrValorMes: TFloatField
      FieldName = 'ValorMes'
      Required = True
    end
    object QrGraGXPatrValorQui: TFloatField
      FieldName = 'ValorQui'
      Required = True
    end
    object QrGraGXPatrValorSem: TFloatField
      FieldName = 'ValorSem'
      Required = True
    end
    object QrGraGXPatrValorDia: TFloatField
      FieldName = 'ValorDia'
      Required = True
    end
    object QrGraGXPatrAgrupador: TIntegerField
      FieldName = 'Agrupador'
      Required = True
    end
    object QrGraGXPatrMarca: TIntegerField
      FieldName = 'Marca'
      Required = True
    end
    object QrGraGXPatrModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 60
    end
    object QrGraGXPatrSerie: TWideStringField
      FieldName = 'Serie'
      Size = 60
    end
    object QrGraGXPatrVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Size = 30
    end
    object QrGraGXPatrPotencia: TWideStringField
      FieldName = 'Potencia'
      Size = 30
    end
    object QrGraGXPatrCapacid: TWideStringField
      FieldName = 'Capacid'
      Size = 30
    end
    object QrGraGXPatrVendaData: TDateField
      FieldName = 'VendaData'
      Required = True
    end
    object QrGraGXPatrVendaDocu: TWideStringField
      FieldName = 'VendaDocu'
      Size = 60
    end
    object QrGraGXPatrVendaValr: TFloatField
      FieldName = 'VendaValr'
      Required = True
    end
    object QrGraGXPatrVendaEnti: TIntegerField
      FieldName = 'VendaEnti'
      Required = True
    end
    object QrGraGXPatrObserva: TWideStringField
      FieldName = 'Observa'
      Size = 510
    end
    object QrGraGXPatrAGRPAT: TWideStringField
      FieldName = 'AGRPAT'
      Size = 25
    end
    object QrGraGXPatrCLVPAT: TWideStringField
      FieldName = 'CLVPAT'
      Size = 25
    end
    object QrGraGXPatrMARPAT: TWideStringField
      FieldName = 'MARPAT'
      Size = 60
    end
    object QrGraGXPatrAplicacao: TIntegerField
      FieldName = 'Aplicacao'
      Required = True
    end
    object QrGraGXPatrDescrTerc: TWideStringField
      FieldName = 'DescrTerc'
      Size = 60
    end
    object QrGraGXPatrValorFDS: TFloatField
      FieldName = 'ValorFDS'
      Required = True
    end
    object QrGraGXPatrDMenos: TSmallintField
      FieldName = 'DMenos'
      Required = True
    end
    object QrGraGXPatrItemUnid: TIntegerField
      FieldName = 'ItemUnid'
      Required = True
    end
    object QrGraGXPatrLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrGraGXPatrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGXPatrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGXPatrUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrGraGXPatrUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrGraGXPatrAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGraGXPatrAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrGraGXPatrAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrGraGXPatrAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsGraGXPatr: TDataSource
    DataSet = QrGraGXPatr
    Left = 480
    Top = 44
  end
  object QrSelecionado: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ' SELECT pri.QtdeLocacao-pri.QtdeDevolucao QtdLocado, '
      ' IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOENT, '
      ' pri.Codigo, pri.CtrID, pri.Item, pri.DtHrLocado, '
      ' pri.DtHrRetorn, QtdeProduto, QtdeDevolucao,  '
      ' CalcAdiLOCACAO, COBRANCALOCACAO  '
      ' FROM loccitslca pri    '
      ' LEFT JOIN loccconcab cab ON cab.Codigo=pri.Codigo   '
      ' LEFT JOIN entidades ent ON ent.Codigo=cab.Cliente '
      ' WHERE pri.GraGrux=14 '
      ' AND pri.QtdeDevolucao < pri.QtdeLocacao    '
      ' AND cab.DtHrBxa < "1900-01-01"    '
      ' AND cab.STATUS=2    ')
    Left = 172
    Top = 288
    object QrSelecionadoQtdLocado: TLargeintField
      FieldName = 'QtdLocado'
      Required = True
    end
    object QrSelecionadoNOENT: TWideStringField
      FieldName = 'NOENT'
      Size = 100
    end
    object QrSelecionadoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSelecionadoCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrSelecionadoItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrSelecionadoDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
      Required = True
    end
    object QrSelecionadoDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
      Required = True
    end
    object QrSelecionadoQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrSelecionadoQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
    end
    object QrSelecionadoCalcAdiLOCACAO: TWideStringField
      FieldName = 'CalcAdiLOCACAO'
      Size = 40
    end
    object QrSelecionadoCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 40
    end
    object QrSelecionadoNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsSelecionado: TDataSource
    DataSet = QrSelecionado
    Left = 173
    Top = 336
  end
  object QrDisponiveis: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ' SELECT pri.QtdeLocacao-pri.QtdeDevolucao QtdLocado, '
      ' IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOENT, '
      ' pri.Codigo, pri.CtrID, pri.Item, pri.DtHrLocado, '
      ' pri.DtHrRetorn, QtdeProduto, QtdeDevolucao,  '
      ' CalcAdiLOCACAO, COBRANCALOCACAO  '
      ' FROM loccitslca pri    '
      ' LEFT JOIN loccconcab cab ON cab.Codigo=pri.Codigo   '
      ' LEFT JOIN entidades ent ON ent.Codigo=cab.Cliente '
      ' WHERE pri.GraGrux=14 '
      ' AND pri.QtdeDevolucao < pri.QtdeLocacao    '
      ' AND cab.DtHrBxa < "1900-01-01"    '
      ' AND cab.STATUS=2    ')
    Left = 356
    Top = 308
    object QrDisponiveisControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrDisponiveisNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrDisponiveisNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrDisponiveisPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 25
    end
    object QrDisponiveisReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 60
    end
  end
  object DsDisponiveis: TDataSource
    DataSet = QrDisponiveis
    Left = 357
    Top = 356
  end
  object QrLcs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 628
    Top = 381
  end
  object QrLocados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ' SELECT ggx.Controle, gg1.Nivel2, gg1.Nome,'
      ' pri.QtdeLocacao-pri.QtdeDevolucao QtdLocado,'
      ' IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOENT,'
      ' pri.Codigo, pri.CtrID, pri.Item, pri.DtHrLocado,'
      ' pri.DtHrRetorn, QtdeProduto, QtdeDevolucao,'
      ' CalcAdiLOCACAO, COBRANCALOCACAO'
      ' FROM loccitslca pri'
      ' LEFT JOIN loccconcab cab ON cab.Codigo=pri.Codigo'
      ' LEFT JOIN entidades ent ON ent.Codigo=cab.Cliente'
      ' LEFT JOIN gragrux ggx ON pri.GraGruX=ggx.Controle'
      ' LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      ' /* WHERE pri.GraGrux=14 */'
      ' WHERE gg1.Nivel2=3003'
      ' AND ggx.Ativo=1'
      ' AND pri.QtdeDevolucao < pri.QtdeLocacao'
      ' AND cab.DtHrBxa < "1900-01-01"'
      ' AND cab.STATUS=2'
      ' ORDER BY ggx.Controle, DtHrRetorn, gg1.Nome')
    Left = 500
    Top = 292
    object QrLocadosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocadosNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrLocadosPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 25
    end
    object QrLocadosReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 60
    end
    object QrLocadosNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrLocadosQtdLocado: TLargeintField
      FieldName = 'QtdLocado'
      Required = True
    end
    object QrLocadosNOENT: TWideStringField
      FieldName = 'NOENT'
      Size = 100
    end
    object QrLocadosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocadosCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrLocadosItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrLocadosDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
      Required = True
    end
    object QrLocadosDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
      Required = True
    end
    object QrLocadosQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrLocadosQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
    end
    object QrLocadosCalcAdiLOCACAO: TWideStringField
      FieldName = 'CalcAdiLOCACAO'
      Size = 40
    end
    object QrLocadosCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 40
    end
  end
  object DsLocados: TDataSource
    DataSet = QrLocados
    Left = 501
    Top = 340
  end
  object QrStqAlocIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      
        'gg1a.REFERENCIA REFERENCIA_A, ggxa.EAN13 EAN13_A, gg1a.Nome Nome' +
        '_A,'
      
        'gg1s.REFERENCIA REFERENCIA_S, ggxs.EAN13 EAN13_S, gg1s.Nome Nome' +
        '_S,'
      'sai.*'
      'FROM stqalocits sai '
      'LEFT JOIN gragrux ggxa ON ggxa.Controle=sai.GGX_Aloc'
      'LEFT JOIN gragru1 gg1a ON gg1a.Nivel1=ggxa.GraGru1 '
      'LEFT JOIN gragrux ggxs ON ggxs.Controle=sai.GGX_SMIA'
      'LEFT JOIN gragru1 gg1s ON gg1s.Nivel1=ggxs.GraGru1 ')
    Left = 812
    Top = 232
    object QrStqAlocItsREFERENCIA_A: TWideStringField
      FieldName = 'REFERENCIA_A'
      Size = 25
    end
    object QrStqAlocItsEAN13_A: TWideStringField
      FieldName = 'EAN13_A'
      Size = 13
    end
    object QrStqAlocItsNome_A: TWideStringField
      FieldName = 'Nome_A'
      Size = 120
    end
    object QrStqAlocItsREFERENCIA_S: TWideStringField
      FieldName = 'REFERENCIA_S'
      Size = 25
    end
    object QrStqAlocItsEAN13_S: TWideStringField
      FieldName = 'EAN13_S'
      Size = 13
    end
    object QrStqAlocItsNome_S: TWideStringField
      FieldName = 'Nome_S'
      Size = 120
    end
    object QrStqAlocItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqAlocItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqAlocItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrStqAlocItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrStqAlocItsFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrStqAlocItsQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrStqAlocItsCustoAll: TFloatField
      FieldName = 'CustoAll'
      Required = True
    end
    object QrStqAlocItsBaixa: TSmallintField
      FieldName = 'Baixa'
      Required = True
    end
    object QrStqAlocItsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrStqAlocItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrStqAlocItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrStqAlocItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrStqAlocItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrStqAlocItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrStqAlocItsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrStqAlocItsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrStqAlocItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrStqAlocItsGGX_Aloc: TIntegerField
      FieldName = 'GGX_Aloc'
      Required = True
    end
    object QrStqAlocItsGGX_SMIA: TIntegerField
      FieldName = 'GGX_SMIA'
      Required = True
    end
    object QrStqAlocItsTpMov_TXT: TWideStringField
      FieldName = 'TpMov_TXT'
    end
    object QrStqAlocItsHistorico: TWideStringField
      FieldName = 'Historico'
      Size = 511
    end
  end
  object DsStqAlocIts: TDataSource
    DataSet = QrStqAlocIts
    Left = 812
    Top = 288
  end
end
