unit StqAlocGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, AppListas;

type
  THackDBGrid = class(TDBGrid);
  TFmStqAlocGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrStqAlocIts: TMySQLQuery;
    DsStqAlocIts: TDataSource;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    QrStqCenCadPsq: TMySQLQuery;
    QrStqCenCadPsqCodigo: TIntegerField;
    QrStqCenCadPsqNome: TWideStringField;
    DsStqCenCadPsq: TDataSource;
    Panel3: TPanel;
    Label1: TLabel;
    dmkLabel3: TdmkLabel;
    EdPesq: TdmkEdit;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    QrStqAlocItsREFERENCIA_A: TWideStringField;
    QrStqAlocItsEAN13_A: TWideStringField;
    QrStqAlocItsNome_A: TWideStringField;
    QrStqAlocItsREFERENCIA_S: TWideStringField;
    QrStqAlocItsEAN13_S: TWideStringField;
    QrStqAlocItsNome_S: TWideStringField;
    QrStqAlocItsCodigo: TIntegerField;
    QrStqAlocItsControle: TIntegerField;
    QrStqAlocItsEmpresa: TIntegerField;
    QrStqAlocItsDataHora: TDateTimeField;
    QrStqAlocItsFatID: TIntegerField;
    QrStqAlocItsQtde: TFloatField;
    QrStqAlocItsCustoAll: TFloatField;
    QrStqAlocItsBaixa: TSmallintField;
    QrStqAlocItsLk: TIntegerField;
    QrStqAlocItsDataCad: TDateField;
    QrStqAlocItsDataAlt: TDateField;
    QrStqAlocItsUserCad: TIntegerField;
    QrStqAlocItsUserAlt: TIntegerField;
    QrStqAlocItsAlterWeb: TSmallintField;
    QrStqAlocItsAWServerID: TIntegerField;
    QrStqAlocItsAWStatSinc: TSmallintField;
    QrStqAlocItsAtivo: TSmallintField;
    QrStqAlocItsGGX_Aloc: TIntegerField;
    QrStqAlocItsGGX_SMIA: TIntegerField;
    QrStqAlocItsTpMov_TXT: TWideStringField;
    EdLimReg: TdmkEdit;
    dmkLabel1: TdmkLabel;
    QrStqAlocItsHistorico: TWideStringField;
    DBMemo1: TDBMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure dmkDBGridZTO1DblClick(Sender: TObject);
    procedure EdLimRegRedefinido(Sender: TObject);
  private
    { Private declarations }
    function DoubleClick_GradeItens(Sender: TDBGrid): Boolean;

  public
    { Public declarations }
    procedure Pesquisa();
  end;

  var
  FmStqAlocGer: TFmStqAlocGer;

implementation

uses UnMyObjects, Module, DmkDAC_PF, StqAlocAdd, ModuleGeral, MyDBCheck,
  ModProd, UnGraL_Jan, UnGrade_Jan;

{$R *.DFM}

procedure TFmStqAlocGer.BtIncluiClick(Sender: TObject);
const
  AvisaEstqNeg = True;
  GeraLog = False;
var
  Codigo, Status, CtrID, Item(*, Status, GraGruX*): Integer;
  //SQLType: TSQLType;
  DtFimCobra: TDateTime;
begin
  if DBCheck.CriaFm(TFmStqAlocAdd, FmStqAlocAdd, afmoNegarComAviso) then
  begin
    FmStqAlocAdd.ImgTipo.SQLType := stIns;
    FmStqAlocAdd.ShowModal;
    FmStqAlocAdd.Destroy;
    Pesquisa();
  end;
end;

procedure TFmStqAlocGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqAlocGer.dmkDBGridZTO1DblClick(Sender: TObject);
begin
  if not DoubleClick_GradeItens(TDBGrid(Sender)) then
  begin
    // ...
  end;
end;

function TFmStqAlocGer.DoubleClick_GradeItens(Sender: TDBGrid): Boolean;
const
  sProcName = 'TFmStqAlocGer.DoubleClick_GradeItens()';
var
  Campo: String;
  Qry: TmySQLQuery;
  GraGruX, GraGruY, Nivel1: Integer;
begin
  Result := False;
  //
  Qry := TmySQLQuery(Sender.DataSource.DataSet);
  if (Qry <> nil) and (Qry.State <> dsInactive) and (Qry.RecordCount > 0) then
  begin
    Campo := Uppercase(Sender.Columns[THackDBGrid(Sender).Col -1].FieldName);
    if (Uppercase(Campo) = Uppercase('GGX_ALoc'))
    or (Uppercase(Campo) = Uppercase('GGX_SMIA'))
    or (Uppercase(Campo) = Uppercase('REFERENCIA_A'))
    or (Uppercase(Campo) = Uppercase('REFERENCIA_S')) then
    begin
      if (Uppercase(Campo) = Uppercase('GGX_ALoc'))
      or (Uppercase(Campo) = Uppercase('REFERENCIA_A')) then
      begin
        GraGruX := Qry.FieldByName('GGX_Aloc').AsInteger;
        GraGruY := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
      end else
      if (Uppercase(Campo) = Uppercase('GGX_SMIA'))
      or (Uppercase(Campo) = Uppercase('REFERENCIA_S')) then
      begin
        GraGruX := Qry.FieldByName('GGX_SMIA').AsInteger;
        GraGruY := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
      end;
      //
      case GraGruY of
        CO_GraGruY_1024_GXPatPri, // = Primario
        CO_GraGruY_2048_GXPatSec, // = Secund�rio
        CO_GraGruY_3072_GXPatAce: // = Acess�rio
        begin
          Result := True;
          GraL_Jan.MostraFormGraGXPatr(GraGruX);
        end;
        CO_GraGruY_4096_GXPatApo, // = Apoio
        CO_GraGruY_5120_GXPatUso,  // = Uso (desgaste)
        CO_GraGruY_6144_GXPrdCns:  // = Uso (consumo)
        begin
          GraL_Jan.MostraFormGraGXOutr(GraGruX);
          Result := True;
        end;
        CO_GraGruY_8192_GXPrdVen:// Produto de Venda
        begin
          GraL_Jan.MostraFormGraGXVend(GraGruX);
          Result := True;
        end;
        else
          Geral.MB_Erro('GraGruY (' + Geral.FF0(GraGruY) +
          ') n�o implementado em ' + sProcName);
          Nivel1 := DmProd.ObtemGraGru1DeGraGruX(GraGruX);
          Grade_Jan.MostraFormGraGruN(Nivel1);
      end;
    end;
  end;
end;

procedure TFmStqAlocGer.EdLimRegRedefinido(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmStqAlocGer.EdPesqChange(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmStqAlocGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqAlocGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmStqAlocGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqAlocGer.Pesquisa();
var
  Texto, Doc, SQL_Text, Masc: String;
  LimReg: Integer;
begin
  QrStqaLocIts.Close;
  LimReg := EdLimReg.ValueVariant;
  //
  Texto := EdPesq.ValueVariant;
  //
  if Texto <> '' then
    Texto := StringReplace(Texto, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
  //
  Texto := '%' + Texto + '%';
  if (Length(Texto) >= 2) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrStqaLocIts, Dmod.MyDB, [
    'SELECT ',
   'gg1a.REFERENCIA REFERENCIA_A, ggxa.EAN13 EAN13_A, gg1a.Nome Nome_A,',
   'gg1s.REFERENCIA REFERENCIA_S, ggxs.EAN13 EAN13_S, gg1s.Nome Nome_S,',
   'IF(Baixa=1, "Aloca��o", "Desaloca��o") TpMov_TXT, ',
   'sai.*',
   'FROM stqalocits sai ',
   'LEFT JOIN gragrux ggxa ON ggxa.Controle=sai.GGX_Aloc',
   'LEFT JOIN gragru1 gg1a ON gg1a.Nivel1=ggxa.GraGru1 ',
   'LEFT JOIN gragrux ggxs ON ggxs.Controle=sai.GGX_SMIA',
   'LEFT JOIN gragru1 gg1s ON gg1s.Nivel1=ggxs.GraGru1 ',
   'WHERE (',
    ' gg1a.Nome LIKE "' + Texto + '" ',
    ' OR gg1a.Referencia LIKE "' + Texto + '" ',
    ' OR gg1a.Patrimonio LIKE "' + Texto + '" ',
    ' OR ggxa.EAN13 LIKE "' + Texto + '" ',
    ') OR (',
    ' gg1s.Nome LIKE "' + Texto + '" ',
    ' OR gg1s.Referencia LIKE "' + Texto + '" ',
    ' OR gg1s.Patrimonio LIKE "' + Texto + '" ',
    ' OR ggxs.EAN13 LIKE "' + Texto + '" ',
    ')',
   'ORDER BY Controle DESC',
   'LIMIT ' + Geral.FF0(LimReg),
   '']);
  end;
end;

end.
