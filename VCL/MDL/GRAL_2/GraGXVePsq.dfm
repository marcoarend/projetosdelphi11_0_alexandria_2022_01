object FmGraGXVePsq: TFmGraGXVePsq
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-054 :: Pesquisa de Mercadorias de Venda'
  ClientHeight = 540
  ClientWidth = 984
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 984
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 936
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 888
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 429
        Height = 32
        Caption = 'Pesquisa de Mercadorias de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 429
        Height = 32
        Caption = 'Pesquisa de Mercadorias de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 429
        Height = 32
        Caption = 'Pesquisa de Mercadorias de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 426
    Width = 984
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 980
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 470
    Width = 984
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 838
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 836
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 984
    Height = 378
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 40
      Width = 984
      Height = 338
      Align = alClient
      DataSource = DsPesq
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = dmkDBGridZTO1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'REFERENCIA'
          Title.Caption = 'Refer'#234'ncia'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EAN13'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FABR'
          Title.Caption = 'Fabricante'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_MARCA'
          Title.Caption = 'Marca'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ItemValr'
          Title.Caption = 'Pre'#231'o'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 400
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Estq'
          Title.Caption = 'Estoque'
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 984
      Height = 40
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1008
      object Label1: TLabel
        Left = 8
        Top = 0
        Width = 93
        Height = 13
        Caption = 'Descri'#231#227'o parcial:'
      end
      object dmkLabel3: TdmkLabel
        Left = 436
        Top = 0
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
        UpdType = utYes
        SQLType = stNil
      end
      object EdPesq: TdmkEdit
        Left = 8
        Top = 16
        Width = 421
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPesqChange
      end
      object EdStqCenCad: TdmkEditCB
        Left = 436
        Top = 16
        Width = 53
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdStqCenCadRedefinido
        DBLookupComboBox = CBStqCenCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStqCenCad: TdmkDBLookupComboBox
        Left = 492
        Top = 16
        Width = 400
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsStqCenCadPsq
        TabOrder = 2
        TabStop = False
        dmkEditCB = EdStqCenCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrStqCenCadPsq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 48
    Top = 152
    object QrStqCenCadPsqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadPsqNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCadPsq: TDataSource
    DataSet = QrStqCenCadPsq
    Left = 76
    Top = 152
  end
  object QrGraCusPrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT gcp.Codigo, gcp.CodUsu, gcp.Nome,'
      'ELT(CustoPreco+1, "Custo", "Pre'#231'o") CusPrc,'
      
        'ELT(TipoCalc+1, "Manual", "Pre'#231'o m'#233'dio", "'#218'ltima compra") NomeTC' +
        ','
      'mda.Nome NOMEMOEDA, mda.Sigla SIGLAMOEDA'
      'FROM gracusprc gcp '
      'LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda')
    Left = 52
    Top = 200
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraCusPrcCusPrc: TWideStringField
      FieldName = 'CusPrc'
      Size = 5
    end
    object QrGraCusPrcNomeTC: TWideStringField
      FieldName = 'NomeTC'
      Size = 13
    end
    object QrGraCusPrcNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 30
    end
    object QrGraCusPrcSIGLAMOEDA: TWideStringField
      FieldName = 'SIGLAMOEDA'
      Size = 5
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 80
    Top = 200
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lpc.GraGruX, gg1.REFERENCIA, ggx.EAN13, '
      'lpc.ItemValr, gfc.Nome NO_FABR, gfm.Nome NO_MARCA, gg1.Nome'
      'FROM gragxvend lpc '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN grafabmar gfm ON lpc.Marca=gfm.Controle'
      'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo')
    Left = 208
    Top = 316
    object QrPesqGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrPesqREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Size = 25
    end
    object QrPesqEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
    object QrPesqItemValr: TFloatField
      FieldName = 'ItemValr'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesqNO_FABR: TWideStringField
      FieldName = 'NO_FABR'
      Size = 120
    end
    object QrPesqNO_MARCA: TWideStringField
      FieldName = 'NO_MARCA'
      Size = 60
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrPesqEstq: TFloatField
      FieldKind = fkLookup
      FieldName = 'Estq'
      LookupDataSet = QrEstq
      LookupKeyFields = 'GraGruX'
      LookupResultField = 'Qtde'
      KeyFields = 'GraGruX'
      Lookup = True
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 208
    Top = 364
  end
  object QrEstq: TMySQLQuery
    Database = Dmod.DBAnt
    SQL.Strings = (
      'SELECT smia.GraGruX, '
      'SUM(smia.Qtde * smia.Baixa) Qtde  '
      'FROM stqmovitsa smia '
      'WHERE smia.Ativo=1  '
      'AND smia.StqCenCad=1 '
      'GROUP BY smia.GraGruX')
    Left = 292
    Top = 316
    object QrEstqGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEstqQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
end
