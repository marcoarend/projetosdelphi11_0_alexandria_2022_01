unit GraGLSitu;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkCheckGroup, UnDmkEnums;

type
  TFmGraGLSitu = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrGraGLSitu: TmySQLQuery;
    DsGraGLSitu: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    EdSigla: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrGraGLSituCodigo: TIntegerField;
    QrGraGLSituNome: TWideStringField;
    QrGraGLSituSigla: TWideStringField;
    QrGraGLSituAplicacao: TIntegerField;
    dmkDBEdit1: TdmkDBEdit;
    Label3: TLabel;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    EdNome: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    CGAplicacao: TdmkCheckGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGLSituAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGLSituBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure EdSiglaChange(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmGraGLSitu: TFmGraGLSitu;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraGLSitu.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraGLSitu.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraGLSituCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraGLSitu.DefParams;
begin
  VAR_GOTOTABELA := 'graglsitu';
  VAR_GOTOMYSQLTABLE := QrGraGLSitu;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM graglsitu');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGraGLSitu.EdSiglaChange(Sender: TObject);
var
  Letra: String;
begin
  if EdSigla.Focused then
  begin
    Letra := EdSigla.Text;
    if Length(Letra) > 0 then
      EdCodigo.ValueVariant := Ord(Letra[1])
    else
      EdCodigo.ValueVariant := 0;
  end;
end;

procedure TFmGraGLSitu.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGraGLSitu.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraGLSitu.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraGLSitu.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraGLSitu.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraGLSitu.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraGLSitu.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraGLSitu.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGLSitu.BtAlteraClick(Sender: TObject);
begin
  EdSigla.Enabled := False;
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGLSitu, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'graglsitu');
end;

procedure TFmGraGLSitu.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraGLSituCodigo.Value;
  Close;
end;

procedure TFmGraGLSitu.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  CodTxt, Nome: String;
begin
  Codigo := EdCodigo.ValueVariant;
  CodTxt := Geral.FF0(Codigo);
  //
  if ImgTipo.SQLType = stIns then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * FROM graglsitu WHERE Codigo=' + CodTxt,
    '']);
    if MyObjects.FIC(Dmod.QrAux.RecordCount > 0, nil,
      'A sigla informada j� est� cadastrada!') then Exit;
  end;
  //
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'graglsitu', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmGraGLSitu.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'graglsitu', 'Codigo');
end;

procedure TFmGraGLSitu.BtIncluiClick(Sender: TObject);
begin
  EdSigla.Enabled := True;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrGraGLSitu, [PnDados],
  [PnEdita], EdSigla, ImgTipo, 'graglsitu');
end;

procedure TFmGraGLSitu.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmGraGLSitu.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraGLSituCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraGLSitu.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraGLSitu.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraGLSituCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraGLSitu.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraGLSitu.QrGraGLSituAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraGLSitu.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGLSitu.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraGLSituCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'graglsitu', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraGLSitu.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGLSitu.QrGraGLSituBeforeOpen(DataSet: TDataSet);
begin
  QrGraGLSituCodigo.DisplayFormat := FFormatFloat;
end;

end.

