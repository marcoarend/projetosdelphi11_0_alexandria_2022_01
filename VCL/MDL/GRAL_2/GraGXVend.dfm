object FmGraGXVend: TFmGraGXVend
  Left = 368
  Top = 194
  Caption = 'PRD-GRUPO-052 :: Cadastro de Produtos de Venda'
  ClientHeight = 645
  ClientWidth = 931
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 90
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 111
    Width = 931
    Height = 534
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 470
      Width = 931
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 12
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 786
        Top = 15
        Width = 143
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 931
      Height = 85
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Identifica'#231#227'o: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 20
        Top = 16
        Width = 14
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 211
        Top = 16
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label39: TLabel
        Left = 78
        Top = 16
        Width = 55
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Refer'#234'ncia:'
      end
      object Label14: TLabel
        Left = 19
        Top = 60
        Width = 94
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fabricante / Marca:'
      end
      object SbMarcas: TSpeedButton
        Left = 460
        Top = 56
        Width = 21
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
      end
      object Label40: TLabel
        Left = 497
        Top = 60
        Width = 38
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Modelo:'
      end
      object EdGraGruX: TdmkEdit
        Left = 20
        Top = 31
        Width = 56
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 211
        Top = 31
        Width = 577
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdReferencia: TdmkEdit
        Left = 78
        Top = 31
        Width = 130
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkAtivo: TdmkCheckBox
        Left = 796
        Top = 32
        Width = 57
        Height = 17
        Caption = 'Ativo?'
        Checked = True
        State = cbChecked
        TabOrder = 3
        QryCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdMarca: TdmkEditCB
        Left = 118
        Top = 56
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMarca
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMarca: TdmkDBLookupComboBox
        Left = 174
        Top = 56
        Width = 287
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Controle'
        ListField = 'NO_MARCA_FABR'
        ListSource = DsMarcas
        TabOrder = 5
        dmkEditCB = EdMarca
        UpdType = utYes
        LocF7CodiFldName = 'Controle'
        LocF7NameFldName = 'Nome'
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdModelo: TdmkEdit
        Left = 541
        Top = 56
        Width = 360
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 85
      Width = 931
      Height = 74
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 927
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label37: TLabel
          Left = 20
          Top = -2
          Width = 93
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '$ Venda (unidade): '
        end
        object Label3: TLabel
          Left = 116
          Top = -2
          Width = 46
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Unidade: '
        end
        object SBItemUnid: TSpeedButton
          Left = 267
          Top = 13
          Width = 21
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SBItemUnidClick
        end
        object Label49: TLabel
          Left = 619
          Top = 4
          Width = 27
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'NCM:'
        end
        object SpeedButton6: TSpeedButton
          Left = 714
          Top = 21
          Width = 21
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '?'
          OnClick = SpeedButton6Click
        end
        object Label10: TLabel
          Left = 292
          Top = -2
          Width = 98
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% Desconto m'#225'ximo:'
        end
        object Label13: TLabel
          Left = 739
          Top = 4
          Width = 37
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'EAN13:'
        end
        object EdItemValr: TdmkEdit
          Left = 20
          Top = 13
          Width = 92
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdItemUnid: TdmkEditCB
          Left = 116
          Top = 13
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBItemUnid
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBItemUnid: TdmkDBLookupComboBox
          Left = 169
          Top = 13
          Width = 97
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Sigla'
          ListSource = DsUnidMed
          TabOrder = 2
          dmkEditCB = EdItemUnid
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object RGAplicacao: TdmkRadioGroup
          Left = 404
          Top = 2
          Width = 209
          Height = 43
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Aplica'#231#227'o: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Inativo'
            'Venda')
          TabOrder = 4
          UpdType = utYes
          OldValor = 0
        end
        object EdNCM: TdmkEdit
          Left = 619
          Top = 21
          Width = 92
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnRedefinido = EdNCMRedefinido
        end
        object EdMaxPercDesco: TdmkEdit
          Left = 292
          Top = 13
          Width = 105
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 10
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdEAN13: TdmkEdit
          Left = 739
          Top = 21
          Width = 173
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 111
    Width = 931
    Height = 534
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 931
      Height = 85
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Identifica'#231#227'o: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label2: TLabel
        Left = 78
        Top = 16
        Width = 55
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Refer'#234'ncia:'
        FocusControl = DBEdNome
      end
      object Label25: TLabel
        Left = 20
        Top = 16
        Width = 48
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Reduzido:'
      end
      object Label46: TLabel
        Left = 211
        Top = 16
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = dmkDBEdit1
      end
      object Label15: TLabel
        Left = 19
        Top = 60
        Width = 94
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fabricante / Marca:'
      end
      object Label16: TLabel
        Left = 497
        Top = 60
        Width = 38
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Modelo:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 20
        Top = 31
        Width = 56
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Controle'
        DataSource = DsGraGXVend
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 78
        Top = 31
        Width = 130
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Referencia'
        DataSource = DsGraGXVend
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 211
        Top = 31
        Width = 618
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NO_GG1'
        DataSource = DsGraGXVend
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBCkAtivo: TDBCheckBox
        Left = 832
        Top = 32
        Width = 49
        Height = 17
        Caption = 'Ativo?'
        DataField = 'Ativo'
        DataSource = DsGraGXVend
        TabOrder = 3
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit7: TDBEdit
        Left = 264
        Top = 56
        Width = 229
        Height = 21
        DataField = 'NO_MARCA'
        DataSource = DsGraGXVend
        TabOrder = 4
      end
      object DBEdit8: TDBEdit
        Left = 172
        Top = 56
        Width = 89
        Height = 21
        DataField = 'NO_FABR'
        DataSource = DsGraGXVend
        TabOrder = 5
      end
      object DBEdit9: TDBEdit
        Left = 540
        Top = 56
        Width = 353
        Height = 21
        DataField = 'Modelo'
        DataSource = DsGraGXVend
        TabOrder = 6
      end
      object DBEdit10: TDBEdit
        Left = 120
        Top = 56
        Width = 49
        Height = 21
        DataField = 'Marca'
        DataSource = DsGraGXVend
        TabOrder = 7
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 467
      Width = 931
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 183
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 134
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 92
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 51
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 185
        Top = 15
        Width = 261
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 446
        Top = 15
        Width = 483
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 231
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 349
          Top = 0
          Width = 134
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 85
      Width = 931
      Height = 60
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 2
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 927
        Height = 43
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 15
          Top = 0
          Width = 93
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '$ Venda (unidade): '
        end
        object Label4: TLabel
          Left = 111
          Top = 0
          Width = 46
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Unidade: '
        end
        object Label8: TLabel
          Left = 623
          Top = 0
          Width = 27
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'NCM:'
        end
        object Label11: TLabel
          Left = 296
          Top = 1
          Width = 98
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% Desconto m'#225'ximo:'
        end
        object Label12: TLabel
          Left = 720
          Top = 0
          Width = 37
          Height = 13
          Caption = 'EAN13:'
          FocusControl = DBEdit6
        end
        object DBEdit1: TDBEdit
          Left = 15
          Top = 16
          Width = 92
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ItemValr'
          DataSource = DsGraGXVend
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 111
          Top = 16
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ItemUnid'
          DataSource = DsGraGXVend
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 164
          Top = 16
          Width = 129
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'SIGLA'
          DataSource = DsGraGXVend
          TabOrder = 2
        end
        object DBRGAplicacao: TDBRadioGroup
          Left = 404
          Top = 0
          Width = 209
          Height = 41
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Aplica'#231#227'o: '
          Columns = 2
          DataField = 'Aplicacao'
          DataSource = DsGraGXVend
          Items.Strings = (
            'Inativo'
            'Venda')
          TabOrder = 3
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
        object DBEdit4: TDBEdit
          Left = 620
          Top = 16
          Width = 97
          Height = 21
          DataField = 'NCM'
          DataSource = DsGraGXVend
          TabOrder = 4
        end
        object DBEdit5: TDBEdit
          Left = 296
          Top = 16
          Width = 105
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'MaxPercDesco'
          DataSource = DsGraGXVend
          TabOrder = 5
        end
        object DBEdit6: TDBEdit
          Left = 720
          Top = 16
          Width = 173
          Height = 21
          DataField = 'EAN13'
          DataSource = DsGraGXVend
          TabOrder = 6
        end
      end
    end
    object GroupBox8: TGroupBox
      Left = 0
      Top = 145
      Width = 931
      Height = 60
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = 'Dados do produto:'
      TabOrder = 3
      object DBText2: TDBText
        Left = 185
        Top = 18
        Width = 175
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TitNiv5'
        DataSource = DsGraGXVend
      end
      object DBText3: TDBText
        Left = 362
        Top = 18
        Width = 175
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TitNiv4'
        DataSource = DsGraGXVend
      end
      object DBText4: TDBText
        Left = 539
        Top = 18
        Width = 175
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TitNiv3'
        DataSource = DsGraGXVend
      end
      object DBText5: TDBText
        Left = 716
        Top = 18
        Width = 175
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TitNiv2'
        DataSource = DsGraGXVend
      end
      object Label47: TLabel
        Left = 7
        Top = 18
        Width = 78
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tipo de produto:'
      end
      object DBEdit24: TDBEdit
        Left = 7
        Top = 33
        Width = 175
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GGT'
        DataSource = DsGraGXVend
        TabOrder = 0
      end
      object DBEdit25: TDBEdit
        Left = 185
        Top = 33
        Width = 175
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GG5'
        DataSource = DsGraGXVend
        TabOrder = 1
      end
      object DBEdit26: TDBEdit
        Left = 539
        Top = 33
        Width = 175
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GG3'
        DataSource = DsGraGXVend
        TabOrder = 2
      end
      object DBEdit30: TDBEdit
        Left = 362
        Top = 33
        Width = 175
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GG4'
        DataSource = DsGraGXVend
        TabOrder = 3
      end
      object DBEdit31: TDBEdit
        Left = 716
        Top = 33
        Width = 175
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GG2'
        DataSource = DsGraGXVend
        TabOrder = 4
      end
    end
    object PnValores: TPanel
      Left = 0
      Top = 205
      Width = 931
      Height = 140
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object Splitter1: TSplitter
        Left = 400
        Top = 0
        Width = 5
        Height = 140
        ExplicitLeft = 417
        ExplicitTop = 13
        ExplicitHeight = 150
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 400
        Height = 140
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label5: TLabel
          Left = 0
          Top = 0
          Width = 400
          Height = 13
          Align = alTop
          Caption = ' Estoque:'
          ExplicitWidth = 45
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 13
          Width = 400
          Height = 127
          Align = alClient
          DataSource = DsEstq
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Empresa'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'StqCenCad'
              Title.Caption = 'Centro'
              Width = 39
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_StqCenCad'
              Title.Caption = 'Descri'#231#227'o do Centro de estoque'
              Width = 172
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Width = 100
              Visible = True
            end>
        end
      end
      object Panel9: TPanel
        Left = 405
        Top = 0
        Width = 526
        Height = 140
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label6: TLabel
          Left = 0
          Top = 0
          Width = 526
          Height = 13
          Align = alTop
          Caption = ' Pre'#231'os e custos: '
          ExplicitWidth = 85
        end
        object DBGrid2: TDBGrid
          Left = 118
          Top = 13
          Width = 408
          Height = 127
          Align = alClient
          DataSource = DsGraGruVal
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_GraCusPrc'
              Title.Caption = 'Tabela'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoPreco'
              Title.Caption = 'Custo/Pre'#231'o'
              Width = 100
              Visible = True
            end>
        end
        object Panel10: TPanel
          Left = 0
          Top = 13
          Width = 118
          Height = 127
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object BtIncVal: TBitBtn
            Tag = 10
            Left = 4
            Top = 4
            Width = 111
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Inclui'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Visible = False
          end
          object BtAltVal: TBitBtn
            Tag = 11
            Left = 4
            Top = 44
            Width = 111
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Altera'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAltValClick
          end
          object BtExcVal: TBitBtn
            Tag = 12
            Left = 4
            Top = 84
            Width = 111
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Exclui'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            Visible = False
          end
        end
      end
    end
    object PnCompras: TPanel
      Left = 0
      Top = 380
      Width = 931
      Height = 87
      Align = alBottom
      TabOrder = 5
      object GBCompras: TDBGrid
        Left = 1
        Top = 1
        Width = 929
        Height = 85
        Align = alClient
        DataSource = DsStqInn
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Empresa'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Fornece'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_FRN'
            Title.Caption = 'Nome do Fornecedor'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Transporta'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TRANSP'
            Title.Caption = 'Nome do transportador'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CustoBuy'
            Title.Caption = '$ Merc.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CustoFrt'
            Title.Caption = '$ Frete'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CustoAll'
            Title.Caption = '$ Total'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CustoUni'
            Title.Caption = '$ Unit'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nfe_nNF'
            Title.Caption = 'NFe'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataHora'
            Title.Caption = 'Data / hora'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IDCtrl'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PackGGX'
            Title.Caption = 'Red. Pacote'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_GG1'
            Title.Caption = 'Produto pacote'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'No_SIGLA'
            Title.Caption = 'Un.Pacote'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PackQtde'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OriCodi'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OriCtrl'
            Width = 40
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 931
    Height = 57
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 872
      Top = 0
      Width = 59
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 13
        Top = 12
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 305
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object SbDescoMin: TBitBtn
        Tag = 188
        Left = 214
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 5
        OnClick = SbDescoMinClick
      end
      object BtPsqEAN13: TBitBtn
        Tag = 358
        Left = 256
        Top = 10
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtPsqEAN13Click
      end
    end
    object GB_M: TGroupBox
      Left = 305
      Top = 0
      Width = 567
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 429
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Mercadorias de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 429
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Mercadorias de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 429
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Mercadorias de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 57
    Width = 931
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 927
      Height = 37
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrGraGXVend: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGraGXVendBeforeOpen
    AfterOpen = QrGraGXVendAfterOpen
    BeforeClose = QrGraGXVendBeforeClose
    AfterScroll = QrGraGXVendAfterScroll
    SQL.Strings = (
      'SELECT '
      'ggx.Controle, gg1.Referencia,'
      'gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, '
      'IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, '
      'med.SIGLA, gg1.Nivel1, gg1.Nivel2, gg1.Nivel3, '
      'gg2.Nome NO_GG2, gg3.Nome NO_GG3, '
      'gg4.Nome NO_GG4, gg5.Nome NO_GG5, '
      'ggt.Nome NO_GGT, gg1.Nivel4, gg1.Nivel5, gg1.CodUsu, '
      'gg1.PrdGrupTip, ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3,'
      'ggt.TitNiv4, ggt.TitNiv5, gg1.NCM, '
      'gg5.CodUsu CUNivel5, gg4.CodUsu CUNivel4,'
      'gg3.CodUsu CUNivel3, gg2.CodUsu CUNivel2,'
      'cpl.* '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2'
      'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3'
      'LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg1.Nivel4'
      'LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5'
      'LEFT JOIN prdgruptip ggt ON ggt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN gragxvend cpl ON cpl.GraGruX=ggx.Controle'
      'LEFT JOIN unidmed med ON med.Codigo=cpl.ItemUnid'
      ''
      'WHERE  gg1.PrdGrupTip=1'
      'AND gg1.Nivel1>0'
      '')
    Left = 408
    Top = 56
    object QrGraGXVendModelo: TWideStringField
      FieldName = 'Modelo'
      Origin = 'gragxpatr.Modelo'
      Size = 60
    end
    object QrGraGXVendNO_MARCA: TWideStringField
      FieldName = 'NO_MARCA'
      Origin = 'grafabmar.Nome'
      Size = 60
    end
    object QrGraGXVendNO_FABR: TWideStringField
      FieldName = 'NO_FABR'
      Origin = 'grafabcad.Nome'
      Size = 60
    end
    object QrGraGXVendGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragxoutr.GraGruX'
    end
    object QrGraGXVendNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrGraGXVendItemValr: TFloatField
      FieldName = 'ItemValr'
      Origin = 'gragxoutr.ItemValr'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrGraGXVendControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gragrux.Controle'
    end
    object QrGraGXVendCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
      Origin = 'gragru1.Nivel1'
      Required = True
    end
    object QrGraGXVendAplicacao: TIntegerField
      FieldName = 'Aplicacao'
      Origin = 'gragxoutr.Aplicacao'
    end
    object QrGraGXVendReferencia: TWideStringField
      FieldName = 'Referencia'
      Origin = 'gragru1.Referencia'
      Required = True
      Size = 25
    end
    object QrGraGXVendItemUnid: TIntegerField
      FieldName = 'ItemUnid'
      Origin = 'gragxoutr.ItemUnid'
    end
    object QrGraGXVendSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
    object QrGraGXVendCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
      Required = True
    end
    object QrGraGXVendNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGXVendNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGXVendNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGXVendNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrGraGXVendNivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrGraGXVendCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGXVendPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGXVendNO_GG2: TWideStringField
      FieldName = 'NO_GG2'
      Size = 30
    end
    object QrGraGXVendNO_GG3: TWideStringField
      FieldName = 'NO_GG3'
      Size = 30
    end
    object QrGraGXVendNO_GG4: TWideStringField
      FieldName = 'NO_GG4'
      Size = 30
    end
    object QrGraGXVendNO_GG5: TWideStringField
      FieldName = 'NO_GG5'
      Size = 30
    end
    object QrGraGXVendNO_GGT: TWideStringField
      FieldName = 'NO_GGT'
      Size = 30
    end
    object QrGraGXVendTitNiv1: TWideStringField
      FieldName = 'TitNiv1'
      Size = 15
    end
    object QrGraGXVendTitNiv2: TWideStringField
      FieldName = 'TitNiv2'
      Size = 15
    end
    object QrGraGXVendTitNiv3: TWideStringField
      FieldName = 'TitNiv3'
      Size = 15
    end
    object QrGraGXVendTitNiv4: TWideStringField
      FieldName = 'TitNiv4'
      Size = 15
    end
    object QrGraGXVendTitNiv5: TWideStringField
      FieldName = 'TitNiv5'
      Size = 15
    end
    object QrGraGXVendNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGXVendCUNivel5: TIntegerField
      FieldName = 'CUNivel5'
    end
    object QrGraGXVendCUNivel4: TIntegerField
      FieldName = 'CUNivel4'
    end
    object QrGraGXVendCUNivel3: TIntegerField
      FieldName = 'CUNivel3'
    end
    object QrGraGXVendCUNivel2: TIntegerField
      FieldName = 'CUNivel2'
    end
    object QrGraGXVendAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGraGXVendMaxPercDesco: TFloatField
      FieldName = 'MaxPercDesco'
      DisplayFormat = '0.0000000000'
    end
    object QrGraGXVendMarca: TIntegerField
      FieldName = 'Marca'
      Origin = 'gragxpatr.Marca'
    end
    object QrGraGXVendEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
  end
  object DsGraGXVend: TDataSource
    DataSet = QrGraGXVend
    Left = 412
    Top = 104
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrMarcas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gfm.Controle, CONCAT(gfm.Nome, " [", '
      'gfc.Nome, "]") NO_MARCA_FABR'
      'FROM grafabmar gfm '
      'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo'
      'ORDER BY NO_MARCA_FABR')
    Left = 152
    Top = 64
    object QrMarcasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMarcasNO_MARCA_FABR: TWideStringField
      FieldName = 'NO_MARCA_FABR'
      Size = 123
    end
  end
  object DsMarcas: TDataSource
    DataSet = QrMarcas
    Left = 180
    Top = 64
  end
  object QrAgrupador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, gg1.Nome'
      'FROM gragxpatr gxp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=gxp.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'ORDER BY gg1.Nome')
    Left = 212
    Top = 64
    object QrAgrupadorControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gragrux.Controle'
    end
    object QrAgrupadorNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru1.Nome'
      Size = 120
    end
  end
  object DsAgrupador: TDataSource
    DataSet = QrAgrupador
    Left = 240
    Top = 64
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Sigla'
      'FROM unidmed'
      'ORDER BY Sigla')
    Left = 272
    Top = 64
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 300
    Top = 64
  end
  object QrLocod: TMySQLQuery
    Database = Dmod.MyDB
    Left = 548
    Top = 10
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 744
    Top = 72
    object Alteraprodutoatual1: TMenuItem
      Caption = '&Altera produto atual'
      OnClick = Alteraprodutoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Editanveis1: TMenuItem
      Caption = '&Edita n'#237'veis'
      OnClick = Editanveis1Click
    end
    object Editadadosdoproduto1: TMenuItem
      Caption = 'Edita dados do &produto'
      OnClick = Editadadosdoproduto1Click
    end
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  gg1.Referencia=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 676
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrEstq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT smia.Empresa, smia.StqCenCad, '
      'scc.NOme NO_StqCenCad,'
      'SUM(smia.Qtde * smia.Baixa) Qtde '
      'FROM stqmovitsa smia'
      'LEFT JOIN stqcencad scc ON scc.Codigo=smia.StqCenCad'
      'WHERE smia.Ativo=1 '
      'AND smia.GraGruX=74'
      'GROUP BY smia.GraGruX, smia.Empresa, smia.StqCenCad')
    Left = 486
    Top = 59
    object QrEstqEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEstqStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Required = True
    end
    object QrEstqNO_StqCenCad: TWideStringField
      FieldName = 'NO_StqCenCad'
      Size = 50
    end
    object QrEstqQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object DsEstq: TDataSource
    DataSet = QrEstq
    Left = 488
    Top = 107
  end
  object QrGraGruVal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gcp.Nome NO_GraCusPrc, ggv.CustoPreco '
      'FROM gragruval ggv'
      'LEFT JOIN gracusprc gcp ON gcp.Codigo=ggv.GraCusPrc'
      'WHERE ggv.GraGruX=2293')
    Left = 544
    Top = 64
    object QrGraGruValNO_GraCusPrc: TWideStringField
      FieldName = 'NO_GraCusPrc'
      Size = 30
    end
    object QrGraGruValCustoPreco: TFloatField
      FieldName = 'CustoPreco'
      Required = True
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrGraGruValControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruValGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruValGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
    object QrGraGruValEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrGraGruValDataIni: TDateField
      FieldName = 'DataIni'
    end
  end
  object DsGraGruVal: TDataSource
    DataSet = QrGraGruVal
    Left = 544
    Top = 112
  end
  object QrExec: TMySQLQuery
    Database = Dmod.MyDB
    Left = 272
    Top = 396
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 668
    Top = 379
  end
  object QrStqInn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sic.Empresa, sic.Codigo, sic.Fornece,'
      'sic.Transporta, sic.nfe_nNF,'
      'smia.DataHora, smia.IDCtrl, smia.OriCodi, '
      'smia.OriCtrl, smia.Qtde, smia.CustoBuy, '
      'smia.CustoFrt, smia.CustoAll,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,'
      'IF(tsp.Tipo=0, tsp.RazaoSocial, tsp.Nome) NO_TRANSP,'
      'IF(smia.Qtde > 0, smia.CustoAll/smia.Qtde, 0.00) CustoUni,'
      'smia.PackGGX, smia.PackQtde, gg1.UnidMed, gg1.Nome NO_GG1,'
      'gcc.Nome NO_COR,  gti.Nome NO_TAM,  '
      'med.Nome No_SIGLA, med.Sigla, med.Grandeza  '
      'FROM stqmovitsa smia'
      'LEFT JOIN stqinncad sic ON sic.Codigo=smia.OriCodi'
      'LEFT JOIN entidades frn ON frn.Codigo=sic.Fornece'
      'LEFT JOIN entidades tsp ON tsp.Codigo=sic.Transporta'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smia.PackGGX '
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip  '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed '
      'WHERE smia.Tipo=5'
      'AND smia.GraGruX=2241'
      'ORDER BY smia.DataHora DESC')
    Left = 136
    Top = 396
    object QrStqInnEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrStqInnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqInnFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrStqInnTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrStqInnnfe_nNF: TIntegerField
      FieldName = 'nfe_nNF'
    end
    object QrStqInnDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrStqInnIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object QrStqInnOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Required = True
    end
    object QrStqInnOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
      Required = True
    end
    object QrStqInnQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrStqInnCustoBuy: TFloatField
      FieldName = 'CustoBuy'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqInnCustoFrt: TFloatField
      FieldName = 'CustoFrt'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqInnCustoAll: TFloatField
      FieldName = 'CustoAll'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqInnNO_FRN: TWideStringField
      FieldName = 'NO_FRN'
      Size = 100
    end
    object QrStqInnNO_TRANSP: TWideStringField
      FieldName = 'NO_TRANSP'
      Size = 100
    end
    object QrStqInnCustoUni: TFloatField
      FieldName = 'CustoUni'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqInnPackGGX: TIntegerField
      FieldName = 'PackGGX'
      Required = True
    end
    object QrStqInnUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrStqInnNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrStqInnNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrStqInnNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrStqInnNo_SIGLA: TWideStringField
      FieldName = 'No_SIGLA'
      Size = 30
    end
    object QrStqInnSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrStqInnGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrStqInnPackQtde: TFloatField
      FieldName = 'PackQtde'
    end
  end
  object DataSource1: TDataSource
    DataSet = QrStqInn
    Left = 460
    Top = 328
  end
  object DsStqInn: TDataSource
    DataSet = QrStqInn
    Left = 136
    Top = 452
  end
end
