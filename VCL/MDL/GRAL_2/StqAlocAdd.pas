unit StqAlocAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, Vcl.Mask, dmkEditCB,
  dmkDBLookupComboBox,
  AppListas, dmkMemo;

type
  TFmStqAlocAdd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrStqaLocIts: TMySQLQuery;
    DsStqaLocIts: TDataSource;
    Panel2: TPanel;
    QrGraGXPatr: TMySQLQuery;
    QrGraGXPatrEstqSdo: TFloatField;
    QrGraGXPatrControle: TIntegerField;
    QrGraGXPatrReferencia: TWideStringField;
    QrGraGXPatrCOD_GG1: TIntegerField;
    QrGraGXPatrNO_GG1: TWideStringField;
    QrGraGXPatrCPL_EXISTE: TFloatField;
    QrGraGXPatrSituacao: TWordField;
    QrGraGXPatrAtualValr: TFloatField;
    QrGraGXPatrValorMes: TFloatField;
    QrGraGXPatrValorQui: TFloatField;
    QrGraGXPatrValorSem: TFloatField;
    QrGraGXPatrValorDia: TFloatField;
    QrGraGXPatrAgrupador: TIntegerField;
    QrGraGXPatrMarca: TIntegerField;
    QrGraGXPatrModelo: TWideStringField;
    QrGraGXPatrSerie: TWideStringField;
    QrGraGXPatrVoltagem: TWideStringField;
    QrGraGXPatrPotencia: TWideStringField;
    QrGraGXPatrCapacid: TWideStringField;
    QrGraGXPatrTXT_MES: TWideStringField;
    QrGraGXPatrTXT_QUI: TWideStringField;
    QrGraGXPatrTXT_SEM: TWideStringField;
    QrGraGXPatrTXT_DIA: TWideStringField;
    QrGraGXPatrNO_SIT: TWideStringField;
    QrGraGXPatrNO_SITAPL: TIntegerField;
    QrGraGXPatrValorFDS: TFloatField;
    QrGraGXPatrDMenos: TIntegerField;
    QrGraGXPatrPatrimonio: TWideStringField;
    QrGraGXPatrNivel2: TIntegerField;
    DsGraGXPatr: TDataSource;
    GBDados: TGroupBox;
    Label6: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    SbRecalculaSaldo: TSpeedButton;
    EdControle: TdmkEdit;
    EdQtde: TdmkEdit;
    DbEdEstqSdo: TDBEdit;
    QrPesq1: TMySQLQuery;
    QrPesq1Referencia: TWideStringField;
    QrPesq1Patrimonio: TWideStringField;
    QrPesq1Controle: TIntegerField;
    QrPesq2: TMySQLQuery;
    QrPesq2Referencia: TWideStringField;
    QrPesq2Patrimonio: TWideStringField;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdQtde: TDBEdit;
    EdStqCenCad: TdmkEdit;
    Label5: TLabel;
    EdIDCtrl: TdmkEdit;
    Label8: TLabel;
    EdCustoAll: TdmkEdit;
    Label9: TLabel;
    QrCus: TMySQLQuery;
    QrCusQtde: TFloatField;
    QrCusCustoAll: TFloatField;
    QrCusCustoUni: TFloatField;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    CbGGX_Aloc: TdmkDBLookupComboBox;
    Label7: TLabel;
    SbGGX_Aloc: TSpeedButton;
    EdRefe_Aloc: TdmkEdit;
    Label19: TLabel;
    Label2: TLabel;
    EdGGX_Aloc: TdmkEditCB;
    Label1: TLabel;
    GBVenda: TGroupBox;
    Panel00A: TPanel;
    Label10: TLabel;
    SbGGX_SMIA: TSpeedButton;
    Label11: TLabel;
    Label13: TLabel;
    CBGGX_SMIA: TdmkDBLookupComboBox;
    EdRefe_SMIA: TdmkEdit;
    EdGGX_SMIA: TdmkEditCB;
    QrGraGXVend: TMySQLQuery;
    QrGraGXVendControle: TIntegerField;
    QrGraGXVendReferencia: TWideStringField;
    QrGraGXVendNO_GG1: TWideStringField;
    QrGraGXVendGraGruX: TIntegerField;
    QrGraGXVendItemValr: TFloatField;
    QrGraGXVendItemUnid: TIntegerField;
    QrGraGXVendGraGru1: TIntegerField;
    QrGraGXVendNCM: TWideStringField;
    QrGraGXVendMaxPercDesco: TFloatField;
    QrGraGXVendUnidMed: TIntegerField;
    QrGraGXVendEAN13: TWideStringField;
    DsGraGXVend: TDataSource;
    QrEstoque: TMySQLQuery;
    QrEstoqueQtde: TFloatField;
    DsEstoque: TDataSource;
    EdEAN13: TdmkEdit;
    Label12: TLabel;
    EdPatrimonio: TdmkEdit;
    QrPesq4: TMySQLQuery;
    QrPesq4Referencia: TWideStringField;
    QrPesq4EAN13: TWideStringField;
    QrPesq3: TMySQLQuery;
    QrPesq3Controle: TIntegerField;
    QrPesq3EAN13: TWideStringField;
    RGFatID: TRadioGroup;
    QrSum: TMySQLQuery;
    QrSumQtdeEnt: TFloatField;
    QrSumQtdeSai: TFloatField;
    MeHistorico: TdmkMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGGX_AlocChange(Sender: TObject);
    procedure EdGGX_AlocRedefinido(Sender: TObject);
    procedure EdPatrimonioChange(Sender: TObject);
    procedure EdPatrimonioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdRefe_AlocChange(Sender: TObject);
    procedure EdRefe_AlocKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbGGX_AlocClick(Sender: TObject);
    procedure SbRecalculaSaldoClick(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure EdStqCenCadRedefinido(Sender: TObject);
    procedure EdQtdeRedefinido(Sender: TObject);
    procedure EdGGX_SMIAChange(Sender: TObject);
    procedure EdGGX_SMIARedefinido(Sender: TObject);
    procedure EdVend_SMIAChange(Sender: TObject);
    procedure EdVend_SMIAKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdRefe_SMIAChange(Sender: TObject);
    procedure EdRefe_SMIAKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbGGX_SMIAClick(Sender: TObject);
    procedure EdEAN13Change(Sender: TObject);
    procedure EdEAN13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEAN13Redefinido(Sender: TObject);
    procedure RGFatIDClick(Sender: TObject);
  private
    { Private declarations }
    FEmpresa, FStqCenCad: Integer;
    //
    procedure PesquisaPorPatrimonio(Limpa: Boolean);
    procedure PesquisaPorRefe_Aloc(Limpa: Boolean);
    procedure PesquisaPorRefe_SMIA(Limpa: Boolean);
    procedure PesquisaPorGGX_Aloc();
    procedure PesquisaPorGGX_SMIA();
    procedure PesquisaPorNome(Key: Integer);
    procedure PesquisaPorEAN13(Limpa: Boolean);
    //procedure ReopenStqaLocIts(Controle: Integer);
    //procedure ReopenDsp_Aloc();
    procedure ReopenCus_Aloc();
    procedure ReopenCus_SMIA();
    procedure RecalculaCusto();
    procedure ReopenGraGXVend();
    procedure ReopenEstoque_SMIA();
    procedure AtualizaMovimentoAloc(GGX_Aloc: Integer);

  public
    { Public declarations }
    procedure ReopenTabelas();
  end;

  var
  FmStqAlocAdd: TFmStqAlocAdd;

implementation

uses UnMyObjects, Module, DmkDAC_PF, MeuDBUses, UnMySQLCuringa, UnGraL_Jan,
  UMySQLModule, UnAppPF, ModuleGeral;

{$R *.DFM}

procedure TFmStqAlocAdd.AtualizaMovimentoAloc(GGX_Aloc: Integer);
var
  GraGruX, Empresa: Integer;
  EstqAnt, EstqEnt, EstqSai, EstqLoc, EstqSdo: Double;
  SQLType: TSQLType;
begin
   UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT GraGruX',
  'FROM gragruepat',
  'WHERE Empresa=' + Geral.FF0(FEmpresa),
  'AND GraGruX=' + Geral.FF0(GGX_Aloc),
  '']);
  if (Dmod.QrAux.RecordCount > 0)
  and (Dmod.QrAux.Fields[0].AsInteger = GGX_Aloc) then
    SQLType := stUpd
  else
    SQLType := stIns;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT ',
  'SUM(IF(Baixa=1, Qtde, 0)) QtdeEnt,  ',
  'SUM(IF(Baixa=-1, Qtde, 0)) QtdeSai  ',
  'FROM stqalocits ',
  'WHERE GGX_Aloc=' + Geral.FF0(GGX_Aloc),
  '']);
  //
  //
  GraGruX        := GGX_Aloc;
  //EstqAnt        := ;
  EstqEnt        := QrSumQtdeEnt.Value;
  EstqSai        := QrSumQtdeSai.Value;
  //EstqLoc        := ;
  //EstqSdo        := ;
  Empresa        := FEmpresa;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruepat', False, [
  'EstqEnt', 'EstqSai'], [
  'GraGruX', 'Empresa'], [
  EstqEnt, EstqSai], [
  GraGruX, Empresa], True) then
  begin
    AppPF.AtualizaEstoqueGraGXPatr(GGX_Aloc, Empresa, True);
  end;
end;

procedure TFmStqAlocAdd.BtOKClick(Sender: TObject);
const
  OriCnta = 0;
  AlterWeb = 1;
  //OriPart_Aloc = 1;
  //OriPart_SMIA = 2;
var
  Codigo, OriCodi, Controle, StqCenCad, GGX_Aloc, GGX_SMIA, IDCtrl, Empresa, Ativo: Integer;
  DataHora: String;
var
  FatID, OriCtrl: Integer;
  Baixa_SMIA, Baixa_Aloc, OriPart: Integer;
  Qtde, CustoAll: Double;
  SQLType: TSQLType;
  Historico: String;
begin
  SQLType    := ImgTipo.SQLType;
  DataHora   := Geral.FDT(DModG.ObtemAgora(), 109);
  Empresa    := FEmpresa;
  StqCenCad  := FStqCenCad;
  Codigo     := 0; // Ainda n�o tem cabe�alho
  OriCodi    := Codigo;
  GGX_Aloc   := EdGGX_Aloc.ValueVariant;
  GGX_SMIA   := EdGGX_SMIA.ValueVariant;
  Qtde       := EdQtde.ValueVariant;
  CustoAll   := EdCustoAll.ValueVariant;
  Ativo      := 1;
  Historico  := MeHistorico.Text;
  //
  if MyObjects.FIC(GGX_Aloc = 0, EdGGX_Aloc, 'Informe o patrim�nio!') then Exit;
  if GBVenda.Visible then
    if MyObjects.FIC(GGX_SMIA = 0, EdGGX_SMIA, 'Informe a Mercadoria de venda!') then Exit;
  case RGFatID.ItemIndex of
    0:
    begin
      FatID := VAR_FATID_3020;
      Baixa_SMIA := -1;
      Baixa_Aloc := 1;
    end;
    1:
    begin
      FatID := VAR_FATID_3030;
      Baixa_SMIA := 1;
      Baixa_Aloc := -1;
    end;
    2:
    begin
      FatID := VAR_FATID_3021;
      Baixa_SMIA := 0;
      Baixa_Aloc := 1;
    end;
    3:
    begin
      FatID := VAR_FATID_3031;
      Baixa_SMIA := 0;
      Baixa_Aloc := -1;
    end else
    begin
      Geral.MB_Aviso('Informe o "Movimento do estoque"');
      Exit;
    end;
  end;
  //
  IDCtrl   := EdIDCtrl.ValueVariant;
  IDCtrl := UMyMod.Busca_IDCtrl_NFe(SQLType, 0);
  //
  Controle := EdControle.ValueVariant;
  //Controle := DModG.BuscaProximoCodigoInt('controle', 'stqmov099', '', Controle);
  Controle := UMyMod.BPGS1I32('stqalocits', 'Controle', '', '', tsPos, SQLType, Controle);
  OriCtrl  := Controle;

  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqalocits', False, [
  'Codigo', 'Empresa', 'DataHora',
  'FatID', 'Qtde', 'CustoAll',
  'Baixa', 'GGX_Aloc', 'GGX_SMIA',
  'Historico'], [
  'Controle'], [
  Codigo, Empresa, DataHora,
  FatID, Qtde, CustoAll,
  Baixa_Aloc, GGX_Aloc, GGX_SMIA,
  Historico], [
  Controle], True) then
  begin
////////////////////////////////////////////////////////////////////////////////
    //OriPart := OriPart_Aloc;
////////////////////////////////////////////////////////////////////////////////
    if GBVenda.Visible then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
      'DataHora', 'Tipo', 'OriCodi',
      'OriCtrl', 'Empresa', 'StqCenCad',
      'GraGruX', 'Qtde', 'Baixa', 'OriCnta',
      'OriPart', 'AlterWeb',
      'Ativo'], [
      'IDCtrl'], [
      DataHora, FatID, OriCodi,
      OriCtrl, Empresa, StqCenCad,
      GGX_SMIA, Qtde, Baixa_SMIA, OriCnta,
      OriPart, AlterWeb, Ativo], [
      IDCtrl], False) then
      begin
      end;
    end;
    AtualizaMovimentoAloc(GGX_Aloc);
    Close;
  end;
end;

procedure TFmStqAlocAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqAlocAdd.EdEAN13Change(Sender: TObject);
begin
  if EdEAN13.Focused then
    PesquisaPorEAN13(False);
end;

procedure TFmStqAlocAdd.EdEAN13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
  begin
    CBGGX_SMIA.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmStqAlocAdd.EdEAN13Redefinido(Sender: TObject);
var
  EAN13: String;
begin
  EAN13 := Geral.SoNumero_TT(StringReplace(EdEAN13.ValueVariant, #$D#$A, '', [rfReplaceAll]));
  if EAN13 <> EdEAN13.ValueVariant then
    EdEAN13.ValueVariant := EAN13;
end;

procedure TFmStqAlocAdd.EdEmpresaRedefinido(Sender: TObject);
begin
  FEmpresa := EdEmpresa.ValueVariant;
end;

procedure TFmStqAlocAdd.EdGGX_AlocChange(Sender: TObject);
begin
  if (not EdPatrimonio.Focused) and
     (not EdRefe_Aloc.Focused) then
    PesquisaPorGGX_Aloc();
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmStqAlocAdd.EdGGX_AlocRedefinido(Sender: TObject);
var
  ValorLocacao: Double;
begin
  if ImgTipo.SQLType = stIns then
  begin
    //ReopenGraGXComEstq();
    //ReopenGraGXSemEstq();
  end;
  //
  //ReopenDsp_ALoc();
  ReopenCus_Aloc();
  if EdGGX_Aloc.ValueVariant <> 0 then
    DbEdEstqSdo.DataSource := DsGraGXPatr
  else
    DbEdEstqSdo.DataSource := nil;
end;

procedure TFmStqAlocAdd.EdGGX_SMIAChange(Sender: TObject);
begin
  if not EdRefe_SMIA.Focused then
    PesquisaPorGGX_SMIA();
(*
  if EdGGX_SMIA.ValueVariant <> 0 then
    DBEdNCM.DataField := 'NCM'
  else
    DBEdNCM.DataField := '';
*)
end;

procedure TFmStqAlocAdd.EdGGX_SMIARedefinido(Sender: TObject);
begin
(*
  EdPercDesco.ValueVariant := 0;
  if ImgTipo.SQLType = stIns then
  begin
    EdPrcUni.ValueVariant := QrGraGXVendItemValr.Value;
    //FPrcUni               := QrGraGXVendItemValr.Value;
  end;
*)
  ReopenEstoque_SMIA();
  ReopenCus_SMIA();
end;

procedure TFmStqAlocAdd.EdPatrimonioChange(Sender: TObject);
begin
  if EdPatrimonio.Focused then
    PesquisaPorPatrimonio(False);
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmStqAlocAdd.EdPatrimonioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  PesquisaPorNome(Key);
  if Key = VK_F7 then
  begin
    CBGGX_Aloc.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmStqAlocAdd.EdVend_SMIAChange(Sender: TObject);
begin
(*
  if EdVend_SMIA.Focused then
    PesquisaPorVendimonio(False);
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXVendNO_SITAPL.Value = 1;
*)
end;

procedure TFmStqAlocAdd.EdVend_SMIAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  PesquisaPorNome(Key);
  if Key = VK_F7 then
  begin
    CBGGX_SMIA.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmStqAlocAdd.EdQtdeRedefinido(Sender: TObject);
begin
  RecalculaCusto();
end;

procedure TFmStqAlocAdd.EdRefe_AlocChange(Sender: TObject);
begin
  if EdRefe_Aloc.Focused then
    PesquisaPorRefe_Aloc(False);
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmStqAlocAdd.EdRefe_AlocKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
  begin
    CBGGX_Aloc.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmStqAlocAdd.EdRefe_SMIAChange(Sender: TObject);
begin
  if EdRefe_SMIA.Focused then
    PesquisaPorRefe_SMIA(False);
  //
  //if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    //BtOK.Enabled := QrGraGXVendNO_SITAPL.Value = 1;
end;

procedure TFmStqAlocAdd.EdRefe_SMIAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
  begin
    CBGGX_SMIA.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmStqAlocAdd.EdStqCenCadRedefinido(Sender: TObject);
begin
  FStqCenCad := EdStqCenCad.ValueVariant;
end;

procedure TFmStqAlocAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqAlocAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FEmpresa   := -11;
  FStqCenCad := 1;
  //
  ReopenGraGXVend();
end;

procedure TFmStqAlocAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqAlocAdd.PesquisaPorEAN13(Limpa: Boolean);
var
  EAN13: String;
begin
  EAN13 := EdEAN13.Text;
  if Length(EAN13) = 13 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
    'SELECT ggx.Controle, ggx.EAN13 ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
    'WHERE ggx.EAN13="' + EAN13 + '"' ,
    'AND NOT (ggo.GraGruX IS NULL)',
    'AND ggo.Aplicacao <> 0 ',
    //
    'AND ggx.Ativo=1 ',
    '']);
    //
    //Geral.MB_Teste(QrPesq1.SQL.Text);
    //
    QrPesq1.Open;
    if QrPesq1.RecordCount > 0 then
    begin
      if EdGGX_SMIA.ValueVariant <> QrPesq1Controle.Value then
      begin
        EdGGX_SMIA.ValueVariant := QrPesq1Controle.Value;
      end;
      if CBGGX_SMIA.KeyValue <> QrPesq1Controle.Value then
        CBGGX_SMIA.KeyValue := QrPesq1Controle.Value;
      //
      //if QrEstoqueQtde.Value > 0 then
        //DBEdQtde_SMIA.SetFocus
      //else
        //Geral.MB_Info('Mercadoria sem estoque positivo!');
    end else if Limpa then
    begin
      EdEAN13.ValueVariant := '';
      EdRefe_SMIA.ValueVariant := '';
    end;
  end;
end;

procedure TFmStqAlocAdd.PesquisaPorGGX_Aloc();
(*
var
  Reabre: Boolean;
*)
begin
  //Reabre := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia, gg1.Patrimonio ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  cpl.GraGruX=' + Geral.FF0(EdGGX_Aloc.ValueVariant),
  'AND NOT (cpl.GraGruX IS NULL) ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdPatrimonio.ValueVariant <> QrPesq2Patrimonio.Value then
    begin
      EdPatrimonio.ValueVariant := QrPesq2Patrimonio.Value;
      //Reabre := True;
    end;
    if EdRefe_Aloc.ValueVariant <> QrPesq2Referencia.Value then
    begin
      EdRefe_Aloc.ValueVariant := QrPesq2Referencia.Value;
      //Reabre := True;
    end;
    //if Reabre then
      //ReopenAgrupado();
  end else
  begin
    EdPatrimonio.ValueVariant := '';
    EdRefe_Aloc.ValueVariant := '';
  end;
  //
  //MostraSituacaoPatrimonio();
end;

procedure TFmStqAlocAdd.PesquisaPorGGX_SMIA;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq4, Dmod.MyDB, [
  'SELECT gg1.Referencia, ggx.EAN13 ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  ggo.GraGruX=' + Geral.FF0(EdGGX_SMIA.ValueVariant),
  'AND NOT (ggo.GraGruX IS NULL) ',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  if QrPesq4.RecordCount > 0 then
  begin
    if EdRefe_SMIA.ValueVariant <> QrPesq4Referencia.Value then
      EdRefe_SMIA.ValueVariant := QrPesq4Referencia.Value;
    //
    if EdEAN13.ValueVariant <> QrPesq4EAN13.Value then
      EdEAN13.ValueVariant := QrPesq4EAN13.Value;
    //
  end else
  begin
    EdRefe_SMIA.ValueVariant := '';
    EdEAN13.ValueVariant := '';
  end;
end;

procedure TFmStqAlocAdd.PesquisaPorNome(Key: Integer);
var
  //Controle
  Nivel1: Integer;
begin
  if Key = VK_F3 then
  begin
    Nivel1 := CuringaLoc.CriaForm('Nivel1', CO_NOME, 'gragru1', Dmod.MyDB, CO_VAZIO);
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM gragrux ',
    'WHERE GraGru1=' + Geral.FF0(Nivel1),
    '']);
    EdGGX_Aloc.ValueVariant := Dmod.QrAux.FieldByName('Controle').AsInteger;
    //ReopenAgrupado();
  end;
  //
  //MostraSituacaoPatrimonio();
end;

procedure TFmStqAlocAdd.PesquisaPorPatrimonio(Limpa: Boolean);
var
  Reabre: Boolean;
begin
  Reabre := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle, gg1.Referencia, gg1.Patrimonio ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Patrimonio="' + EdPatrimonio.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdGGX_Aloc.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGGX_Aloc.ValueVariant := QrPesq1Controle.Value;
      Reabre := True;
    end;
    if EdRefe_Aloc.ValueVariant <> QrPesq1Referencia.Value then
    begin
      EdRefe_Aloc.ValueVariant := QrPesq1Referencia.Value;
      Reabre := True;
    end;
(*
    if Reabre then
      ReopenAgrupado();
*)
    //  Precisa ?
    if EdGGX_Aloc.ValueVariant <> QrPesq1Controle.Value then
      EdGGX_Aloc.ValueVariant := QrPesq1Controle.Value;
    if CBGGX_Aloc.KeyValue <> QrPesq1Controle.Value then
      CBGGX_Aloc.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
  begin
    EdPatrimonio.ValueVariant := '';
    EdRefe_Aloc.ValueVariant := '';
  end;
  //
  //MostraSituacaoPatrimonio();
end;

procedure TFmStqAlocAdd.PesquisaPorRefe_Aloc(Limpa: Boolean);
var
  Reabre: Boolean;
begin
  Reabre := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle, gg1.Referencia, gg1.Patrimonio ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdRefe_Aloc.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdPatrimonio.ValueVariant <> QrPesq1Patrimonio.Value then
    begin
      EdPatrimonio.ValueVariant := QrPesq1Patrimonio.Value;
      Reabre := True;
    end;
    if EdGGX_Aloc.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGGX_Aloc.ValueVariant := QrPesq1Controle.Value;
      Reabre := True;
    end;
(*
    if Reabre then
      ReopenAgrupado();
*)
    if CBGGX_Aloc.KeyValue <> QrPesq1Controle.Value then
      CBGGX_Aloc.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
  begin
    EdPatrimonio.ValueVariant := '';
    EdRefe_Aloc.ValueVariant := '';
  end;
  //
 // MostraSituacaoPatrimonio();
end;

procedure TFmStqAlocAdd.PesquisaPorRefe_SMIA(Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq3, Dmod.MyDB, [
  'SELECT ggx.Controle, ggx.EAN13 ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdRefe_SMIA.Text + '"' ,
  'AND NOT (ggo.GraGruX IS NULL)',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  QrPesq3.Open;
  if QrPesq3.RecordCount > 0 then
  begin
    if EdEAN13.ValueVariant <> QrPesq3EAN13.Value then
      EdEAN13.ValueVariant := QrPesq3EAN13.Value;
    if EdGGX_SMIA.ValueVariant <> QrPesq3Controle.Value then
      EdGGX_SMIA.ValueVariant := QrPesq3Controle.Value;
    if CBGGX_SMIA.KeyValue <> QrPesq3Controle.Value then
      CBGGX_SMIA.KeyValue := QrPesq3Controle.Value;
  end else if Limpa then
  begin
    EdRefe_SMIA.ValueVariant := '';
    EdEAN13.ValueVariant := '';
  end;
end;

procedure TFmStqAlocAdd.RecalculaCusto();
var
  Qtde, CustoUni, CustoAll: Double;
begin
  if (QrCus.State <> dsInactive) and (QrCus.RecordCount > 0) then
  begin
(*
    case RGFatID.ItemIndex of
      0:
      begin
        CustoUni := QrCusCustoUni.Value;
        Qtde     := EdQtde.ValueVariant;
        CustoAll := CustoUni * Qtde;
        //
        EdCustoAll.ValueVariant := CustoAll;
      end;
      1:
      begin
*)
        CustoUni := QrCusCustoUni.Value;
        Qtde     := EdQtde.ValueVariant;
        CustoAll := CustoUni * Qtde;
        //
        EdCustoAll.ValueVariant := CustoAll;
(*
    end;
*)
  end;
end;

procedure TFmStqAlocAdd.ReopenCus_Aloc;
var
  GraGruX: Integer;
begin
  if RGFatID.ItemIndex <> 1 then Exit;
  //
(*
  Screen.Cursor := crHourGlass;
  try
    GraGruX := EdGGX_Aloc.ValueVariant;
    if GraGruX <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCus, Dmod.MyDB, [
      'SELECT Qtde, CustoAll, ',
      'IF(Qtde = 0, 0, CustoAll / Qtde) CustoUni ',
      'FROM stqmovitsa ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      'AND Tipo=' + Geral.FF0(VAR_FATID_0005),
      'AND Empresa=' + Geral.FF0(FEmpresa),
      //'AND smia.StqCenCad=' + Geral.FF0(FStqCenCad),
      'AND Qtde <> 0 ',
      'ORDER BY DataHora DESC ',
      '']);
      //Geral.MB_Teste(QrCus.SQL.Text);
      RecalculaCusto();
    end;
  finally
    Screen.Cursor := crDefault;
  end;
*)
end;

procedure TFmStqAlocAdd.ReopenCus_SMIA();
var
  GraGruX: Integer;
begin
  if RGFatID.ItemIndex <> 0 then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    //GraGruX := EdGGX_Aloc.ValueVariant;
    GraGruX := EdGGX_SMIA.ValueVariant;
    if GraGruX <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCus, Dmod.MyDB, [
      'SELECT Qtde, CustoAll, ',
      'IF(Qtde = 0, 0, CustoAll / Qtde) CustoUni ',
      'FROM stqmovitsa ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      'AND Tipo=' + Geral.FF0(VAR_FATID_0005),
      'AND Empresa=' + Geral.FF0(FEmpresa),
      //'AND smia.StqCenCad=' + Geral.FF0(FStqCenCad),
      'AND Qtde <> 0 ',
      'ORDER BY DataHora DESC ',
      '']);
      //Geral.MB_Teste(QrCus.SQL.Text);
      RecalculaCusto();
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

{
procedure TFmStqAlocAdd.ReopenDsp_Aloc();
var
  GraGruX: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    GraGruX := EdGGX_Aloc.ValueVariant;
    if GraGruX <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrDisp, Dmod.MyDB, [
      'SELECT SUM(smia.Qtde * smia.Baixa) Qtde  ',
      'FROM stqmovitsa smia ',
      'WHERE smia.Ativo=1  ',
      'AND smia.ValiStq <> 101 ',
      'AND smia.GraGruX=' + Geral.FF0(GraGruX),
      'AND smia.Empresa=' + Geral.FF0(FEmpresa),
      'AND smia.StqCenCad=' + Geral.FF0(FStqCenCad),
    (*
      'AND smia.GraGruX IN ( ',
      '  SELECT GROUP_CONCAT(vnd.GraGruX) FROM  ',
      '  gragxpatr vnd ',
      ') ',
      ' ',
    *)
      '']);
      //Geral.MB_Teste(QrDisp.SQL.Text);
      DbEdDisp.Datasource := DsDisp;
    end else
    begin
      DbEdDisp.Datasource := nil;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmStqAlocAdd.ReopenEstoque_SMIA();
var
  GraGruX, StqCenCad: Integer;
begin
  GraGruX    := EdGGX_SMIA.ValueVariant;
  StqCenCad  := EdStqCenCad.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstoque, Dmod.MyDB, [
  'SELECT SUM(Qtde * Baixa) Qtde ',
  'FROM stqmovitsa ',
  'WHERE Ativo=1 ',
  'AND ValiStq <> 101',
  'AND GraGruX=' + Geral.FF0(GraGruX),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND StqCenCad=' + Geral.FF0(StqCenCad),
  '']);
end;

procedure TFmStqAlocAdd.ReopenGraGXVend();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXVend, Dmod.MyDB, [
  'SELECT ggx.Controle, ggx.GraGru1, ggx.EAN13, ',
  'gg1.Referencia, gg1.Nome NO_GG1, gg1.NCM, gg1.UnidMed, ',
  'ggo.GraGruX, ggo.ItemValr, ggo.ItemUnid, ggo.MaxPercDesco ',
  'FROM gragxvend ggo  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY NO_GG1',
  '']);
end;

{
procedure TFmStqAlocAdd.ReopenStqaLocIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqaLocIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM stqalocits ',
  '']);
end;
}

procedure TFmStqAlocAdd.ReopenTabelas();
var
  SQLExtra: String;
begin
  // Sem Filtro: T i s o l i n
  //if Dmod.QrOpcoesTRenGraNivOutr.Value = 7 then
    QrGraGXPatr.Close;
    EdGGX_Aloc.ValueVariant := 0;
    CBGGX_Aloc.KeyValue := 0;
    EdPatrimonio.Text := '';
    EdRefe_Aloc.Text := '';
    if RGFatID.ItemIndex < 0 then
      Exit;
    case RGFatID.ItemIndex of
      0,1: SQLExtra := 'WHERE ggy.Codigo=' + Geral.FF0(CO_GraGruY_1024_GXPatPri);
      //2,3: SQLExtra := 'WHERE ggy.Codigo>' + Geral.FF0(CO_GraGruY_1024_GXPatPri);
      2,3: SQLExtra := 'WHERE ggy.Codigo>=' + Geral.FF0(CO_GraGruY_1024_GXPatPri);
      else SQLExtra := 'WHERE ggy.Codigo=???';
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPatr, Dmod.MyDB, [
    'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL,  ',
    'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1,  ',
    'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE,   ',
    'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,  ',
    'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,  ',
    'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,  ',
    'cpl.Voltagem, cpl.Potencia, cpl.Capacid, ',
    'cpl.ValorFDS, cpl.DMenos, gg1.Patrimonio, gg1.Nivel2 ',
    '    ',
    'FROM gragrux ggx   ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
    'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle   ',
    'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle   ',
    'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo   ',
    'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao ',
    'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle AND gep.Empresa=' + Geral.FF0(FEmpresa),
    'LEFT JOIN gragruY    ggy ON ggy.Codigo=ggx.GraGruY ',
    SQLExtra,
    'AND cpl.Aplicacao <> 0 ',
    'AND ggx.Ativo = 1',
    ' ',
    'ORDER BY NO_GG1  ',
    '']);
  //end;
  //
end;

procedure TFmStqAlocAdd.RGFatIDClick(Sender: TObject);
begin
  ReopenTabelas();
  GBDados.Visible := RGFatID.ItemIndex > -1;
  BtOk.Enabled    := RGFatID.ItemIndex > -1;
  GBVenda.Visible := RGFatID.ItemIndex in ([0,1]);
  RecalculaCusto();
end;

procedure TFmStqAlocAdd.SbGGX_AlocClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  GraL_Jan.MostraFormGraGXPatr(EdGGX_Aloc.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdGGX_Aloc, CBGGX_Aloc, QrGraGXPatr, VAR_CADASTRO, 'Controle');
  PesquisaPorGGX_Aloc();
  EdPatrimonio.SetFocus;
  //
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmStqAlocAdd.SbGGX_SMIAClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  GraL_Jan.MostraFormGraGXVend(EdGGX_SMIA.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdGGX_SMIA, CBGGX_SMIA, QrGraGXVend, VAR_CADASTRO, 'Controle');
  PesquisaPorGGX_SMIA();
  EdRefe_SMIA.SetFocus;
  //
  //if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    //BtOK.Enabled := QrGraGXVendNO_SITAPL.Value = 1;
end;

procedure TFmStqAlocAdd.SbRecalculaSaldoClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := EdGGX_Aloc.ValueVariant;
  if GraGruX > 0 then
  begin
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, FEmpresa, True);
    UMyMod.AbreQuery(QrGraGXPatr, Dmod.MyDB);
    EdGGX_Aloc.ValueVariant := GraGruX;
    CBGGX_Aloc.KeyValue := GraGruX;
    EdGGX_Aloc.SetFocus;
  end else
    Geral.MB_Aviso('Selecione o equipamento!');
end;

{
object QrDisp: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT SUM(smia.Qtde * smia.Baixa) Qtde '
    'FROM stqmovitsa smia'
    'WHERE smia.Ativo=1 '
    'AND smia.ValiStq <> 101'
    'AND smia.GraGruX>0'
    'AND smia.Empresa=-11'
    'AND smia.StqCenCad=1'
    'AND smia.GraGruX IN ('
    '  SELECT GROUP_CONCAT(vnd.GraGruX) FROM '
    '  gragxpatr vnd'
    ')')
  Left = 497
  Top = 180
  object QrDispQtde: TFloatField
    FieldName = 'Qtde'
  end
end
object DsDisp: TDataSource
  DataSet = QrDisp
  Left = 496
  Top = 228
end
}

end.
