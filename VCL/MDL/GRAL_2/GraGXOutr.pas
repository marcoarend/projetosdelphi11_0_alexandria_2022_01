unit GraGXOutr;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, Module, Grids, DBGrids,
  ComCtrls, dmkEditDateTimePicker, dmkMemo, Menus, UnDmkEnums, AppListas,
  dmkCheckBox, UnAppEnums;

type
  TFmGraGXOutr = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrGraGXOutr: TmySQLQuery;
    DsGraGXOutr: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrMarcas: TmySQLQuery;
    DsMarcas: TDataSource;
    QrMarcasControle: TIntegerField;
    QrMarcasNO_MARCA_FABR: TWideStringField;
    QrGraGXOutrGraGruX: TIntegerField;
    QrGraGXOutrNO_GG1: TWideStringField;
    QrGraGXOutrItemValr: TFloatField;
    Label25: TLabel;
    Label7: TLabel;
    EdGraGruX: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrAgrupador: TmySQLQuery;
    QrAgrupadorControle: TIntegerField;
    QrAgrupadorNome: TWideStringField;
    DsAgrupador: TDataSource;
    QrGraGXOutrControle: TIntegerField;
    QrGraGXOutrCOD_GG1: TIntegerField;
    Label39: TLabel;
    EdReferencia: TdmkEdit;
    Label46: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    QrGraGXOutrAplicacao: TIntegerField;
    QrGraGXOutrReferencia: TWideStringField;
    QrUnidMed: TmySQLQuery;
    DsUnidMed: TDataSource;
    QrGraGXOutrItemUnid: TIntegerField;
    QrGraGXOutrSIGLA: TWideStringField;
    QrGraGXOutrCPL_EXISTE: TFloatField;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label37: TLabel;
    EdItemValr: TdmkEdit;
    Label3: TLabel;
    EdItemUnid: TdmkEditCB;
    CBItemUnid: TdmkDBLookupComboBox;
    RGAplicacao: TdmkRadioGroup;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBRGAplicacao: TDBRadioGroup;
    QrLocod: TmySQLQuery;
    SBItemUnid: TSpeedButton;
    PMAltera: TPopupMenu;
    Alteraprodutoatual1: TMenuItem;
    N1: TMenuItem;
    Editanveis1: TMenuItem;
    Editadadosdoproduto1: TMenuItem;
    QrGraGXOutrNivel1: TIntegerField;
    QrGraGXOutrNivel2: TIntegerField;
    QrGraGXOutrNivel3: TIntegerField;
    QrGraGXOutrNivel4: TIntegerField;
    QrGraGXOutrNivel5: TIntegerField;
    QrGraGXOutrCodUsu: TIntegerField;
    QrGraGXOutrPrdGrupTip: TIntegerField;
    GroupBox8: TGroupBox;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    Label47: TLabel;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    QrGraGXOutrNO_GG2: TWideStringField;
    QrGraGXOutrNO_GG3: TWideStringField;
    QrGraGXOutrNO_GG4: TWideStringField;
    QrGraGXOutrNO_GG5: TWideStringField;
    QrGraGXOutrNO_GGT: TWideStringField;
    QrGraGXOutrTitNiv1: TWideStringField;
    QrGraGXOutrTitNiv2: TWideStringField;
    QrGraGXOutrTitNiv3: TWideStringField;
    QrGraGXOutrTitNiv4: TWideStringField;
    QrGraGXOutrTitNiv5: TWideStringField;
    Label49: TLabel;
    EdNCM: TdmkEdit;
    SpeedButton6: TSpeedButton;
    QrGraGXOutrNCM: TWideStringField;
    QrGraGXOutrCUNivel5: TIntegerField;
    QrGraGXOutrCUNivel4: TIntegerField;
    QrGraGXOutrCUNivel3: TIntegerField;
    QrGraGXOutrCUNivel2: TIntegerField;
    QrPesq1: TMySQLQuery;
    QrPesq1Controle: TIntegerField;
    DBCkAtivo: TDBCheckBox;
    CkAtivo: TdmkCheckBox;
    QrGraGXOutrAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGXOutrAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGXOutrBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SBItemUnidClick(Sender: TObject);
    procedure Alteraprodutoatual1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure Editanveis1Click(Sender: TObject);
    procedure Editadadosdoproduto1Click(Sender: TObject);
    procedure PMAlteraPopup(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
  private
    FGraToolRent: TGraToolRent;
    FTabNome,
    FFrmNome: String;
    procedure FiltraItensAExibir(Material: TGraToolRent);
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure PesquisaNomeMateriais();
    //
    procedure IncluiProduto();
    procedure AlteraProduto();
    procedure MostraFormPrdGruNew(SQLTipo: TSQLType; Nivel5, Nivel4, Nivel3,
              Nivel2, Nivel1: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmGraGXOutr: TFmGraGXOutr;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, DmkDAC_PF, Principal, MyDBCheck, GraGru1, ModuleGeral,
  Curinga, UnGrade_Jan, ClasFisc, ModProd, MeuDBUses;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraGXOutr.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraGXOutr.PesquisaNomeMateriais();
var
  Codigo: Integer;
  Filtro, SQL: String;
begin
  // Sem Filtro: T i s o l i n
  if Dmod.QrOpcoesTRenGraNivOutr.Value = 7 then
  begin
(*
    Codigo := CuringaLoc.CriaForm('ggx.Controle', 'gg1.Nome',
      'gragrux ggx', Dmod.MyDB, 'AND ' + Filtro + ' AND gg1.Nivel1>0',
      False,
      'LEFT JOIN gragxoutr cpl ON cpl.GraGruX=ggx.Controle '+ sLineBreak +
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '+ sLineBreak +
      'LEFT JOIN gragruy ggy ON ggy.COdigo=ggx.GraGruY');
*)
    SQL := Geral.ATS([
    'SELECT lpc.GraGruX _CODIGO, gg1.REFERENCIA, gg1.Nome _NOME ',
    'FROM gragxoutr lpc ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE gg1.Nome LIKE "%' + CO_JOKE_SQL + '%" ',
    'OR gg1.Referencia LIKE "%' + CO_JOKE_SQL + '%" ',
    'OR gg1.Patrimonio LIKE "%' + CO_JOKE_SQL + '%" ',
    '']);
    FmMeuDBUses.PesquisaNomeSQLeJoke(SQL, CO_JOKE_SQL);
    if VAR_CADASTRO <> 0 then
      LocCod(QrGraGXOutrControle.Value, VAR_CADASTRO);
  end else
    // Com Filtro: AAPA
  begin
    Dmod.FiltroGrade(FGraToolRent, Filtro);
    //
    Codigo := CuringaLoc.CriaForm('ggx.Controle', 'gg1.Nome',
      'gragrux ggx', Dmod.MyDB, 'AND ' + Filtro + ' AND gg1.Nivel1>0',
      False, 'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
    //
    LocCod(QrGraGXOutrControle.Value, Codigo);
  end;
end;

procedure TFmGraGXOutr.PMAlteraPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(Alteraprodutoatual1, QrGraGXOutr);
  MyObjects.HabilitaMenuItemCabUpd(Editanveis1, QrGraGXOutr);
  MyObjects.HabilitaMenuItemCabUpd(Editadadosdoproduto1, QrGraGXOutr);
end;

procedure TFmGraGXOutr.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraGXOutrControle.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraGXOutr.DefParams;
var
  //GraNiv: Integer;
  Filtro: String;
begin
  if FGraToolRent <> gbsIndef then
  begin
    // Sem Filtro: T i s o l i n
    if Dmod.QrOpcoesTRenGraNivOutr.Value = 7 then
    begin
      VAR_GOTOTABELA := 'gragxoutr cpl, gragrux ggx, gragru1 gg1';
      VAR_GOTOMYSQLTABLE := QrGraGXOutr;
      VAR_GOTONEG := gotoPos;
      VAR_GOTOCAMPO := 'cpl.GraGruX';
      VAR_GOTONOME := 'gg1.Nome';
      VAR_GOTOMySQLDBNAME := Dmod.MyDB;
      VAR_GOTOVAR := 1;

      GOTOy.LimpaVAR_SQL;

      Filtro := ' cpl.GraGrux=ggx.Controle AND ggx.GraGru1=gg1.Nivel1 ';

      VAR_SQLx.Add('SELECT ');
      VAR_SQLx.Add('ggx.Controle, gg1.Referencia, ');
      VAR_SQLx.Add('gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, ');
      VAR_SQLx.Add('IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, ');
      VAR_SQLx.Add('med.SIGLA, gg1.Nivel1, gg1.Nivel2, gg1.Nivel3, gg1.Nivel4, ');
      VAR_SQLx.Add('gg2.Nome NO_GG2, gg3.Nome NO_GG3, gg4.Nome NO_GG4, ');
      VAR_SQLx.Add('gg5.Nome NO_GG5, ggt.Nome NO_GGT, gg1.Nivel5, gg1.CodUsu, ');
      VAR_SQLx.Add('gg1.PrdGrupTip, ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3, ');
      VAR_SQLx.Add('ggt.TitNiv4, ggt.TitNiv5, ');
      VAR_SQLx.Add('gg5.CodUsu CUNivel5, gg4.CodUsu CUNivel4, ');
      VAR_SQLx.Add('gg3.CodUsu CUNivel3, gg2.CodUsu CUNivel2, ');
      VAR_SQLx.Add('gg1.NCM, cpl.* ');
      VAR_SQLx.Add('FROM gragxoutr cpl ');
      VAR_SQLx.Add('LEFT JOIN gragrux ggx ON cpl.GraGruX=ggx.Controle ');
      VAR_SQLx.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ');
      VAR_SQLx.Add('LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2 ');
      VAR_SQLx.Add('LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3 ');
      VAR_SQLx.Add('LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg1.Nivel4 ');
      VAR_SQLx.Add('LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5 ');
      VAR_SQLx.Add('LEFT JOIN prdgruptip ggt ON ggt.Codigo=gg1.PrdGrupTip ');
      VAR_SQLx.Add('LEFT JOIN unidmed med ON med.Codigo=cpl.ItemUnid ');
      //
      VAR_SQLx.Add('LEFT JOIN gragruy ggy ON ggy.Codigo=ggx.Controle ');
      //
    end else
    // Com Filtro: AAPA
    begin
      VAR_GOTOTABELA := 'gragrux ggx, gragru1 gg1';
      VAR_GOTOMYSQLTABLE := QrGraGXOutr;
      VAR_GOTONEG := gotoPos;
      VAR_GOTOCAMPO := 'ggx.Controle';
      VAR_GOTONOME := 'gg1.Nome';
      VAR_GOTOMySQLDBNAME := Dmod.MyDB;
      VAR_GOTOVAR := 1;

      GOTOy.LimpaVAR_SQL;

      Dmod.FiltroGrade(FGraToolRent, Filtro);
      Filtro := Filtro + ' AND (gg1.Nivel1=ggx.GraGru1)';
      //
      VAR_SQLx.Add('SELECT ');
      VAR_SQLx.Add('ggx.Controle, gg1.Referencia, ');
      VAR_SQLx.Add('gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, ');
      VAR_SQLx.Add('IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, ');
      VAR_SQLx.Add('med.SIGLA, gg1.Nivel1, gg1.Nivel2, gg1.Nivel3, gg1.Nivel4, ');
      VAR_SQLx.Add('gg2.Nome NO_GG2, gg3.Nome NO_GG3, gg4.Nome NO_GG4, ');
      VAR_SQLx.Add('gg5.Nome NO_GG5, ggt.Nome NO_GGT, gg1.Nivel5, gg1.CodUsu, ');
      VAR_SQLx.Add('gg1.PrdGrupTip, ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3, ');
      VAR_SQLx.Add('ggt.TitNiv4, ggt.TitNiv5, ');
      VAR_SQLx.Add('gg5.CodUsu CUNivel5, gg4.CodUsu CUNivel4, ');
      VAR_SQLx.Add('gg3.CodUsu CUNivel3, gg2.CodUsu CUNivel2, ');
      VAR_SQLx.Add('gg1.NCM, cpl.* ');
      VAR_SQLx.Add('FROM gragrux ggx ');
      VAR_SQLx.Add('LEFT JOIN ' + FTabNome + ' cpl ON cpl.GraGruX=ggx.Controle ');
      VAR_SQLx.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ');
      VAR_SQLx.Add('LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2 ');
      VAR_SQLx.Add('LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3 ');
      VAR_SQLx.Add('LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg1.Nivel4 ');
      VAR_SQLx.Add('LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5 ');
      VAR_SQLx.Add('LEFT JOIN prdgruptip ggt ON ggt.Codigo=gg1.PrdGrupTip ');
      VAR_SQLx.Add('LEFT JOIN unidmed med ON med.Codigo=cpl.ItemUnid ');
    end;
    VAR_SQLx.Add('');
    VAR_SQLx.Add('WHERE ' + Filtro);
    //
    VAR_SQL1.Add('AND ggx.Controle=:P0');
    //
    //VAR_SQL2.Add('AND gg1.CodUsu=:P0');
    //
    VAR_SQLa.Add('AND gg1.Nome LIKE :P0');
    //
    //
    VAR_GOTOVAR1 := Filtro;
    //
  end;
end;

procedure TFmGraGXOutr.Editadadosdoproduto1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrGraGXOutrControle.Value;
  //
  Grade_Jan.MostraFormGraGruN(QrGraGXOutrNivel1.Value);
  LocCod(Controle, Controle);
end;

procedure TFmGraGXOutr.Editanveis1Click(Sender: TObject);
begin
  MostraFormPrdGruNew(stUpd, QrGraGXOutrCUNivel5.Value, QrGraGXOutrCUNivel4.Value,
    QrGraGXOutrCUNivel3.Value, QrGraGXOutrCUNivel2.Value, QrGraGXOutrCodUsu.Value);
end;

procedure TFmGraGXOutr.CriaOForm;
begin
  DefineONomeDoForm;
end;

procedure TFmGraGXOutr.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraGXOutr.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraGXOutr.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraGXOutr.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraGXOutr.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraGXOutr.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraGXOutr.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmClasFisc, FmClasFisc, afmoNegarComAviso) then
  begin
    FmClasFisc.BtSeleciona.Visible := True;
    FmClasFisc.ShowModal;
    if FmClasFisc.FNCMSelecio <> '' then
      EdNCM.Text := FmClasFisc.FNCMSelecio;
    FmClasFisc.Destroy;
  end;
end;

procedure TFmGraGXOutr.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGXOutr.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraGXOutrGraGruX.Value;
  Close;
end;

procedure TFmGraGXOutr.Alteraprodutoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGXOutr, [PnDados],
    [PnEdita], EdReferencia, ImgTipo, 'Controle');
  //
  EdGraGruX.ValueVariant  := QrGraGXOutrControle.Value;
  EdReferencia.Text       := QrGraGXOutrReferencia.Value;
  EdNome.Text             := QrGraGXOutrNO_GG1.Value;
  RGAplicacao.ItemIndex   := QrGraGXOutrAplicacao.Value;
  EdItemValr.ValueVariant := QrGraGXOutrItemValr.Value;
  EdItemUnid.ValueVariant := QrGraGXOutrItemUnid.Value;
  CBItemUnid.KeyValue     := QrGraGXOutrItemUnid.Value;
  EdNCM.ValueVariant      := QrGraGXOutrNCM.Value;
  CkAtivo.Checked         := Geral.IntToBool(QrGraGXOutrAtivo.Value);
end;

procedure TFmGraGXOutr.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmGraGXOutr.BtConfirmaClick(Sender: TObject);
const
  sProcName = 'TFmGraGXOutr.BtConfirmaClick()';
var
  GraGruX, Nivel1, UnidMed, GraGruY: Integer;
  Nome, Referencia, NCM: String;
  SQLType: TSQLType;
  Aplicacao, ItemUnid, Ativo: Integer;
  ItemValr: Double;
  //
  function InsUpdG1PrAp(SQLType: TSQLType; GraGruX: Integer): Boolean;
  begin
    ItemValr       := EdItemValr.ValueVariant;
    ItemUnid       := EdItemUnid.ValueVariant;
    case RGAplicacao.ItemIndex of
      1: GraGruY := CO_GraGruY_4096_GXPatApo;
      2: GraGruY := CO_GraGruY_5120_GXPatUso;
      3: GraGruY := CO_GraGruY_6144_GXPrdCns;
      else
      begin
        Geral.MB_ERRO('ERRO ao definir o GrGruY! ' + sProcName);
      end;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragxoutr', False, [
    'Aplicacao', 'ItemValr', 'ItemUnid',
    'Ativo'], ['GraGruX'
    ], [Aplicacao, ItemValr, ItemUnid,
    Ativo], [GraGruX], True);
    //
    if Result then
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragrux', False, [
      'GraGruY', 'Ativo'], [
      'Controle'], [
      GraGruY, Ativo], [
      GraGruX], True);
    end;
  end;
begin
  Nome       := EdNome.ValueVariant;
  Referencia := EdReferencia.ValueVariant;
  UnidMed    := EdItemUnid.ValueVariant;
  NCM        := EdNCM.ValueVariant;
  Aplicacao  := RGAplicacao.ItemIndex;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(Referencia) = 0, EdReferencia, 'Defina uma refer�ncia!') then Exit;
  if MyObjects.FIC(UnidMed = 0, EdItemUnid, 'Defina a unidade de medida!') then Exit;
  if MyObjects.FIC(not Aplicacao in ([1,2,3]), RGAplicacao,
    'Defina a aplica��o!') then Exit;
  //
  case Dmod.QrOpcoesTRenFormaCobrLoca.Value of
    0,1: // Modo simplificado - AAPA
    begin

    end;
    2: // Modo completo - T i s o l i n
    begin
      if MyObjects.FIC(Aplicacao < 1, RGAplicacao,
        'Defina a aplica��o!') then Exit;
      //
      case RGAplicacao.ItemIndex of
        1: GraGruY := CO_GraGruY_4096_GXPatApo;
        2: GraGruY := CO_GraGruY_5120_GXPatUso;
        3: GraGruY := CO_GraGruY_6144_GXPrdCns;
        else
        begin
          Geral.MB_ERRO('ERRO ao definir o GrGruY! ' + sProcName);
        end;
      end;
    end;
  end;
  //
  GraGruX := EdGraGruX.ValueVariant;
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT GraGru1 ',
  'FROM gragrux ',
  'WHERE controle=' + Geral.FF0(GraGruX),
  '']);
  Nivel1 := Dmod.QrAux.FieldByName('GraGru1').AsInteger;
  Ativo  := Geral.BoolToInt(CkAtivo.Checked);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False,
    ['Nome', 'Referencia', 'UnidMed', 'NCM'], ['Nivel1'],
    [Nome, Referencia, UnidMed, NCM], [Nivel1], True) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT GraGruX ',
    'FROM ' + FTabNome,
    'WHERE GraGruX=' + Geral.FF0(GraGruX),
    '']);
    if GraGruX = Dmod.QrAux.FieldByName('GraGruX').AsInteger then
      SQLType := stUpd
    else
      SQLType := stIns;
    if InsUpdG1PrAp(SQLType, GraGruX) then
    begin
      LocCod(GraGruX,GraGruX);
      ImgTipo.SQLType := stlok;
      PnDados.Visible := True;
      PnEdita.Visible := False;
      GOTOy.BotoesSb(ImgTipo.SQLType);
    end;
  end;
end;

procedure TFmGraGXOutr.BtDesisteClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(GraGruX, Dmod.MyDB, FTabNome, 'GraGruX');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraGXOutr.BtIncluiClick(Sender: TObject);
begin
  MostraFormPrdGruNew(stIns, 0, 0, 0, 0, 0);
end;

procedure TFmGraGXOutr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  RGAplicacao.Items.Clear;
  DBRGAplicacao.Items.Clear;
  DBRGAplicacao.Values.Clear;
  case Dmod.QrOpcoesTRenFormaCobrLoca.Value of
    0,1: // Modo simplificado - AAPA
    begin
      RGAplicacao.Items.Add('Inativo');
      RGAplicacao.Items.Add('Acess�rio');
      RGAplicacao.Items.Add('Uso');
      RGAplicacao.Items.Add('Consumo');
      //
      DBRGAplicacao.Items.Add('Inativo');
      DBRGAplicacao.Items.Add('Acess�rio');
      DBRGAplicacao.Items.Add('Uso');
      DBRGAplicacao.Items.Add('Consumo');
      //
      DBRGAplicacao.Values.Add('0');
      DBRGAplicacao.Values.Add('1');
      DBRGAplicacao.Values.Add('2');
      DBRGAplicacao.Values.Add('3');
      //
    end;
    2: // Modo completo - T i s o l i n
    begin
      RGAplicacao.Items.Add('Indefinido');
      RGAplicacao.Items.Add('Apoio');
      RGAplicacao.Items.Add('Uso');
      RGAplicacao.Items.Add('Consumo');
      //
      DBRGAplicacao.Items.Add('Indefinido');
      DBRGAplicacao.Items.Add('Apoio');
      DBRGAplicacao.Items.Add('Uso');
      DBRGAplicacao.Items.Add('Consumo');
      //
      DBRGAplicacao.Values.Add('0');
      DBRGAplicacao.Values.Add('1');
      DBRGAplicacao.Values.Add('2');
      DBRGAplicacao.Values.Add('3');
      //
    end;
  end;
  FiltraItensAExibir(gbsOutrs);
  //
  CriaOForm;
  //
  UMyMod.AbreQuery(QrMarcas, Dmod.MyDB);
  UMyMod.AbreQuery(QrAgrupador, Dmod.MyDB);
  UMyMod.AbreQuery(QrUnidMed, Dmod.MyDB);
end;

procedure TFmGraGXOutr.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraGXOutrGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmGraGXOutr.SBItemUnidClick(Sender: TObject);
var
  UnidMed: Integer;
begin
  VAR_CADASTRO := 0;
  UnidMed      := EdItemUnid.ValueVariant;
  //
  FmPrincipal.MostraUnidMed(EdItemUnid.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdItemUnid, CBItemUnid, QrUnidMed, VAR_CADASTRO, 'Codigo');
    EdItemUnid.SetFocus;
  end;
end;

procedure TFmGraGXOutr.SbNomeClick(Sender: TObject);
begin
  PesquisaNomeMateriais();
end;

procedure TFmGraGXOutr.SbNovoClick(Sender: TObject);
var
  Referencia: String;
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrGraGXOutrGraGruX.Value, LaRegistro.Caption);
  //
  Referencia := '';
  if InputQuery('Pesquisa por Refer�ncia', 'Informe a refer�ncia', Referencia) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocod, Dmod.MyDB, [
    'SELECT ggx.Controle ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE gg1.Referencia="' + Referencia + '"',
    '']);
    LocCod(QrLocod.FieldByName('Controle').AsInteger, QrLocod.FieldByName('Controle').AsInteger);
  end;
end;

procedure TFmGraGXOutr.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraGXOutr.QrGraGXOutrAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraGXOutr.FiltraItensAExibir(Material: TGraToolRent);
begin
  FGraToolRent := Material;
  case FGraToolRent of
    gbsOutrs: FTabNome := 'gragxoutr';
    else FTabNome := 'gragx????';
  end;
  //
  case FGraToolRent of
    gbsOutrs  : FFrmNome := 'Material';
    else FFrmNome := '? ? ? ? ?';
  end;
  //
  DefParams;
  Va(vpLast);
end;

procedure TFmGraGXOutr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGXOutr.SbQueryClick(Sender: TObject);
begin
  PesquisaNomeMateriais();
end;

procedure TFmGraGXOutr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGXOutr.MostraFormPrdGruNew(SQLTipo: TSQLType; Nivel5, Nivel4,
  Nivel3, Nivel2, Nivel1: Integer);
const
  ForcaCadastrGGX_SemCorTam = True;
var
  TipCodOutr: Integer;
  InseridoAgora: Boolean;
begin
  InseridoAgora := False;
  VAR_CADASTRO  := 0;
  VAR_CADASTRO2 := 0;
  //
  Dmod.ReopenOpcoesTRen;
  //
  TipCodOutr := Dmod.QrOpcoesTRenTipCodOutr.Value;
  //
  DmProd.InsereItemPrdGruNew(cggnFmGraGXPatr, TipCodOutr, 0, '', '',
  ForcaCadastrGGX_SemCorTam, Nivel5, Nivel4, Nivel3, Nivel2, Nivel1, True, 0, '');
  //
(*
  if VAR_CADASTRO2 <> 0 then
  begin
    LocCod(VAR_CADASTRO2, VAR_CADASTRO2);
    //
    if (QrGraGXOutrCPL_EXISTE.Value = 0) and (SQLTipo = stIns) then
    begin
      Alteraprodutoatual1Click(PMAltera);
    end;
  end;
*)
  if VAR_CADASTRO2 <> 0 then // VAR_CADASTRO2 = GraGruX.Controle
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
    'SELECT GraGruX Controle ',
    'FROM gragxoutr ',
    'WHERE GraGruX=' + Geral.FF0(VAR_CADASTRO2),
    '']);
    QrPesq1.Open;
    if QrPesq1.RecordCount = 0 then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragxoutr', False,
      ['Aplicacao'], ['GraGruX'], [0], [VAR_CADASTRO2], True) then
          InseridoAgora := True;
    end;
    LocCod(VAR_CADASTRO2, VAR_CADASTRO2);
    //
    if ( (QrGraGXOutrCPL_EXISTE.Value = 0) or (InseridoAgora))
    and (SQLTipo = stIns) then
    begin
      Alteraprodutoatual1Click(PMAltera);
    end;
  end;
end;

procedure TFmGraGXOutr.IncluiProduto;
var
  TipCodOutr: Integer;
begin
  VAR_CADASTRO  := 0;
  VAR_CADASTRO2 := 0;
  //
  Dmod.ReopenOpcoesTRen;
  //
  TipCodOutr := Dmod.QrOpcoesTRenTipCodOutr.Value;
  //
  if DBCheck.CriaFm(TFmGraGru1, FmGraGru1, afmoNegarComAviso) then
  begin
    FmGraGru1.ImgTipo.SQLType := stIns;
    FmGraGru1.FQrGraGru1 := nil;
    FmGraGru1.FQrGraGruX := QrGraGXOutr;
    //
    if TipCodOutr <> 0 then
    begin
      FmGraGru1.EdPrdGrupTip.ValueVariant := TipCodOutr;
      FmGraGru1.CBPrdGrupTip.KeyValue     := TipCodOutr;
      FmGraGru1.EdPrdGrupTip.Enabled      := False;
      FmGraGru1.CBPrdGrupTip.Enabled      := False;
    end else
    begin
      FmGraGru1.EdPrdGrupTip.Enabled := True;
      FmGraGru1.CBPrdGrupTip.Enabled := True;
    end;
    //
    FmGraGru1.ShowModal;
    FmGraGru1.Destroy;
  end;
  if VAR_CADASTRO2 <> 0 then
  begin
    LocCod(VAR_CADASTRO2, VAR_CADASTRO2);
    if (VAR_CADASTRO2 = QrGraGXOutrGraGruX.Value) and
    (QrGraGXOutrCPL_EXISTE.Value = 0) then
    begin
      BtAlteraClick(BtAltera);
    end;
  end;
end;

procedure TFmGraGXOutr.AlteraProduto;
var
  TipCodOutr: Integer;
begin
  Dmod.ReopenOpcoesTRen;
  TipCodOutr := Dmod.QrOpcoesTRenTipCodOutr.Value;
  //
  if DBCheck.CriaFm(TFmGraGru1, FmGraGru1, afmoNegarComAviso) then
  begin
    FmGraGru1.ImgTipo.SQLType := stUpd;
    FmGraGru1.FQrGraGru1 := QrGraGXOutr;
    //
    if TipCodOutr <> 0 then
    begin
      FmGraGru1.EdPrdGrupTip.ValueVariant := TipCodOutr;
      FmGraGru1.CBPrdGrupTip.KeyValue     := TipCodOutr;
      FmGraGru1.EdPrdGrupTip.Enabled      := False;
      FmGraGru1.CBPrdGrupTip.Enabled      := False;
    end else
    begin
      FmGraGru1.EdPrdGrupTip.Enabled := True;
      FmGraGru1.CBPrdGrupTip.Enabled := True;
    end;
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdPrdGrupTip, FmGraGru1.CBPrdGrupTip,
      FmGraGru1.QrPrdGrupTip, QrGraGXOutrPrdGrupTip.Value);
    //
    FmGraGru1.HabilitaComponentes();
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel5, FmGraGru1.CBNivel5,
      FmGraGru1.QrGraGru5, QrGraGXOutrNivel5.Value, 'Nivel5', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel4, FmGraGru1.CBNivel4,
      FmGraGru1.QrGraGru4, QrGraGXOutrNivel4.Value, 'Nivel4', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel3, FmGraGru1.CBNivel3,
      FmGraGru1.QrGraGru3, QrGraGXOutrNivel3.Value, 'Nivel3', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel2, FmGraGru1.CBNivel2,
      FmGraGru1.QrGraGru2, QrGraGXOutrNivel2.Value, 'Nivel2', 'CodUsu');
    //
    FmGraGru1.EdNivel1.ValueVariant     := QrGraGXOutrNivel1.Value;
    FmGraGru1.EdCodUsu_Ant.ValueVariant := QrGraGXOutrCodUsu.Value;
    FmGraGru1.EdCodUsu_New.ValueVariant := QrGraGXOutrCodUsu.Value;
    FmGraGru1.EdNome_Ant.ValueVariant   := QrGraGXOutrNO_GG1.Value;
    FmGraGru1.EdNome_New.ValueVariant   := QrGraGXOutrNO_GG1.Value;
    //
    FmGraGru1.ShowModal;
    FmGraGru1.Destroy;
  end;
end;

procedure TFmGraGXOutr.QrGraGXOutrBeforeOpen(DataSet: TDataSet);
begin
  QrGraGXOutrGraGruX.DisplayFormat := FFormatFloat;
end;

end.

