object FmLocCRedef: TFmLocCRedef
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-118 :: Redefinir Loca'#231#227'o'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 219
        Height = 32
        Caption = 'Redefinir Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 219
        Height = 32
        Caption = 'Redefinir Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 219
        Height = 32
        Caption = 'Redefinir Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 1000330
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Recalcula'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtAcoes: TBitBtn
        Tag = 1000233
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Item'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAcoesClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 81
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
    end
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 81
      Width = 812
      Height = 386
      Align = alClient
      DataSource = DsLocCMovAll
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_TipoMotiv'
          Title.Caption = 'Tipo de movimenta'#231#227'o'
          Width = 127
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TipoES'
          Title.Caption = 'E/S'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data / hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LIMPO'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SUJO'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QUEBRADO'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TESTADODEVOLUCAO'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'COBRANCALOCACAO'
          Width = 116
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Quantidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALORLOCACAO'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GGXEntrada'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_MotivoTroca'
          Title.Caption = 'Motivo da troca'
          Width = 212
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ObservacaoTroca'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QuantidadeJaDevolvida'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QuantidadeLocada'
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrLocCMovAll: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lma.* '
      'FROM loccmovall lma'
      'WHERE lma.Codigo=27352'
      'AND lma.GTRTab=1 '
      'AND lma.CtrID=34556 '
      'AND lma.Item=67331')
    Left = 408
    Top = 340
    object QrLocCMovAllCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocCMovAllGTRTab: TIntegerField
      FieldName = 'GTRTab'
      Required = True
    end
    object QrLocCMovAllCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrLocCMovAllItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrLocCMovAllControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLocCMovAllSEQUENCIA: TIntegerField
      FieldName = 'SEQUENCIA'
      Required = True
    end
    object QrLocCMovAllDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrLocCMovAllUSUARIO: TWideStringField
      FieldName = 'USUARIO'
      Size = 10
    end
    object QrLocCMovAllGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrLocCMovAllTipoES: TWideStringField
      FieldName = 'TipoES'
      Size = 1
    end
    object QrLocCMovAllQuantidade: TIntegerField
      FieldName = 'Quantidade'
    end
    object QrLocCMovAllTipoMotiv: TSmallintField
      FieldName = 'TipoMotiv'
    end
    object QrLocCMovAllCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 50
    end
    object QrLocCMovAllVALORLOCACAO: TFloatField
      FieldName = 'VALORLOCACAO'
      Required = True
    end
    object QrLocCMovAllLIMPO: TWideStringField
      FieldName = 'LIMPO'
      Size = 1
    end
    object QrLocCMovAllSUJO: TWideStringField
      FieldName = 'SUJO'
      Size = 1
    end
    object QrLocCMovAllQUEBRADO: TWideStringField
      FieldName = 'QUEBRADO'
      Size = 1
    end
    object QrLocCMovAllTESTADODEVOLUCAO: TWideStringField
      FieldName = 'TESTADODEVOLUCAO'
      Size = 1
    end
    object QrLocCMovAllGGXEntrada: TLargeintField
      FieldName = 'GGXEntrada'
    end
    object QrLocCMovAllMotivoTroca: TIntegerField
      FieldName = 'MotivoTroca'
      Required = True
    end
    object QrLocCMovAllObservacaoTroca: TWideStringField
      FieldName = 'ObservacaoTroca'
      Size = 512
    end
    object QrLocCMovAllQuantidadeLocada: TIntegerField
      FieldName = 'QuantidadeLocada'
    end
    object QrLocCMovAllQuantidadeJaDevolvida: TIntegerField
      FieldName = 'QuantidadeJaDevolvida'
    end
    object QrLocCMovAllLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrLocCMovAllDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocCMovAllDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocCMovAllUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrLocCMovAllUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrLocCMovAllAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLocCMovAllAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrLocCMovAllAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrLocCMovAllAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLocCMovAllNO_ManejoLca: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ManejoLca'
      Size = 60
      Calculated = True
    end
    object QrLocCMovAllNO_TipoMotiv: TWideStringField
      FieldName = 'NO_TipoMotiv'
      Size = 60
    end
    object QrLocCMovAllSubstitutoLca: TIntegerField
      FieldName = 'SubstitutoLca'
    end
    object QrLocCMovAllNO_MotivoTroca: TWideStringField
      FieldName = 'NO_MotivoTroca'
      Size = 60
    end
  end
  object DsLocCMovAll: TDataSource
    DataSet = QrLocCMovAll
    Left = 420
    Top = 404
  end
  object PMAcoes: TPopupMenu
    Left = 314
    Top = 582
    object Alteravalortotaldalocao1: TMenuItem
      Caption = '&Altera valor total da loca'#231#227'o'
      OnClick = Alteravalortotaldalocao1Click
    end
  end
end
