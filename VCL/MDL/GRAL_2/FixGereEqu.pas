unit FixGereEqu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, dmkRadioGroup, ComCtrls, dmkEditDateTimePicker,
  UnDmkEnums;

type
  TFmFixGereEqu = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    DBEdCodigo: TDBEdit;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXCNTRL_GGX: TIntegerField;
    QrGraGruXNO_EQUI: TWideStringField;
    DsGraGruX: TDataSource;
    DsRece: TDataSource;
    QrRece: TmySQLQuery;
    QrResp: TmySQLQuery;
    DsResp: TDataSource;
    QrLibe: TmySQLQuery;
    DsLibe: TDataSource;
    CBQuemLibe: TdmkDBLookupComboBox;
    EdDtHrLibe: TdmkEdit;
    TPDtHrLibe: TdmkEditDateTimePicker;
    Label7: TLabel;
    EdQuemLibe: TdmkEditCB;
    Label5: TLabel;
    QrReceLogin: TWideStringField;
    QrReceNumero: TIntegerField;
    QrRespLogin: TWideStringField;
    QrRespNumero: TIntegerField;
    QrLibeLogin: TWideStringField;
    QrLibeNumero: TIntegerField;
    QrPesq1: TmySQLQuery;
    QrPesq1Controle: TIntegerField;
    QrPesq2: TmySQLQuery;
    QrPesq2Referencia: TWideStringField;
    Panel3: TPanel;
    RGTabela: TdmkRadioGroup;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label1: TLabel;
    EdGraGruX: TdmkEditCB;
    EdReferencia: TdmkEdit;
    EdDtHrRece: TdmkEdit;
    TPDtHrRece: TdmkEditDateTimePicker;
    Label21: TLabel;
    CBQuemRece: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdQuemRece: TdmkEditCB;
    EdQuemResp: TdmkEditCB;
    Label3: TLabel;
    CBQuemResp: TdmkDBLookupComboBox;
    EdDefeitoTxt: TdmkEdit;
    Label8: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    SbGraGruX: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGTabelaClick(Sender: TObject);
    procedure SbGraGruXClick(Sender: TObject);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenFixGereEqu(Controle: Integer);
    procedure PesquisaPorReferencia(Limpa: Boolean);
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorNome(Key: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFixGereEqu: TFmFixGereEqu;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  Principal, UnMySQLCuringa;

{$R *.DFM}

procedure TFmFixGereEqu.BtOKClick(Sender: TObject);
(*
var
  Codigo, Controle: Integer;
begin
  MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descrição!') then
    Exit;
  Codigo   := Geral.IMV(DBEdCodigo.Text);
  Controle? := EdControle.ValueVariant;
  Controle? := UMyMod.BuscaEmLivreY_Def('cadastro_com_itens_its', 'Controle', ImgTipo.SQLType, Controle);
ou > ? := UMyMod.BPGS1I32('cadastro_com_itens_its', 'Controle', '', '', tsPosNeg?, ImgTipo.SQLType, Controle);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, 'cadastro_com_itens_its', auto_increment?[
capos?], [
'Controle'], [
valores?], [
Controle], UserDataAlterweb?, IGNORE?
  begin
    ReopenFixGereEqu(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCargo.ValueVariant     := 0;
      CBCargo.KeyValue         := Null;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else Close;
  end;
*)
var
  DtHrRece, DtHrLibe, DefeitoTxt: String;
  Codigo, Controle, Tabela, GraGruX, QuemRece, QuemResp, QuemLibe: Integer;
  //CustoPeca, CustoServ, CustoTota: Double;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Tabela         := RGTabela.ItemIndex;
  GraGruX        := EdGraGruX.ValueVariant;
  QuemRece       := EdQuemRece.ValueVariant;
  QuemResp       := EdQuemResp.ValueVariant;
  QuemLibe       := EdQuemLibe.ValueVariant;
  DtHrRece       := Geral.FDT(Trunc(TPDtHrRece.Date), 1) + ' ' + EdDtHrRece.Text;
  DtHrLibe       := Geral.FDT(Trunc(TPDtHrLibe.Date), 1) + ' ' + EdDtHrLibe.Text;
  (*
  CustoPeca      := CustoPeca;
  CustoServ      := CustoServ;
  CustoTota      := CustoTota;
  *)
  DefeitoTxt     := EdDefeitoTxt.Text;
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o equipamento!') then Exit;
  if MyObjects.FIC(QuemRece = 0, EdQuemRece, 'Informe quem recebeu o equipamento!') then Exit;
  if MyObjects.FIC(QuemResp = 0, EdQuemResp, 'Informe quem ficou responsável pelo conserto!') then Exit;
  if MyObjects.FIC(DefeitoTxt = '', EdDefeitoTxt, 'Informe a descrição do defeito apresentado!') then Exit;
  //
  Controle := UMyMod.BPGS1I32('fixgereequ', 'Controle', '', '', tsPos,
    ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'fixgereequ', False, [
  'Codigo', 'Tabela', 'GraGruX',
  'QuemRece', 'QuemResp', 'QuemLibe',
  'DtHrRece', 'DtHrLibe', (*'CustoPeca',
  'CustoServ', 'CustoTota',*) 'DefeitoTxt'], [
  'Controle'], [
  Codigo, Tabela, GraGruX,
  QuemRece, QuemResp, QuemLibe,
  DtHrRece, DtHrLibe, (*CustoPeca,
  CustoServ, CustoTota,*) DefeitoTxt], [
  Controle], True) then
  begin
    ReopenFixGereEqu(Controle);
    if CkContinuar.Checked then
    begin
      Geral.MB_Info('Dados salvos com sucesso!');

      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;

      EdGraGruX.ValueVariant   := 0;
      CBGraGruX.KeyValue       := 0;

      EdQuemLibe.ValueVariant  := 0;
      CBQuemLibe.KeyValue      := 0;

      TPDtHrLibe.Date          := 0;
      EdDtHrLibe.ValueVariant  := 0;

      EdDefeitoTxt.Text        := '';

      EdGraGruX.SetFocus;
      //
    end else Close;
  end;
end;

procedure TFmFixGereEqu.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFixGereEqu.EdGraGruXChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    PesquisaPorGraGruX();
end;

procedure TFmFixGereEqu.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaPorNome(Key);
end;

procedure TFmFixGereEqu.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
end;

procedure TFmFixGereEqu.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaPorNome(Key);
end;

procedure TFmFixGereEqu.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFixGereEqu.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrRece, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrResp, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrLibe, Dmod.MyDB);
end;

procedure TFmFixGereEqu.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFixGereEqu.PesquisaPorGraGruX;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  cpl.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (cpl.GraGruX IS NULL) ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
    begin
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
    end;
  end else
    EdReferencia.ValueVariant := '';
end;

procedure TFmFixGereEqu.PesquisaPorNome(Key: Integer);
var
  //Controle
  Nivel1: Integer;
begin
  if Key = VK_F3 then
  begin
    Nivel1 := CuringaLoc.CriaForm('Nivel1', CO_NOME, 'gragru1', Dmod.MyDB, CO_VAZIO);
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM gragrux ',
    'WHERE GraGru1=' + Geral.FF0(Nivel1),
    '']);
    EdGraGruX.ValueVariant := Dmod.QrAux.FieldByName('Controle').AsInteger;
  end;
end;

procedure TFmFixGereEqu.PesquisaPorReferencia(Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
    end;
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
    EdReferencia.ValueVariant := '';
end;

procedure TFmFixGereEqu.ReopenFixGereEqu(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFixGereEqu.RGTabelaClick(Sender: TObject);
begin
  SbGraGruX.Enabled := RGTabela.ItemIndex = 2;
  //
  if RGTabela.ItemIndex = 1 then
  begin
    EdReferencia.Left    := EdGraGruX.Left + EdGraGruX.Width + 1;
    EdReferencia.Visible := True;
    CBGraGruX.Left       := EdGraGruX.Left + EdGraGruX.Width + EdReferencia.Width + 2;
    SbGraGruX.Left       := CBGraGruX.Left + CBGraGruX.Width + 4;
  end else
  begin
    EdReferencia.Visible := False;
    CBGraGruX.Left       := EdGraGruX.Left + EdGraGruX.Width + 1;
    SbGraGruX.Left       := CBGraGruX.Left + CBGraGruX.Width + 4;
  end;
  //
  Dmod.ReopenFixEqui(RGTabela.ItemIndex, QrGraGruX);
end;

procedure TFmFixGereEqu.SbGraGruXClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormFixGXPatr(EdGraGruX.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrGraGruX, VAR_CADASTRO, 'CNTRL_GGX');
end;

end.
