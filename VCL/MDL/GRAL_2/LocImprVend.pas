unit LocImprVend;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkEditDateTimePicker,
  frxClass, frxDBSet, dmkDBLookupComboBox, dmkEditCB;

type
  TFmLocImprVend = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DBGVendas: TdmkDBGridZTO;
    Panel3: TPanel;
    PnAtndCanais: TPanel;
    DBGrid2: TDBGrid;
    Panel5: TPanel;
    Label8: TLabel;
    BtTodosAtndCanais: TBitBtn;
    BtNenhumAtndCanal: TBitBtn;
    QrStqCenCadPsq: TMySQLQuery;
    QrStqCenCadPsqCodigo: TIntegerField;
    QrStqCenCadPsqCodUsu: TIntegerField;
    QrStqCenCadPsqNome: TWideStringField;
    DsStqCenCadPsq: TDataSource;
    QrPrdGrupTip: TMySQLQuery;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    QrPrdGrupTipMadeBy: TSmallintField;
    QrPrdGrupTipFracio: TSmallintField;
    QrPrdGrupTipTipPrd: TSmallintField;
    QrPrdGrupTipNivCad: TSmallintField;
    QrPrdGrupTipFaixaIni: TIntegerField;
    QrPrdGrupTipFaixaFim: TIntegerField;
    QrPrdGrupTipTitNiv1: TWideStringField;
    QrPrdGrupTipTitNiv2: TWideStringField;
    QrPrdGrupTipTitNiv3: TWideStringField;
    QrPrdGrupTipNOME_MADEBY: TWideStringField;
    QrPrdGrupTipNOME_FRACIO: TWideStringField;
    QrPrdGrupTipNOME_TIPPRD: TWideStringField;
    QrPrdGrupTipImpedeCad: TSmallintField;
    QrPrdGrupTipPerComissF: TFloatField;
    QrPrdGrupTipPerComissR: TFloatField;
    QrGraGru3: TMySQLQuery;
    QrGraGru3Codusu: TIntegerField;
    QrGraGru3Nivel3: TIntegerField;
    QrGraGru3Nome: TWideStringField;
    QrGraGru3PrdGrupTip: TIntegerField;
    QrGraGru2: TMySQLQuery;
    QrGraGru2Codusu: TIntegerField;
    QrGraGru2Nivel3: TIntegerField;
    QrGraGru2Nivel2: TIntegerField;
    QrGraGru2Nome: TWideStringField;
    QrGraGru1: TMySQLQuery;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1NOMEGRATAMCAD: TWideStringField;
    QrGraGru1CODUSUGRATAMCAD: TIntegerField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1SIGLAUNIDMED: TWideStringField;
    QrGraGru1NOMEUNIDMED: TWideStringField;
    QrGraGru1CODUSUUNIDMED: TIntegerField;
    QrGraGru1IPI_CST: TSmallintField;
    QrGraGru1IPI_Alq: TFloatField;
    QrGraGru1IPI_cEnq: TWideStringField;
    QrGraGru1TipDimens: TSmallintField;
    QrGraGru1PerCuztMin: TFloatField;
    QrGraGru1PerCuztMax: TFloatField;
    QrGraGru1MedOrdem: TIntegerField;
    QrGraGru1PartePrinc: TIntegerField;
    QrGraGru1NO_MedOrdem: TWideStringField;
    QrGraGru1Medida1: TWideStringField;
    QrGraGru1Medida2: TWideStringField;
    QrGraGru1Medida3: TWideStringField;
    QrGraGru1Medida4: TWideStringField;
    QrGraGru1NO_PartePrinc: TWideStringField;
    QrGraGru1InfAdProd: TWideStringField;
    QrGraGru1SiglaCustm: TWideStringField;
    QrGraGru1HowBxaEstq: TSmallintField;
    QrGraGru1GerBxaEstq: TSmallintField;
    QrGraGru1PIS_CST: TSmallintField;
    QrGraGru1PIS_AlqP: TFloatField;
    QrGraGru1PIS_AlqV: TFloatField;
    QrGraGru1PISST_AlqP: TFloatField;
    QrGraGru1PISST_AlqV: TFloatField;
    QrGraGru1COFINS_CST: TSmallintField;
    QrGraGru1COFINS_AlqP: TFloatField;
    QrGraGru1COFINS_AlqV: TFloatField;
    QrGraGru1COFINSST_AlqP: TFloatField;
    QrGraGru1COFINSST_AlqV: TFloatField;
    QrGraGru1ICMS_modBC: TSmallintField;
    QrGraGru1ICMS_modBCST: TSmallintField;
    QrGraGru1ICMS_pRedBC: TFloatField;
    QrGraGru1ICMS_pRedBCST: TFloatField;
    QrGraGru1ICMS_pMVAST: TFloatField;
    QrGraGru1ICMS_pICMSST: TFloatField;
    QrGraGru1IPI_TpTrib: TSmallintField;
    QrGraGru1IPI_vUnid: TFloatField;
    QrGraGru1ICMS_Pauta: TFloatField;
    QrGraGru1ICMS_MaxTab: TFloatField;
    QrGraGru1IPI_pIPI: TFloatField;
    QrGraGru1cGTIN_EAN: TWideStringField;
    QrGraGru1EX_TIPI: TWideStringField;
    QrGraGru1PIS_pRedBC: TFloatField;
    QrGraGru1PISST_pRedBCST: TFloatField;
    QrGraGru1COFINS_pRedBC: TFloatField;
    QrGraGru1COFINSST_pRedBCST: TFloatField;
    QrGraGru1ICMSRec_pRedBC: TFloatField;
    QrGraGru1IPIRec_pRedBC: TFloatField;
    QrGraGru1PISRec_pRedBC: TFloatField;
    QrGraGru1COFINSRec_pRedBC: TFloatField;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1IPIRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    QrGraGru1ICMSRec_tCalc: TSmallintField;
    QrGraGru1IPIRec_tCalc: TSmallintField;
    QrGraGru1PISRec_tCalc: TSmallintField;
    QrGraGru1COFINSRec_tCalc: TSmallintField;
    QrGraGru1ICMSAliqSINTEGRA: TFloatField;
    QrGraGru1ICMSST_BaseSINTEGRA: TFloatField;
    QrGraGru1FatorClas: TIntegerField;
    QrGraGru1PerComissF: TFloatField;
    QrGraGru1PerComissR: TFloatField;
    QrGraGru1PerComissZ: TSmallintField;
    DsPrdGrupTip: TDataSource;
    DsGraGru3: TDataSource;
    DsGraGru2: TDataSource;
    DsGraGru1: TDataSource;
    QrEstq: TMySQLQuery;
    QrEstqNOMECI: TWideStringField;
    QrEstqNOMEPQ: TWideStringField;
    QrEstqControle: TIntegerField;
    QrEstqPQ: TIntegerField;
    QrEstqCI: TIntegerField;
    QrEstqMinEstq: TFloatField;
    QrEstqLk: TIntegerField;
    QrEstqDataCad: TDateField;
    QrEstqDataAlt: TDateField;
    QrEstqUserCad: TIntegerField;
    QrEstqUserAlt: TIntegerField;
    QrEstqCustoPadrao: TFloatField;
    QrEstqMoedaPadrao: TSmallintField;
    QrEstqEstSegur: TIntegerField;
    QrEstqPeso: TFloatField;
    QrEstqValor: TFloatField;
    QrEstqCusto: TFloatField;
    QrEstqNOMESE: TWideStringField;
    QrEstqNOMEFO: TWideStringField;
    QrEstqCodProprio: TWideStringField;
    QrEstqCustoEstoquePadrao: TFloatField;
    DsEstq: TDataSource;
    frxEstq: TfrxReport;
    frxDsEstq: TfrxDBDataset;
    QrAtndCanalSel: TMySQLQuery;
    QrAtndCanalSelCodigo: TIntegerField;
    QrAtndCanalSelNome: TWideStringField;
    QrAtndCanalSelAtivo: TSmallintField;
    DsAtndCanalSel: TDataSource;
    QrVendas: TMySQLQuery;
    DsVendas: TDataSource;
    QrAux: TMySQLQuery;
    Panel6: TPanel;
    Panel8: TPanel;
    Panel7: TPanel;
    GroupBox6: TGroupBox;
    Label6: TLabel;
    Label19: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    RGStatus: TRadioGroup;
    Panel9: TPanel;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    dmkLabel3: TdmkLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdPrdGrupTip: TdmkEditCB;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    EdGraGru2: TdmkEditCB;
    CBGraGru2: TdmkDBLookupComboBox;
    EdGraGru3: TdmkEditCB;
    CBGraGru3: TdmkDBLookupComboBox;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    Panel10: TPanel;
    frxLOC_IMPRI_002_A: TfrxReport;
    frxDsVendas: TfrxDBDataset;
    QrVendasCodigo: TIntegerField;
    QrVendasCtrID: TIntegerField;
    QrVendasItem: TIntegerField;
    QrVendasManejoLca: TSmallintField;
    QrVendasGraGruX: TIntegerField;
    QrVendasUnidade: TIntegerField;
    QrVendasQuantidade: TFloatField;
    QrVendasPrcUni: TFloatField;
    QrVendasValorTotal: TFloatField;
    QrVendasQtdeProduto: TIntegerField;
    QrVendasValorProduto: TFloatField;
    QrVendasQtdeLocacao: TIntegerField;
    QrVendasValorLocacao: TFloatField;
    QrVendasQtdeDevolucao: TIntegerField;
    QrVendasValDevolParci: TFloatField;
    QrVendasTroca: TSmallintField;
    QrVendasCategoria: TWideStringField;
    QrVendasCOBRANCALOCACAO: TWideStringField;
    QrVendasCobrancaConsumo: TFloatField;
    QrVendasCobrancaRealizadaVenda: TWideStringField;
    QrVendasCobranIniDtHr: TDateTimeField;
    QrVendasSaiDtHr: TDateTimeField;
    QrVendasSMIA_OriCtrl: TIntegerField;
    QrVendasSMIA_IDCtrl: TIntegerField;
    QrVendasLk: TIntegerField;
    QrVendasDataCad: TDateField;
    QrVendasDataAlt: TDateField;
    QrVendasUserCad: TIntegerField;
    QrVendasUserAlt: TIntegerField;
    QrVendasAlterWeb: TSmallintField;
    QrVendasAWServerID: TIntegerField;
    QrVendasAWStatSinc: TSmallintField;
    QrVendasAtivo: TSmallintField;
    QrVendasprod_vFrete: TFloatField;
    QrVendasprod_vSeg: TFloatField;
    QrVendasprod_vDesc: TFloatField;
    QrVendasprod_vOutro: TFloatField;
    QrVendasStqCenCad: TIntegerField;
    QrVendasNF_Stat: TIntegerField;
    QrVendasPercDesco: TFloatField;
    QrVendasValorBruto: TFloatField;
    QrVendasReferencia: TWideStringField;
    QrVendasNivel1: TIntegerField;
    QrVendasNO_PRD: TWideStringField;
    QrVendasPrdGrupTip: TIntegerField;
    QrVendasUnidMed: TIntegerField;
    QrVendasNO_PGT: TWideStringField;
    QrVendasSIGLA: TWideStringField;
    QrVendasNO_TAM: TWideStringField;
    QrVendasNO_COR: TWideStringField;
    QrVendasGraCorCad: TIntegerField;
    QrVendasGraGruC: TIntegerField;
    QrVendasGraGru1: TIntegerField;
    QrVendasGraTamI: TIntegerField;
    BtImprime: TBitBtn;
    QrVendasDtHrSai: TDateTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosAtndCanaisClick(Sender: TObject);
    procedure BtNenhumAtndCanalClick(Sender: TObject);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure frxEstqGetValue(const VarName: string; var Value: Variant);
    procedure EdEmpresaChange(Sender: TObject);
    procedure TPDataIniChange(Sender: TObject);
    procedure TPDataFimChange(Sender: TObject);
    procedure EdPrdGrupTipRedefinido(Sender: TObject);
    procedure EdGraGru3Redefinido(Sender: TObject);
    procedure EdGraGru2Redefinido(Sender: TObject);
    procedure EdGraGru1Redefinido(Sender: TObject);
    procedure EdGraGru1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure QrGraGru2AfterClose(DataSet: TDataSet);
    procedure QrGraGru2AfterScroll(DataSet: TDataSet);
    procedure QrGraGru3AfterClose(DataSet: TDataSet);
    procedure QrGraGru3AfterScroll(DataSet: TDataSet);
    procedure QrGraGru1AfterClose(DataSet: TDataSet);
    procedure QrPrdGrupTipAfterScroll(DataSet: TDataSet);
    procedure QrAtndCanalSelAfterOpen(DataSet: TDataSet);
    procedure frxLOC_IMPRI_002_AGetValue(const VarName: string;
      var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrVendasAfterOpen(DataSet: TDataSet);
    procedure QrVendasBeforeClose(DataSet: TDataSet);
    procedure DBGVendasDblClick(Sender: TObject);
  private
    { Private declarations }
    FEmpresa: Integer;
    FAtndCanal, FCordaAtndCanais: String;
    //
    procedure AtivarTodosAtndCanais(Ativo: Integer);
    procedure FechaPesquisa();
    procedure ReopenAtndCanalSel(Codigo: Integer);
    procedure ReopenGraGru1(Nivel1: Integer);
    procedure ReopenGraGru2(Nivel2: Integer);
    procedure ReopenGraGru3(Nivel3: Integer);
  public
    { Public declarations }
  end;

  var
  FmLocImprVend: TFmLocImprVend;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UCreate, UnDMkProcFunc,
  MyVCLSkin, UnEmpresas, UnGraL_Jan, Principal;

{$R *.DFM}

procedure TFmLocImprVend.AtivarTodosAtndCanais(Ativo: Integer);
begin
  DModG.MyPID_DB.Execute(Geral.ATS([
  'UPDATE ' + FAtndCanal,
  'SET Ativo=' + Geral.FF0(Ativo),
  '']));
  ReopenAtndCanalSel(QrAtndCanalSelCodigo.Value);
end;

procedure TFmLocImprVend.BtImprimeClick(Sender: TObject);
begin
  DBGVendas.DataSource := nil;
  try
    MyObjects.frxDefineDataSets(frxLOC_IMPRI_002_A, [
     frxDsVendas
    ]);
    //
    MyObjects.frxMostra(frxLOC_IMPRI_002_A, 'Vendas');
  finally
    DBGVendas.DataSource := DsVendas;
  end;
end;

procedure TFmLocImprVend.BtNenhumAtndCanalClick(Sender: TObject);
begin
  AtivarTodosAtndCanais(0);
end;

procedure TFmLocImprVend.BtOKClick(Sender: TObject);
var
  sEmpresa, SQL_AtndCanal, SQL_Status: String;
  Empresa, PrdGrupTip, GraGru1, GraGru2, GraGru3, StqCenCad: Integer;
  SQL_PrdGrupTip, SQL_GraGru1, SQL_GraGru2, SQL_GraGru3, SQL_StqCenCad: String;
begin
  Empresa    := EdEmpresa.ValueVariant;
  sEmpresa   := Geral.FF0(DModG.QrEmpresasCodigo.Value);
  GraGru1    := EdGraGru1.ValueVariant;
  GraGru2    := EdGraGru2.ValueVariant;
  GraGru3    := EdGraGru3.ValueVariant;
  PrdGrupTip := EdPrdGrupTip.ValueVariant;
  StqCenCad  := EdStqCenCad.ValueVariant;

  SQL_AtndCanal := EmptyStr;
  SQL_Status := EmptyStr;
  SQL_PrdGrupTip := EmptyStr;
  SQL_GraGru1 := EmptyStr;
  SQL_GraGru2 := EmptyStr;
  SQL_GraGru3 := EmptyStr;
  SQL_StqCenCad := EmptyStr;

  if Trim(FCordaAtndCanais) <> EmptyStr then
    SQL_AtndCanal := 'AND its.AtndCanal IN (' + FCordaAtndCanais + ')';
  if RGStatus.ItemIndex < 3 then
    SQL_Status := 'AND its.Status=' + Geral.FF0(RGStatus.ItemIndex);
  if PrdGrupTip <> 0 then
    SQL_PrdGrupTip := 'AND gg1.PrdGrupTip=' + FormatFloat('0', QrPrdGrupTipCodigo.Value);
  if GraGru1 <> 0 then
    SQL_GraGru1 := 'AND ggx.GraGru1=' + FormatFloat('0', QrGraGru1Nivel1.Value);
  if GraGru2 <> 0 then
    SQL_GraGru2 := 'AND gg1.Nivel2=' + FormatFloat('0', QrGraGru2Nivel2.Value);
  if GraGru3 <> 0 then
    SQL_GraGru3 := 'AND gg1.Nivel3=' + FormatFloat('0', QrGraGru3Nivel3.Value);
  if StqCenCad <> 0 then
  // ini 2023-06-02
    //SQL_StqCenCad := 'AND smia.StqCenCad=' + FormatFloat('0', QrStqCenCadPsqCodigo.Value);
    SQL_StqCenCad := 'AND ven.StqCenCad=' + FormatFloat('0', QrStqCenCadPsqCodigo.Value);
  // fim 2023-06-02

{  // Atualizar gragrux._Marca_
  Fazer?
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando marcas de reduzidos');
  Dmod.MyDB.Execute(Geral.ATS([ ...
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVendas, Dmod.MyDB, [
  'SELECT cab.DtHrSai, ven.*,  ',
  'gg1.Referencia, gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,   ',
  'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,   ',
  'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,   ',
  'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI  ',
  '   ',
  'FROM loccitsven ven  ',
  'LEFT JOIN loccconits its ON its.CtrID=ven.CtrID  ',
  'LEFT JOIN loccconcab cab ON cab.Codigo=its.Codigo  ',
  '  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=ven.GraGruX   ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip   ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed   ',
  '  ',
  'WHERE cab.Empresa=' + sEmpresa,
  dmkPF.SQL_Periodo('AND cab.DtHrEmi ',
      TPDataIni.Date, TPDataFim.Date + 1, True, True), // +1 porque � dd/mm/yyyy hh:nn:ss
  //'AND smia.DataHora  BETWEEN "2021-01-23" AND "2021-04-30 23:59:59" ',
  SQL_AtndCanal,
  SQL_Status,
  SQL_PrdGrupTip,
  SQL_GraGru1,
  SQL_GraGru2,
  SQL_GraGru3,
  SQL_StqCenCad,

  'ORDER BY NO_PRD',
  '']);
  //Geral.MB_Teste(QrVendas.SQL.Text);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmLocImprVend.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocImprVend.BtTodosAtndCanaisClick(Sender: TObject);
begin
  AtivarTodosAtndCanais(1);
end;

procedure TFmLocImprVend.DBGrid2CellClick(Column: TColumn);
var
  AtndCanal, Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    AtndCanal := QrAtndCanalSelCodigo.Value;
    if QrAtndCanalSelAtivo.Value = 0 then
      Ativo := 1
    else
      Ativo := 0;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'UPDATE ' + FAtndCanal,
    'SET Ativo=' + Geral.FF0(Ativo),
    'WHERE Codigo=' + Geral.FF0(AtndCanal),
    '']);
    //
    ReopenAtndCanalSel(AtndCanal);
  end;
end;

procedure TFmLocImprVend.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGrid2, Rect, 1, QrAtndCanalSelAtivo.Value);
end;

procedure TFmLocImprVend.DBGVendasDblClick(Sender: TObject);
var
  //Campo: String;
  Codigo: Integer;
begin
  if(QrVendas.State <> dsInactive) and (QrVendas.RecordCount > 0) then
  begin
    //Campo := TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName;
    Codigo := QrVendasCodigo.Value;
    //
    GraL_Jan.MostraFormLocCCon(True, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPagerNovo, Codigo);
  end;
end;

procedure TFmLocImprVend.EdEmpresaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmLocImprVend.EdGraGru1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
  begin
    EdPrdGrupTip.ValueVariant := 0;
    CBPrdGrupTip.KeyValue     := 0;
    //
    EdGraGru3.ValueVariant := 0;
    CBGraGru3.KeyValue     := 0;
    //
    EdGraGru2.ValueVariant := 0;
    CBGraGru2.KeyValue     := 0;
    //
    ReopenGraGru1(0);
  end;
end;

procedure TFmLocImprVend.EdGraGru1Redefinido(Sender: TObject);
begin
 FechaPesquisa();
end;

procedure TFmLocImprVend.EdGraGru2Redefinido(Sender: TObject);
begin
 FechaPesquisa();
end;

procedure TFmLocImprVend.EdGraGru3Redefinido(Sender: TObject);
begin
 FechaPesquisa();
end;

procedure TFmLocImprVend.EdPrdGrupTipRedefinido(Sender: TObject);
{
var
  PrdGrupTip: Integer;
}
begin
  //PrdGrupTip := EdPrdGrupTip.ValueVariant;
  //ReopenGraGru1(PrdGrupTip);
  QrGraGru3.Close;
  QrGraGru2.Close;
  QrGraGru1.Close;
  //
  case QrPrdgrupTipNivCad.Value of
    3: ReopenGraGru3(0);
    2: ReopenGraGru2(0);
    1: ReopenGraGru1(0);
  end;
  FechaPesquisa();
end;

procedure TFmLocImprVend.FechaPesquisa();
begin
  QrVendas.Close;
end;

procedure TFmLocImprVend.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocImprVend.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCordaAtndCanais := '';
  //
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //

  //
  //
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCadPsq, Dmod.MyDB);
  //
  FAtndCanal := UCriar.RecriaListaSimplesSelMany(DmodG.MyPID_DB, 'atndcanal');
  ReopenAtndCanalSel(0);
  //
  TPDataIni.Date := Date - 30;
  TPDataFim.Date := Date;
  //
(*
  if QrStqCenCadPsq.Locate('Codigo', 1, []) then
  begin
    EdStqCenCad.ValueVariant := 1;
    CBStqCenCad.KeyValue     := 1;
  end;
*)
end;

procedure TFmLocImprVend.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocImprVend.frxEstqGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := CBEmpresa.Text
  else
{
  if AnsiCompareText(VarName, 'VFR_AGR1NOME') = 0 then
  begin
    case PCRelatorio.ActivePageIndex of
      0:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrEstqNOMECI.Value;
          1: Value := QrEstqNOMESE.Value;
          2: Value := QrEstqNOMEPQ.Value;
          3: Value := QrEstqNOMEFO.Value;
          else Value := '';
        end;
      end;
      2:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrUsoTNOMEORI.Value;
          1: Value := QrUsoTNOMESE.Value;
          2: Value := QrUsoTNOMEPQ.Value;
          3: Value := QrUsoTNOMEFO.Value;
          else Value := '';
        end;
      end;
      3,5:
      begin
        if CkNovaForma6.Checked and (PCRelatorio.ActivePageIndex = 5) then
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQNomeCI.Value;
            1: Value := QrMoviPQNomeSE.Value;
            2: Value := QrMoviPQNomePQ.Value;
            3: Value := QrMoviPQNomeFO.Value;
            else Value := '';
          end;
        end else
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQ3NomeCI.Value;
            1: Value := QrMoviPQ3NomeSE.Value;
            2: Value := QrMoviPQ3NomePQ.Value;
            3: Value := QrMoviPQ3NomeFO.Value;
            else Value := '';
          end;
        end;
      end;
      9:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrPQRSumNOMECI.Value;
          1: Value := QrPQRSumNOMESE.Value;
          2: Value := QrPQRSumNOMEPQ.Value;
          3: Value := QrPQRSumNOMEFO.Value;
          else Value := '';
        end;
      end;
    end;
  end else if AnsiCompareText(VarName, 'VFR_AGR2NOME') = 0 then
  begin
    case PCRelatorio.ActivePageIndex of
      0:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrEstqNOMECI.Value;
          1: Value := QrEstqNOMESE.Value;
          2: Value := QrEstqNOMEPQ.Value;
          3: Value := QrEstqNOMEFO.Value;
          else Value := '';
        end;
      end;
      2:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrUsoTNOMEORI.Value;
          1: Value := QrUsoTNOMESE.Value;
          2: Value := QrUsoTNOMEPQ.Value;
          3: Value := QrUsoTNOMEFO.Value;
          else Value := '';
        end;
      end;
      3,5:
      begin
        if CkNovaForma6.Checked and (PCRelatorio.ActivePageIndex = 5) then
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQ3NomeCI.Value;
            1: Value := QrMoviPQ3NomeSE.Value;
            2: Value := QrMoviPQ3NomePQ.Value;
            3: Value := QrMoviPQ3NomeFO.Value;
            else Value := '';
          end;
        end else
        begin
          case RGOrdem2.ItemIndex of
            0: Value := QrMoviPQNomeCI.Value;
            1: Value := QrMoviPQNomeSE.Value;
            2: Value := QrMoviPQNomePQ.Value;
            3: Value := QrMoviPQNomeFO.Value;
            else Value := '';
          end;
        end;
      end;
      9:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrPQRSumNOMECI.Value;
          1: Value := QrPQRSumNOMESE.Value;
          2: Value := QrPQRSumNOMEPQ.Value;
          3: Value := QrPQRSumNOMEFO.Value;
          else Value := '';
        end;
      end;
    end;
  end else if AnsiCompareText(VarName, 'VAR_DATA_HORA') = 0 then
    Value := Geral.Maiusculas(Geral.FDT(Now(), 8), False)
  else if AnsiCompareText(VarName, 'VAR_PQNOME') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBPQ.Text, EdPQ.ValueVariant)
  else if AnsiCompareText(VarName, 'VAR_CINOME') = 0 then
    Value := dmkPF.ParValueCodTxt('', CBCI.Text, EdCI.ValueVariant)
  else if AnsiCompareText(VarName, 'VAR_AMOSTRAS') = 0 then
    Value := ''
  else if AnsiCompareText(VarName, 'VAR_AtndCanaisTXT') = 0 then
    Value := FAtndCanaisTxt
  else if AnsiCompareText(VarName, 'VAR_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp(TPIni.Date, TPFim.Date, 0, 0, True, True,
    False, False, '', '')


  else if AnsiCompareText(VarName, 'VFR_AGR')  = 0 then Value := RGAgrupa.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD1') = 0 then Value := RGOrdem1.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD2') = 0 then Value := RGOrdem2.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD3') = 0 then Value := RGOrdem3.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_GRD') = 0 then
    Value := dmkPF.BoolToInt2(CkGrade.Checked, 15, 0)
  //else if AnsiCompareText(VarName, 'VARF_PECA') = 0 then
    //Value := CBUnidade.Text;

  else if AnsiCompareText(VarName, 'VFR_EMPRESA') = 0 then Value := CBCI.Text
  else if AnsiCompareText(VarName, 'VARF_GRUPO') = 0 then
    Value := QrPQUCabNome.Value
  else
}
end;



procedure TFmLocImprVend.frxLOC_IMPRI_002_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VFR_EMPRESA' then Value := dmkPF.CampoReportTxt(CBEmpresa.Text, 'TODAS')
  else if VarName = 'VFR_NO_PGT' then Value := dmkPF.CampoReportTxt(CBPrdGrupTip.Text, 'TODOS')
  else if VarName = 'VFR_NO_PRD' then Value := dmkPF.CampoReportTxt(CBGraGru1.Text, 'TODOS')
  else if VarName = 'VFR_NO_NI2' then Value := dmkPF.CampoReportTxt(CBGraGru2.Text, 'TODOS')
  else if VarName = 'DATAHORA_TXT' then Value := Geral.FDT(DModG.ObtemAgora(), 8)
  else if VarName = 'DATA_EM_TXT' then
    Value := Geral.FDT(TPDataFim.Date, 2)
  else if VarName = 'PERIODO_TXT' then Value := dmkPF.PeriodoImp(
    TPDataIni.Date, TPDataFim.Date, 0, 0, True, True, False, False, '', '')
{
  else if VarName = 'VARF_PGT_VISIBLE' then Value := PertenceAoGrupoPGT()
  else if VarName = 'VARF_PRD_VISIBLE' then Value := PertenceAoGrupoPrd()
  else if VarName = 'VARF_NI2_VISIBLE' then Value := PertenceAoGrupoNi2()
  else if VarName = 'VFR_LISTA_PRECO' then Value := CBGraCusPrc.Text
  //
  else if VarName = 'PERIODO_TXT_2' then Value := dmkPF.PeriodoImp(
    TPDataIni2.Date, TPDataFim2.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'PERIODO_TXT_5' then Value := dmkPF.PeriodoImp(
    TPDataIni5.Date, TPDataFim5.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'PERIODO_TXT_7' then Value := dmkPF.PeriodoImp(
    TPDataIni7.Date, TPDataFim7.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'VFR_FILTRO_5' then
  begin
   Value := 'TUDO';
    case CBFiltro5.Value of
      //0: CBFiltro5.SetMaxValue; // Tudo
      1: Value := 'Baixa correta';
      2: Value := 'Sem baixa';
      3: Value := 'Baixa correta ou sem baixa';
      4: Value := 'Erro baixa';
      5: Value := 'Baixa correta ou incorreta';
      6: Value := 'Baixa incorreta ou sem baixa';
      7: ; //Tudo
    end;
  end;
}
end;

procedure TFmLocImprVend.QrAtndCanalSelAfterOpen(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, DModG.MyPID_DB, [
  'SELECT GROUP_CONCAT(Codigo) Corda ',
  'FROM _atndcanal_ ',
  'WHERE Ativo=1 ',
  '']);
  FCordaAtndCanais := QrAux.FieldByName('Corda').AsString;
end;

procedure TFmLocImprVend.QrGraGru1AfterClose(DataSet: TDataSet);
begin
  EdGraGru1.ValueVariant := 0;
  CBGraGru1.KeyValue := 0;
end;

procedure TFmLocImprVend.QrGraGru2AfterClose(DataSet: TDataSet);
begin
  ReopenGraGru1(0);
  EdGraGru2.ValueVariant := 0;
  CBGraGru2.KeyValue := 0;
end;

procedure TFmLocImprVend.QrGraGru2AfterScroll(DataSet: TDataSet);
begin
  ReopenGraGru1(0);
end;

procedure TFmLocImprVend.QrGraGru3AfterClose(DataSet: TDataSet);
begin
  ReopenGraGru2(0);
  EdGraGru3.ValueVariant := 0;
  CBGraGru3.KeyValue := 0;
end;

procedure TFmLocImprVend.QrGraGru3AfterScroll(DataSet: TDataSet);
begin
  ReopenGraGru2(0);
end;

procedure TFmLocImprVend.QrPrdGrupTipAfterScroll(DataSet: TDataSet);
begin
  case QrPrdGrupTipNivCad.Value of
    3: ReopenGraGru3(0);
    2: ReopenGraGru2(0);
    1: ReopenGraGru1(0);
  end;
end;

procedure TFmLocImprVend.QrVendasAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := QrVendas.RecordCount > 0;
end;

procedure TFmLocImprVend.QrVendasBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmLocImprVend.ReopenAtndCanalSel(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtndCanalSel, DModG.MyPID_DB, [
  'SELECT * FROM ' + FAtndCanal,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmLocImprVend.ReopenGraGru1(Nivel1: Integer);
var
  COD_PGT, COD_GG2, COD_GG3: Integer;
  SQL_PGT, SQL_GG2, SQL_GG3: String;
begin
  SQL_PGT := '';
  SQL_GG2 := '';
  SQL_GG3 := '';
  //
  COD_PGT := EdPrdGrupTip.ValueVariant;
  COD_GG2 := EdGraGru2.ValueVariant;
  COD_GG3 := EdGraGru2.ValueVariant;
  //
  if COD_PGT <> 0 then
  begin
    SQL_PGT := 'WHERE gg1.PrdGrupTip=' + Geral.FF0(COD_PGT);
    //
    if COD_GG2 <> 0 then
      SQL_GG2 := 'AND gg1.Nivel2=' + Geral.FF0(COD_GG2);
    //
    if COD_GG3 <> 0 then
      SQL_GG3 := 'AND gg1.Nivel3=' + Geral.FF0(COD_GG3);
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
  'SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1, ',
  'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, ',
  'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD, ',
  'gg1.CST_A,gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso, ',
  'gg1.IPI_Alq, gg1.IPI_CST, gg1.IPI_cEnq, gg1.TipDimens, ',
  'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, ',
  'unm.Nome NOMEUNIDMED, gg1.PartePrinc,  ',
  'gg1.PerCuztMin, gg1.PerCuztMax, gg1.MedOrdem, gg1.FatorClas, ',
  'mor.Nome NO_MedOrdem, mor.Medida1, mor.Medida2, ',
  'mor.Medida3, mor.Medida4, mpc.Nome NO_PartePrinc, ',
  'InfAdProd, SiglaCustm, HowBxaEstq, GerBxaEstq, ',
  'PIS_CST, PIS_AlqP, PIS_AlqV, PISST_AlqP, PISST_AlqV, ',
  'COFINS_CST, COFINS_AlqP, COFINS_AlqV,COFINSST_AlqP,  ',
  'COFINSST_AlqV, ICMS_modBC, ICMS_modBCST, ICMS_pRedBC, ',
  'ICMS_pRedBCST, ICMS_pMVAST, ICMS_pICMSST, ',
  'IPI_pIPI, IPI_TpTrib, IPI_vUnid, EX_TIPI, ',
  'ICMS_Pauta, ICMS_MaxTab, cGTIN_EAN, ',
  'PIS_pRedBC, PISST_pRedBCST, ',
  'COFINS_pRedBC, COFINSST_pRedBCST, ',
  ' ',
  'ICMSRec_pRedBC, IPIRec_pRedBC, ',
  'PISRec_pRedBC, COFINSRec_pRedBC, ',
  'ICMSRec_pAliq, IPIRec_pAliq, ',
  'PISRec_pAliq, COFINSRec_pAliq, ',
  'ICMSRec_tCalc, IPIRec_tCalc, ',
  'PISRec_tCalc, COFINSRec_tCalc, ',
  'ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA, ',
  'gg1.PerComissF, gg1.PerComissR, gg1.PerComissZ ',
  ' ',
  'FROM gragru1 gg1 ',
  'LEFT JOIN gragru2   gg2 ON gg1.Nivel2=gg2.Nivel2 ',
  'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN medordem mor ON mor.Codigo= gg1.MedOrdem ',
  'LEFT JOIN matpartcad mpc ON mpc.Codigo=gg1.PartePrinc ',
  //'WHERE gg1.PrdGrupTip=:P0 ',
  SQL_PGT,
  //'AND gg1.Nivel2=:P1 ',
  SQL_GG2,
  SQL_GG3,
  'ORDER BY gg1.Nome ',
  '']);
end;

procedure TFmLocImprVend.ReopenGraGru2(Nivel2: Integer);
var
  Nivel3: Integer;
begin
  QrGraGru2.Close;
  if QrPrdGrupTipNivCad.Value >= 2 then
  begin
    if QrGraGru3.State = dsInactive then
      Nivel3 := 0
    else
      Nivel3 := QrGraGru3Nivel3.Value;
    QrGraGru2.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
    QrGraGru2.Params[1].AsInteger := Nivel3;
    //QrGraGru2.Params[2].AsString  := '%' + EdFiltroNiv2.Text + '%';
    UnDmkDAC_PF.AbreQuery(QrGraGru2, Dmod.MyDB);
    //
    if Nivel2 <> 0 then
      QrGraGru2.Locate('Nivel2', Nivel2, []);
  end;
end;

procedure TFmLocImprVend.ReopenGraGru3(Nivel3: Integer);
begin
  QrGraGru3.Close;
  if QrPrdGrupTipNivCad.Value >= 3 then
  begin
    QrGraGru3.Params[0].AsInteger := QrPrdGrupTipCodigo.Value;
    //QrGraGru3.Params[1].AsString  := '%' + EdFiltroNiv3.Text + '%';
    UnDmkDAC_PF.AbreQuery(QrGraGru3, Dmod.MyDB);
    //
    if Nivel3 <> 0 then
      QrGraGru3.Locate('Nivel3', Nivel3, []);
  end;
end;

procedure TFmLocImprVend.TPDataFimChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmLocImprVend.TPDataIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
