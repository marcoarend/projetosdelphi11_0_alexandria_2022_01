object FmLocCConPsq: TFmLocCConPsq
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-111 :: Pesquisa de Loca'#231#227'o'
  ClientHeight = 629
  ClientWidth = 1342
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1342
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1294
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1246
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 260
        Height = 32
        Caption = 'Pesquisa de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 260
        Height = 32
        Caption = 'Pesquisa de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 260
        Height = 32
        Caption = 'Pesquisa de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1342
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1338
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1342
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 1196
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1194
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 280
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Preview'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1342
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 1342
      Height = 61
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label17: TLabel
        Left = 264
        Top = 16
        Width = 35
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label18: TLabel
        Left = 937
        Top = 16
        Width = 62
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Requisitante:'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label19: TLabel
        Left = 1138
        Top = 16
        Width = 126
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Respons'#225'vel pela retirada:'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object GroupBox1: TGroupBox
        Left = 4
        Top = 0
        Width = 253
        Height = 61
        Caption = ' Per'#237'odo: '
        TabOrder = 0
        object Panel3: TPanel
          Left = 2
          Top = 15
          Width = 249
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label21: TLabel
            Left = 7
            Top = 1
            Width = 42
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Emiss'#227'o:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label1: TLabel
            Left = 119
            Top = 21
            Width = 15
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'at'#233
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object TPDataEmiIni: TdmkEditDateTimePicker
            Left = 9
            Top = 16
            Width = 104
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 41131.724786689820000000
            Time = 41131.724786689820000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DataEmi'
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPDataEmiFim: TdmkEditDateTimePicker
            Left = 141
            Top = 16
            Width = 104
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 41131.724786689820000000
            Time = 41131.724786689820000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DataEmi'
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
      end
      object EdCliente: TdmkEditCB
        Left = 264
        Top = 32
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 317
        Top = 32
        Width = 612
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientes
        ParentFont = False
        TabOrder = 2
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdECComprou: TdmkEditCB
        Left = 936
        Top = 32
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ECComprou'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBECComprou
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBECComprou: TdmkDBLookupComboBox
        Left = 990
        Top = 32
        Width = 142
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        KeyField = 'Controle'
        ListField = 'Nome'
        ListSource = DsECComprou
        ParentFont = False
        TabOrder = 4
        dmkEditCB = EdECComprou
        QryCampo = 'ECComprou'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdECRetirou: TdmkEditCB
        Left = 1138
        Top = 32
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ECRetirou'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBECRetirou
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBECRetirou: TdmkDBLookupComboBox
        Left = 1190
        Top = 32
        Width = 142
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        KeyField = 'Controle'
        ListField = 'Nome'
        ListSource = DsECRetirou
        ParentFont = False
        TabOrder = 6
        dmkEditCB = EdECRetirou
        QryCampo = 'ECRetirou'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 61
      Width = 1342
      Height = 160
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object EdParteTexto: TdmkEdit
        Left = 8
        Top = 24
        Width = 1057
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'LocalObra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkGGY_1024: TCheckBox
        Left = 8
        Top = 50
        Width = 109
        Height = 17
        Caption = 'Patrim'#244'nio Prim'#225'rio:'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
      object CkGGY_2048: TCheckBox
        Left = 128
        Top = 50
        Width = 129
        Height = 17
        Caption = 'Patrimonio Secund'#225'rio:'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
      object CkGGY_3072: TCheckBox
        Left = 264
        Top = 50
        Width = 73
        Height = 17
        Caption = 'Acess'#243'rio:'
        TabOrder = 3
      end
      object CkGGY_4096: TCheckBox
        Left = 344
        Top = 50
        Width = 105
        Height = 17
        Caption = 'Material de apoio:'
        TabOrder = 4
      end
      object CkGGY_5120: TCheckBox
        Left = 452
        Top = 50
        Width = 101
        Height = 17
        Caption = 'Material de Uso:'
        TabOrder = 5
      end
      object CkGGY_6144: TCheckBox
        Left = 556
        Top = 50
        Width = 125
        Height = 17
        Caption = 'Materila de consumo:'
        TabOrder = 6
      end
      object CkGGY_7168: TCheckBox
        Left = 688
        Top = 50
        Width = 65
        Height = 17
        Caption = 'Servi'#231'o:'
        TabOrder = 7
      end
      object CkGGY_8192: TCheckBox
        Left = 756
        Top = 50
        Width = 125
        Height = 17
        Caption = 'Mercadoria de venda:'
        TabOrder = 8
      end
      object EdParteProd: TdmkEdit
        Left = 8
        Top = 68
        Width = 1057
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'LocalObra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkLocalObra: TCheckBox
        Left = 8
        Top = 2
        Width = 109
        Height = 17
        Caption = 'Endere'#231'o da obra:'
        Checked = True
        State = cbChecked
        TabOrder = 10
      end
      object CkLocalCntat: TCheckBox
        Left = 124
        Top = 2
        Width = 101
        Height = 17
        Caption = 'Contato na obra:'
        Checked = True
        State = cbChecked
        TabOrder = 11
      end
      object CkNumOC: TCheckBox
        Left = 228
        Top = 2
        Width = 93
        Height = 17
        Caption = 'N'#186' Requisi'#231#227'o:'
        Checked = True
        State = cbChecked
        TabOrder = 12
      end
      object CkEndCobra: TCheckBox
        Left = 320
        Top = 3
        Width = 129
        Height = 17
        Caption = 'Endere'#231'o de cobran'#231'a:'
        Checked = True
        State = cbChecked
        TabOrder = 13
      end
      object CkObs0: TCheckBox
        Left = 460
        Top = 3
        Width = 93
        Height = 17
        Caption = 'Observa'#231#227'o 1:'
        Checked = True
        State = cbChecked
        TabOrder = 14
      end
      object CkObs1: TCheckBox
        Left = 556
        Top = 3
        Width = 101
        Height = 17
        Caption = 'Observa'#231#227'o 2:'
        Checked = True
        State = cbChecked
        TabOrder = 15
      end
      object CkObs2: TCheckBox
        Left = 652
        Top = 3
        Width = 93
        Height = 17
        Caption = 'Observa'#231#227'o 3:'
        Checked = True
        State = cbChecked
        TabOrder = 16
      end
      object CkHistImprime: TCheckBox
        Left = 744
        Top = 2
        Width = 149
        Height = 17
        Caption = 'Observa'#231#245'es imprim'#237'veis:'
        Checked = True
        State = cbChecked
        TabOrder = 17
      end
      object CkHistNaoImpr: TCheckBox
        Left = 888
        Top = 2
        Width = 165
        Height = 17
        Caption = 'Observa'#231#245'es n'#227'o imprim'#237'veis:'
        Checked = True
        State = cbChecked
        TabOrder = 18
      end
      object CGStatus: TdmkCheckGroup
        Left = 8
        Top = 92
        Width = 1057
        Height = 49
        Caption = '  Status da loca'#231#227'o: '
        Columns = 4
        ItemIndex = 3
        Items.Strings = (
          'Aberto'
          'Locado'
          'Finalizado'
          'Cancelado')
        TabOrder = 19
        UpdType = utYes
        Value = 8
        OldValor = 0
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 221
      Width = 1342
      Height = 246
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 2
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = ' Itens '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGIts: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 1334
          Height = 218
          Align = alClient
          DataSource = DsPsqIts
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGItsDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'N'#176' CT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrEmi'
              Title.Caption = 'Emiss'#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrBxa_TXT'
              Title.Caption = 'Dt Encerr.'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrDevolver'
              Title.Caption = 'Data/hora devolver'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TipoAluguel'
              Title.Caption = 'A'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_STATUS'
              Title.Caption = 'Status'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CLI'
              Title.Caption = 'Cliente'
              Width = 365
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Patrimonio'
              Title.Caption = 'Patrim'#244'nio'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Referencia'
              Title.Caption = 'Refer'#234'ncia'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Equipamento'
              Width = 438
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrRetorn'
              Title.Caption = 'Retorno'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Cabe'#231'alhos '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGCab: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 1334
          Height = 218
          Align = alClient
          DataSource = DsPsqCab
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGCabDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'N'#176' CT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrEmi'
              Title.Caption = 'Emiss'#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrBxa_TXT'
              Title.Caption = 'Dt Encerr.'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TipoAluguel'
              Title.Caption = 'A'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_STATUS'
              Title.Caption = 'Status'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CLI'
              Title.Caption = 'Cliente'
              Width = 365
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Patrimonio'
              Title.Caption = 'Patrim'#244'nio'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Referencia'
              Title.Caption = 'Refer'#234'ncia'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Equipamento'
              Width = 438
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrRetorn'
              Title.Caption = 'Retorno'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Visible = True
            end>
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Top = 3
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Ativo = 1'
      'ORDER BY NOMEENTIDADE')
    Left = 64
    Top = 360
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 64
    Top = 404
  end
  object QrECComprou: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM enticontat'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 132
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrECComprouControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrECComprouNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsECComprou: TDataSource
    DataSet = QrECComprou
    Left = 132
    Top = 404
  end
  object QrECRetirou: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM enticontat'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 208
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Controle'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsECRetirou: TDataSource
    DataSet = QrECRetirou
    Left = 208
    Top = 408
  end
  object QrPsqIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPsqItsAfterOpen
    BeforeClose = QrPsqItsBeforeClose
    SQL.Strings = (
      '')
    Left = 376
    Top = 280
    object QrPsqItsNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 16
    end
    object QrPsqItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPsqItsDtHrEmi: TDateTimeField
      FieldName = 'DtHrEmi'
    end
    object QrPsqItsDtHrLocado_TXT: TWideStringField
      FieldName = 'DtHrLocado_TXT'
      Size = 19
    end
    object QrPsqItsSaiDtHr: TDateTimeField
      FieldName = 'SaiDtHr'
    end
    object QrPsqItsDtHrBxa: TDateTimeField
      FieldName = 'DtHrBxa'
    end
    object QrPsqItsDtHrBxa_TXT: TWideStringField
      FieldName = 'DtHrBxa_TXT'
      Size = 19
    end
    object QrPsqItsDtHrRetorn_TXT: TWideStringField
      FieldName = 'DtHrRetorn_TXT'
      Required = True
    end
    object QrPsqItsTipoAluguel: TWideStringField
      FieldName = 'TipoAluguel'
      Size = 1
    end
    object QrPsqItsStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrPsqItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrPsqItsNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrPsqItsPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 25
    end
    object QrPsqItsReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrPsqItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPsqItsNO_TipoAluguel: TWideStringField
      FieldName = 'NO_TipoAluguel'
    end
    object QrPsqItsDtHrDevolver: TDateTimeField
      FieldName = 'DtHrDevolver'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
  end
  object DsPsqIts: TDataSource
    DataSet = QrPsqIts
    Left = 376
    Top = 328
  end
  object frxLOC_PATRI_111_001: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41802.645199189800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if (<LogoNFeExiste> = True) then'
      '  begin              '
      
        '    Picture2.Visible := True;                                   ' +
        '           '
      '    Picture2.LoadFromFile(<LogoNFePath>);'
      '  end else'
      '  begin'
      
        '    Picture2.Visible := False;                                  ' +
        '            '
      '  end;'
      'end.')
    OnGetValue = frxLOC_PATRI_111_001GetValue
    Left = 376
    Top = 432
    Datasets = <
      item
        DataSet = frxDsEndContratada
        DataSetName = 'frxDsEndContratada'
      end
      item
        DataSet = frxDsEndContratante
        DataSetName = 'frxDsEndContratante'
      end
      item
        DataSet = frxDsPsqIts
        DataSetName = 'frxDsPsqIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GroupHeader10: TfrxGroupHeader
        FillType = ftBrush
        Height = 45.354350240000000000
        Top = 238.110390000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsPsqIts."Codigo"'
        object Memo1: TfrxMemoView
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#176' CT')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 151.181200000000000000
          Width = 102.047310000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Status do atendimento')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 15.118120000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDsPsqIts
          DataSetName = 'frxDsPsqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqIts."Codigo"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 151.181200000000000000
          Top = 15.118120000000000000
          Width = 102.047310000000000000
          Height = 15.118110240000000000
          DataField = 'NO_STATUS'
          DataSet = frxDsPsqIts
          DataSetName = 'frxDsPsqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPsqIts."NO_STATUS"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 15.118120000000000000
          Top = 30.236240000000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 192.756030000000000000
          Top = 30.236240000000000000
          Width = 253.228510000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 105.826840000000000000
          Top = 30.236240000000000000
          Width = 86.929190000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Patrim'#244'nio')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 445.984540000000000000
          Top = 30.236240000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data/hora Loca'#231#227'o')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 514.016080000000000000
          Top = 30.236240000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data/hora Sa'#237'da')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 582.047620000000000000
          Top = 30.236240000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data/hora Baixa')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 650.079160000000000000
          Top = 30.236240000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data/hora Retorno')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 64.252010000000000000
          Width = 86.929190000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Periodicidade')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 64.252010000000000000
          Top = 15.118120000000000000
          Width = 86.929190000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPsqIts
          DataSetName = 'frxDsPsqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqIts."NO_TipoAluguel"]')
          ParentFont = False
        end
      end
      object GroupFooter8: TfrxGroupFooter
        FillType = ftBrush
        Height = 3.779517800000000000
        Top = 343.937230000000000000
        Width = 718.110700000000000000
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        DataSet = frxDsPsqIts
        DataSetName = 'frxDsPsqIts'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo21: TfrxMemoView
          Left = 15.118120000000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DataField = 'Referencia'
          DataSet = frxDsPsqIts
          DataSetName = 'frxDsPsqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqIts."Referencia"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 192.756030000000000000
          Width = 253.228510000000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsPsqIts
          DataSetName = 'frxDsPsqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPsqIts."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 105.826840000000000000
          Width = 86.929190000000000000
          Height = 15.118110240000000000
          DataField = 'Patrimonio'
          DataSet = frxDsPsqIts
          DataSetName = 'frxDsPsqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqIts."Patrimonio"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 445.984540000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'DtHrLocado_TXT'
          DataSet = frxDsPsqIts
          DataSetName = 'frxDsPsqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqIts."DtHrLocado_TXT"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'SaiDtHr'
          DataSet = frxDsPsqIts
          DataSetName = 'frxDsPsqIts'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqIts."SaiDtHr"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'DtHrBxa_TXT'
          DataSet = frxDsPsqIts
          DataSetName = 'frxDsPsqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqIts."DtHrBxa_TXT"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 650.079160000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'DtHrRetorn_TXT'
          DataSet = frxDsPsqIts
          DataSetName = 'frxDsPsqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqIts."DtHrRetorn_TXT"]')
          ParentFont = False
        end
      end
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        Height = 115.275590551181000000
        Top = 98.267780000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Shape2: TfrxShapeView
          Left = 147.401670000000000000
          Top = 10.960637240000000000
          Width = 540.472790000000000000
          Height = 45.354360000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Shape3: TfrxShapeView
          Left = 147.401670000000000000
          Top = 67.653587240000000000
          Width = 540.472790000000000000
          Height = 45.354360000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo4: TfrxMemoView
          Left = 404.409685590000000000
          Top = 14.740157480000000000
          Width = 41.574798270000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 445.984483860000000000
          Top = 14.740157480000000000
          Width = 238.110199610000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CAENDERECO]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 147.401645590000000000
          Top = 27.968503930000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 177.637853860000000000
          Top = 27.968503930000000000
          Width = 193.133983000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."BAIRRO"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 370.393810630000000000
          Top = 27.968503930000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 400.630031100000000000
          Top = 27.968503930000000000
          Width = 154.960656770000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."NO_CIDADE"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 555.590682990000000000
          Top = 27.968503930000000000
          Width = 15.118107800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'UF:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 570.708790790000000000
          Top = 27.968503930000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."NOMEUF"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 147.401645590000000000
          Top = 41.196850390000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CALACNPJCFP]:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 177.637853860000000000
          Top = 41.196850390000000000
          Width = 109.606289450000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 287.244143310000000000
          Top = 41.196850390000000000
          Width = 34.015740710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CALAIERG]:')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 321.259884020000000000
          Top = 41.196850390000000000
          Width = 90.708639450000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CAIERG]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 411.968745590000000000
          Top = 41.196850390000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 449.764013860000000000
          Top = 41.196850390000000000
          Width = 94.488169450000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."TEL_TXT"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 544.252183310000000000
          Top = 41.196850390000000000
          Width = 22.677150710000000000
          Height = 13.228346460000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 566.929334020000000000
          Top = 41.196850390000000000
          Width = 117.165349450000000000
          Height = 13.228346460000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."FAX_TXT"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 147.401408820000000000
          Top = 71.433102590000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 177.637617090000000000
          Top = 71.433102590000000000
          Width = 226.771604720000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CONTRATANTE"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 404.409448820000000000
          Top = 71.433090390000000000
          Width = 41.574798270000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 445.984247090000000000
          Top = 71.433117240000000000
          Width = 238.110236220000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CEENDERECO]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 147.401408820000000000
          Top = 84.661417320000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 177.637617090000000000
          Top = 84.661417320000000000
          Width = 193.133983000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."BAIRRO"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 370.393722760000000000
          Top = 84.661417320000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 400.629943230000000000
          Top = 84.661417320000000000
          Width = 154.960656770000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."NO_CIDADE"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 555.590661020000000000
          Top = 84.661417320000000000
          Width = 15.118107800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'UF:')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 570.708768820000000000
          Top = 84.661417320000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."NOMEUF"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 158.740203860000000000
          Top = 59.314997240000000000
          Width = 71.811070000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'Contratante:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 589.606680000000000000
          Top = 27.968503930000000000
          Width = 22.677167800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 612.283847800000000000
          Top = 27.968503930000000000
          Width = 71.811057800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CEP_TXT"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 589.606680000000000000
          Top = 84.661417320000000000
          Width = 22.677167800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 612.283847800000000000
          Top = 84.661417320000000000
          Width = 71.811057800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CEP_TXT"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 147.401645590000000000
          Top = 14.740157480000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 177.637853860000000000
          Top = 14.740157480000000000
          Width = 226.771609610000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CONTRATADA"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 158.740203860000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338580240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'Contratada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture2: TfrxPictureView
          Tag = 1
          Left = 17.007832520000000000
          Top = 1.511801260000000000
          Width = 109.606299210000000000
          Height = 109.606287010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo54: TfrxMemoView
          Left = 147.401670000000000000
          Top = 98.267780000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CELACNPJCFP]:')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 177.637878270000000000
          Top = 98.267780000000000000
          Width = 83.149579450000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 260.787457720000000000
          Top = 98.267780000000000000
          Width = 34.015740710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CELAIERG]:')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 294.803198420000000000
          Top = 98.267780000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CEIERG]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 377.953000000000000000
          Top = 98.267780000000000000
          Width = 22.677155590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Tel.:')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 400.630148270000000000
          Top = 98.267780000000000000
          Width = 79.370078740000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."TEL_TXT"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 582.047507720000000000
          Top = 98.267780000000000000
          Width = 22.677150710000000000
          Height = 13.228346460000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 604.724658420000000000
          Top = 98.267780000000000000
          Width = 79.370078740000000000
          Height = 13.228346460000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."FAX_TXT"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Left = 480.000571180000000000
          Top = 98.267782450000000000
          Width = 22.677155590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cel.:')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Left = 502.677751180000000000
          Top = 98.267782450000000000
          Width = 79.370078740000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CEL_TXT"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo217: TfrxMemoView
          Left = 154.960730000000000000
          Width = 423.307360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE LOCA'#199#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo220: TfrxMemoView
          Left = 3.779530000000000000
          Width = 151.181136540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy  hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo221: TfrxMemoView
          Left = 597.165740000000000000
          Width = 113.385836540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
    end
  end
  object frxDsPsqIts: TfrxDBDataset
    UserName = 'frxDsPsqIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_STATUS=NO_STATUS'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'DtHrEmi=DtHrEmi'
      'DtHrLocado_TXT=DtHrLocado_TXT'
      'SaiDtHr=SaiDtHr'
      'DtHrBxa=DtHrBxa'
      'DtHrBxa_TXT=DtHrBxa_TXT'
      'DtHrRetorn_TXT=DtHrRetorn_TXT'
      'TipoAluguel=TipoAluguel'
      'Status=Status'
      'GraGruX=GraGruX'
      'NO_CLI=NO_CLI'
      'Patrimonio=Patrimonio'
      'Referencia=Referencia'
      'Codigo=Codigo'
      'NO_TipoAluguel=NO_TipoAluguel'
      'DtHrDevolver=DtHrDevolver')
    DataSet = QrPsqIts
    BCDToCurrency = False
    Left = 376
    Top = 380
  end
  object QrEndContratada: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEndContratadaCalcFields
    SQL.Strings = (
      
        'SELECT mun.Nome NO_CIDADE, pai.Nome NO_PAIS, ent.Codigo, ent.Tip' +
        'o,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATADA,'
      'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,'
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,'
      'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,'
      'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,'
      'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,'
      '/*IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,*/'
      'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,'
      'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,'
      'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX,'
      'IF(ent.Tipo=0, ent.ECep, ent.PCEP) + 0.000 CEP'
      'FROM entidades ent'
      'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF'
      'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF'
      'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd'
      'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd'
      ''
      
        'LEFT JOIN loctoolrall.dtb_munici mun ON mun.Codigo = IF(ent.Tipo' +
        '=0, ent.ECodMunici, ent.PCodMunici)'
      
        'LEFT JOIN loctoolrall.bacen_pais pai ON pai.Codigo = IF(ent.Tipo' +
        '=0, ent.ECodiPais, ent.PCodiPais)'
      ''
      'WHERE ent.Codigo=-11'
      'ORDER BY CONTRATADA')
    Left = 740
    Top = 360
    object QrEndContratadaTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrEndContratadaCONTRATADA: TWideStringField
      FieldName = 'CONTRATADA'
      Size = 100
    end
    object QrEndContratadaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEndContratadaNOMERUA: TWideStringField
      FieldName = 'NOMERUA'
      Size = 30
    end
    object QrEndContratadaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEndContratadaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEndContratadaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEndContratadaCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEndContratadaIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEndContratadaCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEndContratadaTEL: TWideStringField
      FieldName = 'TEL'
    end
    object QrEndContratadaFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEndContratadaCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEndContratadaTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrEndContratadaFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrEndContratadaCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Calculated = True
    end
    object QrEndContratadaCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEndContratadaNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrEndContratadaNO_CIDADE: TWideStringField
      FieldName = 'NO_CIDADE'
      Required = True
      Size = 100
    end
    object QrEndContratadaNO_PAIS: TWideStringField
      FieldName = 'NO_PAIS'
      Size = 100
    end
  end
  object frxDsEndContratada: TfrxDBDataset
    UserName = 'frxDsEndContratada'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Tipo=Tipo'
      'CONTRATADA=CONTRATADA'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMERUA=NOMERUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'NOMEUF=NOMEUF'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'Codigo=Codigo'
      'TEL=TEL'
      'FAX=FAX'
      'CEP=CEP'
      'TEL_TXT=TEL_TXT'
      'FAX_TXT=FAX_TXT'
      'CNPJ_TXT=CNPJ_TXT'
      'CEP_TXT=CEP_TXT'
      'NUMERO=NUMERO'
      'NO_CIDADE=NO_CIDADE'
      'NO_PAIS=NO_PAIS')
    OpenDataSource = False
    DataSet = QrEndContratada
    BCDToCurrency = False
    Left = 740
    Top = 408
  end
  object frxDsEndContratante: TfrxDBDataset
    UserName = 'frxDsEndContratante'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Tipo=Tipo'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMERUA=NOMERUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'NOMEUF=NOMEUF'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'TEL=TEL'
      'CEL=CEL'
      'FAX=FAX'
      'CEP=CEP'
      'TEL_TXT=TEL_TXT'
      'CONTRATANTE=CONTRATANTE'
      'FAX_TXT=FAX_TXT'
      'CNPJ_TXT=CNPJ_TXT'
      'CEP_TXT=CEP_TXT'
      'NUMERO=NUMERO'
      'NO_CIDADE=NO_CIDADE'
      'NO_PAIS=NO_PAIS'
      'CEL_TXT=CEL_TXT')
    OpenDataSource = False
    DataSet = QrEndContratante
    BCDToCurrency = False
    Left = 616
    Top = 408
  end
  object QrEndContratante: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEndContratanteCalcFields
    SQL.Strings = (
      
        'SELECT mun.Nome NO_CIDADE, pai.Nome NO_PAIS, ent.Codigo, ent.Tip' +
        'o,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATANTE,'
      'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,'
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,'
      'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,'
      'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,'
      'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,'
      '/*IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,*/'
      'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,'
      'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,'
      'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX,'
      'IF(ent.Tipo=0, ent.ECep, ent.PCEP) + 0.000 CEP'
      'FROM entidades ent'
      'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF'
      'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF'
      'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd'
      'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd'
      ''
      
        'LEFT JOIN loctoolrall.dtb_munici mun ON mun.Codigo = IF(ent.Tipo' +
        '=0, ent.ECodMunici, ent.PCodMunici)'
      
        'LEFT JOIN loctoolrall.bacen_pais pai ON pai.Codigo = IF(ent.Tipo' +
        '=0, ent.ECodiPais, ent.PCodiPais)'
      ''
      'WHERE ent.Codigo>0'
      'ORDER BY CONTRATANTE')
    Left = 616
    Top = 360
    object QrEndContratanteCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEndContratanteTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrEndContratanteNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEndContratanteNOMERUA: TWideStringField
      FieldName = 'NOMERUA'
      Size = 30
    end
    object QrEndContratanteCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEndContratanteBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEndContratanteNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEndContratanteCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEndContratanteIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEndContratanteTEL: TWideStringField
      FieldName = 'TEL'
    end
    object QrEndContratanteCEL: TWideStringField
      FieldName = 'CEL'
    end
    object QrEndContratanteFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEndContratanteCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEndContratanteTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrEndContratanteCONTRATANTE: TWideStringField
      FieldName = 'CONTRATANTE'
      Size = 100
    end
    object QrEndContratanteFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrEndContratanteCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Calculated = True
    end
    object QrEndContratanteCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEndContratanteNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrEndContratanteNO_CIDADE: TWideStringField
      FieldName = 'NO_CIDADE'
      Size = 100
    end
    object QrEndContratanteNO_PAIS: TWideStringField
      FieldName = 'NO_PAIS'
      Size = 100
    end
    object QrEndContratanteCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Calculated = True
    end
  end
  object QrPsqCab: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPsqItsAfterOpen
    BeforeClose = QrPsqItsBeforeClose
    SQL.Strings = (
      'SELECT cab.Codigo,   '
      'ELT(cab.Status + 1, "N'#195'O DEFINIDO", "ABERTO",  '
      '"LOCADO", "FINALIZADO", "CANCELADO", "SUSPENSO",  '
      '"N'#195'O IMPLEMENTADO") NO_STATUS,  '
      'cab.DtHrEmi, cab.DtHrBxa, cab.TipoAluguel, cab.Status,   '
      
        'IF(cab.DtHrBxa < "1900-01-010", "", DATE_FORMAT(cab.DtHrBxa, "%d' +
        '/%m/%Y %H:%i")) DtHrBxa_TXT,  '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI,  '
      'CASE cab.TipoAluguel '
      '  WHEN "D" THEN "DIARIA" '
      '  WHEN "S" THEN "SEMANAL" '
      '  WHEN "Q" THEN "QUINZENAL" '
      '  WHEN "M" THEN "MENSAL" '
      'ELSE "INDEFINIDO" END NO_TipoAluguel  '
      
        'FROM loccconcab cab LEFT JOIN entidades cli ON cli.Codigo=cab.Cl' +
        'iente  ')
    Left = 524
    Top = 280
    object QrPsqCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPsqCabNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 16
    end
    object QrPsqCabDtHrEmi: TDateTimeField
      FieldName = 'DtHrEmi'
      Required = True
    end
    object QrPsqCabDtHrBxa: TDateTimeField
      FieldName = 'DtHrBxa'
      Required = True
    end
    object QrPsqCabTipoAluguel: TWideStringField
      FieldName = 'TipoAluguel'
      Required = True
      Size = 1
    end
    object QrPsqCabStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrPsqCabDtHrBxa_TXT: TWideStringField
      FieldName = 'DtHrBxa_TXT'
      Size = 21
    end
    object QrPsqCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrPsqCabNO_TipoAluguel: TWideStringField
      FieldName = 'NO_TipoAluguel'
      Required = True
      Size = 10
    end
  end
  object DsPsqCab: TDataSource
    DataSet = QrPsqCab
    Left = 524
    Top = 328
  end
  object frxDsPsqCab: TfrxDBDataset
    UserName = 'frxDsPsqCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NO_STATUS=NO_STATUS'
      'DtHrEmi=DtHrEmi'
      'DtHrBxa=DtHrBxa'
      'TipoAluguel=TipoAluguel'
      'Status=Status'
      'DtHrBxa_TXT=DtHrBxa_TXT'
      'NO_CLI=NO_CLI'
      'NO_TipoAluguel=NO_TipoAluguel')
    DataSet = QrPsqCab
    BCDToCurrency = False
    Left = 524
    Top = 380
  end
  object frxLOC_PATRI_111_002: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41802.645199189800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if (<LogoNFeExiste> = True) then'
      '  begin              '
      
        '    Picture2.Visible := True;                                   ' +
        '           '
      '    Picture2.LoadFromFile(<LogoNFePath>);'
      '  end else'
      '  begin'
      
        '    Picture2.Visible := False;                                  ' +
        '            '
      '  end;'
      'end.')
    OnGetValue = frxLOC_PATRI_111_001GetValue
    Left = 524
    Top = 432
    Datasets = <
      item
        DataSet = frxDsEndContratada
        DataSetName = 'frxDsEndContratada'
      end
      item
        DataSet = frxDsEndContratante
        DataSetName = 'frxDsEndContratante'
      end
      item
        DataSet = frxDsPsqCab
        DataSetName = 'frxDsPsqCab'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 279.685220000000000000
        Width = 718.110700000000000000
        DataSet = frxDsPsqCab
        DataSetName = 'frxDsPsqCab'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo21: TfrxMemoView
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPsqCab."Codigo"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 56.692950000000000000
          Width = 370.393940000000000000
          Height = 15.118110240000000000
          DataField = 'NO_CLI'
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPsqCab."NO_CLI"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 427.086890000000000000
          Width = 86.929190000000000000
          Height = 15.118110240000000000
          DataField = 'NO_STATUS'
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqCab."NO_STATUS"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 514.016080000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'DtHrEmi'
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqCab."DtHrEmi"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 582.047620000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'DtHrBxa_TXT'
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqCab."DtHrBxa_TXT"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 650.079160000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'NO_TipoAluguel'
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPsqCab."NO_TipoAluguel"]')
          ParentFont = False
        end
      end
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        Height = 115.275590551181000000
        Top = 98.267780000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Shape2: TfrxShapeView
          Left = 147.401670000000000000
          Top = 10.960637240000000000
          Width = 540.472790000000000000
          Height = 45.354360000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Shape3: TfrxShapeView
          Left = 147.401670000000000000
          Top = 67.653587240000000000
          Width = 540.472790000000000000
          Height = 45.354360000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo4: TfrxMemoView
          Left = 404.409685590000000000
          Top = 14.740157480000000000
          Width = 41.574798270000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 445.984483860000000000
          Top = 14.740157480000000000
          Width = 238.110199610000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CAENDERECO]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 147.401645590000000000
          Top = 27.968503930000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 177.637853860000000000
          Top = 27.968503930000000000
          Width = 193.133983000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."BAIRRO"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 370.393810630000000000
          Top = 27.968503930000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 400.630031100000000000
          Top = 27.968503930000000000
          Width = 154.960656770000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."NO_CIDADE"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 555.590682990000000000
          Top = 27.968503930000000000
          Width = 15.118107800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'UF:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 570.708790790000000000
          Top = 27.968503930000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."NOMEUF"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 147.401645590000000000
          Top = 41.196850390000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CALACNPJCFP]:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 177.637853860000000000
          Top = 41.196850390000000000
          Width = 109.606289450000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 287.244143310000000000
          Top = 41.196850390000000000
          Width = 34.015740710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CALAIERG]:')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 321.259884020000000000
          Top = 41.196850390000000000
          Width = 90.708639450000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CAIERG]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 411.968745590000000000
          Top = 41.196850390000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 449.764013860000000000
          Top = 41.196850390000000000
          Width = 94.488169450000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."TEL_TXT"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 544.252183310000000000
          Top = 41.196850390000000000
          Width = 22.677150710000000000
          Height = 13.228346460000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 566.929334020000000000
          Top = 41.196850390000000000
          Width = 117.165349450000000000
          Height = 13.228346460000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."FAX_TXT"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 147.401408820000000000
          Top = 71.433102590000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 177.637617090000000000
          Top = 71.433102590000000000
          Width = 226.771604720000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CONTRATANTE"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 404.409448820000000000
          Top = 71.433090390000000000
          Width = 41.574798270000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 445.984247090000000000
          Top = 71.433117240000000000
          Width = 238.110236220000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CEENDERECO]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 147.401408820000000000
          Top = 84.661417320000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 177.637617090000000000
          Top = 84.661417320000000000
          Width = 193.133983000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."BAIRRO"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 370.393722760000000000
          Top = 84.661417320000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 400.629943230000000000
          Top = 84.661417320000000000
          Width = 154.960656770000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."NO_CIDADE"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 555.590661020000000000
          Top = 84.661417320000000000
          Width = 15.118107800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'UF:')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 570.708768820000000000
          Top = 84.661417320000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."NOMEUF"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 158.740203860000000000
          Top = 59.314997240000000000
          Width = 71.811070000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'Contratante:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 589.606680000000000000
          Top = 27.968503930000000000
          Width = 22.677167800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 612.283847800000000000
          Top = 27.968503930000000000
          Width = 71.811057800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CEP_TXT"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 589.606680000000000000
          Top = 84.661417320000000000
          Width = 22.677167800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 612.283847800000000000
          Top = 84.661417320000000000
          Width = 71.811057800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CEP_TXT"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 147.401645590000000000
          Top = 14.740157480000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 177.637853860000000000
          Top = 14.740157480000000000
          Width = 226.771609610000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CONTRATADA"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 158.740203860000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338580240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'Contratada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture2: TfrxPictureView
          Tag = 1
          Left = 17.007832520000000000
          Top = 1.511801260000000000
          Width = 109.606299210000000000
          Height = 109.606287010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo54: TfrxMemoView
          Left = 147.401670000000000000
          Top = 98.267780000000000000
          Width = 30.236215590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CELACNPJCFP]:')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 177.637878270000000000
          Top = 98.267780000000000000
          Width = 83.149579450000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 260.787457720000000000
          Top = 98.267780000000000000
          Width = 34.015740710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CELAIERG]:')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 294.803198420000000000
          Top = 98.267780000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CEIERG]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 377.953000000000000000
          Top = 98.267780000000000000
          Width = 22.677155590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Tel.:')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 400.630148270000000000
          Top = 98.267780000000000000
          Width = 79.370078740000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."TEL_TXT"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 582.047507720000000000
          Top = 98.267780000000000000
          Width = 22.677150710000000000
          Height = 13.228346460000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 604.724658420000000000
          Top = 98.267780000000000000
          Width = 79.370078740000000000
          Height = 13.228346460000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."FAX_TXT"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Left = 480.000571180000000000
          Top = 98.267782450000000000
          Width = 22.677155590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cel.:')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Left = 502.677751180000000000
          Top = 98.267782450000000000
          Width = 79.370078740000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CEL_TXT"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo217: TfrxMemoView
          Left = 154.960730000000000000
          Width = 423.307360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE LOCA'#199#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo220: TfrxMemoView
          Left = 3.779530000000000000
          Width = 151.181136540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy  hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo221: TfrxMemoView
          Left = 597.165740000000000000
          Width = 113.385836540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 238.110390000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Top = 3.779530000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Loca'#231#227'o')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 56.692950000000000000
          Top = 3.779530000000000000
          Width = 370.393940000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 427.086890000000000000
          Top = 3.779530000000000000
          Width = 86.929190000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 514.016080000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 582.047620000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Encerramento')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 650.079160000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsPsqCab
          DataSetName = 'frxDsPsqCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo de Aluguel')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 317.480520000000000000
        Width = 718.110700000000000000
      end
    end
  end
end
