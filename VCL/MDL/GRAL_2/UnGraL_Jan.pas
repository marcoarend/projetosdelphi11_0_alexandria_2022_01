unit UnGraL_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  UnInternalConsts, Vcl.Controls, dmkPageControl, UnAppEnums, dmkGeral,
  UnProjGroup_Vars, AppListas, CfgCadLista, Variants;

type
  TUnGraL_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormFeriados();
    procedure MostraFormGraGXOutr(Codigo: Integer);
    procedure MostraFormGraGXPatr(Codigo: Integer);
    procedure MostraFormGraGXServ(Codigo: Integer);
    procedure MostraFormGraGXVend(Codigo: Integer);
    procedure MostraFormLocados();
    procedure MostraFormLocCCon(AbrirEmAba: Boolean; InOwner: TWincontrol;
              AdvToolBarPager: TdmkPageControl; Codigo: Integer);
    procedure MostraFormLocCMovAll(const GtrTab: TGraToolRent; const Codigo,
              ManejoLca, CtrID, Item, GraGruX, Empresa: Integer; const
              NO_ManejoLca, NO_GraGruX: String; const TipoTool: TGraGXToolRnt;
              const GraGruY: Integer; const TipoAluguel: String; const
              QrLocIPatPri, QrLocIPatSec, QrLocIPatAce, QrLocIPatApo,
              QrLocIPatCns, QrLocIPatUso: TmySQLQuery; const DtHrDevolver:
              TDateTime; var Recalcular: Boolean);
    procedure MostraFormLocCItsRet(const SQLType: TSQLType; const Codigo, CtrID,
              Item, ControleAlt, Empresa: Integer; const TipoMotiv:
              TTipoMotivLocMov; DtHrDevolver: TDateTime; var NewControle:
              Integer; var Recalcular: Boolean);
    procedure MostraFormLocCItsSubs(const SQLType: TSQLType; const Codigo, CtrID,
              Item, ControleAlt, Empresa: Integer; const TipoMotiv: TTipoMotivLocMov;
              const TipoTool: TGraGXToolRnt; const GraGruY: Integer; const
              TipoAluguel: String; const QrLocIPatPri, QrLocIPatSec, QrLocIPatAce,
              QrLocIPatApo, QrLocIPatCns, QrLocIPatUso: TmySQLQuery;
              const DtHrDevolver: TDateTime;
              var NewControle: Integer; var Recalcular: Boolean);
    procedure MostraFormLocCConPsq(const Cliente: Integer; var Codigo, GraGruX:
              Integer);
    procedure MostraFormLocCItsLcLocados(Empresa, GraGruX, Nivel2: Integer);
    procedure MostraFormLocCMovGer(Empresa, LocCConCab, Cliente, GraGruX: Integer);
    procedure MostraFormGraGruEPatNeg();
    procedure MostraFormStqAlocGer(GraGruX, Limite: Integer);
    procedure MostraFormAtndCanal();
    procedure MostraFormLocImpVend();
    procedure MostraFormMarcasSimples();
  end;

var
  GraL_Jan: TUnGraL_Jan;

implementation

uses
  MyDBCheck, LogOff, VerifiDB, Module, UnMyObjects,
  Feriados, LocCCon, LocCConCab, GraGXPatr, GraGXOutr, GraGXServ, GraGXVend,
  LocCItsRet, LocCMovAll, LocCConPsq, LocCItsLcLocados, GraGruEPatNeg,
  LocCMovGer, Locados, LocCItsSubs, UnAppPF, DmkDAC_PF, StqAlocGer, LocImprVend,
  GraFabCad;

{ TUnGraL_Jan }

procedure TUnGraL_Jan.MostraFormAtndCanal;
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'AtndCanal', 60, UnDmkEnums.ncGerlSeq1,
    'Canais de Atendimento', [], False, Null, [], [], False);
end;

procedure TUnGraL_Jan.MostraFormFeriados();
begin
  if DBCheck.CriaFm(TFmFeriados, FmFeriados, afmoNegarComAviso) then
  begin
    FmFeriados.ShowModal;
    FmFeriados.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormGraGruEPatNeg;
begin
  if DBCheck.CriaFm(TFmGraGruEPatNeg, FmGraGruEPatNeg, afmoNegarComAviso) then
  begin
    FmGraGruEPatNeg.DsGraGruEPatNeg.DataSet := Dmod.QrGGXEPatNeg;
    FmGraGruEPatNeg.ShowModal;
    FmGraGruEPatNeg.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormGraGXOutr(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraGXOutr, FmGraGXOutr, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraGXOutr.LocCod(Codigo, Codigo);
    FmGraGXOutr.ShowModal;
    FmGraGXOutr.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormGraGXPatr(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraGXPatr, FmGraGXPatr, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraGXPatr.LocCod(Codigo, Codigo);
    FmGraGXPatr.ShowModal;
    FmGraGXPatr.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormGraGXServ(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraGXServ, FmGraGXServ, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraGXServ.LocCod(Codigo, Codigo);
    FmGraGXServ.ShowModal;
    FmGraGXServ.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormGraGXVend(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraGXVend, FmGraGXVend, afmoNegarComAviso) then
  begin
    if Codigo = -1 then
      FmGraGXVend.PesquisaNomeMateriais()
    else if Codigo <> 0 then
      FmGraGXVend.LocCod(Codigo, Codigo);
    FmGraGXVend.ShowModal;
    FmGraGXVend.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormLocados();
begin
  if DBCheck.CriaFm(TFmLocados, FmLocados, afmoNegarComAviso) then
  begin
    FmLocados.ReopenLocados(0);
    FmLocados.ShowModal;
    FmLocados.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormLocCCon(AbrirEmAba: Boolean; InOwner: TWincontrol;
  AdvToolBarPager: TdmkPageControl; Codigo: Integer);
begin
  case Dmod.QrOpcoesTRenFormaCobrLoca.Value of
    0,1: // Modo simplificado - AAPA
    begin
      if AbrirEmAba then
      begin
        if FmLocCCon = nil then
          MyObjects.FormTDICria(TFmLocCCon, InOwner, AdvToolBarPager, True, True);
      end else
      begin
        if DBCheck.CriaFm(TFmLocCCon, FmLocCCon, afmoNegarComAviso) then
        begin
          FmLocCCon.ShowModal;
          FmLocCCon.Destroy;
          //
          FmLocCCon := nil;
        end;
      end;
    end;
    2: // Modo completo - T i s o l i n
    begin
      if AbrirEmAba then
      begin
        if FmLocCConCab = nil then
        begin
          VAR_FmLocCConCab := MyObjects.FormTDICria(TFmLocCConCab, InOwner, AdvToolBarPager, True, True);
          if Codigo <> 0 then
          begin
            if TFmLocCConCab(VAR_FmLocCConCab).ImgTipo.SQLType = stLok then
              TFmLocCConCab(VAR_FmLocCConCab).LocCod(Codigo, Codigo)
            else
              Geral.MB_Aviso('O contrato n�o ser� localizado pois h� um contrato em edi��o neste momento!');
          end;
        end;
      end else
      begin
        if DBCheck.CriaFm(TFmLocCConCab, FmLocCConCab, afmoNegarComAviso) then
        begin
          FmLocCConCab.ShowModal;
          FmLocCConCab.Destroy;
          //
          FmLocCConCab := nil;
        end;
      end;
    end;
  end;
end;

procedure TUnGraL_Jan.MostraFormLocCConPsq(const Cliente: Integer; var Codigo,
  GraGruX: Integer);
begin
  Application.CreateForm(TFmLocCConPsq, FmLocCConPsq);
  if Cliente <> 0 then
  begin
    FmLocCConPsq.EdCliente.ValueVariant := Cliente;
    FmLocCConPsq.CBCliente.KeyValue     := Cliente;
  end;
  FmLocCConPsq.ShowModal;
  Codigo  := FmLocCConPsq.FCodigo;
  GraGruX := FmLocCConPsq.FGraGruX;
  FmLocCConPsq.Destroy;
end;

procedure TUnGraL_Jan.MostraFormLocCItsLcLocados(Empresa, GraGruX, Nivel2: Integer);
begin
  if GraGruX = 0 then
    Geral.MB_Aviso('Selecione o equipamento!')
  else
  begin
    Application.CreateForm(TFmLocCItsLcLocados, FmLocCItsLcLocados);
    FmLocCItsLcLocados.FEmpresa := Empresa;
    FmLocCItsLcLocados.FGraGruX := GraGruX;
    FmLocCItsLcLocados.FNivel2 := Nivel2;
    FmLocCItsLcLocados.ReopenLocados();
    FmLocCItsLcLocados.ShowModal;
    FmLocCItsLcLocados.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormLocCItsRet(const SQLType: TSQLType; const Codigo,
  CtrID, Item, ControleAlt, Empresa: Integer; const TipoMotiv: TTipoMotivLocMov;
  DtHrDevolver: TDateTime; var NewControle: Integer; var Recalcular: Boolean);
begin
  if DBCheck.CriaFm(TFmLocCItsRet, FmLocCItsRet, afmoNegarComAviso) then
  begin
    FmLocCItsRet.ImgTipo.SQLType         := SQLType;
    FmLocCItsRet.FCodigo                 := Codigo;
    FmLocCItsRet.FCtrID                  := CtrID;
    FmLocCItsRet.FItem                   := Item;
    FmLocCItsRet.FControleAlt            := ControleAlt;
    FmLocCItsRet.EdControle.ValueVariant := ControleAlt;
    FmLocCItsRet.FTipoMotiv              := TipoMotiv;
    FmLocCItsRet.FEmpresa                := Empresa;
    FmLocCItsRet.FDtHrDevolver           := DtHrDevolver;
    FmLocCItsRet.EdDtHrDevolver_TXT.ValueVariant := Geral.FDT(DtHrDevolver, 107);
    //
    //
    if SQLType = stIns then
    begin
      //FmLocCItsRet.ReopenAll(Codigo, CtrID);
      FmLocCItsRet.ReopenItsLca();
    end else
    //if SQLType = stUpd then
    begin
      //FmLocCItsRet.ReopenAll(Codigo, CtrID);
      FmLocCItsRet.ReopenItsLca();
      FmLocCItsRet.ReopenLocCMovAll();
    end;
    //
    FmLocCItsRet.ShowModal;
    Recalcular   := FmLocCItsRet.FRecalcular;
    NewControle  := FmLocCItsRet.FNewControle;
    FmLocCItsRet.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormLocCItsSubs(const SQLType: TSQLType;
  const Codigo, CtrID, Item, ControleAlt, Empresa: Integer;
  const TipoMotiv: TTipoMotivLocMov;

  const TipoTool: TGraGXToolRnt; const GraGruY: Integer; const TipoAluguel: String;
  const QrLocIPatPri, QrLocIPatSec, QrLocIPatAce,
  QrLocIPatApo, QrLocIPatCns, QrLocIPatUso: TmySQLQuery;

  const DtHrDevolver: TDateTime;

  var NewControle: Integer;
  var Recalcular: Boolean);
begin
  if TipoTool = TGraGXToolRnt.ggxoIndef then Exit;
  //
  if DBCheck.CriaFm(TFmLocCItsSubs, FmLocCItsSubs, afmoNegarComAviso) then
  begin
    FmLocCItsSubs.ImgTipo.SQLType         := SQLType;
    FmLocCItsSubs.FCodigo                 := Codigo;
    FmLocCItsSubs.FCtrID                  := CtrID;
    FmLocCItsSubs.FItem                   := Item;
    FmLocCItsSubs.FControleAlt            := ControleAlt;
    FmLocCItsSubs.EdControle.ValueVariant := ControleAlt;
    FmLocCItsSubs.FTipoMotiv              := TipoMotiv;
    FmLocCItsSubs.FEmpresa                := Empresa;
    //
    FmLocCItsSubs.FTipoTool               := TipoTool;
    //
    FmLocCItsSubs.FDtHrDevolver           := DtHrDevolver;
(*
   case TipoTool of
      TGraGXToolRnt.ggxo1gAcessorio:  FmLocCItsSubs.FCaption := 'Acess�rio';
      TGraGXToolRnt.ggxo1gUso:        FmLocCItsSubs.FCaption := 'Material de Uso';
      TGraGXToolRnt.ggxo1gConsumo:    FmLocCItsSubs.FCaption := 'Material de Consumo';
      TGraGXToolRnt.ggxo2gSecundario: FmLocCItsSubs.FCaption := 'Patrim�nio Secund�rio';
      TGraGXToolRnt.ggxo2gAcessorio:  FmLocCItsSubs.FCaption := 'Acess�rio';
      TGraGXToolRnt.ggxo2gApoio:      FmLocCItsSubs.FCaption := 'Material de Apoio';
      TGraGXToolRnt.ggxo2gUso:        FmLocCItsSubs.FCaption := 'Material de Uso';
      TGraGXToolRnt.ggxo2gConsumo:    FmLocCItsSubs.FCaption := 'Material de Consumo';
      else FmLocCItsSubs.FCaption := '?????????????????????';
    end;
*)
    case TipoTool of
      TGraGXToolRnt.ggxo1gAcessorio:  FmLocCItsSubs.CBGraGruX.LocF7TableName := ' ??? ';
      TGraGXToolRnt.ggxo1gUso:        FmLocCItsSubs.CBGraGruX.LocF7TableName := ' ??? ';
      TGraGXToolRnt.ggxo1gConsumo:    FmLocCItsSubs.CBGraGruX.LocF7TableName := ' ??? ';
      TGraGXToolRnt.ggxo2gPrincipal:  FmLocCItsSubs.CBGraGruX.LocF7TableName := 'gragxpatr';
      TGraGXToolRnt.ggxo2gSecundario: FmLocCItsSubs.CBGraGruX.LocF7TableName := 'gragxpatr';
      TGraGXToolRnt.ggxo2gAcessorio:  FmLocCItsSubs.CBGraGruX.LocF7TableName := 'gragxpatr';
      TGraGXToolRnt.ggxo2gApoio:      FmLocCItsSubs.CBGraGruX.LocF7TableName := 'gragxoutr';
      TGraGXToolRnt.ggxo2gUso:        FmLocCItsSubs.CBGraGruX.LocF7TableName := 'gragxoutr';
      TGraGXToolRnt.ggxo2gConsumo:    FmLocCItsSubs.CBGraGruX.LocF7TableName := 'gragxoutr';
      else FmLocCItsSubs.CBGraGruX.LocF7TableName := ' ????? ';
    end;
    case TipoTool of
      TGraGXToolRnt.ggxo1gAcessorio:  FmLocCItsSubs.CBGraGruX.LocF7NameFldName := ' ??? ';
      TGraGXToolRnt.ggxo1gUso:        FmLocCItsSubs.CBGraGruX.LocF7NameFldName := ' ??? ';
      TGraGXToolRnt.ggxo1gConsumo:    FmLocCItsSubs.CBGraGruX.LocF7NameFldName := ' ??? ';
      TGraGXToolRnt.ggxo2gPrincipal:  FmLocCItsSubs.CBGraGruX.LocF7NameFldName := 'GraGruX';
      TGraGXToolRnt.ggxo2gSecundario: FmLocCItsSubs.CBGraGruX.LocF7NameFldName := 'GraGruX';
      TGraGXToolRnt.ggxo2gAcessorio:  FmLocCItsSubs.CBGraGruX.LocF7NameFldName := 'GraGruX';
      TGraGXToolRnt.ggxo2gApoio:      FmLocCItsSubs.CBGraGruX.LocF7NameFldName := 'GraGruX';
      TGraGXToolRnt.ggxo2gUso:        FmLocCItsSubs.CBGraGruX.LocF7NameFldName := 'GraGruX';
      TGraGXToolRnt.ggxo2gConsumo:    FmLocCItsSubs.CBGraGruX.LocF7NameFldName := 'GraGruX';
      else FmLocCItsSubs.CBGraGruX.LocF7NameFldName := ' ????? ';
    end;
    //
    FmLocCItsSubs.CBGraGruX.LocF7SQLText.Text :=  'AND its.Aplicacao=' +
      Geral.FF0(AppPF.ObtemTipoDeTGraGXOutr(TipoTool));
    //
    FmLocCItsSubs.FTipoAluguel  := TipoAluguel;
    FmLocCItsSubs.FGraGruY      := GraGruY;
    FmLocCItsSubs.FCodigo       := Codigo;
    FmLocCItsSubs.FCtrID        := CtrID;
    FmLocCItsSubs.FQrLocIPatSec := QrLocIPatSec;
    FmLocCItsSubs.FQrLocIPatApo := QrLocIPatApo;
    FmLocCItsSubs.FQrLocIPatCns := QrLocIPatCns;
    FmLocCItsSubs.FQrLocIPatUso := QrLocIPatUso;
    FmLocCItsSubs.FQrLocIPatAce := QrLocIPatAce;
    //
    FmLocCItsSubs.LaQtdeLocacao.Enabled := GraGruY <= CO_GraGruY_3072_GXPatAce;
    FmLocCItsSubs.EdQtdeLocacao.Enabled := GraGruY <= CO_GraGruY_3072_GXPatAce;
    //
    if SQLType = stIns then
    begin
      //FmLocCItsSubs.ReopenAll(Codigo, CtrID);
      FmLocCItsSubs.ReopenItsLca();
    end else
    //if SQLType = stUpd then
    begin
      //FmLocCItsSubs.ReopenAll(Codigo, CtrID);
      FmLocCItsSubs.ReopenItsLca();
      FmLocCItsSubs.ReopenLocCMovAll();
    end;
    //
    FmLocCItsSubs.ShowModal;
    Recalcular   := FmLocCItsSubs.FRecalcular;
    NewControle  := FmLocCItsSubs.FNewControle;
    FmLocCItsSubs.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormLocCMovAll(const GtrTab: TGraToolRent; const
  Codigo, ManejoLca, CtrID, Item, GraGruX, Empresa: Integer; const NO_ManejoLca,
  NO_GraGruX: String; const TipoTool: TGraGXToolRnt; const GraGruY: Integer;
  const TipoAluguel: String; const QrLocIPatPri, QrLocIPatSec, QrLocIPatAce,
  QrLocIPatApo, QrLocIPatCns, QrLocIPatUso: TmySQLQuery; const DtHrDevolver:
  TDateTime; var Recalcular: Boolean);
begin
  if DBCheck.CriaFm(TFmLocCMovAll, FmLocCMovAll, afmoNegarComAviso) then
  begin
    FmLocCMovAll.ImgTipo.SQLType         := stPsq;
    FmLocCMovAll.FGtrTab                 := GtrTab;
    FmLocCMovAll.FCodigo                 := Codigo;
    FmLocCMovAll.FCtrID                  := CtrID;
    FmLocCMovAll.FItem                   := Item;
    FmLocCMovAll.FManejoLca              := ManejoLca;
    FmLocCMovAll.EdNO_ManejoLca.Text     := NO_ManejoLca;
    FmLocCMovAll.EdEmpresa.ValueVariant  := Empresa;
    FmLocCMovAll.EdGraGruX.ValueVariant  := GraGruX;
    FmLocCMovAll.EdNO_GraGruX.Text       := NO_GraGruX;
    //
    FmLocCMovAll.FTipoTool               := TipoTool;
    FmLocCMovAll.FGraGruY                := GraGruY;
    FmLocCMovAll.FTipoAluguel            := TipoAluguel;
    FmLocCMovAll.FQrLocIPatPri           := QrLocIPatPri;
    FmLocCMovAll.FQrLocIPatSec           := QrLocIPatSec;
    FmLocCMovAll.FQrLocIPatAce           := QrLocIPatAce;
    FmLocCMovAll.FQrLocIPatApo           := QrLocIPatApo;
    FmLocCMovAll.FQrLocIPatCns           := QrLocIPatCns;
    FmLocCMovAll.FQrLocIPatUso           := QrLocIPatUso;
    //
    FmLocCMovAll.FDtHrDevolver           := DtHrDevolver;
    //
    FmLocCMovAll.ReopenLocCMovAll();
    //
    FmLocCMovAll.ShowModal;
    Recalcular := FmLocCMovAll.FRecalcular;
    FmLocCMovAll.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormLocCMovGer(Empresa, LocCConCab, Cliente,
  GraGruX: Integer);
var
  Recalcular: Boolean;
begin
  if DBCheck.CriaFm(TFmLocCMovGer, FmLocCMovGer, afmoNegarComAviso) then
  begin
    FmLocCMovGer.ImgTipo.SQLType         := stPsq;
(*
    FmLocCMovGer.FGtrTab                 := GtrTab;
    FmLocCMovGer.FCodigo                 := Codigo;
    FmLocCMovGer.FCtrID                  := CtrID;
    FmLocCMovGer.FItem                   := Item;
    FmLocCMovGer.FManejoLca              := ManejoLca;
    FmLocCMovGer.EdNO_ManejoLca.Text     := NO_ManejoLca;
*)
    FmLocCMovGer.EdEmpresa.ValueVariant     := Empresa;
    FmLocCMovGer.EdLocCConCab.ValueVariant  := LocCConCab;
    FmLocCMovGer.EdGraGruX.ValueVariant     := GraGruX;
    FmLocCMovGer.CBGraGruX.KeyValue         := GraGruX;
    FmLocCMovGer.EdCliente.ValueVariant     := Cliente;
    FmLocCMovGer.CBCliente.KeyValue         := Cliente;
    //
    FmLocCMovGer.ReopenGraGxPatr(Empresa);
    //
    if LocCConCab <> 0 then
    begin
      FmLocCMovGer.CkSaiDtHrIni.Checked := False;
      FmLocCMovGer.CkSaiDtHrFim.Checked := False;
      FmLocCMovGer.ReopenLocCItsLca();
    end;
    //
    FmLocCMovGer.ShowModal;
    Recalcular := FmLocCMovGer.FRecalcular;
    FmLocCMovGer.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormLocImpVend();
begin
  if DBCheck.CriaFm(TFmLocImprVend, FmLocImprVend, afmoNegarComAviso) then
  begin
    FmLocImprVend.ShowModal;
    FmLocImprVend.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormMarcasSimples();
begin
  if DBCheck.CriaFm(TFmGraFabCad, FmGraFabCad, afmoNegarComAviso) then
  begin
    FmGraFabCad.ShowModal;
    FmGraFabCad.Destroy;
  end;
end;

procedure TUnGraL_Jan.MostraFormStqAlocGer(GraGruX, Limite: Integer);
begin
  if DBCheck.CriaFm(TFmStqAlocGer, FmStqAlocGer, afmoNegarComAviso) then
  begin
    //if GraGruX = -1 then
      FmStqAlocGer.Pesquisa();
    FmStqAlocGer.ShowModal;
    FmStqAlocGer.Destroy;
  end;
end;

end.
