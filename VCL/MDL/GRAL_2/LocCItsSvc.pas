unit LocCItsSvc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkGeral, DmkDAC_PF, dmkImage, UnDmkEnums, AppListas,
  Vcl.ComCtrls, dmkEditDateTimePicker, UnAppENums, dmkRadioGroup;

type
  TFmLocCItsSvc = class(TForm)
    PainelDados: TPanel;
    DsGraGXServ: TDataSource;
    QrGraGXServ: TMySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    SBCadastro: TSpeedButton;
    CBGraGruX: TdmkDBLookupComboBox;
    Label7: TLabel;
    Label2: TLabel;
    EdReferencia: TdmkEdit;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    QrPesq2: TmySQLQuery;
    QrPesq2Referencia: TWideStringField;
    QrPesq1: TmySQLQuery;
    QrPesq1Controle: TIntegerField;
    QrGraGXServControle: TIntegerField;
    QrGraGXServReferencia: TWideStringField;
    QrGraGXServNO_GG1: TWideStringField;
    QrGraGXServGraGruX: TIntegerField;
    QrGraGXServItemValr: TFloatField;
    QrGraGXServItemUnid: TIntegerField;
    CkContinuar: TCheckBox;
    Label21: TLabel;
    TPDataLoc: TdmkEditDateTimePicker;
    EdHoraLoc: TdmkEdit;
    EdQuantidade: TdmkEdit;
    LaQtdeLocacao: TLabel;
    Label3: TLabel;
    EdPrcUni: TdmkEdit;
    EdValorTotal: TdmkEdit;
    Label4: TLabel;
    RGAtrelamento: TdmkRadioGroup;
    QrLCI: TMySQLQuery;
    DsLCI: TDataSource;
    QrLCICtrID: TIntegerField;
    EdAtrelCtrID: TdmkEditCB;
    CBAtrelCtrID: TdmkDBLookupComboBox;
    Label5: TLabel;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    //procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure SBCadastroClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdReferenciaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdQuantidadeRedefinido(Sender: TObject);
    procedure EdPrcUniRedefinido(Sender: TObject);
    procedure RGAtrelamentoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGXServ();
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorReferencia(Limpa: Boolean);
    procedure AtualizaValorTotal();
    procedure ReopenLCI();

  public
    { Public declarations }
    FQrLocCItsSvc: TmySQLQuery;
    //FTipo, FGraGruY,
    FCodigo, FCtrID, FItem, FNewCtrID, FNewItem: Integer;
    FDtHrLocado: String;
    FAlterou: Boolean;
  end;

var
  FmLocCItsSvc: TFmLocCItsSvc;

implementation

uses UnMyObjects, Module, Principal, UnGraL_Jan, ModProd, UnAppPF;

{$R *.DFM}

var
  FCategoria: String;
  FQtdeProduto: Integer;

procedure TFmLocCItsSvc.AtualizaValorTotal();
var
  Quantidade, PrcUni, ValorTotal: Double;
begin
  Quantidade := EdQuantidade.ValueVariant;
  PrcUni  := EdPrcUni.ValueVariant;
  //
  ValorTotal := Quantidade * PrcUni;
  //
  EdValorTotal.ValueVariant := ValorTotal;
end;

procedure TFmLocCItsSvc.BtOKClick(Sender: TObject);
const
  Categoria = 'S';
var
  Codigo, CtrID, Item, ManejoLca, GraGruX, Atrelamento, AtrelCtrID: Integer;
  Quantidade, PrcUni, ValorTotal, ValorBruto: Double;
  SQLType: TSQLType;
  Erro: Boolean;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  CtrID          := FCtrID;
  Item           := FItem;
  ManejoLca      := Integer(TManejoLca.manelcaServico);
  GraGruX        := EdGraGruX.ValueVariant;
  Quantidade     := EdQuantidade.ValueVariant;
  PrcUni         := EdPrcUni.ValueVariant;
  ValorTotal     := EdValorTotal.ValueVariant;
  ValorBruto     := ValorTotal;
  Atrelamento    := RGAtrelamento.ItemIndex;
  AtrelCtrID     := EdAtrelCtrID.ValueVariant;
  //Categoria      := 'S';  acima!
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o servi�o!') then Exit;
  Erro := (Atrelamento > 1) and (AtrelCtrID = 0);
  if MyObjects.FIC(Erro, EdAtrelCtrID, 'Defina o movimento!') then Exit;
  //
  Item := UMyMod.BPGS1I32('loccitssvc', 'Item', '', '', tsPos, SQLType, Item);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitssvc', False, [
  'Codigo', 'CtrID', 'ManejoLca',
  'GraGruX', 'Quantidade', 'PrcUni',
  'ValorTotal', 'ValorBruto', 'Categoria',
  'Atrelamento', 'AtrelCtrID'], [
  'Item'], [
  Codigo, CtrID, ManejoLca,
  GraGruX, Quantidade, PrcUni,
  ValorTotal, ValorBruto, Categoria,
  Atrelamento, AtrelCtrID], [
  Item], True) then
  begin
    FAlterou  := True;
    FNewCtrID := CtrID;
    FNewItem  := Item;
    AppPF.AtualizaTotaisLocCConCab_Servicos(Codigo);
    if CkContinuar.Checked then
    begin
      if (FQrLocCItsSvc <> nil) and (FQrLocCItsSvc.State <> dsInactive) then
      begin
        FQrLocCItsSvc.Close;
        FQrLocCItsSvc.Open;
        FQrLocCItsSvc.Locate('Item', Item, []);
      end;
      //
      EdGraGruX.ValueVariant    := 0;
      CBGraGruX.KeyValue        := 0;
      EdQuantidade.ValueVariant := 0;
      EdPrcUni.ValueVariant     := 0;
      RGAtrelamento.ItemIndex   := 0;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
    //FmLocCConCab.LocCod(Codigo, Codigo);
  end;
end;

procedure TFmLocCItsSvc.BtSaidaClick(Sender: TObject);
begin
  VAR_SELCOD := 0;
  Close;
end;

procedure TFmLocCItsSvc.EdGraGruXChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    PesquisaPorGraGruX();
end;

procedure TFmLocCItsSvc.EdGraGruXRedefinido(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdPrcUni.ValueVariant := QrGraGXServItemValr.Value;
  end;
end;

procedure TFmLocCItsSvc.EdPrcUniRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmLocCItsSvc.EdQuantidadeRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmLocCItsSvc.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
end;

procedure TFmLocCItsSvc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCItsSvc.FormShow(Sender: TObject);
begin
  ReopenGraGXServ;
end;

procedure TFmLocCItsSvc.PesquisaPorGraGruX;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxserv ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  ggo.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (ggo.GraGruX IS NULL) ',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
    begin
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
    end;
  end else
    EdReferencia.ValueVariant := '';
end;

procedure TFmLocCItsSvc.PesquisaPorReferencia(Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxserv ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
  'AND NOT (ggo.GraGruX IS NULL)',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
    end;
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
    EdReferencia.ValueVariant := '';
end;

procedure TFmLocCItsSvc.ReopenGraGXServ;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXServ, Dmod.MyDB, [
  'SELECT ggx.Controle, gg1.Referencia, gg1.Nome NO_GG1,  ',
  'ggo.GraGruX, ggo.ItemValr, ggo.ItemUnid ',
  'FROM gragxserv ggo  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY NO_GG1',
  '']);
end;

procedure TFmLocCItsSvc.ReopenLCI();
const
  sProcName = 'TFmLocCItsSvc.ReopenLCI();';
var
  IDTab: TTabConLocIts;
begin
  case RGAtrelamento.ItemIndex of
    0, 1: QrLCI.Close;
    2: IDTab := TTabConLocIts.tcliLocacao;
    3: IDTab := TTabConLocIts.tcliVenda;
    else
    begin
      QrLCI.Close;
      Geral.MB_Erro('Tipo de atrelamento n�o definido! ' + sProcName);
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLCI, Dmod.MyDB, [
  'SELECT lci.CtrID ',
  'FROM loccconits lci ',
  'WHERE lci.Codigo=' + Geral.FF0(FCodigo),
  'AND IDTab=' + Geral.FF0(Integer(IDTab)),
  'ORDER BY CtrID ',
  '']);
  if QrLCI.RecordCount = 1 then
  begin
    if QrLCICtrID.Value <> 0 then
    begin
      EdAtrelCtrID.ValueVariant := QrLCICtrID.Value;
      CBAtrelCtrID.KeyValue     := QrLCICtrID.Value;
    end;
  end;
end;

procedure TFmLocCItsSvc.RGAtrelamentoClick(Sender: TObject);
begin
  ReopenLCI();
end;

procedure TFmLocCItsSvc.SBCadastroClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  GraL_Jan.MostraFormGraGXServ(EdGraGruX.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrGraGXServ, VAR_CADASTRO, 'Controle');
  PesquisaPorGraGruX();
  EdReferencia.SetFocus;
end;

procedure TFmLocCItsSvc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCItsSvc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FAlterou     := False;
  FCategoria   := 'S';
  FQtdeProduto := -1;
  TPDataLoc.Date := Trunc(Date);
  EdHoraLoc.Text := FormatDateTime('hh:nn', Now());
end;

end.
