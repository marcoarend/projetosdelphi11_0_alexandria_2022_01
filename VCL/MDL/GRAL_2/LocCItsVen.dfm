object FmLocCItsVen: TFmLocCItsVen
  Left = 450
  Top = 286
  Caption = 'LOC-PATRI-107 :: Adi'#231#227'o de Venda de Mercadorias '#224' Loca'#231#227'o'
  ClientHeight = 455
  ClientWidth = 685
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 685
    Height = 293
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object SBCadastro: TSpeedButton
      Left = 651
      Top = 134
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBCadastroClick
    end
    object Label7: TLabel
      Left = 12
      Top = 118
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label2: TLabel
      Left = 268
      Top = 78
      Width = 55
      Height = 13
      Caption = 'Refer'#234'ncia:'
    end
    object Label1: TLabel
      Left = 210
      Top = 78
      Width = 48
      Height = 13
      Caption = 'Reduzido:'
    end
    object Label21: TLabel
      Left = 12
      Top = 31
      Width = 134
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data / hora loca'#231#227'o / troca:'
      Enabled = False
    end
    object Label5: TLabel
      Left = 160
      Top = 31
      Width = 469
      Height = 13
      Caption = 
        'Centro de estoque (Cadastrados nos itens de movimenta'#231#227'o da regr' +
        'a fiscal selecionada no pedido):'
    end
    object Label6: TLabel
      Left = 12
      Top = 158
      Width = 84
      Height = 13
      Caption = 'Qtde em estoque:'
      FocusControl = DBEdQtde
    end
    object Label4: TLabel
      Left = 467
      Top = 217
      Width = 49
      Height = 13
      Caption = 'Valor item:'
      Enabled = False
    end
    object Label3: TLabel
      Left = 187
      Top = 158
      Width = 63
      Height = 13
      Caption = '$ Pre'#231'o unit.:'
    end
    object LaQtdeLocacao: TLabel
      Left = 103
      Top = 158
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label8: TLabel
      Left = 548
      Top = 158
      Width = 27
      Height = 13
      Caption = 'NCM:'
      Enabled = False
      FocusControl = DBEdNCM
    end
    object SbDesconto: TSpeedButton
      Left = 406
      Top = 174
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbDescontoClick
    end
    object Label13: TLabel
      Left = 303
      Top = 158
      Width = 60
      Height = 13
      Caption = '% Desconto:'
    end
    object Label14: TLabel
      Left = 431
      Top = 158
      Width = 109
      Height = 13
      Caption = 'Valor bruto mercadoria:'
      Enabled = False
    end
    object Label15: TLabel
      Left = 12
      Top = 78
      Width = 40
      Height = 13
      Caption = 'EAN 13:'
    end
    object SbPreco: TSpeedButton
      Left = 278
      Top = 174
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbPrecoClick
    end
    object SbPsqPrc: TSpeedButton
      Left = 483
      Top = 94
      Width = 30
      Height = 21
      Caption = '?$'
      OnClick = SbPsqPrcClick
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 12
      Top = 134
      Width = 633
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_GG1'
      ListSource = DsGraGXVend
      TabOrder = 7
      dmkEditCB = EdGraGruX
      UpdType = utYes
      LocF7SQLText.Strings = (
        'SELECT lpc.GraGruX _CODIGO, gg1.REFERENCIA, gg1.Nome _NOME'
        'FROM gragxvend lpc'
        'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX'
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
        'WHERE gg1.Nome LIKE "%$#%"'
        'OR gg1.Referencia LIKE "%$#%"'
        'OR gg1.Patrimonio LIKE "%$#%"'
        'AND ggx.Ativo=1')
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdReferencia: TdmkEdit
      Left = 268
      Top = 94
      Width = 211
      Height = 21
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdReferenciaChange
      OnKeyDown = EdReferenciaKeyDown
    end
    object EdGraGruX: TdmkEditCB
      Left = 210
      Top = 94
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraGruXChange
      OnRedefinido = EdGraGruXRedefinido
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CkContinuar: TCheckBox
      Left = 12
      Top = 271
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo'
      Checked = True
      State = cbChecked
      TabOrder = 16
    end
    object TPDataLoc: TdmkEditDateTimePicker
      Left = 12
      Top = 47
      Width = 99
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 41131.000000000000000000
      Time = 0.724786689817847200
      Enabled = False
      TabOrder = 0
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdHoraLoc: TdmkEdit
      Left = 116
      Top = 47
      Width = 41
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Enabled = False
      TabOrder = 1
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfLong
      HoraFormat = dmkhfShort
      Texto = '00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdStqCenCad: TdmkEditCB
      Left = 160
      Top = 47
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdStqCenCadRedefinido
      DBLookupComboBox = CBStqCenCad
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBStqCenCad: TdmkDBLookupComboBox
      Left = 216
      Top = 47
      Width = 457
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsStqCenCad
      TabOrder = 3
      dmkEditCB = EdStqCenCad
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object DBEdQtde: TDBEdit
      Left = 12
      Top = 174
      Width = 89
      Height = 21
      TabStop = False
      DataField = 'Qtde'
      DataSource = DsEstoque
      TabOrder = 8
    end
    object EdQuantidade: TdmkEdit
      Left = 104
      Top = 174
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdQuantidadeRedefinido
    end
    object EdValorTotal: TdmkEdit
      Left = 468
      Top = 233
      Width = 205
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 15
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdPrcUni: TdmkEdit
      Left = 188
      Top = 174
      Width = 89
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 10
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdPrcUniRedefinido
    end
    object DBEdNCM: TDBEdit
      Left = 548
      Top = 174
      Width = 125
      Height = 21
      DataField = 'NCM'
      DataSource = DsGraGXVend
      Enabled = False
      TabOrder = 13
    end
    object GroupBox1: TGroupBox
      Left = 12
      Top = 200
      Width = 445
      Height = 65
      Caption = ' Valores para NFC-e'
      TabOrder = 14
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 441
        Height = 48
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 3
          Top = 2
          Width = 36
          Height = 13
          Caption = '$ Frete:'
        end
        object Label10: TLabel
          Left = 107
          Top = 2
          Width = 46
          Height = 13
          Caption = '$ Seguro:'
        end
        object Label12: TLabel
          Left = 315
          Top = 2
          Width = 112
          Height = 13
          Caption = '$ Outros (desp. acess.):'
        end
        object Label11: TLabel
          Left = 211
          Top = 2
          Width = 58
          Height = 13
          Caption = '$ Desconto:'
          Enabled = False
        end
        object Edprod_vFrete: TdmkEdit
          Left = 4
          Top = 18
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdPrcUniRedefinido
        end
        object Edprod_vSeg: TdmkEdit
          Left = 108
          Top = 18
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdPrcUniRedefinido
        end
        object Edprod_vOutro: TdmkEdit
          Left = 316
          Top = 18
          Width = 117
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdPrcUniRedefinido
        end
        object Edprod_vDesc: TdmkEdit
          Left = 212
          Top = 18
          Width = 100
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdPrcUniRedefinido
        end
      end
    end
    object EdPercDesco: TdmkEdit
      Left = 304
      Top = 174
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 10
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMax = '100'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdPercDescoRedefinido
    end
    object EdValorBruto: TdmkEdit
      Left = 432
      Top = 174
      Width = 113
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 12
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdEAN13: TdmkEdit
      Left = 12
      Top = 94
      Width = 195
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdEAN13Change
      OnKeyDown = EdReferenciaKeyDown
      OnRedefinido = EdEAN13Redefinido
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 685
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 637
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 9
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 589
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 535
        Height = 32
        Caption = 'Adi'#231#227'o de Venda de Mercadorias '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 535
        Height = 32
        Caption = 'Adi'#231#227'o de Venda de Mercadorias '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 535
        Height = 32
        Caption = 'Adi'#231#227'o de Venda de Mercadorias '#224' Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 341
    Width = 685
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 681
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 385
    Width = 685
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 539
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 537
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DsGraGXVend: TDataSource
    DataSet = QrGraGXVend
    Left = 488
    Top = 54
  end
  object QrGraGXVend: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, ggx.GraGru1,'
      'gg1.Referencia, gg1.Nome NO_GG1,  gg1.NCM, gg1.UnidMed,'
      'ggo.GraGruX, ggo.ItemValr, ggo.ItemUnid'
      'FROM gragxvend ggo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggo.Aplicacao=:P0'
      'ORDER BY NO_GG1'
      '')
    Left = 488
    Top = 14
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGXVendControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXVendReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXVendNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXVendGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGXVendItemValr: TFloatField
      FieldName = 'ItemValr'
    end
    object QrGraGXVendItemUnid: TIntegerField
      FieldName = 'ItemUnid'
    end
    object QrGraGXVendGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGXVendNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGXVendMaxPercDesco: TFloatField
      FieldName = 'MaxPercDesco'
    end
    object QrGraGXVendUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGXVendEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  cpl.GraGruX=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 600
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrPesq2EAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  gg1.Referencia=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 572
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesq1EAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad scc'
      'WHERE Codigo IN '
      '('
      '  SELECT StqCenCad'
      '  FROM fisregmvt'
      '  WHERE Codigo=:P0'
      '  AND Empresa=:P1'
      ') '
      'ORDER BY Nome')
    Left = 360
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 176
    Top = 48
  end
  object QrFatPedCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvd.CodUsu CU_PediVda, pvd.Empresa, '
      'pvd.TabelaPrc, pvd.CondicaoPG, pvd.RegrFiscal,'
      'pvd.AFP_Sit, pvd.AFP_Per, pvd.Cliente,'
      'pvd.Codigo ID_Pedido, pvd.PedidoCli,'
      'ppc.JurosMes,'
      'frc.Nome NOMEFISREGCAD,'
      'ppc.MedDDReal, ppc.MedDDSimpl,'
      'par.TipMediaDD, par.Associada, par.FatSemEstq,'
      ''
      'par.CtaProdVen EMP_CtaProdVen,'
      'par.FaturaSeq EMP_FaturaSeq,'
      'par.FaturaSep EMP_FaturaSep,'
      'par.FaturaDta EMP_FaturaDta,'
      'par.TxtProdVen EMP_TxtProdVen,'
      'emp.Filial EMP_FILIAL,'
      'ufe.Codigo EMP_UF,'
      ''
      'ass.CtaProdVen ASS_CtaProdVen,'
      'ass.FaturaSeq ASS_FaturaSeq,'
      'ass.FaturaSep ASS_FaturaSep,'
      'ass.FaturaDta ASS_FaturaDta,'
      'ass.TxtProdVen ASS_TxtProdVen,'
      'ufa.Nome ASS_NO_UF,'
      'ufa.Codigo ASS_CO_UF,'
      'ase.Filial ASS_FILIAL,'
      ''
      'tpc.Nome NO_TabelaPrc,'
      '/*frm.TipoCalc,*/ fpc.*'
      'FROM fatpedcab fpc'
      'LEFT JOIN pedivda    pvd ON pvd.Codigo=fpc.Pedido'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc'
      'LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa'
      'LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada'
      'LEFT JOIN entidades  ase ON ase.Codigo=ass.Codigo'
      'LEFT JOIN entidades  emp ON emp.Codigo=par.Codigo'
      
        'LEFT JOIN ufs        ufe ON ufe.Codigo=IF(emp.Tipo=0, emp.EUF, e' +
        'mp.PUF)'
      
        'LEFT JOIN ufs        ufa ON ufa.Codigo=IF(ase.Tipo=0, ase.EUF, a' +
        'se.PUF)'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      '/* N'#227'o tem como'
      'LEFT JOIN fisregmvt frm ON frm.Codigo=pvd.RegrFiscal'
      '  AND frm.StqCenCad=1'
      '  AND frm.Empresa=-11'
      '*/'
      'WHERE fpc.Codigo > -1000')
    Left = 640
    Top = 212
    object QrFatPedCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'pedivda.Empresa'
    end
    object QrFatPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fatpedcab.Codigo'
      Required = True
    end
    object QrFatPedCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'fatpedcab.CodUsu'
      Required = True
    end
    object QrFatPedCabPedido: TIntegerField
      FieldName = 'Pedido'
      Origin = 'fatpedcab.Pedido'
      Required = True
    end
    object QrFatPedCabCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Origin = 'pedivda.CondicaoPG'
    end
    object QrFatPedCabTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Origin = 'pedivda.TabelaPrc'
    end
    object QrFatPedCabMedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Origin = 'pediprzcab.MedDDReal'
    end
    object QrFatPedCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Origin = 'pediprzcab.MedDDSimpl'
    end
    object QrFatPedCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Origin = 'pedivda.RegrFiscal'
    end
    object QrFatPedCabNO_TabelaPrc: TWideStringField
      FieldName = 'NO_TabelaPrc'
      Origin = 'tabeprccab.Nome'
      Required = True
      Size = 50
    end
    object QrFatPedCabTipMediaDD: TSmallintField
      FieldName = 'TipMediaDD'
      Origin = 'paramsemp.TipMediaDD'
      Required = True
    end
    object QrFatPedCabAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
      Origin = 'pedivda.AFP_Sit'
      Required = True
    end
    object QrFatPedCabAFP_Per: TFloatField
      FieldName = 'AFP_Per'
      Origin = 'pedivda.AFP_Per'
      Required = True
    end
    object QrFatPedCabAssociada: TIntegerField
      FieldName = 'Associada'
      Origin = 'paramsemp.Associada'
      Required = True
    end
    object QrFatPedCabCU_PediVda: TIntegerField
      FieldName = 'CU_PediVda'
      Origin = 'pedivda.CodUsu'
      Required = True
    end
    object QrFatPedCabAbertura: TDateTimeField
      FieldName = 'Abertura'
      Origin = 'fatpedcab.Abertura'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFatPedCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Origin = 'fatpedcab.Encerrou'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFatPedCabFatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
      Origin = 'paramsemp.FatSemEstq'
      Required = True
    end
    object QrFatPedCabENCERROU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENCERROU_TXT'
      Size = 30
      Calculated = True
    end
    object QrFatPedCabEMP_CtaProdVen: TIntegerField
      FieldName = 'EMP_CtaProdVen'
      Origin = 'paramsemp.CtaProdVen'
    end
    object QrFatPedCabEMP_FaturaSeq: TSmallintField
      FieldName = 'EMP_FaturaSeq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrFatPedCabEMP_FaturaSep: TWideStringField
      FieldName = 'EMP_FaturaSep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrFatPedCabEMP_FaturaDta: TSmallintField
      FieldName = 'EMP_FaturaDta'
      Origin = 'paramsemp.FaturaDta'
    end
    object QrFatPedCabEMP_TxtProdVen: TWideStringField
      FieldName = 'EMP_TxtProdVen'
      Origin = 'paramsemp.TxtProdVen'
      Size = 100
    end
    object QrFatPedCabEMP_FILIAL: TIntegerField
      FieldName = 'EMP_FILIAL'
      Origin = 'entidades.Filial'
    end
    object QrFatPedCabASS_CtaProdVen: TIntegerField
      FieldName = 'ASS_CtaProdVen'
      Origin = 'paramsemp.CtaProdVen'
    end
    object QrFatPedCabASS_FaturaSeq: TSmallintField
      FieldName = 'ASS_FaturaSeq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrFatPedCabASS_FaturaSep: TWideStringField
      FieldName = 'ASS_FaturaSep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrFatPedCabASS_FaturaDta: TSmallintField
      FieldName = 'ASS_FaturaDta'
      Origin = 'paramsemp.FaturaDta'
    end
    object QrFatPedCabASS_TxtProdVen: TWideStringField
      FieldName = 'ASS_TxtProdVen'
      Origin = 'paramsemp.TxtProdVen'
      Size = 100
    end
    object QrFatPedCabASS_NO_UF: TWideStringField
      FieldName = 'ASS_NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrFatPedCabASS_CO_UF: TIntegerField
      FieldName = 'ASS_CO_UF'
      Origin = 'ufs.Codigo'
      Required = True
    end
    object QrFatPedCabASS_FILIAL: TIntegerField
      FieldName = 'ASS_FILIAL'
      Origin = 'entidades.Filial'
    end
    object QrFatPedCabCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'pedivda.Cliente'
    end
    object QrFatPedCabSerieDesfe: TIntegerField
      FieldName = 'SerieDesfe'
    end
    object QrFatPedCabNFDesfeita: TIntegerField
      FieldName = 'NFDesfeita'
    end
    object QrFatPedCabPEDIDO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PEDIDO_TXT'
      Size = 21
      Calculated = True
    end
    object QrFatPedCabID_Pedido: TIntegerField
      FieldName = 'ID_Pedido'
      Required = True
    end
    object QrFatPedCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrFatPedCabNOMEFISREGCAD: TWideStringField
      FieldName = 'NOMEFISREGCAD'
      Size = 50
    end
    object QrFatPedCabEMP_UF: TIntegerField
      FieldName = 'EMP_UF'
      Required = True
    end
    object QrFatPedCabPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
  end
  object QrCli: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, indIEDest,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0' +
        '.000 NUMERO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL,'
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE,'
      'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END + 0.000 UF,'
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD,'
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF,'
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's,'
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0' +
        '.000 Lograd,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0' +
        '.000 CEP,'
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF,'
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ','
      'L_Ativo '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 572
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrCliUF: TFloatField
      FieldName = 'UF'
    end
    object QrCliCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrCliLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrCliindIEDest: TSmallintField
      FieldName = 'indIEDest'
    end
    object QrCliL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
    end
  end
  object QrItem: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, '
      'gcc.Nome NOMECOR,  gti.Nome NOMETAM, '
      'ggx.Controle GraGruX, ggx.GraGru1, '
      'gg1.CodUsu CU_Nivel1, gg1.IPI_Alq, '
      'pgt.MadeBy, pgt.Fracio, '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.prod_indTot'
      'FROM gragrux ggx '
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 496
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItemNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrItemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrItemNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrItemNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
      Required = True
    end
    object QrItemGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
      Required = True
    end
    object QrItemCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
      Required = True
    end
    object QrItemIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrItemMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrItemFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrItemHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrItemGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrItemprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
  end
  object QrEstoque: TMySQLQuery
    Database = Dmod.MyDB
    Left = 120
    Top = 65524
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
  end
  object DsEstoque: TDataSource
    DataSet = QrEstoque
    Left = 120
    Top = 36
  end
  object PMPrcUni: TPopupMenu
    Left = 336
    Top = 345
    object Aumentar1: TMenuItem
      Caption = '&Aumentar'
      OnClick = Aumentar1Click
    end
    object Diminuir1: TMenuItem
      Caption = '&Diminuir'
      OnClick = Diminuir1Click
    end
  end
end
