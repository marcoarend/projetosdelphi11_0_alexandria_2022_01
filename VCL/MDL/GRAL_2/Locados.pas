unit Locados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmLocados = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Qr_: TMySQLQuery;
    Ds_: TDataSource;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrLocados: TMySQLQuery;
    QrLocadosEmpresa: TIntegerField;
    QrLocadosNO_EMP: TWideStringField;
    QrLocadosNO_COMPRADOR: TWideStringField;
    QrLocadosNO_RECEBEU: TWideStringField;
    QrLocadosCodigo: TIntegerField;
    QrLocadosContrato: TIntegerField;
    QrLocadosCliente: TIntegerField;
    QrLocadosDtHrEmi: TDateTimeField;
    QrLocadosDtHrBxa: TDateTimeField;
    QrLocadosVendedor: TIntegerField;
    QrLocadosECComprou: TIntegerField;
    QrLocadosECRetirou: TIntegerField;
    QrLocadosNumOC: TWideStringField;
    QrLocadosValorTot: TFloatField;
    QrLocadosLocalObra: TWideStringField;
    QrLocadosObs0: TWideStringField;
    QrLocadosObs1: TWideStringField;
    QrLocadosObs2: TWideStringField;
    QrLocadosNO_LOGIN: TWideStringField;
    QrLocadosNO_CLIENTE: TWideStringField;
    QrLocadosEndCobra: TWideStringField;
    QrLocadosLocalCntat: TWideStringField;
    QrLocadosLocalFone: TWideStringField;
    QrLocadosFilial: TIntegerField;
    QrLocadosTipoAluguel: TWideStringField;
    QrLocadosHistImprime: TWideMemoField;
    QrLocadosHistNaoImpr: TWideMemoField;
    QrLocadosDtHrSai: TDateTimeField;
    QrLocadosValorAdicional: TFloatField;
    QrLocadosValorAdiantamento: TFloatField;
    QrLocadosValorDesconto: TFloatField;
    QrLocadosStatus: TIntegerField;
    QrLocadosDtUltCobMens: TDateField;
    QrLocadosImportado: TSmallintField;
    QrLocadosDtHrDevolver: TDateTimeField;
    QrLocadosValorEquipamentos: TFloatField;
    QrLocadosValorLocacao: TFloatField;
    QrLocadosFormaCobrLoca: TSmallintField;
    QrLocadosNO_STATUS: TWideStringField;
    QrLocadosNO_TipoAluguel: TWideStringField;
    QrLocadosDtHrSai_TXT: TWideStringField;
    QrLocadosDtHrDevolver_TXT: TWideStringField;
    QrLocadosDtUltAtzPend: TDateField;
    QrLocadosValorASerAdiantado: TFloatField;
    QrLocadosValorProdutos: TFloatField;
    QrLocadosValorLocAAdiantar: TFloatField;
    QrLocadosValorLocAtualizado: TFloatField;
    QrLocadosValorServicos: TFloatField;
    QrLocadosValorPago: TFloatField;
    QrLocadosValorVendas: TFloatField;
    QrLocadosValorOrca: TFloatField;
    QrLocadosValorPedi: TFloatField;
    QrLocadosValorFatu: TFloatField;
    QrLocadosValorBrto: TFloatField;
    QrLocadosJaLocou: TSmallintField;
    DsLocados: TDataSource;
    QrLocadosDtHrBxa_TXT: TWideStringField;
    QrItsLca: TMySQLQuery;
    QrItsLcaCodigo: TIntegerField;
    QrItsLcaCtrID: TIntegerField;
    QrItsLcaItem: TIntegerField;
    QrItsLcaManejoLca: TSmallintField;
    QrItsLcaGraGruX: TIntegerField;
    QrItsLcaValorDia: TFloatField;
    QrItsLcaValorSem: TFloatField;
    QrItsLcaValorQui: TFloatField;
    QrItsLcaValorMes: TFloatField;
    QrItsLcaRetFunci: TIntegerField;
    QrItsLcaRetExUsr: TIntegerField;
    QrItsLcaDtHrLocado: TDateTimeField;
    QrItsLcaDtHrRetorn: TDateTimeField;
    QrItsLcaLibExUsr: TIntegerField;
    QrItsLcaLibFunci: TIntegerField;
    QrItsLcaLibDtHr: TDateTimeField;
    QrItsLcaRELIB: TWideStringField;
    QrItsLcaHOLIB: TWideStringField;
    QrItsLcaQtdeProduto: TIntegerField;
    QrItsLcaValorProduto: TFloatField;
    QrItsLcaQtdeLocacao: TIntegerField;
    QrItsLcaValorLocacao: TFloatField;
    QrItsLcaQtdeDevolucao: TIntegerField;
    QrItsLcaTroca: TSmallintField;
    QrItsLcaCategoria: TWideStringField;
    QrItsLcaCOBRANCALOCACAO: TWideStringField;
    QrItsLcaCobrancaConsumo: TFloatField;
    QrItsLcaCobrancaRealizadaVenda: TWideStringField;
    QrItsLcaCobranIniDtHr: TDateTimeField;
    QrItsLcaSaiDtHr: TDateTimeField;
    QrItsLcaDMenos: TIntegerField;
    QrItsLcaValorFDS: TFloatField;
    QrItsLcaValorLocAtualizado: TFloatField;
    QrItsLcaValDevolParci: TFloatField;
    PB1: TProgressBar;
    RGStatus: TRadioGroup;
    QrItsLcaOriSubstSaiDH: TDateTimeField;
    QrItsLcaHrTolerancia: TTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure dmkDBGridZTO1DblClick(Sender: TObject);
    procedure RGStatusClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ReopenLocados(Codigo: Integer);
  end;

  var
  FmLocados: TFmLocados;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UnAppEnums, UnAppPF, ModuleGeral,
  Principal, UnGraL_Jan;

{$R *.DFM}

procedure TFmLocados.BtOKClick(Sender: TObject);
  procedure AtualizaAtual();
  const
    GeraLog = False;
  var
    Codigo, Status: Integer;
    DtFimCobra: TDateTime;
  begin
    Codigo := QrLocadosCodigo.Value;
    Status := QrLocadosStatus.Value;
    DtFimCobra := DModG.ObtemAgora();
    if AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, (*CtrID*)0,
    QrLocadosDtHrEmi.Value, QrLocadosDtHrSai.Value,
    QrLocadosDtHrDevolver.Value, DtFimCobra, GeraLog) then
      AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
    //AppPF.AtualizaTotaisLocCConCab_Servicos(Codigo);
    //AppPF.AtualizaTotaisLocCConCab_Vendas(Codigo);
    //
  end;
begin
  QrLocados.DisableControls;
  try
    if QrLocados.State = dsInactive then
      ReopenLocados(0);
    PB1.Position := 0;
    PB1.Max := QrLocados.RecordCount;
    QrLocados.First;
    while not QrLocados.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      AtualizaAtual();
      //
      QrLocados.Next;
    end;
  finally
    QrLocados.EnableControls;
  end;
  ReopenLocados(0);
end;

procedure TFmLocados.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocados.dmkDBGridZTO1DblClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrLocados.State <> dsInactive) and (QrLocados.RecordCount > 0) then
  begin
    Codigo := QrLocadosCodigo.Value;
    FmPrincipal.AdvToolBarPagerNovo.Visible := False;
    GraL_Jan.MostraFormLocCCon(True, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPagerNovo, Codigo);
    Close;
  end;
end;

procedure TFmLocados.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocados.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmLocados.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocados.ReopenLocados(Codigo: Integer);
begin
  if RGStatus.ItemIndex > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocados, Dmod.MyDB, [
    'SELECT ELT(lcc.Status + 1, "N�O DEFINIDO", "ABERTO",  ',
    '"LOCADO", "FINALIZADO", "CANCELADO", "SUSPENSO",  ',
    '"N�O IMPLEMENTADO") NO_STATUS,  ',
    'CASE lcc.TipoAluguel ',
    '  WHEN "D" THEN "DIARIA"  ',
    '  WHEN "S" THEN "SEMANAL"  ',
    '  WHEN "Q" THEN "QUINZENAL"  ',
    '  WHEN "M" THEN "MENSAL"  ',
    'ELSE "INDEFINIDO" END NO_TipoAluguel, ',
    'IF(DtHrSai < "1900-01-010", "",  ',
    '  DATE_FORMAT(DtHrSai, "%d/%m/%Y %H:%i")) DtHrSai_TXT, ',
    'IF(DtHrDevolver < "1900-01-010", "",  ',
    '  DATE_FORMAT(DtHrDevolver, "%d/%m/%Y %H:%i"))  ',
    '  DtHrDevolver_TXT, ',
    'IF(DtHrBxa < "1900-01-010", "",  ',
    '  DATE_FORMAT(DtHrBxa, "%d/%m/%Y %H:%i")) DtHrBxa_TXT, ',
    'com.Nome NO_COMPRADOR, ret.Nome NO_RECEBEU,  ',
    'lcc.*, sen.Login NO_LOGIN, cin.CodCliInt Filial,  ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,  ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE  ',
    'FROM loccconcab lcc  ',
    'LEFT JOIN entidades emp ON emp.Codigo=lcc.Empresa  ',
    'LEFT JOIN enticliint cin ON cin.CodEnti=emp.Codigo  ',
    'LEFT JOIN entidades cli ON cli.Codigo=lcc.Cliente  ',
    'LEFT JOIN senhas sen ON sen.Numero=lcc.Vendedor  ',
    'LEFT JOIN enticontat com ON com.Controle=lcc.ECComprou  ',
    'LEFT JOIN enticontat ret ON ret.Controle=lcc.ECRetirou  ',
    'WHERE lcc.Status=' + Geral.FF0(RGStatus.ItemIndex), // Integer(TStatusLocacao.statlocLocado)),
    '']);
    //
    if Codigo <> 0 then
      QrLocados.Locate('Codigo', Codigo, []);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      Geral.FF0(QrLocados.RecordCount) + ' loca��es.');
  end else
  begin
    QrLocados.Close;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '...');
  end;
end;

procedure TFmLocados.RGStatusClick(Sender: TObject);
begin
  ReopenLocados(0);
  BtOK.Enabled := RGStatus.ItemIndex = Integer(TStatusLocacao.statlocLocado); // 2
end;
(*
SELECT its.*, orc.*
FROM Orcamento orc
LEFT JOIN orcamentoitens its ON its.Orcamento=orc.Orcamento
WHERE orc.Cliente=567
ORDER BY orc.Emissao DESC
*)
end.
