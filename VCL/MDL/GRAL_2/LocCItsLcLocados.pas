unit LocCItsLcLocados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, Vcl.Mask, dmkDBEdit, UnAppEnums,
  dmkDBGridZTO;

type
  TFmLocCItsLcLocados = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    QrGraGXPatr: TMySQLQuery;
    DsGraGXPatr: TDataSource;
    QrGraGXPatrSIT_CHR: TWideStringField;
    QrGraGXPatrControle: TIntegerField;
    QrGraGXPatrPatrimonio: TWideStringField;
    QrGraGXPatrReferencia: TWideStringField;
    QrGraGXPatrNCM: TWideStringField;
    QrGraGXPatrUnidMed: TIntegerField;
    QrGraGXPatrCOD_GG1: TIntegerField;
    QrGraGXPatrNO_GG1: TWideStringField;
    QrGraGXPatrNO_MARCA: TWideStringField;
    QrGraGXPatrNO_FABR: TWideStringField;
    QrGraGXPatrCPL_EXISTE: TFloatField;
    QrGraGXPatrNO_GG2: TWideStringField;
    QrGraGXPatrNO_GG3: TWideStringField;
    QrGraGXPatrNO_GG4: TWideStringField;
    QrGraGXPatrNO_GG5: TWideStringField;
    QrGraGXPatrNO_GGT: TWideStringField;
    QrGraGXPatrTitNiv1: TWideStringField;
    QrGraGXPatrTitNiv2: TWideStringField;
    QrGraGXPatrTitNiv3: TWideStringField;
    QrGraGXPatrTitNiv4: TWideStringField;
    QrGraGXPatrTitNiv5: TWideStringField;
    QrGraGXPatrPrdGrupTip: TIntegerField;
    QrGraGXPatrNivel1: TIntegerField;
    QrGraGXPatrNivel2: TIntegerField;
    QrGraGXPatrNivel3: TIntegerField;
    QrGraGXPatrNivel4: TIntegerField;
    QrGraGXPatrNivel5: TIntegerField;
    QrGraGXPatrCUNivel5: TIntegerField;
    QrGraGXPatrCUNivel4: TIntegerField;
    QrGraGXPatrCUNivel3: TIntegerField;
    QrGraGXPatrCUNivel2: TIntegerField;
    QrGraGXPatrCodUsu: TIntegerField;
    QrGraGXPatrGraGruX: TIntegerField;
    QrGraGXPatrComplem: TWideStringField;
    QrGraGXPatrAquisData: TDateField;
    QrGraGXPatrAquisDocu: TWideStringField;
    QrGraGXPatrAquisValr: TFloatField;
    QrGraGXPatrSituacao: TWordField;
    QrGraGXPatrAtualValr: TFloatField;
    QrGraGXPatrValorMes: TFloatField;
    QrGraGXPatrValorQui: TFloatField;
    QrGraGXPatrValorSem: TFloatField;
    QrGraGXPatrValorDia: TFloatField;
    QrGraGXPatrAgrupador: TIntegerField;
    QrGraGXPatrMarca: TIntegerField;
    QrGraGXPatrModelo: TWideStringField;
    QrGraGXPatrSerie: TWideStringField;
    QrGraGXPatrVoltagem: TWideStringField;
    QrGraGXPatrPotencia: TWideStringField;
    QrGraGXPatrCapacid: TWideStringField;
    QrGraGXPatrVendaData: TDateField;
    QrGraGXPatrVendaDocu: TWideStringField;
    QrGraGXPatrVendaValr: TFloatField;
    QrGraGXPatrVendaEnti: TIntegerField;
    QrGraGXPatrObserva: TWideStringField;
    QrGraGXPatrAGRPAT: TWideStringField;
    QrGraGXPatrCLVPAT: TWideStringField;
    QrGraGXPatrMARPAT: TWideStringField;
    QrGraGXPatrAplicacao: TIntegerField;
    QrGraGXPatrDescrTerc: TWideStringField;
    QrGraGXPatrValorFDS: TFloatField;
    QrGraGXPatrDMenos: TSmallintField;
    QrGraGXPatrItemUnid: TIntegerField;
    QrGraGXPatrLk: TIntegerField;
    QrGraGXPatrDataCad: TDateField;
    QrGraGXPatrDataAlt: TDateField;
    QrGraGXPatrUserCad: TIntegerField;
    QrGraGXPatrUserAlt: TIntegerField;
    QrGraGXPatrAlterWeb: TSmallintField;
    QrGraGXPatrAWServerID: TIntegerField;
    QrGraGXPatrAWStatSinc: TSmallintField;
    QrGraGXPatrAtivo: TSmallintField;
    GBDados: TGroupBox;
    Label2: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label3: TLabel;
    Label12: TLabel;
    Label25: TLabel;
    Label1: TLabel;
    Label46: TLabel;
    Label51: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    DBRGAplicacao: TDBRadioGroup;
    DBCkAtivo: TDBCheckBox;
    dmkDBEdit2: TdmkDBEdit;
    QrSelecionado: TMySQLQuery;
    DsSelecionado: TDataSource;
    QrSelecionadoQtdLocado: TLargeintField;
    QrSelecionadoNOENT: TWideStringField;
    QrSelecionadoCodigo: TIntegerField;
    QrSelecionadoCtrID: TIntegerField;
    QrSelecionadoItem: TIntegerField;
    QrSelecionadoDtHrLocado: TDateTimeField;
    QrSelecionadoDtHrRetorn: TDateTimeField;
    QrSelecionadoQtdeProduto: TIntegerField;
    QrSelecionadoQtdeDevolucao: TIntegerField;
    QrSelecionadoCalcAdiLOCACAO: TWideStringField;
    QrSelecionadoCOBRANCALOCACAO: TWideStringField;
    QrSelecionadoNome: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet3: TTabSheet;
    DBGLocados: TDBGrid;
    DBGrid3: TDBGrid;
    QrDisponiveis: TMySQLQuery;
    DsDisponiveis: TDataSource;
    QrLcs: TMySQLQuery;
    QrLocados: TMySQLQuery;
    DsLocados: TDataSource;
    QrLocadosControle: TIntegerField;
    QrLocadosNivel2: TIntegerField;
    QrLocadosNome: TWideStringField;
    QrLocadosQtdLocado: TLargeintField;
    QrLocadosNOENT: TWideStringField;
    QrLocadosCodigo: TIntegerField;
    QrLocadosCtrID: TIntegerField;
    QrLocadosItem: TIntegerField;
    QrLocadosDtHrLocado: TDateTimeField;
    QrLocadosDtHrRetorn: TDateTimeField;
    QrLocadosQtdeProduto: TIntegerField;
    QrLocadosQtdeDevolucao: TIntegerField;
    QrLocadosCalcAdiLOCACAO: TWideStringField;
    QrLocadosCOBRANCALOCACAO: TWideStringField;
    QrDisponiveisControle: TIntegerField;
    QrDisponiveisNivel2: TIntegerField;
    QrDisponiveisNome: TWideStringField;
    QrDisponiveisPatrimonio: TWideStringField;
    QrDisponiveisReferencia: TWideStringField;
    QrLocadosPatrimonio: TWideStringField;
    QrLocadosReferencia: TWideStringField;
    TabSheet4: TTabSheet;
    Panel3: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    DBMemo1: TDBMemo;
    QrStqAlocIts: TMySQLQuery;
    QrStqAlocItsREFERENCIA_A: TWideStringField;
    QrStqAlocItsEAN13_A: TWideStringField;
    QrStqAlocItsNome_A: TWideStringField;
    QrStqAlocItsREFERENCIA_S: TWideStringField;
    QrStqAlocItsEAN13_S: TWideStringField;
    QrStqAlocItsNome_S: TWideStringField;
    QrStqAlocItsCodigo: TIntegerField;
    QrStqAlocItsControle: TIntegerField;
    QrStqAlocItsEmpresa: TIntegerField;
    QrStqAlocItsDataHora: TDateTimeField;
    QrStqAlocItsFatID: TIntegerField;
    QrStqAlocItsQtde: TFloatField;
    QrStqAlocItsCustoAll: TFloatField;
    QrStqAlocItsBaixa: TSmallintField;
    QrStqAlocItsLk: TIntegerField;
    QrStqAlocItsDataCad: TDateField;
    QrStqAlocItsDataAlt: TDateField;
    QrStqAlocItsUserCad: TIntegerField;
    QrStqAlocItsUserAlt: TIntegerField;
    QrStqAlocItsAlterWeb: TSmallintField;
    QrStqAlocItsAWServerID: TIntegerField;
    QrStqAlocItsAWStatSinc: TSmallintField;
    QrStqAlocItsAtivo: TSmallintField;
    QrStqAlocItsGGX_Aloc: TIntegerField;
    QrStqAlocItsGGX_SMIA: TIntegerField;
    QrStqAlocItsTpMov_TXT: TWideStringField;
    QrStqAlocItsHistorico: TWideStringField;
    DsStqAlocIts: TDataSource;
    Splitter1: TSplitter;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure DBGLocadosDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FGraGruX, FNivel2, FEmpresa: Integer;
    //
    procedure ReopenLocados();
  end;

  var
  FmLocCItsLcLocados: TFmLocCItsLcLocados;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnGraL_Jan;

{$R *.DFM}

procedure TFmLocCItsLcLocados.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCItsLcLocados.DBGLocadosDblClick(Sender: TObject);
begin
  if Uppercase(DBGLocados.Columns[THackStringGrid(DBGLocados).Col -1].FieldName) = 'PATRIMONIO' then
  begin
    GraL_Jan.MostraFormGraGXPatr(QrLocadosControle.Value);

  end;
end;

procedure TFmLocCItsLcLocados.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCItsLcLocados.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
  FEmpresa := -11;
end;

procedure TFmLocCItsLcLocados.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCItsLcLocados.FormShow(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPatr, Dmod.MyDB, [
  'SELECT CHAR(cpl.Situacao) SIT_CHR, ggx.Controle, ',
  'gg1.Patrimonio, gg1.Referencia, gg1.NCM, gg1.UnidMed, ',
  'gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, ',
  'gfm.Nome NO_MARCA, gfc.Nome NO_FABR, ',
  'IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, ',
  'gg2.Nome NO_GG2, gg3.Nome NO_GG3, ',
  'gg4.Nome NO_GG4, gg5.Nome NO_GG5, ggt.Nome NO_GGT, ',
  'ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3, ggt.TitNiv4, ',
  'ggt.TitNiv5, gg1.PrdGrupTip, gg1.Nivel1, gg1.Nivel2, ',
  'gg1.Nivel3, gg1.Nivel4, gg1.Nivel5, ',
  'gg5.CodUsu CUNivel5, gg4.CodUsu CUNivel4, ',
  'gg3.CodUsu CUNivel3, gg2.CodUsu CUNivel2, ',
  'gg1.CodUsu, cpl.*  ',
  'FROM gragxpatr cpl   ',
  'LEFT JOIN gragrux ggx ON cpl.GraGruX=ggx.Controle ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2  ',
  'LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3  ',
  'LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg1.Nivel4  ',
  'LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5  ',
  'LEFT JOIN prdgruptip ggt ON ggt.Codigo=gg1.PrdGrupTip  ',
  'LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle  ',
  'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo  ',
  'LEFT JOIN gragruy ggy ON ggy.Codigo=ggx.Controle  ',
  'WHERE ggx.Controle=' + Geral.FF0(FGraGruX),
  '']);
  //
end;

procedure TFmLocCItsLcLocados.PageControl1Change(Sender: TObject);
var
  GGXs: String;
begin
  case PageControl1.ActivePageIndex of
    1: // Dispon�veis (N�o Locados)
    begin
      if QrDisponiveis.State = dsInactive then
      begin
{
        UnDmkDAC_PF.AbreMySQLQuery0(QrLcs, Dmod.MyDB, [
        ' SELECT GROUP_CONCAT(DISTINCT Controle SEPARATOR ", ")  Controles',
        ' FROM loccitslca pri    ',
        ' LEFT JOIN loccconcab cab ON cab.Codigo=pri.Codigo   ',
        ' LEFT JOIN gragrux ggx ON pri.GraGruX=ggx.Controle',
        ' LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
        ' WHERE gg1.Nivel2=' + Geral.FF0(FNivel2),
        '']);
        GGXs := Trim(QrLcs.FieldByName('Controles').AsWideString);
        if GGXs <> EmptyStr then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrDisponiveis, Dmod.MyDB, [
          ' SELECT ggx.Controle, gg1.Nivel2, gg1.Nome, ',
          ' gg1.Patrimonio, gg1.Referencia ',
          ' FROM gragrux ggx ',
          ' LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
          ' WHERE gg1.Nivel2=' + Geral.FF0(FNivel2),
          ' AND ggx.Controle NOT IN ',
          '(',
          GGXs,
          ')',
          ' ORDER BY gg1.Nome',
          '']);
        end else
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrDisponiveis, Dmod.MyDB, [
          ' SELECT ggx.Controle, gg1.Nivel2, gg1.Nome, ',
          ' gg1.Patrimonio, gg1.Referencia ',
          ' FROM gragrux ggx ',
          ' LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
          ' WHERE gg1.Nivel2=' + Geral.FF0(FNivel2),
          ' AND ggx.Ativo=1 ',
          ' ORDER BY gg1.Nome',
          '']);
        end;
}
        UnDmkDAC_PF.AbreMySQLQuery0(QrDisponiveis, Dmod.MyDB, [
        'SELECT ggx.Controle, gg1.Nivel2, gg1.Nome, ',
        'gg1.Patrimonio, gg1.Referencia,',
        'gep.EstqSdo ',
        'FROM GraGruEPat gep',
        'LEFT JOIN gragrux ggx ON ggx.Controle=gep.GraGruX',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
        'WHERE gep.Empresa=' + Geral.FF0(FEmpresa),
        'AND ggx.Ativo=1',
        'AND gg1.Nivel2=' + Geral.FF0(FNivel2),
        'AND gep.EstqSdo>0',
        'ORDER BY gg1.Nome',
        '']);
      end;
    end;
    2: //LOCADOS
    begin
      if QrLocados.State = dsInactive then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLocados, Dmod.MyDB, [
(*
        ' SELECT ggx.Controle, gg1.Nivel2, gg1.Nome,',
        ' pri.QtdeLocacao-pri.QtdeDevolucao QtdLocado,',
        ' IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOENT,',
        ' pri.Codigo, pri.CtrID, pri.Item, pri.DtHrLocado,',
        ' pri.DtHrRetorn, QtdeProduto, QtdeDevolucao,',
        ' CalcAdiLOCACAO, COBRANCALOCACAO, ',
        ' gg1.Patrimonio, gg1.Referencia ',
        ' FROM loccitslca pri',
        ' LEFT JOIN loccconcab cab ON cab.Codigo=pri.Codigo',
        ' LEFT JOIN entidades ent ON ent.Codigo=cab.Cliente',
        ' LEFT JOIN gragrux ggx ON pri.GraGruX=ggx.Controle',
        ' LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
        ' /* WHERE pri.GraGrux=14 */',
        ' WHERE gg1.Nivel2=' + Geral.FF0(FNivel2),
        ' AND ggx.Ativo=1',
        ' AND pri.QtdeDevolucao < pri.QtdeLocacao',
        ' AND cab.DtHrBxa < "1900-01-01"',
        ' AND cab.STATUS=2', // LOCADO
        ' ORDER BY DtHrRetorn, gg1.Nome',
*)
        'SELECT ggx.Controle, gg1.Nivel2, gg1.Nome,',
        'pri.QtdeLocacao-pri.QtdeDevolucao QtdLocado,',
        'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOENT,',
        'pri.Codigo, pri.CtrID, pri.Item, pri.DtHrLocado,',
        'pri.DtHrRetorn, QtdeProduto, QtdeDevolucao,',
        'CalcAdiLOCACAO, COBRANCALOCACAO, ',
        'gg1.Patrimonio, gg1.Referencia,',
        '(pri.QtdeLocacao * pri.QtdeProduto)-pri.QtdeDevolucao QtdLocado',
        'FROM loccitslca pri',
        'LEFT JOIN loccconcab cab ON cab.Codigo=pri.Codigo',
        'LEFT JOIN entidades ent ON ent.Codigo=cab.Cliente',
        'LEFT JOIN gragrux ggx ON pri.GraGruX=ggx.Controle',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
        'WHERE gg1.Nivel2=' + Geral.FF0(FNivel2),
        'AND ggx.Ativo=1',
        'AND cab.Empresa=' + Geral.FF0(FEmpresa),
        'AND pri.QtdeDevolucao < (pri.QtdeLocacao * pri.QtdeProduto) ',
        'AND cab.DtHrBxa < "1900-01-01"',
        ' AND cab.Status IN (' +
        Geral.FF0(Integer(TStatusLocacao.statlocAberto))
        + ',' +
        Geral.FF0(Integer(TStatusLocacao.statlocLocado))
        + ') ',
        'ORDER BY DtHrRetorn, gg1.Nome',
        '']);
      end;
    end;
    3:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrStqaLocIts, Dmod.MyDB, [
      'SELECT ',
      'gg1a.REFERENCIA REFERENCIA_A, ggxa.EAN13 EAN13_A, gg1a.Nome Nome_A,',
      'gg1s.REFERENCIA REFERENCIA_S, ggxs.EAN13 EAN13_S, gg1s.Nome Nome_S,',
      'IF(Baixa=1, "Aloca��o", "Desaloca��o") TpMov_TXT, ',
      'sai.*',
      'FROM stqalocits sai ',
      'LEFT JOIN gragrux ggxa ON ggxa.Controle=sai.GGX_Aloc',
      'LEFT JOIN gragru1 gg1a ON gg1a.Nivel1=ggxa.GraGru1 ',
      'LEFT JOIN gragrux ggxs ON ggxs.Controle=sai.GGX_SMIA',
      'LEFT JOIN gragru1 gg1s ON gg1s.Nivel1=ggxs.GraGru1 ',
      'WHERE ggxa.Controle=' + Geral.FF0(FGraGruX),
      'ORDER BY Controle DESC',
      '']);
    end;
  end;
end;

procedure TFmLocCItsLcLocados.ReopenLocados();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSelecionado, Dmod.MyDB, [
  '  SELECT gg1.Nome,  ',
  ' IF(cab.DtHrBxa < "1900-01-01" AND cab.Status IN (1,2),  ',
  '   (pri.QtdeLocacao * pri.QtdeProduto)-pri.QtdeDevolucao, 0) QtdLocado, ',
  ' IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOENT,  ',
  ' pri.Codigo, pri.CtrID, pri.Item, pri.DtHrLocado,  ',
  ' pri.DtHrRetorn, QtdeProduto, QtdeDevolucao,   ',
  ' CalcAdiLOCACAO, COBRANCALOCACAO   ',
  ' FROM loccitslca pri     ',
  ' LEFT JOIN loccconcab cab ON cab.Codigo=pri.Codigo    ',
  ' LEFT JOIN entidades ent ON ent.Codigo=cab.Cliente  ',
  ' LEFT JOIN gragrux ggx ON pri.GraGruX=ggx.Controle ',
  ' LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  ' WHERE pri.GraGrux=' + Geral.FF0(FGraGruX),
  //' AND pri.QtdeDevolucao < pri.QtdeLocacao    ',
  //' AND cab.DtHrBxa < "1900-01-01"    ',
  //' AND cab.STATUS=2    ',
  'ORDER BY pri.Item DESC',
  '']);
end;

end.
