unit LocCConPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkDBGridZTO, dmkCheckGroup, frxClass,
  frxDBSet;

type
  TFmLocCConPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    QrClientes: TMySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    Panel5: TPanel;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    Label21: TLabel;
    TPDataEmiIni: TdmkEditDateTimePicker;
    TPDataEmiFim: TdmkEditDateTimePicker;
    Label17: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Panel6: TPanel;
    QrECComprou: TMySQLQuery;
    QrECComprouControle: TIntegerField;
    QrECComprouNome: TWideStringField;
    DsECComprou: TDataSource;
    QrECRetirou: TMySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsECRetirou: TDataSource;
    EdParteTexto: TdmkEdit;
    CkGGY_1024: TCheckBox;
    CkGGY_2048: TCheckBox;
    CkGGY_3072: TCheckBox;
    CkGGY_4096: TCheckBox;
    CkGGY_5120: TCheckBox;
    CkGGY_6144: TCheckBox;
    CkGGY_7168: TCheckBox;
    CkGGY_8192: TCheckBox;
    EdParteProd: TdmkEdit;
    QrPsqIts: TMySQLQuery;
    QrPsqItsNO_STATUS: TWideStringField;
    QrPsqItsNO_PRD_TAM_COR: TWideStringField;
    QrPsqItsDtHrEmi: TDateTimeField;
    QrPsqItsDtHrBxa: TDateTimeField;
    QrPsqItsTipoAluguel: TWideStringField;
    QrPsqItsStatus: TIntegerField;
    QrPsqItsGraGruX: TIntegerField;
    QrPsqItsDtHrRetorn_TXT: TWideStringField;
    QrPsqItsNO_CLI: TWideStringField;
    Label1: TLabel;
    DsPsqIts: TDataSource;
    QrPsqItsPatrimonio: TWideStringField;
    QrPsqItsReferencia: TWideStringField;
    QrPsqItsCodigo: TIntegerField;
    CkLocalObra: TCheckBox;
    CkLocalCntat: TCheckBox;
    CkNumOC: TCheckBox;
    CkEndCobra: TCheckBox;
    CkObs0: TCheckBox;
    CkObs1: TCheckBox;
    CkObs2: TCheckBox;
    CkHistImprime: TCheckBox;
    CkHistNaoImpr: TCheckBox;
    Label18: TLabel;
    EdECComprou: TdmkEditCB;
    CBECComprou: TdmkDBLookupComboBox;
    Label19: TLabel;
    EdECRetirou: TdmkEditCB;
    CBECRetirou: TdmkDBLookupComboBox;
    CGStatus: TdmkCheckGroup;
    frxLOC_PATRI_111_001: TfrxReport;
    frxDsPsqIts: TfrxDBDataset;
    BtImprime: TBitBtn;
    QrEndContratada: TMySQLQuery;
    QrEndContratadaTipo: TSmallintField;
    QrEndContratadaCONTRATADA: TWideStringField;
    QrEndContratadaNOMELOGRAD: TWideStringField;
    QrEndContratadaNOMERUA: TWideStringField;
    QrEndContratadaCOMPL: TWideStringField;
    QrEndContratadaBAIRRO: TWideStringField;
    QrEndContratadaNOMEUF: TWideStringField;
    QrEndContratadaCNPJ_CPF: TWideStringField;
    QrEndContratadaIE_RG: TWideStringField;
    QrEndContratadaCodigo: TIntegerField;
    QrEndContratadaTEL: TWideStringField;
    QrEndContratadaFAX: TWideStringField;
    QrEndContratadaCEP: TFloatField;
    QrEndContratadaTEL_TXT: TWideStringField;
    QrEndContratadaFAX_TXT: TWideStringField;
    QrEndContratadaCNPJ_TXT: TWideStringField;
    QrEndContratadaCEP_TXT: TWideStringField;
    QrEndContratadaNUMERO: TFloatField;
    QrEndContratadaNO_CIDADE: TWideStringField;
    QrEndContratadaNO_PAIS: TWideStringField;
    frxDsEndContratada: TfrxDBDataset;
    QrPsqItsDtHrBxa_TXT: TWideStringField;
    QrPsqItsDtHrLocado_TXT: TWideStringField;
    QrPsqItsSaiDtHr: TDateTimeField;
    frxDsEndContratante: TfrxDBDataset;
    QrEndContratante: TMySQLQuery;
    QrEndContratanteCodigo: TIntegerField;
    QrEndContratanteTipo: TSmallintField;
    QrEndContratanteNOMELOGRAD: TWideStringField;
    QrEndContratanteNOMERUA: TWideStringField;
    QrEndContratanteCOMPL: TWideStringField;
    QrEndContratanteBAIRRO: TWideStringField;
    QrEndContratanteNOMEUF: TWideStringField;
    QrEndContratanteCNPJ_CPF: TWideStringField;
    QrEndContratanteIE_RG: TWideStringField;
    QrEndContratanteTEL: TWideStringField;
    QrEndContratanteCEL: TWideStringField;
    QrEndContratanteFAX: TWideStringField;
    QrEndContratanteCEP: TFloatField;
    QrEndContratanteTEL_TXT: TWideStringField;
    QrEndContratanteCONTRATANTE: TWideStringField;
    QrEndContratanteFAX_TXT: TWideStringField;
    QrEndContratanteCNPJ_TXT: TWideStringField;
    QrEndContratanteCEP_TXT: TWideStringField;
    QrEndContratanteNUMERO: TFloatField;
    QrEndContratanteNO_CIDADE: TWideStringField;
    QrEndContratanteNO_PAIS: TWideStringField;
    QrEndContratanteCEL_TXT: TWideStringField;
    QrPsqItsNO_TipoAluguel: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGIts: TdmkDBGridZTO;
    DBGCab: TdmkDBGridZTO;
    QrPsqCab: TMySQLQuery;
    DsPsqCab: TDataSource;
    frxDsPsqCab: TfrxDBDataset;
    frxLOC_PATRI_111_002: TfrxReport;
    QrPsqCabCodigo: TIntegerField;
    QrPsqCabNO_STATUS: TWideStringField;
    QrPsqCabDtHrEmi: TDateTimeField;
    QrPsqCabDtHrBxa: TDateTimeField;
    QrPsqCabTipoAluguel: TWideStringField;
    QrPsqCabStatus: TIntegerField;
    QrPsqCabDtHrBxa_TXT: TWideStringField;
    QrPsqCabNO_CLI: TWideStringField;
    QrPsqCabNO_TipoAluguel: TWideStringField;
    QrPsqItsDtHrDevolver: TDateTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxLOC_PATRI_111_001GetValue(const VarName: string;
      var Value: Variant);
    procedure QrEndContratanteCalcFields(DataSet: TDataSet);
    procedure QrEndContratadaCalcFields(DataSet: TDataSet);
    procedure QrPsqItsAfterOpen(DataSet: TDataSet);
    procedure QrPsqItsBeforeClose(DataSet: TDataSet);
    procedure DBGItsDblClick(Sender: TObject);
    procedure DBGCabDblClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
    //
    procedure ReopenEndContratante();
    procedure ReopenEndContratada();
    procedure Pesquisa_Its();
    procedure Pesquisa_Cab();
    procedure Imprime_Its();
    procedure Imprime_Cab();
  public
    { Public declarations }
    FCodigo, FGraGruX: Integer;
  end;

  var
  FmLocCConPsq: TFmLocCConPsq;

implementation

uses UnMyObjects, Module, UnDmkProcFunc, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmLocCConPsq.BtImprimeClick(Sender: TObject);
const
  sProcName = 'TFmLocCConPsq.BtImprimeClick()';
begin
  case PageControl1.ActivePageIndex of
    0: Imprime_Its();
    1: Imprime_cab();
    else
      Geral.MB_Erro('"PageControl1.ActivePageIndex" n�o implementado em ' + sProcName);
  end;
end;

procedure TFmLocCConPsq.BtOKClick(Sender: TObject);
const
  sProcName = 'FmLocCConPsq.BtOKClick()';
begin
  case PageControl1.ActivePageIndex of
    0: Pesquisa_Its();
    1: Pesquisa_cab();
    else
      Geral.MB_Erro('"PageControl1.ActivePageIndex" n�o implementado em ' + sProcName);
  end;
end;

procedure TFmLocCConPsq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCConPsq.DBGCabDblClick(Sender: TObject);
begin
  if (QrPsqCab.State <> dsInactive) and (QrPsqCab.RecordCount > 0) then
  begin
    FCodigo  := QrPsqCabCodigo.Value;
    FGraGruX := 0;
    //
    Close;
  end;
end;

procedure TFmLocCConPsq.DBGItsDblClick(Sender: TObject);
begin
  if (QrPsqIts.State <> dsInactive) and (QrPsqIts.RecordCount > 0) then
  begin
    FCodigo  := QrPsqItsCodigo.Value;
    FGraGruX := QrPsqItsGraGruX.Value;
    //
    Close;
  end;
end;

procedure TFmLocCConPsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCConPsq.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  FCodigo  := 0;
  FGraGruX := 0;
  //
  PageControl1.ActivePageIndex := 0;
  //
  Agora := DModG.ObtemAgora();
  TPDataEmiIni.Date := Agora - 1826; // cinco anos
  TPDataEmiFim.Date := Agora; // cinco anos
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrECComprou, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrECRetirou, Dmod.MyDB);
  //
  CGStatus.Value := 3;
end;

procedure TFmLocCConPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCConPsq.frxLOC_PATRI_111_001GetValue(const VarName: string;
  var Value: Variant);
var
  CATipo, CETipo: Integer;
begin
  if VarName = 'LogoNFeExiste' then
    Value := FileExists(Dmod.QrControleLogoNF.Value)
  else
  if VarName = 'LogoNFePath' then
    Value := Dmod.QrControleLogoNF.Value
  else


  CATipo := QrEndContratadaTipo.Value;
  CETipo := QrEndContratanteTipo.Value;
  //
  if VarName = 'VARF_CAENDERECO' then
    Value := QrEndContratadaNOMELOGRAD.Value + ' ' +
     QrEndContratadaNOMERUA.Value + ', N� ' +
     Geral.FFT(QrEndContratadaNUMERO.Value, 0, siPositivo) + ' ' +
     QrEndContratadaCOMPL.Value
  else
  if VarName = 'VARF_CAIERG' then
  begin
    case CATipo of
    0:
      if QrEndContratadaIE_RG.Value = '' then
        Value := 'Isento'
      else
        Value := QrEndContratadaIE_RG.Value;
    1:
      if QrEndContratadaIE_RG.Value = '' then
        Value := ''
      else
        Value := QrEndContratadaIE_RG.Value;
    end;
  end
  else
  if VarName = 'VARF_CALACNPJCFP' then
  begin
    if CATipo = 0 then
      Value := 'CNPJ'
    else
      Value := 'CPF';
  end
  else
  if VarName = 'VARF_CALAIERG' then
  begin
    if CATipo = 0 then
      Value := 'IE'
    else
      Value := 'RG';
  end
  else
  //
  if VarName = 'VARF_CEENDERECO' then
    Value := QrEndContratanteNOMELOGRAD.Value + ' ' +
     QrEndContratanteNOMERUA.Value + ', N� ' +
     FormatFloat('0', QrEndContratanteNUMERO.Value) + ' ' +
     QrEndContratanteCOMPL.Value
  else
  if VarName = 'VARF_CEIERG' then
  begin
    case CETipo of
    0:
      if QrEndContratanteIE_RG.Value = '' then
        Value := 'Isento'
      else
        Value := QrEndContratanteIE_RG.Value;
    1:
      if QrEndContratanteIE_RG.Value = '' then
        Value := ''
      else
        Value := QrEndContratanteIE_RG.Value;
    end;
  end else
  if VarName = 'VARF_CELACNPJCFP' then
  begin
    if CETipo = 0 then
      Value := 'CNPJ'
    else
      Value := 'CPF';
  end else
  if VarName = 'VARF_CELAIERG' then
  begin
    if CETipo = 0 then
      Value := 'IE'
    else
      Value := 'RG';
  end else
end;

procedure TFmLocCConPsq.Imprime_Cab;
begin
  if QrPsqCab.State <> dsInactive then
  begin
    ReopenEndContratada();
    ReopenEndContratante();
    MyObjects.frxDefineDatasets(frxLOC_PATRI_111_002, [
    //DModG.frxDsDono,
    frxDsPsqCab,
    frxDsEndContratada,
    frxDsEndContratante
    ]);
    //
    QrPsqCab.DisableControls;
    try
      MyObjects.frxMostra(frxLOC_PATRI_111_002, 'Relat�rio de cabe�alhos de Loca��es');
    finally
      QrPsqCab.EnableControls;
    end;
  end else
    Geral.MB_Aviso('N�o h� pesquisa a ser impressa!');
end;

procedure TFmLocCConPsq.Imprime_Its;
begin
  if QrPsqIts.State <> dsInactive then
  begin
    ReopenEndContratada();
    ReopenEndContratante();
    MyObjects.frxDefineDatasets(frxLOC_PATRI_111_001, [
    //DModG.frxDsDono,
    frxDsPsqIts,
    frxDsEndContratada,
    frxDsEndContratante
    ]);
    //
    QrPsqIts.DisableControls;
    try
      MyObjects.frxMostra(frxLOC_PATRI_111_001, 'Relat�rio de Loca��es por itens');
    finally
      QrPsqIts.EnableControls;
    end;
  end else
    Geral.MB_Aviso('N�o h� pesquisa a ser impressa!');
end;

procedure TFmLocCConPsq.PageControl1Change(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := PageControl1.ActivePageIndex = 0;
  //
  CkGGY_1024.Enabled := Habilita;
  CkGGY_2048.Enabled := Habilita;
  CkGGY_3072.Enabled := Habilita;
  CkGGY_4096.Enabled := Habilita;
  CkGGY_5120.Enabled := Habilita;
  CkGGY_6144.Enabled := Habilita;
  CkGGY_7168.Enabled := Habilita;
  CkGGY_8192.Enabled := Habilita;
  EdParteProd.Enabled := Habilita;
end;

procedure TFmLocCConPsq.Pesquisa_Cab;
var
  Cliente: Integer;
  //ParteProd, SQL_ParteProd, SQL_GraGruYs, Virgula,
  Union, SQL_Periodo, SQL_Cliente, ParteTexto,
  SQL_ParteTexto, SQL_GraGru1, SQL_Status: String;
  //TemGraGXPatr, TemGraGXOutr, TemGraGXServ, TemGraGXVend: Boolean;
  //
  (*
  procedure DefimneMaisUmGGY(Ck: TCheckBox; GGY: Integer);
  begin
    if Ck.Checked then
    begin
      SQL_GraGruYs := SQL_GraGruYs + Virgula + Geral.FF0(GGY);
      Virgula      := ', ';
      case GGY of
        1024,
        2048,
        3072: TemGraGXPatr  := True;
        4096,
        5120,
        6144: TemGraGXOutr  := True;
        7168: TemGraGXServ  := True;
        8192: TemGraGXVend  := True;
      end;
    end;
  end;
  *)
  procedure PT(Ck: TCheckBox; sCampo: String);
  begin
    if Ck.Checked then
      SQL_ParteTexto := SQL_ParteTexto + ' OR ' + sCampo + ' LIKE "%' + ParteTexto + '%"';
  end;
  function ParteSQL(Tabela: String): String;
  begin
    Result := Geral.ATS([
    Union,
    'SELECT cab.Codigo,   ',
    'ELT(cab.Status + 1, "N�O DEFINIDO", "ABERTO",  ',
    '"LOCADO", "FINALIZADO", "CANCELADO", "SUSPENSO",  ',
    '"N�O IMPLEMENTADO") NO_STATUS,  ',
    'cab.DtHrEmi, cab.DtHrBxa, cab.TipoAluguel, cab.Status,   ',
    'IF(cab.DtHrBxa < "1900-01-010", "", DATE_FORMAT(cab.DtHrBxa, "%d/%m/%Y %H:%i")) DtHrBxa_TXT,  ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI,  ',
    'CASE cab.TipoAluguel ',
    '  WHEN "D" THEN "DIARIA" ',
    '  WHEN "S" THEN "SEMANAL" ',
    '  WHEN "Q" THEN "QUINZENAL" ',
    '  WHEN "M" THEN "MENSAL" ',
    'ELSE "INDEFINIDO" END NO_TipoAluguel  ',
    'FROM loccconcab cab ',
    'LEFT JOIN entidades cli ON cli.Codigo=cab.Cliente ',
    SQL_Periodo,
    //SQL_GraGruYs,
    SQL_CLiente,
    SQL_ParteTexto,
    //SQL_ParteProd,
    SQL_Status,
    //'ORDER BY cab.DtHrEmi DESC, NO_PRD_TAM_COR']);
    '']);
    //
    Union := 'UNION' + sLineBreak;
  end;
var
  SQL: String;
begin
  if MyObjects.FIC(CGStatus.Value = 0, CGStatus,
    'Define pelo menos um status!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    //SQL_GraGruYs   := EmptyStr;
    SQL_Cliente    := EmptyStr;
    SQL_ParteTexto := EmptyStr;
    //SQL_ParteProd  := EmptyStr;
    //
(*
    Virgula       := EmptyStr;
    TemGraGXPatr  := False;
    TemGraGXOutr  := False;
    TemGraGXServ  := False;
    TemGraGXVend  := False;
    DefimneMaisUmGGY(CkGGY_1024, 1024);
    DefimneMaisUmGGY(CkGGY_2048, 2048);
    DefimneMaisUmGGY(CkGGY_3072, 3072);
    DefimneMaisUmGGY(CkGGY_4096, 4096);
    DefimneMaisUmGGY(CkGGY_5120, 5120);
    DefimneMaisUmGGY(CkGGY_6144, 6144);
    DefimneMaisUmGGY(CkGGY_7168, 7168);
    DefimneMaisUmGGY(CkGGY_8192, 8192);
    if MyObjects.FIC(SQL_GraGruYs = EmptyStr, nil,
      'Defina pelo menos um tipo de cadastro de produto!') then Exit;
    SQL_GraGruYs := 'AND ggx.GraGruY IN (' + SQL_GraGruYs + ')';
*)
    //
    Cliente := EdCliente.ValueVariant;
    if Cliente <> 0 then
      SQL_Cliente := 'AND cab.Cliente=' + Geral.FF0(Cliente);
    //
    ParteTexto := Trim(EdParteTexto.Text);
    if ParteTexto <> EmptyStr then
    begin
      PT(CkLocalObra, 'cab.LocalObra');
      PT(CkLocalCntat, 'cab.LocalCntat');
      PT(CkNumOC, 'cab.NumOC');
      PT(CkEndCobra, 'cab.EndCobra');
      PT(CkObs0, 'cab.Obs0');
      PT(CkObs1, 'cab.Obs1');
      PT(CkObs2, 'cab.Obs2');
      PT(CkHistImprime, 'cab.HistImprime');
      PT(CkHistNaoImpr, 'cab.HistNaoImpr');
    end;
    if ParteTexto <> EmptyStr then
      SQL_ParteTexto := 'AND (' + Copy(SQL_ParteTexto, 5) + ')';
    //
(*
    ParteProd := EdParteProd.Text;
    if ParteProd <> EmptyStr then
    begin
      SQL_ParteProd := 'AND (gg1.Nome LIKE "%' + ParteProd + '%" ' + sLineBreak +
      'OR gg1.Patrimonio LIKE "%' + ParteProd + '%" ' + sLineBreak +
      'OR gg1.Referencia LIKE "%' + ParteProd + '%")';
    end;
*)
    //
    SQL_Status := '';
    if CGStatus.Checked[0] then
      SQL_Status := SQL_Status + ',1';
    if CGStatus.Checked[1] then
      SQL_Status := SQL_Status + ',2';
    if CGStatus.Checked[2] then
      SQL_Status := SQL_Status + ',3';
    if CGStatus.Checked[3] then
      SQL_Status := SQL_Status + ',4';
    //
    SQL_Status := Copy(SQL_Status, 2);
    if SQL_Status <> EmptyStr then
      SQL_Status := 'AND cab.Status IN (' + SQL_Status + ')';
    //
    //...
    //
    SQL_Periodo := dmkPF.SQL_Periodo('WHERE cab.DtHrEmi' ,
    TPDataEmiIni.Date, TPDataEmiFim.Date, True, True);
    Union := EmptyStr;
    SQL   := EmptyStr;
    //if TemGraGXPatr then
      SQL := SQL + ParteSQL('loccitslca');
    //if TemGraGXOutr then
      SQL := SQL + ParteSQL('loccitsruc');
    //if TemGraGXServ then
      SQL := SQL + ParteSQL('loccitssvc');
    //if TemGraGXVend then
      SQL := SQL + ParteSQL('loccitsven');
    //
    SQL := SQL + sLineBreak + sLineBreak +
      'ORDER BY DtHrEmi DESC, Codigo DESC';
    //
    QrPsqCab.SQL.Text := SQL;
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.AbreQuery(QrPsqCab, Dmod.MyDB);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocCConPsq.Pesquisa_Its;
var
  Cliente: Integer;
  SQL_GraGruYs, Virgula, Union, SQL_Periodo, SQL_Cliente, ParteTexto, ParteProd,
  SQL_ParteTexto, SQL_ParteProd, SQL_GraGru1, SQL_Status: String;
  TemGraGXPatr, TemGraGXOutr, TemGraGXServ, TemGraGXVend: Boolean;
  //
  procedure DefimneMaisUmGGY(Ck: TCheckBox; GGY: Integer);
  begin
    if Ck.Checked then
    begin
      SQL_GraGruYs := SQL_GraGruYs + Virgula + Geral.FF0(GGY);
      Virgula      := ', ';
      case GGY of
        1024,
        2048,
        3072: TemGraGXPatr  := True;
        4096,
        5120,
        6144: TemGraGXOutr  := True;
        7168: TemGraGXServ  := True;
        8192: TemGraGXVend  := True;
      end;
    end;
  end;
  procedure PT(Ck: TCheckBox; sCampo: String);
  begin
    if Ck.Checked then
      SQL_ParteTexto := SQL_ParteTexto + ' OR ' + sCampo + ' LIKE "%' + ParteTexto + '%"';
  end;
  function ParteSQL(Tabela, DtHrLocado, DtHrRetorn: String): String;
  begin
    Result := Geral.ATS([
    Union,
    'SELECT cab.Codigo, ' + DtHrLocado + ' DtHrLocado_TXT, cab.DtHrDevolver, ',
    'lca.SaiDtHr, ELT(cab.Status + 1, "N�O DEFINIDO", "ABERTO", ',
    '"LOCADO", "FINALIZADO", "CANCELADO", "SUSPENSO", ',
    '"N�O IMPLEMENTADO") NO_STATUS, ',
    'CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR,  ',
    'cab.DtHrEmi, cab.DtHrBxa, cab.TipoAluguel, cab.Status,  ',

    'IF(cab.DtHrBxa < "1900-01-010", "", DATE_FORMAT(cab.DtHrBxa, "%d/%m/%Y %H:%i")) DtHrBxa_TXT, ',

    'lca.GraGruX, ' + DtHrRetorn + ' DtHrRetorn_TXT, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, ',
    'gg1.Referencia, gg1.Patrimonio, ',
    'CASE cab.TipoAluguel',
    '  WHEN "D" THEN "DIARIA"',
    '  WHEN "S" THEN "SEMANAL"',
    '  WHEN "Q" THEN "QUINZENAL"',
    '  WHEN "M" THEN "MENSAL"',
    'ELSE "INDEFINIDO" END NO_TipoAluguel ',
    'FROM ' + Tabela + ' lca ',
    'LEFT JOIN loccconcab cab ON cab.Codigo=lca.Codigo ',
    'LEFT JOIN entidades cli ON cli.Codigo=cab.Cliente ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=lca.GraGruX  ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
    SQL_Periodo,
    SQL_GraGruYs,
    SQL_CLiente,
    SQL_ParteTexto,
    SQL_ParteProd,
    SQL_Status,
    //'ORDER BY cab.DtHrEmi DESC, NO_PRD_TAM_COR']);
    '']);
    //
    Union := 'UNION' + sLineBreak;
  end;
var
  SQL: String;
begin
  if MyObjects.FIC(CGStatus.Value = 0, CGStatus,
    'Define pelo menos um status!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    SQL_GraGruYs   := EmptyStr;
    SQL_Cliente    := EmptyStr;
    SQL_ParteTexto := EmptyStr;
    SQL_ParteProd  := EmptyStr;
    //
    Virgula       := EmptyStr;
    TemGraGXPatr  := False;
    TemGraGXOutr  := False;
    TemGraGXServ  := False;
    TemGraGXVend  := False;
    DefimneMaisUmGGY(CkGGY_1024, 1024);
    DefimneMaisUmGGY(CkGGY_2048, 2048);
    DefimneMaisUmGGY(CkGGY_3072, 3072);
    DefimneMaisUmGGY(CkGGY_4096, 4096);
    DefimneMaisUmGGY(CkGGY_5120, 5120);
    DefimneMaisUmGGY(CkGGY_6144, 6144);
    DefimneMaisUmGGY(CkGGY_7168, 7168);
    DefimneMaisUmGGY(CkGGY_8192, 8192);
    if MyObjects.FIC(SQL_GraGruYs = EmptyStr, nil,
      'Defina pelo menos um tipo de cadastro de produto!') then Exit;
    SQL_GraGruYs := 'AND ggx.GraGruY IN (' + SQL_GraGruYs + ')';
    //
    Cliente := EdCliente.ValueVariant;
    if Cliente <> 0 then
      SQL_Cliente := 'AND cab.Cliente=' + Geral.FF0(Cliente);
    //
    ParteTexto := Trim(EdParteTexto.Text);
    if ParteTexto <> EmptyStr then
    begin
      PT(CkLocalObra, 'cab.LocalObra');
      PT(CkLocalCntat, 'cab.LocalCntat');
      PT(CkNumOC, 'cab.NumOC');
      PT(CkEndCobra, 'cab.EndCobra');
      PT(CkObs0, 'cab.Obs0');
      PT(CkObs1, 'cab.Obs1');
      PT(CkObs2, 'cab.Obs2');
      PT(CkHistImprime, 'cab.HistImprime');
      PT(CkHistNaoImpr, 'cab.HistNaoImpr');
    end;
    if ParteTexto <> EmptyStr then
      SQL_ParteTexto := 'AND (' + Copy(SQL_ParteTexto, 5) + ')';
    //
    ParteProd := EdParteProd.Text;
    if ParteProd <> EmptyStr then
    begin
      SQL_ParteProd := 'AND (gg1.Nome LIKE "%' + ParteProd + '%" ' + sLineBreak +
      'OR gg1.Patrimonio LIKE "%' + ParteProd + '%" ' + sLineBreak +
      'OR gg1.Referencia LIKE "%' + ParteProd + '%")';
    end;
    //
    SQL_Status := '';
    if CGStatus.Checked[0] then
      SQL_Status := SQL_Status + ',1';
    if CGStatus.Checked[1] then
      SQL_Status := SQL_Status + ',2';
    if CGStatus.Checked[2] then
      SQL_Status := SQL_Status + ',3';
    if CGStatus.Checked[3] then
      SQL_Status := SQL_Status + ',4';
    //
    SQL_Status := Copy(SQL_Status, 2);
    if SQL_Status <> EmptyStr then
      SQL_Status := 'AND cab.Status IN (' + SQL_Status + ')';
    //
    //...
    //
    SQL_Periodo := dmkPF.SQL_Periodo('WHERE cab.DtHrEmi' ,
    TPDataEmiIni.Date, TPDataEmiFim.Date, True, True);
    Union := EmptyStr;
    SQL   := EmptyStr;
    if TemGraGXPatr then
      SQL := SQL + ParteSQL('loccitslca', 'DATE_FORMAT(lca.DtHrLocado, "%d/%m/%Y %H:%i")',
      'IF(lca.DtHrRetorn < "1900-01-010", "", DATE_FORMAT(lca.DtHrRetorn, "%d/%m/%Y %H:%i"))');
    if TemGraGXOutr then
      SQL := SQL + ParteSQL('loccitsruc', '"" ', '"" ');
    if TemGraGXServ then
      SQL := SQL + ParteSQL('loccitssvc', '"" ', '"" ');
    if TemGraGXVend then
      SQL := SQL + ParteSQL('loccitsven', '"" ', '"" ');
    //
    SQL := SQL + sLineBreak + sLineBreak +
      'ORDER BY DtHrEmi DESC, NO_PRD_TAM_COR';
    //
    QrPsqIts.SQL.Text := SQL;
    //Geral.MB_Teste(SQL);
    UnDmkDAC_PF.AbreQuery(QrPsqIts, Dmod.MyDB);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocCConPsq.QrEndContratadaCalcFields(DataSet: TDataSet);
begin
  QrEndContratadaTEL_TXT.Value  := Geral.FormataTelefone_TT(QrEndContratadaTEL.Value);
  QrEndContratadaFAX_TXT.Value  := Geral.FormataTelefone_TT(QrEndContratadaFAX.Value);
  QrEndContratadaCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEndContratadaCNPJ_CPF.Value);
  QrEndContratadaCEP_TXT.Value := Geral.FormataCEP_NT(QrEndContratadaCEP.Value);
end;

procedure TFmLocCConPsq.QrEndContratanteCalcFields(DataSet: TDataSet);
begin
  QrEndContratanteTEL_TXT.Value := Geral.FormataTelefone_TT(QrEndContratanteTEL.Value);
  QrEndContratanteCEL_TXT.Value := Geral.FormataTelefone_TT(QrEndContratanteCEL.Value);
  QrEndContratanteFAX_TXT.Value := Geral.FormataTelefone_TT(QrEndContratanteFAX.Value);
  QrEndContratanteCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEndContratanteCNPJ_CPF.Value);
  QrEndContratanteCEP_TXT.Value := Geral.FormataCEP_NT(QrEndContratanteCEP.Value);
end;

procedure TFmLocCConPsq.QrPsqItsAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := True;
end;

procedure TFmLocCConPsq.QrPsqItsBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmLocCConPsq.ReopenEndContratada();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEndContratada, Dmod.MyDB, [
  'SELECT mun.Nome NO_CIDADE, pai.Nome NO_PAIS, ent.Codigo, ent.Tipo,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATADA,',
  'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,',
  'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,',
  'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,',
  'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,',
  'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,',
  '/*IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,*/',
  'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,',
  'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,',
  'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,',
  'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,',
  'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX,',
  'IF(ent.Tipo=0, ent.ECep, ent.PCEP) + 0.000 CEP',
  'FROM entidades ent',
  'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF',
  'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF',
  'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd',
  'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici)',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais)',
  'WHERE ent.Codigo=' + Geral.FF0(VAR_LIB_EMPRESA_SEL),
  'ORDER BY CONTRATADA',
  '']);
end;

procedure TFmLocCConPsq.ReopenEndContratante();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEndContratante, Dmod.MyDB, [
  'SELECT mun.Nome NO_CIDADE, pai.Nome NO_PAIS, ent.Codigo, ent.Tipo,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATANTE,',
  'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,',
  'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,',
  'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,',
  'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,',
  'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,',
  '/*IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,*/',
  'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,',
  'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,',
  'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,',
  'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,',
  'IF(ent.Tipo=0, ent.ECel, ent.Pcel) CEL, ',
  'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX,',
  'IF(ent.Tipo=0, ent.ECep, ent.PCEP) + 0.000 CEP',
  'FROM entidades ent',
  'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF',
  'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF',
  'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd',
  'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici)',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais)',
  'WHERE ent.Codigo=' + Geral.FF0(EdCliente.ValueVariant),
  'ORDER BY CONTRATANTE',
  '']);
end;

end.
