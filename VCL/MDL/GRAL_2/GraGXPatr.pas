unit GraGXPatr;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, Module, Grids, DBGrids,
  ComCtrls, dmkEditDateTimePicker, dmkMemo, Menus, UnDmkEnums, dmkValUsu,
  AppListas, dmkCheckBox, UnAppEnums, dmkCompoStore;

type
  TFmGraGXPatr = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtIts: TBitBtn;
    BtCab: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrGraGXPatr: TmySQLQuery;
    DsGraGXPatr: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrMarcas: TmySQLQuery;
    DsMarcas: TDataSource;
    QrMarcasControle: TIntegerField;
    QrMarcasNO_MARCA_FABR: TWideStringField;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    QrGraGXPatrGraGruX: TIntegerField;
    QrGraGXPatrNO_GG1: TWideStringField;
    QrGraGXPatrNO_MARCA: TWideStringField;
    QrGraGXPatrNO_FABR: TWideStringField;
    QrGraGXPatrComplem: TWideStringField;
    QrGraGXPatrAquisData: TDateField;
    QrGraGXPatrAquisDocu: TWideStringField;
    QrGraGXPatrAquisValr: TFloatField;
    QrGraGXPatrSituacao: TWordField;
    QrGraGXPatrAtualValr: TFloatField;
    QrGraGXPatrValorMes: TFloatField;
    QrGraGXPatrValorQui: TFloatField;
    QrGraGXPatrValorSem: TFloatField;
    QrGraGXPatrValorDia: TFloatField;
    QrGraGXPatrAgrupador: TIntegerField;
    QrGraGXPatrModelo: TWideStringField;
    QrGraGXPatrSerie: TWideStringField;
    QrGraGXPatrVoltagem: TWideStringField;
    QrGraGXPatrPotencia: TWideStringField;
    QrGraGXPatrCapacid: TWideStringField;
    QrGraGXPatrVendaData: TDateField;
    QrGraGXPatrVendaDocu: TWideStringField;
    QrGraGXPatrVendaValr: TFloatField;
    QrGraGXPatrVendaEnti: TIntegerField;
    QrGraGXPatrObserva: TWideStringField;
    QrGraGXPatrAGRPAT: TWideStringField;
    QrGraGXPatrCLVPAT: TWideStringField;
    Panel6: TPanel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    Panel9: TPanel;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label5: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label12: TLabel;
    GroupBox4: TGroupBox;
    Panel11: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit22: TDBEdit;
    Label27: TLabel;
    GroupBox2: TGroupBox;
    Panel8: TPanel;
    Label17: TLabel;
    Label16: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label13: TLabel;
    DBEdit8: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit9: TDBEdit;
    GroupBox3: TGroupBox;
    Panel10: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    QrGraGXPatrMarca: TIntegerField;
    QrGraGXPatrVENDADATA_TXT: TWideStringField;
    QrGraGXPatrSIT_CHR: TWideStringField;
    QrGraGXPatrMARPAT: TWideStringField;
    DBEdit19: TDBEdit;
    Label25: TLabel;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    Label7: TLabel;
    EdGraGruX: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    EdMarca: TdmkEditCB;
    CBMarca: TdmkDBLookupComboBox;
    SbMarcas: TSpeedButton;
    Label38: TLabel;
    EdComplem: TdmkEdit;
    Panel12: TPanel;
    GroupBox5: TGroupBox;
    Panel13: TPanel;
    Label26: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    TPAquisData: TdmkEditDateTimePicker;
    EdAquisDocu: TdmkEdit;
    EdAquisValr: TdmkEdit;
    Panel14: TPanel;
    GroupBox6: TGroupBox;
    Panel15: TPanel;
    Label30: TLabel;
    Label31: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    GroupBox7: TGroupBox;
    Panel16: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    EdAtualValr: TdmkEdit;
    EdValorMes: TdmkEdit;
    EdValorQui: TdmkEdit;
    EdValorSem: TdmkEdit;
    EdValorDia: TdmkEdit;
    EdModelo: TdmkEdit;
    EdSerie: TdmkEdit;
    EdVoltagem: TdmkEdit;
    EdPotencia: TdmkEdit;
    EdCapacid: TdmkEdit;
    QrAgrupador: TmySQLQuery;
    QrAgrupadorControle: TIntegerField;
    QrAgrupadorNome: TWideStringField;
    DsAgrupador: TDataSource;
    QrGraGXPatrControle: TIntegerField;
    QrGraGXPatrCOD_GG1: TIntegerField;
    DBEdit20: TDBEdit;
    Label39: TLabel;
    EdReferencia: TdmkEdit;
    QrGraGXPatrReferencia: TWideStringField;
    Label46: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Panel17: TPanel;
    Panel18: TPanel;
    DBMemo1: TDBMemo;
    Panel19: TPanel;
    Panel20: TPanel;
    MeObserva: TdmkMemo;
    RGAplicacao: TdmkRadioGroup;
    DBRGAplicacao: TDBRadioGroup;
    QrGraGXPatrAplicacao: TIntegerField;
    QrGraGXPatrCPL_EXISTE: TFloatField;
    Label24: TLabel;
    EdSIT_CHR: TdmkEdit;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    ItsExclui1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    QrGraGXPIts: TmySQLQuery;
    DsGraGXPIts: TDataSource;
    QrGraGXPItsGraGruX: TIntegerField;
    QrGraGXPItsAplicacao: TIntegerField;
    QrGraGXPItsItemValr: TFloatField;
    QrGraGXPItsReferencia: TWideStringField;
    QrGraGXPItsCOD_GG1: TIntegerField;
    QrGraGXPItsNO_GG1: TWideStringField;
    QrGraGXPItsNO_APLICACAO: TWideStringField;
    QrGraGXPItsConta: TIntegerField;
    QrGraGXPItsGraGXPatr: TIntegerField;
    QrGraGXPItsGraGXOutr: TIntegerField;
    Label45: TLabel;
    EdAgrupador: TdmkEditCB;
    CBAgrupador: TdmkDBLookupComboBox;
    Label18: TLabel;
    DBEdit13: TDBEdit;
    DBEdit21: TDBEdit;
    QrAgrup: TmySQLQuery;
    DsAgrup: TDataSource;
    QrAgrupControle: TIntegerField;
    QrAgrupNome: TWideStringField;
    DBEdit23: TDBEdit;
    QrAgrupReferencia: TWideStringField;
    QrLocod: TmySQLQuery;
    SBSituacao: TSpeedButton;
    EdCBSituacao: TdmkEditCB;
    CBSituacao: TdmkDBLookupComboBox;
    QrGraGLSitu: TmySQLQuery;
    DsGraGLSitu: TDataSource;
    QrGraGLSituCodigo: TIntegerField;
    QrGraGLSituNome: TWideStringField;
    QrGraGLSituSigla: TWideStringField;
    QrAgrupadorReferencia: TWideStringField;
    QrGraGXPatrNO_GG2: TWideStringField;
    QrGraGXPatrNO_GG3: TWideStringField;
    QrGraGXPatrNO_GG4: TWideStringField;
    QrGraGXPatrNO_GG5: TWideStringField;
    GroupBox8: TGroupBox;
    DBEdit24: TDBEdit;
    DBText2: TDBText;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit30: TDBEdit;
    DBText3: TDBText;
    DBText4: TDBText;
    DBEdit31: TDBEdit;
    DBText5: TDBText;
    QrGraGXPatrNO_GGT: TWideStringField;
    Label47: TLabel;
    QrGraGXPatrTitNiv1: TWideStringField;
    QrGraGXPatrTitNiv2: TWideStringField;
    QrGraGXPatrTitNiv3: TWideStringField;
    QrGraGXPatrTitNiv4: TWideStringField;
    QrGraGXPatrTitNiv5: TWideStringField;
    QrGraGXPatrPrdGrupTip: TIntegerField;
    QrGraGXPatrNivel1: TIntegerField;
    QrGraGXPatrNivel2: TIntegerField;
    QrGraGXPatrNivel3: TIntegerField;
    QrGraGXPatrNivel4: TIntegerField;
    QrGraGXPatrNivel5: TIntegerField;
    QrGraGXPatrCodUsu: TIntegerField;
    N1: TMenuItem;
    Editaniveis1: TMenuItem;
    Editadadosdoproduto1: TMenuItem;
    QrAgrupadorNOMEREF_TXT: TWideStringField;
    BtConserto: TBitBtn;
    EdAgrupRef: TdmkEdit;
    QrPesq2: TmySQLQuery;
    QrPesq2Referencia: TWideStringField;
    QrPesq1: TmySQLQuery;
    QrPesq1Controle: TIntegerField;
    GroupBox9: TGroupBox;
    Panel21: TPanel;
    Label48: TLabel;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    Label49: TLabel;
    EdNCM: TdmkEdit;
    SpeedButton6: TSpeedButton;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    VUUnidMed: TdmkValUsu;
    QrGraGXPatrNCM: TWideStringField;
    QrGraGXPatrUnidMed: TIntegerField;
    QrGraGXPatrCUNivel5: TIntegerField;
    QrGraGXPatrCUNivel4: TIntegerField;
    QrGraGXPatrCUNivel3: TIntegerField;
    QrGraGXPatrCUNivel2: TIntegerField;
    QrGraGXPatrAtivo: TSmallintField;
    CkAtivo: TdmkCheckBox;
    DBCkAtivo: TDBCheckBox;
    QrGraGXPItsQuantidade: TIntegerField;
    Label50: TLabel;
    EdPatrimonio: TdmkEdit;
    QrGraGXPatrPatrimonio: TWideStringField;
    dmkDBEdit2: TdmkDBEdit;
    Label51: TLabel;
    QrGraGXPatrEstqSdo: TFloatField;
    Label52: TLabel;
    DBEdit32: TDBEdit;
    Label53: TLabel;
    DBEdit33: TDBEdit;
    N2: TMenuItem;
    Recalculaestoque1: TMenuItem;
    BtPanSit: TBitBtn;
    CSTabSheetChamou: TdmkCompoStore;
    QrGraGXPatrValorFDS: TFloatField;
    Label54: TLabel;
    DBEdit34: TDBEdit;
    EdValorFDS: TdmkEdit;
    Label55: TLabel;
    QrGraGXPatrDMenos: TIntegerField;
    DBEdit35: TDBEdit;
    Label56: TLabel;
    Label57: TLabel;
    EdDMenos: TdmkEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraGXPatrAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGXPatrBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbMarcasClick(Sender: TObject);
    procedure QrGraGXPatrCalcFields(DataSet: TDataSet);
    procedure EdSIT_CHRChange(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure QrGraGXPatrBeforeClose(DataSet: TDataSet);
    procedure QrGraGXPatrAfterScroll(DataSet: TDataSet);
    procedure ItsExclui1Click(Sender: TObject);
    procedure SBSituacaoClick(Sender: TObject);
    procedure EdCBSituacaoChange(Sender: TObject);
    procedure CBAgrupadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAgrupadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Editaniveis1Click(Sender: TObject);
    procedure Editadadosdoproduto1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtConsertoClick(Sender: TObject);
    procedure EdAgrupadorChange(Sender: TObject);
    procedure EdAgrupRefChange(Sender: TObject);
    procedure EdAgrupRefKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Recalculaestoque1Click(Sender: TObject);
    procedure BtPanSitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FGraToolRent: TGraToolRent;
    FTabNome,
    FFrmNome: String;
    FEmpresa: Integer;
    procedure FiltraItensAExibir(Material: TGraToolRent);
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure DefineSituacao(Sit: Variant; VT: TVarType);
    procedure MostraCadastroGraGXPIts(SQLType: TSQLType);

    procedure ReopenGraGXPIts(Conta: Integer);
    procedure ReopenAgrup();
    //
    procedure PesquisaNomePatrimonio();
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorNome(Key: Integer);
    procedure PesquisaPorReferencia(Limpa: Boolean);
    //
    (*
    procedure IncluiProduto();
    procedure AlteraProduto();
    *)
    procedure MostraFormPrdGruNew(SQLTipo: TSQLType; Nivel5, Nivel4, Nivel3,
              Nivel2, Nivel1: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmGraGXPatr: TFmGraGXPatr;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, DmkDAC_PF, Principal, MyDBCheck, GraGru1, GraGXPIts, Curinga,
  ModuleGeral, UnGrade_Jan, FixGerePsq, UnidMed, ClasFisc, ModProd, GraGruX,
  MeuDBUses, UnAppPF, UnGraL_Jan, MyGlyfs;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraGXPatr.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraGXPatr.MostraCadastroGraGXPIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmGraGXPIts, FmGraGXPIts, afmoNegarComAviso) then
  begin
    FmGraGXPIts.ImgTipo.SQLType := SQLType;
    FmGraGXPIts.FQrCab := QrGraGXPatr;
    FmGraGXPIts.FDsCab := DsGraGXPatr;
    FmGraGXPIts.FQrIts := QrGraGXPIts;
    FmGraGXPIts.FGraGXPatr := QrGraGXPatrGraGruX.Value;
    if SQLType = stIns then
      //?
    else
    begin
      FmGraGXPIts.EdConta.ValueVariant := QrGraGXPItsConta.Value;
      FmGraGXPIts.EdGraGXOutr.ValueVariant := QrGraGXPItsGraGXOutr.Value;
      FmGraGXPIts.CBGraGXOutr.KeyValue := QrGraGXPItsGraGXOutr.Value;
      FmGraGXPIts.EdQuantidade.ValueVariant := QrGraGXPItsQuantidade.Value;
    end;
    FmGraGXPIts.ShowModal;
    FmGraGXPIts.Destroy;
  end;
end;

procedure TFmGraGXPatr.PesquisaNomePatrimonio();
var
  Codigo: Integer;
  Filtro: String;
  SQL: String;
begin
    // Sem Filtro: T i s o l i n
  if Dmod.QrOpcoesTRenGraNivOutr.Value = 7 then
  begin
(*
    Codigo := CuringaLoc.CriaForm('ggx.Controle', 'gg1.Nome',
      'gragrux ggx', Dmod.MyDB, 'AND ' + Filtro + ' AND gg1.Nivel1>0',
      False,
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle '+ sLineBreak +
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '+ sLineBreak +
      'LEFT JOIN gragruy ggy ON ggy.COdigo=ggx.GraGruY');
*)
    SQL := Geral.ATS([
    'SELECT lpc.GraGruX _CODIGO, gg1.REFERENCIA, gg1.Nome _NOME ',
    'FROM gragxpatr lpc ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE gg1.Nome LIKE "%' + CO_JOKE_SQL + '%" ',
    'OR gg1.Referencia LIKE "%' + CO_JOKE_SQL + '%" ',
    'OR gg1.Patrimonio LIKE "%' + CO_JOKE_SQL + '%" ',
    '']);
    FmMeuDBUses.PesquisaNomeSQLeJoke(SQL, CO_JOKE_SQL);
    if VAR_CADASTRO <> 0 then
      LocCod(QrGraGXPatrControle.Value, VAR_CADASTRO);
  end else
    // Com Filtro: AAPA
  begin
    Dmod.FiltroGrade(FGraToolRent, Filtro);
    //
    Codigo := CuringaLoc.CriaForm('ggx.Controle', 'gg1.Nome',
      'gragrux ggx', Dmod.MyDB, 'AND ' + Filtro + ' AND gg1.Nivel1>0',
      False, 'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
    //
    LocCod(QrGraGXPatrControle.Value, Codigo);
  end;
end;

procedure TFmGraGXPatr.PesquisaPorGraGruX;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
    'SELECT ggx.Controle, gg1.Nome, gg1.Referencia, ',
    'CONCAT(gg1.Referencia, " - ", gg1.Nome) NOMEREF_TXT ',
    'FROM gragxpatr gxp ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=gxp.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE gxp.GraGrux=' + Geral.FF0(EdAgrupador.ValueVariant),
    'AND NOT (gxp.GraGruX IS NULL) ',
    '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdAgrupRef.ValueVariant <> QrPesq2Referencia.Value then
    begin
      EdAgrupRef.ValueVariant := QrPesq2Referencia.Value;
      ReopenAgrup();
    end;
  end else EdAgrupRef.ValueVariant := '';
end;

procedure TFmGraGXPatr.PesquisaPorNome(Key: Integer);
var
  //Controle
  Nivel1: Integer;
begin
  if Key = VK_F3 then
  begin
    Nivel1 := CuringaLoc.CriaForm('Nivel1', CO_NOME, 'gragru1', Dmod.MyDB, CO_VAZIO);
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM gragrux ',
    'WHERE GraGru1=' + Geral.FF0(Nivel1),
    '']);
    EdAgrupador.ValueVariant := Dmod.QrAux.FieldByName('Controle').AsInteger;
    ReopenAgrup();
  end;
end;

procedure TFmGraGXPatr.PesquisaPorReferencia(Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdAgrupRef.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdAgrupador.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdAgrupador.ValueVariant := QrPesq1Controle.Value;
      ReopenAgrup();
    end;
    if CBAgrupador.KeyValue <> QrPesq1Controle.Value then
      CBAgrupador.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
    EdAgrupRef.ValueVariant := '';
end;

procedure TFmGraGXPatr.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrGraGXPatr);
  MyObjects.HabilitaMenuItemCabUpd(Editaniveis1, QrGraGXPatr);
  MyObjects.HabilitaMenuItemCabUpd(Editadadosdoproduto1, QrGraGXPatr);
  //
  CabExclui1.Enabled := False;
end;

procedure TFmGraGXPatr.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrGraGXPatr);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrGraGXPIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrGraGXPIts);
end;

procedure TFmGraGXPatr.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraGXPatrControle.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraGXPatr.DefParams;
var
  //GraNiv: Integer;
  Filtro: String;
begin
  if FGraToolRent <> gbsIndef then
  begin
    // Sem Filtro: T i s o l i n
    if Dmod.QrOpcoesTRenGraNivPatr.Value = 7 then
    begin
      VAR_GOTOTABELA := 'gragxpatr cpl, gragrux ggx, gragru1 gg1';
      VAR_GOTOMYSQLTABLE := QrGraGXPatr;
      VAR_GOTONEG := gotoPos;
      VAR_GOTOCAMPO := 'cpl.GraGruX';
      VAR_GOTONOME := 'gg1.Nome';
      VAR_GOTOMySQLDBNAME := Dmod.MyDB;
      VAR_GOTOVAR := 1;

      GOTOy.LimpaVAR_SQL;

      Filtro := ' cpl.GraGrux=ggx.Controle AND ggx.GraGru1=gg1.Nivel1 ';

      VAR_SQLx.Add('SELECT CHAR(cpl.Situacao) SIT_CHR, ggx.Controle, ');
      VAR_SQLx.Add('gg1.Patrimonio, gg1.Referencia, gg1.NCM, gg1.UnidMed, ');
      VAR_SQLx.Add('gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, ');
      VAR_SQLx.Add('gfm.Nome NO_MARCA, gfc.Nome NO_FABR, ');
      VAR_SQLx.Add('IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, ');
      VAR_SQLx.Add('gg2.Nome NO_GG2, gg3.Nome NO_GG3, ');
      VAR_SQLx.Add('gg4.Nome NO_GG4, gg5.Nome NO_GG5, ggt.Nome NO_GGT, ');
      VAR_SQLx.Add('ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3, ggt.TitNiv4, ');
      VAR_SQLx.Add('ggt.TitNiv5, gg1.PrdGrupTip, gg1.Nivel1, gg1.Nivel2, ');
      VAR_SQLx.Add('gg1.Nivel3, gg1.Nivel4, gg1.Nivel5, ');
      VAR_SQLx.Add('gg5.CodUsu CUNivel5, gg4.CodUsu CUNivel4, ');
      VAR_SQLx.Add('gg3.CodUsu CUNivel3, gg2.CodUsu CUNivel2, ');
      VAR_SQLx.Add('gg1.CodUsu, cpl.*, gep.EstqSdo ');
      VAR_SQLx.Add('FROM gragxpatr cpl  ');
      VAR_SQLx.Add('LEFT JOIN gragrux ggx ON cpl.GraGruX=ggx.Controle');
      VAR_SQLx.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ');
      VAR_SQLx.Add('LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2 ');
      VAR_SQLx.Add('LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3 ');
      VAR_SQLx.Add('LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg1.Nivel4 ');
      VAR_SQLx.Add('LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5 ');
      VAR_SQLx.Add('LEFT JOIN prdgruptip ggt ON ggt.Codigo=gg1.PrdGrupTip ');
      VAR_SQLx.Add('LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle AND gep.Empresa=' + Geral.FF0(FEmpresa));
      VAR_SQLx.Add(' ');
      VAR_SQLx.Add('LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle ');
      VAR_SQLx.Add('LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo ');
      VAR_SQLx.Add(' ');
      VAR_SQLx.Add('LEFT JOIN gragruy ggy ON ggy.Codigo=ggx.Controle ');
    end else
    // Com Filtro: AAPA
    begin
      VAR_GOTOTABELA := 'gragrux ggx, gragru1 gg1';
      VAR_GOTOMYSQLTABLE := QrGraGXPatr;
      VAR_GOTONEG := gotoPos;
      VAR_GOTOCAMPO := 'ggx.Controle';
      VAR_GOTONOME := 'gg1.Nome';
      VAR_GOTOMySQLDBNAME := Dmod.MyDB;
      VAR_GOTOVAR := 1;

      GOTOy.LimpaVAR_SQL;

      Dmod.FiltroGrade(FGraToolRent, Filtro);
      Filtro := Filtro + ' AND (gg1.Nivel1=ggx.GraGru1)';
      //
      VAR_SQLx.Add('SELECT CHAR(cpl.Situacao) SIT_CHR, ');
      VAR_SQLx.Add('ggx.Controle, gg1.Referencia, gg1.NCM, gg1.UnidMed, ');
      VAR_SQLx.Add('gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, ');
      VAR_SQLx.Add('gfm.Nome NO_MARCA, gfc.Nome NO_FABR, ');
      VAR_SQLx.Add('IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE, ');
      VAR_SQLx.Add('gg2.Nome NO_GG2, gg3.Nome NO_GG3, ');
      VAR_SQLx.Add('gg4.Nome NO_GG4, gg5.Nome NO_GG5, ggt.Nome NO_GGT, ');
      VAR_SQLx.Add('ggt.TitNiv1, ggt.TitNiv2, ggt.TitNiv3, ggt.TitNiv4, ');
      VAR_SQLx.Add('ggt.TitNiv5, gg1.PrdGrupTip, gg1.Nivel1, gg1.Nivel2, ');
      VAR_SQLx.Add('gg1.Nivel3, gg1.Nivel4, gg1.Nivel5, ');
      VAR_SQLx.Add('gg5.CodUsu CUNivel5, gg4.CodUsu CUNivel4, ');
      VAR_SQLx.Add('gg3.CodUsu CUNivel3, gg2.CodUsu CUNivel2, ');
      VAR_SQLx.Add('gg1.CodUsu, cpl.* ');
      VAR_SQLx.Add('FROM gragrux ggx ');
      VAR_SQLx.Add('LEFT JOIN ' + FTabNome + ' cpl ON cpl.GraGruX=ggx.Controle ');
      VAR_SQLx.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ');
      VAR_SQLx.Add('LEFT JOIN gragru2 gg2 ON gg2.Nivel2=gg1.Nivel2 ');
      VAR_SQLx.Add('LEFT JOIN gragru3 gg3 ON gg3.Nivel3=gg1.Nivel3 ');
      VAR_SQLx.Add('LEFT JOIN gragru4 gg4 ON gg4.Nivel4=gg1.Nivel4 ');
      VAR_SQLx.Add('LEFT JOIN gragru5 gg5 ON gg5.Nivel5=gg1.Nivel5 ');
      VAR_SQLx.Add('LEFT JOIN prdgruptip ggt ON ggt.Codigo=gg1.PrdGrupTip ');
      VAR_SQLx.Add('LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle ');
      VAR_SQLx.Add('LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo ');
    end;
    VAR_SQLx.Add('');
    VAR_SQLx.Add('WHERE ' + Filtro);
    //
    VAR_SQL1.Add('AND ggx.Controle=:P0');
    //
    //VAR_SQL2.Add('AND gg1.CodUsu=:P0');
    //
    VAR_SQLa.Add('AND gg1.Nome LIKE :P0');
    //
    //
    VAR_GOTOVAR1 := Filtro;
    //
  end;
end;

procedure TFmGraGXPatr.EdAgrupadorChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    PesquisaPorGraGruX();
end;

procedure TFmGraGXPatr.EdAgrupadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaPorNome(Key);
  if Key = VK_F5 then
  begin
    QrAgrupador.Close;
    CBAgrupador.ListField := 'NOMEREF_TXT';
    QrAgrupador.Open;
  end;
  if Key = VK_F6 then
  begin
    QrAgrupador.Close;
    CBAgrupador.ListField := 'Nome';
    QrAgrupador.Open;
  end;
end;

procedure TFmGraGXPatr.EdAgrupRefChange(Sender: TObject);
begin
  if EdAgrupRef.Focused then
    PesquisaPorReferencia(False);
end;

procedure TFmGraGXPatr.EdAgrupRefKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaPorNome(Key);
end;

procedure TFmGraGXPatr.EdCBSituacaoChange(Sender: TObject);
begin
  DefineSituacao(EdCBSituacao.ValueVariant, vtInteger);
end;

procedure TFmGraGXPatr.Editadadosdoproduto1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrGraGXPatrControle.Value;
  //
  Grade_Jan.MostraFormGraGruN(QrGraGXPatrNivel1.Value);
  LocCod(Controle, Controle);
end;

procedure TFmGraGXPatr.Editaniveis1Click(Sender: TObject);
begin
  MostraFormPrdGruNew(stUpd, QrGraGXPatrCUNivel5.Value, QrGraGXPatrCUNivel4.Value,
    QrGraGXPatrCUNivel3.Value, QrGraGXPatrCUNivel2.Value, QrGraGXPatrCodUsu.Value);
end;

procedure TFmGraGXPatr.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraGXPatr.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmGraGXPatr.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB, ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmGraGXPatr.EdSIT_CHRChange(Sender: TObject);
begin
  DefineSituacao(EdSIT_CHR.Text, vtString);
end;

procedure TFmGraGXPatr.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmGraGXPatr.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB, ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmGraGXPatr.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGXPatr, [PnDados],
    [PnEdita], EdReferencia, ImgTipo, 'Controle');
  //
  EdGraGruX.ValueVariant    := QrGraGXPatrControle.Value;
  EdReferencia.Text         := QrGraGXPatrReferencia.Value;
  EdPatrimonio.Text         := QrGraGXPatrPatrimonio.Value;
  EdNome.Text               := QrGraGXPatrNO_GG1.Value;
  EdComplem.Text            := QrGraGXPatrComplem.Value;
  TPAquisData.Date          := QrGraGXPatrAquisData.Value;
  EdAquisDocu.Text          := QrGraGXPatrAquisDocu.Value;
  EdAquisValr.ValueVariant  := QrGraGXPatrAquisValr.Value;
  EdAtualValr.ValueVariant  := QrGraGXPatrAtualValr.Value;
  EdValorMes.ValueVariant   := QrGraGXPatrValorMes.Value;
  EdValorQui.ValueVariant   := QrGraGXPatrValorQui.Value;
  EdValorSem.ValueVariant   := QrGraGXPatrValorSem.Value;
  EdValorDia.ValueVariant   := QrGraGXPatrValorDia.Value;
  EdValorFDS.ValueVariant   := QrGraGXPatrValorFDS.Value;
  EdAgrupador.ValueVariant  := QrGraGXPatrAgrupador.Value;
  CBAgrupador.KeyValue      := QrGraGXPatrAgrupador.Value;
  EdMarca.ValueVariant      := QrGraGXPatrMarca.Value;
  CBMarca.KeyValue          := QrGraGXPatrMarca.Value;
  EdModelo.Text             := QrGraGXPatrModelo.Value;
  EdSerie.Text              := QrGraGXPatrSerie.Value;
  EdVoltagem.Text           := QrGraGXPatrVoltagem.Value;
  EdPotencia.Text           := QrGraGXPatrPotencia.Value;
  EdCapacid.Text            := QrGraGXPatrCapacid.Value;
  MeObserva.Text            := QrGraGXPatrObserva.Value;
  RGAplicacao.ItemIndex     := QrGraGXPatrAplicacao.Value;
  EdCBSituacao.ValueVariant := QrGraGXPatrSituacao.Value;
  CBSituacao.KeyValue       := QrGraGXPatrSituacao.Value;
  VUUnidMed.ValueVariant    := QrGraGXPatrUnidMed.Value;
  EdNCM.ValueVariant        := QrGraGXPatrNCM.Value;
  CkAtivo.Checked           := Geral.IntToBool(QrGraGXPatrAtivo.Value);
  EdDMenos.ValueVariant     := QrGraGXPatrDMenos.Value;
end;

procedure TFmGraGXPatr.CabExclui1Click(Sender: TObject);
begin
  //
end;

procedure TFmGraGXPatr.CabInclui1Click(Sender: TObject);
begin
  MostraFormPrdGruNew(stIns, 0, 0, 0, 0, 0);
end;

procedure TFmGraGXPatr.CBAgrupadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
  begin
    QrAgrupador.Close;
    CBAgrupador.ListField := 'NOMEREF_TXT';
    QrAgrupador.Open;
  end;
  if Key = VK_F6 then
  begin
    QrAgrupador.Close;
    CBAgrupador.ListField := 'Nome';
    QrAgrupador.Open;
  end;
end;

procedure TFmGraGXPatr.CBUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB, ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmGraGXPatr.CriaOForm;
begin
  DefineONomeDoForm;
end;

procedure TFmGraGXPatr.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraGXPatr.Recalculaestoque1Click(Sender: TObject);
var
  GraGruX: Integer;
begin
  if QrGraGXPatr.State <> dsInactive then
  begin
    GraGruX := QrGraGXPatrGraGruX.Value;
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, -11, True);
    LocCod(GraGruX, GraGruX);
  end;
end;

procedure TFmGraGXPatr.ReopenAgrup();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgrup, Dmod.MyDB, [
  'SELECT ggx.Controle, gg1.Nome, gg1.Referencia ',
  'FROM gragxpatr gxp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=gxp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE ggx.Controle=' + Geral.FF0(QrGraGXPatrAgrupador.Value),
  '']);
end;

procedure TFmGraGXPatr.ReopenGraGXPIts(Conta: Integer);
var
  SQL_Aplicacao: String;
begin
  case Dmod.QrOpcoesTRenFormaCobrLoca.Value of
    0,1: // Modo simplificado - AAPA
    begin
      SQL_Aplicacao := Geral.ATS([
      'ELT(ggo.Aplicacao, "Acess�rio", "Uso", "Consumo", ',
      '"? ? ?") NO_APLICACAO, ',
      '']);
    end;
    2: // Modo completo - T i s o l i n
    begin
      SQL_Aplicacao := Geral.ATS([
      'CASE ggx.GraGruY',
      '  WHEN 1024 THEN "Principal" ',
      '  WHEN 2048 THEN "Secund�rio" ',
      '  WHEN 3072 THEN "Acess�rio" ',
      '  WHEN 4096 THEN "Apoio" ',
      '  WHEN 5120 THEN "Uso" ',
      '  WHEN 6144 THEN "Consumo" ',
      '  ELSE "?????" END NO_APLICACAO,',
      '']);
    end;
  end;


  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPIts, Dmod.MyDB, [
  'SELECT gxi.GraGXOutr '(*ggo.*)+'GraGruX, ggo.Aplicacao, ggo.ItemValr, ',
  'gg1.Referencia, gg1.Nivel1 COD_GG1, gg1.Nome NO_GG1, ',
  SQL_Aplicacao,
  ' gxi.* ',
  'FROM gragxpits gxi ',
  'LEFT JOIN gragxoutr ggo ON ggo.GraGruX=gxi.GraGXOutr ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=gxi.GraGXOutr ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE gxi.GraGXPatr=' + Geral.FF0(QrGraGXPatrGraGruX.Value),
  'ORDER BY NO_APLICACAO, NO_GG1 ',
  '']);
end;

procedure TFmGraGXPatr.DefineONomeDoForm;
begin
end;

procedure TFmGraGXPatr.DefineSituacao(Sit: Variant; VT: TVarType);
var
  C: Byte;
  A: String;
begin
  if VT = vtInteger then
  begin
    if EdSIT_CHR.Text <> Char(Integer(Sit)) then
      EdSIT_CHR.Text := Char(Integer(Sit));
  end;
  if VT = vtString then
  begin
    A := Sit;
    if Length(A) > 0 then
      C := Ord((A[1]))
    else
      C := 0;
    if EdCBSituacao.ValueVariant <> C then
      EdCBSituacao.ValueVariant := C;
  end;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraGXPatr.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraGXPatr.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraGXPatr.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraGXPatr.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraGXPatr.SpeedButton5Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo       := VUUnidMed.ValueVariant;
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmUnidMed.LocCod(Codigo, Codigo);
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO, 'Codigo', 'CodUsu');
      CBUnidMed.SetFocus;
    end;
  end;
end;

procedure TFmGraGXPatr.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmClasFisc, FmClasFisc, afmoNegarComAviso) then
  begin
    FmClasFisc.BtSeleciona.Visible := True;
    FmClasFisc.ShowModal;
    if FmClasFisc.FNCMSelecio <> '' then
      EdNCM.Text := FmClasFisc.FNCMSelecio;
    FmClasFisc.Destroy;
  end;
end;

procedure TFmGraGXPatr.SBSituacaoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormGraGLSitu(EdCBSituacao.ValueVariant);
  UMyMod.AbreQuery(QrGraGLSitu, Dmod.MyDB);
  if VAR_CADASTRO <> 0 then
    DefineSituacao(VAR_CADASTRO, vtInteger);
end;

procedure TFmGraGXPatr.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGXPatr.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmGraGXPatr.BtPanSitClick(Sender: TObject);
var
  GraGruX, Nivel2: Integer;
begin
  GraGruX := QrGraGXPatrGraGruX.Value;
  Nivel2  := QrGraGXPatrNivel2.Value;
  if GraGruX = 0 then
    Geral.MB_Aviso('Selecione o equipamento!')
  else
    GraL_Jan.MostraFormLocCItsLcLocados(FEmpresa, GraGruX, Nivel2);

end;

procedure TFmGraGXPatr.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraGXPatrGraGruX.Value;
  if TFmGraGXPatr(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmGraGXPatr.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmGraGXPatr.BtConfirmaClick(Sender: TObject);
const
  sProcName = 'TFmGraGXPatr.BtConfirmaClick()';
  //
  function InsUpdG1PrAp(SQLType: TSQLType; GraGruX: Integer): Boolean;
  var
    //VendaData, VendaDocu, AGRPAT, CLVPAT, MARPAT,
    Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia, Capacid,
    Observa: String;
    //Situacao, VendaEnti
    Agrupador, Marca, Aplicacao, Situacao, GraGruY, Ativo, DMenos: Integer;
    //VendaValr,
    AquisValr, AtualValr, ValorMes, ValorQui, ValorSem, ValorDia,
    ValorFDS: Double;
  begin
    //GraGruX        := ;
    Complem        := EdComplem.Text;
    AquisData      := Geral.FDT(TPAquisData.Date, 1);
    AquisDocu      := EdAquisDocu.Text;
    AquisValr      := EdAquisValr.ValueVariant;
    //Situacao       := ;
    AtualValr      := EdAtualValr.ValueVariant;
    ValorMes       := EdValorMes.ValueVariant;
    ValorQui       := EdValorQui.ValueVariant;
    ValorSem       := EdValorSem.ValueVariant;
    ValorDia       := EdValorDia.ValueVariant;
    ValorFDS       := EdValorFDS.ValueVariant;
    Agrupador      := EdAgrupador.ValueVariant;
    Marca          := EdMarca.ValueVariant;
    Modelo         := EdModelo.Text;
    Serie          := EdSerie.Text;
    Voltagem       := EdVoltagem.Text;
    Potencia       := EdPotencia.Text;
    Capacid        := EdCapacid.Text;
    //VendaData      := ;
    //VendaDocu      := ;
    //VendaValr      := ;
    //VendaEnti      := ;
    Observa        := MeObserva.Text;
    //AGRPAT         := ;
    //CLVPAT         := ;
    //MARPAT         := ;
    Aplicacao      := RGAplicacao.ItemIndex;
    Situacao       := EdCBSituacao.ValueVariant;
    Ativo          := Geral.BoolToInt(CkAtivo.Checked);
    //
    DMenos         := EdDMenos.ValueVariant;
    //
    case Dmod.QrOpcoesTRenFormaCobrLoca.Value of
      0,1: // Modo simplificado - AAPA
      begin

      end;
      2: // Modo completo - T i s o l i n
      begin
        if MyObjects.FIC(Aplicacao < 1, RGAplicacao,
          'Defina a aplica��o!') then Exit;
        //
        case RGAplicacao.ItemIndex of
          1: GraGruY := CO_GraGruY_1024_GXPatPri;
          2: GraGruY := CO_GraGruY_2048_GXPatSec;
          3: GraGruY := CO_GraGruY_3072_GXPatAce;
          else
          begin
            Geral.MB_ERRO('ERRO ao definir o GrGruY! ' + sProcName);
          end;
        end;
      end;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragxpatr', False, [
    'Complem', 'AquisData', 'AquisDocu',
    'AquisValr', (*'Situacao',*) 'AtualValr',
    'ValorMes', 'ValorQui', 'ValorSem',
    'ValorDia', 'Agrupador', 'Marca',
    'Modelo', 'Serie', 'Voltagem',
    'Potencia', 'Capacid', (*'VendaData',
    'VendaDocu', 'VendaValr', 'VendaEnti',*)
    'Observa'(*, 'AGRPAT', 'CLVPAT',
    'MARPAT'*), 'Aplicacao', 'Situacao',
    'Ativo', 'ValorFDS', 'DMenos'], [
    'GraGruX'], [
    Complem, AquisData, AquisDocu,
    AquisValr, (*Situacao,*) AtualValr,
    ValorMes, ValorQui, ValorSem,
    ValorDia, Agrupador, Marca,
    Modelo, Serie, Voltagem,
    Potencia, Capacid, (*VendaData,
    VendaDocu, VendaValr, VendaEnti,*)
    Observa(*, AGRPAT, CLVPAT,
    MARPAT*), Aplicacao, Situacao,
    Ativo, ValorFDS, DMenos], [
    GraGruX], True);
    //
    if Result then
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragrux', False, [
      'GraGruY', 'Ativo'], [
      'Controle'], [
      GraGruY, Ativo], [
      GraGruX], True);
    end;
  end;
var
  GraGruX, Nivel1, UnidMed: Integer;
  Nome, Referencia, Patrimonio, NCM: String;
  SQLType: TSQLType;
begin
  Nome       := EdNome.Text;
  Referencia := EdReferencia.Text;
  Patrimonio := EdPatrimonio.Text;
  UnidMed    := EdUnidMed.ValueVariant;
  NCM        := EdNCM.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(Referencia) = 0, EdReferencia, 'Defina uma refer�ncia!') then Exit;
  if MyObjects.FIC(UnidMed = 0, EdUnidMed, 'Defina a unidade de Medida!') then Exit;
  //
  GraGruX := EdGraGruX.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT GraGru1 ',
    'FROM gragrux ',
    'WHERE controle=' + Geral.FF0(GraGruX),
    '']);
  Nivel1 := Dmod.QrAux.FieldByName('GraGru1').AsInteger;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
    'Nome', 'Referencia', 'NCM',
    'UnidMed', 'Patrimonio'], ['Nivel1'],
    [Nome, Referencia, NCM,
    VUUnidMed.ValueVariant, Patrimonio], [Nivel1], True) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM ' + FTabNome,
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
    if GraGruX = Dmod.QrAux.FieldByName('GraGruX').AsInteger then
      SQLType := stUpd
    else
      SQLType := stIns;
    if InsUpdG1PrAp(SQLType, GraGruX) then
    begin
      LocCod(GraGruX, GraGruX);
      ImgTipo.SQLType := stlok;
      PnDados.Visible := True;
      PnEdita.Visible := False;
      GOTOy.BotoesSb(ImgTipo.SQLType);
    end;
  end;
end;

procedure TFmGraGXPatr.BtConsertoClick(Sender: TObject);
var
  Selecionado: Integer;
begin
  if (QrGraGXPatr.State <> dsInactive) and (QrGraGXPatr.RecordCount > 0) and
   (QrGraGXPatrControle.Value <> 0) then
  begin
    if DBCheck.CriaFm(TFmFixGerePsq, FmFixGerePsq, afmoNegarComAviso) then
    begin
      FmFixGerePsq.RGTabela.ItemIndex        := 1;
      FmFixGerePsq.EdGraGruX.ValueVariant    := QrGraGXPatrControle.Value;
      FmFixGerePsq.EdReferencia.ValueVariant := QrGraGXPatrReferencia.Value;
      // Fazer!!! FmFixGerePsq.EdPatrimonio.ValueVariant := QrGraGXPatrPatrimonio.Value;
      FmFixGerePsq.CBGraGruX.KeyValue        := QrGraGXPatrControle.Value;
      //
      FmFixGerePsq.ShowModal;
      //
      Selecionado := FmFixGerePsq.FSelecionado;
      //
      FmFixGerePsq.Destroy;
      if Selecionado <> 0 then
      begin
        FmPrincipal.MostraFormFixGereCab(False, nil, nil, Selecionado);
      end;
    end;
  end;
end;

procedure TFmGraGXPatr.BtDesisteClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(GraGruX, Dmod.MyDB, FTabNome, 'GraGruX');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraGXPatr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FEmpresa := -11;
  DBGrid1.Align := alClient;
  //
  RGAplicacao.Items.Clear;
  DBRGAplicacao.Items.Clear;
  DBRGAplicacao.Values.Clear;
  case Dmod.QrOpcoesTRenFormaCobrLoca.Value of
    0,1: // Modo simplificado - AAPA
    begin
      RGAplicacao.Items.Add('Inativo');
      RGAplicacao.Items.Add('Principal');
      RGAplicacao.Items.Add('Secund�rio');
      //
      DBRGAplicacao.Items.Add('Inativo');
      DBRGAplicacao.Items.Add('Principal');
      DBRGAplicacao.Items.Add('Secund�rio');
      //
      DBRGAplicacao.Values.Add('0');
      DBRGAplicacao.Values.Add('1');
      DBRGAplicacao.Values.Add('2');
      //
    end;
    2: // Modo completo - T i s o l i n
    begin
      RGAplicacao.Items.Add('Indefinido');
      RGAplicacao.Items.Add('Principal');
      RGAplicacao.Items.Add('Secund�rio');
      RGAplicacao.Items.Add('Acess�rio');
      //
      DBRGAplicacao.Items.Add('Indefinido');
      DBRGAplicacao.Items.Add('Principal');
      DBRGAplicacao.Items.Add('Secund�rio');
      DBRGAplicacao.Items.Add('Acess�rio');
      //
      DBRGAplicacao.Values.Add('0');
      DBRGAplicacao.Values.Add('1');
      DBRGAplicacao.Values.Add('2');
      DBRGAplicacao.Values.Add('3');
      //
    end;
  end;
  //
  FiltraItensAExibir(gbsLocar);
  //
  CriaOForm;
  //
  UMyMod.AbreQuery(QrMarcas, Dmod.MyDB);
  UMyMod.AbreQuery(QrAgrupador, Dmod.MyDB);
  UMyMod.AbreQuery(QrGraGLSitu, Dmod.MyDB);
  UMyMod.AbreQuery(QrUnidMed, Dmod.MyDB);
end;

procedure TFmGraGXPatr.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraGXPatrGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmGraGXPatr.SbMarcasClick(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  FmPrincipal.MostraFormGraFabCad();
  UMyMod.SetaCodigoPesquisado(EdMarca, CBMarca, QrMarcas, VAR_CADASTRO2, 'Controle');
end;

procedure TFmGraGXPatr.SbNomeClick(Sender: TObject);
begin
  PesquisaNomePatrimonio;
end;

procedure TFmGraGXPatr.SbNovoClick(Sender: TObject);
var
  Referencia: String;
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrGraGXPatrGraGruX.Value, LaRegistro.Caption);
  //
  Referencia := '';
  if InputQuery('Pesquisa por Refer�ncia', 'Informe a refer�ncia', Referencia) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocod, Dmod.MyDB, [
    'SELECT ggx.Controle ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE gg1.Referencia="' + Referencia + '"',
    '']);
    LocCod(QrLocod.FieldByName('Controle').AsInteger, QrLocod.FieldByName('Controle').AsInteger);
  end;
end;

procedure TFmGraGXPatr.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraGXPatr.QrGraGXPatrAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraGXPatr.QrGraGXPatrAfterScroll(DataSet: TDataSet);
begin
  ReopenGraGXPIts(0);
  ReopenAgrup();
end;

procedure TFmGraGXPatr.FiltraItensAExibir(Material: TGraToolRent);
begin
  FGraToolRent := Material;
  case FGraToolRent of
    gbsLocar: FTabNome := 'gragxpatr';
    else FTabNome := 'gragx????';
  end;
  //
  case FGraToolRent of
    gbsLocar  : FFrmNome := 'Patrim�nio';
    else FFrmNome := '? ? ? ? ?';
  end;
  //
  DefParams;
  Va(vpLast);
end;

procedure TFmGraGXPatr.FormActivate(Sender: TObject);
begin
  if TFmGraGXPatr(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end else
  begin
    // Pode ter mudado para DefParams de outo form em Aba!
    DefParams();
  end;
  //
end;

procedure TFmGraGXPatr.SbQueryClick(Sender: TObject);
begin
  PesquisaNomePatrimonio();
end;

procedure TFmGraGXPatr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGXPatr.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
end;

(*
procedure TFmGraGXPatr.IncluiProduto;
var
  TipCodPatr: Integer;
begin
  VAR_CADASTRO := 0;
  VAR_CADASTRO2 := 0;
  //
  Dmod.ReopenOpcoesTRen;
  //
  TipCodPatr := Dmod.QrOpcoesTRenTipCodPatr.Value;
  //
  if DBCheck.CriaFm(TFmGraGru1, FmGraGru1, afmoNegarComAviso) then
  begin
    FmGraGru1.ImgTipo.SQLType := stIns;
    FmGraGru1.FQrGraGru1      := nil;
    FmGraGru1.FQrGraGruX      := QrGraGXPatr;
    //
    if TipCodPatr <> 0 then
    begin
      FmGraGru1.EdPrdGrupTip.ValueVariant := TipCodPatr;
      FmGraGru1.CBPrdGrupTip.KeyValue     := TipCodPatr;
      FmGraGru1.EdPrdGrupTip.Enabled      := False;
      FmGraGru1.CBPrdGrupTip.Enabled      := False;
    end else
    begin
      FmGraGru1.EdPrdGrupTip.Enabled := True;
      FmGraGru1.CBPrdGrupTip.Enabled := True;
    end;
    FmGraGru1.ShowModal;
    FmGraGru1.Destroy;
  end;
  if VAR_CADASTRO2 <> 0 then
  begin
    LocCod(VAR_CADASTRO2, VAR_CADASTRO2);
    if (VAR_CADASTRO2 = QrGraGXPatrGraGruX.Value) and
      (QrGraGXPatrCPL_EXISTE.Value = 0) then
    begin
      CabAltera1Click(CabAltera1);
    end;
  end;
end;

procedure TFmGraGXPatr.AlteraProduto;
var
  TipCodPatr: Integer;
begin
  Dmod.ReopenOpcoesTRen;
  TipCodPatr := Dmod.QrOpcoesTRenTipCodPatr.Value;
  //
  if DBCheck.CriaFm(TFmGraGru1, FmGraGru1, afmoNegarComAviso) then
  begin
    FmGraGru1.ImgTipo.SQLType := stUpd;
    FmGraGru1.FQrGraGru1 := QrGraGXPatr;
    //
    if TipCodPatr <> 0 then
    begin
      FmGraGru1.EdPrdGrupTip.ValueVariant := TipCodPatr;
      FmGraGru1.CBPrdGrupTip.KeyValue     := TipCodPatr;
      FmGraGru1.EdPrdGrupTip.Enabled      := False;
      FmGraGru1.CBPrdGrupTip.Enabled      := False;
    end else
    begin
      FmGraGru1.EdPrdGrupTip.Enabled := True;
      FmGraGru1.CBPrdGrupTip.Enabled := True;
    end;
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdPrdGrupTip, FmGraGru1.CBPrdGrupTip,
      FmGraGru1.QrPrdGrupTip, QrGraGXPatrPrdGrupTip.Value);
    //
    FmGraGru1.HabilitaComponentes();
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel5, FmGraGru1.CBNivel5,
      FmGraGru1.QrGraGru5, QrGraGXPatrNivel5.Value, 'Nivel5', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel4, FmGraGru1.CBNivel4,
      FmGraGru1.QrGraGru4, QrGraGXPatrNivel4.Value, 'Nivel4', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel3, FmGraGru1.CBNivel3,
      FmGraGru1.QrGraGru3, QrGraGXPatrNivel3.Value, 'Nivel3', 'CodUsu');
    //
    UMyMod.SetaCodUsuDeCodigo(FmGraGru1.EdNivel2, FmGraGru1.CBNivel2,
      FmGraGru1.QrGraGru2, QrGraGXPatrNivel2.Value, 'Nivel2', 'CodUsu');
    //
    FmGraGru1.EdNivel1.ValueVariant     := QrGraGXPatrNivel1.Value;
    FmGraGru1.EdCodUsu_Ant.ValueVariant := QrGraGXPatrCodUsu.Value;
    FmGraGru1.EdCodUsu_New.ValueVariant := QrGraGXPatrCodUsu.Value;
    FmGraGru1.EdNome_Ant.ValueVariant   := QrGraGXPatrNO_GG1.Value;
    FmGraGru1.EdNome_New.ValueVariant   := QrGraGXPatrNO_GG1.Value;
    //
    FmGraGru1.ShowModal;
    FmGraGru1.Destroy;
  end;
end;
*)

procedure TFmGraGXPatr.MostraFormPrdGruNew(SQLTipo: TSQLType; Nivel5, Nivel4,
  Nivel3, Nivel2, Nivel1: Integer);
const
  ForcaCadastrGGX_SemCorTam = True;
var
  TipCodPatr: Integer;
  InseridoAgora: Boolean;
begin
  InseridoAgora := False;
  VAR_CADASTRO := 0;
  VAR_CADASTRO2 := 0;
  //
  Dmod.ReopenOpcoesTRen;
  //
  TipCodPatr := Dmod.QrOpcoesTRenTipCodPatr.Value;
  //
  //DmProd.InsereItemPrdGruNew(cggnFmPQ, TipCodPatr, 0, '', '', Nivel5, Nivel4,
  DmProd.FFmGraGruNew := True;
  DmProd.InsereItemPrdGruNew(cggnFmGraGXPatr, TipCodPatr, 0, '', '',
  ForcaCadastrGGX_SemCorTam, Nivel5, Nivel4, Nivel3, Nivel2, Nivel1, True, 0, '');
  //
  if VAR_CADASTRO2 <> 0 then // VAR_CADASTRO2 = GraGruX.Controle
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
    'SELECT GraGruX Controle ',
    'FROM gragxpatr ',
    'WHERE GraGruX=' + Geral.FF0(VAR_CADASTRO2),
    '']);
    QrPesq1.Open;
    if QrPesq1.RecordCount = 0 then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragxpatr', False,
      ['Aplicacao'], ['GraGruX'], [0], [VAR_CADASTRO2], True) then
          InseridoAgora := True;
    end;
    LocCod(VAR_CADASTRO2, VAR_CADASTRO2);
    //
    if ( (QrGraGXPatrCPL_EXISTE.Value = 0) or (InseridoAgora))
    and (SQLTipo = stIns) then
    begin
      CabAltera1Click(CabAltera1);
    end;
  end;
end;

procedure TFmGraGXPatr.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastroGraGXPIts(stUpd);
end;

procedure TFmGraGXPatr.ItsExclui1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a remo��o do material selecionado?',
  'gragxpits', 'Conta', QrGraGXPItsConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrGraGXPIts, QrGraGXPItsConta,
    QrGraGXPItsConta.Value);
    ReopenGraGXPIts(Conta);
  end;
end;

procedure TFmGraGXPatr.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastroGraGXPIts(stIns);
end;

procedure TFmGraGXPatr.QrGraGXPatrBeforeClose(DataSet: TDataSet);
begin
  QrGraGXPIts.Close;
  QrAgrup.Close;
end;

procedure TFmGraGXPatr.QrGraGXPatrBeforeOpen(DataSet: TDataSet);
begin
  QrGraGXPatrGraGruX.DisplayFormat := FFormatFloat;
end;

procedure TFmGraGXPatr.QrGraGXPatrCalcFields(DataSet: TDataSet);
begin
  QrGraGXPatrVENDADATA_TXT.Value := Geral.FDT(QrGraGXPatrVendaData.Value, 3);
end;

end.

