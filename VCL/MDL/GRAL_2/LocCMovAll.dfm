object FmLocCMovAll: TFmLocCMovAll
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-109 :: Movimenta'#231#245'es de Equipamento Locado'
  ClientHeight = 629
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1168
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 492
        Height = 32
        Caption = 'Movimenta'#231#245'es de Equipamento Locado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 492
        Height = 32
        Caption = 'Movimenta'#231#245'es de Equipamento Locado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 492
        Height = 32
        Caption = 'Movimenta'#231#245'es de Equipamento Locado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1264
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1264
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1264
        Height = 467
        Align = alClient
        TabOrder = 0
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 2
          Top = 41
          Width = 1260
          Height = 424
          Align = alClient
          DataSource = DsLocCMovAll
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TipoMotiv'
              Title.Caption = 'Tipo de movimenta'#231#227'o'
              Width = 127
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TipoES'
              Title.Caption = 'E/S'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LIMPO'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUJO'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUEBRADO'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TESTADODEVOLUCAO'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COBRANCALOCACAO'
              Width = 116
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Quantidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALORLOCACAO'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GGXEntrada'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MotivoTroca'
              Title.Caption = 'Motivo da troca'
              Width = 212
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ObservacaoTroca'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QuantidadeJaDevolvida'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QuantidadeLocada'
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1260
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 24
            Height = 13
            Caption = 'Tipo:'
          end
          object Label2: TLabel
            Left = 248
            Top = 4
            Width = 65
            Height = 13
            Caption = 'Equipamento:'
          end
          object Label3: TLabel
            Left = 912
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object EdNO_ManejoLca: TdmkEdit
            Left = 40
            Top = 0
            Width = 197
            Height = 21
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdGraGruX: TdmkEdit
            Left = 320
            Top = 0
            Width = 73
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNO_GraGruX: TdmkEdit
            Left = 396
            Top = 0
            Width = 513
            Height = 21
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEmpresa: TdmkEdit
            Left = 960
            Top = 0
            Width = 41
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-11'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -11
            ValWarn = False
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1264
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1260
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1264
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1118
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1116
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtDevolucao: TBitBtn
        Tag = 1000108
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Devolu'#231#227'o'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtDevolucaoClick
      end
      object BtSubstituicao: TBitBtn
        Tag = 1000073
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Substitui'#231#227'o'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtSubstituicaoClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrLocCMovAll: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lma.* '
      'FROM loccmovall lma'
      'WHERE lma.Codigo=27352'
      'AND lma.GTRTab=1 '
      'AND lma.CtrID=34556 '
      'AND lma.Item=67331')
    Left = 408
    Top = 340
    object QrLocCMovAllCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocCMovAllGTRTab: TIntegerField
      FieldName = 'GTRTab'
      Required = True
    end
    object QrLocCMovAllCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrLocCMovAllItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrLocCMovAllControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLocCMovAllSEQUENCIA: TIntegerField
      FieldName = 'SEQUENCIA'
      Required = True
    end
    object QrLocCMovAllDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrLocCMovAllUSUARIO: TWideStringField
      FieldName = 'USUARIO'
      Size = 10
    end
    object QrLocCMovAllGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrLocCMovAllTipoES: TWideStringField
      FieldName = 'TipoES'
      Size = 1
    end
    object QrLocCMovAllQuantidade: TIntegerField
      FieldName = 'Quantidade'
    end
    object QrLocCMovAllTipoMotiv: TSmallintField
      FieldName = 'TipoMotiv'
    end
    object QrLocCMovAllCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 50
    end
    object QrLocCMovAllVALORLOCACAO: TFloatField
      FieldName = 'VALORLOCACAO'
      Required = True
    end
    object QrLocCMovAllLIMPO: TWideStringField
      FieldName = 'LIMPO'
      Size = 1
    end
    object QrLocCMovAllSUJO: TWideStringField
      FieldName = 'SUJO'
      Size = 1
    end
    object QrLocCMovAllQUEBRADO: TWideStringField
      FieldName = 'QUEBRADO'
      Size = 1
    end
    object QrLocCMovAllTESTADODEVOLUCAO: TWideStringField
      FieldName = 'TESTADODEVOLUCAO'
      Size = 1
    end
    object QrLocCMovAllGGXEntrada: TLargeintField
      FieldName = 'GGXEntrada'
    end
    object QrLocCMovAllMotivoTroca: TIntegerField
      FieldName = 'MotivoTroca'
      Required = True
    end
    object QrLocCMovAllObservacaoTroca: TWideStringField
      FieldName = 'ObservacaoTroca'
      Size = 512
    end
    object QrLocCMovAllQuantidadeLocada: TIntegerField
      FieldName = 'QuantidadeLocada'
    end
    object QrLocCMovAllQuantidadeJaDevolvida: TIntegerField
      FieldName = 'QuantidadeJaDevolvida'
    end
    object QrLocCMovAllLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrLocCMovAllDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocCMovAllDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocCMovAllUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrLocCMovAllUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrLocCMovAllAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLocCMovAllAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrLocCMovAllAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrLocCMovAllAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLocCMovAllNO_ManejoLca: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ManejoLca'
      Size = 60
      Calculated = True
    end
    object QrLocCMovAllNO_TipoMotiv: TWideStringField
      FieldName = 'NO_TipoMotiv'
      Size = 60
    end
    object QrLocCMovAllSubstitutoLca: TIntegerField
      FieldName = 'SubstitutoLca'
    end
    object QrLocCMovAllNO_MotivoTroca: TWideStringField
      FieldName = 'NO_MotivoTroca'
      Size = 60
    end
  end
  object DsLocCMovAll: TDataSource
    DataSet = QrLocCMovAll
    Left = 420
    Top = 404
  end
  object PMDevolucao: TPopupMenu
    OnPopup = PMDevolucaoPopup
    Left = 156
    Top = 424
    object IncluiDevoluo1: TMenuItem
      Caption = '&Inclui Devolu'#231#227'o'
      OnClick = IncluiDevoluo1Click
    end
    object AlteraDevoluo1: TMenuItem
      Caption = '&Altera Devolu'#231#227'o'
      Enabled = False
      Visible = False
      OnClick = AlteraDevoluo1Click
    end
    object ExcluiDevoluo1: TMenuItem
      Caption = '&Exclui Devolu'#231#227'o'
      OnClick = ExcluiDevoluo1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Recalcula1: TMenuItem
      Caption = '&Recalcula'
      OnClick = Recalcula1Click
    end
  end
  object PMSubstituicao: TPopupMenu
    OnPopup = PMSubstituicaoPopup
    Left = 252
    Top = 444
    object IncluiSubstituio1: TMenuItem
      Caption = '&Inclui Substitui'#231#227'o'
      OnClick = IncluiSubstituio1Click
    end
    object AlteraSubstituio1: TMenuItem
      Caption = '&Altera Substitui'#231#227'o'
      Visible = False
      OnClick = AlteraSubstituio1Click
    end
    object ExcluiSubstituio1: TMenuItem
      Caption = '&Exclui Substitui'#231#227'o'
      OnClick = ExcluiSubstituio1Click
    end
  end
end
