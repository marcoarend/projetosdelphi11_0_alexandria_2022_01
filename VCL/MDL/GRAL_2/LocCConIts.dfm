object FmLocCConIts: TFmLocCConIts
  Left = 339
  Top = 185
  Caption = 'LOC-PATRI-102 :: Tipo de Movimento na Loca'#231#227'o'
  ClientHeight = 381
  ClientWidth = 484
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 484
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 488
    object GB_R: TGroupBox
      Left = 436
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 440
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 388
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 392
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 378
        Height = 32
        Caption = 'Tipo de Movimento na Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 378
        Height = 32
        Caption = 'Tipo de Movimento na Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 378
        Height = 32
        Caption = 'Tipo de Movimento na Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 484
    Height = 219
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 488
    ExplicitHeight = 177
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 484
      Height = 219
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 488
      ExplicitHeight = 177
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 484
        Height = 219
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 488
        ExplicitHeight = 177
        object RGIDTab: TRadioGroup
          Left = 2
          Top = 15
          Width = 480
          Height = 54
          Align = alTop
          Caption = ' Tipo de Movimento: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'Loca'#231#227'o'
            'Servi'#231'o'
            'Venda')
          TabOrder = 0
          OnClick = RGIDTabClick
          ExplicitWidth = 484
        end
        object RGStatus: TRadioGroup
          Left = 2
          Top = 69
          Width = 480
          Height = 54
          Align = alTop
          Caption = ' Status Inicial do Movimento: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'Or'#231'amento'
            'Pedido')
          TabOrder = 1
          OnClick = RGStatusClick
          ExplicitWidth = 484
        end
        object Panel6: TPanel
          Left = 2
          Top = 123
          Width = 480
          Height = 42
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          ExplicitWidth = 484
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 260
            Height = 13
            Caption = 'Canal de atendimento espec'#237'fico desta movimenta'#231#227'o:'
          end
          object SbAtndCanal: TSpeedButton
            Left = 451
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbAtndCanalClick
          end
          object EdAtndCanal: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cargo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBAtndCanal
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBAtndCanal: TdmkDBLookupComboBox
            Left = 66
            Top = 20
            Width = 379
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsAtndCanal
            TabOrder = 1
            dmkEditCB = EdAtndCanal
            QryCampo = 'Cargo'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 165
          Width = 480
          Height = 52
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 3
          ExplicitTop = 188
          ExplicitWidth = 484
          ExplicitHeight = 55
          object LaCondiOrca: TLabel
            Left = 8
            Top = 4
            Width = 119
            Height = 13
            Caption = 'Condi'#231#227'o de pagamento:'
          end
          object SBCondicaoPG: TSpeedButton
            Left = 451
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SBCondicaoPGClick
          end
          object EdCondiOrca: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cargo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCondiOrca
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCondiOrca: TdmkDBLookupComboBox
            Left = 66
            Top = 20
            Width = 379
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DmFatura.DsPediPrzCab
            TabOrder = 1
            dmkEditCB = EdCondiOrca
            QryCampo = 'Cargo'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 267
    Width = 484
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 225
    ExplicitWidth = 488
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 480
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 484
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 311
    Width = 484
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 269
    ExplicitWidth = 488
    object PnSaiDesis: TPanel
      Left = 338
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 342
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 336
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 340
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 192
    Top = 47
  end
  object QrAtndCanal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM atndcanal'
      'ORDER BY Nome')
    Left = 280
    Top = 192
    object QrAtndCanalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtndCanalNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsAtndCanal: TDataSource
    DataSet = QrAtndCanal
    Left = 280
    Top = 236
  end
end
