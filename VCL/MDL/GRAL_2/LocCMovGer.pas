unit LocCMovGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEdit, dmkPermissoes, dmkDBGridZTO,
  mySQLDbTables, UnDmkEnums, UnAppEnums, dmkDBLookupComboBox, dmkEditCB,
  dmkEditDateTimePicker, frxClass, frxDBSet;

type
  TFmLocCMovGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel5: TPanel;
    QrLocCMovAll: TMySQLQuery;
    QrLocCMovAllCodigo: TIntegerField;
    QrLocCMovAllGTRTab: TIntegerField;
    QrLocCMovAllCtrID: TIntegerField;
    QrLocCMovAllItem: TIntegerField;
    QrLocCMovAllControle: TIntegerField;
    QrLocCMovAllSEQUENCIA: TIntegerField;
    QrLocCMovAllDataHora: TDateTimeField;
    QrLocCMovAllUSUARIO: TWideStringField;
    QrLocCMovAllGraGruX: TIntegerField;
    QrLocCMovAllTipoES: TWideStringField;
    QrLocCMovAllQuantidade: TIntegerField;
    QrLocCMovAllTipoMotiv: TSmallintField;
    QrLocCMovAllCOBRANCALOCACAO: TWideStringField;
    QrLocCMovAllVALORLOCACAO: TFloatField;
    QrLocCMovAllLIMPO: TWideStringField;
    QrLocCMovAllSUJO: TWideStringField;
    QrLocCMovAllQUEBRADO: TWideStringField;
    QrLocCMovAllTESTADODEVOLUCAO: TWideStringField;
    QrLocCMovAllGGXEntrada: TLargeintField;
    QrLocCMovAllMotivoTroca: TIntegerField;
    QrLocCMovAllObservacaoTroca: TWideStringField;
    QrLocCMovAllQuantidadeLocada: TIntegerField;
    QrLocCMovAllQuantidadeJaDevolvida: TIntegerField;
    QrLocCMovAllLk: TIntegerField;
    QrLocCMovAllDataCad: TDateField;
    QrLocCMovAllDataAlt: TDateField;
    QrLocCMovAllUserCad: TIntegerField;
    QrLocCMovAllUserAlt: TIntegerField;
    QrLocCMovAllAlterWeb: TSmallintField;
    QrLocCMovAllAWServerID: TIntegerField;
    QrLocCMovAllAWStatSinc: TSmallintField;
    QrLocCMovAllAtivo: TSmallintField;
    QrLocCMovAllNO_ManejoLca: TWideStringField;
    DsLocCMovAll: TDataSource;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label3: TLabel;
    EdEmpresa: TdmkEdit;
    dmkDBGridZTO2: TdmkDBGridZTO;
    EdLocCConCab: TdmkEdit;
    Label1: TLabel;
    QrGraGXPatr: TMySQLQuery;
    QrGraGXPatrEstqSdo: TFloatField;
    QrGraGXPatrControle: TIntegerField;
    QrGraGXPatrReferencia: TWideStringField;
    QrGraGXPatrCOD_GG1: TIntegerField;
    QrGraGXPatrNO_GG1: TWideStringField;
    QrGraGXPatrCPL_EXISTE: TFloatField;
    QrGraGXPatrSituacao: TWordField;
    QrGraGXPatrAtualValr: TFloatField;
    QrGraGXPatrValorMes: TFloatField;
    QrGraGXPatrValorQui: TFloatField;
    QrGraGXPatrValorSem: TFloatField;
    QrGraGXPatrValorDia: TFloatField;
    QrGraGXPatrAgrupador: TIntegerField;
    QrGraGXPatrMarca: TIntegerField;
    QrGraGXPatrModelo: TWideStringField;
    QrGraGXPatrSerie: TWideStringField;
    QrGraGXPatrVoltagem: TWideStringField;
    QrGraGXPatrPotencia: TWideStringField;
    QrGraGXPatrCapacid: TWideStringField;
    QrGraGXPatrTXT_MES: TWideStringField;
    QrGraGXPatrTXT_QUI: TWideStringField;
    QrGraGXPatrTXT_SEM: TWideStringField;
    QrGraGXPatrTXT_DIA: TWideStringField;
    QrGraGXPatrNO_SIT: TWideStringField;
    QrGraGXPatrNO_SITAPL: TIntegerField;
    QrGraGXPatrValorFDS: TFloatField;
    QrGraGXPatrDMenos: TIntegerField;
    QrGraGXPatrPatrimonio: TWideStringField;
    QrGraGXPatrNivel2: TIntegerField;
    DsGraGXPatr: TDataSource;
    Label2: TLabel;
    EdGraGruX: TdmkEditCB;
    EdPatrimonio: TdmkEdit;
    Label4: TLabel;
    Label19: TLabel;
    EdReferencia: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    Label7: TLabel;
    QrPesq1: TMySQLQuery;
    QrPesq1Referencia: TWideStringField;
    QrPesq1Patrimonio: TWideStringField;
    QrPesq1Controle: TIntegerField;
    QrPesq2: TMySQLQuery;
    QrPesq2Referencia: TWideStringField;
    QrPesq2Patrimonio: TWideStringField;
    QrLocCItsLca: TMySQLQuery;
    QrLocCItsLcaCodigo: TIntegerField;
    QrLocCItsLcaCtrID: TIntegerField;
    QrLocCItsLcaItem: TIntegerField;
    QrLocCItsLcaGraGruX: TIntegerField;
    QrLocCItsLcaNO_GGX: TWideStringField;
    QrLocCItsLcaREFERENCIA: TWideStringField;
    QrLocCItsLcaValorDia: TFloatField;
    QrLocCItsLcaValorSem: TFloatField;
    QrLocCItsLcaValorQui: TFloatField;
    QrLocCItsLcaValorMes: TFloatField;
    QrLocCItsLcaDtHrLocado: TDateTimeField;
    QrLocCItsLcaDtHrRetorn: TDateTimeField;
    QrLocCItsLcaQtdeProduto: TIntegerField;
    QrLocCItsLcaValorProduto: TFloatField;
    QrLocCItsLcaQtdeLocacao: TIntegerField;
    QrLocCItsLcaValorLocacao: TFloatField;
    QrLocCItsLcaQtdeDevolucao: TIntegerField;
    QrLocCItsLcaManejoLca: TSmallintField;
    DsLocCItsLca: TDataSource;
    DsClientes: TDataSource;
    QrClientes: TMySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    Label17: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    CkSaiDtHrIni: TCheckBox;
    CkSaiDtHrFim: TCheckBox;
    TPSaiDtHrIni: TdmkEditDateTimePicker;
    TPSaiDtHrFim: TdmkEditDateTimePicker;
    BrReabrePsq: TBitBtn;
    Splitter1: TSplitter;
    QrLocCItsLcaPATRIMONIO: TWideStringField;
    BtImprime: TBitBtn;
    frxLOC_PATRI_114_01: TfrxReport;
    frxDsLocCItsLca: TfrxDBDataset;
    QrLocCItsLcaSaiDtHr: TDateTimeField;
    QrLocCItsLcaRetFunci: TIntegerField;
    QrLocCItsLcaRetExUsr: TIntegerField;
    QrLocCItsLcaLibExUsr: TIntegerField;
    QrLocCItsLcaLibFunci: TIntegerField;
    QrLocCItsLcaLibDtHr: TDateTimeField;
    QrLocCItsLcaRELIB: TWideStringField;
    QrLocCItsLcaHOLIB: TWideStringField;
    QrLocCItsLcaValDevolParci: TFloatField;
    QrLocCItsLcaTroca: TSmallintField;
    QrLocCItsLcaCategoria: TWideStringField;
    QrLocCItsLcaCOBRANCALOCACAO: TWideStringField;
    QrLocCItsLcaCobrancaConsumo: TFloatField;
    QrLocCItsLcaCobrancaRealizadaVenda: TWideStringField;
    QrLocCItsLcaCobranIniDtHr: TDateTimeField;
    QrLocCItsLcaValorFDS: TFloatField;
    QrLocCItsLcaDMenos: TSmallintField;
    QrLocCItsLcaValorLocAAdiantar: TFloatField;
    QrLocCItsLcaValorLocAtualizado: TFloatField;
    QrLocCItsLcaCalcAdiLOCACAO: TWideStringField;
    QrLocCItsLcaDiasUteis: TIntegerField;
    QrLocCItsLcaDiasFDS: TIntegerField;
    QrLocCItsLcaPercDesco: TFloatField;
    QrLocCItsLcaValorBruto: TFloatField;
    QrLocCItsLcaLOGIN: TWideStringField;
    QrLocCItsLcaSaiDtHr_TXT: TWideStringField;
    frxDsLocCMovAll: TfrxDBDataset;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdPatrimonioChange(Sender: TObject);
    procedure EdPatrimonioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdReferenciaChange(Sender: TObject);
    procedure BrReabrePsqClick(Sender: TObject);
    procedure QrLocCItsLcaBeforeClose(DataSet: TDataSet);
    procedure QrLocCItsLcaAfterScroll(DataSet: TDataSet);
    procedure QrLocCItsLcaAfterOpen(DataSet: TDataSet);
    procedure frxLOC_PATRI_114_01GetValue(const VarName: string;
      var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
    procedure EdLocCConCabRedefinido(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure EdClienteRedefinido(Sender: TObject);
    procedure CkSaiDtHrIniClick(Sender: TObject);
    procedure CkSaiDtHrFimClick(Sender: TObject);
    procedure TPSaiDtHrIniChange(Sender: TObject);
    procedure TPSaiDtHrIniClick(Sender: TObject);
    procedure TPSaiDtHrFimChange(Sender: TObject);
    procedure TPSaiDtHrFimClick(Sender: TObject);
  private
    { Private declarations }
    procedure PesquisaPorPatrimonio(Limpa: Boolean);
    procedure PesquisaPorReferencia(Limpa: Boolean);
    procedure PesquisaPorGraGruX();
  public
    { Public declarations }
    //FGtrTab: TGraToolRent;
    //FCodigo, FCtrID, FItem, FManejoLca: Integer;
    FRecalcular: Boolean;
    //
    procedure ReopenLocCItsLca();
    procedure ReopenLocCMovAll();
    procedure ReopenGraGxPatr(Empresa: Integer);
    procedure InsAltMovimento(SQLType: TSQLType; TipoMotiv: TTipoMotivLocMov);
    procedure FechaPesquisa();
  end;

  var
  FmLocCMovGer: TFmLocCMovGer;

implementation

uses UnMyObjects, UnDmkProcFunc, Module, DmkDAC_PF, UnGraL_Jan, UMySQLModule,
  UnAppPF, UnGOTOy, MeuDBUses;

{$R *.DFM}

procedure TFmLocCMovGer.BitBtn1Click(Sender: TObject);
begin
  InsAltMovimento(stUpd, TTipoMotivLocMov(QrLocCMovAllTipoMotiv.Value));
end;

procedure TFmLocCMovGer.BitBtn2Click(Sender: TObject);
var
  GraGruX, Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do movimento selecionado?',
  'loccmovall', 'Controle', QrLocCMovAllControle.Value, Dmod.MyDB) = ID_YES then
  begin
    FRecalcular := True;
    GraGruX     := QrLocCMovAllGraGruX.Value;
    AppPF.AtualizaValDevolParci(FCodigo, Integer(FGTRTab), FCtrID, FItem);
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, EdEmpresa.ValueVariant, True);
    Controle := GOTOy.LocalizaPriorNextIntQr(QrLocCMovAll, QrLocCMovAllControle,
    QrLocCMovAllControle.Value);
    ReopenLocCMovAll();
    QrLocCMovAll.Locate('Controle', Controle, []);
  end;
}
end;

procedure TFmLocCMovGer.BrReabrePsqClick(Sender: TObject);
begin
  ReopenLocCItsLca();
end;

procedure TFmLocCMovGer.BtImprimeClick(Sender: TObject);
begin
  QrLocCItsLca.DisableControls;
  QrLocCMovAll.DisableControls;
  try
    MyObjects.frxDefineDataSets(frxLOC_PATRI_114_01, [
      frxDsLocCItsLca,
      frxDsLocCMovAll]);
    //
    MyObjects.frxMostra(frxLOC_PATRI_114_01, 'Relat�rio de Devolu��es');
  finally
    QrLocCItsLca.EnableControls;
    QrLocCMovAll.EnableControls;
  end;
end;

procedure TFmLocCMovGer.BtIncluiClick(Sender: TObject);
begin
  InsAltMovimento(stIns, TTipoMotivLocMov.tmlmLocarVolta);
end;

procedure TFmLocCMovGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocCMovGer.CkSaiDtHrFimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmLocCMovGer.CkSaiDtHrIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmLocCMovGer.EdClienteRedefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmLocCMovGer.EdEmpresaRedefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmLocCMovGer.EdGraGruXChange(Sender: TObject);
begin
  if (not EdPatrimonio.Focused) and
     (not EdReferencia.Focused) then
    PesquisaPorGraGruX();
  //
  //if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    //BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmLocCMovGer.EdGraGruXRedefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmLocCMovGer.EdLocCConCabRedefinido(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmLocCMovGer.EdPatrimonioChange(Sender: TObject);
begin
  if EdPatrimonio.Focused then
    PesquisaPorPatrimonio(False);
  //
  //if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    //BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmLocCMovGer.EdPatrimonioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmLocCMovGer.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
  //
  //if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    //BtOK.Enabled := QrGraGXPatrNO_SITAPL.Value = 1;
end;

procedure TFmLocCMovGer.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmLocCMovGer.FechaPesquisa();
begin
  QrLocCItsLca.Close;
end;

procedure TFmLocCMovGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCMovGer.FormCreate(Sender: TObject);
begin
  FRecalcular := False;
  ImgTipo.SQLType := stPsq;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrClientes, Dmod.MyDB, [
  'SELECT Codigo, IF(Tipo=0, RazaoSocial, ',
  'Nome) NOMEENTIDADE ',
  'FROM entidades ',
  'WHERE Ativo = 1',
  'ORDER BY NOMEENTIDADE ',
  '']);
  CkSaiDtHrIni.Checked := True;
  TPSaiDtHrIni.Date    := Date - 90;
  TPSaiDtHrFim.Date    := Date;
end;

procedure TFmLocCMovGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCMovGer.frxLOC_PATRI_114_01GetValue(const VarName: string;
  var Value: Variant);
const
  sTodos =  'Todos do per�odo';
begin
  if VarName = 'VAR_EMPRESANOME' then Value := VAR_EMPRESANOME
  else
  if VarName = 'VARF_LocCConCab' then Value :=
    dmkPF.EscolhaDe2Str(EdLocCConCab.ValueVariant <> 0, Geral.FF0(
    EdLocCConCab.ValueVariant), sTodos)
  else
  if VarName = 'VARF_Cliente' then Value :=
    dmkPF.EscolhaDe2Str(EdCliente.ValueVariant <> 0, Geral.FF0(
    EdCliente.ValueVariant) + ' - ' + CBCliente.Text, sTodos)
  else
  if VarName = 'VARF_Equipamento' then Value :=
    dmkPF.EscolhaDe2Str(EdGraGruX.ValueVariant <> 0, EdReferencia.Text + ' - ' +
    CBGraGruX.Text, sTodos)
  else
  if VarName = 'VARF_Periodo' then Value :=
    dmkPF.PeriodoImp2(TPSaiDtHrIni.Date, TPSaiDtHrFim.Date,
      CkSaiDtHrIni.Checked, CkSaiDtHrFim.Checked, '', '', ' ')
  else
end;

procedure TFmLocCMovGer.InsAltMovimento(SQLType: TSQLType; TipoMotiv: TTipoMotivLocMov);
var
  Codigo, CtrID, Item, AtuControle, NewControle, Empresa: Integer;
  Recalcular: Boolean;
begin
{
  Codigo     := FCodigo;
  Empresa    := EdEmpresa.ValueVariant;
  CtrID      := FCtrID;
  Item       := FItem;
  if SQLType = stIns then
    AtuControle := 0
  else
    AtuControle := QrLocCMovAllControle.Value;
  NewControle := 0;
  //
  GraL_Jan.MostraFormLocCItsRet(SQLType, Codigo, CtrID, Item, AtuControle,
  Empresa, TipoMotiv, NewControle, Recalcular);
  if Recalcular then
    FRecalcular := True;
  //
  ReopenLocCMovAll();
  QrLocCMovAll.Locate('Controle', NewControle, []);
}
end;

procedure TFmLocCMovGer.PesquisaPorGraGruX;
var
  Reabre: Boolean;
begin
  Reabre := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia, gg1.Patrimonio ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  cpl.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (cpl.GraGruX IS NULL) ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdPatrimonio.ValueVariant <> QrPesq2Patrimonio.Value then
    begin
      EdPatrimonio.ValueVariant := QrPesq2Patrimonio.Value;
      Reabre := True;
    end;
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
    begin
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
      Reabre := True;
    end;
    if Reabre then
      //ReopenAgrupado()
      ;
  end else
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
  end;
  //
  //MostraSituacaoPatrimonio();
end;

procedure TFmLocCMovGer.PesquisaPorPatrimonio(Limpa: Boolean);
var
  Reabre: Boolean;
begin
  Reabre := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle, gg1.Referencia, gg1.Patrimonio ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Patrimonio="' + EdPatrimonio.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
      Reabre := True;
    end;
    if EdReferencia.ValueVariant <> QrPesq1Referencia.Value then
    begin
      EdReferencia.ValueVariant := QrPesq1Referencia.Value;
      Reabre := True;
    end;
    if Reabre then
      //ReopenAgrupado()
      ;
    //  Precisa ?
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
  end;
  //
  //MostraSituacaoPatrimonio();
end;

procedure TFmLocCMovGer.PesquisaPorReferencia(Limpa: Boolean);
var
  Reabre: Boolean;
begin
  Reabre := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdPatrimonio.ValueVariant <> QrPesq1Patrimonio.Value then
    begin
      EdPatrimonio.ValueVariant := QrPesq1Patrimonio.Value;
      Reabre := True;
    end;
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
      Reabre := True;
    end;
    if Reabre then
      //ReopenAgrupado();
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
  end;
  //
  //MostraSituacaoPatrimonio();
end;

procedure TFmLocCMovGer.QrLocCItsLcaAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := True;
end;

procedure TFmLocCMovGer.QrLocCItsLcaAfterScroll(DataSet: TDataSet);
begin
  ReopenLocCMovAll();
end;

procedure TFmLocCMovGer.QrLocCItsLcaBeforeClose(DataSet: TDataSet);
begin
  QrLocCMovAll.Close;
  BtImprime.Enabled := False;
end;

procedure TFmLocCMovGer.ReopenGraGxPatr(Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPatr, Dmod.MyDB, [
  'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL,  ',
  'ggx.Controle, gg1.Referencia, gg1.Patrimonio, gg1.Nivel1 COD_GG1,  ',
  'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE,   ',
  'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,  ',
  'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,  ',
  'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,  ',
  'cpl.Voltagem, cpl.Potencia, cpl.Capacid, ',
  'cpl.ValorFDS, cpl.DMenos, gg1.Patrimonio, gg1.Nivel2 ',
  '    ',
  'FROM gragrux ggx   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle   ',
  'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle   ',
  'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo   ',
  'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao ',
  'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle AND gep.Empresa=' + Geral.FF0(Empresa),
  'LEFT JOIN gragruY    ggy ON ggy.Codigo=ggx.GraGruY ',
(*
  'WHERE ggy.Codigo=' + Geral.FF0(CO_GraGruY_1024_GXPatPri),
  'AND cpl.Aplicacao <> 0 ',
  'AND ggx.Ativo = 1',
*)
  ' ',
  'ORDER BY NO_GG1  ',
  '']);
end;

procedure TFmLocCMovGer.ReopenLocCItsLca();
var
  SQL_Sai, SQL_Cab, SQL_GGX, SQL_Cli: String;
  Cab, GGX, Cli: Integer;
  SaiIni, SaiFim: TDateTime;
begin
  SQL_Sai :=   dmkPF.SQL_Periodo('WHERE lpp.SaiDtHr ', Trunc(TPSaiDtHrIni.Date),
    Trunc(TPSaiDtHrFim.Date), CkSaiDtHrIni.Checked, CkSaiDtHrFim.Checked);
  //
  SQL_Cab := '';
  SQL_GGX := '';
  SQL_Cli := '';
  //
  Cab := EdLocCConCab.ValueVariant;
  GGX := EdGraGruX.ValueVariant;
  Cli := EdCliente.ValueVariant;
  //
  if Cab <> 0 then
    SQL_Cab := 'AND lpp.Codigo=' + Geral.FF0(Cab);
  if GGX <> 0 then
    SQL_GGX := 'AND lpp.GraGruX=' + Geral.FF0(GGX);
  if Cli <> 0 then
    SQL_Cli := 'AND cab.Cliente=' + Geral.FF0(Cli);

  //

  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCItsLca, Dmod.MyDB, [
  'SELECT lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA, gg1.PATRIMONIO, ',
  'sen.LOGIN, IF(lpp.SaiDtHr < "1900-01-01", "", ',
  'DATE_FORMAT(lpp.SaiDtHr, "%d/%m/%Y %h:%i")) SaiDtHr_TXT ',
  'FROM loccitslca lpp ',
  'LEFT JOIN loccconcab cab ON cab.Codigo=lpp.Codigo ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
  //'WHERE lpp.Codigo <> 0 ',
  SQL_Sai,
  SQL_Cab,
  SQL_GGX,
  SQL_Cli,
  '']);
end;

procedure TFmLocCMovGer.ReopenLocCMovAll();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCMovAll, Dmod.MyDB, [
  'SELECT lma.*',
  //SQL_ManejoLca,
  'FROM loccmovall lma ',
  'WHERE lma.Codigo=' + Geral.FF0(QrLocCItsLcaCodigo.Value),
{
  'AND lma.GTRTab=' + Geral.FF0(Integer(FGtrTab)),
  'AND lma.CtrID=' + Geral.FF0(FCtrID),
}
  'AND lma.Item=' + Geral.FF0(QrLocCItsLcaItem.Value),
  '']);
end;

procedure TFmLocCMovGer.TPSaiDtHrFimChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmLocCMovGer.TPSaiDtHrFimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmLocCMovGer.TPSaiDtHrIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmLocCMovGer.TPSaiDtHrIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
