object FmLocCConCab: TFmLocCConCab
  Left = 368
  Top = 194
  ActiveControl = SbQuery
  Caption = 'LOC-PATRI-101 :: Gerenciamento de Loca'#231#227'o'
  ClientHeight = 763
  ClientWidth = 1326
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 89
    Width = 1326
    Height = 674
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object Panel16: TPanel
      Left = 0
      Top = 309
      Width = 545
      Height = 293
      Align = alLeft
      TabOrder = 2
      object ImgCliente: TImage
        Left = 14
        Top = 12
        Width = 157
        Height = 157
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Proportional = True
        Stretch = True
        OnClick = ImgClienteClick
      end
      object ImgRequisitante: TImage
        Left = 192
        Top = 12
        Width = 158
        Height = 157
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Proportional = True
        Stretch = True
        OnClick = ImgRequisitanteClick
      end
      object ImgRetirada: TImage
        Left = 371
        Top = 12
        Width = 157
        Height = 157
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Proportional = True
        Stretch = True
        OnClick = ImgRetiradaClick
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 602
      Width = 1326
      Height = 72
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 120
        Height = 44
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1160
        Top = 15
        Width = 164
        Height = 55
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 5
          Top = 2
          Width = 120
          Height = 44
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1326
      Height = 309
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Panel11: TPanel
        Left = 2
        Top = 38
        Width = 1322
        Height = 269
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object Label17: TLabel
          Left = 12
          Top = 0
          Width = 35
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cliente:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label18: TLabel
          Left = 529
          Top = 0
          Width = 62
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Requisitante:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label19: TLabel
          Left = 726
          Top = 0
          Width = 126
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Respons'#225'vel pela retirada:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label22: TLabel
          Left = 10
          Top = 84
          Width = 49
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Vendedor:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 290
          Top = 130
          Width = 71
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'N'#186' Requisi'#231#227'o:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label25: TLabel
          Left = 197
          Top = 84
          Width = 88
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Endere'#231'o da obra:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label26: TLabel
          Left = 10
          Top = 167
          Width = 70
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Observa'#231#227'o 1:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label27: TLabel
          Left = 314
          Top = 167
          Width = 70
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Observa'#231#227'o 2:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label28: TLabel
          Left = 619
          Top = 167
          Width = 70
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Observa'#231#227'o 3:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label24: TLabel
          Left = 10
          Top = 128
          Width = 79
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Contato na obra:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label29: TLabel
          Left = 176
          Top = 128
          Width = 108
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Telefone contato obra:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label30: TLabel
          Left = 396
          Top = 130
          Width = 313
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Endere'#231'o de cobran'#231'a (quando diferente do endere'#231'o do cliente):'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object SbCliente: TSpeedButton
          Left = 486
          Top = 16
          Width = 21
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          OnClick = SbClienteClick
        end
        object Label34: TLabel
          Left = 10
          Top = 41
          Width = 98
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Endere'#231'o do cliente:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object DBText1: TDBText
          Left = 104
          Top = 0
          Width = 112
          Height = 17
          Color = clBtnFace
          DataField = 'Vencido'
          DataSource = DsValAberto
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          StyleElements = [seClient, seBorder]
        end
        object DBText2: TDBText
          Left = 260
          Top = 0
          Width = 112
          Height = 17
          DataField = 'Aberto'
          DataSource = DsValAberto
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label35: TLabel
          Left = 56
          Top = 0
          Width = 42
          Height = 13
          Caption = 'Vencido:'
        end
        object Label50: TLabel
          Left = 220
          Top = 0
          Width = 34
          Height = 13
          Caption = 'Aberto:'
        end
        object SpeedButton6: TSpeedButton
          Left = 506
          Top = 16
          Width = 21
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '-$'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton6Click
        end
        object Label52: TLabel
          Left = 380
          Top = 0
          Width = 30
          Height = 13
          Caption = 'Limite:'
        end
        object DBText3: TDBText
          Left = 416
          Top = 0
          Width = 105
          Height = 17
          DataField = 'LimiCred'
          DataSource = DsClientes
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label53: TLabel
          Left = 932
          Top = 226
          Width = 135
          Height = 13
          Caption = 'Canal de atendimento inicial:'
        end
        object SbAtndCanal: TSpeedButton
          Left = 1227
          Top = 242
          Width = 21
          Height = 21
          Caption = '...'
        end
        object EdCliente: TdmkEditCB
          Left = 12
          Top = 16
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cliente'
          UpdCampo = 'Cliente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdClienteChange
          OnRedefinido = EdClienteRedefinido
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 68
          Top = 16
          Width = 417
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsClientes
          ParentFont = False
          TabOrder = 1
          dmkEditCB = EdCliente
          QryCampo = 'Cliente'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdECComprou: TdmkEditCB
          Left = 529
          Top = 16
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ECComprou'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdECComprouChange
          DBLookupComboBox = CBECComprou
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBECComprou: TdmkDBLookupComboBox
          Left = 582
          Top = 16
          Width = 140
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DropDownRows = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          KeyField = 'Controle'
          ListField = 'Nome'
          ListSource = DsECComprou
          ParentFont = False
          TabOrder = 3
          dmkEditCB = EdECComprou
          QryCampo = 'ECComprou'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBECRetirou: TdmkDBLookupComboBox
          Left = 783
          Top = 16
          Width = 142
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          KeyField = 'Controle'
          ListField = 'Nome'
          ListSource = DsECRetirou
          ParentFont = False
          TabOrder = 5
          dmkEditCB = EdECRetirou
          QryCampo = 'ECRetirou'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdECRetirou: TdmkEditCB
          Left = 726
          Top = 16
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ECRetirou'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdECRetirouChange
          DBLookupComboBox = CBECRetirou
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdVendedor: TdmkEditCB
          Left = 10
          Top = 100
          Width = 44
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Vendedor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBVendedor
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBVendedor: TdmkDBLookupComboBox
          Left = 54
          Top = 100
          Width = 139
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          KeyField = 'Numero'
          ListField = 'Login'
          ListSource = DsSenhas
          ParentFont = False
          TabOrder = 8
          dmkEditCB = EdVendedor
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNumOC: TdmkEdit
          Left = 290
          Top = 145
          Width = 100
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'NumOC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdLocalObra: TdmkEdit
          Left = 197
          Top = 100
          Width = 724
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'LocalObra'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObs0: TdmkEdit
          Left = 10
          Top = 182
          Width = 300
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Obs0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObs1: TdmkEdit
          Left = 314
          Top = 182
          Width = 300
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 15
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Obs1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObs2: TdmkEdit
          Left = 619
          Top = 182
          Width = 302
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 16
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Obs2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdLocalCntat: TdmkEdit
          Left = 10
          Top = 144
          Width = 164
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'LocalCntat'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdLocalFone: TdmkEdit
          Left = 176
          Top = 144
          Width = 109
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtTelLongo
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'LocalFone'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEndCobra: TdmkEdit
          Left = 396
          Top = 145
          Width = 525
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 13
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'EndCobra'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEndCliente: TdmkEdit
          Left = 10
          Top = 58
          Width = 911
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'LocalObra'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGFormaCobrLoca: TdmkRadioGroup
          Left = 932
          Top = 184
          Width = 317
          Height = 37
          Caption = ' Forma de cobran'#231'a: '
          Columns = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemIndex = 1
          Items.Strings = (
            'N/D'
            'Devolu'#231#227'o'
            'Loca'#231#227'o')
          ParentFont = False
          TabOrder = 17
          QryCampo = 'FormaCobrLoca'
          UpdCampo = 'FormaCobrLoca'
          UpdType = utYes
          OldValor = 0
        end
        object GroupBox8: TGroupBox
          Left = 8
          Top = 208
          Width = 913
          Height = 57
          Caption = ' Cobran'#231'a de Aluguel Antecipada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 18
          object LaDtHrSai: TLabel
            Left = 10
            Top = 13
            Width = 64
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data retirada:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label36: TLabel
            Left = 182
            Top = 13
            Width = 143
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data prevista para devolu'#231#227'o:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object TPDtHrSai: TdmkEditDateTimePicker
            Left = 10
            Top = 28
            Width = 104
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 41131.000000000000000000
            Time = 0.724786689817847200
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DtHrSai'
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object EDDtHrSai: TdmkEdit
            Left = 115
            Top = 28
            Width = 60
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfLong
            HoraFormat = dmkhfShort
            Texto = '00:00'
            QryCampo = 'DtHrSai'
            UpdCampo = 'HoraIni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EDDtHrSaiRedefinido
          end
          object TPDtHrDevolver: TdmkEditDateTimePicker
            Left = 182
            Top = 28
            Width = 104
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 41131.000000000000000000
            Time = 0.724786689817847200
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DtHrDevolver'
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object EdDtHrDevolver: TdmkEdit
            Left = 287
            Top = 28
            Width = 60
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfLong
            HoraFormat = dmkhfShort
            Texto = '00:00'
            QryCampo = 'DtHrDevolver'
            UpdCampo = 'HoraIni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EDDtHrSaiRedefinido
          end
          object RGTipoAluguel: TdmkRadioGroup
            Left = 352
            Top = 12
            Width = 341
            Height = 38
            Caption = ' Periodicidade pr'#233' definida'
            Columns = 5
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ItemIndex = 0
            Items.Strings = (
              '?'
              'Dia'
              'Semana'
              'Quinzena'
              'M'#234's')
            ParentFont = False
            TabOrder = 4
            TabStop = True
            OnClick = RGTipoAluguelClick
            UpdType = utYes
            OldValor = 0
          end
          object BtForma: TBitBtn
            Left = 699
            Top = 17
            Width = 66
            Height = 32
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Forma'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnClick = BtFormaClick
          end
          object BtData: TBitBtn
            Left = 767
            Top = 17
            Width = 66
            Height = 32
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Data'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 6
            OnClick = BtDataClick
          end
        end
        object EdAtndCanal: TdmkEditCB
          Left = 932
          Top = 242
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 19
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'AtndCanal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAtndCanal
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBAtndCanal: TdmkDBLookupComboBox
          Left = 990
          Top = 242
          Width = 235
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsAtndCanal
          TabOrder = 20
          dmkEditCB = EdAtndCanal
          QryCampo = 'AtndCanal'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 1322
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label9: TLabel
          Left = 106
          Top = 4
          Width = 44
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Empresa:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label21: TLabel
          Left = 754
          Top = 4
          Width = 42
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Emiss'#227'o:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 480
          Top = 4
          Width = 43
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Contrato:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label16: TLabel
          Left = 10
          Top = 4
          Width = 32
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'N'#176' CT:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object EdEmpresa: TdmkEditCB
          Left = 152
          Top = 0
          Width = 40
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 192
          Top = 0
          Width = 285
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ParentFont = False
          TabOrder = 1
          dmkEditCB = EdEmpresa
          QryCampo = 'Filial'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object TPDataEmi: TdmkEditDateTimePicker
          Left = 800
          Top = 0
          Width = 104
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 41131.000000000000000000
          Time = 0.724786689817847200
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataEmi'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdHoraEmi: TdmkEdit
          Left = 907
          Top = 0
          Width = 60
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          QryCampo = 'HoraEmi'
          UpdCampo = 'HoraIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdContrato: TdmkEditCB
          Left = 526
          Top = 0
          Width = 40
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Contrato'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBContrato
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBContrato: TdmkDBLookupComboBox
          Left = 567
          Top = 0
          Width = 185
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContratos
          ParentFont = False
          TabOrder = 3
          dmkEditCB = EdContrato
          QryCampo = 'Contrato'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCodigo: TdmkEdit
          Left = 50
          Top = 0
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
    object PnEntiStatus: TPanel
      Left = 620
      Top = 309
      Width = 706
      Height = 293
      Align = alRight
      TabOrder = 3
      object DBGEntiStatus: TdmkDBGridZTO
        Left = 1
        Top = 1
        Width = 704
        Height = 291
        Align = alClient
        DataSource = DsEntiStatus
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'Status'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Status'
            Title.Caption = 'Descri'#231#227'o'
            Width = 860
            Visible = True
          end>
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 89
    Width = 1326
    Height = 674
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    object GBCntrl: TGroupBox
      Left = 0
      Top = 610
      Width = 1326
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 175
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 177
        Top = 15
        Width = 265
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 442
        Top = 15
        Width = 882
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 719
          Top = 0
          Width = 163
          Height = 47
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Fechar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 499
          Left = 2
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Atendimento'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtLocCItsLca: TBitBtn
          Tag = 497
          Left = 242
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Loca'#231#227'o'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtLocCItsLcaClick
        end
        object BtFat: TBitBtn
          Tag = 414
          Left = 602
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Faturamento'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFatClick
        end
        object BtLocCConIts: TBitBtn
          Tag = 335
          Left = 122
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Movimento'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtLocCConItsClick
        end
        object BtLocCItsSvc: TBitBtn
          Tag = 10085
          Left = 362
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Servi'#231'o'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtLocCItsSvcClick
        end
        object BtLocCItsVen: TBitBtn
          Tag = 1000115
          Left = 482
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Venda'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtLocCItsVenClick
        end
      end
    end
    object GBDados: TPanel
      Left = 0
      Top = 0
      Width = 1326
      Height = 113
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      ParentBackground = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 1
        Top = 1
        Width = 959
        Height = 111
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'Panel6'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Panel12: TPanel
          Left = 0
          Top = 0
          Width = 959
          Height = 23
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label20: TLabel
            Left = 105
            Top = 4
            Width = 44
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Empresa:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label5: TLabel
            Left = 658
            Top = 4
            Width = 42
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Emiss'#227'o:'
            FocusControl = DBEdit3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label31: TLabel
            Left = 512
            Top = 4
            Width = 33
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'N'#186' OC:'
            FocusControl = DBEdit14
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 813
            Top = 4
            Width = 29
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Baixa:'
            FocusControl = DBEdit4
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label1: TLabel
            Left = 5
            Top = 4
            Width = 36
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo:'
            FocusControl = DBEdCodigo
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object DBEdit3: TDBEdit
            Left = 704
            Top = 0
            Width = 104
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'DtHrEmi'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object DBEdit12: TDBEdit
            Left = 188
            Top = 1
            Width = 321
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_EMP'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object DBEdit13: TDBEdit
            Left = 156
            Top = 1
            Width = 33
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Filial'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object DBEdit14: TDBEdit
            Left = 548
            Top = 0
            Width = 104
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NumOC'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object DBEdit4: TDBEdit
            Left = 848
            Top = 0
            Width = 104
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'DtHrBxa_TXT'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
          end
          object DBEdCodigo: TdmkDBEdit
            Left = 45
            Top = 0
            Width = 56
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 5
            UpdType = utYes
            Alignment = taRightJustify
          end
        end
        object Panel14: TPanel
          Left = 0
          Top = 23
          Width = 959
          Height = 88
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label2: TLabel
            Left = 6
            Top = 4
            Width = 35
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente:'
            FocusControl = DBEdNome
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 601
            Top = 4
            Width = 79
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quem requisitou:'
            FocusControl = DBEdit1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TLabel
            Left = 784
            Top = 4
            Width = 73
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quem recebeu:'
            FocusControl = DBEdit2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label8: TLabel
            Left = 5
            Top = 26
            Width = 49
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vendedor:'
            FocusControl = DBEdit5
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label12: TLabel
            Left = 200
            Top = 26
            Width = 88
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Endere'#231'o da obra:'
            FocusControl = DBEdit8
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label13: TLabel
            Left = 5
            Top = 70
            Width = 70
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Observa'#231#227'o 1:'
            FocusControl = DBEdit9
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label14: TLabel
            Left = 320
            Top = 70
            Width = 70
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Observa'#231#227'o 2:'
            FocusControl = DBEdit10
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label15: TLabel
            Left = 638
            Top = 70
            Width = 70
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Observa'#231#227'o 3:'
            FocusControl = DBEdit11
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label10: TLabel
            Left = 6
            Top = 48
            Width = 40
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Contato:'
            FocusControl = DBEdit6
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label32: TLabel
            Left = 281
            Top = 48
            Width = 27
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fone:'
            FocusControl = DBEdit15
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label33: TLabel
            Left = 421
            Top = 48
            Width = 112
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Endere'#231'o da cobran'#231'a:'
            FocusControl = DBEdit16
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object DBEdNome: TdmkDBEdit
            Left = 96
            Top = 0
            Width = 501
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Color = clWhite
            DataField = 'NO_CLIENTE'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBEdit1: TDBEdit
            Left = 685
            Top = 0
            Width = 95
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_COMPRADOR'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object DBEdit2: TDBEdit
            Left = 860
            Top = 0
            Width = 92
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_RECEBEU'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object DBEdit5: TDBEdit
            Left = 61
            Top = 22
            Width = 135
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NO_LOGIN'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object DBEdit8: TDBEdit
            Left = 293
            Top = 22
            Width = 659
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'LocalObra'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
          end
          object DBEdit9: TDBEdit
            Left = 78
            Top = 66
            Width = 237
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Obs0'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 8
          end
          object DBEdit10: TDBEdit
            Left = 395
            Top = 66
            Width = 237
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Obs1'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 9
          end
          object DBEdit11: TDBEdit
            Left = 715
            Top = 66
            Width = 237
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Obs2'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 10
          end
          object DBEdit6: TDBEdit
            Left = 56
            Top = 44
            Width = 221
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'LocalCntat'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
          end
          object DBEdit15: TDBEdit
            Left = 312
            Top = 44
            Width = 104
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'LocalFone'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
          end
          object DBEdit16: TDBEdit
            Left = 540
            Top = 44
            Width = 412
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'EndCobra'
            DataSource = DsLocCConCab
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
          end
          object DBEdit27: TDBEdit
            Left = 47
            Top = 0
            Width = 49
            Height = 21
            DataField = 'Cliente'
            DataSource = DsLocCConCab
            TabOrder = 11
            OnDblClick = DBEdit27DblClick
          end
        end
      end
      object DBGLocIConIts: TDBGrid
        Left = 960
        Top = 1
        Width = 365
        Height = 111
        Align = alClient
        DataSource = DsLocCConIts
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = DBGLocIConItsDblClick
        OnMouseUp = DBGLocIConItsMouseUp
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_AtndCanal'
            Title.Caption = 'Canal'
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CtrID'
            Title.Caption = 'ID'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_IDTab'
            Title.Caption = 'Movimento'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_STATUS'
            Title.Caption = 'Status'
            Width = 63
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorStat'
            Title.Caption = 'Valor'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_nNF'
            Title.Caption = 'N'#176' NF'
            Width = 53
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatNum'
            Title.Caption = 'Fat.Num NF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatPedCab'
            Title.Caption = 'Pedido NF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_ide_mod'
            Title.Caption = 'Modelo NF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NF_Stat'
            Title.Caption = 'Status NF'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorBrto'
            Title.Caption = 'Valor bruto'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CondiOrca'
            Title.Caption = 'Condi'#231#227'o de pagamento or'#231'amento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NFCeide_nNF'
            Title.Caption = 'N'#176' NFe'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NFeFatNum'
            Title.Caption = 'Fat.Num NF-e'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NFeFatPedCab'
            Title.Caption = 'Pedido NF-e'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NFeide_mod'
            Title.Caption = 'Modelo NF-e'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NFeNF_Stat'
            Title.Caption = 'Status NF-e'
            Visible = True
          end>
      end
    end
    object PCMovimento: TPageControl
      Left = 0
      Top = 186
      Width = 1326
      Height = 424
      ActivePage = TabSheet5
      Align = alBottom
      TabOrder = 2
      OnChange = PCMovimentoChange
      OnChanging = PCMovimentoChanging
      object TabSheet1: TTabSheet
        Caption = ' Loca'#231#227'o '
        object Splitter1: TSplitter
          Left = 0
          Top = 75
          Width = 1318
          Height = 5
          Cursor = crVSplit
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          ExplicitTop = 104
          ExplicitWidth = 1256
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 1318
          Height = 75
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = ' Patrim'#244'nios principais: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object DBGLocIPatPri: TDBGrid
            Left = 2
            Top = 15
            Width = 1314
            Height = 58
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsLocIPatPri
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGLocIPatPriDblClick
            OnMouseUp = DBGLocIPatPriMouseUp
            Columns = <
              item
                Expanded = False
                FieldName = 'Item'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduz.'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PATRIMONIO'
                Title.Caption = 'Patrim'#244'nio'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_GGX'
                Title.Caption = 'Descri'#231#227'o do Patrim'#244'nio'
                Width = 292
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'BAIXA_TXT'
                Title.Caption = 'Baixou'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QtdeProduto'
                Title.Caption = 'Qtde Produto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorProduto'
                Title.Caption = '$ Produto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorDia'
                Title.Caption = '$ Dia'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorSem'
                Title.Caption = '$ Semana'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorQui'
                Title.Caption = '$ Quinzena'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMes'
                Title.Caption = '$ M'#234's'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'HrTolerancia_TXT'
                Title.Caption = 'Toler'#226'ncia'
                Width = 55
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QtdeDevolucao'
                Title.Caption = 'Qtd Devolvido'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'REFERENCIA'
                Title.Caption = 'Refer'#234'ncia'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TROCA_TXT'
                Title.Caption = 'Trocado'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SubstitutoLca'
                Title.Caption = 'Substituto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SubstituidoLca'
                Title.Caption = 'Substitu'#237'do'
                Visible = True
              end>
          end
        end
        object Panel15: TPanel
          Left = 0
          Top = 80
          Width = 1318
          Height = 316
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Splitter2: TSplitter
            Left = 637
            Top = 0
            Width = 6
            Height = 316
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ExplicitHeight = 287
          end
          object Panel13: TPanel
            Left = 643
            Top = 0
            Width = 675
            Height = 316
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object PCMateriais: TPageControl
              Left = 0
              Top = 0
              Width = 675
              Height = 316
              ActivePage = TabSheet3
              Align = alClient
              TabOrder = 0
              object TabSheet2: TTabSheet
                Caption = ' Material de Uso '
                object DBGLocIPatUso: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 667
                  Height = 288
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsLocIPatUso
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDblClick = DBGLocIPatUsoDblClick
                  OnMouseUp = DBGLocIPatUsoMouseUp
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'REFERENCIA'
                      Title.Caption = 'Refer'#234'ncia'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_GGX'
                      Title.Caption = 'Descri'#231#227'o'
                      Width = 276
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorRucAAdiantar'
                      Title.Caption = '$ Adiantamento'
                      Width = 68
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_AVALINI'
                      Title.Caption = 'Avalia. inicial'
                      Width = 74
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_AVALFIM'
                      Title.Caption = 'Avalia. final'
                      Width = 74
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PrcUni'
                      Title.Caption = 'Pre'#231'o'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValUso'
                      Title.Caption = 'Valor'
                      Width = 60
                      Visible = True
                    end>
                end
              end
              object TabSheet3: TTabSheet
                Caption = ' Material de Consumo '
                ImageIndex = 1
                object DBGLocIPatCns: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 667
                  Height = 288
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsLocIPatCns
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDblClick = DBGLocIPatCnsDblClick
                  OnMouseUp = DBGLocIPatCnsMouseUp
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'REFERENCIA'
                      Title.Caption = 'Refer'#234'ncia'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_GGX'
                      Title.Caption = 'Descri'#231#227'o'
                      Width = 276
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SIGLA'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorRucAAdiantar'
                      Title.Caption = '$ Adiantamento'
                      Width = 68
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdIni'
                      Title.Caption = 'Qtd. inicial'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdFim'
                      Title.Caption = 'Qtd. final'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdUso'
                      Title.Caption = 'Qtd usado'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValUso'
                      Title.Caption = 'Valor'
                      Width = 56
                      Visible = True
                    end>
                end
              end
              object TabSheet6: TTabSheet
                Caption = ' Material de Apoio '
                ImageIndex = 2
                object DBGLocIPatApo: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 667
                  Height = 288
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsLocIPatApo
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDblClick = DBGLocIPatApoDblClick
                  OnMouseUp = DBGLocIPatApoMouseUp
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'REFERENCIA'
                      Title.Caption = 'Refer'#234'ncia'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_GGX'
                      Title.Caption = 'Descri'#231#227'o'
                      Width = 276
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SIGLA'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdIni'
                      Title.Caption = 'Qtd. inicial'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdFim'
                      Title.Caption = 'Qtd. final'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdUso'
                      Title.Caption = 'Qtd usado'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValUso'
                      Title.Caption = 'Valor'
                      Width = 56
                      Visible = True
                    end>
                end
              end
            end
          end
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 637
            Height = 316
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object Splitter3: TSplitter
              Left = 0
              Top = 95
              Width = 637
              Height = 5
              Cursor = crVSplit
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              ExplicitTop = 77
            end
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 637
              Height = 95
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object GroupBox3: TGroupBox
                Left = 0
                Top = 0
                Width = 637
                Height = 95
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Caption = ' Patrim'#244'nios secund'#225'rios: '
                TabOrder = 0
                object DBGLocIPatSec: TDBGrid
                  Left = 2
                  Top = 15
                  Width = 633
                  Height = 78
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsLocIPatSec
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDblClick = DBGLocIPatSecDblClick
                  OnMouseUp = DBGLocIPatSecMouseUp
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'REFERENCIA'
                      Title.Caption = 'Refer'#234'ncia'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_GGX'
                      Title.Caption = 'Descri'#231#227'o'
                      Width = 154
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorLocacao'
                      Title.Caption = '$ Locacao'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QTDE'
                      Title.Caption = 'Qtde'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdeDevolucao'
                      Title.Caption = 'Qtd Devolvido'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorProduto'
                      Title.Caption = '$ Produto'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Item'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SubstitutoLca'
                      Title.Caption = 'Substituto'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SubstituidoLca'
                      Title.Caption = 'Substitu'#237'do'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdeLocacao'
                      Title.Caption = 'Qtde'
                      Width = 36
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QtdeProduto'
                      Title.Caption = 'Qtde Produto'
                      Visible = True
                    end>
                end
              end
            end
            object GroupBox5: TGroupBox
              Left = 0
              Top = 100
              Width = 637
              Height = 216
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = ' Acess'#243'rios: '
              TabOrder = 1
              object DBGLocIPatAce: TDBGrid
                Left = 2
                Top = 15
                Width = 633
                Height = 199
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsLocIPatAce
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDblClick = DBGLocIPatAceDblClick
                OnMouseUp = DBGLocIPatAceMouseUp
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'REFERENCIA'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_GGX'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 151
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorLocacao'
                    Title.Caption = '$ Locacao'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'QTDE'
                    Title.Caption = 'Qtde'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'QtdeDevolucao'
                    Title.Caption = 'Qtd. Devolvido'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorProduto'
                    Title.Caption = '$ Produto'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Item'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SubstituidoLca'
                    Title.Caption = 'Substitu'#237'do'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SubstitutoLca'
                    Title.Caption = 'Substituto'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'QtdeProduto'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'QtdeLocacao'
                    Title.Caption = 'Qtde'
                    Width = 38
                    Visible = True
                  end>
              end
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Servi'#231'o'
        ImageIndex = 1
        object DBLocCItsSvc: TDBGrid
          Left = 0
          Top = 0
          Width = 1318
          Height = 396
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsLocCItsSvc
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Item'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduz.'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'REFERENCIA'
              Title.Caption = 'Refer'#234'ncia'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_GGX'
              Title.Caption = 'Descri'#231#227'o do Patrim'#244'nio'
              Width = 457
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PrcUni'
              Title.Caption = '$ Unit'#225'rio'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Quantidade'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorTotal'
              Title.Caption = '$ TOTAL'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Atrelamento'
              Title.Caption = 'Atrelamento'
              Width = 100
              Visible = True
            end>
        end
      end
      object TabSheet5: TTabSheet
        Caption = ' Venda '
        ImageIndex = 2
        object Splitter4: TSplitter
          Left = 933
          Top = 0
          Height = 396
          Align = alRight
          ExplicitLeft = 1012
          ExplicitTop = -12
        end
        object DBGLocCItsVen: TDBGrid
          Left = 0
          Top = 0
          Width = 933
          Height = 396
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsLocCItsVen
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGLocCItsVenDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'SMIA_IDCtrl'
              Title.Caption = 'ID Baixa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Item'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduz.'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'REFERENCIA'
              Title.Caption = 'Refer'#234'ncia'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_GGX'
              Title.Caption = 'Descri'#231#227'o do Patrim'#244'nio'
              Width = 457
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PrcUni'
              Title.Caption = '$ Unit'#225'rio'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Quantidade'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PercDesco'
              Title.Caption = '% Desc.mercad.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorBruto'
              Title.Caption = '$ Bruto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SIGLA'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorTotal'
              Title.Caption = '$ TOTAL'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SMIA_OriCtrl'
              Title.Caption = 'SMIA OriCtrl'
              Visible = True
            end>
        end
        object DBGrid1: TDBGrid
          Left = 936
          Top = 0
          Width = 382
          Height = 396
          Align = alRight
          DataSource = DsSmiaVen
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Baixa_TXT'
              Title.Caption = 'Baixou?'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data/hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IDCtrl'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriCnta'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriCodi'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriCtrl'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'StqCencad'
              Title.Caption = 'Centro estq.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tipo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'vDesc'
              Title.Caption = '$ Desc.'
              Visible = True
            end>
        end
      end
      object TabSheet7: TTabSheet
        Caption = ' Observa'#231#245'es '
        ImageIndex = 4
        object Splitter7: TSplitter
          Left = 0
          Top = 89
          Width = 1318
          Height = 5
          Cursor = crVSplit
          Align = alTop
          ExplicitWidth = 1342
        end
        object Label48: TLabel
          Left = 0
          Top = 94
          Width = 271
          Height = 13
          Align = alTop
          Caption = ' Hist'#243'rico n'#227'o imprim'#237'vel. (D'#234' um duplo clique para editar)'
        end
        object DBMeHistImprime: TDBMemo
          Left = 0
          Top = 0
          Width = 1318
          Height = 89
          Align = alTop
          DataField = 'HistImprime'
          DataSource = DsLocCConCab
          TabOrder = 0
          OnDblClick = DBMeHistImprimeDblClick
        end
        object DBMemo2: TDBMemo
          Left = 0
          Top = 107
          Width = 1318
          Height = 289
          Align = alClient
          DataField = 'HistNaoImpr'
          DataSource = DsLocCConCab
          TabOrder = 1
          OnDblClick = DBMemo2DblClick
        end
      end
      object TabSheet8: TTabSheet
        Caption = ' Faturamento '
        ImageIndex = 5
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1318
          Height = 396
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 0
          object Splitter5: TSplitter
            Left = 1
            Top = 174
            Width = 1316
            Height = 6
            Cursor = crVSplit
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            ExplicitWidth = 1229
          end
          object GroupBox4: TGroupBox
            Left = 1
            Top = 1
            Width = 1316
            Height = 173
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Faturamento: '
            TabOrder = 0
            object DBGrid3: TDBGrid
              Left = 2
              Top = 15
              Width = 1312
              Height = 156
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsLocFCab
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_CartEmis'
                  Title.Caption = 'Carteira'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PediPrzCab'
                  Title.Caption = 'Condi'#231#227'o de pagamento'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtHrFat'
                  Title.Caption = 'Faturado em'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NumNF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValTotal'
                  Title.Caption = '$ TOTAL'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValLocad'
                  Title.Caption = '$ Loca'#231#227'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValUsado'
                  Title.Caption = '$ Uso'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValConsu'
                  Title.Caption = '$ Consumo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValVenda'
                  Title.Caption = '$ Ven.Aces.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValVeMaq'
                  Title.Caption = '$ Ven.Maq'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValServi'
                  Title.Caption = '$ Servi'#231'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValDesco'
                  Title.Caption = '$ Desconto'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ReciboImp'
                  Title.Caption = 'Recibo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end>
            end
          end
          object GroupBox7: TGroupBox
            Left = 1
            Top = 180
            Width = 1316
            Height = 215
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = ' Parcelas do faturamento selecionado: '
            TabOrder = 1
            object DBGLctFatRef: TDBGrid
              Left = 2
              Top = 15
              Width = 1312
              Height = 198
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsLctFatRef
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'PARCELA'
                  Title.Caption = 'Parc.'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Lancto'
                  Width = 82
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencto'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1326
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1270
      Top = 0
      Width = 56
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 12
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        PopupMenu = PMLocIPatAce
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 305
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object SbImagens: TBitBtn
        Tag = 102
        Left = 214
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 5
        OnClick = SbImagensClick
      end
      object BtNFSe: TBitBtn
        Tag = 533
        Left = 256
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtNFSeClick
      end
    end
    object GB_M: TGroupBox
      Left = 305
      Top = 0
      Width = 421
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 334
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 334
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 334
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Loca'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 726
      Top = 0
      Width = 544
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Caption = ' Avisos: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 540
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 16
          Top = 2
          Width = 505
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Para adicionar itens clique com o bot'#227'o direito do mouse sobre a' +
            ' grade correspondente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 15
          Top = 1
          Width = 505
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Para adicionar itens clique com o bot'#227'o direito do mouse sobre a' +
            ' grade correspondente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
      object PB1: TProgressBar
        Left = 2
        Top = 34
        Width = 540
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 1
        Visible = False
      end
    end
  end
  object PnTop: TPanel
    Left = 0
    Top = 52
    Width = 1326
    Height = 37
    Align = alTop
    BevelOuter = bvNone
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 2
    StyleElements = [seClient, seBorder]
    object PnInfoTop: TPanel
      Left = 0
      Top = 0
      Width = 1326
      Height = 37
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label37: TLabel
        Left = 96
        Top = 0
        Width = 84
        Height = 13
        Caption = 'Forma do aluguel:'
        FocusControl = DBEdit17
      end
      object Label38: TLabel
        Left = 4
        Top = 0
        Width = 33
        Height = 13
        Caption = 'Status:'
        FocusControl = DBEdNO_STATUS
      end
      object Label39: TLabel
        Left = 188
        Top = 0
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        FocusControl = DBEdit19
      end
      object Label40: TLabel
        Left = 292
        Top = 0
        Width = 85
        Height = 13
        Caption = 'Previs. devolu'#231#227'o'
        FocusControl = DBEdit20
      end
      object Label41: TLabel
        Left = 396
        Top = 0
        Width = 82
        Height = 13
        Caption = 'C'#225'lc. adiant. loc.:'
        FocusControl = DBEdit21
      end
      object Label42: TLabel
        Left = 488
        Top = 0
        Width = 85
        Height = 13
        Caption = #218'lt. atualiz. pend.:'
        FocusControl = DBEdit22
      end
      object Label43: TLabel
        Left = 580
        Top = 0
        Width = 83
        Height = 13
        Caption = 'Valor loc. atualiz.:'
        FocusControl = DBEdit23
      end
      object Label11: TLabel
        Left = 672
        Top = 0
        Width = 50
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Valor total:'
        FocusControl = DBEdit7
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label44: TLabel
        Left = 764
        Top = 0
        Width = 55
        Height = 13
        Caption = 'Or'#231'amento:'
        FocusControl = DBEdit18
      end
      object Label45: TLabel
        Left = 856
        Top = 0
        Width = 36
        Height = 13
        Caption = 'Pedido:'
        FocusControl = DBEdit24
      end
      object Label46: TLabel
        Left = 948
        Top = 0
        Width = 45
        Height = 13
        Caption = 'Faturado:'
        FocusControl = DBEdit25
      end
      object Label47: TLabel
        Left = 1040
        Top = 0
        Width = 28
        Height = 13
        Caption = 'Pago:'
        FocusControl = DBEdit26
      end
      object Label49: TLabel
        Left = 1132
        Top = 0
        Width = 54
        Height = 13
        Caption = 'Valor bruto:'
        FocusControl = DBEdit28
      end
      object Label51: TLabel
        Left = 1224
        Top = 0
        Width = 66
        Height = 13
        Caption = 'Valor a pagar:'
        FocusControl = DBEdit29
      end
      object DBEdit17: TDBEdit
        Left = 96
        Top = 16
        Width = 89
        Height = 21
        DataField = 'NO_TipoAluguel'
        DataSource = DsLocCConCab
        TabOrder = 0
      end
      object DBEdNO_STATUS: TDBEdit
        Left = 4
        Top = 16
        Width = 89
        Height = 21
        Color = clMoneyGreen
        DataField = 'NO_STATUS'
        DataSource = DsLocCConCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        StyleElements = [seClient, seBorder]
        OnChange = DBEdNO_STATUSChange
      end
      object DBEdit19: TDBEdit
        Left = 188
        Top = 16
        Width = 100
        Height = 21
        DataField = 'DtHrSai_TXT'
        DataSource = DsLocCConCab
        TabOrder = 2
      end
      object DBEdit20: TDBEdit
        Left = 292
        Top = 16
        Width = 100
        Height = 21
        DataField = 'DtHrDevolver_TXT'
        DataSource = DsLocCConCab
        TabOrder = 3
      end
      object DBEdit21: TDBEdit
        Left = 396
        Top = 16
        Width = 90
        Height = 21
        DataField = 'ValorLocAAdiantar'
        DataSource = DsLocCConCab
        TabOrder = 4
      end
      object DBEdit22: TDBEdit
        Left = 488
        Top = 16
        Width = 90
        Height = 21
        DataField = 'DtUltAtzPend'
        DataSource = DsLocCConCab
        TabOrder = 5
      end
      object DBEdit23: TDBEdit
        Left = 580
        Top = 16
        Width = 90
        Height = 21
        DataField = 'ValorLocAtualizado'
        DataSource = DsLocCConCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 672
        Top = 16
        Width = 90
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ValorTot'
        DataSource = DsLocCConCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
      end
      object DBEdit18: TDBEdit
        Left = 764
        Top = 16
        Width = 90
        Height = 21
        DataField = 'ValorOrca'
        DataSource = DsLocCConCab
        TabOrder = 8
      end
      object DBEdit24: TDBEdit
        Left = 856
        Top = 16
        Width = 90
        Height = 21
        DataField = 'ValorPedi'
        DataSource = DsLocCConCab
        TabOrder = 9
      end
      object DBEdit25: TDBEdit
        Left = 948
        Top = 16
        Width = 90
        Height = 21
        DataField = 'ValorFatu'
        DataSource = DsLocCConCab
        TabOrder = 10
      end
      object DBEdit26: TDBEdit
        Left = 1040
        Top = 16
        Width = 90
        Height = 21
        DataField = 'ValorPago'
        DataSource = DsLocCConCab
        TabOrder = 11
      end
      object DBEdit28: TDBEdit
        Left = 1132
        Top = 16
        Width = 90
        Height = 21
        DataField = 'ValorBrto'
        DataSource = DsLocCConCab
        TabOrder = 12
      end
      object DBEdit29: TDBEdit
        Left = 1224
        Top = 16
        Width = 90
        Height = 21
        DataField = 'ValorPndt'
        DataSource = DsLocCConCab
        TabOrder = 13
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 16
    Top = 4
  end
  object QrLocCConCab: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    BeforeOpen = QrLocCConCabBeforeOpen
    AfterOpen = QrLocCConCabAfterOpen
    BeforeClose = QrLocCConCabBeforeClose
    AfterScroll = QrLocCConCabAfterScroll
    OnCalcFields = QrLocCConCabCalcFields
    SQL.Strings = (
      'SELECT com.Nome NO_COMPRADOR, ret.Nome NO_RECEBEU,'
      'lcc.*, sen.Login NO_LOGIN, cin.CodCliInt Filial,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE'
      'FROM loccconcab lcc'
      'LEFT JOIN entidades emp ON emp.Codigo=lcc.Empresa'
      'LEFT JOIN enticliint cin ON cin.CodEnti=emp.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=lcc.Cliente'
      'LEFT JOIN senhas sen ON sen.Numero=lcc.Vendedor'
      'LEFT JOIN enticontat com ON com.Controle=lcc.ECComprou'
      'LEFT JOIN enticontat ret ON ret.Controle=lcc.ECRetirou'
      'WHERE lcc.Codigo > 0')
    Left = 28
    Top = 248
    object QrLocCConCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrLocCConCabNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrLocCConCabNO_COMPRADOR: TWideStringField
      FieldName = 'NO_COMPRADOR'
      Size = 30
    end
    object QrLocCConCabNO_RECEBEU: TWideStringField
      FieldName = 'NO_RECEBEU'
      Size = 30
    end
    object QrLocCConCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCConCabContrato: TIntegerField
      FieldName = 'Contrato'
    end
    object QrLocCConCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLocCConCabDtHrEmi: TDateTimeField
      FieldName = 'DtHrEmi'
      DisplayFormat = 'dd/mm/yyyy hh:nn'
    end
    object QrLocCConCabDtHrBxa: TDateTimeField
      FieldName = 'DtHrBxa'
      DisplayFormat = 'dd/mm/yyyy hh:nn'
    end
    object QrLocCConCabVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLocCConCabECComprou: TIntegerField
      FieldName = 'ECComprou'
    end
    object QrLocCConCabECRetirou: TIntegerField
      FieldName = 'ECRetirou'
    end
    object QrLocCConCabNumOC: TWideStringField
      FieldName = 'NumOC'
      Size = 60
    end
    object QrLocCConCabValorTot: TFloatField
      FieldName = 'ValorTot'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabLocalObra: TWideStringField
      DisplayWidth = 255
      FieldName = 'LocalObra'
      Size = 255
    end
    object QrLocCConCabObs0: TWideStringField
      FieldName = 'Obs0'
      Size = 50
    end
    object QrLocCConCabObs1: TWideStringField
      FieldName = 'Obs1'
      Size = 50
    end
    object QrLocCConCabObs2: TWideStringField
      FieldName = 'Obs2'
      Size = 50
    end
    object QrLocCConCabNO_LOGIN: TWideStringField
      FieldName = 'NO_LOGIN'
      Required = True
      Size = 30
    end
    object QrLocCConCabNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrLocCConCabDataEmi: TDateField
      FieldKind = fkCalculated
      FieldName = 'DataEmi'
      LookupDataSet = Dmod.QrAgora
      Calculated = True
    end
    object QrLocCConCabHoraEmi: TTimeField
      FieldKind = fkCalculated
      FieldName = 'HoraEmi'
      Calculated = True
    end
    object QrLocCConCabDtHrBxa_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DtHrBxa_TXT'
      Calculated = True
    end
    object QrLocCConCabEndCobra: TWideStringField
      FieldName = 'EndCobra'
      Size = 100
    end
    object QrLocCConCabLocalCntat: TWideStringField
      FieldName = 'LocalCntat'
      Size = 50
    end
    object QrLocCConCabLocalFone: TWideStringField
      FieldName = 'LocalFone'
    end
    object QrLocCConCabFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrLocCConCabTipoAluguel: TWideStringField
      FieldName = 'TipoAluguel'
      Size = 1
    end
    object QrLocCConCabHistImprime: TWideMemoField
      FieldName = 'HistImprime'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocCConCabHistNaoImpr: TWideMemoField
      FieldName = 'HistNaoImpr'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrLocCConCabDtHrSai: TDateTimeField
      FieldName = 'DtHrSai'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn'
    end
    object QrLocCConCabValorAdicional: TFloatField
      FieldName = 'ValorAdicional'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorAdiantamento: TFloatField
      FieldName = 'ValorAdiantamento'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorDesconto: TFloatField
      FieldName = 'ValorDesconto'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrLocCConCabDtUltCobMens: TDateField
      FieldName = 'DtUltCobMens'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn'
    end
    object QrLocCConCabImportado: TSmallintField
      FieldName = 'Importado'
      Required = True
    end
    object QrLocCConCabDtHrDevolver: TDateTimeField
      FieldName = 'DtHrDevolver'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn'
    end
    object QrLocCConCabValorEquipamentos: TFloatField
      FieldName = 'ValorEquipamentos'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabFormaCobrLoca: TSmallintField
      FieldName = 'FormaCobrLoca'
      Required = True
    end
    object QrLocCConCabNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 30
    end
    object QrLocCConCabNO_TipoAluguel: TWideStringField
      FieldName = 'NO_TipoAluguel'
      Size = 30
    end
    object QrLocCConCabDtHrSai_TXT: TWideStringField
      FieldName = 'DtHrSai_TXT'
    end
    object QrLocCConCabDtHrDevolver_TXT: TWideStringField
      FieldName = 'DtHrDevolver_TXT'
    end
    object QrLocCConCabDtUltAtzPend: TDateField
      FieldName = 'DtUltAtzPend'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrLocCConCabValorASerAdiantado: TFloatField
      FieldName = 'ValorASerAdiantado'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorProdutos: TFloatField
      FieldName = 'ValorProdutos'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorLocAAdiantar: TFloatField
      FieldName = 'ValorLocAAdiantar'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorLocAtualizado: TFloatField
      FieldName = 'ValorLocAtualizado'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorServicos: TFloatField
      FieldName = 'ValorServicos'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorPago: TFloatField
      FieldName = 'ValorPago'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorVendas: TFloatField
      FieldName = 'ValorVendas'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorOrca: TFloatField
      FieldName = 'ValorOrca'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorPedi: TFloatField
      FieldName = 'ValorPedi'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorFatu: TFloatField
      FieldName = 'ValorFatu'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabValorBrto: TFloatField
      FieldName = 'ValorBrto'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabJaLocou: TSmallintField
      FieldName = 'JaLocou'
    end
    object QrLocCConCabValorPndt: TFloatField
      FieldName = 'ValorPndt'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConCabAtndCanal: TIntegerField
      FieldName = 'AtndCanal'
    end
  end
  object DsLocCConCab: TDataSource
    DataSet = QrLocCConCab
    Left = 32
    Top = 296
  end
  object QrLocIPatPri: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    OnCalcFields = QrLocIPatPriCalcFields
    SQL.Strings = (
      
        'SELECT lpp.*, gg1.Nome NO_GGX, gg1.Patrimonio, gg1.REFERENCIA, s' +
        'en.LOGIN, '
      'IF(lpp.Troca>0, "SIM", "") TROCA_TXT,'
      'IF(QtdeLocacao=1, "SIM", "N'#227'o") BAIXA_TXT '
      'FROM loccitslca lpp '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci '
      'WHERE lpp.Codigo=:P0'
      '')
    Left = 200
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocIPatPriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocIPatPriCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocIPatPriGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocIPatPriValorDia: TFloatField
      FieldName = 'ValorDia'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocIPatPriValorSem: TFloatField
      FieldName = 'ValorSem'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocIPatPriValorQui: TFloatField
      FieldName = 'ValorQui'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocIPatPriValorMes: TFloatField
      FieldName = 'ValorMes'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocIPatPriDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
    end
    object QrLocIPatPriLibFunci: TIntegerField
      FieldName = 'LibFunci'
    end
    object QrLocIPatPriLibDtHr: TDateTimeField
      FieldName = 'LibDtHr'
    end
    object QrLocIPatPriRELIB: TWideStringField
      FieldName = 'RELIB'
    end
    object QrLocIPatPriHOLIB: TWideStringField
      FieldName = 'HOLIB'
      Size = 5
    end
    object QrLocIPatPriNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocIPatPriLOGIN: TWideStringField
      FieldName = 'LOGIN'
      Required = True
      Size = 30
    end
    object QrLocIPatPriREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
    object QrLocIPatPriTXT_DTA_DEVOL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DTA_DEVOL'
      Calculated = True
    end
    object QrLocIPatPriTXT_DTA_LIBER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DTA_LIBER'
      Size = 30
      Calculated = True
    end
    object QrLocIPatPriTXT_QEM_LIBER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_QEM_LIBER'
      Size = 100
      Calculated = True
    end
    object QrLocIPatPriDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
    end
    object QrLocIPatPriLibExUsr: TIntegerField
      FieldName = 'LibExUsr'
    end
    object QrLocIPatPriRetFunci: TIntegerField
      FieldName = 'RetFunci'
      Required = True
    end
    object QrLocIPatPriRetExUsr: TIntegerField
      FieldName = 'RetExUsr'
      Required = True
    end
    object QrLocIPatPriValorProduto: TFloatField
      FieldName = 'ValorProduto'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocIPatPriQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrLocIPatPriValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocIPatPriQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrLocIPatPriTroca: TSmallintField
      FieldName = 'Troca'
      Required = True
    end
    object QrLocIPatPriCategoria: TWideStringField
      FieldName = 'Categoria'
      Required = True
      Size = 1
    end
    object QrLocIPatPriCobrancaConsumo: TFloatField
      FieldName = 'CobrancaConsumo'
      Required = True
    end
    object QrLocIPatPriCobrancaRealizadaVenda: TWideStringField
      FieldName = 'CobrancaRealizadaVenda'
      Required = True
      Size = 1
    end
    object QrLocIPatPriSaiDtHr: TDateTimeField
      FieldName = 'SaiDtHr'
      Required = True
    end
    object QrLocIPatPriQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrLocIPatPriCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 40
    end
    object QrLocIPatPriCobranIniDtHr: TDateTimeField
      FieldName = 'CobranIniDtHr'
      Required = True
    end
    object QrLocIPatPriItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocIPatPriManejoLca: TSmallintField
      FieldName = 'ManejoLca'
    end
    object QrLocIPatPriPATRIMONIO: TWideStringField
      FieldName = 'PATRIMONIO'
      Size = 25
    end
    object QrLocIPatPriTROCA_TXT: TWideStringField
      FieldName = 'TROCA_TXT'
      Size = 3
    end
    object QrLocIPatPriSubstituidoLca: TIntegerField
      FieldName = 'SubstituidoLca'
      DisplayFormat = '0;-0; '
    end
    object QrLocIPatPriSubstitutoLca: TIntegerField
      FieldName = 'SubstitutoLca'
      DisplayFormat = '0;-0; '
    end
    object QrLocIPatPriBAIXA_TXT: TWideStringField
      FieldName = 'BAIXA_TXT'
      Size = 3
    end
    object QrLocIPatPriHrTolerancia: TTimeField
      FieldName = 'HrTolerancia'
      DisplayFormat = 'hh:nn'
    end
    object QrLocIPatPriHrTolerancia_TXT: TWideStringField
      FieldName = 'HrTolerancia_TXT'
      Size = 5
    end
  end
  object DsLocIPatPri: TDataSource
    DataSet = QrLocIPatPri
    Left = 200
    Top = 296
  end
  object PMLocIItsLca: TPopupMenu
    OnPopup = PMLocIItsLcaPopup
    Left = 732
    Top = 632
    object ItsInclui1: TMenuItem
      Caption = '&Inclui'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Devolues4: TMenuItem
      Caption = '&Devolu'#231#245'es'
      OnClick = Devolues4Click
    end
    object Retorna1: TMenuItem
      Caption = '&Retorna'
      Enabled = False
      Visible = False
      OnClick = Retorna1Click
    end
    object Libera1: TMenuItem
      Caption = '&Libera'
      Enabled = False
      Visible = False
      OnClick = Libera1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ItsExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N5: TMenuItem
      Caption = '-'
      Visible = False
    end
    object ItsExclui2: TMenuItem
      Caption = 'Exclui re&torno do item selecionado'
      Enabled = False
      Visible = False
      OnClick = ItsExclui2Click
    end
    object ItsExclui3: TMenuItem
      Caption = 'Exclui li&bera'#231#227'o do item selecionado'
      Enabled = False
      Visible = False
      OnClick = ItsExclui3Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 484
    Top = 640
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object Renovar1: TMenuItem
      Caption = '&Renovar'
      OnClick = Renovar1Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Mudastatus1: TMenuItem
      Caption = '&Muda status para...'
      object MudastatusparaLocado1: TMenuItem
        Caption = '&Locado'
        Enabled = False
        OnClick = MudastatusparaLocado1Click
      end
      object MudastatusparaAberto1: TMenuItem
        Caption = '&Aberto'
        Enabled = False
        OnClick = MudastatusparaAberto1Click
      end
      object Finalizado1: TMenuItem
        Caption = '&Finalizado'
        OnClick = Finalizado1Click
      end
      object Cancelado1: TMenuItem
        Caption = '&Cancelado'
        OnClick = Cancelado1Click
      end
      object Suspenso1: TMenuItem
        Caption = '&Suspenso'
        OnClick = Suspenso1Click
      end
    end
    object Devolvertodosequipamentos1: TMenuItem
      Caption = '&Devolver todos equipamentos e finalizar'
      OnClick = Devolvertodosequipamentos1Click
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object Recalculatudo1: TMenuItem
      Caption = 'Re&calcula tudo'
      object Agora1: TMenuItem
        Caption = 'Agora'
        OnClick = Agora1Click
      end
      object Agoramostrandoclculo1: TMenuItem
        Caption = 'Agora mostrando c'#225'lculo'
        OnClick = Agoramostrandoclculo1Click
      end
      object Outradata1: TMenuItem
        Caption = 'Outra data'
        OnClick = Outradata1Click
      end
      object Outradatamostrandoclculo1: TMenuItem
        Caption = 'Outra data mostrando c'#225'lculo'
        OnClick = Outradatamostrandoclculo1Click
      end
      object N16: TMenuItem
        Caption = '-'
      end
      object Redefinir1: TMenuItem
        Caption = '&Redefinir'
        OnClick = Redefinir1Click
      end
    end
    object Alteratolerncia1: TMenuItem
      Caption = 'Toler'#226'ncia'
      OnClick = Alteratolerncia1Click
    end
  end
  object QrLocIPatSec: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN '
      'FROM loccitslca lpp '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ')
    Left = 272
    Top = 248
    object QrLocIPatSecCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocIPatSecCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocIPatSecItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocIPatSecGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocIPatSecNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocIPatSecREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
    object QrLocIPatSecValorDia: TFloatField
      FieldName = 'ValorDia'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocIPatSecValorSem: TFloatField
      FieldName = 'ValorSem'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocIPatSecValorQui: TFloatField
      FieldName = 'ValorQui'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocIPatSecValorMes: TFloatField
      FieldName = 'ValorMes'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocIPatSecDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrLocIPatSecDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrLocIPatSecQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrLocIPatSecValorProduto: TFloatField
      FieldName = 'ValorProduto'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocIPatSecQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrLocIPatSecValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocIPatSecQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrLocIPatSecManejoLca: TSmallintField
      FieldName = 'ManejoLca'
    end
    object QrLocIPatSecSubstituidoLca: TIntegerField
      FieldName = 'SubstituidoLca'
      DisplayFormat = '0;-0; '
    end
    object QrLocIPatSecSubstitutoLca: TIntegerField
      FieldName = 'SubstitutoLca'
      DisplayFormat = '0;-0; '
    end
    object QrLocIPatSecQTDE: TFloatField
      FieldName = 'QTDE'
    end
  end
  object DsLocIPatSec: TDataSource
    DataSet = QrLocIPatSec
    Left = 272
    Top = 296
  end
  object QrLocIPatAce: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN '
      'FROM loccitslca lpp '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ')
    Left = 352
    Top = 248
    object QrLocIPatAceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocIPatAceCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocIPatAceItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocIPatAceGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocIPatAceNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocIPatAceREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
    object QrLocIPatAceQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrLocIPatAceValorProduto: TFloatField
      FieldName = 'ValorProduto'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocIPatAceQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrLocIPatAceValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocIPatAceQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrLocIPatAceDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrLocIPatAceDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrLocIPatAceManejoLca: TSmallintField
      FieldName = 'ManejoLca'
    end
    object QrLocIPatAceSubstituidoLca: TIntegerField
      FieldName = 'SubstituidoLca'
      DisplayFormat = '0;-0; '
    end
    object QrLocIPatAceSubstitutoLca: TIntegerField
      FieldName = 'SubstitutoLca'
      DisplayFormat = '0;-0; '
    end
    object QrLocIPatAceQTDE: TFloatField
      FieldName = 'QTDE'
    end
  end
  object DsLocIPatAce: TDataSource
    DataSet = QrLocIPatAce
    Left = 352
    Top = 296
  end
  object QrLocIPatCns: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    AfterOpen = QrLocIPatCnsAfterOpen
    OnCalcFields = QrLocIPatCnsCalcFields
    SQL.Strings = (
      'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,'
      'med.SIGLA '
      'FROM loccpatcns lpc '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade'
      'WHERE lpc.CtrID>0')
    Left = 436
    Top = 244
    object QrLocIPatCnsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocIPatCnsCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocIPatCnsItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocIPatCnsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocIPatCnsNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocIPatCnsREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
    object QrLocIPatCnsUnidade: TIntegerField
      FieldName = 'Unidade'
    end
    object QrLocIPatCnsQtdIni: TFloatField
      FieldName = 'QtdIni'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLocIPatCnsQtdFim: TFloatField
      FieldName = 'QtdFim'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLocIPatCnsQtdUso: TFloatField
      FieldName = 'QtdUso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLocIPatCnsPrcUni: TFloatField
      FieldName = 'PrcUni'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrLocIPatCnsValUso: TFloatField
      FieldName = 'ValUso'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocIPatCnsSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrLocIPatCnsValorRucAAdiantar: TFloatField
      FieldName = 'ValorRucAAdiantar'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsLocIPatCns: TDataSource
    DataSet = QrLocIPatCns
    Left = 436
    Top = 292
  end
  object QrLocIPatUso: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    AfterOpen = QrLocIPatUsoAfterOpen
    SQL.Strings = (
      'SELECT lpu.*, gg1.Nome NO_GGX, gg1.REFERENCIA,'
      'med.SIGLA, avi.Nome NO_AVALINI, avf.Nome NO_AVALFIM '
      'FROM loccpatuso lpu '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpu.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed med ON med.Codigo=lpu.Unidade'
      'LEFT JOIN graglaval avi ON avi.Codigo=lpu.AvalIni'
      'LEFT JOIN graglaval avf ON avf.Codigo=lpu.AvalFim'
      'WHERE lpu.CtrID>0'
      '')
    Left = 512
    Top = 244
    object QrLocIPatUsoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'loccpatuso.Codigo'
    end
    object QrLocIPatUsoCtrID: TIntegerField
      FieldName = 'CtrID'
      Origin = 'loccpatuso.CtrID'
    end
    object QrLocIPatUsoItem: TIntegerField
      FieldName = 'Item'
      Origin = 'loccpatuso.Item'
    end
    object QrLocIPatUsoGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'loccpatuso.GraGruX'
    end
    object QrLocIPatUsoNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrLocIPatUsoREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Origin = 'gragru1.Referencia'
      Required = True
      Size = 25
    end
    object QrLocIPatUsoAvalIni: TIntegerField
      FieldName = 'AvalIni'
      Origin = 'loccpatuso.AvalIni'
    end
    object QrLocIPatUsoAvalFim: TIntegerField
      FieldName = 'AvalFim'
      Origin = 'loccpatuso.AvalFim'
    end
    object QrLocIPatUsoPrcUni: TFloatField
      FieldName = 'PrcUni'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocIPatUsoValUso: TFloatField
      FieldName = 'ValUso'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocIPatUsoUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'loccpatuso.Unidade'
    end
    object QrLocIPatUsoSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
    object QrLocIPatUsoNO_AVALINI: TWideStringField
      FieldName = 'NO_AVALINI'
      Origin = 'graglaval.Nome'
      Size = 60
    end
    object QrLocIPatUsoNO_AVALFIM: TWideStringField
      FieldName = 'NO_AVALFIM'
      Origin = 'graglaval.Nome'
      Size = 60
    end
    object QrLocIPatUsoValorRucAAdiantar: TFloatField
      FieldName = 'ValorRucAAdiantar'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsLocIPatUso: TDataSource
    DataSet = QrLocIPatUso
    Left = 512
    Top = 292
  end
  object QrLocFCab: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    BeforeClose = QrLocFCabBeforeClose
    AfterScroll = QrLocFCabAfterScroll
    SQL.Strings = (
      'SELECT lfc.*'
      'FROM locfcab lfc'
      'WHERE lfc.Codigo>0'
      ''
      '')
    Left = 820
    Top = 244
    object QrLocFCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocFCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocFCabDtHrFat: TDateTimeField
      FieldName = 'DtHrFat'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrLocFCabValLocad: TFloatField
      FieldName = 'ValLocad'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabValConsu: TFloatField
      FieldName = 'ValConsu'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabValUsado: TFloatField
      FieldName = 'ValUsado'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabValDesco: TFloatField
      FieldName = 'ValDesco'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabValTotal: TFloatField
      FieldName = 'ValTotal'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabSerNF: TWideStringField
      FieldName = 'SerNF'
      Size = 3
    end
    object QrLocFCabNumNF: TIntegerField
      FieldName = 'NumNF'
      DisplayFormat = '000000'
    end
    object QrLocFCabCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
    end
    object QrLocFCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrLocFCabValVenda: TFloatField
      FieldName = 'ValVenda'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabValVeMaq: TFloatField
      FieldName = 'ValVeMaq'
    end
    object QrLocFCabValFrete: TFloatField
      FieldName = 'ValFrete'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabValServi: TFloatField
      FieldName = 'ValServi'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFCabCodHist: TIntegerField
      FieldName = 'CodHist'
    end
    object QrLocFCabNO_CartEmis: TWideStringField
      FieldName = 'NO_CartEmis'
      Size = 100
    end
    object QrLocFCabNO_PediPrzCab: TWideStringField
      FieldName = 'NO_PediPrzCab'
      Size = 60
    end
    object QrLocFCabReciboImp: TIntegerField
      FieldName = 'ReciboImp'
    end
  end
  object DsLocFCab: TDataSource
    DataSet = QrLocFCab
    Left = 820
    Top = 292
  end
  object DsLctFatRef: TDataSource
    DataSet = QrLctFatRef
    Left = 892
    Top = 296
  end
  object QrLctFatRef: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    OnCalcFields = QrLctFatRefCalcFields
    SQL.Strings = (
      'SELECT lfi.*'
      'FROM locfits lfi'
      'WHERE lfi.Controle>0'
      '')
    Left = 892
    Top = 244
    object QrLctFatRefPARCELA: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'PARCELA'
      Calculated = True
    end
    object QrLctFatRefCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLctFatRefControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctFatRefConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrLctFatRefLancto: TLargeintField
      FieldName = 'Lancto'
    end
    object QrLctFatRefValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctFatRefVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 404
    Top = 112
  end
  object QrClientes: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Ativo = 1'
      'ORDER BY NOMEENTIDADE')
    Left = 404
    Top = 68
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrClientesLimiCred: TFloatField
      FieldName = 'LimiCred'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object QrECComprou: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM enticontat'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 460
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrECComprouControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrECComprouNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsECComprou: TDataSource
    DataSet = QrECComprou
    Left = 488
    Top = 68
  end
  object QrECRetirou: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM enticontat'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 516
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Controle'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsECRetirou: TDataSource
    DataSet = QrECRetirou
    Left = 544
    Top = 68
  end
  object QrSenhas: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT Numero, login'
      'FROM senhas'
      'WHERE Ativo=1'
      'ORDER BY Login')
    Left = 572
    Top = 68
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrSenhaslogin: TWideStringField
      FieldName = 'login'
      Required = True
      Size = 30
    end
  end
  object DsSenhas: TDataSource
    DataSet = QrSenhas
    Left = 600
    Top = 68
  end
  object QrContratos: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contratos'
      'ORDER BY Nome')
    Left = 628
    Top = 68
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 656
    Top = 68
  end
  object QrLcts: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    Left = 264
    Top = 104
  end
  object PMFat: TPopupMenu
    OnPopup = PMFatPopup
    Left = 1080
    Top = 632
    object Faturamentoparcial1: TMenuItem
      Caption = 'Faturamento parcial'
      Enabled = False
      OnClick = Faturamentoparcial1Click
    end
    object Faturaeencerracontrato1: TMenuItem
      Caption = 'Fatura e encerra contrato'
      Enabled = False
      OnClick = Faturaeencerracontrato1Click
    end
    object Encerracontrato1: TMenuItem
      Caption = 'Encerra contrato'
      Enabled = False
      OnClick = Encerracontrato1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Excluifaturamento1: TMenuItem
      Caption = 'Exclui todo faturamento selecionado e reabre loca'#231#227'o'
      OnClick = Excluifaturamento1Click
    end
    object Reabrelocao1: TMenuItem
      Caption = 'Reabre loca'#231#227'o'
      OnClick = Reabrelocao1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Gerabloqueto1: TMenuItem
      Caption = 'Emite boleto(s) do faturamento selecionado'
      OnClick = Gerabloqueto1Click
    end
    object Visualizarbloquetos1: TMenuItem
      Caption = 'Visualizar boletos'
      OnClick = Visualizarbloquetos1Click
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object Imprimerecibo1: TMenuItem
      Caption = 'Imprime Recibo'
      OnClick = Imprimerecibo1Click
    end
  end
  object VUEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PnEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 276
    Top = 65528
  end
  object PMLocIPatAce: TPopupMenu
    OnPopup = PMLocIPatAcePopup
    Left = 996
    Top = 268
    object Adicionaacessrio1: TMenuItem
      Caption = '&Adiciona acess'#243'rio'
      OnClick = Adicionaacessrio1Click
    end
    object Removeacessrio1: TMenuItem
      Caption = '&Remove acess'#243'rio'
      OnClick = Removeacessrio1Click
    end
    object Devolues3: TMenuItem
      Caption = '&Devolu'#231#245'es / Substititui'#231#245'es'
      OnClick = Devolues3Click
    end
  end
  object PMLocIPatUso: TPopupMenu
    OnPopup = PMLocIPatUsoPopup
    Left = 1004
    Top = 116
    object Adicionamaterialuso1: TMenuItem
      Caption = '&Adiciona material uso'
      OnClick = Adicionamaterialuso1Click
    end
    object Removematerialuso1: TMenuItem
      Caption = '&Remove material uso'
      OnClick = Removematerialuso1Click
    end
  end
  object PMLocIPatCns: TPopupMenu
    OnPopup = PMLocIPatCnsPopup
    Left = 1004
    Top = 68
    object Adicionamaterialdeconsumo1: TMenuItem
      Caption = '&Adiciona material de consumo'
      OnClick = Adicionamaterialdeconsumo1Click
    end
    object Removematerialdeconsumo1: TMenuItem
      Caption = '&Remove material de consumo'
      OnClick = Removematerialdeconsumo1Click
    end
  end
  object PMImagens: TPopupMenu
    OnPopup = PMImagensPopup
    Left = 828
    Top = 8
    object Visualizarimagemdocliente1: TMenuItem
      Caption = 'Visualizar imagem do &cliente'
      OnClick = Visualizarimagemdocliente1Click
    end
    object Visualizarimagemdorequisitante1: TMenuItem
      Caption = 'Visualizar imagem do &requisitante'
      OnClick = Visualizarimagemdorequisitante1Click
    end
    object Visualizarimagemdorecebedor1: TMenuItem
      Caption = 'Visualizar imagem do r&ecebedor'
      OnClick = Visualizarimagemdorecebedor1Click
    end
  end
  object PMImprime: TPopupMenu
    OnPopup = PMImprimePopup
    Left = 152
    Top = 4
    object Contratofonte71: TMenuItem
      Caption = 'Contrato fonte &7'
      OnClick = Contratofonte71Click
    end
    object Contratofonte101: TMenuItem
      Caption = 'Contrato fonte &10'
      OnClick = Contratofonte101Click
    end
    object Contrato1: TMenuItem
      Caption = '-'
    end
    object Devoluodelocao1: TMenuItem
      Caption = '&Devolu'#231#227'o de loca'#231#227'o'
      object Selecionados1: TMenuItem
        Caption = '&A devolver'
        object Fonte101: TMenuItem
          Caption = 'Fonte &10'
          OnClick = Fonte101Click
        end
        object Fonte71: TMenuItem
          Caption = 'Fonte &7'
          OnClick = Fonte71Click
        end
      end
      object odos1: TMenuItem
        Caption = '&Todos'
        object Fonte102: TMenuItem
          Caption = 'Fonte &10'
          OnClick = Fonte102Click
        end
        object Fonte72: TMenuItem
          Caption = 'Fonte &7'
          OnClick = Fonte72Click
        end
      end
    end
    object OramentodeLocao1: TMenuItem
      Caption = 'Or'#231'amento de &Loca'#231#227'o'
      OnClick = OramentodeLocao1Click
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object Oramentodevenda1: TMenuItem
      Caption = '&Or'#231'amento de venda'
      OnClick = Oramentodevenda1Click
    end
    object Pedidodevenda1: TMenuItem
      Caption = '&Pedido de venda'
      OnClick = Pedidodevenda1Click
    end
    object N13: TMenuItem
      Caption = '-'
    end
    object Devolues5: TMenuItem
      Caption = 'De&volu'#231#245'es'
      OnClick = Devolues5Click
    end
    object Fichadetroca1: TMenuItem
      Caption = 'Ficha de troca'
      object Equipamentoprincipal1: TMenuItem
        Caption = 'Equipamento principal'
        OnClick = Equipamentoprincipal1Click
      end
      object Equipamentosecundrio1: TMenuItem
        Caption = 'Equipamento secund'#225'rio'
        OnClick = Equipamentosecundrio1Click
      end
      object Acessrio1: TMenuItem
        Caption = 'Acess'#243'rio'
        OnClick = Acessrio1Click
      end
      object Indefinido1: TMenuItem
        Caption = 'Indefinido'
        OnClick = Indefinido1Click
      end
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 88
    Top = 65535
  end
  object QrLoc: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    Left = 336
    Top = 96
  end
  object QrLocCConIts: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    BeforeClose = QrLocCConItsBeforeClose
    AfterScroll = QrLocCConItsAfterScroll
    SQL.Strings = (
      'SELECT ELT(its.IDTab+1, "Indefinido",'
      '"Loca'#231#227'o", "Servi'#231'o", "Venda", "? ? ?") NO_IDTab,'
      'ELT(its.Status+1,  "Indefinido",'
      
        '"Or'#231'amento", "Pedido", "Fatura", "Cancelado", "? ? ?") NO_Status' +
        ','
      'CASE its.Status'
      '  WHEN 1 THEN its.ValorOrca'
      '  WHEN 2 THEN its.ValorPedi'
      '  WHEN 3 THEN its.ValorFatu'
      'ELSE 0 END ValorStat, ppc.Nome NO_CondiOrca,'
      'IF(ide_nNF <> 0,'
      '  CASE its.ide_mod'
      '    WHEN 55 THEN "NF-e"'
      '    WHEN 65 THEN "NFC-e"'
      '  ELSE "?????" END,'
      '"S/NF") NO_ide_mod,'
      'its.*'
      'FROM loccconits its'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=its.CondiOrca'
      '')
    Left = 124
    Top = 246
    object QrLocCConItsNO_IDTab: TWideStringField
      FieldName = 'NO_IDTab'
      Size = 10
    end
    object QrLocCConItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocCConItsCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrLocCConItsIDTab: TIntegerField
      FieldName = 'IDTab'
      Required = True
    end
    object QrLocCConItsStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrLocCConItsNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 15
    end
    object QrLocCConItsValorBrto: TFloatField
      FieldName = 'ValorBrto'
      DisplayFormat = '#,###,###,##0.00; -#,###,###,##0.00; '
    end
    object QrLocCConItsValorOrca: TFloatField
      FieldName = 'ValorOrca'
      Required = True
      DisplayFormat = '#,###,###,##0.00; -#,###,###,##0.00; '
    end
    object QrLocCConItsValorPedi: TFloatField
      FieldName = 'ValorPedi'
      Required = True
      DisplayFormat = '#,###,###,##0.00; -#,###,###,##0.00; '
    end
    object QrLocCConItsValorFatu: TFloatField
      FieldName = 'ValorFatu'
      Required = True
      DisplayFormat = '#,###,###,##0.00; -#,###,###,##0.00; '
    end
    object QrLocCConItsValorStat: TFloatField
      FieldName = 'ValorStat'
      DisplayFormat = '#,###,###,##0.00; -#,###,###,##0.00; '
    end
    object QrLocCConItsCondiOrca: TIntegerField
      FieldName = 'CondiOrca'
    end
    object QrLocCConItsNO_CondiOrca: TWideStringField
      FieldName = 'NO_CondiOrca'
      Size = 50
    end
    object QrLocCConItsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrLocCConItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocCConItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocCConItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrLocCConItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrLocCConItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLocCConItsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrLocCConItsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrLocCConItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLocCConItsFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrLocCConItsFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrLocCConItside_mod: TIntegerField
      FieldName = 'ide_mod'
      Required = True
    end
    object QrLocCConItside_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Required = True
    end
    object QrLocCConItsNF_Stat: TIntegerField
      FieldName = 'NF_Stat'
      Required = True
    end
    object QrLocCConItsPediVda: TIntegerField
      FieldName = 'PediVda'
      Required = True
    end
    object QrLocCConItsFatPedCab: TIntegerField
      FieldName = 'FatPedCab'
      Required = True
    end
    object QrLocCConItsFatPedVolCnta: TIntegerField
      FieldName = 'FatPedVolCnta'
      Required = True
    end
    object QrLocCConItsNO_ide_mod: TWideStringField
      FieldName = 'NO_ide_mod'
      Size = 10
    end
    object QrLocCConItsNO_AtndCanal: TWideStringField
      FieldName = 'NO_AtndCanal'
      Size = 60
    end
    object QrLocCConItsAtndCanal: TIntegerField
      FieldName = 'AtndCanal'
    end
  end
  object DsLocCConIts: TDataSource
    DataSet = QrLocCConIts
    Left = 124
    Top = 296
  end
  object QrLocIPatApo: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    AfterOpen = QrLocIPatApoAfterOpen
    SQL.Strings = (
      'SELECT lpu.*, gg1.Nome NO_GGX, gg1.REFERENCIA,'
      'med.SIGLA, avi.Nome NO_AVALINI, avf.Nome NO_AVALFIM '
      'FROM loccpatuso lpu '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpu.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed med ON med.Codigo=lpu.Unidade'
      'LEFT JOIN graglaval avi ON avi.Codigo=lpu.AvalIni'
      'LEFT JOIN graglaval avf ON avf.Codigo=lpu.AvalFim'
      'WHERE lpu.CtrID>0'
      '')
    Left = 600
    Top = 244
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
      Origin = 'loccpatuso.Codigo'
    end
    object IntegerField3: TIntegerField
      FieldName = 'CtrID'
      Origin = 'loccpatuso.CtrID'
    end
    object IntegerField4: TIntegerField
      FieldName = 'Item'
      Origin = 'loccpatuso.Item'
    end
    object IntegerField5: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'loccpatuso.GraGruX'
    end
    object WideStringField1: TWideStringField
      FieldName = 'NO_GGX'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object WideStringField2: TWideStringField
      FieldName = 'REFERENCIA'
      Origin = 'gragru1.Referencia'
      Required = True
      Size = 25
    end
    object IntegerField6: TIntegerField
      FieldName = 'AvalIni'
      Origin = 'loccpatuso.AvalIni'
    end
    object IntegerField7: TIntegerField
      FieldName = 'AvalFim'
      Origin = 'loccpatuso.AvalFim'
    end
    object FloatField1: TFloatField
      FieldName = 'PrcUni'
      DisplayFormat = '#,###,###,##0.00'
    end
    object FloatField2: TFloatField
      FieldName = 'ValUso'
      DisplayFormat = '#,###,###,##0.00'
    end
    object IntegerField8: TIntegerField
      FieldName = 'Unidade'
      Origin = 'loccpatuso.Unidade'
    end
    object WideStringField3: TWideStringField
      FieldName = 'SIGLA'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
  end
  object DsLocIPatApo: TDataSource
    DataSet = QrLocIPatApo
    Left = 600
    Top = 292
  end
  object QrLocCItsSvc: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT ELT(lpp.Atrelamento+1, "Indefinido", '
      '"Nenhum", "Loca'#231#227'o", "Venda", "? ? ?") '
      'NO_Atrelamento, '
      'lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA'
      'FROM loccitssvc lpp '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE lpp.Codigo>0'
      'AND lpp.CtrID>0'
      'AND lpp.ManejoLca>0')
    Left = 684
    Top = 244
    object QrLocCItsSvcCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocCItsSvcCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrLocCItsSvcItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrLocCItsSvcManejoLca: TSmallintField
      FieldName = 'ManejoLca'
      Required = True
    end
    object QrLocCItsSvcGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrLocCItsSvcQuantidade: TFloatField
      FieldName = 'Quantidade'
      Required = True
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrLocCItsSvcPrcUni: TFloatField
      FieldName = 'PrcUni'
      Required = True
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrLocCItsSvcValorTotal: TFloatField
      FieldName = 'ValorTotal'
      Required = True
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrLocCItsSvcQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrLocCItsSvcValorProduto: TFloatField
      FieldName = 'ValorProduto'
      Required = True
    end
    object QrLocCItsSvcQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrLocCItsSvcValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
    end
    object QrLocCItsSvcQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
    end
    object QrLocCItsSvcTroca: TSmallintField
      FieldName = 'Troca'
      Required = True
    end
    object QrLocCItsSvcCategoria: TWideStringField
      FieldName = 'Categoria'
      Required = True
      Size = 1
    end
    object QrLocCItsSvcCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 40
    end
    object QrLocCItsSvcCobrancaConsumo: TFloatField
      FieldName = 'CobrancaConsumo'
      Required = True
    end
    object QrLocCItsSvcCobrancaRealizadaVenda: TWideStringField
      FieldName = 'CobrancaRealizadaVenda'
      Required = True
      Size = 1
    end
    object QrLocCItsSvcCobranIniDtHr: TDateTimeField
      FieldName = 'CobranIniDtHr'
      Required = True
    end
    object QrLocCItsSvcSaiDtHr: TDateTimeField
      FieldName = 'SaiDtHr'
      Required = True
    end
    object QrLocCItsSvcLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrLocCItsSvcDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocCItsSvcDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocCItsSvcUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrLocCItsSvcUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrLocCItsSvcAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLocCItsSvcAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrLocCItsSvcAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrLocCItsSvcAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLocCItsSvcNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocCItsSvcREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Size = 25
    end
    object QrLocCItsSvcNO_Atrelamento: TWideStringField
      FieldName = 'NO_Atrelamento'
      Size = 30
    end
  end
  object DsLocCItsSvc: TDataSource
    DataSet = QrLocCItsSvc
    Left = 684
    Top = 292
  end
  object QrLocCItsVen: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    BeforeClose = QrLocCItsVenBeforeClose
    AfterScroll = QrLocCItsVenAfterScroll
    SQL.Strings = (
      'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,'
      'med.SIGLA '
      'FROM loccitsven lpc '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade')
    Left = 756
    Top = 244
    object QrLocCItsVenCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocCItsVenCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrLocCItsVenItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrLocCItsVenManejoLca: TSmallintField
      FieldName = 'ManejoLca'
      Required = True
    end
    object QrLocCItsVenGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrLocCItsVenUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrLocCItsVenQuantidade: TFloatField
      FieldName = 'Quantidade'
      Required = True
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrLocCItsVenPrcUni: TFloatField
      FieldName = 'PrcUni'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCItsVenValorTotal: TFloatField
      FieldName = 'ValorTotal'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCItsVenQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrLocCItsVenValorProduto: TFloatField
      FieldName = 'ValorProduto'
      Required = True
    end
    object QrLocCItsVenQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrLocCItsVenValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
    end
    object QrLocCItsVenQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
    end
    object QrLocCItsVenTroca: TSmallintField
      FieldName = 'Troca'
      Required = True
    end
    object QrLocCItsVenCategoria: TWideStringField
      FieldName = 'Categoria'
      Required = True
      Size = 1
    end
    object QrLocCItsVenCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 40
    end
    object QrLocCItsVenCobrancaConsumo: TFloatField
      FieldName = 'CobrancaConsumo'
      Required = True
    end
    object QrLocCItsVenCobrancaRealizadaVenda: TWideStringField
      FieldName = 'CobrancaRealizadaVenda'
      Required = True
      Size = 1
    end
    object QrLocCItsVenCobranIniDtHr: TDateTimeField
      FieldName = 'CobranIniDtHr'
      Required = True
    end
    object QrLocCItsVenSaiDtHr: TDateTimeField
      FieldName = 'SaiDtHr'
      Required = True
    end
    object QrLocCItsVenLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrLocCItsVenDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocCItsVenDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocCItsVenUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrLocCItsVenUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrLocCItsVenAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLocCItsVenAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrLocCItsVenAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrLocCItsVenAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLocCItsVenNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocCItsVenREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Size = 25
    end
    object QrLocCItsVenSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrLocCItsVenSMIA_OriCtrl: TIntegerField
      FieldName = 'SMIA_OriCtrl'
      Required = True
    end
    object QrLocCItsVenSMIA_IDCtrl: TIntegerField
      FieldName = 'SMIA_IDCtrl'
      Required = True
    end
    object QrLocCItsVenGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrLocCItsVenprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocCItsVenprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocCItsVenprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocCItsVenprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocCItsVenStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrLocCItsVenPercDesco: TFloatField
      FieldName = 'PercDesco'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrLocCItsVenValorBruto: TFloatField
      FieldName = 'ValorBruto'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsLocCItsVen: TDataSource
    DataSet = QrLocCItsVen
    Left = 756
    Top = 292
  end
  object PMLocIConIts: TPopupMenu
    OnPopup = PMLocIConItsPopup
    Left = 604
    Top = 630
    object IncluiMovimento1: TMenuItem
      Caption = '&Inclui Movimento'
      OnClick = IncluiMovimento1Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Alteracondiodepagamentodooramento1: TMenuItem
      Caption = 'Altera &condi'#231#227'o de pagamento do or'#231'amento '
      OnClick = Alteracondiodepagamentodooramento1Click
    end
    object AlteraStatusdoMovimento1: TMenuItem
      Caption = '&Altera o status do movimento para ...'
      object Oramento2: TMenuItem
        Caption = '&Or'#231'amento'
        OnClick = Oramento2Click
      end
      object Pedido2: TMenuItem
        Caption = '&Pedido'
        OnClick = Pedido2Click
      end
      object Cancelado2: TMenuItem
        Caption = '&Cancelado'
        OnClick = Cancelado2Click
      end
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object ExcluiMovimento1: TMenuItem
      Caption = '&Exclui Movimento'
      OnClick = ExcluiMovimento1Click
    end
    object ExcluiMovimentoetodosseusitens1: TMenuItem
      Caption = 'Exclui Movimento e &Todos seus itens'
      OnClick = ExcluiMovimentoetodosseusitens1Click
    end
    object TMenuItem
      Caption = '-'
    end
    object Atualizatotaidomovimento1: TMenuItem
      Caption = 'At&ualiza totais do movimento'
      OnClick = Atualizatotaidomovimento1Click
    end
  end
  object PMLocIPatApo: TPopupMenu
    OnPopup = PMLocIPatApoPopup
    Left = 1004
    Top = 24
    object AdicionamaterialdeApoio1: TMenuItem
      Caption = '&Adiciona material de Apoio'
      OnClick = AdicionamaterialdeApoio1Click
    end
    object RemovematerialdeApoio1: TMenuItem
      Caption = '&Remove material de Apoio'
      OnClick = RemovematerialdeApoio1Click
    end
  end
  object PMLocIPatSec: TPopupMenu
    OnPopup = PMLocIPatSecPopup
    Left = 996
    Top = 220
    object AdicionaequipamentoSecundrio1: TMenuItem
      Caption = '&Adiciona equipamento Secund'#225'rio'
      OnClick = AdicionaequipamentoSecundrio1Click
    end
    object RemoveequipamentoSecundrio1: TMenuItem
      Caption = '&Remove equipamento Secund'#225'rio'
      OnClick = RemoveequipamentoSecundrio1Click
    end
    object Devolues2: TMenuItem
      Caption = '&Devolu'#231#245'es / Substititui'#231#245'es'
      OnClick = Devolues2Click
    end
  end
  object QrItsLca: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM loccitslca'
      'WHERE Codigo>0')
    Left = 432
    Top = 360
    object QrItsLcaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrItsLcaCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrItsLcaItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrItsLcaManejoLca: TSmallintField
      FieldName = 'ManejoLca'
      Required = True
    end
    object QrItsLcaGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrItsLcaValorDia: TFloatField
      FieldName = 'ValorDia'
      Required = True
    end
    object QrItsLcaValorSem: TFloatField
      FieldName = 'ValorSem'
      Required = True
    end
    object QrItsLcaValorQui: TFloatField
      FieldName = 'ValorQui'
      Required = True
    end
    object QrItsLcaValorMes: TFloatField
      FieldName = 'ValorMes'
      Required = True
    end
    object QrItsLcaRetFunci: TIntegerField
      FieldName = 'RetFunci'
      Required = True
    end
    object QrItsLcaRetExUsr: TIntegerField
      FieldName = 'RetExUsr'
      Required = True
    end
    object QrItsLcaDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
      Required = True
    end
    object QrItsLcaDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
      Required = True
    end
    object QrItsLcaLibExUsr: TIntegerField
      FieldName = 'LibExUsr'
      Required = True
    end
    object QrItsLcaLibFunci: TIntegerField
      FieldName = 'LibFunci'
      Required = True
    end
    object QrItsLcaLibDtHr: TDateTimeField
      FieldName = 'LibDtHr'
      Required = True
    end
    object QrItsLcaRELIB: TWideStringField
      FieldName = 'RELIB'
    end
    object QrItsLcaHOLIB: TWideStringField
      FieldName = 'HOLIB'
      Size = 5
    end
    object QrItsLcaQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrItsLcaValorProduto: TFloatField
      FieldName = 'ValorProduto'
      Required = True
    end
    object QrItsLcaQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrItsLcaValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
    end
    object QrItsLcaQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
    end
    object QrItsLcaTroca: TSmallintField
      FieldName = 'Troca'
      Required = True
    end
    object QrItsLcaCategoria: TWideStringField
      FieldName = 'Categoria'
      Required = True
      Size = 1
    end
    object QrItsLcaCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 40
    end
    object QrItsLcaCobrancaConsumo: TFloatField
      FieldName = 'CobrancaConsumo'
      Required = True
    end
    object QrItsLcaCobrancaRealizadaVenda: TWideStringField
      FieldName = 'CobrancaRealizadaVenda'
      Required = True
      Size = 1
    end
    object QrItsLcaCobranIniDtHr: TDateTimeField
      FieldName = 'CobranIniDtHr'
      Required = True
    end
    object QrItsLcaSaiDtHr: TDateTimeField
      FieldName = 'SaiDtHr'
      Required = True
    end
    object QrItsLcaDMenos: TIntegerField
      FieldName = 'DMenos'
    end
    object QrItsLcaValorFDS: TFloatField
      FieldName = 'ValorFDS'
    end
    object QrItsLcaValorLocAtualizado: TFloatField
      FieldName = 'ValorLocAtualizado'
    end
    object QrItsLcaValDevolParci: TFloatField
      FieldName = 'ValDevolParci'
    end
    object QrItsLcaOriSubstSaiDH: TDateTimeField
      FieldName = 'OriSubstSaiDH'
    end
    object QrItsLcaHrTolerancia: TTimeField
      FieldName = 'HrTolerancia'
    end
  end
  object PMLocIItsSvc: TPopupMenu
    OnPopup = PMLocIItsSvcPopup
    Left = 838
    Top = 624
    object AdicionaServico1: TMenuItem
      Caption = 'Adiciona Servi'#231'o'
      OnClick = AdicionaServico1Click
    end
    object RemoveServico1: TMenuItem
      Caption = 'Remove Servi'#231'o'
      OnClick = RemoveServico1Click
    end
  end
  object PMLocIItsVen: TPopupMenu
    OnPopup = PMLocIItsVenPopup
    Left = 964
    Top = 624
    object AdicionaVenda1: TMenuItem
      Caption = 'Adiciona Venda'
      OnClick = AdicionaVenda1Click
    end
    object Removevenda1: TMenuItem
      Caption = 'Remove venda'
      OnClick = Removevenda1Click
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object EmiteNFCe2: TMenuItem
      Caption = 'Emite CUPOM Fiscal (NFC-e)'
      OnClick = EmiteNFCe2Click
    end
    object EmiteNFe1: TMenuItem
      Caption = 'Emite NOTA fiscal (NF-e)'
      OnClick = EmiteNFe1Click
    end
    object N15: TMenuItem
      Caption = '-'
    end
    object MMCSnapIn2: TMenuItem
      Caption = 'MMC Snap In'
      OnClick = MMCSnapIn2Click
    end
  end
  object PMLocIPatPri: TPopupMenu
    Left = 1000
    Top = 171
    object Devolues1: TMenuItem
      Caption = 'Devolu'#231#245'es / Substititui'#231#245'es'
      OnClick = Devolues1Click
    end
    object olerncia1: TMenuItem
      Caption = 'Toler'#226'ncia'
      OnClick = olerncia1Click
    end
  end
  object QrEntiStatus: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT IF(est.Status > 0, "SIM", "N'#195'O")'
      'SELECI, ste.Codigo Status, ste.Nome NO_Status'
      'FROM statusenti ste '
      'LEFT JOIN entiStatus est ON ste.Codigo=est.Status '
      'AND est.Codigo =10'
      'ORDER BY NO_Status')
    Left = 912
    Top = 524
    object QrEntiStatusSELECI: TWideStringField
      FieldName = 'SELECI'
      Required = True
      Size = 3
    end
    object QrEntiStatusStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrEntiStatusNO_Status: TWideStringField
      FieldName = 'NO_Status'
      Required = True
      Size = 255
    end
  end
  object DsEntiStatus: TDataSource
    DataSet = QrEntiStatus
    Left = 912
    Top = 572
  end
  object QrPsqSum: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    Left = 252
    Top = 559
  end
  object QrEndContratante: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      
        'SELECT mun.Nome NO_CIDADE, pai.Nome NO_PAIS, ent.Codigo, ent.Tip' +
        'o,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATANTE,'
      'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,'
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,'
      'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,'
      'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,'
      'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,'
      '/*IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,*/'
      'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,'
      'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,'
      'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX,'
      'IF(ent.Tipo=0, ent.ECep, ent.PCEP) + 0.000 CEP'
      'FROM entidades ent'
      'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF'
      'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF'
      'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd'
      'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd'
      ''
      
        'LEFT JOIN loctoolrall.dtb_munici mun ON mun.Codigo = IF(ent.Tipo' +
        '=0, ent.ECodMunici, ent.PCodMunici)'
      
        'LEFT JOIN loctoolrall.bacen_pais pai ON pai.Codigo = IF(ent.Tipo' +
        '=0, ent.ECodiPais, ent.PCodiPais)'
      ''
      'WHERE ent.Codigo>0'
      'ORDER BY CONTRATANTE')
    Left = 716
    Top = 188
    object QrEndContratanteCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEndContratanteTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrEndContratanteNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEndContratanteNOMERUA: TWideStringField
      DisplayWidth = 60
      FieldName = 'NOMERUA'
      Size = 60
    end
    object QrEndContratanteCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEndContratanteBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEndContratanteNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEndContratanteCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEndContratanteIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEndContratanteTEL: TWideStringField
      FieldName = 'TEL'
    end
    object QrEndContratanteFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEndContratanteCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEndContratanteTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrEndContratanteCONTRATANTE: TWideStringField
      FieldName = 'CONTRATANTE'
      Size = 100
    end
    object QrEndContratanteFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrEndContratanteCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Calculated = True
    end
    object QrEndContratanteCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEndContratanteNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrEndContratanteNO_CIDADE: TWideStringField
      FieldName = 'NO_CIDADE'
      Size = 100
    end
    object QrEndContratanteNO_PAIS: TWideStringField
      FieldName = 'NO_PAIS'
      Size = 100
    end
  end
  object QrAux: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    Left = 292
    Top = 444
  end
  object QrLocIts: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    Left = 620
    Top = 436
    object QrLocItsCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocItsFatPedCab: TIntegerField
      FieldName = 'FatPedCab'
    end
    object QrLocItsStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrLocItsNF_Stat: TIntegerField
      FieldName = 'NF_Stat'
    end
  end
  object QrValAberto: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    AfterOpen = QrValAbertoAfterOpen
    Left = 552
    Top = 186
    object QrValAbertoAberto: TFloatField
      FieldName = 'Aberto'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrValAbertoVencido: TFloatField
      FieldName = 'Vencido'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsValAberto: TDataSource
    DataSet = QrValAberto
    Left = 552
    Top = 236
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 876
    Top = 462
  end
  object PMNFs: TPopupMenu
    OnPopup = PMNFsPopup
    Left = 948
    Top = 105
    object EmiteNFCe1: TMenuItem
      Caption = 'Emite CUPOM Fiscal (NFC-e)'
      OnClick = EmiteNFCe1Click
    end
    object EmiteNFe2: TMenuItem
      Caption = 'Emite NOTA Fiscal (NF-e)'
      OnClick = EmiteNFe2Click
    end
    object N14: TMenuItem
      Caption = '-'
    end
    object MMCSnapIn1: TMenuItem
      Caption = 'MMC Snap In'
      OnClick = MMCSnapIn1Click
    end
  end
  object QrAtndCanal: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM atndcanal'
      'ORDER BY Nome')
    Left = 244
    Top = 172
    object QrAtndCanalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtndCanalNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsAtndCanal: TDataSource
    DataSet = QrAtndCanal
    Left = 244
    Top = 216
  end
  object QrSmiaVen: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSmiaVenAfterOpen
    SQL.Strings = (
      'SELECT DataHora, IDCtrl, Tipo, OriCodi, '
      'OriCtrl, OriCnta, Empresa, StqCencad,'
      'GraGruX, Qtde, Baixa, vDesc'
      '/* FatorClas, UnidMed, ValiStq   */'
      'FROM stqmovitsa'
      'WHERE IDCtrl=18515')
    Left = 764
    Top = 343
    object QrSmiaVenDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrSmiaVenIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object QrSmiaVenTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrSmiaVenOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Required = True
    end
    object QrSmiaVenOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
      Required = True
    end
    object QrSmiaVenOriCnta: TIntegerField
      FieldName = 'OriCnta'
      Required = True
    end
    object QrSmiaVenEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrSmiaVenStqCencad: TIntegerField
      FieldName = 'StqCencad'
      Required = True
    end
    object QrSmiaVenGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrSmiaVenQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrSmiaVenBaixa: TSmallintField
      FieldName = 'Baixa'
      Required = True
    end
    object QrSmiaVenvDesc: TFloatField
      FieldName = 'vDesc'
      Required = True
    end
    object QrSmiaVenBaixa_TXT: TWideStringField
      FieldName = 'Baixa_TXT'
      Size = 4
    end
  end
  object DsSmiaVen: TDataSource
    DataSet = QrSmiaVen
    Left = 760
    Top = 391
  end
end
