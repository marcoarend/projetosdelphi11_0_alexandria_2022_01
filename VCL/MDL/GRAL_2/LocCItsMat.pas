unit LocCItsMat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB, Variants,
  dmkDBLookupComboBox, dmkGeral, DmkDAC_PF, dmkImage, UnDmkEnums, AppListas,
  Vcl.ComCtrls, dmkEditDateTimePicker, UnAppEnums;

type
  TFmLocCItsMat = class(TForm)
    PainelDados: TPanel;
    DsGraGXToll: TDataSource;
    QrGraGXToll: TMySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    SBCadastro: TSpeedButton;
    CBGraGruX: TdmkDBLookupComboBox;
    Label7: TLabel;
    Label2: TLabel;
    EdReferencia: TdmkEdit;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    QrGraGXTollControle: TIntegerField;
    QrGraGXTollReferencia: TWideStringField;
    QrGraGXTollNO_GG1: TWideStringField;
    QrGraGXTollGraGruX: TIntegerField;
    QrGraGXTollItemValr: TFloatField;
    QrGraGXTollItemUnid: TIntegerField;
    CkContinuar: TCheckBox;
    Label21: TLabel;
    TPDataLoc: TdmkEditDateTimePicker;
    EdHoraLoc: TdmkEdit;
    EdQtdeProduto: TdmkEdit;
    LaQtdeProduto: TLabel;
    LaQtdeLocacao: TLabel;
    EdQtdeLocacao: TdmkEdit;
    DbEdEstqSdo: TDBEdit;
    Label16: TLabel;
    EdPatrimonio: TdmkEdit;
    Label4: TLabel;
    QrGraGXTollEstqSdo: TFloatField;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure SBCadastroClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdReferenciaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdPatrimonioChange(Sender: TObject);
    procedure EdPatrimonioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    //procedure ReopenGraGXToll();
    //procedure PesquisaPorGraGruX();
    //procedure PesquisaPorPatrimonio(Limpa: Boolean);
    //procedure PesquisaPorReferencia(Limpa: Boolean);
    //function  ObtemTabela(): String;
  public
    { Public declarations }
    FQrLocIPatSec, FQrLocIPatApo, FQrLocIPatCns, FQrLocIPatUso, FQrLocIPatAce: TmySQLQuery;
    FTipo: TGraGXToolRnt;
    FGraGruY, FCodigo, FCtrID, FEmpresa: Integer;
    FDtHrLocado, FCaption, FTipoAluguel: String;
  end;

var
  FmLocCItsMat: TFmLocCItsMat;

implementation

uses UnMyObjects, Module, Principal, UnGraL_Jan,
  (*LocCCon,*) LocCConCab, ModProd, UnAppPF, MeuDBUses;

{$R *.DFM}

var
  FCategoria: String;

procedure TFmLocCItsMat.BtOKClick(Sender: TObject);
var
  GraGruX, GraGruY, ManejoLca: Integer;
  //
{
  procedure IncluiSec(Codigo, CtrID: Integer);
  var
    GraGruX, Item, ManejoLca, GraGruY, DMenos: Integer;
    ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS: Double;
    Categoria: String;
    QtdeLocacao, QtdeProduto: Integer;
    ValorProduto, ValorLocacao: Double;
    SQLType: TSQLType;
    SdoFuturo, SaldoMudado: Double;
  begin
    SQLType := ImgTipo.SQLType;
(*&�%$
    if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
    begin
      if MyObjects.FIC(QrGraGXTollNO_SITAPL.Value = 0, nil,
        'A situa��o atual deste patrim�nio n�o permite sua loca��o!')
      then
        Exit;
    end;
*)
    //

    //Codigo         := FQrCab.FieldByName('Codigo').AsInteger;
    //CtrID          := EdCtrID.ValueVariant;
    GraGruX       := EdGraGruX.ValueVariant;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXComEstq, Dmod.MyDB, [
    'SELECT gg1.Nome, ggx.GraGruY, ptr.*',
    'FROM gragxpatr ptr ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=ptr.GraGruX',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE ptr.GraGruX=' + Geral.FF0(GraGruX),
    'AND ggx.Ativo=1 ',
    '']);
    //
    if QrGraGXComEstq.RecordCount > 0 then
    begin
      //GraGruX        := QrGraGXComEstqGraGruX.Value;
      ValorDia       := QrGraGXComEstqValorDia.Value;
      ValorSem       := QrGraGXComEstqValorSem.Value;
      ValorQui       := QrGraGXComEstqValorQui.Value;
      ValorMes       := QrGraGXComEstqValorMes.Value;
      ValorFDS       := QrGraGXComEstqValorFDS.Value;
      DMenos         := QrGraGXComEstqDMenos.Value;
      // Fazer no Cab? DtHrLocado     := Geral.FDT(Trunc(TPDataLoc.Date), 1) + ' ' + EdHoraLoc.Text;
    (*  Dados somente ap�s inclus�o (Versao AAPA)
      RetFunci       := ;
      RetExUsr       := ;
      DtHrRetorn     := ;
      LibExUsr       := ;
      LibFunci       := ;
      LibDtHr        := ;
      RELIB          := ;
      HOLIB          := ;
    *)
      ValorProduto   := QrGraGXComEstqAtualValr.Value;    // Pre�o de venda para indeniza��o
      QtdeLocacao    := EdQtdeLocacao.ValueVariant;
      QtdeProduto    := EdQtdeProduto.ValueVariant;
      //ValorLocacao   := / EdValorLocacao.ValueVariant;
      if FTipoAluguel = 'D' then
        ValorLocacao := ValorDia
      else
      if FTipoAluguel = 'S' then
        ValorLocacao := ValorSem
      else
      if FTipoAluguel = 'Q' then
        ValorLocacao := ValorQui
      else
      if FTipoAluguel = 'M' then
        ValorLocacao := ValorMes
      else
      begin
        Geral.MB_Erro('"Tipo de Aluguel" n�o definido! (2048)');
        ValorLocacao := 0;
      end;

      //QtdeDevolucao  := ;
      //Troca          := ;
      Categoria      := 'L'; // Loca��o
      //CobrancaConsumo:= ; ??? valor quando definido igual ao ValorLocacao
    (*
     SELECT its.CATEGORIA,
      CASE
      WHEN VALORPRODUTO = 0 THEN "VP=0"
      WHEN VALORPRODUTO=COBRANCACONSUMO THEN "VP=CC"
      ELSE "???" END VPxCC,
    COUNT(its.PRODUTO) ITENS
    FROM locacaoitens its
    WHERE its.COBRANCACONSUMO <> 0
    GROUP BY CATEGORIA, VPxCC
    *)
      //CobrancaRealizadaVenda:= ; "S" quando cobrou venda!
      // Fazer no Cab? SaiDtHr        := Geral.FDT(Trunc(TPSaiDtHr.Date), 1) + ' ' + EdSaiDtHr.Text; // := Geral.FDT(FDtHrSai, 109);
      //COBRANCALOCACAO:= '2 M - 1 Q - 1 S - 1 D';
      // Fazer no Cab? CobranIniDtHr  := Geral.FDT(Trunc(TPCobranIniDtHr.Date), 1) + ' ' + EdCobranIniDtHr.Text; ???
      //
  (*
      if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe um patrim�nio principal!') then
        Exit;
  *)
      if Dmod.QrOpcoesTRenPermLocSemEstq.Value = 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrEstq, Dmod.MyDB, [
        'SELECT EstqSdo  ',
        'FROM GraGruEPat ',
        'WHERE GraGruX=' + Geral.FF0(GraGruX),
        'AND Empresa=' + Geral.FF0(FEmpresa),
        '']);
        SdoFuturo := QrEstqEstqSdo.Value - (QtdeLocacao * QtdeProduto);
        //
        if SdoFuturo < 0 then
        begin
          Geral.MB_Info('Saldo insuficiente para loca��o! Produto ' + Geral.FF0(GraGruX));
          //Exit;  impedir na confirma��o?
        end;
      end;
      //
      GraGruY        := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
      ManejoLca      := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);
      //
      //CtrID := UMyMod.BPGS1I32('loccitslca', 'CtrID', '', '', tsPos, SQLType, CtrID);
      Item := UMyMod.BPGS1I32('loccitslca', 'Item', '', '', tsPos, SQLType, CtrID);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitslca', False, [
        'Codigo', 'GraGruX', 'ValorDia',
        'ValorSem', 'ValorQui', 'ValorMes',
        // Fazer no Cab?
        'DtHrLocado',
        'ValorProduto', 'QtdeLocacao', 'ValorLocacao',
        // Fazer no Cab? 'SaiDtHr',
        //'CobranIniDtHr',
        'Categoria', 'QtdeProduto', 'ManejoLca',
        'CtrID', 'ValorFDS', 'DMenos'
      ], [
      'Item'], [
        Codigo, GraGruX, ValorDia,
        ValorSem, ValorQui, ValorMes,
        // Fazer no Cab?
        FDtHrLocado,
        ValorProduto, QtdeLocacao , ValorLocacao,
        // Fazer no Cab? SaiDtHr,
        //CobranIniDtHr,
        Categoria, QtdeProduto, ManejoLca,
        CtrID, ValorFDS, DMenos
      ], [
      Item], True) then
      begin
        AppPF.AtualizaEstoqueGraGXPatr(GraGruX, FEmpresa, True);
        FmLocCConCab.ReopenLocIPatSec(FQrLocIPatSec, Codigo, CtrID, Item);
      end;
    end else
      Geral.MB_Erro('Patrim�nio secund�rio n�o lozalizado!');
  end;
}
  //
{
  procedure IncluiAce(Codigo, CtrID: Integer);
  var
    Item: Integer;
    //ValBem
    //PrcUni, QtdIni
    QtdeProduto, QtdeLocacao, ValorProduto: Double;
  begin
    //ValBem  := QrGraGXTollItemValr.Value;
    ValorProduto  := QrGraGXTollItemValr.Value;
    QtdeProduto   := EdQtdeProduto.ValueVariant;
    QtdeLocacao   := EdQtdeLocacao.ValueVariant;
    //
    //
    Item := UMyMod.BPGS1I32('loccitslca', 'Item', '', '', tsPos, stIns, 0);
    //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatace', False, [
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccitslca', False, [
    'Codigo', 'GraGruX', 'ValorDia',
    'ValorSem', 'ValorQui', 'ValorMes',
    'DtHrLocado',
    'Categoria', 'QtdeProduto',
    'ValorProduto', 'QtdeLocacao', 'ManejoLca',
    'CtrID', 'ValorFDS', 'DMenos'], [
    'Item'], [
    Codigo, GraGruX, (*ValorDia*)0,
    (*ValorSem*)0, (*ValorQui*)0, (*ValorMes*)0,
    FDtHrLocado,
    FCategoria, QtdeProduto,
    ValorProduto, QtdeLocacao, ManejoLca,
    CtrID, (*ValorFDS*)0, (*DMenos*)0], [
    Item], True) then
    begin
      AppPF.AtualizaEstoqueGraGXPatr(GraGruX, FEmpresa, True);
      AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
      FmLocCConCab.ReopenLocIPatAce(FQrLocIPatAce, Codigo, CtrID, Item);
    end;
  end;
}
  //
{
  procedure IncluiApo(Codigo, CtrID: Integer);
  var
    Item: Integer;
    //ValBem
    PrcUni, QtdIni, QtdeProduto, QtdeLocacao, ValorProduto: Double;
  begin
    //ValBem      := QrGraGXTollItemValr.Value;
    PrcUni        := QrGraGXTollItemValr.Value;
    QtdIni        := 0;
    ValorProduto  := QrGraGXTollItemValr.Value;
    QtdeLocacao   := EdQtdeLocacao.ValueVariant;
    QtdeProduto   := EdQtdeProduto.ValueVariant;
    //
    //
    Item := UMyMod.BPGS1I32('loccitsruc', 'Item', '', '', tsPos, stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccitsruc', False, [
      'Codigo', 'CtrID', 'GraGruX',
      //'ValBem'

      'PrcUni', 'QtdIni', 'QtdeLocacao',

      //'PrcUni', 'QtdIni'
      'Categoria', 'QtdeProduto', 'ManejoLca',
      'ValorProduto'
       ], ['Item'], [
      Codigo, CtrID, GraGruX,
      //ValBem

      PrcUni, QtdIni, QtdeLocacao,

      //'PrcUni', 'QtdIni'
      FCategoria, QtdeProduto, ManejoLca,
      ValorProduto
      ], [Item], True)
    then
      FmLocCConCab.ReopenLocIPatApo(FQrLocIPatApo, Codigo, CtrID, Item);
  end;
}
  //
{
  procedure IncluiCns(Codigo, CtrID: Integer);
  var
    Item, Unidade: Integer;
    QtdIni, QtdFim, PrcUni, ValUso, QtdeLocacao, QtdeProduto, ValorProduto: Double;
  begin
    Unidade        := QrGraGXTollItemUnid.Value;
    QtdIni         := 0;
    QtdFim         := 0;
    PrcUni         := QrGraGXTollItemValr.Value;;
    ValUso         := 0;
    QtdeProduto    := EdQtdeProduto.ValueVariant;
    QtdeLocacao    := EdQtdeLocacao.ValueVariant;
    ValorProduto   := QrGraGXTollItemValr.Value;
    //
    Item := UMyMod.BPGS1I32('loccitsruc', 'Item', '', '', tsPos, stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccitsruc', False, [
      'Codigo', 'CtrID', 'GraGruX',

      'Categoria', 'QtdeProduto', 'ManejoLca',

      'Unidade', 'QtdIni', 'QtdFim',
      'PrcUni', 'ValUso', 'QtdeLocacao',
      'ValorProduto'], [
      'Item'], [
      Codigo, CtrID, GraGruX,

      FCategoria, QtdeProduto, ManejoLca,

      Unidade, QtdIni, QtdFim,
      PrcUni, ValUso, QtdeLocacao,
      ValorProduto], [
      Item], True)
    then
      FmLocCConCab.ReopenLocIPatCns(FQrLocIPatCns, Codigo, CtrID, Item);
  end;
}
{
  procedure IncluiUso(Codigo, CtrID: Integer);
  var
    Item, Unidade, AvalIni: Integer;
    AvalFim, PrcUni, ValUso, QtdeProduto, QtdeLocacao, ValorProduto: Double;
  begin
    Unidade        := QrGraGXTollItemUnid.Value;
    AvalIni        := 0;
    AvalFim        := 0;
    PrcUni         := QrGraGXTollItemValr.Value;
    ValUso         := 0;
    QtdeProduto    := EdQtdeProduto.ValueVariant;
    QtdeLocacao    := EdQtdeLocacao.ValueVariant;
    ValorProduto   := QrGraGXTollItemValr.Value;
    //
    Item := UMyMod.BPGS1I32('loccitsruc', 'Item', '', '', tsPos, stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccitsruc', False, [
      'Codigo', 'CtrID', 'GraGruX',

      'Categoria', 'QtdeProduto', 'ManejoLca',

      'Unidade', 'AvalIni', 'AvalFim',
      'PrcUni', 'ValUso', 'QtdeLocacao',
      'ValorProduto'], [
      'Item'], [
      Codigo, CtrID, GraGruX,

      FCategoria, QtdeProduto, ManejoLca,

      Unidade, AvalIni, AvalFim,
      PrcUni, ValUso, QtdeLocacao,
      ValorProduto], [
      Item], True)
    then
      FmLocCConCab.ReopenLocIPatUso(FQrLocIPatUso, Codigo, CtrID, Item);
  end;
}
const
  OriSubstItem  = 0;
  OriSubstSaiDH = '0000-00-00 00:00:00';
var
  Codigo, CtrID, Unidade, Item: Integer;
  QtdeLocacao, QtdeProduto, ValorProduto, PrcUni: Double;
  Categoria: String;
begin
  Codigo := FCodigo;
  CtrID  := FCtrID;
  Categoria     := 'L';
  //
  Item          := 0;
  GraGruX       := EdGraGruX.ValueVariant;
  GraGruY       := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
  ManejoLca     := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);
  FDtHrLocado   := Geral.FDT(Trunc(TPDataLoc.Date), 1) + ' ' + EdHoraLoc.Text;
  QtdeLocacao   := EdQtdeLocacao.ValueVariant;
  QtdeProduto   := EdQtdeProduto.ValueVariant;
  ValorProduto  := QrGraGXTollItemValr.Value;
  PrcUni        := QrGraGXTollItemValr.Value;
  Unidade       := QrGraGXTollItemUnid.Value;
  //
  //case FTipo of
  case FGraGruY of
    CO_GraGruY_2048_GXPatSec: ///IncluiSec(Codigo, CtrID); // = Secund�rio
      AppPF.LocCItsMat_IncluiSec(stIns, Codigo, CtrID, GraGruX, FEmpresa,
      QtdeLocacao, QtdeProduto, FTipoAluguel, FDtHrLocado, FQrLocIPatSec,
      OriSubstItem, OriSubstSaiDH, Item);
    //
    CO_GraGruY_3072_GXPatAce: //IncluiAce(Codigo, CtrID); // = Acess�rio
      AppPF.LocCItsMat_IncluiAce(stIns, Codigo, CtrID, GraGruX, FEmpresa,
      ManejoLca, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
      FQrLocIPatAce, OriSubstItem, OriSubstSaiDH, Item);
    //
    CO_GraGruY_4096_GXPatApo: //IncluiApo(Codigo, CtrID); // = Apoio
      AppPF.LocCItsMat_IncluiApo(stIns, Codigo, CtrID, GraGruX, ManejoLca,
      PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
      FQrLocIPatApo, Item);
    //
    CO_GraGruY_5120_GXPatUso: //IncluiUso(Codigo, CtrID); // = Uso (desgaste)
      AppPF.LocCItsMat_IncluiUso(stIns, Codigo, CtrID, GraGruX, ManejoLca, Unidade,
      PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
      FQrLocIPatUso, Item);
    //
    CO_GraGruY_6144_GXPrdCns: //IncluiCns(Codigo, CtrID); // = Uso (consumo)
      AppPF.LocCItsMat_IncluiCns(stIns, Codigo, CtrID, GraGruX, ManejoLca, Unidade,
      PrcUni, QtdeLocacao, QtdeProduto, ValorProduto, FDtHrLocado, Categoria,
      FQrLocIPatCns, Item);
  end;
  if CkContinuar.Checked then
  begin
    UnDmkDAC_PF.AbreQuery(QrGraGXToll, Dmod.MyDB);
    EdQtdeLocacao.ValueVariant := 0;
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
    EdGraGruX.ValueVariant := 0;
    CBGraGruX.KeyValue := Null;
    ///
    EdGraGruX.SetFocus;
  end else
    Close;
end;

procedure TFmLocCItsMat.BtSaidaClick(Sender: TObject);
begin
  VAR_SELCOD := 0;
  Close;
end;

procedure TFmLocCItsMat.EdGraGruXChange(Sender: TObject);
begin
  if (not EdPatrimonio.Focused) and
     (not EdReferencia.Focused) then
    //PesquisaPorGraGruX();
    AppPF.LocCItsRet_PesquisaPorGraGruX(EdGraGruX, CBGraGruX, EdPatrimonio,
    EdReferencia, FTipo, (*Limpa*)True);
end;

procedure TFmLocCItsMat.EdPatrimonioChange(Sender: TObject);
begin
  if EdPatrimonio.Focused then
    //PesquisaPorPatrimonio(False);
    AppPF.LocCItsRet_PesquisaPOrPatrimonio(EdGraGruX, CBGraGruX, EdPatrimonio,
    EdReferencia, FTipo, (*Limpa*)False);
end;

procedure TFmLocCItsMat.EdPatrimonioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  PesquisaPorNome(Key);
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmLocCItsMat.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    //PesquisaPorReferencia(False);
    AppPF.LocCItsRet_PesquisaPorReferencia(EdGraGruX, CBGraGruX, EdPatrimonio,
    EdReferencia, FTipo, (*Limpa*)False);
end;

procedure TFmLocCItsMat.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], 'LOC-PATRI-105 :: ' + FCaption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocCItsMat.FormShow(Sender: TObject);
begin
  AppPF.ReopenGraGXToll(FGraGruY, FTipo, QrGraGXToll);
end;

{
function TFmLocCItsMat.ObtemTabela(): String;
begin
  case FTipo of
    TGraGXToolRnt.ggxo2gPrincipal,
    TGraGXToolRnt.ggxo2gSecundario,
    TGraGXToolRnt.ggxo2gAcessorio:
      Result := 'gragxpatr';
    TGraGXToolRnt.ggxo2gApoio,
    TGraGXToolRnt.ggxo2gUso,
    TGraGXToolRnt.ggxo2gConsumo:
      Result := 'gragxoutr';
    else Result := '?????'
  end;
end;
}

{
procedure TFmLocCItsMat.PesquisaPorGraGruX;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia, gg1.Patrimonio  ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + ObtemTabela() + ' ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  ggo.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (ggo.GraGruX IS NULL) ',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdPatrimonio.ValueVariant <> QrPesq2Patrimonio.Value then
    begin
      EdPatrimonio.ValueVariant := QrPesq2Patrimonio.Value;
(*&�%$#
      Reabre := True;
*)
    end;
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
    begin
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
    end;
  end else
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
  end;
end;

{
procedure TFmLocCItsMat.PesquisaPorPatrimonio(Limpa: Boolean);
var
  Reabre: Boolean;
begin
  Reabre := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle, gg1.Referencia, gg1.Patrimonio ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Patrimonio="' + EdPatrimonio.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
      Reabre := True;
    end;
    if EdReferencia.ValueVariant <> QrPesq1Referencia.Value then
    begin
      EdReferencia.ValueVariant := QrPesq1Referencia.Value;
      Reabre := True;
    end;
(*&�%$#
    if Reabre then
      ReopenAgrupado();
*)
    //  Precisa ?
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
  end;
end;
}

{
procedure TFmLocCItsMat.PesquisaPorReferencia(Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + ObtemTabela() + ' ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
  'AND NOT (ggo.GraGruX IS NULL)',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdPatrimonio.ValueVariant <> QrPesq1Patrimonio.Value then
    begin
      EdPatrimonio.ValueVariant := QrPesq1Patrimonio.Value;
(*&�%$
      Reabre := True;
*)
    end;
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
    begin
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
    end;
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
  end;
end;
}

{
procedure TFmLocCItsMat.ReopenGraGXToll;
begin
  case FGraGruY of
    CO_GraGruY_2048_GXPatSec,
    CO_GraGruY_3072_GXPatAce:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXToll, Dmod.MyDB, [
      'SELECT ggx.Controle, gg1.Referencia, gg1.Nome NO_GG1,  ',
      'ggo.GraGruX, ggo.AtualValr ItemValr, ggo.ItemUnid ',
      'FROM gragxpatr ggo  ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'WHERE ggo.Aplicacao=' + Geral.FF0(AppPF.ObtemTipoDeTGraGXOutr(FTipo)),
      'ORDER BY NO_GG1',
      '']);
    end;
    CO_GraGruY_4096_GXPatApo,
    CO_GraGruY_5120_GXPatUso,
    CO_GraGruY_6144_GXPrdCns:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXToll, Dmod.MyDB, [
      'SELECT ggx.Controle, gg1.Referencia, gg1.Nome NO_GG1,  ',
      'ggo.GraGruX, ggo.ItemValr, ggo.ItemUnid ',
      'FROM gragxoutr ggo  ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'WHERE ggo.Aplicacao=' + Geral.FF0(AppPF.ObtemTipoDeTGraGXOutr(FTipo)),
      'ORDER BY NO_GG1',
      '']);
    end;
  end;
  //Geral.MB_SQL(Self, QrGraGXToll);
end;
}

procedure TFmLocCItsMat.SBCadastroClick(Sender: TObject);
const
  sProcName = 'TFmLocCItsMat.SBCadastroClick()';
var
  OK: Boolean;
begin
  VAR_CADASTRO := 0;
  OK := True;
  case FTipo of
    TGraGXToolRnt.ggxo2gAcessorio:
      GraL_Jan.MostraFormGraGXPatr(EdGraGruX.ValueVariant);
    TGraGXToolRnt.ggxo2gApoio,
    TGraGXToolRnt.ggxo2gUso,
    TGraGXToolRnt.ggxo2gConsumo:
      GraL_Jan.MostraFormGraGXOutr(EdGraGruX.ValueVariant);
    else
    begin
      OK := False;
      Geral.MB_Erro('"TGraGXToolRnt" n�o implementado em ' + sProcName);
    end;
  end;
  if ok then
  begin
    UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrGraGXToll, VAR_CADASTRO, 'Controle');
    //
    AppPF.LocCItsRet_PesquisaPorGraGruX(EdGraGruX, CBGraGruX, EdPatrimonio,
    EdReferencia, FTipo, (*Limpa*)True);
    //
    EdReferencia.SetFocus;
  end;
end;

procedure TFmLocCItsMat.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocCItsMat.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCategoria   := 'L';
  EdQtdeProduto.ValueVariant := 1;
  EdQtdeLocacao.ValueVariant := 1;
  TPDataLoc.Date := Trunc(Date);
  EdHoraLoc.Text := FormatDateTime('hh:nn', Now());
end;

//    'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle AND gep.Empresa=' + Geral.FF0(FQrCab.FieldByName('Empresa').AsInteger),

end.
