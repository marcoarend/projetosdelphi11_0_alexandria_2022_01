object FmBolImpLct: TFmBolImpLct
  Left = 339
  Top = 185
  Caption = 'BLQ-PRINT-001 :: Impress'#227'o de Bloquetos de Lan'#231'amentos'
  ClientHeight = 492
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 503
        Height = 32
        Caption = 'Impress'#227'o de Bloquetos de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 503
        Height = 32
        Caption = 'Impress'#227'o de Bloquetos de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 503
        Height = 32
        Caption = 'Impress'#227'o de Bloquetos de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 330
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 330
        Align = alClient
        TabOrder = 0
        object DBGrid1: TdmkDBGrid
          Left = 2
          Top = 15
          Width = 1004
          Height = 313
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Cliente'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Title.Caption = 'Sacado'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJCPF'
              Width = 113
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_CEP'
              Title.Caption = 'CEP'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_RUA'
              Title.Caption = 'Endere'#231'o Sacado'
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_NUMERO'
              Title.Caption = 'N'#186
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_BAIRRO'
              Title.Caption = 'Bairro'
              Width = 103
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_CIDADE'
              Title.Caption = 'Cidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_xUF'
              Title.Caption = 'UF'
              Width = 28
              Visible = True
            end>
          Color = clWindow
          DataSource = DsLctBol
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGrid1CellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Cliente'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Title.Caption = 'Sacado'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJCPF'
              Width = 113
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_CEP'
              Title.Caption = 'CEP'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_RUA'
              Title.Caption = 'Endere'#231'o Sacado'
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_NUMERO'
              Title.Caption = 'N'#186
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_BAIRRO'
              Title.Caption = 'Bairro'
              Width = 103
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_CIDADE'
              Title.Caption = 'Cidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SACADO_xUF'
              Title.Caption = 'UF'
              Width = 28
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 378
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 5
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 365
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 493
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Nenhum'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtNenhumClick
      end
    end
  end
  object frxBloqE2: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 40897.760788298600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoBancoExiste> = True then'
      '  begin'
      '    Picture_Bco1.LoadFromFile(<LogoBancoPath>);'
      
        '    Picture_Bco2.LoadFromFile(<LogoBancoPath>);                 ' +
        '                                                            '
      '    MeLogo1.Visible := False;'
      '    MeLogo2.Visible := False;'
      '  end else begin           '
      '    MeLogo1.Visible := True;'
      '    MeLogo2.Visible := True;'
      '  end;              '
      '  MeHomologacao.Visible := <VARF_STATUS>;  '
      'end.')
    OnGetValue = frxBloqE2GetValue
    Left = 420
    Top = 192
    Datasets = <
      item
        DataSet = frxDsBoletos
        DataSetName = 'frxDsBoletos'
      end
      item
        DataSet = frxDsCNAB_Cfg
        DataSetName = 'frxDsCNAB_Cfg'
      end>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      TopMargin = 10.000000000000000000
      Frame.Width = 0.100000000000000000
      LargeDesignHeight = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 1073.386202680000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        DataSet = frxDsBoletos
        DataSetName = 'frxDsBoletos'
        RowCount = 0
        object Memo114: TfrxMemoView
          Left = 599.055115670000000000
          Top = 810.331082130000000000
          Width = 137.952755910000000000
          Height = 17.385826770000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletos."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Picture_Bco2: TfrxPictureView
          Left = 49.133890000000000000
          Top = 680.315400000000000000
          Width = 173.858380000000000000
          Height = 41.574830000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo113: TfrxMemoView
          Left = 599.055115670000000000
          Top = 730.961003390000000000
          Width = 137.952755910000000000
          Height = 23.055118110000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletos."Vencto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          Left = 599.055115670000000000
          Top = 801.260360000000000000
          Width = 137.952755910000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(=) Valor do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Left = 599.055115670000000000
          Top = 721.890230000000000000
          Width = 137.952755910000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line13: TfrxLineView
          Top = 680.315400000000000000
          Width = 789.921770000000000000
          Color = clBlack
          ArrowLength = 100
          ArrowWidth = 20
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo81: TfrxMemoView
          Left = 574.488560000000000000
          Top = 668.976810000000000000
          Width = 162.519790000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'AUTENTICA'#199#195'O MEC'#194'NICA')
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          Left = 86.929131420000000000
          Top = 1020.472814410000000000
          Width = 405.000000000000000000
          Height = 49.133858270000000000
          BarType = bcCode_2_5_interleaved
          Expression = '<VARF_CODIGOBARRAS>'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 3.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Line14: TfrxLineView
          Left = 49.133890000000000000
          Top = 721.890230000000000000
          Width = 687.874015750000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line15: TfrxLineView
          Left = 49.133890000000000000
          Top = 754.016119060000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line16: TfrxLineView
          Left = 49.133890000000000000
          Top = 778.583180000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line17: TfrxLineView
          Left = 49.133890000000000000
          Top = 801.260360000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line18: TfrxLineView
          Left = 222.992123540000000000
          Top = 699.213050000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line19: TfrxLineView
          Left = 291.023622050000000000
          Top = 699.213050000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line20: TfrxLineView
          Left = 599.055115670000000000
          Top = 721.890230000000000000
          Height = 230.551330000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line21: TfrxLineView
          Left = 427.086890000000000000
          Top = 778.583180000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line22: TfrxLineView
          Left = 366.614410000000000000
          Top = 778.583180000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line23: TfrxLineView
          Left = 287.244280000000000000
          Top = 778.583180000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line24: TfrxLineView
          Left = 224.881887320000000000
          Top = 801.260360000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line25: TfrxLineView
          Left = 156.850391260000000000
          Top = 778.583180000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line26: TfrxLineView
          Left = 49.133890000000000000
          Top = 827.717070000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line27: TfrxLineView
          Left = 49.133890000000000000
          Top = 952.441315910000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line28: TfrxLineView
          Left = 49.133890000000000000
          Top = 1012.913756850000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 1.500000000000000000
        end
        object Memo82: TfrxMemoView
          Left = 222.992270000000000000
          Top = 699.213050000000000000
          Width = 68.031540000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VAX]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          Left = 291.023810000000000000
          Top = 699.213050000000000000
          Width = 445.984540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_LINHADIGITAVEL]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 49.133890000000000000
          Top = 721.890230000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Local de Pagamento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 49.133890000000000000
          Top = 755.906000000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cedente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          Left = 49.133890000000000000
          Top = 778.583180000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Data do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          Left = 52.913420000000000000
          Top = 733.228820000000000000
          Width = 544.252320000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."LocalPag"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          Left = 49.133890000000000000
          Top = 801.260360000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Uso do Banco')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          Left = 162.519790000000000000
          Top = 778.583180000000000000
          Width = 64.252010000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'N'#250'mero do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          Left = 291.023810000000000000
          Top = 778.583180000000000000
          Width = 68.031540000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Esp'#233'cie do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          Left = 370.393940000000000000
          Top = 778.583180000000000000
          Width = 26.456710000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Aceite')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          Left = 430.866420000000000000
          Top = 778.583180000000000000
          Width = 68.031540000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Data do Processamento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Left = 162.519790000000000000
          Top = 801.260360000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          Left = 226.771800000000000000
          Top = 801.260360000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Esp'#233'cie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          Left = 291.023810000000000000
          Top = 801.260360000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          Left = 430.866420000000000000
          Top = 801.260360000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          Left = 423.307360000000000000
          Top = 810.709034880000000000
          Width = 7.559060000000000000
          Height = 8.314960630000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'X')
          ParentFont = False
          WordWrap = False
        end
        object Memo99: TfrxMemoView
          Left = 600.945270000000000000
          Top = 755.906000000000000000
          Width = 136.063080000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Ag'#234'ncia/C'#243'digo Cedente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          Left = 600.945270000000000000
          Top = 778.583180000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Nosso N'#250'mero')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          Left = 600.945270000000000000
          Top = 827.717070000000000000
          Width = 136.063080000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(-) Desconto/Abatimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          Left = 600.945270000000000000
          Top = 854.173780000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(-) Outras Dedu'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          Left = 600.945270000000000000
          Top = 880.630490000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(+) Mora/Multa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          Left = 600.945270000000000000
          Top = 903.307670000000000000
          Width = 64.252010000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(+) Outros Acr'#233'scimos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          Left = 600.945270000000000000
          Top = 929.764380000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(=) Valor Cobrado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line29: TfrxLineView
          Left = 599.055115670000000000
          Top = 852.283835590000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line30: TfrxLineView
          Left = 599.055115670000000000
          Top = 876.850764720000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line31: TfrxLineView
          Left = 599.055115670000000000
          Top = 901.417693860000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line32: TfrxLineView
          Left = 599.055115670000000000
          Top = 925.984622990000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo109: TfrxMemoView
          Left = 585.827150000000000000
          Top = 1016.693570000000000000
          Width = 151.181200000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          Left = 616.063390000000000000
          Top = 997.795920000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'digo de Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          Left = 49.133890000000000000
          Top = 828.472814410000000000
          Width = 132.283550000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          Left = 52.913420000000000000
          Top = 763.465060000000000000
          Width = 544.252320000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."NOMECED_IMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo116: TfrxMemoView
          Left = 52.913420000000000000
          Top = 787.653916770000000000
          Width = 102.047310000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          Left = 158.740260000000000000
          Top = 787.653916770000000000
          Width = 128.504020000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBoletos."Boleto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo118: TfrxMemoView
          Left = 291.023810000000000000
          Top = 787.653916770000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."EspecieTit"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          Left = 370.393940000000000000
          Top = 787.653916770000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."ACEITETIT_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo120: TfrxMemoView
          Left = 430.866420000000000000
          Top = 787.653916770000000000
          Width = 166.299320000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          Left = 162.519790000000000000
          Top = 810.331082130000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."CartNum"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          Left = 226.771800000000000000
          Top = 810.331082130000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."EspecieTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          Left = 600.945270000000000000
          Top = 787.653916770000000000
          Width = 136.063080000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_NossoNumero]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          Left = 600.945270000000000000
          Top = 763.465060000000000000
          Width = 136.063080000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."AgContaCed"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeLogo2: TfrxMemoView
          Left = 49.133890000000000000
          Top = 691.653990000000000000
          Width = 173.858380000000000000
          Height = 30.236240000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."NOMEBANCO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 599.055115670000000000
          Top = 387.023722130000000000
          Width = 137.952755910000000000
          Height = 17.385826770000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletos."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Picture_Bco1: TfrxPictureView
          Left = 49.133890000000000000
          Top = 257.008040000000000000
          Width = 173.858380000000000000
          Height = 41.574830000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo3: TfrxMemoView
          Left = 599.055115670000000000
          Top = 307.653643390000000000
          Width = 137.952755910000000000
          Height = 23.055118110000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletos."Vencto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 599.055115670000000000
          Top = 377.953000000000000000
          Width = 137.952755910000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(=) Valor do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 599.055115670000000000
          Top = 298.582870000000000000
          Width = 137.952755910000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 257.008040000000000000
          Width = 789.921770000000000000
          Color = clBlack
          ArrowLength = 100
          ArrowWidth = 20
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo6: TfrxMemoView
          Left = 574.488560000000000000
          Top = 241.889920000000000000
          Width = 162.519790000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'SEGUNDA VIA')
          ParentFont = False
        end
        object BarCode2: TfrxBarCodeView
          Left = 86.929131420000000000
          Top = 597.165454410000000000
          Width = 405.000000000000000000
          Height = 49.133858270000000000
          BarType = bcCode_2_5_interleaved
          Expression = '<VARF_CODIGOBARRAS>'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 3.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Line2: TfrxLineView
          Left = 49.133890000000000000
          Top = 298.582870000000000000
          Width = 687.874015750000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line3: TfrxLineView
          Left = 49.133890000000000000
          Top = 330.708759060000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line4: TfrxLineView
          Left = 49.133890000000000000
          Top = 355.275820000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line5: TfrxLineView
          Left = 49.133890000000000000
          Top = 377.953000000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line6: TfrxLineView
          Left = 222.992123540000000000
          Top = 275.905690000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line7: TfrxLineView
          Left = 291.023622050000000000
          Top = 275.905690000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line8: TfrxLineView
          Left = 599.055115670000000000
          Top = 298.582870000000000000
          Height = 230.551330000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line9: TfrxLineView
          Left = 427.086890000000000000
          Top = 355.275820000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line10: TfrxLineView
          Left = 366.614410000000000000
          Top = 355.275820000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line11: TfrxLineView
          Left = 287.244280000000000000
          Top = 355.275820000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line12: TfrxLineView
          Left = 224.881887320000000000
          Top = 377.953000000000000000
          Height = 22.677180000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line33: TfrxLineView
          Left = 156.850391260000000000
          Top = 355.275820000000000000
          Height = 49.133890000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line34: TfrxLineView
          Left = 49.133890000000000000
          Top = 404.409710000000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line35: TfrxLineView
          Left = 49.133890000000000000
          Top = 529.133955910000100000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line36: TfrxLineView
          Left = 49.133890000000000000
          Top = 589.606396850000000000
          Width = 687.874460000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 1.500000000000000000
        end
        object Memo7: TfrxMemoView
          Left = 222.992270000000000000
          Top = 275.905690000000000000
          Width = 68.031540000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VAX]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 291.023810000000000000
          Top = 275.905690000000000000
          Width = 445.984540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_LINHADIGITAVEL]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 49.133890000000000000
          Top = 298.582870000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Local de Pagamento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 49.133890000000000000
          Top = 332.598640000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cedente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 49.133890000000000000
          Top = 355.275820000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Data do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 52.913420000000000000
          Top = 309.921460000000000000
          Width = 544.252320000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."LocalPag"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 49.133890000000000000
          Top = 377.953000000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Uso do Banco')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 162.519790000000000000
          Top = 355.275820000000000000
          Width = 64.252010000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'N'#250'mero do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 291.023810000000000000
          Top = 355.275820000000000000
          Width = 71.811070000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Esp'#233'cie do Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 370.393940000000000000
          Top = 355.275820000000000000
          Width = 26.456710000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Aceite')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 430.866420000000000000
          Top = 355.275820000000000000
          Width = 68.031540000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Data do Processamento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 162.519790000000000000
          Top = 377.953000000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 226.771800000000000000
          Top = 377.953000000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Esp'#233'cie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 291.023810000000000000
          Top = 377.953000000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 430.866420000000000000
          Top = 377.953000000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 423.307360000000000000
          Top = 387.401674880000000000
          Width = 7.559060000000000000
          Height = 8.314960630000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'X')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 600.945270000000000000
          Top = 332.598640000000000000
          Width = 136.063080000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Ag'#234'ncia/C'#243'digo Cedente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 600.945270000000000000
          Top = 355.275820000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Nosso N'#250'mero')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 600.945270000000000000
          Top = 404.409710000000000000
          Width = 136.063080000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(-) Desconto/Abatimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 600.945270000000000000
          Top = 430.866420000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(-) Outras Dedu'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 600.945270000000000000
          Top = 457.323130000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(+) Mora/Multa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 600.945270000000000000
          Top = 480.000310000000000000
          Width = 64.252010000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(+) Outros Acr'#233'scimos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 600.945270000000000000
          Top = 506.457020000000100000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '(=) Valor Cobrado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line37: TfrxLineView
          Left = 599.055115670000000000
          Top = 428.976475590000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line38: TfrxLineView
          Left = 599.055115670000000000
          Top = 453.543404720000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line39: TfrxLineView
          Left = 599.055115670000000000
          Top = 478.110333860000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line40: TfrxLineView
          Left = 599.055115670000000000
          Top = 502.677262990000000000
          Width = 137.952755910000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo31: TfrxMemoView
          Left = 166.299320000000000000
          Top = 529.134199999999900000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Sacado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 585.827150000000000000
          Top = 593.386210000000000000
          Width = 151.181200000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 616.063390000000000000
          Top = 574.488560000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'digo de Baixa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 49.133890000000000000
          Top = 405.165454410000000000
          Width = 132.283550000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 52.913420000000000000
          Top = 340.157700000000000000
          Width = 544.252320000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."NOMECED_IMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          Left = 52.913420000000000000
          Top = 364.346556770000000000
          Width = 102.047310000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 158.740260000000000000
          Top = 364.346556770000000000
          Width = 128.504020000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBoletos."Boleto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 291.023810000000000000
          Top = 364.346556770000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."EspecieTit"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 370.393940000000000000
          Top = 364.346556770000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."ACEITETIT_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 430.866420000000000000
          Top = 364.346556770000000000
          Width = 166.299320000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 162.519790000000000000
          Top = 387.023722130000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."CartNum"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 226.771800000000000000
          Top = 387.023722130000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."EspecieTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 600.945270000000000000
          Top = 364.346556770000000000
          Width = 136.063080000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_NossoNumero]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Left = 600.945270000000000000
          Top = 340.157700000000000000
          Width = 136.063080000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."AgContaCed"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeLogo1: TfrxMemoView
          Left = 49.133890000000000000
          Top = 268.346630000000000000
          Width = 173.858380000000000000
          Height = 30.236240000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCNAB_Cfg."NOMEBANCO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 52.913420000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Boleto em duas vias')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Left = 166.299320000000000000
          Top = 952.441560000000000000
          Width = 56.692950000000000000
          Height = 9.070866140000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Sacado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 222.992270000000000000
          Top = 529.134199999999900000
          Width = 385.512060000000000000
          Height = 15.118112680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Modelo E2')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo126: TfrxMemoView
          Left = 222.992270000000000000
          Top = 952.441560000000000000
          Width = 385.512060000000000000
          Height = 15.118112680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Modelo E2')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object frxMemoView1: TfrxMemoView
          Left = 53.133890000000000000
          Top = 438.425465350000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_03]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 53.133890000000000000
          Top = 415.748300000000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_01]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 53.133890000000000000
          Top = 427.086882670000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_02]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 53.133890000000000000
          Top = 449.764048030000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_04]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 53.133890000000000000
          Top = 461.102633150000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_05]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 53.133890000000000000
          Top = 472.441213380000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_06]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 53.133890000000000000
          Top = 483.779796060000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_07]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 53.133890000000000000
          Top = 495.118378740000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_08]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 53.133890000000000000
          Top = 506.457312910000100000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_09]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 53.133890000000000000
          Top = 517.795895590000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_10]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 166.299320000000000000
          Top = 559.370440000000000000
          Width = 449.764070000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsBoletos."SACADO_E_ALL"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 166.299320000000000000
          Top = 544.252320000000100000
          Width = 449.764070000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsBoletos."SACADO_NOME"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          Left = 52.913420000000000000
          Top = 861.732825350000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_03]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 52.913420000000000000
          Top = 839.055660000000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_01]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 52.913420000000000000
          Top = 850.394242670000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_02]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 52.913420000000000000
          Top = 873.071408030000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_04]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 52.913420000000000000
          Top = 884.409993150000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_05]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 52.913420000000000000
          Top = 895.748573380000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_06]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 52.913420000000000000
          Top = 907.087156060000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_07]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 52.913420000000000000
          Top = 918.425738740000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_08]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 52.913420000000000000
          Top = 929.764672910000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_09]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 52.913420000000000000
          Top = 941.103255590000000000
          Width = 544.252320000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_INSTRUCAO_10]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 166.299320000000000000
          Top = 982.677800000000000000
          Width = 449.764070000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsBoletos."SACADO_E_ALL"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 166.299320000000000000
          Top = 967.559680000000000000
          Width = 449.764070000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsBoletos."SACADO_NOME"]')
          ParentFont = False
          WordWrap = False
        end
        object MeHomologacao: TfrxMemoView
          Tag = 2
          Left = 49.133890000000000000
          Top = 257.008040000000000000
          Width = 691.653990000000000000
          Height = 676.535870000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -96
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Boleto '
            'em '
            'homologa'#231#227'o')
          ParentFont = False
          Rotation = 45
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsCNAB_Cfg: TfrxDBDataset
    UserName = 'frxDsCNAB_Cfg'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'Cedente=Cedente'
      'SacadAvali=SacadAvali'
      'CedBanco=CedBanco'
      'CedAgencia=CedAgencia'
      'CedConta=CedConta'
      'CedDAC_A=CedDAC_A'
      'CedDAC_C=CedDAC_C'
      'CedDAC_AC=CedDAC_AC'
      'CedNome=CedNome'
      'CedPosto=CedPosto'
      'SacAvaNome=SacAvaNome'
      'TipoCart=TipoCart'
      'CartNum=CartNum'
      'CartCod=CartCod'
      'EspecieTit=EspecieTit'
      'EspecieDoc=EspecieDoc'
      'AceiteTit=AceiteTit'
      'InstrCobr1=InstrCobr1'
      'InstrCobr2=InstrCobr2'
      'InstrDias=InstrDias'
      'CodEmprBco=CodEmprBco'
      'JurosTipo=JurosTipo'
      'JurosPerc=JurosPerc'
      'JurosDias=JurosDias'
      'MultaTipo=MultaTipo'
      'MultaPerc=MultaPerc'
      'MultaDias=MultaDias'
      'Texto01=Texto01'
      'Texto02=Texto02'
      'Texto03=Texto03'
      'Texto04=Texto04'
      'Texto05=Texto05'
      'Texto06=Texto06'
      'Texto07=Texto07'
      'Texto08=Texto08'
      'Texto09=Texto09'
      'Texto10=Texto10'
      'CNAB=CNAB'
      'EnvEmeio=EnvEmeio'
      'Diretorio=Diretorio'
      'QuemPrint=QuemPrint'
      '_237Mens1=_237Mens1'
      '_237Mens2=_237Mens2'
      'CodOculto=CodOculto'
      'SeqArq=SeqArq'
      'LastNosNum=LastNosNum'
      'LocalPag=LocalPag'
      'EspecieVal=EspecieVal'
      'OperCodi=OperCodi'
      'IDCobranca=IDCobranca'
      'AgContaCed=AgContaCed'
      'DirRetorno=DirRetorno'
      'CartRetorno=CartRetorno'
      'PosicoesBB=PosicoesBB'
      'IndicatBB=IndicatBB'
      'TipoCobrBB=TipoCobrBB'
      'Variacao=Variacao'
      'Comando=Comando'
      'DdProtesBB=DdProtesBB'
      'Msg40posBB=Msg40posBB'
      'QuemDistrb=QuemDistrb'
      'Desco1Cod=Desco1Cod'
      'Desco1Dds=Desco1Dds'
      'Desco1Fat=Desco1Fat'
      'Desco2Cod=Desco2Cod'
      'Desco2Dds=Desco2Dds'
      'Desco2Fat=Desco2Fat'
      'Desco3Cod=Desco3Cod'
      'Desco3Dds=Desco3Dds'
      'Desco3Fat=Desco3Fat'
      'ProtesCod=ProtesCod'
      'ProtesDds=ProtesDds'
      'BxaDevCod=BxaDevCod'
      'BxaDevDds=BxaDevDds'
      'MoedaCod=MoedaCod'
      'Contrato=Contrato'
      'ConvCartCobr=ConvCartCobr'
      'ConvVariCart=ConvVariCart'
      'InfNossoNum=InfNossoNum'
      'CodLidrBco=CodLidrBco'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'CartEmiss=CartEmiss'
      'DVB=DVB'
      'NOMEBANCO=NOMEBANCO'
      'NOMECED_IMP=NOMECED_IMP'
      'ACEITETIT_TXT=ACEITETIT_TXT'
      'TipoCobranca=TipoCobranca'
      'EspecieTxt=EspecieTxt'
      'CartTxt=CartTxt'
      'LayoutRem=LayoutRem'
      'LayoutRet=LayoutRet'
      'ConcatCod=ConcatCod'
      'ComplmCod=ComplmCod'
      'NaoRecebDd=NaoRecebDd'
      'TipBloqUso=TipBloqUso'
      'NumVersaoI3=NumVersaoI3'
      'ModalCobr=ModalCobr'
      'CtaCooper=CtaCooper'
      'TermoAceite=TermoAceite'
      'CorresBco=CorresBco'
      'CorresAge=CorresAge'
      'CorresCto=CorresCto'
      'CART_IMP=CART_IMP')
    DataSet = QrCNAB_Cfg
    BCDToCurrency = False
    Left = 448
    Top = 220
  end
  object frxDsBoletos: TfrxDBDataset
    UserName = 'frxDsBoletos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'Tipo=Tipo'
      'Carteira=Carteira'
      'Controle=Controle'
      'Sub=Sub'
      'CliInt=CliInt'
      'Cliente=Cliente'
      'Emitente=Emitente'
      'CNPJCPF=CNPJCPF'
      'Duplicata=Duplicata'
      'SerieCH=SerieCH'
      'Documento=Documento'
      'Credito=Credito'
      'Vencimento=Vencimento'
      'Ativo=Ativo'
      'SACADO_CNPJ=SACADO_CNPJ'
      'SACADO_NOME=SACADO_NOME'
      'SACADO_RUA=SACADO_RUA'
      'SACADO_NUMERO=SACADO_NUMERO'
      'SACADO_COMPL=SACADO_COMPL'
      'SACADO_BAIRRO=SACADO_BAIRRO'
      'SACADO_CIDADE=SACADO_CIDADE'
      'SACADO_xUF=SACADO_xUF'
      'SACADO_CEP=SACADO_CEP'
      'SACADO_TIPO=SACADO_TIPO'
      'NOMECLI=NOMECLI'
      'SACADO_E_ALL=SACADO_E_ALL'
      'SACADO_NUMERO_TXT=SACADO_NUMERO_TXT'
      'SACADO_CEP_TXT=SACADO_CEP_TXT'
      'FatID=FatID'
      'FatNum=FatNum'
      'FatParcela=FatParcela'
      'BOLETO=BOLETO'
      'VENCTO=VENCTO'
      'VALOR=VALOR')
    DataSet = QrBoletos
    BCDToCurrency = False
    Left = 448
    Top = 248
  end
  object QrLctBol: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT lct.*, sac.CNPJ SACADO_CNPJ, '
      'sac.Nome SACADO_NOME, sac.Rua SACADO_RUA,'
      'sac.Numero SACADO_NUMERO, sac.Compl SACADO_COMPL,'
      'sac.Bairro SACADO_BAIRRO, '
      'sac.Cidade SACADO_CIDADE, sac.UF SACADO_xUF,'
      'sac.CEP SACADO_CEP, '
      'IF(LENGTH(sac.CNPJ) >=14, 0, 1) SACADO_TIPO '
      'FROM _lct_boleto_ lct'
      'LEFT JOIN creditor.sacados sac ON sac.CNPJ=lct.CNPJCPF')
    Left = 52
    Top = 128
    object QrLctBolData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrLctBolTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctBolCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctBolControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrLctBolSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLctBolCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctBolCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLctBolEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrLctBolCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 13
    end
    object QrLctBolDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLctBolSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLctBolDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLctBolCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLctBolVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctBolAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrLctBolSACADO_CNPJ: TWideStringField
      FieldName = 'SACADO_CNPJ'
      Required = True
      Size = 15
    end
    object QrLctBolSACADO_NOME: TWideStringField
      FieldName = 'SACADO_NOME'
      Size = 50
    end
    object QrLctBolSACADO_RUA: TWideStringField
      FieldName = 'SACADO_RUA'
      Size = 30
    end
    object QrLctBolSACADO_NUMERO: TLargeintField
      FieldName = 'SACADO_NUMERO'
    end
    object QrLctBolSACADO_COMPL: TWideStringField
      FieldName = 'SACADO_COMPL'
      Size = 30
    end
    object QrLctBolSACADO_BAIRRO: TWideStringField
      FieldName = 'SACADO_BAIRRO'
      Size = 30
    end
    object QrLctBolSACADO_CIDADE: TWideStringField
      FieldName = 'SACADO_CIDADE'
      Size = 25
    end
    object QrLctBolSACADO_xUF: TWideStringField
      FieldName = 'SACADO_xUF'
      Size = 2
    end
    object QrLctBolSACADO_CEP: TIntegerField
      FieldName = 'SACADO_CEP'
    end
    object QrLctBolSACADO_TIPO: TLargeintField
      FieldName = 'SACADO_TIPO'
      Required = True
    end
    object QrLctBolNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrLctBolFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrLctBolFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLctBolFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
  end
  object DsLctBol: TDataSource
    DataSet = QrLctBol
    Left = 80
    Top = 128
  end
  object QrBoletos: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = QrBoletosCalcFields
    SQL.Strings = (
      'SELECT lct.*, sac.CNPJ SACADO_CNPJ, '
      'sac.Nome SACADO_NOME, sac.Rua SACADO_RUA,'
      'sac.Numero SACADO_NUMERO, sac.Compl SACADO_COMPL,'
      'sac.Bairro SACADO_BAIRRO, '
      'sac.Cidade SACADO_CIDADE, sac.UF SACADO_xUF,'
      'sac.CEP SACADO_CEP, '
      'IF(LENGTH(sac.CNPJ) >=14, 0, 1) SACADO_TIPO '
      'FROM _lct_boleto_ lct'
      'LEFT JOIN creditor.sacados sac ON sac.CNPJ=lct.CNPJCPF'
      'WHERE lct.Ativo=1')
    Left = 420
    Top = 248
    object QrBoletosData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrBoletosTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrBoletosCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrBoletosControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrBoletosSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrBoletosCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrBoletosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrBoletosEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrBoletosCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 13
    end
    object QrBoletosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrBoletosSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrBoletosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrBoletosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrBoletosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBoletosAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrBoletosSACADO_CNPJ: TWideStringField
      FieldName = 'SACADO_CNPJ'
      Required = True
      Size = 15
    end
    object QrBoletosSACADO_NOME: TWideStringField
      FieldName = 'SACADO_NOME'
      Size = 50
    end
    object QrBoletosSACADO_RUA: TWideStringField
      FieldName = 'SACADO_RUA'
      Size = 30
    end
    object QrBoletosSACADO_NUMERO: TLargeintField
      FieldName = 'SACADO_NUMERO'
    end
    object QrBoletosSACADO_COMPL: TWideStringField
      FieldName = 'SACADO_COMPL'
      Size = 30
    end
    object QrBoletosSACADO_BAIRRO: TWideStringField
      FieldName = 'SACADO_BAIRRO'
      Size = 30
    end
    object QrBoletosSACADO_CIDADE: TWideStringField
      FieldName = 'SACADO_CIDADE'
      Size = 25
    end
    object QrBoletosSACADO_xUF: TWideStringField
      FieldName = 'SACADO_xUF'
      Size = 2
    end
    object QrBoletosSACADO_CEP: TIntegerField
      FieldName = 'SACADO_CEP'
    end
    object QrBoletosSACADO_TIPO: TLargeintField
      FieldName = 'SACADO_TIPO'
      Required = True
    end
    object QrBoletosNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrBoletosSACADO_E_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SACADO_E_ALL'
      Size = 255
      Calculated = True
    end
    object QrBoletosSACADO_NUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SACADO_NUMERO_TXT'
      Calculated = True
    end
    object QrBoletosSACADO_CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SACADO_CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrBoletosFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrBoletosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBoletosFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrBoletosBOLETO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'BOLETO'
      Calculated = True
    end
    object QrBoletosVENCTO: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCTO'
      Calculated = True
    end
    object QrBoletosVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      Calculated = True
    end
  end
  object QrCNAB_Cfg: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCNAB_CfgCalcFields
    SQL.Strings = (
      'SELECT bco.Nome NOMEBANCO, bco.DVB,  cfg.*,'
      
        'IF(cfg.Cedente=0,"",IF(ced.Tipo=0,ced.RazaoSocial,ced.Nome)) NOM' +
        'ECED_IMP'
      'FROM cnab_cfg cfg'
      'LEFT JOIN bancos bco ON bco.Codigo=cfg.CedBanco'
      'LEFT JOIN entidades ced ON ced.Codigo=cfg.Cedente'
      'WHERE cfg.Codigo=:P0')
    Left = 420
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_cfg.Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrCNAB_CfgCedente: TIntegerField
      FieldName = 'Cedente'
      Origin = 'cnab_cfg.Cedente'
    end
    object QrCNAB_CfgSacadAvali: TIntegerField
      FieldName = 'SacadAvali'
      Origin = 'cnab_cfg.SacadAvali'
    end
    object QrCNAB_CfgCedBanco: TIntegerField
      FieldName = 'CedBanco'
      Origin = 'cnab_cfg.CedBanco'
    end
    object QrCNAB_CfgCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
      Origin = 'cnab_cfg.CedAgencia'
    end
    object QrCNAB_CfgCedConta: TWideStringField
      FieldName = 'CedConta'
      Origin = 'cnab_cfg.CedConta'
      Size = 30
    end
    object QrCNAB_CfgCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Origin = 'cnab_cfg.CedDAC_A'
      Size = 1
    end
    object QrCNAB_CfgCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Origin = 'cnab_cfg.CedDAC_C'
      Size = 1
    end
    object QrCNAB_CfgCedDAC_AC: TWideStringField
      FieldName = 'CedDAC_AC'
      Origin = 'cnab_cfg.CedDAC_AC'
      Size = 1
    end
    object QrCNAB_CfgCedNome: TWideStringField
      FieldName = 'CedNome'
      Origin = 'cnab_cfg.CedNome'
      Size = 100
    end
    object QrCNAB_CfgCedPosto: TIntegerField
      FieldName = 'CedPosto'
      Origin = 'cnab_cfg.CedPosto'
    end
    object QrCNAB_CfgSacAvaNome: TWideStringField
      FieldName = 'SacAvaNome'
      Origin = 'cnab_cfg.SacAvaNome'
      Size = 100
    end
    object QrCNAB_CfgTipoCart: TSmallintField
      FieldName = 'TipoCart'
      Origin = 'cnab_cfg.TipoCart'
    end
    object QrCNAB_CfgCartNum: TWideStringField
      FieldName = 'CartNum'
      Origin = 'cnab_cfg.CartNum'
      Size = 3
    end
    object QrCNAB_CfgCartCod: TWideStringField
      FieldName = 'CartCod'
      Origin = 'cnab_cfg.CartCod'
      Size = 3
    end
    object QrCNAB_CfgEspecieTit: TWideStringField
      FieldName = 'EspecieTit'
      Origin = 'cnab_cfg.EspecieTit'
      Size = 6
    end
    object QrCNAB_CfgEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
      Origin = 'cnab_cfg.EspecieDoc'
    end
    object QrCNAB_CfgAceiteTit: TSmallintField
      FieldName = 'AceiteTit'
      Origin = 'cnab_cfg.AceiteTit'
    end
    object QrCNAB_CfgInstrCobr1: TWideStringField
      FieldName = 'InstrCobr1'
      Origin = 'cnab_cfg.InstrCobr1'
      Size = 2
    end
    object QrCNAB_CfgInstrCobr2: TWideStringField
      FieldName = 'InstrCobr2'
      Origin = 'cnab_cfg.InstrCobr2'
      Size = 2
    end
    object QrCNAB_CfgInstrDias: TSmallintField
      FieldName = 'InstrDias'
      Origin = 'cnab_cfg.InstrDias'
    end
    object QrCNAB_CfgCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
      Origin = 'cnab_cfg.CodEmprBco'
    end
    object QrCNAB_CfgJurosTipo: TSmallintField
      FieldName = 'JurosTipo'
      Origin = 'cnab_cfg.JurosTipo'
    end
    object QrCNAB_CfgJurosPerc: TFloatField
      FieldName = 'JurosPerc'
      Origin = 'cnab_cfg.JurosPerc'
    end
    object QrCNAB_CfgJurosDias: TSmallintField
      FieldName = 'JurosDias'
      Origin = 'cnab_cfg.JurosDias'
    end
    object QrCNAB_CfgMultaTipo: TSmallintField
      FieldName = 'MultaTipo'
      Origin = 'cnab_cfg.MultaTipo'
    end
    object QrCNAB_CfgMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'cnab_cfg.MultaPerc'
    end
    object QrCNAB_CfgMultaDias: TSmallintField
      FieldName = 'MultaDias'
      Origin = 'cnab_cfg.MultaDias'
    end
    object QrCNAB_CfgTexto01: TWideStringField
      FieldName = 'Texto01'
      Origin = 'cnab_cfg.Texto01'
      Size = 100
    end
    object QrCNAB_CfgTexto02: TWideStringField
      FieldName = 'Texto02'
      Origin = 'cnab_cfg.Texto02'
      Size = 100
    end
    object QrCNAB_CfgTexto03: TWideStringField
      FieldName = 'Texto03'
      Origin = 'cnab_cfg.Texto03'
      Size = 100
    end
    object QrCNAB_CfgTexto04: TWideStringField
      FieldName = 'Texto04'
      Origin = 'cnab_cfg.Texto04'
      Size = 100
    end
    object QrCNAB_CfgTexto05: TWideStringField
      FieldName = 'Texto05'
      Origin = 'cnab_cfg.Texto05'
      Size = 100
    end
    object QrCNAB_CfgTexto06: TWideStringField
      FieldName = 'Texto06'
      Origin = 'cnab_cfg.Texto06'
      Size = 100
    end
    object QrCNAB_CfgTexto07: TWideStringField
      FieldName = 'Texto07'
      Origin = 'cnab_cfg.Texto07'
      Size = 100
    end
    object QrCNAB_CfgTexto08: TWideStringField
      FieldName = 'Texto08'
      Origin = 'cnab_cfg.Texto08'
      Size = 100
    end
    object QrCNAB_CfgTexto09: TWideStringField
      FieldName = 'Texto09'
      Origin = 'cnab_cfg.Texto09'
      Size = 100
    end
    object QrCNAB_CfgTexto10: TWideStringField
      FieldName = 'Texto10'
      Origin = 'cnab_cfg.Texto10'
      Size = 100
    end
    object QrCNAB_CfgCNAB: TIntegerField
      FieldName = 'CNAB'
      Origin = 'cnab_cfg.CNAB'
    end
    object QrCNAB_CfgEnvEmeio: TSmallintField
      FieldName = 'EnvEmeio'
      Origin = 'cnab_cfg.EnvEmeio'
    end
    object QrCNAB_CfgDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Origin = 'cnab_cfg.Diretorio'
      Size = 255
    end
    object QrCNAB_CfgQuemPrint: TWideStringField
      FieldName = 'QuemPrint'
      Origin = 'cnab_cfg.QuemPrint'
      Size = 1
    end
    object QrCNAB_Cfg_237Mens1: TWideStringField
      FieldName = '_237Mens1'
      Origin = 'cnab_cfg._237Mens1'
      Size = 12
    end
    object QrCNAB_Cfg_237Mens2: TWideStringField
      FieldName = '_237Mens2'
      Origin = 'cnab_cfg._237Mens2'
      Size = 60
    end
    object QrCNAB_CfgCodOculto: TWideStringField
      FieldName = 'CodOculto'
      Origin = 'cnab_cfg.CodOculto'
    end
    object QrCNAB_CfgSeqArq: TIntegerField
      FieldName = 'SeqArq'
      Origin = 'cnab_cfg.SeqArq'
    end
    object QrCNAB_CfgLastNosNum: TLargeintField
      FieldName = 'LastNosNum'
      Origin = 'cnab_cfg.LastNosNum'
    end
    object QrCNAB_CfgLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Origin = 'cnab_cfg.LocalPag'
      Size = 127
    end
    object QrCNAB_CfgEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Origin = 'cnab_cfg.EspecieVal'
      Size = 5
    end
    object QrCNAB_CfgOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Origin = 'cnab_cfg.OperCodi'
      Size = 3
    end
    object QrCNAB_CfgIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Origin = 'cnab_cfg.IDCobranca'
      Size = 2
    end
    object QrCNAB_CfgAgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Origin = 'cnab_cfg.AgContaCed'
      Size = 40
    end
    object QrCNAB_CfgDirRetorno: TWideStringField
      FieldName = 'DirRetorno'
      Origin = 'cnab_cfg.DirRetorno'
      Size = 255
    end
    object QrCNAB_CfgCartRetorno: TIntegerField
      FieldName = 'CartRetorno'
      Origin = 'cnab_cfg.CartRetorno'
    end
    object QrCNAB_CfgPosicoesBB: TSmallintField
      FieldName = 'PosicoesBB'
      Origin = 'cnab_cfg.PosicoesBB'
    end
    object QrCNAB_CfgIndicatBB: TWideStringField
      FieldName = 'IndicatBB'
      Origin = 'cnab_cfg.IndicatBB'
      Size = 1
    end
    object QrCNAB_CfgTipoCobrBB: TWideStringField
      FieldName = 'TipoCobrBB'
      Origin = 'cnab_cfg.TipoCobrBB'
      Size = 5
    end
    object QrCNAB_CfgVariacao: TIntegerField
      FieldName = 'Variacao'
      Origin = 'cnab_cfg.Variacao'
    end
    object QrCNAB_CfgComando: TIntegerField
      FieldName = 'Comando'
      Origin = 'cnab_cfg.Comando'
    end
    object QrCNAB_CfgDdProtesBB: TIntegerField
      FieldName = 'DdProtesBB'
      Origin = 'cnab_cfg.DdProtesBB'
    end
    object QrCNAB_CfgMsg40posBB: TWideStringField
      FieldName = 'Msg40posBB'
      Origin = 'cnab_cfg.Msg40posBB'
      Size = 40
    end
    object QrCNAB_CfgQuemDistrb: TWideStringField
      FieldName = 'QuemDistrb'
      Origin = 'cnab_cfg.QuemDistrb'
      Size = 1
    end
    object QrCNAB_CfgDesco1Cod: TSmallintField
      FieldName = 'Desco1Cod'
      Origin = 'cnab_cfg.Desco1Cod'
    end
    object QrCNAB_CfgDesco1Dds: TIntegerField
      FieldName = 'Desco1Dds'
      Origin = 'cnab_cfg.Desco1Dds'
    end
    object QrCNAB_CfgDesco1Fat: TFloatField
      FieldName = 'Desco1Fat'
      Origin = 'cnab_cfg.Desco1Fat'
    end
    object QrCNAB_CfgDesco2Cod: TSmallintField
      FieldName = 'Desco2Cod'
      Origin = 'cnab_cfg.Desco2Cod'
    end
    object QrCNAB_CfgDesco2Dds: TIntegerField
      FieldName = 'Desco2Dds'
      Origin = 'cnab_cfg.Desco2Dds'
    end
    object QrCNAB_CfgDesco2Fat: TFloatField
      FieldName = 'Desco2Fat'
      Origin = 'cnab_cfg.Desco2Fat'
    end
    object QrCNAB_CfgDesco3Cod: TSmallintField
      FieldName = 'Desco3Cod'
      Origin = 'cnab_cfg.Desco3Cod'
    end
    object QrCNAB_CfgDesco3Dds: TIntegerField
      FieldName = 'Desco3Dds'
      Origin = 'cnab_cfg.Desco3Dds'
    end
    object QrCNAB_CfgDesco3Fat: TFloatField
      FieldName = 'Desco3Fat'
      Origin = 'cnab_cfg.Desco3Fat'
    end
    object QrCNAB_CfgProtesCod: TSmallintField
      FieldName = 'ProtesCod'
      Origin = 'cnab_cfg.ProtesCod'
    end
    object QrCNAB_CfgProtesDds: TSmallintField
      FieldName = 'ProtesDds'
      Origin = 'cnab_cfg.ProtesDds'
    end
    object QrCNAB_CfgBxaDevCod: TSmallintField
      FieldName = 'BxaDevCod'
      Origin = 'cnab_cfg.BxaDevCod'
    end
    object QrCNAB_CfgBxaDevDds: TIntegerField
      FieldName = 'BxaDevDds'
      Origin = 'cnab_cfg.BxaDevDds'
    end
    object QrCNAB_CfgMoedaCod: TWideStringField
      FieldName = 'MoedaCod'
      Origin = 'cnab_cfg.MoedaCod'
    end
    object QrCNAB_CfgContrato: TFloatField
      FieldName = 'Contrato'
      Origin = 'cnab_cfg.Contrato'
    end
    object QrCNAB_CfgConvCartCobr: TWideStringField
      FieldName = 'ConvCartCobr'
      Origin = 'cnab_cfg.ConvCartCobr'
      Size = 2
    end
    object QrCNAB_CfgConvVariCart: TWideStringField
      FieldName = 'ConvVariCart'
      Origin = 'cnab_cfg.ConvVariCart'
      Size = 3
    end
    object QrCNAB_CfgInfNossoNum: TSmallintField
      FieldName = 'InfNossoNum'
      Origin = 'cnab_cfg.InfNossoNum'
    end
    object QrCNAB_CfgCodLidrBco: TWideStringField
      FieldName = 'CodLidrBco'
      Origin = 'cnab_cfg.CodLidrBco'
    end
    object QrCNAB_CfgLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cnab_cfg.Lk'
    end
    object QrCNAB_CfgDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cnab_cfg.DataCad'
    end
    object QrCNAB_CfgDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cnab_cfg.DataAlt'
    end
    object QrCNAB_CfgUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cnab_cfg.UserCad'
    end
    object QrCNAB_CfgUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cnab_cfg.UserAlt'
    end
    object QrCNAB_CfgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'cnab_cfg.AlterWeb'
    end
    object QrCNAB_CfgAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cnab_cfg.Ativo'
    end
    object QrCNAB_CfgCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Origin = 'cnab_cfg.CartEmiss'
    end
    object QrCNAB_CfgDVB: TWideStringField
      FieldName = 'DVB'
      Origin = 'bancos.DVB'
      Size = 1
    end
    object QrCNAB_CfgNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'bancos.Nome'
      Size = 100
    end
    object QrCNAB_CfgNOMECED_IMP: TWideStringField
      FieldName = 'NOMECED_IMP'
      Size = 100
    end
    object QrCNAB_CfgACEITETIT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ACEITETIT_TXT'
      Size = 1
      Calculated = True
    end
    object QrCNAB_CfgTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
    end
    object QrCNAB_CfgEspecieTxt: TWideStringField
      FieldName = 'EspecieTxt'
    end
    object QrCNAB_CfgCartTxt: TWideStringField
      FieldName = 'CartTxt'
      Size = 10
    end
    object QrCNAB_CfgLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrCNAB_CfgLayoutRet: TWideStringField
      FieldName = 'LayoutRet'
      Size = 50
    end
    object QrCNAB_CfgConcatCod: TWideStringField
      FieldName = 'ConcatCod'
      Size = 50
    end
    object QrCNAB_CfgComplmCod: TWideStringField
      FieldName = 'ComplmCod'
      Size = 10
    end
    object QrCNAB_CfgNaoRecebDd: TSmallintField
      FieldName = 'NaoRecebDd'
    end
    object QrCNAB_CfgTipBloqUso: TWideStringField
      FieldName = 'TipBloqUso'
      Size = 1
    end
    object QrCNAB_CfgNumVersaoI3: TIntegerField
      FieldName = 'NumVersaoI3'
    end
    object QrCNAB_CfgModalCobr: TSmallintField
      FieldName = 'ModalCobr'
    end
    object QrCNAB_CfgCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrCNAB_CfgTermoAceite: TIntegerField
      FieldName = 'TermoAceite'
    end
    object QrCNAB_CfgCorresBco: TIntegerField
      FieldName = 'CorresBco'
    end
    object QrCNAB_CfgCorresAge: TIntegerField
      FieldName = 'CorresAge'
    end
    object QrCNAB_CfgCorresCto: TWideStringField
      FieldName = 'CorresCto'
      Size = 30
    end
    object QrCNAB_CfgCART_IMP: TWideStringField
      FieldName = 'CART_IMP'
      Size = 10
    end
  end
end
