object FmBloArrItsRnw: TFmBloArrItsRnw
  Left = 339
  Top = 185
  Caption = 'ACT-RENEW-004 :: Itens de Arrecada'#231#245'es Vencidas'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 391
        Height = 32
        Caption = 'Itens de Arrecada'#231#245'es Vencidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 391
        Height = 32
        Caption = 'Itens de Arrecada'#231#245'es Vencidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 391
        Height = 32
        Caption = 'Itens de Arrecada'#231#245'es Vencidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object DBGBloArreIts: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 450
          Align = alClient
          Ctl3D = True
          DataSource = DsBloArreIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGBloArreItsDblClick
          FieldsCalcToOrder.Strings = (
            'EXPIRAEM=ParcPerF'
            'InfoParc_TXT=InfoParc'
            'Parc_Calc=Parcelas'
            'INICIO=ParcPerI'
            'FINAL=ParcPerF')
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EXPIRAEM'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'T'#233'rmino'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Entidade'
              Title.Caption = 'ID Entidade'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Entidade (Raz'#227'o social / Nome)'
              Width = 190
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FANENT'
              Title.Caption = 'Entidade (Fantasia / Apelido)'
              Width = 190
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DOCENT'
              Title.Caption = 'Entidade (CNPJ / CPF)'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SitCobr_TXT'
              Title.Caption = 'Sit. de cobran'#231'a'
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Parc_Calc'
              Title.Caption = 'Parc.'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Contrato'
              Title.Caption = 'ID Contrato'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DiaVcto_TXT'
              Title.Caption = 'Dia do vencto.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'INICIO'
              Title.Caption = 'In'#237'cio'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FINAL'
              Title.Caption = 'Final'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor_TXT'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InfoParc_TXT'
              Title.Caption = 'Info parc.'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Texto'
              Title.Caption = 'Descri'#231#227'o substituta'
              Width = 139
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Arredonda'
              Title.Caption = 'Arredondamento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEEMP'
              Title.Caption = 'Empresa'
              Width = 190
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECFG'
              Title.Caption = 'Configura'#231#227'o'
              Width = 150
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrBloArreIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.Fantasia, ent.Apelido) FANENT,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCENT,'
      'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP,'
      'cfg.Nome NOMECFG, '
      'CASE its.SitCobr '
      'WHEN 0 THEN "N'#227'o cobrar" '
      'WHEN 1 THEN "Cont'#237'nua (Anual)" '
      'WHEN 2 THEN "Programada" '
      'WHEN 3 THEN "Contrato" END SitCobr_TXT, '
      'enb.Filial, IF(its.InfoParc = 0, "N'#227'o", "Sim") InfoParc_TXT,'
      
        'IF(its.Contrato<>0, con.ddMesVcto, its.DiaVencto) + 0.000 DiaVct' +
        'o_TXT,'
      'IF(its.Contrato<>0, con.ValorMes, its.Valor) Valor_TXT,'
      'con.DVencimento, con.DtaPrxRenw, its.*'
      'FROM bloarreits its'
      'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade'
      'LEFT JOIN entidades enb ON enb.Codigo = its.Empresa'
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = its.CNAB_Cfg'
      'LEFT JOIN contratos con ON con.Codigo = its.Contrato'
      'WHERE its.Codigo=:P0'
      'AND its.Ativo=:P1')
    Left = 28
    Top = 116
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBloArreItsNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrBloArreItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'bloarreits.Codigo'
    end
    object QrBloArreItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'bloarreits.Controle'
    end
    object QrBloArreItsEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'bloarreits.Entidade'
    end
    object QrBloArreItsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'bloarreits.Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBloArreItsSitCobr: TIntegerField
      FieldName = 'SitCobr'
      Origin = 'bloarreits.SitCobr'
    end
    object QrBloArreItsParcelas: TIntegerField
      FieldName = 'Parcelas'
      Origin = 'bloarreits.Parcelas'
    end
    object QrBloArreItsParcPerI: TIntegerField
      FieldName = 'ParcPerI'
      Origin = 'bloarreits.ParcPerI'
    end
    object QrBloArreItsParcPerF: TIntegerField
      FieldName = 'ParcPerF'
      Origin = 'bloarreits.ParcPerF'
    end
    object QrBloArreItsInfoParc: TSmallintField
      FieldName = 'InfoParc'
      Origin = 'bloarreits.InfoParc'
    end
    object QrBloArreItsArredonda: TFloatField
      FieldName = 'Arredonda'
      Origin = 'bloarreits.Arredonda'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBloArreItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'bloarreits.Lk'
    end
    object QrBloArreItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'bloarreits.DataCad'
    end
    object QrBloArreItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'bloarreits.DataAlt'
    end
    object QrBloArreItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'bloarreits.UserCad'
    end
    object QrBloArreItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'bloarreits.UserAlt'
    end
    object QrBloArreItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'bloarreits.AlterWeb'
    end
    object QrBloArreItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'bloarreits.Ativo'
    end
    object QrBloArreItsValorInf: TSmallintField
      FieldName = 'ValorInf'
      Origin = 'bloarreits.ValorInf'
    end
    object QrBloArreItsFINAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Calculated = True
    end
    object QrBloArreItsINICIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'INICIO'
      Calculated = True
    end
    object QrBloArreItsCnab_Cfg: TIntegerField
      FieldName = 'Cnab_Cfg'
    end
    object QrBloArreItsDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
    end
    object QrBloArreItsEXPIROU: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'EXPIROU'
      Calculated = True
    end
    object QrBloArreItsEXPIRAEM: TWideStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'EXPIRAEM'
      Size = 50
      Calculated = True
    end
    object QrBloArreItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrBloArreItsNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrBloArreItsFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrBloArreItsContrato: TIntegerField
      FieldName = 'Contrato'
    end
    object QrBloArreItsValor_TXT: TFloatField
      FieldName = 'Valor_TXT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBloArreItsDVencimento: TDateField
      FieldName = 'DVencimento'
    end
    object QrBloArreItsDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
    end
    object QrBloArreItsSitCobr_TXT: TWideStringField
      FieldName = 'SitCobr_TXT'
    end
    object QrBloArreItsNOMECFG: TWideStringField
      FieldName = 'NOMECFG'
      Size = 50
    end
    object QrBloArreItsDiaVcto_TXT: TFloatField
      FieldName = 'DiaVcto_TXT'
    end
    object QrBloArreItsParc_Calc: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Parc_Calc'
      Calculated = True
    end
    object QrBloArreItsTexto: TWideStringField
      FieldName = 'Texto'
      Size = 100
    end
    object QrBloArreItsInfoParc_TXT: TWideStringField
      FieldName = 'InfoParc_TXT'
      Size = 3
    end
    object QrBloArreItsFANENT: TWideStringField
      FieldName = 'FANENT'
      Size = 60
    end
    object QrBloArreItsDOCENT: TWideStringField
      FieldName = 'DOCENT'
      Size = 18
    end
  end
  object DsBloArreIts: TDataSource
    DataSet = QrBloArreIts
    Left = 56
    Top = 116
  end
end
