unit BloArre;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, Math,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkEditCB, UnDmkProcFunc,
  dmkDBLookupComboBox, Menus, Grids, DBGrids, dmkDBGrid, ComCtrls, dmkImage,
  DmkDAC_PF, UnDmkEnums;

type
  TFmBloArre = class(TForm)
    PainelDados: TPanel;
    DsBloArre: TDataSource;
    QrBloArre: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label9: TLabel;
    QrBloArreCodigo: TIntegerField;
    QrBloArreConta: TIntegerField;
    QrBloArreNome: TWideStringField;
    QrBloArreSigla: TWideStringField;
    QrBloArreLk: TIntegerField;
    QrBloArreDataCad: TDateField;
    QrBloArreDataAlt: TDateField;
    QrBloArreUserCad: TIntegerField;
    QrBloArreUserAlt: TIntegerField;
    QrBloArreAlterWeb: TSmallintField;
    QrBloArreAtivo: TSmallintField;
    EdSigla: TdmkEdit;
    Label4: TLabel;
    LaForneceI: TLabel;
    CBConta: TdmkDBLookupComboBox;
    EdConta: TdmkEditCB;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    Label5: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label6: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    QrBloArreNOMECONTA: TWideStringField;
    PMBloArre: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    PMEntidade: TPopupMenu;
    Adicionaentidadearrecadaoselecionada1: TMenuItem;
    Editaarrecadaoselecionada1: TMenuItem;
    Retiraentidadedaarrecadaoselecionada1: TMenuItem;
    QrBloArreIts: TmySQLQuery;
    DsBloArreIts: TDataSource;
    QrBloArreItsNOMEENT: TWideStringField;
    QrBloArreItsCodigo: TIntegerField;
    QrBloArreItsControle: TIntegerField;
    QrBloArreItsEntidade: TIntegerField;
    QrBloArreItsValor: TFloatField;
    QrBloArreItsSitCobr: TIntegerField;
    QrBloArreItsParcelas: TIntegerField;
    QrBloArreItsParcPerI: TIntegerField;
    QrBloArreItsParcPerF: TIntegerField;
    QrBloArreItsInfoParc: TSmallintField;
    QrBloArreItsArredonda: TFloatField;
    QrBloArreItsLk: TIntegerField;
    QrBloArreItsDataCad: TDateField;
    QrBloArreItsDataAlt: TDateField;
    QrBloArreItsUserCad: TIntegerField;
    QrBloArreItsUserAlt: TIntegerField;
    QrBloArreItsAlterWeb: TSmallintField;
    QrBloArreItsAtivo: TSmallintField;
    QrBloArreItsValorInf: TSmallintField;
    QrBloArreItsFINAL: TWideStringField;
    QrBloArreItsINICIO: TWideStringField;
    QrBloArreItsCnab_Cfg: TIntegerField;
    QrBloArreItsDiaVencto: TIntegerField;
    QrBloArreItsEXPIROU: TBooleanField;
    QrBloArreItsEXPIRAEM: TWideStringField;
    TabControl1: TTabControl;
    DBGBloArreIts: TdmkDBGrid;
    QrBloArreItsEmpresa: TIntegerField;
    QrBloArreItsNOMEEMP: TWideStringField;
    QrBloArreItsFilial: TIntegerField;
    SpeedButton5: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel4: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtBloArre: TBitBtn;
    BtEntidade: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrBloArreItsContrato: TIntegerField;
    QrBloArreItsValor_TXT: TFloatField;
    QrBloArreItsDVencimento: TDateField;
    QrBloArreItsDtaPrxRenw: TDateField;
    QrBloArreItsSitCobr_TXT: TWideStringField;
    QrBloArreItsNOMECFG: TWideStringField;
    QrBloArreItsDiaVcto_TXT: TFloatField;
    N1: TMenuItem;
    Desativararrecadaoselecionada1: TMenuItem;
    QrLoc: TmySQLQuery;
    N2: TMenuItem;
    ReajustarutilizandoIGPM1: TMenuItem;
    QrBloArreItsParc_Calc: TIntegerField;
    QrBloArreItsTexto: TWideStringField;
    QrBloArreItsInfoParc_TXT: TWideStringField;
    QrBloArreItsFANENT: TWideStringField;
    QrBloArreItsDOCENT: TWideStringField;
    LaTotal: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrBloArreAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrBloArreBeforeOpen(DataSet: TDataSet);
    procedure BtBloArreClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure PMBloArrePopup(Sender: TObject);
    procedure BtEntidadeClick(Sender: TObject);
    procedure Adicionaentidadearrecadaoselecionada1Click(Sender: TObject);
    procedure PMEntidadePopup(Sender: TObject);
    procedure Editaarrecadaoselecionada1Click(Sender: TObject);
    procedure QrBloArreAfterScroll(DataSet: TDataSet);
    procedure QrBloArreBeforeClose(DataSet: TDataSet);
    procedure QrBloArreItsCalcFields(DataSet: TDataSet);
    procedure DBGBloArreItsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TabControl1Change(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Retiraentidadedaarrecadaoselecionada1Click(Sender: TObject);
    procedure Desativararrecadaoselecionada1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure ReajustarutilizandoIGPM1Click(Sender: TObject);
    procedure QrBloArreItsAfterScroll(DataSet: TDataSet);
    procedure QrBloArreItsBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenBloArreIts(Controle: Integer);
  end;

var
  FmBloArre: TFmBloArre;
const
  FFormatFloat = '00000';

implementation

uses Module, BloArreIts, MyDBCheck, Contas,  UnMyObjects, ModuleGeral,
  UnBloquetos, UnFinanceiroJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmBloArre.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmBloArre.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrBloArreCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmBloArre.DefParams;
begin
  VAR_GOTOTABELA := 'bloarre';
  VAR_GOTOMYSQLTABLE := QrBloArre;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT con.Nome NOMECONTA, blo.*');
  VAR_SQLx.Add('FROM bloarre blo');
  VAR_SQLx.Add('LEFT JOIN contas con ON con.Codigo = blo.Conta');
  VAR_SQLx.Add('WHERE blo.Codigo > 0');
  //
  VAR_SQL1.Add('AND blo.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND blo.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND blo.Nome Like :P0');
end;

procedure TFmBloArre.Desativararrecadaoselecionada1Click(Sender: TObject);
var
  Controle, Ativo: Integer;
begin
  Controle := QrBloArreItsControle.Value;
  Ativo    := QrBloArreItsAtivo.Value;
  //
  if UBloquetos.AtivaDesativaArreIts(Controle, Ativo) then
  begin
    try
      Screen.Cursor := crHourGlass;
      //    
      if Ativo = 0 then
        TabControl1.TabIndex := 1
      else
        TabControl1.TabIndex := 0;
      //
      ReopenBloArreIts(Controle);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmBloArre.Editaarrecadaoselecionada1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmBloArreIts, FmBloArreIts, afmoNegarComAviso) then
  begin
    FmBloArreIts.ImgTipo.SQLType := stUpd;
    FmBloArreIts.FMostra        := True;
    FmBloArreIts.FCodigo        := QrBloArreCodigo.Value;
    FmBloArreIts.FControle      := QrBloArreItsControle.Value;
    FmBloArreIts.ShowModal;
    FmBloArreIts.Destroy;
  end;
end;

procedure TFmBloArre.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrBloArreCodigo.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM arreits ',
    'WHERE BloArre=' + Geral.FF0(Codigo),
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
      'Motivo: Esta arrecada��o j� foi utilizada!');
    Exit;
  end;
  try
    UMyMod.ExcluiRegistroInt1('Deseja excluir a arrecada��o selecionada?',
      'bloarre', 'Codigo', Codigo, Dmod.MyDB);
  finally
    Va(vpLast);
  end;
end;

procedure TFmBloArre.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmBloArre.PMBloArrePopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrBloArre.State <> dsInactive) and (QrBloArre.RecordCount > 0);
  Enab2 := (QrBloArreIts.State <> dsInactive) and (QrBloArreIts.RecordCount > 0);
  //
  Altera1.Enabled := Enab;
  Exclui1.Enabled := Enab and (not Enab2);
end;

procedure TFmBloArre.PMEntidadePopup(Sender: TObject);
var
  Enab, Enab2, EnabIGPM: Boolean;
begin
  EnabIGPM := False;
  Enab     := (QrBloArre.State <> dsInactive) and (QrBloArre.RecordCount > 0);
  Enab2    := (QrBloArreIts.State <> dsInactive) and (QrBloArreIts.RecordCount > 0);
  //
  Adicionaentidadearrecadaoselecionada1.Enabled := Enab;
  Editaarrecadaoselecionada1.Enabled            := Enab and Enab2;
  Retiraentidadedaarrecadaoselecionada1.Enabled := Enab and Enab2;
  Desativararrecadaoselecionada1.Enabled        := Enab and Enab2; 
  //
  if Desativararrecadaoselecionada1.Enabled then
  begin
    if QrBloArreItsAtivo.Value = 1 then
      Desativararrecadaoselecionada1.Caption := 'Desativar arrecada��o &selecionada'
    else
      Desativararrecadaoselecionada1.Caption := 'Ativar arrecada��o &selecionada';
  end;
  if Enab2 then
    EnabIGPM := (QrBloArreItsValor_TXT.Value <> 0) and
      (QrBloArreItsSitCobr.Value in [2, 3]); //Programada ou Contrato
  //
  ReajustarutilizandoIGPM1.Enabled := Enab and Enab2 and EnabIGPM;    
end;

procedure TFmBloArre.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmBloArre.QueryPrincipalAfterOpen;
begin
end;

procedure TFmBloArre.ReajustarutilizandoIGPM1Click(Sender: TObject);
var
  IDNum, SitCobr, PeriodoI, PeriodoF: Integer;
  Valor: Double;
begin
  Valor   := QrBloArreItsValor_TXT.Value;
  SitCobr := QrBloArreItsSitCobr.Value;
  //
  if SitCobr = 2 then //Programada
  begin
    IDNum    := QrBloArreItsControle.Value;
    PeriodoI := QrBloArreItsParcPerI.Value;
    PeriodoF := QrBloArreItsParcPerF.Value; 
  end else
  begin  //Contrato
    IDNum    := QrBloArreItsContrato.Value;
    PeriodoI := Geral.Periodo2000(QrBloArreItsDVencimento.Value);
    PeriodoF := Geral.Periodo2000(QrBloArreItsDtaPrxRenw.Value);
  end;
  //
  UBloquetos.ReajustarArrecadacaoIGPM(PeriodoI, PeriodoF, SitCobr, IDNum, Valor);
  //
  try
    Screen.Cursor := crHourGlass;
    //
    ReopenBloArreIts(QrBloArreItsControle.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBloArre.ReopenBloArreIts(Controle: Integer);
begin
(*
  QrBloArreIts.Close;
  QrBloArreIts.Params[0].AsInteger := QrBloArreCodigo.Value;
  QrBloArreIts.Params[1].AsInteger := Geral.BoolToInt(TabControl1.TabIndex = 0);
  QrBloArreIts. O p e n ;
*)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBloArreIts, Dmod.MyDB, [
  'SELECT ',
  'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
  'IF(ent.Tipo=0, ent.Fantasia, ent.Apelido) FANENT, ',
  'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCENT, ',
  'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP, ',
  'cfg.Nome NOMECFG, ',
  'CASE its.SitCobr ',
  'WHEN 0 THEN "N�o cobrar" ',
  'WHEN 1 THEN "Cont�nua (Anual)" ',
  'WHEN 2 THEN "Programada" ',
  'WHEN 3 THEN "Contrato" END SitCobr_TXT, ',
  'enb.Filial, IF(its.InfoParc = 0, "N�o", "Sim") InfoParc_TXT, ',
{$IfNDef sCNTR}
  'IF(its.Contrato<>0, con.ddMesVcto, its.DiaVencto) + 0.000 DiaVcto_TXT, ',
  'IF(its.Contrato<>0, con.ValorMes, its.Valor) Valor_TXT, ',
  'con.DVencimento, con.DtaPrxRenw, ',
{$else}
  '(its.DiaVencto) + 0.000 DiaVcto_TXT, ',
  '(its.Valor) Valor_TXT, ',
  'DATE("1899-12-30") DVencimento, DATE("1899-12-30") DtaPrxRenw, ',
{$EndIf}
  'its.* ',
  'FROM bloarreits its ',
  'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade ',
  'LEFT JOIN entidades enb ON enb.Codigo = its.Empresa ',
  'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = its.CNAB_Cfg ',
{$IfNDef sCNTR}
  'LEFT JOIN contratos con ON con.Codigo = its.Contrato ',
{$EndIf}
  'WHERE its.Codigo=' + Geral.FF0(QrBloArreCodigo.Value),
  'AND its.Ativo=' + Geral.FF0(Geral.BoolToInt(TabControl1.TabIndex = 0)),
  '']);
  //
  if Controle <> 0 then
    QrBloArreIts.Locate('Controle', Controle, []);
end;

procedure TFmBloArre.Retiraentidadedaarrecadaoselecionada1Click(
  Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrBloArreItsControle.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM arreits ',
    'WHERE BloArreIts=' + Geral.FF0(Controle),
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
      'Motivo: Este item de arrecada��o j� foi utilizado!');
    Exit;
  end;
  try
    UMyMod.ExcluiRegistroInt1('Deseja excluir o item de arrecada��o selecionado?',
      'bloarreits', 'Controle', Controle, Dmod.MyDB);
  finally
    ReopenBloArreIts(0);
  end;
end;

procedure TFmBloArre.DBGBloArreItsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if Column.FieldName = 'EXPIRAEM' then
  begin
    if QrBloArreItsEXPIROU.Value then
      Cor := clRed
    else
      Cor := clGreen;
    with DBGBloArreIts.Canvas do
      Font.Color := Cor;
  end;
end;

procedure TFmBloArre.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmBloArre.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmBloArre.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmBloArre.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmBloArre.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmBloArre.SpeedButton5Click(Sender: TObject);
var
  Conta: Integer;
begin
  VAR_CADASTRO := 0;
  Conta        := EdConta.ValueVariant;
  //
  FinanceiroJan.CadastroDeContas(Conta);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdConta, CBConta, QrContas, VAR_CADASTRO, 'Codigo');
    EdConta.SetFocus;
  end;
end;

procedure TFmBloArre.TabControl1Change(Sender: TObject);
begin
  ReopenBloArreIts(0);
end;

procedure TFmBloArre.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrBloArreCodigo.Value;
  Close;
end;

procedure TFmBloArre.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('bloarre', 'Codigo', ImgTipo.SQLType,
    QrBloArreCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmBloArre, PainelEdit,
    'bloarre', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmBloArre.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'bloarre', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'bloarre', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'bloarre', 'Codigo');
end;

procedure TFmBloArre.BtEntidadeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntidade, BtEntidade);
end;

procedure TFmBloArre.Adicionaentidadearrecadaoselecionada1Click(
  Sender: TObject);
begin
  if QrBloArre.RecordCount > 0 then
  begin
    if DBCheck.CriaFm(TFmBloArreIts, FmBloArreIts, afmoNegarComAviso) then
    begin
      FmBloArreIts.ImgTipo.SQLType := stIns;
      FmBloArreIts.FMostra         := True;
      FmBloArreIts.FCodigo         := QrBloArreCodigo.Value;
      FmBloArreIts.ShowModal;
      FmBloArreIts.Destroy;      
    end;
  end else
    Geral.MB_Aviso('Arrecada��o n�o definida!');
end;

procedure TFmBloArre.Altera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrBloArre, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'bloarre');
end;

procedure TFmBloArre.BtBloArreClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBloArre, BtBloArre);
end;

procedure TFmBloArre.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align     := alClient;
  TabControl1.Align    := alClient;
  TabControl1.TabIndex := 0;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmBloArre.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrBloArreCodigo.Value, LaRegistro.Caption);
end;

procedure TFmBloArre.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmBloArre.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmBloArre.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmBloArre.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmBloArre.QrBloArreAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmBloArre.QrBloArreAfterScroll(DataSet: TDataSet);
begin
  ReopenBloArreIts(0);
end;

procedure TFmBloArre.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmBloArre.SbQueryClick(Sender: TObject);
begin
  LocCod(QrBloArreCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'bloarre', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmBloArre.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloArre.Inclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrBloArre, [PainelDados],
    [PainelEdita], EdNome, ImgTipo, 'bloarre');
end;

procedure TFmBloArre.QrBloArreBeforeClose(DataSet: TDataSet);
begin
  QrBloArreIts.Close;
end;

procedure TFmBloArre.QrBloArreBeforeOpen(DataSet: TDataSet);
begin
  QrBloArreCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmBloArre.QrBloArreItsAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrBloArreIts.RecordCount)
end;

procedure TFmBloArre.QrBloArreItsBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmBloArre.QrBloArreItsCalcFields(DataSet: TDataSet);
var
  Expirou: Boolean;
  Ini, Fim, ExpiraEm: String;
  Parcelas: Integer;
begin
  Parcelas := 0;
  Expirou  := UBloquetos.CalculaPeriodoIniFimArrecadacao(QrBloArreItsSitCobr.Value,
    QrBloArreItsParcPerI.Value, QrBloArreItsParcPerF.Value,
    QrBloArreItsParcelas.Value, QrBloArreItsDiaVcto_TXT.Value,
    QrBloArreItsDVencimento.Value, QrBloArreItsDtaPrxRenw.Value, Ini, Fim,
    ExpiraEm, Parcelas);
  //
  QrBloArreItsINICIO.Value    := Ini;
  QrBloArreItsFINAL.Value     := Fim;
  QrBloArreItsEXPIRAEM.Value  := ExpiraEm;
  QrBloArreItsEXPIROU.Value   := Expirou;
  QrBloArreItsParc_Calc.Value := Parcelas;
end;

end.

