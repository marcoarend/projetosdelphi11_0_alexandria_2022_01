object FmBloGerenNew: TFmBloGerenNew
  Left = 339
  Top = 185
  ActiveControl = PageControl1
  BorderStyle = bsDialog
  Caption = 'BLO-GEREN-002 :: Or'#231'amento'
  ClientHeight = 436
  ClientWidth = 814
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 814
    Height = 280
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 340
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 814
      Height = 280
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 340
      object TabSheet4: TTabSheet
        Caption = 'Per'#237'odo'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 312
        object Label32: TLabel
          Left = 8
          Top = 15
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object LaAnoI: TLabel
          Left = 183
          Top = 15
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object CBMes: TComboBox
          Left = 8
          Top = 32
          Width = 172
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object CBAno: TComboBox
          Left = 183
          Top = 32
          Width = 78
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Boleto '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 312
        object RGModelBloq: TRadioGroup
          Left = 0
          Top = 0
          Width = 806
          Height = 172
          Align = alClient
          Caption = ' Modelo de impress'#227'o do boleto: '
          ItemIndex = 0
          Items.Strings = (
            'UnBloquetos.PreencheModelosBloq')
          TabOrder = 0
          OnClick = RGModelBloqClick
          ExplicitHeight = 232
        end
        object RGCompe: TRadioGroup
          Left = 0
          Top = 212
          Width = 806
          Height = 40
          Align = alBottom
          Caption = ' Ficha de compensa'#231#227'o (modelo E): '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            '??'
            '1 (uma via)'
            '2 (duas vias)')
          TabOrder = 1
          Visible = False
          ExplicitTop = 272
        end
        object RGBloqFV: TRadioGroup
          Left = 0
          Top = 172
          Width = 806
          Height = 40
          Align = alBottom
          Caption = 'Frente e verso?'
          Columns = 2
          ItemIndex = 1
          Items.Strings = (
            'N'#227'o'
            'Sim')
          TabOrder = 2
          ExplicitTop = 232
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'Parte interna do boleto'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 312
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 806
          Height = 252
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 312
          object Label11: TLabel
            Left = 63
            Top = 20
            Width = 338
            Height = 13
            AutoSize = False
            Caption = '12345678901234567890123456789012345678901234567'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label12: TLabel
            Left = 63
            Top = 8
            Width = 338
            Height = 13
            AutoSize = False
            Caption = '         1         2         3         4'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label1: TLabel
            Left = 12
            Top = 40
            Width = 38
            Height = 13
            Caption = 'Linha 1:'
          end
          object Label2: TLabel
            Left = 12
            Top = 60
            Width = 38
            Height = 13
            Caption = 'Linha 2:'
          end
          object Label3: TLabel
            Left = 12
            Top = 80
            Width = 38
            Height = 13
            Caption = 'Linha 3:'
          end
          object Label4: TLabel
            Left = 12
            Top = 100
            Width = 38
            Height = 13
            Caption = 'Linha 4:'
          end
          object Label5: TLabel
            Left = 12
            Top = 120
            Width = 38
            Height = 13
            Caption = 'Linha 5:'
          end
          object Label6: TLabel
            Left = 12
            Top = 140
            Width = 38
            Height = 13
            Caption = 'Linha 6:'
          end
          object Label10: TLabel
            Left = 12
            Top = 160
            Width = 38
            Height = 13
            Caption = 'Linha 7:'
          end
          object Label9: TLabel
            Left = 12
            Top = 180
            Width = 38
            Height = 13
            Caption = 'Linha 8:'
          end
          object Label8: TLabel
            Left = 12
            Top = 200
            Width = 38
            Height = 13
            Caption = 'Linha 9:'
          end
          object Label7: TLabel
            Left = 12
            Top = 220
            Width = 44
            Height = 13
            Caption = 'Linha 10:'
          end
          object Edit9: TEdit
            Left = 60
            Top = 196
            Width = 337
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 8
          end
          object Edit10: TEdit
            Left = 60
            Top = 217
            Width = 337
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 9
          end
          object Edit8: TEdit
            Left = 60
            Top = 176
            Width = 337
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 7
          end
          object Edit7: TEdit
            Left = 60
            Top = 156
            Width = 337
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 6
          end
          object Edit6: TEdit
            Left = 60
            Top = 136
            Width = 337
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 5
          end
          object Edit5: TEdit
            Left = 60
            Top = 116
            Width = 337
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 4
          end
          object Edit4: TEdit
            Left = 60
            Top = 96
            Width = 337
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 3
          end
          object Edit3: TEdit
            Left = 60
            Top = 76
            Width = 337
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 2
          end
          object Edit2: TEdit
            Left = 60
            Top = 56
            Width = 337
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 1
          end
          object Edit1: TEdit
            Left = 60
            Top = 36
            Width = 337
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 0
          end
          object BitBtn1: TBitBtn
            Left = 400
            Top = 80
            Width = 160
            Height = 40
            Caption = '&Abaixa uma linha (todos)'
            NumGlyphs = 2
            TabOrder = 10
            OnClick = BitBtn1Click
          end
          object BitBtn2: TBitBtn
            Left = 400
            Top = 36
            Width = 160
            Height = 40
            Caption = '&Sobe uma linha (todos)'
            NumGlyphs = 2
            TabOrder = 11
            OnClick = BitBtn2Click
          end
          object GroupBox1: TGroupBox
            Left = 400
            Top = 126
            Width = 341
            Height = 49
            Caption = ' Modelos de impress'#227'o e tamanho m'#225'ximo da linha (em carateres): '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = ' Modelos de impress'#227'o e tamanho'
            Font.Style = []
            ParentFont = False
            TabOrder = 12
            object Label13: TLabel
              Left = 12
              Top = 15
              Width = 168
              Height = 14
              Caption = 'Modelo A: 40 caracteres.'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
            end
            object Label27: TLabel
              Left = 12
              Top = 31
              Width = 322
              Height = 14
              Caption = 'Modelo E: Avisos n'#227'o aplic'#225'veis a este modelo.'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
            end
          end
          object Edit0: TEdit
            Left = 400
            Top = 177
            Width = 100
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 13
            Visible = False
          end
          object Edit11: TEdit
            Left = 400
            Top = 204
            Width = 100
            Height = 22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            MaxLength = 47
            ParentFont = False
            TabOrder = 14
            Visible = False
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Parte externa do boleto (Verso)'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 312
        object Label15: TLabel
          Left = 0
          Top = 0
          Width = 30
          Height = 13
          Align = alTop
          Caption = 'Texto:'
        end
        object MeAvisoVerso: TMemo
          Left = 0
          Top = 13
          Width = 806
          Height = 37
          Align = alTop
          TabOrder = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 814
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 766
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 718
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 134
        Height = 32
        Caption = 'Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 134
        Height = 32
        Caption = 'Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 134
        Height = 32
        Caption = 'Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 328
    Width = 814
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 388
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 810
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 372
    Width = 814
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 432
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 810
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 666
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM Prev'
      'WHERE Cond=:P0')
    Left = 712
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 448
    Top = 11
  end
end
