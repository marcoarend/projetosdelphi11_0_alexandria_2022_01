unit ModuleBloGeren;

interface

uses
  SysUtils, Classes, DB, mySQLDbTables, Dialogs, Forms, Windows,
  DBGrids, frxClass, dmkGeral, frxDBSet, Controls, Variants, DmkDAC_PF,
  dmkDBGrid, ComCtrls, UnDmkProcFunc, UnDmkEnums;

type
  TDBloGeren = class(TDataModule)
    QrNIA: TmySQLQuery;
    QrNIACodigo: TIntegerField;
    QrNIANome: TWideStringField;
    QrNIAConta: TIntegerField;
    QrNIAValor: TFloatField;
    QrNIASitCobr: TIntegerField;
    QrNIAParcelas: TIntegerField;
    QrNIAParcPerI: TIntegerField;
    QrNIAParcPerF: TIntegerField;
    QrNIAInfoParc: TSmallintField;
    QrNIATexto: TWideStringField;
    QrNIAControle: TIntegerField;
    QrNIAArredonda: TFloatField;
    QrNIAEntidade: TIntegerField;
    QrNIANOMEENT: TWideStringField;
    QrNIACNAB_Cfg: TIntegerField;
    QrNIACartEmiss: TIntegerField;
    QrLocArre: TmySQLQuery;
    QrNIAValorInf: TSmallintField;
    QrPesqMB: TmySQLQuery;
    QrSumBol: TmySQLQuery;
    QrSumBolVALOR: TFloatField;
    QrSumPre: TmySQLQuery;
    QrSumPreVALOR: TFloatField;
    QrLoc: TmySQLQuery;
    QrPesqMBBoleto: TFloatField;
    QrBloOpcoes: TmySQLQuery;
    QrNIADVencimento: TDateField;
    QrNIADtaPrxRenw: TDateField;
    QrNIADiaVencto: TFloatField;
    QrCNABcfg: TmySQLQuery;
    QrCNABcfgCodigo: TIntegerField;
    QrCNABcfgNome: TWideStringField;
    DsCNABcfg: TDataSource;
    QrContas: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    QrArrePesq: TmySQLQuery;
    QrArrePesqCodigo: TIntegerField;
    QrArrePesqNome: TWideStringField;
    DsArrePesq: TDataSource;
    QrEntidades: TmySQLQuery;
    IntegerField2: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    QrBloArreItsVen: TmySQLQuery;
    QrBloArreItsVenNOMEENT: TWideStringField;
    QrBloArreItsVenCodigo: TIntegerField;
    QrBloArreItsVenControle: TIntegerField;
    QrBloArreItsVenEntidade: TIntegerField;
    QrBloArreItsVenValor: TFloatField;
    QrBloArreItsVenSitCobr: TIntegerField;
    QrBloArreItsVenParcelas: TIntegerField;
    QrBloArreItsVenParcPerI: TIntegerField;
    QrBloArreItsVenParcPerF: TIntegerField;
    QrBloArreItsVenInfoParc: TSmallintField;
    QrBloArreItsVenArredonda: TFloatField;
    QrBloArreItsVenLk: TIntegerField;
    QrBloArreItsVenDataCad: TDateField;
    QrBloArreItsVenDataAlt: TDateField;
    QrBloArreItsVenUserCad: TIntegerField;
    QrBloArreItsVenUserAlt: TIntegerField;
    QrBloArreItsVenAlterWeb: TSmallintField;
    QrBloArreItsVenAtivo: TSmallintField;
    QrBloArreItsVenValorInf: TSmallintField;
    QrBloArreItsVenFINAL: TWideStringField;
    QrBloArreItsVenINICIO: TWideStringField;
    QrBloArreItsVenCnab_Cfg: TIntegerField;
    QrBloArreItsVenDiaVencto: TIntegerField;
    QrBloArreItsVenEXPIROU: TBooleanField;
    QrBloArreItsVenEXPIRAEM: TWideStringField;
    QrBloArreItsVenEmpresa: TIntegerField;
    QrBloArreItsVenNOMEEMP: TWideStringField;
    QrBloArreItsVenFilial: TIntegerField;
    QrBloArreItsVenContrato: TIntegerField;
    QrBloArreItsVenValor_TXT: TFloatField;
    QrBloArreItsVenSitCobr_TXT: TWideStringField;
    QrBloArreItsVenNOMECFG: TWideStringField;
    QrBloArreItsVenDiaVcto_TXT: TFloatField;
    QrBloArreItsVenParc_Calc: TIntegerField;
    QrBloArreItsVenTexto: TWideStringField;
    DsBloArreItsVen: TDataSource;
    QrBloArreItsVenDVencimento: TDateField;
    QrBloArreItsVenDtaPrxRenw: TDateField;
    QrBloArreItsVenInfoParc_TXT: TWideStringField;
    QrBloArreItsVenNOMEARRECAD: TWideStringField;
    QrProtocoBol: TmySQLQuery;
    QrProtocoBolVencto: TDateField;
    QrProtocoBolEntidade: TIntegerField;
    QrProtocoBolValor: TFloatField;
    QrProtocoBolNOMECNAB_Cfg: TWideStringField;
    QrProtocoBolNOMEENT: TWideStringField;
    QrProtocoBolBoleto: TFloatField;
    QrProtocoBolCNAB_Cfg: TIntegerField;
    QrProtocoBolPROTOCOD: TIntegerField;
    QrProtocoBolPROTOCOLO: TIntegerField;
    QrProtocoBolTAREFA: TWideStringField;
    QrProtocoBolDELIVER: TWideStringField;
    QrProtocoBolLOTE: TIntegerField;
    QrProtocoBolVENCTO_PROT: TDateField;
    QrProtocoBolMULTAVAL: TFloatField;
    QrProtocoBolPREVCOD: TIntegerField;
    QrProtocoBolPeriodo: TIntegerField;
    QrProtocoBolMORADIAVAL: TFloatField;
    DsProtocoBol: TDataSource;
    QrArre: TmySQLQuery;
    QrArreNomeArre: TWideStringField;
    QrArreNomeEnti: TWideStringField;
    QrArreNomeContrato: TWideStringField;
    QrArreBloArre: TIntegerField;
    QrArreBloArreIts: TIntegerField;
    QrArreCodContrato: TIntegerField;
    QrArreAdiciona: TSmallintField;
    QrArreControle: TIntegerField;
    DsArre: TDataSource;
    DsArreIts: TDataSource;
    QrArreIts: TmySQLQuery;
    QrArreItsAdiciona: TSmallintField;
    QrArreItsValor: TFloatField;
    QrArreItsTexto: TWideStringField;
    QrArreItsControle: TIntegerField;
    QrArreItsDiaVencto: TIntegerField;
    QrArreItsPeriodo: TIntegerField;
    QrArreItsJurosPerc: TFloatField;
    QrArreItsMultaPerc: TFloatField;
    QrArreItsEntidade: TIntegerField;
    QrArreItsCartEmiss: TIntegerField;
    QrArreItsConta: TIntegerField;
    QrArreItsBloArre: TIntegerField;
    QrArreItsBloArreIts: TIntegerField;
    QrArreItsCNAB_Cfg: TIntegerField;
    QrLocArreAll: TmySQLQuery;
    QrProtocoBolProtocoPak: TIntegerField;
    QrProtocoBolTipoProt: TIntegerField;
    QrProtocoBolEmail: TWideStringField;
    QrProtocoBolEMeio_ID: TFloatField;
    QrProtocoBolArreits: TIntegerField;
    QrProtocoBolDataE_Txt: TWideStringField;
    QrProtocoBolDataD_Txt: TWideStringField;
    QrProtocoBolDataE: TDateField;
    QrProtocoBolDataD: TDateTimeField;
    procedure QrBloArreItsVenCalcFields(DataSet: TDataSet);
    procedure QrArreAfterScroll(DataSet: TDataSet);
    procedure QrArreBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FBloArreAll: String;
    (* N�o usa
    function  LocalizaProtocolo(CNAB_Cfg, Entidade: Integer): Integer;
    *)
  public
    { Public declarations }
    procedure ReopenNIA(CliInt: Integer; Todos: Boolean);
    procedure ReopenBloOpcoes();
    procedure ReopenQuery(TipoProtocolo, Protocolo, ArreIts: Integer;
                QueryBloquetosFiltros: String);
    (*
    procedure ProtocoloAbertos(Quais: TSelType; Grade: TdmkDBGrid;
                ProgressBar: TProgressBar; Aba, Tarefa, EntCliInt: Integer;
                QueryBloquetosFiltros: String; var ProtoMail: String;
                var VeriEmeio: Boolean);
    *)
    procedure AtualizaSelecionadoArrPeri(BloArre: String; BloArreIts,
                Controle: Integer);
    procedure AtualizaTodosArrPeri(BloArre: String; BloArreIts: Integer;
                Todos: Boolean);
    procedure ReopenArre(Controle: Integer; BloArre: String);
    procedure ReopenArreIts(Controle: Integer);
    procedure ConfiguraArrecadacoes(CliInt: Integer; Progress: TProgressBar;
                BloArreAll: String);
    procedure ReopenArrePesq();
    procedure ReoepenCNAB_Cfg();
    function  ReopenBloArreItsVen(): Integer;
  end;

var
  DBloGeren: TDBloGeren;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses Module, UnBancos, ModuleGeral, UnGotoy, MyDBCheck, UnInternalConsts,
  UnBco_Rem, UnFinanceiro, UCreate, UMySQLModule, UnMyObjects, GetValor,
  BloGerenNew, UnBloquetos, UnBloqGerl;

{$R *.dfm}

{ TDBloGeren }

procedure TDBloGeren.AtualizaSelecionadoArrPeri(BloArre: String; BloArreIts,
  Controle: Integer);
var
  Sel: Integer;
begin
  if BloArreIts <> 0 then
  begin
    Sel := QrArreAdiciona.Value;
    if Sel = 0 then
      Sel := 1
    else
      Sel := 0;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, BloArre, False, ['Adiciona'],
      ['BloArreIts'], [Sel], [BloArreIts], False);
  end else
  if Controle <> 0 then
  begin
    Sel := QrArreItsAdiciona.Value;
    if Sel = 0 then
      Sel := 1
    else
      Sel := 0;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, BloArre, False, ['Adiciona'],
      ['Controle'], [Sel], [Controle], False);
  end;
end;

procedure TDBloGeren.AtualizaTodosArrPeri(BloArre: String; BloArreIts: Integer;
  Todos: Boolean);
var
  Sel: Integer;
begin
  Sel := Geral.BoolToInt(Todos);
  //
  if BloArreIts <> 0 then
  begin
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, BloArre, False, ['Adiciona'],
      ['BloArreIts'], [Sel], [BloArreIts], False);
    //
    ReopenArreIts(0);
  end else
  begin
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, BloArre, False, ['Adiciona'],
      [], [Sel], [], False);
    //
    ReopenArre(0, BloArre);
  end;
end;

procedure TDBloGeren.ConfiguraArrecadacoes(CliInt: Integer;
  Progress: TProgressBar; BloArreAll: String);
var
  MesI, MesF, MesDif, MesT, AnoT, a, b, c: Word;  Adiciona, Seq,
  ArreInt, Periodo, i, Controle: Integer;
  Texto, Titulo: String;
  Val1: Double;
  ResVar: Variant;
begin
  try
    Screen.Cursor := crHourGlass;
    Seq           := 0;
    Adiciona      := 0;
    MesF          := 0;
    MesI          := 0;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DELETE FROM ' + BloArreAll);
    DmodG.QrUpdPID1.ExecSQL;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + BloArreAll + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Conta=:P0, BloArre=:P1, BloArreIts=:P2, Valor=:P3, ');
    DModG.QrUpdPID1.SQL.Add('Texto=:P4, Adiciona=:P5, Seq=:P6, Entidade=:P7, ');
    DModG.QrUpdPID1.SQL.Add('NomeEnti=:P8, CartEmiss=:P9, CNAB_Cfg=:10, ');
    DModG.QrUpdPID1.SQL.Add('DiaVencto=:P11, Periodo=:P12, Controle=:P13');
    //
    DBloGeren.ReopenNIA(CliInt, True);
    //
    Progress.Position := 0;
    Progress.Max      := DBloGeren.QrNIA.RecordCount;
    // Cobran�as Sobre arrecada��es.
    while not DBloGeren.QrNIA.Eof do
    begin
      case DBloGeren.QrNIASitCobr.Value of
        2: //Cobran�a Programada
        begin
          MesI := DBloGeren.QrNIAParcPerI.Value;
          MesF := DBloGeren.QrNIAParcPerF.Value;
        end;
        3: //Contratos
        begin
          MesI := Geral.Periodo2000(DBloGeren.QrNIADVencimento.Value);
          MesF := Geral.Periodo2000(DBloGeren.QrNIADtaPrxRenw.Value);
        end;
      end;
      MesDif  := (MesF - MesI) + 1;
      Periodo := MesI;
      for i := 0 to MesDif - 1 do
      begin
        if UBloquetos.VerificaArrecadacao(DBloGeren.QrNIAEntidade.Value,
          DBloGeren.QrNIAConta.Value, DBloGeren.QrNIACodigo.Value,
          DBloGeren.QrNIAControle.Value, CliInt, Periodo) then
        begin
          if DBloGeren.QrNIAValorInf.Value = 1 then
          begin
            if DBloGeren.QrNIATexto.Value <> '' then
              Titulo := '(' + DBloGeren.QrNIATexto.Value + ')'
            else
              Titulo := '';
            if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
              DBloGeren.QrNIAValor.Value, 2, 0, '', '', True,
              DBloGeren.QrNIANome.Value, 'Informe o valor: ' + Titulo, 0, ResVar)
            then
              Val1 := ResVar
            else
              Val1 := 0;
          end else
            Val1 := DBloGeren.QrNIAValor.Value;
          //
          // Arredonda valor conforme configurado no ArreBaI
          if DBloGeren.QrNIAArredonda.Value > 0 then
          begin
            ArreInt := Trunc((Val1 + (DBloGeren.QrNIAArredonda.Value * 0.99)) /
              DBloGeren.QrNIAArredonda.Value);
            Val1 := ArreInt * DBloGeren.QrNIAArredonda.Value;
          end else Val1 := Trunc(Val1 * 100) / 100;
          //
          if Trim(DBloGeren.QrNIATexto.Value) <> '' then
            Texto := DBloGeren.QrNIATexto.Value
          else
            Texto := DBloGeren.QrNIANome.Value;
          if DBloGeren.QrNIAInfoParc.Value = 1 then
          begin
            a := 0;
            b := 0;
            c := 0;
            case DBloGeren.QrNIASitCobr.Value of
              1: // Cobran�a Cont�nua (Anual)
              begin
                dmkPF.PeriodoDecode(Periodo, AnoT, MesT);
                a := DBloGeren.QrNIAParcPerI.Value;
                c := DBloGeren.QrNIAParcPerF.Value;
                if c < a then Inc(c, 12);
                b := MesT;
                if b < a then Inc(b, 12);
              end;
              2: // Cobran�a Programada
              begin
                a := DBloGeren.QrNIAParcPerI.Value;
                b := Periodo;
                c := DBloGeren.QrNIAParcPerF.Value;
              end;
              3: // Contrato
              begin
                a := Geral.Periodo2000(DBloGeren.QrNIADVencimento.Value);
                b := Periodo;
                c := Geral.Periodo2000(DBloGeren.QrNIADtaPrxRenw.Value);
              end;
            end;
            Texto := FormatFloat('00', b-a+1) + '/' + FormatFloat('00', c-a+1) +
              ' - ' + Texto;
          end;
          Controle := UMyMod.BuscaNovoCodigo_Int(DModG.QrUpdPID2, BloArreAll,
                        'Controle', [], [], stIns, 0, siPositivo, nil);
          //
          DModG.QrUpdPID1.Params[00].AsInteger := DBloGeren.QrNIAConta.Value;
          DModG.QrUpdPID1.Params[01].AsInteger := DBloGeren.QrNIACodigo.Value;
          DModG.QrUpdPID1.Params[02].AsInteger := DBloGeren.QrNIAControle.Value;
          DModG.QrUpdPID1.Params[03].AsFloat   := Val1;
          DModG.QrUpdPID1.Params[04].AsString  := Texto;
          DModG.QrUpdPID1.Params[05].AsInteger := Adiciona;
          DModG.QrUpdPID1.Params[06].AsInteger := Seq;
          DModG.QrUpdPID1.Params[07].AsInteger := DBloGeren.QrNIAEntidade.Value;
          DModG.QrUpdPID1.Params[08].AsString  := DBloGeren.QrNIANOMEENT.Value;
          DModG.QrUpdPID1.Params[09].AsInteger := DBloGeren.QrNIACartEmiss.Value;
          DModG.QrUpdPID1.Params[10].AsInteger := DBloGeren.QrNIACNAB_Cfg.Value;
          DModG.QrUpdPID1.Params[11].AsInteger := Trunc(DBloGeren.QrNIADiaVencto.Value);
          DModG.QrUpdPID1.Params[12].AsInteger := Periodo;
          DModG.QrUpdPID1.Params[13].AsInteger := Controle;
          DmodG.QrUpdPID1.ExecSQL;
        end;
        Periodo := Periodo + 1;
      end;
      Progress.Position := Progress.Position + 1;
      Progress.Update;
      Application.ProcessMessages;
      //
      DBloGeren.QrNIA.Next;
    end;
  finally
    Progress.Position := 0;
    Screen.Cursor     := crDefault;
  end;
end;

(* N�o usa
function TDBloGeren.LocalizaProtocolo(CNAB_Cfg, Entidade: Integer): Integer;
begin
  Result := 0;
  //
  if (CNAB_Cfg <> 0) and (Entidade = 0) then
  begin
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('SELECT Protocolo');
    Dmod.QrUpd.SQL.Add('FROM proenpr');
    Dmod.QrUpd.SQL.Add('WHERE CNAB_Cfg=:P0');
    Dmod.QrUpd.Params[0].AsInteger := CNAB_Cfg;
    Dmod.QrUpd.Open;
    if Dmod.QrUpd.RecordCount > 0 then
      Result := Dmod.QrUpd.FieldByName('Protocolo').AsInteger;
  end else if (CNAB_Cfg = 0) and (Entidade <> 0) then
  begin
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('SELECT pro.Protocolo');
    Dmod.QrUpd.SQL.Add('FROM proenprit its');
    Dmod.QrUpd.SQL.Add('LEFT JOIN proenpr pro ON pro.Codigo = its.Codigo');
    Dmod.QrUpd.SQL.Add('WHERE Entidade=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Entidade;
    Dmod.QrUpd.Open;
    if Dmod.QrUpd.RecordCount > 0 then
      Result := Dmod.QrUpd.FieldByName('Protocolo').AsInteger;
  end else 
  begin
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('SELECT Protocolo');
    Dmod.QrUpd.SQL.Add('FROM proenpr');
    Dmod.QrUpd.SQL.Add('WHERE CNAB_Cfg=:P0');
    Dmod.QrUpd.Params[0].AsInteger := CNAB_Cfg;
    Dmod.QrUpd.Open;
    if Dmod.QrUpd.RecordCount > 0 then
      Result := Dmod.QrUpd.FieldByName('Protocolo').AsInteger
    else begin
      Dmod.QrUpd.Close;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('SELECT pro.Protocolo');
      Dmod.QrUpd.SQL.Add('FROM proenprit its');
      Dmod.QrUpd.SQL.Add('LEFT JOIN proenpr pro ON pro.Codigo = its.Codigo');
      Dmod.QrUpd.SQL.Add('WHERE Entidade=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Entidade;
      Dmod.QrUpd.Open;
      if Dmod.QrUpd.RecordCount > 0 then
        Result := Dmod.QrUpd.FieldByName('Protocolo').AsInteger;
    end;
  end;
  Dmod.QrUpd.Close;
end;
*)

(*
procedure TDBloGeren.ProtocoloAbertos(Quais: TSelType; Grade: TdmkDBGrid;
  ProgressBar: TProgressBar; Aba, Tarefa, EntCliInt: Integer;
  QueryBloquetosFiltros: String; var ProtoMail: String; var VeriEmeio: Boolean);
var
  Protocolo, Entidade, Periodo, PrevCod: Integer;
  Bloqueto, Valor: Double;
  Vencto: TDate;
  RecipEmeio: String;
  Inseriu, Desistiu: Boolean;
begin
  Inseriu := False;
  //
  case Aba of
    0:
    begin
      Protocolo  := QrCD1PROTOCOLO.Value;
      Entidade   := QrCD1Entidade.Value;
      Bloqueto   := QrCD1Boleto.Value;
      Vencto     := QrCD1Vencto.Value;
      Valor      := QrCD1Valor.Value;
      Periodo    := QrCD1Periodo.Value;
      PrevCod    := QrCD1PREVCOD.Value;
      RecipEmeio := '';
    end;
    1:
    begin
      Protocolo  := QrProtoMailProtocolo.Value;
      Entidade   := QrProtoMailEntid_Cod.Value;
      Bloqueto   := QrProtoMailBloqueto.Value;
      Vencto     := QrProtoMailVencimento.Value;
      Valor      := QrProtoMailValor.Value;
      Periodo    := QrProtoMailPeriodo.Value;
      PrevCod    := QrProtoMailPrevCod.Value;
      RecipEmeio := QrProtoMailRecipEmeio.Value;
    end;
    2:
    begin
      Protocolo  := QrCR1PROTOCOLO.Value;
      Entidade   := QrCR1Entidade.Value;
      Bloqueto   := QrCR1Boleto.Value;
      Vencto     := QrCR1Vencto.Value;
      Valor      := QrCR1Valor.Value;
      Periodo    := QrCR1Periodo.Value;
      PrevCod    := QrCR1PREVCOD.Value;
      RecipEmeio := '';
    end;
    else
    begin
      Protocolo  := 0;
      Entidade   := 0;
      Bloqueto   := 0;
      Vencto     := 0;
      Valor      := 0;
      Periodo    := 0;
      PrevCod    := 0;
      RecipEmeio := '';
    end;
  end;
  UBloquetos.MostraBloGerenProSel(Tarefa, EntCliInt, Protocolo, Entidade,
    Periodo, PrevCod, Aba, GetNomeTab(Aba), RecipEmeio, Bloqueto, Valor, Vencto,
    Quais, GetTabela(Aba), Grade, Inseriu, Desistiu);          
  //
  if Inseriu and not Desistiu then
  begin
    if GetNomeTab(Aba) = 'QrProtoMail' then
      VerificaEntregaPorEmail(QueryBloquetosFiltros, EntCliInt, ProgressBar,
        ProtoMail, VeriEmeio)
    else if GetNomeTab(Aba) <> '' then
      ReopenQuery(GetTabela(Aba), UnProtocolo.GetTipoProtocolo(Aba), Tarefa,
        EntCliInt, QueryBloquetosFiltros)
    else
      Geral.MB_Aviso('Fonte de dados n�o definida para reabertura!');
  end;
end;
*)

procedure TDBloGeren.QrArreAfterScroll(DataSet: TDataSet);
begin
  ReopenArreIts(0);
end;

procedure TDBloGeren.QrArreBeforeClose(DataSet: TDataSet);
begin
  QrArreIts.Close;
end;

procedure TDBloGeren.QrBloArreItsVenCalcFields(DataSet: TDataSet);
var
  Expirou: Boolean;
  Parcelas: Integer;
  ExpiraEm, Ini, Fim: String;
begin
  Parcelas := 0;
  Expirou  := UBloquetos.CalculaPeriodoIniFimArrecadacao(
    QrBloArreItsVenSitCobr.Value, QrBloArreItsVenParcPerI.Value,
    QrBloArreItsVenParcPerF.Value, QrBloArreItsVenParcelas.Value,
    QrBloArreItsVenDiaVcto_TXT.Value, QrBloArreItsVenDVencimento.Value,
    QrBloArreItsVenDtaPrxRenw.Value, Ini, Fim, ExpiraEm, Parcelas);
  //
  QrBloArreItsVenINICIO.Value    := Ini;
  QrBloArreItsVenFINAL.Value     := Fim;
  QrBloArreItsVenEXPIRAEM.Value  := ExpiraEm;
  QrBloArreItsVenEXPIROU.Value   := Expirou;
  QrBloArreItsVenParc_Calc.Value := Parcelas;
end;

procedure TDBloGeren.ReopenArrePesq();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrArrePesq, Dmod.MyDB, [
    'SELECT bar.Codigo, bar.Nome ',
    'FROM bloarre bar ',
    'LEFT JOIN bloarreits its  ON its.Codigo = bar.Codigo ',
    'WHERE bar.Ativo = 1 ',
    'AND its.Empresa = ' + Geral.FF0(VAR_ModBloq_EntCliInt),
    'GROUP BY bar.Codigo ',
    'ORDER BY bar.Nome ',
    '']);
end;

procedure TDBloGeren.ReoepenCNAB_Cfg;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCNABcfg, Dmod.MyDB, [
    'SELECT Codigo, Nome, Cedente ',
    'FROM cnab_cfg ',
    'WHERE Ativo = 1 ',
    'AND Cedente = ' + Geral.FF0(VAR_ModBloq_EntCliInt),
    'ORDER BY Nome ',
    '']);
end;

procedure TDBloGeren.ReopenArre(Controle: Integer; BloArre: String);
begin
  FBloArreAll := BloArre; 
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrArre, DModG.MyPID_DB, [
    'SELECT arr.Nome NomeArre, tmp.NomeEnti, ',
{$IfNDef sCNTR}
    'con.Nome NomeContrato, con.Codigo CodContrato, ',
{$Else}
    '"" NomeContrato, "" CodContrato, ',
{$EndIF}
    'tmp.BloArre, tmp.BloArreIts, tmp.Adiciona, tmp.Controle ',
    'FROM ' + BloArre + ' tmp ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.bloarre arr ON arr.Codigo = tmp.BloArre ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.bloarreits its ON its.Controle = tmp.BloArreIts ',
{$IfNDef sCNTR}
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.contratos con ON con.Codigo = its.Contrato ',
{$EndIF}
    'GROUP BY tmp.BloArreIts ',
    'ORDER BY NomeArre, NomeEnti ',
    '']);
  //
  if Controle <> 0 then
    QrArre.Locate('Controle', Controle, []);
end;

procedure TDBloGeren.ReopenArreIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrArreIts, DModG.MyPID_DB, [
    'SELECT tmp.Adiciona, tmp.Texto, tmp.Valor, tmp.Controle, tmp.CartEmiss, ',
    'tmp.Periodo, tmp.DiaVencto, cfg.JurosPerc, cfg.MultaPerc, tmp.Entidade, ',
    'tmp.Conta, tmp.BloArre, tmp.BloArreIts, tmp.CNAB_Cfg ',
    'FROM ' + FBloArreAll + ' tmp ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.bloarreits its ON its.Controle = tmp.BloArreIts ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.cnab_cfg cfg ON cfg.Codigo = its.Cnab_Cfg ',
    'WHERE tmp.BloArre=' + Geral.FF0(QrArreBloArre.Value),
    'AND tmp.BloArreIts=' + Geral.FF0(QrArreBloArreIts.Value),
    'ORDER BY tmp.Texto, tmp.Valor ',
    '']);
  if Controle <> 0 then
    QrArreIts.Locate('Controle', Controle, []);
end;

function TDBloGeren.ReopenBloArreItsVen(): Integer;
var
  Periodo: Integer;
  Hoje: TDateTime;
begin
  Hoje    := DModG.ObtemAgora;
  Periodo := Geral.Periodo2000(Hoje);
  Periodo := Periodo + 1; //M�s posterior
  //
(*
  QrBloArreItsVen.Close;
  QrBloArreItsVen.Database := Dmod.MyDB;
  QrBloArreItsVen.Params[0].AsInteger  := Periodo;
  QrBloArreItsVen.Params[1].AsDateTime := Hoje;
  QrBloArreItsVen. O p e n;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrBloArreItsVen, Dmod.MyDB, [
  'SELECT IF(its.InfoParc = 0, "N�o", "Sim") InfoParc_TXT, ',
  'are.Nome NOMEARRECAD, ',
  'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
  'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP, ',
  'cfg.Nome NOMECFG, "Programada" SitCobr_TXT, enb.Filial, ',
  'its.DiaVencto + 0.000 DiaVcto_TXT, its.Valor Valor_TXT, ',
  'its.DataCad DVencimento, its.DataCad DtaPrxRenw, its.* ',
  'FROM bloarreits its ',
  'LEFT JOIN bloarre are ON are.Codigo = its.Codigo ',
  'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade ',
  'LEFT JOIN entidades enb ON enb.Codigo = its.Empresa ',
  'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = its.CNAB_Cfg ',
  'WHERE its.Contrato=0 ',
  'AND its.Ativo=1 ',
  'AND its.ParcPerF<=' + Geral.FF0(Periodo),
  'AND its.SitCobr=2 ',
  ' ',
  'UNION ',
  ' ',
  'SELECT IF(its.InfoParc = 0, "N�o", "Sim") InfoParc_TXT, ',
  'are.Nome NOMEARRECAD, ',
  'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
  'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP, ',
  'cfg.Nome NOMECFG, "Contrato" SitCobr_TXT, enb.Filial, ',
{$IfNDef sCNTR}
  'con.ddMesVcto + 0.000 DiaVcto_TXT, con.ValorMes Valor_TXT, ',
  'con.DVencimento, con.DtaPrxRenw, ',
{$Else}
  '0.000 DiaVcto_TXT ,0.000 Valor_TXT, ',
  'DATE("1899-12-30") DVencimento, DATE("1899-12-30") DtaPrxRenw,  ',
{$EndIF}
  'its.* ',
  'FROM bloarreits its ',
  'LEFT JOIN bloarre are ON are.Codigo = its.Codigo ',
  'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade ',
  'LEFT JOIN entidades enb ON enb.Codigo = its.Empresa ',
  'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = its.CNAB_Cfg ',
{$IfNDef sCNTR}
  'LEFT JOIN contratos con ON con.Codigo = its.Contrato',
{$EndIF}
  'WHERE its.Contrato<>0 ',
  'AND its.Ativo=1 ',
{$IfNDef sCNTR}
  'AND con.Ativo=1 ',
  'AND con.DtaPrxRenw<="' + Geral.FDT(Hoje, 1) + '"',
{$EndIF}
  'AND its.SitCobr=3 ',
  '']);
  //
  Result := QrBloArreItsVen.RecordCount;
end;

procedure TDBloGeren.ReopenBloOpcoes;
begin
  UMyMod.AbreQuery(QrBloOpcoes, Dmod.MyDB);
end;

procedure TDBloGeren.ReopenNIA(CLiInt: Integer; Todos: Boolean);
var
  SQL: String;
begin
  if Todos then
    SQL := 'AND its.SitCobr IN (2, 3)'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNIA, Dmod.MyDB, [
    'SELECT arr.Codigo, arr.Nome, arr.Conta, ',
{$IfNDef sCNTR}
    'IF(its.Contrato<>0, con.ValorMes, its.Valor) Valor, ',
    'IF(its.Contrato<>0, con.ddMesVcto, its.DiaVencto) + 0.000 DiaVencto, ',
{$else}
    '(its.Valor) Valor, ',
    '(its.DiaVencto) + 0.000 DiaVencto, ',
{$EndIf}
    'its.SitCobr, its.Parcelas, its.ParcPerI, its.ParcPerF, ',
    'its.InfoParc, its.Texto, its.Controle, its.Arredonda, its.Entidade, ',
    'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
    'its.CNAB_Cfg, cna.CartEmiss, its.ValorInf, ',
{$IfNDef v}
    'con.DVencimento, con.DtaPrxRenw ',
{$else}
    'DATE("1899-12-30") DVencimento, DATE("1899-12-30") DtaPrxRenw ',
{$EndIf}
    'FROM bloarre arr ',
    'LEFT JOIN bloarreits its ON its.Codigo = arr.Codigo ',
    'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade ',
    'LEFT JOIN cnab_cfg cna ON cna.Codigo = its.CNAB_Cfg ',
{$IfNDef sCNTR}
    'LEFT JOIN contratos con ON con.Codigo = its.Contrato ',
{$EndIf}
    'WHERE arr.Codigo > 0 ',
    'AND its.Ativo = 1 ',
    'AND its.Empresa=' + Geral.FF0(CliInt),
    SQL,
    '']);
end;

procedure TDBloGeren.ReopenQuery(TipoProtocolo,
  Protocolo, ArreIts: Integer; QueryBloquetosFiltros: String);
var
  TipoEmail: Integer;
  CampoProto, CampoProtoPak, SQLCompl: String;
begin
  if TipoProtocolo = 4 then //CR
  begin
    CampoProto      := 'ProtocoloCR';
    CampoProtoPak   := 'ProtocoPakCR';
    SQLCompl        := 'AND ari.ProtocoloCR <> 0';
  end else
  begin
    CampoProto      := 'Protocolo';
    CampoProtoPak   := 'ProtocoPak';
    SQLCompl        := '';
  end;
  if TipoProtocolo = 2 then //E-mail
  begin
    DBloGeren.ReopenBloOpcoes;
    //
    TipoEmail := QrBloOpcoes.FieldByName('EntiTipCto').AsInteger;
    //
    if MyObjects.FIC(TipoEmail = 0, nil, 'Tipo de email para envio de boleto n�o definido nas op��es de bloquetos!') then
    begin
      QrProtocoBol.Close;
      Exit;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrProtocoBol, Dmod.MyDB, [
      'SELECT DISTINCT ari.Boleto, ari.Entidade, ari.Vencto, ',
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
      'ELSE ent.Nome END NOMEENT, ari.'+ CampoProto +' PROTOCOD, ',
      'ari.'+ CampoProtoPak +' ProtocoPak, ptc.Tipo TipoProt,  ',
      'CONCAT_WS("-", ari.Boleto, ari.Entidade) BOLENT, ',
      'SUM(ari.Valor) Valor, cna.Nome NOMECNAB_Cfg, ari.CNAB_Cfg, ',
      'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, enm.Conta + 0.000 EMeio_ID, enm.Email, ',
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ',
      'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
      'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
      'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
      'IF(ppi.DataE <= "1899-12-30", "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
      'IF(ppi.DataD <= "1899-12-30", "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y %H:%i")) DataD_Txt  ',
      'FROM entimail enm ',
      'LEFT JOIN enticontat eco ON eco.Controle = enm.Controle ',
      'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
      'LEFT JOIN entidades ent ON ent.Codigo = eco.Codigo ',
      'LEFT JOIN arreits ari ON ari.Entidade = ent.Codigo ',
      'LEFT JOIN prev pre ON pre.Codigo=ari.Codigo ',
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ari.' + CampoProto,
      'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
      'LEFT JOIN protpakits ppi ON (ppi.Docum=ari.Boleto AND ppi.Cliente=ari.Entidade AND ppi.ID_Cod3 = enm.Conta) ',
      'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
      QueryBloquetosFiltros,
      'AND enm.EntiTipCto=' + Geral.FF0(TipoEmail),
      'AND ari.Boleto > 0 ',
      'AND ptc.Codigo=' + Geral.FF0(Protocolo),
      'GROUP BY enm.Email, ari.Boleto, ari.Entidade, ari.Vencto ',
      'ORDER BY NOMEENT, ari.Boleto, enm.Email ',
      '']);
    if ArreIts <> 0 then
      QrProtocoBol.Locate('ArreIts', ArreIts, []);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrProtocoBol, Dmod.MyDB, [
      'SELECT DISTINCT ari.Boleto, ari.Entidade, ari.Vencto, ',
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
      'ELSE ent.Nome END NOMEENT, ari.' + CampoProto + ' PROTOCOD,  ',
      'ari.'+ CampoProtoPak +' ProtocoPak, ptc.Tipo TipoProt,  ',
      'CONCAT_WS("-", ari.Boleto, ari.Entidade) BOLENT, ',
      'SUM(ari.Valor) Valor, cna.Nome NOMECNAB_Cfg, ari.CNAB_Cfg, ',
      'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, 0 + 0.000 EMeio_ID, "" Email, ',
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ',
      'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
      'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
      'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
      'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
      'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DataD_Txt  ',
      'FROM arreits ari ',
      'LEFT JOIN prev pre ON pre.Codigo=ari.Codigo ',
      'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade ',
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ari.' + CampoProto,
      'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
      'LEFT JOIN protpakits ppi ON (ppi.Docum=ari.Boleto AND ppi.Cliente=ari.Entidade AND ppi.Controle=ari.' + CampoProtoPak + ') ',
      'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
      QueryBloquetosFiltros,
      'AND ari.Boleto > 0 ',
      'AND ptc.Codigo=' + Geral.FF0(Protocolo),
      SQLCompl,
      'GROUP BY ari.Boleto, ari.Entidade, ari.Vencto ',
      'ORDER BY NOMEENT, Boleto ',
      '']);
    if ArreIts <> 0 then
      QrProtocoBol.Locate('ArreIts', ArreIts, []);
  end;
end;

end.
