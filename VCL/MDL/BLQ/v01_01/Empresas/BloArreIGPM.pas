unit BloArreIGPM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkEdit,
  DB, mySQLDbTables, dmkGeral, dmkImage, Math, ComCtrls, dmkEditDateTimePicker,
  dmkPermissoes, UnDmkEnums, DmkDAC_PF, UnDmkProcFunc;

type
  TFmBloArreIGPM = class(TForm)
    Panel1: TPanel;
    QrIGPM: TmySQLQuery;
    QrIGPMAnoMes: TIntegerField;
    QrIGPMIGPM_D: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtIGPM: TBitBtn;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBDados: TGroupBox;
    GBIni: TGroupBox;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBMesI: TComboBox;
    CBAnoI: TComboBox;
    GBFim: TGroupBox;
    Label34: TLabel;
    LaAnoF: TLabel;
    CBMesF: TComboBox;
    CBAnoF: TComboBox;
    Label17: TLabel;
    EdValor: TdmkEdit;
    EdParcelas: TdmkEdit;
    Label7: TLabel;
    GBDadosNew: TGroupBox;
    Label1: TLabel;
    EdValorNew: TdmkEdit;
    Label3: TLabel;
    TPDataINew: TdmkEditDateTimePicker;
    TPDataFNew: TdmkEditDateTimePicker;
    Label4: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    BtAtualiza: TBitBtn;
    QrIGPMAnt: TmySQLQuery;
    QrIGPMAtu: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CBMesIChange(Sender: TObject);
    procedure CBAnoIChange(Sender: TObject);
    procedure CBMesFChange(Sender: TObject);
    procedure CBAnoFChange(Sender: TObject);
    procedure BtIGPMClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtAtualizaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ConfiguraJanela();
    function ObtemIndice(Periodo: Integer): Double;
  public
    { Public declarations }
    FMes: Word;
    FIDNum, FSitCobr, FPeriodoI, FPeriodoF: Integer;
    FValor: Double;
  end;

  var
  FmBloArreIGPM: TFmBloArreIGPM;

implementation

uses UnMsgInt, IGPMCad, MyDBCheck, UnMyObjects, Module, UnBloquetos,
  UMySQLModule;

{$R *.DFM}

procedure TFmBloArreIGPM.BtAtualizaClick(Sender: TObject);
var
  DataI, DataF: TDate;
  PerIAnt, PerIAtu, PerF, PeriodoINew, PeriodoFNew: Integer;
  IndIAnt, IndIAtu, IndF, IGPMAnt, IGPMAtu, PerMes, VarMes, VarTot, PerTot,
  ValReaj, Valor: Double;
begin
  DataI   := EncodeDate(Geral.IMV(CBAnoI.Text), CBMesI.ItemIndex + 1, 1);
  DataF   := EncodeDate(Geral.IMV(CBAnoF.Text), CBMesF.ItemIndex + 1, 1);
  PerIAnt := Geral.Periodo2000(DataI) - 1;
  PerIAtu := Geral.Periodo2000(DataI);
  PerF    := Geral.Periodo2000(DataF);
  IndIAnt := ObtemIndice(PerIAnt);
  IndIAtu := ObtemIndice(PerIAtu);
  IndF    := ObtemIndice(PerF);
  PerMes  := 0;
  VarMes  := 0;
  VarTot  := 0;
  PerTot  := 0;
  ValReaj := 0;
  Valor   := EdValor.ValueVariant;
  //
  if MyObjects.FIC(IndIAnt = 0, nil, '�ndice n�o cadastrado para o per�odo inicial informado!') then Exit;
  if MyObjects.FIC(IndIAtu = 0, nil, '�ndice n�o cadastrado para o per�odo inicial informado!') then Exit;
  if MyObjects.FIC(IndF = 0, nil, '�ndice n�o cadastrado para o per�odo final informado!') then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrIGPMAnt, Dmod.MyDB, [
    'SELECT AnoMes, IGPM_D ',
    'FROM igpm ',
    'WHERE AnoMes BETWEEN "' + Geral.FF0(PerIAnt) + '" AND "' + Geral.FF0(PerF) + '" ',
    '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrIGPMAtu, Dmod.MyDB, [
    'SELECT AnoMes, IGPM_D ',
    'FROM igpm ',
    'WHERE AnoMes BETWEEN "' + Geral.FF0(PerIAtu) + '" AND "' + Geral.FF0(PerF) + '" ',
    '']);
  //
  if (QrIGPMAnt.RecordCount > 0) and (QrIGPMAtu.RecordCount > 0) then
  begin
    QrIGPMAnt.First;
    QrIGPMAtu.First;
    //
    while not QrIGPMAtu.Eof do
    begin
      IGPMAnt := QrIGPMAnt.FieldByName('IGPM_D').AsFloat;
      IGPMAtu := QrIGPMAtu.FieldByName('IGPM_D').AsFloat;
      //
      PerMes := IGPMAtu / IGPMAnt;
      PerMes := (PerMes - 1) * 100;
      PerMes := RoundTo(PerMes, -2);
      //
      VarMes := (PerMes / 100) + 1;
      //
      if VarTot = 0 then
        VarTot := VarMes
      else
        VarTot := VarTot * VarMes;
      //
      QrIGPMAnt.Next;
      QrIGPMAtu.Next;
    end;
    VarTot := RoundTo(VarTot, -7);
    PerTot := (VarTot -1) * 100;
    PerTot := RoundTo(PerTot, -4);
    //
    ValReaj := (Valor * PerTot) / 100;
    ValReaj := RoundTo(ValReaj, -2);
    //
    Valor := Valor + ValReaj;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      '�ndice de corre��o no per�odo = ' +  FloatToStr(VarTot) +
      ' Valor percentual correspondente = ' + FloatToStr(PerTot) + ' Valor do reajuste = ' +
      FloatToStr(ValReaj));
    //
    PeriodoINew             := PerF + 1;
    PeriodoFNew             := PerF + EdParcelas.ValueVariant;
    GBDadosNew.Visible      := True;
    EdValorNew.ValueVariant := Valor;
    TPDataINew.Date         := Geral.PeriodoToDate(PeriodoINew, 1, True);
    TPDataFNew.Date         := Geral.PeriodoToDate(PeriodoFNew, 1, True);
    BtOK.Enabled            := True;
  end else
    Geral.MB_Aviso('Nenhum �ndice foi localizado para o per�odo informado!');
end;

procedure TFmBloArreIGPM.BtIGPMClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmIGPMCad, FmIGPMCad, afmoNegarComAviso) then
  begin
    FmIGPMCad.ShowModal;
    FmIGPMCad.Destroy;
  end;
end;

procedure TFmBloArreIGPM.BtOKClick(Sender: TObject);
var
  Valor: Double;
  Parcelas, PeriodoI, PeriodoF: Integer;
  DataI, DataF: String;
begin
  Valor := EdValorNew.ValueVariant;
  //
  if not FSitCobr in [2,3] then //Per�odo e Contrato
  begin
    Geral.MB_Aviso('Situa��o de conbran�a n�o implementada!');
    Exit;
  end;
  if FIDNum = 0 then
  begin
    Geral.MB_Aviso('IDNum n�o definido!');
    Exit;
  end;
  if MyObjects.FIC(Valor = 0, EdValorNew, 'Valor n�o definido!') then Exit;
  if MyObjects.FIC(TPDataINew.Date = 0, TPDataINew, 'Per�odo inicial n�o definido!') then Exit;
  if MyObjects.FIC(TPDataFNew.Date = 0, TPDataFNew, 'Per�odo final n�o definido!') then Exit;
  //
  if FSitCobr = 2 then //Per�odo
  begin
    PeriodoI := Geral.Periodo2000(TPDataINew.Date);
    PeriodoF := Geral.Periodo2000(TPDataFNew.Date);
    Parcelas := PeriodoF - PeriodoI;    
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'bloarreits', False,
      ['Valor', 'ParcPerI', 'ParcPerF', 'Parcelas'], ['Controle'],
      [Valor, PeriodoI, PeriodoF, Parcelas], [FIDNum], True);
  end else
  if FSitCobr = 3 then //Contrato
  begin
    DataI := Geral.FDT(TPDataINew.Date, 1);
    DataF := Geral.FDT(TPDataFNew.Date, 1);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'contratos', False,
      ['ValorMes', 'DVencimento', 'DtaPrxRenw'], ['Codigo'],
      [Valor, DataI, DataF], [FIDNum], True);
  end;
  Close;
end;

procedure TFmBloArreIGPM.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloArreIGPM.CBAnoFChange(Sender: TObject);
begin
  EdParcelas.ValueVariant := UBloquetos.VerificaParcelas(2, CBMesI.ItemIndex,
    CBMesF.ItemIndex, CBAnoI.Text, CBAnoF.Text, True);
end;

procedure TFmBloArreIGPM.CBAnoIChange(Sender: TObject);
begin
  EdParcelas.ValueVariant := UBloquetos.VerificaParcelas(2, CBMesI.ItemIndex,
    CBMesF.ItemIndex, CBAnoI.Text, CBAnoF.Text, True);
end;

procedure TFmBloArreIGPM.CBMesFChange(Sender: TObject);
begin
  EdParcelas.ValueVariant := UBloquetos.VerificaParcelas(2, CBMesI.ItemIndex,
    CBMesF.ItemIndex, CBAnoI.Text, CBAnoF.Text, True);
end;

procedure TFmBloArreIGPM.CBMesIChange(Sender: TObject);
begin
  EdParcelas.ValueVariant := UBloquetos.VerificaParcelas(2, CBMesI.ItemIndex,
    CBMesF.ItemIndex, CBAnoI.Text, CBAnoF.Text, True);
end;

procedure TFmBloArreIGPM.ConfiguraJanela;
var
  DataI, DataF: TDate;
  AnoI, MesI, DiaI, AnoF, MesF, DiaF: Word;
begin
  //Painel de dados para o reajuste
  DataI := Geral.PeriodoToDate((FPeriodoI), 1, True);
  DataF := Geral.PeriodoToDate((FPeriodoF), 1, True);
  //
  DecodeDate(DataI, AnoI, MesI, DiaI);
  DecodeDate(DataF, AnoF, MesF, DiaF);
  //
  CBMesI.ItemIndex := (MesI - 1);
  CBAnoI.ItemIndex := (AnoI - 2000);
  //
  CBMesF.ItemIndex := (MesF - 1);
  CBAnoF.ItemIndex := (AnoF - 2000);
  //
  EdParcelas.ValueVariant := UBloquetos.VerificaParcelas(2, CBMesI.ItemIndex,
    CBMesF.ItemIndex, CBAnoI.Text, CBAnoF.Text, True);
  //
  EdValor.ValueVariant := FValor;
  //
  GBDadosNew.Visible := False;
  BtOK.Enabled       := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmBloArreIGPM.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmBloArreIGPM.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.PreencheCBAnoECBMes(CBAnoI, CBMesI, 0);
  MyObjects.PreencheCBAnoECBMes(CBAnoF, CBMesF, 0);
end;

procedure TFmBloArreIGPM.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloArreIGPM.FormShow(Sender: TObject);
begin
  ConfiguraJanela;
end;

function TFmBloArreIGPM.ObtemIndice(Periodo: Integer): Double;
begin
  QrIGPM.Close;
  QrIGPM.Params[0].AsInteger := Periodo;
  QrIGPM.Open;
  if QrIGPM.RecordCount > 0 then
    Result := QrIGPMIGPM_D.Value
  else
    Result := 0;
end;

end.
