unit BloAvulso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, dmkPermissoes, UnDmkEnums;

type
  TFmBloAvulso = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrCNAB_cfg: TmySQLQuery;
    QrCNAB_cfgCodigo: TIntegerField;
    QrCNAB_cfgNome: TWideStringField;
    QrCNAB_cfgCartEmiss: TIntegerField;
    QrCNAB_cfgTexto01: TWideStringField;
    QrCNAB_cfgTexto02: TWideStringField;
    QrCNAB_cfgTexto03: TWideStringField;
    QrCNAB_cfgTexto04: TWideStringField;
    QrCNAB_cfgTexto05: TWideStringField;
    QrCNAB_cfgTexto06: TWideStringField;
    QrCNAB_cfgTexto07: TWideStringField;
    QrCNAB_cfgTexto08: TWideStringField;
    QrCNAB_cfgTexto09: TWideStringField;
    QrCNAB_cfgTexto10: TWideStringField;
    DsCNAB_cfg: TDataSource;
    Label37: TLabel;
    EdCNAB_cfg: TdmkEditCB;
    CBCNAB_cfg: TdmkDBLookupComboBox;
    SpeedButton2: TSpeedButton;
    QrCNAB_cfgCedente: TIntegerField;
    QrCNAB_cfgSacadAvali: TIntegerField;
    QrCNAB_cfgCedBanco: TIntegerField;
    QrCNAB_cfgCedAgencia: TIntegerField;
    QrCNAB_cfgCedConta: TWideStringField;
    QrCNAB_cfgCedDAC_A: TWideStringField;
    QrCNAB_cfgCedDAC_C: TWideStringField;
    QrCNAB_cfgCedDAC_AC: TWideStringField;
    QrCNAB_cfgCedNome: TWideStringField;
    QrCNAB_cfgCedPosto: TIntegerField;
    QrCNAB_cfgSacAvaNome: TWideStringField;
    QrCNAB_cfgTipoCart: TSmallintField;
    QrCNAB_cfgCartNum: TWideStringField;
    QrCNAB_cfgCartCod: TWideStringField;
    QrCNAB_cfgEspecieTit: TWideStringField;
    QrCNAB_cfgAceiteTit: TSmallintField;
    QrCNAB_cfgInstrCobr1: TWideStringField;
    QrCNAB_cfgInstrCobr2: TWideStringField;
    QrCNAB_cfgInstrDias: TSmallintField;
    QrCNAB_cfgCodEmprBco: TWideStringField;
    QrCNAB_cfgJurosTipo: TSmallintField;
    QrCNAB_cfgJurosPerc: TFloatField;
    QrCNAB_cfgJurosDias: TSmallintField;
    QrCNAB_cfgMultaTipo: TSmallintField;
    QrCNAB_cfgMultaPerc: TFloatField;
    QrCNAB_cfgCNAB: TIntegerField;
    QrCNAB_cfgEnvEmeio: TSmallintField;
    QrCNAB_cfgDiretorio: TWideStringField;
    QrCNAB_cfgQuemPrint: TWideStringField;
    QrCNAB_cfg_237Mens1: TWideStringField;
    QrCNAB_cfg_237Mens2: TWideStringField;
    QrCNAB_cfgCodOculto: TWideStringField;
    QrCNAB_cfgSeqArq: TIntegerField;
    QrCNAB_cfgLastNosNum: TLargeintField;
    QrCNAB_cfgLocalPag: TWideStringField;
    QrCNAB_cfgEspecieVal: TWideStringField;
    QrCNAB_cfgOperCodi: TWideStringField;
    QrCNAB_cfgIDCobranca: TWideStringField;
    QrCNAB_cfgAgContaCed: TWideStringField;
    QrCNAB_cfgDirRetorno: TWideStringField;
    QrCNAB_cfgLk: TIntegerField;
    QrCNAB_cfgDataCad: TDateField;
    QrCNAB_cfgDataAlt: TDateField;
    QrCNAB_cfgUserCad: TIntegerField;
    QrCNAB_cfgUserAlt: TIntegerField;
    QrCNAB_cfgAlterWeb: TSmallintField;
    QrCNAB_cfgAtivo: TSmallintField;
    QrCNAB_cfgEspecieDoc: TWideStringField;
    QrCNAB_cfgMultaDias: TSmallintField;
    QrCNAB_cfgCartRetorno: TIntegerField;
    QrCNAB_cfgPosicoesBB: TSmallintField;
    QrCNAB_cfgIndicatBB: TWideStringField;
    QrCNAB_cfgTipoCobrBB: TWideStringField;
    QrCNAB_cfgVariacao: TIntegerField;
    QrCNAB_cfgComando: TIntegerField;
    QrCNAB_cfgDdProtesBB: TIntegerField;
    QrCNAB_cfgMsg40posBB: TWideStringField;
    QrCNAB_cfgQuemDistrb: TWideStringField;
    QrCNAB_cfgDesco1Cod: TSmallintField;
    QrCNAB_cfgDesco1Dds: TIntegerField;
    QrCNAB_cfgDesco1Fat: TFloatField;
    QrCNAB_cfgDesco2Cod: TSmallintField;
    QrCNAB_cfgDesco2Dds: TIntegerField;
    QrCNAB_cfgDesco2Fat: TFloatField;
    QrCNAB_cfgDesco3Cod: TSmallintField;
    QrCNAB_cfgDesco3Dds: TIntegerField;
    QrCNAB_cfgDesco3Fat: TFloatField;
    QrCNAB_cfgProtesCod: TSmallintField;
    QrCNAB_cfgProtesDds: TSmallintField;
    QrCNAB_cfgBxaDevCod: TSmallintField;
    QrCNAB_cfgBxaDevDds: TIntegerField;
    QrCNAB_cfgMoedaCod: TWideStringField;
    QrCNAB_cfgContrato: TFloatField;
    QrCNAB_cfgConvCartCobr: TWideStringField;
    QrCNAB_cfgConvVariCart: TWideStringField;
    QrCNAB_cfgInfNossoNum: TSmallintField;
    QrCNAB_cfgCodLidrBco: TWideStringField;
    QrCNAB_cfgTipoCobranca: TIntegerField;
    QrCNAB_cfgEspecieTxt: TWideStringField;
    QrCNAB_cfgCartTxt: TWideStringField;
    QrCNAB_cfgLayoutRem: TWideStringField;
    QrCNAB_cfgLayoutRet: TWideStringField;
    QrCNAB_cfgConcatCod: TWideStringField;
    QrCNAB_cfgComplmCod: TWideStringField;
    QrCNAB_cfgNaoRecebDd: TSmallintField;
    QrCNAB_cfgTipBloqUso: TWideStringField;
    QrCNAB_cfgNumVersaoI3: TIntegerField;
    QrCNAB_cfgModalCobr: TSmallintField;
    QrCNAB_cfgCtaCooper: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCNAB_Cfg, FCarteira: Integer;
  end;

  var
  FmBloAvulso: TFmBloAvulso;

implementation

uses UnMyObjects, Module, CNAB_Cfg, MyDBCheck, UMySQLModule, ModuleBloGeren;

{$R *.DFM}

procedure TFmBloAvulso.BtOKClick(Sender: TObject);
var
  CNAB_Cfg: Integer;
begin
  CNAB_Cfg := EdCNAB_cfg.ValueVariant;
  //
  if MyObjects.FIC(CNAB_Cfg = 0, EdCNAB_cfg, 'Defina a configuração do boleto!') then Exit;
  //
  FCNAB_Cfg := CNAB_Cfg;
  //
  Close;
end;

procedure TFmBloAvulso.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloAvulso.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBloAvulso.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBCNAB_cfg.ListSource := DsCNAB_cfg;
end;

procedure TFmBloAvulso.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloAvulso.FormShow(Sender: TObject);
begin
  QrCNAB_cfg.Close;
  QrCNAB_cfg.Params[0].AsInteger := FCarteira;
  UMyMod.AbreQuery(QrCNAB_cfg, Dmod.MyDB);
end;

procedure TFmBloAvulso.SpeedButton2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
    //
    QrCNAB_cfg.Close;
    QrCNAB_cfg.Open;
    //
    EdCNAB_cfg.ValueVariant := VAR_CADASTRO;
    CBCNAB_cfg.KeyValue     := VAR_CADASTRO;
    CBCNAB_cfg.SetFocus;
  end;
end;

end.
