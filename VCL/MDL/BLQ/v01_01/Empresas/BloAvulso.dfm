object FmBloAvulso: TFmBloAvulso
  Left = 339
  Top = 185
  Caption = 'BLO-GEREN-008 :: Boleto Avulso'
  ClientHeight = 225
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 554
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 506
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 458
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 165
        Height = 32
        Caption = 'Boleto Avulso'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 165
        Height = 32
        Caption = 'Boleto Avulso'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 165
        Height = 32
        Caption = 'Boleto Avulso'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 554
    Height = 63
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 554
      Height = 63
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 554
        Height = 63
        Align = alClient
        TabOrder = 0
        object Label37: TLabel
          Left = 12
          Top = 8
          Width = 113
          Height = 13
          Caption = 'Configura'#231#227'o do boleto:'
        end
        object SpeedButton2: TSpeedButton
          Left = 520
          Top = 25
          Width = 21
          Height = 21
          Hint = 'Inclui item de carteira'
          Caption = '...'
          OnClick = SpeedButton2Click
        end
        object EdCNAB_cfg: TdmkEditCB
          Left = 12
          Top = 25
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCNAB_cfg
          IgnoraDBLookupComboBox = False
        end
        object CBCNAB_cfg: TdmkDBLookupComboBox
          Left = 68
          Top = 25
          Width = 450
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCNAB_cfg
          TabOrder = 1
          dmkEditCB = EdCNAB_cfg
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 111
    Width = 554
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 550
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 155
    Width = 554
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 408
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 406
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrCNAB_cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM cnab_cfg'
      'WHERE Ativo = 1'
      'AND CartEmiss=:P0'
      'ORDER BY Nome')
    Left = 336
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_cfgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_cfg.Codigo'
    end
    object QrCNAB_cfgNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrCNAB_cfgCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrCNAB_cfgTexto01: TWideStringField
      FieldName = 'Texto01'
      Size = 100
    end
    object QrCNAB_cfgTexto02: TWideStringField
      FieldName = 'Texto02'
      Size = 100
    end
    object QrCNAB_cfgTexto03: TWideStringField
      FieldName = 'Texto03'
      Size = 100
    end
    object QrCNAB_cfgTexto04: TWideStringField
      FieldName = 'Texto04'
      Size = 100
    end
    object QrCNAB_cfgTexto05: TWideStringField
      FieldName = 'Texto05'
      Size = 100
    end
    object QrCNAB_cfgTexto06: TWideStringField
      FieldName = 'Texto06'
      Size = 100
    end
    object QrCNAB_cfgTexto07: TWideStringField
      FieldName = 'Texto07'
      Size = 100
    end
    object QrCNAB_cfgTexto08: TWideStringField
      FieldName = 'Texto08'
      Size = 100
    end
    object QrCNAB_cfgTexto09: TWideStringField
      FieldName = 'Texto09'
      Size = 100
    end
    object QrCNAB_cfgTexto10: TWideStringField
      FieldName = 'Texto10'
      Size = 100
    end
    object QrCNAB_cfgCedente: TIntegerField
      FieldName = 'Cedente'
    end
    object QrCNAB_cfgSacadAvali: TIntegerField
      FieldName = 'SacadAvali'
    end
    object QrCNAB_cfgCedBanco: TIntegerField
      FieldName = 'CedBanco'
    end
    object QrCNAB_cfgCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
    end
    object QrCNAB_cfgCedConta: TWideStringField
      FieldName = 'CedConta'
      Size = 30
    end
    object QrCNAB_cfgCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Size = 1
    end
    object QrCNAB_cfgCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Size = 1
    end
    object QrCNAB_cfgCedDAC_AC: TWideStringField
      FieldName = 'CedDAC_AC'
      Size = 1
    end
    object QrCNAB_cfgCedNome: TWideStringField
      FieldName = 'CedNome'
      Size = 100
    end
    object QrCNAB_cfgCedPosto: TIntegerField
      FieldName = 'CedPosto'
    end
    object QrCNAB_cfgSacAvaNome: TWideStringField
      FieldName = 'SacAvaNome'
      Size = 100
    end
    object QrCNAB_cfgTipoCart: TSmallintField
      FieldName = 'TipoCart'
    end
    object QrCNAB_cfgCartNum: TWideStringField
      FieldName = 'CartNum'
      Size = 3
    end
    object QrCNAB_cfgCartCod: TWideStringField
      FieldName = 'CartCod'
      Size = 3
    end
    object QrCNAB_cfgEspecieTit: TWideStringField
      FieldName = 'EspecieTit'
      Size = 6
    end
    object QrCNAB_cfgAceiteTit: TSmallintField
      FieldName = 'AceiteTit'
    end
    object QrCNAB_cfgInstrCobr1: TWideStringField
      FieldName = 'InstrCobr1'
      Size = 2
    end
    object QrCNAB_cfgInstrCobr2: TWideStringField
      FieldName = 'InstrCobr2'
      Size = 2
    end
    object QrCNAB_cfgInstrDias: TSmallintField
      FieldName = 'InstrDias'
    end
    object QrCNAB_cfgCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
    end
    object QrCNAB_cfgJurosTipo: TSmallintField
      FieldName = 'JurosTipo'
    end
    object QrCNAB_cfgJurosPerc: TFloatField
      FieldName = 'JurosPerc'
    end
    object QrCNAB_cfgJurosDias: TSmallintField
      FieldName = 'JurosDias'
    end
    object QrCNAB_cfgMultaTipo: TSmallintField
      FieldName = 'MultaTipo'
    end
    object QrCNAB_cfgMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrCNAB_cfgCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrCNAB_cfgEnvEmeio: TSmallintField
      FieldName = 'EnvEmeio'
    end
    object QrCNAB_cfgDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Size = 255
    end
    object QrCNAB_cfgQuemPrint: TWideStringField
      FieldName = 'QuemPrint'
      Size = 1
    end
    object QrCNAB_cfg_237Mens1: TWideStringField
      FieldName = '_237Mens1'
      Size = 12
    end
    object QrCNAB_cfg_237Mens2: TWideStringField
      FieldName = '_237Mens2'
      Size = 60
    end
    object QrCNAB_cfgCodOculto: TWideStringField
      FieldName = 'CodOculto'
    end
    object QrCNAB_cfgSeqArq: TIntegerField
      FieldName = 'SeqArq'
    end
    object QrCNAB_cfgLastNosNum: TLargeintField
      FieldName = 'LastNosNum'
    end
    object QrCNAB_cfgLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Size = 127
    end
    object QrCNAB_cfgEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Size = 5
    end
    object QrCNAB_cfgOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Size = 3
    end
    object QrCNAB_cfgIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Size = 2
    end
    object QrCNAB_cfgAgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Size = 40
    end
    object QrCNAB_cfgDirRetorno: TWideStringField
      FieldName = 'DirRetorno'
      Size = 255
    end
    object QrCNAB_cfgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNAB_cfgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNAB_cfgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNAB_cfgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNAB_cfgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNAB_cfgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCNAB_cfgAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCNAB_cfgEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
    end
    object QrCNAB_cfgMultaDias: TSmallintField
      FieldName = 'MultaDias'
    end
    object QrCNAB_cfgCartRetorno: TIntegerField
      FieldName = 'CartRetorno'
    end
    object QrCNAB_cfgPosicoesBB: TSmallintField
      FieldName = 'PosicoesBB'
    end
    object QrCNAB_cfgIndicatBB: TWideStringField
      FieldName = 'IndicatBB'
      Size = 1
    end
    object QrCNAB_cfgTipoCobrBB: TWideStringField
      FieldName = 'TipoCobrBB'
      Size = 5
    end
    object QrCNAB_cfgVariacao: TIntegerField
      FieldName = 'Variacao'
    end
    object QrCNAB_cfgComando: TIntegerField
      FieldName = 'Comando'
    end
    object QrCNAB_cfgDdProtesBB: TIntegerField
      FieldName = 'DdProtesBB'
    end
    object QrCNAB_cfgMsg40posBB: TWideStringField
      FieldName = 'Msg40posBB'
      Size = 40
    end
    object QrCNAB_cfgQuemDistrb: TWideStringField
      FieldName = 'QuemDistrb'
      Size = 1
    end
    object QrCNAB_cfgDesco1Cod: TSmallintField
      FieldName = 'Desco1Cod'
    end
    object QrCNAB_cfgDesco1Dds: TIntegerField
      FieldName = 'Desco1Dds'
    end
    object QrCNAB_cfgDesco1Fat: TFloatField
      FieldName = 'Desco1Fat'
    end
    object QrCNAB_cfgDesco2Cod: TSmallintField
      FieldName = 'Desco2Cod'
    end
    object QrCNAB_cfgDesco2Dds: TIntegerField
      FieldName = 'Desco2Dds'
    end
    object QrCNAB_cfgDesco2Fat: TFloatField
      FieldName = 'Desco2Fat'
    end
    object QrCNAB_cfgDesco3Cod: TSmallintField
      FieldName = 'Desco3Cod'
    end
    object QrCNAB_cfgDesco3Dds: TIntegerField
      FieldName = 'Desco3Dds'
    end
    object QrCNAB_cfgDesco3Fat: TFloatField
      FieldName = 'Desco3Fat'
    end
    object QrCNAB_cfgProtesCod: TSmallintField
      FieldName = 'ProtesCod'
    end
    object QrCNAB_cfgProtesDds: TSmallintField
      FieldName = 'ProtesDds'
    end
    object QrCNAB_cfgBxaDevCod: TSmallintField
      FieldName = 'BxaDevCod'
    end
    object QrCNAB_cfgBxaDevDds: TIntegerField
      FieldName = 'BxaDevDds'
    end
    object QrCNAB_cfgMoedaCod: TWideStringField
      FieldName = 'MoedaCod'
    end
    object QrCNAB_cfgContrato: TFloatField
      FieldName = 'Contrato'
    end
    object QrCNAB_cfgConvCartCobr: TWideStringField
      FieldName = 'ConvCartCobr'
      Size = 2
    end
    object QrCNAB_cfgConvVariCart: TWideStringField
      FieldName = 'ConvVariCart'
      Size = 3
    end
    object QrCNAB_cfgInfNossoNum: TSmallintField
      FieldName = 'InfNossoNum'
    end
    object QrCNAB_cfgCodLidrBco: TWideStringField
      FieldName = 'CodLidrBco'
    end
    object QrCNAB_cfgTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
    end
    object QrCNAB_cfgEspecieTxt: TWideStringField
      FieldName = 'EspecieTxt'
    end
    object QrCNAB_cfgCartTxt: TWideStringField
      FieldName = 'CartTxt'
      Size = 10
    end
    object QrCNAB_cfgLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrCNAB_cfgLayoutRet: TWideStringField
      FieldName = 'LayoutRet'
      Size = 50
    end
    object QrCNAB_cfgConcatCod: TWideStringField
      FieldName = 'ConcatCod'
      Size = 50
    end
    object QrCNAB_cfgComplmCod: TWideStringField
      FieldName = 'ComplmCod'
      Size = 10
    end
    object QrCNAB_cfgNaoRecebDd: TSmallintField
      FieldName = 'NaoRecebDd'
    end
    object QrCNAB_cfgTipBloqUso: TWideStringField
      FieldName = 'TipBloqUso'
      Size = 1
    end
    object QrCNAB_cfgNumVersaoI3: TIntegerField
      FieldName = 'NumVersaoI3'
    end
    object QrCNAB_cfgModalCobr: TSmallintField
      FieldName = 'ModalCobr'
    end
    object QrCNAB_cfgCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
  end
  object DsCNAB_cfg: TDataSource
    DataSet = QrCNAB_cfg
    Left = 364
    Top = 10
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 448
    Top = 11
  end
end
