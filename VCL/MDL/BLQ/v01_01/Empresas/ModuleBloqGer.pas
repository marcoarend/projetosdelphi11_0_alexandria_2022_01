unit ModuleBloqGer;

interface

uses
  SysUtils, Classes, DB, mySQLDbTables, Dialogs, Forms, Windows, ExtCtrls,
  UnMLAGeral, DBGrids, frxClass, dmkGeral, frxDBSet, Controls, UnMyObjects,
  UnDmkEnums;

type
  TDmBloqGer = class(TDataModule)
    QrNIA: TmySQLQuery;
    QrNIACodigo: TIntegerField;
    QrNIACodUsu: TIntegerField;
    QrNIANome: TWideStringField;
    QrNIAConta: TIntegerField;
    QrNIAValor: TFloatField;
    QrNIASitCobr: TIntegerField;
    QrNIAParcelas: TIntegerField;
    QrNIAParcPerI: TIntegerField;
    QrNIAParcPerF: TIntegerField;
    QrNIAInfoParc: TSmallintField;
    QrNIATexto: TWideStringField;
    QrNIAControle: TIntegerField;
    QrNIAArredonda: TFloatField;
    QrNIAEntidade: TIntegerField;
    QrNIANOMEENT: TWideStringField;
    QrNIACNAB_Cfg: TIntegerField;
    QrNIACartEmiss: TIntegerField;
    QrLocArre: TmySQLQuery;
    QrNIADiaVencto: TIntegerField;
    QrNIAValorInf: TSmallintField;
    QrArre: TmySQLQuery;
    QrArreEntidade: TIntegerField;
    QrArreValor: TFloatField;
    QrArreNOMEENT: TWideStringField;
    DsArre: TDataSource;
    QrArreBol: TmySQLQuery;
    DsArreBol: TDataSource;
    DsArreIts: TDataSource;
    QrArreIts: TmySQLQuery;
    QrArreItsCodigo: TIntegerField;
    QrArreItsControle: TIntegerField;
    QrArreItsCartEmiss: TIntegerField;
    QrArreItsCNAB_Cfg: TIntegerField;
    QrArreItsConta: TIntegerField;
    QrArreItsEntidade: TIntegerField;
    QrArreItsLancto: TIntegerField;
    QrArreItsNum: TIntegerField;
    QrArreItsTexto: TWideStringField;
    QrArreItsValor: TFloatField;
    QrArreItsVencto: TDateField;
    QrArreItsLk: TIntegerField;
    QrArreItsDataCad: TDateField;
    QrArreItsDataAlt: TDateField;
    QrArreItsUserCad: TIntegerField;
    QrArreItsUserAlt: TIntegerField;
    QrArreItsAlterWeb: TSmallintField;
    QrArreItsAtivo: TSmallintField;
    QrArreItsBloArre: TIntegerField;
    QrArreItsBloArreIts: TIntegerField;
    QrSumARRE: TmySQLQuery;
    QrSumARREValor: TFloatField;
    DsSumARRE: TDataSource;
    QrCNAB_Cfg: TmySQLQuery;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    QrCNAB_CfgCedente: TIntegerField;
    QrCNAB_CfgSacadAvali: TIntegerField;
    QrCNAB_CfgCedBanco: TIntegerField;
    QrCNAB_CfgCedAgencia: TIntegerField;
    QrCNAB_CfgCedConta: TWideStringField;
    QrCNAB_CfgCedDAC_A: TWideStringField;
    QrCNAB_CfgCedDAC_C: TWideStringField;
    QrCNAB_CfgCedDAC_AC: TWideStringField;
    QrCNAB_CfgCedNome: TWideStringField;
    QrCNAB_CfgCedPosto: TIntegerField;
    QrCNAB_CfgSacAvaNome: TWideStringField;
    QrCNAB_CfgTipoCart: TSmallintField;
    QrCNAB_CfgCartNum: TWideStringField;
    QrCNAB_CfgCartCod: TWideStringField;
    QrCNAB_CfgEspecieTit: TWideStringField;
    QrCNAB_CfgEspecieDoc: TWideStringField;
    QrCNAB_CfgAceiteTit: TSmallintField;
    QrCNAB_CfgInstrCobr1: TWideStringField;
    QrCNAB_CfgInstrCobr2: TWideStringField;
    QrCNAB_CfgInstrDias: TSmallintField;
    QrCNAB_CfgCodEmprBco: TWideStringField;
    QrCNAB_CfgJurosTipo: TSmallintField;
    QrCNAB_CfgJurosPerc: TFloatField;
    QrCNAB_CfgJurosDias: TSmallintField;
    QrCNAB_CfgMultaTipo: TSmallintField;
    QrCNAB_CfgMultaPerc: TFloatField;
    QrCNAB_CfgMultaDias: TSmallintField;
    QrCNAB_CfgTexto01: TWideStringField;
    QrCNAB_CfgTexto02: TWideStringField;
    QrCNAB_CfgTexto03: TWideStringField;
    QrCNAB_CfgTexto04: TWideStringField;
    QrCNAB_CfgTexto05: TWideStringField;
    QrCNAB_CfgTexto06: TWideStringField;
    QrCNAB_CfgTexto07: TWideStringField;
    QrCNAB_CfgTexto08: TWideStringField;
    QrCNAB_CfgTexto09: TWideStringField;
    QrCNAB_CfgTexto10: TWideStringField;
    QrCNAB_CfgCNAB: TIntegerField;
    QrCNAB_CfgEnvEmeio: TSmallintField;
    QrCNAB_CfgDiretorio: TWideStringField;
    QrCNAB_CfgQuemPrint: TWideStringField;
    QrCNAB_Cfg_237Mens1: TWideStringField;
    QrCNAB_Cfg_237Mens2: TWideStringField;
    QrCNAB_CfgCodOculto: TWideStringField;
    QrCNAB_CfgSeqArq: TIntegerField;
    QrCNAB_CfgLastNosNum: TLargeintField;
    QrCNAB_CfgLocalPag: TWideStringField;
    QrCNAB_CfgEspecieVal: TWideStringField;
    QrCNAB_CfgOperCodi: TWideStringField;
    QrCNAB_CfgIDCobranca: TWideStringField;
    QrCNAB_CfgAgContaCed: TWideStringField;
    QrCNAB_CfgDirRetorno: TWideStringField;
    QrCNAB_CfgCartRetorno: TIntegerField;
    QrCNAB_CfgPosicoesBB: TSmallintField;
    QrCNAB_CfgIndicatBB: TWideStringField;
    QrCNAB_CfgTipoCobrBB: TWideStringField;
    QrCNAB_CfgVariacao: TIntegerField;
    QrCNAB_CfgComando: TIntegerField;
    QrCNAB_CfgDdProtesBB: TIntegerField;
    QrCNAB_CfgMsg40posBB: TWideStringField;
    QrCNAB_CfgQuemDistrb: TWideStringField;
    QrCNAB_CfgDesco1Cod: TSmallintField;
    QrCNAB_CfgDesco1Dds: TIntegerField;
    QrCNAB_CfgDesco1Fat: TFloatField;
    QrCNAB_CfgDesco2Cod: TSmallintField;
    QrCNAB_CfgDesco2Dds: TIntegerField;
    QrCNAB_CfgDesco2Fat: TFloatField;
    QrCNAB_CfgDesco3Cod: TSmallintField;
    QrCNAB_CfgDesco3Dds: TIntegerField;
    QrCNAB_CfgDesco3Fat: TFloatField;
    QrCNAB_CfgProtesCod: TSmallintField;
    QrCNAB_CfgProtesDds: TSmallintField;
    QrCNAB_CfgBxaDevCod: TSmallintField;
    QrCNAB_CfgBxaDevDds: TIntegerField;
    QrCNAB_CfgMoedaCod: TWideStringField;
    QrCNAB_CfgContrato: TFloatField;
    QrCNAB_CfgConvCartCobr: TWideStringField;
    QrCNAB_CfgConvVariCart: TWideStringField;
    QrCNAB_CfgInfNossoNum: TSmallintField;
    QrCNAB_CfgCodLidrBco: TWideStringField;
    QrCNAB_CfgLk: TIntegerField;
    QrCNAB_CfgDataCad: TDateField;
    QrCNAB_CfgDataAlt: TDateField;
    QrCNAB_CfgUserCad: TIntegerField;
    QrCNAB_CfgUserAlt: TIntegerField;
    QrCNAB_CfgAlterWeb: TSmallintField;
    QrCNAB_CfgAtivo: TSmallintField;
    QrCNAB_CfgCartEmiss: TIntegerField;
    QrPesqMB: TmySQLQuery;
    QrSumBol: TmySQLQuery;
    QrSumBolVALOR: TFloatField;
    QrSumPre: TmySQLQuery;
    QrSumPreVALOR: TFloatField;
    QrLoc: TmySQLQuery;
    QrArreCNAB_Cfg: TIntegerField;
    QrArreNOMECNAB_Cfg: TWideStringField;
    frxBloqE2: TfrxReport;
    frxBloqE1: TfrxReport;
    QrCNAB_CfgDVB: TWideStringField;
    QrCNAB_CfgNOMEBANCO: TWideStringField;
    frxDsCNAB_Cfg: TfrxDBDataset;
    QrCNAB_CfgNOMECED_IMP: TWideStringField;
    QrCNAB_CfgACEITETIT_TXT: TWideStringField;
    frxDsBoletos: TfrxDBDataset;
    frxBloq: TfrxReport;
    frxBloqA: TfrxReport;
    frxDsBoletosIts: TfrxDBDataset;
    QrArreBoleto: TFloatField;
    QrArreBolBoleto: TFloatField;
    QrArreItsBoleto: TFloatField;
    QrPesqMBBoleto: TFloatField;
    frxVerso: TfrxReport;
    QrArreVencto: TDateField;
    QrBoletos: TmySQLQuery;
    QrBoletosApto: TIntegerField;
    QrBoletosUnidade: TWideStringField;
    QrBoletosSUB_ARR: TFloatField;
    QrBoletosSUB_LEI: TFloatField;
    QrBoletosSUB_TOT: TFloatField;
    QrBoletosVencto: TDateField;
    QrBoletosPropriet: TIntegerField;
    QrBoletosNOMEPROPRIET: TWideStringField;
    QrBoletosBOLAPTO: TWideStringField;
    QrBoletosVENCTO_TXT: TWideStringField;
    QrBoletosLNR: TWideStringField;
    QrBoletosLN2: TWideStringField;
    QrBoletosBoleto: TFloatField;
    QrBoletosBLOQUETO: TFloatField;
    DsBoletos: TDataSource;
    QrBoletosIts: TmySQLQuery;
    QrBoletosItsTEXTO: TWideStringField;
    QrBoletosItsVALOR: TFloatField;
    QrBoletosItsVencto: TDateField;
    QrBoletosItsTEXTO_IMP: TWideStringField;
    QrBoletosItsMedAnt: TFloatField;
    QrBoletosItsMedAtu: TFloatField;
    QrBoletosItsConsumo: TFloatField;
    QrBoletosItsCasas: TLargeintField;
    QrBoletosItsUnidLei: TWideStringField;
    QrBoletosItsUnidImp: TWideStringField;
    QrBoletosItsUnidFat: TFloatField;
    QrBoletosItsTipo: TLargeintField;
    QrBoletosItsVENCTO_TXT: TWideStringField;
    QrBoletosItsControle: TIntegerField;
    QrBoletosItsLancto: TIntegerField;
    DsBoletosIts: TDataSource;
    frxDsInquilino: TfrxDBDataset;
    QrInquilino: TmySQLQuery;
    QrInquilinoCodigo: TIntegerField;
    QrInquilinoTipo: TSmallintField;
    QrInquilinoNOMEENT: TWideStringField;
    QrInquilinoCNPJ_CPF: TWideStringField;
    QrInquilinoIE_RG: TWideStringField;
    QrInquilinoNIRE_: TWideStringField;
    QrInquilinoRUA: TWideStringField;
    QrInquilinoNUMERO: TWideStringField;
    QrInquilinoCOMPL: TWideStringField;
    QrInquilinoBAIRRO: TWideStringField;
    QrInquilinoCIDADE: TWideStringField;
    QrInquilinoNOMELOGRAD: TWideStringField;
    QrInquilinoNOMEUF: TWideStringField;
    QrInquilinoPais: TWideStringField;
    QrInquilinoLograd: TWideStringField;
    QrInquilinoCEP: TWideStringField;
    QrInquilinoTE1: TWideStringField;
    QrInquilinoFAX: TWideStringField;
    QrInquilinoNATAL_TXT: TWideStringField;
    QrInquilinoTE1_TXT: TWideStringField;
    QrInquilinoFAX_TXT: TWideStringField;
    QrInquilinoCNPJ_TXT: TWideStringField;
    QrInquilinoE_ALL: TWideStringField;
    QrInquilinoNUMERO_TXT: TWideStringField;
    QrInquilinoECEP_TXT: TWideStringField;
    QrComposA: TmySQLQuery;
    QrComposATEXTO: TWideStringField;
    QrComposAVALOR: TFloatField;
    QrComposAConta: TIntegerField;
    QrComposAArreBaI: TIntegerField;
    QrComposAIts: TmySQLQuery;
    QrComposAItsNOMEPROPRIET: TWideStringField;
    QrComposAItsUnidade: TWideStringField;
    QrComposAItsApto: TIntegerField;
    QrComposAItsValor: TFloatField;
    QrComposAItsPropriet: TIntegerField;
    QrComposAItsControle: TIntegerField;
    DsComposAIts: TDataSource;
    DsComposA: TDataSource;
    QrComposL: TmySQLQuery;
    QrComposLCodigo: TIntegerField;
    QrComposLNome: TWideStringField;
    QrComposLVALOR: TFloatField;
    QrComposLCasas: TSmallintField;
    DsComposL: TDataSource;
    DsComposLIts: TDataSource;
    QrComposLIts: TmySQLQuery;
    QrComposLItsApto: TIntegerField;
    QrComposLItsMedAnt: TFloatField;
    QrComposLItsMedAtu: TFloatField;
    QrComposLItsConsumo: TFloatField;
    QrComposLItsNOMEPROPRIET: TWideStringField;
    QrComposLItsUnidade: TWideStringField;
    QrComposLItsValor: TFloatField;
    QrComposLItsControle: TIntegerField;
    QrBolArr: TmySQLQuery;
    QrBolArrValor: TFloatField;
    QrBolArrApto: TIntegerField;
    QrBolArrBOLAPTO: TWideStringField;
    QrBolArrBoleto: TFloatField;
    QrBolLei: TmySQLQuery;
    QrBolLeiValor: TFloatField;
    QrBolLeiApto: TIntegerField;
    QrBolLeiBOLAPTO: TWideStringField;
    QrBolLeiBoleto: TFloatField;
    QrPropriet: TmySQLQuery;
    QrProprietCodigo: TIntegerField;
    QrProprietCadastro: TDateField;
    QrProprietENatal: TDateField;
    QrProprietPNatal: TDateField;
    QrProprietTipo: TSmallintField;
    QrProprietRespons1: TWideStringField;
    QrProprietNOMEDONO: TWideStringField;
    QrProprietCNPJ_CPF: TWideStringField;
    QrProprietIE_RG: TWideStringField;
    QrProprietNIRE_: TWideStringField;
    QrProprietRUA: TWideStringField;
    QrProprietNUMERO: TLargeintField;
    QrProprietCOMPL: TWideStringField;
    QrProprietBAIRRO: TWideStringField;
    QrProprietCIDADE: TWideStringField;
    QrProprietNOMELOGRAD: TWideStringField;
    QrProprietNOMEUF: TWideStringField;
    QrProprietPais: TWideStringField;
    QrProprietLograd: TLargeintField;
    QrProprietCEP: TLargeintField;
    QrProprietTE1: TWideStringField;
    QrProprietFAX: TWideStringField;
    QrProprietNUMERO_TXT: TWideStringField;
    QrProprietLNR: TWideStringField;
    QrProprietLN2: TWideStringField;
    QrCNAB_CfgForneceI: TIntegerField;
    QrBoletosItsApto: TIntegerField;
    QrCNAB_CfgTipoCobranca: TIntegerField;
    QrCNAB_CfgEspecieTxt: TWideStringField;
    QrCNAB_CfgCartTxt: TWideStringField;
    QrCNAB_CfgLayoutRem: TWideStringField;
    QrCNAB_CfgLayoutRet: TWideStringField;
    QrCNAB_CfgConcatCod: TWideStringField;
    QrCNAB_CfgComplmCod: TWideStringField;
    QrCNAB_CfgNaoRecebDd: TSmallintField;
    QrCNAB_CfgTipBloqUso: TWideStringField;
    QrCNAB_CfgNumVersaoI3: TIntegerField;
    QrCNAB_CfgModalCobr: TSmallintField;
    QrCNAB_CfgCtaCooper: TWideStringField;
    procedure QrArreAfterScroll(DataSet: TDataSet);
    procedure QrArreBeforeClose(DataSet: TDataSet);
    procedure QrArreBolAfterScroll(DataSet: TDataSet);
    procedure QrArreBolBeforeClose(DataSet: TDataSet);
    procedure QrArreCfgAfterScroll(DataSet: TDataSet);
    procedure QrArreCfgBeforeClose(DataSet: TDataSet);
    procedure frxBloqE1GetValue(const VarName: string; var Value: Variant);
    procedure QrCNAB_CfgCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure frxVersoGetValue(const VarName: string; var Value: Variant);
    procedure QrBoletosAfterScroll(DataSet: TDataSet);
    procedure QrBoletosBeforeClose(DataSet: TDataSet);
    procedure QrBoletosCalcFields(DataSet: TDataSet);
    procedure QrBoletosItsCalcFields(DataSet: TDataSet);
    procedure QrInquilinoCalcFields(DataSet: TDataSet);
    procedure QrComposAAfterScroll(DataSet: TDataSet);
    procedure QrComposABeforeClose(DataSet: TDataSet);
    procedure QrComposLAfterScroll(DataSet: TDataSet);
    procedure QrComposLBeforeClose(DataSet: TDataSet);
    procedure QrProprietCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    // 2010-07-14
    FCkZerado: Boolean;
{
    FBolItsSimNao, FCkZerado: Boolean;
}
    FAviso01, FAviso02, FAviso03, FAviso04, FAviso05, FAviso06, FAviso07,
    FAviso08, FAviso09, FAviso10, FAviso11, FAviso12, FAviso13, FAviso14,
    FAviso15, FAviso16, FAviso17, FAviso18, FAviso19, FAviso20,
    FAvisoVerso: String;
    procedure ReopenEnderecoInquilino();
    // fim 2010-07-14
  public
    { Public declarations }
    FBolSim, FBolNao, FBolItsSim, FBolItsNao: String;
    FPrevCodigo, FPrevCond, FPrevPeriodo: Integer;
    FEhPreBloqueto: Boolean;
    FNossoNumero: String;
    function VerificaArrecadacao(Entidade, Conta, BloArre, BloArreIts,
      Empresa, Periodo: Integer): Boolean;

    procedure ReopenArreBol(Boleto: Integer);
    procedure ReopenArre(Entidade: Integer);
    procedure ReopenArreIts(Controle: Integer);
    procedure ReopenBoletos(BOLAPTO: String);
    procedure ReopenBoletosIts();
    procedure ReopenComposAIts(Apto: Integer);
    procedure ReopenComposLIts(Apto: Integer);

    procedure AtualizaArrecadacao(Periodo, Entidade, Controle: Integer);
    procedure PreencheModelosBloq(RG: TRadioGroup);
    //
    procedure ImprimeBloq(Tipo: TselType; ModelBloq, Compe: Integer; Zerado:
    Boolean; Periodo: Integer; DBGradeBlo: TDBGrid;
    Aviso01, Aviso02, Aviso03, Aviso04, Aviso05, Aviso06, Aviso07,
    Aviso08, Aviso09, Aviso10, Aviso11, Aviso12, Aviso13, Aviso14,
    Aviso15, Aviso16, Aviso17, Aviso18, Aviso19, Aviso20,
    AvisoVerso: String);
    procedure InsereLancto(Vencto: TDateTime; Boleto, Valor, JurosPerc,
      MultaPerc: Double; Prev, Entidade, Periodo, CartEmiss, Genero,
      Controle: Integer; Descri: String; Avulso: Boolean; CondCliente, Apto: Integer);
    function GeraBoleto(CodPeriodo, CNAB_Cfg: Integer; Vencto: TDate;
    Avulso: Boolean; var JurosPerc, MultaPerc: Double; var CartEmiss: Integer;
    var NumBloq: Double; AvisaCNABNaoImplementado: Boolean;
    CondCliente: Integer): Boolean;
    // 2010-07-14
    function ObtemValorQrBoletosIts(Item, Tipo: Integer): String;
  end;

var
  DmBloqGer: TDmBloqGer;

implementation

uses Module, UnBancos, MeuFrx, ModuleGeral, MyGlyfs, UnGotoy,
  UnInternalConsts, ModuleBco, UnBco_Rem, UnFinanceiro, UMySQLModule;

{$R *.dfm}

{ TDBloGeren }

function TDmBloqGer.VerificaArrecadacao(Entidade, Conta, BloArre, BloArreIts,
  Empresa, Periodo: Integer): Boolean;
begin
  QrLocArre.Close;
  QrLocArre.SQL.Clear;
  QrLocArre.SQL.Add('SELECT its.Controle');
  QrLocArre.SQL.Add('FROM arreits its');
  QrLocArre.SQL.Add('LEFT JOIN prev pre ON pre.Codigo = its.Codigo');
  QrLocArre.SQL.Add('WHERE its.BloArre=:P0');
  QrLocArre.SQL.Add('AND its.BloArreIts=:P1');
  QrLocArre.SQL.Add('AND pre.Empresa=:P2');
  QrLocArre.SQL.Add('AND pre.Periodo=:P3');
  QrLocArre.Params[0].AsInteger := BloArre;
  QrLocArre.Params[1].AsInteger := BloArreIts;
  QrLocArre.Params[2].AsInteger := Empresa;
  QrLocArre.Params[3].AsInteger := Periodo;
  QrLocArre.Open;
  //
  if QrLocArre.RecordCount > 0 then
    Result := False
  else
    Result := True;
end;

procedure TDmBloqGer.AtualizaArrecadacao(Periodo, Entidade, Controle: Integer);
begin
  ReopenArre(Entidade);
  ReopenArreIts(Controle);
end;

procedure TDmBloqGer.DataModuleCreate(Sender: TObject);
begin
  FBolSim    := '';
  FBolItsSim := '';
  FBolNao    := '';
  FBolItsNao := '';
  frxBloqE2.ScriptText := frxBloqE1.ScriptText;
end;

procedure TDmBloqGer.frxBloqE1GetValue(const VarName: string;
  var Value: Variant);
var
  DVB: String;
  V, M, J, C, P: Double;
  GeraNossoGeral, CodigoDeCobranca, LinhaDigital, NossoNumero_Rem: String;
begin
  P := 0;
  C := 0;
  //V := QrBoletosValor.Value;
  V := QrBoletosSUB_TOT.Value;
  M := QrCNAB_CfgMultaPerc.Value;
  J := QrCNAB_CfgJurosPerc.Value;
  //
  UBancos.GeraNossoNumero(QrCNAB_CfgTipoCart.Value, QrCNAB_CfgCedBanco.Value,
    QrCNAB_CfgCedAgencia.Value, QrCNAB_CfgCedPosto.Value, QrBoletosBoleto.Value,
    QrCNAB_CfgCedConta.Value, QrCNAB_CfgCartNum.Value, QrCNAB_CfgIDCobranca.Value,
    Geral.SoNumero_TT(QrCNAB_CfgCodEmprBco.Value), QrBoletosVencto.Value,
    QrCNAB_CfgTipoCobranca.Value, QrCNAB_CfgEspecieDoc.Value, QrCNAB_CfgCNAB.Value,
    QrCNAB_CfgCtaCooper.Value, QrCNAB_CfgLayoutRem.Value, FNossoNumero, NossoNumero_Rem);
  GeraNossoGeral := FNossoNumero;
  CodigoDeCobranca := UBancos.CodigoDeBarra_BoletoDeCobranca(QrCNAB_CfgCedBanco.Value,
    QrCNAB_CfgCedAgencia.Value, QrCNAB_CfgCedDAC_A.Value,
    QrCNAB_CfgCedPosto.Value, QrCNAB_CfgCedConta.Value,
    QrCNAB_CfgCedDAC_C.Value, 9, 3, 1, FNossoNumero,
    QrCNAB_CfgCodEmprBco.Value, QrCNAB_CfgCartNum.Value,
    QrCNAB_CfgIDCobranca.Value, QrCNAB_CfgOperCodi.Value,
    QrBoletosVencto.Value, QrBoletosSUB_TOT.Value, 0, 0,
    not FCkZerado(*FmBloGeren.CkZerado.Checked*), QrCNAB_CfgTipoCart.Value);
  LinhaDigital := UBancos.LinhaDigitavel_BoletoDeCobranca(
    UBancos.CodigoDeBarra_BoletoDeCobranca(QrCNAB_CfgCedBanco.Value,
    QrCNAB_CfgCedAgencia.Value, QrCNAB_CfgCedDAC_A.Value, QrCNAB_CfgCedPosto.Value,
    QrCNAB_CfgCedConta.Value, QrCNAB_CfgCedDAC_C.Value, 9, 3, 1, FNossoNumero,
    QrCNAB_CfgCodEmprBco.Value, QrCNAB_CfgCartNum.Value, QrCNAB_CfgIDCobranca.Value,
    QrCNAB_CfgOperCodi.Value, QrBoletosVencto.Value, QrBoletosSUB_TOT.Value,
    0, 0, not FCkZerado(*FmBloGeren.CkZerado.Checked*), QrCNAB_CfgTipoCart.Value));
  //
  if AnsiCompareText(VarName, 'VARF_NossoNumero') = 0 then
  begin
    Value := GeraNossoGeral;
  end else
  if AnsiCompareText(VarName, 'VAX') = 0 then
  begin
    if QrCNAB_CfgDVB.Value <> '?' then DVB := QrCNAB_CfgDVB.Value else
    DVB := UBancos.DigitoVerificadorCodigoBanco(QrCNAB_CfgCedBanco.Value);
    Value := FormatFloat('000', QrCNAB_CfgCedBanco.Value) + '-' + DVB;
  end else
  if AnsiCompareText(VarName, 'VARF_CODIGOBARRAS') = 0 then
  begin
    Value := CodigoDeCobranca;
  end else
  if AnsiCompareText(VarName, 'VARF_LINHADIGITAVEL') = 0 then
  begin
    Value := LinhaDigital;
  end else
  if AnsiCompareText(VarName, 'VARF_MESANOA') = 0 then
    Value := MLAGeral.MesEAnoDoPeriodo(FPrevPeriodo (*FmBloGeren.QrPrevPeriodo.Value*) + 1)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_01') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto01.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_02') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto02.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_03') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto03.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_04') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto04.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_05') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto05.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_06') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto06.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_07') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto07.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_08') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto08.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_09') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto09.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_10') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto10.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_1') = 0 then
    Value := FAviso01 (*FmBloGeren.QrPrevAviso01.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_2') = 0 then
    Value := FAviso02 (*FmBloGeren.QrPrevAviso02.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_3') = 0 then
    Value := FAviso03 (*FmBloGeren.QrPrevAviso03.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_4') = 0 then
    Value := FAviso04 (*FmBloGeren.QrPrevAviso04.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_5') = 0 then
    Value := FAviso05 (*FmBloGeren.QrPrevAviso05.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_6') = 0 then
    Value := FAviso06 (*FmBloGeren.QrPrevAviso06.Value *)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_7') = 0 then
    Value := FAviso07 (*FmBloGeren.QrPrevAviso07.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_8') = 0 then
    Value := FAviso08 (*FmBloGeren.QrPrevAviso08.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_9') = 0 then
    Value := FAviso09 (*FmBloGeren.QrPrevAviso09.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_10') = 0 then
    Value := FAviso10 (*FmBloGeren.QrPrevAviso10.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_11') = 0 then
    Value := FAviso11 (*FmBloGeren.QrPrevAviso11.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_12') = 0 then
    Value := FAviso12 (*FmBloGeren.QrPrevAviso12.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_13') = 0 then
    Value := FAviso13 (*FmBloGeren.QrPrevAviso13.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_14') = 0 then
    Value := FAviso14 (*FmBloGeren.QrPrevAviso14.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_15') = 0 then
    Value := FAviso15 (*FmBloGeren.QrPrevAviso15.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_16') = 0 then
    Value := FAviso16 (*FmBloGeren.QrPrevAviso16.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_17') = 0 then
    Value := FAviso17 (*FmBloGeren.QrPrevAviso17.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_18') = 0 then
    Value := FAviso18 (*FmBloGeren.QrPrevAviso18.Value *)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_19') = 0 then
    Value := FAviso19 (*FmBloGeren.QrPrevAviso19.Value*)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_20') = 0 then
    Value := FAviso20 (*FmBloGeren.QrPrevAviso20.Value*)
  else
  if AnsiCompareText(VarName, 'LogoBancoExiste') = 0 then
    Value := FmMyGlyfs.LogoBancoExiste(QrCNAB_CfgCedBanco.Value)
  else
  if AnsiCompareText(VarName, 'LogoBancoPath') = 0 then
    Value := FmMyGlyfs.CaminhoLogoBanco(QrCNAB_CfgCedBanco.Value)
end;

procedure TDmBloqGer.frxVersoGetValue(const VarName: string;
  var Value: Variant);
var
  Calculo: Integer;
begin
  if AnsiCompareText(VarName, 'VARF_BLQ_AltuHeader') = 0 then
    Value := Int(Dmod.QrControleBLQ_TopoAvisoV.Value / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuRotulo') = 0 then
  begin
    Calculo := 26000 (*- 4500*) - Dmod.QrControleBLQ_TopoAvisoV.Value;
    Value := Int(Calculo / VAR_frCM);
  end
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuDestin') = 0 then
    Value := Dmod.QrControleBLQ_AltuDestin.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_MEsqDestin') = 0 then
    Value := Dmod.QrControleBLQ_MEsqDestin.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_LargDestin') = 0 then
    Value := Dmod.QrControleBLQ_LargDestin.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_TopoDestin') = 0 then
  begin
    Calculo := Dmod.QrControleBLQ_TopoDestin.Value -
               Dmod.QrControleBLQ_TopoAvisoV.Value;
    Value := Int(Calculo / VAR_frCM);
  end
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuAvisoV') = 0 then
    Value := Dmod.QrControleBLQ_AltuAvisoV.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_MEsqAvisoV') = 0 then
    Value := Dmod.QrControleBLQ_MEsqAvisoV.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_LargAvisoV') = 0 then
    Value := Dmod.QrControleBLQ_LargAvisoV.Value / VAR_frCM
  //
  else if AnsiCompareText(VarName, 'VARF_NOMEPROPRIET') = 0 then
    Value := QrBoletosNOMEPROPRIET.Value
  else if AnsiCompareText(VarName, 'VARF_AVISOVERSO') = 0 then
    Value := FAvisoVerso (*FmBloGeren.QrPrevAvisoVerso.Value*);
end;

function TDmBloqGer.GeraBoleto(CodPeriodo, CNAB_Cfg: Integer; Vencto: TDate;
Avulso: Boolean; var JurosPerc, MultaPerc: Double; var CartEmiss: Integer;
var NumBloq: Double; AvisaCNABNaoImplementado: Boolean;
CondCliente: Integer): Boolean;
var
  NossoNumero_Rem: String;
begin
  Result := False;
  //
  if MyObjects.FIC(CNAB_Cfg = 0, nil,
    'Configura��o CNAB n�o definida no condom�nio!') then
      Exit;
  //
  if (QrCNAB_Cfg.State <> dsInactive) or
  (CNAB_Cfg <>  QrCNAB_Cfg.Params[0].AsInteger) then
  begin
    QrCNAB_Cfg.Close;
    QrCNAB_Cfg.Params[0].AsInteger := CNAB_Cfg;
    QrCNAB_Cfg.Open;
  end;
  if MyObjects.FIC(QrCNAB_CfgCartEmiss.Value = 0, nil,
    'Carteira de emiss�o de bloquetos n�o definida na configura��o CNAB!') then
      Exit;
  if MyObjects.FIC(QrCNAB_CfgForneceI.Value <> CondCliente, nil,
    'Carteira de emiss�o de bloquetos n�o pertence ao cliente interno!') then
      Exit;

  //
  NumBloq := UBco_Rem.ObtemProximoNossoNumero(CNAB_Cfg, AvisaCNABNaoImplementado);

  JurosPerc := QrCNAB_CfgJurosPerc.Value;
  MultaPerc := QrCNAB_CfgMultaPerc.Value;
  CartEmiss := QrCNAB_CfgCartEmiss.Value;
  //
  UBancos.GeraNossoNumero(QrCNAB_CfgTipoCart.Value,
    QrCNAB_CfgCedBanco.Value, QrCNAB_CfgCedAgencia.Value,
    QrCNAB_CfgCedPosto.Value, NumBloq, QrCNAB_CfgCedConta.Value,
    QrCNAB_CfgCartNum.Value, QrCNAB_CfgIDCobranca.Value,
    Geral.SoNumero_TT(QrCNAB_CfgCodEmprBco.Value), Vencto,
    QrCNAB_CfgTipoCobranca.Value, QrCNAB_CfgEspecieDoc.Value, QrCNAB_CfgCNAB.Value,
    QrCNAB_CfgCtaCooper.Value, QrCNAB_CfgLayoutRem.Value, FNossoNumero, NossoNumero_Rem);
  //
  if not Avulso then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE arreits SET Boleto=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Boleto=0 AND Codigo=:P1 ');
    Dmod.QrUpd.SQL.Add('AND Propriet=:P2 AND Vencto=:P3');
    Dmod.QrUpd.Params[00].AsFloat    := NumBloq;
    Dmod.QrUpd.Params[01].AsInteger  := CodPeriodo;
    Dmod.QrUpd.Params[02].AsInteger  := QrBoletosPropriet.Value;
    Dmod.QrUpd.Params[03].AsDateTime := Vencto;
    Dmod.QrUpd.ExecSQL;
  end;
  Result := True;
end;

procedure TDmBloqGer.ImprimeBloq(Tipo: TselType; ModelBloq, Compe: Integer;
Zerado: Boolean; Periodo: Integer; DBGradeBlo: TDBGrid;
    Aviso01, Aviso02, Aviso03, Aviso04, Aviso05, Aviso06, Aviso07,
    Aviso08, Aviso09, Aviso10, Aviso11, Aviso12, Aviso13, Aviso14,
    Aviso15, Aviso16, Aviso17, Aviso18, Aviso19, Aviso20,
    AvisoVerso: String);
var
  prim: Boolean;
  procedure GeraBloq(ModelBloq, Compe, CNAB_Cfg, Entidade: Integer);
  var
    s: TMemoryStream;
    frxAtu: TfrxReport;
  begin
    frxAtu := nil;
    //
    QrCNAB_Cfg.Close;
    QrCNAB_Cfg.Params[0].AsInteger := CNAB_Cfg;
    QrCNAB_Cfg.Open;
    DModG.ReopenEndereco(Entidade);
    case ModelBloq of
      0: frxAtu := frxVerso;
      1: frxAtu := frxBloqA;
      2:
      begin
        case Compe of
          1: frxAtu := frxBloqE1;
          2: frxAtu := frxBloqE2;
        end;
      end;
    end;
    //
    frxBloq.OnGetValue         := frxAtu.OnGetValue;
    frxBloq.OnUserFunction     := frxAtu.OnUserFunction;
    frxBloq.ReportOptions.Name := 'Boletos';
    //
    if ModelBloq <> 0 then
    begin
      frxAtu.Variables['MeuLogoExiste']  := VAR_MEULOGOEXISTE;
      frxAtu.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
      frxAtu.Variables['LogoNFExiste']   := VAR_LOGONFEXISTE;
      frxAtu.Variables['LogoNFCaminho']  := QuotedStr(VAR_LOGONFPATH);
    end;
    //
    s := TMemoryStream.Create;
    frxAtu.SaveToStream(s);
    s.Position := 0;
    frxBloq.LoadFromStream(s);
    //
    if not prim then
    begin
      frxBloq.PrepareReport(True);
      prim := True;
    end else
      frxBloq.PrepareReport(False);
  end;
begin
{ Parei aqui 2010-07-14 Falta fazer
  FCkZerado := Zerado;
  FPrevPeriodo  := Periodo;
  FAviso01 := Aviso01;
  FAviso02 := Aviso02;
  FAviso03 := Aviso03;
  FAviso04 := Aviso04;
  FAviso05 := Aviso05;
  FAviso06 := Aviso06;
  FAviso07 := Aviso07;
  FAviso08 := Aviso08;
  FAviso09 := Aviso09;
  FAviso10 := Aviso10;
  FAviso11 := Aviso11;
  FAviso12 := Aviso12;
  FAviso13 := Aviso13;
  FAviso14 := Aviso14;
  FAviso15 := Aviso15;
  FAviso16 := Aviso16;
  FAviso17 := Aviso17;
  FAviso18 := Aviso18;
  FAviso19 := Aviso19;
  FAviso20 := Aviso20;
  FAvisoVerso := AvisoVerso;
  //
  prim := False;
  frxBloq.Clear;
  case Tipo of
    istAtual:
    begin
      GeraBloq(ModelBloq, Compe, QrBoletosCNAB_Cfg.Value, QrBoletosPropriet.Value);
    end;
    istSelecionados:
    begin
      if DBGradeBlo.SelectedRows.Count > 1 then
      begin
        with DBGradeBlo.DataSource.DataSet do
        for i:= 0 to DBGradeBlo.SelectedRows.Count - 1 do
        begin
          GotoBookmark(pointer(DBGradeBlo.SelectedRows.Items[i]));
          GeraBloq(ModelBloq, Compe, QrBoletosCNAB_Cfg.Value, QrBoletosPropriet.Value);
        end;
      end else
        GeraBloq(ModelBloq, Compe, QrBoletosCNAB_Cfg.Value, QrBoletosPropriet.Value);
    end;
    istTodos:
    begin
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        GeraBloq(ModelBloq, Compe, QrBoletosCNAB_Cfg.Value, QrBoletosPropriet.Value);
        //
        QrBoletos.Next;
      end;
    end;
  end;
  Application.CreateForm(TFmMeuFrx, FmMeuFrx);
  frxBloq.Preview := FmMeufrx.PvVer;
  //
  FmMeufrx.PvVer.OutlineWidth := frxBloq.PreviewOptions.OutlineWidth;
  FmMeufrx.PvVer.Zoom         := frxBloq.PreviewOptions.Zoom;
  FmMeufrx.PvVer.ZoomMode     := frxBloq.PreviewOptions.ZoomMode;
  FmMeufrx.UpdateZoom;
  //
  FmMeufrx.ShowModal;
  FmMeufrx.Destroy;
}
end;

procedure TDmBloqGer.InsereLancto(Vencto: TDateTime; Boleto, Valor, JurosPerc,
MultaPerc: Double; Prev, Entidade, Periodo, CartEmiss, Genero, Controle:
Integer; Descri: String; Avulso: Boolean; CondCliente, Apto: Integer);
begin
  UFinanceiro.LancamentoDefaultVARS;
  //
  FLAN_Data       := Geral.FDT(Date, 1);
  FLAN_Tipo       := 2;
  FLAN_Documento  := Boleto;
  FLAN_Credito    := Valor;
  FLAN_MoraDia    := JurosPerc;
  FLAN_Multa      := MultaPerc;
  FLAN_Carteira   := CartEmiss;
  FLAN_Genero     := Genero;
  FLAN_Cliente    := Entidade;
  FLAN_CliInt     := CondCliente;
  FLAN_Vencimento := Geral.FDT(Vencto, 1);
  FLAN_Mez        := IntToStr(MLAGeral.PeriodoToAnoMes(Periodo));
  FLAN_FatID      := 600;
  FLAN_FatNum     := Trunc(Boleto);
  FLAN_FatParcela := 1;
  FLAN_Depto      := Apto;
  FLAN_ForneceI   := Entidade;
  if Avulso then
    FLAN_Descricao := Descri
  else
    FLAN_Descricao  := Descri + ' - ' + MLAGeral.MesEAnoDoPeriodo(Periodo);
  FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
    'Controle', 'Lanctos', 'Lanctos', 'Controle');
  if UFinanceiro.InsereLancamento then
  begin
    if not Avulso then
    begin
      Dmod.QrUpd2.SQL.Clear;
      Dmod.QrUpd2.SQL.Add('UPDATE arreits SET Lancto=:P0 WHERE Controle=:P1');
      Dmod.QrUpd2.Params[00].AsInteger := FLAN_Controle;
      Dmod.QrUpd2.Params[01].AsInteger := Controle;
      Dmod.QrUpd2.ExecSQL;
    end;
  end;
end;

function TDmBloqGer.ObtemValorQrBoletosIts(Item, Tipo: Integer): String;
begin
  if QrBoletosIts.RecordCount >= Item then
  begin
    if QrBoletosIts.RecNo < Item then
      while QrBoletosIts.RecNo < Item do
        QrBoletosIts.Next;
    if QrBoletosIts.RecNo > Item then
      while QrBoletosIts.RecNo > Item do
        QrBoletosIts.Prior;
    case Tipo of
      0: Result := QrBoletosItsTEXTO_IMP.Value;
      1: Result := Geral.FFT(QrBoletosItsVALOR.Value, 2, siPositivo);
      else Result := '<?>';
    end;
  end else Result := ' ';
end;

procedure TDmBloqGer.PreencheModelosBloq(RG: TRadioGroup);
begin
  RG.Items.Clear;
  {0} RG.Items.Add('? - Definir no momento da impress�o');
  {1} RG.Items.Add('A - 3 colunas fonte tamanho 7 sem rodap� (3 n�veis)');
  {2} RG.Items.Add('E - Somente boleto (nenhum n�vel)');
end;

procedure TDmBloqGer.QrArreAfterScroll(DataSet: TDataSet);
begin
  ReopenArreBol(0);
end;

procedure TDmBloqGer.QrArreBeforeClose(DataSet: TDataSet);
begin
  QrArreBol.Close;
end;

procedure TDmBloqGer.QrArreBolAfterScroll(DataSet: TDataSet);
begin
  ReopenArreIts(0);
end;

procedure TDmBloqGer.QrArreBolBeforeClose(DataSet: TDataSet);
begin
  QrArreIts.Close;
end;

procedure TDmBloqGer.QrArreCfgAfterScroll(DataSet: TDataSet);
begin
  ReopenArre(0);
end;

procedure TDmBloqGer.QrArreCfgBeforeClose(DataSet: TDataSet);
begin
  QrArre.Close;
end;

procedure TDmBloqGer.QrBoletosAfterScroll(DataSet: TDataSet);
begin
  ReopenBoletosIts;
  ReopenEnderecoInquilino;
end;

procedure TDmBloqGer.QrBoletosBeforeClose(DataSet: TDataSet);
begin
  QrBoletosIts.Close;
end;

procedure TDmBloqGer.QrBoletosCalcFields(DataSet: TDataSet);
begin
  QrBoletosSUB_TOT.Value :=
  QrBoletosSUB_ARR.Value +
  QrBoletosSUB_LEI.Value;
  //
  if QrBoletosBoleto.Value = 0 then
    QrBoletosBLOQUETO.Value := -1
  else
    QrBoletosBLOQUETO.Value := QrBoletosBoleto.Value;
  //
  QrBoletosVENCTO_TXT.Value := Geral.FDT(QrBoletosVencto.Value, 3);
end;

procedure TDmBloqGer.QrBoletosItsCalcFields(DataSet: TDataSet);
var
  Texto: String;
begin
  if QrBoletosItsTipo.Value = 0 then Texto := '' else
  begin
   Texto := ' (' +
     Geral.FFT(QrBoletosItsMedAnt.Value,
       QrBoletosItsCasas.Value, siNegativo) + ' ' +
     ' > ' +
     Geral.FFT(QrBoletosItsMedAtu.Value,
       QrBoletosItsCasas.Value, siNegativo) + ' ' +
     ' = ' +
     Geral.FFT(QrBoletosItsConsumo.Value,
       QrBoletosItsCasas.Value, siNegativo) + ' ' +QrBoletosItsUnidLei.Value +
     ' ~ ' +
     Geral.FFT(QrBoletosItsConsumo.Value * QrBoletosItsUnidFat.VAlue,
       QrBoletosItsCasas.Value, siNegativo) + ' ' + QrBoletosItsUnidImp.Value + ')';
  end;
  //
  QrBoletosItsTEXTO_IMP.Value := QrBoletosItsTEXTO.Value + Texto;
  //
  QrBoletosItsVENCTO_TXT.Value := Geral.FDT(QrBoletosItsVencto.Value, 3);
end;

procedure TDmBloqGer.QrCNAB_CfgCalcFields(DataSet: TDataSet);
begin
  if QrCNAB_CfgAceiteTit.Value = 1 then
    QrCNAB_CfgACEITETIT_TXT.Value := 'S'
  else
    QrCNAB_CfgACEITETIT_TXT.Value := 'N';
end;

procedure TDmBloqGer.QrComposAAfterScroll(DataSet: TDataSet);
begin
  ReopenComposAIts(0);
end;

procedure TDmBloqGer.QrComposABeforeClose(DataSet: TDataSet);
begin
  QrComposAIts.Close;
end;

procedure TDmBloqGer.QrComposLAfterScroll(DataSet: TDataSet);
begin
  ReopenComposLIts(0);
end;

procedure TDmBloqGer.QrComposLBeforeClose(DataSet: TDataSet);
begin
  QrComposLIts.Close;
end;

procedure TDmBloqGer.QrInquilinoCalcFields(DataSet: TDataSet);
begin
  QrInquilinoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrInquilinoTe1.Value);
  QrInquilinoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrInquilinoFax.Value);
  QrInquilinoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrInquilinoCNPJ_CPF.Value);
  //
  QrInquilinoNUMERO_TXT.Value := QrInquilinoNumero.Value;
  QrInquilinoE_ALL.Value := QrInquilinoNOMELOGRAD.Value;
  if Trim(QrInquilinoE_ALL.Value) <> '' then QrInquilinoE_ALL.Value :=
    QrInquilinoE_ALL.Value + ' ';
  QrInquilinoE_ALL.Value := QrInquilinoE_ALL.Value + QrInquilinoRua.Value;
  if Trim(QrInquilinoRua.Value) <> '' then QrInquilinoE_ALL.Value :=
    QrInquilinoE_ALL.Value + ', ' + QrInquilinoNUMERO_TXT.Value;
  if Trim(QrInquilinoCompl.Value) <>  '' then QrInquilinoE_ALL.Value :=
    QrInquilinoE_ALL.Value + ' ' + QrInquilinoCompl.Value;
  if Trim(QrInquilinoBairro.Value) <>  '' then QrInquilinoE_ALL.Value :=
    QrInquilinoE_ALL.Value + ' - ' + QrInquilinoBairro.Value;
  if Trim(QrInquilinoCEP.Value) <> '' then QrInquilinoE_ALL.Value :=
    QrInquilinoE_ALL.Value + ' CEP ' + MLAGeral.FormataCEP_TT(QrInquilinoCEP.Value);
  if Trim(QrInquilinoCidade.Value) <>  '' then QrInquilinoE_ALL.Value :=
    QrInquilinoE_ALL.Value + ' - ' + QrInquilinoCidade.Value;
  //
  QrInquilinoECEP_TXT.Value := MLAGeral.FormataCEP_TT(QrInquilinoCEP.Value);
  //
  (*if QrInquilinoTipo.Value = 0 then Natal := QrInquilinoENatal.Value
  else Natal := QrInquilinoPNatal.Value;
  if Natal < 2 then QrInquilinoNATAL_TXT.Value := ''
  else QrInquilinoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);*)
end;

procedure TDmBloqGer.QrProprietCalcFields(DataSet: TDataSet);
begin
  QrProprietNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrProprietRUA.Value, QrProprietNumero.Value, False);
  QrProprietLNR.Value := '';//QrProprietNOMELOGRAD.Value;
  if Trim(QrProprietLNR.Value) <> '' then QrProprietLNR.Value :=
    QrProprietLNR.Value + ' ';
  QrProprietLNR.Value := QrProprietLNR.Value + QrProprietRua.Value;
  if Trim(QrProprietRua.Value) <> '' then QrProprietLNR.Value :=
    QrProprietLNR.Value + ', ' + QrProprietNUMERO_TXT.Value;
  if Trim(QrProprietCompl.Value) <>  '' then QrProprietLNR.Value :=
    QrProprietLNR.Value + ' ' + QrProprietCompl.Value;
  if Trim(QrProprietBairro.Value) <>  '' then QrProprietLNR.Value :=
    QrProprietLNR.Value + ' - ' + QrProprietBairro.Value;
  //
  QrProprietLN2.Value := '';
  if Trim(QrProprietCidade.Value) <>  '' then QrProprietLN2.Value :=
    QrProprietLN2.Value + QrProprietCIDADE.Value;
  QrProprietLN2.Value := QrProprietLN2.Value + ' - '+QrProprietNOMEUF.Value;
  if QrProprietCEP.Value > 0 then QrProprietLN2.Value :=
    QrProprietLN2.Value + '     CEP ' + Geral.FormataCEP_NT(QrProprietCEP.Value);
  //
  {QrProprietCUC.Value := QrProprietLNR.Value+ '   ' +QrProprietLN2.Value;
  //
  QrProprietCEP_TXT.Value := MLAGeral.FormataCEP_NT(QrProprietCEP.Value);}
  //
end;

procedure TDmBloqGer.ReopenArre(Entidade: Integer);
begin
  QrArre.Close;
  QrArre.Params[0].AsInteger := FPrevCodigo (*FmBloGeren.QrPrevCodigo.Value*);
  QrArre.Open;
  //
  if Entidade <> 0 then
    QrArre.Locate('Entidade', Entidade, []);
  //
  QrSumARRE.Close;
  QrSumARRE.Params[0].AsInteger := FPrevCodigo (*FmBloGeren.QrPrevCodigo.Value*);
  QrSumARRE.Open;
end;

procedure TDmBloqGer.ReopenArreBol(Boleto: Integer);
begin
  QrArreBol.Close;
  QrArreBol.Params[00].AsInteger  := FPrevCodigo (*FmBloGeren.QrPrevCodigo.Value*);
  QrArreBol.Params[01].AsInteger  := QrArreEntidade.Value;
  QrArreBol.Params[02].AsInteger  := QrArreCNAB_Cfg.Value;
  QrArreBol.Params[03].AsDateTime := QrArreVencto.Value;
  QrArreBol.Open;
  //
  if Boleto > 0 then
    QrArreBol.Locate('Boleto', Boleto, []);
end;

procedure TDmBloqGer.ReopenArreIts(Controle: Integer);
begin
  QrArreIts.Close;
  QrArreIts.Params[00].AsInteger  := FPrevCodigo (*FmBloGeren.QrPrevCodigo.Value*);
  QrArreIts.Params[01].AsInteger  := QrArreEntidade.Value;
  QrArreIts.Params[02].AsInteger  := QrArreCNAB_Cfg.Value;
  QrArreIts.Params[03].AsFloat    := QrArreBolBoleto.Value;
  QrArreIts.Params[04].AsDateTime := QrArreVencto.Value;
  QrArreIts.Open;
  if Controle > 0 then
    QrArreIts.Locate('Controle', Controle, []);
end;

procedure TDmBloqGer.ReopenBoletos(BOLAPTO: String);
  function TipoBol: String;
  begin
    if FEhPreBloqueto then
      Result := '='
    else
      Result := '<>';
  end;
begin
  QrBolArr.Close;
  QrBolArr.Params[00].AsInteger := FPrevCodigo; // QrPrevCodigo.Value;
  QrBolArr.Open;
  //
  QrBolLei.Close;
  QrBolLei.Params[00].AsInteger := FPrevCodigo; // QrPrevCondCod.Value;
  QrBolLei.Params[01].AsInteger := FPrevCodigo; // QrPrevPeriodo.Value;
  QrBolLei.Open;
  //
  QrBoletos.Close;
  QrBoletos.SQL.Clear;
  QrBoletos.SQL.Add('SELECT DISTINCT ari.Boleto, ari.Apto, ari.Propriet, ari.Vencto,');
  QrBoletos.SQL.Add('cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrBoletos.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
  QrBoletos.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO');
  QrBoletos.SQL.Add('FROM arreits ari');
  QrBoletos.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
  QrBoletos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
  QrBoletos.SQL.Add('WHERE ari.Codigo=:P0');
  QrBoletos.SQL.Add('AND ari.Boleto '+TipoBol+' 0');
  QrBoletos.SQL.Add('');
  QrBoletos.SQL.Add('UNION');
  QrBoletos.SQL.Add('');
  QrBoletos.SQL.Add('SELECT DISTINCT cni.Boleto, cni.Apto, cni.Propriet, cni.Vencto,');
  QrBoletos.SQL.Add('cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrBoletos.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
  QrBoletos.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO');
  QrBoletos.SQL.Add('FROM consits  cni');
  QrBoletos.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
  QrBoletos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
  QrBoletos.SQL.Add('WHERE cni.Cond=:P1');
  QrBoletos.SQL.Add('AND cni.Periodo=:P2');
  QrBoletos.SQL.Add('AND cni.Boleto '+TipoBol+' 0');
  QrBoletos.SQL.Add('');
  QrBoletos.SQL.Add('ORDER BY Unidade, Boleto');
  //
  QrBoletos.Params[00].AsInteger := FPrevCodigo; // QrPrevCodigo.Value;
  QrBoletos.Params[01].AsInteger := FPrevCond; // QrPrevCond.Value;
  QrBoletos.Params[02].AsInteger := FPrevPeriodo;// QrPrevPeriodo.Value;
  QrBoletos.Open;
  //
  if BOLAPTO <> '' then
    QrBoletos.Locate('BOLAPTO', BOLAPTO, [])
  else if not FEhPreBloqueto then
    QrBoletos.Locate('BOLAPTO', FBolSim, [])
  else
    QrBoletos.Locate('BOLAPTO', FBolNao, []);
  //

  //
  QrComposA.Close;
  QrComposA.Params[00].AsInteger := FPrevCodigo; // QrPrevCodigo.Value;
  QrComposA.Open;
  //
  QrComposL.Close;
  QrComposL.Params[00].AsInteger := FPrevCond; // QrPrevCond.Value;
  QrComposL.Params[01].AsInteger := FPrevCond; // QrPrevCond.Value;
  QrComposL.Params[02].AsInteger := FPrevPeriodo; // QrPrevPeriodo.Value;
  QrComposL.Open;
  //
end;

procedure TDmBloqGer.ReopenBoletosIts();
begin
  QrBoletosIts.Close;
  QrBoletosIts.Params[00].AsFloat   := QrBoletosBoleto.Value;
  QrBoletosIts.Params[01].AsInteger := QrBoletosApto.Value;
  QrBoletosIts.Params[02].AsInteger := FPrevCodigo; // QrPrevCodigo.Value;
  QrBoletosIts.Params[03].AsFloat   := QrBoletosBoleto.Value;
  QrBoletosIts.Params[04].AsInteger := QrBoletosApto.Value;
  QrBoletosIts.Params[05].AsInteger := FPrevPeriodo; // QrPrevPeriodo.Value;
  QrBoletosIts.Open;
  //
  if FEhPreBloqueto then
    QrBoletosIts.Locate('TEXTO', FBolItsSim, [])
  else
    QrBoletosIts.Locate('TEXTO', FBolItsNao, []);
end;

procedure TDmBloqGer.ReopenComposAIts(Apto: Integer);
begin
  QrComposAIts.Close;
  QrComposAIts.Params[00].AsInteger := FPrevCodigo; // QrPrevCodigo.Value;
  QrComposAIts.Params[01].AsInteger := QrComposAConta.Value;
  QrComposAIts.Params[02].AsInteger := QrComposAArreBaI.Value;
  QrComposAIts.Open;
  //
end;

procedure TDmBloqGer.ReopenComposLIts(Apto: Integer);
var
  FmtTxT: String;
begin
  FmtTxT := MLAGeral.FormataCasas(QrComposLCasas.Value);
  QrComposLItsMedAnt.DisplayFormat := FmtTxt;
  QrComposLItsMedAtu.DisplayFormat := FmtTxt;
  QrComposLItsConsumo.DisplayFormat := FmtTxt;
  //
  QrComposLIts.Close;
  QrComposLIts.Params[00].AsInteger := FPrevCond; // QrPrevCond.Value;
  QrComposLIts.Params[01].AsInteger := FPrevPeriodo; // QrPrevPeriodo.Value;
  QrComposLIts.Params[02].AsInteger := QrComposLCodigo.Value;
  QrComposLIts.Open;
  //
end;

procedure TDmBloqGer.ReopenEnderecoInquilino();
begin
  QrInquilino.Close;
  QrInquilino.Params[0].AsInteger := QrBoletosPropriet.Value;
  QrInquilino.Open;
end;

end.
