object FmBloGerenInadimpBloq: TFmBloGerenInadimpBloq
  Left = 339
  Top = 185
  Caption = 'BLO-GEREN-014 :: Boletos Inadimplentes'
  ClientHeight = 714
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 558
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 595
      Top = 56
      Width = 10
      Height = 502
      Align = alRight
      ExplicitLeft = 601
      ExplicitTop = 57
      ExplicitHeight = 448
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 87
        Height = 13
        Caption = 'Novo vencimento:'
      end
      object Label2: TLabel
        Left = 132
        Top = 8
        Width = 40
        Height = 13
        Caption = '% Multa:'
      end
      object Label3: TLabel
        Left = 216
        Top = 8
        Width = 69
        Height = 13
        Caption = '% Juros / m'#234's:'
      end
      object LaAviso: TLabel
        Left = 308
        Top = 28
        Width = 585
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TPVencto: TdmkEditDateTimePicker
        Left = 16
        Top = 24
        Width = 112
        Height = 21
        Date = 40440.407388738430000000
        Time = 40440.407388738430000000
        TabOrder = 0
        OnClick = TPVenctoClick
        OnChange = TPVenctoChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdMulta: TdmkEdit
        Left = 132
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2.000000000000000000
        ValWarn = False
        OnChange = EdMultaChange
      end
      object EdJuros: TdmkEdit
        Left = 216
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.000000000000000000
        ValWarn = False
        OnChange = EdJurosChange
      end
      object CkZerado: TCheckBox
        Left = 904
        Top = 10
        Width = 97
        Height = 17
        Caption = 'Sem val/vcto.'
        TabOrder = 3
        Visible = False
      end
      object CkDesign: TCheckBox
        Left = 904
        Top = 28
        Width = 97
        Height = 17
        Caption = 'Design'
        TabOrder = 4
        Visible = False
      end
    end
    object DBGBloq: TdmkDBGridDAC
      Left = 0
      Top = 56
      Width = 595
      Height = 502
      SQLFieldsToChange.Strings = (
        'Ativo')
      SQLIndexesOnUpdate.Strings = (
        'Controle'
        'FatNum')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Imp.'
          Width = 31
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENT'
          Title.Caption = 'Entidade'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Boleto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NewVencto'
          Title.Caption = 'Novo Vct.'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PEND_VAL'
          Title.Caption = '$ A pagar'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEZ_TXT'
          Title.Caption = 'M'#234's'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vct. Original'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CREDITO'
          Title.Caption = 'Val. Original'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PAGO'
          Title.Caption = 'Pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Multa'
          Title.Caption = '$ Multa'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Juros'
          Title.Caption = '$ Juros'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 173
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEEMP'
          Title.Caption = 'Empresa'
          Width = 228
          Visible = True
        end>
      Color = clWindow
      DataSource = DsBloqInad
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
      SQLTable = 'bloqinadcli'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Imp.'
          Width = 31
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENT'
          Title.Caption = 'Entidade'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Boleto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NewVencto'
          Title.Caption = 'Novo Vct.'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PEND_VAL'
          Title.Caption = '$ A pagar'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEZ_TXT'
          Title.Caption = 'M'#234's'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vct. Original'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CREDITO'
          Title.Caption = 'Val. Original'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PAGO'
          Title.Caption = 'Pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Multa'
          Title.Caption = '$ Multa'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Juros'
          Title.Caption = '$ Juros'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 173
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEEMP'
          Title.Caption = 'Empresa'
          Width = 228
          Visible = True
        end>
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 605
      Top = 56
      Width = 403
      Height = 502
      Align = alRight
      Columns = <
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'Conta'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Hist'#243'rico'
          Width = 257
          Visible = True
        end>
      Color = clWindow
      DataSource = DsBloqIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'Conta'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Hist'#243'rico'
          Width = 257
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 268
        Height = 32
        Caption = 'Boletos Inadimplentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 268
        Height = 32
        Caption = 'Boletos Inadimplentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 268
        Height = 32
        Caption = 'Boletos Inadimplentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 606
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 650
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 4
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtRecalcula: TBitBtn
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Recalcula'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtRecalculaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 144
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 338
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 462
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtNenhumClick
      end
    end
  end
  object QrBloqInad: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrBloqInadBeforeClose
    AfterScroll = QrBloqInadAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM bloqinadcli')
    Left = 284
    Top = 220
    object QrBloqInadData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBloqInadCREDITO: TFloatField
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadPAGO: TFloatField
      FieldName = 'PAGO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrBloqInadVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrBloqInadCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBloqInadControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBloqInadDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrBloqInadJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadMulta: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object QrBloqInadFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBloqInadPEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadMEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Required = True
      Size = 7
    end
    object QrBloqInadVCTO_TXT: TWideStringField
      FieldName = 'VCTO_TXT'
      Size = 10
    end
    object QrBloqInadCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrBloqInadNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrBloqInadNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrBloqInadAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'clientes.Ativo'
      MaxValue = 1
    end
    object QrBloqInadCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrBloqInadNewVencto: TDateField
      FieldName = 'NewVencto'
    end
  end
  object DsBloqInad: TDataSource
    DataSet = QrBloqInad
    Left = 312
    Top = 220
  end
  object frxDsBoletos: TfrxDBDataset
    UserName = 'frxDsBoletos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Entidade=Entidade'
      'Vencto=Vencto'
      'NOMEENT=NOMEENT'
      'BOLENT=BOLENT'
      'KGT=KGT'
      'VENCTO_TXT=VENCTO_TXT'
      'Valor=Valor'
      'CartEmiss=CartEmiss'
      'CAR_TIPODOC=CAR_TIPODOC'
      'CART_ATIVO=CART_ATIVO'
      'CedBanco=CedBanco'
      'JurosPerc=JurosPerc'
      'MultaPerc=MultaPerc'
      'CNAB_Cfg=CNAB_Cfg'
      'Empresa=Empresa'
      'Genero=Genero'
      'Texto=Texto'
      'Controle=Controle'
      'NOMECNAB_Cfg=NOMECNAB_Cfg'
      'Boleto=Boleto'
      'Protocolo=Protocolo'
      'Codigo=Codigo'
      'Avulso=Avulso'
      'Periodo=Periodo'
      'FatID=FatID'
      'FatNum=FatNum'
      'ModelBloq=ModelBloq'
      'Compe=Compe'
      'BloqFV=BloqFV'
      'ProtocoPak=ProtocoPak'
      'ProtocoPakCR=ProtocoPakCR'
      'Protocolo_TXT=Protocolo_TXT'
      'ProtocoloCR_TXT=ProtocoloCR_TXT'
      'Aviso01=Aviso01'
      'Aviso02=Aviso02'
      'Aviso03=Aviso03'
      'Aviso04=Aviso04'
      'Aviso05=Aviso05'
      'Aviso06=Aviso06'
      'Aviso07=Aviso07'
      'Aviso08=Aviso08'
      'Aviso09=Aviso09'
      'Aviso10=Aviso10'
      'AvisoVerso=AvisoVerso'
      'DataCad=DataCad'
      'Cedente=Cedente')
    DataSet = QrBoletos
    BCDToCurrency = False
    Left = 52
    Top = 284
  end
  object QrBoletos: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrBoletosBeforeClose
    AfterScroll = QrBoletosAfterScroll
    Left = 80
    Top = 284
    object QrBoletosEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'arreits.Entidade'
    end
    object QrBoletosVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'arreits.Vencto'
    end
    object QrBoletosNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrBoletosBOLENT: TWideStringField
      FieldName = 'BOLENT'
      Required = True
      Size = 65
    end
    object QrBoletosKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrBoletosVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Calculated = True
    end
    object QrBoletosValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBoletosCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Origin = 'arreits.CartEmiss'
    end
    object QrBoletosCAR_TIPODOC: TSmallintField
      FieldName = 'CAR_TIPODOC'
      Origin = 'carteiras.TipoDoc'
    end
    object QrBoletosCART_ATIVO: TSmallintField
      FieldName = 'CART_ATIVO'
      Origin = 'carteiras.Ativo'
    end
    object QrBoletosCedBanco: TIntegerField
      FieldName = 'CedBanco'
      Origin = 'cnab_cfg.CedBanco'
    end
    object QrBoletosJurosPerc: TFloatField
      FieldName = 'JurosPerc'
      Origin = 'cnab_cfg.JurosPerc'
    end
    object QrBoletosMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'cnab_cfg.MultaPerc'
    end
    object QrBoletosCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
      Origin = 'arreits.CNAB_Cfg'
    end
    object QrBoletosEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'prev.Empresa'
    end
    object QrBoletosGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'arreits.Conta'
    end
    object QrBoletosTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'arreits.Texto'
      Size = 50
    end
    object QrBoletosControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'arreits.Controle'
    end
    object QrBoletosNOMECNAB_Cfg: TWideStringField
      FieldName = 'NOMECNAB_Cfg'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrBoletosBoleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'arreits.Boleto'
    end
    object QrBoletosProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrBoletosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBoletosAvulso: TSmallintField
      FieldName = 'Avulso'
      MaxValue = 1
    end
    object QrBoletosPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBoletosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrBoletosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBoletosModelBloq: TIntegerField
      FieldName = 'ModelBloq'
    end
    object QrBoletosCompe: TSmallintField
      FieldName = 'Compe'
    end
    object QrBoletosBloqFV: TIntegerField
      FieldName = 'BloqFV'
    end
    object QrBoletosProtocoPak: TIntegerField
      FieldName = 'ProtocoPak'
    end
    object QrBoletosProtocoPakCR: TIntegerField
      FieldName = 'ProtocoPakCR'
    end
    object QrBoletosProtocolo_TXT: TWideStringField
      FieldName = 'Protocolo_TXT'
      Size = 100
    end
    object QrBoletosProtocoloCR_TXT: TWideStringField
      FieldName = 'ProtocoloCR_TXT'
      Size = 100
    end
    object QrBoletosAviso01: TWideStringField
      FieldName = 'Aviso01'
      Origin = 'prev.Aviso01'
      Size = 50
    end
    object QrBoletosAviso02: TWideStringField
      FieldName = 'Aviso02'
      Origin = 'prev.Aviso02'
      Size = 50
    end
    object QrBoletosAviso03: TWideStringField
      FieldName = 'Aviso03'
      Origin = 'prev.Aviso03'
      Size = 50
    end
    object QrBoletosAviso04: TWideStringField
      FieldName = 'Aviso04'
      Origin = 'prev.Aviso04'
      Size = 50
    end
    object QrBoletosAviso05: TWideStringField
      FieldName = 'Aviso05'
      Origin = 'prev.Aviso05'
      Size = 50
    end
    object QrBoletosAviso06: TWideStringField
      FieldName = 'Aviso06'
      Origin = 'prev.Aviso06'
      Size = 50
    end
    object QrBoletosAviso07: TWideStringField
      FieldName = 'Aviso07'
      Origin = 'prev.Aviso07'
      Size = 50
    end
    object QrBoletosAviso08: TWideStringField
      FieldName = 'Aviso08'
      Origin = 'prev.Aviso08'
      Size = 50
    end
    object QrBoletosAviso09: TWideStringField
      FieldName = 'Aviso09'
      Origin = 'prev.Aviso09'
      Size = 50
    end
    object QrBoletosAviso10: TWideStringField
      FieldName = 'Aviso10'
      Origin = 'prev.Aviso10'
      Size = 50
    end
    object QrBoletosAvisoVerso: TWideStringField
      FieldName = 'AvisoVerso'
      Origin = 'prev.AvisoVerso'
      Size = 180
    end
    object QrBoletosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBoletosCedente: TIntegerField
      FieldName = 'Cedente'
    end
  end
  object QrBloqIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 916
    Top = 156
    object QrBloqItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBloqItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrBloqItsCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsBloqIts: TDataSource
    DataSet = QrBloqIts
    Left = 944
    Top = 156
  end
  object frxDsBoletosIts: TfrxDBDataset
    UserName = 'frxDsBoletosIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TEXTO=TEXTO'
      'VALOR=VALOR'
      'Vencto=Vencto'
      'Controle=Controle'
      'Lancto=Lancto'
      'CNAB_Cfg=CNAB_Cfg'
      'Entidade=Entidade'
      'Avulso=Avulso'
      'Boleto=Boleto'
      'FatNum=FatNum'
      'ProtocoPak=ProtocoPak'
      'ProtocoPakCR=ProtocoPakCR')
    DataSet = QrBoletosIts
    BCDToCurrency = False
    Left = 108
    Top = 212
  end
  object QrBoletosIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 136
    Top = 212
    object QrBoletosItsTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Size = 50
    end
    object QrBoletosItsVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBoletosItsVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrBoletosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrBoletosItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrBoletosItsCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrBoletosItsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrBoletosItsAvulso: TIntegerField
      FieldName = 'Avulso'
    end
    object QrBoletosItsBoleto: TFloatField
      FieldName = 'Boleto'
    end
    object QrBoletosItsFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBoletosItsProtocoPak: TIntegerField
      FieldName = 'ProtocoPak'
    end
    object QrBoletosItsProtocoPakCR: TIntegerField
      FieldName = 'ProtocoPakCR'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 448
    Top = 11
  end
end
