object FmBloGerenInadimpBloq: TFmBloGerenInadimpBloq
  Left = 339
  Top = 185
  Caption = 'BLO-GEREN-014 :: Boletos Inadimplentes'
  ClientHeight = 714
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 558
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 595
      Top = 56
      Width = 10
      Height = 502
      Align = alRight
      ExplicitLeft = 601
      ExplicitTop = 57
      ExplicitHeight = 448
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 87
        Height = 13
        Caption = 'Novo vencimento:'
      end
      object Label2: TLabel
        Left = 132
        Top = 8
        Width = 40
        Height = 13
        Caption = '% Multa:'
      end
      object Label3: TLabel
        Left = 216
        Top = 8
        Width = 69
        Height = 13
        Caption = '% Juros / m'#234's:'
      end
      object LaAviso: TLabel
        Left = 308
        Top = 28
        Width = 585
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TPVencto: TdmkEditDateTimePicker
        Left = 16
        Top = 24
        Width = 112
        Height = 21
        Date = 40440.407388738430000000
        Time = 40440.407388738430000000
        TabOrder = 0
        OnClick = TPVenctoClick
        OnChange = TPVenctoChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdMulta: TdmkEdit
        Left = 132
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2.000000000000000000
        ValWarn = False
        OnChange = EdMultaChange
      end
      object EdJuros: TdmkEdit
        Left = 216
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.000000000000000000
        ValWarn = False
        OnChange = EdJurosChange
      end
      object CkZerado: TCheckBox
        Left = 904
        Top = 10
        Width = 97
        Height = 17
        Caption = 'Sem val/vcto.'
        TabOrder = 3
        Visible = False
      end
      object CkDesign: TCheckBox
        Left = 904
        Top = 28
        Width = 97
        Height = 17
        Caption = 'Design'
        TabOrder = 4
        Visible = False
      end
    end
    object DBGBloq: TdmkDBGridDAC
      Left = 0
      Top = 56
      Width = 595
      Height = 502
      SQLFieldsToChange.Strings = (
        'Ativo')
      SQLIndexesOnUpdate.Strings = (
        'Controle'
        'FatNum')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Imp.'
          Width = 31
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENT'
          Title.Caption = 'Entidade'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Boleto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vct. Original'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEZ_TXT'
          Title.Caption = 'M'#234's'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CREDITO'
          Title.Caption = 'Val. Original'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PAGO'
          Title.Caption = 'Pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NewVencto'
          Title.Caption = 'Novo Vct.'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Multa'
          Title.Caption = '$ Multa'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Juros'
          Title.Caption = '$ Juros'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PEND_VAL'
          Title.Caption = '$ A pagar'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 173
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEEMP'
          Title.Caption = 'Empresa'
          Width = 228
          Visible = True
        end>
      Color = clWindow
      DataSource = DsBloqInad
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
      SQLTable = 'bloqinadcli'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Imp.'
          Width = 31
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENT'
          Title.Caption = 'Entidade'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Boleto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vct. Original'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEZ_TXT'
          Title.Caption = 'M'#234's'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CREDITO'
          Title.Caption = 'Val. Original'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PAGO'
          Title.Caption = 'Pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NewVencto'
          Title.Caption = 'Novo Vct.'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Multa'
          Title.Caption = '$ Multa'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Juros'
          Title.Caption = '$ Juros'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PEND_VAL'
          Title.Caption = '$ A pagar'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 173
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEEMP'
          Title.Caption = 'Empresa'
          Width = 228
          Visible = True
        end>
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 605
      Top = 56
      Width = 403
      Height = 502
      Align = alRight
      Columns = <
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'Conta'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Hist'#243'rico'
          Width = 257
          Visible = True
        end>
      Color = clWindow
      DataSource = DsBloqIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'Conta'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Hist'#243'rico'
          Width = 257
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 268
        Height = 32
        Caption = 'Boletos Inadimplentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 268
        Height = 32
        Caption = 'Boletos Inadimplentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 268
        Height = 32
        Caption = 'Boletos Inadimplentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 606
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 368
        Height = 16
        Caption = 'AVISO: Para boletos com registro n'#227'o emita 2'#170' via nesta janela!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 368
        Height = 16
        Caption = 'AVISO: Para boletos com registro n'#227'o emita 2'#170' via nesta janela!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 650
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 4
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtRecalcula: TBitBtn
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Recalcula'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtRecalculaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 144
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 338
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 462
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtNenhumClick
      end
    end
  end
  object QrBloqInad: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrBloqInadBeforeClose
    AfterScroll = QrBloqInadAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM bloqinadcli')
    Left = 284
    Top = 220
    object QrBloqInadData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBloqInadCREDITO: TFloatField
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadPAGO: TFloatField
      FieldName = 'PAGO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrBloqInadVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrBloqInadCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBloqInadControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBloqInadDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrBloqInadJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadMulta: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object QrBloqInadFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBloqInadPEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadMEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Required = True
      Size = 7
    end
    object QrBloqInadVCTO_TXT: TWideStringField
      FieldName = 'VCTO_TXT'
      Size = 10
    end
    object QrBloqInadCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrBloqInadNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrBloqInadNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrBloqInadAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'clientes.Ativo'
      MaxValue = 1
    end
    object QrBloqInadCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrBloqInadNewVencto: TDateField
      FieldName = 'NewVencto'
    end
  end
  object DsBloqInad: TDataSource
    DataSet = QrBloqInad
    Left = 312
    Top = 220
  end
  object frxCondE2: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 40897.760974201390000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoBancoExiste> = True then'
      '  begin'
      '    Picture1.LoadFromFile(<LogoBancoPath>);'
      
        '    Picture2.LoadFromFile(<LogoBancoPath>);                     ' +
        '                                                        '
      '    MeLogo1.Visible := False;'
      '    MeLogo2.Visible := False;'
      '  end else begin           '
      '    MeLogo1.Visible := True;'
      '    MeLogo2.Visible := True;'
      '  end;              '
      'end.')
    OnGetValue = frxCondE2GetValue
    Left = 20
    Top = 164
    Datasets = <
      item
        DataSet = frxDsBoletos
        DataSetName = 'frxDsBoletos'
      end
      item
        DataSet = frxDsBoletosIts
        DataSetName = 'frxDsBoletosIts'
      end
      item
        DataSet = frxDsCNABCfg
        DataSetName = 'frxDsCNABCfg'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Width = 0.100000000000000000
      object Memo129: TfrxMemoView
        Left = 56.692950000000000000
        Top = 904.063390000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO3]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo114: TfrxMemoView
        Left = 602.834645670000000000
        Top = 848.126382130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."SUB_TOT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Picture2: TfrxPictureView
        Left = 52.913420000000000000
        Top = 718.110700000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo113: TfrxMemoView
        Left = 602.834645670000000000
        Top = 768.756303390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo101: TfrxMemoView
        Left = 602.834645670000000000
        Top = 839.055660000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo85: TfrxMemoView
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line13: TfrxLineView
        Left = 3.779530000000000000
        Top = 718.110700000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Style = fsDash
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo81: TfrxMemoView
        Left = 578.268090000000000000
        Top = 706.772110000000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'AUTENTICA'#199#195'O MEC'#194'NICA')
        ParentFont = False
      end
      object BarCode1: TfrxBarCodeView
        Left = 90.708661417322800000
        Top = 1058.268114410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Rotation = 0
        ShowText = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Line14: TfrxLineView
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 687.874015750000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 52.913420000000000000
        Top = 791.811419060000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 226.771653540000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 294.803152050000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Height = 230.551330000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 430.866420000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 370.393940000000000000
        Top = 816.378480000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line23: TfrxLineView
        Left = 291.023810000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 228.661417320000000000
        Top = 839.055660000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 160.629921260000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 52.913420000000000000
        Top = 865.512370000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line27: TfrxLineView
        Left = 52.913420000000000000
        Top = 990.236615910000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line28: TfrxLineView
        Left = 52.913420000000000000
        Top = 1050.709056850000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo82: TfrxMemoView
        Left = 226.771800000000000000
        Top = 737.008350000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo83: TfrxMemoView
        Left = 294.803340000000000000
        Top = 737.008350000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo84: TfrxMemoView
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo86: TfrxMemoView
        Left = 52.913420000000000000
        Top = 793.701300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo88: TfrxMemoView
        Left = 56.692950000000000000
        Top = 771.024120000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNABCfg."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo89: TfrxMemoView
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo90: TfrxMemoView
        Left = 166.299320000000000000
        Top = 816.378480000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo91: TfrxMemoView
        Left = 294.803340000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo92: TfrxMemoView
        Left = 374.173470000000000000
        Top = 816.378480000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo93: TfrxMemoView
        Left = 434.645950000000000000
        Top = 816.378480000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo94: TfrxMemoView
        Left = 166.299320000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo95: TfrxMemoView
        Left = 230.551330000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo96: TfrxMemoView
        Left = 294.803340000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo97: TfrxMemoView
        Left = 434.645950000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo98: TfrxMemoView
        Left = 427.086890000000000000
        Top = 848.504334880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Fill.BackColor = clWhite
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo99: TfrxMemoView
        Left = 604.724800000000000000
        Top = 793.701300000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo100: TfrxMemoView
        Left = 604.724800000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo102: TfrxMemoView
        Left = 604.724800000000000000
        Top = 865.512370000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo103: TfrxMemoView
        Left = 604.724800000000000000
        Top = 891.969080000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo104: TfrxMemoView
        Left = 604.724800000000000000
        Top = 918.425790000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo105: TfrxMemoView
        Left = 604.724800000000000000
        Top = 941.102970000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo106: TfrxMemoView
        Left = 604.724800000000000000
        Top = 967.559680000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line29: TfrxLineView
        Left = 602.834645670000000000
        Top = 890.079135590000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line30: TfrxLineView
        Left = 602.834645670000000000
        Top = 914.646064720000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line31: TfrxLineView
        Left = 602.834645670000000000
        Top = 939.212993860000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line32: TfrxLineView
        Left = 602.834645670000000000
        Top = 963.779922990000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo109: TfrxMemoView
        Left = 589.606680000000000000
        Top = 1054.488870000000000000
        Width = 151.181200000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo110: TfrxMemoView
        Left = 619.842920000000000000
        Top = 1035.591220000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo111: TfrxMemoView
        Left = 52.913420000000000000
        Top = 866.268114410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo115: TfrxMemoView
        Left = 56.692950000000000000
        Top = 801.260360000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNABCfg."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo116: TfrxMemoView
        Left = 56.692950000000000000
        Top = 825.449216770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo117: TfrxMemoView
        Left = 162.519790000000000000
        Top = 825.449216770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."Boleto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo118: TfrxMemoView
        Left = 294.803340000000000000
        Top = 825.449216770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNABCfg."EspecieTit"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo119: TfrxMemoView
        Left = 374.173470000000000000
        Top = 825.449216770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNABCfg."ACEITETIT_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo120: TfrxMemoView
        Left = 434.645950000000000000
        Top = 825.449216770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo122: TfrxMemoView
        Left = 166.299320000000000000
        Top = 848.126382130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNABCfg."CartNum"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo123: TfrxMemoView
        Left = 230.551330000000000000
        Top = 848.126382130000000000
        Width = 56.692950000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNABCfg."EspecieVal"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo124: TfrxMemoView
        Left = 604.724800000000000000
        Top = 825.449216770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo127: TfrxMemoView
        Left = 56.692950000000000000
        Top = 875.338980550000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO1]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo128: TfrxMemoView
        Left = 56.692950000000000000
        Top = 889.701185280000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo130: TfrxMemoView
        Left = 56.692950000000000000
        Top = 918.425790000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO4]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo131: TfrxMemoView
        Left = 56.692950000000000000
        Top = 932.787799450000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO5]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo132: TfrxMemoView
        Left = 56.692950000000000000
        Top = 947.150004170000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO6]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo133: TfrxMemoView
        Left = 56.692950000000000000
        Top = 961.512208900000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO7]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo134: TfrxMemoView
        Left = 56.692950000000000000
        Top = 975.874413620000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO8]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo121: TfrxMemoView
        Left = 604.724800000000000000
        Top = 801.260360000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_AGCodCed]')
        ParentFont = False
        VAlign = vaBottom
      end
      object MeLogo2: TfrxMemoView
        Left = 52.913420000000000000
        Top = 729.449290000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNABCfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo1: TfrxMemoView
        Left = 56.692950000000000000
        Top = 480.756030000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO3]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo2: TfrxMemoView
        Left = 602.834645670000000000
        Top = 424.819022130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."SUB_TOT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Picture1: TfrxPictureView
        Left = 52.913420000000000000
        Top = 294.803340000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo3: TfrxMemoView
        Left = 602.834645670000000000
        Top = 345.448943390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo4: TfrxMemoView
        Left = 602.834645670000000000
        Top = 415.748300000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo5: TfrxMemoView
        Left = 602.834645670000000000
        Top = 336.378170000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line1: TfrxLineView
        Left = 52.913420000000000000
        Top = 294.803340000000000000
        Width = 691.653990000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object BarCode2: TfrxBarCodeView
        Left = 90.708661420000000000
        Top = 634.960754410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Rotation = 0
        ShowText = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Line2: TfrxLineView
        Left = 52.913420000000000000
        Top = 336.378170000000000000
        Width = 687.874015750000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line3: TfrxLineView
        Left = 52.913420000000000000
        Top = 368.504059060000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line4: TfrxLineView
        Left = 52.913420000000000000
        Top = 393.071120000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line5: TfrxLineView
        Left = 52.913420000000000000
        Top = 415.748300000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line6: TfrxLineView
        Left = 226.771653540000000000
        Top = 313.700990000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line7: TfrxLineView
        Left = 294.803152050000000000
        Top = 313.700990000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line8: TfrxLineView
        Left = 602.834645670000000000
        Top = 336.378170000000000000
        Height = 230.551330000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line9: TfrxLineView
        Left = 430.866420000000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line10: TfrxLineView
        Left = 370.393940000000000000
        Top = 393.071120000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line11: TfrxLineView
        Left = 291.023810000000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line12: TfrxLineView
        Left = 228.661417320000000000
        Top = 415.748300000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line33: TfrxLineView
        Left = 160.629921260000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line34: TfrxLineView
        Left = 52.913420000000000000
        Top = 442.205010000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line35: TfrxLineView
        Left = 52.913420000000000000
        Top = 566.929255910000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line36: TfrxLineView
        Left = 52.913420000000000000
        Top = 627.401696850000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo7: TfrxMemoView
        Left = 226.771800000000000000
        Top = 313.700990000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo8: TfrxMemoView
        Left = 294.803340000000000000
        Top = 313.700990000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo9: TfrxMemoView
        Left = 52.913420000000000000
        Top = 336.378170000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo11: TfrxMemoView
        Left = 52.913420000000000000
        Top = 370.393940000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo12: TfrxMemoView
        Left = 52.913420000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        Left = 56.692950000000000000
        Top = 347.716760000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNABCfg."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo14: TfrxMemoView
        Left = 52.913420000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo15: TfrxMemoView
        Left = 166.299320000000000000
        Top = 393.071120000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo16: TfrxMemoView
        Left = 294.803340000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        Left = 374.173470000000000000
        Top = 393.071120000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo18: TfrxMemoView
        Left = 434.645950000000000000
        Top = 393.071120000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo19: TfrxMemoView
        Left = 166.299320000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo20: TfrxMemoView
        Left = 230.551330000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo21: TfrxMemoView
        Left = 294.803340000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo22: TfrxMemoView
        Left = 434.645950000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo23: TfrxMemoView
        Left = 427.086890000000000000
        Top = 425.196974880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Fill.BackColor = clWhite
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo24: TfrxMemoView
        Left = 604.724800000000000000
        Top = 370.393940000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo25: TfrxMemoView
        Left = 604.724800000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo26: TfrxMemoView
        Left = 604.724800000000000000
        Top = 442.205010000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo27: TfrxMemoView
        Left = 604.724800000000000000
        Top = 468.661720000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo28: TfrxMemoView
        Left = 604.724800000000000000
        Top = 495.118430000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo29: TfrxMemoView
        Left = 604.724800000000000000
        Top = 517.795610000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo30: TfrxMemoView
        Left = 604.724800000000000000
        Top = 544.252320000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line37: TfrxLineView
        Left = 602.834645670000000000
        Top = 466.771775590000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line38: TfrxLineView
        Left = 602.834645670000000000
        Top = 491.338704720000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line39: TfrxLineView
        Left = 602.834645670000000000
        Top = 515.905633860000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line40: TfrxLineView
        Left = 602.834645670000000000
        Top = 540.472562990000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo33: TfrxMemoView
        Left = 589.606680000000000000
        Top = 631.181510000000000000
        Width = 151.181200000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo35: TfrxMemoView
        Left = 52.913420000000000000
        Top = 442.960754410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo36: TfrxMemoView
        Left = 56.692950000000000000
        Top = 377.953000000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNABCfg."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo37: TfrxMemoView
        Left = 56.692950000000000000
        Top = 402.141856770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo38: TfrxMemoView
        Left = 162.519790000000000000
        Top = 402.141856770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."Boleto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo39: TfrxMemoView
        Left = 294.803340000000000000
        Top = 402.141856770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNABCfg."EspecieTit"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo40: TfrxMemoView
        Left = 374.173470000000000000
        Top = 402.141856770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNABCfg."ACEITETIT_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo41: TfrxMemoView
        Left = 434.645950000000000000
        Top = 402.141856770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo42: TfrxMemoView
        Left = 166.299320000000000000
        Top = 424.819022130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNABCfg."CartNum"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo43: TfrxMemoView
        Left = 230.551330000000000000
        Top = 424.819022130000000000
        Width = 56.692950000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNABCfg."EspecieVal"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo44: TfrxMemoView
        Left = 604.724800000000000000
        Top = 402.141856770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo46: TfrxMemoView
        Left = 56.692950000000000000
        Top = 452.031620550000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO1]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo47: TfrxMemoView
        Left = 56.692950000000000000
        Top = 466.393825280000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo48: TfrxMemoView
        Left = 56.692950000000000000
        Top = 495.118430000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO4]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo49: TfrxMemoView
        Left = 56.692950000000000000
        Top = 509.480439450000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO5]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo50: TfrxMemoView
        Left = 56.692950000000000000
        Top = 523.842644170000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO6]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo51: TfrxMemoView
        Left = 56.692950000000000000
        Top = 538.204848900000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO7]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo52: TfrxMemoView
        Left = 56.692950000000000000
        Top = 552.567053620000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO8]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo53: TfrxMemoView
        Left = 604.724800000000000000
        Top = 377.953000000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_AGCodCed]')
        ParentFont = False
        VAlign = vaBottom
      end
      object MeLogo1: TfrxMemoView
        Left = 52.913420000000000000
        Top = 306.141930000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNABCfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo_049: TfrxMemoView
        Left = 170.078850000000000000
        Top = 990.236860000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo31: TfrxMemoView
        Left = 619.842920000000000000
        Top = 612.283860000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo45: TfrxMemoView
        Left = 170.078850000000000000
        Top = 566.929500000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 37.795275590000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object Shape_001: TfrxShapeView
          Left = 52.913420000000000000
          Top = 37.795275590000000000
          Width = 345.826771650000000000
          Height = 257.007610390000000000
          Frame.Width = 0.500000000000000000
        end
        object Shape_002: TfrxShapeView
          Left = 398.740157480000000000
          Top = 37.795300000000000000
          Width = 345.826771650000000000
          Height = 257.007610390000000000
          Frame.Width = 0.500000000000000000
        end
        object Memo58: TfrxMemoView
          Left = 52.913420000000000000
          Top = 22.677165350000000000
          Width = 691.653543310000000000
          Height = 14.362204720000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'SEGUNDA VIA RECALCULADA DE BOLETO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Memo6: TfrxMemoView
        Left = 578.268090000000000000
        Top = 279.685220000000000000
        Width = 162.519790000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          'SEGUNDA VIA')
        ParentFont = False
      end
      object Memo32: TfrxMemoView
        Left = 170.078850000000000000
        Top = 597.165740000000000000
        Width = 449.764070000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."E_ALL"]')
        ParentFont = False
      end
      object Memo55: TfrxMemoView
        Left = 170.078850000000000000
        Top = 582.047620000000000000
        Width = 449.764070000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."NOME_ENT"]')
        ParentFont = False
        WordWrap = False
      end
      object Memo34: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1020.473100000000000000
        Width = 449.764070000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."E_ALL"]')
        ParentFont = False
      end
      object Memo56: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1005.354980000000000000
        Width = 449.764070000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."NOME_ENT"]')
        ParentFont = False
        WordWrap = False
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 78.000000000000000000
      PaperSize = 256
      Columns = 2
      ColumnWidth = 91.500000000000000000
      ColumnPositions.Strings = (
        '0'
        '91,50')
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 56.692950000000000000
        Width = 345.826995000000000000
        DataSet = frxDsBoletosIts
        DataSetName = 'frxDsBoletosIts'
        RowCount = 0
        object Memo60: TfrxMemoView
          Left = 52.913420000000000000
          Width = 279.685049130000000000
          Height = 15.118110240000000000
          DataField = 'Descricao'
          DataSet = frxDsBoletosIts
          DataSetName = 'frxDsBoletosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsBoletosIts."Descricao"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 332.598640000000000000
          Width = 66.141732280000000000
          Height = 15.118110240000000000
          DataField = 'Credito'
          DataSet = frxDsBoletosIts
          DataSetName = 'frxDsBoletosIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletosIts."Credito"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 18.897650000000000000
        Width = 345.826995000000000000
        object Memo62: TfrxMemoView
          Left = 52.913420000000000000
          Width = 345.826771650000000000
          Height = 15.118110240000000000
          DataSet = frxDsBoletosIts
          DataSetName = 'frxDsBoletosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'COMPOSI'#199#195'O DA ARRECADA'#199#195'O ORIGINAL - [frxDsBoletos."MEZ_TXT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 94.488250000000000000
        Width = 345.826995000000000000
        object Memo63: TfrxMemoView
          Left = 52.913420000000000000
          Width = 279.685049130000000000
          Height = 15.118110240000000000
          DataSet = frxDsBoletosIts
          DataSetName = 'frxDsBoletosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL DO VALOR ORIGINAL')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 332.598640000000000000
          Width = 66.141732280000000000
          Height = 15.118110240000000000
          DataSet = frxDsBoletosIts
          DataSetName = 'frxDsBoletosIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBoletosIts."Credito">,MasterData1)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsBoletos: TfrxDBDataset
    UserName = 'frxDsBoletos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SUB_TOT=SUB_TOT'
      'Vencto=Vencto'
      'Boleto=Boleto'
      'BLOQUETO=BLOQUETO'
      'MEZ_TXT=MEZ_TXT'
      'Mez=Mez'
      'FatNum=FatNum'
      'Vencimento=Vencimento'
      'Cliente=Cliente'
      'Controle=Controle')
    DataSet = QrBoletos
    BCDToCurrency = False
    Left = 20
    Top = 220
  end
  object QrBoletos: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterScroll = QrBoletosAfterScroll
    OnCalcFields = QrBoletosCalcFields
    SQL.Strings = (
      'SELECT PEND_VAL SUB_TOT, NewVencto Vencto,'
      'FatNum Boleto, Cliente, MEZ_TXT, FatNum,'
      'Mez, Vencimento, Controle'
      'FROM bloqinadcli'
      'WHERE Ativo = 1')
    Left = 48
    Top = 220
    object QrBoletosSUB_TOT: TFloatField
      FieldName = 'SUB_TOT'
    end
    object QrBoletosVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrBoletosBoleto: TFloatField
      FieldName = 'Boleto'
    end
    object QrBoletosBLOQUETO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'BLOQUETO'
      Calculated = True
    end
    object QrBoletosMEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Size = 7
    end
    object QrBoletosMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrBoletosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBoletosVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrBoletosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrBoletosControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object frxBloq: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39911.688433344920000000
    ReportOptions.LastChange = 39911.688433344920000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 48
    Top = 164
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object QrBloqIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 916
    Top = 156
    object QrBloqItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBloqItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrBloqItsCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsBloqIts: TDataSource
    DataSet = QrBloqIts
    Left = 944
    Top = 156
  end
  object frxDsBoletosIts: TfrxDBDataset
    UserName = 'frxDsBoletosIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Genero=Genero'
      'Descricao=Descricao'
      'Credito=Credito')
    DataSet = QrBoletosIts
    BCDToCurrency = False
    Left = 20
    Top = 324
  end
  object QrBoletosIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 48
    Top = 324
    object QrBoletosItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBoletosItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrBoletosItsCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrCNABCfg: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCNABCfgCalcFields
    SQL.Strings = (
      'SELECT bco.Nome NOMEBANCO, bco.DVB,  cfg.*,'
      
        'IF(cfg.Cedente=0,"",IF(ced.Tipo=0,ced.RazaoSocial,ced.Nome)) NOM' +
        'ECED_IMP'
      'FROM arreits its'
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = CNAB_Cfg'
      'LEFT JOIN bancos bco ON bco.Codigo=cfg.CedBanco'
      'LEFT JOIN entidades ced ON ced.Codigo=cfg.Cedente'
      'WHERE its.Lancto=:P0'
      'GROUP BY its.Lancto')
    Left = 48
    Top = 273
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNABCfgCedBanco: TIntegerField
      FieldName = 'CedBanco'
    end
    object QrCNABCfgCedConta: TWideStringField
      FieldName = 'CedConta'
      Size = 30
    end
    object QrCNABCfgMoedaCod: TWideStringField
      FieldName = 'MoedaCod'
    end
    object QrCNABCfgCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
    end
    object QrCNABCfgCedPosto: TIntegerField
      FieldName = 'CedPosto'
    end
    object QrCNABCfgCartNum: TWideStringField
      FieldName = 'CartNum'
      Size = 3
    end
    object QrCNABCfgIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Size = 2
    end
    object QrCNABCfgCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
    end
    object QrCNABCfgTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
    end
    object QrCNABCfgTipoCart: TSmallintField
      FieldName = 'TipoCart'
    end
    object QrCNABCfgCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Size = 1
    end
    object QrCNABCfgCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Size = 1
    end
    object QrCNABCfgACEITETIT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ACEITETIT_TXT'
      Size = 1
      Calculated = True
    end
    object QrCNABCfgOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Size = 3
    end
    object QrCNABCfgDVB: TWideStringField
      FieldName = 'DVB'
      Size = 1
    end
    object QrCNABCfgTexto01: TWideStringField
      FieldName = 'Texto01'
      Size = 100
    end
    object QrCNABCfgTexto02: TWideStringField
      FieldName = 'Texto02'
      Size = 100
    end
    object QrCNABCfgTexto03: TWideStringField
      FieldName = 'Texto03'
      Size = 100
    end
    object QrCNABCfgTexto04: TWideStringField
      FieldName = 'Texto04'
      Size = 100
    end
    object QrCNABCfgTexto05: TWideStringField
      FieldName = 'Texto05'
      Size = 100
    end
    object QrCNABCfgTexto06: TWideStringField
      FieldName = 'Texto06'
      Size = 100
    end
    object QrCNABCfgTexto07: TWideStringField
      FieldName = 'Texto07'
      Size = 100
    end
    object QrCNABCfgTexto08: TWideStringField
      FieldName = 'Texto08'
      Size = 100
    end
    object QrCNABCfgJurosPerc: TFloatField
      FieldName = 'JurosPerc'
    end
    object QrCNABCfgMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrCNABCfgNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCNABCfgLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Size = 127
    end
    object QrCNABCfgNOMECED_IMP: TWideStringField
      FieldName = 'NOMECED_IMP'
      Size = 100
    end
    object QrCNABCfgEspecieTit: TWideStringField
      FieldName = 'EspecieTit'
      Size = 6
    end
    object QrCNABCfgAceiteTit: TSmallintField
      FieldName = 'AceiteTit'
    end
    object QrCNABCfgEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Size = 5
    end
    object QrCNABCfgEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
    end
    object QrCNABCfgCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrCNABCfgCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrCNABCfgLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrCNABCfgModalCobr: TIntegerField
      FieldName = 'ModalCobr'
    end
    object QrCNABCfgCorresBco: TIntegerField
      FieldName = 'CorresBco'
    end
    object QrCNABCfgCorresAge: TIntegerField
      FieldName = 'CorresAge'
    end
    object QrCNABCfgCorresCto: TWideStringField
      FieldName = 'CorresCto'
      Size = 30
    end
    object QrCNABCfgCartCod: TWideStringField
      FieldName = 'CartCod'
      Origin = 'cnab_cfg.CartCod'
      Size = 3
    end
    object QrCNABCfgNPrinBc: TWideStringField
      FieldName = 'NPrinBc'
      Size = 30
    end
  end
  object frxDsCNABCfg: TfrxDBDataset
    UserName = 'frxDsCNABCfg'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CedBanco=CedBanco'
      'CedConta=CedConta'
      'MoedaCod=MoedaCod'
      'CedAgencia=CedAgencia'
      'CedPosto=CedPosto'
      'CartNum=CartNum'
      'IDCobranca=IDCobranca'
      'CodEmprBco=CodEmprBco'
      'TipoCobranca=TipoCobranca'
      'TipoCart=TipoCart'
      'CedDAC_A=CedDAC_A'
      'CedDAC_C=CedDAC_C'
      'ACEITETIT_TXT=ACEITETIT_TXT'
      'OperCodi=OperCodi'
      'DVB=DVB'
      'Texto01=Texto01'
      'Texto02=Texto02'
      'Texto03=Texto03'
      'Texto04=Texto04'
      'Texto05=Texto05'
      'Texto06=Texto06'
      'Texto07=Texto07'
      'Texto08=Texto08'
      'JurosPerc=JurosPerc'
      'MultaPerc=MultaPerc'
      'NOMEBANCO=NOMEBANCO'
      'LocalPag=LocalPag'
      'NOMECED_IMP=NOMECED_IMP'
      'EspecieTit=EspecieTit'
      'AceiteTit=AceiteTit'
      'EspecieVal=EspecieVal'
      'EspecieDoc=EspecieDoc'
      'CtaCooper=CtaCooper'
      'CNAB=CNAB'
      'LayoutRem=LayoutRem'
      'ModalCobr=ModalCobr'
      'CorresBco=CorresBco'
      'CorresAge=CorresAge'
      'CorresCto=CorresCto'
      'CartCod=CartCod'
      'NPrinBc=NPrinBc')
    DataSet = QrCNABCfg
    BCDToCurrency = False
    Left = 20
    Top = 272
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 448
    Top = 11
  end
  object QrBloOpcoes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM bloopcoes')
    Left = 167
    Top = 323
  end
end
