unit BloGerenNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, ComCtrls, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, dmkGeral, Variants,
  dmkImage, dmkPermissoes, UnBloquetos, DmkDAC_PF, UnDmkEnums, UnDmkProcFunc,
  Vcl.Grids, Vcl.DBGrids, UnBloqGerl;

type
  TFmBloGerenNew = class(TForm)
    Panel1: TPanel;
    QrPesq: TmySQLQuery;
    QrPesqPeriodo: TIntegerField;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet1: TTabSheet;
    Panel4: TPanel;
    TabSheet2: TTabSheet;
    TabSheet4: TTabSheet;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label32: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    LaAnoI: TLabel;
    RGModelBloq: TRadioGroup;
    RGCompe: TRadioGroup;
    RGBloqFV: TRadioGroup;
    Label11: TLabel;
    Label12: TLabel;
    Edit9: TEdit;
    Edit10: TEdit;
    Edit8: TEdit;
    Edit7: TEdit;
    Edit6: TEdit;
    Edit5: TEdit;
    Edit4: TEdit;
    Edit3: TEdit;
    Edit2: TEdit;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label27: TLabel;
    Edit0: TEdit;
    Edit11: TEdit;
    MeAvisoVerso: TMemo;
    Label15: TLabel;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGModelBloqClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Tipo: TSQLType; Modo: TModoVisualizacaoBloquetos);
  public
    { Public declarations }
    F_CliInt, FPeriodo: Integer;
    FMes, FAno: Word;
    FQueryPrev, FQueryBoletos: TmySQLQuery;
    FDBGrid: TDBGrid;
    FModo: TModoVisualizacaoBloquetos;
  end;

  var
  FmBloGerenNew: TFmBloGerenNew;

implementation

uses Module, Principal, BloGeren, UnInternalConsts, UMySQLModule,
  UnMsgInt, CNAB_Cfg, MyDBCheck, ModuleBloGeren, UnMyObjects;

{$R *.DFM}

procedure TFmBloGerenNew.BtOKClick(Sender: TObject);
var
  i, Codigo, Controle, Periodo, Empresa, ModelBloq, Compe, Prev: Integer;
  Aviso01, Aviso02, Aviso03, Aviso04, Aviso05, Aviso06, Aviso07, Aviso08,
  Aviso09, Aviso10, AvisoVerso, Mensagem: String;
begin
  Codigo    := 0;
  Controle  := 0;
  ModelBloq := RGModelBloq.ItemIndex;
  //
  if RGCompe.Visible then
    Compe := RGCompe.ItemIndex
  else
    Compe := 0;
  //
  if ModelBloq < 1 then
  begin
    Geral.MB_Aviso('Defina o modelo de impress�o do boleto!');
    PageControl1.ActivePageIndex := 1;
    RGModelBloq.SetFocus;
    Exit;
  end;
  if (ModelBloq = 3) and (RGCompe.Visible = True) and (Compe < 1) then
  begin
    Geral.MB_Aviso('Defina a ficha de compensa��o!');
    PageControl1.ActivePageIndex := 1;
    RGCompe.SetFocus;
    Exit;
  end;
  if MyObjects.FIC(RGBloqFV.ItemIndex < 0, RGBloqFV, 'Defina se o boleto ser� impresso frente e verso!') then Exit;
  //
  Empresa    := F_CliInt;
  Periodo    := dmkPF.PeriodoEncode(Geral.IMV(CBAno.Items[CBAno.ItemIndex]), CBMes.ItemIndex+1);
  Aviso01    := Edit1.Text;
  Aviso02    := Edit2.Text;
  Aviso03    := Edit3.Text;
  Aviso04    := Edit4.Text;
  Aviso05    := Edit5.Text;
  Aviso06    := Edit6.Text;
  Aviso07    := Edit7.Text;
  Aviso08    := Edit8.Text;
  Aviso09    := Edit9.Text;
  Aviso10    := Edit10.Text;
  AvisoVerso := MeAvisoVerso.Text;
  //
  if FModo = istVisPesquisa then
  begin
    if MyObjects.FIC(FDBGrid = nil, nil, 'Grade n�o definida!') then Exit;
    if MyObjects.FIC(FQueryBoletos = nil, nil, 'Query boletos n�o definida!') then Exit;
    //
    if FDBGrid.SelectedRows.Count > 1 then
    begin
      with FDBGrid.DataSource.DataSet do
      for i := 0 to FDBGrid.SelectedRows.Count - 1 do
      begin
        GotoBookmark(pointer(FDBGrid.SelectedRows.Items[i]));
        //
        Controle := FQueryBoletos.FieldByName('Controle').AsInteger;
        //
        if not UBloquetos.AtualizaLayoutAvisosBoleto(Aviso01, Aviso02, Aviso03,
          Aviso04, Aviso05, Aviso06, Aviso07, Aviso08, Aviso09, Aviso10,
          AvisoVerso, ModelBloq, RGBloqFV.ItemIndex, Compe, 0, Controle)
        then
          Geral.MB_Aviso(Mensagem)
      end;
    end else
    begin
      Controle := FQueryBoletos.FieldByName('Controle').AsInteger;
      //
      if not UBloquetos.AtualizaLayoutAvisosBoleto(Aviso01, Aviso02, Aviso03,
        Aviso04, Aviso05, Aviso06, Aviso07, Aviso08, Aviso09, Aviso10,
        AvisoVerso, ModelBloq, RGBloqFV.ItemIndex, Compe, 0, Controle)
      then
        Geral.MB_Aviso(Mensagem)
    end;
    Close;
  end else
  begin
    if ImgTipo.SQLType = stIns then
      Codigo := 0
    else
    Codigo := FQueryPrev.FieldByName('Codigo').AsInteger;
    Prev   := UBloquetos.AtualizaBloGerenNew(ImgTipo.SQLType, Codigo, 0, Empresa,
                Periodo, ModelBloq, Compe, RGBloqFV.ItemIndex, Aviso01, Aviso02,
                Aviso03, Aviso04, Aviso05, Aviso06, Aviso07, Aviso08, Aviso09,
                Aviso10, AvisoVerso, Mensagem);
    if Prev = 0 then
      Geral.MB_Aviso(Mensagem)
    else begin
      //Atualiza modelo de boleto e avisos dos boletos
      if UBloquetos.AtualizaLayoutAvisosBoleto(Aviso01, Aviso02, Aviso03, Aviso04,
        Aviso05, Aviso06, Aviso07, Aviso08, Aviso09, Aviso10, AvisoVerso,
        ModelBloq, RGBloqFV.ItemIndex, Compe, Prev, 0) then
      begin
        FPeriodo := Periodo;
        Close;
      end else
      begin
        Geral.MB_Aviso('Falha ao atualizar layout e avisos nos boletos!');
        Close;
      end;
    end;
  end;
end;

procedure TFmBloGerenNew.BitBtn1Click(Sender: TObject);
begin
  Edit11.Text := Edit10.Text;
  Edit10.Text := Edit9.Text;
  Edit9.Text  := Edit8.Text;
  Edit8.Text  := Edit7.Text;
  Edit7.Text  := Edit6.Text;
  Edit6.Text  := Edit5.Text;
  Edit5.Text  := Edit4.Text;
  Edit4.Text  := Edit3.Text;
  Edit3.Text  := Edit2.Text;
  Edit2.Text  := Edit1.Text;
  Edit1.Text  := Edit0.Text;
  Edit0.Text  := '';
end;

procedure TFmBloGerenNew.BitBtn2Click(Sender: TObject);
begin
  Edit0.Text  := Edit1.Text;
  Edit1.Text  := Edit2.Text;
  Edit2.Text  := Edit3.Text;
  Edit3.Text  := Edit4.Text;
  Edit4.Text  := Edit5.Text;
  Edit5.Text  := Edit6.Text;
  Edit6.Text  := Edit7.Text;
  Edit7.Text  := Edit8.Text;
  Edit8.Text  := Edit9.Text;
  Edit9.Text  := Edit10.Text;
  Edit10.Text := Edit11.Text;
  Edit11.Text := '';
end;

procedure TFmBloGerenNew.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloGerenNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmBloGerenNew.FormCreate(Sender: TObject);
var
  i: Integer;
  //
  Ano, Mes, Dia: Word;
begin
  ImgTipo.SQLType := stLok;
  //
  with CBMes.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  DecodeDate(Date, Ano, Mes, Dia);
  with CBAno.Items do
  begin
    for i := Ano-50 to Ano + 50 do Add(IntToStr(i));
  end;
  CBAno.ItemIndex := 50;
  CBMes.ItemIndex := (Mes - 1);
  //////////////////////////////////////////////////////////////////////////
  UBloquetos.PreencheModelosBloq(RGModelBloq);
  //
  DBloGeren.ReopenBloOpcoes;
end;

procedure TFmBloGerenNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloGerenNew.FormShow(Sender: TObject);
var
  i: Integer;
  Ano, Mes: Word;
begin
  if (FMes = 0) and (FAno = 0) then
  begin
    QrPesq.Close;
    QrPesq.SQL.Clear;
    QrPesq.SQL.Add('SELECT Max(Periodo) Periodo');
    QrPesq.SQL.Add('FROM Prev');
    QrPesq.SQL.Add('WHERE Empresa=:P0');
    QrPesq.Params[0].AsInteger := FmPrincipal.FEntInt;
    QrPesq.Open;
    if QrPesqPeriodo.Value > 0 then
    begin
      dmkPF.MesEAnoDePeriodoShort(QrPesqPeriodo.Value + 1, Mes, Ano);
      for i := 0 to CBAno.Items.Count do
      begin
        if CBAno.Items[i] = IntToStr(Ano) then
        begin
          CBAno.ItemIndex := i;
          Break;
        end;
      end;
      CBMes.ItemIndex := Mes -1;
    end;
  end else
  begin
    CBMes.ItemIndex := FMes - 1;
    CBAno.ItemIndex := FAno;
  end;
  MostraEdicao(ImgTipo.SQLType, FModo);
end;

procedure TFmBloGerenNew.MostraEdicao(Tipo: TSQLType;
  Modo: TModoVisualizacaoBloquetos);
begin
  PageControl1.ActivePageIndex := 0;
  if Modo = istVisPesquisa then
  begin
    TabSheet1.TabVisible := True;
    TabSheet2.TabVisible := True;
    TabSheet3.TabVisible := True;
    TabSheet4.TabVisible := False;
    //
    RGModelBloq.ItemIndex := -1;
    RGBloqFV.ItemIndex    := -1;
    RGCompe.ItemIndex     := -1;
  end else
  begin
    TabSheet1.TabVisible := True;
    TabSheet2.TabVisible := True;
    TabSheet3.TabVisible := True;
    TabSheet4.TabVisible := True;
    //
    case Tipo of
      stIns:
      begin
        TabSheet4.TabVisible  := True;
        RGModelBloq.ItemIndex := DBloGeren.QrBloOpcoes.FieldByName('ModelBloq').AsInteger;
        RGCompe.ItemIndex     := DBloGeren.QrBloOpcoes.FieldByName('Compe').AsInteger;
        Edit0.Text            := '';
        Edit1.Text            := '';
        Edit2.Text            := '';
        Edit3.Text            := '';
        Edit4.Text            := '';
        Edit5.Text            := '';
        Edit6.Text            := '';
        Edit7.Text            := '';
        Edit8.Text            := '';
        Edit9.Text            := '';
        Edit10.Text           := '';
        Edit11.Text           := '';
        MeAvisoVerso.Text     := '';
        RGBloqFV.ItemIndex    := DBloGeren.QrBloOpcoes.FieldByName('BloqFV').AsInteger;
      end;
      stUpd:
      begin
        TabSheet4.TabVisible  := False;
        RGModelBloq.ItemIndex := FQueryPrev.FieldByName('ModelBloq').AsInteger;
        RGCompe.ItemIndex     := FQueryPrev.FieldByName('Compe').AsInteger;
        Edit0.Text            := '';
        Edit1.Text            := FQueryPrev.FieldByName('Aviso01').AsString;
        Edit2.Text            := FQueryPrev.FieldByName('Aviso02').AsString;
        Edit3.Text            := FQueryPrev.FieldByName('Aviso03').AsString;
        Edit4.Text            := FQueryPrev.FieldByName('Aviso04').AsString;
        Edit5.Text            := FQueryPrev.FieldByName('Aviso05').AsString;
        Edit6.Text            := FQueryPrev.FieldByName('Aviso06').AsString;
        Edit7.Text            := FQueryPrev.FieldByName('Aviso07').AsString;
        Edit8.Text            := FQueryPrev.FieldByName('Aviso08').AsString;
        Edit9.Text            := FQueryPrev.FieldByName('Aviso09').AsString;
        Edit10.Text           := FQueryPrev.FieldByName('Aviso10').AsString;
        Edit11.Text           := '';
        MeAvisoVerso.Text     := FQueryPrev.FieldByName('AvisoVerso').AsString;
        RGBloqFV.ItemIndex    := FQueryPrev.FieldByName('BloqFV').AsInteger;
      end;
    end;
  end;
end;

procedure TFmBloGerenNew.RGModelBloqClick(Sender: TObject);
begin
  if RGModelBloq.ItemIndex = 3 then
    RGCompe.Visible := True
  else
    RGCompe.Visible := False;
end;

end.
