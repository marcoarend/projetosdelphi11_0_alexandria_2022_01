object FmBloGerenArreEnt: TFmBloGerenArreEnt
  Left = 445
  Top = 185
  BorderStyle = bsDialog
  Caption = 'BLO-GEREN-005 :: Item de Arrecada'#231#227'o'
  ClientHeight = 381
  ClientWidth = 454
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 454
    Height = 225
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 52
      Width = 45
      Height = 13
      Caption = 'Entidade:'
    end
    object Label2: TLabel
      Left = 12
      Top = 92
      Width = 31
      Height = 13
      Caption = 'Conta:'
    end
    object Label13: TLabel
      Left = 12
      Top = 134
      Width = 114
      Height = 13
      Caption = 'Descri'#231#227'o [F4 : F5 : F6]:'
    end
    object LaDeb: TLabel
      Left = 124
      Top = 174
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object SpeedButton1: TSpeedButton
      Left = 408
      Top = 108
      Width = 21
      Height = 21
      Hint = 'Inclui item de carteira'
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object SpeedButton3: TSpeedButton
      Left = 408
      Top = 68
      Width = 21
      Height = 21
      Hint = 'Inclui item de carteira'
      Caption = '...'
      OnClick = SpeedButton3Click
    end
    object Label37: TLabel
      Left = 12
      Top = 8
      Width = 113
      Height = 13
      Caption = 'Configura'#231#227'o do boleto:'
    end
    object SpeedButton2: TSpeedButton
      Left = 408
      Top = 25
      Width = 21
      Height = 21
      Hint = 'Inclui item de carteira'
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object Label5: TLabel
      Left = 12
      Top = 174
      Width = 59
      Height = 13
      Caption = 'Vencimento:'
    end
    object CBEntidade: TdmkDBLookupComboBox
      Left = 68
      Top = 68
      Width = 337
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsEntidades
      TabOrder = 3
      dmkEditCB = EdEntidade
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdEntidade: TdmkEditCB
      Left = 12
      Top = 68
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEntidade
      IgnoraDBLookupComboBox = False
    end
    object EdConta: TdmkEditCB
      Left = 12
      Top = 108
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBConta
      IgnoraDBLookupComboBox = False
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 68
      Top = 108
      Width = 337
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 5
      dmkEditCB = EdConta
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDescricao: TdmkEdit
      Left = 12
      Top = 150
      Width = 417
      Height = 21
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnKeyDown = EdDescricaoKeyDown
    end
    object EdValor: TdmkEdit
      Left = 124
      Top = 190
      Width = 88
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object CkContinuar: TCheckBox
      Left = 220
      Top = 192
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 9
    end
    object EdCNAB_cfg: TdmkEditCB
      Left = 12
      Top = 25
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCNAB_cfg
      IgnoraDBLookupComboBox = False
    end
    object CBCNAB_cfg: TdmkDBLookupComboBox
      Left = 68
      Top = 25
      Width = 337
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCNAB_cfg
      TabOrder = 1
      dmkEditCB = EdCNAB_cfg
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object TPData: TDateTimePicker
      Left = 12
      Top = 190
      Width = 106
      Height = 21
      Date = 39328.122587314800000000
      Time = 39328.122587314800000000
      TabOrder = 7
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 454
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 406
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 358
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 252
        Height = 32
        Caption = 'Item de Arrecada'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 252
        Height = 32
        Caption = 'Item de Arrecada'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 252
        Height = 32
        Caption = 'Item de Arrecada'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 273
    Width = 454
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 450
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 317
    Width = 454
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 450
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 306
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM entidades ent'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 12
    Top = 10
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 40
    Top = 10
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.*, sg.Nome NOMESUBGRUPO ,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1'
      'WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMEEMPRESA'
      'FROM contas co, Subgrupos sg,'
      'Grupos gr, Conjuntos cj, Entidades cl'
      'WHERE sg.Codigo=co.Subgrupo'
      'AND gr.Codigo=sg.Grupo'
      'AND cj.Codigo=gr.Conjunto'
      'AND cl.Codigo=co.Empresa'
      'AND co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 68
    Top = 10
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 96
    Top = 10
  end
  object DsCNAB_cfg: TDataSource
    DataSet = QrCNAB_cfg
    Left = 152
    Top = 10
  end
  object QrCNAB_cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, CartEmiss, Texto01, Texto02, Texto03, '
      'Texto04, Texto05, Texto06, Texto07, Texto08, Texto09, Texto10'
      'FROM cnab_cfg'
      'ORDER BY Nome')
    Left = 124
    Top = 10
    object QrCNAB_cfgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_cfg.Codigo'
    end
    object QrCNAB_cfgNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrCNAB_cfgCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrCNAB_cfgTexto01: TWideStringField
      FieldName = 'Texto01'
      Size = 100
    end
    object QrCNAB_cfgTexto02: TWideStringField
      FieldName = 'Texto02'
      Size = 100
    end
    object QrCNAB_cfgTexto03: TWideStringField
      FieldName = 'Texto03'
      Size = 100
    end
    object QrCNAB_cfgTexto04: TWideStringField
      FieldName = 'Texto04'
      Size = 100
    end
    object QrCNAB_cfgTexto05: TWideStringField
      FieldName = 'Texto05'
      Size = 100
    end
    object QrCNAB_cfgTexto06: TWideStringField
      FieldName = 'Texto06'
      Size = 100
    end
    object QrCNAB_cfgTexto07: TWideStringField
      FieldName = 'Texto07'
      Size = 100
    end
    object QrCNAB_cfgTexto08: TWideStringField
      FieldName = 'Texto08'
      Size = 100
    end
    object QrCNAB_cfgTexto09: TWideStringField
      FieldName = 'Texto09'
      Size = 100
    end
    object QrCNAB_cfgTexto10: TWideStringField
      FieldName = 'Texto10'
      Size = 100
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 11
  end
end
