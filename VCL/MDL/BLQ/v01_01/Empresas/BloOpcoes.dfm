object FmBloOpcoes: TFmBloOpcoes
  Left = 339
  Top = 185
  Caption = 'BLO-OPCAO-001 :: Op'#231#245'es Boletos'
  ClientHeight = 542
  ClientWidth = 764
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 764
    Height = 386
    ActivePage = TabSheet1
    Align = alClient
    TabHeight = 25
    TabOrder = 0
    ExplicitHeight = 366
    object TabSheet1: TTabSheet
      Caption = 'Geral'
      ExplicitHeight = 331
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 756
        Height = 351
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        ExplicitHeight = 331
        object Label98: TLabel
          Left = 8
          Top = 11
          Width = 166
          Height = 13
          Caption = 'Tipo de email para envio de boleto:'
        end
        object SpeedButton15: TSpeedButton
          Left = 455
          Top = 30
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton15Click
        end
        object Label3: TLabel
          Left = 8
          Top = 330
          Width = 375
          Height = 13
          Caption = 
            'Caso n'#227'o haja n'#250'mero de fatura ou nota fiscal ser'#225' utilizado o n' +
            #250'mero do boleto'
        end
        object EdEntiTipCto: TdmkEditCB
          Left = 8
          Top = 30
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'EntiTipCto'
          UpdCampo = 'EntiTipCto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEntiTipCto
          IgnoraDBLookupComboBox = False
        end
        object CBEntiTipCto: TdmkDBLookupComboBox
          Left = 66
          Top = 30
          Width = 385
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsEntiTipCto
          TabOrder = 1
          dmkEditCB = EdEntiTipCto
          QryCampo = 'EntiTipCto'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object GroupBox1: TGroupBox
          Left = 8
          Top = 64
          Width = 468
          Height = 145
          Caption = ' Envio de emeio de boleto: '
          TabOrder = 2
          object Label19: TLabel
            Left = 8
            Top = 20
            Width = 197
            Height = 13
            Caption = 'Considerar que o e-mail foi recebido ap'#243's '
          end
          object Label20: TLabel
            Left = 304
            Top = 20
            Width = 115
            Height = 13
            Caption = 'dias do seu vencimento.'
          end
          object Label4: TLabel
            Left = 8
            Top = 43
            Width = 200
            Height = 13
            Caption = 'Pr'#233' e-mail para envio de aviso de reajuste:'
          end
          object SpeedButton1: TSpeedButton
            Left = 440
            Top = 62
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object SpeedButton2: TSpeedButton
            Left = 440
            Top = 110
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object Label5: TLabel
            Left = 8
            Top = 91
            Width = 406
            Height = 13
            Caption = 
              'Pr'#233' e-mail para re-envio de aviso da falta de confirma'#231#227'o do rec' +
              'ebimento do bloqueto:'
          end
          object EdDdAutConfMail: TdmkEdit
            Left = 213
            Top = 17
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '60'
            QryCampo = 'DdAutConfMail'
            UpdCampo = 'DdAutConfMail'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 60
            ValWarn = False
          end
          object EdPreMailReaj: TdmkEditCB
            Left = 8
            Top = 62
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PreMailReaj'
            UpdCampo = 'PreMailReaj'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPreMailReaj
            IgnoraDBLookupComboBox = False
          end
          object CBPreMailReaj: TdmkDBLookupComboBox
            Left = 65
            Top = 62
            Width = 370
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPreEmail
            TabOrder = 2
            dmkEditCB = EdPreMailReaj
            QryCampo = 'PreMailReaj'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPreMailReenv: TdmkEditCB
            Left = 8
            Top = 110
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PreMailReenv'
            UpdCampo = 'PreMailReenv'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPreMailReenv
            IgnoraDBLookupComboBox = False
          end
          object CBPreMailReenv: TdmkDBLookupComboBox
            Left = 65
            Top = 110
            Width = 370
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPreEmailRe
            TabOrder = 4
            dmkEditCB = EdPreMailReenv
            QryCampo = 'PreMailReenv'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object GroupBox2: TGroupBox
          Left = 8
          Top = 218
          Width = 468
          Height = 50
          Caption = 'Configura'#231#245'es base de dados WEB'
          TabOrder = 3
          object Label1: TLabel
            Left = 8
            Top = 23
            Width = 111
            Height = 13
            Caption = 'Quantidade m'#225'xima de '
          end
          object Label2: TLabel
            Left = 208
            Top = 23
            Width = 237
            Height = 13
            Caption = 'dias para a configura'#231#227'o do vencimento do boleto'
          end
          object EdMaxDias: TdmkEdit
            Left = 123
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '60'
            QryCampo = 'MaxDias'
            UpdCampo = 'MaxDias'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 60
            ValWarn = False
          end
        end
        object RGPerGerBol: TRadioGroup
          Left = 482
          Top = 11
          Width = 265
          Height = 45
          Caption = 'Per'#237'odo no gerenciamento dos boletos: '
          Columns = 3
          ItemIndex = 1
          Items.Strings = (
            'Anterior'
            'Atual'
            'Pr'#243'ximo')
          TabOrder = 5
        end
        object RGAbreEmTab: TRadioGroup
          Left = 482
          Top = 113
          Width = 265
          Height = 80
          Caption = ' Modo de abertura da janela de geren. de boletos'
          ItemIndex = 0
          Items.Strings = (
            'Janela'
            'Aba'
            'Escolher na abertura')
          TabOrder = 7
        end
        object RGVisPadrao: TRadioGroup
          Left = 482
          Top = 62
          Width = 265
          Height = 45
          Caption = 'Modo de visualiza'#231#227'o padr'#227'o'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Mensal'
            'Pesquisa')
          TabOrder = 6
        end
        object RGNumDocPad: TRadioGroup
          Left = 8
          Top = 274
          Width = 468
          Height = 50
          Caption = ' N'#250'mero do documento no boleto'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'N'#250'mero do boleto'
            'N'#250'mero da fatura'
            'N'#250'mero da nota fiscal')
          TabOrder = 4
        end
        object RGVctoFeriado: TRadioGroup
          Left = 482
          Top = 199
          Width = 265
          Height = 69
          Caption = 'Vencimento do boleto'
          ItemIndex = 0
          Items.Strings = (
            'Utilizar a data selecionada'
            'Pular feriados e dias n'#227'o '#250'teis')
          TabOrder = 8
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Modelo padr'#227'o'
      ImageIndex = 1
      ExplicitHeight = 331
      object RGModelBloq: TdmkRadioGroup
        Left = 0
        Top = 0
        Width = 756
        Height = 271
        Align = alClient
        Caption = ' Modelo de impress'#227'o do boleto: '
        Items.Strings = (
          'ModuleBloGeren.PreencheModelosBloq')
        TabOrder = 0
        OnClick = RGModelBloqClick
        QryCampo = 'ModelBloq'
        UpdCampo = 'ModelBloq'
        UpdType = utYes
        OldValor = 0
        ExplicitHeight = 251
      end
      object RGCompe: TdmkRadioGroup
        Left = 0
        Top = 271
        Width = 756
        Height = 40
        Align = alBottom
        Caption = ' Ficha de compensa'#231#227'o (modelo E): '
        Columns = 3
        Items.Strings = (
          '??'
          '1 (uma via)'
          '2 (duas vias)')
        TabOrder = 1
        QryCampo = 'Compe'
        UpdCampo = 'Compe'
        UpdType = utYes
        OldValor = 0
        ExplicitTop = 251
      end
      object RGBloqFV: TdmkRadioGroup
        Left = 0
        Top = 311
        Width = 756
        Height = 40
        Align = alBottom
        Caption = 'Frente e verso?'
        Columns = 2
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 2
        QryCampo = 'BloqFV'
        UpdCampo = 'BloqFV'
        UpdType = utYes
        OldValor = 0
        ExplicitTop = 291
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 764
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 716
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 668
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 193
        Height = 32
        Caption = 'Op'#231#245'es Boletos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 193
        Height = 32
        Caption = 'Op'#231#245'es Boletos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 193
        Height = 32
        Caption = 'Op'#231#245'es Boletos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 434
    Width = 764
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 414
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 760
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 478
    Width = 764
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 458
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 760
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 616
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 18
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntiTipCto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 476
    Top = 8
    object QrEntiTipCtoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiTipCtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntiTipCtoNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiTipCto: TDataSource
    DataSet = QrEntiTipCto
    Left = 504
    Top = 8
  end
  object VUEntiTipCto: TdmkValUsu
    dmkEditCB = EdEntiTipCto
    Panel = TabSheet1
    QryCampo = 'EntiTipCto'
    UpdCampo = 'EntiTipCto'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 532
    Top = 8
  end
  object QrBloOpcoes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM bloopcoes')
    Left = 420
    Top = 8
  end
  object DsBloOpcoes: TDataSource
    DataSet = QrBloOpcoes
    Left = 448
    Top = 8
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 616
    Top = 8
  end
  object QrPreEmail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 560
    Top = 8
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreEmailNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreEmail: TDataSource
    DataSet = QrPreEmail
    Left = 588
    Top = 8
  end
  object QrPreEmailRe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 644
    Top = 8
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreEmailRe: TDataSource
    DataSet = QrPreEmailRe
    Left = 672
    Top = 8
  end
end
