object FmBloGerenBaAVen: TFmBloGerenBaAVen
  Left = 339
  Top = 185
  Caption = 'BLO-GEREN-018 :: Renova'#231#227'o de Contas Bases'
  ClientHeight = 562
  ClientWidth = 964
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 964
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 916
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 868
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 352
        Height = 32
        Caption = 'Renova'#231#227'o de Contas Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 352
        Height = 32
        Caption = 'Renova'#231#227'o de Contas Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 352
        Height = 32
        Caption = 'Renova'#231#227'o de Contas Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 964
    Height = 400
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 964
      Height = 400
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 964
        Height = 400
        Align = alClient
        TabOrder = 0
        object DBGBloArreIts: TdmkDBGrid
          Left = 2
          Top = 15
          Width = 960
          Height = 383
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEARRECAD'
              Title.Caption = 'Arrecada'#231#227'o'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EXPIRAEM'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'T'#233'rmino'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Entidade'
              Width = 190
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SitCobr_TXT'
              Title.Caption = 'Sit. de cobran'#231'a'
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Parc_Calc'
              Title.Caption = 'Parc.'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Contrato'
              Title.Caption = 'ID Contrato'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DiaVcto_TXT'
              Title.Caption = 'Dia do vencto.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'INICIO'
              Title.Caption = 'In'#237'cio'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FINAL'
              Title.Caption = 'Final'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor_TXT'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InfoParc_TXT'
              Title.Caption = 'Info parc.'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Texto'
              Title.Caption = 'Descri'#231#227'o substituta'
              Width = 139
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Arredonda'
              Title.Caption = 'Arredondamento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEEMP'
              Title.Caption = 'Empresa'
              Width = 190
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECFG'
              Title.Caption = 'Configura'#231#227'o'
              Width = 150
              Visible = True
            end>
          Color = clWindow
          Ctl3D = True
          DataSource = DBloGeren.DsBloArreItsVen
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGBloArreItsDrawColumnCell
          FieldsCalcToOrder.Strings = (
            'EXPIRAEM=ParcPerF'
            'InfoParc_TXT=InfoParc'
            'Parc_Calc=Parcelas'
            'INICIO=ParcPerI'
            'FINAL=ParcPerF')
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEARRECAD'
              Title.Caption = 'Arrecada'#231#227'o'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EXPIRAEM'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'T'#233'rmino'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Entidade'
              Width = 190
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SitCobr_TXT'
              Title.Caption = 'Sit. de cobran'#231'a'
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Parc_Calc'
              Title.Caption = 'Parc.'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Contrato'
              Title.Caption = 'ID Contrato'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DiaVcto_TXT'
              Title.Caption = 'Dia do vencto.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'INICIO'
              Title.Caption = 'In'#237'cio'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FINAL'
              Title.Caption = 'Final'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor_TXT'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'InfoParc_TXT'
              Title.Caption = 'Info parc.'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Texto'
              Title.Caption = 'Descri'#231#227'o substituta'
              Width = 139
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Arredonda'
              Title.Caption = 'Arredondamento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEEMP'
              Title.Caption = 'Empresa'
              Width = 190
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECFG'
              Title.Caption = 'Configura'#231#227'o'
              Width = 150
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 448
    Width = 964
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 960
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 576
        Height = 16
        Caption = 
          'ATEN'#199#195'O: Atualize a arrecada'#231#227'o somente ap'#243's sua expira'#231#227'o pois ' +
          'os valores ser'#227'o sobrescritos!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 576
        Height = 16
        Caption = 
          'ATEN'#199#195'O: Atualize a arrecada'#231#227'o somente ap'#243's sua expira'#231#227'o pois ' +
          'os valores ser'#227'o sobrescritos!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 492
    Width = 964
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 818
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 816
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAcao: TBitBtn
        Tag = 294
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&A'#231#227'o'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAcaoClick
      end
    end
  end
  object PMAcao: TPopupMenu
    OnPopup = PMAcaoPopup
    Left = 56
    Top = 360
    object ReajustarutilizandoIGPM1: TMenuItem
      Caption = '&Reajustar utilizando IGP-M'
      OnClick = ReajustarutilizandoIGPM1Click
    end
    object Desativarativararrecadaoselecionada1: TMenuItem
      Caption = '&Desativar / ativar arrecada'#231#227'o atual'
      OnClick = Desativarativararrecadaoselecionada1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Enviaremail1: TMenuItem
      Caption = '&Enviar e-mail'
      OnClick = Enviaremail1Click
    end
    object N2: TMenuItem
      Caption = '-'
      Visible = False
    end
    object Localizararrecadao1: TMenuItem
      Caption = '&Localizar arrecada'#231#227'o'
      OnClick = Localizararrecadao1Click
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 232
    Top = 180
  end
end
