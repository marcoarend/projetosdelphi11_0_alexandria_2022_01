unit CondGerArreMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, Grids, DBGrids,
  DBCtrls, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmCondGerArreMul = class(TForm)
    Panel1: TPanel;
    QrUnidCond: TmySQLQuery;
    DsUnidCond: TDataSource;
    QrUnidCondApto: TIntegerField;
    QrUnidCondUnidade: TWideStringField;
    QrUnidCondProprie: TWideStringField;
    QrUnidCondSelecio: TSmallintField;
    DBGrid1: TDBGrid;
    QrSolo: TmySQLQuery;
    QrSoloConta: TIntegerField;
    Panel3: TPanel;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    DsContas: TDataSource;
    Label2: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    EdValor: TdmkEdit;
    LaDeb: TLabel;
    CkContinuar: TCheckBox;
    QrUnidCondEntidad: TIntegerField;
    CkNaoArreRisco: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtSaida: TBitBtn;
    QrLoc: TmySQLQuery;
    Label4: TLabel;
    EdCNAB_Cfg: TdmkEditCB;
    CBCNAB_Cfg: TdmkDBLookupComboBox;
    QrCNAB_Cfg: TmySQLQuery;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    DsCNAB_Cfg: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenUnidCond(Apto: Integer);
    procedure AtualizaTodos(Status: Integer);
  public
    { Public declarations }
    FCodiCliente, FCond, FApto, FPropriet, FCodigo: Integer;
    FPrevPeriodo, FTabAriA, FTmpTabUnidCond: String;
  end;

  var
  FmCondGerArreMul: TFmCondGerArreMul;

implementation

uses Module, MyVCLSkin, ArreBaC, UMySQLModule, UnInternalConsts, ModuleGeral,
  UnMyObjects, UnBloqGerl;

{$R *.DFM}

procedure TFmCondGerArreMul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerArreMul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerArreMul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerArreMul.FormShow(Sender: TObject);
begin
  ReopenUnidCond(0);
  UBloqGerl.ReopenCNAB_Cfg(QrCNAB_Cfg, Dmod.MyDB, FCodiCliente);
end;

procedure TFmCondGerArreMul.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmCondGerArreMul.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Selecio' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, QrUnidCondSelecio.Value);
end;

procedure TFmCondGerArreMul.DBGrid1CellClick(Column: TColumn);
var
  Status, Apto: Integer;
begin
  if Column.FieldName = 'Selecio' then
  begin
    Status := QrUnidCondSelecio.Value;
    //
    if Status = 0 then
      Status := 1
    else
      Status := 0;
    //
    Apto := QrUnidCondApto.Value;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
      'UPDATE ' + FTmpTabUnidCond + ' SET Selecio=' + Geral.FF0(Status),
      'WHERE Apto=' + Geral.FF0(Apto),
      '']);
    ReopenUnidCond(Apto);
  end;
end;

procedure TFmCondGerArreMul.ReopenUnidCond(Apto: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUnidCond, DModG.MyPID_DB, [
    'SELECT uc.* ',
    'FROM ' + FTmpTabUnidCond + ' uc ',
    'ORDER BY uc.Unidade ',
    '']);
  if Apto <> 0 then
    QrUnidCond.Locate('Apto', Apto, []);
end;

procedure TFmCondGerArreMul.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmCondGerArreMul.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmCondGerArreMul.AtualizaTodos(Status: Integer);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'UPDATE ' + FTmpTabUnidCond,
    'SET Selecio=' + Geral.FF0(Status),
    '']);
  //
  ReopenUnidCond(QrUnidCondApto.Value);
end;

procedure TFmCondGerArreMul.BtOKClick(Sender: TObject);
var
  CNAB_Cfg, Conta, Controle, i: Integer;
  Valor: Double;
  Texto, Mensagem, Msg: String;
begin
  i        := 0;
  Controle := 0;
  Conta    := EdConta.ValueVariant;
  Valor    := EdValor.ValueVariant;
  Texto    := EdDescricao.ValueVariant;
  CNAB_Cfg := EdCNAB_Cfg.ValueVariant;
  Msg      := '';
  //
  if MyObjects.FIC(Conta = 0, EdConta, 'Conta n�o informada!') then Exit;
  if MyObjects.FIC(Valor < 0.01, EdValor, 'Valor n�o informado!') then Exit;
  if MyObjects.FIC(Texto = '', EdDescricao, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(CNAB_Cfg = 0, EdCNAB_Cfg, 'Defina uma configura��o de boleto!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO ' + FTabAriA + ' SET ');
    Dmod.QrUpd.SQL.Add('ComoAdd=3, Conta=:P0, Texto=:P1, Valor=:P2, Apto=:P3, ');
    Dmod.QrUpd.SQL.Add('Propriet=:P4, NaoArreRisco=:P5, CNAB_Cfg=:P6, ');
    Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
    //
    QrUnidCond.First;
    while not QrUnidCond.Eof do
    begin
      if QrUnidCondSelecio.Value = 1 then
      begin
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                      FTabAriA, TAB_ARI, 'Controle');
        //
        Dmod.QrUpd.Params[00].AsInteger := Conta;
        Dmod.QrUpd.Params[01].AsString  := Texto;
        Dmod.QrUpd.Params[02].AsFloat   := Valor;
        Dmod.QrUpd.Params[03].AsInteger := QrUnidCondApto.Value;
        Dmod.QrUpd.Params[04].AsInteger := QrUnidCondEntidad.Value;
        Dmod.QrUpd.Params[05].AsInteger := MLAGeral.BTI(CkNaoArreRisco.Checked);
        Dmod.QrUpd.Params[06].AsInteger := CNAB_Cfg;
        //
        Dmod.QrUpd.Params[07].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpd.Params[08].AsInteger := VAR_USUARIO;
        Dmod.QrUpd.Params[09].AsInteger := FCodigo;
        Dmod.QrUpd.Params[10].AsInteger := Controle;
        Dmod.QrUpd.ExecSQL;
        //
        Inc(i, 1);
      end;
      QrUnidCond.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  case i of
      0: Mensagem := 'N�o foi inclu�do nenhum item!';
      1: Mensagem := 'Foi inclu�do um item!';
    else Mensagem := 'Foram inclu�dos ' + Geral.FF0(i) + ' itens!';
  end;
  Geral.MB_Aviso(Mensagem);
  //
  if CkContinuar.Checked then
  begin
    EdConta.Text     := '';
    CBConta.KeyValue := NULL;
    EdValor.Text     := '';
    EdDescricao.Text := '';
    //
    EdConta.SetFocus;
    AtualizaTodos(0);
  end else
    Close;
end;

procedure TFmCondGerArreMul.EdDescricaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if QrContasMensal.Value = 'V' then
      Mes := FPrevPeriodo
    else
      Mes := '';
    if key=VK_F4 then
      EdDescricao.Text := QrContasNome.Value + ' ' + Mes;
    if key=VK_F5 then
      EdDescricao.Text := QrContasNome2.Value + ' ' + Mes;
    if key=VK_F6 then
      EdDescricao.Text := QrContasNome3.Value + ' ' + Mes;
    //
    EdDescricao.SetFocus;
    EdDescricao.SelStart  := Length(EdDescricao.ValueVariant);
    EdDescricao.SelLength := 0;
  end;
end;

end.
