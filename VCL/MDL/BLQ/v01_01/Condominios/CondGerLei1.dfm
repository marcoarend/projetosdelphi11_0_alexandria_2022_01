object FmCondGerLei1: TFmCondGerLei1
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-017 :: Leituras de Produtos Consumidos'
  ClientHeight = 560
  ClientWidth = 877
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnProduto: TPanel
    Left = 0
    Top = 48
    Width = 877
    Height = 65
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 41
      Height = 13
      Caption = 'Per'#237'odo:'
    end
    object Label2: TLabel
      Left = 88
      Top = 20
      Width = 40
      Height = 13
      Caption = 'Produto:'
    end
    object SpeedButton7: TSpeedButton
      Left = 832
      Top = 36
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton7Click
    end
    object EdPeriodo: TEdit
      Left = 16
      Top = 36
      Width = 69
      Height = 21
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Text = 'XXX/00'
    end
    object EdProduto: TdmkEditCB
      Left = 88
      Top = 36
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBProduto
      IgnoraDBLookupComboBox = False
    end
    object CBProduto: TdmkDBLookupComboBox
      Left = 144
      Top = 36
      Width = 685
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCons
      TabOrder = 2
      dmkEditCB = EdProduto
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object STCli: TStaticText
      Left = 0
      Top = 0
      Width = 877
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = '{}'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
  end
  object PnLeitura: TPanel
    Left = 0
    Top = 113
    Width = 877
    Height = 333
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 877
      Height = 333
      Align = alClient
      DataSource = DsConsLei
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnColEnter = DBGrid1ColEnter
      OnDrawColumnCell = DBGrid1DrawColumnCell
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Apto_Un'
          ReadOnly = True
          Title.Caption = 'Apartamento'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedAnt'
          Title.Caption = 'Medida anterior'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedAtu'
          Title.Caption = 'Medida atual'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Carencia'
          Title.Caption = 'Car'#234'ncia'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DifCaren'
          ReadOnly = True
          Title.Caption = 'dc'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GASTO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ReadOnly = True
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEDIA_VEZES'
          ReadOnly = True
          Title.Caption = 'M* Vezes'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEDIA_MENOR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ReadOnly = True
          Title.Caption = 'M* Menor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEDIA_MEDIA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ReadOnly = True
          Title.Caption = 'M* M'#233'dia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEDIA_MAIOR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ReadOnly = True
          Title.Caption = 'M* Maior'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 877
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 829
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 781
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 410
        Height = 32
        Caption = 'Leituras de Produtos Consumidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 410
        Height = 32
        Caption = 'Leituras de Produtos Consumidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 410
        Height = 32
        Caption = 'Leituras de Produtos Consumidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 446
    Width = 877
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 873
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 148
        Height = 16
        Caption = 'M*  :  '#218'ltimos consumos.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 148
        Height = 16
        Caption = 'M*  :  '#218'ltimos consumos.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 490
    Width = 877
    Height = 70
    Align = alBottom
    TabOrder = 4
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 873
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 729
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrCons: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cns.Codigo, cns.Nome, cnp.Casas, cnp.Preco, '
      'cnp.UnidFat, cnp.UnidLei, cnp.UnidImp, cnp.Carencia,'
      'cnp.DifCaren, cnp.CNAB_Cfg'
      'FROM consprc cnp'
      'LEFT JOIN cons cns ON cns.Codigo=cnp.Codigo'
      'WHERE cnp.Cond=:P0'
      'ORDER BY cns.Nome')
    Left = 280
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrConsCasas: TSmallintField
      FieldName = 'Casas'
    end
    object QrConsPreco: TFloatField
      FieldName = 'Preco'
      Required = True
      EditFormat = '#,###,##0.0000'
    end
    object QrConsUnidFat: TFloatField
      FieldName = 'UnidFat'
      Required = True
    end
    object QrConsUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Required = True
      Size = 5
    end
    object QrConsUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Required = True
      Size = 5
    end
    object QrConsCarencia: TFloatField
      FieldName = 'Carencia'
      Required = True
    end
    object QrConsDifCaren: TSmallintField
      FieldName = 'DifCaren'
    end
    object QrConsCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
  end
  object DsCons: TDataSource
    DataSet = QrCons
    Left = 308
    Top = 104
  end
  object QrAptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cdi.Conta, cdi.Unidade, cdi.Propriet'
      'FROM condimov cdi'
      'WHERE cdi.Codigo=:P0')
    Left = 356
    Top = 209
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAptosConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'condimov.Conta'
      Required = True
    end
    object QrAptosMedAnt: TFloatField
      FieldKind = fkLookup
      FieldName = 'MedAnt'
      LookupDataSet = QrAnt
      LookupKeyFields = 'Apto'
      LookupResultField = 'MedAtu'
      KeyFields = 'Conta'
      Lookup = True
    end
    object QrAptosAptoJa: TIntegerField
      FieldKind = fkLookup
      FieldName = 'AptoJa'
      LookupDataSet = QrJaTem
      LookupKeyFields = 'Apto'
      LookupResultField = 'Apto'
      KeyFields = 'Conta'
      Lookup = True
    end
    object QrAptosUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'condimov.Unidade'
      Required = True
      Size = 10
    end
    object QrAptosPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'condimov.Propriet'
      Required = True
    end
  end
  object QrAnt: TmySQLQuery
    Database = Dmod.MyDB
    Left = 384
    Top = 209
    object QrAntApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrAntMedAtu: TFloatField
      FieldName = 'MedAtu'
      Required = True
    end
  end
  object TbConsLei: TmySQLTable
    Database = DModG.MyPID_DB
    BeforeOpen = TbConsLeiBeforeOpen
    BeforePost = TbConsLeiBeforePost
    OnCalcFields = TbConsLeiCalcFields
    SortFieldNames = 'Apto_Un'
    TableName = 'conslei'
    Left = 360
    Top = 237
    object TbConsLeiID_Rand: TWideStringField
      FieldName = 'ID_Rand'
      Origin = 'conslei.ID_Rand'
      Size = 15
    end
    object TbConsLeiApto_ID: TIntegerField
      FieldName = 'Apto_ID'
      Origin = 'conslei.Apto_ID'
    end
    object TbConsLeiApto_Un: TWideStringField
      FieldName = 'Apto_Un'
      Origin = 'conslei.Apto_Un'
      Size = 10
    end
    object TbConsLeiMedAnt: TFloatField
      DisplayWidth = 16
      FieldName = 'MedAnt'
      Origin = 'conslei.MedAnt'
    end
    object TbConsLeiMedAtu: TFloatField
      DisplayWidth = 16
      FieldName = 'MedAtu'
      Origin = 'conslei.MedAtu'
    end
    object TbConsLeiCarencia: TFloatField
      DisplayWidth = 16
      FieldName = 'Carencia'
      Origin = 'conslei.MedAtu'
    end
    object TbConsLeiAdiciona: TSmallintField
      FieldName = 'Adiciona'
      Origin = 'conslei.Adiciona'
    end
    object TbConsLeiPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'conslei.Propriet'
    end
    object TbConsLeiGASTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GASTO'
      Calculated = True
    end
    object TbConsLeiVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object TbConsLeiMEDIA_VEZES: TFloatField
      FieldKind = fkLookup
      FieldName = 'MEDIA_VEZES'
      LookupDataSet = QrMedias
      LookupKeyFields = 'Apto'
      LookupResultField = 'Vezes'
      KeyFields = 'Apto_ID'
      Lookup = True
    end
    object TbConsLeiMEDIA_MENOR: TFloatField
      FieldKind = fkLookup
      FieldName = 'MEDIA_MENOR'
      LookupDataSet = QrMedias
      LookupKeyFields = 'Apto'
      LookupResultField = 'MENOR'
      KeyFields = 'Apto_ID'
      Lookup = True
    end
    object TbConsLeiMEDIA_MEDIA: TFloatField
      FieldKind = fkLookup
      FieldName = 'MEDIA_MEDIA'
      LookupDataSet = QrMedias
      LookupKeyFields = 'Apto'
      LookupResultField = 'MEDIA'
      KeyFields = 'Apto_ID'
      Lookup = True
    end
    object TbConsLeiMEDIA_MAIOR: TFloatField
      FieldKind = fkLookup
      FieldName = 'MEDIA_MAIOR'
      LookupDataSet = QrMedias
      LookupKeyFields = 'Apto'
      LookupResultField = 'MAIOR'
      KeyFields = 'Apto_ID'
      Lookup = True
    end
    object TbConsLeiDifCaren: TSmallintField
      FieldName = 'DifCaren'
      MaxValue = 1
    end
  end
  object DsConsLei: TDataSource
    DataSet = TbConsLei
    Left = 384
    Top = 237
  end
  object QrPesq: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM conslei'
      'WHERE MedAtu <= MedAnt'
      'AND ID_Rand=:P0')
    Left = 356
    Top = 265
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrJaTem: TmySQLQuery
    Database = Dmod.MyDB
    Left = 328
    Top = 209
    object QrJaTemApto: TIntegerField
      FieldName = 'Apto'
    end
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    Left = 412
    Top = 209
  end
  object QrMedias: TmySQLQuery
    Database = Dmod.MyDB
    Left = 412
    Top = 237
    object QrMediasVezes: TLargeintField
      FieldName = 'Vezes'
      Required = True
    end
    object QrMediasMENOR: TFloatField
      FieldName = 'MENOR'
    end
    object QrMediasMEDIA: TFloatField
      FieldName = 'MEDIA'
    end
    object QrMediasMAIOR: TFloatField
      FieldName = 'MAIOR'
    end
    object QrMediasApto: TIntegerField
      FieldName = 'Apto'
    end
  end
  object QrUni: TmySQLQuery
    Database = Dmod.MyDB
    Left = 384
    Top = 181
    object QrUniApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrUniMedAtu: TFloatField
      FieldName = 'MedAtu'
      Required = True
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 648
    Top = 221
  end
end
