object FmCons: TFmCons
  Left = 368
  Top = 194
  Caption = 'LEI-CADAS-001 :: Consumos por Leitura'
  ClientHeight = 662
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 566
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 820
      Height = 136
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 120
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 16
        Top = 47
        Width = 131
        Height = 13
        Caption = 'Conta (do plano de contas):'
      end
      object SpeedButton5: TSpeedButton
        Left = 380
        Top = 63
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 120
        Top = 24
        Width = 281
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdGenero: TdmkEditCB
        Left = 16
        Top = 63
        Width = 41
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGenero
        IgnoraDBLookupComboBox = False
      end
      object CBGenero: TdmkDBLookupComboBox
        Left = 60
        Top = 63
        Width = 315
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 3
        dmkEditCB = EdGenero
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGExport_Tip: TRadioGroup
        Left = 16
        Top = 88
        Width = 385
        Height = 45
        Caption = ' Tipo de Leitura: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Outros'
          'G'#225's'
          #193'gua')
        TabOrder = 4
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 496
      Width = 784
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 636
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 10
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 566
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object PainelCab: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 72
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label8: TLabel
        Left = 420
        Top = 8
        Width = 131
        Height = 13
        Caption = 'Conta (do plano de contas):'
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 24
        Width = 60
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCons
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 72
        Top = 24
        Width = 345
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsCons
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdGenero: TDBEdit
        Left = 420
        Top = 24
        Width = 357
        Height = 21
        DataField = 'NOMEGENERO'
        DataSource = DsCons
        TabOrder = 2
      end
    end
    object PainelData: TPanel
      Left = 0
      Top = 383
      Width = 784
      Height = 119
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object DBGPeriodo: TDBGrid
        Left = 0
        Top = 0
        Width = 88
        Height = 119
        Align = alLeft
        DataSource = DsConsPer
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'PERIODO_TXT'
            Title.Caption = 'Per'#237'odo'
            Visible = True
          end>
      end
      object DBGrid1: TDBGrid
        Left = 88
        Top = 0
        Width = 696
        Height = 119
        Align = alClient
        DataSource = DsConsIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object DBGCond: TdmkDBGrid
      Left = 4
      Top = 72
      Width = 749
      Height = 120
      Columns = <
        item
          Expanded = False
          FieldName = 'Cond'
          Title.Caption = 'C'#243'd. cliente'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente (Condom'#237'nio)'
          Width = 321
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = 'Pre'#231'o'
          Width = 88
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UnidImp'
          Title.Caption = 'Un. impres.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UnidLei'
          Title.Caption = 'Un. leitura'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UnidFat'
          Title.Caption = 'Fator'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Casas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Carencia'
          Title.Caption = 'Car'#234'ncia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DifCaren'
          Title.Caption = 'dc'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CNAB_Cfg_Txt'
          Title.Caption = 'Configura'#231#227'o do boleto'
          Width = 200
          Visible = True
        end>
      Color = clWindow
      DataSource = DsConsPrc
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = DBGCondDrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Cond'
          Title.Caption = 'C'#243'd. cliente'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente (Condom'#237'nio)'
          Width = 321
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = 'Pre'#231'o'
          Width = 88
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UnidImp'
          Title.Caption = 'Un. impres.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UnidLei'
          Title.Caption = 'Un. leitura'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UnidFat'
          Title.Caption = 'Fator'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Casas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Carencia'
          Title.Caption = 'Car'#234'ncia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DifCaren'
          Title.Caption = 'dc'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CNAB_Cfg_Txt'
          Title.Caption = 'Configura'#231#227'o do boleto'
          Width = 200
          Visible = True
        end>
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 502
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 3
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel7: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtProduto: TBitBtn
          Tag = 10015
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Produto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtProdutoClick
        end
        object BtCond: TBitBtn
          Tag = 10005
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Condom.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtCondClick
        end
        object BtPeriodo: TBitBtn
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'P&er'#237'odo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          Visible = False
          OnClick = BtPeriodoClick
        end
        object BtLeitura: TBitBtn
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Leitura'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          Visible = False
        end
      end
    end
  end
  object PnCond: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 566
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    Visible = False
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label6: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = EdDBCodigo
      end
      object Label7: TLabel
        Left = 120
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = EdDBNome
      end
      object Label5: TLabel
        Left = 468
        Top = 8
        Width = 131
        Height = 13
        Caption = 'Conta (do plano de contas):'
      end
      object EdDBCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCons
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object EdDBNome: TDBEdit
        Left = 120
        Top = 24
        Width = 345
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsCons
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object EdDBGenero: TDBEdit
        Left = 468
        Top = 24
        Width = 209
        Height = 21
        DataField = 'NOMEGENERO'
        DataSource = DsCons
        TabOrder = 2
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 52
      Width = 784
      Height = 413
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox2: TGroupBox
        Left = 368
        Top = 0
        Width = 416
        Height = 413
        Align = alClient
        Caption = ' Leitura por rateio: '
        TabOrder = 1
        object Label14: TLabel
          Left = 8
          Top = 16
          Width = 138
          Height = 13
          Caption = 'Conta de origem (valor base):'
        end
        object Label16: TLabel
          Left = 2
          Top = 372
          Width = 412
          Height = 39
          Align = alBottom
          Alignment = taCenter
          Caption = 
            'Observa'#231#227'o: '#13#10'As altera'#231#245'es da leitura por rateio deste cadastro' +
            ' s'#243' tem efeito na gera'#231#227'o de nova leitura!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
          ExplicitWidth = 380
        end
        object RGFatorBase: TRadioGroup
          Left = 8
          Top = 112
          Width = 294
          Height = 49
          Caption = ' Forma de rateio: (cfe cadastro UH): '
          Columns = 2
          ItemIndex = 1
          Items.Strings = (
            'Moradores'
            'Fra'#231#227'o ideal')
          TabOrder = 3
        end
        object RGPerioBase: TRadioGroup
          Left = 8
          Top = 60
          Width = 294
          Height = 49
          Caption = ' Per'#237'odo de procura do valor base: '
          Columns = 3
          ItemIndex = 1
          Items.Strings = (
            'Anterior'
            'Atual'
            #218'ltimo pago')
          TabOrder = 2
        end
        object EdContaBase: TdmkEditCB
          Left = 8
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBContaBase
          IgnoraDBLookupComboBox = False
        end
        object CBContaBase: TdmkDBLookupComboBox
          Left = 68
          Top = 32
          Width = 233
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContas
          TabOrder = 1
          dmkEditCB = EdContaBase
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object RGCasRat: TRadioGroup
          Left = 8
          Top = 164
          Width = 293
          Height = 49
          Caption = ' Casas decimais ap'#243's (do item selecionado no rateio):  '
          Columns = 7
          ItemIndex = 4
          Items.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6')
          TabOrder = 4
        end
        object CkNaoImpLei: TCheckBox
          Left = 8
          Top = 220
          Width = 165
          Height = 17
          Caption = 'N'#227'o mostrar leitura calculada.'
          TabOrder = 5
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 368
        Height = 413
        Align = alLeft
        TabOrder = 0
        object Label12: TLabel
          Left = 8
          Top = 4
          Width = 60
          Height = 13
          Caption = 'Condom'#237'nio:'
        end
        object LaPreco: TLabel
          Left = 12
          Top = 192
          Width = 93
          Height = 13
          Caption = 'Pre'#231'o $/Impress'#227'o:'
        end
        object Label15: TLabel
          Left = 116
          Top = 192
          Width = 81
          Height = 13
          Caption = 'Arredondamento:'
        end
        object Label23: TLabel
          Left = 8
          Top = 46
          Width = 113
          Height = 13
          Caption = 'Configura'#231#227'o do boleto:'
        end
        object SpeedButton6: TSpeedButton
          Left = 336
          Top = 62
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton6Click
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 20
          Width = 41
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 52
          Top = 20
          Width = 305
          Height = 21
          Color = clWhite
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object GroupBox3: TGroupBox
          Left = 8
          Top = 84
          Width = 349
          Height = 61
          Caption = ' Unidades e convers'#227'o: '
          TabOrder = 4
          object Label4: TLabel
            Left = 168
            Top = 16
            Width = 80
            Height = 13
            Caption = 'Fator convers'#227'o:'
          end
          object Label11: TLabel
            Left = 72
            Top = 16
            Width = 51
            Height = 13
            Caption = 'Impress'#227'o:'
          end
          object Label13: TLabel
            Left = 8
            Top = 16
            Width = 35
            Height = 13
            Caption = 'Leitura:'
          end
          object LaFator: TLabel
            Left = 252
            Top = 34
            Width = 85
            Height = 13
            Caption = 'Impress'#227'o/Leitura'
          end
          object EdUnidFat: TdmkEdit
            Left = 168
            Top = 32
            Width = 81
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0,000001'
            ValMax = '999999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000001'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000001000000000000
            ValWarn = False
          end
          object EdUnidImp: TdmkEdit
            Left = 72
            Top = 32
            Width = 89
            Height = 21
            MaxLength = 10
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdUnidImpChange
          end
          object EdUnidLei: TdmkEdit
            Left = 8
            Top = 32
            Width = 56
            Height = 21
            MaxLength = 5
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdUnidLeiChange
          end
        end
        object RGCasas: TRadioGroup
          Left = 8
          Top = 148
          Width = 349
          Height = 41
          Caption = ' Casas decimais ap'#243's (na medi'#231#227'o):  '
          Columns = 7
          ItemIndex = 4
          Items.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6')
          TabOrder = 5
        end
        object EdPreco: TdmkEdit
          Left = 12
          Top = 208
          Width = 101
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdArredonda: TdmkEdit
          Left = 116
          Top = 208
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0,01'
          ValMax = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1.000000000000000000
          ValWarn = False
        end
        object GroupBox4: TGroupBox
          Left = 8
          Top = 232
          Width = 349
          Height = 61
          Caption = ' Car'#234'ncia de isen'#231#227'o: '
          TabOrder = 8
          object Label17: TLabel
            Left = 8
            Top = 16
            Width = 63
            Height = 13
            Caption = 'Teto m'#225'ximo:'
          end
          object EdCarencia: TdmkEdit
            Left = 8
            Top = 32
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object CkDifCaren: TCheckBox
            Left = 100
            Top = 32
            Width = 229
            Height = 17
            Caption = 'Cobrar somente o medido al'#233'm da car'#234'ncia.'
            TabOrder = 1
          end
        end
        object GroupBox5: TGroupBox
          Left = 8
          Top = 296
          Width = 349
          Height = 105
          Caption = ' Exporta'#231#227'o de dados: '
          TabOrder = 9
          object Label18: TLabel
            Left = 8
            Top = 60
            Width = 95
            Height = 13
            Caption = 'Forma de cobran'#231'a:'
          end
          object RGExport_Apl: TRadioGroup
            Left = 2
            Top = 15
            Width = 345
            Height = 42
            Align = alTop
            Caption = ' Aplicativo: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Nenhum'
              'Produsys')
            TabOrder = 0
            OnClick = RGExport_AplClick
          end
          object EdExport_Med: TdmkEdit
            Left = 8
            Top = 76
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdExport_MedChange
          end
          object EdExport_Med_TXT: TEdit
            Left = 64
            Top = 76
            Width = 277
            Height = 21
            ReadOnly = True
            TabOrder = 2
            Text = '???'
          end
        end
        object EdCNAB_Cfg: TdmkEditCB
          Left = 8
          Top = 62
          Width = 41
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCNAB_Cfg
          IgnoraDBLookupComboBox = False
        end
        object CBCNAB_Cfg: TdmkDBLookupComboBox
          Left = 52
          Top = 62
          Width = 282
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCNAB_Cfg
          TabOrder = 3
          dmkEditCB = EdCNAB_Cfg
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object GroupBox6: TGroupBox
      Left = 0
      Top = 496
      Width = 784
      Height = 70
      Align = alBottom
      TabOrder = 2
      object Panel9: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel10: TPanel
          Left = 636
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn2: TBitBtn
            Tag = 15
            Left = 4
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn2Click
          end
        end
        object BitBtn1: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn1Click
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 268
        Height = 32
        Caption = 'Consumos por Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 268
        Height = 32
        Caption = 'Consumos por Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 268
        Height = 32
        Caption = 'Consumos por Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsCons: TDataSource
    DataSet = QrCons
    Left = 728
    Top = 61
  end
  object QrCons: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrConsBeforeOpen
    AfterOpen = QrConsAfterOpen
    AfterScroll = QrConsAfterScroll
    SQL.Strings = (
      'SELECT con.Nome NOMEGENERO, cns.* '
      'FROM cons cns'
      'LEFT JOIN contas con ON con.Codigo=cns.Genero'
      'WHERE cns.Codigo > 0')
    Left = 700
    Top = 61
    object QrConsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrConsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrConsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrConsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrConsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrConsCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrConsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrConsNOMEGENERO: TWideStringField
      FieldName = 'NOMEGENERO'
      Size = 50
    end
    object QrConsGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrConsExport_Tip: TSmallintField
      FieldName = 'Export_Tip'
    end
  end
  object QrConsPer: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrConsPerAfterScroll
    OnCalcFields = QrConsPerCalcFields
    SQL.Strings = (
      'SELECT * FROM consper'
      'WHERE Codigo=:P0'
      'ORDER BY Periodo DESC')
    Left = 700
    Top = 117
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConsPerCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConsPerControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrConsPerPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrConsPerLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrConsPerDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrConsPerDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrConsPerUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrConsPerUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrConsPerPERIODO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Size = 8
      Calculated = True
    end
  end
  object DsConsPer: TDataSource
    DataSet = QrConsPer
    Left = 728
    Top = 117
  end
  object QrConsIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM consits'
      'WHERE Controle=:P0')
    Left = 700
    Top = 145
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConsItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConsItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrConsItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrConsItsApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrConsItsMedAnt: TFloatField
      FieldName = 'MedAnt'
    end
    object QrConsItsMedAtu: TFloatField
      FieldName = 'MedAtu'
    end
    object QrConsItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrConsItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrConsItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrConsItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrConsItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsConsIts: TDataSource
    DataSet = QrConsIts
    Left = 728
    Top = 145
  end
  object PMProduto: TPopupMenu
    Left = 193
    Top = 49
    object Incluinovoproduto1: TMenuItem
      Caption = '&Inclui novo produto'
      OnClick = Incluinovoproduto1Click
    end
    object Alteraprodutoatual1: TMenuItem
      Caption = '&Altera produto atual'
      OnClick = Alteraprodutoatual1Click
    end
    object Excluiprodutoatual1: TMenuItem
      Caption = '&Exclui produto atual'
      Enabled = False
    end
  end
  object PMPeriodo: TPopupMenu
    OnPopup = PMPeriodoPopup
    Left = 249
    Top = 49
    object Incluinovoperodo1: TMenuItem
      Caption = '&Inclui novo per'#237'odo'
      Enabled = False
      OnClick = Incluinovoperodo1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Excluiperodo1: TMenuItem
      Caption = '&Exclui per'#237'odo selecionado'
      Enabled = False
      OnClick = Excluiperodo1Click
    end
  end
  object QrDup: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Periodo '
      'FROM consper'
      'WHERE Codigo=:P0'
      'AND Periodo=:P1')
    Left = 417
    Top = 49
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDupPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
  object PMLeitura: TPopupMenu
    Left = 277
    Top = 49
    object Adicionarcondomnio1: TMenuItem
      Caption = '&Adicionar condom'#237'nio'
    end
    object N2: TMenuItem
      Caption = '-'
    end
  end
  object QrConsPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMECLI, csp.*'
      'FROM consprc csp'
      'LEFT JOIN cond cnd ON cnd.Codigo=csp.Cond'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE csp.Codigo=:P0')
    Left = 700
    Top = 89
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConsPrcNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrConsPrcCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'consprc.Codigo'
      Required = True
    end
    object QrConsPrcControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'consprc.Controle'
      Required = True
    end
    object QrConsPrcCond: TIntegerField
      FieldName = 'Cond'
      Origin = 'consprc.Cond'
      Required = True
    end
    object QrConsPrcPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'consprc.Preco'
      Required = True
      DisplayFormat = '#,###,##0.0000'
    end
    object QrConsPrcLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'consprc.Lk'
    end
    object QrConsPrcDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'consprc.DataCad'
    end
    object QrConsPrcDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'consprc.DataAlt'
    end
    object QrConsPrcUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'consprc.UserCad'
    end
    object QrConsPrcUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'consprc.UserAlt'
    end
    object QrConsPrcCasas: TSmallintField
      FieldName = 'Casas'
      Origin = 'consprc.Casas'
      Required = True
    end
    object QrConsPrcUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Origin = 'consprc.UnidLei'
      Required = True
      Size = 5
    end
    object QrConsPrcUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Origin = 'consprc.UnidImp'
      Required = True
      Size = 5
    end
    object QrConsPrcUnidFat: TFloatField
      FieldName = 'UnidFat'
      Origin = 'consprc.UnidFat'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrConsPrcContaBase: TIntegerField
      FieldName = 'ContaBase'
      Origin = 'consprc.ContaBase'
    end
    object QrConsPrcFatorBase: TSmallintField
      FieldName = 'FatorBase'
      Origin = 'consprc.FatorBase'
      Required = True
    end
    object QrConsPrcCasRat: TSmallintField
      FieldName = 'CasRat'
      Origin = 'consprc.CasRat'
    end
    object QrConsPrcArredonda: TFloatField
      FieldName = 'Arredonda'
      Origin = 'consprc.Arredonda'
      Required = True
    end
    object QrConsPrcNaoImpLei: TSmallintField
      FieldName = 'NaoImpLei'
      Origin = 'consprc.NaoImpLei'
      Required = True
    end
    object QrConsPrcPerioBase: TSmallintField
      FieldName = 'PerioBase'
      Origin = 'consprc.PerioBase'
      Required = True
    end
    object QrConsPrcCarencia: TFloatField
      FieldName = 'Carencia'
      Origin = 'consprc.Carencia'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrConsPrcDifCaren: TSmallintField
      FieldName = 'DifCaren'
      Origin = 'consprc.DifCaren'
      Required = True
    end
    object QrConsPrcExport_Apl: TSmallintField
      FieldName = 'Export_Apl'
    end
    object QrConsPrcExport_Med: TSmallintField
      FieldName = 'Export_Med'
    end
    object QrConsPrcCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrConsPrcCNAB_Cfg_Txt: TWideStringField
      FieldName = 'CNAB_Cfg_Txt'
      Size = 50
    end
  end
  object DsConsPrc: TDataSource
    DataSet = QrConsPrc
    Left = 728
    Top = 89
  end
  object PMCond: TPopupMenu
    Left = 221
    Top = 49
    object Incluicondomnio1: TMenuItem
      Caption = '&Inclui condom'#237'nio'
      OnClick = Incluicondomnio1Click
    end
    object Alteradadosdocondomnioselecionado1: TMenuItem
      Caption = '&Altera dados do condom'#237'nio selecionado'
      OnClick = Alteradadosdocondomnioselecionado1Click
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 361
    Top = 49
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 389
    Top = 49
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 544
    Top = 428
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 516
    Top = 428
  end
  object QrCNAB_Cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT abc.Codigo, abc.Sigla, abc.Nome'
      'FROM arrebai abi'
      'LEFT JOIN arrebac abc ON abc.Codigo=abi.Codigo'
      'WHERE Cond=:P0'
      'ORDER BY Nome')
    Left = 488
    Top = 428
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
end
