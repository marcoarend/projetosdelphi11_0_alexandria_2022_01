unit CondGerLeiEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Mask, DBCtrls, dmkEdit, dmkGeral,
  UMySQLModule, dmkImage, UnDmkEnums, UnInternalConsts;

type
  TFmCondGerLeiEdit = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Panel3: TPanel;
    Label9: TLabel;
    EdLeiAnt: TdmkEdit;
    EdLeiAtu: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    EdPreco: TdmkEdit;
    Label12: TLabel;
    EdConsumo: TdmkEdit;
    Label13: TLabel;
    EdValor: TdmkEdit;
    DBText1: TDBText;
    Label8: TLabel;
    EdConsum2: TdmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdLeiAntChange(Sender: TObject);
    procedure EdLeiAtuChange(Sender: TObject);
    procedure EdPrecoChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaConsumoECusto;
  public
    { Public declarations }
    FControle, FLancto, FCasas: Integer;
    FUnidFat: Double;
    FTabLctA, FTabCnsA: String;
  end;

  var
    FmCondGerLeiEdit: TFmCondGerLeiEdit;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmCondGerLeiEdit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerLeiEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerLeiEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCondGerLeiEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerLeiEdit.FormShow(Sender: TObject);
begin
  EdLeiAnt.DecimalSize  := FCasas;
  EdLeiAtu.DecimalSize  := FCasas;
  EdPreco.DecimalSize   := FCasas;
  EdConsumo.DecimalSize := FCasas;
  EdConsum2.DecimalSize := FCasas;
end;

procedure TFmCondGerLeiEdit.EdLeiAntChange(Sender: TObject);
begin
  CalculaConsumoECusto;
end;

procedure TFmCondGerLeiEdit.EdLeiAtuChange(Sender: TObject);
begin
  CalculaConsumoECusto;
end;

procedure TFmCondGerLeiEdit.EdPrecoChange(Sender: TObject);
begin
  CalculaConsumoECusto;
end;

procedure TFmCondGerLeiEdit.CalculaConsumoECusto;
var
  Ant, Atu, Prc, Uso, Val, Us2: Double;
begin
  Ant := Geral.DMV(EdLeiAnt.Text);
  Atu := Geral.DMV(EdLeiAtu.Text);
  Prc := Geral.DMV(EdPreco.Text);
  //
  Uso := Atu - Ant;
  Us2 := Uso * FUnidFat;
  if Us2 <= 0 then
    Val := 0
  else
    Val := Trunc(((Us2 * Prc) + 0.005) * 100) / 100;
  //
  EdConsumo.ValueVariant := Uso;
  EdConsum2.ValueVariant := Us2;
  EdValor.Text           := Geral.FFT(Val, 2, siPositivo);
  BtOK.Enabled           := Us2 >= 0;
end;

procedure TFmCondGerLeiEdit.BtOKClick(Sender: TObject);
var
  MedAnt, MedAtu, Consumo, Preco, Valor: Double;
begin
  MedAnt  := EdLeiAnt.ValueVariant;
  MedAtu  := EdLeiAtu.ValueVariant;
  Consumo := EdConsumo.ValueVariant;
  Preco   := EdPreco.ValueVariant;
  Valor   := EdValor.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, FTabCnsA, False,
    ['MedAnt', 'MedAtu', 'Consumo', 'Preco', 'Valor'], ['Controle'],
    [MedAnt, MedAtu, Consumo, Preco, Valor], [FControle], True) then
  begin
    if FLancto <> 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, FTabLctA, False,
        ['Credito'], ['FatID', 'Controle'], [Valor],
        [VAR_FATID_0601, FLancto], True);
    end;
    Close;
  end;
end;

end.
