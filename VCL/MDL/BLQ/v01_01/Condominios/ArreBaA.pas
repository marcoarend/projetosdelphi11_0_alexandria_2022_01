unit ArreBaA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, Db, mySQLDbTables,
  dmkGeral, MyDBCheck, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmArreBaA = class(TForm)
    Panel1: TPanel;
    StaticText2: TStaticText;
    DBGrid1: TDBGrid;
    TbArreBaA: TmySQLTable;
    DsArreBaA: TDataSource;
    QrArreBaC: TmySQLQuery;
    QrArreBaCCodigo: TIntegerField;
    QrArreBaCConta: TIntegerField;
    QrArreBaCNome: TWideStringField;
    DsArreBaC: TDataSource;
    QrConta: TmySQLQuery;
    QrContaZeros: TLargeintField;
    TbArreBaAConta: TIntegerField;
    TbArreBaAArreBaI: TIntegerField;
    TbArreBaAArreBaC: TIntegerField;
    TbArreBaAValor: TFloatField;
    TbArreBaATexto: TWideStringField;
    TbArreBaAAdiciona: TSmallintField;
    TbArreBaANOMECONTABASE: TWideStringField;
    DBGrid2: TDBGrid;
    TbArreBaAUni: TmySQLTable;
    DsArreBaAUni: TDataSource;
    TbArreBaAUniSeq: TIntegerField;
    TbArreBaAUniApto: TIntegerField;
    TbArreBaAUniUnidade: TWideStringField;
    TbArreBaAUniValor: TFloatField;
    TbArreBaAUniAdiciona: TSmallintField;
    TbArreBaASeq: TIntegerField;
    TbArreBaAUniPropriet: TIntegerField;
    TbArreBaAUniNomePropriet: TWideStringField;
    TbArreBaAUniTextoCota: TWideStringField;
    TbArreBaAUniNaoArreSobre: TIntegerField;
    TbArreBaAUniCalculo: TSmallintField;
    TbArreBaAUniCotas: TFloatField;
    TbArreBaAUniDescriCota: TWideStringField;
    QrDuplic: TmySQLQuery;
    QrDuplicITENS: TLargeintField;
    QrDuplicConta: TIntegerField;
    QrDuplicValor: TFloatField;
    TbArreBaAUniNaoArreRisco: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    QrLoc: TmySQLQuery;
    BitBtn4: TBitBtn;
    TbArreBaACNAB_Cfg: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbArreBaAAfterInsert(DataSet: TDataSet);
    procedure TbArreBaASitCobrValidate(Sender: TField);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure TbArreBaAAfterScroll(DataSet: TDataSet);
    procedure TbArreBaAUniBeforePost(DataSet: TDataSet);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure TbArreBaABeforePost(DataSet: TDataSet);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetaTodos(Marca: Boolean);
    function ImpedePelosDuplicados(): Boolean;
  public
    { Public declarations }
    FTabAriA, FNomeCliente, FArreBaA, FArreBaAUni: String;
    FCodigo, FCond, FCodiCliente: Integer;
  end;

  var
  FmArreBaA: TFmArreBaA;

implementation

uses Module, UMySQLModule, UnInternalConsts, MyVCLSkin, ModuleGeral, UnMyObjects,
  UnBloqCond_Jan;

{$R *.DFM}

procedure TFmArreBaA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmArreBaA.FormActivate(Sender: TObject);
begin
  StaticText2.Caption := FNomeCliente;
  MyObjects.CorIniComponente;
end;

procedure TFmArreBaA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmArreBaA.FormShow(Sender: TObject);
begin
  TbArreBaAUni.TableName := FArreBaAUni;
  TbArreBaA.TableName    := FArreBaA;
  //
  UMyMod.AbreQuery(QrArreBaC, DMod.MyDB);
  UMyMod.AbreTable(TbArreBaA, DModG.MyPID_DB);
  UMyMod.AbreTable(TbArreBaAUni, DModG.MyPID_DB);
end;

function TFmArreBaA.ImpedePelosDuplicados: Boolean;
begin
  UMyMod.AbreQuery(QrDuplic, DModG.MyPID_DB);
  //
  Result := (QrDuplic.RecordCount > 0) and (QrDuplicITENS.Value > 1);
  //
  if Result then
    Result := not UBloqCond_Jan.MostraArreBaADupl;
end;

procedure TFmArreBaA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmArreBaA.TbArreBaAAfterInsert(DataSet: TDataSet);
begin
  TbArreBaA.Cancel;
end;

procedure TFmArreBaA.TbArreBaASitCobrValidate(Sender: TField);
begin
  if Sender.AsString = '' then Sender.AsInteger := 0;
end;

procedure TFmArreBaA.BtOKClick(Sender: TObject);
var
  Controle, CNAB_Cfg: Integer;
  Texto, Msg: String;
begin
  if ImpedePelosDuplicados() then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrConta, DModG.MyPID_DB, [
    'SELECT COUNT(Adiciona) Zeros ',
    'FROM ' + FArreBaAUni,
    'WHERE Adiciona = 1 ',
    '']);
  if QrContaZeros.Value = 0 then
  begin
    Geral.MB_Aviso('Nenhum item foi selecionado!');
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrConta, DModG.MyPID_DB, [
    'SELECT COUNT(Valor) Zeros ',
    'FROM ' + FArreBaAUni,
    'WHERE Valor < 0.01 AND Adiciona=1 ',
    '']);
  if QrContaZeros.Value > 0 then
    if Geral.MB_Pergunta('Existem ' + Geral.FF0(QrContaZeros.Value) +
      ' itens selecionados sem valor informado.' + sLineBreak +
      'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  //
  TbArreBaA.First;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ' + FTabAriA + ' SET ');
  Dmod.QrUpd.SQL.Add('ComoAdd=1, Conta=:P0, Valor=:P1, Texto=:P2, ');
  Dmod.QrUpd.SQL.Add('ArreBaC=:P3, ArreBaI=:P4, Apto=:P5, Propriet=:P6, ');
  Dmod.QrUpd.SQL.Add('NaoArreRisco=:P7, CNAB_Cfg=:P8, ');
  Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
  //
  Msg := '';
  //
  while not TbArreBaA.Eof do
  begin
    //if TbArreBaAAdiciona.Value = 1 then
    //begin
      TbArreBaAUni.First;
      while not TbArreBaAUni.Eof do
      begin
        if TbArreBaAUniAdiciona.Value = 1 then
        begin
          CNAB_Cfg := TbArreBaACNAB_Cfg.Value;
          //
          if CNAB_Cfg <> 0 then
          begin
            Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                        FTabAriA, TAB_ARI, 'Controle');
            //
            Texto := TbArreBaATexto.Value;
            //
            if Trim(TbArreBaAUniTextoCota.Value) <> '' then
              Texto := Texto + ' - ' + Trim(TbArreBaAUniTextoCota.Value);
            //
            Dmod.QrUpd.Params[00].AsInteger := TbArreBaAConta.Value;
            Dmod.QrUpd.Params[01].AsFloat   := TbArreBaAUniValor.Value;
            Dmod.QrUpd.Params[02].AsString  := Texto;
            Dmod.QrUpd.Params[03].AsInteger := TbArreBaAArreBaC.Value;
            Dmod.QrUpd.Params[04].AsInteger := TbArreBaAArreBaI.Value;
            Dmod.QrUpd.Params[05].AsInteger := TbArreBaAUniApto.Value;
            Dmod.QrUpd.Params[06].AsInteger := TbArreBaAUniPropriet.Value;
            Dmod.QrUpd.Params[07].AsInteger := TbArreBaAUniNaoArreRisco.Value;
            Dmod.QrUpd.Params[08].AsInteger := CNAB_Cfg;
            //
            Dmod.QrUpd.Params[09].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
            Dmod.QrUpd.Params[10].AsInteger := VAR_USUARIO;
            Dmod.QrUpd.Params[11].AsInteger := FCodigo;
            Dmod.QrUpd.Params[12].AsInteger := Controle;
            //
            Dmod.QrUpd.ExecSQL;
          end;
        end;
        TbArreBaAUni.Next;
      end;
    //end;
    //
    TbArreBaA.Next;
  end;
  if Msg <> '' then
    Geral.MB_Aviso(Msg);
  Close;
end;

procedure TFmArreBaA.BitBtn1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a retirada do item "' +
    TbArreBaANOMECONTABASE.Value + '" destes itens?') = ID_YES
  then
    TbArreBaA.Delete;
end;

procedure TFmArreBaA.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Adiciona' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbArreBaAAdiciona.Value);
end;

procedure TFmArreBaA.BitBtn2Click(Sender: TObject);
begin
  SetaTodos(True);
end;

procedure TFmArreBaA.BitBtn3Click(Sender: TObject);
begin
  SetaTodos(False);
end;

procedure TFmArreBaA.BitBtn4Click(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  if (TbArreBaA.State <> dsInactive) and (TbArreBaA.RecordCount > 0) then
  begin
    Codigo   := TbArreBaAArreBaC.Value;
    Controle := TbArreBaAArreBaI.Value;
    //
    UBloqCond_Jan.CadastroDeArrecadacoes(Codigo, Controle);
  end;
  Close;
end;

procedure TFmArreBaA.SetaTodos(Marca: Boolean);
var
  Codigo: Integer;
begin
  Codigo := TbArreBaAArreBaI.Value;
  TbArreBaA.First;
  while not TbArreBaA.Eof do
  begin
    TbArreBaA.Edit;
    TbArreBaAAdiciona.Value := Geral.BoolToInt(Marca);
    TbArreBaA.Post;
    //
    TbArreBaA.Next;
  end;
  TbArreBaA.Locate('ArreBaI', Codigo, []);
end;

procedure TFmArreBaA.DBGrid1CellClick(Column: TColumn);
begin
  if (DBGrid1.SelectedField.Name = 'TbArreBaAAdiciona') then
  begin
    TbArreBaA.Edit;
    if TbArreBaAAdiciona.Value = 0 then
      TbArreBaAAdiciona.Value := 1
    else
      TbArreBaAAdiciona.Value := 0;
    TbArreBaA.Post;
  end;
end;

procedure TFmArreBaA.TbArreBaAAfterScroll(DataSet: TDataSet);
begin
  UMyMod.AbreTable(TbArreBaAUni, DModG.MyPID_DB);
  //
  TbArreBaAUni.Filtered := False;
  TbArreBaAUni.Filter := 'Seq='+IntToStr(TbArreBaASeq.Value);
  TbArreBaAUni.Filtered := True;
  TbArreBaAUni.Refresh;
end;

procedure TFmArreBaA.TbArreBaAUniBeforePost(DataSet: TDataSet);
begin
  if TbArreBaAUni.State <> dsEdit then TbArreBaAUni.Cancel;
end;

procedure TFmArreBaA.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Adiciona' then
    MeuVCLSkin.DrawGrid(DBGrid2, Rect, 1, TbArreBaAUniAdiciona.Value);
end;

procedure TFmArreBaA.DBGrid2CellClick(Column: TColumn);
begin
  if (DBGrid2.SelectedField.Name = 'TbArreBaAUniAdiciona') then
  begin
    TbArreBaAUni.Edit;
    if TbArreBaAUniAdiciona.Value = 0 then
      TbArreBaAUniAdiciona.Value := 1
    else
      TbArreBaAUniAdiciona.Value := 0;
    TbArreBaAUni.Post;
  end;
end;

procedure TFmArreBaA.TbArreBaABeforePost(DataSet: TDataSet);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DModG.MyPID_DB, [
    'UPDATE ' + FArreBaAUni,
    'SET Adiciona=' + Geral.FF0(TbArreBaAAdiciona.Value),
    'WHERE Seq=' + Geral.FF0(TbArreBaASeq.Value),
    '']);
  TbArreBaAUni.Refresh;
end;

end.

