object FmArreBaADupl: TFmArreBaADupl
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-023 :: Arrecada'#231#245'es Duplicadas'
  ClientHeight = 613
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 451
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 424
      Height = 451
      Align = alLeft
      DataSource = DsDuplic
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'ITENS'
          Title.Caption = 'Itens'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTA'
          Title.Caption = 'Descri'#231#227'o da conta'
          Width = 246
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end>
    end
    object DBGrid2: TDBGrid
      Left = 424
      Top = 0
      Width = 584
      Height = 451
      Align = alClient
      DataSource = DsArreBaA
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'ArreBaC'
          Title.Caption = 'Arrec.'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_ARREBAC'
          Title.Caption = 'Descri'#231#227'o arrecada'#231#227'o'
          Width = 215
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Width = 218
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 499
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 647
        Height = 16
        Caption = 
          'Confirme para incluir todas arrecada'#231#245'es (inclusive duplicadas) ' +
          'e desista para n'#227'o incluir nenhuma arrecada'#231#227'o!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 647
        Height = 16
        Caption = 
          'Confirme para incluir todas arrecada'#231#245'es (inclusive duplicadas) ' +
          'e desista para n'#227'o incluir nenhuma arrecada'#231#227'o!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 543
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas')
    Left = 68
    Top = 8
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 96
    Top = 8
  end
  object QrDuplic: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterScroll = QrDuplicAfterScroll
    SQL.Strings = (
      'SELECT COUNT(aba.Conta) ITENS, aba.Conta, aba.Valor'
      'FROM arrebaa aba'
      'GROUP BY aba.Conta, aba.Valor'
      'ORDER BY ITENS Desc')
    Left = 8
    Top = 8
    object QrDuplicITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrDuplicConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrDuplicValor: TFloatField
      FieldName = 'Valor'
    end
    object QrDuplicNOMECONTA: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECONTA'
      LookupDataSet = QrContas
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Conta'
      Size = 100
      Lookup = True
    end
  end
  object DsDuplic: TDataSource
    DataSet = QrDuplic
    Left = 36
    Top = 8
  end
  object QrArreBaA: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT aba.*'
      'FROM arrebaa aba'
      'WHERE Conta=:P0'
      'AND Valor=:P1'
      '')
    Left = 128
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrArreBaASeq: TIntegerField
      FieldName = 'Seq'
    end
    object QrArreBaAConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrArreBaAArreBaI: TIntegerField
      FieldName = 'ArreBaI'
    end
    object QrArreBaAArreBaC: TIntegerField
      FieldName = 'ArreBaC'
    end
    object QrArreBaAValor: TFloatField
      FieldName = 'Valor'
    end
    object QrArreBaATexto: TWideStringField
      FieldName = 'Texto'
      Size = 40
    end
    object QrArreBaAAdiciona: TSmallintField
      FieldName = 'Adiciona'
    end
    object QrArreBaANOME_ARREBAC: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_ARREBAC'
      LookupDataSet = QrArreBaC
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'ArreBaC'
      Size = 50
      Lookup = True
    end
  end
  object DsArreBaA: TDataSource
    DataSet = QrArreBaA
    Left = 156
    Top = 8
  end
  object QrArreBaC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM arrebac')
    Left = 188
    Top = 8
  end
  object DsArreBaC: TDataSource
    DataSet = QrArreBaC
    Left = 216
    Top = 8
  end
end
