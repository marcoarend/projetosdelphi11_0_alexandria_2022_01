unit UnBloquetosCond;

interface

uses System.Classes, mySQLDbTables, UMySQLModule, dmkGeral, ModuleGeral,
  dmkImage, Forms, Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids,
  AdvToolBar, DB, UnDmkProcFunc, ExtCtrls, dmkDBGrid, Protocolo, Math, Dialogs,
  UnDmkEnums, dmkDBGridZTO;

type
  TUnBloquetosCond = class(TObject)
  private
    function  AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                QueryUpd: TmySQLQuery; Database: TmySQLDatabase;
                DataSel: TDateTime; Prebol: Boolean; TabLctA: String): Boolean;
    function  InsereLancto(QueryUpd: TmySQLQuery; Database: TmySQLDatabase;
                Vencto: TDateTime; Boleto, Valor, JurosPerc, MultaPerc: Double;
                Empresa, Propriet, Periodo, CartEmiss, Genero, Apto, Controle,
                FatID: Integer; Descri, TabLctA, TabOrig: String): Integer;
    function  GeraNovoNumeroDeBoleto(const Cond, Cliente, Banco: Integer;
                var Boleto: Double; const EscolheNum: Boolean;
                const QueryLoc, QueryCNAB_Cfg, QueryUpd: TmySQLQuery;
                const Database: TmySQLDatabase): Boolean;
    function  GeraBoletoAtual(Cond, Cliente, Periodo: Integer; QueryBoletos,
                QueryBoletosIts, QueryLoc, QueryUpd: TmySQLQuery;
                Database: TmySQLDatabase; EscolheNum: Boolean;
                Vencto: TDateTime; TabLctA, TabArre, TabCons: String): Boolean;
    function  NaoContinuaPelaCarteira(Carteira, CAR_TIPODOC, CART_ATIVO: Integer): Boolean;
    function  VerificaSeHouveEmissaoPosterior(QueryAux: TmySQLQuery;
                Database: TmySQLDatabase; TabAriA, TabPrvA, TabCnsA: String;
                Cond, Periodo: Integer): Boolean;
    function  DesfazerBoletoAtual(QueryBoletosIts, QueryAux, QueryUpd: TmySQLQuery;
                Database: TmySQLDatabase; TabLctA, TabAriA, TabCnsA: String): Boolean;
    function  VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts,
                QueryAux: TmySQLQuery; Database: TmySQLDatabase; TabLctA: String): Boolean;
    function  VerificaSeAlgumItemDoBoletoEstaPago(QueryBoletosIts, QueryAux: TmySQLQuery;
                Database: TmySQLDatabase; TabLctA: String): Boolean;
  public
    function  VerificaSeLctEstaoNaTabLctA(TabLctA: String; Lancto: Integer;
                QueryAux: TmySQLQuery; Database: TmySQLDatabase; var Msg: String): Boolean;
    function  VerificaSeLancamentoEstaPago(Lancto: Integer; QueryAux: TmySQLQuery;
                Database: TmySQLDatabase; TabLctA: String; var Msg: String): Boolean;
    function  ValidaArrecadacaoConsumoCond(Cond, CNAB_Cfg: Integer;
                Query: TmySQLQuery; Database: TmySQLDatabase; var Msg: String): Boolean;
    procedure ReopenCNAB_Cfg(Query: TmySQLQuery; Database: TmySQLDatabase;
                Cliente: Integer; Codigo: Integer = 0);
    procedure MostraCNAB_Cfg(Codigo: Integer);
    function  AlteraVencimentoPreBol(Periodo, DiaVencto: Integer;
                Prebol: Boolean; Tipo: TSelType; Grade: TDBGrid; QueryBoletos,
                QueryBoletosIts, QueryUpd, QueryAux: TmySQLQuery;
                Database: TmySQLDatabase; Progress: TProgressBar;
                TabLctA: String): Boolean;
    function  GeraBoletosAbertos(Cond, Cliente, Periodo, DiaVencto: Integer;
                QueryBoletos, QueryBoeltosIts, QueryLoc, QueryUpd: TmySQLQuery;
                Database: TmySQLDatabase; Grade: TDBGrid; Selecao: TSelType;
                ProgressBar: TProgressBar; TabLctA, TabArre, TabCons: String): Boolean;
    function   DesfazerBoletos(Selecao: TSelType; QueryUpd, QueryAux,
                 QueryBoletos, QueryBoletosIts: TmySQLQuery;
                 Database: TmySQLDatabase; Grade: TDBGrid; TabLctA, TabPrvA,
                 TabAriA, TabCnsA: String; Cond, Periodo: Integer;
                 Progress: TProgressBar): Boolean;
    //Migra��es
    procedure ConverteCondBoletosParaCNABcfg(DataBase: TMySQLDataBase; PB1: TProgressBar);
    procedure AtualizaDadosProtocolosNosBoletos(DataBase: TMySQLDataBase; PB1: TProgressBar);
  end;

var
  UBloquetosCond: TUnBloquetosCond;

const
  FArre_FatID = 600;
  FCons_FatID = 601;

implementation

uses MyDBCheck, DmkDAC_PF, CNAB_Cfg, Module, ModuleCond, ModuleBloq, UnBancos,
  UnBco_Rem, UnMLAGeral, UnInternalConsts, UnFinanceiro;

{ TUnBloquetos }

function TUnBloquetosCond.InsereLancto(QueryUpd: TmySQLQuery;
  Database: TmySQLDatabase; Vencto: TDateTime; Boleto, Valor, JurosPerc,
  MultaPerc: Double; Empresa, Propriet, Periodo, CartEmiss, Genero, Apto,
  Controle, FatID: Integer; Descri, TabLctA, TabOrig: String): Integer;
var
  Res: Boolean;
begin
  Result := 0;
  //
  UFinanceiro.LancamentoDefaultVARS;
  //
  FLAN_Data       := Geral.FDT(DModG.ObtemAgora(), 1);
  FLAN_Tipo       := 2;
  FLAN_Documento  := Trunc(Boleto);
  FLAN_Credito    := Valor;
  FLAN_MoraDia    := JurosPerc;
  FLAN_Multa      := MultaPerc;
  FLAN_Carteira   := CartEmiss;
  FLAN_Genero     := Genero;
  FLAN_Cliente    := Propriet;
  FLAN_CliInt     := Empresa;
  FLAN_Depto      := Apto;
  FLAN_ForneceI   := Propriet;
  FLAN_Vencimento := Geral.FDT(Vencto, 1);
  FLAN_Mez        := dmkPF.PeriodoToAnoMes(Periodo);
  FLAN_FatID      := FatID;
  FLAN_FatNum     := Trunc(Boleto);
  FLAN_FatParcela := 1;
  FLAN_Descricao  := Descri;
  //
  FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', TabLctA, LAN_CTOS, 'Controle');
  //
  {$IFDEF DEFINE_VARLCT}
    Res := UFinanceiro.InsereLancamento(TabLctA);
  {$ELSE}
    Res := UFinanceiro.InsereLancamento;
  {$ENDIF}
  //
  if Res then
  begin
    QueryUpd.SQL.Clear;
    QueryUpd.SQL.Add('UPDATE ' + TabOrig + ' SET Lancto=:P0 WHERE Controle=:P1');
    QueryUpd.Params[00].AsInteger := FLAN_Controle;
    QueryUpd.Params[01].AsInteger := Controle;
    QueryUpd.ExecSQL;
    //
    Result := FLAN_Controle;
  end;
end;

function TUnBloquetosCond.GeraNovoNumeroDeBoleto(const Cond, Cliente,
  Banco: Integer; var Boleto: Double; const EscolheNum: Boolean;
  const QueryLoc, QueryCNAB_Cfg, QueryUpd: TmySQLQuery;
  const Database: TmySQLDatabase): Boolean;
var
  CNAB_Cfg, NosNumFxaU, NosNumFxaI, NosNumFxaF: Integer;
  BloqStr, LayoutRem: String;
begin
  CNAB_Cfg := QueryCNAB_Cfg.FieldByName('Codigo').AsInteger;
  //
  if (EscolheNum) and (CNAB_Cfg = 0) then //Somente no cond CNAB_Cfg j� tem um campo para isso
  begin
    BloqStr := '0';
    //
    if InputQuery('N�mero de Bloqueto (Manual)',
      'Informe o n� do bloqueto (deixe zero p/ n� autom�tico):', BloqStr) then
    begin
      try
        Boleto := Geral.DMV(BloqStr);
        //
        if Boleto > 0 then
        begin
          if DmBloq.BoletoJaExiste(Boleto, Cond, DModG.ObtemAgora - 1000) then
          begin
            Boleto := 0;
            Result := False;
            Exit;
          end;
        end;
      except
        Boleto := 0;
      end;
    end;
  end;
  //
  if CNAB_Cfg = 0 then
  begin
    if EscolheNum and (Boleto > 0) and (CNAB_Cfg = 0) then
    begin
      Boleto := Boleto - 1
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
        'SELECT Bloqueto ',
        'FROM bancosblo ',
        'WHERE Codigo=' + Geral.FF0(Banco),
        'AND Entidade=' + Geral.FF0(Cliente),
        '']);
      //
      Boleto := QueryLoc.FieldByName('Bloqueto').AsFloat;
    end;
    //
    NosNumFxaU := QueryCNAB_Cfg.FieldByName('NosNumFxaU').AsInteger;
    NosNumFxaI := QueryCNAB_Cfg.FieldByName('NosNumFxaI').AsInteger;
    NosNumFxaF := QueryCNAB_Cfg.FieldByName('NosNumFxaF').AsInteger;
    LayoutRem  := QueryCNAB_Cfg.FieldByName('LayoutRem').AsString;
    //
    UBancos.GeraBloquetoNumero(Banco, Boleto, True, Geral.IntToBool(NosNumFxaU),
      NosNumFxaI, NosNumFxaF, Boleto, LayoutRem);
    //
    if Boleto <> 0 then
    begin
      QueryUpd.SQL.Clear;
      if QueryLoc.RecordCount = 0 then
      begin
        QueryUpd.SQL.Add('INSERT INTO bancosblo SET ');
        QueryUpd.SQL.Add('Bloqueto=:0, Codigo=:P1, Entidade=:P2');
        QueryUpd.SQL.Add('');
      end else begin
        QueryUpd.SQL.Add('UPDATE bancosblo SET ');
        QueryUpd.SQL.Add('Bloqueto=:0 WHERE Codigo=:P1 AND Entidade=:P2');
        QueryUpd.SQL.Add('');
      end;
      QueryUpd.Params[00].AsFloat   := Boleto;
      QueryUpd.Params[01].AsInteger := Banco;
      QueryUpd.Params[02].AsInteger := DmCond.QrCondCliente.Value;
      //
      QueryUpd.ExecSQL;
    end;
  end else
  begin
    Boleto := UBco_Rem.ObtemProximoNossoNumero(CNAB_Cfg, False);
  end;
  Result := Boleto > 0;
end;

function TUnBloquetosCond.AlteraVencimentoPreBol(Periodo, DiaVencto: Integer;
  Prebol: Boolean; Tipo: TSelType; Grade: TDBGrid; QueryBoletos, QueryBoletosIts,
  QueryUpd, QueryAux: TmySQLQuery; Database: TmySQLDatabase;
  Progress: TProgressBar; TabLctA: String): Boolean;
var
  I: Integer;
  DataDef, DataSel: TDateTime;
begin
  Result := False;
  //
  if (QueryBoletos.State <> dsInactive) and (QueryBoletos.RecordCount > 0) then
  begin
    DataDef := MLAGeral.UltimoDiaDoPeriodo_Date(Periodo) + DiaVencto;
    //
    if not DBCheck.ObtemData(DataDef, DataSel, VAR_DATA_MINIMA) then Exit;
    //
    if Geral.MB_Pergunta('Confirma a altera��o do vencimento para a data ' +
      Geral.FDT(DataSel, 2) + '?') = ID_YES then
    begin
      try
        Progress.Position := 0;
        Progress.Visible  := True;
        Grade.Enabled     := False;
        //
        QueryBoletos.DisableControls;
        QueryBoletosIts.DisableControls;
        //
        case Tipo of
          istAtual:
          begin
            Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                        QueryUpd, Database, DataSel, Prebol, TabLctA);
          end;
          istSelecionados:
          begin
            if Grade.SelectedRows.Count > 0 then
            begin
              with Grade.DataSource.DataSet do
              for I := 0 to Grade.SelectedRows.Count - 1 do
              begin
                GotoBookmark(pointer(Grade.SelectedRows.Items[I]));
                //
                Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                            QueryUpd, Database, DataSel, Prebol, TabLctA);
              end;
            end else
              Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                          QueryUpd, Database, DataSel, Prebol, TabLctA);
          end;
          istTodos:
          begin
            Progress.Max := QueryBoletos.RecordCount;
            //
            QueryBoletos.First;
            //
            while not QueryBoletos.Eof do
            begin
              Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                          QueryUpd, Database, DataSel, Prebol, TabLctA);
              //
              Progress.Position := Progress.Position + 1;
              Progress.Update;
              Application.ProcessMessages;
              //
              QueryBoletos.Next;
            end;
          end
          else
          begin
            Geral.MB_Aviso('Tipo de sele��o n�o implementada!');
            Exit;
          end;
        end;
      finally
        Progress.Visible := False;
        Grade.Enabled    := True;
        //
        QueryBoletos.EnableControls;
        QueryBoletosIts.EnableControls;
      end;
    end;
  end;
end;

function TUnBloquetosCond.AtualizaVenctoBoletosIts(QueryBoletosIts,
  QueryAux, QueryUpd: TmySQLQuery; Database: TmySQLDatabase; DataSel: TDateTime;
  Prebol: Boolean; TabLctA: String): Boolean;
var
  TabUpd, Vencto: String;
  Controle, Lancto: Integer;
  Continua: Boolean;
begin
  try
    Result := True;
    //
    if (Lancto <> 0) and (Prebol = False) and (TabLctA <> '') then
      Continua := VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts, QueryAux,
                    Database, TabLctA)
    else
      Continua := True;
    //
    if Continua then
    begin
      QueryBoletosIts.First;
      //
      while not QueryBoletosIts.Eof do
      begin
        TabUpd   := QueryBoletosIts.FieldByName('TabelaOrig').AsString;
        Controle := QueryBoletosIts.FieldByName('Controle').AsInteger;
        Vencto   := Geral.FDT(DataSel, 1);
        Lancto   := QueryBoletosIts.FieldByName('Lancto').AsInteger;
        //
        Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabUpd, False,
                    ['Vencto'], ['Controle'], [Vencto], [Controle], True);
        //
        if (Lancto <> 0) and (Prebol = False) and (TabLctA <> '') then
        begin
          Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabLctA, False,
                      ['Vencimento'], ['Controle'], [Vencto], [Lancto], True);
        end;
        //
        QueryBoletosIts.Next;
      end;
    end else
    begin
      Geral.MB_Aviso('Lan�amento(s) financeiro(s) n�o foram localizados para o boleto n�mero ' +
        FloatToStr(QueryAux.FieldByName('Documento').AsFloat) + sLineBreak +
        'Altera��o de vencimento cancelada!');
      Result := False;
    end;
  except
    Result := False;
  end;
end;

function TUnBloquetosCond.DesfazerBoletoAtual(QueryBoletosIts,
  QueryAux, QueryUpd: TmySQLQuery; Database: TmySQLDatabase; TabLctA, TabAriA,
  TabCnsA: String): Boolean;
var
  Data: TDateTime;
  TabelaOrig: String;
  MotivDel, Tipo, Carteira, Lancto, Sub, Controle: Integer;
begin
  Result := False;
  //
  if not VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts,QueryAux, Database, TabLctA) then
  begin
    Geral.MB_Aviso('A��o abortada!' + sLineBreak +
      'Motivo: N�o foi poss�vel localizar todos os lan�amentos financeiros referentes ao boleto.');
    Exit;
  end;
  if VerificaSeAlgumItemDoBoletoEstaPago(QueryBoletosIts, QueryAux, Database, TabLctA) then
  begin
    Geral.MB_Aviso('A��o abortada!' + sLineBreak +
      'Motivo: O boleto j� possu� pagamentos lan�ados nele!');
    Exit;
  end;
  try
    QueryBoletosIts.First;
    //
    while not QueryBoletosIts.Eof do
    begin
      TabelaOrig := QueryBoletosIts.FieldByName('TabelaOrig').AsString;
      Controle   := QueryBoletosIts.FieldByName('Controle').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
        'SELECT Data, Tipo, Carteira, Controle, Sub, FatID',
        'FROM ' + TabLctA,
        'WHERE Controle=' + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger),
        '']);
      //Caso exista mais exista um lan�amento
      if QueryAux.RecordCount > 0 then
      begin
        QueryAux.First;
        while not QueryAux.Eof do
        begin
          if LowerCase(TabelaOrig) = LowerCase(TabAriA) then
            MotivDel := CO_MOTVDEL_306_EXCLUILCTARRECADACAO
          else
            MotivDel := CO_MOTVDEL_307_EXCLUILCTLEITURA;
          //
          Data     := QueryAux.FieldByName('Data').AsDateTime;
          Tipo     := QueryAux.FieldByName('Tipo').AsInteger;
          Carteira := QueryAux.FieldByName('Carteira').AsInteger;
          Lancto   := QueryAux.FieldByName('Controle').AsInteger;
          Sub      := QueryAux.FieldByName('Sub').AsInteger;
          //
          try
            //Lan�amento financeiro
            UFinanceiro.ExcluiLct_Unico(TabLctA, Database, Data, Tipo, Carteira,
              Lancto, Sub, MotivDel, False);
          except
            ;
          end;
          //
          QueryAux.Next;
        end;
        try
          if LowerCase(TabelaOrig) = LowerCase(TabAriA) then
          begin
            //Arrecada��es
            Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabAriA, False,
                        ['Boleto', 'Lancto'], ['Controle'], [0, 0], [Controle], True);
          end else
          begin
            //Consumos
            Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabCnsA, False,
                        ['Boleto', 'Lancto'], ['Controle'], [0, 0], [Controle], True);
          end;
        except
          ;
        end;
        //
        QueryBoletosIts.Next;
      end;
    end;
  finally
    ;
  end;
end;

function TUnBloquetosCond.DesfazerBoletos(Selecao: TSelType; QueryUpd, QueryAux,
  QueryBoletos, QueryBoletosIts: TmySQLQuery; Database: TmySQLDatabase;
  Grade: TDBGrid; TabLctA, TabPrvA, TabAriA, TabCnsA: String; Cond,
  Periodo: Integer; Progress: TProgressBar): Boolean;
var
  I: Integer;
  Resposta: Integer;
begin
  if (QueryBoletos.RecordCount > 0) then
  begin
    if not VerificaSeHouveEmissaoPosterior(QueryAux, Database, TabAriA, TabPrvA,
      TabCnsA, Cond, Periodo) then
    begin
      try
        QueryBoletos.DisableControls;
        QueryBoletosIts.DisableControls;
        //
        Grade.Enabled     := False;
        Progress.Position := 0;
        Progress.Visible  := True;
        //
        case Selecao of
          istAtual:
            Resposta := Geral.MB_Pergunta('Tem certeza que deseja desfazer a gera��o do boleto atual?' +
                          sLineBreak + 'ATEN��O: Todos os lan�amentos financeiros atrelados ao boleto ser�o exclu�dos!');
          istSelecionados:
            Resposta := Geral.MB_Pergunta('Tem certeza que deseja desfazer a gera��o dos boletos selecionados?' +
                          sLineBreak + 'ATEN��O: Todos os lan�amentos financeiros atrelados ao boleto ser�o exclu�dos!');
          istTodos:
            Resposta := Geral.MB_Pergunta('Tem certeza que deseja desfazer a gera��o de todos boletos?' +
                          sLineBreak + 'ATEN��O: Todos os lan�amentos financeiros atrelados ao boleto ser�o exclu�dos!');
          else
          begin
            Geral.MB_Aviso('Tipo de sele��o n�o implementada!');
            Resposta := ID_NO;
          end;
        end;
        if Resposta = ID_YES then
        begin
          case Selecao of
            istAtual:
            begin
              Result := DesfazerBoletoAtual(QueryBoletosIts, QueryAux, QueryUpd,
                          Database, TabLctA, TabAriA, TabCnsA);
            end;
            istSelecionados:
            begin
              if Grade.SelectedRows.Count > 0 then
              begin
                Progress.Max := Grade.SelectedRows.Count;
                //
                with Grade.DataSource.DataSet do
                begin
                  for I := 0 to Grade.SelectedRows.Count - 1 do
                  begin
                    GotoBookmark(pointer(Grade.SelectedRows.Items[I]));
                    //
                    Result := DesfazerBoletoAtual(QueryBoletosIts, QueryAux,
                                QueryUpd, Database, TabLctA, TabAriA, TabCnsA);
                    //
                    Progress.Position := Progress.Position + 1;
                    Progress.Update;
                    Application.ProcessMessages;
                  end;
                end;
              end else
                Result := DesfazerBoletoAtual(QueryBoletosIts, QueryAux,
                            QueryUpd, Database, TabLctA, TabAriA, TabCnsA);
            end;
            istTodos:
            begin
              Progress.Max := QueryBoletos.RecordCount;
              //
              QueryBoletos.First;
              //
              while not QueryBoletos.Eof do
              begin
                Result := DesfazerBoletoAtual(QueryBoletosIts, QueryAux, QueryUpd,
                            Database, TabLctA, TabAriA, TabCnsA);
                //
                Progress.Position := Progress.Position + 1;
                Progress.Update;
                Application.ProcessMessages;
                //
                QueryBoletos.Next;
              end;
            end;
          end;
        end else
          Result := False;
      finally
        QueryBoletos.EnableControls;
        QueryBoletosIts.EnableControls;
        //
        Grade.Enabled    := True;
        Progress.Visible := False;
      end;
    end else
    begin
      Geral.MB_Aviso('A��o cancelada! J� houve emiss�o posterior!');
      Result := False;
    end;
    if Result = False then
      Geral.MB_Aviso('Falha ao desfazer boletos!');
  end else
    Result := True;
end;

function TUnBloquetosCond.GeraBoletoAtual(Cond, Cliente, Periodo: Integer;
  QueryBoletos, QueryBoletosIts, QueryLoc, QueryUpd: TmySQLQuery;
  Database: TmySQLDatabase; EscolheNum: Boolean; Vencto: TDateTime;
  TabLctA, TabArre, TabCons: String): Boolean;
var
  CNAB_Cfg, CartEmiss, CAR_TIPODOC, CART_ATIVO, CedBanco, Propriet, Apto,
  Genero, Lancto, Controle, Casas, GeraTyp, CasRat, NaoImpLei: Integer;
  Boleto, Valor, JurosPerc, MultaPerc, MedAnt, MedAtu, Consumo, UnidFat, GeraFat: Double;
  TabelaUpd, Descri, Unidade, UnidLei, UnidImp, Vcto: String;
begin
  CNAB_Cfg := QueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
  Propriet := QueryBoletos.FieldByName('Propriet').AsInteger;
  Apto     := QueryBoletos.FieldByName('Apto').AsInteger;
  Unidade  := QueryBoletos.FieldByName('Unidade').AsString;
  //
  if (DmBloq.QrCNAB_Cfg_B.State = dsInactive) or
    (DmBloq.QrCNAB_Cfg_B.RecordCount = 0) or
    (DmBloq.QrCNAB_Cfg_B.FieldByName('Codigo').AsInteger <> CNAB_Cfg)
  then
    DmBloq.ReopenCNAB_Cfg_Cond(DmBloq.QrCNAB_Cfg_B, DMod.MyDB, Cond, CNAB_Cfg);
  //
  if (DmBloq.QrCNAB_Cfg_B.State <> dsInactive) and (DmBloq.QrCNAB_Cfg_B.RecordCount > 0) then
  begin
    CartEmiss   := DmBloq.QrCNAB_Cfg_B.FieldByName('CartEmiss').AsInteger;
    CAR_TIPODOC := DmBloq.QrCNAB_Cfg_B.FieldByName('CAR_TIPODOC').AsInteger;
    CART_ATIVO  := DmBloq.QrCNAB_Cfg_B.FieldByName('CART_ATIVO').AsInteger;
    JurosPerc   := DmBloq.QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat;
    MultaPerc   := DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat;
    //
    if NaoContinuaPelaCarteira(CartEmiss, CAR_TIPODOC, CART_ATIVO) = False then
    begin
      CedBanco := DmBloq.QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger;
      //
      if QueryBoletos.FieldByName('Boleto').AsFloat = 0 then
      begin
        if not GeraNovoNumeroDeBoleto(Cond, Cliente, CedBanco, Boleto, EscolheNum,
          QueryLoc, DmBloq.QrCNAB_Cfg_B, QueryUpd, Database) then
        begin
          Result := False;
          Exit;
        end;
      end else
        Boleto := QueryBoletos.FieldByName('Boleto').AsFloat;
      //
      QueryBoletosIts.First;
      //
      while not QueryBoletosIts.Eof do
      begin
        Controle  := QueryBoletosIts.FieldByName('Controle').AsInteger;
        TabelaUpd := QueryBoletosIts.FieldByName('TabelaOrig').AsString;
        Valor     := QueryBoletosIts.FieldByName('VALOR').AsFloat;
        Genero    := QueryBoletosIts.FieldByName('Genero').AsInteger;
        Lancto    := QueryBoletosIts.FieldByName('Lancto').AsInteger;
        Casas     := QueryBoletosIts.FieldByName('Casas').AsInteger;
        MedAnt    := QueryBoletosIts.FieldByName('MedAnt').AsFloat;
        MedAtu    := QueryBoletosIts.FieldByName('MedAtu').AsFloat;
        Consumo   := QueryBoletosIts.FieldByName('Consumo').AsFloat;
        UnidFat   := QueryBoletosIts.FieldByName('UnidFat').AsFloat;
        UnidLei   := QueryBoletosIts.FieldByName('UnidLei').AsString;
        UnidImp   := QueryBoletosIts.FieldByName('UnidImp').AsString;
        GeraTyp   := QueryBoletosIts.FieldByName('GeraTyp').AsInteger;
        CasRat    := QueryBoletosIts.FieldByName('CasRat').AsInteger;
        NaoImpLei := QueryBoletosIts.FieldByName('NaoImpLei').AsInteger;
        GeraFat   := QueryBoletosIts.FieldByName('GeraFat').AsFloat;
        Vcto      := Geral.FDT(Vencto, 1);
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Database, [
          'UPDATE ' + LowerCase(TabelaUpd) + ' SET Boleto=' + FloatToStr(Boleto) + ', ',
          'Vencto="' + Vcto + '" WHERE Boleto=0 ',
          'AND Controle=' + Geral.FF0(Controle),
          '']);
        //
        //Insere financeiro
        if LowerCase(TabArre) = LowerCase(TabelaUpd) then
        begin
          //Arrecada��o
          if Lancto = 0 then
          begin
            Descri := QueryBoletosIts.FieldByName('TEXTO').AsString + ' - ' +
                        dmkPF.MesEAnoDoPeriodo(Periodo) + ' - ' + Unidade;
            //
            if InsereLancto(QueryUpd, Database, Vencto, Boleto, Valor, JurosPerc,
              MultaPerc, Cliente, Propriet, Periodo, CartEmiss, Genero, Apto,
              Controle, FArre_FatID, Descri, TabLctA, TabArre) = 0 then
            begin
              Result := False;
              Exit;
            end;
          end;
        end else
        if LowerCase(TabCons) = LowerCase(TabelaUpd) then
        begin
          //Consumo por leitura
          if Lancto = 0 then
          begin
            Descri := QueryBoletosIts.FieldByName('TEXTO').AsString + ' (' +
                        DmBloq.TextoExplicativoItemBoleto(1, Casas, MedAnt,
                        MedAtu, Consumo, UnidFat, UnidLei, UnidImp, GeraTyp,
                        CasRat, NaoImpLei, GeraFat);

            //
            if InsereLancto(QueryUpd, Database, Vencto, Boleto, Valor, JurosPerc,
              MultaPerc, Cliente, Propriet, Periodo, CartEmiss, Genero, Apto,
              Controle, FCons_FatID, Descri, TabLctA, TabCons) = 0 then
            begin
              Result := False;
              Exit;
            end;
          end;
        end else
        begin
          Geral.MB_Aviso('Tabel inv�lida para inser��o de lan�amentos financeiros!');
          Result := False;
          Exit;
        end;
        //
        QueryBoletosIts.Next;
      end;
      Result := True;
    end else
      Result := False;
  end else
    Result := False;
end;

function TUnBloquetosCond.GeraBoletosAbertos(Cond, Cliente, Periodo,
  DiaVencto: Integer; QueryBoletos, QueryBoeltosIts, QueryLoc,
  QueryUpd: TmySQLQuery; Database: TmySQLDatabase; Grade: TDBGrid;
  Selecao: TSelType; ProgressBar: TProgressBar; TabLctA, TabArre,
  TabCons: String): Boolean;
var
  I: Integer;
  Continua: Boolean;
  DataDef, DataSel: TDateTime;
begin
  Result := False;
  //
  if (QueryBoletos.State <> dsInactive) and (QueryBoletos.RecordCount > 0) then
  begin
    DataDef := MLAGeral.UltimoDiaDoPeriodo_Date(Periodo) + DiaVencto;
    //
    if not DBCheck.ObtemData(DataDef, DataSel, VAR_DATA_MINIMA) then
      Exit;
    try
      Grade.Enabled        := False;
      ProgressBar.Position := 0;
      ProgressBar.Visible  := True;
      Continua             := True;
      //
      QueryBoletos.DisableControls;
      QueryBoeltosIts.DisableControls;
      //
      case Selecao of
        istAtual:
        begin
          if Geral.MB_Pergunta('Confirma a gera��o do pr�-boleto atual?') = ID_YES then
          begin
            Continua := GeraBoletoAtual(Cond, Cliente, Periodo, QueryBoletos,
                          QueryBoeltosIts, QueryLoc, QueryUpd, Database, True,
                          DataSel, TabLctA, TabArre, TabCons);
          end else
            Continua := False;
        end;
        istSelecionados:
        begin
          if Continua then
          begin
            if Grade.SelectedRows.Count > 1 then
            begin
              if Geral.MB_Pergunta('Confirma a gera��o dos pr�-boletos selecionados?') = ID_YES then
              begin
                ProgressBar.Max := Grade.SelectedRows.Count;
                //
                with Grade.DataSource.DataSet do
                begin
                  for I := 0 to Grade.SelectedRows.Count - 1 do
                  begin
                    GotoBookmark(pointer(Grade.SelectedRows.Items[I]));
                    //
                    Continua := GeraBoletoAtual(Cond, Cliente, Periodo,
                                  QueryBoletos, QueryBoeltosIts, QueryLoc,
                                  QueryUpd, Database, False, DataSel, TabLctA,
                                  TabArre, TabCons);
                    //
                    if Continua then
                    begin
                      ProgressBar.Position := ProgressBar.Position + 1;
                      ProgressBar.Update;
                      Application.ProcessMessages;
                    end else
                      Exit;
                  end;
                end;
              end else
                Continua := False;
            end else
            begin
              if Geral.MB_Pergunta('Confirma a gera��o do pr�-boleto selecionado?') = ID_YES then
              begin
                Continua := GeraBoletoAtual(Cond, Cliente, Periodo, QueryBoletos,
                              QueryBoeltosIts, QueryLoc, QueryUpd, Database,
                              False, DataSel, TabLctA, TabArre, TabCons);
              end else
                Continua := False;
            end;
          end;
        end;
        istTodos:
        begin
          if Geral.MB_Pergunta('Confirma a gera��o de todos pr�-boletos abertos?') = ID_YES then
          begin
            ProgressBar.Max := QueryBoletos.RecordCount;
            //
            QueryBoletos.First;
            //
            while not QueryBoletos.Eof do
            begin
              Continua := GeraBoletoAtual(Cond, Cliente, Periodo, QueryBoletos,
                            QueryBoeltosIts, QueryLoc, QueryUpd, Database,
                            False, DataSel, TabLctA, TabArre, TabCons);
              //
              if Continua then
              begin
                ProgressBar.Position := ProgressBar.Position + 1;
                ProgressBar.Update;
                Application.ProcessMessages;
                //
                QueryBoletos.Next;
              end else
                Exit;
            end;
          end else
            Continua := False;
        end else
        begin
          Geral.MB_Aviso('Tipo de sele��o n�o implementada!');
          Exit;
        end;
      end;
    finally
      QueryBoletos.EnableControls;
      QueryBoeltosIts.EnableControls;
      //
      Grade.Enabled        := True;
      ProgressBar.Visible  := False;
      //
      if not Continua then
        Geral.MB_Aviso('Falha ao gerar boletos!');
      //
      Result := Continua;
    end;
  end;
end;

procedure TUnBloquetosCond.MostraCNAB_Cfg(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCNAB_Cfg.LocCod(Codigo, Codigo);
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
  end;
end;

function TUnBloquetosCond.NaoContinuaPelaCarteira(Carteira, CAR_TIPODOC,
  CART_ATIVO: Integer): Boolean;
var
  NumCartEmiss: String;
begin
  Result := True;
  //
  NumCartEmiss := Geral.FF0(Carteira);
  //
  if Carteira = 0 then
  begin
    Geral.MB_Aviso('A carteira de emiss�o dos boletos n�o foi definida no cadastro do condom�nio! A��o abortada!');
    Exit;
  end;
  if CAR_TIPODOC <> 5 then
  begin
    if Geral.MB_Pergunta('A carteira de emiss�o n� ' + NumCartEmiss +
      ' n�o est� configurada como "Tipo de documento" = bloqueto!' + sLineBreak +
      'Deseja continuar assim mesmo?') <> ID_YES
    then
      Exit;
  end;
  if CART_ATIVO <> 1 then
  begin
    if Geral.MB_Pergunta('A carteira de emiss�o de bloquetos (n� ' +
      NumCartEmiss + ') n�o est� configurada como ativa!' + sLineBreak +
      'Deseja continuar assim mesmo?') <> ID_YES
    then
      Exit;
  end;
  Result := False;
end;

procedure TUnBloquetosCond.ReopenCNAB_Cfg(Query: TmySQLQuery;
  Database: TmySQLDatabase; Cliente: Integer; Codigo: Integer = 0);
var
  SQL: String;
begin
  if Codigo = 0 then
    SQL := 'AND (Cedente=' + Geral.FF0(Cliente) + ' OR SacadAvali=' + Geral.FF0(Cliente) + ')'
  else
    SQL := 'AND Codigo=' + Geral.FF0(Codigo);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
    'SELECT Codigo, Nome ',
    'FROM cnab_cfg ',
    'WHERE Ativo = 1 ',
    SQL,
    'ORDER BY Nome ',
    '']);
end;

function TUnBloquetosCond.ValidaArrecadacaoConsumoCond(Cond, CNAB_Cfg: Integer;
  Query: TmySQLQuery; Database: TmySQLDatabase; var Msg: String): Boolean;
var
  UsaCNAB_Cfg: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
    'SELECT UsaCNAB_Cfg ',
    'FROM cond ',
    'WHERE Codigo=' + Geral.FF0(Cond),
    '']);
  UsaCNAB_Cfg := Query.FieldByName('UsaCNAB_Cfg').AsInteger;
  //
  if UsaCNAB_Cfg = 1 then
  begin
    if CNAB_Cfg = 0 then
    begin
      Msg    := 'Defina a configura��o do boleto!';
      Result := False;
    end else
    begin
      Msg    := '';
      Result := True;
    end;
  end else
  begin
    Msg    := '';
    Result := True;
  end;
end;

function TUnBloquetosCond.VerificaSeAlgumItemDoBoletoEstaPago(QueryBoletosIts,
  QueryAux: TmySQLQuery; Database: TmySQLDatabase; TabLctA: String): Boolean;
var
  Lcts: String;
begin
  //Verifica se todos os lan�amentos est�o em aberto
  //True  = Pago
  //False = Em aberto
  //
  try
    Lcts := '';
    //
    QueryBoletosIts.First;
    //
    while not QueryBoletosIts.Eof do
    begin
      if QueryBoletosIts.RecNo = QueryBoletosIts.RecordCount then
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger)
      else
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger) + ', ';
      //
      QueryBoletosIts.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
      'SELECT * ',
      'FROM ' + TabLctA,
      'WHERE Controle IN (' + Lcts + ')',
      'AND Sit < 2 ',
      'AND Pago = 0 ',
      '']);
    if QueryAux.RecordCount > 0 then
    begin
      if QueryBoletosIts.RecordCount = QueryAux.RecordCount then
        Result := False
      else
        Result := True;
    end else
      Result := True;
  except
    Result := True;
  end;
end;

function TUnBloquetosCond.VerificaSeHouveEmissaoPosterior(QueryAux: TmySQLQuery;
  Database: TmySQLDatabase; TabAriA, TabPrvA, TabCnsA: String; Cond,
  Periodo: Integer): Boolean;
begin
  //True  = Houve emiss�o posterior
  //False = N�o houve emiss�o posterior
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
    'SELECT MAX(Boleto) Boleto ',
    'FROM ' + TabCnsA,
    'WHERE Cond=' + Geral.FF0(Cond),
    'AND Periodo > ' + Geral.FF0(Periodo),
    ' ',
    'UNION ',
    ' ',
    'SELECT MAX(ari.Boleto) Boleto ',
    'FROM ' + TabAriA + ' ari ',
    'LEFT JOIN ' + TabPrvA + ' prv ON  prv.Codigo=ari.Codigo ',
    'WHERE prv.Cond= ' + Geral.FF0(Cond),
    'AND prv.Periodo > ' + Geral.FF0(Periodo),
    '',
    'ORDER BY Boleto DESC ',
    '']);
  //
  if QueryAux.FieldByName('Boleto').AsFloat > 0 then
    Result := True
  else
    Result := False;
end;

function TUnBloquetosCond.VerificaSeLancamentoEstaPago(Lancto: Integer;
  QueryAux: TmySQLQuery; Database: TmySQLDatabase; TabLctA: String;
  var Msg: String): Boolean;
begin
  //True  = N�o possui pagamentos
  //False = Possui pagamentos
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
    'SELECT * ',
    'FROM ' + TabLctA,
    'WHERE Controle=' + Geral.FF0(Lancto),
    'AND Sit < 2 ',
    'AND Pago = 0 ',
    '']);
  if QueryAux.RecordCount > 0 then
  begin
    Msg    := 'O lan�amento financeiro possui pagamentos nele!';
    Result := False;
  end else
  begin
    Msg    := '';
    Result := True;
  end;
end;

function TUnBloquetosCond.VerificaSeLctEstaoNaTabLctA(TabLctA: String;
  Lancto: Integer; QueryAux: TmySQLQuery; Database: TmySQLDatabase;
  var Msg: String): Boolean;
begin
  //True  = Est� na tabela LctA
  //False = N�o est� na tabela LctA
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
    'SELECT Controle ',
    'FROM ' + TabLctA,
    'WHERE Controle=' + Geral.FF0(Lancto),
    '']);
  if QueryAux.RecordCount > 0 then
  begin
    Msg    := '';
    Result := True;
  end else
  begin
    Msg    := 'N�o foi poss�vel localizar o lan�amento financeiro!';
    Result := False;
  end;
end;

function TUnBloquetosCond.VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts,
  QueryAux: TmySQLQuery; Database: TmySQLDatabase; TabLctA: String): Boolean;
var
  Lcts: String;
begin
  //Verifica se todos os lan�amentos est�o na tabela TabLctA caso n�o, n�o atualiza nenhum
  try
    Lcts := '';
    //
    QueryBoletosIts.First;
    //
    while not QueryBoletosIts.Eof do
    begin
      if QueryBoletosIts.RecNo = QueryBoletosIts.RecordCount then
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger)
      else
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger) + ', ';
      //
      QueryBoletosIts.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
      'SELECT Documento ',
      'FROM ' + TabLctA,
      'WHERE Controle IN (' + Lcts + ')',
      '']);
    if QueryBoletosIts.RecordCount = QueryAux.RecordCount then
      Result := True
    else
      Result := False;
  except
    Result := False;
  end;
end;

//Migra��es
procedure TUnBloquetosCond.ConverteCondBoletosParaCNABcfg(
  DataBase: TMySQLDataBase; PB1: TProgressBar);
var
  Qry, QryLoc, QryUpd: TMySQLQuery;
  Cond, CNAB_Cfg, Aceite, Correio: Integer;
  TabAriA, TabCnsA: String;
  LastNosNum: Largeint;
begin
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryLoc        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    PB1.Visible := True;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT con.*, ',
      'CONCAT("BOLETOS - ", IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NomeCfg ',
      'FROM cond con ',
      'LEFT JOIN entidades ent ON ent.Codigo = con.Cliente ',
      'WHERE MigrouPCNAB_Cfg=0 ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max      := Qry.RecordCount;
      //
      while not Qry.EOF do
      begin
        Cond := Qry.FieldByName('Codigo').AsInteger;
        //
        if Qry.FieldByName('UsaCNAB_Cfg').AsInteger = 0 then
        begin
          CNAB_Cfg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'CNAB_Cfg', 'CNAB_Cfg', 'Codigo');
          //
          if UpperCase(Qry.FieldByName('Aceite').AsString) = 'S' then
            Aceite := 1
          else
            Aceite := 0;
          //
          if UpperCase(Qry.FieldByName('Correio').AsString) = 'S' then
            Correio := 1
          else
            Correio := 0;
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QryLoc, Database, [
            'SELECT Bloqueto ',
            'FROM bancosblo ',
            'WHERE Codigo=' + Geral.FF0(Qry.FieldByName('Banco').AsInteger),
            'AND Entidade=' + Geral.FF0(Qry.FieldByName('Cliente').AsInteger),
            '']);
          LastNosNum := QryLoc.FieldByName('Bloqueto').AsLargeInt;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'CNAB_Cfg', False,
          [
            'Nome', 'CedBanco',
            'Cedente', 'SacadAvali',
            'CedAgencia', 'CedConta',
            'CedDAC_A', 'CedDAC_C',
            'CartNum', 'AceiteTit',
            'Correio', 'CodEmprBco',
            'JurosPerc', 'MultaPerc',
            'Texto01', 'Texto02',
            'Texto03', 'Texto04',
            'Texto05', 'Texto06',
            'Texto07', 'Texto08',
            'CNAB', 'LastNosNum', 'LocalPag',
            'EspecieVal', 'OperCodi',
            'IDCobranca', 'AgContaCed',
            'EspecieDoc', 'CedPosto',
            'CartEmiss', 'TipoCobranca',
            'EspecieTxt', 'CartTxt',
            'ModalCobr', 'CtaCooper',
            'VTCBBNITAR', 'CorresBco',
            'CorresAge', 'CorresCto',
            'NPrinBc', 'TermoAceite'
          ], ['Codigo'], [
            Qry.FieldByName('NomeCfg').AsString, Qry.FieldByName('Banco').AsInteger,
            Qry.FieldByName('Cedente').AsInteger, Qry.FieldByName('SacadAvali').AsInteger,
            Qry.FieldByName('Agencia').AsInteger, Qry.FieldByName('Conta').AsString,
            Qry.FieldByName('DVAgencia').AsString, Qry.FieldByName('DVConta').AsString,
            Qry.FieldByName('Carteira').AsString, Aceite,
            Correio, Qry.FieldByName('CodCedente').AsString,
            Qry.FieldByName('PercJuros').AsFloat, Qry.FieldByName('PercMulta').AsFloat,
            Qry.FieldByName('Instrucao1').AsString, Qry.FieldByName('Instrucao2').AsString,
            Qry.FieldByName('Instrucao3').AsString, Qry.FieldByName('Instrucao4').AsString,
            Qry.FieldByName('Instrucao5').AsString, Qry.FieldByName('Instrucao6').AsString,
            Qry.FieldByName('Instrucao7').AsString, Qry.FieldByName('Instrucao8').AsString,
            Qry.FieldByName('CNAB').AsInteger, LastNosNum, Qry.FieldByName('LocalPag').AsString,
            Qry.FieldByName('EspecieVal').AsString, Qry.FieldByName('OperCodi').AsString,
            Qry.FieldByName('IDCobranca').AsString, Qry.FieldByName('AgContaCed').AsString,
            Qry.FieldByName('EspecieDoc').AsString, Qry.FieldByName('Posto').AsInteger,
            Qry.FieldByName('CartEmiss').AsInteger, Qry.FieldByName('TipoCobranca').AsInteger,
            Qry.FieldByName('EspecieVal').AsString, Qry.FieldByName('CartTxt').AsString,
            Qry.FieldByName('ModalCobr').AsInteger, Qry.FieldByName('CtaCooper').AsString,
            Qry.FieldByName('VTCBBNITAR').AsFloat, Qry.FieldByName('CorresBco').AsInteger,
            Qry.FieldByName('CorresAge').AsInteger, Qry.FieldByName('CorresCto').AsString,
            Qry.FieldByName('NPrinBc').AsString, -1
          ], [CNAB_Cfg], True) then
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
              'UPDATE arrefut SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
              'WHERE Cond=' + Geral.FF0(Cond),
              '']);
            UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
              'UPDATE arrebai SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
              'WHERE Cond=' + Geral.FF0(Cond),
              '']);
            UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
              'UPDATE bloqparc SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
              'WHERE CodCliEsp=' + Geral.FF0(Cond),
              '']);
            UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
              'UPDATE consprc SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
              'WHERE Cond=' + Geral.FF0(Cond),
              '']);
            //
            TabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, Cond);

            if (TabAriA <> '') then
            begin
              UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
                'UPDATE ' + TabAriA + ' SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
                '']);
            end;

            TabCnsA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, Cond);

            if (TabCnsA <> '') then
            begin
              UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
                'UPDATE ' + TabCnsA + ' SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
                '']);
            end;
          end;
        end;
        UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
          'UPDATE cond SET MigrouPCNAB_Cfg=1 ',
          'WHERE Codigo=' + Geral.FF0(Cond),
          '']);
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        Qry.Next;
      end;
    end;
  finally
    PB1.Visible := False;
    Qry.Free;
    QryLoc.Free;
    QryUpd.Free;
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnBloquetosCond.AtualizaDadosProtocolosNosBoletos(
  DataBase: TMySQLDataBase; PB1: TProgressBar);
var
  Qry, Qry2, Qry3, QryUpd: TMySQLQuery;
  Cond, Prev, Depto, Periodo, Tipo, ProtocoloCR, ProtocoCod, Protocolo, ProtocoPak: Integer;
  TabAriA, TabCnsA, TabPrvA: String;
  Docum: Double;
begin
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  Qry2          := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  Qry3          := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM bloopcoes ',
      'WHERE CorrigiuProtAriCns=1 ',
      '']);
    if Qry.RecordCount > 0 then
      Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
      'SELECT its.ID_Cod2 ',
      'FROM protpakits its ',
      'LEFT JOIN protocolos pro ON pro.Codigo = its.Codigo ',
      'WHERE pro.Tipo IN (1,2,4) ',
      'AND its.ID_Cod1 <> 0 ',
      'AND its.ID_Cod2 <> 0 ',
      'AND its.Link_ID=' + Geral.FF0(VAR_TIPO_LINK_ID_01_GENERICO),
      'AND its.Depto <> 0 ',
      'AND its.Docum <> 0 ',
      'GROUP BY its.ID_Cod2 ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max      := Qry.RecordCount;
      //
      while not Qry.EOF do
      begin
        Cond    := Qry.FieldByName('ID_Cod2').AsInteger;
        TabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, Cond);
        TabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, Cond);
        TabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, Cond);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Database, [
          'SELECT its.ID_Cod1, its.ID_Cod2, its.Link_ID, its.Controle, ',
          'its.Depto, its.Docum, prv.Periodo, pro.Tipo, its.Codigo ',
          'FROM protpakits its ',
          'LEFT JOIN protocolos pro ON pro.Codigo = its.Codigo ',
          'LEFT JOIN ' + TabPrvA + ' prv ON prv.Codigo = its.ID_Cod1 ',
          'WHERE pro.Tipo IN (1,2,4) ',
          'AND its.ID_Cod1 <> 0 ',
          'AND its.ID_Cod2 =' + Geral.FF0(Cond),
          'AND its.Link_ID =' + Geral.FF0(VAR_TIPO_LINK_ID_01_GENERICO),
          'AND its.Depto <> 0 ',
          'AND its.Docum <> 0 ',
          'ORDER BY its.ID_Cod2, its.ID_Cod1 ',
          '']);
        if Qry2.RecordCount > 0 then
        begin
          while not Qry2.EOF do
          begin
            Prev       := Qry2.FieldByName('ID_Cod1').AsInteger;
            Periodo    := Qry2.FieldByName('Periodo').AsInteger;
            Depto      := Qry2.FieldByName('Depto').AsInteger;
            Docum      := Qry2.FieldByName('Docum').AsFloat;
            Tipo       := Qry2.FieldByName('Tipo').AsInteger;
            ProtocoCod := Qry2.FieldByName('Codigo').AsInteger;
            ProtocoPak := Qry2.FieldByName('Controle').AsInteger;
            //
            //Atualiza arrecada��o
            if Tipo = 4 then //Com registro
            begin
              UnDmkDAC_PF.AbreMySQLQuery0(Qry3, Database, [
                'SELECT ProtocoloCR ',
                'FROM ' + TabAriA,
                'WHERE Codigo=' + Geral.FF0(Prev),
                'AND Apto=' + Geral.FF0(Depto),
                'AND Boleto=' + Geral.FFI(Docum),
                '']);
              ProtocoloCR := Qry3.FieldByName('ProtocoloCR').AsInteger;
              //
              if ProtocoloCR = 0 then
              begin
                UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, DataBase, [
                  'UPDATE ' + TabAriA + ' SET ',
                  'ProtocoloCR=' + Geral.FF0(ProtocoCod) + ', ',
                  'ProtocoPakCR=' + Geral.FF0(ProtocoPak),
                  'WHERE Codigo=' + Geral.FF0(Prev),
                  'AND Apto=' + Geral.FF0(Depto),
                  'AND Boleto=' + Geral.FFI(Docum),
                  '']);
              end;
            end else
            begin
              UnDmkDAC_PF.AbreMySQLQuery0(Qry3, Database, [
                'SELECT Protocolo ',
                'FROM ' + TabAriA,
                'WHERE Codigo=' + Geral.FF0(Prev),
                'AND Apto=' + Geral.FF0(Depto),
                'AND Boleto=' + Geral.FFI(Docum),
                '']);
              Protocolo := Qry3.FieldByName('Protocolo').AsInteger;
              //
              if Protocolo = 0 then
              begin
                UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, DataBase, [
                  'UPDATE ' + TabAriA + ' SET ',
                  'Protocolo=' + Geral.FF0(ProtocoCod) + ', ',
                  'ProtocoPak=' + Geral.FF0(ProtocoPak),
                  'WHERE Codigo=' + Geral.FF0(Prev),
                  'AND Apto=' + Geral.FF0(Depto),
                  'AND Boleto=' + Geral.FFI(Docum),
                  '']);
              end else
              begin
                UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, DataBase, [
                  'UPDATE ' + TabAriA + ' SET ',
                  'Protocolo2=' + Geral.FF0(ProtocoCod) + ', ',
                  'ProtocoPak2=' + Geral.FF0(ProtocoPak),
                  'WHERE Codigo=' + Geral.FF0(Prev),
                  'AND Apto=' + Geral.FF0(Depto),
                  'AND Boleto=' + Geral.FFI(Docum),
                  '']);
              end;
            end;
            //
            Qry2.Next;
          end;
        end;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        //
        Qry.Next;
      end;
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, DataBase, [
      'UPDATE bloopcoes ',
      'SET CorrigiuProtAriCns = 1 ',
      '']);
  finally
    PB1.Position := 0;
    //
    Qry.Free;
    Qry2.Free;
    Qry3.Free;
    QryUpd.Free;
    Screen.Cursor := crDefault;
  end;
end;

end.
