unit CondGerArreFutA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkImage, UnDmkEnums,
  Data.DB, mySQLDbTables, DmkDAC_PF;

type
  TFmCondGerArreFutA = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    CkNaoArreRisco: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BtSaida: TBitBtn;
    BitBtn1: TBitBtn;
    QrLoc: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetaTodos(Marca: Boolean);
  public
    { Public declarations }
    FCond, FCodigo: Integer;
    FTabAriA: String;
    FQueryArreFutA, FQueryArreFutI: TmySQLQuery;
    FDataSourceArreFutA: TDataSource;
  end;

  var
  FmCondGerArreFutA: TFmCondGerArreFutA;

implementation

uses Module, MyVCLSkin, UMySQLModule, UnInternalConsts, UnMyObjects,
  UnBloqCond_Jan;

{$R *.DFM}

procedure TFmCondGerArreFutA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerArreFutA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerArreFutA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCondGerArreFutA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerArreFutA.FormShow(Sender: TObject);
begin
  DBGrid1.DataSource := FDataSourceArreFutA;
end;

procedure TFmCondGerArreFutA.BitBtn1Click(Sender: TObject);
const
  Msg = 'Falha ao localizar arrecada��o!';
var
  Controle: Integer;
begin
  if (FQueryArreFutI.State <> dsInactive) and (FQueryArreFutI.RecordCount > 0) then
  begin
    Controle := FQueryArreFutA.FieldByName('Controle').AsInteger;
    //
    if FQueryArreFutI.Locate('Controle', Controle, []) then
    begin
      UBloqCond_Jan.MostraFmCondGerArreFut(stUpd, FCond, FQueryArreFutI);
      //
      UMyMod.AbreQuery(FQueryArreFutA, Dmod.MyDB);
    end else
      Geral.MB_Aviso(Msg);
  end;
end;

procedure TFmCondGerArreFutA.BitBtn2Click(Sender: TObject);
begin
  SetaTodos(True);
end;

procedure TFmCondGerArreFutA.BitBtn3Click(Sender: TObject);
begin
  SetaTodos(False);
end;

procedure TFmCondGerArreFutA.SetaTodos(Marca: Boolean);
var
  Controle, Status: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Controle := FQueryArreFutA.FieldByName('Controle').AsInteger;
    Status   := Geral.BoolToInt(Marca);
    //
    FQueryArreFutA.DisableControls;
    //
    FQueryArreFutA.First;
    while not FQueryArreFutA.Eof do
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE ArreFut SET ',
        'Inclui=' + Geral.FF0(Status),
        'WHERE Controle=' + Geral.FF0(FQueryArreFutA.FieldByName('Controle').AsInteger),
        '']);
      //
      FQueryArreFutA.Next;
    end;
  finally
    FQueryArreFutA.EnableControls;
    UMyMod.AbreQuery(FQueryArreFutA, Dmod.MyDB);
    FQueryArreFutA.Locate('Controle', Controle, []);
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGerArreFutA.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Inclui' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, FQueryArreFutA.FieldByName('Inclui').AsInteger);
end;

procedure TFmCondGerArreFutA.DBGrid1CellClick(Column: TColumn);
var
  Controle, Status: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Controle := FQueryArreFutA.FieldByName('Controle').AsInteger;
    //
    if FQueryArreFutA.FieldByName('Inclui').AsInteger = 0 then
      Status := 1
    else
      Status := 0;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE ArreFut SET ',
      'Inclui=' + Geral.FF0(Status),
      'WHERE Controle=' + Geral.FF0(Controle),
      '']);
    //
    UMyMod.AbreQuery(FQueryArreFutA, Dmod.MyDB);
    FQueryArreFutA.Locate('Controle', Controle, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGerArreFutA.BtOKClick(Sender: TObject);
var
  Controle, Contar: Integer;
  Msg, MsgVal: String;
begin
  Screen.Cursor := crHourGlass;
  try
    FQueryArreFutA.First;
    //
    Contar := 0;
    MsgVal := '';
    //
    while not FQueryArreFutA.Eof do
    begin
      if FQueryArreFutA.FieldByName('Inclui').AsInteger = 1 then
      begin
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                    FTabAriA, TAB_ARI, 'Controle');
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, FTabAriA, False,
          ['ComoAdd',
          'Conta',
          'Valor',
          'Texto',
          'ArreBaC',
          'ArreBaI',
          'Apto',
          'Propriet',
          'NaoArreRisco',
          'CNAB_Cfg',
          'Codigo'], ['Controle'],
          [2,
          FQueryArreFutA.FieldByName('Conta').AsInteger,
          FQueryArreFutA.FieldByName('Valor').AsFloat,
          FQueryArreFutA.FieldByName('Texto').AsString,
          0,
          0,
          FQueryArreFutA.FieldByName('Apto').AsInteger,
          FQueryArreFutA.FieldByName('Propriet').AsInteger,
          Geral.BoolToInt(CkNaoArreRisco.Checked),
          FQueryArreFutA.FieldByName('CNAB_Cfg').AsInteger,
          FCodigo], [Controle], True) then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(DMod.QrUpdM, Dmod.MyDB, [
            'UPDATE ArreFut SET ',
            TAB_ARI + '=' + Geral.FF0(Controle),
            'WHERE Controle=' + Geral.FF0(FQueryArreFutA.FieldByName('Controle').AsInteger),
            '']);
          Contar := Contar + 1;
        end;
      end;
      FQueryArreFutA.Next;
    end;
    if MsgVal <> '' then
      Geral.MB_Aviso(MsgVal);
    //
    UMyMod.AbreQuery(FQueryArreFutA, Dmod.MyDB);
    UMyMod.AbreQuery(FQueryArreFutI, Dmod.MyDB);
    //
    case Contar of
        0: Msg := 'Nenhum item foi inclu�do!';
        1: Msg := 'Um item foi inclu�do!';
      else Msg := Geral.FF0(Contar) + ' itens foram inclu�dos!';
    end;
    Geral.MB_Aviso(Msg);
    //
    if FQueryArreFutA.RecordCount = 0 then
      Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
