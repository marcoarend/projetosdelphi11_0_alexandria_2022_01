object FmCondGerNew: TFmCondGerNew
  Left = 520
  Top = 259
  Caption = 'GER-CONDM-012 :: Novo Or'#231'amento'
  ClientHeight = 262
  ClientWidth = 351
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 351
    Height = 100
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label32: TLabel
      Left = 48
      Top = 15
      Width = 23
      Height = 13
      Caption = 'M'#234's:'
    end
    object LaAnoI: TLabel
      Left = 224
      Top = 15
      Width = 22
      Height = 13
      Caption = 'Ano:'
    end
    object CBMes: TComboBox
      Left = 49
      Top = 32
      Width = 172
      Height = 21
      Style = csDropDownList
      Color = clWhite
      DropDownCount = 12
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object CBAno: TComboBox
      Left = 223
      Top = 32
      Width = 78
      Height = 21
      Style = csDropDownList
      Color = clWhite
      DropDownCount = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object CkModBol: TCheckBox
      Left = 8
      Top = 60
      Width = 337
      Height = 17
      Caption = 'Inclui automaticamente modelos de bloquetos espec'#237'ficos de UHs.'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object CkBloqFV: TCheckBox
      Left = 8
      Top = 80
      Width = 337
      Height = 17
      Caption = 'Boleto frente e verso'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 351
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 303
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 255
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 204
        Height = 32
        Caption = 'Novo Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 204
        Height = 32
        Caption = 'Novo Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 204
        Height = 32
        Caption = 'Novo Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 148
    Width = 351
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 347
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 192
    Width = 351
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 347
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 203
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    Left = 244
    Top = 140
    object QrPesqPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ModelBloq, ConfigBol'
      'FROM cond'
      'WHERE Codigo=:P0')
    Left = 272
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrCondConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
  end
  object QrCondModBol: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Apto, ModelBloq, ConfigBol, BalAgrMens, Compe '
      'FROM condmodbol'
      'WHERE Codigo=:P0')
    Left = 300
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondModBolApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrCondModBolModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrCondModBolConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
    object QrCondModBolBalAgrMens: TSmallintField
      FieldName = 'BalAgrMens'
    end
    object QrCondModBolCompe: TSmallintField
      FieldName = 'Compe'
    end
  end
end
