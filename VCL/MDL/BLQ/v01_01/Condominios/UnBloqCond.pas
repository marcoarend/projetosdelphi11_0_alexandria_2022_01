unit UnBloqCond;

interface

uses System.Variants, mySQLDbTables, UMySQLModule, dmkGeral, Module, ModuleGeral,
  dmkImage, Forms, Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids,
  AdvToolBar, DB, UnDmkProcFunc, ExtCtrls, dmkDBGrid, Math, Dialogs, UnDmkEnums,
  Classes, dmkDBGridZTO, frxDBSet, frxClass, UnInternalConsts, Vcl.StdCtrls;

type
  TUnBloqCond = class(TObject)
  private
  public
    function  LetraModelosBloq(Item: Integer): String;
    function  TextoExplicativoItemBoleto(EhConsumo, Casas: Integer; MedAnt,
              MedAtu, Consumo, UnidFat: Double; UnidLei, UnidImp: String;
              GeraTyp, CasRat, NaoImpLei: Integer; GeraFat: Double): String;
    function  CalculaTotalPRI(TabelaPriA, TabelaPrvA: String; Periodo, Codigo,
              Controle: Integer): Boolean;
    function  RecalculaArrecadacoes(TabelaAriA, TabelaLctA: String; Codigo,
              CliInt: Integer; Gastos: Double): Boolean;
    function  VerificaSeLctEstaoNaTabLctA(TabLctA: String; Lancto: Integer;
                QueryAux: TmySQLQuery; var Msg: String): Boolean;
    function  VerificaSeLancamentoEstaPago(Lancto: Integer; QueryAux: TmySQLQuery;
              TabLctA: String; var Msg: String): Boolean;
    function  GetPercAptUni(Controle, ListaBaU, Conta: Integer; NomeConta: String): Double;
    function  GetPercAptAll(Controle, ListaBaU: Integer): Double;
    function  ObtemDiaVenctoDeApto(CliInt, Depto: Integer): Integer;
    function  AlteraVencimentoPreBol(CliInt, Periodo, Depto: Integer;
              Prebol: Boolean; Tipo: TSelType; Grade: TDBGrid; QueryBoletos,
              QueryBoletosIts, QueryUpd, QueryAux: TmySQLQuery; Database: TmySQLDatabase;
              Progress: TProgressBar; TabLctA: String): Boolean;
    function  VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts,
              QueryAux: TmySQLQuery; Database: TmySQLDatabase; TabLctA: String): Boolean;
    function  DesfazerBoletos(Selecao: TSelType; QueryUpd, QueryAux,
                QueryBoletos, QueryBoletosIts: TmySQLQuery;
                Database: TmySQLDatabase; Grade: TDBGrid; TabLctA, TabPrvA,
                TabAriA, TabCnsA: String; Cond, Periodo: Integer;
                Progress: TProgressBar): Boolean;
    function  VerificaSeHouveEmissaoPosterior(QueryAux: TmySQLQuery;
                Database: TmySQLDatabase; TabAriA, TabPrvA, TabCnsA: String;
                Cond, Periodo: Integer): Boolean;
    function  VerificaSeAlgumItemDoBoletoEstaPago(QueryBoletosIts, QueryAux: TmySQLQuery;
                Database: TmySQLDatabase; TabLctA: String): Boolean;
    function  ImprimeBloqueto(CkZerado, Escolher: Boolean; TabLctA, TabLctB,
                TabLctD: String; CliInt, EntCliInt: Integer; QueryPrev,
                QueryBoletos, QueryBoletosIts: TmySQLQuery; frxDsPRI,
                frxDsBoletos, frxDsBoletosIts: TfrxDBDataset; Quais: TselType;
                Como: TfrxImpComo; Arquivo: String; Filtro: TfrxCustomExportFilter;
                Grade: TDBGrid): TfrxReport;
    procedure PreencheModelosBloq(RG: TRadioGroup);
    procedure ReopenPRI(Query: TmySQLQuery; Codigo, Controle: Integer; Tabela: String);
    procedure ReopenQrCons(Query, QuerySum: TmySQLQuery; Tabela: String; CliInt,
              Periodo, Codigo: Integer);
    procedure ReopenCNS(Query: TmySQLQuery; CliInt, Periodo, Codigo,
              Controle: Integer; Tabela: String);
    procedure ReopenArre(Query, QuerySum: TmySQLQuery; Codigo, Apto,
              Propriet: Integer; Tabela: String);
    procedure ReopenArreBol(Query: TmySQLQuery; Codigo, Apto, Propriet: Integer;
              Boleto: Double; Tabela: String);
    procedure ReopenARI(Query: TmySQLQuery; Codigo, Controle, Apto,
              Propriet: Integer; Boleto: Double; Tabela: String);
    procedure ReopenBoletos(Query, QryArr, QryLei: TmySQLQuery; BOLAPTO: String;
              PreBol: Boolean; TabelaPrvA, TabelaAriA, TabelaCnsA: String;
              CliInt, Periodo, Codigo: Integer);
    procedure ReopenBoletosIts(Query: TmySQLQuery; TabelaAriA, TabelaCnsA: String;
              Boleto: Double; Codigo, Periodo, Apto, CNAB_Cfg: Integer);
    procedure ReopenPrevModBol(Query: TmySQLQuery; Codigo, Apto: Integer);
    procedure ReopenQrSumBol(Query: TmySQLQuery; TabelaAriA, TabelaCnsA: String;
              CliInt, Periodo, Codigo: Integer; PreBol: Boolean);
    procedure ReopenQrSumCP(Query: TmySQLQuery; TabelaCnsA: String; CliInt,
              Periodo, Codigo: Integer);
    procedure ReopenMU6PM(CliInt, LastP, Genero: Integer; TabelaLctA,
              TabelaLctB, TabelaLctD: String; QueryMU6PM, QueryMU6PT: TmySQLQuery);
    procedure ReopenMU6MM(CliInt, LastP, Genero: Integer; TabelaLctA,
              TabelaLctB, TabelaLctD: String; QueryMU6MM, QueryMU6MT: TmySQLQuery);
    procedure ExcluiLeituraAtual(Controle: Integer; Lancto, Boleto: Double;
              TabCnsA, TabLctA: String);
    procedure ReopenArreFut(QueryArreFutI: TmySQLQuery; CliInt, MesesAnt,
              Controle: Integer);
    procedure ReopenCNAB_CfgSel(Query: TmySQLQuery; Cliente: Integer);
    procedure ExcluiItesArrecadacao(TabAriA, TabLctA: String; Controle,
              Lancto: Integer; Boleto: Double);
    procedure ExcluiItemPreBloqueto(Tipo: TselType; TabAriA, TabCnsA: String;
              QueryBoletosIts: TmySQLQuery; Grade: TDBGrid);
    procedure ReopenProt1(QueryProt1: TmySQLQuery; Aba, EntCliInt, Codigo,
              Periodo: Integer; TabPrvA, TabAriA, TabCnsA: String);
  end;

var
  UBloqCond: TUnBloqCond;

implementation

uses
{$IfNDef SemProtocolo}
  Protocolo,
{$EndIf}
  DmkDAC_PF, UnBloqGerl, UCreate, UnFinanceiro, UnMyObjects, MyDBCheck,
  UnBloqCond_Jan, BloImp;

{ TUnBloqCond }

function TUnBloqCond.GetPercAptAll(Controle, ListaBaU: Integer): Double;
var
  Ctrl: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    if ListaBau <> 0 then
      Ctrl := ListaBaU
    else
      Ctrl := Controle;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(Percent) TotPerc ',
      'FROM arrebau ',
      'WHERE Controle=' + Geral.FF0(Ctrl),
      '']);
    Result := Qry.FieldByName('TotPerc').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnBloqCond.VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts,
  QueryAux: TmySQLQuery; Database: TmySQLDatabase; TabLctA: String): Boolean;
var
  Lcts: String;
begin
  //Verifica se todos os lan�amentos est�o na tabela TabLctA caso n�o, n�o atualiza nenhum
  try
    Lcts := '';
    //
    QueryBoletosIts.First;
    //
    while not QueryBoletosIts.Eof do
    begin
      if QueryBoletosIts.RecNo = QueryBoletosIts.RecordCount then
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger)
      else
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger) + ', ';
      //
      QueryBoletosIts.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
      'SELECT Documento ',
      'FROM ' + TabLctA,
      'WHERE Controle IN (' + Lcts + ')',
      '']);
    if QueryBoletosIts.RecordCount = QueryAux.RecordCount then
      Result := True
    else
      Result := False;
  except
    Result := False;
  end;
end;

function TUnBloqCond.AlteraVencimentoPreBol(CliInt, Periodo, Depto: Integer;
  Prebol: Boolean; Tipo: TSelType; Grade: TDBGrid; QueryBoletos, QueryBoletosIts,
  QueryUpd, QueryAux: TmySQLQuery; Database: TmySQLDatabase; Progress: TProgressBar;
  TabLctA: String): Boolean;

  function AtualizaVenctoBoletosIts(QueryBoletosIts,
    QueryAux, QueryUpd: TmySQLQuery; Database: TmySQLDatabase; DataSel: TDateTime;
    Prebol: Boolean; TabLctA: String): Boolean;
  var
    TabUpd, Vencto: String;
    Controle, Lancto: Integer;
    Continua: Boolean;
  begin
    try
      Result := True;
      //
      if (Lancto <> 0) and (Prebol = False) and (TabLctA <> '') then
        Continua := VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts, QueryAux,
                      Database, TabLctA)
      else
        Continua := True;
      //
      if Continua then
      begin
        QueryBoletosIts.First;
        //
        while not QueryBoletosIts.Eof do
        begin
          TabUpd   := QueryBoletosIts.FieldByName('TabelaOrig').AsString;
          Controle := QueryBoletosIts.FieldByName('Controle').AsInteger;
          Vencto   := Geral.FDT(DataSel, 1);
          Lancto   := QueryBoletosIts.FieldByName('Lancto').AsInteger;
          //
          Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabUpd, False,
                      ['Vencto'], ['Controle'], [Vencto], [Controle], True);
          //
          if (Lancto <> 0) and (Prebol = False) and (TabLctA <> '') then
          begin
            Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabLctA, False,
                        ['Vencimento'], ['Controle'], [Vencto], [Lancto], True);
          end;
          //
          QueryBoletosIts.Next;
        end;
      end else
      begin
        Geral.MB_Aviso('Lan�amento(s) financeiro(s) n�o foram localizados para o boleto n�mero ' +
          FloatToStr(QueryAux.FieldByName('Documento').AsFloat) + sLineBreak +
          'Altera��o de vencimento cancelada!');
        Result := False;
      end;
    except
      Result := False;
    end;
  end;

var
  I, MesValido, DiaVencto: Integer;
  MesTxt: String;
  DataDef, DataSel: TDateTime;
  ValidouMes: Boolean;
begin
  Result := False;
  //
  if (QueryBoletos.State <> dsInactive) and (QueryBoletos.RecordCount > 0) then
  begin
    DiaVencto := ObtemDiaVenctoDeApto(CliInt, Depto);
    DataDef   := dmkPF.UltimoDiaDoPeriodo_Date(Periodo) + DiaVencto;
    //
    if not DBCheck.ObtemData(DataDef, DataSel, VAR_DATA_MINIMA) then Exit;
    //
    ValidouMes := UBloqGerl.ValidaVencimentoPeriodo(nil, nil, Periodo, DataSel, MesValido);
    MesTxt     := dmkPF.VerificaMes(MesValido, False);
    //
    if MyObjects.FIC(ValidouMes, nil, 'O vencimento deve ser correspondente ao m�s ' + MesTxt + '!') then Exit;
    //
    if Geral.MB_Pergunta('Confirma a altera��o do vencimento para a data ' +
      Geral.FDT(DataSel, 2) + '?') = ID_YES then
    begin
      try
        Progress.Position := 0;
        Grade.Enabled     := False;
        //
        QueryBoletos.DisableControls;
        QueryBoletosIts.DisableControls;
        //
        case Tipo of
          istAtual:
          begin
            Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                        QueryUpd, Database, DataSel, Prebol, TabLctA);
          end;
          istSelecionados:
          begin
            if Grade.SelectedRows.Count > 0 then
            begin
              with Grade.DataSource.DataSet do
              for I := 0 to Grade.SelectedRows.Count - 1 do
              begin
                GotoBookmark(pointer(Grade.SelectedRows.Items[I]));
                //
                Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                            QueryUpd, Database, DataSel, Prebol, TabLctA);
              end;
            end else
              Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                          QueryUpd, Database, DataSel, Prebol, TabLctA);
          end;
          istTodos:
          begin
            Progress.Max := QueryBoletos.RecordCount;
            //
            QueryBoletos.First;
            //
            while not QueryBoletos.Eof do
            begin
              Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                          QueryUpd, Database, DataSel, Prebol, TabLctA);
              //
              Progress.Position := Progress.Position + 1;
              Progress.Update;
              Application.ProcessMessages;
              //
              QueryBoletos.Next;
            end;
          end
          else
          begin
            Geral.MB_Aviso('Tipo de sele��o n�o implementada!');
            Exit;
          end;
        end;
      finally
        Progress.Position := 0;
        Grade.Enabled     := True;
        //
        QueryBoletos.EnableControls;
        QueryBoletosIts.EnableControls;
      end;
    end;
  end;
end;

procedure TUnBloqCond.ExcluiItemPreBloqueto(Tipo: TselType; TabAriA, TabCnsA: String;
  QueryBoletosIts: TmySQLQuery; Grade: TDBGrid);

  procedure ExcluiLeituraAtual;
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      DELETE_FROM  + TabCnsA,
      'WHERE Controle=' + Geral.FF0(QueryBoletosIts.FieldByName('Controle').AsInteger),
      '']);
  end;

  procedure ExcluiArrecadacaoAtual;
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      DELETE_FROM  + TabAriA,
      'WHERE Controle=' + Geral.FF0(QueryBoletosIts.FieldByName('Controle').AsInteger),
      '']);
  end;

var
  i: integer;
begin
  i := 0;
  if Tipo <> istTodos then
  begin
    case Tipo of
      istAtual: i := 1;
      istSelecionados:
      begin
        i := Grade.SelectedRows.Count;
        if i = 0 then i := 1;
      end;
    end;
    if i = 1 then
    begin
      if Geral.MensagemBox('Confirma a exclus�o do item de pr�-bloqueto selecionado?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        if QueryBoletosIts.FieldByName('Tipo').AsInteger = 0 then
          ExcluiArrecadacaoAtual
        else
          ExcluiLeituraAtual;
      end;
    end else begin
      if Geral.MB_Pergunta('Confirma a exclus�o dos ' +
        Geral.FF0(Grade.SelectedRows.Count) +
        ' itens de pr�-bloquetos selecionados?') = ID_YES then
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          if QueryBoletosIts.FieldByName('Tipo').AsInteger = 0 then
            ExcluiArrecadacaoAtual
          else
            ExcluiLeituraAtual;
        end;
      end;
    end;
  end else
  begin
    QueryBoletosIts.First;
    while not QueryBoletosIts.Eof do
    begin
      if QueryBoletosIts.FieldByName('Tipo').AsInteger = 0 then
        ExcluiArrecadacaoAtual
      else
        ExcluiLeituraAtual;
      QueryBoletosIts.Next;
    end;
  end;
end;

procedure TUnBloqCond.ExcluiItesArrecadacao(TabAriA, TabLctA: String; Controle,
  Lancto: Integer; Boleto: Double);
var
  QrLocLct: TmySQLQuery;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    DELETE_FROM  + TabAriA,
    'WHERE Controle=' + Geral.FF0(Controle) + ' AND Boleto=0',
    '']);
  if (Lancto <> 0) and (Boleto <> 0) then
  begin
    QrLocLct := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QrLocLct, Dmod.MyDB, [
        'SELECT Data, Tipo, Carteira, Sub ',
        'FROM ' + TabLctA + ' ',
        'WHERE Controle=' + Geral.FF0(Lancto),
        'AND FatID=' + Geral.FF0(VAR_FATID_0600), //600 FATID da Arrecada��o
        '']);
      if (Lancto <> 0) and (Boleto <> 0) then
      begin
        if QrLocLct.RecordCount > 0 then
        begin
          UFinanceiro.ExcluiLct_Unico(TabLctA, Dmod.MyDB,
            QrLocLct.FieldByName('Data').AsDateTime,
            QrLocLct.FieldByName('Tipo').AsInteger,
            QrLocLct.FieldByName('Carteira').AsInteger,
            Lancto, QrLocLct.FieldByName('Sub').AsInteger,
            CO_MOTVDEL_306_EXCLUILCTARRECADACAO, False, False);
        end;
      end;
    finally
      QrLocLct.Free;
    end;
  end;
end;

function TUnBloqCond.GetPercAptUni(Controle, ListaBaU, Conta: Integer;
  NomeConta: String): Double;
var
  Ctrl: Integer;
  UH: String;
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    if ListaBau <> 0 then
      Ctrl := ListaBaU
    else
      Ctrl := Controle;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Percent ',
      'FROM arrebau ',
      'WHERE Controle=' + Geral.FF0(Ctrl),
      'AND Apto=' + Geral.FF0(Conta),
      '']);
    // Deve encontrar apenas um registro
    if Qry.RecordCount = 1 then
      Result := Qry.FieldByName('Percent').AsInteger
    else begin
      UH := DModG.ObtemNomeDepto(Conta);
      //
      if Qry.RecordCount = 0 then
        Geral.MB_Aviso('N�o h� item cadastrado para a unidade ' + UH +
          ' para a arrecada��o "' + NomeConta + '"! ')
      else
        Geral.MB_Aviso('H� mais de um item cadastrados para a unidade ' + UH +
          ' para a arrecada��o "' + NomeConta + '"! ');
    end;
  finally
    Qry.Free;
  end;
end;

function TUnBloqCond.ImprimeBloqueto(CkZerado, Escolher: Boolean; TabLctA,
  TabLctB, TabLctD: String; CliInt, EntCliInt: Integer; QueryPrev, QueryBoletos,
  QueryBoletosIts: TmySQLQuery; frxDsPRI, frxDsBoletos,
  frxDsBoletosIts: TfrxDBDataset; Quais: TselType; Como: TfrxImpComo;
  Arquivo: String; Filtro: TfrxCustomExportFilter; Grade: TDBGrid): TfrxReport;
begin
  Result := nil;
  //
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmBloImp, FmBloImp);
    //
    Result := FmBloImp.ImprimeBloq(CkZerado, Escolher, TabLctA, TabLctB, TabLctD,
                CliInt, EntCliInt, QueryPrev, QueryBoletos, QueryBoletosIts,
                frxDsPRI, frxDsBoletos, frxDsBoletosIts, Quais, Como, Arquivo,
                Filtro, Grade);
    //
    FmBloImp.Destroy;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnBloqCond.LetraModelosBloq(Item: Integer): String;
begin
  case Item of
     1: Result := 'A';
     2: Result := 'B';
     3: Result := 'C';
     4: Result := 'D';
     5: Result := 'E';
     6: Result := 'G';
     7: Result := 'G';
     8: Result := 'H';
     9: Result := 'R';
    10: Result := 'IB';
    11: Result := 'IR';
    else Result := '?';
  end;
end;

function TUnBloqCond.ObtemDiaVenctoDeApto(CliInt, Depto: Integer): Integer;
var
  Qry: TMySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ddVctEsp ',
      'FROM condimov ',
      'WHERE Conta=' + Geral.FF0(Depto),
      'AND ddVctEsp <> 0 ',
      '']);
    if Qry.RecordCount = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT DiaVencto ',
        'FROM cond ',
        'WHERE Codigo=' + Geral.FF0(CliInt),
        '']);
      if Qry.RecordCount > 0 then
        Result := Qry.FieldByName('DiaVencto').AsInteger
      else
        Result := 0;
    end else
      Result := Qry.FieldByName('ddVctEsp').AsInteger;
  finally
    Qry.Free;
  end;
end;

procedure TUnBloqCond.ReopenCNAB_CfgSel(Query: TmySQLQuery; Cliente: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM CNAB_Cfg ',
    'WHERE Cedente=' + Geral.FF0(Cliente),
    '']);
end;

procedure TUnBloqCond.PreencheModelosBloq(RG: TRadioGroup);
begin
  RG.Items.Clear;
  {00} RG.Items.Add('? - Definir no momento da impress�o');
  {01} RG.Items.Add('A - 3 colunas fonte tamanho 7 sem rodap� (3 n�veis)');
  {02} RG.Items.Add('B - 3 colunas fonte tamanho 6 sem rodap� (3 n�veis)');
  {03} RG.Items.Add('C - 3 colunas fonte tamanho 7 com rodap� (3 n�veis)');
  {04} RG.Items.Add('D - 2 colunas fonte tamanho 6 com rodap� (3 n�veis)');
  {05} RG.Items.Add('E - Somente bloqueto (nenhum n�vel)');
  {06} RG.Items.Add('F - Extinto (se selecionado ser� usado o modelo G)');
  {07} RG.Items.Add('G - 3 colunas fonte tamanho 7 com rodap� (4 n�veis)');
  {08} RG.Items.Add('H - Colunas, fonte e tamanho vari�vel (4 n�veis) - C�digo de barras');
  {09} RG.Items.Add('R - Colunas, fonte e tamanho vari�vel (4 n�veis) - Recibo');
  {10} RG.Items.Add('IB - 1 coluna, fonte e tamanho vari�vel (1 n�vel)');
  {11} RG.Items.Add('IR - 1 coluna, fonte e tamanho vari�vel (1 n�vel) - Recibo');
end;

procedure TUnBloqCond.ReopenCNS(Query: TmySQLQuery; CliInt, Periodo, Codigo,
  Controle: Integer; Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT cdb.Descri Bloco, cdi.Unidade, cnp.Casas, cnp.UnidLei, ',
    'cnp.UnidImp, cnp.UnidFat, cnp.CasRat, cni.* ',
    'FROM cons cns ',
    'LEFT JOIN ' + Tabela + '  cni ON cni.Codigo=cns.Codigo ',
    'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
    'LEFT JOIN condimov cdi ON cdi.Conta=cni.Apto ',
    'LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle ',
    'WHERE cni.Cond=' + Geral.FF0(CliInt),
    'AND cnp.Cond=' + Geral.FF0(CliInt),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    'AND cni.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY cdb.Ordem, cdi.Andar, cdi.Unidade ',
    '']);
  if Controle <> 0 then
    Query.Locate('Controle', Controle, []);
end;

procedure TUnBloqCond.ReopenMU6MM(CliInt, LastP, Genero: Integer; TabelaLctA,
  TabelaLctB, TabelaLctD: String; QueryMU6MM, QueryMU6MT: TmySQLQuery);

  procedure GeraParteSQL_MU6MM(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QueryMU6MM.SQL.Add('SELECT ((YEAR(lan.Data)-2000) * 100) + MONTH(lan.Data) MEZ,');
    QueryMU6MM.SQL.Add('SUM(lan.Debito) SUM_DEB');
    QueryMU6MM.SQL.Add('FROM ' + TabLct + ' lan');
    QueryMU6MM.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QueryMU6MM.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QueryMU6MM.SQL.Add('AND car.Tipo<>2');
    QueryMU6MM.SQL.Add('AND lan.Mez=0');
    QueryMU6MM.SQL.Add('AND lan.Debito>0');
    QueryMU6MM.SQL.Add('AND lan.Genero=' + GenTXT);
    QueryMU6MM.SQL.Add('AND ((YEAR(lan.Data)-2000) * 100) + ');
    QueryMU6MM.SQL.Add('MONTH(lan.Data) BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
    QueryMU6MM.SQL.Add('GROUP BY ((YEAR(lan.Data)-2000) * 100) + MONTH(lan.Data)');
  end;

  procedure GeraParteSQL_MU6MT(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QueryMU6MT.SQL.Add('SELECT SUM(lan.Debito) SUM_DEB');
    QueryMU6MT.SQL.Add('FROM ' + TabLct + ' lan');
    QueryMU6MT.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QueryMU6MT.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QueryMU6MT.SQL.Add('AND car.Tipo<>2');
    QueryMU6MT.SQL.Add('AND lan.Debito>0');
    QueryMU6MT.SQL.Add('AND lan.Mez=0');
    QueryMU6MT.SQL.Add('AND lan.Genero=' + GenTXT);
    QueryMU6MT.SQL.Add('AND ((YEAR(lan.Data)-2000) * 100) +');
    QueryMU6MT.SQL.Add('MONTH(lan.Data) BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
  end;

var
  MU6MM, MU6MT, Mez1, Mez6, CliTXT, GenTXT: String;
begin
  Mez1   := Geral.FF0(dmkPF.PeriodoToAnoMes(LastP-1));
  Mez6   := Geral.FF0(dmkPF.PeriodoToAnoMes(LastP-6));
  CliTXT := Geral.FF0(CliInt);
  GenTXT := Geral.FF0(Genero);
  //
  QueryMU6MM.Close;
  QueryMU6MM.Database := DModG.MyPID_DB;
  MU6MM               := UCriar.RecriaTempTableNovo(ntrttMU6MM, DmodG.QrUpdPID1, False);
  QueryMU6MM.SQL.Clear;
  //
  QueryMU6MM.SQL.Add('INSERT INTO ' + MU6MM);
  QueryMU6MM.SQL.Add('');
  GeraParteSQL_MU6MM(TabelaLctA, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MM.SQL.Add('UNION');
  GeraParteSQL_MU6MM(TabelaLctB, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MM.SQL.Add('UNION');
  GeraParteSQL_MU6MM(TabelaLctD, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MM.SQL.Add(';');
  QueryMU6MM.SQL.Add('');
  QueryMU6MM.SQL.Add('SELECT MEZ, SUM(SUM_DEB) SUM_DEB ');
  QueryMU6MM.SQL.Add('FROM ' + MU6MM);
  QueryMU6MM.SQL.Add('GROUP BY MEZ ');
  QueryMU6MM.SQL.Add('ORDER BY MEZ;');
  QueryMU6MM.SQL.Add('');
  UMyMod.AbreQuery(QueryMU6MM, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  //
  QueryMU6MT.Close;
  QueryMU6MT.Database := DModG.MyPID_DB;
  MU6MT               := UCriar.RecriaTempTableNovo(ntrttMU6MT, DmodG.QrUpdPID1, False);
  QueryMU6MT.SQL.Clear;
  //
  QueryMU6MT.SQL.Add('INSERT INTO ' + MU6MT);
  QueryMU6MT.SQL.Add('');
  GeraParteSQL_MU6MT(TabelaLctA, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MT.SQL.Add('UNION');
  GeraParteSQL_MU6MT(TabelaLctB, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MT.SQL.Add('UNION');
  GeraParteSQL_MU6MT(TabelaLctD, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MT.SQL.Add(';');
  QueryMU6MT.SQL.Add('');
  QueryMU6MT.SQL.Add('SELECT SUM(SUM_DEB) SUM_DEB ');
  QueryMU6MT.SQL.Add('FROM ' + MU6MT + ';');
  QueryMU6MT.SQL.Add('');
  UMyMod.AbreQuery(QueryMU6MT, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
end;

procedure TUnBloqCond.ReopenMU6PM(CliInt, LastP, Genero: Integer; TabelaLctA,
  TabelaLctB, TabelaLctD: String; QueryMU6PM, QueryMU6PT: TmySQLQuery);

  procedure GeraParteSQL_MU6PM(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QueryMU6PM.SQL.Add('SELECT lan.Mez, SUM(lan.Debito) SUM_DEB');
    QueryMU6PM.SQL.Add('FROM ' + TabLct + ' lan');
    QueryMU6PM.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QueryMU6PM.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QueryMU6PM.SQL.Add('AND car.Tipo<>2');
    QueryMU6PM.SQL.Add('AND lan.Debito>0');
    QueryMU6PM.SQL.Add('AND lan.Genero=' + GenTXT);
    QueryMU6PM.SQL.Add('AND lan.Mez BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
    QueryMU6PM.SQL.Add('GROUP BY lan.Mez');
  end;

  procedure GeraParteSQL_MU6PT(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QueryMU6PT.SQL.Add('SELECT SUM(lan.Debito) SUM_DEB');
    QueryMU6PT.SQL.Add('FROM ' + TabLct + ' lan');
    QueryMU6PT.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QueryMU6PT.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QueryMU6PT.SQL.Add('AND car.Tipo<>2');
    QueryMU6PT.SQL.Add('AND lan.Debito>0');
    QueryMU6PT.SQL.Add('AND lan.Genero=' + GenTXT);
    QueryMU6PT.SQL.Add('AND lan.Mez BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
  end;

var
  Mez1, Mez6, CliTXT, GenTxt: String;
begin
  Mez1   := Geral.FF0(dmkPF.PeriodoToAnoMes(LastP-1));
  Mez6   := Geral.FF0(dmkPF.PeriodoToAnoMes(LastP-6));
  CliTXT := Geral.FF0(CliInt);
  GenTXT := Geral.FF0(Genero);
  //
  QueryMU6PM.Close;
  QueryMU6PM.Database := DModG.MyPID_DB;
  QueryMU6PM.SQL.Clear;
  //
  QueryMU6PM.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_MU6PM_;');
  QueryMU6PM.SQL.Add('CREATE TABLE _MOD_COND_MU6PM_');
  QueryMU6PM.SQL.Add('');
  GeraParteSQL_MU6PM(TabelaLctA, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PM.SQL.Add('UNION');
  GeraParteSQL_MU6PM(TabelaLctB, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PM.SQL.Add('UNION');
  GeraParteSQL_MU6PM(TabelaLctD, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PM.SQL.Add(';');
  QueryMU6PM.SQL.Add('');
  QueryMU6PM.SQL.Add('SELECT Mez, SUM(SUM_DEB) SUM_DEB ');
  QueryMU6PM.SQL.Add('FROM _MOD_COND_MU6PM_');
  QueryMU6PM.SQL.Add('GROUP BY Mez');
  QueryMU6PM.SQL.Add('ORDER BY Mez DESC');
  QueryMU6PM.SQL.Add('');
  UMyMod.AbreQuery(QueryMU6PM, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  //
  //
  QueryMU6PT.Close;
  QueryMU6PT.Database := DModG.MyPID_DB;
  QueryMU6PT.SQL.Clear;
  //
  QueryMU6PT.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_MU6PT_;');
  QueryMU6PT.SQL.Add('CREATE TABLE _MOD_COND_MU6PT_');
  QueryMU6PT.SQL.Add('');
  GeraParteSQL_MU6PT(TabelaLctA, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PT.SQL.Add('UNION');
  GeraParteSQL_MU6PT(TabelaLctB, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PT.SQL.Add('UNION');
  GeraParteSQL_MU6PT(TabelaLctD, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PT.SQL.Add(';');
  QueryMU6PT.SQL.Add('');
  QueryMU6PT.SQL.Add('SELECT SUM(SUM_DEB) SUM_DEB ');
  QueryMU6PT.SQL.Add('FROM _MOD_COND_MU6PT_;');
  QueryMU6PT.SQL.Add('');
  UMyMod.AbreQuery(QueryMU6PT, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
end;

procedure TUnBloqCond.ReopenPRI(Query: TmySQLQuery; Codigo, Controle: Integer;
  Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT pit.*, con.SubGrupo, ',
    'con.Codigo CODCONTA, sgo.Codigo CODSUBGRUPO, ',
    'con.Nome NOMECONTA, sgo.Nome NOMESUBGRUPO ',
    'FROM ' + Tabela + ' pit ',
    'LEFT JOIN contas con ON con.Codigo=pit.Conta ',
    'LEFT JOIN subgrupos sgo ON sgo.Codigo=con.SubGrupo ',
    'WHERE pit.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY NOMESUBGRUPO, NOMECONTA ',
    '']);
  if Controle <> 0 then
    Query.Locate('Controle', Controle, []);
end;

procedure TUnBloqCond.ReopenProt1(QueryProt1: TmySQLQuery; Aba, EntCliInt,
  Codigo, Periodo: Integer; TabPrvA, TabAriA, TabCnsA: String);
{$IfNDef SemProtocolo}
var
  ProtTipo: Integer;
  SQLCompl, Campo: String;
begin
  case Aba of
    0:
    begin
      Campo    := 'Protocolo';
      ProtTipo := 1;
      SQLCompl := 'OR (ari.' + Campo + ' = 0)';
    end;
    1:
    begin
      Campo    := 'Protocolo';
      ProtTipo := 2;
      SQLCompl := 'OR (ari.' + Campo + ' = 0)';
    end;
    2:
    begin
      ProtTipo := 4;
    end;
    else
    begin
      ProtTipo := -1;
    end;
  end;
  if Aba in [0,1] then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QueryProt1, Dmod.MyDB, [
      'SELECT ',
      'pro.Codigo ',
      'pro.Nome ',
      'pro.PreEmeio ',
      'mai.NaoEnvBloq ',
      'FROM ' + TabAriA + ' ari ',
      'LEFT JOIN ' + TabPrvA + ' prv ON prv.Codigo=ari.Codigo ',
      'LEFT JOIN protocolos pro ON pro.Codigo = ari.Protocolo ',
      'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
      'LEFT JOIN protocolos pr2 ON pr2.Codigo = ari.Protocolo2 ',
      'LEFT JOIN preemail ma2 ON ma2.Codigo = pr2.PreEmeio ',
      'WHERE ari.Codigo=' + Geral.FF0(Codigo),
      'AND ari.Boleto <> 0 ',
      'AND ((pro.Tipo=' + Geral.FF0(ProtTipo) + ' ',
      'AND pro.def_client=' + Geral.FF0(EntCliInt) + ') ',
      SQLCompl + ')',
      'GROUP BY pro.Codigo ',
      ' ',
      'UNION ',
      ' ',
      'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
      'FROM ' + TabCnsA + ' cns ',
      'LEFT JOIN ' + TabPrvA + ' prv ON prv.Codigo=cns.Periodo ',
      'LEFT JOIN protocolos pro ON pro.Codigo = cns.Protocolo ',
      'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
      'LEFT JOIN protocolos pr2 ON pr2.Codigo = ari.Protocolo2 ',
      'LEFT JOIN preemail ma2 ON ma2.Codigo = pr2.PreEmeio ',
      'WHERE cns.Periodo=' + Geral.FF0(Periodo),
      'AND cns.Boleto <> 0 ',
      'AND ((pro.Tipo=' + Geral.FF0(ProtTipo) + ' ',
      'AND pro.def_client=' + Geral.FF0(EntCliInt) + ') ',
      SQLCompl + ')',
      'GROUP BY pro.Codigo ',
      '']);
  end else
  begin
    //Com registro
    UnDmkDAC_PF.AbreMySQLQuery0(QueryProt1, Dmod.MyDB, [
      'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
      'FROM ' + TabAriA + ' ari ',
      'LEFT JOIN protocolos pro ON pro.Codigo = ari.ProtocoloCR ',
      'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
      'LEFT JOIN ' + TabPrvA + ' prv ON prv.Codigo=ari.Codigo ',
      'WHERE ari.Codigo=' + Geral.FF0(Codigo),
      'AND ari.Boleto <> 0 ',
      'AND (pro.Tipo=' + Geral.FF0(ProtTipo) + ' ',
      'AND pro.def_client=' + Geral.FF0(EntCliInt) + ') ',
      'GROUP BY pro.Codigo ',
      ' ',
      'UNION ',
      ' ',
      'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
      'FROM ' + TabCnsA + ' cns ',
      'LEFT JOIN protocolos pro ON pro.Codigo = cns.ProtocoloCR ',
      'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
      'LEFT JOIN ' + TabPrvA + ' prv ON prv.Codigo=cns.Periodo ',
      'WHERE cns.Periodo=' + Geral.FF0(Periodo),
      'AND cns.Boleto <> 0 ',
      'AND (pro.Tipo=' + Geral.FF0(ProtTipo) + ' ',
      'AND pro.def_client=' + Geral.FF0(EntCliInt) + ') ',
      'GROUP BY pro.Codigo ',
      '']);
  end;
  (*
    'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
    'FROM arreits ari ',
    'LEFT JOIN protocolos pro ON pro.Codigo = ari.' + Campo,
    'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
    'LEFT JOIN prev pre ON pre.Codigo = ari.Codigo ',
    'WHERE '
    'AND ((pro.Tipo=' + Geral.FF0(ProtTipo) + ' ',
    'AND pro.def_client=' + Geral.FF0(EntCliInt) + ') ',
    SQLCompl + ')',
    'GROUP BY pro.Codigo',
    '']);
  *)
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TUnBloqCond.ReopenQrCons(Query, QuerySum: TmySQLQuery; Tabela: String;
  CliInt, Periodo, Codigo: Integer);
var
  Cond_Txt, Peri_Txt:String;
begin
  Cond_Txt := Geral.FF0(CliInt);
  Peri_Txt := Geral.FF0(Periodo);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT DISTINCT cns.Codigo, cns.Nome, cnp.Preco, ',
    'cnp.Casas, cnp.UnidLei, cnp.UnidImp, cnp.UnidFat ',
    'FROM consprc cnp ',
    'LEFT JOIN cons    cns ON cns.Codigo=cnp.Codigo ',
    'LEFT JOIN ' + Tabela + ' cni ON cni.Codigo=cns.Codigo ',
    'WHERE cni.Cond=' + Cond_Txt,
    'AND cnp.Cond=' + Cond_Txt,
    'AND cni.Periodo=' + Peri_Txt,
    '']);
  if Codigo <> 0 then
    Query.Locate('Codigo', Codigo, []);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QuerySum, Dmod.MyDB, [
    'SELECT SUM(Consumo) CONSUMO, SUM(Valor) VALOR ',
    'FROM ' + Tabela + ' cni ',
    'WHERE cni.Cond=' + Cond_Txt,
    'AND cni.Periodo=' + Peri_Txt,
    '']);
end;

function TUnBloqCond.TextoExplicativoItemBoleto(EhConsumo, Casas: Integer;
  MedAnt, MedAtu, Consumo, UnidFat: Double; UnidLei, UnidImp: String; GeraTyp,
  CasRat, NaoImpLei: Integer; GeraFat: Double): String;
var
  Leitura: String;
begin
  if EhConsumo = 0 then Result := '' else
  begin
    if GeraTyp = 0 then
      Result := ' (' + Geral.FFT(MedAnt, Casas, siNegativo) + ' ' +
      ' > ' + Geral.FFT(MedAtu, Casas, siNegativo) + ' ' +
      ' = ' + Geral.FFT(Consumo, Casas, siNegativo) + ' ' +UnidLei +
      ' ~ ' + Geral.FFT(Consumo * UnidFat, Casas, siNegativo) +
      ' ' + UnidImp + ')'
    else begin
      if NaoImpLei = 1 then Leitura := '' else Leitura := ' ~ ' +
        Geral.FFT(Consumo * UnidFat, Casas, siNegativo) + ' ' + UnidLei;
      Result := ' (' + UnidImp + ': ' + Geral.FFT(GeraFat, CasRat, siNegativo) +
      Leitura + ')';
    end;
  end;
end;

function TUnBloqCond.VerificaSeLctEstaoNaTabLctA(TabLctA: String;
  Lancto: Integer; QueryAux: TmySQLQuery; var Msg: String): Boolean;
begin
  //True  = Est� na tabela LctA
  //False = N�o est� na tabela LctA
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + TabLctA,
    'WHERE Controle=' + Geral.FF0(Lancto),
    '']);
  if QueryAux.RecordCount > 0 then
  begin
    Msg    := '';
    Result := True;
  end else
  begin
    Msg    := 'N�o foi poss�vel localizar o lan�amento financeiro!';
    Result := False;
  end;
end;

function TUnBloqCond.VerificaSeAlgumItemDoBoletoEstaPago(QueryBoletosIts,
  QueryAux: TmySQLQuery; Database: TmySQLDatabase; TabLctA: String): Boolean;
var
  Lcts: String;
begin
  //Verifica se todos os lan�amentos est�o em aberto
  //True  = Pago
  //False = Em aberto
  //
  try
    Lcts := '';
    //
    QueryBoletosIts.First;
    //
    while not QueryBoletosIts.Eof do
    begin
      if QueryBoletosIts.RecNo = QueryBoletosIts.RecordCount then
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger)
      else
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger) + ', ';
      //
      QueryBoletosIts.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
      'SELECT * ',
      'FROM ' + TabLctA,
      'WHERE Controle IN (' + Lcts + ')',
      'AND Sit < 2 ',
      'AND Pago = 0 ',
      '']);
    if QueryAux.RecordCount > 0 then
    begin
      if QueryBoletosIts.RecordCount = QueryAux.RecordCount then
        Result := False
      else
        Result := True;
    end else
      Result := True;
  except
    Result := True;
  end;
end;

function TUnBloqCond.VerificaSeHouveEmissaoPosterior(QueryAux: TmySQLQuery;
  Database: TmySQLDatabase; TabAriA, TabPrvA, TabCnsA: String; Cond,
  Periodo: Integer): Boolean;
begin
  //True  = Houve emiss�o posterior
  //False = N�o houve emiss�o posterior
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
    'SELECT MAX(Boleto) Boleto ',
    'FROM ' + TabCnsA,
    'WHERE Cond=' + Geral.FF0(Cond),
    'AND Periodo > ' + Geral.FF0(Periodo),
    ' ',
    'UNION ',
    ' ',
    'SELECT MAX(ari.Boleto) Boleto ',
    'FROM ' + TabAriA + ' ari ',
    'LEFT JOIN ' + TabPrvA + ' prv ON  prv.Codigo=ari.Codigo ',
    'WHERE prv.Cond= ' + Geral.FF0(Cond),
    'AND prv.Periodo > ' + Geral.FF0(Periodo),
    '',
    'ORDER BY Boleto DESC ',
    '']);
  //
  if QueryAux.FieldByName('Boleto').AsFloat > 0 then
    Result := True
  else
    Result := False;
end;

function TUnBloqCond.VerificaSeLancamentoEstaPago(Lancto: Integer;
  QueryAux: TmySQLQuery; TabLctA: String;
  var Msg: String): Boolean;
begin
  //True  = N�o possui pagamentos
  //False = Possui pagamentos
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabLctA,
    'WHERE Controle=' + Geral.FF0(Lancto),
    'AND Sit < 2 ',
    'AND Pago = 0 ',
    '']);
  if QueryAux.RecordCount > 0 then
  begin
    Msg    := 'O lan�amento financeiro possui pagamentos nele!';
    Result := False;
  end else
  begin
    Msg    := '';
    Result := True;
  end;
end;

procedure TUnBloqCond.ReopenArre(Query, QuerySum: TmySQLQuery; Codigo, Apto,
  Propriet: Integer; Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT cdi.Andar, ari.Propriet, ari.Apto, SUM(ari.Valor) Valor, ',
    'cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ',
    'ent.Nome END NOMEPROPRIET ',
    'FROM ' + Tabela + ' ari ',
    'LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet ',
    'LEFT JOIN condimov  cdi ON cdi.Conta=ari.Apto ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    'GROUP BY Propriet, Apto ',
    'ORDER BY cdi.Andar, Unidade, NOMEPROPRIET ',
    '']);
  if (Apto <> 0) and (Propriet <> 0) then
    Query.Locate('Apto;Propriet', VarArrayOf([Apto, Propriet]), []);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QuerySum, Dmod.MyDB, [
    'SELECT SUM(ari.Valor) Valor ',
    'FROM ' + Tabela + ' ari ',
    'LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet ',
    'LEFT JOIN condimov  cdi ON cdi.Conta=ari.Apto ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    '']);
end;

procedure TUnBloqCond.ReopenArreBol(Query: TmySQLQuery; Codigo, Apto,
  Propriet: Integer; Boleto: Double; Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT ari.Boleto ',
    'FROM ' + Tabela + ' ari ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    'AND ari.Apto=' + Geral.FF0(Apto),
    'AND ari.Propriet=' + Geral.FF0(Propriet),
    'GROUP BY ari.Propriet, ari.Apto, ari.Boleto ',
    'ORDER BY ari.Boleto ',
    '']);
  if Boleto <> 0 then
    Query.Locate('Boleto', Boleto, []);
end;

function TUnBloqCond.RecalculaArrecadacoes(TabelaAriA, TabelaLctA: String;
  Codigo, CliInt: Integer; Gastos: Double): Boolean;
var
  Qry, Qry2, QryUpd, QryUpd2: TmySQLQuery;
  Valor: Double;
begin
  Screen.Cursor := crHourGlass;
  //
  Result  := False;
  Qry     := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  Qry2    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QryUpd  := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QryUpd2 := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DISTINCT abi.Codigo, abi.Percent, ari.Lancto ',
      'FROM ' + TabelaAriA + ' ari',
      'LEFT JOIN arrebai abi ON abi.Codigo=ari.ArreBaC',
      'WHERE abi.Fator=1 ',
      'AND ari.Codigo=' + Geral.FF0(Codigo),
      '']);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
      'SELECT cdi.Conta, cdi.Unidade, cdi.Propriet, CASE WHEN ',
      'ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEPROPRIET, ',
      'FracaoIdeal, Moradores ',
      'FROM condimov cdi ',
      'LEFT JOIN entidades ent ON ent.Codigo=cdi.Propriet ',
      'WHERE cdi.SitImv=1 ',
      'AND cdi.Codigo=' + Geral.FF0(CliInt),
      '']);
    if (Qry.RecordCount > 0) and (Qry2.RecordCount > 0) then
    begin
      if Geral.MB_Pergunta('Houve altera��es de provis�es, ap�s ' +
        'adi��o de arrecada��es. Deseja atualizar as arrecada��es que s�o ' +
        'calculadas pelo valor das provis�es?' + sLineBreak +
        'ATEN��O: Este procedimento pode demorar alguns minutos!') = ID_YES then
      begin
        while not Qry.Eof do
        begin
          Valor := Int((Gastos * Qry.FieldByName('Percent').AsFloat / Qry2.RecordCount) + 0.005) / 100;
          //
          UMyMod.SQLInsUpd(QryUpd, stUpd, TabelaAriA, False,
            ['Valor'], ['Codigo', 'ArreBaC'],
            [Valor], [Codigo, Qry.FieldByName('Codigo').AsInteger], True);
          //
          UMyMod.SQLInsUpd(QryUpd2, stUpd, TabelaLctA, False,
            ['Credito'], ['Controle'], [Valor], [Qry.FieldByName('Lancto').AsInteger], True);
          //
          Qry.Next;
        end;
      end;
    end;
    Result := True;
  finally
    Qry.Free;
    Qry2.Free;
    QryUpd.Free;
    QryUpd2.Free;
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnBloqCond.ReopenARI(Query: TmySQLQuery; Codigo, Controle, Apto,
  Propriet: Integer; Boleto: Double; Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT ari.* ',
    'FROM ' + Tabela + ' ari ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    'AND ari.Apto=' + Geral.FF0(Apto),
    'AND ari.Propriet=' + Geral.FF0(Propriet),
    'AND ari.Boleto=' + Geral.FFI(Boleto),
    'ORDER BY Valor DESC ',
    '']);
  if Controle <> 0 then
    Query.Locate('Controle', Controle, []);
end;

procedure TUnBloqCond.ReopenBoletos(Query, QryArr, QryLei: TmySQLQuery;
  BOLAPTO: String; PreBol: Boolean; TabelaPrvA, TabelaAriA, TabelaCnsA: String;
  CliInt, Periodo, Codigo: Integer);
var
  TipoBol: String;
begin
  if PreBol then
    TipoBol := '='
  else
    TipoBol := '<>';
  //
  // Arrecada��es
  if QryArr <> nil then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QryArr, Dmod.MyDB, [
      'SELECT ari.Boleto, ari.Apto, SUM(ari.Valor) Valor, ',
      'CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO ',
      'FROM ' + TabelaAriA + ' ari ',
      'WHERE ari.Codigo=' + Geral.FF0(Codigo),
      'GROUP BY ari.Boleto, ari.Apto ',
      '']);
  end;
  // Leituras
  if QryLei <> nil then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QryLei, Dmod.MyDB, [
      'SELECT cni.Boleto, cni.Apto, SUM(cni.Valor) Valor, ',
      'CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO ',
      'FROM ' + TabelaCnsA + ' cni ',
      'WHERE cni.Cond=' + Geral.FF0(CliInt),
      'AND cni.Periodo=' + Geral.FF0(Periodo),
      'GROUP BY cni.Boleto, cni.Apto ',
      '']);
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, ',
    'cdi.Juridico, ari.Boleto, ari.Apto, ',
    'ari.Propriet, ari.Vencto, cdi.Unidade, ',
    'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
    'ELSE ent.Nome END NOMEPROPRIET, ',
    'CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO, ',
    'cdb.IDExporta cdb_IDExpota, cdi.IDExporta cdi_IDExpota,',
    '0 KGT, 1 Ativo, ari.CNAB_Cfg, cdi.Conta, ',
{$IfNDef SemProtocolo}
    'ptc.Nome Protocolo_TXT, ptd.Nome Protocolo2_TXT, pcr.Nome ProtocoloCR_TXT, ',
{$Else}
    '"" Protocolo_TXT, "" Protocolo2_TXT, "" ProtocoloCR_TXT, ',
{$EndIf}
    'ari.ProtocoPak, ari.ProtocoPak2, ari.ProtocoPakCR, ',
    'prv.ModelBloq, prv.ConfigBol, prv.Compe, prv.BalAgrMens, cfb.Colunas, ',
    '"" USERNAME, "" PASSWORD ', //Provis�rio
    'FROM ' + TabelaAriA + ' ari',
    'LEFT JOIN ' + TabelaPrvA + ' prv ON prv.Codigo=ari.Codigo ',
    'LEFT JOIN configbol cfb ON cfb.Codigo=prv.ConfigBol ',
    'LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto ',
    'LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle ',
    'LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet ',
{$IfNDef SemProtocolo}
    'LEFT JOIN protocolos ptc ON ptc.Codigo=ari.Protocolo ',
    'LEFT JOIN protocolos ptd ON ptd.Codigo=ari.Protocolo2 ',
    'LEFT JOIN protocolos pcr ON pcr.Codigo=ari.ProtocoloCR ',
{$EndIf}
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    'AND ari.Boleto ' + TipoBol + ' 0 ',
    'GROUP BY ari.Apto, ari.CNAB_Cfg, ari.Boleto, ari.Vencto ',
    ' ',
    'UNION ',
    ' ',
    'SELECT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, ',
    'cdi.Juridico, cni.Boleto, cni.Apto, ',
    'cni.Propriet, cni.Vencto, cdi.Unidade, ',
    'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
    'ELSE ent.Nome END NOMEPROPRIET, ',
    'CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO, ',
    'cdb.IDExporta cdb_IDExpota, cdi.IDExporta cdi_IDExpota, ',
    '0 KGT, 1 Ativo, cni.CNAB_Cfg, cdi.Conta, ',
{$IfNDef SemProtocolo}
    'ptc.Nome Protocolo_TXT, ptd.Nome Protocolo2_TXT, pcr.Nome ProtocoloCR_TXT, ',
{$Else}
    '"" Protocolo_TXT, "" Protocolo2_TXT, "" ProtocoloCR_TXT, ',
{$EndIf}
    'cni.ProtocoPak, cni.ProtocoPak2, cni.ProtocoPakCR, ',
    'prv.ModelBloq, prv.ConfigBol, prv.Compe, prv.BalAgrMens, cfb.Colunas, ',
    '"" USERNAME, "" PASSWORD ', //Provis�rio
    'FROM ' + TabelaCnsA + ' cni ',
    'LEFT JOIN ' + TabelaPrvA + ' prv ON prv.Codigo=cni.Periodo ',
    'LEFT JOIN configbol cfb ON cfb.Codigo=prv.ConfigBol ',
    'LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto ',
    'LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle ',
    'LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet ',
{$IfNDef SemProtocolo}
    'LEFT JOIN protocolos ptc ON ptc.Codigo=cni.Protocolo ',
    'LEFT JOIN protocolos ptd ON ptd.Codigo=cni.Protocolo2 ',
    'LEFT JOIN protocolos pcr ON pcr.Codigo=cni.ProtocoloCR ',
{$EndIf}
    'WHERE cni.Cond=' + Geral.FF0(CliInt),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    'AND cni.Boleto ' + TipoBol + ' 0 ',
    'GROUP BY cni.Apto, cni.CNAB_Cfg, cni.Boleto, cni.Vencto ',
    ' ',
    'ORDER BY Ordem, Andar, Unidade, Boleto ',
    '']);
  if BOLAPTO <> '' then
    Query.Locate('BOLAPTO', BOLAPTO, [])
end;

procedure TUnBloqCond.ReopenBoletosIts(Query: TmySQLQuery; TabelaAriA,
  TabelaCnsA: String; Boleto: Double; Codigo, Periodo, Apto, CNAB_Cfg: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT ari.Texto TEXTO, ari.Valor VALOR, ari.Vencto, ',
    '0 Tipo, 0 MedAnt, 0 MedAtu, 0 Consumo, 0 Casas, ',
    '"" UnidLei, "" UnidImp, 1 UnidFat, Controle, Lancto, ',
    '0 GeraTyp, 0 GeraFat, 0 CasRat, 0 NaoImpLei, ',
    '"' + TabelaAriA + '" TabelaOrig, ari.Conta Genero ',
    'FROM ' + TabelaAriA + ' ari ',
    'WHERE ari.Boleto=' + Geral.FFI(Boleto),
    'AND ari.Apto=' + Geral.FF0(Apto),
    'AND ari.Codigo=' + Geral.FF0(Codigo),
    'AND ari.CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
    ' ',
    'UNION ',
    ' ',
    'SELECT cns.Nome TEXTO, cni.Valor VALOR, cni.Vencto, ',
    '1 Tipo, MedAnt, MedAtu, Consumo, Casas, ',
    'UnidLei, UnidImp, UnidFat, Controle, Lancto, ',
    'cni.GeraTyp, cni.GeraFat, cni.CasRat, cni.NaoImpLei, ',
    '"' + TabelaCnsA + '" TabelaOrig, cns.Genero ',
    'FROM cons cns ',
    'LEFT JOIN ' + TabelaCnsA + ' cni ON cni.Codigo=cns.Codigo ',
    'WHERE cni.Boleto=' + Geral.FFI(Boleto),
    'AND cni.Apto=' + Geral.FF0(Apto),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    'AND cni.CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
    ' ',
    'ORDER BY VALOR DESC ',
    '']);
end;

procedure TUnBloqCond.ReopenPrevModBol(Query: TmySQLQuery; Codigo, Apto: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT imv.Unidade, cfb.Nome NOME_CONFIGBOL, ',
    'pmb.Apto, pmb.ConfigBol, pmb.ModelBloq, ',
    'pmb.BalAgrMens, pmb.Compe, pmb.Codigo ',
    'FROM prevmodbol pmb ',
    'LEFT JOIN condimov imv ON imv.Conta=pmb.Apto ',
    'LEFT JOIN configbol cfb ON cfb.Codigo=pmb.ConfigBol ',
    'WHERE pmb.Codigo=' + Geral.FF0(Codigo),
    '']);
  if Apto <> 0 then
    Query.Locate('Apto', Apto, []);
end;

procedure TUnBloqCond.ReopenQrSumBol(Query: TmySQLQuery; TabelaAriA,
  TabelaCnsA: String; CliInt, Periodo, Codigo: Integer; PreBol: Boolean);
var
  TipoBol: String;
begin
  if PreBol then
    TipoBol := '='
  else
    TipoBol := '<>';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT SUM(Valor) VALOR ',
    'FROM ' + TabelaAriA + ' ari ',
    'LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto ',
    'LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    'AND ari.Boleto ' + TipoBol + ' 0 ',
    ' ',
    'UNION ',
    ' ',
    'SELECT SUM(Valor) VALOR ',
    'FROM ' + TabelaCnsA + ' cni ',
    'LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto ',
    'LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet ',
    'WHERE cni.Cond=' + Geral.FF0(CliInt),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    'AND cni.Boleto ' + TipoBol + ' 0 ',
    '']);
end;

procedure TUnBloqCond.ReopenQrSumCP(Query: TmySQLQuery; TabelaCnsA: String;
  CliInt, Periodo, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT SUM(Consumo) CONSUMO, SUM(Valor) VALOR ',
    'FROM ' + TabelaCnsA + ' cni ',
    'WHERE cni.Cond=' + Geral.FF0(CliInt),
    'AND cni.Codigo=' + Geral.FF0(Codigo),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    '']);
end;

function TUnBloqCond.CalculaTotalPRI(TabelaPriA, TabelaPrvA: String; Periodo,
  Codigo, Controle: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result        := False;
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(Valor) Total ',
      'FROM ' + TabelaPriA,
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabelaPrvA, False,
      ['Gastos'], ['Codigo'], [Qry.FieldByName('Total').AsFloat], [Codigo], True) then
    begin
      Result := True;
    end;
  finally
    Qry.Free;
    Screen.Cursor := crDefault;
  end;
end;

function TUnBloqCond.DesfazerBoletos(Selecao: TSelType; QueryUpd, QueryAux,
  QueryBoletos, QueryBoletosIts: TmySQLQuery; Database: TmySQLDatabase;
  Grade: TDBGrid; TabLctA, TabPrvA, TabAriA, TabCnsA: String; Cond,
  Periodo: Integer; Progress: TProgressBar): Boolean;

  function DesfazerBoletoAtual(QueryBoletos, QueryBoletosIts, QueryAux,
    QueryUpd: TmySQLQuery; Database: TmySQLDatabase; TabLctA, TabAriA,
    TabCnsA: String): Boolean;
  var
    Data: TDateTime;
    TabelaOrig: String;
    MotivDel, Tipo, Carteira, Lancto, Sub, Controle: Integer;
  begin
    Result := False;
    //
    if UBloqGerl.VerificaSeLoteCRFoiGerado(Dmod.MyDB,
      QueryBoletos.FieldByName('ProtocoPakCR').AsInteger) then
    begin
      Geral.MB_Aviso('A��o abortada!' + sLineBreak +
        'Motivo: Foi gerado um arquivo remessa para este boleto!');
      Exit;
    end;
    if not VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts,QueryAux, Database, TabLctA) then
    begin
      Geral.MB_Aviso('A��o abortada!' + sLineBreak +
        'Motivo: N�o foi poss�vel localizar todos os lan�amentos financeiros referentes ao boleto!');
      Exit;
    end;
    if True then

    if VerificaSeAlgumItemDoBoletoEstaPago(QueryBoletosIts, QueryAux, Database, TabLctA) then
    begin
      Geral.MB_Aviso('A��o abortada!' + sLineBreak +
        'Motivo: O boleto j� possu� pagamentos lan�ados nele!');
      Exit;
    end;
    try
      QueryBoletosIts.First;
      //
      while not QueryBoletosIts.Eof do
      begin
        TabelaOrig := QueryBoletosIts.FieldByName('TabelaOrig').AsString;
        Controle   := QueryBoletosIts.FieldByName('Controle').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
          'SELECT Data, Tipo, Carteira, Controle, Sub, FatID',
          'FROM ' + TabLctA,
          'WHERE Controle=' + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger),
          '']);
        //Caso exista mais exista um lan�amento
        if QueryAux.RecordCount > 0 then
        begin
          QueryAux.First;
          while not QueryAux.Eof do
          begin
            if LowerCase(TabelaOrig) = LowerCase(TabAriA) then
              MotivDel := CO_MOTVDEL_306_EXCLUILCTARRECADACAO
            else
              MotivDel := CO_MOTVDEL_307_EXCLUILCTLEITURA;
            //
            Data     := QueryAux.FieldByName('Data').AsDateTime;
            Tipo     := QueryAux.FieldByName('Tipo').AsInteger;
            Carteira := QueryAux.FieldByName('Carteira').AsInteger;
            Lancto   := QueryAux.FieldByName('Controle').AsInteger;
            Sub      := QueryAux.FieldByName('Sub').AsInteger;
            //
            try
              //Lan�amento financeiro
              UFinanceiro.ExcluiLct_Unico(TabLctA, Database, Data, Tipo, Carteira,
                Lancto, Sub, MotivDel, False);
            except
              ;
            end;
            //
            QueryAux.Next;
          end;
          try
            {$IfNDef SemProtocolo}
            UnProtocolo.ExcluiLoteProtocoloBoleto(Database, Controle, TabelaOrig);
            {$EndIf}
            if LowerCase(TabelaOrig) = LowerCase(TabAriA) then
            begin
              //Arrecada��es
              Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabAriA, False,
                          ['Boleto', 'Lancto'], ['Controle'], [0, 0], [Controle], True);
            end else
            begin
              //Consumos
              Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabCnsA, False,
                          ['Boleto', 'Lancto'], ['Controle'], [0, 0], [Controle], True);
            end;
          except
            ;
          end;
          //
          QueryBoletosIts.Next;
        end;
      end;
    finally
      ;
    end;
  end;

var
  I: Integer;
  Resposta: Integer;
begin
  if (QueryBoletos.RecordCount > 0) then
  begin
    if not VerificaSeHouveEmissaoPosterior(QueryAux, Database, TabAriA, TabPrvA,
      TabCnsA, Cond, Periodo) then
    begin
      try
        QueryBoletos.DisableControls;
        QueryBoletosIts.DisableControls;
        //
        Grade.Enabled     := False;
        Progress.Position := 0;
        //
        case Selecao of
          istAtual:
            Resposta := Geral.MB_Pergunta('Tem certeza que deseja desfazer a gera��o do boleto atual?' +
                          sLineBreak + 'ATEN��O: Todos os lan�amentos financeiros atrelados ao boleto ser�o exclu�dos!');
          istSelecionados:
            Resposta := Geral.MB_Pergunta('Tem certeza que deseja desfazer a gera��o dos boletos selecionados?' +
                          sLineBreak + 'ATEN��O: Todos os lan�amentos financeiros atrelados ao boleto ser�o exclu�dos!');
          istTodos:
            Resposta := Geral.MB_Pergunta('Tem certeza que deseja desfazer a gera��o de todos boletos?' +
                          sLineBreak + 'ATEN��O: Todos os lan�amentos financeiros atrelados ao boleto ser�o exclu�dos!');
          else
          begin
            Geral.MB_Aviso('Tipo de sele��o n�o implementada!');
            Resposta := ID_NO;
          end;
        end;
        if Resposta = ID_YES then
        begin
          case Selecao of
            istAtual:
            begin
              Result := DesfazerBoletoAtual(QueryBoletos, QueryBoletosIts,
                        QueryAux, QueryUpd, Database, TabLctA, TabAriA, TabCnsA);
            end;
            istSelecionados:
            begin
              if Grade.SelectedRows.Count > 0 then
              begin
                Progress.Max := Grade.SelectedRows.Count;
                //
                with Grade.DataSource.DataSet do
                begin
                  for I := 0 to Grade.SelectedRows.Count - 1 do
                  begin
                    GotoBookmark(pointer(Grade.SelectedRows.Items[I]));
                    //
                    Result := DesfazerBoletoAtual(QueryBoletos, QueryBoletosIts,
                              QueryAux, QueryUpd, Database, TabLctA, TabAriA, TabCnsA);
                    //
                    Progress.Position := Progress.Position + 1;
                    Progress.Update;
                    Application.ProcessMessages;
                  end;
                end;
              end else
                Result := DesfazerBoletoAtual(QueryBoletos, QueryBoletosIts,
                          QueryAux, QueryUpd, Database, TabLctA, TabAriA, TabCnsA);
            end;
            istTodos:
            begin
              Progress.Max := QueryBoletos.RecordCount;
              //
              QueryBoletos.First;
              //
              while not QueryBoletos.Eof do
              begin
                Result := DesfazerBoletoAtual(QueryBoletos, QueryBoletosIts,
                          QueryAux, QueryUpd, Database, TabLctA, TabAriA, TabCnsA);
                //
                Progress.Position := Progress.Position + 1;
                Progress.Update;
                Application.ProcessMessages;
                //
                QueryBoletos.Next;
              end;
            end;
          end;
        end else
          Result := False;
      finally
        QueryBoletos.EnableControls;
        QueryBoletosIts.EnableControls;
        //
        Progress.Position := 0;
        Grade.Enabled     := True;
      end;
    end else
    begin
      Geral.MB_Aviso('A��o cancelada! J� houve emiss�o posterior!');
      Result := False;
    end;
    if (Result = False) and (Resposta = ID_YES) then
      Geral.MB_Aviso('Falha ao desfazer boletos!');
  end else
    Result := True;
end;

procedure TUnBloqCond.ReopenArreFut(QueryArreFutI: TmySQLQuery; CliInt, MesesAnt,
  Controle: Integer);
var
  Loc, Peri: Integer;
begin
  Peri := Geral.Periodo2000(DModG.ObtemAgora());
  Loc  := Controle;
  //
  if Controle < 1 then
    if QueryArreFutI.State = dsBrowse then
      Loc := QueryArreFutI.FieldByName('Controle').AsInteger;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryArreFutI, Dmod.MyDB, [
    'SELECT IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPROP, ',
    'imv.Unidade UNIDADE, con.Nome NOMECONTA, arf.* ',
    'FROM arrefut arf ',
    'LEFT JOIN entidades prp ON prp.Codigo=arf.Propriet ',
    'LEFT JOIN condimov  imv ON imv.Conta=arf.Apto ',
    'LEFT JOIN contas    con ON con.Codigo=arf.Conta ',
    'WHERE arf.' + TAB_ARI + '=0 ',
    'AND arf.Cond=' + Geral.FF0(CliInt),
    'AND arf.Periodo>=' + Geral.FF0(Peri - MesesAnt),
    'ORDER BY arf.Periodo DESC ',
    '']);
  //
  if Loc > 0 then
    QueryArreFutI.Locate('Controle', Loc, []);
end;

procedure TUnBloqCond.ExcluiLeituraAtual(Controle: Integer; Lancto, Boleto: Double;
  TabCnsA, TabLctA: String);
var
  QueryUpd, QrLocLct: TmySQLQuery;
begin
  QueryUpd := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QrLocLct := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Dmod.MyDB, [
      DELETE_FROM  + TabCnsA,
      'WHERE Controle=' + Geral.FF0(Controle) + ' AND Boleto=0',
      '']);
    if (Lancto <> 0) and (Boleto = 0) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLocLct, Dmod.MyDB, [
        'SELECT Data, Tipo, Carteira, Sub ',
        'FROM ' + TabCnsA,
        'WHERE Controle=' + Geral.FFI(Lancto),
        'AND FatID=' + Geral.FF0(VAR_FATID_0601),
        '']);
      if QrLocLct.RecordCount > 0 then
      begin
        UFinanceiro.ExcluiLct_Unico(TabLctA, Dmod.MyDB,
          QrLocLct.FieldByName('Data').AsDateTime,
          QrLocLct.FieldByName('Tipo').AsInteger,
          QrLocLct.FieldByName('Carteira').AsInteger,
          Lancto, QrLocLct.FieldByName('Sub').AsInteger,
          CO_MOTVDEL_307_EXCLUILCTLEITURA, False, False);
      end;
    end;
  finally
    QueryUpd.Free;
    QrLocLct.Free;
  end;
end;

end.
