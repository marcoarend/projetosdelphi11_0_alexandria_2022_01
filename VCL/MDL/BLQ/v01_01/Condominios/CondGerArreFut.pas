unit CondGerArreFut;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, DBCtrls, ComCtrls, Variants,
  dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, UnDmkEnums,
  DmkDAC_PF;

type
  TFmCondGerArreFut = class(TForm)
    Panel1: TPanel;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    DsContas: TDataSource;
    Label2: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    LaDeb: TLabel;
    EdValor: TdmkEdit;
    CkContinuar: TCheckBox;
    Label1: TLabel;
    EdApto: TdmkEditCB;
    CBApto: TdmkDBLookupComboBox;
    QrAptos: TmySQLQuery;
    DsAptos: TDataSource;
    QrAptosUnidade: TWideStringField;
    QrAptosConta: TIntegerField;
    QrAptosPropriet: TIntegerField;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    LaAno: TLabel;
    RichEdit1: TRichEdit;
    Label3: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    EdCNAB_Cfg: TdmkEditCB;
    CBCNAB_Cfg: TdmkDBLookupComboBox;
    Label4: TLabel;
    QrCNAB_Cfg: TmySQLQuery;
    DsCNAB_Cfg: TDataSource;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    QrAptosCodigo: TIntegerField;
    QrAptosCliente: TIntegerField;
    QrLoc: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdAptoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCond, FArreFutControle: Integer;
  end;

  var
  FmCondGerArreFut: TFmCondGerArreFut;

implementation

uses Module, UnInternalConsts, UMySQLModule, UnMSgInt, UnMyObjects,
  UnDmkProcFunc, UnBloqCond;

{$R *.DFM}

procedure TFmCondGerArreFut.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerArreFut.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerArreFut.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerArreFut.EdAptoChange(Sender: TObject);
var
  Apto: Integer;
begin
  Apto := EdApto.ValueVariant;
  //
  if Apto <> 0 then
  begin
    UBloqCond.ReopenCNAB_CfgSel(QrCNAB_Cfg, QrAptosCliente.Value);
  end;
end;

procedure TFmCondGerArreFut.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Periodo: Integer;
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if QrContasMensal.Value = 'V' then
    begin
      Periodo := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
      Mes     := dmkPF.MesEAnoDoPeriodoLongo(Periodo);
    end;
    if key=VK_F4 then EdDescricao.Text := QrContasNome.Value  + ' ' + Mes;
    if key=VK_F5 then EdDescricao.Text := QrContasNome2.Value + ' ' + Mes;
    if key=VK_F6 then EdDescricao.Text := QrContasNome3.Value + ' ' + Mes;
    //
    EdDescricao.SetFocus;
    EdDescricao.SelStart  := Length(EdDescricao.Text);
    EdDescricao.SelLength := 0;
  end;
end;

procedure TFmCondGerArreFut.BtOKClick(Sender: TObject);
var
  Conta, Controle, Apto, Periodo, CNAB_Cfg: Integer;
  Valor: Double;
  Texto, Msg: String;
begin
  Periodo  := dmkPF.PeriodoEncode(Geral.IMV(CBAno.Items[CBAno.ItemIndex]), CBMes.ItemIndex+1);
  Apto     := EdApto.ValueVariant;
  Conta    := EdConta.ValueVariant;
  Valor    := EdValor.ValueVariant;
  Texto    := Trim(EdDescricao.Text);
  CNAB_Cfg := EdCNAB_Cfg.ValueVariant;
  //
  if MyObjects.FIC(Apto = 0, EdApto, 'A unidade deve ser informada!') then Exit;
  if MyObjects.FIC(CNAB_Cfg = 0, EdCNAB_Cfg, 'A configuração do boleto deve ser informada!') then Exit;
  if MyObjects.FIC(Conta = 0, EdConta, 'A conta deve ser informada!') then Exit;
  if MyObjects.FIC(Valor < 0.01, EdValor, 'O valor deve ser informado!') then Exit;
  if MyObjects.FIC(Texto = '', EdDescricao, 'O texto deve ser informado!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    if ImgTipo.SQLType = stIns then
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'ArreFut', 'ArreFut', 'Controle')
    else
      Controle := FArreFutControle;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ArreFut', False,
      ['Conta', 'Texto', 'Valor', 'Apto', 'Propriet', 'Cond', 'Periodo', 'CNAB_Cfg'], ['Controle'],
      [Conta, Texto, Valor, Apto, QrAptosPropriet.Value, FCond, Periodo, CNAB_Cfg], [Controle], True) then
    begin
      if CkContinuar.Checked then
      begin
        ImgTipo.SQLType         := stIns;
        EdApto.ValueVariant     := 0;
        CBApto.KeyValue         := NULL;
        EdCNAB_Cfg.ValueVariant := 0;
        CBCNAB_Cfg.KeyValue     := Null;
        //
        Geral.MB_Aviso('Item incluído com sucesso!');
        //
        CBMes.SetFocus;
      end else
        Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGerArreFut.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, 0);
  //
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
end;

end.
