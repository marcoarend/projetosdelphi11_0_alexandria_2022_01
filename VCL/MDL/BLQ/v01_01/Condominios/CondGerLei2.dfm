object FmCondGerLei2: TFmCondGerLei2
  Left = 448
  Top = 169
  Caption = 'GER-CONDM-016 :: Rateio de Consumo'
  ClientHeight = 604
  ClientWidth = 768
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnProduto: TPanel
    Left = 0
    Top = 48
    Width = 768
    Height = 113
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object STCli: TStaticText
      Left = 0
      Top = 0
      Width = 768
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = '{}'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object PanelZ: TPanel
      Left = 0
      Top = 17
      Width = 768
      Height = 46
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 41
        Height = 13
        Caption = 'Per'#237'odo:'
      end
      object Label2: TLabel
        Left = 64
        Top = 4
        Width = 40
        Height = 13
        Caption = 'Produto:'
      end
      object Label5: TLabel
        Left = 355
        Top = 4
        Width = 67
        Height = 13
        Caption = 'Per'#237'odo base:'
      end
      object SpeedButton7: TSpeedButton
        Left = 328
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton7Click
      end
      object EdPeriodo: TEdit
        Left = 4
        Top = 20
        Width = 57
        Height = 21
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clInactiveCaption
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        Text = 'XXX/00'
      end
      object EdProduto: TdmkEditCB
        Left = 64
        Top = 20
        Width = 33
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdProdutoChange
        DBLookupComboBox = CBProduto
        IgnoraDBLookupComboBox = False
      end
      object CBProduto: TdmkDBLookupComboBox
        Left = 99
        Top = 20
        Width = 226
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCons
        TabOrder = 2
        dmkEditCB = EdProduto
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPerioBase: TdmkEdit
        Left = 355
        Top = 20
        Width = 69
        Height = 21
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 63
      Width = 768
      Height = 50
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object Label4: TLabel
        Left = 4
        Top = 4
        Width = 53
        Height = 13
        Caption = 'Valor base:'
      end
      object Label6: TLabel
        Left = 88
        Top = 4
        Width = 56
        Height = 13
        Caption = 'Fator (total):'
        FocusControl = DBEdit1
      end
      object Label7: TLabel
        Left = 180
        Top = 4
        Width = 64
        Height = 13
        Caption = 'Valor unit'#225'rio:'
      end
      object LaGastoTotal: TLabel
        Left = 252
        Top = 4
        Width = 9
        Height = 13
        Caption = '?:'
      end
      object LaGastoUnita: TLabel
        Left = 344
        Top = 4
        Width = 9
        Height = 13
        Caption = '?:'
      end
      object EdValorBase: TdmkEdit
        Left = 4
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdValorBaseChange
      end
      object DBEdit1: TDBEdit
        Left = 88
        Top = 20
        Width = 88
        Height = 21
        DataField = 'TOTAL'
        DataSource = DsFracao
        TabOrder = 1
      end
      object EdUnitario: TdmkEdit
        Left = 180
        Top = 20
        Width = 68
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdGastoTotal: TdmkEdit
        Left = 252
        Top = 20
        Width = 88
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdGastoUnita: TdmkEdit
        Left = 344
        Top = 20
        Width = 88
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnLeitura: TPanel
    Left = 0
    Top = 161
    Width = 768
    Height = 329
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 768
      Height = 329
      Align = alClient
      DataSource = DsConsLei
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = DBGrid1DrawColumnCell
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Apto_Un'
          ReadOnly = True
          Title.Caption = 'Apartamento'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdFator'
          Title.Caption = 'Fator'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedAnt'
          Title.Caption = 'Medida anterior'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MedAtu'
          Title.Caption = 'Medida atual'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GASTO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ReadOnly = True
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEDIA_VEZES'
          ReadOnly = True
          Title.Caption = 'M* Vezes'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEDIA_MENOR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ReadOnly = True
          Title.Caption = 'M* Menor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEDIA_MEDIA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ReadOnly = True
          Title.Caption = 'M* M'#233'dia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEDIA_MAIOR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ReadOnly = True
          Title.Caption = 'M* Maior'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 768
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 720
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 672
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 241
        Height = 32
        Caption = 'Rateio de Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 241
        Height = 32
        Caption = 'Rateio de Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 241
        Height = 32
        Caption = 'Rateio de Consumo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 490
    Width = 768
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 764
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 148
        Height = 16
        Caption = 'M*  :  '#218'ltimos consumos.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 148
        Height = 16
        Caption = 'M*  :  '#218'ltimos consumos.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 534
    Width = 768
    Height = 70
    Align = alBottom
    TabOrder = 4
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 764
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 620
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrCons: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cns.Codigo, cns.Nome, cnp.Casas, cnp.Preco, '
      'cnp.UnidFat, cnp.UnidLei, cnp.UnidImp, cnp.ContaBase, '
      'cnp.PerioBase, cnp.FatorBase, cnp.Arredonda, cnp.CasRat,'
      'cnp.NaoImpLei, cnp.CNAB_Cfg'
      'FROM consprc cnp'
      'LEFT JOIN cons cns ON cns.Codigo=cnp.Codigo'
      'WHERE cnp.Cond=:P0'
      'ORDER BY cns.Nome')
    Left = 12
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrConsCasas: TSmallintField
      FieldName = 'Casas'
    end
    object QrConsPreco: TFloatField
      FieldName = 'Preco'
      Required = True
      EditFormat = '#,###,##0.0000'
    end
    object QrConsUnidFat: TFloatField
      FieldName = 'UnidFat'
      Required = True
    end
    object QrConsUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Required = True
      Size = 5
    end
    object QrConsUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Required = True
      Size = 5
    end
    object QrConsContaBase: TIntegerField
      FieldName = 'ContaBase'
      Required = True
    end
    object QrConsFatorBase: TSmallintField
      FieldName = 'FatorBase'
      Required = True
    end
    object QrConsArredonda: TFloatField
      FieldName = 'Arredonda'
      Required = True
    end
    object QrConsCasRat: TSmallintField
      FieldName = 'CasRat'
      Required = True
    end
    object QrConsPerioBase: TSmallintField
      FieldName = 'PerioBase'
      Required = True
    end
    object QrConsNaoImpLei: TSmallintField
      FieldName = 'NaoImpLei'
      Required = True
    end
    object QrConsCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
  end
  object DsCons: TDataSource
    DataSet = QrCons
    Left = 40
    Top = 204
  end
  object QrAptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cdi.Conta, cdi.Unidade, cdi.Propriet,'
      'cdi.Moradores, cdi.FracaoIdeal, cdi.Andar,'
      'blq.Ordem'
      'FROM condimov cdi'
      'LEFT JOIN condbloco blq ON blq.Controle=cdi.Controle'
      'WHERE cdi.SitImv=1 '
      'AND cdi.Codigo=:P0'
      'ORDER BY blq.Ordem, cdi.Andar, cdi.Unidade')
    Left = 356
    Top = 209
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAptosConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'condimov.Conta'
      Required = True
    end
    object QrAptosMedAnt: TFloatField
      FieldKind = fkLookup
      FieldName = 'MedAnt'
      LookupDataSet = QrAnt
      LookupKeyFields = 'Apto'
      LookupResultField = 'MedAtu'
      KeyFields = 'Conta'
      Lookup = True
    end
    object QrAptosAptoJa: TIntegerField
      FieldKind = fkLookup
      FieldName = 'AptoJa'
      LookupDataSet = QrJaTem
      LookupKeyFields = 'Apto'
      LookupResultField = 'Apto'
      KeyFields = 'Conta'
      Lookup = True
    end
    object QrAptosUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'condimov.Unidade'
      Required = True
      Size = 10
    end
    object QrAptosPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'condimov.Propriet'
      Required = True
    end
    object QrAptosMoradores: TIntegerField
      FieldName = 'Moradores'
      Required = True
    end
    object QrAptosFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
      Required = True
    end
    object QrAptosAndar: TIntegerField
      FieldName = 'Andar'
      Required = True
    end
    object QrAptosOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object QrAnt: TmySQLQuery
    Database = Dmod.MyDB
    Left = 384
    Top = 209
    object QrAntApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrAntMedAtu: TFloatField
      FieldName = 'MedAtu'
      Required = True
    end
  end
  object TbConsLei: TmySQLTable
    Database = DModG.MyPID_DB
    BeforeOpen = TbConsLeiBeforeOpen
    BeforePost = TbConsLeiBeforePost
    OnCalcFields = TbConsLeiCalcFields
    SortFieldNames = 'Apto_Un'
    TableName = 'conslei'
    Left = 356
    Top = 237
    object TbConsLeiID_Rand: TWideStringField
      FieldName = 'ID_Rand'
      Origin = 'conslei.ID_Rand'
      Size = 15
    end
    object TbConsLeiApto_ID: TIntegerField
      FieldName = 'Apto_ID'
      Origin = 'conslei.Apto_ID'
    end
    object TbConsLeiApto_Un: TWideStringField
      FieldName = 'Apto_Un'
      Origin = 'conslei.Apto_Un'
      Size = 10
    end
    object TbConsLeiMedAnt: TFloatField
      FieldName = 'MedAnt'
      Origin = 'conslei.MedAnt'
    end
    object TbConsLeiMedAtu: TFloatField
      FieldName = 'MedAtu'
      Origin = 'conslei.MedAtu'
    end
    object TbConsLeiAdiciona: TSmallintField
      FieldName = 'Adiciona'
      Origin = 'conslei.Adiciona'
    end
    object TbConsLeiPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'conslei.Propriet'
    end
    object TbConsLeiGASTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GASTO'
      Calculated = True
    end
    object TbConsLeiVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object TbConsLeiMEDIA_VEZES: TFloatField
      FieldKind = fkLookup
      FieldName = 'MEDIA_VEZES'
      LookupDataSet = QrMedias
      LookupKeyFields = 'Apto'
      LookupResultField = 'Vezes'
      KeyFields = 'Apto_ID'
      Lookup = True
    end
    object TbConsLeiMEDIA_MENOR: TFloatField
      FieldKind = fkLookup
      FieldName = 'MEDIA_MENOR'
      LookupDataSet = QrMedias
      LookupKeyFields = 'Apto'
      LookupResultField = 'MENOR'
      KeyFields = 'Apto_ID'
      Lookup = True
    end
    object TbConsLeiMEDIA_MEDIA: TFloatField
      FieldKind = fkLookup
      FieldName = 'MEDIA_MEDIA'
      LookupDataSet = QrMedias
      LookupKeyFields = 'Apto'
      LookupResultField = 'MEDIA'
      KeyFields = 'Apto_ID'
      Lookup = True
    end
    object TbConsLeiMEDIA_MAIOR: TFloatField
      FieldKind = fkLookup
      FieldName = 'MEDIA_MAIOR'
      LookupDataSet = QrMedias
      LookupKeyFields = 'Apto'
      LookupResultField = 'MAIOR'
      KeyFields = 'Apto_ID'
      Lookup = True
    end
    object TbConsLeiQtdFator: TFloatField
      FieldName = 'QtdFator'
    end
    object TbConsLeiValUnita: TFloatField
      FieldName = 'ValUnita'
    end
  end
  object DsConsLei: TDataSource
    DataSet = TbConsLei
    Left = 384
    Top = 237
  end
  object QrPesq: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM conslei'
      'WHERE MedAtu <= MedAnt'
      'AND ID_Rand=:P0')
    Left = 356
    Top = 265
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrJaTem: TmySQLQuery
    Database = Dmod.MyDB
    Left = 328
    Top = 209
    object QrJaTemApto: TIntegerField
      FieldName = 'Apto'
    end
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    Left = 412
    Top = 209
  end
  object QrMedias: TmySQLQuery
    Database = Dmod.MyDB
    Left = 412
    Top = 237
    object QrMediasVezes: TLargeintField
      FieldName = 'Vezes'
      Required = True
    end
    object QrMediasMENOR: TFloatField
      FieldName = 'MENOR'
    end
    object QrMediasMEDIA: TFloatField
      FieldName = 'MEDIA'
    end
    object QrMediasMAIOR: TFloatField
      FieldName = 'MAIOR'
    end
    object QrMediasApto: TIntegerField
      FieldName = 'Apto'
    end
  end
  object QrUni: TmySQLQuery
    Database = Dmod.MyDB
    Left = 384
    Top = 181
    object QrUniApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrUniMedAtu: TFloatField
      FieldName = 'MedAtu'
      Required = True
    end
  end
  object QrFracao: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(FracaoIdeal) TOTAL'
      'FROM condimov'
      'WHERE Codigo=:P0'
      '')
    Left = 12
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFracaoTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.000000'
    end
  end
  object DsFracao: TDataSource
    DataSet = QrFracao
    Left = 40
    Top = 232
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 600
    Top = 317
  end
end
