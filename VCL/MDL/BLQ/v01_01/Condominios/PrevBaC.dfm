object FmPrevBaC: TFmPrevBaC
  Left = 368
  Top = 194
  Caption = 'CAD-PROVI-001 :: Cadastro de Provis'#245'es Bases'
  ClientHeight = 537
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCond: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 441
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PnCondData: TPanel
      Left = 1
      Top = 1
      Width = 820
      Height = 272
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 1
        Top = 1
        Width = 818
        Height = 270
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Fluxo de provis'#227'o da despesa'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label4: TLabel
            Left = 16
            Top = 8
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            FocusControl = EdDBCodigoFluxo
          end
          object Label5: TLabel
            Left = 120
            Top = 8
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
            FocusControl = EdDBNomeFluxo
          end
          object Label8: TLabel
            Left = 568
            Top = 88
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object Label6: TLabel
            Left = 16
            Top = 48
            Width = 60
            Height = 13
            Caption = 'Condom'#237'nio:'
          end
          object Label11: TLabel
            Left = 16
            Top = 88
            Width = 340
            Height = 13
            Caption = 
              'Texto a ser usado no lugar do texto base (deixe vazio para usar ' +
              'o base):'
          end
          object EdDBCodigoFluxo: TDBEdit
            Left = 16
            Top = 24
            Width = 100
            Height = 21
            Hint = 'N'#186' do banco'
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsPrevBaC
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8281908
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
          end
          object EdDBNomeFluxo: TDBEdit
            Left = 120
            Top = 24
            Width = 437
            Height = 21
            Hint = 'Nome do banco'
            TabStop = False
            Color = clWhite
            DataField = 'Nome'
            DataSource = DsPrevBaC
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 30
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
          end
          object RgSitCobr: TRadioGroup
            Left = 564
            Top = 0
            Width = 133
            Height = 85
            Caption = ' Fluxo da cobran'#231'a: '
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o cobrar'
              'Cont'#237'nua (mensal)'
              'Programada'
              'Por agendamento')
            TabOrder = 4
            OnClick = RgSitCobrClick
          end
          object EdValor: TdmkEdit
            Left = 568
            Top = 104
            Width = 129
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdValorExit
          end
          object GBPer: TGroupBox
            Left = 16
            Top = 128
            Width = 549
            Height = 89
            Caption = 
              ' Per'#237'odo de cobran'#231'a programada (m'#234's e ano) ou cont'#237'nua (s'#243' m'#234's)' +
              ': '
            TabOrder = 7
            Visible = False
            object GBIni: TGroupBox
              Left = 9
              Top = 17
              Width = 264
              Height = 64
              Caption = ' Per'#237'odo Inicial: '
              TabOrder = 0
              object Label32: TLabel
                Left = 4
                Top = 15
                Width = 23
                Height = 13
                Caption = 'M'#234's:'
              end
              object LaAnoI: TLabel
                Left = 180
                Top = 15
                Width = 22
                Height = 13
                Caption = 'Ano:'
              end
              object CBMesI: TComboBox
                Left = 5
                Top = 32
                Width = 172
                Height = 21
                Style = csDropDownList
                Color = clWhite
                DropDownCount = 12
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 7622183
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
              object CBAnoI: TComboBox
                Left = 179
                Top = 32
                Width = 78
                Height = 21
                Style = csDropDownList
                Color = clWhite
                DropDownCount = 3
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 7622183
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object GBFim: TGroupBox
              Left = 277
              Top = 17
              Width = 264
              Height = 64
              Caption = ' Per'#237'odo final: '
              TabOrder = 1
              object Label34: TLabel
                Left = 4
                Top = 15
                Width = 23
                Height = 13
                Caption = 'M'#234's:'
              end
              object LaAnoF: TLabel
                Left = 180
                Top = 15
                Width = 22
                Height = 13
                Caption = 'Ano:'
              end
              object CBMesF: TComboBox
                Left = 5
                Top = 32
                Width = 172
                Height = 21
                Style = csDropDownList
                Color = clWhite
                DropDownCount = 12
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 7622183
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
              object CBAnoF: TComboBox
                Left = 179
                Top = 32
                Width = 78
                Height = 21
                Style = csDropDownList
                Color = clWhite
                DropDownCount = 3
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 7622183
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
          end
          object EdEmpresa: TdmkEditCB
            Left = 16
            Top = 64
            Width = 41
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 60
            Top = 64
            Width = 497
            Height = 21
            Color = clWhite
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 3
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdTexto: TdmkEdit
            Left = 16
            Top = 104
            Width = 549
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CkInfoParc: TCheckBox
            Left = 16
            Top = 220
            Width = 245
            Height = 17
            Caption = 'Informar n'#250'mero da parcela / total de parcelas.'
            TabOrder = 9
          end
          object GbPro: TGroupBox
            Left = 568
            Top = 128
            Width = 129
            Height = 89
            Caption = ' Cobran'#231'a programada: '
            TabOrder = 8
            Visible = False
            object Label7: TLabel
              Left = 12
              Top = 40
              Width = 44
              Height = 13
              Caption = 'Parcelas:'
            end
            object EdParcelas: TdmkEdit
              Left = 64
              Top = 36
              Width = 41
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnExit = EdParcelasExit
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Fluxo do pagamento da despesa'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel1: TPanel
            Left = 560
            Top = 0
            Width = 250
            Height = 242
            Align = alRight
            BevelOuter = bvNone
            Caption = 'Panel1'
            TabOrder = 1
            object RadioGroup1: TRadioGroup
              Left = 0
              Top = 0
              Width = 250
              Height = 85
              Align = alTop
              Caption = ' Fluxo do pagamento: '
              ItemIndex = 0
              Items.Strings = (
                'N'#227'o considerar'
                'Cont'#237'nua (mensal)'
                'Programada')
              TabOrder = 0
              OnClick = RgSitCobrClick
            end
            object RadioGroup2: TRadioGroup
              Left = 0
              Top = 85
              Width = 250
              Height = 161
              Align = alTop
              Caption = ' Dia do m'#234's (Fluxo cont'#237'nuo): '
              Columns = 4
              ItemIndex = 0
              Items.Strings = (
                '01'
                '02'
                '03'
                '04'
                '05'
                '06'
                '07'
                '08'
                '09'
                '10'
                '11'
                '12'
                '13'
                '14'
                '15'
                '16'
                '17'
                '18'
                '19'
                '20'
                '21'
                '22'
                '23'
                '24'
                '25'
                '26'
                '27'
                '28')
              TabOrder = 1
            end
          end
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 560
            Height = 242
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel4'
            TabOrder = 0
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 560
              Height = 69
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label12: TLabel
                Left = 12
                Top = 4
                Width = 36
                Height = 13
                Caption = 'C'#243'digo:'
                FocusControl = EdCodigoFluxo
              end
              object Label13: TLabel
                Left = 116
                Top = 4
                Width = 51
                Height = 13
                Caption = 'Descri'#231#227'o:'
                FocusControl = EdNomeFluxo
              end
              object Label14: TLabel
                Left = 12
                Top = 52
                Width = 231
                Height = 13
                Caption = 'Pagamentos programados (apenas monitora'#231#227'o):'
              end
              object EdCodigoFluxo: TDBEdit
                Left = 10
                Top = 20
                Width = 100
                Height = 21
                Hint = 'N'#186' do banco'
                TabStop = False
                DataField = 'Codigo'
                DataSource = DsPrevBaC
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 8281908
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
              end
              object EdNomeFluxo: TDBEdit
                Left = 116
                Top = 20
                Width = 437
                Height = 21
                Hint = 'Nome do banco'
                TabStop = False
                Color = clWhite
                DataField = 'Nome'
                DataSource = DsPrevBaC
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                MaxLength = 30
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
              end
            end
            object DBGrid2: TDBGrid
              Left = 0
              Top = 69
              Width = 560
              Height = 173
              Align = alClient
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 371
      Width = 792
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel10: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel11: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn2: TBitBtn
            Tag = 15
            Left = 4
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn2Click
          end
        end
        object BtConfCond: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfCondClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 441
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 104
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 120
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object LaForneceI: TLabel
        Left = 16
        Top = 48
        Width = 80
        Height = 13
        Caption = 'Conta vinculada:'
      end
      object SpeedButton5: TSpeedButton
        Left = 756
        Top = 64
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNome: TEdit
        Left = 120
        Top = 24
        Width = 653
        Height = 21
        MaxLength = 255
        TabOrder = 1
      end
      object EdConta: TdmkEditCB
        Left = 16
        Top = 64
        Width = 41
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConta
        IgnoraDBLookupComboBox = False
      end
      object CBConta: TdmkDBLookupComboBox
        Left = 60
        Top = 64
        Width = 690
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 3
        dmkEditCB = EdConta
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 371
      Width = 792
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel9: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 4
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 441
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 96
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 120
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 16
        Top = 48
        Width = 80
        Height = 13
        Caption = 'Conta vinculada:'
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPrevBaC
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 120
        Top = 24
        Width = 653
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPrevBaC
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object EdDBConta: TDBEdit
        Left = 16
        Top = 64
        Width = 757
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMECONTA'
        DataSource = DsPrevBaC
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
    end
    object DBGrid1: TDBGrid
      Left = 8
      Top = 104
      Width = 644
      Height = 85
      DataSource = DsPrevBaI
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECOND'
          Title.Caption = 'Condom'#237'nio'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESITCOBR'
          Title.Caption = 'Situa'#231#227'o de cobran'#231'a'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Parcelas'
          Width = 46
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'INICIO'
          Title.Caption = 'In'#237'cio'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FINAL'
          Title.Caption = 'Final'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InfoParc_TXT'
          Title.Caption = 'InfoParc'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Descri'#231#227'o substituta'
          Visible = True
        end>
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 377
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 269
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel7: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtProvisao: TBitBtn
          Tag = 10004
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Provis'#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtProvisaoClick
        end
        object BtCondom: TBitBtn
          Tag = 10003
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Condom.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtCondomClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 359
        Height = 32
        Caption = 'Cadastro de Provis'#245'es Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 359
        Height = 32
        Caption = 'Cadastro de Provis'#245'es Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 359
        Height = 32
        Caption = 'Cadastro de Provis'#245'es Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso2: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso1: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsPrevBaC: TDataSource
    DataSet = QrPrevBaC
    Left = 256
    Top = 77
  end
  object QrPrevBaC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPrevBaCBeforeOpen
    AfterOpen = QrPrevBaCAfterOpen
    BeforeClose = QrPrevBaCBeforeClose
    AfterScroll = QrPrevBaCAfterScroll
    SQL.Strings = (
      'SELECT con.Nome NOMECONTA, pbc.* '
      'FROM prevbac pbc'
      'LEFT JOIN contas con ON con.Codigo=pbc.Conta'
      'WHERE pbc.Codigo > 0')
    Left = 228
    Top = 77
    object QrPrevBaCCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'prevbac.Codigo'
    end
    object QrPrevBaCConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'prevbac.Conta'
    end
    object QrPrevBaCLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'prevbac.Lk'
    end
    object QrPrevBaCDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'prevbac.DataCad'
    end
    object QrPrevBaCDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'prevbac.DataAlt'
    end
    object QrPrevBaCUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'prevbac.UserCad'
    end
    object QrPrevBaCUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'prevbac.UserAlt'
    end
    object QrPrevBaCNome: TWideStringField
      DisplayWidth = 40
      FieldName = 'Nome'
      Origin = 'prevbac.Nome'
      Size = 40
    end
    object QrPrevBaCNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Debito="V"'
      'ORDER BY Nome')
    Left = 377
    Top = 77
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 405
    Top = 77
  end
  object QrPrevBaI: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPrevBaICalcFields
    SQL.Strings = (
      'SELECT DISTINCT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial '
      'ELSE ent.Nome END NOMECOND, pbi.*'
      'FROM cond cnd'
      'LEFT JOIN prevbai pbi ON cnd.Codigo=pbi.Cond'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE pbi.Codigo=:P0')
    Left = 285
    Top = 77
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrevBaINOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
    object QrPrevBaICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrevBaIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPrevBaICond: TIntegerField
      FieldName = 'Cond'
    end
    object QrPrevBaIValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPrevBaITexto: TWideStringField
      DisplayWidth = 50
      FieldName = 'Texto'
      Size = 50
    end
    object QrPrevBaISitCobr: TIntegerField
      FieldName = 'SitCobr'
    end
    object QrPrevBaIParcelas: TIntegerField
      FieldName = 'Parcelas'
      DisplayFormat = '0;-0; '
    end
    object QrPrevBaIParcPerI: TIntegerField
      FieldName = 'ParcPerI'
    end
    object QrPrevBaIParcPerF: TIntegerField
      FieldName = 'ParcPerF'
    end
    object QrPrevBaILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPrevBaIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPrevBaIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPrevBaIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPrevBaIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPrevBaINOMESITCOBR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESITCOBR'
      Size = 50
      Calculated = True
    end
    object QrPrevBaIINICIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'INICIO'
      Size = 30
      Calculated = True
    end
    object QrPrevBaIFINAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Size = 30
      Calculated = True
    end
    object QrPrevBaIInfoParc: TIntegerField
      FieldName = 'InfoParc'
    end
    object QrPrevBaIInfoParc_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'InfoParc_TXT'
      Size = 3
      Calculated = True
    end
  end
  object DsPrevBaI: TDataSource
    DataSet = QrPrevBaI
    Left = 313
    Top = 77
  end
  object PMProvisao: TPopupMenu
    OnPopup = PMProvisaoPopup
    Left = 384
    Top = 272
    object Incluiproviso1: TMenuItem
      Caption = '&Inclui provis'#227'o'
      OnClick = Incluiproviso1Click
    end
    object Alteraproviso1: TMenuItem
      Caption = '&Altera provis'#227'o'
      OnClick = Alteraproviso1Click
    end
    object Excluiproviso1: TMenuItem
      Caption = '&Exclui provis'#227'o'
      Enabled = False
    end
  end
  object PMCondom: TPopupMenu
    OnPopup = PMCondomPopup
    Left = 412
    Top = 272
    object AdicionaCondomnioprovisoselecionada1: TMenuItem
      Caption = '&Adiciona condom'#237'nio '#224' provis'#227'o selecionada'
      OnClick = AdicionaCondomnioprovisoselecionada1Click
    end
    object Editaprovisoselecionada1: TMenuItem
      Caption = '&Edita provis'#227'o selecionada'
      OnClick = Editaprovisoselecionada1Click
    end
    object Retiraocondominiodaprovisoselecionada1: TMenuItem
      Caption = '&Retira o condominio da provis'#227'o selecionada'
      OnClick = Retiraocondominiodaprovisoselecionada1Click
    end
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = Timer1Timer
    Left = 608
    Top = 12
  end
  object QrDupl: TmySQLQuery
    Database = Dmod.MyDB
    Left = 345
    Top = 77
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Conta'
      'FROM prevbac'
      'WHERE Conta=:P0')
    Left = 189
    Top = 169
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqConta: TIntegerField
      FieldName = 'Conta'
    end
  end
end
