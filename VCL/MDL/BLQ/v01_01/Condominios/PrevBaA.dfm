object FmPrevBaA: TFmPrevBaA
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-008 :: Provis'#245'es Agendadas'
  ClientHeight = 506
  ClientWidth = 847
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 847
    Height = 344
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TdmkDBGrid
      Left = 0
      Top = 0
      Width = 847
      Height = 344
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Adiciona'
          Title.Caption = 'OK'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTA'
          Title.Caption = 'Descri'#231#227'o da conta'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrevBaC'
          Title.Caption = 'Provis'#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrevBaI'
          Title.Caption = 'ID '
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end>
      Color = clWindow
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Adiciona'
          Title.Caption = 'OK'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTA'
          Title.Caption = 'Descri'#231#227'o da conta'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrevBaC'
          Title.Caption = 'Provis'#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrevBaI'
          Title.Caption = 'ID '
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 847
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 799
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 751
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 262
        Height = 32
        Caption = 'Provis'#245'es Agendadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 262
        Height = 32
        Caption = 'Provis'#245'es Agendadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 262
        Height = 32
        Caption = 'Provis'#245'es Agendadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 392
    Width = 847
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 843
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 436
    Width = 847
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 843
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 428
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Total:'
      end
      object Label2: TLabel
        Left = 512
        Top = 4
        Width = 67
        Height = 13
        Caption = 'Selecionados:'
      end
      object Label3: TLabel
        Left = 596
        Top = 4
        Width = 30
        Height = 13
        Caption = 'Saldo:'
      end
      object PnSaiDesis: TPanel
        Left = 699
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 6
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 162
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 286
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNenhumClick
      end
      object DBEdit1: TDBEdit
        Left = 428
        Top = 20
        Width = 80
        Height = 21
        DataField = 'TOTAL'
        DataSource = DsSumTot
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 512
        Top = 20
        Width = 80
        Height = 21
        DataField = 'TOTAL'
        DataSource = DsSumSel
        TabOrder = 4
      end
      object DBEdit3: TDBEdit
        Left = 596
        Top = 20
        Width = 80
        Height = 21
        DataField = 'TOTAL'
        DataSource = DsSumSdo
        TabOrder = 5
      end
    end
  end
  object Query: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    AfterOpen = QueryAfterOpen
    AfterPost = QueryAfterPost
    RequestLive = True
    Left = 196
    Top = 156
    object QueryConta: TIntegerField
      FieldName = 'Conta'
    end
    object QueryNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 100
    end
    object QueryPrevBaI: TIntegerField
      FieldName = 'PrevBaI'
    end
    object QueryPrevBaC: TIntegerField
      FieldName = 'PrevBaC'
    end
    object QueryValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QueryTexto: TWideStringField
      FieldName = 'Texto'
      Size = 40
    end
    object QueryAdiciona: TSmallintField
      FieldName = 'Adiciona'
      MaxValue = 1
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 224
    Top = 156
  end
  object QrSumTot: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    SQL.Strings = (
      'SELECT SUM(Valor) TOTAL'
      'FROM prevbaa')
    Left = 252
    Top = 156
    object QrSumTotTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumTot: TDataSource
    DataSet = QrSumTot
    Left = 280
    Top = 156
  end
  object QrSumSel: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    SQL.Strings = (
      'SELECT SUM(Valor) TOTAL'
      'FROM prevbaa'
      'WHERE Adiciona=1')
    Left = 308
    Top = 156
    object FloatField1: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumSel: TDataSource
    DataSet = QrSumSel
    Left = 336
    Top = 156
  end
  object QrSumSdo: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    SQL.Strings = (
      'SELECT SUM(Valor) TOTAL'
      'FROM prevbaa'
      'WHERE Adiciona<>1')
    Left = 364
    Top = 156
    object FloatField3: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumSdo: TDataSource
    DataSet = QrSumSdo
    Left = 392
    Top = 156
  end
  object QrConta: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    Left = 420
    Top = 156
    object QrContaZeros: TIntegerField
      FieldName = 'Zeros'
    end
  end
end
