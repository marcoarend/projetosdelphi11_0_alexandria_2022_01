unit PrevBaI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, Mask, dmkDBEdit, dmkRadioGroup, dmkCheckBox, dmkGeral, dmkImage,
  UnDmkEnums;

type
  TFmPrevBaI = class(TForm)
    Panel1: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label6: TLabel;
    Label11: TLabel;
    EdControle: TdmkEdit;
    EdNome: TdmkEdit;
    RgSitCobr: TdmkRadioGroup;
    EdValor: TdmkEdit;
    GBPer: TGroupBox;
    GBIni: TGroupBox;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBMesI: TComboBox;
    CBAnoI: TComboBox;
    GBFim: TGroupBox;
    Label34: TLabel;
    LaAnoF: TLabel;
    CBMesF: TComboBox;
    CBAnoF: TComboBox;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdTexto: TdmkEdit;
    CkInfoParc: TdmkCheckBox;
    GbPro: TGroupBox;
    Label7: TLabel;
    EdParcelas: TdmkEdit;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    EdParcPerI: TdmkEdit;
    EdParcPerF: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RgSitCobrClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdParcPerIChange(Sender: TObject);
    procedure CBMesIChange(Sender: TObject);
    procedure CBAnoIChange(Sender: TObject);
    procedure CBMesFChange(Sender: TObject);
    procedure CBAnoFChange(Sender: TObject);
    procedure EdParcPerFChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdParcPerIKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdParcPerFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CalculaParcelas();
  end;

  var
  FmPrevBaI: TFmPrevBaI;

implementation

uses UnMyObjects, Module, PrevVeri, UMySQLModule, ModuleGeral;

{$R *.DFM}

procedure TFmPrevBaI.BtOKClick(Sender: TObject);
var
  SitCobr, Ano, PeriodoI, PeriodoF, PeriodoA, Parcelas, Controle, Cond, pp: Integer;
  Valor: Double;
begin
  //
  SitCobr := RgSitCobr.ItemIndex;
  Cond := Geral.IMV(EdEmpresa.Text);
  if Cond = 0 then
  begin
    Geral.MensagemBox('Informe o condom�nio!', 'Erro', MB_OK+MB_ICONERROR);
    EdEmpresa.SetFocus;
    Exit;
  end;
  Valor := Geral.DMV(EdValor.Text);
  if (Valor < 0.01) and (Valor > -0.01) then
  begin
    if Geral.MensagemBox('Nenhum valor foi definido! Deseja continuar assim mesmo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      EdValor.SetFocus;
      Exit;
    end;
  end;
  PeriodoA := Geral.Periodo2000(Date);
  Parcelas := Geral.IMV(EdParcelas.Text);
  //
  PeriodoI := CBMesI.ItemIndex + 1;
  if SitCobr = 2 then
  begin
    Ano := Geral.IMV(CBAnoI.Text);
    PeriodoI := ((Ano - 2000) * 12) + PeriodoI;
  end;
  //
  PeriodoF := CBMesF.ItemIndex + 1;
  if SitCobr = 2 then
  begin
    Ano := Geral.IMV(CBAnoF.Text);
    PeriodoF := ((Ano - 2000) * 12) + PeriodoF;
    //
    if (PeriodoF < PeriodoA) then
    if Geral.MensagemBox('Per�odo j� expirado! Deseja continuar assim mesmo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
    //
    pp := PeriodoF - PeriodoI + 1;
    if Parcelas <> pp then
    begin
      Geral.MensagemBox('N�mero de parcelas n�o confere com per�odo! '+
      'Confirma��o abortada!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //
  end;
  Controle := EdControle.ValueVariant;
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, FmPrevVeri.FPrevVeri, Controle,
  DModG.QrUpdPID1) then
  begin
    FmPrevVeri.ReopenPrevVeri();
    Close;
  end;
end;

procedure TFmPrevBaI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrevBaI.CalculaParcelas();
var
  I, F, T: Integer;
begin
  I := EdParcPerI.ValueVariant;
  F := EdParcPerF.ValueVariant;
  T := F - I + 1;
  if (T > 0) and (F > 0)then
    EdParcelas.ValueVariant := T;
end;

procedure TFmPrevBaI.CBAnoFChange(Sender: TObject);
begin
  EdParcPerF.ValueVariant :=
    MyObjects.SetaPeriodoDeCBANoMes(CBAnoF, CBMesF);
end;

procedure TFmPrevBaI.CBAnoIChange(Sender: TObject);
begin
  EdParcPerI.ValueVariant :=
    MyObjects.SetaPeriodoDeCBANoMes(CBAnoI, CBMesI);
end;

procedure TFmPrevBaI.CBMesFChange(Sender: TObject);
begin
  EdParcPerF.ValueVariant :=
    MyObjects.SetaPeriodoDeCBANoMes(CBAnoF, CBMesF);
end;

procedure TFmPrevBaI.CBMesIChange(Sender: TObject);
begin
  EdParcPerI.ValueVariant :=
    MyObjects.SetaPeriodoDeCBANoMes(CBAnoI, CBMesI);
end;

procedure TFmPrevBaI.EdParcPerFChange(Sender: TObject);
begin
  if EdParcPerF.Focused then
    MyObjects.CBAnoMesFromPeriodo(EdParcPerF.ValueVariant, CBAnoF, CBMesF, False);
  CalculaParcelas();
end;

procedure TFmPrevBaI.EdParcPerFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdParcPerF.ValueVariant := Geral.Periodo2000(Date);
end;

procedure TFmPrevBaI.EdParcPerIChange(Sender: TObject);
begin
  if EdParcPerI.Focused then
    MyObjects.CBAnoMesFromPeriodo(EdParcPerI.ValueVariant, CBAnoI, CBMesI, False);
  CalculaParcelas();
end;

procedure TFmPrevBaI.EdParcPerIKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdParcPerI.ValueVariant := Geral.Periodo2000(Date);
end;

procedure TFmPrevBaI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPrevBaI.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  MyObjects.PreencheCBAnoECBMes(CBAnoI, CBMesI, 0, False);
  MyObjects.PreencheCBAnoECBMes(CBAnoF, CBMesF, 0, False);
  //
  MyObjects.CBAnoMesFromPeriodo(EdParcPerI.ValueVariant, CBAnoI, CBMesI, False);
  MyObjects.CBAnoMesFromPeriodo(EdParcPerF.ValueVariant, CBAnoF, CBMesF, False);
end;

procedure TFmPrevBaI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrevBaI.RgSitCobrClick(Sender: TObject);
begin
  EdParcPerI.ValMax := '';
  EdParcPerF.ValMax := '';
  case RgSitCobr.ItemIndex of
    0: // N�o cobrar
    begin
      GbPer.Visible      := False;
      GbPro.Visible      := False;
      CkInfoParc.Visible := False;
    end;
    1: // Cont�nua
    begin
      GbPer.Visible      := True;
      GbPro.Visible      := False;
      LaAnoI.Visible     := False;
      CBAnoI.Visible     := False;
      LaAnoF.Visible     := False;
      CBAnoF.Visible     := False;
      CkInfoParc.Visible := True;
      EdParcPerI.ValMax  := '12';
      EdParcPerF.ValMax  := '12';
      MyObjects.CBAnoMesFromPeriodo(EdParcPerI.ValueVariant, CBAnoI, CBMesI, False);
      MyObjects.CBAnoMesFromPeriodo(EdParcPerF.ValueVariant, CBAnoF, CBMesF, False);
    end;
    2: // Programada
    begin
      GbPer.Visible      := True;
      GbPro.Visible      := True;
      LaAnoI.Visible     := True;
      CBAnoI.Visible     := True;
      LaAnoF.Visible     := True;
      CBAnoF.Visible     := True;
      CkInfoParc.Visible := True;
    end;
    3: // Por agendamento
    begin
      GbPer.Visible      := False;
      GbPro.Visible      := False;
      CkInfoParc.Visible := False;
    end;
  end;
end;

end.
