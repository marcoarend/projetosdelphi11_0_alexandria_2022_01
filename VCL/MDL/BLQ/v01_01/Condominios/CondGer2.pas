unit CondGer2;

interface

uses
  Vcl.ComCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkCompoStore, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGrid, dmkDBGridZTO,
  frxClass, frxDBSet, UnBloqGerl;

type
  TFmCondGer2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrPrev: TmySQLQuery;
    QrPrevCodigo: TIntegerField;
    QrPrevCond: TIntegerField;
    QrPrevPeriodo: TIntegerField;
    QrPrevGastos: TFloatField;
    QrPrevLk: TIntegerField;
    QrPrevDataCad: TDateField;
    QrPrevDataAlt: TDateField;
    QrPrevUserCad: TIntegerField;
    QrPrevUserAlt: TIntegerField;
    QrPrevPERIODO_TXT: TWideStringField;
    QrPrevCondCli: TIntegerField;
    QrPrevTOT_BOL: TFloatField;
    QrPrevTOT_PRE: TFloatField;
    QrPrevAviso01: TWideStringField;
    QrPrevAviso02: TWideStringField;
    QrPrevAviso03: TWideStringField;
    QrPrevAviso04: TWideStringField;
    QrPrevAviso05: TWideStringField;
    QrPrevAviso06: TWideStringField;
    QrPrevAviso07: TWideStringField;
    QrPrevAviso08: TWideStringField;
    QrPrevAviso09: TWideStringField;
    QrPrevAviso10: TWideStringField;
    QrPrevCondCod: TIntegerField;
    QrPrevAvisoVerso: TWideStringField;
    QrPrevAviso11: TWideStringField;
    QrPrevAviso12: TWideStringField;
    QrPrevAviso13: TWideStringField;
    QrPrevAviso14: TWideStringField;
    QrPrevAviso15: TWideStringField;
    QrPrevAviso16: TWideStringField;
    QrPrevAviso17: TWideStringField;
    QrPrevAviso18: TWideStringField;
    QrPrevAviso19: TWideStringField;
    QrPrevAviso20: TWideStringField;
    QrPrevConfigBol: TIntegerField;
    QrPrevModelBloq: TSmallintField;
    QrPrevNOMECONFIGBOL: TWideStringField;
    QrPrevBalAgrMens: TSmallintField;
    QrPrevCompe: TSmallintField;
    QrPrevEncerrado: TSmallintField;
    QrPrevNOME_ENCERRADO: TWideStringField;
    DsPrev: TDataSource;
    CSTabSheetChamou: TdmkCompoStore;
    PMOrcamento: TPopupMenu;
    Incluinovooramento1: TMenuItem;
    Alteraperiodoatual1: TMenuItem;
    N9: TMenuItem;
    TravadestravaPerodo1: TMenuItem;
    PMProvisao: TPopupMenu;
    Adicinaitensbase1: TMenuItem;
    Adicionaitensdeprovisoagendados1: TMenuItem;
    N1: TMenuItem;
    Novoitemdeoramento1: TMenuItem;
    Alteraitemselecionado1: TMenuItem;
    Excluiitemdeoramentoselecionado1: TMenuItem;
    Devolveparaagendados1: TMenuItem;
    ItemAtual2: TMenuItem;
    ItensSelecionados2: TMenuItem;
    odosItens1: TMenuItem;
    Excluipermanentemente1: TMenuItem;
    ItemSelecionado3: TMenuItem;
    ItensSelecionados1: TMenuItem;
    Todositens1: TMenuItem;
    N20: TMenuItem;
    Agendamentoporvarreduradelanamentos1: TMenuItem;
    PMLeitura: TPopupMenu;
    Incluileituras1: TMenuItem;
    Rateiraconsumo1: TMenuItem;
    N21: TMenuItem;
    Alteraleituraatual1: TMenuItem;
    Excluileituraatual1: TMenuItem;
    Excluileituraatual2: TMenuItem;
    Excluileiturasselecionadas1: TMenuItem;
    Excluitodasleituras1: TMenuItem;
    PMArrecada: TPopupMenu;
    Incluiintensbasedearrecadao1: TMenuItem;
    N4: TMenuItem;
    Novoitemdearrecadao1: TMenuItem;
    nicoimvel1: TMenuItem;
    Mltiplosimveis1: TMenuItem;
    Alteraarrecadao1: TMenuItem;
    Itemselecionado1: TMenuItem;
    N5: TMenuItem;
    Exclusodearrecadao1: TMenuItem;
    Itematual3: TMenuItem;
    TODOSitensdearrecadao1: TMenuItem;
    N14: TMenuItem;
    Incluiitenspragendados1: TMenuItem;
    PMArreFut: TPopupMenu;
    Incluiarrecadaofutura1: TMenuItem;
    Alteraarrecadaofutura1: TMenuItem;
    Excluiarrecadaofutura1: TMenuItem;
    PMPrebol0: TPopupMenu;
    Bloqueto1: TMenuItem;
    Gerartodosabertos1: TMenuItem;
    GerarAtual1: TMenuItem;
    Gerarselecionados1: TMenuItem;
    N24: TMenuItem;
    AlteraVencimento2: TMenuItem;
    DobloquetoAtual2: TMenuItem;
    DosbloquetosSelecionados1: TMenuItem;
    DeTodosbloquetos2: TMenuItem;
    N25: TMenuItem;
    Excluir2: TMenuItem;
    ItemAtualdoprbloqueto1: TMenuItem;
    OsitensSelecionadosdoprbloquetoselecionado1: TMenuItem;
    Todositensdoprbloquetoatual1: TMenuItem;
    PMBoleto: TPopupMenu;
    Desfazerboletos1: TMenuItem;
    Atual3: TMenuItem;
    Selecionados3: TMenuItem;
    Todos3: TMenuItem;
    N3: TMenuItem;
    Alteravencimento1: TMenuItem;
    DoBloquetoatual1: TMenuItem;
    BloquetosSelecionados1: TMenuItem;
    DeTodosBloquetos1: TMenuItem;
    QrPRI: TmySQLQuery;
    QrPRIConta: TIntegerField;
    QrPRITexto: TWideStringField;
    QrPRIValor: TFloatField;
    QrPRISubGrupo: TIntegerField;
    QrPRINOMECONTA: TWideStringField;
    QrPRINOMESUBGRUPO: TWideStringField;
    QrPRICodigo: TIntegerField;
    QrPRIControle: TIntegerField;
    QrPRILk: TIntegerField;
    QrPRIDataCad: TDateField;
    QrPRIDataAlt: TDateField;
    QrPRIUserCad: TIntegerField;
    QrPRIUserAlt: TIntegerField;
    QrPRIPrevBaI: TIntegerField;
    QrPRICODCONTA: TIntegerField;
    QrPRICODSUBGRUPO: TIntegerField;
    DsPRI: TDataSource;
    QrCons: TmySQLQuery;
    QrConsCodigo: TIntegerField;
    QrConsNome: TWideStringField;
    QrConsPreco: TFloatField;
    QrConsCasas: TSmallintField;
    QrConsUnidLei: TWideStringField;
    QrConsUnidImp: TWideStringField;
    QrConsUnidFat: TFloatField;
    QrConsTOT_UNID1: TFloatField;
    QrConsTOT_UNID2: TFloatField;
    QrConsTOT_VALOR: TFloatField;
    QrConsTOT_UNID1_TXT: TWideStringField;
    QrConsTOT_UNID2_TXT: TWideStringField;
    DsCons: TDataSource;
    QrCNS: TmySQLQuery;
    QrCNSUnidade: TWideStringField;
    QrCNSCodigo: TIntegerField;
    QrCNSControle: TIntegerField;
    QrCNSApto: TIntegerField;
    QrCNSMedAnt: TFloatField;
    QrCNSMedAtu: TFloatField;
    QrCNSLk: TIntegerField;
    QrCNSDataCad: TDateField;
    QrCNSDataAlt: TDateField;
    QrCNSUserCad: TIntegerField;
    QrCNSUserAlt: TIntegerField;
    QrCNSCond: TIntegerField;
    QrCNSPeriodo: TIntegerField;
    QrCNSConsumo: TFloatField;
    QrCNSValor: TFloatField;
    QrCNSPreco: TFloatField;
    QrCNSCasas: TSmallintField;
    QrCNSUnidLei: TWideStringField;
    QrCNSUnidImp: TWideStringField;
    QrCNSUnidFat: TFloatField;
    QrCNSCONSUMO1_TXT: TWideStringField;
    QrCNSCONSUMO2_TXT: TWideStringField;
    QrCNSPropriet: TIntegerField;
    QrCNSCONSUMO2: TFloatField;
    QrCNSBoleto: TFloatField;
    QrCNSGeraTyp: TSmallintField;
    QrCNSGeraFat: TFloatField;
    QrCNSCasRat: TSmallintField;
    QrCNSDifCaren: TSmallintField;
    QrCNSCarencia: TFloatField;
    QrCNSLancto: TIntegerField;
    QrCNSBloco: TWideStringField;
    QrCNSCNAB_Cfg: TIntegerField;
    DsCNS: TDataSource;
    QrArre: TmySQLQuery;
    QrArreApto: TIntegerField;
    QrArreValor: TFloatField;
    QrArreUnidade: TWideStringField;
    QrArrePropriet: TIntegerField;
    QrArreNOMEPROPRIET: TWideStringField;
    DsArre: TDataSource;
    QrSumARRE: TmySQLQuery;
    QrSumARREValor: TFloatField;
    DsSumARRE: TDataSource;
    QrArreBol: TmySQLQuery;
    QrArreBolBoleto: TFloatField;
    DsArreBol: TDataSource;
    QrSumCT: TmySQLQuery;
    QrSumCTCONSUMO: TFloatField;
    QrSumCTVALOR: TFloatField;
    DsSumCT: TDataSource;
    QrAri: TmySQLQuery;
    QrAriCodigo: TIntegerField;
    QrAriControle: TIntegerField;
    QrAriConta: TIntegerField;
    QrAriValor: TFloatField;
    QrAriArreBaI: TIntegerField;
    QrAriTexto: TWideStringField;
    QrAriLk: TIntegerField;
    QrAriDataCad: TDateField;
    QrAriDataAlt: TDateField;
    QrAriUserCad: TIntegerField;
    QrAriUserAlt: TIntegerField;
    QrAriApto: TIntegerField;
    QrAriArreBaC: TIntegerField;
    QrAriVencto: TDateField;
    QrAriPropriet: TIntegerField;
    QrAriBoleto: TFloatField;
    QrAriLancto: TIntegerField;
    QrAriCNAB_Cfg: TIntegerField;
    DsAri: TDataSource;
    QrBoletos: TmySQLQuery;
    QrBoletosApto: TIntegerField;
    QrBoletosUnidade: TWideStringField;
    QrBoletosSUB_ARR: TFloatField;
    QrBoletosSUB_LEI: TFloatField;
    QrBoletosSUB_TOT: TFloatField;
    QrBoletosVencto: TDateField;
    QrBoletosPropriet: TIntegerField;
    QrBoletosNOMEPROPRIET: TWideStringField;
    QrBoletosBOLAPTO: TWideStringField;
    QrBoletosVENCTO_TXT: TWideStringField;
    QrBoletosPWD_WEB: TWideStringField;
    QrBoletosAndar: TIntegerField;
    QrBoletosBoleto: TFloatField;
    QrBoletosBLOQUETO: TFloatField;
    QrBoletosKGT: TLargeintField;
    QrBoletosOrdem: TIntegerField;
    QrBoletosFracaoIdeal: TFloatField;
    QrBoletosJuridico: TSmallintField;
    QrBoletosJuridico_TXT: TWideStringField;
    QrBoletosJuridico_DESCRI: TWideStringField;
    QrBoletosCNAB_Cfg: TIntegerField;
    DsBoletos: TDataSource;
    QrBoletosIts: TmySQLQuery;
    QrBoletosItsTEXTO: TWideStringField;
    QrBoletosItsVALOR: TFloatField;
    QrBoletosItsVencto: TDateField;
    QrBoletosItsTEXTO_IMP: TWideStringField;
    QrBoletosItsMedAnt: TFloatField;
    QrBoletosItsMedAtu: TFloatField;
    QrBoletosItsConsumo: TFloatField;
    QrBoletosItsCasas: TLargeintField;
    QrBoletosItsUnidLei: TWideStringField;
    QrBoletosItsUnidImp: TWideStringField;
    QrBoletosItsUnidFat: TFloatField;
    QrBoletosItsTipo: TLargeintField;
    QrBoletosItsVENCTO_TXT: TWideStringField;
    QrBoletosItsControle: TIntegerField;
    QrBoletosItsLancto: TIntegerField;
    QrBoletosItsGeraTyp: TLargeintField;
    QrBoletosItsGeraFat: TFloatField;
    QrBoletosItsCasRat: TLargeintField;
    QrBoletosItsNaoImpLei: TLargeintField;
    QrBoletosItsTabelaOrig: TWideStringField;
    QrBoletosItsGenero: TIntegerField;
    DsBoletosIts: TDataSource;
    QrPrevModBol: TmySQLQuery;
    QrPrevModBolUnidade: TWideStringField;
    QrPrevModBolNOME_CONFIGBOL: TWideStringField;
    QrPrevModBolApto: TIntegerField;
    QrPrevModBolConfigBol: TIntegerField;
    QrPrevModBolModelBloq: TSmallintField;
    QrPrevModBolBalAgrMens: TSmallintField;
    QrPrevModBolCompe: TSmallintField;
    QrPrevModBolNOME_MODELBLOQ: TWideStringField;
    QrPrevModBolCodigo: TIntegerField;
    DsPrevModBol: TDataSource;
    QrPrevNOMEMODELBLOQ: TWideStringField;
    QrSumBol: TmySQLQuery;
    QrSumBolVALOR: TFloatField;
    QrSumPre: TmySQLQuery;
    QrSumPreVALOR: TFloatField;
    QrSumCP: TmySQLQuery;
    QrSumCPCONSUMO: TFloatField;
    QrSumCPVALOR: TFloatField;
    QrArreFutI: TmySQLQuery;
    QrArreFutIControle: TIntegerField;
    QrArreFutIConta: TIntegerField;
    QrArreFutICond: TIntegerField;
    QrArreFutIPeriodo: TIntegerField;
    QrArreFutIApto: TIntegerField;
    QrArreFutIPropriet: TIntegerField;
    QrArreFutIValor: TFloatField;
    QrArreFutITexto: TWideStringField;
    QrArreFutILk: TIntegerField;
    QrArreFutIDataCad: TDateField;
    QrArreFutIDataAlt: TDateField;
    QrArreFutIUserCad: TIntegerField;
    QrArreFutIUserAlt: TIntegerField;
    QrArreFutINOMEPROP: TWideStringField;
    QrArreFutIUNIDADE: TWideStringField;
    QrArreFutINOMECONTA: TWideStringField;
    QrArreFutIPERIODO_TXT: TWideStringField;
    QrArreFutICNAB_Cfg: TIntegerField;
    DsArreFutI: TDataSource;
    QrArreFutA: TmySQLQuery;
    QrArreFutANOMEPROP: TWideStringField;
    QrArreFutAUNIDADE: TWideStringField;
    QrArreFutANOMECONTA: TWideStringField;
    QrArreFutAControle: TIntegerField;
    QrArreFutAConta: TIntegerField;
    QrArreFutACond: TIntegerField;
    QrArreFutAPeriodo: TIntegerField;
    QrArreFutAApto: TIntegerField;
    QrArreFutAPropriet: TIntegerField;
    QrArreFutAValor: TFloatField;
    QrArreFutATexto: TWideStringField;
    QrArreFutALk: TIntegerField;
    QrArreFutADataCad: TDateField;
    QrArreFutADataAlt: TDateField;
    QrArreFutAUserCad: TIntegerField;
    QrArreFutAUserAlt: TIntegerField;
    QrArreFutAPERIODO_TXT: TWideStringField;
    QrArreFutAInclui: TIntegerField;
    QrArreFutACNAB_Cfg: TIntegerField;
    DsArreFutA: TDataSource;
    QrBolArr: TmySQLQuery;
    QrBolArrValor: TFloatField;
    QrBolArrApto: TIntegerField;
    QrBolArrBOLAPTO: TWideStringField;
    QrBolArrBoleto: TFloatField;
    QrBolLei: TmySQLQuery;
    QrBolLeiValor: TFloatField;
    QrBolLeiApto: TIntegerField;
    QrBolLeiBOLAPTO: TWideStringField;
    QrBolLeiBoleto: TFloatField;
    QrBoletosUSERNAME: TWideStringField;
    QrBoletosPASSWORD: TWideStringField;
    QrBoletosConta: TIntegerField;
    PMPrevModBol: TPopupMenu;
    ModelosdeimpressoporUH1: TMenuItem;
    Definemodelo1: TMenuItem;
    Excluiitemns1: TMenuItem;
    QrBoletosProtocolo_TXT: TWideStringField;
    QrBoletosProtocolo2_TXT: TWideStringField;
    QrBoletosProtocoloCR_TXT: TWideStringField;
    QrBoletosProtocoPakCR: TIntegerField;
    QrBoletosProtocoPak2: TIntegerField;
    QrBoletosProtocoPak: TIntegerField;
    frxDsBoletosIts: TfrxDBDataset;
    frxDsBoletos: TfrxDBDataset;
    QrBoletosModelBloq: TSmallintField;
    QrBoletosCompe: TSmallintField;
    QrBoletosColunas: TSmallintField;
    QrBoletosBalAgrMens: TSmallintField;
    frxDsPRI: TfrxDBDataset;
    PMImprime: TPopupMenu;
    ImprimirboletosSelecionados1: TMenuItem;
    ImprimirTodosboletos1: TMenuItem;
    N2: TMenuItem;
    ImprimirboletosAtualescolhermodelo1: TMenuItem;
    QrPrevBloqFV: TIntegerField;
    QrBoletosConfigBol: TIntegerField;
    PnDados: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    BitBtn5: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    CkZerado: TCheckBox;
    PB1: TProgressBar;
    PB2: TProgressBar;
    PCModo: TPageControl;
    TabSheet13: TTabSheet;
    TabSheet14: TTabSheet;
    Panel42: TPanel;
    DBText12: TDBText;
    DBText13: TDBText;
    DBText1: TDBText;
    Panel14: TPanel;
    DBText3: TDBText;
    Label16: TLabel;
    DBText4: TDBText;
    Label17: TLabel;
    DBText7: TDBText;
    Label18: TLabel;
    Label19: TLabel;
    DBText8: TDBText;
    Label22: TLabel;
    DBText9: TDBText;
    PCGeral: TPageControl;
    TabSheet6: TTabSheet;
    Panel9: TPanel;
    BtOrcamento: TBitBtn;
    BtOrcProx: TBitBtn;
    PCOrcamento: TPageControl;
    TabSheet9: TTabSheet;
    ImgCondGer: TImage;
    TabSheet10: TTabSheet;
    Panel19: TPanel;
    Label33: TLabel;
    Label32: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label1: TLabel;
    DBEdit24: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit5: TDBEdit;
    GroupBox3: TGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    MeDBAvisoVerso: TDBMemo;
    TabSheet1: TTabSheet;
    Panel11: TPanel;
    BtProvisao: TBitBtn;
    BtProvProx: TBitBtn;
    BtProvAnt: TBitBtn;
    DBGPrevIts: TDBGrid;
    TabSheet2: TTabSheet;
    Panel12: TPanel;
    BtLeiProx: TBitBtn;
    BtLaiAnt: TBitBtn;
    BtLeitura: TBitBtn;
    BtLeiDesOrd: TBitBtn;
    Panel15: TPanel;
    DBGCons: TDBGrid;
    Panel16: TPanel;
    Label21: TLabel;
    DBText5: TDBText;
    DBText6: TDBText;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBGCNS: TdmkDBGrid;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    BtArrProx: TBitBtn;
    BtArrAnt: TBitBtn;
    BtArrecada: TBitBtn;
    PCArrecada: TPageControl;
    TabSheet7: TTabSheet;
    Panel13: TPanel;
    DBGrid7: TDBGrid;
    DBGrid8: TDBGrid;
    DBGrid6: TDBGrid;
    TabSheet8: TTabSheet;
    DBGrid19: TDBGrid;
    TabSheet4: TTabSheet;
    DBGradePreBloIts: TDBGrid;
    DBGradePreBlo: TDBGrid;
    Panel6: TPanel;
    BtPreProx: TBitBtn;
    BtPreAnt: TBitBtn;
    BtPrebol: TBitBtn;
    TabSheet5: TTabSheet;
    Panel17: TPanel;
    DBGradeBlo: TdmkDBGrid;
    Panel8: TPanel;
    BitBtn8: TBitBtn;
    BtBoleto: TBitBtn;
    BtImprime: TBitBtn;
    BtProtocolo: TBitBtn;
    BtRetorno: TBitBtn;
    BtPesq: TBitBtn;
    BtDesfazOrdemBloq: TBitBtn;
    BtTodosMens: TBitBtn;
    BtNenhumMens: TBitBtn;
    PCBoletosIts: TPageControl;
    TabSheet11: TTabSheet;
    GroupBox1: TGroupBox;
    DBGradeBloIts: TDBGrid;
    Panel32: TPanel;
    GroupBox5: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    GroupBox6: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    dmkDBEdit9: TdmkDBEdit;
    dmkDBEdit10: TdmkDBEdit;
    GroupBox8: TGroupBox;
    dmkDBGridZTO2: TdmkDBGridZTO;
    TabSheet12: TTabSheet;
    Panel10: TPanel;
    BtPrevModBol: TBitBtn;
    DBGrid21: TDBGrid;
    Panel23: TPanel;
    BtPEBProtocolo: TBitBtn;
    BtPEBVoltar: TBitBtn;
    BtPEBNenhum: TBitBtn;
    BtPEBEnvia: TBitBtn;
    BtPEBTodos: TBitBtn;
    TabControl1: TTabControl;
    Splitter6: TSplitter;
    DBGrid1: TDBGrid;
    DBG_ProtocoBol: TdmkDBGridZTO;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrPrevAfterOpen(DataSet: TDataSet);
    procedure QrPrevBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure QrPrevCalcFields(DataSet: TDataSet);
    procedure BitBtn5Click(Sender: TObject);
    procedure BtOrcamentoClick(Sender: TObject);
    procedure BtOrcProxClick(Sender: TObject);
    procedure BtProvisaoClick(Sender: TObject);
    procedure BtProvAntClick(Sender: TObject);
    procedure BtProvProxClick(Sender: TObject);
    procedure BtLeituraClick(Sender: TObject);
    procedure BtLaiAntClick(Sender: TObject);
    procedure BtLeiProxClick(Sender: TObject);
    procedure BtLeiDesOrdClick(Sender: TObject);
    procedure BtArrecadaClick(Sender: TObject);
    procedure BtArrAntClick(Sender: TObject);
    procedure BtArrProxClick(Sender: TObject);
    procedure BtPrebolClick(Sender: TObject);
    procedure BtPreAntClick(Sender: TObject);
    procedure BtPreProxClick(Sender: TObject);
    procedure BtBoletoClick(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BtDesfazOrdemBloqClick(Sender: TObject);
    procedure BtTodosMensClick(Sender: TObject);
    procedure BtNenhumMensClick(Sender: TObject);
    procedure Incluinovooramento1Click(Sender: TObject);
    procedure Alteraperiodoatual1Click(Sender: TObject);
    procedure TravadestravaPerodo1Click(Sender: TObject);
    procedure Adicinaitensbase1Click(Sender: TObject);
    procedure Novoitemdeoramento1Click(Sender: TObject);
    procedure Alteraitemselecionado1Click(Sender: TObject);
    procedure QrPrevAfterScroll(DataSet: TDataSet);
    procedure QrPrevBeforeClose(DataSet: TDataSet);
    procedure QrConsAfterScroll(DataSet: TDataSet);
    procedure QrConsBeforeClose(DataSet: TDataSet);
    procedure QrArreAfterScroll(DataSet: TDataSet);
    procedure QrArreBeforeClose(DataSet: TDataSet);
    procedure QrArreBolBeforeClose(DataSet: TDataSet);
    procedure QrArreBolAfterScroll(DataSet: TDataSet);
    procedure QrBoletosCalcFields(DataSet: TDataSet);
    procedure QrBoletosItsCalcFields(DataSet: TDataSet);
    procedure QrBoletosBeforeClose(DataSet: TDataSet);
    procedure QrBoletosAfterScroll(DataSet: TDataSet);
    procedure QrPrevModBolCalcFields(DataSet: TDataSet);
    procedure QrConsCalcFields(DataSet: TDataSet);
    procedure QrCNSCalcFields(DataSet: TDataSet);
    procedure DBGPrevItsDblClick(Sender: TObject);
    procedure DBGCNSDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Agendamentoporvarreduradelanamentos1Click(Sender: TObject);
    procedure ItemAtual2Click(Sender: TObject);
    procedure ItensSelecionados2Click(Sender: TObject);
    procedure odosItens1Click(Sender: TObject);
    procedure ItemSelecionado3Click(Sender: TObject);
    procedure ItensSelecionados1Click(Sender: TObject);
    procedure Todositens1Click(Sender: TObject);
    procedure Adicionaitensdeprovisoagendados1Click(Sender: TObject);
    procedure Excluileituraatual2Click(Sender: TObject);
    procedure Excluileiturasselecionadas1Click(Sender: TObject);
    procedure Excluitodasleituras1Click(Sender: TObject);
    procedure Alteraleituraatual1Click(Sender: TObject);
    procedure Rateiraconsumo1Click(Sender: TObject);
    procedure Incluileituras1Click(Sender: TObject);
    procedure Incluiarrecadaofutura1Click(Sender: TObject);
    procedure QrArreFutICalcFields(DataSet: TDataSet);
    procedure Alteraarrecadaofutura1Click(Sender: TObject);
    procedure Excluiarrecadaofutura1Click(Sender: TObject);
    procedure QrArreFutACalcFields(DataSet: TDataSet);
    procedure Incluiitenspragendados1Click(Sender: TObject);
    procedure Incluiintensbasedearrecadao1Click(Sender: TObject);
    procedure nicoimvel1Click(Sender: TObject);
    procedure Itemselecionado1Click(Sender: TObject);
    procedure Mltiplosimveis1Click(Sender: TObject);
    procedure Itematual3Click(Sender: TObject);
    procedure TODOSitensdearrecadao1Click(Sender: TObject);
    procedure PMOrcamentoPopup(Sender: TObject);
    procedure PMProvisaoPopup(Sender: TObject);
    procedure PMLeituraPopup(Sender: TObject);
    procedure PMArrecadaPopup(Sender: TObject);
    procedure PMArreFutPopup(Sender: TObject);
    procedure ItemAtualdoprbloqueto1Click(Sender: TObject);
    procedure OsitensSelecionadosdoprbloquetoselecionado1Click(Sender: TObject);
    procedure Todositensdoprbloquetoatual1Click(Sender: TObject);
    procedure Gerartodosabertos1Click(Sender: TObject);
    procedure GerarAtual1Click(Sender: TObject);
    procedure Gerarselecionados1Click(Sender: TObject);
    procedure DobloquetoAtual2Click(Sender: TObject);
    procedure DosbloquetosSelecionados1Click(Sender: TObject);
    procedure DeTodosbloquetos2Click(Sender: TObject);
    procedure DoBloquetoatual1Click(Sender: TObject);
    procedure BloquetosSelecionados1Click(Sender: TObject);
    procedure DeTodosBloquetos1Click(Sender: TObject);
    procedure Atual3Click(Sender: TObject);
    procedure Selecionados3Click(Sender: TObject);
    procedure Todos3Click(Sender: TObject);
    procedure PCGeralChange(Sender: TObject);
    procedure PMPrebol0Popup(Sender: TObject);
    procedure PMBoletoPopup(Sender: TObject);
    procedure Definemodelo1Click(Sender: TObject);
    procedure Excluiitemns1Click(Sender: TObject);
    procedure PMPrevModBolPopup(Sender: TObject);
    procedure BtPrevModBolClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure ImprimirboletosSelecionados1Click(Sender: TObject);
    procedure ImprimirTodosboletos1Click(Sender: TObject);
    procedure ImprimirboletosAtualescolhermodelo1Click(Sender: TObject);
    procedure PMImprimePopup(Sender: TObject);
    procedure BtProtocoloClick(Sender: TObject);
    procedure BtPEBVoltarClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure LocPeriodo(Atual, Periodo: Integer);
    procedure ConfiguraJanela(Aba: Integer; Modo: TModoVisualizacaoBloquetos = istVisMensal);
    procedure ExcluiItemProvisao(Tipo: TSelType; Reagenda: Boolean);
    procedure RecalculaArrecadacoes;
    procedure CalculaTotalARIEReabreArreEARI(Apto, Propriet, Controle: Integer);
    procedure ExcluiLeitura(Tipo: TselType);
    procedure ExcluiItemPreBloqueto(Tipo: TselType);
    procedure GeraBloq(Tipo: TSelType);
    procedure AlteraVencimentoPreBol(Selecao: TSelType; PreBol: Boolean; Grade: TDBGrid);
    procedure DesfazerBoletos(Tipo: TSelType);
    function  ImprimeBloq(Quais: TselType; Como: TfrxImpComo; Arquivo: String;
                Filtro: TfrxCustomExportFilter; Escolher: Boolean): TfrxReport;
  public
    { Public declarations }
  end;

var
  FmCondGer2: TFmCondGer2;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyGlyfs, Principal, UnBloqGerl_Jan, ModuleGeral,
  DmkDAC_PF, UnBloqCond_Jan, UnBloqCond, MyVCLSkin, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCondGer2.LocPeriodo(Atual, Periodo: Integer);
begin
  Screen.Cursor := crHourGlass;
  DefParams;
  GOTOy.LC(Atual, Periodo);
  Screen.Cursor := crDefault;
end;

procedure TFmCondGer2.Mltiplosimveis1Click(Sender: TObject);
begin
  UBloqCond_Jan.MostraFmCondGerArreMul(stIns, VAR_ModBloq_CliInt,
    VAR_ModBloq_EntCliInt, QrPrevCodigo.Value, QrArreApto.Value,
    QrArrePropriet.Value, VAR_ModBloq_TabAriA);
  CalculaTotalARIEReabreArreEARI(0, 0, 0);
end;

procedure TFmCondGer2.nicoimvel1Click(Sender: TObject);
begin
  UBloqCond_Jan.MostraFmCondGerArreUni(stIns, QrAri, QrPrevCodigo.Value,
    VAR_ModBloq_CliInt, VAR_ModBloq_EntCliInt, QrPrevPERIODO_TXT.Value,
    VAR_ModBloq_TabLctA, VAR_ModBloq_TabAriA);
  CalculaTotalARIEReabreArreEARI(0, 0, 0);
end;

procedure TFmCondGer2.Novoitemdeoramento1Click(Sender: TObject);
var
  Codigo, Controle, Periodo: Integer;
begin
  UBloqCond_Jan.MostraFmCondGerPrev(stIns, QrPRI, VAR_ModBloq_TabPriA,
    QrPrevPERIODO_TXT.Value, QrPrevCodigo.Value);
  //
  Controle := QrPRIControle.Value;
  Codigo   := QrPrevCodigo.Value;
  Periodo  := QrPrevPeriodo.Value;
  //
  if UBloqCond.CalculaTotalPRI(VAR_ModBloq_TabPriA, VAR_ModBloq_TabPrvA,
    Periodo, Codigo, Controle) then
  begin
    LocPeriodo(Periodo, Periodo);
    //
    UBloqCond.ReopenPRI(QrPRI, QrPrevCodigo.Value, Controle, VAR_ModBloq_TabPriA);
  end;
  RecalculaArrecadacoes;
end;

procedure TFmCondGer2.odosItens1Click(Sender: TObject);
begin
  ExcluiItemProvisao(istTodos, True);
end;

procedure TFmCondGer2.OsitensSelecionadosdoprbloquetoselecionado1Click(
  Sender: TObject);
begin
  ExcluiItemPreBloqueto(istSelecionados);
end;

procedure TFmCondGer2.PCGeralChange(Sender: TObject);
begin
  ConfiguraJanela(PCGeral.ActivePageIndex);
end;

procedure TFmCondGer2.PMArrecadaPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0) and (QrPrevEncerrado.Value = 0);
  Enab2 := (QrArre.State <> dsInactive) and (QrArre.RecordCount > 0);
  //
  Incluiintensbasedearrecadao1.Enabled := Enab;
  Novoitemdearrecadao1.Enabled         := Enab;
  Incluiitenspragendados1.Enabled      := Enab;
  //
  Alteraarrecadao1.Enabled    := Enab and Enab2;
  Exclusodearrecadao1.Enabled := Enab and Enab2;
end;

procedure TFmCondGer2.PMArreFutPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0) and (QrPrevEncerrado.Value = 0);
  Enab2 := (QrArreFutI.State <> dsInactive) and (QrArreFutI.RecordCount > 0);
  //
  Incluiarrecadaofutura1.Enabled := Enab;
  //
  Alteraarrecadaofutura1.Enabled := Enab and Enab2;
  Excluiarrecadaofutura1.Enabled := Enab and Enab2;
end;

procedure TFmCondGer2.PMBoletoPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0) and (QrPrevEncerrado.Value = 0);
  Enab2 := (QrBoletos.State <> dsInactive) and (QrBoletos.RecordCount > 0);
  //
  Desfazerboletos1.Enabled  := Enab and Enab2;
  Alteravencimento1.Enabled := Enab and Enab2;
end;

procedure TFmCondGer2.PMImprimePopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrBoletos.State <> dsInactive) and (QrBoletos.RecordCount > 0);
  //
  ImprimirboletosSelecionados1.Enabled        := Enab;
  ImprimirTodosboletos1.Enabled               := Enab;
  ImprimirboletosAtualescolhermodelo1.Enabled := Enab;
end;

procedure TFmCondGer2.PMLeituraPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0) and (QrPrevEncerrado.Value = 0);
  Enab2 := (QrCNS.State <> dsInactive) and (QrCNS.RecordCount > 0);
  //
  Incluileituras1.Enabled := Enab;
  Rateiraconsumo1.Enabled := Enab;
  //
  Alteraleituraatual1.Enabled := Enab and Enab2;
  Excluileituraatual1.Enabled := Enab and Enab2;
end;

procedure TFmCondGer2.PMOrcamentoPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  //
  Alteraperiodoatual1.Enabled  := Enab and (QrPrevEncerrado.Value = 0);
  TravadestravaPerodo1.Enabled := Enab;
end;

procedure TFmCondGer2.PMPrebol0Popup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0) and (QrPrevEncerrado.Value = 0);
  Enab2 := (QrBoletos.State <> dsInactive) and (QrBoletos.RecordCount > 0);
  //
  Bloqueto1.Enabled         := Enab and Enab2;
  AlteraVencimento2.Enabled := Enab and Enab2;
  Excluir2.Enabled          := Enab and Enab2;
end;

procedure TFmCondGer2.PMPrevModBolPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0) and (QrPrevEncerrado.Value = 0);
  Enab2 := (QrPrevModBol.State <> dsInactive) and (QrPrevModBol.RecordCount > 0);
  //
  Definemodelo1.Enabled := Enab;
  Excluiitemns1.Enabled := Enab and Enab2;
end;

procedure TFmCondGer2.PMProvisaoPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0) and (QrPrevEncerrado.Value = 0);
  Enab2 := (QrPRI.State <> dsInactive) and (QrPRI.RecordCount > 0);
  //
  Adicinaitensbase1.Enabled                    := Enab;
  Adicionaitensdeprovisoagendados1.Enabled     := Enab;
  Novoitemdeoramento1.Enabled                  := Enab;
  Agendamentoporvarreduradelanamentos1.Enabled := Enab;
  //
  Alteraitemselecionado1.Enabled           := Enab and Enab2;
  Excluiitemdeoramentoselecionado1.Enabled := Enab and Enab2;
end;

procedure TFmCondGer2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrevPeriodo.Value, LaRegistro.Caption[2]);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCondGer2.DefParams;
begin
  VAR_GOTOTABELA := VAR_ModBloq_TabPrvA;
  VAR_GOTOMYSQLTABLE := QrPrev;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := 'Periodo';
  VAR_GOTONOME := '';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT prv.*, cnd.Cliente CondCli, cnd.Codigo CondCod, ');
  VAR_SQLx.Add('cfb.Nome NOMECONFIGBOL');
  VAR_SQLx.Add('FROM ' + VAR_ModBloq_TabPrvA + ' prv');
  VAR_SQLx.Add('LEFT JOIN cond cnd ON cnd.Codigo=prv.Cond');
  VAR_SQLx.Add('LEFT JOIN configbol cfb ON cfb.Codigo=prv.ConfigBol');
  VAR_SQLx.Add('WHERE prv.Cond=' + Geral.FF0(VAR_ModBloq_CliInt));
  //
  VAR_SQL1.Add('AND prv.Periodo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('');//AND prv.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Cond=' + Geral.FF0(VAR_ModBloq_CliInt);
end;

procedure TFmCondGer2.DesfazerBoletos(Tipo: TSelType);
var
  Desfez: Boolean;
begin
  case Tipo of
    istAtual:
      Desfez := UBloqCond.DesfazerBoletos(Tipo, Dmod.QrUpd, Dmod.QrAux,
                  QrBoletos, QrBoletosIts, Dmod.MyDB, TDBGrid(DBGradeBlo),
                  VAR_ModBloq_TabLctA, VAR_ModBloq_TabPrvA,
                  VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA, VAR_ModBloq_CliInt,
                  QrPrevPeriodo.Value, PB1);
    istSelecionados:
      Desfez := UBloqCond.DesfazerBoletos(Tipo, Dmod.QrUpd, Dmod.QrAux,
                  QrBoletos, QrBoletosIts, Dmod.MyDB, TDBGrid(DBGradeBlo),
                  VAR_ModBloq_TabLctA, VAR_ModBloq_TabPrvA,
                  VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA, VAR_ModBloq_CliInt,
                  QrPrevPeriodo.Value, PB1);
    istTodos:
      Desfez := UBloqCond.DesfazerBoletos(Tipo, Dmod.QrUpd, Dmod.QrAux,
                  QrBoletos, QrBoletosIts, Dmod.MyDB, TDBGrid(DBGradeBlo),
                  VAR_ModBloq_TabLctA, VAR_ModBloq_TabPrvA,
                  VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA, VAR_ModBloq_CliInt,
                  QrPrevPeriodo.Value, PB1);
  end;
  if Desfez then
  begin
    CalculaTotalARIEReabreArreEARI(0, 0, 0);
    UBloqCond.ReopenBoletos(QrBoletos, QrBolArr, QrBolLei, QrBoletosBOLAPTO.Value,
      False, VAR_ModBloq_TabPrvA, VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA,
      VAR_ModBloq_CliInt, QrPrevPeriodo.Value, QrPrevCodigo.Value);
  end;
end;

procedure TFmCondGer2.DeTodosBloquetos1Click(Sender: TObject);
begin
  AlteraVencimentoPreBol(istTodos, False, TDBGrid(DBGradeBlo));
end;

procedure TFmCondGer2.DeTodosbloquetos2Click(Sender: TObject);
begin
  AlteraVencimentoPrebol(istSelecionados, True, DBGradePreBlo);
end;

procedure TFmCondGer2.DoBloquetoatual1Click(Sender: TObject);
begin
  AlteraVencimentoPreBol(istAtual, False, TDBGrid(DBGradeBlo));
end;

procedure TFmCondGer2.DobloquetoAtual2Click(Sender: TObject);
begin
  AlteraVencimentoPrebol(istAtual, True, DBGradePreBlo);
end;

procedure TFmCondGer2.DosbloquetosSelecionados1Click(Sender: TObject);
begin
  AlteraVencimentoPrebol(istSelecionados, True, DBGradePreBlo);
end;

procedure TFmCondGer2.Excluiarrecadaofutura1Click(Sender: TObject);
begin
  if UMyMod.SQLDel1(Dmod.QrUpd, QrArreFutI, 'ArreFut', 'Controle',
    QrArreFutI.FieldByName('Controle').AsInteger, True,
    'Confirma a exclus�o da arrecada��o futura selecionada?', True)
  then
    Geral.MB_Aviso('AVISO! A exclus�o de item futuro n�o elimina item j� arrecadado!');
end;

procedure TFmCondGer2.Excluiitemns1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPrevModBol, DBGrid21,
    'prevmodbol', ['Codigo', 'Apto'], ['Codigo', 'Apto'], istPergunta, '');
end;

procedure TFmCondGer2.ExcluiItemPreBloqueto(Tipo: TselType);
begin
  UBloqCond.ExcluiItemPreBloqueto(Tipo, VAR_ModBloq_TabAriA,
    VAR_ModBloq_TabCnsA, QrBoletosIts, DBGradePreBloIts);
  CalculaTotalARIEReabreArreEARI(0, 0, 0);
  UBloqCond.ReopenBoletos(QrBoletos, QrBolArr, QrBolLei, QrBoletosBOLAPTO.Value,
    True, VAR_ModBloq_TabPrvA, VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA,
    VAR_ModBloq_CliInt, QrPrevPeriodo.Value, QrPrevCodigo.Value);
end;

procedure TFmCondGer2.ExcluiItemProvisao(Tipo: TSelType; Reagenda: Boolean);

  procedure ExcluiProvisaoAtual(Reagenda: Boolean);
  begin
    if Reagenda then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'prevbai', False, [
        'prevcod'], ['Controle'], [0], [QrPRIPrevBaI.Value], True);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      DELETE_FROM  + VAR_ModBloq_TabPriA,
      'WHERE Controle=' + Geral.FF0(QrPRIControle.Value),
      '']);
  end;

var
  i, Prox, Periodo, Codigo: integer;
begin
  i := 0;
  if Tipo <> istTodos then
  begin
    case Tipo of
      istAtual: i := 1;
      istSelecionados:
      begin
        i := DBGPrevIts.SelectedRows.Count;
        if i = 0 then i := 1;
      end;
    end;
    if i = 1 then
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o do item de provis�o selecionado?') = ID_YES then
        ExcluiProvisaoAtual(Reagenda);
    end else
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o dos ' +
        Geral.FF0(DBGPrevIts.SelectedRows.Count) + ' itens de provis�o selecionados?') = ID_YES then
      begin
        with DBGPrevIts.DataSource.DataSet do
        for i:= 0 to DBGPrevIts.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGPrevIts.SelectedRows.Items[i]));
          ExcluiProvisaoAtual(Reagenda);
        end;
      end;
    end;
  end else
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o de TODOS ITENS de provis�o deste per�odo?') = ID_YES then
    begin
      QrPRI.First;
      while not QrPRI.Eof do
      begin
        ExcluiProvisaoAtual(Reagenda);
        QrPRI.Next;
      end;
    end;
  end;
  Prox     := UMyMod.ProximoRegistro(QrPRI, 'Controle', QrPRIControle.Value);
  Codigo   := QrPrevCodigo.Value;
  Periodo  := QrPrevPeriodo.Value;
  //
  if UBloqCond.CalculaTotalPRI(VAR_ModBloq_TabPriA, VAR_ModBloq_TabPrvA,
    Periodo, Codigo, Prox) then
  begin
    LocPeriodo(Periodo, Periodo);
    //
    UBloqCond.ReopenPRI(QrPRI, QrPrevCodigo.Value, Prox, VAR_ModBloq_TabPriA);
  end;
  RecalculaArrecadacoes;
end;

procedure TFmCondGer2.CalculaTotalARIEReabreArreEARI(Apto, Propriet,
  Controle: Integer);
var
  Qry: TmySQLQuery;
  Codigo, Periodo: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    Codigo  := QrPrevCodigo.Value;
    Periodo := QrPrevPeriodo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(Valor) Total ',
      'FROM ' + VAR_ModBloq_TabPriA,
      'WHERE Codigo=' + Geral.FF0(QrPrevCodigo.Value),
      '']);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'arre', False,
      ['Gastos'], ['Codigo'], [Qry.FieldByName('Total').AsFloat], [Codigo], True) then
    begin
      LocPeriodo(Periodo, Periodo);
      UBloqCond.ReopenArre(QrArre, QrSumARRE, QrPrevCodigo.Value, Apto,
        Propriet, VAR_ModBloq_TabAriA);
      UBloqCond.ReopenARI(QrAri, QrPrevCodigo.Value, Controle, QrArreApto.Value,
        QrArrePropriet.Value, QrArreBolBoleto.Value, VAR_ModBloq_TabAriA);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGer2.ConfiguraJanela(Aba: Integer; Modo: TModoVisualizacaoBloquetos = istVisMensal);
var
  Hoje: TDateTime;
  Periodo: Integer;
  Imagem: String;
begin
  if Modo = istVisProtEnt then
  begin
    PCModo.ActivePageIndex := 1;
    //
  end else
  begin
    PCModo.ActivePageIndex := 0;
    //
    DBGPrevIts.DataSource       := DsPRI;
    DBGCons.DataSource          := DsCons;
    DBGCNS.DataSource           := DsCNS;
    DBGrid6.DataSource          := DsArre;
    DBGrid8.DataSource          := DsArreBol;
    DBGrid7.DataSource          := DsAri;
    DBGradePreBlo.DataSource    := DsBoletos;
    DBGradePreBloIts.DataSource := DsBoletosIts;
    DBGradeBlo.DataSource       := DsBoletos;
    DBGradeBloIts.DataSource    := DsBoletosIts;
    DBGrid21.DataSource         := DsPrevModBol;
    DBGrid19.DataSource         := DsArreFutI;
    //
    Imagem := Geral.ReadAppKey('ImagemFundo', Application.Title, ktString,
                VAR_APP + 'Fundo.jpg', HKEY_LOCAL_MACHINE);
    //
    if FileExists(Imagem) then
      ImgCondGer.Picture.LoadFromFile(Imagem);
    //
    case Aba of
      0: //Or�amento
      begin
        PCGeral.ActivePageIndex     := 0;
        PCOrcamento.ActivePageIndex := 0;
      end;
      1: //Provis�es
      begin
        PCGeral.ActivePageIndex := 1;
        //
        UBloqCond.ReopenPRI(QrPRI, QrPrevCodigo.Value, 0, VAR_ModBloq_TabPriA);
      end;
      2: //Consumo por leitura
      begin
        PCGeral.ActivePageIndex := 2;
        //
        UBloqCond.ReopenQrCons(QrCons, QrSumCT, VAR_ModBloq_TabCnsA,
          VAR_ModBloq_CliInt, QrPrevPeriodo.Value, 0);
      end;
      3: //Arrecada��es
      begin
        PCGeral.ActivePageIndex    := 3;
        PCArrecada.ActivePageIndex := 0;
        //
        UBloqCond.ReopenArre(QrArre, QrSumARRE, QrPrevCodigo.Value, 0, 0, VAR_ModBloq_TabAriA);
        UBloqCond.ReopenArreFut(QrArreFutI, VAR_ModBloq_CliInt, 0, 0);
      end;
      4: //Pr�-boletos
      begin
        PCGeral.ActivePageIndex := 4;
        //
        UBloqCond.ReopenBoletos(QrBoletos, QrBolArr, QrBolLei, '', True,
          VAR_ModBloq_TabPrvA, VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA,
          VAR_ModBloq_CliInt, QrPrevPeriodo.Value, QrPrevCodigo.Value);
      end;
      5: //Boletos
      begin
        PCGeral.ActivePageIndex      := 5;
        PCBoletosIts.ActivePageIndex := 0;
        //
        UBloqCond.ReopenBoletos(QrBoletos, QrBolArr, QrBolLei, '', False,
          VAR_ModBloq_TabPrvA, VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA,
          VAR_ModBloq_CliInt, QrPrevPeriodo.Value, QrPrevCodigo.Value);
        //
        UBloqCond.ReopenPrevModBol(QrPrevModBol, QrPrevCodigo.Value, 0);
      end;
      else
      begin
        //Configura autom�tico na abertura
        PCGeral.ActivePageIndex     := 0;
        PCOrcamento.ActivePageIndex := 0;
        PCArrecada.ActivePageIndex  := 0;
        //
        Hoje    := DModG.ObtemAgora;
        Periodo := Geral.Periodo2000(Hoje);
        //
        if UBloqGerl.PeriodoExiste(Periodo, 'prev', Dmod.MyDB) = True then
          LocPeriodo(Periodo, Periodo)
        else
          Va(vpLast);
        //
        if (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0) then
        begin
          UBloqCond.ReopenBoletos(QrBoletos, QrBolArr, QrBolLei, '', False,
            VAR_ModBloq_TabPrvA, VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA,
            VAR_ModBloq_CliInt, QrPrevPeriodo.Value, QrPrevCodigo.Value);
          //
          if QrBoletos.RecordCount > 0 then
          begin
            PCGeral.ActivePageIndex := 5;
            PCBoletosIts.ActivePageIndex := 0;
            //
            UBloqCond.ReopenPrevModBol(QrPrevModBol, QrPrevCodigo.Value, 0);
          end;
        end;
      end;
    end;
  end;
end;

procedure TFmCondGer2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmCondGer2.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCondGer2.Rateiraconsumo1Click(Sender: TObject);
begin
  UBloqCond_Jan.MostraCondGerLei2(VAR_ModBloq_CliInt, VAR_ModBloq_EntCliInt,
    QrPrevPeriodo.Value, DmodG.QrEmpresasNOMEFILIAL.Value, VAR_ModBloq_TabLctA,
    VAR_ModBloq_TabCnsA);
  UBloqCond.ReopenQrCons(QrCons, QrSumCT, VAR_ModBloq_TabCnsA, VAR_ModBloq_CliInt,
    QrPrevPeriodo.Value, QrConsCodigo.Value);
end;

procedure TFmCondGer2.RecalculaArrecadacoes;
begin
  if UBloqCond.RecalculaArrecadacoes(VAR_ModBloq_TabAriA, VAR_ModBloq_TabLctA,
    QrPrevCodigo.Value, VAR_ModBloq_CliInt, QrPrevGastos.Value) then
  begin
    CalculaTotalARIEReabreArreEARI(QrArreApto.Value, QrArrePropriet.Value,
      QrAriControle.Value);
  end;
end;

procedure TFmCondGer2.DBGCNSDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'DifCaren' then
    MeuVCLSkin.DrawGrid(TDBGrid(DBGCNS), Rect, 1, QrCNSDifCaren.Value);
end;

procedure TFmCondGer2.DBGPrevItsDblClick(Sender: TObject);
var
  ValorTxt: String;
  Codigo, Controle, Periodo: Integer;
begin
  if (QrPRI.State <> dsInactive) and (QrPRI.RecordCount > 0) then
  begin
    ValorTxt := FormatFloat('0.00', QrPRIValor.Value);
    //
    if InputQuery('Obten��o de valor', 'Informe o novo valor:', ValorTxt) then
    begin
      Codigo   := QrPrevCodigo.Value;
      Periodo  := QrPrevPeriodo.Value;
      Controle := QrPRIControle.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, VAR_ModBloq_TabPriA, False,
        ['Valor'], ['Controle'], [Geral.DMV(ValorTxt)], [Controle], True) then
      begin
        if UBloqCond.CalculaTotalPRI(VAR_ModBloq_TabPriA, VAR_ModBloq_TabPrvA,
          Periodo, Codigo, Controle) then
        begin
          LocPeriodo(Periodo, Periodo);
          //
          UBloqCond.ReopenPRI(QrPRI, QrPrevCodigo.Value, Controle, VAR_ModBloq_TabPriA);
        end;
      end;
    end;
  end;
end;

procedure TFmCondGer2.Definemodelo1Click(Sender: TObject);
begin
  UBloqCond_Jan.MostraAptosModBol(QrPrevCodigo.Value, VAR_ModBloq_CliInt, 'prevmodbol');
  UBloqCond.ReopenPrevModBol(QrPrevModBol, QrPrevCodigo.Value, 0);
end;

procedure TFmCondGer2.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCondGer2.Selecionados3Click(Sender: TObject);
begin
  DesfazerBoletos(istSelecionados);
end;

procedure TFmCondGer2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCondGer2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCondGer2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCondGer2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCondGer2.Todos3Click(Sender: TObject);
begin
  DesfazerBoletos(istTodos);
end;

procedure TFmCondGer2.Todositens1Click(Sender: TObject);
begin
  ExcluiItemProvisao(istTodos, False);
end;

procedure TFmCondGer2.TODOSitensdearrecadao1Click(Sender: TObject);
begin
  UBloqCond_Jan.MostraCondGerDelArre(VAR_ModBloq_TabAriA, VAR_ModBloq_TabLctA,
    QrPrevCodigo.Value);
  CalculaTotalARIEReabreArreEARI(0, 0, 0);
end;

procedure TFmCondGer2.Todositensdoprbloquetoatual1Click(Sender: TObject);
begin
  ExcluiItemPreBloqueto(istTodos);
end;

procedure TFmCondGer2.TravadestravaPerodo1Click(Sender: TObject);
var
  Periodo: Integer;
begin
  Periodo := UBloqGerl.EncerraPeriodo(Dmod.QrUpd, QrPrev, VAR_ModBloq_TabPrvA);
  //
  if Periodo <> 0 then
  begin
    LocPeriodo(Periodo, Periodo);
  end;
end;

procedure TFmCondGer2.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGer2.BtTodosMensClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGradeBlo), True);
end;

procedure TFmCondGer2.BtLeiDesOrdClick(Sender: TObject);
begin
  UnDmkDAC_PF.DesfazOrdenacaoDmkDBGrid(TDmkDBGrid(DBGCNS), 'Controle');
end;

procedure TFmCondGer2.BtArrAntClick(Sender: TObject);
begin
  ConfiguraJanela(2);
end;

procedure TFmCondGer2.BtPreProxClick(Sender: TObject);
begin
  ConfiguraJanela(5);
end;

procedure TFmCondGer2.BtPrevModBolClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPrevModBol, BtPrevModBol);
end;

procedure TFmCondGer2.Adicinaitensbase1Click(Sender: TObject);
var
  Codigo, Controle, Periodo: Integer;
begin
  UBloqCond_Jan.MostraPrevBAB(VAR_ModBloq_CliInt, VAR_ModBloq_EntCliInt,
    QrPrevCodigo.Value, QrPrevPeriodo.Value, VAR_ModBloq_TabPriA,
    VAR_ModBloq_TabLctA, VAR_ModBloq_TabLctB, VAR_ModBloq_TabLctD,
    DmodG.QrEmpresasNOMEFILIAL.Value, PB1);
  //
  Controle := QrPRIControle.Value;
  Codigo   := QrPrevCodigo.Value;
  Periodo  := QrPrevPeriodo.Value;
  //
  if UBloqCond.CalculaTotalPRI(VAR_ModBloq_TabPriA, VAR_ModBloq_TabPrvA,
    Periodo, Codigo, Controle) then
  begin
    LocPeriodo(Periodo, Periodo);
    //
    UBloqCond.ReopenPRI(QrPRI, QrPrevCodigo.Value, Controle, VAR_ModBloq_TabPriA);
  end;
  RecalculaArrecadacoes;
end;

procedure TFmCondGer2.Adicionaitensdeprovisoagendados1Click(Sender: TObject);
var
  Codigo, Controle, Periodo: Integer;
begin
  UBloqCond_Jan.MostraPrevBaA(QrPrevCodigo.Value, VAR_ModBloq_CliInt,
    VAR_ModBloq_TabPriA);
  //
  Controle := QrPRIControle.Value;
  Codigo   := QrPrevCodigo.Value;
  Periodo  := QrPrevPeriodo.Value;
  //
  if UBloqCond.CalculaTotalPRI(VAR_ModBloq_TabPriA, VAR_ModBloq_TabPrvA,
    Periodo, Codigo, Controle) then
  begin
    LocPeriodo(Periodo, Periodo);
    //
    UBloqCond.ReopenPRI(QrPRI, QrPrevCodigo.Value, Controle, VAR_ModBloq_TabPriA);
  end;
  RecalculaArrecadacoes;
end;

procedure TFmCondGer2.Agendamentoporvarreduradelanamentos1Click(
  Sender: TObject);
var
  Codigo, Controle, Periodo: Integer;
begin
  UBloqCond_Jan.MostraPrevVeri(VAR_ModBloq_EntCliInt, VAR_ModBloq_CliInt,
    VAR_ModBloq_TabLctA, VAR_ModBloq_TabPrvA);
  //
  Controle := QrPRIControle.Value;
  Codigo   := QrPrevCodigo.Value;
  Periodo  := QrPrevPeriodo.Value;
  //
  if UBloqCond.CalculaTotalPRI(VAR_ModBloq_TabPriA, VAR_ModBloq_TabPrvA,
    Periodo, Codigo, Controle) then
  begin
    LocPeriodo(Periodo, Periodo);
    //
    UBloqCond.ReopenPRI(QrPRI, QrPrevCodigo.Value, Controle, VAR_ModBloq_TabPriA);
  end;
  RecalculaArrecadacoes;
end;

procedure TFmCondGer2.Alteraarrecadaofutura1Click(Sender: TObject);
begin
  UBloqCond_Jan.MostraFmCondGerArreFut(stUpd, VAR_ModBloq_CliInt, QrArreFutI);
  UBloqCond.ReopenArreFut(QrArreFutI, VAR_ModBloq_CliInt, 0, QrArreFutIControle.Value);
end;

procedure TFmCondGer2.Alteraitemselecionado1Click(Sender: TObject);
var
  Codigo, Controle, Periodo: Integer;
begin
  UBloqCond_Jan.MostraFmCondGerPrev(stUpd, QrPRI, VAR_ModBloq_TabPriA,
    QrPrevPERIODO_TXT.Value, QrPrevCodigo.Value);
  //
  Controle := QrPRIControle.Value;
  Codigo   := QrPrevCodigo.Value;
  Periodo  := QrPrevPeriodo.Value;
  //
  if UBloqCond.CalculaTotalPRI(VAR_ModBloq_TabPriA, VAR_ModBloq_TabPrvA,
    Periodo, Codigo, Controle) then
  begin
    LocPeriodo(Periodo, Periodo);
    //
    UBloqCond.ReopenPRI(QrPRI, QrPrevCodigo.Value, Controle, VAR_ModBloq_TabPriA);
  end;
  RecalculaArrecadacoes;
end;

procedure TFmCondGer2.Alteraleituraatual1Click(Sender: TObject);
begin
  UBloqCond_Jan.MostraCondGerLeiEdit(QrCNS, QrConsCasas.Value,
    QrConsUnidFat.AsFloat, VAR_ModBloq_TabLctA, VAR_ModBloq_TabCnsA);
  UBloqCond.ReopenQrCons(QrCons, QrSumCT, VAR_ModBloq_TabCnsA, VAR_ModBloq_CliInt,
    QrPrevPeriodo.Value, QrConsCodigo.Value);
end;

procedure TFmCondGer2.Alteraperiodoatual1Click(Sender: TObject);
var
  Periodo: Integer;
begin
  Periodo := UBloqCond_Jan.MostraCondGerNew(stUpd, QrPrev, VAR_ModBloq_CliInt,
               VAR_ModBloq_TabPrvA);
  if Periodo <> 0 then
    LocPeriodo(Periodo, Periodo);
end;

procedure TFmCondGer2.AlteraVencimentoPreBol(Selecao: TSelType; PreBol: Boolean;
  Grade: TDBGrid);
var
  Alterou: Boolean;
begin
  Alterou := False;
  //
  case Selecao of
    istAtual:
      Alterou := UBloqCond.AlteraVencimentoPrebol(VAR_ModBloq_CliInt,
                   QrPrevPeriodo.Value, QrBoletosApto.Value, PreBol, istAtual,
                   Grade, QrBoletos, QrBoletosIts, Dmod.QrUpd, Dmod.QrAux,
                   Dmod.MyDB, PB1, VAR_ModBloq_TabLctA);
    istSelecionados:
      Alterou := UBloqCond.AlteraVencimentoPrebol(VAR_ModBloq_CliInt,
                   QrPrevPeriodo.Value, QrBoletosApto.Value, PreBol, istSelecionados,
                   Grade, QrBoletos, QrBoletosIts, Dmod.QrUpd,
                   Dmod.QrAux, Dmod.MyDB, PB1, VAR_ModBloq_TabLctA);
    istTodos:
      Alterou := UBloqCond.AlteraVencimentoPrebol(VAR_ModBloq_CliInt,
                   QrPrevPeriodo.Value, QrBoletosApto.Value, PreBol, istTodos,
                   Grade, QrBoletos, QrBoletosIts, Dmod.QrUpd, Dmod.QrAux,
                   Dmod.MyDB, PB1, VAR_ModBloq_TabLctA);
  end;
  if Alterou then
  begin
    CalculaTotalARIEReabreArreEARI(0, 0, 0);
    UBloqCond.ReopenBoletos(QrBoletos, QrBolArr, QrBolLei, QrBoletosBOLAPTO.Value,
      True, VAR_ModBloq_TabPrvA, VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA,
      VAR_ModBloq_CliInt, QrPrevPeriodo.Value, QrPrevCodigo.Value);
  end;
end;

procedure TFmCondGer2.Atual3Click(Sender: TObject);
begin
  DesfazerBoletos(istAtual);
end;

procedure TFmCondGer2.BitBtn5Click(Sender: TObject);
var
  Periodo: Integer;
begin
  Periodo := UBloqGerl_Jan.LocalizarPeriodoAtual(VAR_ModBloq_CliInt,
               VAR_ModBloq_TabPrvA, True);
  //
  if Periodo <> 0 then
    LocPeriodo(Periodo, Periodo);
end;

procedure TFmCondGer2.BitBtn8Click(Sender: TObject);
begin
  ConfiguraJanela(4);
end;

procedure TFmCondGer2.BloquetosSelecionados1Click(Sender: TObject);
begin
  AlteraVencimentoPreBol(istSelecionados, False, TDBGrid(DBGradeBlo));
end;

procedure TFmCondGer2.BtProvProxClick(Sender: TObject);
begin
  ConfiguraJanela(2);
end;

procedure TFmCondGer2.BtArrecadaClick(Sender: TObject);
begin
  if PCArrecada.ActivePageIndex = 0 then
    MyObjects.MostraPopUpDeBotao(PMArrecada, BtArrecada)
  else
    MyObjects.MostraPopUpDeBotao(PMArreFut, BtArrecada);
end;

procedure TFmCondGer2.BtArrProxClick(Sender: TObject);
begin
  ConfiguraJanela(4);
end;

procedure TFmCondGer2.BtBoletoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBoleto, BtBoleto);
end;

procedure TFmCondGer2.BtDesfazOrdemBloqClick(Sender: TObject);
begin
  UnDmkDAC_PF.DesfazOrdenacaoDmkDBGrid(TDmkDBGrid(DBGradeBlo), 'Boleto');
end;

procedure TFmCondGer2.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmCondGer2.BtLaiAntClick(Sender: TObject);
begin
  ConfiguraJanela(1);
end;

procedure TFmCondGer2.BtLeiProxClick(Sender: TObject);
begin
  ConfiguraJanela(3);
end;

procedure TFmCondGer2.BtLeituraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLeitura, BtLeitura);
end;

procedure TFmCondGer2.BtNenhumMensClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGradeBlo), False);
end;

procedure TFmCondGer2.BtOrcamentoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOrcamento, BtOrcamento);
end;

procedure TFmCondGer2.BtOrcProxClick(Sender: TObject);
begin
  ConfiguraJanela(1);
end;

procedure TFmCondGer2.BtPEBVoltarClick(Sender: TObject);
begin
  ConfiguraJanela(5);
end;

procedure TFmCondGer2.BtPreAntClick(Sender: TObject);
begin
  ConfiguraJanela(3);
end;

procedure TFmCondGer2.BtPrebolClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPrebol0, BtPrebol);
end;

procedure TFmCondGer2.BtProtocoloClick(Sender: TObject);
begin
  ConfiguraJanela(0, istVisProtEnt);
end;

procedure TFmCondGer2.BtProvAntClick(Sender: TObject);
begin
  ConfiguraJanela(0);
end;

procedure TFmCondGer2.BtProvisaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProvisao, BtProvisao);
end;

procedure TFmCondGer2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPrevCodigo.Value;
  //
  if TFmCondGer2(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmCondGer2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CriaOForm;
end;

procedure TFmCondGer2.FormActivate(Sender: TObject);
begin
  if TFmCondGer2(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmCondGer2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], ' ' + Geral.FFN(VAR_ModBloq_CliInt, 3) + ' - ' +
    DmodG.QrEmpresasNOMEFILIAL.Value, False, taLeftJustify, 2, 10, 20);
end;

procedure TFmCondGer2.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  //
  ConfiguraJanela(-1);
end;

procedure TFmCondGer2.GerarAtual1Click(Sender: TObject);
begin
  GeraBloq(istAtual);
end;

procedure TFmCondGer2.Gerarselecionados1Click(Sender: TObject);
begin
  GeraBloq(istSelecionados);
end;

procedure TFmCondGer2.Gerartodosabertos1Click(Sender: TObject);
begin
  GeraBloq(istTodos);
end;

procedure TFmCondGer2.GeraBloq(Tipo: TSelType);

  function GeraBloqAtual: Double;
  var
    Protocolo, Protocolo2, ProtocoloCR: Integer;
    DataDef, DataSel: TDateTime;
    DiaDef: Word;
    Continua: Boolean;
  begin
    Continua := True;
    DiaDef   := UBloqCond.ObtemDiaVenctoDeApto(VAR_ModBloq_CliInt, QrBoletosApto.Value);
    //
    if DiaDef = 0 then
    begin
      Geral.MB_Aviso('O dia do vencimento n�o foi definido no cadastro do condom�nio!');
      Continua := False;
      Exit;
    end;
    if (Continua) and (DiaDef <> 0) then
    begin
      DataSel := dmkPF.UltimoDiaDoPeriodo_Date(QrPrevPeriodo.Value) + DiaDef;
      //
      Result := UBloqGerl.GeraBloquetoAtual(QrPrevCodigo.Value,
                  QrPrevPeriodo.Value, VAR_ModBloq_EntCliInt, DataSel, True,
                  VAR_ModBloq_TabLctA, VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA,
                  QrBoletos, QrBoletosIts, Dmod.MyDB, Protocolo, Protocolo2,
                  ProtocoloCR);
    end;
  end;

var
  Aba, i, Periodo: Integer;
begin
  PB1.Position := 0;
  //
  case Tipo of
    istSelecionados:
    begin
      if DBGradePreBlo.SelectedRows.Count > 1 then
      begin
        PB1.Max := DBGradePreBlo.SelectedRows.Count;
        try
          DBGradePreBlo.Enabled := False;
          //
          QrBoletos.DisableControls;
          //
          with DBGradePreBlo.DataSource.DataSet do
          for i:= 0 to DBGradePreBlo.SelectedRows.Count-1 do
          begin
            GotoBookmark(pointer(DBGradePreBlo.SelectedRows.Items[i]));

            if (GeraBloqAtual <> 0) then
            begin
              PB1.Position := PB1.Position + 1;
              PB1.Update;
              Application.ProcessMessages;
            end else
            begin
              Geral.MB_Erro('Falha ao gerar boleto(s)!');
              Exit;
            end;
          end;
        finally
          QrBoletos.EnableControls;
          //
          DBGradePreBlo.Enabled := True;
        end;
        PB1.Position := 0;
      end else
        GeraBloqAtual;
    end;
    istTodos:
    begin
      PB1.Max := QrBoletos.RecordCount;
      //
      try
        DBGradePreBlo.Enabled := False;
        //
        QrBoletos.DisableControls;
        //
        QrBoletos.First;
        while not QrBoletos.Eof do
        begin
          if (GeraBloqAtual <> 0) then
          begin
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
          end else
          begin
            Geral.MB_Erro('Falha ao gerar boleto(s)!');
            Exit;
          end;
          QrBoletos.Next;
        end;
      finally
        QrBoletos.EnableControls;
        //
        DBGradePreBlo.Enabled := True;
      end;
    end;
  end;
  ConfiguraJanela(4);
  //
  if Tipo = istTodos then
  begin
    if Geral.MB_Pergunta('Deseja travar o per�odo atual?') = ID_YES then
    begin
      Periodo := UBloqGerl.EncerraPeriodo(Dmod.QrUpd, QrPrev, VAR_ModBloq_TabPrvA);
      //
      if Periodo <> 0 then
      begin
        LocPeriodo(Periodo, Periodo);
      end;
    end;
  end;
  PB1.Position := 0;
end;

function TFmCondGer2.ImprimeBloq(Quais: TselType; Como: TfrxImpComo;
  Arquivo: String; Filtro: TfrxCustomExportFilter; Escolher: Boolean): TfrxReport;
var
  TabLctA, TabLctB, TabLctD: String;
  Grade: TDBGrid;
begin
  {$IFDEF DEFINE_VARLCT}
    TabLctA := VAR_ModBloq_TabLctA;
    TabLctB := VAR_ModBloq_TabLctA;
    TabLctD := VAR_ModBloq_TabLctA;
  {$ELSE}
    TabLctA := VAR_LCT;
    TabLctB := '';
    TabLctD := '';
  {$ENDIF}
  //
  case PCGeral.ActivePageIndex of
    4: Grade := TDBGrid(DBGradePreBlo);
    5: Grade := TDBGrid(DBGradeBlo);
    else Grade := nil;
  end;
  //
  UBloqCond.ReopenPRI(QrPRI, QrPrevCodigo.Value, 0, VAR_ModBloq_TabPriA);
  //
  Result := UBloqCond.ImprimeBloqueto(CkZerado.Checked, Escolher, TabLctA,
              TabLctB, TabLctD, VAR_ModBloq_CliInt, VAR_ModBloq_EntCliInt,
              QrPrev, QrBoletos, QrBoletosIts, frxDsPRI, frxDsBoletos,
              frxDsBoletosIts, Quais, Como, Arquivo, Filtro, Grade);
end;

procedure TFmCondGer2.ImprimirboletosAtualescolhermodelo1Click(Sender: TObject);
begin
  ImprimeBloq(istAtual, ficMostra, '', nil, True);
end;

procedure TFmCondGer2.ImprimirboletosSelecionados1Click(Sender: TObject);
begin
  ImprimeBloq(istSelecionados, ficMostra, '', nil, False);
end;

procedure TFmCondGer2.ImprimirTodosboletos1Click(Sender: TObject);
begin
  ImprimeBloq(istTodos, ficMostra, '', nil, False);
end;

procedure TFmCondGer2.Incluiarrecadaofutura1Click(Sender: TObject);
begin
  UBloqCond_Jan.MostraFmCondGerArreFut(stIns, VAR_ModBloq_CliInt, QrArreFutI);
  UBloqCond.ReopenArreFut(QrArreFutI, VAR_ModBloq_CliInt, 0, 0);
end;

procedure TFmCondGer2.Incluiintensbasedearrecadao1Click(Sender: TObject);
begin
  UBloqCond_Jan.VerificaArreFut(QrArreFutA, QrArreFutI, DsArreFutA,
    VAR_ModBloq_CliInt, QrPrevCodigo.Value, QrPrevPeriodo.Value, VAR_ModBloq_TabAriA);
  UBloqCond_Jan.IncluiIntensBaseDeArrecadacao(False, VAR_ModBloq_TabAriA,
    VAR_ModBloq_TabCnsA, DmodG.QrEmpresasNOMEFILIAL.Value, VAR_ModBloq_CliInt,
    VAR_ModBloq_EntCliInt, QrPrevCodigo.Value, QrPrevPeriodo.Value,
    QrPrevGastos.Value, PB1, PB2, TFmCondGer2(Self));
  UBloqCond.ReopenArre(QrArre, QrSumARRE, QrPrevCodigo.Value, 0,
    0, VAR_ModBloq_TabAriA);
end;

procedure TFmCondGer2.Incluiitenspragendados1Click(Sender: TObject);
begin
  UBloqCond_Jan.VerificaArreFut(QrArreFutA, QrArreFutI, DsArreFutA,
    VAR_ModBloq_CliInt, QrPrevCodigo.Value, QrPrevPeriodo.Value, VAR_ModBloq_TabAriA);
  UBloqCond.ReopenArre(QrArre, QrSumARRE, QrPrevCodigo.Value, 0,
    0, VAR_ModBloq_TabAriA);
end;

procedure TFmCondGer2.Incluileituras1Click(Sender: TObject);
begin
  UBloqCond_Jan.MostraCondGerLei1(VAR_ModBloq_CliInt, QrPrevPeriodo.Value,
    DmodG.QrEmpresasNOMEFILIAL.Value, VAR_ModBloq_TabCnsA);
  UBloqCond.ReopenQrCons(QrCons, QrSumCT, VAR_ModBloq_TabCnsA, VAR_ModBloq_CliInt,
    QrPrevPeriodo.Value, QrConsCodigo.Value);
end;

procedure TFmCondGer2.Incluinovooramento1Click(Sender: TObject);
var
  Periodo: Integer;
begin
  Periodo := UBloqCond_Jan.MostraCondGerNew(stIns, QrPrev, VAR_ModBloq_CliInt,
               VAR_ModBloq_TabPrvA);
  //
  if Periodo <> 0 then
    LocPeriodo(Periodo, Periodo);
end;

procedure TFmCondGer2.ItemAtual2Click(Sender: TObject);
begin
  ExcluiItemProvisao(istAtual, True);
end;

procedure TFmCondGer2.Itematual3Click(Sender: TObject);
var
  Prox: Integer;
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do item "'+ QrAriTexto.Value + '"?' +
    sLineBreak + 'ATEN��O: Arrecada��es que j� possuam n�mero de boleto n�o ser�o exclu�das!') = ID_YES then
  begin
    UBloqCond.ExcluiItesArrecadacao(VAR_ModBloq_TabAriA, VAR_ModBloq_TabLctA,
      QrAriControle.Value, QrAriLancto.Value, QrAriBoleto.Value);
    //
    Prox := UMyMod.ProximoRegistro(QrAri, 'Controle', QrAriControle.Value);
    //
    CalculaTotalARIEReabreArreEARI(QrAriApto.Value, QrAriPropriet.Value, Prox);
  end;
end;

procedure TFmCondGer2.ItemAtualdoprbloqueto1Click(Sender: TObject);
begin
  ExcluiItemPreBloqueto(istAtual);
end;

procedure TFmCondGer2.Itemselecionado1Click(Sender: TObject);
begin
  UBloqCond_Jan.MostraFmCondGerArreUni(stUpd, QrAri, QrPrevCodigo.Value,
    VAR_ModBloq_CliInt, VAR_ModBloq_EntCliInt, QrPrevPERIODO_TXT.Value,
    VAR_ModBloq_TabLctA, VAR_ModBloq_TabAriA);
  CalculaTotalARIEReabreArreEARI(0, 0, 0);
end;

procedure TFmCondGer2.ItemSelecionado3Click(Sender: TObject);
begin
  ExcluiItemProvisao(istAtual, False);
end;

procedure TFmCondGer2.ItensSelecionados1Click(Sender: TObject);
begin
  ExcluiItemProvisao(istSelecionados, False);
end;

procedure TFmCondGer2.ItensSelecionados2Click(Sender: TObject);
begin
  ExcluiItemProvisao(istSelecionados, True);
end;

procedure TFmCondGer2.QrArreAfterScroll(DataSet: TDataSet);
begin
  UBloqCond.ReopenArreBol(QrArreBol, QrPrevCodigo.Value, QrArreApto.Value,
    QrArrePropriet.Value, 0, VAR_ModBloq_TabAriA);
end;

procedure TFmCondGer2.QrArreBeforeClose(DataSet: TDataSet);
begin
  QrArreBol.Close;
end;

procedure TFmCondGer2.QrArreBolAfterScroll(DataSet: TDataSet);
begin
  UBloqCond.ReopenARI(QrAri, QrPrevCodigo.Value, 0, QrArreApto.Value,
    QrArrePropriet.Value, QrArreBolBoleto.Value, VAR_ModBloq_TabAriA);
end;

procedure TFmCondGer2.QrArreBolBeforeClose(DataSet: TDataSet);
begin
  QrAri.Close;
end;

procedure TFmCondGer2.QrArreFutACalcFields(DataSet: TDataSet);
begin
  QrArreFutAPERIODO_TXT.Value := dmkPF.PeriodoToMensal(QrArreFutAPeriodo.Value);
end;

procedure TFmCondGer2.QrArreFutICalcFields(DataSet: TDataSet);
begin
  QrArreFutIPERIODO_TXT.Value := dmkPF.PeriodoToMensal(QrArreFutIPeriodo.Value);
end;

procedure TFmCondGer2.QrBoletosAfterScroll(DataSet: TDataSet);
begin
  UBloqCond.ReopenBoletosIts(QrBoletosIts, VAR_ModBloq_TabAriA,
    VAR_ModBloq_TabCnsA, QrBoletosBoleto.Value, QrPrevCodigo.Value,
    QrPrevPeriodo.Value, QrBoletosApto.Value, QrBoletosCNAB_Cfg.Value);
end;

procedure TFmCondGer2.QrBoletosBeforeClose(DataSet: TDataSet);
begin
  QrBoletosIts.Close;
end;

procedure TFmCondGer2.QrBoletosCalcFields(DataSet: TDataSet);
begin
  QrBoletosSUB_TOT.Value := QrBoletosSUB_ARR.Value + QrBoletosSUB_LEI.Value;
  //
  if QrBoletosBoleto.Value = 0 then
    QrBoletosBLOQUETO.Value := -1
  else
    QrBoletosBLOQUETO.Value := QrBoletosBoleto.Value;
  //
  QrBoletosVENCTO_TXT.Value := Geral.FDT(QrBoletosVencto.Value, 3);
  //
  if ((Trim(QrBoletosUSERNAME.Value) <> '') and
    (Trim(QrBoletosPASSWORD.Value) <> ''))
  then
    QrBoletosPWD_WEB.Value := 'Login: ' + QrBoletosUSERNAME.Value + '   Senha: ' + QrBoletosPASSWORD.Value
  else
    QrBoletosPWD_WEB.Value := '';
  //
  QrBoletosJuridico_TXT.Value    := dmkPF.DefineJuricoSigla(QrBoletosJuridico.Value);
  QrBoletosJuridico_DESCRI.Value := dmkPF.DefineJuricoTexto(QrBoletosJuridico.Value);
end;

procedure TFmCondGer2.QrBoletosItsCalcFields(DataSet: TDataSet);
begin
  QrBoletosItsTEXTO_IMP.Value := QrBoletosItsTEXTO.Value +
                                 UBloqCond.TextoExplicativoItemBoleto(
                                   QrBoletosItsTipo.Value,
                                   QrBoletosItsCasas.Value,
                                   QrBoletosItsMedAnt.Value,
                                   QrBoletosItsMedAtu.Value,
                                   QrBoletosItsConsumo.Value,
                                   QrBoletosItsUnidFat.Value,
                                   QrBoletosItsUnidLei.Value,
                                   QrBoletosItsUnidImp.Value,
                                   QrBoletosItsGeraTyp.Value,
                                   QrBoletosItsCasRat.Value,
                                   QrBoletosItsNaoImpLei.Value,
                                   QrBoletosItsGeraFat.Value);
  //
  QrBoletosItsVENCTO_TXT.Value := Geral.FDT(QrBoletosItsVencto.Value, 3);
end;

procedure TFmCondGer2.QrCNSCalcFields(DataSet: TDataSet);
begin
  if QrCNSGeraTyp.Value = 0 then
  begin
    QrCNSCONSUMO2.Value     := QrCNSConsumo.Value * QrCNSUnidFat.Value;
    QrCNSConsumo2_TXT.Value := Geral.FFT(QrCNSCONSUMO2.Value,
                                 QrCNSCasas.Value, siNegativo) + ' ' +
                                 QrCNSUnidImp.Value;
    QrCNSConsumo1_TXT.Value := Geral.FFT(QrCNSConsumo.Value,
                                 QrCNSCasas.Value, siNegativo) + ' ' +
                                 QrCNSUnidLei.Value;
  end else begin
    QrCNSCONSUMO2.Value     := QrCNSGeraFat.Value;
    QrCNSConsumo2_TXT.Value := Geral.FFT(QrCNSCONSUMO2.Value,
                                 QrCNSCasRat.Value, siNegativo) + ' ' +
                                 QrCNSUnidImp.Value;
    QrCNSConsumo1_TXT.Value := Geral.FFT(QrCNSConsumo.Value,
                                 QrCNSCasas.Value, siNegativo) + ' ' +
                                 QrCNSUnidLei.Value;
  end;
end;

procedure TFmCondGer2.QrConsAfterScroll(DataSet: TDataSet);
var
  FmtTxT: String;
begin
  FmtTxT := dmkPF.FormataCasas(QrConsCasas.Value);
  //
  QrConsTOT_UNID1.DisplayFormat := FmtTxt;
  QrConsTOT_UNID2.DisplayFormat := FmtTxt;
  //
  UBloqCond.ReopenCNS(QrCNS, VAR_ModBloq_CliInt, QrPrevPeriodo.Value,
    QrConsCodigo.Value, 0, VAR_ModBloq_TabCnsA);
  UBloqCond.ReopenQrSumCP(QrSumCP, VAR_ModBloq_TabCnsA, VAR_ModBloq_CliInt,
    QrPrevPeriodo.Value, QrConsCodigo.Value);
end;

procedure TFmCondGer2.QrConsBeforeClose(DataSet: TDataSet);
begin
  QrCNS.Close;
  QrSumCP.Close;
end;

procedure TFmCondGer2.QrConsCalcFields(DataSet: TDataSet);
begin
  QrConsTOT_UNID1.Value     := QrSumCPCONSUMO.Value;
  QrConsTOT_UNID1_TXT.Value := Geral.FFT(QrSumCPCONSUMO.Value, QrConsCasas.Value, siNegativo);
  QrConsTOT_UNID2.Value     := QrSumCPCONSUMO.Value * QrConsUnidFat.Value;
  QrConsTOT_UNID2_TXT.Value := Geral.FFT(QrConsTOT_UNID2.Value, 3, siNegativo);
  QrConsTOT_VALOR.Value     := QrSumCPVALOR.Value;
end;

procedure TFmCondGer2.QrPrevAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCondGer2.QrPrevAfterScroll(DataSet: TDataSet);
begin
  ConfiguraJanela(PCGeral.ActivePageIndex);
  //
  UBloqCond.ReopenQrSumBol(QrSumBol, VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA,
    QrPrevCond.Value, QrPrevPeriodo.Value, QrPrevCodigo.Value, False);
  //
  UBloqCond.ReopenQrSumBol(QrSumPre, VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA,
    QrPrevCond.Value, QrPrevPeriodo.Value, QrPrevCodigo.Value, True);
end;

procedure TFmCondGer2.QrPrevBeforeClose(DataSet: TDataSet);
begin
  QrPRI.Close;
  QrCons.Close;
  QrSumCT.Close;
  QrArre.Close;
  QrSumARRE.Close;
  QrBoletos.Close;
  QrPrevModBol.Close;
  QrSumBol.Close;
  QrSumPre.Close;
end;

procedure TFmCondGer2.QrPrevBeforeOpen(DataSet: TDataSet);
begin
  QrPrevCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCondGer2.QrPrevCalcFields(DataSet: TDataSet);
var
  Valor: Double;
begin
  if QrPrevEncerrado.Value = 0 then
  begin
    QrPrevNOME_ENCERRADO.Value := 'ABERTO';
    DBText12.Font.Color := clGreen;
  end else begin
    QrPrevNOME_ENCERRADO.Value := 'ENCERRADO';
    DBText12.Font.Color := clRed;
  end;
  QrPrevNOMEMODELBLOQ.Value := UBloqCond.LetraModelosBloq(QrPrevModelBloq.Value);
  QrPrevPERIODO_TXT.Value   := dmkPF.MesEAnoDoPeriodoLongo(QrPrevPeriodo.Value);
  //
  Valor := 0;
  while not QrSumBol.Eof do
  begin
    Valor := Valor + QrSumBolVALOR.Value;
    QrSumBol.Next;
  end;
  QrPrevTOT_BOL.Value := Valor;
  //
  Valor := 0;
  while not QrSumPre.Eof do
  begin
    Valor := Valor + QrSumPreVALOR.Value;
    QrSumPre.Next;
  end;
  QrPrevTOT_PRE.Value := Valor;
end;

procedure TFmCondGer2.QrPrevModBolCalcFields(DataSet: TDataSet);
begin
  QrPrevModBolNOME_MODELBLOQ.Value := UBloqCond.LetraModelosBloq(QrPrevModBolModelBloq.Value);
end;

procedure TFmCondGer2.ExcluiLeitura(Tipo: TselType);
var
  i, Prox: integer;
begin
  i := 0;
  if Tipo <> istTodos then
  begin
    case Tipo of
      istAtual: i := 1;
      istSelecionados:
      begin
        i := DBGCNS.SelectedRows.Count;
        if i = 0 then i := 1;
      end;
    end;
    if i = 1 then
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o da leitura selecionada?' + sLineBreak +
        'ATEN��O: Leituras que j� possuam n�mero de boleto n�o ser�o exclu�das!') = ID_YES then
      begin
        UBloqCond.ExcluiLeituraAtual(QrCNSControle.Value, QrCNSLancto.Value,
          QrCNSBoleto.Value, VAR_ModBloq_TabCnsA, VAR_ModBloq_TabLctA);
      end;
    end else begin
      if Geral.MB_Pergunta('Confirma a exclus�o das ' + Geral.FF0(DBGCNS.SelectedRows.Count)
        + ' leituras selecionadas?' + sLineBreak +
        'ATEN��O: Leituras que j� possuam n�mero de boleto n�o ser�o exclu�das!') = ID_YES then
      begin
        with DBGCNS.DataSource.DataSet do
        begin
          for i:= 0 to DBGCNS.SelectedRows.Count - 1 do
          begin
            GotoBookmark(pointer(DBGCNS.SelectedRows.Items[i]));
            //
            UBloqCond.ExcluiLeituraAtual(QrCNSControle.Value, QrCNSLancto.Value,
              QrCNSBoleto.Value, VAR_ModBloq_TabCnsA, VAR_ModBloq_TabLctA);
          end;
        end;
      end;
    end;
  end else
  begin
    QrCNS.First;
    while not QrCNS.Eof do
    begin
      UBloqCond.ExcluiLeituraAtual(QrCNSControle.Value, QrCNSLancto.Value,
        QrCNSBoleto.Value, VAR_ModBloq_TabCnsA, VAR_ModBloq_TabLctA);
      //
      QrCNS.Next;
    end;
  end;
  Prox := UMyMod.ProximoRegistro(QrCNS, 'Controle', 0);
  //
  UBloqCond.ReopenQrCons(QrCons, QrSumCT, VAR_ModBloq_TabCnsA, VAR_ModBloq_CliInt,
    QrPrevPeriodo.Value, QrConsCodigo.Value);
end;

procedure TFmCondGer2.Excluileituraatual2Click(Sender: TObject);
begin
  ExcluiLeitura(istAtual);
end;

procedure TFmCondGer2.Excluileiturasselecionadas1Click(Sender: TObject);
begin
  ExcluiLeitura(istSelecionados);
end;

procedure TFmCondGer2.Excluitodasleituras1Click(Sender: TObject);
begin
  ExcluiLeitura(istTodos);
end;

end.

