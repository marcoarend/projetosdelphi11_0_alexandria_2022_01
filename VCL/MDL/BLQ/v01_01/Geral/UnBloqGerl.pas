unit UnBloqGerl;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, ModuleGeral, dmkImage, Forms,
  Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids, AdvToolBar, DB,
  UnDmkProcFunc, ExtCtrls, dmkDBGrid, Math, Dialogs, UnDmkEnums, Classes,
  dmkDBGridZTO, frxDBSet, frxClass, UnInternalConsts;

type
  TModoVisualizacaoBloquetos = (istVisNenhum, istVisMensal, istVisPesquisa, istVisProtEnt, istArrPeri);
  TUnBloqGerl = class(TObject)
  private
  public
    function  EncerraPeriodo(QueryUpd, QueryPrev: TmySQLQuery; TabelaPrvA: String): Integer;
    function  DefineVarsBloGeren(var EntCliInt, CliInt: Integer; var TabLctA,
              TabLctB, TabLctD, TabAriA, TabCnsA, TabPriA, TabPrvA: String): Boolean;
    function  PeriodoExiste(Periodo: Integer; Tabela: String;
              DataBase: TmySQLDatabase): Boolean;
    function  GeraBloquetoAtual(Codigo, CNAB_Cfg, Entidade, CliInt: Integer;
              Vencimento: TDate; AtualizaAreeIts: Boolean; TabLctA,
              TabAriA, TabPrvA: String; DataBase: TmySQLDatabase; var Protocolo,
              Protocolo2, ProtocoloCR: Integer): Double;
    function  GeraBloquetoAtual2(Codigo, Periodo, EntCliInt: Integer;
              Vencimento: TDate; AtualizaAreeIts: Boolean; TabLctA, TabAriA,
              TabCnsA: String; QueryBoletos, QueryBoletosIts: TmySQLQuery;
              DataBase: TmySQLDatabase; var Protocolo, Protocolo2,
              ProtocoloCR: Integer): Double;
    function  ValidaVencimentoPeriodo(const DataBase: TmySQLDatabase;
              const QueryBloOpcoes: TmySQLQuery; const Periodo: Integer;
              const Data: TDateTime; var MesValido: Integer): Boolean;
    function  VerificaSeLoteCRFoiGerado(Database: TmySQLDatabase;
              ProtocoPakCR: Integer): Boolean;
    function  LiberaImpressaoBoletosCR(DataBase: TmySQLDatabase;
              QueryBoletos: TmySQLQuery): Boolean;
    procedure ReopenCNAB_Cfg(Query: TmySQLQuery; Database: TmySQLDatabase;
              Cliente: Integer; Codigo: Integer = 0);
  end;

var
  UBloqGerl: TUnBloqGerl;
  //
  VAR_ModBloq_EntCliInt, VAR_ModBloq_CliInt, VAR_ModBloq_Peri,
  VAR_ModBloq_FatID, VAR_ModBloq_Lancto: Integer;
  VAR_ModBloq_TabAriA, VAR_ModBloq_TabPrvA, VAR_ModBloq_TabCnsA,
  VAR_ModBloq_TabPriA, VAR_ModBloq_TabLctA, VAR_ModBloq_TabLctB,
  VAR_ModBloq_TabLctD: String;
  VAR_ModBloq_FatNum: Double;

const
  FMdlBloq = 'BLOQ';
  FArre_FatID = VAR_FATID_0600;
  FCons_FatID = VAR_FATID_0601;


implementation

uses
{$IfNDef SemProtocolo} Protocolo, {$EndIf}
{$IfNDef NO_CNAB} UnBco_Rem, UnBancos, {$EndIf}
{$IfNDef NO_FINANCEIRO} UnFinanceiro, {$EndIf}
{$IfDef TEM_UH} UnBloqCond, {$EndIf}
DmkDAC_PF, UnMyObjects, MyDBCheck;

{ TUnBloqGerl }

function TUnBloqGerl.ValidaVencimentoPeriodo(const DataBase: TmySQLDatabase;
  const QueryBloOpcoes: TmySQLQuery; const Periodo: Integer;
  const Data: TDateTime; var MesValido: Integer): Boolean;
var
  //Mes: String;
  DataPeri, Peri: Integer;
begin
  {$IfDef TEM_UH}
  Peri := Periodo + 1; //� fixo
  {$Else}
  UMyMod.AbreQuery(QueryBloOpcoes, DataBase);
  //
  case QueryBloOpcoes.FieldByName('PerGerBol').AsInteger of
    0: //Anterior
      Peri := Periodo - 1;
    1: //Atual
      Peri := Periodo;
    2: //Pr�ximo
      Peri := Periodo + 1;
    else
      Peri := 0;
  end;
  {$EndIf}
  DataPeri  := Geral.Periodo2000(Data);
  MesValido := dmkPF.MesDoPeriodo(Peri);
  //
  if DataPeri <> Peri then
    Result := True //Per�odo inv�lido
  else
    Result := False; //Per�odo v�lido
end;

function TUnBloqGerl.VerificaSeLoteCRFoiGerado(Database: TmySQLDatabase;
  ProtocoPakCR: Integer): Boolean;
var
  Query: TmySQLQuery;
begin
  Result := False;
  Query  := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
      'SELECT * ',
      'FROM protocopak ',
      'WHERE Controle=' + Geral.FF0(ProtocoPakCR),
      'AND SeqArq <> 0 ',
      '']);
    if Query.RecordCount > 0 then
      Result := True;
  finally
    Query.Free;
  end;
end;

procedure TUnBloqGerl.ReopenCNAB_Cfg(Query: TmySQLQuery;
  Database: TmySQLDatabase; Cliente: Integer; Codigo: Integer = 0);
var
  SQL: String;
begin
  if Codigo = 0 then
    SQL := 'AND (Cedente=' + Geral.FF0(Cliente) + ' OR SacadAvali=' + Geral.FF0(Cliente) + ')'
  else
    SQL := 'AND Codigo=' + Geral.FF0(Codigo);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
    'SELECT Codigo, Nome ',
    'FROM cnab_cfg ',
    'WHERE Ativo = 1 ',
    SQL,
    'ORDER BY Nome ',
    '']);
end;

function TUnBloqGerl.EncerraPeriodo(QueryUpd, QueryPrev: TmySQLQuery;
  TabelaPrvA: String): Integer;
var
  Encerrado, Codigo, Periodo: Integer;
begin
  Result := 0;
  //
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Encerrado := QueryPrev.FieldByName('Encerrado').AsInteger;
  Codigo    := QueryPrev.FieldByName('Codigo').AsInteger;
  Periodo   := QueryPrev.FieldByName('Periodo').AsInteger;
  //
  if MyObjects.FIC(Codigo = 0, nil, 'ID do per�odo n�o definido!') then Exit;
  if MyObjects.FIC(Periodo = 0, nil, 'Per�odo n�o definido!') then Exit;
  //
  if Encerrado = 1 then
    Encerrado := 0
  else
    Encerrado := 1;
  //
  if UMyMod.SQLInsUpd(QueryUpd, stUpd, TabelaPrvA, False,
    ['Encerrado'], ['Codigo'], [Encerrado], [Codigo], True) then
  begin
    Result := Periodo;
  end;
end;

function TUnBloqGerl.DefineVarsBloGeren(var EntCliInt, CliInt: Integer; var TabLctA,
  TabLctB, TabLctD, TabAriA, TabCnsA, TabPriA, TabPrvA: String): Boolean;
var
  Enti, CliIn: Integer;
  {$IFDEF DEFINE_VARLCT}
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
  TbLctA, TbLctB, TbLctD: String;
  SelecionouEmpresa, Validou: Boolean;
begin
  Result    := False;
  Enti      := 0;
  CliIn     := 0;
  EntCliInt := 0;
  CliInt    := 0;
  TabLctA   := '';
  TbLctB    := '';
  TbLctD    := '';
  //
  begin
// Usa no lct antigo?
    SelecionouEmpresa := True;
    Enti  := -11;
    CliIn := 1;
  end;
{$IFDEF DEFINE_VARLCT}
{$IfNDef NO_FINANCEIRO}
  SelecionouEmpresa := DModG.SelecionaEmpresa(sllLivre);
  if SelecionouEmpresa then
  begin
    Enti  := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    CliIn := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
  end else
    Exit;
{$ENDIF}
{$ENDIF}
  //
  if (Enti <> 0) and (CliIn <> 0) then
  begin
    {$IFDEF DEFINE_VARLCT}
    DModG.Def_EM_ABD(TMeuDB, Enti, CliIn, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
    {$ELSE}
    TbLctA := VAR_LCT;
    {$ENDIF}
    //
    EntCliInt := Enti;
    CliInt    := CliIn;
    TabLctA   := TbLctA;
    TabLctB   := TbLctB;
    TabLctD   := TbLctD;
    //
    {$IfDef TEM_UH}
    TabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CliInt);
    TabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, CliInt);
    TabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, CliInt);
    TabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, CliInt);
    //
    Validou := (TabAriA <> '') and (TabCnsA <> '') and (TabPriA <> '') and
               (TabPrvA <> '');
    {$Else}
    TabAriA := 'arreits';
    TabCnsA := ''; //N�o tem
    TabPriA := ''; //N�o tem
    TabPrvA := 'prev';
    //
    Validou := (TabAriA <> '') and (TabPrvA <> '');
    {$EndIf}
    //
    Result := (EntCliInt <> 0) and (CliInt <> 0) and (TabLctA <> '') and
              (TabLctB <> '') and (TabLctD <> '') and Validou;
  end;
end;

function TUnBloqGerl.GeraBloquetoAtual(Codigo, CNAB_Cfg, Entidade,
  CliInt: Integer; Vencimento: TDate; AtualizaAreeIts: Boolean; TabLctA,
  TabAriA, TabPrvA: String; DataBase: TmySQLDatabase; var Protocolo, Protocolo2,
  ProtocoloCR: Integer): Double;

  function NaoContinuaPelaCarteira(Carteira, TipoDoc, Ativo: Integer): Boolean;
  var
    NumCartEmiss: String;
  begin
    Result := True;
    //
    NumCartEmiss := Geral.FF0(Carteira);
    //
    if Carteira = 0 then
    begin
      Geral.MB_Aviso('A carteira de emiss�o dos boletos n�o foi definida no cadastro do condom�nio! A��o abortada!');
      Exit;
    end;
    if TipoDoc <> 5 then
    begin
      if Geral.MB_Pergunta('A carteira de emiss�o n� ' + NumCartEmiss +
        ' n�o est� configurada como "Tipo de documento" = bloqueto!' + sLineBreak +
        'Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
    if Ativo <> 1 then
    begin
      if Geral.MB_Pergunta('A carteira de emiss�o de bloquetos (n� ' +
        NumCartEmiss + ') n�o est� configurada como ativa!' + sLineBreak +
        'Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
    Result := False;
  end;

  function InsereLancto(Vencto: TDateTime; Boleto, Valor, JurosPerc,
    MultaPerc: Double; Empresa, Entidade, Periodo, CartEmiss, Genero,
    Controle: Integer; Descri: String; Avulso: Boolean): Integer;
  {$IfNDef NO_FINANCEIRO}
  var
    Res: Boolean;
    Qry: TmySQLQuery;
  begin
    Result := 0;
    Qry    := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      UFinanceiro.LancamentoDefaultVARS;
      //
      FLAN_Data       := Geral.FDT(Date, 1);
      FLAN_Tipo       := 2;
      FLAN_Documento  := Boleto;
      FLAN_Credito    := Valor;
      FLAN_MoraDia    := JurosPerc;
      FLAN_Multa      := MultaPerc;
      FLAN_Carteira   := CartEmiss;
      FLAN_Genero     := Genero;
      FLAN_Cliente    := Entidade;
      FLAN_CliInt     := Empresa;
      FLAN_Vencimento := Geral.FDT(Vencto, 1);
      FLAN_Mez        := dmkPF.PeriodoToAnoMes(Periodo);
      FLAN_FatID      := FArre_FatID;
      FLAN_FatNum     := Trunc(Boleto);
      FLAN_FatParcela := 1;
      //
      if Avulso then
        FLAN_Descricao := Descri
      else
        FLAN_Descricao := Descri + ' - ' + dmkPF.MesEAnoDoPeriodo(Periodo);
      //
      FLAN_Controle := UMyMod.BuscaEmLivreY(DataBase, 'Livres', 'Controle',
                         TabLctA, LAN_CTOS, 'Controle');
      //
      {$IFDEF DEFINE_VARLCT}
        Res := UFinanceiro.InsereLancamento(TabLctA);
      {$ELSE}
        Res := UFinanceiro.InsereLancamento;
      {$ENDIF}
      //
      if Res then
      begin
        if not Avulso then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DataBase, [
            'UPDATE ' + TabAriA + ' SET ',
            'Lancto=' + Geral.FF0(FLAN_Controle),
            'WHERE Controle=' + Geral.FF0(Controle),
            '']);
        end;
        Result := FLAN_Controle;
      end;
    finally
      Qry.Free;
    end;
  {$Else}
  begin
    dmkPF.InfoSemModulo(mdlappFinanceiro);
  {$EndIf}
  end;

var
  NumBloq: Double;
  NossoNumero, NossoNumero_Rem: String;
  QryCNAB_Cfg, QryUpd, QryAux: TmySQLQuery;
begin
  Result      := 0;
  QryCNAB_Cfg := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd      := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryAux      := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    NumBloq := UBco_Rem.ObtemProximoNossoNumero(CNAB_Cfg, False);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QryCNAB_Cfg, DataBase, [
      'SELECT bco.Nome NOMEBANCO, bco.DVB,  cfg.*, ',
      'IF(cfg.Cedente=0,"",IF(cfg.CedNome <> "", cfg.CedNome, IF(ced.Tipo=0,ced.RazaoSocial,ced.Nome))) NOMECED_IMP, ',
      'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CartNum, ',
      'IF(cfg.AceiteTit = 1, "S", "N") ACEITETIT_TXT, ',
      'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CART_IMP, ',
      'cfg.CartEmiss, car.TipoDoc, car.Ativo ',
      'FROM cnab_cfg cfg ',
      'LEFT JOIN carteiras car ON car.Codigo=cfg.CartEmiss ',
      'LEFT JOIN bancos bco ON bco.Codigo=cfg.CedBanco ',
      'LEFT JOIN entidades ced ON ced.Codigo=cfg.Cedente ',
      'WHERE cfg.Codigo=' + Geral.FF0(CNAB_Cfg),
      '']);
    //
    {$IfDef TEM_UH}
    if not NaoContinuaPelaCarteira(
      QryCNAB_Cfg.FieldByName('CartEmiss').AsInteger,
      QryCNAB_Cfg.FieldByName('TipoDoc').AsInteger,
      QryCNAB_Cfg.FieldByName('Ativo').AsInteger) then Exit;
    {$EndIf}
    //
    if (UBancos.GeraNossoNumero(
      QryCNAB_Cfg.FieldByName('TipoCart').AsInteger,
      QryCNAB_Cfg.FieldByName('CedBanco').AsInteger,
      QryCNAB_Cfg.FieldByName('CedAgencia').AsInteger,
      QryCNAB_Cfg.FieldByName('CedPosto').AsInteger,
      NumBloq,
      QryCNAB_Cfg.FieldByName('CedConta').AsString,
      QryCNAB_Cfg.FieldByName('CartNum').AsString,
      QryCNAB_Cfg.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(QryCNAB_Cfg.FieldByName('CodEmprBco').AsString),
      Vencimento,
      QryCNAB_Cfg.FieldByName('TipoCobranca').AsInteger,
      QryCNAB_Cfg.FieldByName('EspecieDoc').AsString,
      QryCNAB_Cfg.FieldByName('CNAB').AsInteger,
      QryCNAB_Cfg.FieldByName('CtaCooper').AsString,
      QryCNAB_Cfg.FieldByName('LayoutRem').AsString,
      NossoNumero, NossoNumero_Rem)) then
    begin
      {$IfNDef SemProtocolo}
        Protocolo   := UnProtocolo.LocalizaProtocolo(DataBase, Entidade);
        Protocolo2  := UnProtocolo.LocalizaProtocolo(DataBase, Entidade, Protocolo);
        ProtocoloCR := UnProtocolo.LocalizaProtocoloCR(DataBase, CNAB_Cfg);
      {$Else}
        Protocolo   := 0;
        Protocolo2  := 0;
        ProtocoloCR := 0;
      {$EndIf}
      //
      if AtualizaAreeIts then
      begin
        QryUpd.DataBase := DataBase;
        //
        UMyMod.SQLInsUpd(QryUpd, stUpd, TabAriA, False,
          ['Boleto', 'ProtocoloCR', 'Protocolo', 'Protocolo2'],
          ['Boleto', 'Codigo', 'Entidade', 'CNAB_Cfg', 'Vencto'],
          [NumBloq, ProtocoloCR, Protocolo, Protocolo2],
          [0, Codigo, Entidade, CNAB_Cfg, Geral.FDT(Vencimento, 1)], True);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QryAux, DataBase, [
          'SELECT ari.Valor, cna.JurosPerc, cna.MultaPerc, ',
          'ari.Entidade, pre.Periodo, ari.CartEmiss, ',
          'ari.Conta, ari.Controle, ari.Texto ',
          'FROM ' + TabAriA + ' ari ',
          'LEFT JOIN ' + TabPrvA + ' pre ON pre.Codigo = ari.Codigo ',
          'LEFT JOIN cnab_cfg cna ON cna.Codigo = ari.CNAB_Cfg ',
          'WHERE ari.Boleto=' + Geral.FFI(NumBloq),
          'AND ari.Codigo=' + Geral.FF0(Codigo),
          'AND ari.Entidade=' + Geral.FF0(Entidade),
          'AND ari.CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
          'AND ari.Vencto="' + Geral.FDT(Vencimento, 1) + '"',
          '']);
        if QryAux.RecordCount > 0 then
        begin
          QryAux.First;
          while not QryAux.Eof do
          begin
            InsereLancto(Vencimento, NumBloq, QryAux.FieldByName('Valor').AsFloat,
              QryAux.FieldByName('JurosPerc').AsFloat,
              QryAux.FieldByName('MultaPerc').AsFloat, CliInt,
              QryAux.FieldByName('Entidade').AsInteger,
              QryAux.FieldByName('Periodo').AsInteger,
              QryAux.FieldByName('CartEmiss').AsInteger,
              QryAux.FieldByName('Conta').AsInteger,
              QryAux.FieldByName('Controle').AsInteger,
              QryAux.FieldByName('Texto').AsString, False);
            //
            QryAux.Next;
          end;
          Result := NumBloq;
        end;
      end else
        Result := NumBloq;
    end;
  finally
    QryCNAB_Cfg.Free;
    QryUpd.Free;
    QryAux.Free;
  end;
end;

function TUnBloqGerl.GeraBloquetoAtual2(Codigo, Periodo, EntCliInt: Integer;
  Vencimento: TDate; AtualizaAreeIts: Boolean; TabLctA, TabAriA, TabCnsA: String;
  QueryBoletos, QueryBoletosIts: TmySQLQuery; DataBase: TmySQLDatabase;
  var Protocolo, Protocolo2, ProtocoloCR: Integer): Double;

  function NaoContinuaPelaCarteira(Carteira, TipoDoc, Ativo: Integer): Boolean;
  var
    NumCartEmiss: String;
  begin
    Result := True;
    //
    NumCartEmiss := Geral.FF0(Carteira);
    //
    if Carteira = 0 then
    begin
      Geral.MB_Aviso('A carteira de emiss�o dos boletos n�o foi definida no cadastro do condom�nio! A��o abortada!');
      Exit;
    end;
    if TipoDoc <> 5 then
    begin
      if Geral.MB_Pergunta('A carteira de emiss�o n� ' + NumCartEmiss +
        ' n�o est� configurada como "Tipo de documento" = bloqueto!' + sLineBreak +
        'Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
    if Ativo <> 1 then
    begin
      if Geral.MB_Pergunta('A carteira de emiss�o de bloquetos (n� ' +
        NumCartEmiss + ') n�o est� configurada como ativa!' + sLineBreak +
        'Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
    Result := False;
  end;

  function InsereLancto(Vencto: TDateTime; Boleto, Valor, JurosPerc,
    MultaPerc: Double; Empresa, Entidade, CartEmiss, Genero, Apto, Controle,
    FatID: Integer; Descri, TabelaUpd: String; Avulso: Boolean): Integer;
  {$IfNDef NO_FINANCEIRO}
  var
    Res: Boolean;
    Qry: TmySQLQuery;
    SQLCompl: String;
  begin
    Result := 0;
    Qry    := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      UFinanceiro.LancamentoDefaultVARS;
      //
      FLAN_Data       := Geral.FDT(DModG.ObtemAgora(), 1);
      FLAN_Tipo       := 2;
      FLAN_Documento  := Trunc(Boleto);
      FLAN_Credito    := Valor;
      FLAN_MoraDia    := JurosPerc;
      FLAN_Multa      := MultaPerc;
      FLAN_Carteira   := CartEmiss;
      FLAN_Genero     := Genero;
      FLAN_Cliente    := Entidade;
      FLAN_CliInt     := Empresa;
      FLAN_Depto      := Apto;
      {$IfDef TEM_UH}
      FLAN_ForneceI   := Entidade;
      {$EndIf}
      FLAN_Vencimento := Geral.FDT(Vencto, 1);
      FLAN_Mez        := dmkPF.PeriodoToAnoMes(Periodo);
      FLAN_FatID      := FatID;
      FLAN_FatNum     := Trunc(Boleto);
      FLAN_FatParcela := 1;
      //
      {$IfDef TEM_UH}
      FLAN_Descricao  := Descri;
      {$Else}
      if Avulso then
        FLAN_Descricao := Descri
      else
        FLAN_Descricao := Descri + ' - ' + dmkPF.MesEAnoDoPeriodo(Periodo);
      {$EndIf}
      //
      FLAN_Controle := UMyMod.BuscaEmLivreY(DataBase, 'Livres', 'Controle',
                         TabLctA, LAN_CTOS, 'Controle');
      //
      {$IFDEF DEFINE_VARLCT}
        Res := UFinanceiro.InsereLancamento(TabLctA);
      {$ELSE}
        Res := UFinanceiro.InsereLancamento;
      {$ENDIF}
      //
      if Res then
      begin
        if not Avulso then
        begin
          {$IfDef TEM_UH}
          SQLCompl := ', Vencto="' + Geral.FDT(Vencto, 1) + '"';
          {$Else}
          SQLCompl := '';
          {$EndIf}
          //
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DataBase, [
            'UPDATE ' + TabelaUpd + ' SET ',
            'Lancto=' + Geral.FF0(FLAN_Controle),
            SQLCompl,
            'WHERE Controle=' + Geral.FF0(Controle),
            '']);
        end;
        Result := FLAN_Controle;
      end;
    finally
      Qry.Free;
    end;
  {$Else}
  begin
    dmkPF.InfoSemModulo(mdlappFinanceiro);
  {$EndIf}
  end;

var
  CNAB_Cfg, EntiDepto, Controle, Lancto: Integer;
  NumBloq: Double;
  NossoNumero, NossoNumero_Rem, TabelaUpd, Descri: String;
  QryCNAB_Cfg, QryUpd, QryAux: TmySQLQuery;
begin
  Result := 0;
  //
  if MyObjects.FIC(QueryBoletos = nil, nil, 'Query Boletos n�o definida!') then Exit;
  if MyObjects.FIC(QueryBoletosIts = nil, nil, 'Query BoletosIts n�o definida!') then Exit;
  //
  QryCNAB_Cfg := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd      := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryAux      := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  //
  QryUpd.DataBase := DataBase;
  try
    CNAB_Cfg := QueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
    //
    if MyObjects.FIC(CNAB_Cfg = 0, nil, 'Configura��o de boleto n�o definida!') then Exit;
    //
    NumBloq  := UBco_Rem.ObtemProximoNossoNumero(CNAB_Cfg, False);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QryCNAB_Cfg, DataBase, [
      'SELECT bco.Nome NOMEBANCO, bco.DVB,  cfg.*, ',
      'IF(cfg.Cedente=0,"",IF(cfg.CedNome <> "", cfg.CedNome, ',
      'IF(ced.Tipo=0,ced.RazaoSocial,ced.Nome))) NOMECED_IMP, ',
      'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CartNum, ',
      'IF(cfg.AceiteTit = 1, "S", "N") ACEITETIT_TXT, ',
      'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CART_IMP, ',
      'cfg.CartEmiss, car.TipoDoc, car.Ativo ',
      'FROM cnab_cfg cfg ',
      'LEFT JOIN carteiras car ON car.Codigo=cfg.CartEmiss ',
      'LEFT JOIN bancos bco ON bco.Codigo=cfg.CedBanco ',
      'LEFT JOIN entidades ced ON ced.Codigo=cfg.Cedente ',
      'WHERE cfg.Codigo=' + Geral.FF0(CNAB_Cfg),
      '']);
    //
    {$IfDef TEM_UH}
    if NaoContinuaPelaCarteira(
      QryCNAB_Cfg.FieldByName('CartEmiss').AsInteger,
      QryCNAB_Cfg.FieldByName('TipoDoc').AsInteger,
      QryCNAB_Cfg.FieldByName('Ativo').AsInteger) = True then Exit;
    {$EndIf}
    //
    if (UBancos.GeraNossoNumero(
      QryCNAB_Cfg.FieldByName('TipoCart').AsInteger,
      QryCNAB_Cfg.FieldByName('CedBanco').AsInteger,
      QryCNAB_Cfg.FieldByName('CedAgencia').AsInteger,
      QryCNAB_Cfg.FieldByName('CedPosto').AsInteger,
      NumBloq,
      QryCNAB_Cfg.FieldByName('CedConta').AsString,
      QryCNAB_Cfg.FieldByName('CartNum').AsString,
      QryCNAB_Cfg.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(QryCNAB_Cfg.FieldByName('CodEmprBco').AsString),
      Vencimento,
      QryCNAB_Cfg.FieldByName('TipoCobranca').AsInteger,
      QryCNAB_Cfg.FieldByName('EspecieDoc').AsString,
      QryCNAB_Cfg.FieldByName('CNAB').AsInteger,
      QryCNAB_Cfg.FieldByName('CtaCooper').AsString,
      QryCNAB_Cfg.FieldByName('LayoutRem').AsString,
      NossoNumero, NossoNumero_Rem)) then
    begin
      {$IfNDef SemProtocolo}
        {$IfDef TEM_UH}
          EntiDepto := QueryBoletos.FieldByName('Conta').AsInteger;
        {$Else}
          EntiDepto := QueryBoletos.FieldByName('Entidade').AsInteger;
        {$EndIf}
        Protocolo   := UnProtocolo.LocalizaProtocolo(DataBase, EntiDepto);
        Protocolo2  := UnProtocolo.LocalizaProtocolo(DataBase, EntiDepto, Protocolo);
        ProtocoloCR := UnProtocolo.LocalizaProtocoloCR(DataBase, CNAB_Cfg);
      {$Else}
        Protocolo   := 0;
        Protocolo2  := 0;
        ProtocoloCR := 0;
      {$EndIf}
      //
      if AtualizaAreeIts then
      begin
        if QueryBoletosIts.RecordCount > 0 then
        begin
          QueryBoletosIts.First;
          //
          while not QueryBoletosIts.EOF do
          begin
            Controle := QueryBoletosIts.FieldByName('Controle').AsInteger;
            Lancto   := QueryBoletosIts.FieldByName('Lancto').AsInteger;
            //
            {$IfDef TEM_UH}
              TabelaUpd := QueryBoletosIts.FieldByName('TabelaOrig').AsString;
            {$Else}
              TabelaUpd := TabAriA;
            {$EndIf}
            //
            UMyMod.SQLInsUpd(QryUpd, stUpd, TabelaUpd, False,
              ['Boleto', 'ProtocoloCR', 'Protocolo', 'Protocolo2'], ['Controle'],
              [NumBloq, ProtocoloCR, Protocolo, Protocolo2], [Controle], True);
            //
            //Cria os lan�amentos financeiros
            {$IfDef TEM_UH}
              if LowerCase(TabAriA) = LowerCase(TabelaUpd) then //Arrecada��es
              begin
                //Arrecada��o
                if Lancto = 0 then
                begin
                  Descri := QueryBoletosIts.FieldByName('TEXTO').AsString + ' - ' +
                              dmkPF.MesEAnoDoPeriodo(Periodo) + ' - ' +
                              QueryBoletos.FieldByName('Unidade').AsString;
                  //
                  if InsereLancto(Vencimento, NumBloq,
                       QueryBoletosIts.FieldByName('VALOR').AsFloat,
                       QryCNAB_Cfg.FieldByName('JurosPerc').AsFloat,
                       QryCNAB_Cfg.FieldByName('MultaPerc').AsFloat, EntCliInt,
                       QueryBoletos.FieldByName('Propriet').AsInteger,
                       QryCNAB_Cfg.FieldByName('CartEmiss').AsInteger,
                       QueryBoletosIts.FieldByName('Genero').AsInteger,
                       QueryBoletos.FieldByName('Apto').AsInteger, Controle,
                       FArre_FatID, Descri, TabelaUpd, False) = 0 then
                  begin
                    Result := 0;
                    Exit;
                  end else
                    Result := NumBloq;
                end;
              end else
              if LowerCase(TabCnsA) = LowerCase(TabelaUpd) then //Consumo por leitura
              begin
                if Lancto = 0 then
                begin
                  Descri := QueryBoletosIts.FieldByName('TEXTO').AsString + ' (' +
                              UBloqCond.TextoExplicativoItemBoleto(1,
                                QueryBoletosIts.FieldByName('Casas').AsInteger,
                                QueryBoletosIts.FieldByName('MedAnt').AsFloat,
                                QueryBoletosIts.FieldByName('MedAtu').AsFloat,
                                QueryBoletosIts.FieldByName('Consumo').AsFloat,
                                QueryBoletosIts.FieldByName('UnidFat').AsFloat,
                                QueryBoletosIts.FieldByName('UnidLei').AsString,
                                QueryBoletosIts.FieldByName('UnidImp').AsString,
                                QueryBoletosIts.FieldByName('GeraTyp').AsInteger,
                                QueryBoletosIts.FieldByName('CasRat').AsInteger,
                                QueryBoletosIts.FieldByName('NaoImpLei').AsInteger,
                                QueryBoletosIts.FieldByName('GeraFat').AsFloat);

                  //
                  if InsereLancto(Vencimento, NumBloq,
                       QueryBoletosIts.FieldByName('VALOR').AsFloat,
                       QryCNAB_Cfg.FieldByName('JurosPerc').AsFloat,
                       QryCNAB_Cfg.FieldByName('MultaPerc').AsFloat, EntCliInt,
                       QueryBoletos.FieldByName('Propriet').AsInteger,
                       QryCNAB_Cfg.FieldByName('CartEmiss').AsInteger,
                       QueryBoletosIts.FieldByName('Genero').AsInteger,
                       QueryBoletos.FieldByName('Apto').AsInteger, Controle,
                       FCons_FatID, Descri, TabelaUpd, False) = 0 then
                  begin
                    Result := 0;
                    Exit;
                  end else
                    Result := NumBloq;
                end;
              end else
              begin
                Geral.MB_Aviso('Tabel inv�lida para inser��o de lan�amentos financeiros!');
                Exit;
              end;
            {$Else}
              if Lancto = 0 then
              begin
                Descri := QueryBoletosIts.FieldByName('TEXTO').AsString;
                //
                if InsereLancto(Vencimento, NumBloq,
                     QueryBoletosIts.FieldByName('VALOR').AsFloat,
                     QryCNAB_Cfg.FieldByName('JurosPerc').AsFloat,
                     QryCNAB_Cfg.FieldByName('MultaPerc').AsFloat, EntCliInt,
                     QueryBoletosIts.FieldByName('Entidade').AsInteger,
                     QryCNAB_Cfg.FieldByName('CartEmiss').AsInteger,
                     QueryBoletos.FieldByName('Conta').AsInteger,
                     0, Controle,
                     FArre_FatID, Descri, TabelaUpd, False) = 0 then
                  begin
                    Result := 0;
                    Exit;
                  end else
                    Result := NumBloq;
              end;
            {$EndIf}
            QueryBoletosIts.Next;
          end
        end;
      end else
        Result := NumBloq;
    end;
  finally
    QryCNAB_Cfg.Free;
    QryUpd.Free;
    QryAux.Free;
  end;
end;

function TUnBloqGerl.LiberaImpressaoBoletosCR(DataBase: TmySQLDatabase;
  QueryBoletos: TmySQLQuery): Boolean;
var
  Qry: TmySQLQuery;
  Entidade: Integer;
begin
  if (QueryBoletos.State <> dsInactive) and (QueryBoletos.RecordCount > 0) then
  begin
    Qry := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      {$IfDef TEM_UH}
        Entidade := QueryBoletos.FieldByName('Propriet').AsInteger;
      {$Else}
        Entidade := QueryBoletos.FieldByName('Entidade').AsInteger;
      {$EndIf}
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
        'SELECT ppi.DataE, pro.Def_Retorn ',
        'FROM protpakits ppi ',
        'LEFT JOIN protocolos pro ON pro.Codigo = ppi.Codigo ',
        'WHERE ppi.Docum=' + Geral.FFI(QueryBoletos.FieldByName('Boleto').AsFloat),
        'AND ppi.Cliente=' + Geral.FF0(Entidade),
        'AND ppi.Controle=' + Geral.FF0(QueryBoletos.FieldByName('ProtocoPakCR').AsInteger),
        '']);
      if Qry.RecordCount > 0 then
      begin
        if Qry.FieldByName('Def_Retorn').AsInteger = 1 then //Verifica retorno
        begin
          if Qry.FieldByName('DataE').AsDateTime < 2 then
            Result := False
          else
            Result := True;
        end else
          Result := True; //N�o verifica retorno
      end else
        Result := True; //Sem registro
    finally
      Qry.Free;
    end;
  end else
    Result := False;
end;

function TUnBloqGerl.PeriodoExiste(Periodo: Integer; Tabela: String;
  DataBase: TmySQLDatabase): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry    := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
      'SELECT Codigo ',
      'FROM ' + Tabela,
      'WHERE Periodo=' + Geral.FF0(Periodo),
      '']);
    if Qry.RecordCount > 0 then
      Result := True;
  finally
    Qry.Free;
  end;
end;

end.
