unit UnBloqGerl_Jan;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, ModuleGeral, dmkImage, Forms,
  Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids, AdvToolBar, DB,
  UnDmkProcFunc, ExtCtrls, dmkDBGrid, Math, Dialogs, UnDmkEnums, Classes,
  dmkDBGridZTO, frxDBSet, frxClass, UnInternalConsts;

type
  TUnBloqGerl_Jan = class(TObject)
  private
  public
    function  LocalizarPeriodoAtual(CliInt: Integer; Tabela: String;
              Cond: Boolean = False): Integer;
    procedure MostraCNAB_Cfg(Codigo: Integer);
  end;

var
  UBloqGerl_Jan: TUnBloqGerl_Jan;

implementation

uses MyDBCheck, UnMyObjects, BloGerenLocper, DmkDAC_PF, Module, CNAB_Cfg;

{ TUnBloqGerl_Jan }

function TUnBloqGerl_Jan.LocalizarPeriodoAtual(CliInt: Integer; Tabela: String;
  Cond: Boolean = False): Integer;
var
  Localizar: Boolean;
  Periodo: Integer;
begin
  Result := 0;
  //
  if DBCheck.CriaFm(TFmBloGerenLocper, FmBloGerenLocper, afmoNegarComAviso) then
  begin
    FmBloGerenLocper.FLocalizar := False;
    //
    if not Cond then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(FmBloGerenLocper.QrPrev, Dmod.MyDB, [
        'SELECT Periodo ',
        'FROM ' + Tabela,
        'WHERE Empresa=' + Geral.FF0(CliInt),
        'ORDER BY Periodo DESC ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(FmBloGerenLocper.QrPrev, Dmod.MyDB, [
        'SELECT Periodo ',
        'FROM ' + Tabela,
        'WHERE Cond=' + Geral.FF0(CliInt),
        'ORDER BY Periodo DESC ',
        '']);
    end;
    //
    FmBloGerenLocper.ShowModal;
    //
    Localizar := FmBloGerenLocper.FLocalizar;
    Periodo   := FmBloGerenLocper.FPeriodo;
    //
    FmBloGerenLocper.Destroy;
    Application.ProcessMessages;
    if Localizar then
      Result := Periodo;
  end;
end;

procedure TUnBloqGerl_Jan.MostraCNAB_Cfg(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCNAB_Cfg.LocCod(Codigo, Codigo);
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
  end;
end;

end.
