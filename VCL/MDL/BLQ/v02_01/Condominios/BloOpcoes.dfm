object FmBloOpcoes: TFmBloOpcoes
  Left = 339
  Top = 185
  Caption = 'BLO-OPCAO-001 :: Op'#231#245'es Boletos'
  ClientHeight = 523
  ClientWidth = 494
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 494
    Height = 367
    ActivePage = TabSheet1
    Align = alClient
    TabHeight = 25
    TabOrder = 0
    ExplicitHeight = 262
    object TabSheet1: TTabSheet
      Caption = 'Geral'
      ExplicitHeight = 227
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 486
        Height = 332
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        ExplicitHeight = 227
        object Label98: TLabel
          Left = 8
          Top = 11
          Width = 166
          Height = 13
          Caption = 'Tipo de email para envio de boleto:'
        end
        object SpeedButton15: TSpeedButton
          Left = 455
          Top = 30
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton15Click
        end
        object EdEntiTipCto: TdmkEditCB
          Left = 8
          Top = 30
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'EntiTipCto'
          UpdCampo = 'EntiTipCto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEntiTipCto
          IgnoraDBLookupComboBox = False
        end
        object CBEntiTipCto: TdmkDBLookupComboBox
          Left = 66
          Top = 30
          Width = 385
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsEntiTipCto
          TabOrder = 1
          dmkEditCB = EdEntiTipCto
          QryCampo = 'EntiTipCto'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object GroupBox1: TGroupBox
          Left = 8
          Top = 64
          Width = 468
          Height = 90
          Caption = ' Envio de emeio de boleto: '
          TabOrder = 2
          object Label19: TLabel
            Left = 8
            Top = 20
            Width = 197
            Height = 13
            Caption = 'Considerar que o e-mail foi recebido ap'#243's '
          end
          object Label20: TLabel
            Left = 304
            Top = 20
            Width = 115
            Height = 13
            Caption = 'dias do seu vencimento.'
          end
          object SpeedButton2: TSpeedButton
            Left = 440
            Top = 62
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object Label5: TLabel
            Left = 8
            Top = 43
            Width = 406
            Height = 13
            Caption = 
              'Pr'#233' e-mail para re-envio de aviso da falta de confirma'#231#227'o do rec' +
              'ebimento do bloqueto:'
          end
          object EdDdAutConfMail: TdmkEdit
            Left = 213
            Top = 17
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '60'
            QryCampo = 'DdAutConfMail'
            UpdCampo = 'DdAutConfMail'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 60
            ValWarn = False
          end
          object EdPreMailReenv: TdmkEditCB
            Left = 8
            Top = 62
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PreMailReenv'
            UpdCampo = 'PreMailReenv'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPreMailReenv
            IgnoraDBLookupComboBox = False
          end
          object CBPreMailReenv: TdmkDBLookupComboBox
            Left = 65
            Top = 62
            Width = 370
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPreEmailRe
            TabOrder = 2
            dmkEditCB = EdPreMailReenv
            QryCampo = 'PreMailReenv'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object GroupBox2: TGroupBox
          Left = 8
          Top = 160
          Width = 468
          Height = 50
          Caption = 'Configura'#231#245'es base de dados WEB'
          TabOrder = 3
          object Label1: TLabel
            Left = 8
            Top = 23
            Width = 111
            Height = 13
            Caption = 'Quantidade m'#225'xima de '
          end
          object Label2: TLabel
            Left = 208
            Top = 23
            Width = 237
            Height = 13
            Caption = 'dias para a configura'#231#227'o do vencimento do boleto'
          end
          object EdMaxDias: TdmkEdit
            Left = 123
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '60'
            QryCampo = 'MaxDias'
            UpdCampo = 'MaxDias'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 60
            ValWarn = False
          end
        end
        object GroupBox6: TGroupBox
          Left = 8
          Top = 214
          Width = 468
          Height = 90
          Caption = ' Bloqueto condominial: '
          TabOrder = 4
          object Label28: TLabel
            Left = 112
            Top = 20
            Width = 64
            Height = 13
            Caption = 'Margem esq.:'
          end
          object Label29: TLabel
            Left = 188
            Top = 20
            Width = 28
            Height = 13
            Caption = 'Topo:'
          end
          object Label30: TLabel
            Left = 12
            Top = 62
            Width = 94
            Height = 13
            Caption = 'Destinat'#225'rio correio:'
          end
          object Label3: TLabel
            Left = 12
            Top = 38
            Width = 58
            Height = 13
            Caption = 'Aviso verso:'
          end
          object Label4: TLabel
            Left = 264
            Top = 20
            Width = 30
            Height = 13
            Caption = 'Altura:'
          end
          object Label6: TLabel
            Left = 340
            Top = 20
            Width = 39
            Height = 13
            Caption = 'Largura:'
          end
          object EdBLQ_MEsqDestin: TdmkEdit
            Left = 112
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_TopoDestin: TdmkEdit
            Left = 188
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_MEsqAvisoV: TdmkEdit
            Left = 112
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_TopoAvisoV: TdmkEdit
            Left = 188
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_AltuAvisoV: TdmkEdit
            Left = 264
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_AltuDestin: TdmkEdit
            Left = 264
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_LargAvisoV: TdmkEdit
            Left = 340
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdBLQ_LargDestin: TdmkEdit
            Left = 340
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 494
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 446
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 398
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 193
        Height = 32
        Caption = 'Op'#231#245'es Boletos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 193
        Height = 32
        Caption = 'Op'#231#245'es Boletos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 193
        Height = 32
        Caption = 'Op'#231#245'es Boletos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 415
    Width = 494
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 310
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 490
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 459
    Width = 494
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 354
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 490
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 346
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 18
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntiTipCto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 276
    Top = 56
    object QrEntiTipCtoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiTipCtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntiTipCtoNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiTipCto: TDataSource
    DataSet = QrEntiTipCto
    Left = 304
    Top = 56
  end
  object VUEntiTipCto: TdmkValUsu
    dmkEditCB = EdEntiTipCto
    Panel = TabSheet1
    QryCampo = 'EntiTipCto'
    UpdCampo = 'EntiTipCto'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 332
    Top = 56
  end
  object QrBloOpcoes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM bloopcoes')
    Left = 220
    Top = 56
  end
  object DsBloOpcoes: TDataSource
    DataSet = QrBloOpcoes
    Left = 248
    Top = 56
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 360
    Top = 56
  end
  object QrPreEmailRe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 388
    Top = 56
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreEmailRe: TDataSource
    DataSet = QrPreEmailRe
    Left = 416
    Top = 56
  end
end
