object FmCondGerAgefet: TFmCondGerAgefet
  Left = 339
  Top = 185
  Caption = 'CAD-PROVI-003 :: Agendamento de Provis'#227'o'
  ClientHeight = 315
  ClientWidth = 655
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 655
    Height = 153
    Align = alClient
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 128
      Width = 70
      Height = 13
      Caption = 'Provis'#227'o base:'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 184
      Top = 128
      Width = 172
      Height = 13
      Caption = 'ID do condom'#237'nio na provis'#227'o base:'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 12
      Top = 8
      Width = 60
      Height = 13
      Caption = 'Condom'#237'nio:'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Left = 12
      Top = 104
      Width = 83
      Height = 13
      Caption = 'Per'#237'odo cobrado:'
      FocusControl = DBEdit5
    end
    object Label5: TLabel
      Left = 500
      Top = 56
      Width = 44
      Height = 13
      Caption = 'Parcelas:'
      FocusControl = DBEdit6
    end
    object Label6: TLabel
      Left = 12
      Top = 56
      Width = 89
      Height = 13
      Caption = 'Valor total agenda:'
      FocusControl = DBEdit7
    end
    object Label7: TLabel
      Left = 12
      Top = 80
      Width = 78
      Height = 13
      Caption = 'Texto substituto:'
      FocusControl = DBEdit8
    end
    object Label8: TLabel
      Left = 12
      Top = 32
      Width = 91
      Height = 13
      Caption = 'Fluxo de cobran'#231'a:'
      FocusControl = DBEdit9
    end
    object Label9: TLabel
      Left = 432
      Top = 128
      Width = 135
      Height = 13
      Caption = 'ID do per'#237'odo da cobran'#231'a :'
      FocusControl = DBEdit10
    end
    object DBEdit1: TDBEdit
      Left = 112
      Top = 124
      Width = 64
      Height = 21
      DataField = 'PrevBaC'
      DataSource = DsAgeFet
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 360
      Top = 124
      Width = 64
      Height = 21
      DataField = 'PrevBaI'
      DataSource = DsAgeFet
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 112
      Top = 4
      Width = 80
      Height = 21
      DataField = 'Cond'
      DataSource = DsAgeFet
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Left = 196
      Top = 4
      Width = 440
      Height = 21
      DataField = 'NOMECOND'
      DataSource = DsAgeFet
      TabOrder = 3
    end
    object DBEdit5: TDBEdit
      Left = 196
      Top = 100
      Width = 440
      Height = 21
      DataField = 'PERIODO_TXT'
      DataSource = DsAgeFet
      TabOrder = 4
    end
    object DBEdit6: TDBEdit
      Left = 556
      Top = 52
      Width = 80
      Height = 21
      DataField = 'Parcelas'
      DataSource = DsAgeFet
      TabOrder = 5
    end
    object DBEdit7: TDBEdit
      Left = 112
      Top = 52
      Width = 80
      Height = 21
      DataField = 'Valor'
      DataSource = DsAgeFet
      TabOrder = 6
    end
    object DBEdit8: TDBEdit
      Left = 112
      Top = 76
      Width = 524
      Height = 21
      DataField = 'Texto'
      DataSource = DsAgeFet
      TabOrder = 7
    end
    object DBEdit9: TDBEdit
      Left = 112
      Top = 28
      Width = 80
      Height = 21
      DataField = 'SitCobr'
      DataSource = DsAgeFet
      TabOrder = 8
    end
    object DBEdit10: TDBEdit
      Left = 572
      Top = 124
      Width = 64
      Height = 21
      DataField = 'PrevCod'
      DataSource = DsAgeFet
      TabOrder = 9
    end
    object DBEdit11: TDBEdit
      Left = 112
      Top = 100
      Width = 80
      Height = 21
      DataField = 'Periodo'
      DataSource = DsAgeFet
      TabOrder = 10
    end
    object DBEdit12: TDBEdit
      Left = 196
      Top = 28
      Width = 440
      Height = 21
      DataField = 'NOMESITCOBR'
      DataSource = DsAgeFet
      TabOrder = 11
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 655
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 607
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 559
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 318
        Height = 32
        Caption = 'Agendamento de Provis'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 318
        Height = 32
        Caption = 'Agendamento de Provis'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 318
        Height = 32
        Caption = 'Agendamento de Provis'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 201
    Width = 655
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 651
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 245
    Width = 655
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 651
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 507
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fechar'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
  end
  object DsAgeFet: TDataSource
    Left = 8
    Top = 12
  end
end
