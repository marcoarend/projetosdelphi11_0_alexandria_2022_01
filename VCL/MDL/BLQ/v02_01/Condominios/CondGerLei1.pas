unit CondGerLei1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, Db, mySQLDbTables, Grids, DBGrids,
  dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnDmkProcFunc, dmkImage,
  UnDmkEnums, DmkDAC_PF;

type
  THackDBGrid = class(TDBGrid);
  TFmCondGerLei1 = class(TForm)
    PnProduto: TPanel;
    Label1: TLabel;
    EdPeriodo: TEdit;
    Label2: TLabel;
    EdProduto: TdmkEditCB;
    CBProduto: TdmkDBLookupComboBox;
    QrCons: TmySQLQuery;
    DsCons: TDataSource;
    QrConsCodigo: TIntegerField;
    QrConsNome: TWideStringField;
    PnLeitura: TPanel;
    QrAptos: TmySQLQuery;
    QrAptosConta: TIntegerField;
    QrAnt: TmySQLQuery;
    QrAntApto: TIntegerField;
    QrAntMedAtu: TFloatField;
    QrAptosMedAnt: TFloatField;
    TbConsLei: TmySQLTable;
    DBGrid1: TDBGrid;
    DsConsLei: TDataSource;
    TbConsLeiID_Rand: TWideStringField;
    TbConsLeiApto_ID: TIntegerField;
    TbConsLeiMedAnt: TFloatField;
    TbConsLeiCarencia: TFloatField;
    TbConsLeiAdiciona: TSmallintField;
    STCli: TStaticText;
    QrConsCasas: TSmallintField;
    TbConsLeiGASTO: TFloatField;
    QrPesq: TmySQLQuery;
    QrConsPreco: TFloatField;
    QrJaTem: TmySQLQuery;
    QrJaTemApto: TIntegerField;
    QrAptosAptoJa: TIntegerField;
    QrMax: TmySQLQuery;
    QrAptosUnidade: TWideStringField;
    TbConsLeiApto_Un: TWideStringField;
    QrConsUnidFat: TFloatField;
    QrConsUnidLei: TWideStringField;
    QrConsUnidImp: TWideStringField;
    TbConsLeiPropriet: TIntegerField;
    QrAptosPropriet: TIntegerField;
    TbConsLeiVALOR: TFloatField;
    QrMedias: TmySQLQuery;
    QrMediasVezes: TLargeintField;
    QrMediasMENOR: TFloatField;
    QrMediasMEDIA: TFloatField;
    QrMediasMAIOR: TFloatField;
    QrMediasApto: TIntegerField;
    TbConsLeiMEDIA_VEZES: TFloatField;
    TbConsLeiMEDIA_MENOR: TFloatField;
    TbConsLeiMEDIA_MEDIA: TFloatField;
    TbConsLeiMEDIA_MAIOR: TFloatField;
    QrUni: TmySQLQuery;
    QrUniApto: TIntegerField;
    QrUniMedAtu: TFloatField;
    TbConsLeiMedAtu: TFloatField;
    QrConsCarencia: TFloatField;
    QrConsDifCaren: TSmallintField;
    TbConsLeiDifCaren: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    SpeedButton7: TSpeedButton;
    QrConsCNAB_Cfg: TIntegerField;
    QrLoc: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure TbConsLeiBeforePost(DataSet: TDataSet);
    procedure TbConsLeiCalcFields(DataSet: TDataSet);
    procedure TbConsLeiBeforeOpen(DataSet: TDataSet);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
  private
    { Private declarations }
    FID_Rand: String;
  public
    { Public declarations }
    FPeriodo, FCond: Integer;
    FTabCnsA: String;
  end;

  var
  FmCondGerLei1: TFmCondGerLei1;

implementation

uses Module, UCreate, UnInternalConsts, UMySQLModule, MyVCLSkin, ModuleGeral,
  UnMyObjects, UnBloquetos_Jan;

{$R *.DFM}

procedure TFmCondGerLei1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerLei1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerLei1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerLei1.SpeedButton7Click(Sender: TObject);
var
  Produto: Integer;
begin
  Produto      := EdProduto.ValueVariant;
  VAR_CADASTRO := 0;
  //
  UBloquetos_Jan.MostraCons(Produto);
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrCons, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdProduto, CBProduto, QrCons, VAR_CADASTRO);
    EdProduto.SetFocus;
  end;
end;

procedure TFmCondGerLei1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TbConsLei.Close;
  TbConsLei.DataBase := DModG.MyPID_DB;
  QrPesq.Close;
  QrPesq.DataBase := DModG.MyPID_DB;
  //
  PnProduto.Align := alClient;
  //
  if Dmod.QrControle.FieldByName('LokMedAnt').AsInteger = 1 then
    DBGrid1.Columns[1].ReadOnly := true
  else
    DBGrid1.Columns[1].ReadOnly := false;
end;

procedure TFmCondGerLei1.BtOKClick(Sender: TObject);
var
  Produto, i, Controle, AptoNao, CNAB_Cfg: Integer;
  FmtTxT, TmpTable: String;
  MedAnt: Double;
begin
  Produto  := EdProduto.ValueVariant;
  CNAB_Cfg := QrConsCNAB_Cfg.Value;
  //
  if MyObjects.FIC(Produto = 0, EdProduto, 'Nenhum produto foi selecionado!') then Exit;
  if MyObjects.FIC(CNAB_Cfg = 0, EdProduto, 'Defina a configura��o do boleto!') then Exit;
  //
  if not PnLeitura.Visible then
  begin
    Screen.Cursor := crHourGlass;
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QrAnt, Dmod.MyDB, [
        'SELECT csi.Apto, csi.MedAtu ',
        'FROM ' + FTabCnsA + ' csi ',
        'LEFT JOIN condimov cdi ON csi.Apto=cdi.Conta ',
        'WHERE cdi.Codigo=' + Geral.FF0(FCond),
        'AND csi.Codigo=' + Geral.FF0(Produto),
        'AND csi.Periodo = ( ',
        '  SELECT MAX(csi.Periodo) ',
        '  FROM ' + FTabCnsA + ' csi ',
        '  LEFT JOIN condimov cdi ON csi.Apto=cdi.Conta ',
        '  WHERE cdi.Codigo=' + Geral.FF0(FCond),
        '  AND csi.Codigo=' + Geral.FF0(Produto),
        '  AND csi.Periodo<' + Geral.FF0(FPeriodo) + ')',
        '']);
      UnDmkDAC_PF.AbreMySQLQuery0(QrJaTem, Dmod.MyDB, [
        'SELECT Apto FROM ' + FTabCnsA,
        'WHERE Codigo=' + Geral.FF0(Produto),
        'AND Cond=' + Geral.FF0(FCond),
        'AND Periodo=' + Geral.FF0(FPeriodo),
        '']);
      UnDmkDAC_PF.AbreMySQLQuery0(QrAptos, Dmod.MyDB, [
        'SELECT cdi.Conta, cdi.Unidade, cdi.Propriet ',
        'FROM condimov cdi ',
        'WHERE cdi.Codigo=' + Geral.FF0(FCond),
        '']);
      //
      AptoNao := 0;
      //
      TmpTable := UCriar.RecriaTempTableNovo(ntrttConsLei, DmodG.QrUpdPID1, False);
      //
      FID_Rand := Geral.SoNumeroELetra_TT(dmkPF.PWDGenerateSecutityString);
      //
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + TmpTable + ' SET ');
      DModG.QrUpdPID1.SQL.Add('ID_Rand=:P0, Apto_ID=:P1, Apto_Un=:P2, ');
      DModG.QrUpdPID1.SQL.Add('MedAnt=:P3, MedAtu=:P4, Propriet=:P5,');
      DModG.QrUpdPID1.SQL.Add('Carencia=:P6, DifCaren=:P7 ');
      QrAptos.First;
      while not QrAptos.Eof do
      begin
        if QrAptosAptoJa.Value <> QrAptosConta.Value then
        begin
          Inc(AptoNao, 1);
          //
          MedAnt := QrAptosMedAnt.Value;
          if MedAnt < 0.000001 then
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrUni, Dmod.MyDB, [
              'SELECT csi.Apto, csi.MedAtu ',
              'FROM ' + FTabCnsA + ' csi ',
              'LEFT JOIN condimov cdi ON csi.Apto=cdi.Conta ',
              'WHERE cdi.Codigo=' + Geral.FF0(FCond),
              'AND csi.Codigo=' + Geral.FF0(Produto),
              'AND csi.Apto=' + Geral.FF0(QrAptosConta.Value),
              'AND csi.Periodo = ( ',
              '  SELECT MAX(csi.Periodo) ',
              '  FROM ' + FTabCnsA + ' csi ',
              '  LEFT JOIN condimov cdi ON csi.Apto=cdi.Conta ',
              '  WHERE cdi.Codigo=' + Geral.FF0(FCond),
              '  AND csi.Codigo=' + Geral.FF0(Produto),
              '  AND csi.Periodo<' + Geral.FF0(FPeriodo),
              '  AND csi.Apto=' + Geral.FF0(QrAptosConta.Value),
              ') ',
              '']);
            MedAnt := QrUniMedAtu.Value;
          end;
          //
          DModG.QrUpdPID1.Params[00].AsString  := FID_Rand;
          DModG.QrUpdPID1.Params[01].AsInteger := QrAptosConta.Value;
          DModG.QrUpdPID1.Params[02].AsString  := QrAptosUnidade.Value;
          DModG.QrUpdPID1.Params[03].AsFloat   := MedAnt;
          DModG.QrUpdPID1.Params[04].AsFloat   := 0; // Provocar resposta
          DModG.QrUpdPID1.Params[05].AsInteger := QrAptosPropriet.Value;
          DModG.QrUpdPID1.Params[06].AsFloat   := QrConsCarencia.Value;
          DModG.QrUpdPID1.Params[07].AsInteger := QrConsDifCaren.Value;
          DModG.QrUpdPID1.ExecSQL;
          //
        end;
        QrAptos.Next;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
    if AptoNao = 0 then
    begin
      Geral.MB_Aviso('Todos os ' + Geral.FF0(QrAptos.RecordCount) +
        ' apartamentos ativos deste condom�nio j� possuem leitura para este ' +
        'per�odo!');
      Exit;
    end else begin
      Geral.MB_Aviso('Foram encontrados ' + Geral.FF0(AptoNao)+ ' de ' +
        Geral.FF0(QrAptos.RecordCount) + ' unidades ativas que ainda n�o tem leitura no per�odo!');
      //
      PnProduto.Enabled := False;
      PnProduto.Align   := alTop;
      PnLeitura.Visible := True;
      PnProduto.Height  := 65;
      //
      TbConsLei.Close;
      TbConsLei.Filtered  := False;
      TbConsLei.TableName := TmpTable;
      UnDmkDAC_PF.AbreTable(TbConsLei, DmodG.MyPID_DB);
      //
      FmtTxT := '';
      //
      for i := 1 to QrConsCasas.Value do
        FmtTxT := FmtTxT + '0';
      //
      FmtTxT := '#,###,##0.' + FmtTxT +';-#,###,##0.'+FmtTxT+'; ';
      //
      TbConsLeiMedAnt.DisplayFormat   := FmtTxT;
      TbConsLeiMedAtu.DisplayFormat   := FmtTxT;
      TbConsLeiGasto.DisplayFormat    := FmtTxT;
      TbConsLeiCarencia.DisplayFormat := FmtTxT;
      //
      TbConsLeiMEDIA_MENOR.DisplayFormat := FmtTxT;
      TbConsLeiMEDIA_MEDIA.DisplayFormat := FmtTxT;
      TbConsLeiMEDIA_MAIOR.DisplayFormat := FmtTxT;
      TbConsLeiMEDIA_VEZES.DisplayFormat := '0;-0; ';
    end;
  end else
  begin
    Screen.Cursor := crHourGlass;
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, DmodG.MyPID_DB, [
        'SELECT * ',
        'FROM conslei ',
        'WHERE MedAtu <= MedAnt ',
        'AND ID_Rand="' + FID_Rand + '"',
        '']);
      //
      if QrPesq.RecordCount > 0 then
      begin
        if Geral.MB_Pergunta('Existem ' + Geral.FF0(QrPesq.RecordCount) +
          ' registros que n�o ser�o inclu�dos pois n�o possuem consumo. Deseja ' +
          'continuar assim mesmo?') <> ID_YES
        then
          Exit;
      end;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO ' + FTabCnsA + ' SET ');
      Dmod.QrUpd.SQL.Add('Apto=:P0, MedAnt=:P1, MedAtu=:P2, Consumo=:P3, ');
      Dmod.QrUpd.SQL.Add('Preco=:P4, Valor=:P5, Cond=:P6, Periodo=:P7, ');
      Dmod.QrUpd.SQL.Add('Propriet=:P8, Casas=:P9, ');
      Dmod.QrUpd.SQL.Add('UnidImp=:P10, UnidFat=:P11, UnidLei=:P12, ');
      Dmod.QrUpd.SQL.Add('Carencia=:P13, DifCaren=:P14, CNAB_Cfg=:P15, ');
      Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
      TbConsLei.First;
      while not TbConsLei.Eof do
      begin
        if TbConsLeiMedAtu.Value > TbConsLeiMedAnt.Value then
        begin
          Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                      FTabCnsA, TAB_CNS, 'Controle');
          //
          Dmod.QrUpd.Params[00].AsInteger := TbConsLeiApto_ID.Value;
          Dmod.QrUpd.Params[01].AsFloat   := TbConsLeiMedAnt.Value;
          Dmod.QrUpd.Params[02].AsFloat   := TbConsLeiMedAtu.Value;
          Dmod.QrUpd.Params[03].AsFloat   := TbConsLeiGasto.Value;
          Dmod.QrUpd.Params[04].AsFloat   := QrConsPreco.Value;
          Dmod.QrUpd.Params[05].AsFloat   := TbConsLeiValor.Value;
          Dmod.QrUpd.Params[06].AsInteger := FCond;
          Dmod.QrUpd.Params[07].AsInteger := FPeriodo;
          Dmod.QrUpd.Params[08].AsInteger := TbConsLeiPropriet.Value;
          Dmod.QrUpd.Params[09].AsInteger := QrConsCasas.Value;
          Dmod.QrUpd.Params[10].AsString  := QrConsUnidImp.Value;
          Dmod.QrUpd.Params[11].AsFloat   := QrConsUnidFat.Value;
          Dmod.QrUpd.Params[12].AsString  := QrConsUnidLei.Value;
          Dmod.QrUpd.Params[13].AsFloat   := TbConsLeiCarencia.Value;
          Dmod.QrUpd.Params[14].AsInteger := TbConsLeiDifCaren.Value;
          Dmod.QrUpd.Params[15].AsInteger := CNAB_Cfg;
          //
          Dmod.QrUpd.Params[16].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
          Dmod.QrUpd.Params[17].AsInteger := VAR_USUARIO;
          Dmod.QrUpd.Params[18].AsInteger := Produto;
          Dmod.QrUpd.Params[19].AsInteger := Controle;
          //
          Dmod.QrUpd.ExecSQL;
          //
        end;
        TbConsLei.Next;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
    Close;
  end;
end;

procedure TFmCondGerLei1.TbConsLeiBeforePost(DataSet: TDataSet);
begin
  if TbConsLei.State = dsInsert then TbConsLei.Cancel;
end;

procedure TFmCondGerLei1.TbConsLeiCalcFields(DataSet: TDataSet);
var
  Gasto: Double;
begin
  Gasto := TbConsLeiMedAtu.Value - TbConsLeiMedAnt.Value;
  //
  if Gasto <= TbConsLeiCarencia.Value then
    Gasto := 0
  else if TbConsLeiDifCaren.Value = 1 then
    Gasto := Gasto - TbConsLeiCarencia.Value;
  //
  TbConsLeiGasto.Value := Gasto;
  //
  if Gasto <= 0 then TbConsLeiValor.Value := 0 else
    TbConsLeiValor.Value := Geral.RoundC(Gasto * QrConsPreco.Value * QrConsUnidFat.Value, 2);
end;

procedure TFmCondGerLei1.TbConsLeiBeforeOpen(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMedias, Dmod.MyDB, [
    'SELECT COUNT(cdi.Conta) Vezes, MIN(cni.Consumo) MENOR, ',
    '(SUM(cni.Consumo) / COUNT(cdi.Conta)) MEDIA, ',
    'MAX(cni.Consumo) MAIOR, cdi.Conta Apto ',
    'FROM cons cns ',
    'LEFT JOIN ' + FTabCnsA + '  cni ON cni.Codigo=cns.Codigo ',
    'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
    'LEFT JOIN condimov cdi ON cdi.Conta=cni.Apto ',
    'WHERE cni.Cond=' + Geral.FF0(FCond),
    'AND cnp.Cond=' + Geral.FF0(FCond),
    'AND cni.Codigo=' + Geral.FF0(QrConsCodigo.Value),
    'AND cni.Periodo > ' + Geral.FF0(FPeriodo) + ' -13 ',
    'AND cni.Periodo < ' + Geral.FF0(FPeriodo),
    'GROUP BY cdi.Conta ',
    '']);
end;

procedure TFmCondGerLei1.DBGrid1CellClick(Column: TColumn);
begin
  if Column.FieldName = 'DifCaren' then
  begin
    TbConsLei.Edit;
    if TbConsLeiDifCaren.Value = 1 then
      TbConsLeiDifCaren.Value := 0
    else
      TbConsLeiDifCaren.Value := 1;
    TbConsLei.Post;
    //
    TbConsLei.Refresh;
  end;
end;

procedure TFmCondGerLei1.DBGrid1ColEnter(Sender: TObject);
begin
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = 'DifCaren' then
    DBGrid1.Options := DBGrid1.Options - [dgEditing] else
    DBGrid1.Options := DBGrid1.Options + [dgEditing];
end;

procedure TFmCondGerLei1.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
  Extra = 1.3;
var
  OldAlign: Integer;
  Cor: TColor;
  Baixo, Medio, Alto, Gasto, Vi, Vf, Vt, Vm, R, G, B: Double;
  //Txt: String;
begin
  if Column.FieldName = 'DifCaren' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbConsLeiDifCaren.Value);
  if (Column.FieldName = 'GASTO') then
  begin
    //Txt := '';
    Baixo := TbConsLeiMEDIA_MENOR.Value;
    Medio := TbConsLeiMEDIA_MEDIA.Value;
    Alto  := TbConsLeiMEDIA_MAIOR.Value;
    Gasto := TbConsLeiGASTO.Value;
    //
    if Gasto < 0 then
    begin
      R := 128;
      G := 128;
      B := 128;
    end else begin
      R := 0;
      G := 0;
      B := 0;
      if Medio > 0 then
      begin
        if Gasto > Medio then G := 0 else
        begin
          Vi := Baixo / Extra;
          if Gasto < Vi then Gasto := Vi;
          Vf := Medio;
          Vt := Vf - Vi;
          Vm := Vf - Gasto;
          G := (Vm / Vt) * 255;
        end;
        if Gasto < Medio then R := 0 else
        begin
          Vf := Alto * Extra;
          if Alto > Vf then Alto := Vf;
          Vi := Medio;
          Vt := Vf - Vi;
          Vm := Vf - Gasto;
          R := 255 - ((Vm / Vt) * 255);
        end;
        Vi := Baixo;
        Vf := Alto;
        if (Gasto < Vi) or (Gasto > Vf) then B := 0 else
        begin
          if Gasto < Medio then
          begin
            Gasto := Gasto - Vi;
            B := (Gasto / Medio) * 255;
          end else begin
            Gasto := Gasto - Medio;
            B := 255 - ((Gasto / Vf) * 255);
          end;
        end;

      end;
    end;
    {Txt := Txt + 'R'+FormatFloat('0',R);
    Txt := Txt + 'G'+FormatFloat('0',G);
    Txt := Txt + 'B'+FormatFloat('0',B);}
    with DBGrid1.Canvas do
    begin
      if R > 255 then R := 255;
      if G > 255 then G := 255;
      if B > 255 then B := 255;
      Cor := RGB(Trunc(R), Trunc(G), Trunc(B));
      if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Color := Cor;

      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      FillRect(Rect);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText {+ Txt});
      SetTextAlign(Handle, OldAlign);

    end;
  end;
end;

end.
