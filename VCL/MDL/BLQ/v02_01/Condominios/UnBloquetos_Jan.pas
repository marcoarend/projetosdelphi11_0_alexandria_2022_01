unit UnBloquetos_Jan;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, ModuleGeral, Module, dmkImage, Forms,
  Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids, AdvToolBar, DB,
  UnDmkProcFunc, ExtCtrls, dmkDBGrid, Math, Dialogs, UnDmkEnums, Classes,
  dmkDBGridZTO, frxDBSet, frxClass, UnInternalConsts;

type
  TUnBloquetos_Jan = class(TObject)
  private
    procedure CadastroCondGer(AbrirEmAba: Boolean; InOwner: TWincontrol;
                AdvToolBarPager: TAdvToolBarPager);
  public
    function  MostraCondGerNew(SQLTipo: TSQLType; QueryPrev: TMySQLQuery;
              CliInt: Integer; Tabela: String): Integer;
    function  AgendamentoDeProvisaoEfetivado(Lancamento: Integer; TabPrvA: String;
              Mostra: Boolean): Boolean;
    function  MostraArreBaADupl(): Boolean;
    procedure MostraCondGer2(AbrirEmAba: Boolean; FatID, Lancamento: Integer;
              FatNum: Double; PageControl: TPageControl;
              AdvToolBarPager: TAdvToolBarPager);
    procedure MostraConfigBol(Codigo: Integer);
    procedure MostraFmCondGerPrev(SQLType: TSQLType; QueryPri: TmySQLQuery;
              Tabela, PrevPeriodoTxt: String; PrevCodigo: Integer);
    procedure MostraPrevVeri(EntCliInt, CliInt: Integer; TabelaLctA,
              TabelaPrvA: String);
    procedure MostraPrevVeriBaC(Controle: Integer);
    procedure MostraPrevBaI(Query: TmySQLQuery);
    procedure MostraPrevBaCLctos(Form: String);
    procedure MostraPrevBaA(Codigo, CliInt: Integer; TabelaPriA: String);
    procedure MostraPrevBAB(CliInt, EntCliInt, Codigo, Periodo: Integer;
              TabelaPriA, TabelaLctA, TabelaLctB, TabelaLctD, NomeCli: String;
              Progress: TProgressBar);
    (*
    procedure MostraCondGerLeiEdit(QueryCNS: TmySQLQuery; Casas: Integer;
              UnidFat: Double; TabLctA, TabCnsA: String);
    *)
    procedure MostraCondGerLeiEdit(QueryCNS: TmySQLQuery;
              DataSourceCNS: TDataSource; CliInt, Periodo, Controle: Integer;
              PeriodoTXT, NomeCliInt, TabLctA, TabCnsA: String);
    procedure MostraCondGerLei2(CliInt, EntCliInt, Periodo: Integer; NomeCli,
              TabLctA, TabCnsA: String);
    procedure MostraCondGerLei1(CliInt, Periodo: Integer; NomeCli, TabCnsA: String);
    procedure IncluiIntensBaseDeArrecadacao(SoListaProvisoes: Boolean;
              TabAriA, TabCnsA, NomeCli: String; CliInt, EntCliInt, Codigo,
              Periodo: Integer; PrevGastos: Double; Progress,
              Progress2: TProgressBar; Form: TForm);
    procedure MostraFmCondGerArreUni(SQLType: TSQLType; QueryAri: TMySQLQuery;
              Codigo, CliInt, EntCliInt: Integer; PeriodoTxt, TabLctA, TabAriA: String);
    procedure MostraFmCondGerArreMul(SQLType: TSQLType; CliInt, EntCliInt,
              Codigo, Apto, Propriet: Integer; TabAriA: String);
    procedure MostraCondGerDelArre(TabAriA, TabLctA: String; Codigo: Integer);
    procedure CadastroDeArrecadacoes(Codigo, Controle: Integer);
    procedure CadastroDeProvisoes();
    procedure MostraCons(Codigo: Integer);
    procedure MostraCondGerModBol(var ModelBloq, ConfigBol, BalAgrMens, Colunas,
              Compe: Integer);
    procedure MostraAptosModBol(Codigo, CliInt: Integer; Tabela: String);
    procedure MostraBloOpcoes(Query: TmySQLQuery);
  end;

var
  UBloquetos_Jan: TUnBloquetos_Jan;

implementation

uses UnBloqGerl, MyDBCheck, UnMyObjects, UnBloquetos, CondGer2, CondGerNew,
  CondGerAvisos, ConfigBol, CondGerPrev, PrevVeri, DmkDAC_PF, CondGerAgefet,
  PrevVeriBaC, PrevBaI, PrevBaCLctos, PrevBaA, UCreate, PrevBAB, BloOpcoes,
  CondGerLei2, CondGerLei1, ArreBaA, GetValor, CondGerArreUni, CondGerArreMul,
  CondGerDelArre, ArreBaC, PrevBaC, ArreBaADupl, Cons, UnConsumoGerlJan;

{ TUnBloquetos_Jan }

function TUnBloquetos_Jan.MostraArreBaADupl(): Boolean;
begin
  Result := False;
  //
  if DBCheck.CriaFm(TFmArreBaADupl, FmArreBaADupl, afmoLiberado) then
  begin
    FmArreBaADupl.ShowModal;
    Result := not FmArreBaADupl.FContinua;
    FmArreBaADupl.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.MostraBloOpcoes(Query: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmBloOpcoes, FmBloOpcoes, afmoNegarComAviso) then
  begin
    FmBloOpcoes.ShowModal;
    FmBloOpcoes.Destroy;
    //
    UBloquetos.ReopenBloOpcoes(Query);
  end;
end;

procedure TUnBloquetos_Jan.MostraCons(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCons, FmCons, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCons.LocCod(Codigo, Codigo);
    FmCons.ShowModal;
    FmCons.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.CadastroDeArrecadacoes(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmArreBaC, FmArreBaC, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmArreBaC.LocCod(Codigo, Codigo);
      FmArreBaC.ReopenQrArreBaI(Controle);
    end;
    FmArreBaC.ShowModal;
    FmArreBaC.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.CadastroDeProvisoes();
begin
  if DBCheck.CriaFm(TFmPrevBaC, FmPrevBaC, afmoNegarComAviso) then
  begin
    FmPrevBaC.ShowModal;
    FmPrevBaC.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.MostraFmCondGerArreMul(SQLType: TSQLType;
  CliInt, EntCliInt, Codigo, Apto, Propriet: Integer; TabAriA: String);
var
  TmpTab_UnidCond: String;
  QryAptos: TmySQLQuery;
begin
  if SQLType <> stIns then
  begin
    Geral.MB_Aviso('A a��o selecionada n�o foi implementada!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  QryAptos      := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    //
    TmpTab_UnidCond := UCriar.RecriaTempTableNovo(ntrttUnidCond, DmodG.QrUpdPID1, False);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QryAptos, Dmod.MyDB, [
      'SELECT cdi.Conta, cdi.Unidade, cdi.Propriet, CASE WHEN  ',
      'ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEPROPRIET,  ',
      'FracaoIdeal, Moradores ',
      'FROM condimov cdi ',
      'LEFT JOIN entidades ent ON ent.Codigo=cdi.Propriet ',
      'WHERE cdi.Codigo=' + Geral.FF0(CliInt),
      '']);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + TmpTab_UnidCond + ' SET ');
    DModG.QrUpdPID1.SQL.Add('Apto=:P0, Unidade=:P1, Proprie=:P2, Selecio=:P3, ');
    DModG.QrUpdPID1.SQL.Add('Entidad=:P4');
    //
    while not QryAptos.Eof do
    begin
      DModG.QrUpdPID1.Params[00].AsInteger := QryAptos.FieldByName('Conta').AsInteger;
      DModG.QrUpdPID1.Params[01].AsString  := QryAptos.FieldByName('Unidade').AsString;
      DModG.QrUpdPID1.Params[02].AsString  := QryAptos.FieldByName('NOMEPROPRIET').AsString;
      DModG.QrUpdPID1.Params[03].AsInteger := 0;
      DModG.QrUpdPID1.Params[04].AsInteger := QryAptos.FieldByName('Propriet').AsInteger;
      DModG.QrUpdPID1.ExecSQL;
      //
      QryAptos.Next;
    end;
  finally
    QryAptos.Free;
    //
    Screen.Cursor := crDefault;
    //
    if DBCheck.CriaFm(TFmCondGerArreMul, FmCondGerArreMul, afmoNegarComAviso) then
    begin
      FmCondGerArreMul.ImgTipo.SQLType := stIns;
      FmCondGerArreMul.FCodigo         := Codigo;
      FmCondGerArreMul.FApto           := Apto;
      FmCondGerArreMul.FPropriet       := Propriet;
      FmCondGerArreMul.FCodiCliente    := EntCliInt;
      FmCondGerArreMul.FCond           := CliInt;
      FmCondGerArreMul.FTabAriA        := TabAriA;
      FmCondGerArreMul.FTmpTabUnidCond := TmpTab_UnidCond;
      FmCondGerArreMul.ShowModal;
      FmCondGerArreMul.Destroy;
    end;
  end;
end;

procedure TUnBloquetos_Jan.MostraFmCondGerArreUni(SQLType: TSQLType;
  QueryAri: TMySQLQuery; Codigo, CliInt, EntCliInt: Integer; PeriodoTxt,
  TabLctA, TabAriA: String);
begin
  if DBCheck.CriaFm(TFmCondGerArreUni, FmCondGerArreUni, afmoNegarComAviso) then
  begin
    FmCondGerArreUni.ImgTipo.SQLType := SQLType;
    FmCondGerArreUni.FPrevPeriodo    := PeriodoTxt;
    FmCondGerArreUni.FCodigo         := Codigo;
    FmCondGerArreUni.FCond           := CliInt;
    FmCondGerArreUni.FCliente        := EntCliInt;
    FmCondGerArreUni.FTabLctA        := TabLctA;
    FmCondGerArreUni.FTabAriA        := TabAriA;
    //
  UnDmkDAC_PF.AbreMySQLQuery0(FmCondGerArreUni.QrAptos, Dmod.MyDB, [
    'SELECT imv.Andar, imv.Unidade, imv.Conta, imv.Propriet, ',
    'con.Codigo, con.Cliente ',
    'FROM condimov imv ',
    'LEFT JOIN cond con ON con.Codigo = imv.Codigo ',
    'WHERE imv.Codigo=' + Geral.FF0(CliInt),
    'ORDER BY imv.Andar, imv.Unidade ',
    '']);
    //
    if SQLType = stUpd then
    begin
      //Criado em 27/07/2013 para evitar erros nos boletos e lan�amentos
      FmCondGerArreUni.FApto     := QueryAri.FieldByName('Apto').AsInteger;
      FmCondGerArreUni.FPropriet := QueryAri.FieldByName('Propriet').AsInteger;
      FmCondGerArreUni.FControle := QueryAri.FieldByName('Controle').AsInteger;
      FmCondGerArreUni.FLancto   := QueryAri.FieldByName('Lancto').AsInteger;
      //
      FmCondGerArreUni.EdApto.ValueVariant       := QueryAri.FieldByName('Apto').AsInteger;
      FmCondGerArreUni.CBApto.KeyValue           := QueryAri.FieldByName('Apto').AsInteger;
      FmCondGerArreUni.EdCNAB_Cfg.ValueVariant   := QueryAri.FieldByName('CNAB_Cfg').AsInteger;
      FmCondGerArreUni.CBCNAB_Cfg.KeyValue       := QueryAri.FieldByName('CNAB_Cfg').AsInteger;
      FmCondGerArreUni.EdConta.ValueVariant      := QueryAri.FieldByName('Conta').AsInteger;
      FmCondGerArreUni.CBConta.KeyValue          := QueryAri.FieldByName('Conta').AsInteger;
      FmCondGerArreUni.EdNFSeSrvCad.ValueVariant := QueryAri.FieldByName('NFSeSrvCad').AsInteger;
      FmCondGerArreUni.CBNFSeSrvCad.KeyValue     := QueryAri.FieldByName('NFSeSrvCad').AsInteger;
      FmCondGerArreUni.EdValor.ValueVariant      := QueryAri.FieldByName('Valor').AsFloat;
      FmCondGerArreUni.EdDescricao.ValueVariant  := QueryAri.FieldByName('Texto').AsString;
      //
      FmCondGerArreUni.EdApto.Enabled      := False;
      FmCondGerArreUni.CBApto.Enabled      := False;
      FmCondGerArreUni.CkContinuar.Visible := False;
      FmCondGerArreUni.CkContinuar.Checked := False;
    end else
    begin
      FmCondGerArreUni.FApto     := 0;
      FmCondGerArreUni.FPropriet := 0;
      FmCondGerArreUni.FControle := 0;
      FmCondGerArreUni.FLancto   := 0;
      //
      FmCondGerArreUni.EdApto.Enabled      := True;
      FmCondGerArreUni.CBApto.Enabled      := True;
      FmCondGerArreUni.CkContinuar.Visible := True;
      FmCondGerArreUni.CkContinuar.Checked := True;
    end;
    FmCondGerArreUni.ShowModal;
    FmCondGerArreUni.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.IncluiIntensBaseDeArrecadacao(SoListaProvisoes: Boolean;
  TabAriA, TabCnsA, NomeCli: String; CliInt, EntCliInt, Codigo, Periodo: Integer;
  PrevGastos: Double; Progress, Progress2: TProgressBar; Form: TForm);
var
  Pode, Aded, MesI, MesF, MesT, AnoT, a, b, c: Word;
  Adiciona, Seq, ArreInt: Integer;
  Texto, ComplTxt, Titulo, ArreBaA, ArreBaAUni: String;
  Txa, ValB, Val1, Val2, Val3, ValCalc, FatorUH, Fator_u, Fator_t, PercXtra: Double;
  ResVar: Variant;
  IncluiArrec: Boolean;
  Qry, QryAptos, QryTFracao, QryTMoradores, QryConsApt, QrySumBaAUni,
  QrySumArreUH: TmySQLQuery;
begin
  if not SoListaProvisoes then
  begin
    if Geral.MB_Pergunta('� aconselhavel incluir estes itens ' +
      'somente ap�s definido toda provis�o or�amentaria, todas leituras, e ' +
      'todas arrecada��es extras, pois ela afeta os valores aqui definidos. ' +
      sLineBreak + 'Confirma a inclus�o dos itens?') <> ID_YES
    then
      Exit;
  end;
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QryAptos      := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QryTFracao    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QryConsApt    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QrySumBaAUni  := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QrySumArreUH  := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    Pode := 0;
    Aded := 0;
    Seq  := 0;
    Val3 := 0;
    //
    ArreBaA    := UCriar.RecriaTempTableNovo(ntrttArreBaA, DmodG.QrUpdPID1, False);
    ArreBaAUni := UCriar.RecriaTempTableNovo(ntrttArreBaAUni, DmodG.QrUpdPID1, False);
    //
    DmodG.QrUpdPID2.Close;
    DmodG.QrUpdPID2.SQL.Clear;
    DmodG.QrUpdPID2.SQL.Add('INSERT INTO ' + ArreBaAUni + ' SET ');
    DmodG.QrUpdPID2.SQL.Add('Seq=:P0, Apto=:P1, Unidade=:P2, Propriet=:P3, ');
    DmodG.QrUpdPID2.SQL.Add('NomePropriet=:P4, Valor=:P5, Adiciona=:P6, ');
    DmodG.QrUpdPID2.SQL.Add('NaoArreSobre=:P7, Calculo=:P8, Cotas=:P9, ');
    DmodG.QrUpdPID2.SQL.Add('DescriCota=:P10, TextoCota=:P11, ');
    DmodG.QrUpdPID2.SQL.Add('ArreBac=:P12, ArreBaI=:P13, NaoArreRisco=:P14');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT abc.Codigo, abc.Nome, abc.Conta, ',
      'abi.Valor, abi.SitCobr, abi.Parcelas, abi.ParcPerI, ',
      'abi.ParcPerF, abi.InfoParc, abi.Texto, abi.Controle, ',
      'abi.Fator,abi.Percent, abi.DeQuem, abi.DoQue, ',
      'abi.ComoCobra, abi.NaoArreSobre, ',
      'abi.Calculo, abi.Arredonda, abi.ListaBaU, ',
      'abi.DescriCota, abi.ValFracDef, abi.ValFracAsk, ',
      'abi.Partilha, abi.NaoArreRisco, abi.CNAB_Cfg, abi.NFSeSrvCad ',
      'FROM arrebac abc ',
      'LEFT JOIN arrebai abi ON abi.Codigo=Abc.Codigo ',
      'WHERE abi.Cond=' + Geral.FF0(CliInt),
      'AND abc.Codigo NOT IN ',
      '( ',
      '  SELECT ArreBaC ',
      '  FROM ' + TabAriA,
      '  WHERE Codigo=' + Geral.FF0(Codigo),
      ') ',
      'ORDER BY DestCobra ',
      '']);
      //
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + ArreBaA + ' SET ');
      DmodG.QrUpdPID1.SQL.Add('Conta=:P0, ArreBac=:P1, ArreBaI=:P2, Valor=:P3, ');
      DmodG.QrUpdPID1.SQL.Add('Texto=:P4, Adiciona=:P5, Seq=:P6, CNAB_Cfg=:P7, NFSeSrvCad=:P8 ');
      //
      Progress.Visible := True;
      Progress.Max     := Qry.RecordCount;
      // Cobran�as Sobre provis�es.
      while not Qry.Eof do
      begin
        if SoListaProvisoes then
        begin
          IncluiArrec := (Qry.FieldByName('Fator').AsInteger = 1) and
                         (Qry.FieldByName('Doque').AsInteger = 0);
        end else
          IncluiArrec := True;
        if IncluiArrec then
        begin
          case Qry.FieldByName('SitCobr').AsInteger of
            0: ; // N�o cobrar
            1:   // Cobran�a Cont�nua (Anual)
            begin
              MesI := Qry.FieldByName('ParcPerI').AsInteger;
              MesF := Qry.FieldByName('ParcPerF').AsInteger;
              dmkPF.PeriodoDecode(Periodo, AnoT, MesT);
              //
              if (MesI > 0) and (MesF > 0) and (MesI < 13) and (MesF < 13) then
              begin
                if (MesF > MesI) then
                begin
                  if (MesF >= MesT) and (MesI <= MesT) then
                  inc(Pode, 1);
                end else
                if (MesF < MesI) then
                begin
                  a := MesI;
                  b := MesT;
                  if MesF > MesT then b := b + 12;
                  c := MesF + 12;
                  if (c >= b) and (a <= b) then
                  inc(Pode, 1);
                end;
              end;
            end;
            2:  // Cobran�a Programada
            begin
              MesI := Qry.FieldByName('ParcPerI').AsInteger;
              MesF := Qry.FieldByName('ParcPerF').AsInteger;
              MesT := Periodo;
              //
              if MesI > MesF then
              begin
                if MesF > MesT then MesT := MesT + 12;
                MesF := MesF + 12;
              end;
              if (MesF >= MesT)  and (MesI <= MesT) then
                inc(Pode, 1);
            end;
          end;
          if Aded < Pode then
          begin
            ValB := 0;
            inc(Aded, 1);
            Adiciona := 1;
            if Trim(Qry.FieldByName('Texto').AsString) <> '' then
              Texto := Qry.FieldByName('Texto').AsString
            else
              Texto := Qry.FieldByName('Nome').AsString;
            //
            if Qry.FieldByName('InfoParc').AsInteger = 1 then
            begin
              a := 0;
              b := 0;
              c := 0;
              case Qry.FieldByName('SitCobr').AsInteger of
                1: // Cobran�a Cont�nua (Anual)
                begin
                  dmkPF.PeriodoDecode(Periodo, AnoT, MesT);
                  a := Qry.FieldByName('ParcPerI').AsInteger;
                  c := Qry.FieldByName('ParcPerF').AsInteger;
                  if c < a then Inc(c, 12);
                  b := MesT;
                  if b < a then Inc(b, 12);
                end;
                2: // Cobran�a Programada
                begin
                  a := Qry.FieldByName('ParcPerI').AsInteger;
                  b := Periodo;
                  c := Qry.FieldByName('ParcPerF').AsInteger;
                end;
              end;
              Texto := FormatFloat('00', b-a+1) + '/' + FormatFloat('00', c-a+1) +
                ' - ' + Texto;
            end;
            Progress.Position := Progress.Position + 1;
            Form.Update;
            Application.ProcessMessages;
            //
            QryAptos.Close;
            QryAptos.SQL.Clear;
            if Qry.FieldByName('DeQuem').AsInteger in ([0,1]) then
            begin
              // De todos  e selecionados
              QryAptos.SQL.Add('SELECT cdi.Conta, cdi.Unidade, cdi.Propriet, CASE WHEN ');
              QryAptos.SQL.Add('ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEPROPRIET,');
              QryAptos.SQL.Add('FracaoIdeal, Moradores');
              QryAptos.SQL.Add('FROM condimov cdi');
              QryAptos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cdi.Propriet');
              QryAptos.SQL.Add('WHERE cdi.Codigo=:P0');
              QryAptos.Params[0].AsInteger := CliInt;
            end;
            if Qry.FieldByName('DeQuem').AsInteger = 0 then
            begin
              // De todos Habilitados
              QryAptos.SQL.Add('AND cdi.SitImv=1');
            end else if Qry.FieldByName('DeQuem').AsInteger = 1 then
            begin
              // Selecionados
              QryAptos.SQL.Add('AND cdi.Conta IN (');
              QryAptos.SQL.Add('  SELECT Apto FROM arrebau');
              QryAptos.SQL.Add('  WHERE Controle=:P1)');
              //
              if Qry.FieldByName('ListaBaU').AsInteger <> 0 then
                QryAptos.Params[1].AsInteger := Qry.FieldByName('ListaBaU').AsInteger
              else
                QryAptos.Params[1].AsInteger := Qry.FieldByName('Controle').AsInteger;
            end;
            QryAptos.SQL.Add('ORDER BY cdi.Unidade');
            UMyMod.AbreQuery(QryAptos, Dmod.MyDB, 'TUnBloquetos_Jan.IncluiIntensBaseDeArrecadacao()');
            Inc(Seq, 1);
            //
            Progress2.Visible  := True;
            Progress2.Position := 0;
            Progress2.Max      := QryAptos.RecordCount;
            //
            //  ArreBaIFator para ValB
            case Qry.FieldByName('Fator').AsInteger of
              0: // Fator = Valor
                ValB := Qry.FieldByName('Valor').AsFloat;
              1: // 1: Fator = Percentual
              begin
                if QryAptos.RecordCount = 0 then ValB := 0 else
                begin
                  ValCalc := 0;
                  case Qry.FieldByName('DoQue').AsInteger of
                    0: //Sobre Provis�es
                      ValCalc := PrevGastos;
                    1: //Sobre Consumos
                      ValCalc := 0;
                    2: //Sobre ambos
                      ValCalc := PrevGastos;
                    //  Valor a definir / definido
                    3:
                    begin
                      if Qry.FieldByName('ValFracAsk').AsInteger = 1 then
                      begin
                        // Parei aqui
                        if Qry.FieldByName('Texto').AsString <> '' then
                          Titulo := '(' + Qry.FieldByName('Texto').AsString + ')'
                        else
                          Titulo := '';
                        if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
                          Qry.FieldByName('ValFracDef').AsFloat, 2, 0, '', '', True,
                          'XXX-XXXXX-998 :: ' + Qry.FieldByName('Nome').AsString,
                          'Informe o valor total: ' + Titulo, 0, ResVar)
                        then
                          ValB := ResVar
                        else
                          ValB := 0;
                      end else
                        ValB := Qry.FieldByName('ValFracDef').AsFloat;
                    end;
                    4:
                    begin
                      // Parei aqui!!!
                      // Precisa Fazer ArreBaI
                    end;
                    else
                      ValCalc := 0;
                  end;
                  if Qry.FieldByName('DoQue').AsInteger <> 3 then
                  begin
                    if Qry.FieldByName('ComoCobra').AsInteger = 0 then
                    //  Calcular o percentual sobre o valor cobrado.
                    //n�o dividir por apto aqui! ????
                    //  ValB := (DmCond.QrNIA1Percent.Value * ValCalc) / QryAptos.RecordCount
                      ValB := Qry.FieldByName('Percent').AsFloat * ValCalc / 100
                    else
                    begin
                    //  Incluir o pr�prio valor gerado no c�lculo.
                      if Qry.FieldByName('Percent').AsFloat = 0 then
                        ValB := 0
                      else begin
                        PercXtra := Qry.FieldByName('Percent').AsFloat;
                        PercXtra := PercXtra + (PercXtra * PercXtra / 100);
                        // ... nem dividir por apto aqui! ????
                        //  ValB := ValCalc / (1 - (DmCond.QrNIA1Percent.Value/100)) / QryAptos.RecordCount;
                        ValB := PercXtra * ValCalc / 100;
                        //ValB := ValCalc / (1 - (PercXtra/100));
                        //ShowMessage(FloatToStr(ValB));
                      end;
                    end;
                  end;
                end;
                // n�o precisa ainda ??
                //if ValB > 0 then ValB := Int(ValB + 0.5) / 100;
              end;
              else begin
                ValB := 0;
                Geral.MB_Aviso('Fator n�o definido no cadastro de arrecada��es base!');
              end;
            end;
            // FIM ArreBaIFator para ValB

            //Total das fra��es ideais ou moradores na partilha
            case Qry.FieldByName('Partilha').AsInteger of
              1:
              begin
                UnDmkDAC_PF.AbreMySQLQuery0(QryTFracao, Dmod.MyDB, [
                  'SELECT SUM(FracaoIdeal) TOTAL ',
                  'FROM condimov cdi ',
                  'WHERE cdi.SitImv=1 ',
                  'AND cdi.Codigo=' + Geral.FF0(CliInt),
                  '']);
              end;
              2:
              begin
                UnDmkDAC_PF.AbreMySQLQuery0(QryTMoradores, Dmod.MyDB, [
                  'SELECT SUM(Moradores) TOTAL ',
                  'FROM condimov cdi ',
                  'WHERE cdi.SitImv=1 ',
                  'AND cdi.Codigo=' + Geral.FF0(CliInt),
                  '']);
              end;
            end;
            //calcula % por condomino aqui ?
            QryAptos.First;
            while not QryAptos.Eof do
            begin
              // calcular aqui o Val1 por apto conforme o campo "ArreBaI.Calculo"
              Val1    := 0;
              Fator_u := 0;
              case Qry.FieldByName('Fator').AsInteger of
                0: // Fator: Valor
                  Val1 := ValB;
                1: // Fator: Percentual
                begin
                  case Qry.FieldByName('DeQuem').AsInteger of
                    // Todos
                    0:
                    begin
                      // Partilha
                      case Qry.FieldByName('Partilha').AsInteger of
                        // Equitativa
                        0: FatorUH := 1 / QryAptos.RecordCount;
                        // Fra��o ideal
                        1:
                        begin
                          if QryTFracao.FieldByName('TOTAL').AsFloat = 0 then
                            FatorUH := 0
                          else
                            FatorUH := QryAptos.FieldByName('FracaoIdeal').AsFloat / QryTFracao.FieldByName('TOTAL').AsFloat;
                        end;
                        // Moradores
                        2:
                        begin
                          if QryTMoradores.FieldByName('TOTAL').AsFloat = 0 then
                            FatorUH := 0
                          else
                            FatorUH := QryAptos.FieldByName('Moradores').AsInteger / QryTMoradores.FieldByName('TOTAL').AsFloat;
                        end
                        else begin
                          FatorUH := 0;
                          Geral.MB_Erro('Partilha n�o definida!');
                        end;
                      end;
                      Val1 := ValB * FatorUH;
                    end;
                    // Selecionados
                    1:
                    begin
                      case Qry.FieldByName('Calculo').AsInteger of
                        0: FatorUH := 1 / QryAptos.RecordCount;
                        1: FatorUH := UBloquetos.GetPercAptUni(
                             Qry.FieldByName('Controle').AsInteger,
                             Qry.FieldByName('ListaBaU').AsInteger,
                             QryAptos.FieldByName('Conta').AsInteger,
                             Qry.FieldByName('Nome').AsString) / 100;
                        2:
                        begin
                          Fator_u := UBloquetos.GetPercAptUni(
                            Qry.FieldByName('Controle').AsInteger,
                            Qry.FieldByName('ListaBaU').AsInteger,
                            QryAptos.FieldByName('Conta').AsInteger,
                            Qry.FieldByName('Nome').AsString);
                          Fator_t := UBloquetos.GetPercAptAll(
                            Qry.FieldByName('Controle').AsInteger,
                            Qry.FieldByName('ListaBaU').AsInteger);
                          if Fator_t <> 0 then
                            FatorUH := Fator_u / Fator_t
                          else
                            FatorUH := 0;
                          // se for cotas ...
                          // N�o pode ser aqui!
                          (*
                          if DmCond.QrNIA1Calculo.Value = 2 then
                          begin
                            Texto := Texto + ' ' +
                              FloatToStr(Fator_u) + DmCond.QrNIA1DescriCota.Value
                          end;
                          *)
                          //
                        end;
                        else begin
                          FatorUH := 0;
                          Geral.MensagemBox('C�lculo n�o definido!', 'ERRO',
                          MB_OK+MB_ICONERROR);
                        end;
                      end;
                      Val1 := ValB * FatorUH;
                    end;
                  end;
                end;
                else begin
                  Val1 := 0;
                  (*
                  J� avisado acima!!
                  Geral.MensagemBox(
                    'Fator n�o definido no cadastro de arrecada��es base!',
                    'Avise a DERMATEK!', MB_OK+MB_ICONWARNING);
                  *)
                end;
              end;
              //
            // Arredonda valor conforme configurado no ArreBaI
            if Qry.FieldByName('Arredonda').AsFloat > 0 then
            begin
              ArreInt := Trunc((Val1 + (Qry.FieldByName('Arredonda').AsFloat * 0.99)) /
                Qry.FieldByName('Arredonda').AsFloat);
              Val1 := ArreInt * Qry.FieldByName('Arredonda').Value;
            end else
              Val1 := Trunc(Val1 * 100) / 100;
            //
            Progress2.Position := Progress2.Position + 1;
            Val2 := 0;
            // Quando for cobrar sobre consumos ou Consumos + Provis�es
            if Qry.FieldByName('DoQue').AsInteger > 0 then
            begin
              UnDmkDAC_PF.AbreMySQLQuery0(QryConsApt, Dmod.MyDB, [
                'SELECT SUM(cni.Valor) Valor ',
                'FROM cons cns ',
                'LEFT JOIN ' + TabCnsA + ' cni ON cni.Codigo=cns.Codigo ',
                'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
                'LEFT JOIN condimov cdi ON cdi.Conta=cni.Apto ',
                'WHERE cni.Cond=' + Geral.FF0(CliInt),
                'AND cnp.Cond=' + Geral.FF0(CliInt),
                'AND cni.Periodo=' + Geral.FF0(Periodo),
                'AND cdi.Conta=' + Geral.FF0(QryAptos.FieldByName('Conta').AsInteger),
                '']);
              case Qry.FieldByName('DoQue').AsInteger of
                // Sobre Provis�es
                0: ValCalc := 0;//QrPrevGastos.Value;
                // Sobre Consumos
                1: ValCalc := QryConsApt.FieldByName('Valor').AsFloat;
                // Sobre Ambos
                //2: ValCalc := (*QrPrevGastos.Value +*) DmCond.QrConsAptValor.Value;
                2: ValCalc := QryConsApt.FieldByName('Valor').AsFloat;
                // Valor para c�lculo
                // N�o Calcula Val2 para "Valor para c�lculo" ?
                3: ValCalc := 0;
                // Sobre as pr�prias arrecada��es rec�m geradas
                // Taxa de Administra��o de Cobran�a (Seguro de recebimento)
                4:
                begin
                  UnDmkDAC_PF.AbreMySQLQuery0(QrySumBaAUni, DModG.MyPID_DB, [
                    'SELECT SUM(Valor) Valor ',
                    'FROM ' + ArreBaAUni,
                    'WHERE NaoArreRisco=0 ',
                    'AND Apto=' + Geral.FF0(QryAptos.FieldByName('Conta').AsInteger),
                    '']);
                  UnDmkDAC_PF.AbreMySQLQuery0(QrySumArreUH, Dmod.MyDB, [
                    'SELECT SUM(Valor) Valor ',
                    'FROM ' + TabAriA,
                    'WHERE NaoArreRisco=0 ',
                    'AND Codigo=' + Geral.FF0(Codigo),
                    'AND Apto=' + Geral.FF0(QryAptos.FieldByName('Conta').AsInteger),
                    '']);
                  ValCalc := QrySumBaAUni.FieldByName('Valor').AsFloat +
                             QrySumArreUH.FieldByName('Valor').AsFloat +
                             QryConsApt.FieldByName('Valor').AsFloat;
                end;
                else
                  ValCalc := 0;
              end;
              if Qry.FieldByName('ComoCobra').AsInteger = 0 then
              //  Calcular o percentual sobre o valor cobrado.
                Val2 := (Qry.FieldByName('Percent').AsFloat * ValCalc) / 100
              else
              begin
              //  Incluir o pr�prio valor gerado no c�lculo.
                if Qry.FieldByName('Percent').AsFloat = 0 then
                  Val2 := 0
                else
                begin
                  //Val2 := (ValCalc / (1 - (DmCond.QrNIA1Percent.Value/100))) - ValCalc;
                  Txa := Trunc(((ValCalc * Qry.FieldByName('Percent').AsFloat / 100) + 0.01) * 100) / 100;
                  Val2 := (ValCalc + Txa) * Qry.FieldByName('Percent').AsFloat / 100;
                end;
              end;
            end;
            //
            // Arredonda valor conforme configurado no ArreBaI
            if (Val2 >= 0.01) and (Qry.FieldByName('Arredonda').AsFloat > 0) then
            begin
              ArreInt := Trunc((Val2 + (Qry.FieldByName('Arredonda').AsFloat * 0.99)) /
                Qry.FieldByName('Arredonda').AsFloat);
              Val2 := ArreInt * Qry.FieldByName('Arredonda').AsFloat;
            end else Val2 := Trunc(Val2 * 100) / 100;
            //
            if (Fator_u > 0) and (Qry.FieldByName('Calculo').AsInteger = 2)
            and (Qry.FieldByName('DescriCota').AsString <> '') then
              ComplTxt := Qry.FieldByName('DescriCota').AsString + ': ' + FloatToStr(Fator_u)
            else
              ComplTxt := '';
            DmodG.QrUpdPID2.Params[00].AsInteger := Seq;
            DmodG.QrUpdPID2.Params[01].AsInteger := QryAptos.FieldByName('Conta').AsInteger;
            DmodG.QrUpdPID2.Params[02].AsString  := QryAptos.FieldByName('Unidade').AsString;
            DmodG.QrUpdPID2.Params[03].AsInteger := QryAptos.FieldByName('Propriet').AsInteger;
            DmodG.QrUpdPID2.Params[04].AsString  := QryAptos.FieldByName('NOMEPROPRIET').AsString;
            DmodG.QrUpdPID2.Params[05].AsFloat   := Val1 + Val2;
            DmodG.QrUpdPID2.Params[06].AsInteger := Adiciona;
            DmodG.QrUpdPID2.Params[07].AsInteger := Qry.FieldByName('NaoArreSobre').AsInteger;
            DmodG.QrUpdPID2.Params[08].AsInteger := Qry.FieldByName('Calculo').AsInteger;
            DmodG.QrUpdPID2.Params[09].AsFloat   := Fator_u;
            DmodG.QrUpdPID2.Params[10].AsString  := Qry.FieldByName('DescriCota').AsString;
            DmodG.QrUpdPID2.Params[11].AsString  := ComplTxt;
            DmodG.QrUpdPID2.Params[12].AsInteger := Qry.FieldByName('Codigo').AsInteger;   // ArreBaC
            DmodG.QrUpdPID2.Params[13].AsInteger := Qry.FieldByName('Controle').AsInteger; // ArreBaI
            DmodG.QrUpdPID2.Params[14].AsInteger := Qry.FieldByName('NaoArreRisco').AsInteger;
            UMyMod.ExecutaQuery(DmodG.QrUpdPID2);
            //
            Val3 := Val3 + Val1 + Val2;
            QryAptos.Next;
          end;
          DmodG.QrUpdPID1.Params[00].AsInteger := Qry.FieldByName('Conta').AsInteger;
          DmodG.QrUpdPID1.Params[01].AsInteger := Qry.FieldByName('Codigo').AsInteger;
          DmodG.QrUpdPID1.Params[02].AsInteger := Qry.FieldByName('Controle').AsInteger;
          DmodG.QrUpdPID1.Params[03].AsFloat   := Val3;
          DmodG.QrUpdPID1.Params[04].AsString  := Texto;
          DmodG.QrUpdPID1.Params[05].AsInteger := Adiciona;
          DmodG.QrUpdPID1.Params[06].AsInteger := Seq;
          DModG.QrUpdPID1.Params[07].AsInteger := Qry.FieldByName('CNAB_Cfg').AsInteger;
          DModG.QrUpdPID1.Params[08].AsInteger := Qry.FieldByName('NFSeSrvCad').AsInteger;
          DmodG.QrUpdPID1.ExecSQL;
          Val3 := 0;
        end;
      end;
      Qry.Next;
    end;
    Progress.Position  := 0;
    Progress2.Position := 0;
    Screen.Cursor := crDefault;
    if not SoListaProvisoes then
    begin
      case Pode of
          0: Texto := 'N�o foi localizado nenhum item base de or�amento aplic�vel';
          1: Texto := 'Foi localizado 1 (um) item base de or�amento aplic�vel';
        else Texto := 'Foram localizados ' + Geral.FF0(Pode) + ' itens base de or�amento aplic�veis';
      end;
      Texto := Texto + ' de ' + Geral.FF0(Qry.RecordCount);
      case Qry.RecordCount of
          0: Texto := 'N�o h� itens base de or�amento cadastrados para este cliente!';
          1: Texto := Texto + ' pesquisado!';
        else Texto := Texto + ' pesquisados!';
      end;
      Geral.MB_Aviso(Texto);
      //
      if (Pode > 0) then
      begin
        if DBCheck.CriaFm(TFmArreBaA, FmArreBaA, afmoNegarComAviso) then
        begin
          FmArreBaA.FNomeCliente := NomeCli;
          FmArreBaA.FCodiCliente := EntCliInt;
          FmArreBaA.FCond        := CliInt;
          FmArreBaA.FCodigo      := Codigo;
          FmArreBaA.FArreBaA     := ArreBaA;
          FmArreBaA.FArreBaAUni  := ArreBaAUni;
          FmArreBaA.FTabAriA     := TabAriA;
          FmArreBaA.ShowModal;
          FmArreBaA.Destroy;
        end;
      end;
    end;
  finally
    Qry.Free;
    QryAptos.Free;
    QryTFracao.Free;
    QryConsApt.Free;
    QrySumBaAUni.Free;
    QrySumArreUH.Free;
    //
    Screen.Cursor := crDefault;
  end;
end;

function TUnBloquetos_Jan.AgendamentoDeProvisaoEfetivado(Lancamento: Integer;
  TabPrvA: String; Mostra: Boolean): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DATE_FORMAT(DATE_ADD("1999-12-01", ',
      '  INTERVAL Periodo MONTH), "%m/%Y") PERIODO_TXT, ',
      'IF (ent.Tipo=0, ',
      'ent.RazaoSocial,ent.Nome) NOMECOND, ',
      'pba.Codigo PrevBaC, pba.Controle PrevBaI, pba.Cond, ',
      'pba.Valor, pba.Texto, pba.SitCobr, ',
      'pba.Parcelas, pba.PrevCod, prv.Periodo ',
      'FROM prevbailct pbl ',
      'LEFT JOIN prevbai pba ON pba.Controle=pbl.Controle ',
      'LEFT JOIN ' + TabPrvA + ' prv ON prv.Codigo=pba.PrevCod ',
      'LEFT JOIN cond cnd ON cnd.Codigo=pba.Cond ',
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente ',
      'WHERE pbl.lancto=' + Geral.FF0(Lancamento),
      '']);
    Result := Qry.RecordCount > 0;
    if Mostra then
    begin
      if not Result then
      begin
        Geral.MensagemBox('O lan�amento atual n�o foi agendado em ' +
        'nenhum item de provis�o!', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end else begin
        if DBCheck.CriaFm(TFmCondGerAgefet, FmCondGerAgefet, afmoNegarComAviso) then
        begin
          FmCondGerAgefet.FQuery := Qry;
          FmCondGerAgefet.ShowModal;
          FmCondGerAgefet.Destroy;
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnBloquetos_Jan.CadastroCondGer(AbrirEmAba: Boolean;
  InOwner: TWincontrol; AdvToolBarPager: TAdvToolBarPager);
begin
  if not AbrirEmAba then
  begin
    if DBCheck.CriaFm(TFmCondGer2, FmCondGer2, afmoNegarComAviso) then
    begin
      FmCondGer2.ShowModal;
      FmCondGer2.Destroy;
      //
      FmCondGer2 := nil;
    end;
  end else
  begin
      MyObjects.FormTDICria(TFmCondGer2, InOwner, AdvToolBarPager, True, False);
  end;
end;

procedure TUnBloquetos_Jan.MostraCondGer2(AbrirEmAba: Boolean; FatID,
  Lancamento: Integer; FatNum: Double; PageControl: TPageControl;
  AdvToolBarPager: TAdvToolBarPager);
begin
  VAR_ModBloq_FatID  := FatID;
  VAR_ModBloq_FatNum := FatNum;
  VAR_ModBloq_Lancto := Lancamento;
  //
  if UBloqGerl.DefineVarsBloGeren(VAR_ModBloq_EntCliInt, VAR_ModBloq_CliInt,
    VAR_ModBloq_TabLctA, VAR_ModBloq_TabLctB, VAR_ModBloq_TabLctD,
    VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA, VAR_ModBloq_TabPriA,
    VAR_ModBloq_TabPrvA) then
  begin
    CadastroCondGer(AbrirEmAba, PageControl, AdvToolBarPager);
  end;
end;

procedure TUnBloquetos_Jan.MostraCondGerDelArre(TabAriA, TabLctA: String; Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCondGerDelArre, FmCondGerDelArre, afmoNegarComAviso) then
  begin
    FmCondGerDelArre.FTabAriA := TabAriA;
    FmCondGerDelArre.FTabLctA := TabLctA;
    FmCondGerDelArre.FCodigo  := Codigo;
    FmCondGerDelArre.ShowModal;
    FmCondGerDelArre.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.MostraAptosModBol(Codigo, CliInt: Integer; Tabela: String);

  procedure InsereItemAtual(ModelBloq, ConfigBol, BalAgrMens, Compe: Integer);
  begin
    UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, Tabela, False,
      ['ModelBloq', 'ConfigBol', 'BalAgrMens', 'Compe'],
      ['Codigo', 'Apto'],
      ['ModelBloq', 'ConfigBol', 'BalAgrMens', 'Compe'],
      [ModelBloq, ConfigBol, BalAgrMens, Compe],
      [Codigo, DModG.QrSelCodsNivel1.Value],
      [ModelBloq, ConfigBol, BalAgrMens, Compe], True);
  end;

var
  ModelBloq, ConfigBol, BalAgrMens, Colunas, Compe: Integer;
begin
  MostraCondGerModBol(ModelBloq, ConfigBol, BalAgrMens, Colunas, Compe);
  //
  if ModelBloq <> 0 then
  begin
    if DBCheck.EscolheCodigosMultiplos_0('...', 'XXX-XXXXX-000 :: Sele��o de UHs',
      'Seleciones as UHs desejados:', nil, 'Ativo', 'Nivel1', 'Nome', [
      'DELETE FROM _selcods_; ',
      'INSERT INTO _selcods_ ',
      'SELECT Conta Nivel1, 0 Nivel2, ',
      '0 Nivel3, 0 Nivel4, 0 Nivel5, Unidade Nome, 0 Ativo ',
      'FROM ' + TMeuDB + '.condimov ',
      'WHERE Codigo=' + Geral.FF0(CliInt),
      'AND Conta NOT IN ',
      '( ',
      'SELECT Apto ',
      'FROM ' + TMeuDB + '.' + Tabela,
      'WHERE Codigo=' + Geral.FF0(Codigo),
      ') ',
      ''],[
      'SELECT * FROM _selcods_; ',
      ''], Dmod.QrUpd) then
    begin
      DModG.QrSelCods.First;
      while not DModG.QrSelCods.Eof do
      begin
        if DModG.QrSelCodsAtivo.Value = 1 then
          InsereItemAtual(ModelBloq, ConfigBol, BalAgrMens, Compe);
        DModG.QrSelCods.Next;
      end;
    end;
  end;
end;

procedure TUnBloquetos_Jan.MostraCondGerModBol(var ModelBloq, ConfigBol,
  BalAgrMens, Colunas, Compe: Integer);
begin
  ModelBloq  := 0;
  ConfigBol  := 0;
  BalAgrMens := 0;
  Colunas    := 0;
  Compe      := 0;
  //
  if DBCheck.CriaFm(TFmCondGerAvisos, FmCondGerAvisos, afmoNegarComAviso) then
  begin
    FmCondGerAvisos.FApenasSelModelBol := True;
    //
    FmCondGerAvisos.ShowModal;
    //
    ModelBloq  := FmCondGerAvisos.FModelBloq;
    ConfigBol  := FmCondGerAvisos.FConfigBol;
    BalAgrMens := FmCondGerAvisos.FBalAgrMens;
    Colunas    := FmCondGerAvisos.FColunas;
    Compe      := FmCondGerAvisos.FCompe;
    //
    FmCondGerAvisos.Destroy;
    //
    if MyObjects.FIC(ModelBloq = 0, nil, 'Modelo de boleto n�o definido!') then
    begin
      ModelBloq  := 0;
      ConfigBol  := 0;
      BalAgrMens := 0;
      Colunas    := 0;
      Compe      := 0;
      //
      Exit;
    end;
  end;
end;

function TUnBloquetos_Jan.MostraCondGerNew(SQLTipo: TSQLType;
  QueryPrev: TMySQLQuery; CliInt: Integer; Tabela: String): Integer;
var
  Periodo: Integer;
begin
  Periodo := 0;
  //
  if SQLTipo = stIns then
  begin
    if DBCheck.CriaFm(TFmCondGerNew, FmCondGerNew, afmoNegarComAviso) then
    begin
      FmCondGerNew.FCliInt := CliInt;
      FmCondGerNew.FTabela := Tabela;
      FmCondGerNew.ShowModal;
      //
      Periodo := FmCondGerNew.FPeriodo;
      //
      FmCondGerNew.Destroy;
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmCondGerAvisos, FmCondGerAvisos, afmoNegarComAviso) then
    begin
      FmCondGerAvisos.FQueryPrev    := QueryPrev;
      FmCondGerAvisos.FCodigo       := QueryPrev.FieldByName('Codigo').AsInteger;
      FmCondGerAvisos.FTabela       := Tabela;
      FmCondGerAvisos.ShowModal;
      FmCondGerAvisos.Destroy;
      //
      Periodo := QueryPrev.FieldByName('Periodo').AsInteger;
    end;
  end;
  Result := Periodo;
end;

procedure TUnBloquetos_Jan.MostraConfigBol(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmConfigBol, FmConfigBol, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmConfigBol.LocCod(Codigo, Codigo);
    //
    with FmConfigBol.LBTitAAptTex.Items do
    begin
      Add('[PROPRIET] ' + DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O));
      Add('[UNIDADE]  ' + DModG.ReCaptionTexto(VAR_U_H_LONGO));
      Add('[VALFRAC]  Valor da fra��o ideal');
      Add('[EMPRNOME] Nome da empresa');
      Add('[EMPRCNPJ] CNPJ da empresa');
    end;
    FmConfigBol.ShowModal;
    FmConfigBol.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.MostraFmCondGerPrev(SQLType: TSQLType;
  QueryPri: TmySQLQuery; Tabela, PrevPeriodoTxt: String; PrevCodigo: Integer);
begin
  if DBCheck.CriaFm(TFmCondGerPrev, FmCondGerPrev, afmoNegarComAviso) then
  begin
    FmCondGerPrev.ImgTipo.SQLType := SQLType;
    FmCondGerPrev.FPrevCodigo     := PrevCodigo;
    FmCondGerPrev.FPrevPeriodoTxt := PrevPeriodoTxt;
    FmCondGerPrev.FTabela         := Tabela;
    FmCondGerPrev.FQueryPri       := QueryPri;
    //
    FmCondGerPrev.ShowModal;
    FmCondGerPrev.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.MostraPrevBaA(Codigo, CliInt: Integer; TabelaPriA: String);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT pbc.Codigo, pbc.Nome, pbc.Conta, ',
      'pbi.Valor, pbi.SitCobr, pbi.Texto, ',
      'pbi.Controle, con.Nome NOMECON ',
      'FROM prevbac pbc ',
      'LEFT JOIN prevbai pbi ON pbi.Codigo=pbc.Codigo ',
      'LEFT JOIN contas con ON con.Codigo=pbc.Conta ',
      'WHERE pbi.SitCobr = 3 ',
      'AND pbi.PrevCod=0 ',
      'AND pbi.Cond=' + Geral.FF0(CliInt),
      'AND pbc.Codigo NOT IN ',
      '( ',
      '  SELECT Codigo ',
      '  FROM ' + TabelaPriA,
      '  WHERE Codigo='+ Geral.FF0(Codigo),
      ') ',
      '']);
    if Qry.RecordCount = 0 then
    begin
      Geral.MB_Aviso('N�o h� nenhum item agendado para este condom�nio!');
      Exit;
    end else
    begin
      if DBCheck.CriaFm(TFmPrevBaA, FmPrevBaA, afmoNegarComAviso) then
      begin
        FmPrevBaA.FTabelaPriA := TabelaPriA;
        FmPrevBaA.FCodigo     := Codigo;
        FmPrevBaA.FQueryNIO_A := Qry;
        FmPrevBaA.ShowModal;
        FmPrevBaA.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnBloquetos_Jan.MostraPrevBAB(CliInt, EntCliInt, Codigo,
  Periodo: Integer; TabelaPriA, TabelaLctA, TabelaLctB, TabelaLctD,
  NomeCli: String; Progress: TProgressBar);
var
  Pode, Aded, MesI, MesF, MesT, AnoT, a, b, c: Word;
  Adiciona: Integer;
  TabelaTmp, Texto, LastM: String;
  Last1, Last6: Double;
  Qry, QueryMU6PM, QueryMU6PT, QueryMU6MM, QueryMU6MT: TmySQLQuery;
begin
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QueryMU6PM    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QueryMU6PT    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QueryMU6MM    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QueryMU6MT    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    Pode := 0;
    Aded := 0;
    //
    TabelaTmp := UCriar.RecriaTempTableNovo(ntrttPrevBAB, DmodG.QrUpdPID1, False);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT pbc.Codigo, pbc.Nome, pbc.Conta, ',
      'pbi.Valor, pbi.SitCobr, pbi.Parcelas, ',
      'pbi.ParcPerI, pbi.ParcPerF, pbi.InfoParc, ',
      'pbi.Texto, pbi.Controle, con.Nome NOMECON ',
      'FROM prevbac pbc ',
      'LEFT JOIN prevbai pbi ON pbi.Codigo=pbc.Codigo ',
      'LEFT JOIN contas con ON con.Codigo=pbc.Conta ',
      'WHERE SitCobr in (1,2) ',
      'AND pbi.Cond=' + Geral.FF0(CliInt),
      'AND pbc.Codigo NOT IN ',
      '( ',
      '  SELECT PrevBaC ',
      '  FROM ' + TabelaPriA,
      '  WHERE Codigo=' + Geral.FF0(Codigo),
      ') ',
      '']);
    //
    Progress.Max := Qry.RecordCount;
    while not Qry.Eof do
    begin
      case Qry.FieldByName('SitCobr').AsInteger of
        0: ; // nada
        1:
        begin
          MesI := Qry.FieldByName('ParcPerI').AsInteger;
          MesF := Qry.FieldByName('ParcPerF').AsInteger;
          dmkPF.PeriodoDecode(Periodo, AnoT, MesT);
          //
          if (MesI > 0) and (MesF > 0) and (MesI < 13) and (MesF < 13) then
          begin
            if (MesF > MesI) then
            begin
              if (MesF >= MesT)  and (MesI <= MesT) then
                inc(Pode, 1);
            end else
            if (MesF < MesI) then
            begin
              a := MesI;
              b := MesT;
              if MesF >= MesT then
                b := b + 12;
              c := MesF + 12;
              if (c >= b) and (a <= b) then
                inc(Pode, 1);
            end;
          end;
        end;
        2:
        begin
          MesI := Qry.FieldByName('ParcPerI').AsInteger;
          MesF := Qry.FieldByName('ParcPerF').AsInteger;
          MesT := Periodo;
          //
          if MesI > MesF then
          begin
            if MesF > MesT then MesT := MesT + 12;
            MesF := MesF + 12;
          end;
          if (MesF >= MesT)  and (MesI <= MesT) then
            inc(Pode, 1);
        end;
      end;

      ////////////////////////////////////////////////////////////////////////////

      if Aded < Pode then
      begin
        inc(Aded, 1);
        Adiciona := 1;
        if Trim(Qry.FieldByName('Texto').AsString) <> '' then
          Texto := Qry.FieldByName('Texto').AsString
        else
          Texto := Qry.FieldByName('Nome').AsString;
        if Qry.FieldByName('InfoParc').AsInteger = 1 then
        begin
          a := 0;
          b := 0;
          c := 0;
          case Qry.FieldByName('SitCobr').AsInteger of
            1:
            begin
              dmkPF.PeriodoDecode(Periodo, AnoT, MesT);
              a := Qry.FieldByName('ParcPerI').AsInteger;
              c := Qry.FieldByName('ParcPerF').AsInteger;
              if c < a then Inc(c, 12);
              b := MesT;
              if b < a then Inc(b, 12);
            end;
            2:
            begin
              a := Qry.FieldByName('ParcPerI').AsInteger;
              b := Periodo;
              c := Qry.FieldByName('ParcPerF').AsInteger;
            end;
          end;
          Texto := FormatFloat('00', b-a+1) + '/' + FormatFloat('00', c-a+1) +
            ' - ' + Texto;
        end;
        Progress.Position := Progress.Position + 1;
        Progress.Update;
        Application.ProcessMessages;
        //
        LastM := '';
        Last1 := 0;
        Last6 := 0;
        //
        UBloquetos.ReopenMU6PM(EntCliInt, Periodo,
          Qry.FieldByName('Conta').AsInteger, TabelaLctA, TabelaLctB,
          TabelaLctD, QueryMU6PM, QueryMU6PT);
        //
        if QueryMU6PM.RecordCount = 0 then
        begin
          UBloquetos.ReopenMU6MM(EntCliInt, Periodo,
            Qry.FieldByName('Conta').AsInteger, TabelaLctA, TabelaLctB,
            TabelaLctD, QueryMU6MM, QueryMU6MT);
          //
          if QueryMU6MM.RecordCount = 0 then
          begin
            // nada
          end else begin
            LastM := dmkPF.MezToMesEAno(QueryMU6MM.FieldByName('MEZ').AsInteger) + '*';
            Last1 := QueryMU6MM.FieldByName('SUM_DEB').AsFloat;
            Last6 := QueryMU6MT.FieldByName('SUM_DEB').AsFloat / QueryMU6MM.RecordCount;
          end;
        end else begin
          LastM := dmkPF.MezToMesEAno(QueryMU6PM.FieldByName('MEZ').AsInteger);
          Last1 := QueryMU6PM.FieldByName('SUM_DEB').AsFloat;
          Last6 := QueryMU6PT.FieldByName('SUM_DEB').AsFloat / QueryMU6PM.RecordCount;
        end;
        // Coloquei aqui para evitar erro!
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + TabelaTmp + ' SET ');
        DModG.QrUpdPID1.SQL.Add('Conta=:P0, PrevBac=:P1, PrevBaI=:P2, Valor=:P3, ');
        DModG.QrUpdPID1.SQL.Add('Texto=:P4, Adiciona=:P5, Last1=:P6, Last6=:P7, ');
        DModG.QrUpdPID1.SQL.Add('NOMECONTA=:P8, LastM=:P9 ');
        //
        DModG.QrUpdPID1.Params[00].AsInteger := Qry.FieldByName('Conta').AsInteger;
        DModG.QrUpdPID1.Params[01].AsInteger := Qry.FieldByName('Codigo').AsInteger;   // BaC
        DModG.QrUpdPID1.Params[02].AsInteger := Qry.FieldByName('Controle').AsInteger; // BaI
        DModG.QrUpdPID1.Params[03].AsFloat   := Qry.FieldByName('Valor').AsFloat;
        DModG.QrUpdPID1.Params[04].AsString  := Texto;
        DModG.QrUpdPID1.Params[05].AsInteger := Adiciona;
        DModG.QrUpdPID1.Params[06].AsFloat   := Last1;
        DModG.QrUpdPID1.Params[07].AsFloat   := Last6;
        DModG.QrUpdPID1.Params[08].AsString  := Qry.FieldByName('NOMECON').AsString;
        DModG.QrUpdPID1.Params[09].AsString  := LastM;
        DModG.QrUpdPID1.ExecSQL;
      end;

      ////////////////////////////////////////////////////////////////////////////

      Qry.Next;
    end;
    Progress.Visible := False;
    case Pode of
        0: Texto := 'N�o foi localizado nenhum item base de or�amento aplic�vel';
        1: Texto := 'Foi localizado 1 (um) item base de or�amento aplic�vel';
      else Texto := 'Foram localizados ' + Geral.FF0(Pode) + ' itens base de or�amento aplic�veis';
    end;
    Texto := Texto + ' de ' + Geral.FF0(Qry.RecordCount);
    case Qry.RecordCount of
        0: Texto := 'N�o h� itens base de or�amento cadastrados para este cliente!';
        1: Texto := Texto + ' pesquisado!';
      else Texto := Texto + ' pesquisados!';
    end;
    Geral.MB_Aviso(Texto);
    //
    if Pode > 0 then
    begin
      if DBCheck.CriaFm(TFmPrevBAB, FmPrevBAB, afmoNegarComAviso) then
      begin
        FmPrevBAB.FNomeCliente := NomeCli;
        FmPrevBAB.FCodiCliente := CliInt;
        FmPrevBAB.FTabelaTmp   := TabelaTmp;
        FmPrevBAB.FTabPriA     := TabelaPriA;
        FmPrevBAB.FCodigo      := Codigo;
        FmPrevBAB.ShowModal;
        FmPrevBAB.Destroy;
      end;
    end;
  finally
    QueryMU6PM.Free;
    QueryMU6PT.Free;
    QueryMU6MM.Free;
    QueryMU6MT.Free;
    Qry.Free;
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnBloquetos_Jan.MostraPrevBaCLctos(Form: String);
begin
  if DBCheck.CriaFm(TFmPrevBaCLctos, FmPrevBaCLctos, afmoNegarComAviso) then
  begin
    FmPrevBaCLctos.FForm := Form;
    FmPrevBaCLctos.ShowModal;
    FmPrevBaCLctos.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.MostraPrevBaI(Query: TmySQLQuery);
begin
  if UMyMod.FormInsUpd_Cria(TFmPrevBaI, FmPrevBaI, afmoNegarComAviso, Query, stUpd) then
  begin
    FmPrevBaI.EdNome.Text := Query.FieldByName('PrevBaC_Txt').AsString;
    //
    MyObjects.CBAnoMesFromPeriodo(FmPrevBaI.EdParcPerI.ValueVariant,
      FmPrevBaI.CBAnoI, FmPrevBaI.CBMesI, False);
    //
    MyObjects.CBAnoMesFromPeriodo(FmPrevBaI.EdParcPerF.ValueVariant,
      FmPrevBaI.CBAnoF, FmPrevBaI.CBMesF, False);
    //
    FmPrevBaI.ShowModal;
    FmPrevBaI.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.MostraPrevVeri(EntCliInt, CliInt: Integer; TabelaLctA,
  TabelaPrvA: String);
begin
  if DBCheck.CriaFm(TFmPrevVeri, FmPrevVeri, afmoNegarComAviso) then
  begin
    FmPrevVeri.FCodEnti := EntCliInt;
    FmPrevVeri.FCodCliI := CliInt;
    FmPrevVeri.FTabLctA := TabelaLctA;
    FmPrevVeri.FTabPrvA := TabelaPrvA;
    FmPrevVeri.ShowModal;
    FmPrevVeri.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.MostraPrevVeriBaC(Controle: Integer);
begin
  if DBCheck.CriaFm(TFmPrevVeriBaC, FmPrevVeriBaC, afmoNegarComAviso) then
  begin
    FmPrevVeriBaC.FControle := Controle;
    FmPrevVeriBaC.ShowModal;
    FmPrevVeriBaC.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.MostraCondGerLei1(CliInt, Periodo: Integer; NomeCli, TabCnsA: String);
begin
  if DBCheck.CriaFm(TFmCondGerLei1, FmCondGerLei1, afmoNegarComAviso) then
  begin
    FmCondGerLei1.FPeriodo       := Periodo;
    FmCondGerLei1.FCond          := CliInt;
    FmCondGerLei1.EdPeriodo.Text := dmkPF.MesEAnoDoPeriodo(Periodo);
    FmCondGerLei1.STCli.Caption  := NomeCli;
    FmCondGerLei1.FTabCnsA       := TabCnsA;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(FmCondGerLei1.QrCons, Dmod.MyDB, [
      'SELECT cns.Codigo, cns.Nome, cnp.Casas, cnp.Preco, ',
      'cnp.UnidFat, cnp.UnidLei, cnp.UnidImp, cnp.Carencia, ',
      'cnp.DifCaren, cnp.CNAB_Cfg ',
      'FROM consprc cnp ',
      'LEFT JOIN cons cns ON cns.Codigo=cnp.Codigo ',
      'WHERE cnp.Cond=' + Geral.FF0(CliInt),
      'ORDER BY cns.Nome ',
      '']);
    FmCondGerLei1.ShowModal;
    FmCondGerLei1.Destroy;
  end;
end;

procedure TUnBloquetos_Jan.MostraCondGerLei2(CliInt, EntCliInt, Periodo: Integer;
  NomeCli, TabLctA, TabCnsA: String);
begin
  if DBCheck.CriaFm(TFmCondGerLei2, FmCondGerLei2, afmoNegarComAviso) then
  begin
    FmCondGerLei2.FPeriodo       := Periodo;
    FmCondGerLei2.FCond          := CliInt;
    FmCondGerLei2.F_CliInt       := EntCliInt;
    FmCondGerLei2.FTabLctA       := TabLctA;
    FmCOndGerLei2.FTabCnsA       := TabCnsA;
    FmCondGerLei2.EdPeriodo.Text := dmkPF.MesEAnoDoPeriodo(Periodo);
    FmCondGerLei2.STCli.Caption  := NomeCli;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(FmCondGerLei2.QrCons, Dmod.MyDB, [
      'SELECT cns.Codigo, cns.Nome, cnp.Casas, cnp.Preco, ',
      'cnp.UnidFat, cnp.UnidLei, cnp.UnidImp, cnp.ContaBase, ',
      'cnp.PerioBase, cnp.FatorBase, cnp.Arredonda, cnp.CasRat, ',
      'cnp.NaoImpLei, cnp.CNAB_Cfg ',
      'FROM consprc cnp ',
      'LEFT JOIN cons cns ON cns.Codigo=cnp.Codigo ',
      'WHERE cnp.Cond=' + Geral.FF0(CliInt),
      'ORDER BY cns.Nome ',
      '']);
    FmCondGerLei2.ShowModal;
    FmCondGerLei2.Destroy;
  end;
end;

(*
procedure TUnBloquetos_Jan.MostraCondGerLeiEdit(QueryCNS: TmySQLQuery;
  Casas: Integer; UnidFat: Double; TabLctA, TabCnsA: String);
var
  Qry: TmySQLQuery;
  Lancto: Integer;
  Msg: String;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    Lancto := QueryCNS.FieldByName('Lancto').AsInteger;
    Msg    := '';
    //
    if (Lancto <> 0) and (UBloquetos.VerificaSeLctEstaoNaTabLctA(TabLctA, Lancto,
      Qry, Msg) = False) then
    begin
      Geral.MB_Aviso(Msg);
      Exit;
    end;
    Msg := '';
    //
    if (Lancto <> 0) and (not UBloquetos.VerificaSeLancamentoEstaPago(Lancto,
      Qry, TabLctA, Msg)) then
    begin
      Geral.MB_Aviso(Msg);
      Exit;
    end;
    if DBCheck.CriaFm(TFmLeiGerEdit, FmLeiGerEdit, afmoNegarComAviso) then
    begin
      FmLeiGerEdit.FControle             := QueryCNS.FieldByName('Controle').AsInteger;
      FmLeiGerEdit.FLancto               := Lancto;
      FmLeiGerEdit.FCasas                := Casas;
      FmLeiGerEdit.FTabLctA              := TabLctA;
      FmLeiGerEdit.FTabCnsA              := TabCnsA;
      FmLeiGerEdit.FUnidFat              := UnidFat;
      FmLeiGerEdit.EdLeiAnt.ValueVariant := Geral.FFT(QueryCNS.FieldByName('MedAnt').AsFloat, QueryCNS.FieldByName('Casas').AsInteger, siPositivo);
      FmLeiGerEdit.EdLeiAtu.ValueVariant := Geral.FFT(QueryCNS.FieldByName('MedAtu').AsFloat, QueryCNS.FieldByName('Casas').AsInteger, siPositivo);
      FmLeiGerEdit.EdPreco.ValueVariant  := QueryCNS.FieldByName('Preco').AsFloat;
      FmLeiGerEdit.Label14.Caption       := QueryCNS.FieldByName('UnidLei').AsString;
      FmLeiGerEdit.Label15.Caption       := QueryCNS.FieldByName('UnidImp').AsString;
      FmLeiGerEdit.ShowModal;
      FmLeiGerEdit.Destroy;
    end;
  finally
    Qry.Free;
  end;
end;
*)

procedure TUnBloquetos_Jan.MostraCondGerLeiEdit(QueryCNS: TmySQLQuery;
  DataSourceCNS: TDataSource; CliInt, Periodo, Controle: Integer; PeriodoTXT,
  NomeCliInt, TabLctA, TabCnsA: String);
var
  Qry: TmySQLQuery;
  Lancto: Integer;
  Msg: String;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    Lancto := QueryCNS.FieldByName('Lancto').AsInteger;
    Msg    := '';
    //
    if (Lancto <> 0) and (UBloquetos.VerificaSeLctEstaoNaTabLctA(TabLctA, Lancto,
      Qry, Msg) = False) then
    begin
      Geral.MB_Aviso(Msg);
      Exit;
    end;
    Msg := '';
    //
    if (Lancto <> 0) and (not UBloquetos.VerificaSeLancamentoEstaPago(Lancto,
      Qry, TabLctA, Msg)) then
    begin
      Geral.MB_Aviso(Msg);
      Exit;
    end;
    ConsumoGerlJan.MostraCondGerLeiEdit(QueryCNS, DataSourceCNS, CliInt,
      Periodo, Lancto, Controle, PeriodoTXT, NomeCliInt, TabLctA, TabCnsA);
  finally
    Qry.Free;
  end;
end;

end.
