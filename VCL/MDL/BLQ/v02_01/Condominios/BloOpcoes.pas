unit BloOpcoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkValUsu, DB, mySQLDbTables,
  dmkGeral, ComCtrls, dmkRadioGroup, dmkImage, dmkPermissoes, UnDmkEnums,
  dmkCheckBox, UnDmkProcFunc;

type
  TFmBloOpcoes = class(TForm)
    QrEntiTipCto: TmySQLQuery;
    QrEntiTipCtoCodigo: TIntegerField;
    QrEntiTipCtoCodUsu: TIntegerField;
    QrEntiTipCtoNome: TWideStringField;
    DsEntiTipCto: TDataSource;
    VUEntiTipCto: TdmkValUsu;
    QrBloOpcoes: TmySQLQuery;
    DsBloOpcoes: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Label98: TLabel;
    SpeedButton15: TSpeedButton;
    EdEntiTipCto: TdmkEditCB;
    CBEntiTipCto: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    EdDdAutConfMail: TdmkEdit;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    EdMaxDias: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    SpeedButton2: TSpeedButton;
    EdPreMailReenv: TdmkEditCB;
    CBPreMailReenv: TdmkDBLookupComboBox;
    Label5: TLabel;
    QrPreEmailRe: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField1: TWideStringField;
    DsPreEmailRe: TDataSource;
    GroupBox6: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    EdBLQ_MEsqDestin: TdmkEdit;
    EdBLQ_TopoDestin: TdmkEdit;
    EdBLQ_MEsqAvisoV: TdmkEdit;
    EdBLQ_TopoAvisoV: TdmkEdit;
    EdBLQ_AltuAvisoV: TdmkEdit;
    EdBLQ_AltuDestin: TdmkEdit;
    EdBLQ_LargAvisoV: TdmkEdit;
    EdBLQ_LargDestin: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton15Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmBloOpcoes: TFmBloOpcoes;

implementation

uses
{$IfNDef NO_USE_EMAILDMK} PreEmail, {$EndIf}
EntiTipCto, UMySQLModule, Module, UnInternalConsts, MyDBCheck, UnMyObjects;

{$R *.DFM}

procedure TFmBloOpcoes.BtOKClick(Sender: TObject);
var
  Codigo, Compe, Configurado: Integer;
begin
  Codigo      := QrBloOpcoes.FieldByName('Codigo').AsInteger;
  Configurado := 1;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'bloopcoes', False,
    [
    'EntiTipCto', 'DdAutConfMail',
    'MaxDias', 'Configurado',
    'PreMailReenv', 'BLQ_MEsqAvisoV',
    'BLQ_TopoAvisoV', 'BLQ_AltuAvisoV',
    'BLQ_LargAvisoV', 'BLQ_MEsqDestin',
    'BLQ_TopoDestin', 'BLQ_AltuDestin',
    'BLQ_LargDestin'
    ], ['Codigo'],
    [
    EdEntiTipCto.ValueVariant, EdDdAutConfMail.ValueVariant,
    EdMaxDias.ValueVariant, Configurado,
    EdPreMailReenv.ValueVariant, EdBLQ_MEsqAvisoV.ValueVariant,
    EdBLQ_TopoAvisoV.ValueVariant, EdBLQ_AltuAvisoV.ValueVariant,
    EdBLQ_LargAvisoV.ValueVariant, EdBLQ_MEsqDestin.ValueVariant,
    EdBLQ_TopoDestin.ValueVariant, EdBLQ_AltuDestin.ValueVariant,
    EdBLQ_LargDestin.ValueVariant
    ], [Codigo], True) then
  begin
    Close;
  end;
end;

procedure TFmBloOpcoes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloOpcoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmBloOpcoes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  //
  UMyMod.AbreQuery(QrBloOpcoes, DMod.MyDB);
  UMyMod.AbreQuery(QrEntiTipCto, DMod.MyDB);
{$IfNDef NO_USE_EMAILDMK}
  UMyMod.AbreQuery(QrPreEmailRe, DMod.MyDB);
{$EndIf}
  //
  EdEntiTipCto.ValueVariant    := QrBloOpcoes.FieldByName('EntiTipCto').AsInteger;
  CBEntiTipCto.KeyValue        := QrBloOpcoes.FieldByName('EntiTipCto').AsInteger;
  EdPreMailReenv.ValueVariant  := QrBloOpcoes.FieldByName('PreMailReenv').AsInteger;
  CBPreMailReenv.KeyValue      := QrBloOpcoes.FieldByName('PreMailReenv').AsInteger;
  EdDdAutConfMail.ValueVariant := QrBloOpcoes.FieldByName('DdAutConfMail').AsInteger;
  EdMaxDias.ValueVariant       := QrBloOpcoes.FieldByName('MaxDias').AsInteger;
  //
  EdBLQ_MEsqAvisoV.ValueVariant := QrBloOpcoes.FieldByName('BLQ_MEsqAvisoV').AsInteger;
  EdBLQ_TopoAvisoV.ValueVariant := QrBloOpcoes.FieldByName('BLQ_TopoAvisoV').AsInteger;
  EdBLQ_AltuAvisoV.ValueVariant := QrBloOpcoes.FieldByName('BLQ_AltuAvisoV').AsInteger;
  EdBLQ_LargAvisoV.ValueVariant := QrBloOpcoes.FieldByName('BLQ_LargAvisoV').AsInteger;
  EdBLQ_MEsqDestin.ValueVariant := QrBloOpcoes.FieldByName('BLQ_MEsqDestin').AsInteger;
  EdBLQ_TopoDestin.ValueVariant := QrBloOpcoes.FieldByName('BLQ_TopoDestin').AsInteger;
  EdBLQ_AltuDestin.ValueVariant := QrBloOpcoes.FieldByName('BLQ_AltuDestin').AsInteger;
  EdBLQ_LargDestin.ValueVariant := QrBloOpcoes.FieldByName('BLQ_LargDestin').AsInteger;
end;

procedure TFmBloOpcoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloOpcoes.SpeedButton15Click(Sender: TObject);
var
  TipEmail: Integer;
begin
  VAR_CADASTRO := 0;
  TipEmail     := EdEntiTipCto.ValueVariant;
  //
  if DBCheck.CriaFm(TFmEntiTipCto, FmEntiTipCto, afmoNegarComAviso) then
  begin
    if TipEmail <> 0 then
      FmEntiTipCto.LocCod(TipEmail, TipEmail);
    FmEntiTipCto.ShowModal;
    FmEntiTipCto.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdEntiTipCto, CBEntiTipCto, QrEntiTipCto, VAR_CADASTRO);
      EdEntiTipCto.SetFocus;
    end;
  end;
end;

procedure TFmBloOpcoes.SpeedButton2Click(Sender: TObject);
{$IfNDef NO_USE_EMAILDMK}
var
  PreEmail: Integer;
begin
  VAR_CADASTRO := 0;
  PreEmail     := EdPreMailReenv.ValueVariant;
  //
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    if PreEmail <> 0 then
      FmPreEmail.LocCod(PreEmail, PreEmail);
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodigoPesquisado(EdPreMailReenv, CBPreMailReenv, QrPreEmailRe, VAR_CADASTRO);
      EdPreMailReenv.SetFocus;
    end;
  end;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappEmail);
{$EndIf}
end;

end.
