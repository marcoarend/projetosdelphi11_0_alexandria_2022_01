unit PrevVeri;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, ComCtrls, dmkEditDateTimePicker, DB, mySQLDbTables,
  dmkDBGridDAC, dmkRadioGroup, Menus, dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TFmPrevVeri = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    TPDesde: TdmkEditDateTimePicker;
    Label1: TLabel;
    Panel4: TPanel;
    RGValores: TRadioGroup;
    QrLct: TmySQLQuery;
    PB1: TProgressBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    QrLctProvRat: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctData: TDateField;
    QrLctValor: TFloatField;
    QrLctVencimento: TDateField;
    QrLctDocumento: TFloatField;
    QrLctCompensado: TDateField;
    QrLctNotaFiscal: TIntegerField;
    QrLctTERCEIRO: TIntegerField;
    QrLctNO_TERCEIRO: TWideStringField;
    QrLctDescricao: TWideStringField;
    QrLctMotivo: TSmallintField;
    QrLctAtivo: TSmallintField;
    QrPrevVeri1: TmySQLQuery;
    DsPrevVeri1: TDataSource;
    QrPrevVeri1ProvRat: TSmallintField;
    QrPrevVeri1Controle: TIntegerField;
    QrPrevVeri1Data: TDateField;
    QrPrevVeri1Valor: TFloatField;
    QrPrevVeri1Vencimento: TDateField;
    QrPrevVeri1Documento: TFloatField;
    QrPrevVeri1Compensado: TDateField;
    QrPrevVeri1NotaFiscal: TIntegerField;
    QrPrevVeri1TERCEIRO: TIntegerField;
    QrPrevVeri1NO_TERCEIRO: TWideStringField;
    QrPrevVeri1Descricao: TWideStringField;
    QrPrevVeri1Motivo: TSmallintField;
    QrPrevVeri1Ativo: TSmallintField;
    QrPrevVeri1DC: TWideStringField;
    QrPrevVeri0: TmySQLQuery;
    DsPrevVeri0: TDataSource;
    QrPrevVeri0ProvRat: TSmallintField;
    QrPrevVeri0Controle: TIntegerField;
    QrPrevVeri0Data: TDateField;
    QrPrevVeri0Valor: TFloatField;
    QrPrevVeri0Vencimento: TDateField;
    QrPrevVeri0Documento: TFloatField;
    QrPrevVeri0Compensado: TDateField;
    QrPrevVeri0NotaFiscal: TIntegerField;
    QrPrevVeri0TERCEIRO: TIntegerField;
    QrPrevVeri0NO_TERCEIRO: TWideStringField;
    QrPrevVeri0Descricao: TWideStringField;
    QrPrevVeri0Motivo: TSmallintField;
    QrPrevVeri0Ativo: TSmallintField;
    QrPrevVeri0DC: TWideStringField;
    QrPrevVeri2: TmySQLQuery;
    DsPrevVeri2: TDataSource;
    DBGrid0: TdmkDBGridDAC;
    QrPrevVeri1PrevBaC_Cod: TIntegerField;
    QrPrevVeri1PrevBaC_Txt: TWideStringField;
    QrLctDC: TWideStringField;
    QrLctPrevBaC_Cod: TIntegerField;
    QrLctPrevBaC_Txt: TWideStringField;
    QrPrevVeri0PrevBaC_Cod: TIntegerField;
    QrPrevVeri0PrevBaC_Txt: TWideStringField;
    DBGrid1: TdmkDBGridDAC;
    DBGrid2: TdmkDBGridDAC;
    QrPrevVeri2ProvRat: TSmallintField;
    QrPrevVeri2Controle: TIntegerField;
    QrPrevVeri2Data: TDateField;
    QrPrevVeri2Valor: TFloatField;
    QrPrevVeri2Vencimento: TDateField;
    QrPrevVeri2Documento: TFloatField;
    QrPrevVeri2Compensado: TDateField;
    QrPrevVeri2NotaFiscal: TIntegerField;
    QrPrevVeri2TERCEIRO: TIntegerField;
    QrPrevVeri2NO_TERCEIRO: TWideStringField;
    QrPrevVeri2Descricao: TWideStringField;
    QrPrevVeri2Motivo: TSmallintField;
    QrPrevVeri2Ativo: TSmallintField;
    QrPrevVeri2DC: TWideStringField;
    QrPrevVeri2PrevBaC_Cod: TIntegerField;
    QrPrevVeri2PrevBaC_Txt: TWideStringField;
    TPAte: TdmkEditDateTimePicker;
    Label2: TLabel;
    QrPrevVeri1SitCobr: TIntegerField;
    QrPrevVeri0SitCobr: TIntegerField;
    QrPrevVeri2SitCobr: TIntegerField;
    QrPrevVeri1Cond: TIntegerField;
    QrPrevVeri1Texto: TWideStringField;
    QrPrevVeri1Parcelas: TIntegerField;
    QrPrevVeri1ParcPerI: TIntegerField;
    QrPrevVeri1ParcPerF: TIntegerField;
    QrPrevVeri1InfoParc: TIntegerField;
    QrPrevVeri1InfoParc_TXT: TWideStringField;
    QrPrevVeri0Cond: TIntegerField;
    QrPrevVeri0Texto: TWideStringField;
    QrPrevVeri0Parcelas: TIntegerField;
    QrPrevVeri0ParcPerI: TIntegerField;
    QrPrevVeri0ParcPerF: TIntegerField;
    QrPrevVeri0InfoParc: TIntegerField;
    QrPrevVeri0InfoParc_TXT: TWideStringField;
    QrPrevVeri2Cond: TIntegerField;
    QrPrevVeri2Texto: TWideStringField;
    QrPrevVeri2Parcelas: TIntegerField;
    QrPrevVeri2ParcPerI: TIntegerField;
    QrPrevVeri2ParcPerF: TIntegerField;
    QrPrevVeri2InfoParc: TIntegerField;
    QrPrevVeri2InfoParc_TXT: TWideStringField;
    QrPrevVeri1NOMESITCOBR: TWideStringField;
    QrPrevVeri1INICIO: TWideStringField;
    QrPrevVeri1FINAL: TWideStringField;
    QrPrevVeri0NOMESITCOBR: TWideStringField;
    QrPrevVeri0INICIO: TWideStringField;
    QrPrevVeri0FINAL: TWideStringField;
    QrPrevVeri2NOMESITCOBR: TWideStringField;
    QrPrevVeri2INICIO: TWideStringField;
    QrPrevVeri2FINAL: TWideStringField;
    RgSitCobr: TdmkRadioGroup;
    QrLctSitCobr: TIntegerField;
    QrLctCond: TIntegerField;
    QrLctTexto: TWideStringField;
    QrLctParcelas: TIntegerField;
    QrLctParcPerI: TIntegerField;
    QrLctParcPerF: TIntegerField;
    QrLctInfoParc: TIntegerField;
    QrLctPrevCod: TIntegerField;
    QrPrevVeri1DESCRI_SHOW: TWideStringField;
    PMAgenda: TPopupMenu;
    Agrupatodoschecadosemumitem1: TMenuItem;
    Criaitemparacadachecado1: TMenuItem;
    QrLctGenero: TIntegerField;
    QrLctNO_CONTA: TWideStringField;
    QrLctID_Pgto: TIntegerField;
    QrLctSerieCH: TWideStringField;
    QrLctSERIE_CHEQUE: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtPesquisa: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BitBtn1: TBitBtn;
    BtConfigura: TBitBtn;
    BtAgenda: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtConfiguraClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure QrPrevVeri1BeforeClose(DataSet: TDataSet);
    procedure QrPrevVeri1AfterScroll(DataSet: TDataSet);
    procedure QrPrevVeri1CalcFields(DataSet: TDataSet);
    procedure QrPrevVeri1AfterOpen(DataSet: TDataSet);
    procedure BtAgendaClick(Sender: TObject);
    procedure QrLctCalcFields(DataSet: TDataSet);
    procedure Agrupatodoschecadosemumitem1Click(Sender: TObject);
    procedure Criaitemparacadachecado1Click(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaTodos(Status: Integer);
    procedure AgendaChecados_Prepara(Multiplos: Boolean);
    procedure AgendaChecados_Mul();
    procedure AgendaChecados_Uni();
    //
  public
    { Public declarations }
    FCodEnti, FCodCliI: Integer;
    FPrevVeri,
    FTabLctA, FTabPrvA: String;
    //
    procedure ReopenPrevVeri();
  end;

  var
  FmPrevVeri: TFmPrevVeri;

implementation

uses Module, ModuleGeral, UCreate, UMySQLModule, UnInternalConsts,
  UnMyObjects, MyDBCheck, DmkDAC_PF, UnBloquetos_Jan;

{$R *.DFM}

procedure TFmPrevVeri.AgendaChecados_Prepara(Multiplos: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLct, DModG.MyPID_DB, [
    'SELECT * FROM ' + FPrevVeri,
    'WHERE Ativo=1 AND PrevBaC_Cod=0 ',
    '']);
  if QrLct.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(QrLct.RecordCount) +
      ' lan�amentos selecionados sem provis�o base definida!');
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLct, DModG.MyPID_DB, [
    'SELECT * FROM ' + FPrevVeri,
    'WHERE Ativo=1 AND Valor=0 ',
    '']);
  if QrLct.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(QrLct.RecordCount) +
      ' lan�amentos selecionados sem valor definido!');
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLct, DModG.MyPID_DB, [
    'SELECT * FROM ' + FPrevVeri,
    'WHERE Ativo=1 ',
    '']);
  //
  PB1.Visible  := True;
  PB1.Max      := QrLct.RecordCount;
  PB1.Position := 0;
  PB1.Update;
  Application.ProcessMessages;
  //
  try
    if Multiplos then
      AgendaChecados_Mul()
    else
      AgendaChecados_Uni();
  finally
    BtPesquisaClick(Self);
  end;
end;

procedure TFmPrevVeri.AgendaChecados_Uni();
const
  PrevCod = 0;
var
  Controle, Codigo, Lancto, Cond, SitCobr,
  Parcelas, ParcPerI, ParcPerF, InfoParc: Integer;
  Valor: Double;
  Texto: String;
begin
  QrLct.First;
  while not QrLct.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    Codigo   := QrLctPrevBaC_Cod.Value;
    Valor    := QrLctValor.Value;
    Texto    := QrLctDescricao.Value;
    Cond     := FCodCliI;
    SitCobr  := QrLctSitCobr.Value;
    Parcelas := QrLctParcelas.Value;
    ParcPerI := QrLctParcPerI.Value;
    ParcPerF := QrLctParcPerF.Value;
    InfoParc := QrLctInfoParc.Value;
    Controle := UMyMod.BuscaEmLivreY_Def('prevbai', 'Controle', stIns, 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prevbai', False, [
      'Codigo', 'Cond', 'Valor', 'Texto', 'SitCobr', 'Parcelas',
      'ParcPerI', 'ParcPerF', 'InfoParc', 'PrevCod'], ['Controle'], [
      Codigo, Cond, Valor, Texto, SitCobr, Parcelas,
      ParcPerI, ParcPerF, InfoParc, PrevCod], [Controle], True) then
    begin
      Lancto := QrLctControle.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prevbailct', False,
        ['Controle'], ['Lancto'], [Controle], [Lancto], True);
    end;
    //
    QrLct.Next;
  end;
end;

procedure TFmPrevVeri.Agrupatodoschecadosemumitem1Click(Sender: TObject);
begin
  AgendaChecados_Prepara(True);
end;

procedure TFmPrevVeri.AgendaChecados_Mul();
begin
  UBloquetos_Jan.MostraPrevBaCLctos('PrevVeri');
end;

procedure TFmPrevVeri.AtualizaTodos(Status: Integer);
var
  Motivo: Integer;
begin
  case PageControl1.ActivePageIndex of
    0: Motivo := 1;
    1: Motivo := 0;
    2: Motivo := 2;
    else Motivo := -1000;
  end;
  if UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'UPDATE ' + FPrevVeri + ' SET Ativo=' + Geral.FF0(Status),
    'WHERE Motivo=' + Geral.FF0(Motivo),
    '']) then
  begin
    ReopenPrevVeri();
  end;
end;

procedure TFmPrevVeri.BitBtn1Click(Sender: TObject);
var
  Controle: Integer;
begin
  case PageControl1.ActivePageIndex of
      0: Controle := QrPrevVeri1Controle.Value;
      1: Controle := QrPrevVeri0Controle.Value;
      2: Controle := QrPrevVeri2Controle.Value;
    else Controle := 0;
  end;
  if Controle > 0 then
    UBloquetos_Jan.MostraPrevVeriBaC(Controle);
end;

procedure TFmPrevVeri.BtAgendaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAgenda, BtAgenda);
end;

procedure TFmPrevVeri.BtConfiguraClick(Sender: TObject);
var
  Controle: Integer;
  Query: TmySQLQuery;
begin
  case PageControl1.ActivePageIndex of
      0: Controle := QrPrevVeri1Controle.Value;
      1: Controle := QrPrevVeri0Controle.Value;
      2: Controle := QrPrevVeri2Controle.Value;
    else Controle := 0;
  end;
  if Controle > 0 then
  begin
    case PageControl1.ActivePageIndex of
        0: Query := QrPrevVeri1;
        1: Query := QrPrevVeri0;
        2: Query := QrPrevVeri2;
      else Query := nil;
    end;
    UBloquetos_Jan.MostraPrevBaI(Query);
    ReopenPrevVeri();
  end;
end;

procedure TFmPrevVeri.BtPesquisaClick(Sender: TObject);
var
  Desde, Ate, Valores: String;
  Motivo, Ativo: Byte;
begin
  if MyObjects.FIC((TPAte.Date) < Int(TPDesde.Date), TPDesde,
    'Data final inferior a inicial!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    FPrevVeri := UCriar.RecriaTempTableNovo(ntrttPrevVeri, DmodG.QrUpdPID1, False);
    Desde := Geral.FDT(TPDesde.Date, 1);
    Ate := Geral.FDT(TPAte.Date, 1);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO '+ FPrevVeri);
    DModG.QrUpdPID1.SQL.Add('SELECT cta.ProvRat, lct.Controle, lct.Data,');
    DModG.QrUpdPID1.SQL.Add('lct.Debito-lct.Credito Valor, lct.Vencimento,');
    DModG.QrUpdPID1.SQL.Add('lct.Documento, lct.Compensado, lct.NotaFiscal,');
    DModG.QrUpdPID1.SQL.Add('IF(lct.Fornecedor > 0, lct.Fornecedor, ');
    DModG.QrUpdPID1.SQL.Add('lct.Cliente) TERCEIRO, IF(lct.Compensado = 0, ');
    DModG.QrUpdPID1.SQL.Add('"", DATE_FORMAT(lct.Compensado, "%d/%m/%y")) DC,');
    DModG.QrUpdPID1.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ');
    DModG.QrUpdPID1.SQL.Add('NO_TERCEIRO, bac.Codigo PrevBaC_Cod,  bac.Nome ');
    DModG.QrUpdPID1.SQL.Add('PrevBaC_Txt, lct.Descricao, 0 Motivo, 3 SitCobr, ');
    DModG.QrUpdPID1.SQL.Add(FormatFloat('0', FCodCliI) + ' Cond, "" Texto, ');
    DModG.QrUpdPID1.SQL.Add('0 Parcelas, 0 ParcPerI, 0 ParcPerF, 0 InfoParc, ');
    DModG.QrUpdPID1.SQL.Add('0 PrevCod, lct.Genero, cta.Nome NO_CONTA, ');
    DModG.QrUpdPID1.SQL.Add('lct.ID_Pgto, lct.SerieCH, 0 Ativo');
    DModG.QrUpdPID1.SQL.Add('FROM ' + FTabLctA + ' lct');
    DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira');
    DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=lct.Genero');
    DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=');
    DModG.QrUpdPID1.SQL.Add('  IF(lct.Fornecedor > 0, lct.Fornecedor, lct.Cliente)');
    DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.prevbac bac ON bac.Conta=lct.Genero');
    DModG.QrUpdPID1.SQL.Add('WHERE crt.Codigo > 0');
    DModG.QrUpdPID1.SQL.Add('AND (crt.Tipo <> 1 OR lct.ID_Pgto=0)');
    DModG.QrUpdPID1.SQL.Add('AND crt.ForneceI = ' + FormatFloat('0', FCodEnti));
    DModG.QrUpdPID1.SQL.Add('AND lct.Data BETWEEN "' + Desde + '" AND "' + Ate + '"');
    case RGValores.ItemIndex of
        0: Valores := 'AND lct.Debito > 0';
        1: Valores := 'AND lct.Credito > 0';
      else Valores := '';
    end;
    DModG.QrUpdPID1.SQL.Add(Valores);
    DModG.QrUpdPID1.ExecSQL;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLct, DModG.MyPID_DB, [
      'SELECT * FROM ' + FPrevVeri,
      '']);
    //
    PB1.Visible  := True;
    PB1.Max      := QrLct.RecordCount;
    PB1.Position := 0;
    PB1.Update;
    Application.ProcessMessages;
    //
    QrLct.First;
    while not QrLct.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      Ativo  := 0;
      Motivo := 0;
      //
      if UBloquetos_Jan.AgendamentoDeProvisaoEfetivado(QrLctControle.Value,
        FTabPrvA, False) then
      begin
        Ativo  := 0;
        Motivo := 2;
      end else
      begin
        if QrLctProvRat.Value = 1 then
        begin
          Ativo  := 1;
          Motivo := 1;
        end
      end;
      if (Motivo <> 0) or (Ativo <> 0) then
      begin
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FPrevVeri, False,
          ['Ativo', 'Motivo'], ['Controle'],
          [Ativo, Motivo], [QrLctControle.Value], False);
      end;
      //
      QrLct.Next;
    end;
    ReopenPrevVeri();
    //
    PB1.Visible := False;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrevVeri.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmPrevVeri.Criaitemparacadachecado1Click(Sender: TObject);
begin
  AgendaChecados_Prepara(False);
end;

procedure TFmPrevVeri.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrevVeri.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmPrevVeri.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if (QrPrevVeri1PrevBaC_Cod.Value = 0) then
    Cor := clSilver
  else
    Cor := clBlack;
  //
  MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGrid1), Rect,
    Cor, clWindow, Column.Alignment, Column.Field.DisplayText);
end;

procedure TFmPrevVeri.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPrevVeri.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPDesde.Date    := Date - 60;
  TPAte.Date      := Date;
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPrevVeri.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrevVeri.PageControl1Change(Sender: TObject);
begin
  BtConfigura.Visible := PageControl1.ActivePageIndex = 0;
end;

procedure TFmPrevVeri.QrLctCalcFields(DataSet: TDataSet);
begin
  QrLctSERIE_CHEQUE.Value := QrLctSerieCH.Value +
                               FormatFloat('000000;-0; ', QrLctDocumento.Value);
end;

procedure TFmPrevVeri.QrPrevVeri1AfterOpen(DataSet: TDataSet);
begin
  BtAgenda.Enabled := QrPrevVeri1.RecordCount > 0;
end;

procedure TFmPrevVeri.QrPrevVeri1AfterScroll(DataSet: TDataSet);
begin
  BtConfigura.Enabled := QrPrevVeri1PrevBaC_Cod.Value > 0;
end;

procedure TFmPrevVeri.QrPrevVeri1BeforeClose(DataSet: TDataSet);
begin
  BtConfigura.Enabled := False;
  BtAgenda.Enabled    := False;
end;

procedure TFmPrevVeri.QrPrevVeri1CalcFields(DataSet: TDataSet);
var
  Ini, Fim: String;
begin
  QrPrevVeri1NOMESITCOBR.Value := RgSitCobr.Items[QrPrevVeri1SitCobr.Value];
  case QrPrevVeri1SitCobr.Value of
    1:
    begin
      Ini := dmkPF.VerificaMes(QrPrevVeri1ParcPerI.Value, False);
      Fim := dmkPF.VerificaMes(QrPrevVeri1ParcPerF.Value, False);
    end;
    2:
    begin
      Ini := dmkPF.MesEAnoDoPeriodo(QrPrevVeri1ParcPerI.Value);
      Fim := dmkPF.MesEAnoDoPeriodo(QrPrevVeri1ParcPerF.Value);
    end;
    else
    begin
      Ini := '';
      Fim := '';
    end;
  end;
  QrPrevVeri1INICIO.Value := Ini;
  QrPrevVeri1FINAL.Value  := Fim;
  //
  if QrPrevVeri1InfoParc.Value = 0 then
    QrPrevVeri1InfoParc_TXT.Value := 'N�o'
  else
    QrPrevVeri1InfoParc_TXT.Value := 'Sim';
  //
  if QrPrevVeri1Texto.Value <> '' then
    QrPrevVeri1DESCRI_SHOW.Value := QrPrevVeri1Texto.Value
  else
    QrPrevVeri1DESCRI_SHOW.Value := QrPrevVeri1Descricao.Value;
end;

procedure TFmPrevVeri.ReopenPrevVeri();
var
  Controle0, Controle1, Controle2: Integer;
begin
  if QrPrevVeri0.State <> dsInactive then
    Controle0 := QrPrevVeri0Controle.Value
  else
    Controle0 := 0;
  //
  if QrPrevVeri1.State <> dsInactive then
    Controle1 := QrPrevVeri1Controle.Value
  else
    Controle1 := 0;
  //
  if QrPrevVeri2.State <> dsInactive then
    Controle2 := QrPrevVeri2Controle.Value
  else
    Controle2 := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrPrevVeri0, DModG.MyPID_DB);
  QrPrevVeri0.Locate('Controle', Controle0, []);
  //
  UnDmkDAC_PF.AbreQuery(QrPrevVeri1, DModG.MyPID_DB);
  QrPrevVeri1.Locate('Controle', Controle1, []);
  //
  UnDmkDAC_PF.AbreQuery(QrPrevVeri2, DModG.MyPID_DB);
  QrPrevVeri2.Locate('Controle', Controle2, []);
end;

end.
