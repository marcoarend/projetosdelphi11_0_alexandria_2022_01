unit PrevBaA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, ABSMain, DBGrids,
  dmkDBGrid, DBCtrls, dmkEdit, Mask, mySQLDbTables, dmkGeral, UnDmkProcFunc,
  dmkImage, UnDmkEnums, DmkDAC_PF, UnDmkABS_PF;

type
  TFmPrevBaA = class(TForm)
    Panel1: TPanel;
    Query: TABSQuery;
    DataSource1: TDataSource;
    DBGrid1: TdmkDBGrid;
    QueryConta: TIntegerField;
    QueryNOMECONTA: TWideStringField;
    QueryPrevBaI: TIntegerField;
    QueryPrevBaC: TIntegerField;
    QueryValor: TFloatField;
    QueryTexto: TWideStringField;
    QueryAdiciona: TSmallintField;
    QrSumTot: TABSQuery;
    QrSumTotTOTAL: TFloatField;
    DsSumTot: TDataSource;
    QrSumSel: TABSQuery;
    FloatField1: TFloatField;
    DsSumSel: TDataSource;
    QrSumSdo: TABSQuery;
    FloatField3: TFloatField;
    DsSumSdo: TDataSource;
    QrConta: TABSQuery;
    QrContaZeros: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure QueryAfterOpen(DataSet: TDataSet);
    procedure QueryAfterPost(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure CriaTabela();
    procedure ReopenSum();
    procedure SelecionarVarios(Adiciona: Integer);
  public
    { Public declarations }
    FCadastrar: Boolean;
    FCodigo: Integer;
    FTabelaPriA: String;
    FQueryNIO_A: TmySQLQuery;
  end;

  var
  FmPrevBaA: TFmPrevBaA;

implementation

uses Module, UMySQLModule, UnInternalConsts, UnMyObjects;

{$R *.DFM}

procedure TFmPrevBaA.BtNenhumClick(Sender: TObject);
begin
  SelecionarVarios(0);
end;

procedure TFmPrevBaA.BtOKClick(Sender: TObject);
var
  Controle, Codigo, Conta, PrevBaI, PrevBaC: Integer;
  Valor: Double;
  Texto: String;
begin
  Controle := 0;
  //
  DmkABS_PF.AbreABSQuery0(QrConta, 'MEMORY', [
    'SELECT COUNT(Adiciona) Zeros ',
    'FROM prevbaa ',
    'WHERE Adiciona = 1 ',
    '']);
  if QrContaZeros.Value = 0 then
  begin
    Geral.MB_Aviso('Nenhum item foi selecionado!');
    Exit;
  end;
  DmkABS_PF.AbreABSQuery0(QrConta, 'MEMORY', [
    'SELECT COUNT(Valor) Zeros ',
    'FROM prevbaa ',
    'WHERE Valor < 0.01 AND Adiciona=1 ',
    '']);
  if QrContaZeros.Value > 0 then
  begin
    if Geral.MB_Pergunta('Existem ' + Geral.FF0(QrContaZeros.Value) +
      ' itens selecionados sem valor informado. Deseja continuar assim mesmo?') <> ID_YES
    then
      Exit;
  end;
  Query.First;
  while not Query.Eof do
  begin
    if QueryAdiciona.Value = 1 then
    begin
      Codigo  := FCodigo;
      Conta   := QueryConta.Value;
      Valor   := QueryValor.Value;
      PrevBaI := QueryPrevBaI.Value;
      PrevBaC := QueryPrevBaC.Value;
      Texto   := QueryTexto.Value;
      //
      Controle := UMyMod.BuscaEmLivreY_Def(TAB_PRI, 'Controle', stIns, 0);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, FTabelaPriA, False,
        ['Codigo', 'Conta', 'Valor', 'PrevBaI', 'Texto', 'PrevBaC'], ['Controle'],
        [Codigo, Conta, Valor, PrevBaI, Texto, PrevBaC], [Controle], True) then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'prevbai', False,
          ['prevcod'], ['Controle'], [Codigo], [PrevBaI], True);
      end;
    end;
    Query.Next;
  end;
  Close;
end;

procedure TFmPrevBaA.BtSaidaClick(Sender: TObject);
begin
  FCadastrar := False;
  Close;
end;

procedure TFmPrevBaA.BtTodosClick(Sender: TObject);
begin
  SelecionarVarios(1);
end;

procedure TFmPrevBaA.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
begin
  if Column.FieldName = 'Adiciona' then
  begin
    if Query.FieldByName('Adiciona').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    Query.Edit;
    Query.FieldByName('Adiciona').Value := Status;
    Query.Post;
  end;
end;

procedure TFmPrevBaA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPrevBaA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCadastrar := False;
end;

procedure TFmPrevBaA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrevBaA.FormShow(Sender: TObject);
begin
  CriaTabela();
end;

procedure TFmPrevBaA.QueryAfterOpen(DataSet: TDataSet);
begin
  ReopenSum;
end;

procedure TFmPrevBaA.QueryAfterPost(DataSet: TDataSet);
begin
  ReopenSum;
end;

procedure TFmPrevBaA.ReopenSum;
begin
  DmkABS_PF.AbreQuery(QrSumTot);
  DmkABS_PF.AbreQuery(QrSumSel);
  DmkABS_PF.AbreQuery(QrSumSdo);
end;

procedure TFmPrevBaA.SelecionarVarios(Adiciona: Integer);
begin
  Query.First;
  while not Query.Eof do
  begin
    Query.Edit;
    QueryAdiciona.Value := Adiciona;
    Query.Post;
    //
    Query.Next;
  end;
end;

procedure TFmPrevBaA.CriaTabela();

  procedure InsereItemAtual();
  begin
    Query.SQL.Add('INSERT INTO prevbaa (' +
      'Conta, NOMECONTA, PrevBaI, PrevBaC, Valor, Texto, Adiciona) VALUES (' +
      Geral.FF0(FQueryNIO_A.FieldByName('Conta').AsInteger) + ',' +
      '"' + FQueryNIO_A.FieldByName('NOMECON').AsString + '",' +
      Geral.FF0(FQueryNIO_A.FieldByName('Controle').AsInteger) + ',' +
      Geral.FF0(FQueryNIO_A.FieldByName('Codigo').AsInteger) + ',' +
      dmkPF.FFP(FQueryNIO_A.FieldByName('Valor').AsFloat, 2) + ',' +
      '"' + FQueryNIO_A.FieldByName('Texto').AsString + '",' +
      '1);');
  end;

begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE prevbaa; ');
  Query.SQL.Add('CREATE TABLE prevbaa (');
  Query.SQL.Add('  Conta     integer       ,');
  Query.SQL.Add('  NOMECONTA varchar(100)  ,');
  Query.SQL.Add('  PrevBaI   integer       ,');
  Query.SQL.Add('  PrevBaC   integer       ,');
  Query.SQL.Add('  Valor     float         ,');
  Query.SQL.Add('  Texto     varchar(40)   ,');
  Query.SQL.Add('  Adiciona  smallint       ');
  Query.SQL.Add(');');
  //
  while not FQueryNIO_A.Eof do
  begin
    InsereItemAtual();
    //
    FQueryNIO_A.Next;
  end;
  Query.SQL.Add('SELECT * FROM prevbaa;');
  DmkABS_PF.AbreQuery(Query);
end;

end.

