unit PrevVeriBaC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmPrevVeriBaC = class(TForm)
    Panel1: TPanel;
    QrPrevBaC: TmySQLQuery;
    QrPrevBaCCodigo: TIntegerField;
    QrPrevBaCNome: TWideStringField;
    QrPrevBaCConta: TIntegerField;
    DsPrevBac: TDataSource;
    Label1: TLabel;
    EdPrevBaC: TdmkEditCB;
    CBPrevBac: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FControle: Integer;
  end;

  var
  FmPrevVeriBaC: TFmPrevVeriBaC;

implementation

uses PrevBaC, PrevVeri, ModuleGeral, UMySQLModule, MyDBCheck, UnInternalConsts,
UnMyObjects;

{$R *.DFM}

procedure TFmPrevVeriBaC.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Codigo := EdPrevBac.ValueVariant;
  Nome   := CBPrevBac.Text;
  //
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FmPrevVeri.FPrevVeri, False,
    ['PrevBaC_Cod', 'PrevBaC_Txt'], ['Controle'],
    [Codigo, Nome], [FControle], False);
  //
  FmPrevVeri.ReopenPrevVeri();
  Close;
end;

procedure TFmPrevVeriBaC.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrevVeriBaC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPrevVeriBaC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrPrevBaC.Open;
end;

procedure TFmPrevVeriBaC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrevVeriBaC.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmPrevBaC, FmPrevBaC, afmoNegarComAviso) then
  begin
    FmPrevBaC.ShowModal;
    FmPrevBaC.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      QrPrevBaC.Close;
      QrPrevBaC.Open;
      //
      EdPrevBaC.ValueVariant := VAR_CADASTRO;
      CBPrevBac.KeyValue     := VAR_CADASTRO;
      EdPrevBaC.SetFocus;
    end;
  end;
end;

end.
