object FmCondGerArreUni: TFmCondGerArreUni
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-024 :: Item de Arrecada'#231#227'o'
  ClientHeight = 534
  ClientWidth = 545
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 545
    Height = 335
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label2: TLabel
      Left = 15
      Top = 121
      Width = 38
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Conta:'
    end
    object Label13: TLabel
      Left = 15
      Top = 234
      Width = 139
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o [F4 : F5 : F6]:'
    end
    object LaDeb: TLabel
      Left = 420
      Top = 234
      Width = 35
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Valor:'
    end
    object Label1: TLabel
      Left = 15
      Top = 15
      Width = 55
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Unidade:'
    end
    object Label4: TLabel
      Left = 15
      Top = 68
      Width = 143
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Configura'#231#227'o do boleto:'
    end
    object LaNFSeSrvCad: TLabel
      Left = 15
      Top = 176
      Width = 80
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Regra Fiscal:'
    end
    object SBNFSeSrvCad: TSpeedButton
      Left = 502
      Top = 197
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SBNFSeSrvCadClick
    end
    object EdConta: TdmkEditCB
      Left = 15
      Top = 140
      Width = 65
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBConta
      IgnoraDBLookupComboBox = False
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 84
      Top = 140
      Width = 444
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 5
      dmkEditCB = EdConta
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDescricao: TdmkEdit
      Left = 15
      Top = 254
      Width = 400
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnKeyDown = EdDescricaoKeyDown
    end
    object EdValor: TdmkEdit
      Left = 420
      Top = 254
      Width = 108
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdValorExit
    end
    object CkContinuar: TCheckBox
      Left = 20
      Top = 288
      Width = 144
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 10
    end
    object EdApto: TdmkEditCB
      Left = 15
      Top = 34
      Width = 65
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBApto
      IgnoraDBLookupComboBox = False
    end
    object CBApto: TdmkDBLookupComboBox
      Left = 84
      Top = 34
      Width = 444
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Color = clWhite
      KeyField = 'Conta'
      ListField = 'Unidade'
      ListSource = DsAptos
      TabOrder = 1
      dmkEditCB = EdApto
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCNAB_Cfg: TdmkEditCB
      Left = 15
      Top = 89
      Width = 65
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCNAB_Cfg
      IgnoraDBLookupComboBox = False
    end
    object CBCNAB_Cfg: TdmkDBLookupComboBox
      Left = 84
      Top = 89
      Width = 444
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCNAB_Cfg
      TabOrder = 3
      dmkEditCB = EdCNAB_Cfg
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdNFSeSrvCad: TdmkEditCB
      Left = 15
      Top = 197
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBNFSeSrvCad
      IgnoraDBLookupComboBox = False
    end
    object CBNFSeSrvCad: TdmkDBLookupComboBox
      Left = 84
      Top = 197
      Width = 414
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsNFSeSrvCad
      TabOrder = 7
      dmkEditCB = EdNFSeSrvCad
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 545
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 486
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 427
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 294
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Arrecada'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 294
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Arrecada'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 294
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Arrecada'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 394
    Width = 545
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 541
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 448
    Width = 545
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 18
      Width = 541
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 368
        Top = 0
        Width = 172
        Height = 65
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 10
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 162
        Top = 4
        Width = 213
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Ver dados atr'#225's'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = BitBtn1Click
      end
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.*, sg.Nome NOMESUBGRUPO ,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1'
      'WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMEEMPRESA'
      'FROM contas co, Subgrupos sg,'
      'Grupos gr, Conjuntos cj, Entidades cl'
      'WHERE sg.Codigo=co.Subgrupo'
      'AND gr.Codigo=sg.Grupo'
      'AND cj.Codigo=gr.Conjunto'
      'AND cl.Codigo=co.Empresa'
      'AND co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 156
    Top = 288
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 184
    Top = 288
  end
  object QrAptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imv.Andar, imv.Unidade, imv.Conta, imv.Propriet,'
      'con.Codigo, con.Cliente'
      'FROM condimov imv'
      'LEFT JOIN cond con ON con.Codigo = imv.Codigo'
      'WHERE imv.Codigo=:P0'
      'ORDER BY imv.Andar, imv.Unidade'
      '')
    Left = 268
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAptosUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrAptosConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrAptosPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrAptosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrAptosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsAptos: TDataSource
    DataSet = QrAptos
    Left = 296
    Top = 288
  end
  object QrCNAB_Cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM CNAB_Cfg'
      'WHERE Cedente=:P0')
    Left = 212
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 240
    Top = 288
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 356
    Top = 16
  end
  object QrNFSeSrvCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM nfsesrvcad'
      'ORDER BY Nome')
    Left = 324
    Top = 288
    object QrNFSeSrvCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeSrvCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsNFSeSrvCad: TDataSource
    DataSet = QrNFSeSrvCad
    Left = 352
    Top = 288
  end
end
