unit PrevBaCLctos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DB, DBGrids, dmkDBGrid,
  DBCtrls, mySQLDbTables, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral,
  UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmPrevBaCLctos = class(TForm)
    Panel1: TPanel;
    DsAgeLct: TDataSource;
    DBGrid1: TdmkDBGrid;
    Panel3: TPanel;
    EdPrevBaC: TdmkEditCB;
    CBPrevBac: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrPrevBaC: TmySQLQuery;
    DsPrevBac: TDataSource;
    QrPrevBaCCodigo: TIntegerField;
    QrPrevBaCNome: TWideStringField;
    EdValor: TdmkEdit;
    Label2: TLabel;
    QrPrevBaCConta: TIntegerField;
    SpeedButton1: TSpeedButton;
    Label11: TLabel;
    EdTexto: TdmkEdit;
    QrPesq: TmySQLQuery;
    QrAgeLct: TmySQLQuery;
    QrAgeLctControle: TIntegerField;
    QrAgeLctSerieDoc: TWideStringField;
    QrAgeLctDescricao: TWideStringField;
    QrAgeLctNomeCliFor: TWideStringField;
    QrAgeLctDebito: TFloatField;
    QrAgeLctNF: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FAgeLct: String;
    procedure InsereItens_CondGer();
    procedure InsereItens_PrevVeri();
    //
    procedure DefinePrevBaC(Genero: Integer; NOMECONTA: String);
    procedure InsereItemAtual(const Controle, ID_Pgto,
              NotaFiscal: Integer; const Debito: Double;
              SERIE_CHEQUE, Descricao, NOMERELACIONADO: String; var Valor: Double);
  public
    { Public declarations }
    FForm: String;
  end;

  var
  FmPrevBaCLctos: TFmPrevBaCLctos;

implementation

{$R *.DFM}

uses MyDBCheck, CondGer, Principal, Module, PrevBaC, UnInternalConsts,
UMySQLModule, ModuleCond, Ucreate, ModuleGeral, ModuleLct2, PrevVeri,
UnMyObjects;

procedure TFmPrevBaCLctos.BtOKClick(Sender: TObject);
var
  Codigo, Cond, SitCobr, Controle, Parcelas, ParcPerI, ParcPerF, InfoParc,
  PrevCod, Lancto: Integer;

  Valor: Double;

  Texto: String;
begin
  Codigo := EdPrevBaC.ValueVariant;
  Valor  := EdValor.ValueVariant;
  //
  if Codigo = 0 then
  begin
    Geral.MB_Aviso('Informe a provis�o base!');
    Exit;
  end;
  if Valor <= 0 then
  begin
    Geral.MB_Aviso('Informe o Valor!');
    Exit;
  end;
  Cond     := DmCond.QrCondCodigo.Value;
  Texto    := EdTexto.Text;
  SitCobr  := 3; // Por agendamento
  Parcelas := 1;
  ParcPerI := 0;
  ParcPerF := 0;
  InfoParc := 0;
  PrevCod  := 0;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('prevbai', 'Controle', stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prevbai', False, [
    'Codigo', 'Cond', 'Valor', 'Texto', 'SitCobr', 'Parcelas',
    'ParcPerI', 'ParcPerF', 'InfoParc', 'PrevCod'
  ], ['Controle'], [
    Codigo, Cond, Valor, Texto, SitCobr, Parcelas,
    ParcPerI, ParcPerF, InfoParc, PrevCod
  ], [Controle], True) then
  begin
    QrAgeLct.First;
    while not QrAgeLct.Eof do
    begin
      Lancto := QrAgeLctControle.Value;
      try
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prevbailct', False, [
        'Controle'], ['Lancto'], [Controle], [Lancto], True);
      finally
        QrAgeLct.Next;
      end;
    end;
    Close;
  end;
end;

procedure TFmPrevBaCLctos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrevBaCLctos.DefinePrevBaC(Genero: Integer; NOMECONTA: String);
begin
  if QrPrevBaC.Locate('Conta', Genero(*FmPrincipal.QrLctGenero.Value*), []) then
  begin
    EdPrevBaC.ValueVariant := QrPrevBaCCodigo.Value;
    CBPrevBac.KeyValue     := QrPrevBaCCodigo.Value;
  end else
    Geral.MB_Aviso('N�o foi localizado nenhuma ' +
      'provis�o para a conta "' + NOMECONTA(*FmPrincipal.QrLctNOMECONTA.Value*) + '". ' +
      '� necess�rio cadastr�-la!');
end;

procedure TFmPrevBaCLctos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if (FForm = 'CondGer') or (FForm = 'LctGer2') then
    InsereItens_CondGer()
  else
  if FForm = 'PrevVeri' then
    InsereItens_PrevVeri()
  else
    Geral.MB_Aviso('Janela n�o implementada para defini��o de provis�o:' +
      sLineBreak + '"' + FForm + '"');
end;

procedure TFmPrevBaCLctos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrAgeLct.Close;
  QrAgeLct.Database := DModG.MyPID_DB;
  QrPrevBaC.Open;
end;

procedure TFmPrevBaCLctos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrevBaCLctos.InsereItemAtual(const Controle, ID_Pgto,
  NotaFiscal: Integer; const Debito: Double; SERIE_CHEQUE, Descricao,
  NOMERELACIONADO: String; var Valor: Double);
var
  Acao: Integer;
begin
 // Acao := 0;
  QrPesq.Close;
  QrPesq.Params[0].AsInteger := Controle;//FmPrincipal.QrLctControle.Value;
  QrPesq.Open;
  if QrPesq.RecordCount = 0 then
  begin
    if ID_Pgto(*FmPrincipal.QrLctID_Pgto.Value*) <> 0 then
    begin
      QrPesq.Close;
      QrPesq.Params[0].AsInteger := ID_Pgto;//FmPrincipal.QrLctID_Pgto.Value;
      QrPesq.Open;
      if QrPesq.RecordCount = 0 then Acao := 1 else Acao := 3;
    end else Acao := 1;
  end else Acao := 2;
  case Acao of
    1:
    begin
      Valor := Valor + Debito(*FmPrincipal.QrLctDebito.Value*);
      QrAgeLct.SQL.Add('INSERT INTO agelct (' +
      'Controle, SerieDoc, Descricao, NomeCliFor, Debito, NF) VALUES (' +
        FormatFloat('0', Controle(*FmPrincipal.QrLctControle.Value*)) + ',' +
        '"' + SERIE_CHEQUE(*FmPrincipal.QrLctSERIE_CHEQUE.Value*) + '",' +
        '"' + Descricao(*FmPrincipal.QrLctDescricao.Value*) + '",' +
        '"' + NOMERELACIONADO(*FmPrincipal.QrLctNOMERELACIONADO.Value*) + '",' +
        dmkPF.FFP(Debito(*FmPrincipal.QrLctDebito.Value*), 2) + ',' +
        FormatFloat('0', NotaFiscal(*FmPrincipal.QrLctNotaFiscal.Value*)) + ');');
    end;
    2: Geral.MB_Aviso('O lan�amento ' + Geral.FF0(Controle) +
        ' n�o ser� considerado, pois a emiss�o dele j� pertence a uma provis�o!');
    3: Geral.MB_Aviso('O lan�amento ' + Geral.FF0(Controle) +
        ' n�o ser� considerado, pois ele j� pertence a uma provis�o!');
    else Geral.MB_Erro('A��o n�o definida na inclus�o de item!');
  end;
end;

procedure TFmPrevBaCLctos.InsereItens_CondGer();
  procedure InsereItemAtual(var Valor: Double);
  var
    Acao: Integer;
  begin
   // Acao := 0;
    QrPesq.Close;
    QrPesq.Params[0].AsInteger := DmLct2.QrLctControle.Value;
    QrPesq.Open;
    if QrPesq.RecordCount = 0 then
    begin
      if DmLct2.QrLctID_Pgto.Value <> 0 then
      begin
        QrPesq.Close;
        QrPesq.Params[0].AsInteger := DmLct2.QrLctID_Pgto.Value;
        QrPesq.Open;
        if QrPesq.RecordCount = 0 then Acao := 1 else Acao := 3;
      end else Acao := 1;
    end else Acao := 2;
    case Acao of
      1:
      begin
        Valor := Valor + DmLct2.QrLctDebito.Value;
        QrAgeLct.SQL.Add('INSERT INTO agelct (' +
        'Controle, SerieDoc, Descricao, NomeCliFor, Debito, NF) VALUES (' +
          FormatFloat('0', DmLct2.QrLctControle.Value) + ',' +
          '"' + DmLct2.QrLctSERIE_CHEQUE.Value + '",' +
          '"' + DmLct2.QrLctDescricao.Value + '",' +
          '"' + DmLct2.QrLctNOMERELACIONADO.Value + '",' +
          dmkPF.FFP(DmLct2.QrLctDebito.Value, 2) + ',' +
          FormatFloat('0', DmLct2.QrLctNotaFiscal.Value) + ');');
      end;
      2: Geral.MB_Aviso('O lan�amento ' +
          Geral.FF0(DmLct2.QrLctControle.Value) + ' n�o ser� considerado, ' +
          'pois a emiss�o dele j� pertence a uma provis�o!');
      3: Geral.MB_Aviso('O lan�amento ' +
          Geral.FF0(DmLct2.QrLctControle.Value) + ' n�o ser� considerado, ' +
          'pois ele j� pertence a uma provis�o!');
      else Geral.MB_Erro('A��o n�o definida na inclus�o de item!');
    end;
  end;
  procedure DefinePrevBAC;
  begin
    if QrPrevBaC.Locate('Conta', DmLct2.QrLctGenero.Value, []) then
    begin
      EdPrevBaC.ValueVariant := QrPrevBaCCodigo.Value;
      CBPrevBac.KeyValue     := QrPrevBaCCodigo.Value;
    end else
      Geral.MB_Aviso('N�o foi localizado nenhuma ' +
        'provis�o para a conta "' + DmLct2.QrLctNOMECONTA.Value + '". ' +
        '� necess�rio cadastr�-la!');
  end;
var
  i: Integer;
  v: Double;
begin
  v := 0;
  //
  FAgeLct := UCriar.RecriaTempTable('AgeLct', DmodG.QrUpdPID1, False);
  QrAgeLct.SQL.Clear;
  //
  if FmCondGer.DBGLct.SelectedRows.Count > 1 then
  begin
    with FmCondGer.DBGLct.DataSource.DataSet do
    for i:= 0 to FmCondGer.DBGLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(FmCondGer.DBGLct.SelectedRows.Items[i]));
      if i = 0 then DefinePrevBaC;
      InsereItemAtual(v);
    end;
  end else begin
    InsereItemAtual(v);
    DefinePrevBaC;
  end;
  QrAgeLct.SQL.Add('SELECT * FROM agelct;');
  QrAgeLct.Open;
  EdValor.ValueVariant := v;
  EdTexto.Text := QrAgeLctDescricao.Value;
end;

procedure TFmPrevBaCLctos.InsereItens_PrevVeri();
var
  v: Double;
begin
  v := 0;
  //
  FAgeLct := UCriar.RecriaTempTable('AgeLct', DmodG.QrUpdPID1, False);
  QrAgeLct.SQL.Clear;
  //
  FmPrevVeri.QrLct.First;
  DefinePrevBaC(FmPrevVeri.QrLctGenero.Value, FmPrevVeri.QrLctNO_CONTA.Value);
  while not FmPrevVeri.QrLct.Eof do
  begin
    InsereItemAtual(FmPrevVeri.QrLctControle.Value, FmPrevVeri.QrLctID_Pgto.Value,
    FmPrevVeri.QrLctNotaFiscal.Value, FmPrevVeri.QrLctValor.Value,
    FmPrevVeri.QrLctSERIE_CHEQUE.Value, FmPrevVeri.QrLctDescricao.Value,
    FmPrevVeri.QrLctNO_TERCEIRO.Value, v);
    //
    FmPrevVeri.QrLct.Next;
  end;
  QrAgeLct.SQL.Add('SELECT * FROM agelct;');
  QrAgeLct.Open;
  EdValor.ValueVariant := v;
  EdTexto.Text := QrAgeLctDescricao.Value;
end;

procedure TFmPrevBaCLctos.SpeedButton1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPrevBaC, FmPrevBaC, afmoNegarComAviso) then
  begin
    VAR_CADASTRO := 0;
    FmPrevBaC.ShowModal;
    FmPrevBaC.Destroy;
    if VAR_CADASTRO > 0 then
    begin
      QrPrevBaC.Close;
      QrPrevBaC.Open;
      //
      EdPrevBaC.ValueVariant := VAR_CADASTRO;
      CBPrevBac.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

end.

