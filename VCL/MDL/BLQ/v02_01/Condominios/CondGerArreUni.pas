unit CondGerArreUni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, Variants,
  dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, DmkDAC_PF,
  UnDmkEnums;

type
  TFmCondGerArreUni = class(TForm)
    Panel1: TPanel;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    DsContas: TDataSource;
    Label2: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    LaDeb: TLabel;
    EdValor: TdmkEdit;
    CkContinuar: TCheckBox;
    Label1: TLabel;
    EdApto: TdmkEditCB;
    CBApto: TdmkDBLookupComboBox;
    QrAptos: TmySQLQuery;
    DsAptos: TDataSource;
    QrAptosUnidade: TWideStringField;
    QrAptosConta: TIntegerField;
    QrAptosPropriet: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    Label4: TLabel;
    EdCNAB_Cfg: TdmkEditCB;
    CBCNAB_Cfg: TdmkDBLookupComboBox;
    QrCNAB_Cfg: TmySQLQuery;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    DsCNAB_Cfg: TDataSource;
    QrAptosCliente: TIntegerField;
    QrAptosCodigo: TIntegerField;
    QrLoc: TmySQLQuery;
    LaNFSeSrvCad: TLabel;
    EdNFSeSrvCad: TdmkEditCB;
    CBNFSeSrvCad: TdmkDBLookupComboBox;
    SBNFSeSrvCad: TSpeedButton;
    QrNFSeSrvCad: TmySQLQuery;
    QrNFSeSrvCadCodigo: TIntegerField;
    QrNFSeSrvCadNome: TWideStringField;
    DsNFSeSrvCad: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SBNFSeSrvCadClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCond, FCliente, FApto, FPropriet, FControle, FLancto, FCodigo: Integer;
    FPrevPeriodo, FTabLctA, FTabAriA: String;
  end;

  var
  FmCondGerArreUni: TFmCondGerArreUni;

implementation

uses Module, UnInternalConsts, UMySQLModule, UnMyObjects, UnBloquetos,
  {$IfNDef sNFSe} NFSe_PF_0000, {$EndIf}
  UnBloqGerl, MyDBCheck, MyListas, ModuleGeral, UnDmkProcFunc;

{$R *.DFM}

procedure TFmCondGerArreUni.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerArreUni.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerArreUni.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerArreUni.FormShow(Sender: TObject);
begin
  UBloqGerl.ReopenCNAB_Cfg(QrCNAB_Cfg, Dmod.MyDB, FCliente);
end;

procedure TFmCondGerArreUni.SBNFSeSrvCadClick(Sender: TObject);
begin
  {$IfNDef sNFSe}
  VAR_CADASTRO := 0;
  //
  UnNFSe_PF_0000.MostraFormNFSeSrvCad(EdNFSeSrvCad.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdNFSeSrvCad, CBNFSeSrvCad, QrNFSeSrvCad, VAR_CADASTRO);
    //
    EdNFSeSrvCad.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmCondGerArreUni.EdValorExit(Sender: TObject);
begin
  EdValor.Text := Geral.TFT(EdValor.Text, 2, siNegativo);
end;

procedure TFmCondGerArreUni.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if QrContasMensal.Value = 'V' then
      Mes := FPrevPeriodo
    else
      Mes := '';
    if key=VK_F4 then
      EdDescricao.ValueVariant := QrContasNome.Value + ' ' + Mes;
    if key=VK_F5 then
      EdDescricao.ValueVariant := QrContasNome2.Value + ' ' + Mes;
    if key=VK_F6 then
      EdDescricao.ValueVariant := QrContasNome3.Value + ' ' + Mes;
    //
    EdDescricao.SetFocus;
    EdDescricao.SelStart := Length(EdDescricao.Text);
    EdDescricao.SelLength := 0;
  end;
end;

procedure TFmCondGerArreUni.BtOKClick(Sender: TObject);
  procedure AtualizaValorLct(AriControle: Integer; AriTabela: String; Valor: Double);
  var
    Lancto: Integer;
  begin
    if UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Lancto ',
    'FROM ' + AriTabela,
    'WHERE Controle=' + Geral.FF0(AriControle),
    '']) then
    begin
      Lancto := Dmod.QrAux.FieldByName('Lancto').AsInteger;
      //
      if Lancto <> 0 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, FTabLctA, False,
          ['Credito'], ['FatID', 'Controle'], [Valor], [600, Lancto], True);
      end;
    end;
  end;
var
  Apto, CNAB_Cfg, NFSeSrvCad, Propriet, Conta, Controle: Integer;
  Texto, Msg: String;
  Valor: Double;
begin
  Apto       := EdApto.ValueVariant;
  Conta      := EdConta.ValueVariant;
  Texto      := EdDescricao.ValueVariant;
  Valor      := EdValor.ValueVariant;
  CNAB_Cfg   := EdCNAB_Cfg.ValueVariant;
  NFSeSrvCad := EdNFSeSrvCad.ValueVariant;
  //
  if MyObjects.FIC(Apto = 0, EdApto, 'Unidade n�o definida!') then Exit;
  if MyObjects.FIC(Conta = 0, EdConta, 'Conta n�o definida!') then Exit;
  if MyObjects.FIC(Texto = '', EdDescricao, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Valor = 0, EdValor, 'Valor n�o definido!') then Exit;
  if MyObjects.FIC(CNAB_Cfg = 0, EdCNAB_Cfg, 'Defina a configura��o do boleto!') then Exit;
  //
  Msg := '';
  //
  if Valor > 0 then
  begin
    if MyObjects.FIC(UpperCase(QrContasCredito.Value) = 'F', EdConta, 'A conta selecionada deve ser de cr�dito!') then Exit;
  end else
  begin
    if MyObjects.FIC(UpperCase(QrContasDebito.Value) = 'F', EdConta, 'A conta selecionada deve ser de d�bito!') then Exit;
  end;
  //
  if (FLancto <> 0) and (UBloquetos.VerificaSeLctEstaoNaTabLctA(
    FTabLctA, FLancto, QrLoc, Msg) = False) then
  begin
    Geral.MB_Aviso(Msg);
    Exit;
  end;
  Msg := '';
  //
  if (FLancto <> 0) and (not UBloquetos.VerificaSeLancamentoEstaPago(FLancto,
    QrLoc, FTabLctA, Msg)) then
  begin
    Geral.MB_Aviso(Msg);
    Exit;
  end;
  //
  if ImgTipo.SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                  FTabAriA, TAB_ARI, 'Controle');
    Propriet := QrAptosPropriet.Value;
  end else
  begin
    Controle := FControle;
    Apto     := FApto; //Para evitar de pegar o errado na altera��o
    Propriet := FPropriet;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, FTabAriA, False,
    ['Conta', 'Texto', 'Valor', 'Apto', 'Propriet',
    'CNAB_Cfg', 'NFSeSrvCad', 'Codigo'], ['Controle'],
    [Conta, Texto, Valor, Apto, Propriet,
    CNAB_Cfg, NFSeSrvCad, FCodigo], [Controle], True) then
  begin
    AtualizaValorLct(Controle, FTabAriA, Valor);
    //
    if CkContinuar.Checked then
    begin
      EdApto.Text     := '';
      CBApto.KeyValue := NULL;
      //
      CBApto.SetFocus;
      ImgTipo.SQLType := stIns;
    end else
      Close;
  end;
end;

procedure TFmCondGerArreUni.FormCreate(Sender: TObject);
var
  TemNFSe: Boolean;
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
  //
  {$IfNDef sNFSe}
  UMyMod.AbreQuery(QrNFSeSrvCad, Dmod.MyDB);
  //
  TemNFSe := DBCheck.LiberaModulo(CO_DMKID_APP, dmkPF.ObtemSiglaModulo(mdlappNFSe),
               DModG.QrMaster.FieldByName('HabilModulos').AsString);
  {$Else}
  QrNFSeSrvCad.Close;
  //
  TemNFSe := False;
  {$EndIf}
  LaNFSeSrvCad.Visible := TemNFSe;
  EdNFSeSrvCad.Visible := TemNFSe;
  CBNFSeSrvCad.Visible := TemNFSe;
  SBNFSeSrvCad.Visible := TemNFSe;
end;

procedure TFmCondGerArreUni.BitBtn1Click(Sender: TObject);
begin
  //FmCondger.QrArre.Locate('Apto', EdApto.Text, []);
end;

end.
