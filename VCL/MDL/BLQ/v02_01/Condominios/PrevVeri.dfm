object FmPrevVeri: TFmPrevVeri
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-004 :: Agendamento por Varredura de Lan'#231'amentos'
  ClientHeight = 662
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 500
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 67
        Height = 13
        Caption = 'Desde a data:'
      end
      object Label2: TLabel
        Left = 128
        Top = 4
        Width = 52
        Height = 13
        Caption = 'At'#233' a data:'
      end
      object TPDesde: TdmkEditDateTimePicker
        Left = 8
        Top = 20
        Width = 112
        Height = 21
        Date = 40549.891782534730000000
        Time = 40549.891782534730000000
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object RGValores: TRadioGroup
        Left = 244
        Top = 4
        Width = 217
        Height = 41
        Caption = ' Valores: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'D'#233'bito'
          'Credito'
          'Ambos')
        TabOrder = 2
      end
      object PB1: TProgressBar
        Left = 468
        Top = 20
        Width = 529
        Height = 17
        TabOrder = 3
        Visible = False
      end
      object TPAte: TdmkEditDateTimePicker
        Left = 128
        Top = 20
        Width = 112
        Height = 21
        Date = 40549.891782534730000000
        Time = 40549.891782534730000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 52
      Width = 1008
      Height = 448
      Align = alClient
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 1
        Top = 1
        Width = 1006
        Height = 446
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        OnChange = PageControl1Change
        object TabSheet1: TTabSheet
          Caption = ' Lan'#231'amentos n'#227'o agendados de contas cadastradas para rateio '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid1: TdmkDBGridDAC
            Left = 0
            Top = 0
            Width = 998
            Height = 418
            SQLFieldsToChange.Strings = (
              'Ativo')
            SQLIndexesOnUpdate.Strings = (
              'Controle')
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Ativo'
                Title.Caption = 'ok'
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Cod'
                Title.Caption = 'Provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Txt'
                Title.Caption = 'Nome da provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DESCRI_SHOW'
                Title.Caption = 'Descri'#231#227'o'
                Width = 320
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaFiscal'
                Title.Caption = 'N. Fiscal'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Title.Caption = 'Vencto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DC'
                Title.Caption = 'Compens.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TERCEIRO'
                Title.Caption = 'Cliente / Fornecedor'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'INICIO'
                Title.Caption = 'In'#237'cio'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FINAL'
                Title.Caption = 'Final'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'InfoParc_TXT'
                Title.Caption = 'Info'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESITCOBR'
                Title.Caption = 'Cobran'#231'a'
                Width = 94
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Parcelas'
                Title.Caption = 'Parc.'
                Width = 32
                Visible = True
              end>
            Color = clWindow
            DataSource = DsPrevVeri1
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = DBGrid1DrawColumnCell
            SQLTable = 'prevveri'
            GrayField = 'PrevBaC_Cod'
            EditForceNextYear = False
            Columns = <
              item
                Expanded = False
                FieldName = 'Ativo'
                Title.Caption = 'ok'
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Cod'
                Title.Caption = 'Provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Txt'
                Title.Caption = 'Nome da provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DESCRI_SHOW'
                Title.Caption = 'Descri'#231#227'o'
                Width = 320
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaFiscal'
                Title.Caption = 'N. Fiscal'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Title.Caption = 'Vencto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DC'
                Title.Caption = 'Compens.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TERCEIRO'
                Title.Caption = 'Cliente / Fornecedor'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'INICIO'
                Title.Caption = 'In'#237'cio'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FINAL'
                Title.Caption = 'Final'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'InfoParc_TXT'
                Title.Caption = 'Info'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESITCOBR'
                Title.Caption = 'Cobran'#231'a'
                Width = 94
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Parcelas'
                Title.Caption = 'Parc.'
                Width = 32
                Visible = True
              end>
          end
          object RgSitCobr: TdmkRadioGroup
            Left = 296
            Top = 156
            Width = 133
            Height = 85
            Caption = ' Fluxo da cobran'#231'a: '
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o cobrar'
              'Cont'#237'nua (mensal)'
              'Programada'
              'Por agendamento')
            TabOrder = 1
            Visible = False
            QryCampo = 'SitCobr'
            UpdCampo = 'SitCobr'
            UpdType = utYes
            OldValor = 0
          end
        end
        object TabSheet2: TTabSheet
          Caption = 
            ' Lan'#231'amentos n'#227'o agendados de contas N'#195'O cadastradas para rateio' +
            ' '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid0: TdmkDBGridDAC
            Left = 0
            Top = 0
            Width = 998
            Height = 418
            SQLFieldsToChange.Strings = (
              'Ativo')
            SQLIndexesOnUpdate.Strings = (
              'Controle')
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Ativo'
                Title.Caption = 'ok'
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Cod'
                Title.Caption = 'Provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Txt'
                Title.Caption = 'Nome da provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Descri'#231#227'o'
                Width = 320
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaFiscal'
                Title.Caption = 'N. Fiscal'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Title.Caption = 'Vencto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DC'
                Title.Caption = 'Compens.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TERCEIRO'
                Title.Caption = 'Cliente / Fornecedor'
                Width = 200
                Visible = True
              end>
            Color = clWindow
            DataSource = DsPrevVeri0
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            SQLTable = 'prevveri'
            GrayField = 'PrevBaC_Cod'
            EditForceNextYear = False
            Columns = <
              item
                Expanded = False
                FieldName = 'Ativo'
                Title.Caption = 'ok'
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Cod'
                Title.Caption = 'Provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Txt'
                Title.Caption = 'Nome da provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Descri'#231#227'o'
                Width = 320
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaFiscal'
                Title.Caption = 'N. Fiscal'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Title.Caption = 'Vencto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DC'
                Title.Caption = 'Compens.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TERCEIRO'
                Title.Caption = 'Cliente / Fornecedor'
                Width = 200
                Visible = True
              end>
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' J'#225' agendados '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid2: TdmkDBGridDAC
            Left = 0
            Top = 0
            Width = 998
            Height = 418
            SQLFieldsToChange.Strings = (
              'Ativo')
            SQLIndexesOnUpdate.Strings = (
              'Controle')
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Ativo'
                Title.Caption = 'ok'
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Cod'
                Title.Caption = 'Provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Txt'
                Title.Caption = 'Nome da provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Descri'#231#227'o'
                Width = 320
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaFiscal'
                Title.Caption = 'N. Fiscal'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Title.Caption = 'Vencto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DC'
                Title.Caption = 'Compens.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TERCEIRO'
                Title.Caption = 'Cliente / Fornecedor'
                Width = 200
                Visible = True
              end>
            Color = clWindow
            DataSource = DsPrevVeri2
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            SQLTable = 'prevveri'
            GrayField = 'PrevBaC_Cod'
            EditForceNextYear = False
            Columns = <
              item
                Expanded = False
                FieldName = 'Ativo'
                Title.Caption = 'ok'
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Cod'
                Title.Caption = 'Provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaC_Txt'
                Title.Caption = 'Nome da provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Descri'#231#227'o'
                Width = 320
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaFiscal'
                Title.Caption = 'N. Fiscal'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Title.Caption = 'Vencto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DC'
                Title.Caption = 'Compens.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TERCEIRO'
                Title.Caption = 'Cliente / Fornecedor'
                Width = 200
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 549
        Height = 32
        Caption = 'Agendamento por Varredura de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 549
        Height = 32
        Caption = 'Agendamento por Varredura de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 549
        Height = 32
        Caption = 'Agendamento por Varredura de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 548
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 592
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 6
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 15
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtPesquisa: TBitBtn
        Tag = 40
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 140
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNenhumClick
      end
      object BitBtn1: TBitBtn
        Tag = 10023
        Left = 380
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Provis'#227'o'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BitBtn1Click
      end
      object BtConfigura: TBitBtn
        Tag = 263
        Left = 500
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Configura'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtConfiguraClick
      end
      object BtAgenda: TBitBtn
        Tag = 14
        Left = 620
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Agenda'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtAgendaClick
      end
    end
  end
  object QrLct: TmySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrLctCalcFields
    SQL.Strings = (
      'SELECT * FROM prevveri'
      '')
    Left = 8
    Top = 12
    object QrLctProvRat: TSmallintField
      FieldName = 'ProvRat'
      Origin = 'prevveri.ProvRat'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'prevveri.Controle'
    end
    object QrLctData: TDateField
      FieldName = 'Data'
      Origin = 'prevveri.Data'
    end
    object QrLctValor: TFloatField
      FieldName = 'Valor'
      Origin = 'prevveri.Valor'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'prevveri.Vencimento'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'prevveri.Documento'
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'prevveri.Compensado'
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'prevveri.NotaFiscal'
    end
    object QrLctTERCEIRO: TIntegerField
      FieldName = 'TERCEIRO'
      Origin = 'prevveri.TERCEIRO'
    end
    object QrLctNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Origin = 'prevveri.NO_TERCEIRO'
      Size = 100
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'prevveri.Descricao'
      Size = 100
    end
    object QrLctMotivo: TSmallintField
      FieldName = 'Motivo'
      Origin = 'prevveri.Motivo'
    end
    object QrLctAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'prevveri.Ativo'
    end
    object QrLctDC: TWideStringField
      FieldName = 'DC'
      Origin = 'prevveri.DC'
      Size = 10
    end
    object QrLctPrevBaC_Cod: TIntegerField
      FieldName = 'PrevBaC_Cod'
      Origin = 'prevveri.PrevBaC_Cod'
    end
    object QrLctPrevBaC_Txt: TWideStringField
      FieldName = 'PrevBaC_Txt'
      Origin = 'prevveri.PrevBaC_Txt'
      Size = 40
    end
    object QrLctSitCobr: TIntegerField
      FieldName = 'SitCobr'
      Origin = 'prevveri.SitCobr'
    end
    object QrLctCond: TIntegerField
      FieldName = 'Cond'
      Origin = 'prevveri.Cond'
    end
    object QrLctTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'prevveri.Texto'
      Size = 40
    end
    object QrLctParcelas: TIntegerField
      FieldName = 'Parcelas'
      Origin = 'prevveri.Parcelas'
    end
    object QrLctParcPerI: TIntegerField
      FieldName = 'ParcPerI'
      Origin = 'prevveri.ParcPerI'
    end
    object QrLctParcPerF: TIntegerField
      FieldName = 'ParcPerF'
      Origin = 'prevveri.ParcPerF'
    end
    object QrLctInfoParc: TIntegerField
      FieldName = 'InfoParc'
      Origin = 'prevveri.InfoParc'
    end
    object QrLctPrevCod: TIntegerField
      FieldName = 'PrevCod'
      Origin = 'prevveri.PrevCod'
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'prevveri.Genero'
    end
    object QrLctNO_CONTA: TWideStringField
      FieldName = 'NO_CONTA'
      Origin = 'prevveri.NO_CONTA'
      Size = 50
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Origin = 'prevveri.ID_Pgto'
    end
    object QrLctSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Origin = 'prevveri.SerieCH'
      Size = 10
    end
    object QrLctSERIE_CHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_CHEQUE'
      Size = 40
      Calculated = True
    end
  end
  object QrPrevVeri1: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrPrevVeri1AfterOpen
    BeforeClose = QrPrevVeri1BeforeClose
    AfterScroll = QrPrevVeri1AfterScroll
    OnCalcFields = QrPrevVeri1CalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM prevveri'
      'WHERE Motivo=1'
      'ORDER BY PrevBaC_Txt')
    Left = 36
    Top = 12
    object QrPrevVeri1ProvRat: TSmallintField
      FieldName = 'ProvRat'
      Origin = 'prevveri.ProvRat'
    end
    object QrPrevVeri1Controle: TIntegerField
      FieldName = 'Controle'
      Origin = 'prevveri.Controle'
    end
    object QrPrevVeri1Data: TDateField
      FieldName = 'Data'
      Origin = 'prevveri.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPrevVeri1Valor: TFloatField
      FieldName = 'Valor'
      Origin = 'prevveri.Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPrevVeri1Vencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'prevveri.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPrevVeri1Documento: TFloatField
      FieldName = 'Documento'
      Origin = 'prevveri.Documento'
      DisplayFormat = '0;-0; '
    end
    object QrPrevVeri1Compensado: TDateField
      FieldName = 'Compensado'
      Origin = 'prevveri.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPrevVeri1NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'prevveri.NotaFiscal'
      DisplayFormat = '0;-0; '
    end
    object QrPrevVeri1TERCEIRO: TIntegerField
      FieldName = 'TERCEIRO'
      Origin = 'prevveri.TERCEIRO'
    end
    object QrPrevVeri1NO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Origin = 'prevveri.NO_TERCEIRO'
      Size = 100
    end
    object QrPrevVeri1Descricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'prevveri.Descricao'
      Size = 100
    end
    object QrPrevVeri1Motivo: TSmallintField
      FieldName = 'Motivo'
      Origin = 'prevveri.Motivo'
    end
    object QrPrevVeri1Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'prevveri.Ativo'
      MaxValue = 1
    end
    object QrPrevVeri1DC: TWideStringField
      FieldName = 'DC'
      Origin = 'prevveri.DC'
      Size = 10
    end
    object QrPrevVeri1PrevBaC_Cod: TIntegerField
      FieldName = 'PrevBaC_Cod'
      Origin = 'prevveri.PrevBaC_Cod'
    end
    object QrPrevVeri1PrevBaC_Txt: TWideStringField
      FieldName = 'PrevBaC_Txt'
      Origin = 'prevveri.PrevBaC_Txt'
      Size = 40
    end
    object QrPrevVeri1SitCobr: TIntegerField
      FieldName = 'SitCobr'
    end
    object QrPrevVeri1Cond: TIntegerField
      FieldName = 'Cond'
    end
    object QrPrevVeri1Texto: TWideStringField
      DisplayWidth = 50
      FieldName = 'Texto'
      Size = 50
    end
    object QrPrevVeri1Parcelas: TIntegerField
      FieldName = 'Parcelas'
      DisplayFormat = '0;-0; '
    end
    object QrPrevVeri1ParcPerI: TIntegerField
      FieldName = 'ParcPerI'
    end
    object QrPrevVeri1ParcPerF: TIntegerField
      FieldName = 'ParcPerF'
    end
    object QrPrevVeri1InfoParc: TIntegerField
      FieldName = 'InfoParc'
    end
    object QrPrevVeri1InfoParc_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'InfoParc_TXT'
      Size = 3
      Calculated = True
    end
    object QrPrevVeri1NOMESITCOBR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESITCOBR'
      Size = 50
      Calculated = True
    end
    object QrPrevVeri1INICIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'INICIO'
      Size = 30
      Calculated = True
    end
    object QrPrevVeri1FINAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Size = 30
      Calculated = True
    end
    object QrPrevVeri1DESCRI_SHOW: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DESCRI_SHOW'
      Size = 100
      Calculated = True
    end
  end
  object DsPrevVeri1: TDataSource
    DataSet = QrPrevVeri1
    Left = 64
    Top = 12
  end
  object QrPrevVeri0: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM prevveri'
      'WHERE Motivo=0'
      'ORDER BY PrevBaC_Txt')
    Left = 92
    Top = 12
    object QrPrevVeri0ProvRat: TSmallintField
      FieldName = 'ProvRat'
      Origin = 'prevveri.ProvRat'
    end
    object QrPrevVeri0Controle: TIntegerField
      FieldName = 'Controle'
      Origin = 'prevveri.Controle'
    end
    object QrPrevVeri0Data: TDateField
      FieldName = 'Data'
      Origin = 'prevveri.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPrevVeri0Valor: TFloatField
      FieldName = 'Valor'
      Origin = 'prevveri.Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPrevVeri0Vencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'prevveri.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPrevVeri0Documento: TFloatField
      FieldName = 'Documento'
      Origin = 'prevveri.Documento'
      DisplayFormat = '0;-0; '
    end
    object QrPrevVeri0Compensado: TDateField
      FieldName = 'Compensado'
      Origin = 'prevveri.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPrevVeri0NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'prevveri.NotaFiscal'
      DisplayFormat = '0;-0; '
    end
    object QrPrevVeri0TERCEIRO: TIntegerField
      FieldName = 'TERCEIRO'
      Origin = 'prevveri.TERCEIRO'
    end
    object QrPrevVeri0NO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Origin = 'prevveri.NO_TERCEIRO'
      Size = 100
    end
    object QrPrevVeri0Descricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'prevveri.Descricao'
      Size = 100
    end
    object QrPrevVeri0Motivo: TSmallintField
      FieldName = 'Motivo'
      Origin = 'prevveri.Motivo'
    end
    object QrPrevVeri0Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'prevveri.Ativo'
      MaxValue = 1
    end
    object QrPrevVeri0DC: TWideStringField
      FieldName = 'DC'
      Origin = 'prevveri.DC'
      Size = 10
    end
    object QrPrevVeri0PrevBaC_Cod: TIntegerField
      FieldName = 'PrevBaC_Cod'
      Origin = 'prevveri.PrevBaC_Cod'
    end
    object QrPrevVeri0PrevBaC_Txt: TWideStringField
      FieldName = 'PrevBaC_Txt'
      Origin = 'prevveri.PrevBaC_Txt'
      Size = 40
    end
    object QrPrevVeri0SitCobr: TIntegerField
      FieldName = 'SitCobr'
    end
    object QrPrevVeri0Cond: TIntegerField
      FieldName = 'Cond'
    end
    object QrPrevVeri0Texto: TWideStringField
      DisplayWidth = 50
      FieldName = 'Texto'
      Size = 50
    end
    object QrPrevVeri0Parcelas: TIntegerField
      FieldName = 'Parcelas'
      DisplayFormat = '0;-0; '
    end
    object QrPrevVeri0ParcPerI: TIntegerField
      FieldName = 'ParcPerI'
    end
    object QrPrevVeri0ParcPerF: TIntegerField
      FieldName = 'ParcPerF'
    end
    object QrPrevVeri0InfoParc: TIntegerField
      FieldName = 'InfoParc'
    end
    object QrPrevVeri0InfoParc_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'InfoParc_TXT'
      Size = 3
      Calculated = True
    end
    object QrPrevVeri0NOMESITCOBR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESITCOBR'
      Size = 50
      Calculated = True
    end
    object QrPrevVeri0INICIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'INICIO'
      Size = 30
      Calculated = True
    end
    object QrPrevVeri0FINAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Size = 30
      Calculated = True
    end
  end
  object DsPrevVeri0: TDataSource
    DataSet = QrPrevVeri0
    Left = 120
    Top = 12
  end
  object QrPrevVeri2: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM prevveri'
      'WHERE Motivo=2'
      'ORDER BY PrevBaC_Txt')
    Left = 152
    Top = 12
    object QrPrevVeri2ProvRat: TSmallintField
      FieldName = 'ProvRat'
      Origin = 'prevveri.ProvRat'
    end
    object QrPrevVeri2Controle: TIntegerField
      FieldName = 'Controle'
      Origin = 'prevveri.Controle'
    end
    object QrPrevVeri2Data: TDateField
      FieldName = 'Data'
      Origin = 'prevveri.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPrevVeri2Valor: TFloatField
      FieldName = 'Valor'
      Origin = 'prevveri.Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPrevVeri2Vencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'prevveri.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPrevVeri2Documento: TFloatField
      FieldName = 'Documento'
      Origin = 'prevveri.Documento'
      DisplayFormat = '0;-0; '
    end
    object QrPrevVeri2Compensado: TDateField
      FieldName = 'Compensado'
      Origin = 'prevveri.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPrevVeri2NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'prevveri.NotaFiscal'
      DisplayFormat = '0;-0; '
    end
    object QrPrevVeri2TERCEIRO: TIntegerField
      FieldName = 'TERCEIRO'
      Origin = 'prevveri.TERCEIRO'
    end
    object QrPrevVeri2NO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Origin = 'prevveri.NO_TERCEIRO'
      Size = 100
    end
    object QrPrevVeri2Descricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'prevveri.Descricao'
      Size = 100
    end
    object QrPrevVeri2Motivo: TSmallintField
      FieldName = 'Motivo'
      Origin = 'prevveri.Motivo'
    end
    object QrPrevVeri2Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'prevveri.Ativo'
      MaxValue = 1
    end
    object QrPrevVeri2DC: TWideStringField
      FieldName = 'DC'
      Origin = 'prevveri.DC'
      Size = 10
    end
    object QrPrevVeri2PrevBaC_Cod: TIntegerField
      FieldName = 'PrevBaC_Cod'
      Origin = 'prevveri.PrevBaC_Cod'
    end
    object QrPrevVeri2PrevBaC_Txt: TWideStringField
      FieldName = 'PrevBaC_Txt'
      Origin = 'prevveri.PrevBaC_Txt'
      Size = 40
    end
    object QrPrevVeri2SitCobr: TIntegerField
      FieldName = 'SitCobr'
    end
    object QrPrevVeri2Cond: TIntegerField
      FieldName = 'Cond'
    end
    object QrPrevVeri2Texto: TWideStringField
      DisplayWidth = 50
      FieldName = 'Texto'
      Size = 50
    end
    object QrPrevVeri2Parcelas: TIntegerField
      FieldName = 'Parcelas'
      DisplayFormat = '0;-0; '
    end
    object QrPrevVeri2ParcPerI: TIntegerField
      FieldName = 'ParcPerI'
    end
    object QrPrevVeri2ParcPerF: TIntegerField
      FieldName = 'ParcPerF'
    end
    object QrPrevVeri2InfoParc: TIntegerField
      FieldName = 'InfoParc'
    end
    object QrPrevVeri2InfoParc_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'InfoParc_TXT'
      Size = 3
      Calculated = True
    end
    object QrPrevVeri2NOMESITCOBR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESITCOBR'
      Size = 50
      Calculated = True
    end
    object QrPrevVeri2INICIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'INICIO'
      Size = 30
      Calculated = True
    end
    object QrPrevVeri2FINAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Size = 30
      Calculated = True
    end
  end
  object DsPrevVeri2: TDataSource
    DataSet = QrPrevVeri2
    Left = 176
    Top = 12
  end
  object PMAgenda: TPopupMenu
    Left = 548
    Top = 492
    object Agrupatodoschecadosemumitem1: TMenuItem
      Caption = 'Agrupa todos checados em um item'
      OnClick = Agrupatodoschecadosemumitem1Click
    end
    object Criaitemparacadachecado1: TMenuItem
      Caption = 'Cria um item para cada checado'
      OnClick = Criaitemparacadachecado1Click
    end
  end
end
