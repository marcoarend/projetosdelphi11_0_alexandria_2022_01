unit ArreBaC;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  Grids, DBGrids, Menus, dmkEdit, dmkLabel, dmkDBLookupComboBox, dmkEditCB,
  Variants, dmkCheckBox, dmkRadioGroup, dmkDBGrid, UnDmkProcFunc, DmkDAC_PF,
  dmkImage, ComCtrls, UnDmkEnums;

type
  TFmArreBaC = class(TForm)
    DsArreBaC: TDataSource;
    QrArreBaC: TmySQLQuery;
    PainelEdita: TPanel;
    QrArreBaCCodigo: TIntegerField;
    QrArreBaCConta: TIntegerField;
    QrArreBaCLk: TIntegerField;
    QrArreBaCDataCad: TDateField;
    QrArreBaCDataAlt: TDateField;
    QrArreBaCUserCad: TIntegerField;
    QrArreBaCUserAlt: TIntegerField;
    QrArreBaCNome: TWideStringField;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    QrArreBaCNOMECONTA: TWideStringField;
    QrArreBaI: TmySQLQuery;
    DsArreBaI: TDataSource;
    QrArreBaICodigo: TIntegerField;
    QrArreBaIControle: TIntegerField;
    QrArreBaICond: TIntegerField;
    QrArreBaIValor: TFloatField;
    QrArreBaITexto: TWideStringField;
    QrArreBaISitCobr: TIntegerField;
    QrArreBaIParcelas: TIntegerField;
    QrArreBaIParcPerI: TIntegerField;
    QrArreBaIParcPerF: TIntegerField;
    QrArreBaILk: TIntegerField;
    QrArreBaIDataCad: TDateField;
    QrArreBaIDataAlt: TDateField;
    QrArreBaIUserAlt: TIntegerField;
    PMArrecad: TPopupMenu;
    Incluiarrecadao1: TMenuItem;
    Alteraarrecadao1: TMenuItem;
    Excluiarrecadao1: TMenuItem;
    PMCondom: TPopupMenu;
    AdicionaCondomnioarrecadaoselecionada1: TMenuItem;
    Retiraocondominiodaarrecadaoselecionada1: TMenuItem;
    PnCond: TPanel;
    Editaarrecadaoselecionada1: TMenuItem;
    QrArreBaINOMECOND: TWideStringField;
    QrArreBaINOMESITCOBR: TWideStringField;
    QrArreBaIINICIO: TWideStringField;
    QrArreBaIFINAL: TWideStringField;
    QrArreBaIInfoParc: TIntegerField;
    QrArreBaIInfoParc_TXT: TWideStringField;
    Timer1: TTimer;
    QrArreBaIFator: TSmallintField;
    QrArreBaIPercent: TFloatField;
    QrArreBaINOMEFATOR: TWideStringField;
    PMUnidades: TPopupMenu;
    Retiraunidade1: TMenuItem;
    Incluiunidade1: TMenuItem;
    QrArreBaU: TmySQLQuery;
    DsArreBaU: TDataSource;
    QrArreBaIDeQuem: TSmallintField;
    QrArreBaINOMEDEQUEM: TWideStringField;
    QrArreBaUUnidade: TWideStringField;
    QrArreBaUCodigo: TIntegerField;
    QrArreBaUControle: TIntegerField;
    QrArreBaUConta: TIntegerField;
    QrArreBaUApto: TIntegerField;
    QrArreBaULk: TIntegerField;
    QrArreBaUDataCad: TDateField;
    QrArreBaUDataAlt: TDateField;
    QrArreBaUUserCad: TIntegerField;
    QrArreBaUUserAlt: TIntegerField;
    Incluidiversasunidades1: TMenuItem;
    N1: TMenuItem;
    QrAptos: TmySQLQuery;
    QrAptosConta: TIntegerField;
    QrAptosUnidade: TWideStringField;
    QrAptosNOMEDONO: TWideStringField;
    QrAptosSel: TIntegerField;
    QrAptosSELECIONADO: TIntegerField;
    QrArreBaIDoQue: TSmallintField;
    QrArreBaIComoCobra: TSmallintField;
    QrArreBaINaoArreSobre: TSmallintField;
    QrArreBaUAlterWeb: TSmallintField;
    QrArreBaUAtivo: TSmallintField;
    QrArreBaUPercent: TFloatField;
    Retiradiversasunidades1: TMenuItem;
    Alteraunidadeatual1: TMenuItem;
    N2: TMenuItem;
    QrArreBaICalculo: TSmallintField;
    QrArreBaIArredonda: TFloatField;
    QrArreBaINOME_CALCULO: TWideStringField;
    QrListaBaU: TmySQLQuery;
    DsListaBaU: TDataSource;
    QrListaBaUControle: TIntegerField;
    QrListaBaUNome: TWideStringField;
    QrArreBaIListaBaU: TIntegerField;
    QrArreBaINOMELISTABAU: TWideStringField;
    QrArreBaIAlterWeb: TSmallintField;
    QrArreBaIAtivo: TSmallintField;
    QrArreBaIDescriCota: TWideStringField;
    QrAptosAndar: TIntegerField;
    QrArreBaIValFracDef: TFloatField;
    QrArreBaIValFracAsk: TSmallintField;
    QrArreBaIPartilha: TSmallintField;
    QrSABaU: TmySQLQuery;
    DsSABaU: TDataSource;
    QrSABaUPercent: TFloatField;
    QrSABaUITENS: TLargeintField;
    QrArreBaCSigla: TWideStringField;
    QrArreBaIInfoRelArr: TSmallintField;
    QrArreBaINaoArreRisco: TSmallintField;
    QrArreBaIDestCobra: TSmallintField;
    QrListaCond: TmySQLQuery;
    QrListaCondCodigo: TIntegerField;
    QrListaCondNOME_CND: TWideStringField;
    DsListaCond: TDataSource;
    DsListaItens: TDataSource;
    QrListaItens: TmySQLQuery;
    QrListaItensCodigo: TIntegerField;
    QrListaItensSigla: TWideStringField;
    QrListaItensNome: TWideStringField;
    PainelDados: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel8: TPanel;
    BtSaida: TBitBtn;
    BtArrecad: TBitBtn;
    BtCondom: TBitBtn;
    BtUnidades: TBitBtn;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    LaAviso2: TLabel;
    LaAviso1: TLabel;
    GBRodaPe: TGroupBox;
    Panel11: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GroupBox4: TGroupBox;
    PainelEdit: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    LaForneceI: TLabel;
    Label18: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    EdSigla: TEdit;
    GroupBox1: TGroupBox;
    PnCondDados: TPanel;
    Label6: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label16: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    DBEdit02: TDBEdit;
    DBEdit03: TDBEdit;
    RGSitCobr: TRadioGroup;
    GbPro: TGroupBox;
    Label7: TLabel;
    EdParcelas: TdmkEdit;
    GBPer: TGroupBox;
    GBIni: TGroupBox;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBMesI: TComboBox;
    CBAnoI: TComboBox;
    GBFim: TGroupBox;
    Label34: TLabel;
    LaAnoF: TLabel;
    CBMesF: TComboBox;
    CBAnoF: TComboBox;
    EdValor: TdmkEdit;
    EdTexto: TdmkEdit;
    CkInfoParc: TCheckBox;
    RGFator: TRadioGroup;
    EdPercent: TdmkEdit;
    RGDeQuem: TRadioGroup;
    RGDoQue: TRadioGroup;
    RGComoCobra: TRadioGroup;
    dmkEdArredonda: TdmkEdit;
    RGCalculo: TRadioGroup;
    dmkEdListaBaU: TdmkEditCB;
    dmkCBListaBaU: TdmkDBLookupComboBox;
    EdDescriCota: TEdit;
    GroupBox2: TGroupBox;
    Label17: TLabel;
    CkValFracAsk: TCheckBox;
    EdValFracDef: TdmkEdit;
    RGPartilha: TRadioGroup;
    CkInfoRelArr: TdmkCheckBox;
    RGDestCobra: TdmkRadioGroup;
    GroupBox5: TGroupBox;
    CkNaoArreSobre: TCheckBox;
    CkNaoArreRisco: TCheckBox;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Panel12: TPanel;
    BtConfCond: TBitBtn;
    BitBtn2: TBitBtn;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label22: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Splitter2: TSplitter;
    Panel6: TPanel;
    DBGCondominios: TdmkDBGrid;
    Panel7: TPanel;
    Label21: TLabel;
    EdPesqCond: TEdit;
    DBGItensCond: TdmkDBGrid;
    Panel4: TPanel;
    DBText1: TDBText;
    Splitter1: TSplitter;
    DBGUnid: TdmkDBGrid;
    StaticText1: TStaticText;
    DBGrid1: TdmkDBGrid;
    Panel1: TPanel;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit01: TDBEdit;
    DBGCond: TdmkDBGrid;
    Splitter3: TSplitter;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    SpeedButton5: TSpeedButton;
    QrLoc: TmySQLQuery;
    Label23: TLabel;
    EdCNAB_Cfg: TdmkEditCB;
    CBCNAB_Cfg: TdmkDBLookupComboBox;
    QrCNAB_Cfg: TmySQLQuery;
    DsCNAB_Cfg: TDataSource;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    QrArreBaICNAB_Cfg: TIntegerField;
    SpeedButton6: TSpeedButton;
    QrArreBaICNAB_Cfg_Txt: TWideStringField;
    QrArreBaINFSeSrvCad: TIntegerField;
    LaNFSeSrvCad: TLabel;
    EdNFSeSrvCad: TdmkEditCB;
    CBNFSeSrvCad: TdmkDBLookupComboBox;
    SBNFSeSrvCad: TSpeedButton;
    QrNFSeSrvCad: TmySQLQuery;
    QrNFSeSrvCadCodigo: TIntegerField;
    QrNFSeSrvCadNome: TWideStringField;
    DsNFSeSrvCad: TDataSource;
    QrArreBaINFSeSrvCad_TXT: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtArrecadClick(Sender: TObject);
    procedure BtCondomClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrArreBaCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrArreBaCAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrArreBaCBeforeOpen(DataSet: TDataSet);
    procedure QrArreBaCBeforeClose(DataSet: TDataSet);
    procedure Incluiarrecadao1Click(Sender: TObject);
    procedure Alteraarrecadao1Click(Sender: TObject);
    procedure PMArrecadPopup(Sender: TObject);
    procedure PMCondomPopup(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure AdicionaCondomnioarrecadaoselecionada1Click(Sender: TObject);
    procedure BtConfCondClick(Sender: TObject);
    procedure RGSitCobrClick(Sender: TObject);
    procedure Editaarrecadaoselecionada1Click(Sender: TObject);
    procedure Retiraocondominiodaarrecadaoselecionada1Click(Sender: TObject);
    procedure QrArreBaICalcFields(DataSet: TDataSet);
    procedure Timer1Timer(Sender: TObject);
    procedure BtUnidadesClick(Sender: TObject);
    procedure Incluiunidade1Click(Sender: TObject);
    procedure QrArreBaIAfterScroll(DataSet: TDataSet);
    procedure QrArreBaIBeforeClose(DataSet: TDataSet);
    procedure QrArreBaIAfterOpen(DataSet: TDataSet);
    procedure PMUnidadesPopup(Sender: TObject);
    procedure Retiraunidade1Click(Sender: TObject);
    procedure Incluidiversasunidades1Click(Sender: TObject);
    procedure QrAptosCalcFields(DataSet: TDataSet);
    procedure Retiradiversasunidades1Click(Sender: TObject);
    procedure Alteraunidadeatual1Click(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure DBGUnidDblClick(Sender: TObject);
    procedure QrListaCondAfterScroll(DataSet: TDataSet);
    procedure EdPesqCondChange(Sender: TObject);
    procedure DBGItensCondCellClick(Column: TColumn);
    procedure DBGCondCellClick(Column: TColumn);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure DBGItensCondDblClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SBNFSeSrvCadClick(Sender: TObject);
  private
    FMes: Word;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure MostraFmArreBaCAptos(Acao: TSQLType);
    procedure ReopenListaBaU(Cond: Integer);
    procedure ReopenListCond();
  public
    { Public declarations }
    FMostra: Integer;
    FMostrou: Boolean;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenQrArreBaI(Controle: Integer);
    procedure ReopenQrArreBaU(Conta: Integer);
  end;

var
  FmArreBaC: TFmArreBaC;
const
  FFormatFloat = '00000';

implementation

uses Module, Principal, SelCod, UCreate, ArreBaCAptos, ArreBaCAddUni,
  {$IfNDef sNFSe} NFSe_PF_0000, {$EndIf}
  ModuleGeral, UnMyObjects, UnFinanceiroJan, UnBloqGerl, UnBloqGerl_Jan,
  MyListas, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmArreBaC.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmArreBaC.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrArreBaCCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmArreBaC.DefParams;
begin
  VAR_GOTOTABELA := 'ArreBaC';
  VAR_GOTOMYSQLTABLE := QrArreBaC;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT con.Nome NOMECONTA, pbc.*');
  VAR_SQLx.Add('FROM arrebac pbc');
  VAR_SQLx.Add('LEFT JOIN contas con ON con.Codigo=pbc.Conta');
  VAR_SQLx.Add('WHERE pbc.Codigo > 0');
  //
  VAR_SQL1.Add('AND pbc.Codigo=:P0');
  //
  VAR_SQLa.Add('AND pbc.Nome Like :P0');
  //
end;

procedure TFmArreBaC.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
var
  Mes, Ano: Word;
  TemNFSe: Boolean;
begin
  FMostrou := true;
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PnCond.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text    := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text      := '';
        EdSigla.Text     := '';
        EdConta.Text     := '';
        CBConta.KeyValue := NULL;
      end else begin
        EdCodigo.Text    := DBEdCodigo.Text;
        EdNome.Text      := DBEdNome.Text;
        EdSigla.Text     := QrArreBaCSigla.Value;
        EdConta.Text     := IntToStr(QrArreBaCConta.Value);
        CBConta.KeyValue := QrArreBaCConta.Value;
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      {$IfNDef sNFSe}
      TemNFSe := DBCheck.LiberaModulo(CO_DMKID_APP, dmkPF.ObtemSiglaModulo(mdlappNFSe),
                   DModG.QrMaster.FieldByName('HabilModulos').AsString);
      {$Else}
      TemNFSe := False;
      {$EndIf}
      LaNFSeSrvCad.Visible := TemNFSe;
      EdNFSeSrvCad.Visible := TemNFSe;
      CBNFSeSrvCad.Visible := TemNFSe;
      SBNFSeSrvCad.Visible := TemNFSe;
      //
      PnCond.Visible      := True;
      PainelDados.Visible := False;
      //
      if SQLType = stIns then
      begin
        EdEmpresa.ValueVariant      := 0;
        CBEmpresa.KeyValue          := Null;
        EdCNAB_Cfg.ValueVariant     := 0;
        CBCNAB_Cfg.KeyValue         := Null;
        EdNFSeSrvCad.ValueVariant   := 0;
        CBNFSeSrvCad.KeyValue       := Null;
        RGSitCobr.ItemIndex         := 0;
        RGFator.ItemIndex           := 0;
        RGDequem.ItemIndex          := 0;
        RGDoQue.ItemIndex           := 0;
        RGComoCobra.ItemIndex       := 0;
        EdValor.Text                := '';
        EdPercent.Text              := '';
        EdParcelas.Text             := '';
        CBAnoI.ItemIndex            := 50;
        CBAnoF.ItemIndex            := 50;
        CBMesI.ItemIndex            := FMes - 1;
        CBMesF.ItemIndex            := FMes;
        CkInfoParc.Checked          := True;
        CkNaoArreSobre.Checked      := False;
        CkNaoArreRisco.Checked      := False;
        RGCalculo.ItemIndex         := 0;
        dmkEdArredonda.ValueVariant := 0.01;
        dmkEdListaBaU.ValueVariant  := 0;
        dmkCBListaBaU.KeyValue      := Null;
        EdDescriCota.Text           := 'Cotas';
        EdValFracDef.ValueVariant   := 0;
        CkValFracAsk.Checked        := True;
        RGPartilha.ItemIndex        := 0;
        CkInfoRelArr.Checked        := False;
        RGDestCobra.ItemIndex       := 0;
      end else begin
        EdEmpresa.ValueVariant      := QrArreBaICond.Value;
        CBEmpresa.KeyValue          := QrArreBaICond.Value;
        EdCNAB_Cfg.ValueVariant     := QrArreBaICNAB_Cfg.Value;
        CBCNAB_Cfg.KeyValue         := QrArreBaICNAB_Cfg.Value;
        EdNFSeSrvCad.ValueVariant   := QrArreBaINFSeSrvCad.Value;
        CBNFSeSrvCad.KeyValue       := QrArreBaINFSeSrvCad.Value;
        RGSitCobr.ItemIndex         := QrArreBaISitCobr.Value;
        RGFator.ItemIndex           := QrArreBaIFator.Value;
        RGDeQuem.ItemIndex          := QrArreBaIDeQuem.Value;
        RGDoQue.ItemIndex           := QrArreBaIDoQue.Value;
        RGComoCobra.ItemIndex       := QrArreBaIComoCobra.Value;
        EdValor.Text                := Geral.FFT(QrArreBaIValor.Value, 2, siNegativo);
        EdPercent.Text              := Geral.FFT(QrArreBaIPercent.Value, 4, siNegativo);
        EdParcelas.Text             := Geral.FFT(QrArreBaIParcelas.Value, 0, siPositivo);
        RGCalculo.ItemIndex         := QrArreBaICalculo.Value;
        dmkEdArredonda.ValueVariant := QrArreBaIArredonda.Value;
        EdDescriCota.Text           := QrArreBaIDescriCota.Value;
        EdValFracDef.ValueVariant   := QrArreBaIValFracDef.Value;
        CkValFracAsk.Checked        := MLAGeral.ITB(QrArreBaIValFracAsk.Value);
        RGPartilha.ItemIndex        := QrArreBaIPartilha.Value;
        CkInfoRelArr.Checked        := MLAGeral.ITB(QrArreBaIInfoRelArr.Value);
        RGDestCobra.ItemIndex       := QrArreBaIDestCobra.Value;
        CkInfoParc.Checked          := Geral.IntToBool_0(QrArreBaIInfoParc.Value);
        CkNaoArreSobre.Checked      := Geral.IntToBool_0(QrArreBaINaoArreSobre.Value);
        CkNaoArreRisco.Checked      := Geral.IntToBool_0(QrArreBaINaoArreRisco.Value);
        dmkEdListaBaU.ValueVariant  := QrArreBaIListaBaU.Value;
        dmkCBListaBaU.KeyValue      := QrArreBaIListaBaU.Value;
        //
        UBloqGerl.ReopenCNAB_Cfg(QrCNAB_Cfg, Dmod.MyDB, DModG.QrEmpresasCodigo.Value);
        //
        if QrArreBaIParcPerI.Value < 13 then
        begin
          CBAnoI.Itemindex := 50;
          CBMesI.ItemIndex := QrArreBaIParcPerI.Value - 1;
        end else begin
          MLAGeral.MesEAnoDePeriodoShort(QrArreBaIParcPerI.Value, Mes, Ano);
          CBAnoI.Itemindex := Ano-Geral.IMV(CBAnoI.Items[0]);
          CBMesI.ItemIndex := Mes - 1;
        end;
        //
        if QrArreBaIParcPerF.Value < 13 then
        begin
          CBAnoF.Itemindex := 50;
          CBMesF.ItemIndex := QrArreBaIParcPerF.Value - 1;
        end else begin
          MLAGeral.MesEAnoDePeriodoShort(QrArreBaIParcPerF.Value, Mes, Ano);
          CBAnoF.Itemindex := Ano-Geral.IMV(CBAnoF.Items[0]);
          CBMesF.ItemIndex := Mes - 1;
        end;
        ReopenListaBaU(QrArreBaICond.Value);
      end;
      EdEmpresa.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmArreBaC.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmArreBaC.AlteraRegistro;
var
  ArreBaC : Integer;
begin
  ArreBaC := QrArreBaCCodigo.Value;
  if not UMyMod.SelLockY(ArreBaC, Dmod.MyDB, 'ArreBaC', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(ArreBaC, Dmod.MyDB, 'ArreBaC', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmArreBaC.IncluiRegistro;
var
  Cursor : TCursor;
  ArreBaC : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    ArreBaC := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ArreBaC', 'ArreBaC', 'Codigo');
    if Length(FormatFloat(FFormatFloat, ArreBaC))>Length(FFormatFloat) then
    begin
      Geral.MensagemBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, ArreBaC);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmArreBaC.QueryPrincipalAfterOpen;
begin
end;

procedure TFmArreBaC.DBGCondCellClick(Column: TColumn);
begin
  QrListaCond.Locate('Codigo', QrArreBaICond.Value, []);
  QrListaItens.Locate('Codigo', QrArreBaICodigo.Value, []);
end;

procedure TFmArreBaC.DBGItensCondCellClick(Column: TColumn);
begin
  LocCod(QrArreBaCCodigo.Value, QrListaItensCodigo.Value);
  QrArreBaI.Locate('Cond', QrListaCondCodigo.Value, []);
end;

procedure TFmArreBaC.DBGItensCondDblClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmArreBaC.DBGUnidDblClick(Sender: TObject);
var
  ValorTxt: String;
  Conta: Integer;
begin
  if (QrArreBau.State <> dsInactive) and (QrArreBau.RecordCount > 0) then
  begin
    ValorTxt := FormatFloat('0.0000000000', QrArreBaUPercent.Value);
    if InputQuery('Obten��o de quantidade', 'Informe a nova quantidade:', ValorTxt) then
    begin
      Conta := QrArreBaUConta.Value;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE arrebau SET Percent=:P0 WHERE Conta=:P1');
      Dmod.QrUpd.Params[00].AsFloat   := Geral.DMV(ValorTxt);
      Dmod.QrUpd.Params[01].AsInteger := Conta;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenQrArreBaU(Conta);
    end;
  end;
end;

procedure TFmArreBaC.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmArreBaC.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmArreBaC.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmArreBaC.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmArreBaC.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmArreBaC.SpeedButton5Click(Sender: TObject);
var
  Conta: Integer;
begin
  if EdConta.Enabled and CBConta.Enabled then
  begin
    VAR_CADASTRO := 0;
    Conta        := EdConta.ValueVariant;
    //
    FinanceiroJan.CadastroDeContas(Conta);
    //
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.AbreQuery(QrContas, DMod.MyDB);
      //
      EdConta.ValueVariant := VAR_CADASTRO;
      CBConta.KeyValue     := VAR_CADASTRO;
      EdConta.SetFocus;
    end;
  end;
end;

procedure TFmArreBaC.SpeedButton6Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdCNAB_Cfg.ValueVariant;
  //
  UBloqGerl_Jan.MostraCNAB_Cfg(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCNAB_Cfg, CBCNAB_Cfg, QrCNAB_Cfg, VAR_CADASTRO);
    EdCNAB_Cfg.SetFocus;
  end;
end;

procedure TFmArreBaC.BtArrecadClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArrecad, BtArrecad);
end;

procedure TFmArreBaC.BtCondomClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCondom, BtCondom);
end;

procedure TFmArreBaC.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrArreBaCCodigo.Value;
  Close;
end;

procedure TFmArreBaC.BtConfirmaClick(Sender: TObject);
var
  Conta, Codigo : Integer;
  Nome, Sigla: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONWARNING);
    EdNome.SetFocus;
    Exit;
  end;

  //

  Sigla := EdSigla.Text;
  if Length(Sigla) = 0 then
  begin
    Geral.MensagemBox('Defina uma sigla.', 'Erro', MB_OK+MB_ICONWARNING);
    EdSigla.SetFocus;
    Exit;
  end;

  //

  Conta := Geral.IMV(EdConta.Text);
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO arrebac SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE arrebac SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Conta=:P1, Sigla=:P2, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Conta;
  Dmod.QrUpdU.Params[02].AsString  := Sigla;
  //
  Dmod.QrUpdU.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[04].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[05].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ArreBaC', 'Codigo');
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmArreBaC.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ArreBaC', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ArreBaC', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ArreBaC', 'Codigo');
end;

procedure TFmArreBaC.FormCreate(Sender: TObject);
var
  TemNFSe: Boolean;
begin
  ImgTipo.SQLType := stLok;
  //
  FMostra := 0;
  FMostrou := False;
  RgSitCobr.Items := FmPrincipal.RgSitCobr.Items;
  MyObjects.PreencheCBAnoECBMes(CBAnoI, CBMesI, -1);
  MyObjects.PreencheCBAnoECBMes(CBAnoF, CBMesF, -1);

  //////////////////////////////////////////////////////////////////////////
  PageControl1.ActivePageIndex := 0;
  PainelEdita.Align            := alClient;
  PainelDados.Align            := alClient;
  PainelEdit.Align             := alClient;
  PageControl1.Align           := alClient;
  PnCondDados.Align            := alClient;
  //
  CBConta.ListSource       := DsContas;
  CBEmpresa.ListSource     := DModG.DsEmpresas;
  CBCNAB_Cfg.ListSource    := DsCNAB_Cfg;
  dmkCBListaBaU.ListSource := DsListaBaU;
  //
  UMyMod.AbreQuery(QrContas, DMod.MyDB);
  //
  {$IfNDef sNFSe}
  TemNFSe := DBCheck.LiberaModulo(CO_DMKID_APP, dmkPF.ObtemSiglaModulo(mdlappNFSe),
               DModG.QrMaster.FieldByName('HabilModulos').AsString);
  //
  UMyMod.AbreQuery(QrNFSeSrvCad, Dmod.MyDB);
  {$Else}
  TemNFSe := False;
  //
  QrNFSeSrvCad.Close;
  {$EndIf}
  //
  DBGCond.Columns[15].Visible := TemNFSe;
  //
  CriaOForm;
  ReopenListCond();
end;

procedure TFmArreBaC.SbNumeroClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  LaRegistro.Caption := GOTOy.Codigo(QrArreBaCCodigo.Value,LaRegistro.Caption);
end;

procedure TFmArreBaC.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmArreBaC.SBNFSeSrvCadClick(Sender: TObject);
begin
  {$IfNDef sNFSe}
  VAR_CADASTRO := 0;
  //
  UnNFSe_PF_0000.MostraFormNFSeSrvCad(EdNFSeSrvCad.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdNFSeSrvCad, CBNFSeSrvCad, QrNFSeSrvCad, VAR_CADASTRO);
    //
    EdNFSeSrvCad.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmArreBaC.SbNomeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmArreBaC.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmArreBaC.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmArreBaC.QrArreBaCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmArreBaC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'ArreBaC', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmArreBaC.QrArreBaCAfterScroll(DataSet: TDataSet);
begin
  //BtAltera.Enabled := GOTOy.BtEnabled(QrArreBaCCodigo.Value, False);
  ReopenQrArreBaI(0);
end;

procedure TFmArreBaC.SbQueryClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  LocCod(QrArreBaCCodigo.Value,
    CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ArreBaC', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmArreBaC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmArreBaC.QrArreBaCBeforeOpen(DataSet: TDataSet);
begin
  QrArreBaCCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmArreBaC.QrArreBaCBeforeClose(DataSet: TDataSet);
begin
  QrArreBaI.Close;
end;

procedure TFmArreBaC.ReopenQrArreBaI(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrArreBaI, Dmod.MyDB, [
  'SELECT ELT(pbi.Calculo+1, "Rateio", "Percentual", "Cotas","?") ',
  'NOME_CALCULO, IF(pbi.ListaBaU<>0, bc2.Nome, "[------]") NOMELISTABAU,',
  'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
  'ELSE ent.Nome END NOMECOND, cfg.Nome CNAB_Cfg_Txt, ',
{$IfNDef sNFSe}
  'nsc.Nome NFSeSrvCad_TXT, ',
{$Else}
  '"" NFSeSrvCad_TXT, ',
{$EndIf}
  'pbi.*',
  'FROM arrebai pbi',
{$IfNDef sNFSe}
  'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo = pbi.NFSeSrvCad ',
{$EndIf}
  'LEFT JOIN cond      cnd ON cnd.Codigo=pbi.Cond',
  'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=pbi.CNAB_Cfg',
  'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente',
  'LEFT JOIN arrebai   bi2 ON bi2.Controle=pbi.ListaBaU',
  'LEFT JOIN arrebac   bc2 ON bc2.Codigo=bi2.Codigo',
  'WHERE pbi.Codigo=' + FormatFloat('0', QrArreBaCCodigo.Value),
  'AND cnd.Codigo IN (' + VAR_LIB_FILIAIS + ')',
  '']);
  //
  QrArreBaI.Locate('Controle', Controle, []);
end;

procedure TFmArreBaC.Incluiarrecadao1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmArreBaC.Alteraarrecadao1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmArreBaC.PMArrecadPopup(Sender: TObject);
begin
  Alteraarrecadao1.Enabled := Geral.IntToBool_0(QrArreBaCCodigo.Value);
end;

procedure TFmArreBaC.PMCondomPopup(Sender: TObject);
begin
  AdicionaCondomnioarrecadaoselecionada1.Enabled := Geral.IntToBool_0(QrArreBaCCodigo.Value);
  Retiraocondominiodaarrecadaoselecionada1.Enabled := Geral.IntToBool_0(QrArreBaIControle.Value);
  Editaarrecadaoselecionada1.Enabled := Geral.IntToBool_0(QrArreBaIControle.Value);
end;

procedure TFmArreBaC.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
  if FMostra > 0 then Close;
end;

procedure TFmArreBaC.AdicionaCondomnioarrecadaoselecionada1Click(
  Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmArreBaC.BtConfCondClick(Sender: TObject);
var
  InfoParc, Fator, DeQuem, DoQue, ComoCobra, NaoArreSobre, NaoArreRisco, pp,
  Calculo, SitCobr, Ano, ParcPerI, ParcPerF, PeriodoA, Parcelas, Controle,
  Partilha, InfoRelArr, DestCobra, ValFracAsk, ListaBaU, Cond, Codigo,
  CNAB_Cfg, NFSeSrvCad: Integer;
  Percent, Valor: Double;
  DescriCota, Texto, Txt: String;
  ValFracDef, Arredonda: Double;
begin
  SitCobr    := RgSitCobr.ItemIndex;
  Cond       := EdEmpresa.ValueVariant;
  CNAB_Cfg   := EdCNAB_Cfg.ValueVariant;
  NFSeSrvCad := EdNFSeSrvCad.ValueVariant;
  //
  if MyObjects.FIC(Cond = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  if MyObjects.FIC(CNAB_Cfg = 0, EdCNAB_Cfg, 'Defina a configura��o do boleto!') then Exit;
  //
  Valor := Geral.DMV(EdValor.Text);
  if (Valor < 0.01) and (Valor > -0.01) and (RGFator.ItemIndex = 0) then
  begin
    if Geral.MensagemBox('Nenhum valor foi definido! Deseja continuar assim mesmo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      EdValor.SetFocus;
      Exit;
    end;
  end;
  Percent := Geral.DMV(EdPercent.Text);
  if (Percent < 0.0001) and (Percent > -0.0001) and (RGFator.ItemIndex = 1) then
  begin
    if Geral.MensagemBox('Nenhum percentual foi definido! Deseja continuar assim mesmo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      EdPercent.SetFocus;
      Exit;
    end;
  end;
  PeriodoA := Geral.Periodo2000(Date);
  Parcelas := Geral.IMV(EdParcelas.Text);
  //
  ParcPerI := CBMesI.ItemIndex + 1;
  if SitCobr = 2 then
  begin
    Ano := Geral.IMV(CBAnoI.Text);
    ParcPerI := ((Ano - 2000) * 12) + ParcPerI;
  end;
  //
  ParcPerF := CBMesF.ItemIndex + 1;
  if SitCobr = 2 then
  begin
    Ano := Geral.IMV(CBAnoF.Text);
    ParcPerF := ((Ano - 2000) * 12) + ParcPerF;
    //
    if (ParcPerF < PeriodoA) then
    if Geral.MensagemBox('Per�odo j� expirado! Deseja continuar assim mesmo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
    //
    pp := ParcPerF - ParcPerI + 1;
    //
    if MyObjects.FIC(Parcelas <> pp, nil, 'N�mero de parcelas n�o confere com per�odo! ' +
      sLineBreak + 'Confirma��o abortada!') then Exit;
  end;
  Codigo       := QrArreBaCCodigo.Value;
  Texto        := EdTexto.Text;
  InfoParc     := MLAGeral.BoolToInt(CkInfoParc.Checked);
  Fator        := RGFator.ItemIndex;
  DeQuem       := RGDeQuem.ItemIndex;
  DoQue        := RGDoQue.ItemIndex;
  ComoCobra    := RGComoCobra.ItemIndex;
  NaoArreSobre := MLAGeral.BoolToInt(CkNaoArreSobre.Checked);
  NaoArreRisco := MLAGeral.BoolToInt(CkNaoArreRisco.Checked);
  Calculo      := RGCalculo.ItemIndex;
  Arredonda    := dmkEdArredonda.ValueVariant;
  ListaBaU     := dmkEdListaBaU.ValueVariant;
  DescriCota   := EdDescriCota.Text;
  ValFracDef   := EdValFracDef.ValueVariant;
  ValFracAsk   := MLAGeral.BTI(CkValFracAsk.Checked);
  Partilha     := RGPartilha.ItemIndex;
  InfoRelArr   := MLAGeral.BTI(CkInfoRelArr.Checked);
  DestCobra    := RGDestCobra.ItemIndex;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('arrebai', 'Controle', ImgTipo.SQLType,
    QrArreBaIControle.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'arrebai', False, [
    'Codigo', 'Cond', 'Valor',
    'Texto', 'SitCobr', 'Parcelas',
    'ParcPerI', 'ParcPerF', 'InfoParc',
    'Fator', 'Percent', 'DeQuem',
    'DoQue', 'ComoCobra', 'NaoArreSobre',
    'Calculo', 'Arredonda', 'ListaBaU',
    'DescriCota', 'ValFracDef', 'ValFracAsk',
    'Partilha', 'InfoRelArr', 'NaoArreRisco',
    'DestCobra', 'CNAB_Cfg', 'NFSeSrvCad'
  ], ['Controle'], [
    Codigo, Cond, Valor,
    Texto, SitCobr, Parcelas,
    ParcPerI, ParcPerF, InfoParc,
    Fator, Percent, DeQuem,
    DoQue, ComoCobra, NaoArreSobre,
    Calculo, Arredonda, ListaBaU,
    DescriCota, ValFracDef, ValFracAsk,
    Partilha, InfoRelArr, NaoArreRisco,
    DestCobra, CNAB_Cfg, NFSeSrvCad
  ], [Controle], True) then
  begin
  {
  Dmod.QrUpd.SQL.Clear;

  if ImgTipo.SQLType = stIns then
    Dmod.QrUpd.SQL.Add('INSERT INTO arrebai SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE arrebai SET ');
  //
  Dmod.QrUpd.SQL.Add('Cond=:P0, Valor=:P1, Texto=:P2, SitCobr=:P3, ');
  Dmod.QrUpd.SQL.Add('Parcelas=:P4, ParcPerI=:P5, ParcPerF=:P6, ');
  Dmod.QrUpd.SQL.Add('InfoParc=:P7, Fator=:P8, Percent=:P9, DeQuem=:P10, ');
  Dmod.QrUpd.SQL.Add('DoQue=:P11, ComoCobra=:P12, NaoArreSobre=:P13, ');
  Dmod.QrUpd.SQL.Add('Calculo=:P14, Arredonda=:P15, ListaBaU=:P16, ');
  Dmod.QrUpd.SQL.Add('DescriCota=:P17, ValFracDef=:P18, ValFracAsk=:P19, ');
  Dmod.QrUpd.SQL.Add('Partilha=:P20, InfoRelArr=:P21, ');
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ArreBaI', 'ArreBaI', 'Controle');
  end else begin
    Dmod.QrUpd.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
    Controle := QrArreBaIControle.Value;
  end;
  Dmod.QrUpd.Params[00].AsInteger := Cond;
  Dmod.QrUpd.Params[01].AsFloat   := Valor;
  Dmod.QrUpd.Params[02].AsString  := EdTexto.Text;
  Dmod.QrUpd.Params[03].AsInteger := SitCobr;
  Dmod.QrUpd.Params[04].AsInteger := Parcelas;
  Dmod.QrUpd.Params[05].AsInteger := ParcPerI;
  Dmod.QrUpd.Params[06].AsInteger := ParcPerF;
  Dmod.QrUpd.Params[07].AsInteger := MLAGeral.BoolToInt(CkInfoParc.Checked);
  Dmod.QrUpd.Params[08].AsInteger := RGFator.ItemIndex;
  Dmod.QrUpd.Params[09].AsFloat   := Percent;
  Dmod.QrUpd.Params[10].AsInteger := RGDeQuem.ItemIndex;
  Dmod.QrUpd.Params[11].AsInteger := RGDoQue.ItemIndex;
  Dmod.QrUpd.Params[12].AsInteger := RGComoCobra.ItemIndex;
  Dmod.QrUpd.Params[13].AsInteger := MLAGeral.BoolToInt(CkNaoArreSobre.Checked);
  Dmod.QrUpd.Params[14].AsInteger := RGCalculo.ItemIndex;
  Dmod.QrUpd.Params[15].AsFloat   := dmkEdArredonda.ValueVariant;
  Dmod.QrUpd.Params[16].AsFloat   := dmkEdListaBaU.ValueVariant;
  Dmod.QrUpd.Params[17].AsString  := EdDescriCota.Text;
  Dmod.QrUpd.Params[18].AsFloat   := EdValFracDef.ValueVariant;
  Dmod.QrUpd.Params[19].AsInteger := MLAGeral.BTI(CkValFracAsk.Checked);
  Dmod.QrUpd.Params[20].AsInteger := RGPartilha.ItemIndex;
  Dmod.QrUpd.Params[21].AsInteger := MLAGeral.BTI(CkInfoRelArr.Checked);
  //
  Dmod.QrUpd.Params[22].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[23].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[24].AsInteger := QrArreBaCCodigo.Value;
  Dmod.QrUpd.Params[25].AsInteger := Controle;
  //
  Dmod.QrUpd.ExecSQL;
  }
    ReopenQrArreBaI(Controle);
    //
    MostraEdicao(0, stLok, 0);
    if FMostra > 0 then Close;
  end;
end;

procedure TFmArreBaC.RGSitCobrClick(Sender: TObject);
begin
  case RgSitCobr.ItemIndex of
    0:
    begin
      GbPer.Visible := False;
      GbPro.Visible := False;
    end;
    1:
    begin
      GbPer.Visible := True;
      GbPro.Visible := False;
      LaAnoI.Visible := False;
      CBAnoI.Visible := False;
      LaAnoF.Visible := False;
      CBAnoF.Visible := False;
    end;
    2:
    begin
      GbPer.Visible := True;
      GbPro.Visible := True;
      LaAnoI.Visible := True;
      CBAnoI.Visible := True;
      LaAnoF.Visible := True;
      CBAnoF.Visible := True;
    end;
  end;
end;

procedure TFmArreBaC.Editaarrecadaoselecionada1Click(Sender: TObject);
begin
  MostraEdicao(2, stUpd, 0);
end;

procedure TFmArreBaC.Retiraocondominiodaarrecadaoselecionada1Click(
  Sender: TObject);
var
  Prox: Integer;
begin
  if Geral.MensagemBox('Confirma a retirada da empresa "' +
  QrArreBaINOMECOND.Value + '" desta arrecada��o base?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM arrebai WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrArreBaIControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Prox := UMyMod.ProximoRegistro(QrArreBaI, 'Controle', QrArreBaIControle.Value);
    ReopenQrArreBaI(Prox);
  end;
end;

procedure TFmArreBaC.QrArreBaICalcFields(DataSet: TDataSet);
var
  Ini, Fim: String;
begin
  QrArreBaINOMESITCOBR.Value := RGSitCobr.Items[QrArreBaISitCobr.Value];
  QrArreBaINOMEFATOR.Value := RGFator.Items[QrArreBaIFator.Value];
  QrArreBaINOMEDEQUEM.Value := RGDeQuem.Items[QrArreBaIDeQuem.Value];
  case QrArreBaISitCobr.Value of
    1:
    begin
      Ini := dmkPF.VerificaMes(QrArreBaIParcPerI.Value, False);
      Fim := dmkPF.VerificaMes(QrArreBaIParcPerF.Value, False);
    end;
    2:
    begin
      Ini := dmkPF.MesEAnoDoPeriodo(QrArreBaIParcPerI.Value);
      Fim := dmkPF.MesEAnoDoPeriodo(QrArreBaIParcPerF.Value);
    end;
    else
    begin
      Ini := '';
      Fim := '';
    end;
  end;
  QrArreBaIINICIO.Value := Ini;
  QrArreBaIFINAL.Value := Fim;
  //
  if QrArreBaIInfoParc.Value = 0 then
    QrArreBaIInfoParc_TXT.Value := 'N�o'
  else
    QrArreBaIInfoParc_TXT.Value := 'Sim';
end;

procedure TFmArreBaC.QrListaCondAfterScroll(DataSet: TDataSet);
begin
  QrListaItens.Close;
  QrListaItens.Params[0].AsInteger := QrListaCondCodigo.Value;
  QrListaItens.Open;
end;

procedure TFmArreBaC.Timer1Timer(Sender: TObject);
begin
  if FMostra > 0 then
  begin
    Timer1.Enabled := False;
    if FMostrou = False then
    begin
      FMostrou := True;
      if FMostra = 2 then MostraEdicao(2, stUpd, 0);
    end;
  end;
end;

procedure TFmArreBaC.EdPesqCondChange(Sender: TObject);
begin
  ReopenListCond();
end;

procedure TFmArreBaC.BtUnidadesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMUnidades, BtUnidades);
end;

procedure TFmArreBaC.Incluiunidade1Click(Sender: TObject);
begin
  Application.CreateForm(TFmArreBaCAddUni, FmArreBaCAddUni);
  FmArreBaCAddUni.ImgTipo.SQLType := stIns;
  FmArreBaCAddUni.QrAptos.Close;
  FmArreBaCAddUni.QrAptos.SQL.Clear;
  FmArreBaCAddUni.QrAptos.SQL.Add('SELECT cni.Andar, cni.Conta, cni.Unidade');
  FmArreBaCAddUni.QrAptos.SQL.Add('FROM condimov cni');
  FmArreBaCAddUni.QrAptos.SQL.Add('WHERE Codigo=:P0');
  FmArreBaCAddUni.QrAptos.SQL.Add('AND cni.Conta NOT IN (');
  FmArreBaCAddUni.QrAptos.SQL.Add('  SELECT Apto FROM arrebau');
  FmArreBaCAddUni.QrAptos.SQL.Add('  WHERE Codigo=:P1');
  FmArreBaCAddUni.QrAptos.SQL.Add('  AND Controle=:P2)');
  FmArreBaCAddUni.QrAptos.SQL.Add('ORDER BY cni.Andar, Unidade');
  FmArreBaCAddUni.QrAptos.SQL.Add('');
  FmArreBaCAddUni.QrAptos.Params[00].AsInteger := QrArreBaICond.Value;
  FmArreBaCAddUni.QrAptos.Params[01].AsInteger := QrArreBaCCodigo.Value;
  FmArreBaCAddUni.QrAptos.Params[02].AsInteger := QrArreBaIControle.Value;
  FmArreBaCAddUni.QrAptos.Open;
  //
  FmArreBaCAddUni.dmkLabel1.Caption            := QrArreBaINOME_CALCULO.Value + ':';
  FmArreBaCAddUni.dmkLabel1.Enabled            := QrArreBaICalculo.Value > 0;
  FmArreBaCAddUni.dmkEdPercent.Enabled         := QrArreBaICalculo.Value > 0;
  //
  FmArreBaCAddUni.ShowModal;
  FmArreBaCAddUni.Destroy;
end;

procedure TFmArreBaC.ReopenQrArreBaU(Conta: Integer);
var
  Controle: Integer;
begin
  if QrArreBaIListaBaU.Value > 0 then
    Controle := QrArreBaIListaBaU.Value
  else
    Controle := QrArreBaIControle.Value;
  //
  QrArreBaU.Close;
  QrArreBaU.Params[0].AsInteger := Controle;
  QrArreBaU.Open;
  //
  QrArreBaU.Locate('Conta', Conta, []);
  //
  QrSABaU.Close;
  QrSABaU.Params[0].AsInteger := Controle;
  QrSABaU.Open;
  //
end;

procedure TFmArreBaC.QrArreBaIAfterScroll(DataSet: TDataSet);
begin
  ReopenQrArreBaU(0);
  DBGUnid.Columns[1].Title.Caption := QrArreBaINOME_CALCULO.Value;
end;

procedure TFmArreBaC.QrArreBaIBeforeClose(DataSet: TDataSet);
begin
  BtUnidades.Enabled := False;
  QrArreBaU.Close;
end;

procedure TFmArreBaC.QrArreBaIAfterOpen(DataSet: TDataSet);
begin
  BtUnidades.Enabled := MLAGeral.IntToBool_Query(QrArreBaI);
end;

procedure TFmArreBaC.PMUnidadesPopup(Sender: TObject);
begin
  Retiraunidade1.Enabled := MLAGeral.IntToBool_Query(QrArreBaU);
end;

procedure TFmArreBaC.Retiraunidade1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a retirada da unidade '+
  QrArreBaUUnidade.Text + ' do condom�nio ' + QrArreBaINOMECOND.Value +
  '?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM arrebau WHERE Conta=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrArreBaUConta.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrArreBaU(UMyMod.ProximoRegistro(
      QrArreBaU, 'Conta', QrArreBaUConta.Value));
  end;
end;

procedure TFmArreBaC.QrAptosCalcFields(DataSet: TDataSet);
begin
  QrAptosSELECIONADO.Value := MLAGeral.BoolToInt(
    QrAptosSel.Value = QrAptosConta.Value);
end;

procedure TFmArreBaC.Incluidiversasunidades1Click(Sender: TObject);
begin
  MostraFmArreBaCAptos(stIns);
end;

procedure TFmArreBaC.Retiradiversasunidades1Click(Sender: TObject);
begin
  MostraFmArreBaCAptos(stDel);
end;

procedure TFmArreBaC.MostraFmArreBaCAptos(Acao: TSQLType);
var
  Selecionado: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    //FSdoOldNew :=
    UCriar.RecriaTempTable('UnidCond', DmodG.QrUpdPID1, False);
    QrAptos.Close;
    QrAptos.Params[00].AsInteger := QrArreBaIControle.Value;
    QrAptos.Params[01].AsInteger := QrArreBaICond.Value;
    QrAptos.Open;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO unidcond SET ');
    DModG.QrUpdPID1.SQL.Add('Andar=:P0, Apto=:P1, Unidade=:P2, Proprie=:P3, Selecio=:P4');
    DModG.QrUpdPID1.SQL.Add('');
    while not QrAptos.Eof do
    begin
      //if Acao = stDel then
        Selecionado := 0;
      //else
        //Selecionado := QrAptosSELECIONADO.Value;
      DModG.QrUpdPID1.Params[00].AsInteger  := QrAptosAndar.Value;
      DModG.QrUpdPID1.Params[01].AsInteger  := QrAptosConta.Value;
      DModG.QrUpdPID1.Params[02].AsString   := QrAptosUnidade.Value;
      DModG.QrUpdPID1.Params[03].AsString   := QrAptosNOMEDONO.Value;
      DModG.QrUpdPID1.Params[04].AsInteger  := Selecionado;
      DModG.QrUpdPID1.ExecSQL;
      //
      QrAptos.Next;
    end;
    Application.CreateForm(TFmArreBaCAptos, FmArreBaCAptos);
    FmArreBaCAptos.ImgTipo.SQLType       := Acao;
    FmArreBaCAptos.dmkLabel1.Caption    := QrArreBaINOME_CALCULO.Value + ':';
    FmArreBaCAptos.dmkLabel1.Enabled    := QrArreBaICalculo.Value > 0;
    FmArreBaCAptos.dmkEdPercent.Enabled := QrArreBaICalculo.Value > 0;
    FmArreBaCAptos.Panel3.Visible       := Acao <> stDel;
  //
  finally
    Screen.Cursor := crDefault;
  end;
  FmArreBaCAptos.ShowModal;
  FmArreBaCAptos.Destroy;
end;

procedure TFmArreBaC.Alteraunidadeatual1Click(Sender: TObject);
begin
  Application.CreateForm(TFmArreBaCAddUni, FmArreBaCAddUni);
  FmArreBaCAddUni.ImgTipo.SQLType := stUpd;
  FmArreBaCAddUni.QrAptos.Close;
  FmArreBaCAddUni.QrAptos.SQL.Clear;
  FmArreBaCAddUni.QrAptos.SQL.Add('SELECT cni.Conta, cni.Unidade');
  FmArreBaCAddUni.QrAptos.SQL.Add('FROM condimov cni');
  FmArreBaCAddUni.QrAptos.SQL.Add('WHERE Conta=:P0');
  FmArreBaCAddUni.QrAptos.Params[00].AsInteger := QrArreBaUApto.Value;
  FmArreBaCAddUni.QrAptos.Open;
  //
  FmArreBaCAddUni.dmkEdCBApto.ValueVariant    := QrArreBaUApto.Value;
  FmArreBaCAddUni.dmkCBApto.KeyValue          := QrArreBaUApto.Value;
  FmArreBaCAddUni.dmkEdCBApto.Enabled         := False;
  FmArreBaCAddUni.dmkCBApto.Enabled           := False;
  FmArreBaCAddUni.dmkLabel1.Caption           := QrArreBaINOME_CALCULO.Value + ':';
  //FmArreBaCAddUni.RGCalculo.ItemIndex         := QrArreBaUCalculo.Value;
  FmArreBaCAddUni.dmkEdPercent.ValueVariant   := QrArreBaUPercent.Value;
  //FmArreBaCAddUni.dmkEdArredonda.ValueVariant := QrArreBaUArredonda.Value;
  //
  FmArreBaCAddUni.dmkLabel1.Enabled           := QrArreBaICalculo.Value > 0;
  FmArreBaCAddUni.dmkEdPercent.Enabled        := QrArreBaICalculo.Value > 0;
  //
  FmArreBaCAddUni.ShowModal;
  FmArreBaCAddUni.Destroy;
end;

procedure TFmArreBaC.ReopenListaBaU(Cond: Integer);
begin
  QrListaBaU.Close;
  if Cond <> 0 then
  begin
    QrListaBaU.Params[0].AsInteger := Cond;
    QrListaBaU.Open;
  end;  
end;

procedure TFmArreBaC.ReopenListCond();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrListaCond, Dmod.MyDB, [
  'SELECT cnd.Codigo,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CND',
  'FROM cond cnd',
  'LEFT JOIN Entidades ent ON ent.Codigo=cnd.Cliente',
  'WHERE cnd.Codigo IN (' + VAR_LIB_FILIAIS + ')',
  'AND IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE "%' + EdPesqCond.Text + '%"',
  'ORDER BY NOME_CND',
  '']);
end;

procedure TFmArreBaC.EdEmpresaChange(Sender: TObject);
var
  Empresa: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  //
  ReopenListaBaU(Empresa);
  //
  if Empresa <> 0 then
    UBloqGerl.ReopenCNAB_Cfg(QrCNAB_Cfg, Dmod.MyDB, DModG.QrEmpresasCodigo.Value)
  else
    QrCNAB_Cfg.Close;
end;

end.

