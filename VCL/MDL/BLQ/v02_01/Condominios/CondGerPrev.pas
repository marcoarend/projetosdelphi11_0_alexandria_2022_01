unit CondGerPrev;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls,
  Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage,
  UnDmkEnums;

type
  TFmCondGerPrev = class(TForm)
    Panel1: TPanel;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    DsContas: TDataSource;
    Label2: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    LaDeb: TLabel;
    EdValor: TdmkEdit;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(SQLType: TSQLType);
  public
    { Public declarations }
    FQueryPri: TmySQLQuery;
    FPrevCodigo: Integer;
    FTabela, FPrevPeriodoTxt: String;
  end;

  var
  FmCondGerPrev: TFmCondGerPrev;

implementation

uses Module, UnInternalConsts, UMySQLModule, UnMyObjects, DmkDAC_PF;

{$R *.DFM}

procedure TFmCondGerPrev.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerPrev.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerPrev.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerPrev.FormShow(Sender: TObject);
begin
  MostraEdicao(ImgTipo.SQLType);
end;

procedure TFmCondGerPrev.MostraEdicao(SQLType: TSQLType);
begin
  ImgTipo.SQLType := SQLType;
  //
  if SQLType = stUpd then
  begin
    EdConta.ValueVariant     := FQueryPri.FieldByName('Conta').AsInteger;
    CBConta.KeyValue         := FQueryPri.FieldByName('Conta').AsInteger;
    EdValor.Text             := Geral.FFT(FQueryPri.FieldByName('Valor').AsFloat, 2, siPositivo);
    EdDescricao.ValueVariant := FQueryPri.FieldByName('Texto').AsString;
    CkContinuar.Visible      := False;
    CkContinuar.Checked      := False;
  end else
  begin
    EdConta.ValueVariant     := 0;
    CBConta.KeyValue         := Null;
    EdValor.ValueVariant     := 0;
    EdDescricao.ValueVariant := Null;
    CkContinuar.Visible      := True;
    CkContinuar.Checked      := False;
  end;
  EdConta.SetFocus;
end;

procedure TFmCondGerPrev.EdValorExit(Sender: TObject);
begin
  EdValor.Text := Geral.TFT(EdValor.Text, 2, siPositivo);
end;

procedure TFmCondGerPrev.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if QrContasMensal.Value = 'V' then
      Mes := FPrevPeriodoTxt
    else
      Mes := '';
    if key=VK_F4 then
      EdDescricao.Text := QrContasNome.Value + ' ' + Mes;
    if key=VK_F5 then
      EdDescricao.Text := QrContasNome2.Value + ' ' + Mes;
    if key=VK_F6 then
      EdDescricao.Text := QrContasNome3.Value + ' ' + Mes;
    //
    EdDescricao.SetFocus;
    EdDescricao.SelStart  := Length(EdDescricao.Text);
    EdDescricao.SelLength := 0;
  end;
end;

procedure TFmCondGerPrev.BtOKClick(Sender: TObject);
var
  Conta, Controle: Integer;
  Valor: Double;
  Texto: String;
begin
  if MyObjects.FIC(FTabela = '', nil, 'Tabela n�o informada!') then Exit;
  if MyObjects.FIC(FPrevCodigo = 0, nil, 'CodPrev n�o informado!') then Exit;
  //
  Conta := EdConta.ValueVariant;
  Valor := EdValor.ValueVariant;
  Texto := Trim(EdDescricao.ValueVariant);
  //
  if MyObjects.FIC(Conta = 0, EdConta, 'A conta deve ser informada!') then Exit;
  if MyObjects.FIC(Valor < 0.01, EdValor, 'O valor deve ser informado!') then Exit;
  if MyObjects.FIC(Texto = '', EdDescricao, 'Defina uma descri��o!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', FTabela, TAB_PRI, 'Controle')
  else
    Controle := FQueryPri.FieldByName('Controle').AsInteger;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, FTabela, False,
    ['Conta', 'Texto', 'Valor', 'Codigo'], ['Controle'],
    [Conta, Texto, Valor, FPrevCodigo], [Controle], True) then
  begin
    if CkContinuar.Checked then
      MostraEdicao(stIns)
    else
      Close;
  end;
end;

procedure TFmCondGerPrev.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
end;

end.
