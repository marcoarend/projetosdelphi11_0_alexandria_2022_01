unit BloArreIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkLabel, DBCtrls, dmkGeral,
  dmkDBLookupComboBox, dmkEditCB, UnMsgInt, DB, mySQLDbTables, Variants, Menus,
  dmkValUsu, dmkPermissoes, UnDmkProcFunc, dmkImage, DmkDAC_PF, UnDmkEnums;

type
  TFmBloArreIts = class(TForm)
    Panel1: TPanel;
    Label12: TLabel;
    EdCodigo: TdmkEdit;
    RGSitCobr: TRadioGroup;
    GBPer: TGroupBox;
    GBIni: TGroupBox;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBMesI: TComboBox;
    CBAnoI: TComboBox;
    GBFim: TGroupBox;
    Label34: TLabel;
    LaAnoF: TLabel;
    CBMesF: TComboBox;
    CBAnoF: TComboBox;
    GbPro: TGroupBox;
    Label7: TLabel;
    Label1: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    SpeedButton7: TSpeedButton;
    EdParcelas: TdmkEdit;
    GBValor: TGroupBox;
    Label17: TLabel;
    CkValor: TCheckBox;
    EdValor: TdmkEdit;
    Label2: TLabel;
    EdTexto: TdmkEdit;
    CkInfoParc: TCheckBox;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    DsEntidades: TDataSource;
    QrEntidadesNOMEENT: TWideStringField;
    Label19: TLabel;
    EdArredonda: TdmkEdit;
    QrCnab_Cfg: TmySQLQuery;
    QrCnab_CfgCodigo: TIntegerField;
    QrCnab_CfgNome: TWideStringField;
    DsCnab_Cfg: TDataSource;
    RGDiaVencto: TRadioGroup;
    CkAtivo: TCheckBox;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    Label4: TLabel;
    VuEmpresa: TdmkValUsu;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    QrContratos: TmySQLQuery;
    DsContratos: TDataSource;
    LaContrato: TLabel;
    EdContrato: TdmkEditCB;
    CBContrato: TdmkDBLookupComboBox;
    SBContrato: TSpeedButton;
    QrContratosCodigo: TIntegerField;
    QrContratosContratada: TIntegerField;
    QrContratosContratante: TIntegerField;
    QrContratosNome: TWideStringField;
    QrContratosDVencimento: TDateField;
    QrContratosDtaPrxRenw: TDateField;
    QrContratosValorMes: TFloatField;
    QrContratosddMesVcto: TSmallintField;
    Label3: TLabel;
    CBCNABConf: TdmkDBLookupComboBox;
    EdCNABConf: TdmkEditCB;
    SpeedButton1: TSpeedButton;
    LaNFSeSrvCad: TLabel;
    EdNFSeSrvCad: TdmkEditCB;
    CBNFSeSrvCad: TdmkDBLookupComboBox;
    SBNFSeSrvCad: TSpeedButton;
    QrNFSeSrvCad: TmySQLQuery;
    QrNFSeSrvCadCodigo: TIntegerField;
    QrNFSeSrvCadNome: TWideStringField;
    DsNFSeSrvCad: TDataSource;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGSitCobrClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CBMesIChange(Sender: TObject);
    procedure CBAnoIChange(Sender: TObject);
    procedure CBMesFChange(Sender: TObject);
    procedure CBAnoFChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SBContratoClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdContratoChange(Sender: TObject);
    procedure SBNFSeSrvCadClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Controle: Integer);
    procedure AtualizaAtivo(Controle: Integer);
    procedure ReabreContratos(Contratada, Contratante: Integer);
    procedure ConfiguraCampos(Contrato: Integer);
    procedure ReabreCNAB_Cfg(Empresa: Integer);
  public
    { Public declarations }
    FMostra: Boolean;
    FCodigo, FControle: Integer;
  end;

  var
  FmBloArreIts: TFmBloArreIts;

implementation

uses
  {$IfNDef sCNTR} Contratos, {$EndIf}
  {$IfNDef sNFSe} NFSe_PF_0000, {$EndIf}
  Entidade2, MyDBCheck, UnInternalConsts, BloArre, UnGotoy, UnMyObjects,
  UMySQLModule, Module, CNAB_Cfg, ModuleGeral, UnBloquetos, MyListas;

{$R *.DFM}

procedure TFmBloArreIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Empresa, Entidade, SitCobr, Parcelas, ParcPerI, PeriodoA,
  ParcPerF, Ano, pp, InfoParc, ValorInf, Cnab_Cfg, Contrato, NFSeSrvCad,
  DiaVencto: Integer;
  Valor, Arredonda: Double;
  Texto: String;
begin
  Codigo     := FCodigo;
  Empresa    := EdEmpresa.ValueVariant;
  Entidade   := EdEntidade.ValueVariant;
  Contrato   := EdContrato.ValueVariant;
  NFSeSrvCad := EdNFSeSrvCad.ValueVariant;
  Texto      := EdTexto.ValueVariant;
  InfoParc   := Geral.BoolToInt(CkInfoParc.Checked);
  Arredonda  := EdArredonda.ValueVariant;
  Cnab_Cfg   := EdCNABConf.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  if MyObjects.FIC(Entidade = 0, EdEntidade, 'Informe a entidade!') then Exit;
  if MyObjects.FIC(Cnab_Cfg = 0, EdCNABConf, 'Informe a configura��o!') then Exit;
  //
  if Contrato <> 0 then
  begin
    if MyObjects.FIC(QrContratosddMesVcto.Value = 0, nil,
      'Dia do vencimento n�o definido!') then Exit;
    if MyObjects.FIC(QrContratosDVencimento.Value < 2, nil,
      'Data do primeiro vencimento n�o definida no cadastro do contrato!') then Exit;
    if MyObjects.FIC(QrContratosDtaPrxRenw.Value < 2, nil,
      'Data da pr�xima renova��o n�o definida no cadastro do contrato!') then Exit;
    //
    DiaVencto := 0;
    Valor     := 0;
    ValorInf  := 0;
    SitCobr   := 3;
    Parcelas  := 0;
    ParcPerI  := 0;
    ParcPerF  := 0;
  end else
  begin
    DiaVencto := RGDiaVencto.ItemIndex + 1;
    Valor     := EdValor.ValueVariant;
    ValorInf  := Geral.BoolToInt(CkValor.Checked);
    SitCobr   := RGSitCobr.ItemIndex;
    Parcelas  := EdParcelas.ValueVariant;
    PeriodoA  := Geral.Periodo2000(Date);
    ParcPerI  := CBMesI.ItemIndex + 1;
    //
    if MyObjects.FIC(RGDiaVencto.ItemIndex < 0, RGDiaVencto, 'Defina o dia do m�s do vencimento!') then Exit;
    //    
    if Valor = 0 then
    begin
      if Geral.MB_Pergunta(
      'Nenhum valor foi definido! Deseja continuar assim mesmo?') <> ID_YES then
      begin
        EdValor.SetFocus;
        Exit;
      end;
    end;
    if SitCobr = 2 then
    begin
      Ano := Geral.IMV(CBAnoI.Text);
      ParcPerI := ((Ano - 2000) * 12) + ParcPerI;
    end;
    //
    ParcPerF  := CBMesF.ItemIndex + 1;
    //
    if SitCobr = 2 then
    begin
      Ano := Geral.IMV(CBAnoF.Text);
      ParcPerF := ((Ano - 2000) * 12) + ParcPerF;
      //
      if (ParcPerF < PeriodoA) then
      if Geral.MB_Pergunta(
      'Per�odo j� expirado! Deseja continuar assim mesmo?') <> ID_YES then Exit;
      //
      pp := ParcPerF - ParcPerI + 1;
      if Parcelas <> pp then
      begin
        Geral.MB_Aviso('N�mero de parcelas n�o confere com per�odo! ' +
        sLineBreak + 'Confirma��o abortada!');
        Exit;
      end;
    end;
  end;
  //
  if ImgTipo.SQLType = stIns then
    Controle := UMyMod.BuscaEmLivreY_Def('bloarreits', 'Controle', ImgTipo.SQLType, FControle)
  else
    Controle := FControle;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'bloarreits', False,
  [
    'Empresa', 'Entidade', 'Valor', 'Texto', 'SitCobr', 'Parcelas', 'ParcPerI',
    'ParcPerF', 'InfoParc', 'Arredonda', 'ValorInf', 'DiaVencto', 'Cnab_Cfg',
    'Contrato', 'NFSeSrvCad', 'Ativo', 'Codigo'
  ], ['Controle'],
  [
    VuEmpresa.ValueVariant, Entidade, Valor, Texto, SitCobr, Parcelas, ParcPerI,
    ParcPerF, InfoParc, Arredonda, ValorInf, DiaVencto, Cnab_Cfg,
    Contrato, NFSeSrvCad, CkAtivo.Checked, Codigo
  ], [Controle], True) then
  begin
    if not FMostra then
      AtualizaAtivo(FControle);  
    //
    FmBloArre.ReopenBloArreIts(Controle);
    Close;
  end;
end;

procedure TFmBloArreIts.AtualizaAtivo(Controle: Integer);
begin
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE bloarreits SET Ativo=0 WHERE Controle=:P0');
  Dmod.QrUpd.Params[0].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
end;

procedure TFmBloArreIts.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloArreIts.CBAnoFChange(Sender: TObject);
begin
  EdParcelas.ValueVariant := UBloquetos.VerificaParcelas(RGSitCobr.ItemIndex,
    CBMesI.ItemIndex, CBMesF.ItemIndex, CBAnoI.Text, CBAnoF.Text, True);
end;

procedure TFmBloArreIts.CBAnoIChange(Sender: TObject);
begin
  EdParcelas.ValueVariant := UBloquetos.VerificaParcelas(RGSitCobr.ItemIndex,
    CBMesI.ItemIndex, CBMesF.ItemIndex, CBAnoI.Text, CBAnoF.Text, True);
end;

procedure TFmBloArreIts.CBMesFChange(Sender: TObject);
begin
  EdParcelas.ValueVariant := UBloquetos.VerificaParcelas(RGSitCobr.ItemIndex,
    CBMesI.ItemIndex, CBMesF.ItemIndex, CBAnoI.Text, CBAnoF.Text, True);
end;

procedure TFmBloArreIts.CBMesIChange(Sender: TObject);
begin
  EdParcelas.ValueVariant := UBloquetos.VerificaParcelas(RGSitCobr.ItemIndex,
    CBMesI.ItemIndex, CBMesF.ItemIndex, CBAnoI.Text, CBAnoF.Text, True);
end;

procedure TFmBloArreIts.ConfiguraCampos(Contrato: Integer);
var
  Enab: Boolean;
begin
  Enab := Contrato = 0;
  //
  GBValor.Visible     := Enab;
  RGSitCobr.Visible   := Enab;
  GbPro.Visible       := Enab;
  RGDiaVencto.Visible := Enab;
  GBPer.Visible       := Enab;
end;

procedure TFmBloArreIts.EdContratoChange(Sender: TObject);
begin
  ConfiguraCampos(EdContrato.ValueVariant);
end;

procedure TFmBloArreIts.EdEmpresaChange(Sender: TObject);
begin
  ReabreContratos(VUEmpresa.ValueVariant, EdEntidade.ValueVariant);
  ReabreCNAB_Cfg(VUEmpresa.ValueVariant);
end;

procedure TFmBloArreIts.EdEntidadeChange(Sender: TObject);
begin
  ReabreContratos(VUEmpresa.ValueVariant, EdEntidade.ValueVariant);
  //
  ConfiguraCampos(EdContrato.ValueVariant);
end;

procedure TFmBloArreIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  MostraEdicao(FMostra, ImgTipo.SQLType, FControle); 
end;

procedure TFmBloArreIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.PreencheCBAnoECBMes(CBAnoI, CBMesI, 0);
  MyObjects.PreencheCBAnoECBMes(CBAnoF, CBMesF, 0);
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  UMyMod.AbreQuery(QrEntidades, DMod.MyDB);
  //
  {$IfNDef sNFSe}
  UMyMod.AbreQuery(QrNFSeSrvCad, Dmod.MyDB);
  {$Else}
  QrNFSeSrvCad.Close;
  {$EndIf}
end;

procedure TFmBloArreIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloArreIts.MostraEdicao(Mostra: Boolean; SQLType: TSQLType;
  Controle: Integer);
var
  Mes, Ano: Word;
  TemCNTR, TemNFSe: Boolean;
begin
  {$IfNDef sCNTR}
  TemCNTR := DBCheck.LiberaModulo(CO_DMKID_APP, dmkPF.ObtemSiglaModulo(mdlappContratos),
               DModG.QrMaster.FieldByName('HabilModulos').AsString);
  {$Else}
  TemCNTR := False;
  {$EndIf}
  LaContrato.Visible := TemCNTR;
  EdContrato.Visible := TemCNTR;
  CBContrato.Visible := TemCNTR;
  SBContrato.Visible := TemCNTR;
  //
  {$IfNDef sNFSe}
  TemNFSe := DBCheck.LiberaModulo(CO_DMKID_APP, dmkPF.ObtemSiglaModulo(mdlappNFSe),
               DModG.QrMaster.FieldByName('HabilModulos').AsString);
  {$Else}
  TemNFSe := False;
  {$EndIf}
  LaNFSeSrvCad.Visible := TemNFSe;
  EdNFSeSrvCad.Visible := TemNFSe;
  CBNFSeSrvCad.Visible := TemNFSe;
  SBNFSeSrvCad.Visible := TemNFSe;
  //
  if Mostra then
  begin
    if SQLType = stIns then
    begin
      EdCodigo.ValueVariant     := FormatFloat('000', Controle);
      EdCNABConf.ValueVariant   := 0;
      CBCNABConf.KeyValue       := Null;
      EdEmpresa.ValueVariant    := DmodG.QrFiliLogFilial.Value;
      CBEmpresa.KeyValue        := DmodG.QrFiliLogFilial.Value;
      EdEntidade.ValueVariant   := 0;
      CBEntidade.KeyValue       := Null;
      EdContrato.ValueVariant   := 0;
      CBContrato.KeyValue       := Null;
      EdNFSeSrvCad.ValueVariant := 0;
      CBNFSeSrvCad.KeyValue     := Null;
      EdTexto.ValueVariant      := '';
      CkInfoParc.Checked        := False;
      EdValor.ValueVariant      := 0;
      CkValor.Checked           := False;
      RGSitCobr.ItemIndex       := -1;
      EdParcelas.ValueVariant   := 0;
      RGDiaVencto.ItemIndex     := -1;
      CkAtivo.Checked           := True;
    end else begin
      EdCodigo.ValueVariant     := FmBloArre.QrBloArreItsControle.Value;
      EdCNABConf.ValueVariant   := FmBloArre.QrBloArreItsCnab_Cfg.Value;
      CBCNABConf.KeyValue       := FmBloArre.QrBloArreItsCnab_Cfg.Value;
      EdEmpresa.ValueVariant    := FmBloArre.QrBloArreItsFilial.Value;
      CBEmpresa.KeyValue        := FmBloArre.QrBloArreItsFilial.Value;
      EdEntidade.ValueVariant   := FmBloArre.QrBloArreItsEntidade.Value;
      CBEntidade.KeyValue       := FmBloArre.QrBloArreItsEntidade.Value;
      EdContrato.ValueVariant   := FmBloArre.QrBloArreItsContrato.Value;
      CBContrato.KeyValue       := FmBloArre.QrBloArreItsContrato.Value;
      EdNFSeSrvCad.ValueVariant := FmBloArre.QrBloArreItsNFSeSrvCad.Value;
      CBNFSeSrvCad.KeyValue     := FmBloArre.QrBloArreItsNFSeSrvCad.Value;
      EdTexto.ValueVariant      := FmBloArre.QrBloArreItsTexto.Value;
      CkInfoParc.Checked        := Geral.IntToBool(FmBloArre.QrBloArreItsInfoParc.Value);
      EdValor.ValueVariant      := FmBloArre.QrBloArreItsValor.Value;
      CkValor.Checked           := Geral.IntToBool(FmBloArre.QrBloArreItsValorInf.Value);
      RGSitCobr.ItemIndex       := FmBloArre.QrBloArreItsSitCobr.Value;
      EdParcelas.ValueVariant   := FmBloArre.QrBloArreItsParcelas.Value;
      RGDiaVencto.ItemIndex     := FmBloArre.QrBloArreItsDiaVencto.Value - 1;
      CkAtivo.Checked           := Geral.IntToBool(FmBloArre.QrBloArreItsAtivo.Value);
      //
      if FmBloArre.QrBloArreItsParcPerI.Value < 13 then
      begin
        CBAnoI.Itemindex := 50;
        CBMesI.ItemIndex := FmBloArre.QrBloArreItsParcPerI.Value -1;
      end else begin
        dmkPF.MesEAnoDePeriodoShort(FmBloArre.QrBloArreItsParcPerI.Value, Mes, Ano);
        CBAnoI.Itemindex := Ano - Geral.IMV(CBAnoI.Items[0]);
        CBMesI.ItemIndex := Mes - 1;
      end;
      //
      if FmBloArre.QrBloArreItsParcPerF.Value < 13 then
      begin
        CBAnoF.Itemindex := 50;
        CBMesF.ItemIndex := FmBloArre.QrBloArreItsParcPerF.Value - 1;
      end else begin
        dmkPF.MesEAnoDePeriodoShort(FmBloArre.QrBloArreItsParcPerF.Value, Mes, Ano);
        CBAnoF.Itemindex := Ano - Geral.IMV(CBAnoF.Items[0]);
        CBMesF.ItemIndex := Mes - 1;
      end;
      //
      ReabreContratos(VuEmpresa.ValueVariant, EdEntidade.ValueVariant);
    end;
    EdCNABConf.SetFocus;
  end else
  begin
    EdCNABConf.ValueVariant  := FmBloArre.QrBloArreItsCnab_Cfg.Value;
    CBCNABConf.KeyValue      := FmBloArre.QrBloArreItsCnab_Cfg.Value;
    EdEntidade.ValueVariant  := FmBloArre.QrBloArreItsEntidade.Value;
    CBEntidade.KeyValue      := FmBloArre.QrBloArreItsEntidade.Value;
    EdTexto.ValueVariant     := FmBloArre.QrBloArreItsTexto.Value;
    EdArredonda.ValueVariant := FmBloArre.QrBloArreItsArredonda.Value;
    CkInfoParc.Checked       := Geral.IntToBool(FmBloArre.QrBloArreItsInfoParc.Value);
    EdValor.ValueVariant     := FmBloArre.QrBloArreItsValor.Value;
    RGDiaVencto.ItemIndex    := FmBloArre.QrBloArreItsDiaVencto.Value - 1;
    RGSitCobr.ItemIndex      := FmBloArre.QrBloArreItsSitCobr.Value;
    if FmBloArre.QrBloArreItsParcPerI.Value > -1000 then
    begin
      dmkPF.PeriodoDecode(FmBloArre.QrBloArreItsParcPerI.Value, Ano, Mes);
      CBAnoI.ItemIndex := Ano - Geral.IMV(CBAnoI.Items[0]);
      CBMesI.ItemIndex := Mes - 1;
    end;
    if FmBloArre.QrBloArreItsParcPerF.Value > -1000 then
    begin
      dmkPF.PeriodoDecode(FmBloArre.QrBloArreItsParcPerF.Value, Ano, Mes);
      CBAnoF.ItemIndex := Ano - Geral.IMV(CBAnoF.Items[0]);
      CBMesF.ItemIndex := Mes - 1;
    end;
    EdValor.ValueVariant    := FmBloArre.QrBloArreItsValor.Value;
    CkValor.Checked         := Geral.IntToBool(FmBloArre.QrBloArreItsValorInf.Value);
    EdParcelas.ValueVariant := UBloquetos.VerificaParcelas(RGSitCobr.ItemIndex,
                                 CBMesI.ItemIndex, CBMesF.ItemIndex,
                                 CBAnoI.Text, CBAnoF.Text, True);
    //
    EdCNABConf.SetFocus;
  end;
  ConfiguraCampos(FmBloArre.QrBloArreItsContrato.Value);
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmBloArreIts.ReabreContratos(Contratada, Contratante: Integer);
begin
  {$IfNDef sCNTR}
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratos, Dmod.MyDB, [
    'SELECT Codigo, Nome, Contratada, Contratante, ',
    'DVencimento, DtaPrxRenw, ValorMes, ddMesVcto ',
    'FROM contratos ',
    'WHERE Ativo = 1 ',
    'AND Contratada=' + Geral.FF0(Contratada),
    'AND Contratante=' + Geral.FF0(Contratante),
    '']);
{$EndIf}
end;

procedure TFmBloArreIts.ReabreCNAB_Cfg(Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCnab_Cfg, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM  cnab_cfg ',
    'WHERE Codigo > 0 ',
    'AND Ativo = 1 ',
    'AND (Cedente=' + Geral.FF0(Empresa),
    'OR SacadAvali=' + Geral.FF0(Empresa) + ')',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmBloArreIts.RGSitCobrClick(Sender: TObject);
begin
  case RGSitCobr.ItemIndex of
    0:
    begin
      GBPer.Visible := False;
      GbPro.Visible := False;
    end;
    1:
    begin
      GbPer.Visible := True;
      GbPro.Visible := False;
      LaAnoI.Visible := False;
      CBAnoI.Visible := False;
      LaAnoF.Visible := False;
      CBAnoF.Visible := False;
    end;
    2:
    begin
      GbPer.Visible := True;
      GbPro.Visible := True;
      LaAnoI.Visible := True;
      CBAnoI.Visible := True;
      LaAnoF.Visible := True;
      CBAnoF.Visible := True;
    end;
  end;
  EdParcelas.ValueVariant := UBloquetos.VerificaParcelas(RGSitCobr.ItemIndex,
    CBMesI.ItemIndex, CBMesF.ItemIndex, CBAnoI.Text, CBAnoF.Text, True);
end;

procedure TFmBloArreIts.SBNFSeSrvCadClick(Sender: TObject);
begin
  {$IfNDef sNFSe}
  VAR_CADASTRO := 0;
  //
  UnNFSe_PF_0000.MostraFormNFSeSrvCad(EdNFSeSrvCad.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdNFSeSrvCad, CBNFSeSrvCad, QrNFSeSrvCad, VAR_CADASTRO);
    //
    EdNFSeSrvCad.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmBloArreIts.SpeedButton1Click(Sender: TObject);
var
  CNAB_Cfg: Integer;
begin
  VAR_CADASTRO := 0;
  CNAB_Cfg     := EdCNABConf.ValueVariant;
  //
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    if CNAB_Cfg <> 0 then
      FmCNAB_Cfg.LocCod(CNAB_Cfg, CNAB_Cfg);
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    ReabreCNAB_Cfg(VuEmpresa.ValueVariant);
    //
    EdCNABConf.ValueVariant := VAR_CADASTRO;
    CBCNABConf.KeyValue     := VAR_CADASTRO;
    CBCNABConf.SetFocus;
  end;
end;

procedure TFmBloArreIts.SBContratoClick(Sender: TObject);
{$IfNDef sCNTR}
var
  Contrato: Integer;
begin
  VAR_CADASTRO := 0;
  Contrato     := EdContrato.ValueVariant;
  //
  if DBCheck.CriaFm(TFmContratos, FmContratos, afmoNegarComAviso) then
  begin
    if Contrato <> 0 then
      FmContratos.LocCod(Contrato, Contrato);
    FmContratos.ShowModal;
    FmContratos.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    ReabreContratos(VUEmpresa.ValueVariant, EdEntidade.ValueVariant);
    //
    EdContrato.ValueVariant := VAR_CADASTRO;
    CBContrato.KeyValue     := VAR_CADASTRO;
    CBContrato.SetFocus;
  end;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappContratos);
{$EndIf}
end;

procedure TFmBloArreIts.SpeedButton7Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DmodG.CadastroDeEntidade(EdEntidade.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEntidade, CBEntidade, QrEntidades, VAR_CADASTRO);
    EdEntidade.SetFocus;
  end;
end;

end.
