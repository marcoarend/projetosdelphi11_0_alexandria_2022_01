unit BloGerenBaA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkDBGrid,
  mySQLDbTables, DB, dmkImage, DmkDAC_PF, dmkPermissoes, UnDmkEnums,
  Vcl.ComCtrls;

type
  TFmBloGerenBaA = class(TForm)
    Panel1: TPanel;
    StaticText2: TStaticText;
    dmkDBGrid1: TdmkDBGrid;
    TbBloArre: TmySQLTable;
    TbBloArreNOMECONTABASE: TWideStringField;
    TbBloArreSeq: TIntegerField;
    TbBloArreConta: TIntegerField;
    TbBloArreBloArre: TIntegerField;
    TbBloArreBloArreIts: TIntegerField;
    TbBloArreValor: TFloatField;
    TbBloArreTexto: TWideStringField;
    TbBloArreAdiciona: TSmallintField;
    TbBloArreEntidade: TIntegerField;
    TbBloArreNomeEnti: TWideStringField;
    TbBloArreCartEmiss: TIntegerField;
    TbBloArreCNAB_Cfg: TIntegerField;
    TbBloArreDiaVencto: TIntegerField;
    DsBloArre: TDataSource;
    QrBloArr: TmySQLQuery;
    QrBloArrCodigo: TIntegerField;
    QrBloArrConta: TIntegerField;
    QrBloArrNome: TWideStringField;
    DsBloArr: TDataSource;
    QrConta: TmySQLQuery;
    QrContaZeros: TLargeintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    PB1: TProgressBar;
    TbBloArreNFSeSrvCad: TIntegerField;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure TbBloArreAfterInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetaTodos(Marca: Boolean);
  public
    { Public declarations }
    FTmpTabBloArre, FNomeCliente: String;
    FCodigo, FCodiCliente, FPeriodo, FEntidade: Integer;
  end;

  var
  FmBloGerenBaA: TFmBloGerenBaA;

implementation

uses Module, BloGeren, ModuleBloGeren, ModuleGeral, UMySQLModule, dmkGeral,
  UnInternalConsts, UnMyObjects, UnBloquetos;

{$R *.DFM}

procedure TFmBloGerenBaA.BitBtn1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a retirada do item "'+
    TbBloArreNOMECONTABASE.Value + '" destes itens?') = ID_YES then
    TbBloArre.Delete;
end;

procedure TFmBloGerenBaA.BitBtn2Click(Sender: TObject);
begin
  SetaTodos(True);
end;

procedure TFmBloGerenBaA.BitBtn3Click(Sender: TObject);
begin
  SetaTodos(False);
end;

procedure TFmBloGerenBaA.BtOKClick(Sender: TObject);
var
  DiaVcto: Word;
  //Dia, Mes, Ano, MesVcto, AnoVcto: Word;
  Vencto: TDateTime;
  Mensagem: String;
begin
  if(TbBloArre.State = dsInactive) or (TbBloArre.RecordCount = 0) then Exit;
  //
  //DecodeDate(Date, Ano, Mes, Dia);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrConta, DModG.MyPID_DB, [
    'SELECT COUNT(Adiciona) Zeros ',
    'FROM ' + FTmpTabBloArre,
    'WHERE Adiciona = 1 ',
    '']);
  if QrContaZeros.Value = 0 then
  begin
    Geral.MB_Aviso('Nenhum item foi selecionado!');
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrConta, DModG.MyPID_DB, [
    'SELECT COUNT(Valor) Zeros ',
    'FROM ' + FTmpTabBloArre,
    'WHERE Valor < 0.01 AND Adiciona=1 ',
    '']);
  if QrContaZeros.Value > 0 then
    if Geral.MB_Pergunta('Existem '+IntToStr(QrContaZeros.Value) +
    ' itens selecionados sem valor informado. Deseja continuar assim mesmo?')
    <> ID_YES then
      Exit;
  //
  TbBloArre.First;
  //
  try
    Screen.Cursor      := crHourGlass;
    dmkDBGrid1.Enabled := False;
    PB1.Max            := TbBloArre.RecordCount;
    PB1.Position       := 0;
    PB1.Visible        := True;
    //
    TbBloArre.DisableControls;
    //
    while not TbBloArre.Eof do
    begin
      if TbBloArreAdiciona.Value = 1 then
      begin
        DiaVcto := TbBloArreDiaVencto.Value;
        //DecodeDate(DModG.ObtemAgora(), Dia, Mes, Ano);
        Vencto := Geral.PeriodoToDate(FPeriodo, DiaVcto, True);
        Vencto := UBloquetos.AjustaVencimentoPeriodo(FPeriodo, Vencto);

        (*
        Vencto  := ???.PeriodoToDate(FPeriodo, DiaVcto);
        DecodeDate(Vencto, Ano, Mes, Dia);
        if Mes < 12 then
        begin
          MesVcto := Mes + 1;
          AnoVcto := Ano;
        end else
        begin
          MesVcto := 1;
          AnoVcto := Ano + 1;
        end;
        Vencto  := EncodeDate(AnoVcto, MesVcto, DiaVcto);
        *)

        if UBloquetos.AtualizaArrecadacao(stIns, FCodigo, 0, TbBloArreConta.Value,
             TbBloArreBloArre.Value, TbBloArreBloArreIts.Value,
             TbBloArreEntidade.Value, TbBloArreCartEmiss.Value,
             TbBloArreCNAB_Cfg.Value, TbBloArreNFSeSrvCad.Value,
             0, 0, 0, 0, 0, Geral.FDT(Vencto, 1), TbBloArreValor.Value,
             0, 0, TbBloArreTexto.Value, Mensagem) = 0 then
        begin
          Geral.MB_Aviso(Mensagem);
          Exit;
        end;
      end;
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      TbBloArre.Next;
    end;
  finally
    TbBloArre.EnableControls;
    //
    PB1.Visible        := False;
    dmkDBGrid1.Enabled := True;
    Screen.Cursor      := crDefault;
  end;
  FEntidade := TbBloArreEntidade.Value;
  Close;
end;

procedure TFmBloGerenBaA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloGerenBaA.dmkDBGrid1CellClick(Column: TColumn);
begin
  if (dmkDBGrid1.SelectedField.Name = 'TbBloArreAdiciona') then
  begin
    TbBloArre.Edit;
    if TbBloArreAdiciona.Value = 0 then
      TbBloArreAdiciona.Value := 1
    else
      TbBloArreAdiciona.Value := 0;
    TbBloArre.Post;
  end;
end;

procedure TFmBloGerenBaA.FormActivate(Sender: TObject);
begin
  StaticText2.Caption := FNomeCliente;
  MyObjects.CorIniComponente;
end;

procedure TFmBloGerenBaA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PB1.Visible := False;
end;

procedure TFmBloGerenBaA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloGerenBaA.FormShow(Sender: TObject);
begin
  QrBloArr.Open;
  //
  QrConta.Close;
  QrConta.Database := DModG.MyPID_DB;
  //
  TbBloArre.Close;
  TbBloArre.Database  := DModG.MyPID_DB;
  TbBloArre.TableName := FTmpTabBloArre;
  TbBloArre.Open;
end;

procedure TFmBloGerenBaA.SetaTodos(Marca: Boolean);
var
  Codigo: Integer;
begin
  Codigo := TbBloArreBloArre.Value;
  TbBloArre.First;
  while not TbBloArre.Eof do
  begin
    TbBloArre.Edit;
    TbBloArreAdiciona.Value := Geral.BoolToInt(Marca);
    TbBloArre.Post;
    //
    TbBloArre.Next;
  end;
  TbBloArre.Locate('BloArre', Codigo, []);
end;

procedure TFmBloGerenBaA.TbBloArreAfterInsert(DataSet: TDataSet);
begin
  TbBloArre.Cancel;
end;

end.
