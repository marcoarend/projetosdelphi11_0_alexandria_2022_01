unit BloGerenBaAVen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBGrid, Menus, dmkPermissoes,
  UnDmkEnums, UnDmkProcFunc;

type
  TFmBloGerenBaAVen = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAcao: TBitBtn;
    LaTitulo1C: TLabel;
    DBGBloArreIts: TdmkDBGrid;
    PMAcao: TPopupMenu;
    ReajustarutilizandoIGPM1: TMenuItem;
    dmkPermissoes1: TdmkPermissoes;
    Desativarativararrecadaoselecionada1: TMenuItem;
    Enviaremail1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    Localizararrecadao1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ReajustarutilizandoIGPM1Click(Sender: TObject);
    procedure Desativarativararrecadaoselecionada1Click(Sender: TObject);
    procedure Enviaremail1Click(Sender: TObject);
    procedure PMAcaoPopup(Sender: TObject);
    procedure DBGBloArreItsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtAcaoClick(Sender: TObject);
    procedure Localizararrecadao1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmBloGerenBaAVen: TFmBloGerenBaAVen;

implementation

uses
{$IfNDef NO_USE_EMAILDMK} UnMailEnv, {$EndIf}
UnMyObjects, Module, ModuleBloGeren, UnBloquetos;

{$R *.DFM}

procedure TFmBloGerenBaAVen.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmBloGerenBaAVen.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloGerenBaAVen.DBGBloArreItsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if Column.FieldName = 'EXPIRAEM' then
  begin
    if DBloGeren.QrBloArreItsVenEXPIROU .Value then
      Cor := clRed
    else
      Cor := clGreen;
    with DBGBloArreIts.Canvas do
      Font.Color := Cor;
  end;
end;

procedure TFmBloGerenBaAVen.Desativarativararrecadaoselecionada1Click(
  Sender: TObject);
var
  Controle, Ativo: Integer;
begin
  Controle := DBloGeren.QrBloArreItsVenControle.Value;
  Ativo    := DBloGeren.QrBloArreItsVenAtivo.Value;
  //
  if UBloquetos.AtivaDesativaArreIts(Controle, Ativo) then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      DBloGeren.ReopenBloArreItsVen;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmBloGerenBaAVen.Enviaremail1Click(Sender: TObject);
{$IfNDef NO_USE_EMAILDMK}
var
  SitCobr, PeriodoF, Entidade, EntiTipCto, PreEmail: Integer;
  Data, EntidadeNome, Para: String;
begin
  EntiTipCto   := DBloGeren.QrBloOpcoes.FieldByName('EntiTipCto').AsInteger;
  PreEmail     := DBloGeren.QrBloOpcoes.FieldByName('PreMailReaj').AsInteger;
  Entidade     := DBloGeren.QrBloArreItsVenEntidade.Value;
  Para         := UMailEnv.ObtemListaDeEntiMails(Entidade, [EntiTipCto]);
  EntidadeNome := DBloGeren.QrBloArreItsVenNOMEENT.Value;
  //
  SitCobr := DBloGeren.QrBloArreItsVenSitCobr.Value;
  //
  if SitCobr = 2 then //Programada
    PeriodoF := DBloGeren.QrBloArreItsVenParcPerF.Value
  else //Contrato
    PeriodoF := Geral.Periodo2000(DBloGeren.QrBloArreItsVenDtaPrxRenw.Value);
  //
  Data := Geral.FDT(Geral.PeriodoToDate(PeriodoF + 1, 1, False), 7);
  //
  UMailEnv.Monta_e_Envia_Mail(['0', '', Para, Geral.FF0(PreEmail)], meAvul,
    [], [EntidadeNome, Data], True);

{$Else}
begin
  dmkPF.InfoSemModulo(mdlappEmail);
{$EndIf}
end;

procedure TFmBloGerenBaAVen.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBloGerenBaAVen.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DBGBloArreIts.DataSource := DBloGeren.DsBloArreItsVen;
end;

procedure TFmBloGerenBaAVen.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloGerenBaAVen.Localizararrecadao1Click(Sender: TObject);
begin
  UBloquetos.MostraBloArre(DBloGeren.QrBloArreItsVenCodigo.Value,
    DBloGeren.QrBloArreItsVenControle.Value);
  try
    Screen.Cursor := crHourGlass;
    //
    DBloGeren.ReopenBloArreItsVen;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBloGerenBaAVen.PMAcaoPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (DBloGeren.QrBloArreItsVen.State <> dsInactive) and
            (DBloGeren.QrBloArreItsVen.RecordCount > 0);
  //
  ReajustarutilizandoIGPM1.Enabled             := Enab;
  Desativarativararrecadaoselecionada1.Enabled := Enab;
  //Enviaremail1.Enabled                         := Enab;
  Localizararrecadao1.Enabled                  := Enab;
  //
  if Desativarativararrecadaoselecionada1.Enabled then
  begin
    if DBloGeren.QrBloArreItsVenAtivo.Value = 1 then
      Desativarativararrecadaoselecionada1.Caption := 'Desativar arrecadação &selecionada'
    else
      Desativarativararrecadaoselecionada1.Caption := 'Ativar arrecadação &selecionada';
  end;
  if ReajustarutilizandoIGPM1.Enabled then
  begin
    ReajustarutilizandoIGPM1.Enabled := (DBloGeren.QrBloArreItsVenValor_TXT.Value <> 0) and
      (DBloGeren.QrBloArreItsVenSitCobr.Value in [2, 3]); //Programada ou Contrato
  end;    
end;

procedure TFmBloGerenBaAVen.ReajustarutilizandoIGPM1Click(Sender: TObject);
var
  IDNum, SitCobr, PeriodoI, PeriodoF: Integer;
  Valor: Double;
begin
  Valor   := DBloGeren.QrBloArreItsVenValor_TXT.Value;
  SitCobr := DBloGeren.QrBloArreItsVenSitCobr.Value;
  //
  if SitCobr = 2 then //Programada
  begin
    IDNum    := DBloGeren.QrBloArreItsVenControle.Value;
    PeriodoI := DBloGeren.QrBloArreItsVenParcPerI.Value;
    PeriodoF := DBloGeren.QrBloArreItsVenParcPerF.Value; 
  end else
  begin  //Contrato
    IDNum    := DBloGeren.QrBloArreItsVenContrato.Value;
    PeriodoI := Geral.Periodo2000(DBloGeren.QrBloArreItsVenDVencimento.Value);
    PeriodoF := Geral.Periodo2000(DBloGeren.QrBloArreItsVenDtaPrxRenw.Value);
  end;
  //
  UBloquetos.ReajustarArrecadacaoIGPM(PeriodoI, PeriodoF, SitCobr, IDNum, Valor);
  //
  try
    Screen.Cursor := crHourGlass;
    //
    DBloGeren.ReopenBloArreItsVen;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
