object FmBloArreIts: TFmBloArreIts
  Left = 339
  Top = 185
  Caption = 'BLO-ARREC-002 :: Item de Arrecada'#231#227'o Base'
  ClientHeight = 779
  ClientWidth = 801
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 801
    Height = 587
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label12: TLabel
      Left = 14
      Top = 5
      Width = 16
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID:'
    end
    object Label1: TLabel
      Left = 14
      Top = 110
      Width = 57
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Entidade:'
    end
    object SpeedButton7: TSpeedButton
      Left = 590
      Top = 129
      Width = 29
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton7Click
    end
    object Label2: TLabel
      Left = 14
      Top = 282
      Width = 430
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 
        'Texto a ser usado no lugar do texto base (deixe vazio para usar ' +
        'o base):'
    end
    object Label19: TLabel
      Left = 514
      Top = 282
      Width = 104
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Arredondamento:'
    end
    object Label4: TLabel
      Left = 106
      Top = 5
      Width = 58
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Empresa:'
    end
    object LaContrato: TLabel
      Left = 14
      Top = 166
      Width = 524
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 
        'Contrato (Antes de selecionar o contrato '#233' necess'#225'rio selecionar' +
        ' a empresa e o cliente):'
    end
    object SBContrato: TSpeedButton
      Left = 590
      Top = 187
      Width = 29
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SBContratoClick
    end
    object Label3: TLabel
      Left = 14
      Top = 55
      Width = 83
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Configura'#231#227'o:'
    end
    object SpeedButton1: TSpeedButton
      Left = 590
      Top = 75
      Width = 29
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object LaNFSeSrvCad: TLabel
      Left = 14
      Top = 225
      Width = 80
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Regra Fiscal:'
    end
    object SBNFSeSrvCad: TSpeedButton
      Left = 590
      Top = 246
      Width = 29
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SBNFSeSrvCadClick
    end
    object EdCodigo: TdmkEdit
      Left = 14
      Top = 25
      Width = 87
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Color = clInactiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGSitCobr: TRadioGroup
      Left = 626
      Top = 121
      Width = 160
      Height = 104
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Forma de cobran'#231'a: '
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o cobrar'
        'Cont'#237'nua (Anual)'
        'Programada')
      TabOrder = 17
      OnClick = RGSitCobrClick
    end
    object GBPer: TGroupBox
      Left = 14
      Top = 480
      Width = 605
      Height = 110
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 
        ' Per'#237'odo de cobran'#231'a programada (m'#234's e ano) ou cont'#237'nua (s'#243' m'#234's)' +
        ': '
      TabOrder = 18
      Visible = False
      object GBIni: TGroupBox
        Left = 6
        Top = 21
        Width = 296
        Height = 79
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Per'#237'odo Inicial: '
        TabOrder = 0
        object Label32: TLabel
          Left = 5
          Top = 18
          Width = 29
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'M'#234's:'
        end
        object LaAnoI: TLabel
          Left = 210
          Top = 18
          Width = 27
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ano:'
        end
        object CBMesI: TComboBox
          Left = 6
          Top = 39
          Width = 200
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          AutoDropDown = True
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnChange = CBMesIChange
        end
        object CBAnoI: TComboBox
          Left = 210
          Top = 39
          Width = 79
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          AutoDropDown = True
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnChange = CBAnoIChange
        end
      end
      object GBFim: TGroupBox
        Left = 305
        Top = 21
        Width = 296
        Height = 79
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Per'#237'odo final: '
        TabOrder = 1
        object Label34: TLabel
          Left = 6
          Top = 18
          Width = 29
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'M'#234's:'
        end
        object LaAnoF: TLabel
          Left = 210
          Top = 18
          Width = 27
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ano:'
        end
        object CBMesF: TComboBox
          Left = 6
          Top = 39
          Width = 200
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          AutoDropDown = True
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnChange = CBMesFChange
        end
        object CBAnoF: TComboBox
          Left = 210
          Top = 39
          Width = 79
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          AutoDropDown = True
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnChange = CBAnoFChange
        end
      end
    end
    object GbPro: TGroupBox
      Left = 626
      Top = 238
      Width = 160
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Cobran'#231'a programada: '
      TabOrder = 19
      Visible = False
      object Label7: TLabel
        Left = 20
        Top = 34
        Width = 57
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Parcelas:'
      end
      object EdParcelas: TdmkEdit
        Left = 81
        Top = 31
        Width = 56
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object EdEntidade: TdmkEditCB
      Left = 14
      Top = 129
      Width = 68
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEntidadeChange
      DBLookupComboBox = CBEntidade
      IgnoraDBLookupComboBox = False
    end
    object CBEntidade: TdmkDBLookupComboBox
      Left = 82
      Top = 129
      Width = 505
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'NOMEENT'
      ListSource = DsEntidades
      TabOrder = 6
      dmkEditCB = EdEntidade
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object GBValor: TGroupBox
      Left = 626
      Top = 5
      Width = 160
      Height = 106
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Valor para c'#225'lculo: '
      TabOrder = 16
      object Label17: TLabel
        Left = 22
        Top = 25
        Width = 69
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Valor base:'
      end
      object CkValor: TCheckBox
        Left = 22
        Top = 74
        Width = 116
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Solicitar o valor.'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
      object EdValor: TdmkEdit
        Left = 22
        Top = 44
        Width = 120
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object EdTexto: TdmkEdit
      Left = 14
      Top = 303
      Width = 493
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 11
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CkInfoParc: TCheckBox
      Left = 14
      Top = 336
      Width = 301
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Informar n'#250'mero da parcela / total de parcelas.'
      TabOrder = 13
    end
    object EdArredonda: TdmkEdit
      Left = 514
      Top = 303
      Width = 105
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 12
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,01'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.010000000000000000
      ValWarn = False
    end
    object RGDiaVencto: TRadioGroup
      Left = 14
      Top = 363
      Width = 605
      Height = 111
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Dia do m'#234's do vencimento: '
      Columns = 7
      ItemIndex = 4
      Items.Strings = (
        '01'
        '02'
        '03'
        '04'
        '05'
        '06'
        '07'
        '08'
        '09'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28'
        '29'
        '30'
        '31')
      TabOrder = 15
    end
    object CkAtivo: TCheckBox
      Left = 558
      Top = 336
      Width = 61
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      BiDiMode = bdLeftToRight
      Caption = 'Ativo'
      Checked = True
      ParentBiDiMode = False
      State = cbChecked
      TabOrder = 14
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 175
      Top = 25
      Width = 444
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 2
      dmkEditCB = EdEmpresa
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdEmpresa: TdmkEditCB
      Left = 106
      Top = 25
      Width = 69
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEmpresaChange
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
    end
    object EdContrato: TdmkEditCB
      Left = 14
      Top = 187
      Width = 68
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdContratoChange
      DBLookupComboBox = CBContrato
      IgnoraDBLookupComboBox = False
    end
    object CBContrato: TdmkDBLookupComboBox
      Left = 82
      Top = 187
      Width = 505
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContratos
      TabOrder = 8
      dmkEditCB = EdContrato
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object CBCNABConf: TdmkDBLookupComboBox
      Left = 82
      Top = 75
      Width = 505
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCnab_Cfg
      TabOrder = 4
      dmkEditCB = EdCNABConf
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCNABConf: TdmkEditCB
      Left = 14
      Top = 75
      Width = 68
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCNABConf
      IgnoraDBLookupComboBox = False
    end
    object EdNFSeSrvCad: TdmkEditCB
      Left = 14
      Top = 246
      Width = 68
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdContratoChange
      DBLookupComboBox = CBNFSeSrvCad
      IgnoraDBLookupComboBox = False
    end
    object CBNFSeSrvCad: TdmkDBLookupComboBox
      Left = 82
      Top = 246
      Width = 505
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsNFSeSrvCad
      TabOrder = 10
      dmkEditCB = EdNFSeSrvCad
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 801
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 742
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 683
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 376
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Arrecada'#231#227'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 376
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Arrecada'#231#227'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 376
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Arrecada'#231#227'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 646
    Width = 801
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 797
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 700
    Width = 801
    Height = 79
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 18
      Width = 797
      Height = 58
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 619
        Top = 0
        Width = 177
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 2
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 23
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT'
      'FROM entidades ent'
      'WHERE Codigo > 0'
      'ORDER BY NOMEENT')
    Left = 333
    Top = 376
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 361
    Top = 376
  end
  object QrCnab_Cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM  cnab_cfg'
      'WHERE Codigo > 0'
      'AND Ativo = 1'
      'ORDER BY Nome')
    Left = 389
    Top = 376
    object QrCnab_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCnab_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCnab_Cfg: TDataSource
    DataSet = QrCnab_Cfg
    Left = 417
    Top = 376
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 445
    Top = 376
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 474
    Top = 376
  end
  object QrContratos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Contratada, Contratante, '
      'DVencimento, DtaPrxRenw, ValorMes, ddMesVcto'
      'FROM contratos'
      'WHERE DtaCntrFim < 2'
      'AND Ativo = 1'
      'AND Contratada=:P0'
      'AND Contratante=:P1')
    Left = 502
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contratos.Codigo'
    end
    object QrContratosContratada: TIntegerField
      FieldName = 'Contratada'
      Origin = 'contratos.Contratada'
    end
    object QrContratosNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contratos.Nome'
      Size = 255
    end
    object QrContratosDVencimento: TDateField
      FieldName = 'DVencimento'
      Origin = 'contratos.DVencimento'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosValorMes: TFloatField
      FieldName = 'ValorMes'
      Origin = 'contratos.ValorMes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrContratosddMesVcto: TSmallintField
      FieldName = 'ddMesVcto'
      Origin = 'contratos.ddMesVcto'
    end
    object QrContratosContratante: TIntegerField
      FieldName = 'Contratante'
      Origin = 'contratos.Contratante'
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 530
    Top = 376
  end
  object QrNFSeSrvCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM nfsesrvcad'
      'ORDER BY Nome')
    Left = 558
    Top = 376
    object QrNFSeSrvCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeSrvCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsNFSeSrvCad: TDataSource
    DataSet = QrNFSeSrvCad
    Left = 586
    Top = 376
  end
end
