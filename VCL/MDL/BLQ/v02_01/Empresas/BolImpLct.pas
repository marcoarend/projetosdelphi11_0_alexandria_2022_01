unit BolImpLct;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, frxClass, frxDBSet, dmkDBGrid, UnDmkEnums;

type
  TFmBolImpLct = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    frxBloqE2: TfrxReport;
    frxDsCNAB_Cfg: TfrxDBDataset;
    frxDsBoletos: TfrxDBDataset;
    QrLctBol: TmySQLQuery;
    DsLctBol: TDataSource;
    DBGrid1: TdmkDBGrid;
    QrLctBolData: TDateField;
    QrLctBolTipo: TSmallintField;
    QrLctBolCarteira: TIntegerField;
    QrLctBolControle: TLargeintField;
    QrLctBolSub: TSmallintField;
    QrLctBolCliInt: TIntegerField;
    QrLctBolCliente: TIntegerField;
    QrLctBolEmitente: TWideStringField;
    QrLctBolCNPJCPF: TWideStringField;
    QrLctBolDuplicata: TWideStringField;
    QrLctBolSerieCH: TWideStringField;
    QrLctBolDocumento: TFloatField;
    QrLctBolCredito: TFloatField;
    QrLctBolVencimento: TDateField;
    QrLctBolAtivo: TSmallintField;
    QrLctBolNOMECLI: TWideStringField;
    QrLctBolSACADO_CNPJ: TWideStringField;
    QrLctBolSACADO_NOME: TWideStringField;
    QrLctBolSACADO_RUA: TWideStringField;
    QrLctBolSACADO_NUMERO: TLargeintField;
    QrLctBolSACADO_COMPL: TWideStringField;
    QrLctBolSACADO_BAIRRO: TWideStringField;
    QrLctBolSACADO_CIDADE: TWideStringField;
    QrLctBolSACADO_xUF: TWideStringField;
    QrLctBolSACADO_CEP: TIntegerField;
    QrLctBolSACADO_TIPO: TLargeintField;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrBoletos: TmySQLQuery;
    QrBoletosData: TDateField;
    QrBoletosTipo: TSmallintField;
    QrBoletosCarteira: TIntegerField;
    QrBoletosControle: TLargeintField;
    QrBoletosSub: TSmallintField;
    QrBoletosCliInt: TIntegerField;
    QrBoletosCliente: TIntegerField;
    QrBoletosEmitente: TWideStringField;
    QrBoletosCNPJCPF: TWideStringField;
    QrBoletosDuplicata: TWideStringField;
    QrBoletosSerieCH: TWideStringField;
    QrBoletosDocumento: TFloatField;
    QrBoletosCredito: TFloatField;
    QrBoletosVencimento: TDateField;
    QrBoletosAtivo: TSmallintField;
    QrBoletosSACADO_CNPJ: TWideStringField;
    QrBoletosSACADO_NOME: TWideStringField;
    QrBoletosSACADO_RUA: TWideStringField;
    QrBoletosSACADO_NUMERO: TLargeintField;
    QrBoletosSACADO_COMPL: TWideStringField;
    QrBoletosSACADO_BAIRRO: TWideStringField;
    QrBoletosSACADO_CIDADE: TWideStringField;
    QrBoletosSACADO_xUF: TWideStringField;
    QrBoletosSACADO_CEP: TIntegerField;
    QrBoletosSACADO_TIPO: TLargeintField;
    QrBoletosNOMECLI: TWideStringField;
    QrBoletosSACADO_E_ALL: TWideStringField;
    QrBoletosSACADO_NUMERO_TXT: TWideStringField;
    QrBoletosSACADO_CEP_TXT: TWideStringField;
    QrLctBolFatID: TIntegerField;
    QrLctBolFatParcela: TIntegerField;
    QrLctBolFatNum: TFloatField;
    QrBoletosFatID: TIntegerField;
    QrBoletosFatNum: TFloatField;
    QrBoletosFatParcela: TIntegerField;
    QrCNAB_Cfg: TmySQLQuery;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    QrCNAB_CfgCedente: TIntegerField;
    QrCNAB_CfgSacadAvali: TIntegerField;
    QrCNAB_CfgCedBanco: TIntegerField;
    QrCNAB_CfgCedAgencia: TIntegerField;
    QrCNAB_CfgCedConta: TWideStringField;
    QrCNAB_CfgCedDAC_A: TWideStringField;
    QrCNAB_CfgCedDAC_C: TWideStringField;
    QrCNAB_CfgCedDAC_AC: TWideStringField;
    QrCNAB_CfgCedNome: TWideStringField;
    QrCNAB_CfgCedPosto: TIntegerField;
    QrCNAB_CfgSacAvaNome: TWideStringField;
    QrCNAB_CfgTipoCart: TSmallintField;
    QrCNAB_CfgCartNum: TWideStringField;
    QrCNAB_CfgCartCod: TWideStringField;
    QrCNAB_CfgEspecieTit: TWideStringField;
    QrCNAB_CfgEspecieDoc: TWideStringField;
    QrCNAB_CfgAceiteTit: TSmallintField;
    QrCNAB_CfgInstrCobr1: TWideStringField;
    QrCNAB_CfgInstrCobr2: TWideStringField;
    QrCNAB_CfgInstrDias: TSmallintField;
    QrCNAB_CfgCodEmprBco: TWideStringField;
    QrCNAB_CfgJurosTipo: TSmallintField;
    QrCNAB_CfgJurosPerc: TFloatField;
    QrCNAB_CfgJurosDias: TSmallintField;
    QrCNAB_CfgMultaTipo: TSmallintField;
    QrCNAB_CfgMultaPerc: TFloatField;
    QrCNAB_CfgMultaDias: TSmallintField;
    QrCNAB_CfgTexto01: TWideStringField;
    QrCNAB_CfgTexto02: TWideStringField;
    QrCNAB_CfgTexto03: TWideStringField;
    QrCNAB_CfgTexto04: TWideStringField;
    QrCNAB_CfgTexto05: TWideStringField;
    QrCNAB_CfgTexto06: TWideStringField;
    QrCNAB_CfgTexto07: TWideStringField;
    QrCNAB_CfgTexto08: TWideStringField;
    QrCNAB_CfgTexto09: TWideStringField;
    QrCNAB_CfgTexto10: TWideStringField;
    QrCNAB_CfgCNAB: TIntegerField;
    QrCNAB_CfgEnvEmeio: TSmallintField;
    QrCNAB_CfgDiretorio: TWideStringField;
    QrCNAB_CfgQuemPrint: TWideStringField;
    QrCNAB_Cfg_237Mens1: TWideStringField;
    QrCNAB_Cfg_237Mens2: TWideStringField;
    QrCNAB_CfgCodOculto: TWideStringField;
    QrCNAB_CfgSeqArq: TIntegerField;
    QrCNAB_CfgLastNosNum: TLargeintField;
    QrCNAB_CfgLocalPag: TWideStringField;
    QrCNAB_CfgEspecieVal: TWideStringField;
    QrCNAB_CfgOperCodi: TWideStringField;
    QrCNAB_CfgIDCobranca: TWideStringField;
    QrCNAB_CfgAgContaCed: TWideStringField;
    QrCNAB_CfgDirRetorno: TWideStringField;
    QrCNAB_CfgCartRetorno: TIntegerField;
    QrCNAB_CfgPosicoesBB: TSmallintField;
    QrCNAB_CfgIndicatBB: TWideStringField;
    QrCNAB_CfgTipoCobrBB: TWideStringField;
    QrCNAB_CfgVariacao: TIntegerField;
    QrCNAB_CfgComando: TIntegerField;
    QrCNAB_CfgDdProtesBB: TIntegerField;
    QrCNAB_CfgMsg40posBB: TWideStringField;
    QrCNAB_CfgQuemDistrb: TWideStringField;
    QrCNAB_CfgDesco1Cod: TSmallintField;
    QrCNAB_CfgDesco1Dds: TIntegerField;
    QrCNAB_CfgDesco1Fat: TFloatField;
    QrCNAB_CfgDesco2Cod: TSmallintField;
    QrCNAB_CfgDesco2Dds: TIntegerField;
    QrCNAB_CfgDesco2Fat: TFloatField;
    QrCNAB_CfgDesco3Cod: TSmallintField;
    QrCNAB_CfgDesco3Dds: TIntegerField;
    QrCNAB_CfgDesco3Fat: TFloatField;
    QrCNAB_CfgProtesCod: TSmallintField;
    QrCNAB_CfgProtesDds: TSmallintField;
    QrCNAB_CfgBxaDevCod: TSmallintField;
    QrCNAB_CfgBxaDevDds: TIntegerField;
    QrCNAB_CfgMoedaCod: TWideStringField;
    QrCNAB_CfgContrato: TFloatField;
    QrCNAB_CfgConvCartCobr: TWideStringField;
    QrCNAB_CfgConvVariCart: TWideStringField;
    QrCNAB_CfgInfNossoNum: TSmallintField;
    QrCNAB_CfgCodLidrBco: TWideStringField;
    QrCNAB_CfgLk: TIntegerField;
    QrCNAB_CfgDataCad: TDateField;
    QrCNAB_CfgDataAlt: TDateField;
    QrCNAB_CfgUserCad: TIntegerField;
    QrCNAB_CfgUserAlt: TIntegerField;
    QrCNAB_CfgAlterWeb: TSmallintField;
    QrCNAB_CfgAtivo: TSmallintField;
    QrCNAB_CfgCartEmiss: TIntegerField;
    QrCNAB_CfgDVB: TWideStringField;
    QrCNAB_CfgNOMEBANCO: TWideStringField;
    QrCNAB_CfgNOMECED_IMP: TWideStringField;
    QrCNAB_CfgACEITETIT_TXT: TWideStringField;
    QrCNAB_CfgTipoCobranca: TIntegerField;
    QrCNAB_CfgEspecieTxt: TWideStringField;
    QrCNAB_CfgCartTxt: TWideStringField;
    QrCNAB_CfgLayoutRem: TWideStringField;
    QrCNAB_CfgLayoutRet: TWideStringField;
    QrCNAB_CfgConcatCod: TWideStringField;
    QrCNAB_CfgComplmCod: TWideStringField;
    QrCNAB_CfgNaoRecebDd: TSmallintField;
    QrCNAB_CfgTipBloqUso: TWideStringField;
    QrCNAB_CfgNumVersaoI3: TIntegerField;
    QrCNAB_CfgModalCobr: TSmallintField;
    QrCNAB_CfgCtaCooper: TWideStringField;
    QrBoletosBOLETO: TFloatField;
    QrBoletosVENCTO: TDateField;
    QrBoletosVALOR: TFloatField;
    QrCNAB_CfgTermoAceite: TIntegerField;
    QrCNAB_CfgCorresBco: TIntegerField;
    QrCNAB_CfgCorresAge: TIntegerField;
    QrCNAB_CfgCorresCto: TWideStringField;
    QrCNAB_CfgCART_IMP: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BtOKClick(Sender: TObject);
    procedure QrBoletosCalcFields(DataSet: TDataSet);
    procedure frxBloqE2GetValue(const VarName: string; var Value: Variant);
    procedure QrCNAB_CfgCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FNossoNumero: String;
    FLctBoleto: String;
    procedure AtivarTodos(Ativo: Integer);
  public
    { Public declarations }
    FCNAB_Cfg: Integer;
    procedure ReopenLctBol(Qry: TmySQLQuery; Controle: Int64);
    procedure ReopenCNAB_Cfg(CNAB_Cfg: Integer);
  end;

  var
  FmBolImpLct: TFmBolImpLct;

implementation

uses UnMyObjects, Module, ModuleGeral, UCreateFin, DmkDAC_PF, ModuleBco,
  UnBancos, MyGlyfs, UnBco_Rem;

{$R *.DFM}

procedure TFmBolImpLct.AtivarTodos(Ativo: Integer);
var
  Controle: Int64;
begin
  Screen.Cursor := crHourGlass;
  try
    Controle := QrLctBolControle.Value;
    UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DModG.MyPID_DB, [
    'UPDATE ' + FLctBoleto + ' SET ',
    'Ativo=' + Geral.FF0(Ativo),
    '']);
    //
    ReopenLctBol(QrLctBol, Controle);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBolImpLct.BtNenhumClick(Sender: TObject);
begin
  AtivarTodos(0);
end;

procedure TFmBolImpLct.BtOKClick(Sender: TObject);
var
  Def_Client, CedenteTipo, SacadorTipo: Integer;
  SQL: String;
  CedenteNome, CedenteCNPJ, SacadorNome, SacadorCNPJ: String;
begin
  Def_Client := QrLctBolCliInt.Value;  // ver!
  SQL := 'SELECT 0 Itens';
  //
  if not DmBco.ReopenCNAB_Cfg(FCNAB_Cfg,
    Def_Client, SQL,
    CedenteTipo, CedenteNome, CedenteCNPJ,
    SacadorTipo, SacadorNome, SacadorCNPJ) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  UBco_Rem.QuemImprimeBloqueto_Verifica(QrCNAB_CfgCedBanco.Value,
    QrCNAB_CfgCNAB.Value, QrCNAB_CfgQuemPrint.Value);
  //
  ReopenLctBol(QrBoletos, 0);
  frxDsBoletos.DataSet := QrBoletos;
  frxDsCNAB_Cfg.DataSet := QrCNAB_Cfg;
  MyObjects.frxDefineDataSets(frxBloqE2, [
    frxDsCNAB_Cfg,
    frxDsBoletos
  ]);
  MyObjects.frxMostra(frxBloqE2, 'Bloquetos duplicatas');
end;

procedure TFmBolImpLct.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBolImpLct.BtTodosClick(Sender: TObject);
begin
  AtivarTodos(1);
end;

procedure TFmBolImpLct.DBGrid1CellClick(Column: TColumn);
var
  Controle: Int64;
  Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Screen.Cursor := crHourGlass;
    try
      Controle := QrLctBolControle.Value;
      if QrLctBolAtivo.Value = 0 then
        Ativo := 1
      else
        Ativo := 0;
      UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DModG.MyPID_DB, [
      'UPDATE ' + FLctBoleto + ' SET ',
      'Ativo=' + Geral.FF0(Ativo),
      'WHERE Controle=' + Geral.FI64(Controle),
      '']);
      //
      ReopenLctBol(QrLctBol, Controle);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmBolImpLct.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBolImpLct.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FLctBoleto := UCriarFin.RecriaTempTableNovo(ntrtt_LctBoleto, DmodG.QrUpdPID1, False);
end;

procedure TFmBolImpLct.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBolImpLct.frxBloqE2GetValue(const VarName: string;
  var Value: Variant);
const
  MostraValorEVencto = True;
var
  DVB: String;
  V, M, J, C, P: Double;
  GeraNossoGeral, CodigoDeCobranca, LinhaDigital, NossoNumero_Rem: String;
  Status: Boolean;
begin
  P      := 0;
  C      := 0;
  V      := QrBoletosCredito.Value;
  M      := QrCNAB_CfgMultaPerc.Value;
  J      := QrCNAB_CfgJurosPerc.Value;
  Status := QrCNAB_CfgTermoAceite.Value = 0;
  //
  UBancos.GeraNossoNumero(QrCNAB_CfgTipoCart.Value, QrCNAB_CfgCedBanco.Value,
    QrCNAB_CfgCedAgencia.Value, QrCNAB_CfgCedPosto.Value, QrBoletosBOLETO.Value,
    QrCNAB_CfgCedConta.Value, QrCNAB_CfgCartNum.Value, QrCNAB_CfgIDCobranca.Value,
    Geral.SoNumero_TT(QrCNAB_CfgCodEmprBco.Value), QrBoletosVencimento.Value,
    QrCNAB_CfgTipoCobranca.Value, QrCNAB_CfgEspecieDoc.Value, QrCNAB_CfgCNAB.Value,
    QrCNAB_CfgCtaCooper.Value, QrCNAB_CfgLayoutRem.Value, FNossoNumero, NossoNumero_Rem);
  GeraNossoGeral := FNossoNumero;
  CodigoDeCobranca := UBancos.CodigoDeBarra_BoletoDeCobranca(
    QrCNAB_CfgCedBanco.Value, QrCNAB_CfgCedAgencia.Value, QrCNAB_CfgCorresBco.Value,
    QrCNAB_CfgCorresAge.Value, QrCNAB_CfgCedDAC_A.Value, QrCNAB_CfgCedPosto.Value,
    QrCNAB_CfgCedConta.Value, QrCNAB_CfgCedDAC_C.Value, QrCNAB_CfgCorresCto.Value,
    9, 3, 1, FNossoNumero, QrCNAB_CfgCodEmprBco.Value, QrCNAB_CfgCartNum.Value,
    QrCNAB_CfgCART_IMP.Value, QrCNAB_CfgIDCobranca.Value, QrCNAB_CfgOperCodi.Value,
    QrBoletosVencimento.Value, QrBoletosCredito.Value, 0, 0,
    MostraValorEVencto, QrCNAB_CfgTipoCart.Value, QrCNAB_CfgLayoutRem.Value);
  LinhaDigital := UBancos.LinhaDigitavel_BoletoDeCobranca(
    UBancos.CodigoDeBarra_BoletoDeCobranca(QrCNAB_CfgCedBanco.Value,
    QrCNAB_CfgCedAgencia.Value, QrCNAB_CfgCorresBco.Value,
    QrCNAB_CfgCorresAge.Value, QrCNAB_CfgCedDAC_A.Value, QrCNAB_CfgCedPosto.Value,
    QrCNAB_CfgCedConta.Value, QrCNAB_CfgCedDAC_C.Value, QrCNAB_CfgCorresCto.Value,
    9, 3, 1, FNossoNumero, QrCNAB_CfgCodEmprBco.Value, QrCNAB_CfgCartNum.Value,
    QrCNAB_CfgCART_IMP.Value, QrCNAB_CfgIDCobranca.Value, QrCNAB_CfgOperCodi.Value,
    QrBoletosVencimento.Value, QrBoletosCredito.Value, 0, 0,
    MostraValorEVencto, QrCNAB_CfgTipoCart.Value, QrCNAB_CfgLayoutRem.Value));
  //
  if AnsiCompareText(VarName, 'VARF_NossoNumero') = 0 then
  begin
    Value := GeraNossoGeral;
  end else
  if AnsiCompareText(VarName, 'VAX') = 0 then
  begin
    if QrCNAB_CfgDVB.Value <> '?' then DVB := QrCNAB_CfgDVB.Value else
    DVB := UBancos.DigitoVerificadorCodigoBanco(QrCNAB_CfgCedBanco.Value);
    Value := FormatFloat('000', QrCNAB_CfgCedBanco.Value) + '-' + DVB;
  end else
  if AnsiCompareText(VarName, 'VARF_CODIGOBARRAS') = 0 then
  begin
    Value := CodigoDeCobranca;
  end else
  if AnsiCompareText(VarName, 'VARF_LINHADIGITAVEL') = 0 then
  begin
    Value := LinhaDigital;
  end else
  (*
  if AnsiCompareText(VarName, 'VARF_MESANOA') = 0 then
    Value := MLAGeral.MesEAnoDoPeriodo(FmBloGeren.QrPrevPeriodo.Value + 1)
  else
  *)
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_01') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto01.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_02') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto02.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_03') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto03.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_04') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto04.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_05') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto05.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_06') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto06.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_07') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto07.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_08') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto08.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_09') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto09.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_10') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNAB_CfgTexto10.Value, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'LogoBancoExiste') = 0 then
    Value := FmMyGlyfs.LogoBancoExiste(QrCNAB_CfgCedBanco.Value)
  else
  if AnsiCompareText(VarName, 'LogoBancoPath') = 0 then
    Value := FmMyGlyfs.CaminhoLogoBanco(QrCNAB_CfgCedBanco.Value)
  else
  if AnsiCompareText(VarName, 'VARF_STATUS') = 0 then
    Value := Status;
end;

procedure TFmBolImpLct.QrBoletosCalcFields(DataSet: TDataSet);
begin
  // Compatibilidade com outros frxBloqE2 de outros Forms / DataModules
  QrBoletosBOLETO.Value := QrBoletosFatParcela.Value;
  QrBoletosVENCTO.Value := QrBoletosVencimento.Value;
  QrBoletosVALOR.Value := QrBoletosCredito.Value;
  // FIM Compatibilidade com outros frxBloqE2 de outros Forms / DataModules
  //
  QrBoletosSACADO_NUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrBoletosSACADO_Rua.Value, QrBoletosSACADO_Numero.Value, False);
  QrBoletosSACADO_E_ALL.Value := QrBoletosSACADO_RUA.Value;
  if Trim(QrBoletosSACADO_Rua.Value) <> '' then QrBoletosSACADO_E_ALL.Value :=
    QrBoletosSACADO_E_ALL.Value + ', ' + QrBoletosSACADO_NUMERO_TXT.Value;
  if Trim(QrBoletosSACADO_Compl.Value) <>  '' then QrBoletosSACADO_E_ALL.Value :=
    QrBoletosSACADO_E_ALL.Value + ' ' + QrBoletosSACADO_Compl.Value;
  if Trim(QrBoletosSACADO_Bairro.Value) <>  '' then QrBoletosSACADO_E_ALL.Value :=
    QrBoletosSACADO_E_ALL.Value + ' - BAIRRO ' + QrBoletosSACADO_Bairro.Value;
  if QrBoletosSACADO_CEP.Value > 0 then QrBoletosSACADO_E_ALL.Value :=
    QrBoletosSACADO_E_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrBoletosSACADO_CEP.Value);
  if Trim(QrBoletosSACADO_Cidade.Value) <>  '' then QrBoletosSACADO_E_ALL.Value :=
    QrBoletosSACADO_E_ALL.Value + '   ' + QrBoletosSACADO_Cidade.Value;
  if Trim(QrBoletosSACADO_xUF.Value) <>  '' then QrBoletosSACADO_E_ALL.Value :=
    QrBoletosSACADO_E_ALL.Value + ' / ' + QrBoletosSACADO_xUF.Value;
  //
  QrBoletosSACADO_CEP_TXT.Value :=Geral.FormataCEP_NT(QrBoletosSACADO_CEP.Value);
  //
end;

procedure TFmBolImpLct.QrCNAB_CfgCalcFields(DataSet: TDataSet);
begin
  if QrCNAB_CfgAceiteTit.Value = 1 then
    QrCNAB_CfgACEITETIT_TXT.Value := 'S'
  else
    QrCNAB_CfgACEITETIT_TXT.Value := 'N';
end;

procedure TFmBolImpLct.ReopenCNAB_Cfg(CNAB_Cfg: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCNAB_Cfg, Dmod.MyDB, [
  'SELECT bco.Nome NOMEBANCO, bco.DVB,  cfg.*, ',
  'IF(cfg.Cedente=0,"",IF(ced.Tipo=0, ',
  'ced.RazaoSocial,ced.Nome)) NOMECED_IMP, ',
  'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CART_IMP ',
  'FROM cnab_cfg cfg ',
  'LEFT JOIN bancos bco ON bco.Codigo=cfg.CedBanco ',
  'LEFT JOIN entidades ced ON ced.Codigo=cfg.Cedente ',
  'WHERE cfg.Codigo=' + Geral.FF0(CNAB_Cfg),
  '']);
end;

procedure TFmBolImpLct.ReopenLctBol(Qry: TmySQLQuery; Controle: Int64);
var
  Ativos: String;
begin
  if Qry = QrBoletos then
    Ativos := 'WHERE lct.Ativo=1'
  else
    Ativos := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
  'SELECT lct.*, sac.CNPJ SACADO_CNPJ, ',
  'sac.Nome SACADO_NOME, sac.Rua SACADO_RUA, ',
  'sac.Numero SACADO_NUMERO, sac.Compl SACADO_COMPL, ',
  'sac.Bairro SACADO_BAIRRO, ',
  'sac.Cidade SACADO_CIDADE, sac.UF SACADO_xUF, ',
  'sac.CEP SACADO_CEP, ',
  'IF(LENGTH(sac.CNPJ) >=14, 0, 1) SACADO_TIPO, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI ',
  'FROM ' + FLctBoleto + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.sacados sac ON sac.CNPJ=lct.CNPJCPF ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente ',
  Ativos,
  '']);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

end.
