object FmBloGeren: TFmBloGeren
  Left = 339
  Top = 185
  Caption = 'BLO-GEREN-001 :: Gerencia Boletos'
  ClientHeight = 940
  ClientWidth = 1334
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1334
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1275
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 265
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object BtOpcoes: TBitBtn
        Tag = 348
        Left = 4
        Top = 7
        Width = 49
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOpcoesClick
      end
      object BtBloArre: TBitBtn
        Tag = 335
        Left = 106
        Top = 7
        Width = 49
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtBloArreClick
      end
      object BtCNABCfg: TBitBtn
        Tag = 10200
        Left = 55
        Top = 7
        Width = 50
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtCNABCfgClick
      end
      object BtProPad: TBitBtn
        Tag = 10201
        Left = 158
        Top = 7
        Width = 49
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtProPadClick
      end
      object BtProtoGer: TBitBtn
        Tag = 10202
        Left = 209
        Top = 7
        Width = 49
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtProtoGerClick
      end
    end
    object GB_M: TGroupBox
      Left = 265
      Top = 0
      Width = 1010
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 245
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerencia Boletos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 245
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerencia Boletos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 245
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerencia Boletos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBCntrl: TGroupBox
    Left = 0
    Top = 862
    Width = 1334
    Height = 78
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 2
      Top = 18
      Width = 265
      Height = 58
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 207
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 158
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 59
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 10
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
      object BitBtn2: TBitBtn
        Tag = 310
        Left = 108
        Top = 5
        Width = 50
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BitBtn2Click
      end
    end
    object LaRegistro: TStaticText
      Left = 267
      Top = 18
      Width = 35
      Height = 20
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelInner = bvLowered
      BevelKind = bkFlat
      Caption = '[N]: 0'
      TabOrder = 2
    end
    object Panel7: TPanel
      Left = 379
      Top = 18
      Width = 953
      Height = 58
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Panel10: TPanel
        Left = 789
        Top = 0
        Width = 164
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 6
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object GBAvisos1: TGroupBox
        Left = 0
        Top = 0
        Width = 789
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 1
        object Panel18: TPanel
          Left = 2
          Top = 18
          Width = 784
          Height = 37
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 16
            Top = 2
            Width = 15
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 15
            Top = 1
            Width = 15
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object CkZerado: TCheckBox
            Left = 374
            Top = 6
            Width = 104
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Sem val/vcto.'
            TabOrder = 0
            Visible = False
          end
        end
      end
    end
  end
  object PageControl2: TPageControl
    Left = 0
    Top = 85
    Width = 1334
    Height = 777
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = TabSheet7
    Align = alClient
    TabHeight = 25
    TabOrder = 2
    OnChange = PageControl2Change
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Mensal'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1326
        Height = 742
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        ExplicitWidth = 1324
        ExplicitHeight = 734
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 1326
          Height = 52
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 1324
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 1324
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object DBText12: TDBText
              Left = 0
              Top = 0
              Width = 246
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              DataField = 'NOME_ENCERRADO'
              DataSource = DsPrev
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -20
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBText13: TDBText
              Left = 1078
              Top = 0
              Width = 246
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              Alignment = taRightJustify
              DataField = 'Codigo'
              DataSource = DsPrev
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -20
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBText1: TDBText
              Left = 246
              Top = 0
              Width = 832
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Alignment = taCenter
              DataField = 'PERIODO_TXT'
              DataSource = DsPrev
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clHighlight
              Font.Height = -20
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
          end
          object Panel5: TPanel
            Left = 0
            Top = 21
            Width = 1324
            Height = 31
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object Label18: TLabel
              Left = 0
              Top = 7
              Width = 114
              Height = 19
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Arrecada'#231#245'es:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -17
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBText7: TDBText
              Left = 123
              Top = 6
              Width = 100
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Valor'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clTeal
              Font.Height = -17
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label19: TLabel
              Left = 231
              Top = 7
              Width = 97
              Height = 19
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Pr'#233'-boletos:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -17
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBText8: TDBText
              Left = 354
              Top = 6
              Width = 100
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'TOT_PRE'
              DataSource = DsPrev
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clTeal
              Font.Height = -17
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label22: TLabel
              Left = 458
              Top = 7
              Width = 66
              Height = 19
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Boletos:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -17
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBText9: TDBText
              Left = 546
              Top = 6
              Width = 100
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'TOT_BOL'
              DataSource = DsPrev
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clTeal
              Font.Height = -17
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
          end
        end
        object PageControl1: TPageControl
          Left = 0
          Top = 52
          Width = 1326
          Height = 690
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet6
          Align = alClient
          MultiLine = True
          TabHeight = 25
          TabOrder = 1
          OnChange = PageControl1Change
          ExplicitWidth = 1324
          ExplicitHeight = 682
          object TabSheet3: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Or'#231'amento'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object ImgCondGer: TImage
              Left = 0
              Top = 57
              Width = 1314
              Height = 582
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Center = True
              Picture.Data = {
                07544269746D617036640000424D36640000000000003600000028000000AA00
                000032000000010018000000000000640000120B0000120B0000000000000000
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000096616196606000FF00955F5F955F5F00FF00955F5F955F5F00FF00955F5F
                955F5F00FF00955F5F95606000FF0096606096606000FF006B28286C29296E2B
                2B6F2D2D702E2E712F2F72313172313173323273323273323273323273323273
                3232733232733232733232733232733232733232733232733232733232733232
                7332327332327332327332327332327332327332327332327332327332327332
                3273323273323273323273323273323273323273323273323273323273323273
                3232733232733232733232733232733232733232733232733232733232733232
                7332327332327332327332327332327332327332327332327332327332327332
                3273323273323273323273323273323273323273323273323273323273323273
                3232733232733232733232733232733232733232733232733232733232733232
                7332327332327332327332327332327332327332327332327332327332327332
                3273323273323273323273323273323273323273323273323273323273323273
                3232733232733232733232733232733232733232733232733232733232733232
                7332327332327332327332327332327332327332327332327332327332327332
                3273323273323273323273323273323273323273323273323273323273323273
                3232733232733232723131723131712F2F702E2E6F2D2D6E2B2B6C29296B2828
                000097616196606000FF0095606095606000FF0095606095606000FF00956060
                95606000FF0095606096606000FF0097616198616100FF006D29296E2A2A6F2E
                2E71303072313175343474353575353575363676353576363676363676363676
                3636763636763636763636763636763636763636763636763636763636763636
                7636367636367636367636367636367636367636367636367636367636367636
                3676363676363676363676363676363676363676363676363676363676363676
                3636763636763636763636763636763636763636763636763636763636763636
                7636367636367636367636367636367636367636367636367636367636367636
                3676363676363676363676363676363676363676363676363676363676363676
                3636763636763636763636763636763636763636763636763636763636763636
                7636367636367636367636367636367636367636367636367636367636367636
                3676363676363676363676363676363676363676363676363676363676363676
                3636763636763636763636763636763636763636763636763636763636763636
                7636367636367636367636367636367636367636367636367636367636367636
                3676363676363676363676363676363676363676363676363676363676363676
                36367635357536367535357435357534347231317130306F2E2E6E2A2A6D2929
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF006E2B2B702D2D7230
                30743434763636783939793A3A7A3B3B7B3C3C7B3C3C7C3D3D7C3D3D7C3D3D7C
                3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D
                7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D
                3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C
                3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D
                7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D
                3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C
                3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D
                7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D
                3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C
                3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D
                7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D
                3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C3D3D7C
                3D3D7B3C3C7B3C3C7A3B3B793A3A783939763636743434723030702D2D6E2B2B
                000098626297616100FF0096616196606000FF0096606096606000FF00966060
                96606000FF0096606097616100FF009B63639D656500FF006F2E2E7130307433
                337736367939397C3C3C7D3F3F7F40407F424280434380424280424280424280
                4242804242804242804242804242804242804242804242804242804242804242
                8042428042428042428042428042428042428042428042428042428042428042
                4280424280424280424280424280424280424280424280424280424280424280
                4242804242804242804242804242804242804242804242804242804242804242
                8042428042428042428042428042428042428042428042428042428042428042
                4280424280424280424280424280424280424280424280424280424280424280
                4242804242804242804242804242804242804242804242804242804242804242
                8042428042428042428042428042428042428042428042428042428042428042
                4280424280424280424280424280424280424280424280424280424280424280
                4242804242804242804242804242804242804242804242804242804242804242
                8042428042428042428042428042428042428042428042428042428042428042
                4280424280424280424280424280424280424280424280424280424280424280
                42428043437F42427F40407D3F3F7C3C3C7939397736367433337130306F2E2E
                000098626297616100FF0097616196616100FF0096606096616100FF00966060
                96616100FF0096616197616100FF009B64649F666600FF007230307332327635
                357939397C3D3D7F404080434382454582464683464683474783474783474783
                4747834747834747834747834747834747834747834747834747834747834747
                8347478347478347478347478347478347478347478347478347478347478347
                4783474783474783474783474783474783474783474783474783474783474783
                4747834747834747834747834747834747834747834747834747834747834747
                8347478347478347478347478347478347478347478347478347478347478347
                4783474783474783474783474783474783474783474783474783474783474783
                4747834747834747834747834747834747834747834747834747834747834747
                8347478347478347478347478347478347478347478347478347478347478347
                4783474783474783474783474783474783474783474783474783474783474783
                4747834747834747834747834747834747834747834747834747834747834747
                8347478347478347478347478347478347478347478347478347478347478347
                4783474783474783474783474783474783474783474783474783474783474783
                47478346468246468245458043437F40407C3D3D793939763535733232723030
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00743333753535793A
                3A7D3D3D804141814444834646844747854949864A4A864B4B864B4B854B4B85
                4A4A854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B
                854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B
                4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B85
                4B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B
                854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B
                4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B85
                4B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B
                854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B
                4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B85
                4B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B
                854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854B
                4B854B4B854B4B854B4B854B4B854B4B854B4B854B4B854A4A854B4B864B4B86
                4B4B864A4A8549498447478346468144448041417D3D3D793A3A753535743333
                000098626298626200FF0097616197616100FF0097616197616100FF00976161
                97616100FF0097616198626200FF009D6565A1686800FF007636367838387C3E
                3E804343834747854A4A874C4C894D4D894E4E8A4F4F8A4F4F8A4F4F894F4F89
                4F4F894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E
                894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E
                4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E89
                4E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E
                894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E
                4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E89
                4E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E
                894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E
                4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E89
                4E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E
                894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894E
                4E894E4E894E4E894E4E894E4E894E4E894E4E894E4E894F4F894F4F8A4F4F8A
                4F4F8A4F4F894E4E894D4D874C4C854A4A8347478043437C3E3E783838763636
                000098626298616100FF0097616197616100FF0096616197616100FF00966161
                97616100FF0097616198616100FF009D6565A1686800FF007738387B3B3B7F42
                42834747874C4C8A4F4F8C52528E54548F55559056568F56568E56568F56568F
                56568F56568F56568F56568F56568F56568F56568F56568F56568F56568F5656
                8F56568F56568F56568F56568F56568F56568F56568F56568F56568F56568F56
                568F56568F56568F56568F56568F56568F56568F56568F56568F56568F56568F
                56568F56568F56568F56568F56568F56568F56568F56568F56568F56568F5656
                8F56568F56568F56568F56568F56568F56568F56568F56568F56568F56568F56
                568F56568F56568F56568F56568F56568F56568F56568F56568F56568F56568F
                56568F56568F56568F56568F56568F56568F56568F56568F56568F56568F5656
                8F56568F56568F56568F56568F56568F56568F56568F56568F56568F56568F56
                568F56568F56568F56568F56568F56568F56568F56568F56568F56568F56568F
                56568F56568F56568F56568F56568F56568F56568F56568F56568F56568F5656
                8F56568F56568F56568F56568F56568F56568F56568F56568F56568F56568F56
                568F56568F56568F56568F56568F56568F56568F56568F56568F56568E56568F
                56569056568F55558E54548C52528A4F4F874C4C8347477F42427B3B3B773838
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF007839397C3D3D8143
                43854A4A8A4F4F8D53538F5656925959935C5C955E5E955E5E945E5E945E5E94
                5D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D
                945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D
                5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D94
                5D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D
                945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D
                5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D94
                5D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D
                945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D
                5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D94
                5D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D
                945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D
                5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945D5D945E5E945E5E95
                5E5E955E5E935C5C9259598F56568D53538A4F4F854A4A8143437C3D3D783939
                000098626298626200FF0097616197616100FF0097616197616100FF00976161
                97616100FF0097616198626200FF009D6565A1686800FF007B3C3C7D40408247
                47884E4E8C5353905858935C5C965F5F96616197626298636398636398636398
                6464986464986464986464996464996464996464996464996464996464996464
                9964649964649964649964649964649964649964649964649964649964649964
                6499646499646499646499646499646499646499646499646499646499646499
                6464996464996464996464996464996464996464996464996464996464996464
                9964649964649964649964649964649964649964649964649964649964649964
                6499646499646499646499646499646499646499646499646499646499646499
                6464996464996464996464996464996464996464996464996464996464996464
                9964649964649964649964649964649964649964649964649964649964649964
                6499646499646499646499646499646499646499646499646499646499646499
                6464996464996464996464996464996464996464996464996464996464996464
                9964649964649964649964649964649964649964649964649964649964649964
                6499646499646499646499646498646498646498646498646498636398636398
                6363976262966161965F5F935C5C9058588C5353884E4E8247477D40407B3C3C
                000098626298616100FF0097616197616100FF0096616197616100FF00966161
                97616100FF0097616198616100FF009D6565A1686800FF007B3E3E7E4242844A
                4A8A50508F5656925A5A955F5F9863639A67679D6A6A9F6D6DA17070A37272A5
                7575A67777A67979A67979A87878A87878A87878A87878A87878A87878A87878
                A87878A87878A87878A87878A87878A87878A87878A87878A87878A87878A878
                78A87878A87878A87878A87878A87878A87878A87878A87878A87878A87878A8
                7878A87878A87878A87878A87878A87878A87878A87878A87878A87878A87878
                A87878A87878A87878A87878A87878A87878A87878A87878A87878A87878A878
                78A87878A87878A87878A87878A87878A87878A87878A87878A87878A87878A8
                7878A87878A87878A87878A87878A87878A87878A87878A87878A87878A87878
                A87878A87878A87878A87878A87878A87878A87878A87878A87878A87878A878
                78A87878A87878A87878A87878A87878A87878A87878A87878A87878A87878A8
                7878A87878A87878A87878A87878A87878A87878A87878A87878A87878A87878
                A87878A87878A87878A87878A87878A87878A87878A87878A87878A87878A878
                78A87878A87878A87878A87878A67979A67979A67777A57575A37272A170709F
                6D6D9D6A6A9A6767986363955F5F925A5A8F56568A5050844A4A7E42427B3E3E
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF007C3F3F7F4343854A
                4A8B52529059599661619C6A6AA27171A87979AB7F7FAF8585B38A8AB58D8DB8
                9191BA9494BB9696BB9696BB9797BB9797BB9797BB9797BB9797BB9797BB9797
                BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB97
                97BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB
                9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797
                BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB97
                97BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB
                9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797
                BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB97
                97BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB
                9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797
                BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB9797BB97
                97BB9797BB9797BB9797BB9797BB9696BB9696BA9494B89191B58D8DB38A8AAF
                8585AB7F7FA87979A271719C6A6A9661619059598B5252854A4A7F43437C3F3F
                000098626298626200FF0097616197616100FF0097616197616100FF00976161
                97616100FF0097616198626200FF009D6565A1686800FF007C3E3E7F4343874E
                4E905858986464A07070A77B7BAE8484B48C8CBA9393BD9999C09E9EC3A2A2C5
                A5A5C8A8A8C8A9A9C9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAA
                C9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AA
                AAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9
                AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAA
                C9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AA
                AAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9
                AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAA
                C9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AA
                AAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9
                AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAA
                C9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC9AA
                AAC9AAAAC9AAAAC9AAAAC9AAAAC9AAAAC8A9A9C8A8A8C5A5A5C3A2A2C09E9EBD
                9999BA9393B48C8CAE8484A77B7BA07070986464905858874E4E7F43437C3E3E
                000098626298616100FF0097616197616100FF0096616197616100FF00966161
                97616100FF0097616198616100FF009D6565A1686800FF007D40408348488E57
                57976464A17171A97D7DB08888B79191BD9999C3A1A1C6A7A7C9ABABCCAFAFCE
                B2B2D0B4B4D0B5B5D1B6B6D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7
                D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7
                B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1
                B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7
                D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7
                B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1
                B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7
                D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7
                B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1
                B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7
                D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7B7D1B7
                B7D1B7B7D1B7B7D1B7B7D1B7B7D1B6B6D0B5B5D0B4B4CEB2B2CCAFAFC9ABABC6
                A7A7C3A1A1BD9999B79191B08888A97D7DA171719764648E57578348487D4040
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF008248488950509460
                609D6E6EA67A7AAF8686B69191BD9A9AC3A2A2C7A9A9CAADADCDB2B2CFB5B5D1
                B8B8D3B9B9D3BBBBD3BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBC
                D4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BC
                BCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4
                BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBC
                D4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BC
                BCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4
                BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBC
                D4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BC
                BCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4
                BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBC
                D4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BCBCD4BC
                BCD4BCBCD4BCBCD4BCBCD4BCBCD3BCBCD3BBBBD3B9B9D1B8B8CFB5B5CDB2B2CA
                ADADC7A9A9C3A2A2BD9A9AB69191AF8686A67A7A9D6E6E946060895050824848
                000098626298626200FF0097616197616100FF0097616197616100FF00976161
                97616100FF0097616198626200FF009D6565A1686800FF008850508E58589867
                67A17474AA8181B28C8CBA9595C09E9EC4A6A6C8ABABCBAFAFCDB2B2CFB5B5D1
                B7B7D1B9B9D2BABAD2BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABA
                D3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BA
                BAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3
                BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABA
                D3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BA
                BAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3
                BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABA
                D3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BA
                BAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3
                BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABA
                D3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BABAD3BA
                BAD3BABAD3BABAD3BABAD3BABAD2BABAD2BABAD1B9B9D1B7B7CFB5B5CDB2B2CB
                AFAFC8ABABC4A6A6C09E9EBA9595B28C8CAA8181A174749867678E5858885050
                000098626298626200FF0097616197616100FF0097616197616100FF00976161
                97616100FF0097616198626200FF009D6565A1686800FF008B5656925E5E9C6D
                6DA57979AC8484B48D8DBA9696C09F9FC4A5A5C7A9A9C9ACACCAAEAECBB0B0CD
                B1B1CDB3B3CDB4B4CDB4B4CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5
                CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5
                B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CE
                B5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5
                CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5
                B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CE
                B5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5
                CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5
                B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CE
                B5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5
                CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5B5CEB5
                B5CEB5B5CEB5B5CEB5B5CEB5B5CDB4B4CDB4B4CDB3B3CDB1B1CBB0B0CAAEAEC9
                ACACC7A9A9C4A5A5C09F9FBA9696B48D8DAC8484A579799C6D6D925E5E8B5656
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF008E5A5A9461619D6E
                6EA47979AA8282B18C8CB69393BA9999BE9D9DC1A1A1C2A4A4C3A6A6C4A7A7C5
                A7A7C6A8A8C6A8A8C6A8A8C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9
                C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9
                A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6
                A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9
                C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9
                A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6
                A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9
                C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9
                A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6
                A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9
                C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9A9C6A9
                A9C6A9A9C6A9A9C6A9A9C6A9A9C6A8A8C6A8A8C6A8A8C5A7A7C4A7A7C3A6A6C2
                A4A4C1A1A1BE9D9DBA9999B69393B18C8CAA8282A479799D6E6E9461618E5A5A
                000099626299626200FF0098626298626200FF0098626298626200FF00986262
                98626200FF0098626299626200FF009D6565A1686800FF00905D5D9564649C6E
                6EA27676A77F7FAC8585B18B8BB49090B69393B89595B99898BA9999BB9A9ABB
                9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9B
                BC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B
                9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC
                9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9B
                BC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B
                9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC
                9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9B
                BC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B
                9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC
                9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9B
                BC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B
                9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBC9B9BBB9B9BBB9A9ABA9999B9
                9898B89595B69393B49090B18B8BAC8585A77F7FA276769C6E6E956464905D5D
                000099636399636300FF0099626299626200FF0099626299626200FF00996262
                99626200FF0099626299636300FF009C6565A0676700FF00915E5E9665659C6E
                6EA17575A67B7BA98181AD8686B08A8AB18C8CB38F8FB49090B59191B59191B5
                9191B69393B69494B69494B69494B69494B69494B69494B69494B69494B69494
                B69494B69494B69494B69494B69494B69494B69494B69494B69494B69494B694
                94B69494B69494B69494B69494B69494B69494B69494B69494B69494B69494B6
                9494B69494B69494B69494B69494B69494B69494B69494B69494B69494B69494
                B69494B69494B69494B69494B69494B69494B69494B69494B69494B69494B694
                94B69494B69494B69494B69494B69494B69494B69494B69494B69494B69494B6
                9494B69494B69494B69494B69494B69494B69494B69494B69494B69494B69494
                B69494B69494B69494B69494B69494B69494B69494B69494B69494B69494B694
                94B69494B69494B69494B69494B69494B69494B69494B69494B69494B69494B6
                9494B69494B69494B69494B69494B69494B69494B69494B69494B69494B69494
                B69494B69494B69494B69494B69494B69494B69494B69494B69494B69494B694
                94B69494B69494B69494B69494B69494B69494B69393B59191B59191B59191B4
                9090B38F8FB18C8CB08A8AAD8686A98181A67B7BA175759C6E6E966565915E5E
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00009F57579F57579F57579F57579F57579F57579F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F5757C59494D8B8B800FF0000FF0000FF
                0000FF00D8B8B8C594949F57579F57579F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F57579F57579F575700FF0000FF00
                00FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF00C594949F575700FF0000
                FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF00
                9F57579F575700FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF
                0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF009F57579F5757
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF00D8B8B8C594949F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F57579F57579F57579F57579F57579F57
                579F575700FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00C594949F5757
                00009F57579F57579F57579F57579F57579F57579F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F57579F5757C5949400FF0000FF0000FF
                0000FF00C594949F57579F57579F57579F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F57579F57579F575700FF0000FF00
                00FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF00C594949F5757C5949400FF0000
                FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF00
                9F57579F575700FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF
                0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF009F57579F5757
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF00C594949F57579F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F57579F57579F57579F57579F57579F57
                579F575700FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00C594949F5757C59494
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF00C594949F5757C5949400FF0000FF0000
                FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF00
                9F57579F575700FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF
                0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF009F57579F5757
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF00C594949F5757C5949400FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF00C594949F5757C5949400FF0000FF0000FF0000
                FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF00
                9F57579F575700FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF
                0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF009F57579F5757
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF00C594949F5757C5949400FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF
                0000FF009F57579F57579F57579F57579F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F57579F57579F575700FF0000FF00
                00FF0000FF009F57579F57579F57579F57579F57579F57579F57579F57579F57
                579F57579F57579F57579F57579F57579F57579F5757C59494D8B8B800FF0000
                FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF00
                9F57579F575700FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF
                0000FF0000FF0000FF009F57579F57579F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F57579F57579F57579F57579F5757
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF009F57579F57579F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F57579F57579F57579F57579F57579F57
                579F575700FF0000FF0000FF0000FF009F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F5757C5949400FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF
                0000FF009F57579F57579F57579F57579F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F57579F57579F575700FF0000FF00
                00FF0000FF009F57579F57579F57579F57579F57579F57579F57579F57579F57
                579F57579F57579F57579F57579F57579F57579F57579F5757C5949400FF0000
                FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF00
                9F57579F575700FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF
                0000FF0000FF0000FF009F57579F57579F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F57579F57579F57579F57579F5757
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF009F57579F57579F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F57579F57579F57579F57579F57579F57
                579F575700FF0000FF0000FF0000FF009F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F5757C5949400FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000
                FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF00
                9F57579F575700FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF
                0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF009F57579F5757
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF00C594949F5757C5949400FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000
                FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF00
                9F57579F575700FF0000FF0000FF0000FF0000FF0000FF009F57579F575700FF
                0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF009F57579F5757
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF009F57579F575700FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF00C594949F5757C5949400FF00
                00009F57579F57579F57579F57579F57579F57579F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F57579F5757C5949400FF0000FF0000FF
                0000FF00C594949F57579F57579F57579F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F57579F57579F575700FF0000FF00
                00FF0000FF009F57579F57579F57579F57579F57579F57579F57579F57579F57
                579F57579F57579F57579F57579F57579F57579F57579F5757C5949400FF0000
                FF0000FF0000FF00C594949F57579F57579F57579F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F57579F57579F57579F5757C5949400FF
                0000FF0000FF0000FF00C594949F57579F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F57579F57579F57579F5757C59494
                00FF0000FF0000FF0000FF00C594949F57579F57579F57579F57579F57579F57
                579F57579F57579F57579F57579F57579F57579F57579F57579F57579F5757C5
                949400FF0000FF0000FF0000FF00C594949F57579F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F57579F57579F57579F57579F57579F57
                579F575700FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00C594949F5757C59494
                00009F57579F57579F57579F57579F57579F57579F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F5757D8B8B8D8B8B800FF0000FF0000FF
                0000FF00D8B8B8C594949F57579F57579F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F57579F57579F575700FF0000FF00
                00FF0000FF009F57579F57579F57579F57579F57579F57579F57579F57579F57
                579F57579F57579F57579F57579F57579F57579F5757C59494D8B8B800FF0000
                FF0000FF0000FF00D8B8B8C594949F57579F57579F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F57579F57579F5757C59494D8B8B800FF
                0000FF0000FF0000FF00D8B8B8C594949F57579F57579F57579F57579F57579F
                57579F57579F57579F57579F57579F57579F57579F57579F5757C59494D8B8B8
                00FF0000FF0000FF0000FF00D8B8B8C594949F57579F57579F57579F57579F57
                579F57579F57579F57579F57579F57579F57579F57579F57579F5757C59494D8
                B8B800FF0000FF0000FF0000FF00D8B8B8C594949F57579F57579F57579F5757
                9F57579F57579F57579F57579F57579F57579F57579F57579F57579F57579F57
                579F575700FF0000FF0000FF0000FF009F57579F575700FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00C594949F5757
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
                0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
                FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
                0000}
              Proportional = True
              Transparent = True
            end
            object Panel11: TPanel
              Left = 0
              Top = 0
              Width = 1314
              Height = 57
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object BtOrc: TBitBtn
                Tag = 10006
                Left = 6
                Top = 2
                Width = 148
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Or'#231'amen.'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtOrcClick
              end
              object BtAvanca1: TBitBtn
                Tag = 10034
                Left = 314
                Top = 2
                Width = 148
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Pr'#243'ximo'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtAvanca1Click
              end
            end
          end
          object TabSheet4: TTabSheet
            Tag = 10014
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Arrecada'#231#245'es'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel12: TPanel
              Left = 0
              Top = 0
              Width = 1318
              Height = 57
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 1314
              object BtArrecada: TBitBtn
                Tag = 10014
                Left = 6
                Top = 2
                Width = 148
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Arrecada'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtArrecadaClick
              end
              object BtAvanca2: TBitBtn
                Tag = 10034
                Left = 314
                Top = 2
                Width = 148
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Pr'#243'ximo'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtAvanca2Click
              end
              object BtVolta2: TBitBtn
                Tag = 10033
                Left = 160
                Top = 2
                Width = 148
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Anterior'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtVolta2Click
              end
            end
            object PCArrecada: TPageControl
              Left = 0
              Top = 57
              Width = 1318
              Height = 598
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              ActivePage = TabSheet8
              Align = alClient
              TabHeight = 25
              TabOrder = 1
              OnChange = PCArrecadaChange
              ExplicitWidth = 1314
              ExplicitHeight = 582
              object TabSheet8: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Arrecada'#231#245'es'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Splitter3: TSplitter
                  Left = 809
                  Top = 0
                  Width = 12
                  Height = 539
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alRight
                end
                object Panel8: TPanel
                  Left = 0
                  Top = 0
                  Width = 809
                  Height = 539
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Splitter2: TSplitter
                    Left = 624
                    Top = 0
                    Width = 12
                    Height = 523
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alRight
                  end
                  object LaMesArr: TLabel
                    Left = 0
                    Top = 523
                    Width = 58
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alBottom
                    Caption = 'LaMesArr'
                  end
                  object DBGradeArre: TdmkDBGridZTO
                    Left = 0
                    Top = 0
                    Width = 624
                    Height = 523
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    DataSource = DsArre
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -14
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'NOMECNAB_Cfg'
                        Title.Caption = 'Configura'#231#227'o do boleto'
                        Width = 150
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMEENT'
                        Title.Caption = 'Entidade'
                        Width = 250
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Valor'
                        Visible = True
                      end>
                  end
                  object DBGradeArreBlo: TdmkDBGridZTO
                    Left = 636
                    Top = 0
                    Width = 173
                    Height = 523
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alRight
                    DataSource = DsArreBol
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -14
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Boleto'
                        Width = 80
                        Visible = True
                      end>
                  end
                end
                object Panel9: TPanel
                  Left = 821
                  Top = 0
                  Width = 484
                  Height = 539
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alRight
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 1
                  object DBGradeArreIts: TdmkDBGridZTO
                    Left = 0
                    Top = 0
                    Width = 484
                    Height = 539
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    DataSource = DsArreIts
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -14
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Texto'
                        Title.Caption = 'Entidade'
                        Width = 255
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Valor'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NFSeSrvCad_TXT'
                        Title.Caption = 'Regra Fiscal'
                        Width = 150
                        Visible = True
                      end>
                  end
                end
              end
              object TabSheet9: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Arrecada'#231#245'es futuras'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object DBGradeArreFutt: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 1305
                  Height = 539
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsArreFutI
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -14
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'PERIODO_TXT'
                      Title.Caption = 'Per'#237'odo'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOMEENT'
                      Title.Caption = 'Entidade'
                      Width = 300
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Texto'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Valor'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOMECONTA'
                      Title.Caption = 'Conta (Plano de contas)'
                      Width = 160
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NFSeSrvCad_TXT'
                      Title.Caption = 'Regra Fiscal'
                      Width = 150
                      Visible = True
                    end>
                end
              end
            end
          end
          object TabSheet5: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pr'#233'-boletos'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Splitter5: TSplitter
              Left = 825
              Top = 57
              Width = 12
              Height = 582
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
            end
            object Panel6: TPanel
              Left = 0
              Top = 57
              Width = 825
              Height = 582
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object LaMesPreBol: TLabel
                Left = 0
                Top = 566
                Width = 82
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
                Caption = 'LaMesPreBol'
              end
              object DBGradePreBlo: TdmkDBGridZTO
                Left = 0
                Top = 0
                Width = 825
                Height = 566
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                FieldsCalcToOrder.Strings = (
                  'VENCTO_TXT=Vencto')
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOMECNAB_Cfg'
                    Title.Caption = 'Configura'#231#227'o do boleto'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEENT'
                    Title.Caption = 'Entidade'
                    Width = 275
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VENCTO_TXT'
                    Title.Caption = 'Vencimento'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Visible = True
                  end>
              end
            end
            object Panel13: TPanel
              Left = 837
              Top = 57
              Width = 477
              Height = 582
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object DBGradePreBloIts: TdmkDBGridZTO
                Left = 0
                Top = 0
                Width = 478
                Height = 582
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'TEXTO'
                    Title.Caption = 'Texto'
                    Width = 255
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VALOR'
                    Title.Caption = 'Valor'
                    Visible = True
                  end>
              end
            end
            object Panel14: TPanel
              Left = 0
              Top = 0
              Width = 1314
              Height = 57
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 2
              object BtPrebol: TBitBtn
                Tag = 10020
                Left = 6
                Top = 2
                Width = 148
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'P&r'#233'-bol.'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtPrebolClick
              end
              object BtVolta3: TBitBtn
                Tag = 10033
                Left = 160
                Top = 2
                Width = 148
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Anterior'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtVolta3Click
              end
              object BtAvanca3: TBitBtn
                Tag = 10034
                Left = 314
                Top = 2
                Width = 148
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Pr'#243'ximo'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtAvanca3Click
              end
            end
          end
          object TabSheet6: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Boletos'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Splitter1: TSplitter
              Left = 687
              Top = 57
              Width = 12
              Height = 582
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
            end
            object Panel16: TPanel
              Left = 0
              Top = 57
              Width = 687
              Height = 582
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object LaMesBol: TLabel
                Left = 0
                Top = 566
                Width = 61
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
                Caption = 'LaMesBol'
              end
              object DBGradeBlo: TdmkDBGridZTO
                Left = 0
                Top = 0
                Width = 687
                Height = 566
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsBoletos
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                OnDblClick = DBGradeBloDblClick
                FieldsCalcToOrder.Strings = (
                  'VENCTO_TXT=Vencto')
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Avulso'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMECNAB_Cfg'
                    Title.Caption = 'Configura'#231#227'o do boleto'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Boleto'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEENT'
                    Title.Caption = 'Entidade'
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VENCTO_TXT'
                    Title.Caption = 'Vencimento'
                    Width = 85
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'FatID'
                    Title.Caption = 'Fat. Orig.'
                    Width = 65
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'FatNum'
                    Title.Caption = 'Fat. Num.'
                    Width = 65
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataCad'
                    Title.Caption = 'Data emiss'#227'o'
                    Width = 85
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Envios'
                    Title.Caption = 'Formas de Env.'
                    Width = 90
                    Visible = True
                  end>
              end
            end
            object Panel17: TPanel
              Left = 699
              Top = 57
              Width = 615
              Height = 582
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object DBGradeBloIts: TdmkDBGridZTO
                Left = 0
                Top = 129
                Width = 615
                Height = 453
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'TEXTO'
                    Title.Caption = 'Texto'
                    Width = 255
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VALOR'
                    Title.Caption = 'Valor'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Lancto'
                    Title.Caption = 'Lan'#231'amento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SerieNF'
                    Title.Caption = 'S'#233'rie NF'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NotaFiscal'
                    Title.Caption = 'Nota Fiscal'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'ID'
                    Visible = True
                  end>
              end
              object GroupBox8: TGroupBox
                Left = 0
                Top = 0
                Width = 615
                Height = 129
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = ' Hist'#243'rico bancario do boleto (retorno CNAB):'
                TabOrder = 1
                object dmkDBGridZTO2: TdmkDBGridZTO
                  Left = 2
                  Top = 18
                  Width = 611
                  Height = 109
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsLeiBol
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -14
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  FieldsCalcToOrder.Strings = (
                    'OCOR_TXT=OcorrCodi')
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'ID_Link'
                      Title.Caption = 'Link ID'
                      Width = 54
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OcorrCodi'
                      Title.Caption = 'Ocorr'#234'ncia'
                      Width = 34
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OCOR_TXT'
                      Title.Caption = 'Descri'#231#227'o Ocorr'#234'ncia'
                      Width = 260
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OcorrData'
                      Title.Caption = 'Dta ocor.'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValTarif'
                      Title.Caption = '$ tarifa'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValTitul'
                      Title.Caption = '$ t'#237'tulo'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValPago'
                      Title.Caption = '$ pago'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Motivo1'
                      Title.Caption = 'Mot.1'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Motivo2'
                      Title.Caption = 'Mot.2'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Motivo3'
                      Title.Caption = 'Mot.3'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Motivo4'
                      Title.Caption = 'Mot.4'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Motivo5'
                      Title.Caption = 'Mot.5'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValJuros'
                      Title.Caption = '$ juros'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValMulta'
                      Title.Caption = '$ multa'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValJuMul'
                      Title.Caption = '$ jur+mul.'
                      Visible = True
                    end>
                end
              end
            end
            object Panel15: TPanel
              Left = 0
              Top = 0
              Width = 1314
              Height = 57
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 2
              object BtBoleto1: TBitBtn
                Tag = 10007
                Left = 6
                Top = 2
                Width = 148
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Boleto'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtBoleto1Click
              end
              object BtImprime: TBitBtn
                Tag = 5
                Left = 314
                Top = 2
                Width = 148
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Imprime'
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BtImprimeClick
              end
              object BtProtocolo: TBitBtn
                Tag = 10051
                Left = 468
                Top = 2
                Width = 147
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Protocolo'
                NumGlyphs = 2
                TabOrder = 3
                OnClick = BtProtocoloClick
              end
              object BtRetorno: TBitBtn
                Tag = 339
                Left = 622
                Top = 2
                Width = 147
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Retorno &CNAB'
                NumGlyphs = 2
                TabOrder = 4
                OnClick = BtRetornoClick
              end
              object BtPesq: TBitBtn
                Tag = 319
                Left = 775
                Top = 2
                Width = 148
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'P&agamentos'
                NumGlyphs = 2
                TabOrder = 5
                OnClick = BtPesqClick
              end
              object BtVolta4: TBitBtn
                Tag = 10033
                Left = 160
                Top = 2
                Width = 148
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Anterior'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtVolta4Click
              end
              object BtTodosMens: TBitBtn
                Tag = 127
                Left = 929
                Top = 2
                Width = 49
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                NumGlyphs = 2
                TabOrder = 6
                OnClick = BtTodosMensClick
              end
              object BtNenhumMens: TBitBtn
                Tag = 128
                Left = 985
                Top = 2
                Width = 49
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                NumGlyphs = 2
                TabOrder = 7
                OnClick = BtNenhumMensClick
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Pesquisa'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter4: TSplitter
        Left = 653
        Top = 267
        Width = 9
        Height = 475
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        ExplicitLeft = 651
        ExplicitHeight = 467
      end
      object Panel19: TPanel
        Left = 0
        Top = 0
        Width = 1326
        Height = 267
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        ExplicitWidth = 1324
        object Label2: TLabel
          Left = 7
          Top = 10
          Width = 143
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Configura'#231#227'o do boleto:'
        end
        object Label1: TLabel
          Left = 667
          Top = 10
          Width = 116
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Arrecada'#231#227'o base:'
        end
        object Label3: TLabel
          Left = 452
          Top = 69
          Width = 57
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Entidade:'
        end
        object Label4: TLabel
          Left = 7
          Top = 69
          Width = 164
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Conta (do plano de contas):'
        end
        object Bevel2: TBevel
          Left = 512
          Top = 126
          Width = 308
          Height = 73
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
        end
        object Label17: TLabel
          Left = 1202
          Top = 69
          Width = 42
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Boleto:'
        end
        object Label20: TLabel
          Left = 1088
          Top = 69
          Width = 77
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Lan'#231'amento:'
        end
        object CBCNABCfg: TdmkDBLookupComboBox
          Left = 76
          Top = 30
          Width = 579
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DBloGeren.DsCNABcfg
          TabOrder = 1
          dmkEditCB = EdCNABCfg
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCNABCfg: TdmkEditCB
          Left = 7
          Top = 30
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCNABCfg
          IgnoraDBLookupComboBox = False
        end
        object CBArre: TdmkDBLookupComboBox
          Left = 736
          Top = 30
          Width = 578
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          TabOrder = 3
          dmkEditCB = EdArre
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdArre: TdmkEditCB
          Left = 667
          Top = 30
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBArre
          IgnoraDBLookupComboBox = False
        end
        object CBEntidade: TdmkDBLookupComboBox
          Left = 521
          Top = 89
          Width = 560
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          TabOrder = 7
          dmkEditCB = EdEntidade
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdEntidade: TdmkEditCB
          Left = 452
          Top = 89
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEntidade
          IgnoraDBLookupComboBox = False
        end
        object EdConta: TdmkEditCB
          Left = 7
          Top = 89
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBConta
          IgnoraDBLookupComboBox = False
        end
        object CBConta: TdmkDBLookupComboBox
          Left = 76
          Top = 89
          Width = 370
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          TabOrder = 5
          dmkEditCB = EdConta
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object GBEmissao: TGroupBox
          Left = 7
          Top = 126
          Width = 247
          Height = 73
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Per'#237'odo de emiss'#227'o: '
          TabOrder = 10
          object CkDataEmiIni: TCheckBox
            Left = 10
            Top = 17
            Width = 111
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
            TabOrder = 0
          end
          object TPDataEmiIni: TDateTimePicker
            Left = 10
            Top = 39
            Width = 111
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            CalColors.TextColor = clMenuText
            Date = 37636.777157974500000000
            Time = 37636.777157974500000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object CkDataEmiFim: TCheckBox
            Left = 123
            Top = 17
            Width = 111
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final:'
            TabOrder = 2
          end
          object TPDataEmiFim: TDateTimePicker
            Left = 123
            Top = 39
            Width = 111
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 37636.777203761600000000
            Time = 37636.777203761600000000
            TabOrder = 3
          end
        end
        object GroupBox1: TGroupBox
          Left = 258
          Top = 126
          Width = 247
          Height = 73
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Per'#237'odo de vencimento: '
          TabOrder = 11
          object CkDataVctIni: TCheckBox
            Left = 10
            Top = 17
            Width = 111
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
            TabOrder = 0
          end
          object TPDataVctIni: TDateTimePicker
            Left = 10
            Top = 39
            Width = 111
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            CalColors.TextColor = clMenuText
            Date = 37636.777157974500000000
            Time = 37636.777157974500000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object CkDataVctFim: TCheckBox
            Left = 123
            Top = 17
            Width = 111
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final:'
            TabOrder = 2
          end
          object TPDataVctFim: TDateTimePicker
            Left = 123
            Top = 39
            Width = 111
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 37636.777203761600000000
            Time = 37636.777203761600000000
            TabOrder = 3
          end
        end
        object CkValor: TCheckBox
          Left = 521
          Top = 137
          Width = 297
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor (para '#250'nico preencha apenas o primeiro):'
          TabOrder = 12
        end
        object EdValorIni: TdmkEdit
          Left = 521
          Top = 165
          Width = 93
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdValorFim: TdmkEdit
          Left = 619
          Top = 165
          Width = 94
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object Panel20: TPanel
          Left = 1
          Top = 209
          Width = 1324
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 16
          ExplicitWidth = 1322
          object BtBoleto2: TBitBtn
            Tag = 10007
            Left = 159
            Top = 2
            Width = 147
            Height = 50
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Boleto'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtBoleto2Click
          end
          object BitBtn3: TBitBtn
            Tag = 5
            Left = 311
            Top = 2
            Width = 197
            Height = 50
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Imprime selecionados'
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BitBtn3Click
          end
          object BitBtn4: TBitBtn
            Tag = 10051
            Left = 513
            Top = 2
            Width = 148
            Height = 50
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Protocolo'
            NumGlyphs = 2
            TabOrder = 3
            OnClick = BitBtn4Click
          end
          object BitBtn5: TBitBtn
            Tag = 339
            Left = 666
            Top = 2
            Width = 148
            Height = 50
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Retorno &CNAB'
            NumGlyphs = 2
            TabOrder = 4
            OnClick = BitBtn5Click
          end
          object BitBtn6: TBitBtn
            Tag = 319
            Left = 818
            Top = 2
            Width = 148
            Height = 50
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'P&agamentos'
            NumGlyphs = 2
            TabOrder = 5
            OnClick = BitBtn6Click
          end
          object BtPesquisa: TBitBtn
            Tag = 22
            Left = 6
            Top = 2
            Width = 148
            Height = 50
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Pesquisa'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtPesquisaClick
          end
          object BtTodosPesq: TBitBtn
            Tag = 127
            Left = 972
            Top = 2
            Width = 50
            Height = 50
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            TabOrder = 6
            OnClick = BtTodosPesqClick
          end
          object BtNenhumPesq: TBitBtn
            Tag = 128
            Left = 1028
            Top = 2
            Width = 49
            Height = 50
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            TabOrder = 7
            OnClick = BtNenhumPesqClick
          end
        end
        object GroupBox2: TGroupBox
          Left = 830
          Top = 126
          Width = 484
          Height = 73
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Emiss'#227'o atrav'#233's de faturamento:'
          TabOrder = 15
          object Label75: TLabel
            Left = 11
            Top = 18
            Width = 165
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Origem do faturamento [F4]:'
          end
          object Label5: TLabel
            Left = 384
            Top = 18
            Width = 16
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ID:'
          end
          object EdFatID_Cod: TdmkEdit
            Left = 11
            Top = 39
            Width = 62
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            CharCase = ecUpperCase
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFatID_CodChange
            OnKeyDown = EdFatID_CodKeyDown
          end
          object EdFatID_Txt: TEdit
            Left = 74
            Top = 39
            Width = 305
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Color = clInactiveCaption
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
          object EdFatNum: TdmkEdit
            Left = 384
            Top = 39
            Width = 92
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            CharCase = ecUpperCase
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object EdBoleto: TdmkEdit
          Left = 1202
          Top = 89
          Width = 111
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          CharCase = ecUpperCase
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdLancto: TdmkEdit
          Left = 1088
          Top = 89
          Width = 111
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          CharCase = ecUpperCase
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object Panel22: TPanel
        Left = 0
        Top = 267
        Width = 653
        Height = 475
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        ExplicitWidth = 651
        ExplicitHeight = 467
        object LaPesBol: TLabel
          Left = 0
          Top = 450
          Width = 59
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Caption = 'LaPesBol'
        end
        object DBGradeBlo2: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 651
          Height = 450
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGradeBlo2DblClick
          FieldsCalcToOrder.Strings = (
            'VENCTO_TXT=Vencto')
          Columns = <
            item
              Expanded = False
              FieldName = 'Avulso'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECNAB_Cfg'
              Title.Caption = 'Configura'#231#227'o do boleto'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Boleto'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Entidade'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VENCTO_TXT'
              Title.Caption = 'Vencimento'
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatID'
              Title.Caption = 'Orig. Fat.'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Fat. Num.'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataCad'
              Title.Caption = 'Data emiss'#227'o'
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Envios'
              Title.Caption = 'Formas de Env.'
              Width = 90
              Visible = True
            end>
        end
      end
      object Panel33: TPanel
        Left = 662
        Top = 267
        Width = 664
        Height = 475
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
        ExplicitLeft = 660
        ExplicitHeight = 467
        object Splitter10: TSplitter
          Left = 0
          Top = 129
          Width = 665
          Height = 6
          Cursor = crVSplit
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
        end
        object Panel21: TPanel
          Left = 0
          Top = 135
          Width = 665
          Height = 331
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGradeBloIts2: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 665
            Height = 331
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'TEXTO'
                Title.Caption = 'Texto'
                Width = 255
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VALOR'
                Title.Caption = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Lancto'
                Title.Caption = 'Lan'#231'amento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SerieNF'
                Title.Caption = 'S'#233'rie NF'
                Visible = True
              end
              item
                Alignment = taRightJustify
                Expanded = False
                FieldName = 'NotaFiscal'
                Title.Caption = 'Nota Fiscal'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
        end
        object GroupBox7: TGroupBox
          Left = 0
          Top = 0
          Width = 665
          Height = 129
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = ' Hist'#243'rico bancario do boleto (retorno CNAB):'
          TabOrder = 1
          object dmkDBGridZTO1: TdmkDBGridZTO
            Left = 2
            Top = 18
            Width = 660
            Height = 109
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsLeiBol
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'ID_Link'
                Title.Caption = 'Link ID'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'Ocorr'#234'ncia'
                Width = 34
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OCOR_TXT'
                Title.Caption = 'Descri'#231#227'o Ocorr'#234'ncia'
                Width = 260
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Dta ocor.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTarif'
                Title.Caption = '$ tarifa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = '$ t'#237'tulo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = '$ pago'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Motivo1'
                Title.Caption = 'Mot.1'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Motivo2'
                Title.Caption = 'Mot.2'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Motivo3'
                Title.Caption = 'Mot.3'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Motivo4'
                Title.Caption = 'Mot.4'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Motivo5'
                Title.Caption = 'Mot.5'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = '$ juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = '$ multa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = '$ jur+mul.'
                Visible = True
              end>
          end
        end
      end
    end
    object TabSheet7: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Protocolo de Entrega de Boleto'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel23: TPanel
        Left = 0
        Top = 0
        Width = 1326
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        ExplicitWidth = 1324
        object BtPEBProtocolo: TBitBtn
          Tag = 14
          Left = 6
          Top = 2
          Width = 148
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Protocolo'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtPEBProtocoloClick
        end
        object BtPEBVoltar: TBitBtn
          Tag = 10071
          Left = 622
          Top = 2
          Width = 147
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Voltar'
          NumGlyphs = 2
          TabOrder = 4
          OnClick = BtPEBVoltarClick
        end
        object BtPEBNenhum: TBitBtn
          Tag = 128
          Left = 314
          Top = 2
          Width = 148
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Nenhum'
          NumGlyphs = 2
          TabOrder = 2
          OnClick = BtPEBNenhumClick
        end
        object BtPEBEnvia: TBitBtn
          Tag = 244
          Left = 468
          Top = 2
          Width = 147
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Envia sel.'
          NumGlyphs = 2
          TabOrder = 3
          OnClick = BtPEBEnviaClick
        end
        object BtPEBTodos: TBitBtn
          Tag = 127
          Left = 160
          Top = 2
          Width = 148
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Todos'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtPEBTodosClick
        end
      end
      object TBProtocolos: TTabControl
        Left = 0
        Top = 57
        Width = 1326
        Height = 685
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabHeight = 25
        TabOrder = 1
        Tabs.Strings = (
          'Entidades cadastradas para entrega do documento'
          'Entidades cadastradas para entrega por e-mail'
          'Entidades cadastradas para cobran'#231'a registrada')
        TabIndex = 0
        OnChange = TBProtocolosChange
        ExplicitWidth = 1324
        ExplicitHeight = 677
        object LaProBol: TLabel
          Left = 4
          Top = 665
          Width = 1318
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Caption = 'LaMesBol'
          ExplicitTop = 661
          ExplicitWidth = 61
        end
        object TBGerados: TTabControl
          Left = 4
          Top = 31
          Width = 1318
          Height = 634
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabHeight = 25
          TabOrder = 0
          Tabs.Strings = (
            'N'#227'o gerados'
            'Gerados')
          TabIndex = 0
          OnChange = TBGeradosChange
          ExplicitLeft = 5
          ExplicitTop = 38
          ExplicitWidth = 1314
          ExplicitHeight = 614
          object Splitter6: TSplitter
            Left = 4
            Top = 154
            Width = 1310
            Height = 13
            Cursor = crVSplit
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            ExplicitLeft = 5
            ExplicitTop = 161
            ExplicitWidth = 1305
          end
          object DBGrid1: TDBGrid
            Left = 4
            Top = 31
            Width = 1310
            Height = 123
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            DataSource = DsProt1
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 800
                Visible = True
              end>
          end
          object DBG_ProtocoBol: TdmkDBGridZTO
            Left = 4
            Top = 167
            Width = 1310
            Height = 463
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DBloGeren.DsProtocoBol
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            OnDblClick = DBG_ProtocoBolDblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'LOTE'
                Title.Caption = 'Lote'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PROTOCOLO'
                Title.Caption = 'Protocolo'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 75
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataE_Txt'
                Title.Caption = 'Envio'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataD_Txt'
                Title.Caption = 'Retorno'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Boleto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Entidade'
                Width = 215
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Email'
                Title.Caption = 'E-mail'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DELIVER'
                Title.Caption = 'Respons'#225'vel pela tarefa'
                Width = 200
                Visible = True
              end>
          end
        end
      end
    end
    object TabSheet13: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Arrecada'#231#245'es por per'#237'odo'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter9: TSplitter
        Left = 758
        Top = 57
        Width = 12
        Height = 677
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
      end
      object Panel25: TPanel
        Left = 0
        Top = 0
        Width = 1324
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 1147
          Top = 0
          Width = 177
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
        end
        object BtAPCarrega: TBitBtn
          Tag = 14
          Left = 7
          Top = 2
          Width = 148
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Carrega'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtAPCarregaClick
        end
        object BtAPGera: TBitBtn
          Tag = 10007
          Left = 158
          Top = 2
          Width = 147
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Gera'
          NumGlyphs = 2
          TabOrder = 2
          OnClick = BtAPGeraClick
        end
        object BtAPVoltar: TBitBtn
          Tag = 10071
          Left = 308
          Top = 2
          Width = 147
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Voltar'
          NumGlyphs = 2
          TabOrder = 3
          OnClick = BtAPVoltarClick
        end
      end
      object Panel26: TPanel
        Left = 770
        Top = 57
        Width = 554
        Height = 677
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object dmkDBGrid2: TdmkDBGrid
          Left = 59
          Top = 0
          Width = 495
          Height = 677
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Adiciona'
              Title.Caption = 'Ok'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TextoTmp'
              Title.Caption = 'Texto a ser impresso'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsArreIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dmkDBGrid2CellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Adiciona'
              Title.Caption = 'Ok'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TextoTmp'
              Title.Caption = 'Texto a ser impresso'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end>
        end
        object Panel27: TPanel
          Left = 0
          Top = 0
          Width = 59
          Height = 677
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          ParentBackground = False
          TabOrder = 1
          object BtAPItsTodos: TBitBtn
            Tag = 127
            Left = 5
            Top = 4
            Width = 49
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtAPItsTodosClick
          end
          object BtAPItsNenhum: TBitBtn
            Tag = 128
            Left = 5
            Top = 57
            Width = 49
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtAPItsNenhumClick
          end
        end
      end
      object Panel28: TPanel
        Left = 0
        Top = 57
        Width = 758
        Height = 677
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        object dmkDBGrid3: TdmkDBGrid
          Left = 59
          Top = 0
          Width = 699
          Height = 677
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Adiciona'
              Title.Caption = 'Ok'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomeArre'
              Title.Caption = 'Conta'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomeEnti'
              Title.Caption = 'Entidade'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CodContrato'
              Title.Caption = 'ID Contr.'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomeContrato'
              Title.Caption = 'Contrato'
              Width = 300
              Visible = True
            end>
          Color = clWindow
          DataSource = DsArre
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dmkDBGrid3CellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Adiciona'
              Title.Caption = 'Ok'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomeArre'
              Title.Caption = 'Conta'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomeEnti'
              Title.Caption = 'Entidade'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CodContrato'
              Title.Caption = 'ID Contr.'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NomeContrato'
              Title.Caption = 'Contrato'
              Width = 300
              Visible = True
            end>
        end
        object Panel29: TPanel
          Left = 0
          Top = 0
          Width = 59
          Height = 677
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          ParentBackground = False
          TabOrder = 1
          object BtAPArreTodos: TBitBtn
            Tag = 127
            Left = 5
            Top = 4
            Width = 49
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtAPArreTodosClick
          end
          object BtAPArreNenhum: TBitBtn
            Tag = 128
            Left = 5
            Top = 57
            Width = 49
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtAPArreNenhumClick
          end
        end
      end
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 64
    Width = 1334
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 3
  end
  object PMArrecada: TPopupMenu
    OnPopup = PMArrecadaPopup
    Left = 234
    Top = 10
    object Incluiitensbasedearrecadao1: TMenuItem
      Caption = '&Inclui itens base de arrecada'#231#227'o'
      OnClick = Incluiitensbasedearrecadao1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Novoitemdearrecadao1: TMenuItem
      Caption = '&Novo item de arrecada'#231#227'o'
      OnClick = Novoitemdearrecadao1Click
    end
    object Alteraarrecadao1: TMenuItem
      Caption = '&Altera arrecada'#231#227'o selecionada'
      OnClick = Alteraarrecadao1Click
    end
    object Exclusodearrecadao1: TMenuItem
      Caption = '&Exclus'#227'o de arrecada'#231#227'o'
      object Itemselecionado1: TMenuItem
        Caption = '&Item selecionado'
        OnClick = Itemselecionado1Click
      end
      object ODOSitensdearrecadao1: TMenuItem
        Caption = '&Todos itens'
        OnClick = ODOSitensdearrecadao1Click
      end
    end
  end
  object PMPrebol: TPopupMenu
    OnPopup = PMPrebolPopup
    Left = 262
    Top = 10
    object Boleto1: TMenuItem
      Caption = '&Boleto'
      object Gerartodosabertos1: TMenuItem
        Caption = 'Gerar &Todos abertos'
        OnClick = Gerartodosabertos1Click
      end
      object Gerarselecionados1: TMenuItem
        Caption = 'Gerar &Selecionado(s)'
        OnClick = Gerarselecionados1Click
      end
    end
    object AlteraVencimento2: TMenuItem
      Caption = 'Altera &Vencimento'
      object DosboletosSelecionados1: TMenuItem
        Caption = 'Dos boletos &Selecionado(s)'
        OnClick = DosboletosSelecionados1Click
      end
      object DeTodosboletos1: TMenuItem
        Caption = 'De &Todos boletos'
        OnClick = DeTodosboletos1Click
      end
    end
  end
  object PMBoleto: TPopupMenu
    OnPopup = PMBoletoPopup
    Left = 290
    Top = 10
    object Gerartodooperodo1: TMenuItem
      Caption = '&Gerar per'#237'odo'
      OnClick = Gerartodooperodo1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Alteravencimento1: TMenuItem
      Caption = 'Altera &Vencimento'
      object DosboletosSelecionados2: TMenuItem
        Caption = 'Do(s) boleto(s) &Selecionado(s)'
        OnClick = DosboletosSelecionados2Click
      end
      object DeTodosboletos2: TMenuItem
        Caption = 'De &Todos boletos'
        OnClick = DeTodosboletos2Click
      end
    end
    object Desfazerboletos2: TMenuItem
      Caption = '&Desfazer boletos'
      object Selecionados3: TMenuItem
        Caption = '&Selecionado(s)'
        OnClick = Selecionados3Click
      end
      object Todos3: TMenuItem
        Caption = '&Todos'
        OnClick = Todos3Click
      end
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Editar1: TMenuItem
      Caption = '&Editar protocolo de entrega (Selecionados)'
      OnClick = Editar1Click
    end
    object Gerenciar1: TMenuItem
      Caption = 'Gerenciar &protocolo de entrega para entidade atual'
      OnClick = Gerenciar1Click
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object GerarNFSeparaosboletosselecionados1: TMenuItem
      Caption = 'Gerar &NFS-e para o(s) boleto(s) selecionado(s)'
      OnClick = GerarNFSeparaosboletosselecionados1Click
    end
  end
  object DsPrev: TDataSource
    DataSet = QrPrev
    Left = 507
    Top = 10
  end
  object QrPrev: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPrevAfterOpen
    BeforeClose = QrPrevBeforeClose
    AfterClose = QrPrevAfterClose
    AfterScroll = QrPrevAfterScroll
    OnCalcFields = QrPrevCalcFields
    SQL.Strings = (
      'SELECT prv.*, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEEMP'
      'FROM prev prv'
      'LEFT JOIN entidades ent ON ent.Codigo = prv.Empresa'
      'WHERE prv.Empresa=:P0')
    Left = 479
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrevTOT_BOL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT_BOL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrPrevTOT_PRE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT_PRE'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrPrevCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'prev.Codigo'
      Required = True
    end
    object QrPrevPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'prev.Periodo'
      Required = True
    end
    object QrPrevAviso01: TWideStringField
      FieldName = 'Aviso01'
      Origin = 'prev.Aviso01'
      Size = 50
    end
    object QrPrevAviso02: TWideStringField
      FieldName = 'Aviso02'
      Origin = 'prev.Aviso02'
      Size = 50
    end
    object QrPrevAviso03: TWideStringField
      FieldName = 'Aviso03'
      Origin = 'prev.Aviso03'
      Size = 50
    end
    object QrPrevAviso04: TWideStringField
      FieldName = 'Aviso04'
      Origin = 'prev.Aviso04'
      Size = 50
    end
    object QrPrevAviso05: TWideStringField
      FieldName = 'Aviso05'
      Origin = 'prev.Aviso05'
      Size = 50
    end
    object QrPrevAviso06: TWideStringField
      FieldName = 'Aviso06'
      Origin = 'prev.Aviso06'
      Size = 50
    end
    object QrPrevAviso07: TWideStringField
      FieldName = 'Aviso07'
      Origin = 'prev.Aviso07'
      Size = 50
    end
    object QrPrevAviso08: TWideStringField
      FieldName = 'Aviso08'
      Origin = 'prev.Aviso08'
      Size = 50
    end
    object QrPrevAviso09: TWideStringField
      FieldName = 'Aviso09'
      Origin = 'prev.Aviso09'
      Size = 50
    end
    object QrPrevAviso10: TWideStringField
      FieldName = 'Aviso10'
      Origin = 'prev.Aviso10'
      Size = 50
    end
    object QrPrevAvisoVerso: TWideStringField
      FieldName = 'AvisoVerso'
      Origin = 'prev.AvisoVerso'
      Size = 180
    end
    object QrPrevCompe: TSmallintField
      FieldName = 'Compe'
      Origin = 'prev.Compe'
      Required = True
    end
    object QrPrevEncerrado: TSmallintField
      FieldName = 'Encerrado'
      Origin = 'prev.Encerrado'
      Required = True
    end
    object QrPrevLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'prev.Lk'
    end
    object QrPrevDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'prev.DataCad'
    end
    object QrPrevDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'prev.DataAlt'
    end
    object QrPrevUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'prev.UserCad'
    end
    object QrPrevUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'prev.UserAlt'
    end
    object QrPrevAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'prev.AlterWeb'
      Required = True
    end
    object QrPrevEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'prev.Empresa'
      Required = True
    end
    object QrPrevAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'prev.Ativo'
      Required = True
    end
    object QrPrevPERIODO_TXT: TWideStringField
      DisplayWidth = 255
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Size = 255
      Calculated = True
    end
    object QrPrevNOME_ENCERRADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_ENCERRADO'
      Size = 15
      Calculated = True
    end
    object QrPrevNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrPrevModelBloq: TIntegerField
      FieldName = 'ModelBloq'
      Origin = 'prev.ModelBloq'
    end
    object QrPrevBloqFV: TIntegerField
      FieldName = 'BloqFV'
    end
  end
  object PmImprime: TPopupMenu
    OnPopup = PmImprimePopup
    Left = 318
    Top = 10
    object ImprimeBloqSel: TMenuItem
      Caption = 'Imprimir boletos &Selecionado(s)'
      OnClick = ImprimeBloqSelClick
    end
    object ImprimeBloqTodos: TMenuItem
      Caption = 'Imprimir &Todos boletos'
      OnClick = ImprimeBloqTodosClick
    end
  end
  object PMOrc: TPopupMenu
    OnPopup = PMOrcPopup
    Left = 206
    Top = 10
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object TravaDestr: TMenuItem
      Caption = 'Trava / Destr.'
      OnClick = TravaDestrClick
    end
  end
  object QrArre: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrArreAfterOpen
    BeforeClose = QrArreBeforeClose
    AfterScroll = QrArreAfterScroll
    SQL.Strings = (
      'SELECT ari.Vencto, ari.Entidade, SUM(ari.Valor) Valor, '
      'cna.Nome NOMECNAB_Cfg, CASE WHEN ent.Tipo=0 '
      'THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, '
      'ari.Boleto, ari.CNAB_Cfg'
      'FROM arreits ari'
      'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade'
      'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg'
      'WHERE ari.Codigo=:P0'
      'GROUP BY ari.CNAB_Cfg, ari.Entidade, ari.Vencto'
      'ORDER BY  NOMECNAB_Cfg, NOMEENT')
    Left = 535
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArreEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'arreits.Entidade'
    end
    object QrArreValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrArreNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Origin = 'NOMEENT'
      Size = 100
    end
    object QrArreCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
      Origin = 'arreits.CNAB_Cfg'
    end
    object QrArreNOMECNAB_Cfg: TWideStringField
      FieldName = 'NOMECNAB_Cfg'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrArreBoleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'arreits.Boleto'
    end
    object QrArreVencto: TDateField
      FieldName = 'Vencto'
    end
  end
  object DsArre: TDataSource
    DataSet = QrArre
    Left = 563
    Top = 10
  end
  object QrSumARRE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ari.Valor) Valor'
      'FROM arreits ari'
      'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade'
      'WHERE ari.Codigo=:P0')
    Left = 591
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumARREValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumARRE: TDataSource
    DataSet = QrSumARRE
    Left = 619
    Top = 10
  end
  object QrArreBol: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrArreBolBeforeClose
    AfterScroll = QrArreBolAfterScroll
    SQL.Strings = (
      'SELECT ari.Boleto'
      'FROM arreits ari'
      'WHERE ari.Codigo=:P0'
      'AND ari.Entidade=:P1'
      'AND ari.CNAB_Cfg=:P2'
      'AND ari.Vencto=:P3'
      'GROUP BY ari.CNAB_Cfg, ari.Entidade, ari.Boleto, ari.Vencto'
      'ORDER BY ari.Boleto')
    Left = 647
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrArreBolBoleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'arreits.Boleto'
    end
  end
  object DsArreBol: TDataSource
    DataSet = QrArreBol
    Left = 675
    Top = 10
  end
  object QrArreIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ari.*'
      'FROM arreits ari'
      'WHERE ari.Codigo=:P0'
      'AND ari.Entidade=:P1'
      'AND ari.CNAB_Cfg=:P2'
      'AND ari.Boleto=:P3'
      'AND ari.Vencto=:P4'
      'ORDER BY Valor DESC')
    Left = 703
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrArreItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'arreits.Codigo'
    end
    object QrArreItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'arreits.Controle'
    end
    object QrArreItsCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Origin = 'arreits.CartEmiss'
    end
    object QrArreItsCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
      Origin = 'arreits.CNAB_Cfg'
    end
    object QrArreItsConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'arreits.Conta'
    end
    object QrArreItsEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'arreits.Entidade'
    end
    object QrArreItsLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'arreits.Lancto'
    end
    object QrArreItsNum: TIntegerField
      FieldName = 'Num'
      Origin = 'arreits.Num'
    end
    object QrArreItsTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'arreits.Texto'
      Size = 50
    end
    object QrArreItsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'arreits.Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrArreItsVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'arreits.Vencto'
    end
    object QrArreItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'arreits.Lk'
    end
    object QrArreItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'arreits.DataCad'
    end
    object QrArreItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'arreits.DataAlt'
    end
    object QrArreItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'arreits.UserCad'
    end
    object QrArreItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'arreits.UserAlt'
    end
    object QrArreItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'arreits.AlterWeb'
    end
    object QrArreItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'arreits.Ativo'
    end
    object QrArreItsBloArre: TIntegerField
      FieldName = 'BloArre'
      Origin = 'arreits.BloArre'
    end
    object QrArreItsBloArreIts: TIntegerField
      FieldName = 'BloArreIts'
      Origin = 'arreits.BloArreIts'
    end
    object QrArreItsBoleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'arreits.Boleto'
    end
    object QrArreItsNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrArreItsNFSeSrvCad_TXT: TWideStringField
      FieldName = 'NFSeSrvCad_TXT'
      Size = 255
    end
  end
  object DsArreIts: TDataSource
    DataSet = QrArreIts
    Left = 731
    Top = 10
  end
  object QrBoletos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrBoletosAfterOpen
    BeforeClose = QrBoletosBeforeClose
    AfterScroll = QrBoletosAfterScroll
    OnCalcFields = QrBoletosCalcFields
    SQL.Strings = (
      'SELECT DISTINCT ari.Boleto, ari.Entidade, ari.Vencto, '
      'ptc.Nome NOMEPROT, pt2.Nome NOMEPROT2, pt3.Nome NOMEPROT3,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'ENT, '
      
        'SUM(ari.Valor) Valor, CONCAT_WS("-", ari.Boleto, ari.Entidade) B' +
        'OLENT, '
      
        '0 KGT, ari.CartEmiss, car.TipoDoc CAR_TIPODOC, car.Ativo CART_AT' +
        'IVO,'
      
        'cna.CedBanco, cna.JurosPerc, cna.MultaPerc, ari.CNAB_Cfg, pre.Em' +
        'presa, '
      
        'ari.Conta Genero, ari.Texto, ari.Controle, cna.Nome NOMECNAB_Cfg' +
        ','
      'ari.Protocolo, ari.Codigo, ari.Avulso'
      'FROM arreits ari'
      'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade'
      'LEFT JOIN carteiras car ON car.Codigo=ari.CartEmiss'
      'LEFT JOIN cnab_cfg cna ON cna.Codigo = ari.CNAB_Cfg'
      'LEFT JOIN prev pre ON pre.Codigo = ari.Codigo '
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ari.Protocolo'
      'LEFT JOIN protocolos pt2 ON pt2.Codigo=ari.Protocolo'
      'LEFT JOIN protocolos pt3 ON pt3.Codigo=ari.Protocolo'
      'WHERE ari.Codigo=:P0'
      'AND ari.Boleto=:P1'
      'GROUP BY ari.CNAB_Cfg, ari.Boleto'
      'ORDER BY NOMECNAB_Cfg, ari.Boleto, NOMEENT')
    Left = 759
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBoletosEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'arreits.Entidade'
    end
    object QrBoletosVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'arreits.Vencto'
    end
    object QrBoletosNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrBoletosBOLENT: TWideStringField
      FieldName = 'BOLENT'
      Required = True
      Size = 65
    end
    object QrBoletosKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrBoletosVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Calculated = True
    end
    object QrBoletosValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBoletosCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Origin = 'arreits.CartEmiss'
    end
    object QrBoletosCAR_TIPODOC: TSmallintField
      FieldName = 'CAR_TIPODOC'
      Origin = 'carteiras.TipoDoc'
    end
    object QrBoletosCART_ATIVO: TSmallintField
      FieldName = 'CART_ATIVO'
      Origin = 'carteiras.Ativo'
    end
    object QrBoletosCedBanco: TIntegerField
      FieldName = 'CedBanco'
      Origin = 'cnab_cfg.CedBanco'
    end
    object QrBoletosJurosPerc: TFloatField
      FieldName = 'JurosPerc'
      Origin = 'cnab_cfg.JurosPerc'
    end
    object QrBoletosMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'cnab_cfg.MultaPerc'
    end
    object QrBoletosCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
      Origin = 'arreits.CNAB_Cfg'
    end
    object QrBoletosEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'prev.Empresa'
    end
    object QrBoletosTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'arreits.Texto'
      Size = 50
    end
    object QrBoletosControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'arreits.Controle'
    end
    object QrBoletosNOMECNAB_Cfg: TWideStringField
      FieldName = 'NOMECNAB_Cfg'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrBoletosBoleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'arreits.Boleto'
    end
    object QrBoletosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBoletosAvulso: TSmallintField
      FieldName = 'Avulso'
      MaxValue = 1
    end
    object QrBoletosPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBoletosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrBoletosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBoletosModelBloq: TIntegerField
      FieldName = 'ModelBloq'
    end
    object QrBoletosCompe: TSmallintField
      FieldName = 'Compe'
    end
    object QrBoletosBloqFV: TIntegerField
      FieldName = 'BloqFV'
    end
    object QrBoletosAviso01: TWideStringField
      FieldName = 'Aviso01'
      Origin = 'prev.Aviso01'
      Size = 50
    end
    object QrBoletosAviso02: TWideStringField
      FieldName = 'Aviso02'
      Origin = 'prev.Aviso02'
      Size = 50
    end
    object QrBoletosAviso03: TWideStringField
      FieldName = 'Aviso03'
      Origin = 'prev.Aviso03'
      Size = 50
    end
    object QrBoletosAviso04: TWideStringField
      FieldName = 'Aviso04'
      Origin = 'prev.Aviso04'
      Size = 50
    end
    object QrBoletosAviso05: TWideStringField
      FieldName = 'Aviso05'
      Origin = 'prev.Aviso05'
      Size = 50
    end
    object QrBoletosAviso06: TWideStringField
      FieldName = 'Aviso06'
      Origin = 'prev.Aviso06'
      Size = 50
    end
    object QrBoletosAviso07: TWideStringField
      FieldName = 'Aviso07'
      Origin = 'prev.Aviso07'
      Size = 50
    end
    object QrBoletosAviso08: TWideStringField
      FieldName = 'Aviso08'
      Origin = 'prev.Aviso08'
      Size = 50
    end
    object QrBoletosAviso09: TWideStringField
      FieldName = 'Aviso09'
      Origin = 'prev.Aviso09'
      Size = 50
    end
    object QrBoletosAviso10: TWideStringField
      FieldName = 'Aviso10'
      Origin = 'prev.Aviso10'
      Size = 50
    end
    object QrBoletosAvisoVerso: TWideStringField
      FieldName = 'AvisoVerso'
      Origin = 'prev.AvisoVerso'
      Size = 180
    end
    object QrBoletosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBoletosTotIts: TFloatField
      FieldName = 'TotIts'
    end
    object QrBoletosCedente: TIntegerField
      FieldName = 'Cedente'
    end
    object QrBoletosEnvios: TFloatField
      FieldName = 'Envios'
    end
    object QrBoletosGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object DsBoletos: TDataSource
    DataSet = QrBoletos
    Left = 787
    Top = 10
  end
  object QrBoletosIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ari.Texto TEXTO, ari.Valor VALOR, '
      'ari.Entidade, ari.Vencto, ari.Controle, ari.Lancto, '
      'ari.CNAB_Cfg, ari.Avulso, ari.Boleto, ari.FatNum,'
      'ari.ProtocoPak, ari.ProtocoPakCR'
      'FROM arreits ari'
      'WHERE ari.Boleto=:P0'
      'AND ari.Entidade=:P1'
      'AND ari.CNAB_Cfg=:P2'
      'AND ari.Codigo=:P3'
      'AND ari.Vencto=:P4'
      'ORDER BY VALOR DESC')
    Left = 844
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrBoletosItsTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Size = 50
    end
    object QrBoletosItsVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBoletosItsVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrBoletosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrBoletosItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrBoletosItsCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrBoletosItsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrBoletosItsAvulso: TIntegerField
      FieldName = 'Avulso'
    end
    object QrBoletosItsBoleto: TFloatField
      FieldName = 'Boleto'
    end
    object QrBoletosItsFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBoletosItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBoletosItsSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 5
    end
    object QrBoletosItsNotaFiscal: TFloatField
      FieldName = 'NotaFiscal'
    end
    object QrBoletosItsNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrBoletosItsBloArreIts: TIntegerField
      FieldName = 'BloArreIts'
    end
  end
  object DsBoletosIts: TDataSource
    DataSet = QrBoletosIts
    Left = 873
    Top = 10
  end
  object frxDsBoletosIts: TfrxDBDataset
    UserName = 'frxDsBoletosIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TEXTO=Texto'
      'VALOR=Valor'
      'Vencto=Vencto'
      'Controle=Controle'
      'Lancto=Lancto'
      'CNAB_Cfg=CNAB_Cfg'
      'Entidade=Entidade'
      'Avulso=Avulso'
      'Boleto=Boleto'
      'FatNum=FatNum'
      'Genero=Genero'
      'SerieNF=SerieNF'
      'NotaFiscal=NotaFiscal'
      'NFSeSrvCad=NFSeSrvCad'
      'BloArreIts=BloArreIts'
      'Cancelado=Cancelado')
    DataSet = QrBoletosIts
    BCDToCurrency = False
    Left = 902
    Top = 10
  end
  object frxDsBoletos: TfrxDBDataset
    UserName = 'frxDsBoletos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Entidade=Entidade'
      'Vencto=Vencto'
      'NOMEENT=NOMEENT'
      'BOLENT=BOLENT'
      'KGT=KGT'
      'VENCTO_TXT=VENCTO_TXT'
      'Valor=Valor'
      'CartEmiss=CartEmiss'
      'CAR_TIPODOC=CAR_TIPODOC'
      'CART_ATIVO=CART_ATIVO'
      'CedBanco=CedBanco'
      'JurosPerc=JurosPerc'
      'MultaPerc=MultaPerc'
      'CNAB_Cfg=CNAB_Cfg'
      'Empresa=Empresa'
      'Texto=Texto'
      'Controle=Controle'
      'NOMECNAB_Cfg=NOMECNAB_Cfg'
      'Boleto=Boleto'
      'Codigo=Codigo'
      'Avulso=Avulso'
      'Periodo=Periodo'
      'FatID=FatID'
      'FatNum=FatNum'
      'ModelBloq=ModelBloq'
      'Compe=Compe'
      'BloqFV=BloqFV'
      'Aviso01=Aviso01'
      'Aviso02=Aviso02'
      'Aviso03=Aviso03'
      'Aviso04=Aviso04'
      'Aviso05=Aviso05'
      'Aviso06=Aviso06'
      'Aviso07=Aviso07'
      'Aviso08=Aviso08'
      'Aviso09=Aviso09'
      'Aviso10=Aviso10'
      'AvisoVerso=AvisoVerso'
      'DataCad=DataCad'
      'TotIts=TotIts'
      'Cedente=Cedente'
      'Envios=Envios'
      'Genero=Genero')
    DataSet = QrBoletos
    BCDToCurrency = False
    Left = 816
    Top = 10
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 420
    Top = 11
  end
  object PMBoleto2: TPopupMenu
    OnPopup = PMBoleto2Popup
    Left = 290
    Top = 38
    object Gerartodooperodo2: TMenuItem
      Caption = '&Gerar per'#237'odo'
      OnClick = Gerartodooperodo2Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Desfazerboletosselecionados1: TMenuItem
      Caption = '&Desfazer boletos (selecionados)'
      OnClick = Desfazerboletosselecionados1Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Modificarlayoutdobloqueto1: TMenuItem
      Caption = '&Modificar layout do boleto'
      OnClick = Modificarlayoutdobloqueto1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object EditarprotocolodeentregaSelecionados1: TMenuItem
      Caption = '&Editar protocolo de entrega (selecionados)'
      OnClick = EditarprotocolodeentregaSelecionados1Click
    end
    object Gerenciar2: TMenuItem
      Caption = 'Gerenciar &protocolo de entrega para entidade atual'
      OnClick = Gerenciar2Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object AlteraVencimentoSelecionados1: TMenuItem
      Caption = 'Altera &vencimento (selecionados)'
      OnClick = AlteraVencimentoSelecionados1Click
    end
    object Alteravalor1: TMenuItem
      Caption = '&Altera valor (da arrecada'#231#227'o atual)'
      OnClick = Alteravalor1Click
    end
    object Alteratextodaarrecadaoatual1: TMenuItem
      Caption = 'Altera &texto (da arrecada'#231#227'o atual)'
      OnClick = Alteratextodaarrecadaoatual1Click
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object GerarNFSeparaosboletosselecionados2: TMenuItem
      Caption = 'Gerar &NFS-e para o(s) boleto(s) selecionado(s)'
      OnClick = GerarNFSeparaosboletosselecionados2Click
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 448
    Top = 11
  end
  object QrProt1: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrProt1BeforeClose
    AfterScroll = QrProt1AfterScroll
    SQL.Strings = (
      'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq'
      'FROM protocolos pro'
      'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio'
      'WHERE pro.Tipo=1'
      'AND pro.def_client=:P0')
    Left = 478
    Top = 38
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProt1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProt1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrProt1PreEmeio: TIntegerField
      FieldName = 'PreEmeio'
    end
    object QrProt1NaoEnvBloq: TIntegerField
      FieldName = 'NaoEnvBloq'
    end
  end
  object DsProt1: TDataSource
    DataSet = QrProt1
    Left = 506
    Top = 38
  end
  object PMProtocolo: TPopupMenu
    OnPopup = PMProtocoloPopup
    Left = 290
    Top = 66
    object Gera1: TMenuItem
      Caption = '&Gera protocolo'
      object Todosabertos1: TMenuItem
        Caption = '&Todos abertos'
        OnClick = Todosabertos1Click
      end
      object Abertosseleciondos1: TMenuItem
        Caption = '&Aberto(s) seleciondo(s)'
        OnClick = Abertosseleciondos1Click
      end
    end
    object Desfazprotocolo1: TMenuItem
      Caption = '&Desfaz protocolo(s) selecionado(s)'
      OnClick = Desfazprotocolo1Click
    end
    object MenuItem1: TMenuItem
      Caption = '-'
    end
    object Localizaprotocolo1: TMenuItem
      Caption = '&Localiza lote'
      OnClick = Localizaprotocolo1Click
    end
  end
  object QrLeiBol: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLeiBolCalcFields
    SQL.Strings = (
      'SELECT cfg.ModalCobr, cfg.CNAB, lei.* '
      'FROM cnab_lei lei'
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=lei.Diretorio'
      'WHERE lei.ID_Link>0'
      '')
    Left = 192
    Top = 568
    object QrLeiBolID_Link: TLargeintField
      FieldName = 'ID_Link'
    end
    object QrLeiBolOcorrCodi: TWideStringField
      FieldName = 'OcorrCodi'
      Size = 10
    end
    object QrLeiBolOcorrData: TDateField
      FieldName = 'OcorrData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiBolValTitul: TFloatField
      FieldName = 'ValTitul'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLeiBolValPago: TFloatField
      FieldName = 'ValPago'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLeiBolValJuros: TFloatField
      FieldName = 'ValJuros'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLeiBolValMulta: TFloatField
      FieldName = 'ValMulta'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLeiBolValJuMul: TFloatField
      FieldName = 'ValJuMul'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLeiBolMotivo1: TWideStringField
      FieldName = 'Motivo1'
      Size = 2
    end
    object QrLeiBolMotivo2: TWideStringField
      FieldName = 'Motivo2'
      Size = 2
    end
    object QrLeiBolMotivo3: TWideStringField
      FieldName = 'Motivo3'
      Size = 2
    end
    object QrLeiBolMotivo4: TWideStringField
      FieldName = 'Motivo4'
      Size = 2
    end
    object QrLeiBolMotivo5: TWideStringField
      FieldName = 'Motivo5'
      Size = 2
    end
    object QrLeiBolValTarif: TFloatField
      FieldName = 'ValTarif'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLeiBolDtaTarif: TDateField
      FieldName = 'DtaTarif'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiBolEhLiquida: TSmallintField
      FieldName = 'EhLiquida'
    end
    object QrLeiBolOCOR_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'OCOR_TXT'
      Size = 255
      Calculated = True
    end
    object QrLeiBolTamReg: TIntegerField
      FieldName = 'TamReg'
    end
    object QrLeiBolDiretorio: TIntegerField
      FieldName = 'Diretorio'
    end
    object QrLeiBolModalCobr: TSmallintField
      FieldName = 'ModalCobr'
    end
    object QrLeiBolBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLeiBolCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrLeiBolLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
  end
  object DsLeiBol: TDataSource
    DataSet = QrLeiBol
    Left = 221
    Top = 568
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ari.Texto TEXTO, ari.Valor VALOR, '
      'ari.Entidade, ari.Vencto, ari.Controle, ari.Lancto, '
      'ari.CNAB_Cfg, ari.Avulso, ari.Boleto, ari.FatNum,'
      'ari.ProtocoPak, ari.ProtocoPakCR'
      'FROM arreits ari'
      'WHERE ari.Boleto=:P0'
      'AND ari.Entidade=:P1'
      'AND ari.CNAB_Cfg=:P2'
      'AND ari.Codigo=:P3'
      'AND ari.Vencto=:P4'
      'ORDER BY VALOR DESC')
    Left = 364
    Top = 442
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
  end
  object frxPDFExport: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'Dermatek'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 63
    Top = 423
  end
  object QrArreFutI: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrArreFutICalcFields
    Left = 508
    Top = 332
    object QrArreFutIControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'arrefut.Controle'
      Required = True
    end
    object QrArreFutIConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'arrefut.Conta'
      Required = True
    end
    object QrArreFutIPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'arrefut.Periodo'
      Required = True
    end
    object QrArreFutIValor: TFloatField
      FieldName = 'Valor'
      Origin = 'arrefut.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrArreFutITexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'arrefut.Texto'
      Required = True
      Size = 50
    end
    object QrArreFutILk: TIntegerField
      FieldName = 'Lk'
      Origin = 'arrefut.Lk'
    end
    object QrArreFutIDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'arrefut.DataCad'
    end
    object QrArreFutIDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'arrefut.DataAlt'
    end
    object QrArreFutIUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'arrefut.UserCad'
    end
    object QrArreFutIUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'arrefut.UserAlt'
    end
    object QrArreFutINOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrArreFutIPERIODO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Size = 8
      Calculated = True
    end
    object QrArreFutICNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrArreFutIDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrArreFutICliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrArreFutINOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrArreFutINFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrArreFutINFSeSrvCad_TXT: TWideStringField
      FieldName = 'NFSeSrvCad_TXT'
      Size = 255
    end
  end
  object DsArreFutI: TDataSource
    DataSet = QrArreFutI
    Left = 537
    Top = 332
  end
  object QrArreFutA: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrArreFutACalcFields
    Left = 565
    Top = 332
    object QrArreFutANOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrArreFutAControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'arrefut.Controle'
      Required = True
    end
    object QrArreFutAConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'arrefut.Conta'
      Required = True
    end
    object QrArreFutAPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'arrefut.Periodo'
      Required = True
    end
    object QrArreFutAValor: TFloatField
      FieldName = 'Valor'
      Origin = 'arrefut.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrArreFutATexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'arrefut.Texto'
      Required = True
      Size = 50
    end
    object QrArreFutALk: TIntegerField
      FieldName = 'Lk'
      Origin = 'arrefut.Lk'
    end
    object QrArreFutADataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'arrefut.DataCad'
    end
    object QrArreFutADataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'arrefut.DataAlt'
    end
    object QrArreFutAUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'arrefut.UserCad'
    end
    object QrArreFutAUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'arrefut.UserAlt'
    end
    object QrArreFutAPERIODO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Size = 8
      Calculated = True
    end
    object QrArreFutAInclui: TIntegerField
      FieldName = 'Inclui'
      Origin = 'arrefut.Inclui'
      Required = True
    end
    object QrArreFutACNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrArreFutADepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrArreFutANOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrArreFutANFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrArreFutACartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
  end
  object DsArreFutA: TDataSource
    DataSet = QrArreFutA
    Left = 593
    Top = 332
  end
  object PMArreFut: TPopupMenu
    OnPopup = PMArreFutPopup
    Left = 235
    Top = 39
    object Incluiarrecadaofutura1: TMenuItem
      Caption = '&Inclui arrecada'#231#227'o futura'
      OnClick = Incluiarrecadaofutura1Click
    end
    object Alteraarrecadaofutura1: TMenuItem
      Caption = '&Altera arrecada'#231#227'o futura'
      OnClick = Alteraarrecadaofutura1Click
    end
    object Excluiarrecadaofutura1: TMenuItem
      Caption = '&Exclui arrecada'#231#227'o futura'
      OnClick = Excluiarrecadaofutura1Click
    end
  end
end
