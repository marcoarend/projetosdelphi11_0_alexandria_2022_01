object FmBloGerenModelCad: TFmBloGerenModelCad
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Selecione o modelo'
  ClientHeight = 83
  ClientWidth = 194
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object RGModelo: TRadioGroup
    Left = 0
    Top = 0
    Width = 194
    Height = 83
    Align = alClient
    Columns = 2
    Items.Strings = (
      'Janela'
      'Aba')
    TabOrder = 0
    OnClick = RGModeloClick
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 64
    Top = 11
  end
end
