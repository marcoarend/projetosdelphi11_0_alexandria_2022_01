unit BloGerenInadimpBloq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkEdit, unDmkProcFunc,
  ComCtrls, dmkEditDateTimePicker, dmkDBGrid, DB, mySQLDbTables, frxClass,
  frxDBSet, dmkGeral, dmkDBGridDAC, dmkImage, dmkPermissoes, UnDmkEnums;

type
  TFmBloGerenInadimpBloq = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    TPVencto: TdmkEditDateTimePicker;
    EdMulta: TdmkEdit;
    EdJuros: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBGBloq: TdmkDBGridDAC;
    QrBloqInad: TmySQLQuery;
    DsBloqInad: TDataSource;
    frxCondE2: TfrxReport;
    frxDsBoletos: TfrxDBDataset;
    QrBoletos: TmySQLQuery;
    QrBoletosSUB_TOT: TFloatField;
    QrBoletosVencto: TDateField;
    QrBoletosBoleto: TFloatField;
    frxBloq: TfrxReport;
    CkZerado: TCheckBox;
    CkDesign: TCheckBox;
    QrBoletosBLOQUETO: TFloatField;
    LaAviso: TLabel;
    QrBoletosMEZ_TXT: TWideStringField;
    Splitter1: TSplitter;
    dmkDBGrid1: TdmkDBGrid;
    QrBloqIts: TmySQLQuery;
    QrBloqItsGenero: TIntegerField;
    QrBloqItsDescricao: TWideStringField;
    QrBloqItsCredito: TFloatField;
    DsBloqIts: TDataSource;
    frxDsBoletosIts: TfrxDBDataset;
    QrBoletosMez: TIntegerField;
    QrBoletosFatNum: TFloatField;
    QrBoletosVencimento: TDateField;
    QrBloqInadData: TDateField;
    QrBloqInadCREDITO: TFloatField;
    QrBloqInadPAGO: TFloatField;
    QrBloqInadSALDO: TFloatField;
    QrBloqInadMez: TIntegerField;
    QrBloqInadVencimento: TDateField;
    QrBloqInadCompensado: TDateField;
    QrBloqInadControle: TIntegerField;
    QrBloqInadDescricao: TWideStringField;
    QrBloqInadJuros: TFloatField;
    QrBloqInadMulta: TFloatField;
    QrBloqInadTOTAL: TFloatField;
    QrBloqInadFatNum: TFloatField;
    QrBloqInadPEND_VAL: TFloatField;
    QrBloqInadMEZ_TXT: TWideStringField;
    QrBloqInadVCTO_TXT: TWideStringField;
    QrBloqInadCliInt: TIntegerField;
    QrBloqInadNOMEEMP: TWideStringField;
    QrBloqInadNOMEENT: TWideStringField;
    QrBloqInadCliente: TIntegerField;
    QrBoletosIts: TmySQLQuery;
    QrBoletosItsGenero: TIntegerField;
    QrBoletosItsDescricao: TWideStringField;
    QrBoletosItsCredito: TFloatField;
    QrBloqInadAtivo: TSmallintField;
    QrBoletosCliente: TIntegerField;
    QrCNABCfg: TmySQLQuery;
    QrCNABCfgCedBanco: TIntegerField;
    QrCNABCfgCedConta: TWideStringField;
    QrCNABCfgMoedaCod: TWideStringField;
    QrCNABCfgCedAgencia: TIntegerField;
    QrCNABCfgCedPosto: TIntegerField;
    QrCNABCfgCartNum: TWideStringField;
    QrCNABCfgIDCobranca: TWideStringField;
    QrCNABCfgCodEmprBco: TWideStringField;
    QrCNABCfgTipoCobranca: TIntegerField;
    QrCNABCfgTipoCart: TSmallintField;
    QrCNABCfgCedDAC_A: TWideStringField;
    QrCNABCfgCedDAC_C: TWideStringField;
    QrCNABCfgOperCodi: TWideStringField;
    QrCNABCfgDVB: TWideStringField;
    QrCNABCfgTexto01: TWideStringField;
    QrCNABCfgTexto02: TWideStringField;
    QrCNABCfgTexto03: TWideStringField;
    QrCNABCfgTexto04: TWideStringField;
    QrCNABCfgTexto05: TWideStringField;
    QrCNABCfgTexto06: TWideStringField;
    QrCNABCfgTexto07: TWideStringField;
    QrCNABCfgTexto08: TWideStringField;
    QrCNABCfgJurosPerc: TFloatField;
    QrCNABCfgMultaPerc: TFloatField;
    QrBoletosControle: TIntegerField;
    frxDsCNABCfg: TfrxDBDataset;
    QrCNABCfgNOMEBANCO: TWideStringField;
    QrCNABCfgLocalPag: TWideStringField;
    QrCNABCfgNOMECED_IMP: TWideStringField;
    QrCNABCfgEspecieTit: TWideStringField;
    QrCNABCfgAceiteTit: TSmallintField;
    QrCNABCfgACEITETIT_TXT: TWideStringField;
    QrCNABCfgEspecieVal: TWideStringField;
    QrCNABCfgEspecieDoc: TWideStringField;
    QrCNABCfgCNAB: TIntegerField;
    QrCNABCfgCtaCooper: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtRecalcula: TBitBtn;
    BtImprime: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrCNABCfgLayoutRem: TWideStringField;
    QrCNABCfgModalCobr: TIntegerField;
    QrBloqInadNewVencto: TDateField;
    QrCNABCfgCorresBco: TIntegerField;
    QrCNABCfgCorresAge: TIntegerField;
    QrCNABCfgCorresCto: TWideStringField;
    QrCNABCfgNPrinBc: TWideStringField;
    QrCNABCfgCartCod: TWideStringField;
    QrBloOpcoes: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TPVenctoChange(Sender: TObject);
    procedure TPVenctoClick(Sender: TObject);
    procedure EdMultaChange(Sender: TObject);
    procedure EdJurosChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtRecalculaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrBoletosAfterScroll(DataSet: TDataSet);
    procedure frxCondE2GetValue(const VarName: string; var Value: Variant);
    procedure QrBoletosCalcFields(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure QrBloqInadBeforeClose(DataSet: TDataSet);
    procedure QrBloqInadAfterScroll(DataSet: TDataSet);
    procedure QrCNABCfgCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    DesativaScrool: Boolean;
    FNossoNumero: String;
    FLoteImp: Integer;
    procedure FechaPesquisa();
    function  ImprimeBoletos(Quais: TselType; Como: TfrxImpComo;
              Arquivo: String; Filtro: TfrxCustomExportFilter;
              PrevCod: Integer; Antigo: Boolean; var SdoJaAtz: Boolean): TfrxReport;
    function  frxMostra(PrevCod: Integer; Titulo: string; Tipo: TselType;
              Antigo: Boolean): Boolean;
    function  frxComposite(PrevCod, Apto: Integer; Titulo: string;
              var ModelBloqAnt, ConfigAnt: Integer; ClearLastReport, Antigo: Boolean):
              Boolean;
    procedure AtualizaTodos(Status: Integer);
    procedure ReopenBloqInad(Controle: Integer);
  public
    { Public declarations }
    FTabLctA: String;
  end;

  var
  FmBloGerenInadimpBloq: TFmBloGerenInadimpBloq;

implementation

uses ModuleGeral, UMySQLModule, UnBancos, MyGlyfs, MeuFrx, UnInternalConsts,
  UnMyObjects, Module, ModuleBco, UnBloquetos;

{$R *.DFM}

procedure TFmBloGerenInadimpBloq.AtualizaTodos(Status: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE bloqinadcli SET Ativo=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Status;
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenBloqInad(QrBloqInadControle.Value);
end;

procedure TFmBloGerenInadimpBloq.BtImprimeClick(Sender: TObject);
var
  SdoJaAtz: Boolean;
begin
  QrBoletos.Close;
  QrBoletos.Open;
  if QrBoletos.RecordCount = 0 then
  begin
    Geral.MB_Aviso('N�o foi selecionado nenhum boleto para ser impresso!');
    Exit;
  end;
  //
  (*
  SdoJaAtz := False;
  ImprimeBoletos(istTodos, ficMostra, '', nil, 0, False, SdoJaAtz);
  //
  *)
  UBloquetos.ImprimeBloqueto(CkZerado.Checked, FTabLctA, FNossoNumero,
    Dmod.QrControle, QrBoletos, QrBoletosIts, QrCNABCfg, QrBloOpcoes, frxDsBoletos,
    frxDsBoletosIts, frxDsCNABCfg, istTodos, ficMostra, '', nil, TDBGrid(DBGBloq));
end;

procedure TFmBloGerenInadimpBloq.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmBloGerenInadimpBloq.BtRecalculaClick(Sender: TObject);
var
  NewVencto: TDateTime;
  TOTAL, PEND_VAL, TaxaJ, Juros, Multa, ValJuros, ValMulta, Dias: Double;
  Controle: Integer;
begin
  try
    Screen.Cursor  := crHourGlass;
    DesativaScrool := True;
    //
    if QrBloqInad.State = dsBrowse then
      Controle := QrBloqInadControle.Value
    else
      Controle := 0;
    //
    ReopenBloqInad(0);
    //
    DBGBloq.Visible := True;
    NewVencto       := Trunc(TPVencto.Date);
    Multa           := EdMulta.ValueVariant;
    Juros           := EdJuros.ValueVariant;
    //
    QrBloqInad.First;
    //
    while not QrBloqInad.Eof do
    begin
      Dias     := NewVencto - QrBloqInadVencimento.Value;
      TaxaJ    := dmkPF.CalculaJuroSimples(Juros, Dias);
      ValMulta := Round(QrBloqInadCREDITO.Value * Multa) / 100;
      ValJuros := Round(QrBloqInadCREDITO.Value * TaxaJ) / 100;
      TOTAL    := QrBloqInadCREDITO.Value + ValMulta + ValJuros;
      PEND_VAL := TOTAL - QrBloqInadPago.Value;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, 'bloqinadcli', False, [
        'Juros', 'Multa', 'TOTAL', 'PEND_VAL', 'NewVencto'], ['Controle', 'FatNum'],
        [ValJuros, ValMulta, TOTAL, PEND_VAL, NewVencto], [
        QrBloqInadControle.Value, QrBloqInadFatNum.Value], False);
      //
      QrBloqInad.Next;
    end;
  finally
    DesativaScrool := False;
    //
    ReopenBloqInad(Controle);
    //
    BtImprime.Enabled := QrBloqInad.RecordCount > 0;
    Screen.Cursor     := crDefault;
  end;
end;

procedure TFmBloGerenInadimpBloq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloGerenInadimpBloq.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmBloGerenInadimpBloq.EdJurosChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmBloGerenInadimpBloq.EdMultaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmBloGerenInadimpBloq.FechaPesquisa;
begin
  BtImprime.Enabled := False;
  QrBloqInad.Close;
  DBGBloq.Visible := False;
end;

procedure TFmBloGerenInadimpBloq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmBloGerenInadimpBloq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DesativaScrool  := False;
  //
  UMyMod.AbreQuery(QrBloOpcoes, Dmod.MyDB);
  //
  FLoteImp               := -1000000000;
  TPVencto.Date          := UMyMod.CalculaDataDeposito(Date + 1);
  QrBloqInad.Database    := DModG.MyPID_DB;
  QrBoletos.Database     := DModG.MyPID_DB;
  //Fazer frxDsInquilino.DataSet := DmBloq.QrInquilino;
end;

procedure TFmBloGerenInadimpBloq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmBloGerenInadimpBloq.frxComposite(PrevCod, Apto: Integer; Titulo: string;
  var ModelBloqAnt, ConfigAnt: Integer; ClearLastReport,
  Antigo: Boolean): Boolean;
var
  s: TMemoryStream;
  frxAtu: TfrxReport;
begin
  Result := False;
  //
  frxAtu := frxCondE2;
  try
    if frxAtu <> nil then
    begin
      frxBloq.OnGetValue := frxAtu.OnGetValue;
      frxBloq.OnUserFunction := frxAtu.OnUserFunction;
      frxBloq.ReportOptions.Name := Titulo;
      //  mudado de frxBloq para frxAtu
      frxAtu.Variables['MeuLogoExiste'] := VAR_MEULOGOEXISTE;
      frxAtu.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
      frxAtu.Variables['LogoNFExiste'] := VAR_LOGONFEXISTE;
      frxAtu.Variables['LogoNFCaminho'] := QuotedStr(VAR_LOGONFPATH);
      //
      frxAtu.Variables['LogoBancoExiste'] := True;//FileExists(QrCNABCfgCedBanco.Value);
      frxAtu.Variables['LogoBancoPath'] := QuotedStr(FmMyGlyfs.CaminhoLogoBanco(QrCNABCfgCedBanco.Value));
      //
      frxAtu.Variables['LogoEmpresaExiste'] := True;//FileExists(Dmod.QrControle.FieldByName('MeuLogoPath').AsString);
      frxAtu.Variables['LogoEmpresaPath'] := QuotedStr(Dmod.QrControle.FieldByName('MeuLogoPath').AsString);
      //  FIM mudado de frxBloq para frxAtu


      // agregar frx selecionado ao frxBloq (frx virtual que est� agrupando v�rios frx para imprimir tudo junto)
      s := TMemoryStream.Create;
      frxAtu.SaveToStream(s);
      s.Position := 0;
      frxBloq.LoadFromStream(s);
      //#$%
      frxBloq.PrepareReport(ClearLastReport);
      //
      Result := True;
    end else
      Geral.MB_Erro('ERRO. "frxCond?" n�o definido.');
  except
    raise;
  end;
end;

procedure TFmBloGerenInadimpBloq.frxCondE2GetValue(const VarName: string;
  var Value: Variant);
var
  DVB, LocalEData, UH, NossoNumero_Rem, DAC_NossoNum: String;
  V, M, J, C, P: Double;
  ModelBloq, Banco: Integer;
begin
  P := 0;
  C := 0;
  V := QrBoletosSUB_TOT.Value;
  M := QrCNABCfgMultaPerc.Value;
  J := QrCNABCfgJurosPerc.Value;
  //
  // In�cio da Ficha de compensa��o
  if AnsiCompareText(VarName, 'VARF_AGCodCed') = 0 then
    Value := QrCNABCfgCedConta.Value
  else if AnsiCompareText(VarName, 'VARF_NossoNumero') = 0 then
  begin
    UBancos.GeraNossoNumero(QrCNABCfgTipoCart.Value,
      QrCNABCfgCedBanco.Value, QrCNABCfgCedAgencia.Value,
      QrCNABCfgCedPosto.Value, QrBoletosBLOQUETO.Value,
      QrCNABCfgCedConta.Value, QrCNABCfgCartNum.Value,
      QrCNABCfgIDCobranca.Value,
      Geral.SoNumero_TT(QrCNABCfgCodEmprBco.Value),
      QrBoletosVencto.Value, QrCNABCfgTipoCobranca.Value,
      QrCNABCfgEspecieDoc.Value, QrCNABCfgCNAB.Value,
      QrCNABCfgCtaCooper.Value, QrCNABCfgLayoutRem.Value,
      FNossoNumero, NossoNumero_Rem);

    UBancos.DACNossoNumero(QrCNABCfgCedBanco.Value,
      QrCNABCfgCedAgencia.Value, QrCNABCfgCedPosto.Value,
      QrBoletosBLOQUETO.Value, QrCNABCfgCedConta.Value,
      QrCNABCfgCartCod.Value, QrCNABCfgCartNum.Value,
      Geral.SoNumero_TT(QrCNABCfgCodEmprBco.Value),
      QrCNABCfgTipoCobranca.Value, QrCNABCfgEspecieDoc.Value,
      QrCNABCfgCNAB.Value, QrCNABCfgCtaCooper.Value,
      QrCNABCfgLayoutRem.Value, DAC_NossoNum);

    if QrCNABCfgLayoutRem.Value = CO_756_CORRESPONDENTE_BRADESCO_2015 then
      Value := FNossoNumero + DAC_NossoNum
    else
      Value := FNossoNumero;
  end else
   if AnsiCompareText(VarName, 'VARF_CODIGOBARRAS') = 0 then
  begin
    UBancos.GeraNossoNumero(QrCNABCfgTipoCart.Value,
      QrCNABCfgCedBanco.Value, QrCNABCfgCedAgencia.Value,
      QrCNABCfgCedPosto.Value, QrBoletosBLOQUETO.Value,
      QrCNABCfgCedConta.Value, QrCNABCfgCartNum.Value,
      QrCNABCfgIDCobranca.Value,
      Geral.SoNumero_TT(QrCNABCfgCodEmprBco.Value),
      QrBoletosVencto.Value, QrCNABCfgTipoCobranca.Value,
      QrCNABCfgEspecieDoc.Value, QrCNABCfgCNAB.Value,
      QrCNABCfgCtaCooper.Value, QrCNABCfgLayoutRem.Value,
      FNossoNumero, NossoNumero_Rem);
    //
    Value := UBancos.CodigoDeBarra_BoletoDeCobranca(QrCNABCfgCedBanco.Value,
      QrCNABCfgCedAgencia.Value, QrCNABCfgCorresBco.Value, QrCNABCfgCorresAge.Value,
      QrCNABCfgCedDAC_A.Value, QrCNABCfgCedPosto.Value, QrCNABCfgCedConta.Value,
      QrCNABCfgCedDAC_C.Value, QrCNABCfgCorresCto.Value, 9, 3, 1, FNossoNumero,
      QrCNABCfgCodEmprBco.Value, QrCNABCfgCartNum.Value, QrCNABCfgIDCobranca.Value,
      QrCNABCfgOperCodi.Value,
      QrBoletosVencto.Value, QrBoletosSUB_TOT.Value, 0,
      0, not CkZerado.Checked, QrCNABCfgModalCobr.Value, QrCNABCfgLayoutRem.Value)
  end else if AnsiCompareText(VarName, 'VAX') = 0 then
  begin
    if QrCNABCfgLayoutRem.Value = CO_756_CORRESPONDENTE_BRADESCO_2015 then
    begin
      Banco := QrCNABCfgCorresBco.Value;
      //
      DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end else
    begin
      Banco := QrCNABCfgCedBanco.Value;
      //
      if QrCNABCfgDVB.Value <> '?' then
        DVB := QrCNABCfgDVB.Value
      else
        DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end;
    //
    Value := FormatFloat('000', Banco) + '-' + DVB;
  end
  else if AnsiCompareText(VarName, 'VARF_LINHADIGITAVEL') = 0 then
  begin
    UBancos.GeraNossoNumero(QrCNABCfgTipoCart.Value,
      QrCNABCfgCedBanco.Value, QrCNABCfgCedAgencia.Value,
      QrCNABCfgCedPosto.Value, QrBoletosBLOQUETO.Value,
      QrCNABCfgCedConta.Value, QrCNABCfgCartNum.Value,
      QrCNABCfgIDCobranca.Value,
      Geral.SoNumero_TT(QrCNABCfgCodEmprBco.Value),
      QrBoletosVencto.Value, QrCNABCfgTipoCobranca.Value,
      QrCNABCfgEspecieDoc.Value, QrCNABCfgCNAB.Value,
      QrCNABCfgCtaCooper.Value, QrCNABCfgLayoutRem.Value,
      FNossoNumero, NossoNumero_Rem);
    //
    Value := '';
    Value := UBancos.LinhaDigitavel_BoletoDeCobranca(
      UBancos.CodigoDeBarra_BoletoDeCobranca(QrCNABCfgCedBanco.Value,
      QrCNABCfgCedAgencia.Value, QrCNABCfgCorresBco.Value,
      QrCNABCfgCorresAge.Value, QrCNABCfgCedDAC_A.Value, QrCNABCfgCedPosto.Value,
      QrCNABCfgCedConta.Value, QrCNABCfgCedDAC_C.Value, QrCNABCfgCorresCto.Value,
      9, 3, 1, FNossoNumero, QrCNABCfgCodEmprBco.Value, QrCNABCfgCartNum.Value,
      QrCNABCfgIDCobranca.Value, QrCNABCfgOperCodi.Value, QrBoletosVencto.Value,
      QrBoletosSUB_TOT.Value, 0, 0, not CkZerado.Checked,
      QrCNABCfgModalCobr.Value, QrCNABCfgLayoutRem.Value))
  end else if AnsiCompareText(VarName, 'LogoBancoExiste') = 0 then
    Value := FmMyGlyfs.LogoBancoExiste(QrCNABCfgCedBanco.Value)
  else if AnsiCompareText(VarName, 'LogoBancoPath') = 0 then
    Value := FmMyGlyfs.CaminhoLogoBanco(QrCNABCfgCedBanco.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO1') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNABCfgTexto01.Value, V, M, J, C, P)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO2') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNABCfgTexto02.Value, V, M, J, C, P)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO3') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNABCfgTexto03.Value, V, M, J, C, P)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO4') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNABCfgTexto04.Value, V, M, J, C, P)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO5') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNABCfgTexto05.Value, V, M, J, C, P)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO6') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNABCfgTexto06.Value, V, M, J, C, P)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO7') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNABCfgTexto07.Value, V, M, J, C, P)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO8') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(QrCNABCfgTexto08.Value, V, M, J, C, P)
  // Fim Ficha de compensa��o
end;

function TFmBloGerenInadimpBloq.frxMostra(PrevCod: Integer; Titulo: string;
  Tipo: TselType; Antigo: Boolean): Boolean;
var
  ModelBloqAnt, ConfigAnt: Integer;
begin
  Result := True;
  ModelBloqAnt := -1000;
  ConfigAnt := -1000;
  case Tipo of
    istTodos:
    begin
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        QrCNABCfg.Close;
        QrCNABCfg.Params[0].AsInteger := QrBoletosControle.Value;
        QrCNABCfg.Open;
        //
        MyObjects.Informa(LaAviso, True, 'Gerando boleto ' + IntToStr(
          QrBoletos.RecNo) + ' de ' + IntToStr(QrBoletos.RecordCount));
        if Result then
        begin
          if QrBoletos.RecNo = 1 then
            Result := frxComposite(PrevCod, QrBoletosCliente.Value, Titulo, ModelBloqAnt, ConfigAnt, True, Antigo)
          else
            Result := frxComposite(PrevCod, QrBoletosCliente.Value, Titulo, ModelBloqAnt, ConfigAnt, False, Antigo);
        end;
        //#$%
        QrBoletos.Next;
      end;
    end;
    istMarcados:
    begin
    end;
    istSelecionados:
    begin
    end;
    istAtual:
    begin
      QrCNABCfg.Close;
      QrCNABCfg.Params[0].AsInteger := QrBoletosControle.Value;
      QrCNABCfg.Open;
      //
      Result := frxComposite(PrevCod, QrBoletosCliente.Value, Titulo, ModelBloqAnt, ConfigAnt, True, Antigo);
    end;
  end;
  if (Tipo in ([istAtual, istTodos, istMarcados, istSelecionados])) and Result then
  begin
    Application.CreateForm(TFmMeufrx, FmMeufrx);
    if CkDesign.Checked then
    begin
      frxBloq.DesignReport;
    end else
    begin
      frxBloq.Preview := FmMeufrx.PvVer;
      //
      FmMeufrx.PvVer.OutlineWidth := frxBloq.PreviewOptions.OutlineWidth;
      FmMeufrx.PvVer.Zoom := frxBloq.PreviewOptions.Zoom;
      FmMeufrx.PvVer.ZoomMode := frxBloq.PreviewOptions.ZoomMode;
      FmMeufrx.UpdateZoom;
      //
      FmMeufrx.ShowModal;
      FmMeufrx.Destroy;
    end;
  end;
  MyObjects.Informa(LaAviso, False, '...');
end;

function TFmBloGerenInadimpBloq.ImprimeBoletos(Quais: TselType; Como: TfrxImpComo;
  Arquivo: String; Filtro: TfrxCustomExportFilter; PrevCod: Integer;
  Antigo: Boolean; var SdoJaAtz: Boolean): TfrxReport;
const
  MaxHear = 4;
var
  Definido: Boolean;
begin
  Result := nil;
  //
  Screen.Cursor := crHourGlass;
  try
    Definido := False;
    case Como of
      ficMostra: Definido := frxMostra(PrevCod, 'Boletos', Quais, Antigo);
      else Geral.MB_Erro('Tipo de sa�da para impress�o de boleto n�o definida.' +
        sLineBreak + 'Informe a DERMATEK!');
    end;
    if Definido then Result := frxBloq;
    Application.ProcessMessages;
  finally
    frxBloq.Clear;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBloGerenInadimpBloq.QrBloqInadAfterScroll(DataSet: TDataSet);
var
  Cliente, FatNum, Mez, Vencto: String;
begin
  if DesativaScrool = False then
  begin
    Cliente := FormatFloat('0', QrBloqInadCliente.Value);
    FatNum  := FormatFloat('0', QrBloqInadFatNum.Value);
    Mez     := FormatFloat('0', QrBloqInadMez.Value);
    Vencto  := Geral.FDT(QrBloqInadVencimento.Value, 1);
    //
    QrBloqIts.Close;
    QrBloqIts.SQL.Clear;
    QrBloqIts.SQL.Add('SELECT lct.Genero, lct.Descricao, lct.Credito');
    QrBloqIts.SQL.Add('FROM ' + FTabLctA + ' lct');
    QrBloqIts.SQL.Add('WHERE lct.Cliente=' + Cliente);
    QrBloqIts.SQL.Add('AND lct.FatNum=' + FatNum);
    QrBloqIts.SQL.Add('AND lct.Mez=' + Mez);
    QrBloqIts.SQL.Add('AND lct.Vencimento="' + Vencto + '"');
    QrBloqIts.Open;
  end;
end;

procedure TFmBloGerenInadimpBloq.QrBloqInadBeforeClose(DataSet: TDataSet);
begin
  QrBloqIts.Close;
end;

procedure TFmBloGerenInadimpBloq.QrBoletosAfterScroll(DataSet: TDataSet);
var
  Cliente, FatNum, Mez, Vencto: String;
begin
  Cliente := FormatFloat('0', QrBoletosCliente.Value);
  FatNum  := FormatFloat('0', QrBoletosFatNum.Value);
  Mez     := FormatFloat('0', QrBoletosMez.Value);
  Vencto  := Geral.FDT(QrBoletosVencimento.Value, 1);
  //
  QrBoletosIts.Close;
  QrBoletosIts.SQL.Clear;
  QrBoletosIts.SQL.Add('SELECT lct.Genero, lct.Descricao, lct.Credito');
  QrBoletosIts.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrBoletosIts.SQL.Add('WHERE lct.Cliente=' + Cliente);
  QrBoletosIts.SQL.Add('AND lct.FatNum=' + FatNum);
  QrBoletosIts.SQL.Add('AND lct.Mez=' + Mez);
  QrBoletosIts.SQL.Add('AND lct.Vencimento="' + Vencto + '"');
  QrBoletosIts.Open;
  //
  DModG.ReopenEndereco(QrBoletosCliente.Value);
end;

procedure TFmBloGerenInadimpBloq.QrBoletosCalcFields(DataSet: TDataSet);
begin
  if QrBoletosBoleto.Value = 0 then
    QrBoletosBLOQUETO.Value := -1
  else
    QrBoletosBLOQUETO.Value := QrBoletosBoleto.Value;
  //
end;

procedure TFmBloGerenInadimpBloq.QrCNABCfgCalcFields(DataSet: TDataSet);
begin
  if QrCNABCfgAceiteTit.Value = 1 then
    QrCNABCfgACEITETIT_TXT.Value := 'S'
  else
    QrCNABCfgACEITETIT_TXT.Value := 'N';
end;

procedure TFmBloGerenInadimpBloq.ReopenBloqInad(Controle: Integer);
begin
  UMyMod.AbreQuery(QrBloqInad, DModG.MyPID_DB);
  //
  if Controle <> 0 then
    QrBloqInad.Locate('Controle', Controle, []);
end;

procedure TFmBloGerenInadimpBloq.TPVenctoChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmBloGerenInadimpBloq.TPVenctoClick(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
