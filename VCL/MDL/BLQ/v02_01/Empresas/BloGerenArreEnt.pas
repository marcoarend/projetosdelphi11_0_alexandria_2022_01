unit BloGerenArreEnt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, DB, mySQLDbTables, dmkEdit,
  dmkEditCB, DBCtrls, dmkDBLookupComboBox, dmkGeral, Variants, ComCtrls,
  dmkImage, dmkPermissoes, DmkDAC_PF, UnDmkEnums, UnDmkProcFunc;

type
  TFmBloGerenArreEnt = class(TForm)
    Panel1: TPanel;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    DsContas: TDataSource;
    CBEntidade: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    Label1: TLabel;
    Label2: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    EdDescricao: TdmkEdit;
    Label13: TLabel;
    EdValor: TdmkEdit;
    LaDeb: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton3: TSpeedButton;
    CkContinuar: TCheckBox;
    Label37: TLabel;
    EdCNAB_cfg: TdmkEditCB;
    CBCNAB_cfg: TdmkDBLookupComboBox;
    SpeedButton2: TSpeedButton;
    DsCNAB_cfg: TDataSource;
    QrCNAB_cfg: TmySQLQuery;
    QrCNAB_cfgCodigo: TIntegerField;
    QrCNAB_cfgNome: TWideStringField;
    QrCNAB_cfgCartEmiss: TIntegerField;
    QrCNAB_cfgTexto01: TWideStringField;
    QrCNAB_cfgTexto02: TWideStringField;
    QrCNAB_cfgTexto03: TWideStringField;
    QrCNAB_cfgTexto04: TWideStringField;
    QrCNAB_cfgTexto05: TWideStringField;
    QrCNAB_cfgTexto06: TWideStringField;
    QrCNAB_cfgTexto07: TWideStringField;
    QrCNAB_cfgTexto08: TWideStringField;
    QrCNAB_cfgTexto09: TWideStringField;
    QrCNAB_cfgTexto10: TWideStringField;
    TPData: TDateTimePicker;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrNFSeSrvCad: TmySQLQuery;
    QrNFSeSrvCadCodigo: TIntegerField;
    QrNFSeSrvCadNome: TWideStringField;
    DsNFSeSrvCad: TDataSource;
    LaNFSeSrvCad: TLabel;
    EdNFSeSrvCad: TdmkEditCB;
    CBNFSeSrvCad: TdmkDBLookupComboBox;
    SBNFSeSrvCad: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SBNFSeSrvCadClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Tipo: TSQLType);
    procedure ReabreCNAB_Cfg();
  public
    { Public declarations }
    FCodigo, FControle, FCliInt, FEntidade, FPeriodo: Integer;
    FQueryArreIts: TmySQLQuery;
  end;

  var
  FmBloGerenArreEnt: TFmBloGerenArreEnt;

implementation

uses ModuleBloGeren, UnFinanceiroJan, CNAB_Cfg, Entidade2, UMySQLModule, Module,
  {$IfNDef sNFSe} NFSe_PF_0000, {$EndIf}
  MyDBCheck, UnInternalConsts, BloGeren, UnMyObjects, ModuleGeral, UnBloquetos,
  UnBloqGerl, MyListas;

{$R *.DFM}

procedure TFmBloGerenArreEnt.BtOKClick(Sender: TObject);
var
  Codigo, Entidade, Conta, CNAB_Cfg, CartEmiss, MesValido, NFSeSrvCad: Integer;
  Descri, MesTxt, Mensagem: String;
  Valor: Double;
  Vencto, VenctoValido: TDateTime;
  ValidouMes: Boolean;
begin
  Vencto     := TPData.Date;
  Entidade   := EdEntidade.ValueVariant;
  Conta      := EdConta.ValueVariant;
  Descri     := EdDescricao.ValueVariant;
  Valor      := EdValor.ValueVariant;
  CNAB_Cfg   := EdCNAB_cfg.ValueVariant;
  CartEmiss  := QrCNAB_cfgCartEmiss.Value;
  NFSeSrvCad := EdNFSeSrvCad.ValueVariant;
  Codigo     := FCodigo;
  //
  if MyObjects.FIC(Vencto = 0, TPData, 'Defina o vencimento!') then Exit;
  //
  ValidouMes := UBloqGerl.ValidaVencimentoPeriodo(Dmod.MyDB,
                  DBloGeren.QrBloOpcoes, FPeriodo, Vencto, MesValido);
  MesTxt     := dmkPF.VerificaMes(MesValido, False);
  // 
  if MyObjects.FIC(ValidouMes, TPData, 'O vencimento deve ser correspondente ao m�s ' + MesTxt + '!') then Exit;
  if MyObjects.FIC(Valor = 0, EdValor, 'Defina o valor!') then Exit;
  //
  if DBloGeren.QrBloOpcoes.FieldByName('VctoFeriado').AsInteger = 1 then
  begin
    VenctoValido := UBloquetos.AjustaVencimentoPeriodo(FPeriodo, Vencto);
    //
    if Trunc(Vencto) <> Trunc(VenctoValido) then
    begin
      Geral.MB_Aviso('O vencimento n�o corresponde a uma data �til!');
      TPData.Date := VenctoValido;
      Exit;
    end;
  end;
  if Valor > 0 then
  begin
    if MyObjects.FIC(UpperCase(QrContasCredito.Value) = 'F', EdConta, 'A conta selecionada deve ser de cr�dito!') then Exit;
  end else
  begin
    if MyObjects.FIC(UpperCase(QrContasDebito.Value) = 'F', EdConta, 'A conta selecionada deve ser de d�bito!') then Exit;
  end;
  //
  //
  if UBloquetos.AtualizaArrecadacao(ImgTipo.SQLType, Codigo, FControle, Conta,
    0, 0, Entidade, CartEmiss, CNAB_Cfg, NFSeSrvCad, 0, 0, 0, 0, 0,
    Geral.FDT(Vencto, 1), Valor, 0, 0, Descri, Mensagem) <> 0 then
  begin
    FEntidade := Entidade;
    //
    if (ImgTipo.SQLType = stUpd) or (CkContinuar.Checked = False) then
      Close
    else
      MostraEdicao(stIns);
  end else
    Geral.MB_Aviso(Mensagem);
end;

procedure TFmBloGerenArreEnt.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloGerenArreEnt.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if key=VK_F4 then EdDescricao.ValueVariant := QrContasNome.Value;
    if key=VK_F5 then EdDescricao.ValueVariant := QrContasNome2.Value;
    if key=VK_F6 then EdDescricao.ValueVariant := QrContasNome3.Value;
    EdDescricao.SetFocus;
    EdDescricao.SelStart := Length(EdDescricao.ValueVariant);
    EdDescricao.SelLength := 0;
  end;
end;

procedure TFmBloGerenArreEnt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  MostraEdicao(ImgTipo.SQLType);
end;

procedure TFmBloGerenArreEnt.ReabreCNAB_Cfg();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCNAB_cfg, Dmod.MyDB, [
    'SELECT Codigo, Nome, CartEmiss, Texto01, Texto02, Texto03, ',
    'Texto04, Texto05, Texto06, Texto07, Texto08, Texto09, Texto10 ',
    'FROM cnab_cfg ',
    'WHERE (Cedente=' + Geral.FF0(FCliInt),
    'OR SacadAvali=' + Geral.FF0(FCliInt) + ')',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmBloGerenArreEnt.FormCreate(Sender: TObject);
var
  TemNFSe: Boolean;
begin
  ImgTipo.SQLType := stLok;
  //
  {$IfNDef sNFSe}
  UMyMod.AbreQuery(QrNFSeSrvCad, Dmod.MyDB);
  //
  TemNFSe := DBCheck.LiberaModulo(CO_DMKID_APP, dmkPF.ObtemSiglaModulo(mdlappNFSe),
               DModG.QrMaster.FieldByName('HabilModulos').AsString);
  {$Else}
  QrNFSeSrvCad.Close;
  //
  TemNFSe := False;
  {$EndIf}
  LaNFSeSrvCad.Visible := TemNFSe;
  EdNFSeSrvCad.Visible := TemNFSe;
  CBNFSeSrvCad.Visible := TemNFSe;
  SBNFSeSrvCad.Visible := TemNFSe;
end;

procedure TFmBloGerenArreEnt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloGerenArreEnt.FormShow(Sender: TObject);
begin
  DBloGeren.ReopenBloOpcoes;
  UMyMod.AbreQuery(QrEntidades, Dmod.MyDB);
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
  ReabreCNAB_Cfg;
end;

procedure TFmBloGerenArreEnt.MostraEdicao(Tipo: TSQLType);
begin
  case Tipo of
    stIns:
    begin
      EdCNAB_cfg.ValueVariant   := 0;
      CBCNAB_cfg.KeyValue       := Null;
      EdEntidade.ValueVariant   := 0;
      CBEntidade.KeyValue       := Null;
      EdConta.ValueVariant      := 0;
      CBConta.KeyValue          := Null;
      EdNFSeSrvCad.ValueVariant := 0;
      CBNFSeSrvCad.KeyValue     := Null;
      EdDescricao.ValueVariant  := '';
      EdValor.ValueVariant      := 0;
      //
      if (FQueryArreIts.State <> dsInactive) and (FQueryArreIts.RecordCount > 0) then
        TPData.Date := UBloquetos.AjustaVencimentoPeriodo(FPeriodo,
                         FQueryArreIts.FieldByName('Vencto').AsDateTime)
      else
        TPData.Date := UBloquetos.AjustaVencimentoPeriodo(FPeriodo, DModG.ObtemAgora);
      //
      CkContinuar.Checked := True;
      CkContinuar.Visible := True;
    end;
    stUpd:
    begin
      EdCNAB_cfg.ValueVariant   := FQueryArreIts.FieldByName('CNAB_Cfg').AsInteger;
      CBCNAB_cfg.KeyValue       := FQueryArreIts.FieldByName('CNAB_Cfg').AsInteger;
      EdEntidade.ValueVariant   := FQueryArreIts.FieldByName('Entidade').AsInteger;
      CBEntidade.KeyValue       := FQueryArreIts.FieldByName('Entidade').AsInteger;
      EdConta.ValueVariant      := FQueryArreIts.FieldByName('Conta').AsInteger;
      CBConta.KeyValue          := FQueryArreIts.FieldByName('Conta').AsInteger;
      EdNFSeSrvCad.ValueVariant := FQueryArreIts.FieldByName('NFSeSrvCad').AsInteger;
      CBNFSeSrvCad.KeyValue     := FQueryArreIts.FieldByName('NFSeSrvCad').AsInteger;
      EdDescricao.ValueVariant  := FQueryArreIts.FieldByName('Texto').AsString;
      EdValor.ValueVariant      := FQueryArreIts.FieldByName('Valor').AsFloat;
      TPData.Date               := FQueryArreIts.FieldByName('Vencto').AsDateTime;
      CkContinuar.Checked       := False;
      CkContinuar.Visible       := False;
    end;
  end;
  EdCNAB_cfg.SetFocus;
end;

procedure TFmBloGerenArreEnt.SBNFSeSrvCadClick(Sender: TObject);
begin
  {$IfNDef sNFSe}
  VAR_CADASTRO := 0;
  //
  UnNFSe_PF_0000.MostraFormNFSeSrvCad(EdNFSeSrvCad.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdNFSeSrvCad, CBNFSeSrvCad, QrNFSeSrvCad, VAR_CADASTRO);
    //
    EdNFSeSrvCad.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmBloGerenArreEnt.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FinanceiroJan.CadastroDeContas(EdConta.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdConta, CBConta, QrContas, VAR_CADASTRO);
    //
    EdConta.SetFocus;
  end;
end;

procedure TFmBloGerenArreEnt.SpeedButton2Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UBloquetos.CadastroCNAB_Cfg(EdCNAB_cfg.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCNAB_cfg, CBCNAB_cfg, QrCNAB_cfg, VAR_CADASTRO);
    //
    EdCNAB_cfg.SetFocus;
  end;
end;

procedure TFmBloGerenArreEnt.SpeedButton3Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    //
    QrEntidades.Close;
    QrEntidades.Open;
    //
    EdEntidade.ValueVariant := VAR_CADASTRO;
    CBEntidade.KeyValue     := VAR_CADASTRO;
    CBEntidade.SetFocus;
  end;
end;

end.
