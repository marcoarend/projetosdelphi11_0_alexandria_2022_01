unit BloGerenInadimpBloq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkEdit, unDmkProcFunc,
  ComCtrls, dmkEditDateTimePicker, dmkDBGrid, DB, mySQLDbTables, frxClass,
  frxDBSet, dmkGeral, dmkDBGridDAC, dmkImage, dmkPermissoes, UnDmkEnums;

type
  TFmBloGerenInadimpBloq = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    TPVencto: TdmkEditDateTimePicker;
    EdMulta: TdmkEdit;
    EdJuros: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBGBloq: TdmkDBGridDAC;
    QrBloqInad: TmySQLQuery;
    DsBloqInad: TDataSource;
    frxDsBoletos: TfrxDBDataset;
    QrBoletos: TmySQLQuery;
    CkZerado: TCheckBox;
    CkDesign: TCheckBox;
    LaAviso: TLabel;
    Splitter1: TSplitter;
    dmkDBGrid1: TdmkDBGrid;
    QrBloqIts: TmySQLQuery;
    QrBloqItsGenero: TIntegerField;
    QrBloqItsDescricao: TWideStringField;
    QrBloqItsCredito: TFloatField;
    DsBloqIts: TDataSource;
    frxDsBoletosIts: TfrxDBDataset;
    QrBloqInadData: TDateField;
    QrBloqInadCREDITO: TFloatField;
    QrBloqInadPAGO: TFloatField;
    QrBloqInadSALDO: TFloatField;
    QrBloqInadMez: TIntegerField;
    QrBloqInadVencimento: TDateField;
    QrBloqInadCompensado: TDateField;
    QrBloqInadControle: TIntegerField;
    QrBloqInadDescricao: TWideStringField;
    QrBloqInadJuros: TFloatField;
    QrBloqInadMulta: TFloatField;
    QrBloqInadTOTAL: TFloatField;
    QrBloqInadFatNum: TFloatField;
    QrBloqInadPEND_VAL: TFloatField;
    QrBloqInadMEZ_TXT: TWideStringField;
    QrBloqInadVCTO_TXT: TWideStringField;
    QrBloqInadCliInt: TIntegerField;
    QrBloqInadNOMEEMP: TWideStringField;
    QrBloqInadNOMEENT: TWideStringField;
    QrBloqInadCliente: TIntegerField;
    QrBoletosIts: TmySQLQuery;
    QrBloqInadAtivo: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtRecalcula: TBitBtn;
    BtImprime: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrBloqInadNewVencto: TDateField;
    QrBoletosEntidade: TIntegerField;
    QrBoletosVencto: TDateField;
    QrBoletosNOMEENT: TWideStringField;
    QrBoletosBOLENT: TWideStringField;
    QrBoletosKGT: TLargeintField;
    QrBoletosVENCTO_TXT: TWideStringField;
    QrBoletosValor: TFloatField;
    QrBoletosCartEmiss: TIntegerField;
    QrBoletosCAR_TIPODOC: TSmallintField;
    QrBoletosCART_ATIVO: TSmallintField;
    QrBoletosCedBanco: TIntegerField;
    QrBoletosJurosPerc: TFloatField;
    QrBoletosMultaPerc: TFloatField;
    QrBoletosCNAB_Cfg: TIntegerField;
    QrBoletosEmpresa: TIntegerField;
    QrBoletosGenero: TIntegerField;
    QrBoletosTexto: TWideStringField;
    QrBoletosControle: TIntegerField;
    QrBoletosNOMECNAB_Cfg: TWideStringField;
    QrBoletosBoleto: TFloatField;
    QrBoletosCodigo: TIntegerField;
    QrBoletosAvulso: TSmallintField;
    QrBoletosPeriodo: TIntegerField;
    QrBoletosFatID: TIntegerField;
    QrBoletosFatNum: TFloatField;
    QrBoletosModelBloq: TIntegerField;
    QrBoletosCompe: TSmallintField;
    QrBoletosBloqFV: TIntegerField;
    QrBoletosAviso01: TWideStringField;
    QrBoletosAviso02: TWideStringField;
    QrBoletosAviso03: TWideStringField;
    QrBoletosAviso04: TWideStringField;
    QrBoletosAviso05: TWideStringField;
    QrBoletosAviso06: TWideStringField;
    QrBoletosAviso07: TWideStringField;
    QrBoletosAviso08: TWideStringField;
    QrBoletosAviso09: TWideStringField;
    QrBoletosAviso10: TWideStringField;
    QrBoletosAvisoVerso: TWideStringField;
    QrBoletosDataCad: TDateField;
    QrBoletosCedente: TIntegerField;
    QrBoletosItsTEXTO: TWideStringField;
    QrBoletosItsVALOR: TFloatField;
    QrBoletosItsVencto: TDateField;
    QrBoletosItsControle: TIntegerField;
    QrBoletosItsLancto: TIntegerField;
    QrBoletosItsCNAB_Cfg: TIntegerField;
    QrBoletosItsEntidade: TIntegerField;
    QrBoletosItsAvulso: TIntegerField;
    QrBoletosItsBoleto: TFloatField;
    QrBoletosItsFatNum: TFloatField;
    QrBoletosItsNotaFiscal: TFloatField;
    QrBoletosItsSerieNF: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TPVenctoChange(Sender: TObject);
    procedure TPVenctoClick(Sender: TObject);
    procedure EdMultaChange(Sender: TObject);
    procedure EdJurosChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtRecalculaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrBoletosAfterScroll(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure QrBloqInadBeforeClose(DataSet: TDataSet);
    procedure QrBloqInadAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure QrBoletosBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    DesativaScrool: Boolean;
    FLoteImp: Integer;
    procedure FechaPesquisa();
    procedure AtualizaTodos(Status: Integer);
    procedure ReopenBloqInad(Controle: Integer);
  public
    { Public declarations }
    FTabLctA, FTmpBloqInadCli: String;
    FEmpresa: Integer;
  end;

  var
  FmBloGerenInadimpBloq: TFmBloGerenInadimpBloq;

implementation

uses ModuleGeral, UMySQLModule, UnBancos, MyGlyfs, MeuFrx, UnInternalConsts,
  UnMyObjects, Module, ModuleBco, UnBloquetos, DmkDAC_PF, ModuleBloGeren;

{$R *.DFM}

procedure TFmBloGerenInadimpBloq.AtualizaTodos(Status: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + VAR_MyPID_DB_NOME + '.' + FTmpBloqInadCli + ' SET Ativo=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Status;
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenBloqInad(QrBloqInadControle.Value);
end;

procedure TFmBloGerenInadimpBloq.BtImprimeClick(Sender: TObject);
const
  CO_Txt = 'SEGUNDA VIA ATUALIZADA DE BLOQUETO';
var
  Txt: String;
  SdoJaAtz: Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrBoletos, DModG.MyPID_DB, [
    'SELECT cli.FatNum Boleto, cli.Cliente Entidade, cli.NewVencto Vencto, ',
    'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
    'cli.PEND_VAL Valor, CONCAT_WS("-", cli.FatNum, cli.Cliente) BOLENT, 0 KGT, ',
    'lct.Carteira CartEmiss, car.TipoDoc CAR_TIPODOC, car.Ativo CART_ATIVO, ',
    'cna.CedBanco, cna.JurosPerc, cna.MultaPerc, ari.CNAB_Cfg, pre.Empresa, ',
    'ari.Conta Genero, ari.Texto, ari.Controle, cna.Nome NOMECNAB_Cfg, ',
    'ari.Codigo, ari.Avulso, pre.Periodo, cna.Cedente, ',
    'ari.FatID, ari.FatNum, ari.ModelBloq, ari.Compe, ari.BloqFV, ari.DataCad, ',
    'ari.Aviso01, ari.Aviso02, ari.Aviso03, ari.Aviso04, ari.Aviso05, ',
    'ari.Aviso06, ari.Aviso07, ari.Aviso08, ari.Aviso09, ari.Aviso10, pre.AvisoVerso  ',
    'FROM ' + VAR_MyPID_DB_NOME + '.' + FTmpBloqInadCli + ' cli ',
    'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo = cli.Cliente ',
    'LEFT JOIN ' + FTabLctA + ' lct ON lct.Controle = cli.Controle ',
    'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo = lct.Carteira ',
    'LEFT JOIN ' + TMeuDB + '.arreits ari ON ari.Lancto = cli.Controle ',
    'LEFT JOIN ' + TMeuDB + '.cnab_cfg cna ON cna.Codigo = ari.CNAB_Cfg ',
    'LEFT JOIN ' + TMeuDB + '.prev pre ON pre.Codigo = ari.Codigo ',
    'WHERE cli.Ativo = 1 ',
    '']);
  if QrBoletos.RecordCount = 0 then
  begin
    Geral.MB_Aviso('N�o foi selecionado nenhum boleto para ser impresso!');
    Exit;
  end;
  //
  UBloquetos.ImprimeBloqueto(CkZerado.Checked, FTabLctA, QrBoletos,
    QrBoletosIts, frxDsBoletos, frxDsBoletosIts, istTodos, ficMostra, '', nil,
    TDBGrid(DBGBloq), CO_Txt);
end;

procedure TFmBloGerenInadimpBloq.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmBloGerenInadimpBloq.BtRecalculaClick(Sender: TObject);
var
  NewVencto: TDateTime;
  TOTAL, PEND_VAL, TaxaJ, Juros, Multa, ValJuros, ValMulta, Dias: Double;
  Controle: Integer;
begin
  try
    Screen.Cursor  := crHourGlass;
    DesativaScrool := True;
    //
    if QrBloqInad.State = dsBrowse then
      Controle := QrBloqInadControle.Value
    else
      Controle := 0;
    //
    ReopenBloqInad(0);
    //
    DBGBloq.Visible := True;
    NewVencto       := Trunc(TPVencto.Date);
    Multa           := EdMulta.ValueVariant;
    Juros           := EdJuros.ValueVariant;
    //
    QrBloqInad.First;
    //
    while not QrBloqInad.Eof do
    begin
      if DBloGeren.QrBloOpcoes.FieldByName('VctoFeriado').AsInteger = 0 then //N�o pula dias n�o �teis
        Dias := NewVencto - (QrBloqInadVencimento.Value + DmBco.QrCNAB_CfgJurosDias.Value)
      else
        Dias := UMyMod.DiasUteis((QrBloqInadVencimento.Value + DmBco.QrCNAB_CfgJurosDias.Value), NewVencto);
      //
      TaxaJ    := dmkPF.CalculaJuroSimples(Juros, Dias);
      ValMulta := Round(QrBloqInadCREDITO.Value * Multa) / 100;
      ValJuros := Round(QrBloqInadCREDITO.Value * TaxaJ) / 100;
      TOTAL    := QrBloqInadCREDITO.Value + ValMulta + ValJuros;
      PEND_VAL := TOTAL - QrBloqInadPago.Value;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, VAR_MyPID_DB_NOME + '.' + FTmpBloqInadCli, False, [
        'Juros', 'Multa', 'TOTAL', 'PEND_VAL', 'NewVencto'], ['Controle', 'FatNum'],
        [ValJuros, ValMulta, TOTAL, PEND_VAL, NewVencto], [
        QrBloqInadControle.Value, QrBloqInadFatNum.Value], False);
      //
      QrBloqInad.Next;
    end;
  finally
    DesativaScrool := False;
    //
    ReopenBloqInad(Controle);
    //
    BtImprime.Enabled := QrBloqInad.RecordCount > 0;
    Screen.Cursor     := crDefault;
  end;
end;

procedure TFmBloGerenInadimpBloq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloGerenInadimpBloq.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmBloGerenInadimpBloq.EdJurosChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmBloGerenInadimpBloq.EdMultaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmBloGerenInadimpBloq.FechaPesquisa;
begin
  BtImprime.Enabled := False;
  QrBloqInad.Close;
  DBGBloq.Visible := False;
end;

procedure TFmBloGerenInadimpBloq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmBloGerenInadimpBloq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DesativaScrool  := False;
  //
  DBloGeren.ReopenBloOpcoes;
  //
  FLoteImp            := -1000000000;
  TPVencto.Date       := UMyMod.CalculaDataDeposito(Date + 1);
  QrBloqInad.Database := DModG.MyPID_DB;
  QrBoletos.Database  := DModG.MyPID_DB;
end;

procedure TFmBloGerenInadimpBloq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloGerenInadimpBloq.FormShow(Sender: TObject);
begin
  DModG.ReopenParamsEmp(FEmpresa);
end;

procedure TFmBloGerenInadimpBloq.QrBloqInadAfterScroll(DataSet: TDataSet);
var
  Cliente, FatNum, Mez, Vencto: String;
begin
  if DesativaScrool = False then
  begin
    Cliente := FormatFloat('0', QrBloqInadCliente.Value);
    FatNum  := FormatFloat('0', QrBloqInadFatNum.Value);
    Mez     := FormatFloat('0', QrBloqInadMez.Value);
    Vencto  := Geral.FDT(QrBloqInadVencimento.Value, 1);
    //
    QrBloqIts.Close;
    QrBloqIts.SQL.Clear;
    QrBloqIts.SQL.Add('SELECT lct.Genero, lct.Descricao, lct.Credito');
    QrBloqIts.SQL.Add('FROM ' + FTabLctA + ' lct');
    QrBloqIts.SQL.Add('WHERE lct.Cliente=' + Cliente);
    QrBloqIts.SQL.Add('AND lct.FatNum=' + FatNum);
    QrBloqIts.SQL.Add('AND lct.Mez=' + Mez);
    QrBloqIts.SQL.Add('AND lct.Vencimento="' + Vencto + '"');
    QrBloqIts.Open;
  end;
end;

procedure TFmBloGerenInadimpBloq.QrBloqInadBeforeClose(DataSet: TDataSet);
begin
  QrBloqIts.Close;
end;

procedure TFmBloGerenInadimpBloq.QrBoletosAfterScroll(DataSet: TDataSet);
begin
  UBloquetos.ReopenBoletosIts(QrBoletosIts, QrBoletosBoleto.Value,
    QrBoletosEntidade.Value, QrBoletosCNAB_Cfg.Value, QrBoletosCodigo.Value, 0);
end;

procedure TFmBloGerenInadimpBloq.QrBoletosBeforeClose(DataSet: TDataSet);
begin
  QrBloqIts.Close;
end;

procedure TFmBloGerenInadimpBloq.ReopenBloqInad(Controle: Integer);
begin
  UMyMod.AbreQuery(QrBloqInad, DModG.MyPID_DB);
  //
  if Controle <> 0 then
    QrBloqInad.Locate('Controle', Controle, []);
end;

procedure TFmBloGerenInadimpBloq.TPVenctoChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmBloGerenInadimpBloq.TPVenctoClick(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
