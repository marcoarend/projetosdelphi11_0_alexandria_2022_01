unit BloGeren;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, Menus, Grids,
  DBGrids, dmkDBGrid, ComCtrls, DBCtrls, dmkGeral, dmkLabel, UnDmkProcFunc,
  dmkImage, dmkDBLookupComboBox, dmkEdit, dmkEditCB, Variants, frxClass,
  DmkDAC_PF, frxDBSet, dmkCompoStore, dmkPermissoes, UnBloqGerl, Mask,
  dmkDBEdit, UnDmkEnums, UnInternalConsts, frxExportPDF, dmkDBGridZTO;

type
  THackDBGrid = class(TDBGrid);
  TFmBloGeren = class(TForm)
    PMArrecada: TPopupMenu;
    Incluiitensbasedearrecadao1: TMenuItem;
    N1: TMenuItem;
    Novoitemdearrecadao1: TMenuItem;
    Alteraarrecadao1: TMenuItem;
    Exclusodearrecadao1: TMenuItem;
    Itemselecionado1: TMenuItem;
    ODOSitensdearrecadao1: TMenuItem;
    PMPrebol: TPopupMenu;
    Gerartodosabertos1: TMenuItem;
    Gerarselecionados1: TMenuItem;
    AlteraVencimento2: TMenuItem;
    PMBoleto: TPopupMenu;
    Alteravencimento1: TMenuItem;
    Selecionados3: TMenuItem;
    Todos3: TMenuItem;
    DsPrev: TDataSource;
    QrPrev: TmySQLQuery;
    QrPrevTOT_BOL: TFloatField;
    QrPrevTOT_PRE: TFloatField;
    QrPrevCodigo: TIntegerField;
    QrPrevPeriodo: TIntegerField;
    QrPrevAviso01: TWideStringField;
    QrPrevAviso02: TWideStringField;
    QrPrevAviso03: TWideStringField;
    QrPrevAviso04: TWideStringField;
    QrPrevAviso05: TWideStringField;
    QrPrevAviso06: TWideStringField;
    QrPrevAviso07: TWideStringField;
    QrPrevAviso08: TWideStringField;
    QrPrevAviso09: TWideStringField;
    QrPrevAviso10: TWideStringField;
    QrPrevAvisoVerso: TWideStringField;
    QrPrevCompe: TSmallintField;
    QrPrevEncerrado: TSmallintField;
    QrPrevLk: TIntegerField;
    QrPrevDataCad: TDateField;
    QrPrevDataAlt: TDateField;
    QrPrevUserCad: TIntegerField;
    QrPrevUserAlt: TIntegerField;
    QrPrevAlterWeb: TSmallintField;
    QrPrevEmpresa: TIntegerField;
    QrPrevAtivo: TSmallintField;
    QrPrevPERIODO_TXT: TWideStringField;
    QrPrevNOME_ENCERRADO: TWideStringField;
    QrPrevNOMEEMP: TWideStringField;
    QrPrevModelBloq: TIntegerField;
    PmImprime: TPopupMenu;
    ImprimeBloqSel: TMenuItem;
    ImprimeBloqTodos: TMenuItem;
    Boleto1: TMenuItem;
    DosboletosSelecionados1: TMenuItem;
    DeTodosboletos1: TMenuItem;
    DosboletosSelecionados2: TMenuItem;
    DeTodosboletos2: TMenuItem;
    Desfazerboletos2: TMenuItem;
    PMOrc: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    N2: TMenuItem;
    TravaDestr: TMenuItem;
    QrPrevBloqFV: TIntegerField;
    N3: TMenuItem;
    Editar1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel2: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel7: TPanel;
    Panel10: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel18: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    CkZerado: TCheckBox;
    BtOpcoes: TBitBtn;
    Gerartodooperodo1: TMenuItem;
    N4: TMenuItem;
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    DBText12: TDBText;
    DBText13: TDBText;
    DBText1: TDBText;
    Panel5: TPanel;
    Label18: TLabel;
    DBText7: TDBText;
    Label19: TLabel;
    DBText8: TDBText;
    Label22: TLabel;
    DBText9: TDBText;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    ImgCondGer: TImage;
    Panel11: TPanel;
    BtOrc: TBitBtn;
    TabSheet4: TTabSheet;
    Panel12: TPanel;
    BtArrecada: TBitBtn;
    TabSheet5: TTabSheet;
    Splitter5: TSplitter;
    Panel6: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    BtPrebol: TBitBtn;
    TabSheet6: TTabSheet;
    Splitter1: TSplitter;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel15: TPanel;
    BtBoleto1: TBitBtn;
    BtImprime: TBitBtn;
    BtProtocolo: TBitBtn;
    BtRetorno: TBitBtn;
    BtPesq: TBitBtn;
    Panel19: TPanel;
    CBCNABCfg: TdmkDBLookupComboBox;
    EdCNABCfg: TdmkEditCB;
    Label2: TLabel;
    CBArre: TdmkDBLookupComboBox;
    EdArre: TdmkEditCB;
    Label1: TLabel;
    CBEntidade: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    Label3: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label4: TLabel;
    GBEmissao: TGroupBox;
    CkDataEmiIni: TCheckBox;
    TPDataEmiIni: TDateTimePicker;
    CkDataEmiFim: TCheckBox;
    TPDataEmiFim: TDateTimePicker;
    GroupBox1: TGroupBox;
    CkDataVctIni: TCheckBox;
    TPDataVctIni: TDateTimePicker;
    CkDataVctFim: TCheckBox;
    TPDataVctFim: TDateTimePicker;
    CkValor: TCheckBox;
    EdValorIni: TdmkEdit;
    EdValorFim: TdmkEdit;
    Bevel2: TBevel;
    Panel20: TPanel;
    BtBoleto2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    Splitter4: TSplitter;
    Panel22: TPanel;
    BtPesquisa: TBitBtn;
    QrArre: TmySQLQuery;
    QrArreEntidade: TIntegerField;
    QrArreValor: TFloatField;
    QrArreNOMEENT: TWideStringField;
    QrArreCNAB_Cfg: TIntegerField;
    QrArreNOMECNAB_Cfg: TWideStringField;
    QrArreBoleto: TFloatField;
    QrArreVencto: TDateField;
    DsArre: TDataSource;
    QrSumARRE: TmySQLQuery;
    QrSumARREValor: TFloatField;
    DsSumARRE: TDataSource;
    QrArreBol: TmySQLQuery;
    QrArreBolBoleto: TFloatField;
    DsArreBol: TDataSource;
    QrArreIts: TmySQLQuery;
    QrArreItsCodigo: TIntegerField;
    QrArreItsControle: TIntegerField;
    QrArreItsCartEmiss: TIntegerField;
    QrArreItsCNAB_Cfg: TIntegerField;
    QrArreItsConta: TIntegerField;
    QrArreItsEntidade: TIntegerField;
    QrArreItsLancto: TIntegerField;
    QrArreItsNum: TIntegerField;
    QrArreItsTexto: TWideStringField;
    QrArreItsValor: TFloatField;
    QrArreItsVencto: TDateField;
    QrArreItsLk: TIntegerField;
    QrArreItsDataCad: TDateField;
    QrArreItsDataAlt: TDateField;
    QrArreItsUserCad: TIntegerField;
    QrArreItsUserAlt: TIntegerField;
    QrArreItsAlterWeb: TSmallintField;
    QrArreItsAtivo: TSmallintField;
    QrArreItsBloArre: TIntegerField;
    QrArreItsBloArreIts: TIntegerField;
    QrArreItsBoleto: TFloatField;
    DsArreIts: TDataSource;
    QrBoletos: TmySQLQuery;
    QrBoletosEntidade: TIntegerField;
    QrBoletosVencto: TDateField;
    QrBoletosNOMEENT: TWideStringField;
    QrBoletosBOLENT: TWideStringField;
    QrBoletosKGT: TLargeintField;
    QrBoletosVENCTO_TXT: TWideStringField;
    QrBoletosValor: TFloatField;
    QrBoletosCartEmiss: TIntegerField;
    QrBoletosCAR_TIPODOC: TSmallintField;
    QrBoletosCART_ATIVO: TSmallintField;
    QrBoletosCedBanco: TIntegerField;
    QrBoletosJurosPerc: TFloatField;
    QrBoletosMultaPerc: TFloatField;
    QrBoletosCNAB_Cfg: TIntegerField;
    QrBoletosEmpresa: TIntegerField;
    QrBoletosTexto: TWideStringField;
    QrBoletosControle: TIntegerField;
    QrBoletosNOMECNAB_Cfg: TWideStringField;
    QrBoletosBoleto: TFloatField;
    QrBoletosCodigo: TIntegerField;
    QrBoletosAvulso: TSmallintField;
    DsBoletos: TDataSource;
    QrBoletosIts: TmySQLQuery;
    QrBoletosItsTEXTO: TWideStringField;
    QrBoletosItsVALOR: TFloatField;
    QrBoletosItsVencto: TDateField;
    QrBoletosItsControle: TIntegerField;
    QrBoletosItsLancto: TIntegerField;
    QrBoletosItsCNAB_Cfg: TIntegerField;
    QrBoletosItsEntidade: TIntegerField;
    DsBoletosIts: TDataSource;
    frxDsBoletosIts: TfrxDBDataset;
    frxDsBoletos: TfrxDBDataset;
    CSTabSheetChamou: TdmkCompoStore;
    PMBoleto2: TPopupMenu;
    Gerartodooperodo2: TMenuItem;
    N5: TMenuItem;
    QrBoletosPeriodo: TIntegerField;
    AlteraVencimentoSelecionados1: TMenuItem;
    Desfazerboletosselecionados1: TMenuItem;
    QrBoletosItsAvulso: TIntegerField;
    QrBoletosItsBoleto: TFloatField;
    EditarprotocolodeentregaSelecionados1: TMenuItem;
    BtBloArre: TBitBtn;
    BtCNABCfg: TBitBtn;
    BtProPad: TBitBtn;
    BtProtoGer: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox2: TGroupBox;
    Label75: TLabel;
    EdFatID_Cod: TdmkEdit;
    EdFatID_Txt: TEdit;
    Label5: TLabel;
    EdFatNum: TdmkEdit;
    BtAvanca1: TBitBtn;
    BtAvanca2: TBitBtn;
    BtVolta2: TBitBtn;
    BtVolta3: TBitBtn;
    BtVolta4: TBitBtn;
    BtAvanca3: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrBoletosFatID: TIntegerField;
    QrBoletosFatNum: TFloatField;
    QrBoletosModelBloq: TIntegerField;
    QrBoletosCompe: TSmallintField;
    QrBoletosBloqFV: TIntegerField;
    N7: TMenuItem;
    Modificarlayoutdobloqueto1: TMenuItem;
    TabSheet7: TTabSheet;
    Panel23: TPanel;
    BtPEBProtocolo: TBitBtn;
    QrProt1: TmySQLQuery;
    QrProt1Codigo: TIntegerField;
    QrProt1Nome: TWideStringField;
    DsProt1: TDataSource;
    PMProtocolo: TPopupMenu;
    Gera1: TMenuItem;
    Todosabertos1: TMenuItem;
    Abertosseleciondos1: TMenuItem;
    Desfazprotocolo1: TMenuItem;
    MenuItem1: TMenuItem;
    Localizaprotocolo1: TMenuItem;
    BtPEBVoltar: TBitBtn;
    TabSheet13: TTabSheet;
    Panel25: TPanel;
    PnSaiDesis: TPanel;
    BtAPCarrega: TBitBtn;
    BtAPGera: TBitBtn;
    BtAPVoltar: TBitBtn;
    Panel26: TPanel;
    dmkDBGrid2: TdmkDBGrid;
    Panel27: TPanel;
    BtAPItsTodos: TBitBtn;
    BtAPItsNenhum: TBitBtn;
    Splitter9: TSplitter;
    Panel28: TPanel;
    dmkDBGrid3: TdmkDBGrid;
    Panel29: TPanel;
    BtAPArreTodos: TBitBtn;
    BtAPArreNenhum: TBitBtn;
    EdBoleto: TdmkEdit;
    Label17: TLabel;
    PB1: TProgressBar;
    Alteravalor1: TMenuItem;
    N8: TMenuItem;
    Alteratextodaarrecadaoatual1: TMenuItem;
    QrBoletosItsFatNum: TFloatField;
    QrBoletosDataCad: TDateField;
    EdLancto: TdmkEdit;
    Label20: TLabel;
    QrBoletosAviso01: TWideStringField;
    QrBoletosAviso02: TWideStringField;
    QrBoletosAviso03: TWideStringField;
    QrBoletosAviso04: TWideStringField;
    QrBoletosAviso05: TWideStringField;
    QrBoletosAviso06: TWideStringField;
    QrBoletosAviso07: TWideStringField;
    QrBoletosAviso08: TWideStringField;
    QrBoletosAviso09: TWideStringField;
    QrBoletosAviso10: TWideStringField;
    QrBoletosAvisoVerso: TWideStringField;
    QrProt1PreEmeio: TIntegerField;
    QrProt1NaoEnvBloq: TIntegerField;
    DBGradeBlo2: TdmkDBGridZTO;
    DBGradeBlo: TdmkDBGridZTO;
    DBGradeBloIts: TdmkDBGridZTO;
    DBGradePreBlo: TdmkDBGridZTO;
    DBGradePreBloIts: TdmkDBGridZTO;
    LaPesBol: TLabel;
    LaMesBol: TLabel;
    LaMesPreBol: TLabel;
    Panel33: TPanel;
    Panel21: TPanel;
    DBGradeBloIts2: TdmkDBGridZTO;
    GroupBox7: TGroupBox;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Splitter10: TSplitter;
    QrLeiBol: TmySQLQuery;
    DsLeiBol: TDataSource;
    QrLeiBolID_Link: TLargeintField;
    QrLeiBolOcorrCodi: TWideStringField;
    QrLeiBolOcorrData: TDateField;
    QrLeiBolValTitul: TFloatField;
    QrLeiBolValPago: TFloatField;
    QrLeiBolValJuros: TFloatField;
    QrLeiBolValMulta: TFloatField;
    QrLeiBolValJuMul: TFloatField;
    QrLeiBolMotivo1: TWideStringField;
    QrLeiBolMotivo2: TWideStringField;
    QrLeiBolMotivo3: TWideStringField;
    QrLeiBolMotivo4: TWideStringField;
    QrLeiBolMotivo5: TWideStringField;
    QrLeiBolValTarif: TFloatField;
    QrLeiBolDtaTarif: TDateField;
    QrLeiBolEhLiquida: TSmallintField;
    QrLeiBolOCOR_TXT: TWideStringField;
    QrLeiBolTamReg: TIntegerField;
    QrLeiBolDiretorio: TIntegerField;
    QrLeiBolModalCobr: TSmallintField;
    QrLeiBolBanco: TIntegerField;
    QrLeiBolCNAB: TIntegerField;
    BtTodosMens: TBitBtn;
    BtNenhumMens: TBitBtn;
    BtTodosPesq: TBitBtn;
    BtNenhumPesq: TBitBtn;
    TBProtocolos: TTabControl;
    BtPEBNenhum: TBitBtn;
    QrLoc: TmySQLQuery;
    QrBoletosTotIts: TFloatField;
    QrBoletosCedente: TIntegerField;
    BtPEBEnvia: TBitBtn;
    BtPEBTodos: TBitBtn;
    frxPDFExport: TfrxPDFExport;
    QrLeiBolLayoutRem: TWideStringField;
    GroupBox8: TGroupBox;
    dmkDBGridZTO2: TdmkDBGridZTO;
    LaProBol: TLabel;
    QrBoletosEnvios: TFloatField;
    Gerenciar1: TMenuItem;
    Gerenciar2: TMenuItem;
    N6: TMenuItem;
    TBGerados: TTabControl;
    Splitter6: TSplitter;
    DBGrid1: TDBGrid;
    DBG_ProtocoBol: TdmkDBGridZTO;
    QrBoletosGenero: TIntegerField;
    QrBoletosItsGenero: TIntegerField;
    PCArrecada: TPageControl;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    Panel8: TPanel;
    Splitter2: TSplitter;
    LaMesArr: TLabel;
    DBGradeArre: TdmkDBGridZTO;
    DBGradeArreBlo: TdmkDBGridZTO;
    Splitter3: TSplitter;
    Panel9: TPanel;
    DBGradeArreIts: TdmkDBGridZTO;
    DBGradeArreFutt: TDBGrid;
    QrArreFutI: TmySQLQuery;
    QrArreFutIControle: TIntegerField;
    QrArreFutIConta: TIntegerField;
    QrArreFutIPeriodo: TIntegerField;
    QrArreFutIValor: TFloatField;
    QrArreFutITexto: TWideStringField;
    QrArreFutILk: TIntegerField;
    QrArreFutIDataCad: TDateField;
    QrArreFutIDataAlt: TDateField;
    QrArreFutIUserCad: TIntegerField;
    QrArreFutIUserAlt: TIntegerField;
    QrArreFutINOMECONTA: TWideStringField;
    QrArreFutIPERIODO_TXT: TWideStringField;
    QrArreFutICNAB_Cfg: TIntegerField;
    QrArreFutIDepto: TIntegerField;
    QrArreFutICliInt: TIntegerField;
    DsArreFutI: TDataSource;
    QrArreFutA: TmySQLQuery;
    QrArreFutANOMECONTA: TWideStringField;
    QrArreFutAControle: TIntegerField;
    QrArreFutAConta: TIntegerField;
    QrArreFutAPeriodo: TIntegerField;
    QrArreFutAValor: TFloatField;
    QrArreFutATexto: TWideStringField;
    QrArreFutALk: TIntegerField;
    QrArreFutADataCad: TDateField;
    QrArreFutADataAlt: TDateField;
    QrArreFutAUserCad: TIntegerField;
    QrArreFutAUserAlt: TIntegerField;
    QrArreFutAPERIODO_TXT: TWideStringField;
    QrArreFutAInclui: TIntegerField;
    QrArreFutACNAB_Cfg: TIntegerField;
    QrArreFutADepto: TIntegerField;
    DsArreFutA: TDataSource;
    QrArreFutINOMEENT: TWideStringField;
    QrArreFutANOMEENT: TWideStringField;
    PMArreFut: TPopupMenu;
    Incluiarrecadaofutura1: TMenuItem;
    Alteraarrecadaofutura1: TMenuItem;
    Excluiarrecadaofutura1: TMenuItem;
    QrArreFutACartEmiss: TIntegerField;
    QrBoletosItsSerieNF: TWideStringField;
    QrBoletosItsNotaFiscal: TFloatField;
    QrArreFutINFSeSrvCad: TIntegerField;
    QrArreFutINFSeSrvCad_TXT: TWideStringField;
    QrArreItsNFSeSrvCad: TIntegerField;
    QrArreItsNFSeSrvCad_TXT: TWideStringField;
    QrArreFutANFSeSrvCad: TIntegerField;
    N9: TMenuItem;
    GerarNFSeparaosboletosselecionados1: TMenuItem;
    N10: TMenuItem;
    GerarNFSeparaosboletosselecionados2: TMenuItem;
    QrBoletosItsNFSeSrvCad: TIntegerField;
    QrBoletosItsBloArreIts: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtArrecadaClick(Sender: TObject);
    procedure BtPrebolClick(Sender: TObject);
    procedure BtBoleto1Click(Sender: TObject);
    procedure Incluiitensbasedearrecadao1Click(Sender: TObject);
    procedure Novoitemdearrecadao1Click(Sender: TObject);
    procedure Alteraarrecadao1Click(Sender: TObject);
    procedure Itemselecionado1Click(Sender: TObject);
    procedure ODOSitensdearrecadao1Click(Sender: TObject);
    procedure Gerartodosabertos1Click(Sender: TObject);
    procedure Gerarselecionados1Click(Sender: TObject);
    procedure Selecionados3Click(Sender: TObject);
    procedure Todos3Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure QrPrevAfterScroll(DataSet: TDataSet);
    procedure QrPrevBeforeClose(DataSet: TDataSet);
    procedure QrPrevCalcFields(DataSet: TDataSet);
    procedure PMArrecadaPopup(Sender: TObject);
    procedure PMPrebolPopup(Sender: TObject);
    procedure PMBoletoPopup(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure PmImprimePopup(Sender: TObject);
    procedure ImprimeBloqTodosClick(Sender: TObject);
    procedure ImprimeBloqSelClick(Sender: TObject);
    procedure QrPrevAfterClose(DataSet: TDataSet);
    procedure QrPrevAfterOpen(DataSet: TDataSet);
    procedure DosboletosSelecionados1Click(Sender: TObject);
    procedure DeTodosboletos1Click(Sender: TObject);
    procedure DosboletosSelecionados2Click(Sender: TObject);
    procedure DeTodosboletos2Click(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure TravaDestrClick(Sender: TObject);
    procedure BtOrcClick(Sender: TObject);
    procedure BtProtocoloClick(Sender: TObject);
    procedure BtOpcoesClick(Sender: TObject);
    procedure BtRetornoClick(Sender: TObject);
    procedure Gerartodooperodo1Click(Sender: TObject);
    procedure QrArreAfterScroll(DataSet: TDataSet);
    procedure QrArreBeforeClose(DataSet: TDataSet);
    procedure QrArreBolAfterScroll(DataSet: TDataSet);
    procedure QrArreBolBeforeClose(DataSet: TDataSet);
    procedure QrBoletosAfterScroll(DataSet: TDataSet);
    procedure QrBoletosBeforeClose(DataSet: TDataSet);
    procedure QrBoletosCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure Gerartodooperodo2Click(Sender: TObject);
    procedure BtBoleto2Click(Sender: TObject);
    procedure AlteraVencimentoSelecionados1Click(Sender: TObject);
    procedure Desfazerboletosselecionados1Click(Sender: TObject);
    procedure Editar1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure EditarprotocolodeentregaSelecionados1Click(Sender: TObject);
    procedure BtBloArreClick(Sender: TObject);
    procedure BtCNABCfgClick(Sender: TObject);
    procedure BtProPadClick(Sender: TObject);
    procedure BtProtoGerClick(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure PMOrcPopup(Sender: TObject);
    procedure PMBoleto2Popup(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure EdFatID_CodKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFatID_CodChange(Sender: TObject);
    procedure BtAvanca1Click(Sender: TObject);
    procedure BtVolta2Click(Sender: TObject);
    procedure BtAvanca2Click(Sender: TObject);
    procedure BtVolta3Click(Sender: TObject);
    procedure BtAvanca3Click(Sender: TObject);
    procedure BtVolta4Click(Sender: TObject);
    procedure T(DataSet: TDataSet);
    procedure BitBtn3Click(Sender: TObject);
    procedure Modificarlayoutdobloqueto1Click(Sender: TObject);
    procedure QrProt1BeforeClose(DataSet: TDataSet);
    procedure QrProt1AfterScroll(DataSet: TDataSet);
    procedure BtPEBProtocoloClick(Sender: TObject);
    procedure PMProtocoloPopup(Sender: TObject);
    procedure Localizaprotocolo1Click(Sender: TObject);
    procedure Desfazprotocolo1Click(Sender: TObject);
    procedure Todosabertos1Click(Sender: TObject);
    procedure Abertosseleciondos1Click(Sender: TObject);
    procedure BtPEBVoltarClick(Sender: TObject);
    procedure BtPEBEnviaClick(Sender: TObject);
    procedure BtAPVoltarClick(Sender: TObject);
    procedure BtAPArreTodosClick(Sender: TObject);
    procedure BtAPArreNenhumClick(Sender: TObject);
    procedure BtAPItsTodosClick(Sender: TObject);
    procedure BtAPItsNenhumClick(Sender: TObject);
    procedure BtAPCarregaClick(Sender: TObject);
    procedure dmkDBGrid2CellClick(Column: TColumn);
    procedure dmkDBGrid3CellClick(Column: TColumn);
    procedure BtAPGeraClick(Sender: TObject);
    procedure Alteravalor1Click(Sender: TObject);
    procedure Alteratextodaarrecadaoatual1Click(Sender: TObject);
    procedure DBGradeBlo2DblClick(Sender: TObject);
    procedure QrArreAfterOpen(DataSet: TDataSet);
    procedure QrBoletosAfterOpen(DataSet: TDataSet);
    procedure QrLeiBolCalcFields(DataSet: TDataSet);
    procedure BtTodosMensClick(Sender: TObject);
    procedure BtTodosPesqClick(Sender: TObject);
    procedure BtNenhumPesqClick(Sender: TObject);
    procedure BtNenhumMensClick(Sender: TObject);
    procedure TBProtocolosChange(Sender: TObject);
    procedure DBG_ProtocoBolDblClick(Sender: TObject);
    procedure BtPEBTodosClick(Sender: TObject);
    procedure BtPEBNenhumClick(Sender: TObject);
    procedure DBGradeBloDblClick(Sender: TObject);
    procedure Gerenciar1Click(
      Sender: TObject);
    procedure Gerenciar2Click(
      Sender: TObject);
    procedure TBGeradosChange(Sender: TObject);
    procedure QrArreFutICalcFields(DataSet: TDataSet);
    procedure QrArreFutACalcFields(DataSet: TDataSet);
    procedure Incluiarrecadaofutura1Click(Sender: TObject);
    procedure Alteraarrecadaofutura1Click(Sender: TObject);
    procedure Excluiarrecadaofutura1Click(Sender: TObject);
    procedure PMArreFutPopup(Sender: TObject);
    procedure PCArrecadaChange(Sender: TObject);
    procedure GerarNFSeparaosboletosselecionados1Click(Sender: TObject);
    procedure GerarNFSeparaosboletosselecionados2Click(Sender: TObject);
  private
    { Private declarations }
    FOrigem: TModoVisualizacaoBloquetos;
    FBloArre, FBloArreAll: String;
    FFatIdTxt: MyArrayLista;
    procedure ConfiguraJanela(Modo: TModoVisualizacaoBloquetos; Aba: Integer;
                ReconfiguraData: Boolean = True);
    procedure TravaDestPeriodo(Encerrado: Integer; Liberar: Boolean);
    procedure Incluiintensbasedearrecadao();
    procedure MostraBloGerenArreEnt(Tipo: TSQLType);
    procedure GeraBloq(Tipo: TSelType);
    procedure AlteraVencimentoPrebol(Tipo: TselType);
    procedure DesfazerBoletos(Tipo: TSelType; DBGradeBol: TdmkDBGridZTO;
                Modo: TModoVisualizacaoBloquetos);
    procedure AlteraVencimentoBoleto(Tipo: TselType; DBGradeBol: TdmkDBGridZTO;
                Modo: TModoVisualizacaoBloquetos);
    procedure HabilitaBotoes;
    procedure MostraBloGerenNew(SQLType: TSQLType; CliInt: Integer;
                Modo: TModoVisualizacaoBloquetos; DBGrid: TDBGrid;
                QueryBoletos: TmySQLQuery);
    procedure ConfiguraAbas(Aba: Integer);
    procedure ReopenProt1(AbaPro, AbaGerados: Integer);
    procedure ConfiguraBotoes(Aba: Integer);
    procedure AtivDesatBotoesArrePeri(Ativa: Boolean);
    procedure ConfiguraTotalDeItensDeBoletos();
    procedure ReopenProtocoBol();
    procedure GradeBoletosDBClick(Sender: TObject; Modo: TModoVisualizacaoBloquetos);
    procedure MostraPreEnPrGer(Modo: TModoVisualizacaoBloquetos);
    procedure GeraNFSeBoletos(Grade: TDBGrid);
  public
    { Public declarations }
    FBolItsSim, FBolItsNao, FQrBloquetosFiltros: String;
    procedure ReopenBoletos(Modo: TModoVisualizacaoBloquetos; Tipo, Controle: Integer);
    procedure ReopenArre(Entidade: Integer);
    function Va(Para: TVaiPara): Boolean;
    procedure DefParams;
    procedure LocPeriodo(Atual, Periodo: Integer);
    procedure ReopenArreBol(Boleto: Integer);
    procedure ReopenArreIts(Controle: Integer);
    procedure ReopenBoletosIts();
    function  ImprimeBloq(Quais: TselType; Como: TfrxImpComo; Arquivo: String;
                Filtro: TfrxCustomExportFilter): TfrxReport;
  end;

  var
    FmBloGeren: TFmBloGeren;
  const
    FCaminhoEMail = CO_DIR_RAIZ_DMK + '\EMails\Bloquetos\';

implementation

uses
  {$IfNDef NO_USE_EMAILDMK} UnMailEnv, {$EndIf}
  {$IfNDef SemProtocolo} Protocolo, UnProtocoUnit, {$EndIf}
  {$IfDef TEM_DBWEB} UnDmkWeb, {$EndIf}
  {$IfNDef sNFSe}NFSe_PF_0000, {$EndIf}
  ModuleBloGeren, Principal, Module, ModuleGeral, GetValor, BloGerenBaA,
  BloGerenArreEnt, MyDBCheck, UnGotoy, UMySQLModule, UnBco_Rem, UnBancos,
  UCreate, UnMyObjects, ModuleBco, MyGlyfs, MeuFrx, MyListas,
  BloGerenNew, UnFinanceiro, UnitMD5, UnBloquetos, UnBloqGerl_Jan;

{$R *.DFM}

procedure TFmBloGeren.Abertosseleciondos1Click(Sender: TObject);
begin
  {$IfNDef SemProtocolo}
    try
      TBProtocolos.Enabled   := False;
      BtPEBProtocolo.Enabled := False;
      //
      UnProtocolo.GeraProtocoloBoleto(istSelecionados, DBloGeren.QrProtocoBol,
        Dmod.QrUpd, DBloGeren.QrLoc, Dmod.MyDB, TDBGrid(DBG_ProtocoBol), PB1,
        VAR_ModBloq_EntCliInt);
      //
      ReopenProtocoBol;
    finally
      TBProtocolos.Enabled   := True;
      BtPEBProtocolo.Enabled := True;
    end;
  {$Else}
    dmkPF.InfoSemModulo(mdlappProtocolos);
  {$EndIf}
end;

procedure TFmBloGeren.Altera1Click(Sender: TObject);
begin
  MostraBloGerenNew(stUpd, VAR_ModBloq_EntCliInt, istVisMensal, nil, nil);
end;

procedure TFmBloGeren.Alteraarrecadao1Click(Sender: TObject);
begin
  MostraBloGerenArreEnt(stUpd);
end;

procedure TFmBloGeren.Alteraarrecadaofutura1Click(Sender: TObject);
begin
  UBloqGerl_Jan.MostraFmCondGerArreFut(stUpd, VAR_ModBloq_CliInt, VAR_ModBloq_EntCliInt, QrArreFutI);
  UBloquetos.ReopenArreFut(QrArreFutI, VAR_ModBloq_CliInt, 0, QrArreFutIControle.Value);
end;

procedure TFmBloGeren.Alteratextodaarrecadaoatual1Click(Sender: TObject);
var
  Controle: Integer;
  TabLct: String;
begin
  {$IFDEF DEFINE_VARLCT}
    TabLct := VAR_ModBloq_TabLctA;
  {$ELSE}
    TabLct := VAR_LCT;
  {$ENDIF}
  Controle := QrBoletosItsControle.Value;
  //
  UBloquetos.AteraDescricaoDeBoletoIts(QrBoletosCodigo.Value,
    VAR_ModBloq_EntCliInt, Controle, QrBoletosItsLancto.Value,
    QrBoletosCNAB_Cfg.Value, QrBoletosEntidade.Value, QrBoletosItsBoleto.Value,
    QrBoletosItsTEXTO.Value, TabLct);
  ReopenBoletosIts;
  ReopenBoletos(istVisPesquisa, 0, Controle);
end;

procedure TFmBloGeren.Alteravalor1Click(Sender: TObject);
var
  Controle: Integer;
  TabLct: String;
begin
  {$IFDEF DEFINE_VARLCT}
    TabLct := VAR_ModBloq_TabLctA;
  {$ELSE}
    TabLct := VAR_LCT;
  {$ENDIF}
  Controle := QrBoletosItsControle.Value;
  //
  UBloquetos.AlteraValorDeBoletoIts(QrBoletosCodigo.Value, VAR_ModBloq_EntCliInt,
    Controle, QrBoletosItsLancto.Value, QrBoletosCNAB_Cfg.Value,
    QrBoletosEntidade.Value, QrBoletosItsVALOR.Value, QrBoletosItsBoleto.Value,
    TabLct);
  ReopenBoletosIts;
  ReopenBoletos(istVisPesquisa, 0, Controle);
end;

procedure TFmBloGeren.AlteraVencimentoBoleto(Tipo: TselType;
  DBGradeBol: TdmkDBGridZTO; Modo: TModoVisualizacaoBloquetos);
var
  TabLct: String;
  Entidade, Controle: Integer;
begin
  Entidade := QrBoletosItsEntidade.Value;
  Controle := QrBoletosItsControle.Value;
  //
  {$IFDEF DEFINE_VARLCT}
    TabLct := VAR_ModBloq_TabLctA;
  {$ELSE}
    TabLct := VAR_LCT;
  {$ENDIF}
  //
  if UBloquetos.AlteraVencimentoBoleto(Tipo, PB1, TDBGrid(DBGradeBol),
    QrBoletos, TabLct) then
  begin
    ReopenArre(Entidade);
    //
    ReopenBoletos(Modo, PageControl1.ActivePageIndex, Controle);
  end;
end;

procedure TFmBloGeren.AlteraVencimentoPrebol(Tipo: TselType);
var
  Aba: Integer;
begin
  if UBloquetos.AlteraVencimentoPrebol(Tipo, PB1, TDBGrid(DBGradePreBlo),
    QrBoletos) then
  begin
    Aba := PageControl1.ActivePageIndex;
    ReopenBoletos(istVisMensal, Aba, QrBoletosControle.Value);
  end;
end;

procedure TFmBloGeren.AlteraVencimentoSelecionados1Click(Sender: TObject);
begin
  AlteraVencimentoBoleto(istSelecionados, DBGradeBlo2, istVisPesquisa);
end;

procedure TFmBloGeren.BtAPVoltarClick(Sender: TObject);
begin
  ConfiguraJanela(FOrigem, PageControl1.ActivePageIndex);
end;

function TFmBloGeren.ImprimeBloq(Quais: TselType; Como: TfrxImpComo;
  Arquivo: String; Filtro: TfrxCustomExportFilter): TfrxReport;
var
  TabLct: String;
  Grade: TDBGrid;
begin
  {$IFDEF DEFINE_VARLCT}
    TabLct := VAR_ModBloq_TabLctA;
  {$ELSE}
    TabLct := VAR_LCT;
  {$ENDIF}
  //
  if PageControl2.ActivePageIndex = 0 then
  begin
    case PageControl1.ActivePageIndex of
      2: Grade := TDBGrid(DBGradePreBlo);
      3: Grade := TDBGrid(DBGradeBlo);
      else Grade := nil;
    end;
  end else
    Grade := TDBGrid(DBGradeBlo2);
  //
  Result := UBloquetos.ImprimeBloqueto(CkZerado.Checked, TabLct, QrBoletos,
              QrBoletosIts, frxDsBoletos, frxDsBoletosIts, Quais, Como, Arquivo,
              Filtro, Grade);
end;

procedure TFmBloGeren.ImprimeBloqSelClick(Sender: TObject);
begin
  if DBGradeBlo.SelectedRows.Count > 1 then
  begin
    ImprimeBloq(istSelecionados, ficMostra, '', nil);
  end else
  begin
    ImprimeBloq(istAtual, ficMostra, '', nil);
  end;
end;

procedure TFmBloGeren.ImprimeBloqTodosClick(Sender: TObject);
begin
  ImprimeBloq(istTodos, ficMostra, '', nil);
end;

procedure TFmBloGeren.BtPEBVoltarClick(Sender: TObject);
begin
  ConfiguraJanela(FOrigem, PageControl1.ActivePageIndex, False);
end;

procedure TFmBloGeren.BtAPArreNenhumClick(Sender: TObject);
begin
  if (DBloGeren.QrArre.State <> dsInactive) and (DBloGeren.QrArre.RecordCount > 0) then
    DBloGeren.AtualizaTodosArrPeri(FBloArreAll, 0, False);
end;

procedure TFmBloGeren.BitBtn2Click(Sender: TObject);
var
  Periodo: Integer;
begin
  Periodo := UBloqGerl_Jan.LocalizarPeriodoAtual(VAR_ModBloq_EntCliInt, 'prev');
  //
  if Periodo <> 0 then
    LocPeriodo(Periodo, Periodo);
end;

procedure TFmBloGeren.BitBtn3Click(Sender: TObject);
begin
  if DBGradeBlo2.SelectedRows.Count > 1 then
  begin
    ImprimeBloq(istSelecionados, ficMostra, '', nil);
  end else
  begin
    ImprimeBloq(istAtual, ficMostra, '', nil);
  end;
end;

procedure TFmBloGeren.BitBtn4Click(Sender: TObject);
begin
  FOrigem := istVisPesquisa;
  //
  ConfiguraJanela(istVisProtEnt, 2);
end;

procedure TFmBloGeren.BtBoleto2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBoleto2, BtBoleto2);
end;

procedure TFmBloGeren.AtivDesatBotoesArrePeri(Ativa: Boolean);
begin
  BtAPCarrega.Enabled    := Ativa;
  BtAPGera.Enabled       := Ativa;
  BtAPVoltar.Enabled     := Ativa;
  BtAPArreTodos.Enabled  := Ativa;
  BtAPArreNenhum.Enabled := Ativa;
  BtAPItsTodos.Enabled   := Ativa;
  BtAPItsNenhum.Enabled  := Ativa;
end;

procedure TFmBloGeren.BtAPCarregaClick(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    AtivDesatBotoesArrePeri(False);
    //
    if VAR_ModBloq_EntCliInt <> 0 then
    begin
      DBloGeren.ConfiguraArrecadacoes(VAR_ModBloq_EntCliInt, PB1, FBloArreAll);
      DBloGeren.AtualizaTodosArrPeri(FBloArreAll, 0, True);
      DBloGeren.ReopenArre(0, FBloArreAll);
    end;
  finally
    AtivDesatBotoesArrePeri(True);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBloGeren.BtAPGeraClick(Sender: TObject);
{$IfNDef SemProtocolo}
var
  Prev, Periodo, DiaVcto, ProtocoloCR, Lancto,
  LoteProtoCR, Controle: Integer;
  NumBloq: Double;
  Vencto: TDateTime;
  TabLct, Msg: String;
  Finalizou, VerificouProtocoloCR: Boolean;
begin
  Finalizou            := False;
  ProtocoloCR          := 0;
  LoteProtoCR          := 0;
  VerificouProtocoloCR := False;
  //
  if (DBloGeren.QrArreIts.State = dsInactive) or (DBloGeren.QrArreIts.RecordCount = 0) then Exit;
  //
  try
    Screen.Cursor      := crHourGlass;
    TabSheet13.Enabled := False;
    PB1.Position       := 0;
    PB1.Max            := DBloGeren.QrArre.RecordCount;
    Lancto             := 0;
    AtivDesatBotoesArrePeri(False);
    //
    DBloGeren.QrArre.First;
    while not DBloGeren.QrArre.Eof do
    begin
      if DBloGeren.QrArreAdiciona.Value = 1 then
      begin
        DBloGeren.QrArreIts.First;
        while not DBloGeren.QrArreIts.Eof do
        begin
          //Insere lan�amentos financeiros
          MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arrecada��es!');
          if DBloGeren.QrArreItsAdiciona.Value = 1 then
          begin
            if VerificouProtocoloCR = False then
            begin
              ProtocoloCR := UnProtocolo.LocalizaProtocoloCR(Dmod.MyDB, DBloGeren.QrArreItsCNAB_Cfg.Value);
              //
              if ProtocoloCR <> 0 then
                LoteProtoCR := UnProtocolo.GeraLoteProtocolo(Dmod.MyDB, ProtocoloCR,
                                 DBloGeren.QrArreItsCNAB_Cfg.Value, 4);
              //
              VerificouProtocoloCR := True;
            end;
            //
            Periodo := DBloGeren.QrArreItsPeriodo.Value;
            DiaVcto := DBloGeren.QrArreItsDiaVencto.Value;
            Vencto  := Geral.PeriodoToDate(Periodo, DiaVcto, True);
            Vencto  := UBloquetos.AjustaVencimentoPeriodo(Periodo, Vencto);
            //
            {$IfDef DEFINE_VARLCT}
            TabLct := VAR_ModBloq_TabLctA;
            {$Else}
            TabLct := VAR_LANCTO;
            {$EndIf}
            //
            //Verifica se per�odo existe!
            Prev := UBloquetos.LocalizaPrev(VAR_ModBloq_EntCliInt,
              DBloGeren.QrArreItsPeriodo.Value);
            //
            if Prev = 0 then
            begin
              DBloGeren.ReopenBloOpcoes;
              //
              Prev := UBloquetos.AtualizaBloGerenNew(stIns, 0, 0,
                VAR_ModBloq_EntCliInt, DBloGeren.QrArreItsPeriodo.Value,
                DBloGeren.QrBloOpcoes.FieldByName('ModelBloq').AsInteger,
                DBloGeren.QrBloOpcoes.FieldByName('Compe').AsInteger,
                DBloGeren.QrBloOpcoes.FieldByName('BloqFV').AsInteger,
                '', '', '', '', '', '', '', '', '', '', '', Msg);
              if Prev = 0 then
              begin
                Geral.MB_Aviso(Msg);
                Exit;
              end;
            end;
            //Gera as arrecada��es
            Controle := UBloquetos.AtualizaArrecadacao(stIns, Prev, 0,
                          DBloGeren.QrArreItsConta.Value,
                          DBloGeren.QrArreItsBloArre.Value,
                          DBloGeren.QrArreItsBloArreIts.Value,
                          DBloGeren.QrArreItsEntidade.Value,
                          DBloGeren.QrArreItsCartEmiss.Value,
                          DBloGeren.QrArreItsCNAB_Cfg.Value,
                          DBloGeren.QrArreItsNFSeSrvCad.Value,
                          Lancto, 0, 0, 0, 0, Geral.FDT(Vencto, 1),
                          DBloGeren.QrArreItsValor.Value, 0, 0,
                          DBloGeren.QrArreItsTextoTmp.Value, Msg);
            if Controle = 0 then
            begin
              Geral.MB_Aviso(Msg);
              Exit;
            end else
            begin
              if not UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DmodG.MyPID_DB, [
                'UPDATE ' + FBloArreAll + ' SET IDArre=' + Geral.FF0(Controle),
                'WHERE Controle=' + Geral.FF0(DBloGeren.QrArreItsControleTmp.Value),
                '']) then
              begin
                Geral.MB_Erro('Falha ao atualizar dados tempor�rios!');
                Exit;
              end;
            end;
          end;
          DBloGeren.QrArreIts.Next;
        end;
        DBloGeren.ReopenArreIts(0);
        //
        DBloGeren.QrArreIts.First;
        while not DBloGeren.QrArreIts.Eof do
        begin
          if DBloGeren.QrArreItsAdiciona.Value = 1 then
          begin
            //Gera o n�mero do boleto e os lan�amentos financeiros
            MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando boleto e tamb�m os lan�amentos financeiros!');
            //
            NumBloq := UBloqGerl.GeraBloquetoAtual(DBloGeren.QrArreItsPeriodo.Value,
                         VAR_ModBloq_EntCliInt, Vencto, True, VAR_ModBloq_TabLctA,
                         VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA, DBloGeren.QrArre,
                         DBloGeren.QrArreIts, Dmod.MyDB, True, ProtocoloCR);
            //
            //Insere protoclo CR se tiver
            if (ProtocoloCR <> 0) and (LoteProtoCR <> 0) then
            begin
              UBloquetos.GeraProtocoloCR(ProtocoloCR, LoteProtoCR,
                DBloGeren.QrArreItsEntidade.Value, DBloGeren.QrArreItsCodigo.Value,
                DBloGeren.QrArreItsCNAB_Cfg.Value, DBloGeren.QrArreItsValor.Value,
                NumBloq, Geral.FDT(Vencto, 1));
            end;
          end;
          DBloGeren.QrArreIts.Next;
        end;
      end;
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      DBloGeren.QrArre.Next;
    end;
    Finalizou := True;
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    AtivDesatBotoesArrePeri(True);
    TabSheet13.Enabled := True;
    PB1.Position       := 0;
    Screen.Cursor      := crDefault;
    //
    if Finalizou then
      ConfiguraJanela(FOrigem, PageControl1.ActivePageIndex);
  end;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TFmBloGeren.BtCNABCfgClick(Sender: TObject);
begin
  UBloquetos.CadastroCNAB_Cfg;
  //
  DmBco.QrCNAB_Cfg.Close;
  DmBco.QrCNAB_Cfg.Open;
end;

procedure TFmBloGeren.BitBtn5Click(Sender: TObject);
begin
  UBloqGerl_Jan.MostraBloCNAB_Ret;
end;

procedure TFmBloGeren.BitBtn6Click(Sender: TObject);
begin
  UBloquetos.MostraBloGerenInadImp(FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
end;

procedure TFmBloGeren.BtAPItsTodosClick(Sender: TObject);
begin
  if (DBloGeren.QrArreIts.State <> dsInactive) and (DBloGeren.QrArreIts.RecordCount > 0) then
    DBloGeren.AtualizaTodosArrPeri(FBloArreAll, DBloGeren.QrArreBloArreIts.Value, True);
end;

procedure TFmBloGeren.BtAPItsNenhumClick(Sender: TObject);
begin
  if (DBloGeren.QrArreIts.State <> dsInactive) and (DBloGeren.QrArreIts.RecordCount > 0) then
    DBloGeren.AtualizaTodosArrPeri(FBloArreAll, DBloGeren.QrArreBloArreIts.Value, False);
end;

procedure TFmBloGeren.BtAPArreTodosClick(Sender: TObject);
begin
  if (DBloGeren.QrArre.State <> dsInactive) and (DBloGeren.QrArre.RecordCount > 0) then 
    DBloGeren.AtualizaTodosArrPeri(FBloArreAll, 0, True);
end;

procedure TFmBloGeren.BtArrecadaClick(Sender: TObject);
begin
  if PCArrecada.ActivePageIndex = 0 then
    MyObjects.MostraPopUpDeBotao(PMArrecada, BtArrecada)
  else
    MyObjects.MostraPopUpDeBotao(PMArreFut, BtArrecada);
end;

procedure TFmBloGeren.BtAvanca1Click(Sender: TObject);
begin
  ConfiguraAbas(1);
end;

procedure TFmBloGeren.BtAvanca2Click(Sender: TObject);
begin
  ConfiguraAbas(2);
end;

procedure TFmBloGeren.BtAvanca3Click(Sender: TObject);
begin
  ConfiguraAbas(3);
end;

procedure TFmBloGeren.BtBloArreClick(Sender: TObject);
begin
  UBloquetos.MostraBloArre(0, 0);
end;

procedure TFmBloGeren.BtBoleto1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBoleto, BtBoleto1);
end;

procedure TFmBloGeren.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PmImprime, BtImprime);
end;

procedure TFmBloGeren.BtNenhumMensClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGradeBlo), False);
end;

procedure TFmBloGeren.BtNenhumPesqClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGradeBlo2), False);
end;

procedure TFmBloGeren.BtOpcoesClick(Sender: TObject);
var
  Confirmou: Boolean;
begin
  Confirmou := UBloquetos.CadastroDeBloOpcoes();
  //
  DBloGeren.ReopenBloOpcoes;
  //
  if Confirmou then
  begin
    if TFmBloGeren(Self).Owner is TApplication then
      Close
    else
      MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
  end;
end;

procedure TFmBloGeren.BtOrcClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOrc, BtOrc);
end;

procedure TFmBloGeren.BtPEBEnviaClick(Sender: TObject);
{$IfNDef SemProtocolo}
{$IfDef TEM_DBWEB}
  function EnviaEmail(Anexo: String): Boolean;
  var
    ParamLink, ParamTopo, Email: String;
    Cliente, Protocolo: Integer;
  begin
    Email     := DBloGeren.QrProtocoBolEmail.Value;
    Cliente   := DBloGeren.QrProtocoBolEntidade.Value;
    Protocolo := DBloGeren.QrProtocoBolPROTOCOLO.Value;

    UnProtocolo.Proto_Email_GeraLinksConfirmacaoReceb(Dmod.MyDB,
      DmodG.QrOpcoesGerl.FieldByName('Web_MyURL').AsString,
      DmodG.QrOpcoesGerl.FieldByName('WebId').AsString, Email,
      Cliente, Protocolo, CO_DMKID_APP, ParamLink, ParamTopo);

    Application.ProcessMessages;

    Result := UMailEnv.Monta_e_Envia_Mail([QrProt1PreEmeio.Value,
                DBloGeren.QrProtocoBolEmail.Value], meBloq, [Anexo],
                [DBloGeren.QrProtocoBolNOMEENT.Value,
                Geral.FFT(DBloGeren.QrProtocoBolValor.Value, 2, siPositivo),
                Geral.FDT(DBloGeren.QrProtocoBolVencto.Value, 2),
                ParamLink, ParamTopo], False);
  end;

  function AtualisaProtoloEmail(): Boolean;
  var
    NaoEnvBloq: Integer;
    Arquivo: String;
    frxModelo: TfrxReport;
  begin
    Result := False;
    //
    if (DBloGeren.QrProtocoBolPROTOCOLO.Value <> 0) then //Tem protocolo
    begin
      NaoEnvBloq := QrProt1.FieldByName('NaoEnvBloq').AsInteger;
      //
      if NaoEnvBloq = 1 then
      begin
        //Reenvio sem bloqueto!
        EnviaEmail('')
      end else
      begin
        Arquivo := FCaminhoEMail + 'Prot_' +
          FormatFloat('000000', DBloGeren.QrProtocoBolPROTOCOD.Value) + '_Bloq_' +
          FormatFloat('000000', DBloGeren.QrProtocoBolBoleto.Value) + '.pdf';
        //
        if FileExists(Arquivo) then
          DeleteFile(Arquivo);
        if not DirectoryExists(ExtractFilePath(Arquivo)) then
        begin
          try
            ForceDirectories(ExtractFilePath(Arquivo));
          except
            Geral.MB_Aviso('N�o foi poss�vel criar o diret�rio: ' + ExtractFilePath(Arquivo) + '!');
            Exit;
          end;
        end;
        if QrBoletos.State = dsInactive then
        begin
          Geral.MB_Aviso('"QrBoletos" n�o est� aberta. ' + sLineBreak +
            'Verifique se o envio de emeio requer anexo de bloqueto e comunique a DERMATEK!' +
            sLineBreak + 'Em caso de re-aviso marque o campo indicativo no cadastro do pr�-e-mail!');
          Exit;
        end;
        if QrBoletos.Locate('Boleto;Entidade;Codigo;Empresa;CNAB_Cfg',
          VarArrayOf([DBloGeren.QrProtocoBolBoleto.Value,
                      DBloGeren.QrProtocoBolEntidade.Value,
                      DBloGeren.QrProtocoBolCodigo.Value,
                      DBloGeren.QrProtocoBolEmpresa.Value,
                      DBloGeren.QrProtocoBolCNAB_Cfg.Value]), []) then
        begin
          frxPDFExport.FileName := Arquivo;
          //
          frxModelo := ImprimeBloq(istAtual, ficExporta, Arquivo, frxPDFExport);
          //
          if frxModelo = nil then
          begin
            DeleteFile(Arquivo);
            //
            Geral.MB_Aviso('Falha ao exportar boleto!');
            Exit;
          end;
          if DBloGeren.QrProtocoBolDataE_Txt.Value = '' then
          begin
            if not UnProtocolo.Proto_Email_CriaProtocoloEnvio(Dmod.MyDBn,
              DBloGeren.QrProtocoBolEntidade.Value,
              DBloGeren.QrProtocoBolPROTOCOLO.Value,
              DBloGeren.QrProtocoBolEmail.Value)
            then
              Exit;
          end;
          //Enviar e-mail
          try
            if EnviaEmail(Arquivo) then
            begin
              if FileExists(Arquivo) then
                DeleteFile(Arquivo);
              //
              Application.ProcessMessages;
              //
              //Alterar status do protocolo para enviado, colocando a data de envio
              if UnProtocolo.Proto_Email_AtualizDataE(Dmod.MyDB,
                DBloGeren.QrProtocoBolPROTOCOLO.Value, DModG.ObtemAgora)
              then
                Result := True
              else
                Exit;
            end;
          except
            Exit;
          end;
        end else
          Geral.MB_Aviso('O Boleto - Entidade ' + Geral.FFI(DBloGeren.QrProtocoBolBoleto.Value)
            + ' n�o foi localizado para ser salvo!');
      end;
    end else
      Result := True;
  end;
var
  Res: Boolean;
  i: Integer;
begin
  if (TBProtocolos.TabIndex <> 1) or (TBGerados.TabIndex <> 1) then Exit; //E-mail
  //
  if (DBloGeren.QrProtocoBol.State = dsInactive) or (DBloGeren.QrProtocoBol.RecordCount = 0) then Exit;
  //
  if not DmkWeb.RemoteConnection then
  begin
    Geral.MB_Aviso('Seu computador n�o est� conectado na internet!' +
      sLineBreak + 'Estabele�a uma conex�o com a internet e tente novamente!');
    Exit;
  end;
  // necess�rio para iserir no servidor protocolos de emeios enviados
  if not DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then Exit;
  //
  Res := False;
  //
  try
    TBProtocolos.Enabled := False;
    BtPEBEnvia.Enabled   := False;
    //
    if DBG_ProtocoBol.SelectedRows.Count > 1 then
    begin
      PB1.Position := 0;
      PB1.Max      := DBG_ProtocoBol.SelectedRows.Count;
      //
      with DBG_ProtocoBol.DataSource.DataSet do
      begin
        for i:= 0 to DBG_ProtocoBol.SelectedRows.Count - 1 do
        begin
          GotoBookmark(pointer(DBG_ProtocoBol.SelectedRows.Items[i]));
          //
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          //
          Res := AtualisaProtoloEmail();
          //
          if not Res then
          begin
            Geral.MB_Aviso('Falha ao enviar e-mail!');
            Exit;
          end;
        end;
      end;
    end else
    begin
      Res := AtualisaProtoloEmail();
      //
      if not Res then
      begin
        Geral.MB_Aviso('Falha ao enviar e-mail!');
        Exit;
      end;
    end;
  finally
    PB1.Position := 0;
    //
    DBloGeren.ReopenQuery(2, QrProt1Codigo.Value, 0, TBGerados.TabIndex, FQrBloquetosFiltros);
    //
    TBProtocolos.Enabled := True;
    BtPEBEnvia.Enabled   := True;
    //
    if Res then
      Geral.MB_Aviso('Envio finalizado!');
  end;
{$Else}
begin
  Geral.MB_Aviso('O controle dos protocolos de recebimento dever� ser feito manualmente!'
    + sLineBreak + 'Motivo: O m�dulo WEB n�o est� habilitado!');
{$EndIf}
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TFmBloGeren.BtPEBNenhumClick(Sender: TObject);
begin
  if (DBloGeren.QrProtocoBol.State <> dsInactive) and
    (DBloGeren.QrProtocoBol.RecordCount > 0) then
  begin
    MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG_ProtocoBol), False);
  end;
end;

procedure TFmBloGeren.BtPEBProtocoloClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProtocolo, BtPEBProtocolo);
end;

procedure TFmBloGeren.BtPEBTodosClick(Sender: TObject);
begin
  if (DBloGeren.QrProtocoBol.State <> dsInactive) and
    (DBloGeren.QrProtocoBol.RecordCount > 0) then
  begin
    MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBG_ProtocoBol), True);
  end;
end;

procedure TFmBloGeren.BtPesqClick(Sender: TObject);
begin
  UBloquetos.MostraBloGerenInadImp(FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
end;

procedure TFmBloGeren.BtPesquisaClick(Sender: TObject);
begin
  ReopenBoletos(istVisPesquisa, 0, 0);
end;

procedure TFmBloGeren.BtPrebolClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPrebol, BtPrebol);
end;

procedure TFmBloGeren.BtSaidaClick(Sender: TObject);
begin
  if TFmBloGeren(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmBloGeren.BtTodosMensClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGradeBlo), True);
end;

procedure TFmBloGeren.BtTodosPesqClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGradeBlo2), True);
end;

procedure TFmBloGeren.BtVolta2Click(Sender: TObject);
begin
  ConfiguraAbas(0);
end;

procedure TFmBloGeren.BtVolta3Click(Sender: TObject);
begin
  ConfiguraAbas(1);
end;

procedure TFmBloGeren.BtVolta4Click(Sender: TObject);
begin
  ConfiguraAbas(2);
end;

procedure TFmBloGeren.ConfiguraAbas(Aba: Integer);
begin
  PageControl1.ActivePageIndex := Aba;
  //
  ConfiguraJanela(istVisMensal, Aba);
end;

procedure TFmBloGeren.ConfiguraBotoes(Aba: Integer);
begin
  BtPEBProtocolo.Visible := True;
  BtPEBTodos.Visible     := True;
  BtPEBNenhum.Visible    := True;
  //
  if Aba = 1 then
  begin
    if TBGerados.TabIndex = 1 then
    begin
      BtPEBEnvia.Enabled := True;
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Para cadastrar um e-mail para a entidade de um duplo clique na linha correspondente!');
    end else
    begin
      BtPEBEnvia.Enabled := False;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end;
    DBG_ProtocoBol.Columns[3].Visible := True;
    DBG_ProtocoBol.Columns[8].Visible := True;
  end else
  begin
    BtPEBEnvia.Enabled := False;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    DBG_ProtocoBol.Columns[3].Visible := False;
    DBG_ProtocoBol.Columns[8].Visible := False;
  end;
end;

procedure TFmBloGeren.ConfiguraJanela(Modo: TModoVisualizacaoBloquetos; Aba: Integer;
  ReconfiguraData: Boolean = True);
var
  Periodo: Integer;
  Hoje: TDateTime;
  Imagem: String;
  ModoSel: TModoVisualizacaoBloquetos;
  EnabData: Boolean;
begin
  DBloGeren.ReopenBloOpcoes;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  FFatIdTxt          := UFinanceiro.ObtemListaNomeFatID(CO_DMKID_APP);
  ModoSel            := Modo;
  Hoje               := DModG.ObtemAgora;
  DBText7.DataSource := DsSumARRE;
  DBText8.DataSource := DsPrev;
  DBText9.DataSource := DsPrev;
  Imagem             := Geral.ReadAppKey('ImagemFundo', Application.Title,
                          ktString, VAR_APP + 'Fundo.jpg', HKEY_LOCAL_MACHINE);
  //
  if FileExists(Imagem) then
    ImgCondGer.Picture.LoadFromFile(Imagem);
  //
  if ModoSel = istVisNenhum then
  begin
    if DBloGeren.QrBloOpcoes.FieldByName('VisPadrao').AsInteger = 0 then
      ModoSel := istVisMensal
    else
      ModoSel := istVisPesquisa;
  end;
  if ModoSel = istVisMensal then
  begin
    PageControl2.ActivePageIndex := 0;
    Panel2.Visible               := True;
    LaRegistro.Visible           := True;
    TabSheet1.TabVisible         := True;
    TabSheet2.TabVisible         := False;
    TabSheet7.TabVisible         := False;
    TabSheet13.TabVisible        := False;
    //
    case Aba of
      0: //Or�amento
      begin
      end;
      1: //Arrecada��es
      begin
        PCArrecada.ActivePageIndex := 0;
        //
        DBGradeArre.DataSource     := DsArre;
        DBGradeArreBlo.DataSource  := DsArreBol;
        DBGradeArreIts.DataSource  := DsArreIts;
        DBGradeArreFutt.DataSource := DsArreFutI;
        //
        ReopenArre(0);
        UBloquetos.ReopenArreFut(QrArreFutI, VAR_ModBloq_CliInt, 0, 0);
      end;
      2: //Pr�-boletos
      begin
        DBGradePreBlo.DataSource    := DsBoletos;
        DBGradePreBloIts.DataSource := DsBoletosIts;
        //
        ReopenBoletos(istVisMensal, Aba, 0);
      end;
      3: //Boletos
      begin
        DBGradeBlo.DataSource    := DsBoletos;
        DBGradeBloIts.DataSource := DsBoletosIts;
        //
        ReopenBoletos(istVisMensal, Aba, 0);
      end;
      else
      begin
        Periodo := UBloquetos.ValidaPeriodo(Geral.Periodo2000(Hoje), True);
        //
        if UBloqGerl.PeriodoExiste(Periodo, 'prev', Dmod.MyDB) = True then
          LocPeriodo(Periodo, Periodo)
        else
          Va(vpLast);
        //
        DBGradeBlo.DataSource    := DsBoletos;
        DBGradeBloIts.DataSource := DsBoletosIts;
        //
        ReopenBoletos(istVisMensal, Aba, 0);
        //
        if (QrBoletos.State <> dsInactive) and (QrBoletos.RecordCount > 0) then
        begin
          PageControl1.ActivePageIndex := 3;
          ConfiguraTotalDeItensDeBoletos();
        end else
          PageControl1.ActivePageIndex := 0;
      end;
    end;
  end else
  if ModoSel = istVisPesquisa then
  begin
    PageControl2.ActivePageIndex := 1;
    Panel2.Visible               := False;
    LaRegistro.Visible           := False;
    TabSheet1.TabVisible         := False;
    TabSheet2.TabVisible         := True;
    TabSheet7.TabVisible         := False;
    TabSheet13.TabVisible        := False;
    //
    DBloGeren.ReoepenCNAB_Cfg;
    DBloGeren.ReopenArrePesq;
    //
    UMyMod.AbreQuery(DBloGeren.QrContas, DMod.MyDB);
    UMyMod.AbreQuery(DBloGeren.QrEntidades, DMod.MyDB);
    //
    EdCNABCfg.ValueVariant  := 0;
    CBCNABCfg.KeyValue      := Null;
    CBCNABCfg.ListSource    := DBloGeren.DsCNABcfg;
    EdConta.ValueVariant    := 0;
    CBConta.KeyValue        := Null;
    CBConta.ListSource      := DBloGeren.DsContas;
    EdArre.ValueVariant     := 0;
    CBArre.KeyValue         := Null;
    CBArre.ListSource       := DBloGeren.DsArrePesq;
    EdEntidade.ValueVariant := 0;
    CBEntidade.KeyValue     := Null;
    CBEntidade.ListSource   := DBloGeren.DsEntidades;
    EdBoleto.ValueVariant   := 0;
    CkValor.Checked         := False;
    EdValorIni.ValueVariant := 0;
    EdValorFim.ValueVariant := 0;
    //
    if ReconfiguraData = True then
    begin
      EnabData := (VAR_ModBloq_FatID = 0) and
                    (VAR_ModBloq_FatNum = 0);
      //
      CkDataEmiIni.Checked    := EnabData;
      TPDataEmiIni.Date       := Geral.PrimeiroDiaDoMes(Hoje);
      CkDataEmiFim.Checked    := EnabData;
      TPDataEmiFim.Date       := Geral.UltimoDiaDoMes(Hoje);
      CkDataVctIni.Checked    := False;
      TPDataVctIni.Date       := Geral.PrimeiroDiaDoMes(Hoje);
      CkDataVctFim.Checked    := False;
      TPDataVctFim.Date       := Geral.PrimeiroDiaDoMes(Hoje);
      //
      if VAR_ModBloq_Lancto = 0 then
        EdLancto.ValueVariant := 0
      else
        EdLancto.ValueVariant := VAR_ModBloq_Lancto;
      if VAR_ModBloq_FatID = 0 then
      begin
        EdFatID_Cod.ValueVariant := 0;
        EdFatID_Txt.Text         := '';
      end else
        EdFatID_Cod.ValueVariant := VAR_ModBloq_FatID;
      //
      if VAR_ModBloq_FatNum = 0 then
        EdFatNum.ValueVariant := 0
      else
        EdFatNum.ValueVariant := VAR_ModBloq_FatNum;
    end;
    DBGradeBlo2.DataSource    := DsBoletos;
    DBGradeBloIts2.DataSource := DsBoletosIts;
    //
    ReopenBoletos(istVisPesquisa, Aba, 0);
  end else
  if ModoSel = istVisProtEnt then
  begin
    PageControl2.ActivePageIndex := 2;
    TBProtocolos.TabIndex        := 0;
    TBGerados.TabIndex           := 0;
    Panel2.Visible               := False;
    LaRegistro.Visible           := False;
    BtPEBProtocolo.Enabled       := True;
    BtPEBEnvia.Enabled           := False;
    LaProBol.Caption             := '';
    //
    DBGrid1.DataSource        := DsProt1;
    DBG_ProtocoBol.DataSource := DBloGeren.DsProtocoBol;
    //
    TabSheet1.TabVisible  := False;
    TabSheet2.TabVisible  := False;
    TabSheet7.TabVisible  := True;
    TabSheet13.TabVisible := False;
    //
    ReopenProt1(0, 0);
  end else
  if ModoSel = istArrPeri then
  begin
    FBloArreAll := UCriar.RecriaTempTableNovo(ntrttBloArreTmp, DModG.QrUpdPID1, False);
    //
    DBloGeren.ReopenArre(0, FBloArreAll);
    //
    PageControl2.ActivePageIndex := 3;
    Panel2.Visible               := False;
    LaRegistro.Visible           := False;
    //
    TabSheet1.TabVisible  := False;
    TabSheet2.TabVisible  := False;
    TabSheet7.TabVisible  := False;
    TabSheet13.TabVisible := True;
    //
    dmkDBGrid3.DataSource := DBloGeren.DsArre;
    dmkDBGrid2.DataSource := DBloGeren.DsArreIts;
  end;
end;

procedure TFmBloGeren.DBGradeBlo2DblClick(Sender: TObject);
begin
  GradeBoletosDBClick(Sender, istVisPesquisa);
end;

procedure TFmBloGeren.DBGradeBloDblClick(Sender: TObject);
begin
  GradeBoletosDBClick(Sender, istVisMensal);
end;

procedure TFmBloGeren.DBG_ProtocoBolDblClick(Sender: TObject);
begin
  if (DBloGeren.QrProtocoBol.State = dsInactive) or (DBloGeren.QrProtocoBol.RecordCount = 0) then Exit;
  //
  if TBProtocolos.TabIndex = 1 then
  begin
    DModG.CadastroDeEntidade(DBloGeren.QrProtocoBolEntidade.Value, fmcadEntidade2, fmcadEntidade2, True);
    //
    DBloGeren.ReopenQuery(2, QrProt1Codigo.Value, 0, TBGerados.TabIndex, FQrBloquetosFiltros);
  end;
end;

procedure TFmBloGeren.DefParams;
begin
  VAR_GOTOTABELA := 'prev';
  VAR_GOTOmySQLTABLE := QrPrev;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := 'Periodo';
  VAR_GOTONOME := '';
  VAR_GOTOMYSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT prv.*, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE');
  VAR_SQLx.Add('ent.Nome END NOMEEMP');
  VAR_SQLx.Add('FROM prev prv');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = prv.Empresa');
  VAR_SQLx.Add('WHERE prv.Empresa='+ FormatFloat('0', VAR_ModBloq_EntCliInt));
  //
  VAR_SQL1.Add('AND prv.Periodo=:P0');
  //
  VAR_SQLa.Add('');//AND prv.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Empresa='+ Geral.FF0(VAR_ModBloq_EntCliInt);
end;

procedure TFmBloGeren.DesfazerBoletos(Tipo: TSelType; DBGradeBol: TdmkDBGridZTO;
  Modo: TModoVisualizacaoBloquetos);
var
  i: Integer;
  ImpedeSePosterior, MostrouMsg: Boolean;
begin
  MostrouMsg := False;
  //
  if Modo = istVisPesquisa then
    ImpedeSePosterior := False
  else
    ImpedeSePosterior := True;
  //
  case Tipo of
    istSelecionados:
      if DBGradeBol.SelectedRows.Count > 1 then
      begin
        if Geral.MB_Pergunta('Tem certeza que deseja desfazer a ' +
          'gera��o dos boletos selecionados?') <> ID_YES then
        begin
          Exit;
        end;
      end else
      if Geral.MB_Pergunta(
      'Tem certeza que deseja desfazer a gera��o do atual?') <> ID_YES then
      begin
        Exit;
      end;
    istTodos:
      if Geral.MB_Pergunta('Tem certeza que deseja desfazer a ' +
        'gera��o de todos boletos?') <> ID_YES then
      begin
        Exit;
      end;
  end;
  try
    PB1.Position       := 0;
    DBGradeBlo.Enabled := False;
    //
    QrBoletos.DisableControls;
    QrBoletosIts.DisableControls;
    //
    case Tipo of
      istSelecionados:
      begin
        if DBGradeBol.SelectedRows.Count > 1 then
        begin
          PB1.Max := DBGradeBol.SelectedRows.Count;
          //
          with DBGradeBol.DataSource.DataSet do
          for i := 0 to DBGradeBol.SelectedRows.Count-1 do
          begin
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
            //
            GotoBookmark(pointer(DBGradeBol.SelectedRows.Items[i]));
            //
            QrBoletosIts.First;
            while not QrBoletosIts.Eof do
            begin
              if not UBloquetos.VerificaSePeriodoEstaEncerrado(QrBoletosCodigo.Value) then
              begin
                if not UBloquetos.DesfazerBoletoAtual(QrBoletosCodigo.Value,
                  VAR_ModBloq_EntCliInt, QrBoletosItsControle.Value,
                  QrBoletosPeriodo.Value, QrBoletosItsLancto.Value,
                  QrBoletosCNAB_Cfg.Value, QrBoletosEntidade.Value,
                  QrBoletosItsAvulso.Value, QrBoletosItsBoleto.Value,
                  VAR_ModBloq_TabLctA, ImpedeSePosterior, Modo, MostrouMsg) then
                begin
                  Geral.MB_Aviso('N�o foi poss�vel desfazer o boleto n�mero ' +
                    Geral.FFI(QrBoletosItsBoleto.Value));
                  Exit;
                end;
              end else
                UBloquetos.MostraMensagemPeriodoEncerrado(QrBoletosBoleto.Value);
              QrBoletosIts.Next;
            end;
          end;
        end else
        begin
          if not UBloquetos.VerificaSePeriodoEstaEncerrado(QrBoletosCodigo.Value) then
          begin
            QrBoletosIts.First;
            while not QrBoletosIts.Eof do
            begin
              if not UBloquetos.DesfazerBoletoAtual(QrBoletosCodigo.Value,
                VAR_ModBloq_EntCliInt, QrBoletosItsControle.Value,
                QrBoletosPeriodo.Value, QrBoletosItsLancto.Value,
                QrBoletosCNAB_Cfg.Value, QrBoletosEntidade.Value,
                QrBoletosItsAvulso.Value, QrBoletosItsBoleto.Value,
                VAR_ModBloq_TabLctA, ImpedeSePosterior, Modo, MostrouMsg)  then
              begin
                Geral.MB_Aviso('N�o foi poss�vel desfazer o boleto n�mero ' +
                  Geral.FFI(QrBoletosItsBoleto.Value));
                Exit;
              end;
              QrBoletosIts.Next;
            end;
          end else
            UBloquetos.MostraMensagemPeriodoEncerrado(QrBoletosBoleto.Value);
        end;
      end;
      istTodos:
      begin
        PB1.Max := QrBoletos.RecordCount;
        //
        QrBoletos.First;
        while not QrBoletos.Eof do
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          //
          QrBoletosIts.First;
          while not QrBoletosIts.Eof do
          begin
            if not UBloquetos.VerificaSePeriodoEstaEncerrado(QrBoletosCodigo.Value) then
            begin
              if not UBloquetos.DesfazerBoletoAtual(QrBoletosCodigo.Value,
                VAR_ModBloq_EntCliInt, QrBoletosItsControle.Value,
                QrBoletosPeriodo.Value, QrBoletosItsLancto.Value,
                QrBoletosCNAB_Cfg.Value, QrBoletosEntidade.Value,
                QrBoletosItsAvulso.Value, QrBoletosItsBoleto.Value,
                VAR_ModBloq_TabLctA, ImpedeSePosterior, Modo, MostrouMsg) then
              begin
                Geral.MB_Aviso('N�o foi poss�vel desfazer o boleto n�mero ' +
                  Geral.FFI(QrBoletosItsBoleto.Value));
              end;
            end else
              UBloquetos.MostraMensagemPeriodoEncerrado(QrBoletosBoleto.Value);
            QrBoletosIts.Next;
          end;
          QrBoletos.Next;
        end;
      end;
    end;
    ReopenArre(QrBoletosEntidade.Value);
    ReopenBoletos(Modo, PageControl1.ActivePageIndex, 0);
  finally
    QrBoletos.EnableControls;
    QrBoletosIts.EnableControls;
    //
    PB1.Position       := 0;
    DBGradeBlo.Enabled := True;
  end;
end;

procedure TFmBloGeren.Desfazerboletosselecionados1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  DesfazerBoletos(istSelecionados, DBGradeBlo2, istVisPesquisa);
end;

procedure TFmBloGeren.Desfazprotocolo1Click(Sender: TObject);
  procedure DesfazProtocolo();
  begin
    Screen.Cursor := crHourGlass;
    try
      if (DBloGeren.QrProtocoBolDataD_Txt.Value = '') then //N�o tem retorno
      begin
        UMyMod.ExcluiRegistroInt1('', 'protpakits', 'Conta',
          DBloGeren.QrProtocoBolPROTOCOLO.Value, Dmod.MyDB);
{$IfNDef SemProtocolo}
        UnProtocolo.ExcluiLoteProtocoloBoleto(Dmod.MyDB,
          DBloGeren.QrProtocoBolPROTOCOD.Value,
          DBloGeren.QrProtocoBolPREVCOD.Value, VAR_ModBloq_EntCliInt,
          DBloGeren.QrProtocoBolArreits.Value, VAR_ModBloq_TabAriA);
{$Else}
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
var
  i: Integer;
begin
  if Geral.MB_Pergunta('Deseja desfazer o protocolo dos itens selecionados?'
  ) <> ID_YES
  then
    Exit;
  try
    TBProtocolos.Enabled   := False;
    BtPEBProtocolo.Enabled := False;
    //  
    if DBG_ProtocoBol.SelectedRows.Count > 1 then
    begin
      PB1.Position := 0;
      PB1.Max      := DBG_ProtocoBol.SelectedRows.Count;
      //
      with DBG_ProtocoBol.DataSource.DataSet do
      begin
        for i:= 0 to DBG_ProtocoBol.SelectedRows.Count-1 do
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          //
          GotoBookmark(pointer(DBG_ProtocoBol.SelectedRows.Items[i]));
          //
          DesfazProtocolo();
        end;
      end;
      PB1.Position := 0;
    end else
      DesfazProtocolo();
    //
    ReopenProtocoBol;
  finally
    TBProtocolos.Enabled   := True;
    BtPEBProtocolo.Enabled := True;
  end;
end;

procedure TFmBloGeren.DeTodosboletos1Click(Sender: TObject);
begin
  AlteraVencimentoPrebol(istTodos);
end;

procedure TFmBloGeren.DeTodosboletos2Click(Sender: TObject);
begin
  AlteraVencimentoBoleto(istTodos, DBGradeBlo, istVisMensal);
end;

procedure TFmBloGeren.dmkDBGrid2CellClick(Column: TColumn);
begin
  if Column.FieldName = 'Adiciona' then
  begin
    if (DBloGeren.QrArreIts.State = dsInactive) or
      (DBloGeren.QrArreIts.RecordCount = 0) then Exit;
    //    
    DBloGeren.AtualizaSelecionadoArrPeri(FBloArreAll, 0, DBloGeren.QrArreItsControleTmp.Value);
    DBloGeren.ReopenArreIts(DBloGeren.QrArreItsControleTmp.Value);
  end;
end;

procedure TFmBloGeren.dmkDBGrid3CellClick(Column: TColumn);
begin
  if Column.FieldName = 'Adiciona' then
  begin
    if (DBloGeren.QrArre.State = dsInactive) or
      (DBloGeren.QrArre.RecordCount = 0) then Exit;
    //
    DBloGeren.AtualizaSelecionadoArrPeri(FBloArreAll, DBloGeren.QrArreBloArreIts.Value, 0);
    DBloGeren.ReopenArre(DBloGeren.QrArreControle.Value, FBloArreAll);
  end;
end;

procedure TFmBloGeren.DosboletosSelecionados1Click(Sender: TObject);
begin
  AlteraVencimentoPrebol(istSelecionados);
end;

procedure TFmBloGeren.DosboletosSelecionados2Click(Sender: TObject);
begin
  AlteraVencimentoBoleto(istSelecionados, DBGradeBlo, istVisMensal);
end;

procedure TFmBloGeren.Editar1Click(Sender: TObject);
{$IfNDef SemProtocolo}
var
  TiposProt: String;
begin
  TiposProt := Geral.FF0(VAR_TIPO_PROTOCOLO_EB_01) + ',' + Geral.FF0(VAR_TIPO_PROTOCOLO_CE_02);
  //
  ProtocoUnit.MostraProtoAddTar(ptkBoleto, TiposProt, DBGradeBlo, QrBoletos);
  ReopenBoletos(istVisMensal, 0, QrBoletos.FieldByName('Controle').AsInteger);
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TFmBloGeren.EditarprotocolodeentregaSelecionados1Click(
  Sender: TObject);
{$IfNDef SemProtocolo}
var
  TiposProt: String;
begin
  TiposProt := Geral.FF0(VAR_TIPO_PROTOCOLO_EB_01) + ',' + Geral.FF0(VAR_TIPO_PROTOCOLO_CE_02);
  //
  ProtocoUnit.MostraProtoAddTar(ptkBoleto, TiposProt, DBGradeBlo2, QrBoletos);
  ReopenBoletos(istVisPesquisa, 0, QrBoletos.FieldByName('Controle').AsInteger);
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TFmBloGeren.Excluiarrecadaofutura1Click(Sender: TObject);
begin
  if UMyMod.SQLDel1(Dmod.QrUpd, QrArreFutI, 'ArreFut', 'Controle',
    QrArreFutI.FieldByName('Controle').AsInteger, True,
    'Confirma a exclus�o da arrecada��o futura selecionada?', True)
  then
    Geral.MB_Aviso('AVISO! A exclus�o de item futuro n�o elimina item j� arrecadado!');
end;

procedure TFmBloGeren.EdFatID_CodChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdFatID_Cod.ValueVariant, FFatIdTxt, Texto, 0, 1);
  //
  EdFatID_Txt.Text := Texto;
end;

procedure TFmBloGeren.EdFatID_CodKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'ID';
    TitCols[1] := 'Descri��o';
    //
    EdFatID_Cod.ValueVariant := Geral.SelecionaItem(FFatIdTxt, 0,
      'SEL-LISTA-000 :: Sele��o da origem do faturamento', TitCols, Screen.Width);
  end;
end;

procedure TFmBloGeren.FormActivate(Sender: TObject);
begin
  if TFmBloGeren(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
  UBloquetos.AtualizaModeloDeBoletoPrev();
  UBloquetos.Migra_BloToProtocolo();
  UBloquetos.AtualizaProtocolosBoletos(PB1);
end;

procedure TFmBloGeren.FormCreate(Sender: TObject);
var
  TemNFSe: Boolean;
begin
  ImgTipo.SQLType := stLok;
  //
  FOrigem := istVisNenhum;
  //
  DmodG.ReopenControle;
  //
  {$IfNDef sNFSe}
  TemNFSe := DBCheck.LiberaModulo(CO_DMKID_APP, dmkPF.ObtemSiglaModulo(mdlappNFSe),
               DModG.QrMaster.FieldByName('HabilModulos').AsString);
  {$Else}
  TemNFSe := False;
  {$EndIf}
  //
  DBGradeArreFutt.Columns[5].Visible := TemNFSe;
  DBGradeArreIts.Columns[2].Visible  := TemNFSe;
  //
  GerarNFSeparaosboletosselecionados1.Visible := TemNFSe;
  GerarNFSeparaosboletosselecionados2.Visible := TemNFSe;
end;

procedure TFmBloGeren.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], ' ' + Geral.FFN(VAR_ModBloq_CliInt, 3) + ' - ' +
    DmodG.QrEmpresasNOMEFILIAL.Value, False, taLeftJustify, 2, 10, 20);
end;

procedure TFmBloGeren.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  //
  DModG.ReopenParamsEmp(VAR_ModBloq_EntCliInt);
  //
  ConfiguraJanela(istVisNenhum, -1);
end;

procedure TFmBloGeren.GeraBloq(Tipo: TSelType);

  function GeraBloqAtual: Double;
  var
    ProtocoloCR: Integer;
  begin
    Result := UBloqGerl.GeraBloquetoAtual(QrPrevPeriodo.Value,
                VAR_ModBloq_EntCliInt, QrBoletosVencto.Value, True,
                VAR_ModBloq_TabLctA, VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA,
                QrBoletos, QrBoletosIts, Dmod.MyDB, False, ProtocoloCR);
  end;

var
  Aba, i: Integer;
begin
  PB1.Position := 0;
  //
  case Tipo of
    istSelecionados:
    begin
      if DBGradePreBlo.SelectedRows.Count > 1 then
      begin
        PB1.Max := DBGradePreBlo.SelectedRows.Count;
        try
          DBGradePreBlo.Enabled := False;
          //
          QrBoletos.DisableControls;
          //
          with DBGradePreBlo.DataSource.DataSet do
          for i:= 0 to DBGradePreBlo.SelectedRows.Count-1 do
          begin
            GotoBookmark(pointer(DBGradePreBlo.SelectedRows.Items[i]));

            if (GeraBloqAtual <> 0) then
            begin
              PB1.Position := PB1.Position + 1;
              PB1.Update;
              Application.ProcessMessages;
            end else
            begin
              Geral.MB_Erro('Falha ao gerar boleto(s)!');
              Exit;
            end;
          end;
        finally
          QrBoletos.EnableControls;
          //
          DBGradePreBlo.Enabled := True;
        end;
        PB1.Position := 0;
      end else
        GeraBloqAtual;
    end;
    istTodos:
    begin
      PB1.Max := QrBoletos.RecordCount;
      //
      try
        DBGradePreBlo.Enabled := False;
        //
        QrBoletos.DisableControls;
        //
        QrBoletos.First;
        while not QrBoletos.Eof do
        begin
          if (GeraBloqAtual <> 0) then
          begin
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
          end else
          begin
            Geral.MB_Erro('Falha ao gerar boleto(s)!');
            Exit;
          end;
          QrBoletos.Next;
        end;
      finally
        QrBoletos.EnableControls;
        //
        DBGradePreBlo.Enabled := True;
      end;
    end;
  end;
  ReopenArre(QrBoletosEntidade.Value);
  Aba := PageControl1.ActivePageIndex;
  ReopenBoletos(istVisMensal, Aba, 0);
  //
  if Tipo = istTodos then
  begin
    if Geral.MB_Pergunta('Deseja travar o per�odo atual?') = ID_YES
    then
      TravaDestPeriodo(QrPrevEncerrado.Value, True);
  end;
  PB1.Position := 0;
end;

procedure TFmBloGeren.GeraNFSeBoletos(Grade: TDBGrid);
{$IfNDef sNFSe}
var
  NFSeFatCab: Integer;
{$EndIf}
begin
{$IfNDef sNFSe}
  NFSeFatCab := UBloqGerl.CriaNFSeBoleto(VAR_ModBloq_EntCliInt,
                  DBloGeren.QrBloOpcoes.FieldByName('PerGerBol').AsInteger, PB1,
                  Grade, QrBoletos, QrBoletosIts, Dmod.QrUpd, Dmod.QrAux, Dmod.MyDB);
  //
  if NFSeFatCab <> 0 then
  begin
    UnNFSe_PF_0000.MostraFormNFSeFatCab(True, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
    //
    if TFmBloGeren(Self).Owner is TApplication then
      Close
    else
      MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
  end;
{$Else}
  dmkPF.InfoSemModulo(mdlappNFSe);
{$EndIf}
end;

procedure TFmBloGeren.GerarNFSeparaosboletosselecionados1Click(Sender: TObject);
begin
  GeraNFSeBoletos(TDBGrid(DBGradeBlo));
end;

procedure TFmBloGeren.GerarNFSeparaosboletosselecionados2Click(Sender: TObject);
begin
  GeraNFSeBoletos(TDBGrid(DBGradeBlo2));
end;

procedure TFmBloGeren.Gerarselecionados1Click(Sender: TObject);
begin
  GeraBloq(istSelecionados);
end;

procedure TFmBloGeren.Gerartodooperodo1Click(Sender: TObject);
begin
  FOrigem := istVisMensal;
  //
  UBloquetos.VerificaArrencadacoesVencendo;
  ConfiguraJanela(istArrPeri, 3);
  //
  ReopenBoletos(istVisMensal, PageControl1.ActivePageIndex, 0);
end;

procedure TFmBloGeren.Gerartodooperodo2Click(Sender: TObject);
begin
  FOrigem := istVisPesquisa;
  //
  UBloquetos.VerificaArrencadacoesVencendo;
  ConfiguraJanela(istArrPeri, 3);
end;

procedure TFmBloGeren.Gerartodosabertos1Click(Sender: TObject);
begin
  GeraBloq(istTodos);
end;

procedure TFmBloGeren.Gerenciar1Click(
  Sender: TObject);
begin
  MostraPreEnPrGer(istVisMensal);
end;

procedure TFmBloGeren.Gerenciar2Click(
  Sender: TObject);
begin
  MostraPreEnPrGer(istVisPesquisa);
end;

procedure TFmBloGeren.HabilitaBotoes;
var
  Aberto: Boolean;
begin
  Aberto := QrPrevEncerrado.Value = 0;
  //
  if Aberto then
    TravaDestr.Caption := 'Trava'
  else
    TravaDestr.Caption := 'Destrava';
  //
  Altera1.Enabled    := Aberto;
  BtArrecada.Enabled := Aberto;
  BtPrebol.Enabled   := Aberto;
  BtBoleto1.Enabled  := Aberto;
end;

procedure TFmBloGeren.Inclui1Click(Sender: TObject);
begin
  MostraBloGerenNew(stIns, VAR_ModBloq_EntCliInt, istVisMensal, nil, nil);
end;

procedure TFmBloGeren.Incluiarrecadaofutura1Click(Sender: TObject);
begin
  UBloqGerl_Jan.MostraFmCondGerArreFut(stIns, VAR_ModBloq_CliInt, VAR_ModBloq_EntCliInt, QrArreFutI);
  UBloquetos.ReopenArreFut(QrArreFutI, VAR_ModBloq_CliInt, 0, 0);
end;

procedure TFmBloGeren.Incluiintensbasedearrecadao;
var
  Pode, Aded, MesI, MesF, MesT, AnoT, a, b, c: Word;  Adiciona, Seq, ArreInt,
  Empresa, Entidade: Integer;
  Texto, Titulo: String;
  Val1: Double;
  ResVar: Variant;
begin
  Screen.Cursor := crHourGlass;
  Pode          := 0;
  Aded          := 0;
  Seq           := 0;
  Empresa       := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  //
  if Empresa = 0 then
    Empresa := -100000000; 
  //
  FBloArre := UCriar.RecriaTempTableNovo(ntrttBloArreTmp, DModG.QrUpdPID1, False);
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FBloArre + ' SET ');
  DmodG.QrUpdPID1.SQL.Add('Conta=:P0, BloArre=:P1, BloArreIts=:P2, Valor=:P3, ');
  DModG.QrUpdPID1.SQL.Add('Texto=:P4, Adiciona=:P5, Seq=:P6, Entidade=:P7, ');
  DModG.QrUpdPID1.SQL.Add('NomeEnti=:P8, CartEmiss=:P9, CNAB_Cfg=:10, ');
  DModG.QrUpdPID1.SQL.Add('NFSeSrvCad=:11, DiaVencto=:P12');
  //
  DBloGeren.ReopenNIA(VAR_ModBloq_EntCliInt, False);
  //
  PB1.Position := 0;
  PB1.Max      := DBloGeren.QrNIA.RecordCount;
  // Cobran�as Sobre arrecada��es.
  while not DBloGeren.QrNIA.Eof do
  begin
    if UBloquetos.VerificaArrecadacao(DBloGeren.QrNIAEntidade.Value,
      DBloGeren.QrNIAConta.Value, DBloGeren.QrNIACodigo.Value,
      DBloGeren.QrNIAControle.Value, VAR_ModBloq_EntCliInt, QrPrevPeriodo.Value) then
    begin
      case DBloGeren.QrNIASitCobr.Value of
        0: ; // N�o cobrar
        1:   // Cobran�a Cont�nua (Anual)
        begin
          MesI := DBloGeren.QrNIAParcPerI.Value;
          MesF := DBloGeren.QrNIAParcPerF.Value;
          dmkPF.PeriodoDecode(QrPrevPeriodo.Value, AnoT, MesT);
          //
          if (MesI > 0) and (MesF > 0) and (MesI < 13) and (MesF < 13) then
          begin
            if (MesF > MesI) then
            begin
              if (MesF >= MesT)  and (MesI <= MesT) then
              inc(Pode, 1);
            end else
            if (MesF < MesI) then
            begin
              a := MesI;
              b := MesT;
              if MesF > MesT then b := b + 12;
              c := MesF + 12;
              if (c >= b) and (a <= b) then
                inc(Pode, 1);
            end;
          end;
        end;
        2:  // Cobran�a Programada
        begin
          MesI := DBloGeren.QrNIAParcPerI.Value;
          MesF := DBloGeren.QrNIAParcPerF.Value;
          MesT := QrPrevPeriodo.Value;
          MesT := UBloquetos.ValidaPeriodo(MesT);
          //
          if MesI > MesF then
          begin
            if MesF > MesT then MesT := MesT + 12;
            MesF := MesF + 12;
          end;
          if (MesF >= MesT)  and (MesI <= MesT) then
            inc(Pode, 1);
        end;
        3:
        begin //Contrato
          MesI := Geral.Periodo2000(DBloGeren.QrNIADVencimento.Value);
          MesF := Geral.Periodo2000(DBloGeren.QrNIADtaPrxRenw.Value);
          MesT := QrPrevPeriodo.Value;
          MesT := UBloquetos.ValidaPeriodo(MesT);
          //
          if MesI > MesF then
          begin
            if MesF > MesT then MesT := MesT + 12;
            MesF := MesF + 12;
          end;
          if (MesF >= MesT)  and (MesI <= MesT) then
            inc(Pode, 1);
        end;
      end;

      ////////////////////////////////////////////////////////////////////////////

      if Aded < Pode then
      begin
        inc(Aded, 1);
        Adiciona := 1;
        if Trim(DBloGeren.QrNIATexto.Value) <> '' then
          Texto := DBloGeren.QrNIATexto.Value
        else
          Texto := DBloGeren.QrNIANome.Value;
        if DBloGeren.QrNIAInfoParc.Value = 1 then
        begin
          a := 0;
          b := 0;
          c := 0;
          case DBloGeren.QrNIASitCobr.Value of
            1: // Cobran�a Cont�nua (Anual)
            begin
              dmkPF.PeriodoDecode(QrPrevPeriodo.Value, AnoT, MesT);
              a := DBloGeren.QrNIAParcPerI.Value;
              c := DBloGeren.QrNIAParcPerF.Value;
              if c < a then Inc(c, 12);
              b := MesT;
              if b < a then Inc(b, 12);
            end;
            2: // Cobran�a Programada
            begin
              a := DBloGeren.QrNIAParcPerI.Value;
              b := UBloquetos.ValidaPeriodo(QrPrevPeriodo.Value);
              c := DBloGeren.QrNIAParcPerF.Value;
            end;
            3: // Contrato
            begin
              a := Geral.Periodo2000(DBloGeren.QrNIADVencimento.Value);
              b := UBloquetos.ValidaPeriodo(QrPrevPeriodo.Value);
              c := Geral.Periodo2000(DBloGeren.QrNIADtaPrxRenw.Value);
            end;
          end;
          Texto := FormatFloat('00', b-a+1) + '/' + FormatFloat('00', c-a+1) +
            ' - ' + Texto;
        end;
        PB1.Position := PB1.Position + 1;
        Update;
        Application.ProcessMessages;
        //
        if DBloGeren.QrNIAValorInf.Value = 1 then
        begin
          if DBloGeren.QrNIATexto.Value <> '' then
            Titulo := '(' + DBloGeren.QrNIATexto.Value + ')'
          else
            Titulo := '';
          if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
            DBloGeren.QrNIAValor.Value, 2, 0, '', '', True,
            DBloGeren.QrNIANome.Value, 'Informe o valor: ' + Titulo, 0, ResVar)
          then
            Val1 := ResVar
          else
            Val1 := 0;
        end else
          Val1 := DBloGeren.QrNIAValor.Value;
        //
        // Arredonda valor conforme configurado no ArreBaI
        if DBloGeren.QrNIAArredonda.Value > 0 then
        begin
          ArreInt := Trunc((Val1 + (DBloGeren.QrNIAArredonda.Value * 0.99)) /
            DBloGeren.QrNIAArredonda.Value);
          Val1 := ArreInt * DBloGeren.QrNIAArredonda.Value;
        end else Val1 := Trunc(Val1 * 100) / 100;
        //
        DModG.QrUpdPID1.Params[00].AsInteger := DBloGeren.QrNIAConta.Value;
        DModG.QrUpdPID1.Params[01].AsInteger := DBloGeren.QrNIACodigo.Value;
        DModG.QrUpdPID1.Params[02].AsInteger := DBloGeren.QrNIAControle.Value;
        DModG.QrUpdPID1.Params[03].AsFloat   := Val1;
        DModG.QrUpdPID1.Params[04].AsString  := Texto;
        DModG.QrUpdPID1.Params[05].AsInteger := Adiciona;
        DModG.QrUpdPID1.Params[06].AsInteger := Seq;
        DModG.QrUpdPID1.Params[07].AsInteger := DBloGeren.QrNIAEntidade.Value;
        DModG.QrUpdPID1.Params[08].AsString  := DBloGeren.QrNIANOMEENT.Value;
        DModG.QrUpdPID1.Params[09].AsInteger := DBloGeren.QrNIACartEmiss.Value;
        DModG.QrUpdPID1.Params[10].AsInteger := DBloGeren.QrNIACNAB_Cfg.Value;
        DModG.QrUpdPID1.Params[11].AsInteger := DBloGeren.QrNIANFSeSrvCad.Value;
        DModG.QrUpdPID1.Params[12].AsInteger := Trunc(DBloGeren.QrNIADiaVencto.Value);
        DmodG.QrUpdPID1.ExecSQL;
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    DBloGeren.QrNIA.Next;
  end;
  PB1.Position  := 0;
  Screen.Cursor := crDefault;
  case Pode of
    0: Texto := 'N�o foi localizado nenhum item base de or�amento aplic�vel';
    1: Texto := 'Foi localizado 1 (um) item base de or�amento aplic�vel';
    else Texto := 'Foram localizados '+IntToStr(Pode)+' itens base de or�amento aplic�veis';
  end;
  Texto := Texto + ' de ' + IntToStr(DBloGeren.QrNIA.RecordCount);
  case DBloGeren.QrNIA.RecordCount of
    0: Texto := 'N�o h� itens base de or�amento cadastrados para este cliente!';
    1: Texto := Texto + ' pesquisado!';
    else Texto := Texto + ' pesquisados!';
  end;
  Geral.MB_Aviso(Texto);
  //
  if (Pode > 0) then
  begin
    if DBCheck.CriaFm(TFmBloGerenBaA, FmBloGerenBaA, afmoNegarComAviso) then
    begin
      FmBloGerenBaA.FNomeCliente   := VAR_EMPRESANOME;
      FmBloGerenBaA.FCodiCliente   := Empresa;
      FmBloGerenBaA.FCodigo        := QrPrevCodigo.Value;
      FmBloGerenBaA.FTmpTabBloArre := FBloArre;
      FmBloGerenBaA.FPeriodo       := QrPrevPeriodo.Value;
      FmBloGerenBaA.ShowModal;
      //
      Entidade := FmBloGerenBaA.FEntidade;
      //
      FmBloGerenBaA.Destroy;
      //
      ReopenArre(Entidade);
    end;
  end;
end;

procedure TFmBloGeren.Incluiitensbasedearrecadao1Click(Sender: TObject);
begin
  UBloqGerl_Jan.VerificaArreFut(QrArreFutA, QrArreFutI, DsArreFutA,
    VAR_ModBloq_CliInt, QrPrevCodigo.Value,
    UBloquetos.ValidaPeriodo(QrPrevPeriodo.Value), VAR_ModBloq_TabAriA);
  //
  UBloquetos.VerificaArrencadacoesVencendo;
  //
  Incluiintensbasedearrecadao;
end;

procedure TFmBloGeren.Itemselecionado1Click(Sender: TObject);
begin
  if UBloquetos.ExcluiArrecadacao(QrPrevCodigo.Value,
    QrArreItsControle.Value, False)
  then
    ReopenArre(0);
end;

procedure TFmBloGeren.Localizaprotocolo1Click(Sender: TObject);
{$IfNDef SemProtocolo}
var
  Lote: Integer;
begin
  Lote := DBloGeren.QrProtocoBolProtocoPak.Value;
  //
  if Lote = 0 then
    Geral.MB_Aviso('Lote n�o localizado!')
  else
    ProtocoUnit.MostraFormProtocolos(Lote, 0);
  //
  ReopenProtocoBol;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TFmBloGeren.LocPeriodo(Atual, Periodo: Integer);
begin
  Screen.Cursor := crHourGlass;
  DefParams;
  GOTOy.LC(Atual, Periodo);
  Screen.Cursor := crDefault;
end;

procedure TFmBloGeren.Modificarlayoutdobloqueto1Click(Sender: TObject);
begin
  MostraBloGerenNew(stUpd, VAR_ModBloq_EntCliInt, istVisPesquisa,
    TDBGrid(DBGradeBlo2), QrBoletos);
  //
  ReopenBoletos(istVisPesquisa, 0, QrBoletosControle.Value);
end;

procedure TFmBloGeren.MostraBloGerenArreEnt(Tipo: TSQLType);
var
  Entidade: Integer;
begin
  if (QrArreBolBoleto.Value > 0) and (Tipo = stUpd) then
  begin
    Geral.MB_Aviso('Altera��o cancelada!' + sLineBreak +
      'J� foi gerado um boleto para esta arrecada��o!' + sLineBreak +
      'Desfa�a o boleto referente a esta arrecada��o e tente novamente!');
    Exit;
  end;
  //
  if DBCheck.CriaFm(TFmBloGerenArreEnt, FmBloGerenArreEnt, afmoNegarComAviso) then
  begin
    FmBloGerenArreEnt.ImgTipo.SQLType := Tipo;
    FmBloGerenArreEnt.FCodigo         := QrPrevCodigo.Value;
    FmBloGerenArreEnt.FPeriodo        := QrPrevPeriodo.Value;
    FmBloGerenArreEnt.FQueryArreIts   := QrArreIts;
    FmBloGerenArreEnt.FCliInt         := VAR_ModBloq_EntCliInt;
    //
    if Tipo = stUpd then
      FmBloGerenArreEnt.FControle := QrArreItsControle.Value
    else
      FmBloGerenArreEnt.FControle := 0;
    //
    FmBloGerenArreEnt.ShowModal;
    //
    Entidade := FmBloGerenArreEnt.FEntidade;
    //
    FmBloGerenArreEnt.Destroy;
    //
    ReopenArre(Entidade);
  end;
end;

procedure TFmBloGeren.MostraBloGerenNew(SQLType: TSQLType; CliInt: Integer;
  Modo: TModoVisualizacaoBloquetos; DBGrid: TDBGrid; QueryBoletos: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmBloGerenNew, FmBloGerenNew, afmoNegarComAviso) then
  begin
    FmBloGerenNew.ImgTipo.SQLType := SQLType;
    FmBloGerenNew.F_CliInt        := CliInt;
    FmBloGerenNew.FQueryPrev      := QrPrev;
    FmBloGerenNew.FModo           := Modo;
    //
    if DBGrid <> nil then
      FmBloGerenNew.FDBGrid := DBGrid;
    if QueryBoletos <> nil then
      FmBloGerenNew.FQueryBoletos := QueryBoletos;
    //
    FmBloGerenNew.ShowModal;
    //
    if FmBloGerenNew.FPeriodo <> 0 then
      LocPeriodo(FmBloGerenNew.FPeriodo, FmBloGerenNew.FPeriodo);
    //
    FmBloGerenNew.Destroy;
  end;
end;

procedure TFmBloGeren.MostraPreEnPrGer(Modo: TModoVisualizacaoBloquetos);
var
  Empresa, Entidade: Integer;
begin
  if (QrBoletos.State <> dsInactive) and (QrBoletos.RecordCount > 0) then
  begin
    Entidade := QrBoletosEntidade.Value;
    Empresa  := QrBoletosEmpresa.Value;
    //
    ProtocoUnit.MostraPreEnPrGer(ptkBoleto, Empresa, Entidade);
    ReopenBoletos(Modo, 0, QrBoletos.FieldByName('Controle').AsInteger);
  end;
end;

procedure TFmBloGeren.GradeBoletosDBClick(Sender: TObject;
  Modo: TModoVisualizacaoBloquetos);
{$IfNDef SemProtocolo}
var
  FatIdTxt: MyArrayLista;
  TitCols: array[0..1] of String;
  Campo: String;
  Empresa, Entidade: Integer;
begin
  if (QrBoletos.State <> dsInactive) and (QrBoletos.RecordCount > 0) then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if UpperCase(Campo) = UpperCase('FatID') then
    begin
      FatIdTxt := UFinanceiro.ObtemListaNomeFatID(CO_DMKID_APP);
      //
      if Length(FatIdTxt) > 0 then
      begin
        TitCols[0] := 'ID';
        TitCols[1] := 'Descri��o';
        //
        Geral.SelecionaItem(FatIdTxt, 0, 'SEL-LISTA-000 :: Sele��o da origem do faturamento',
          TitCols, Screen.Width);
      end;
    end else
    if UpperCase(Campo) = UpperCase('NOMEENT') then
    begin
      Entidade := QrBoletosEntidade.Value;
      //
      DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2, False);
    end else
    if UpperCase(Campo) = UpperCase('Envios') then
    begin
      MostraPreEnPrGer(Modo);
    end;
  end;
{$Else}
begin
{$EndIf}
end;

procedure TFmBloGeren.Novoitemdearrecadao1Click(Sender: TObject);
begin
  MostraBloGerenArreEnt(stIns);
end;

procedure TFmBloGeren.ODOSitensdearrecadao1Click(Sender: TObject);
begin
  if UBloquetos.ExcluiArrecadacao(QrPrevCodigo.Value, 0, True) then
    ReopenArre(0);
end;

procedure TFmBloGeren.PageControl1Change(Sender: TObject);
begin
  ConfiguraAbas(PageControl1.ActivePageIndex);
end;

procedure TFmBloGeren.PageControl2Change(Sender: TObject);
var
  Aba: Integer;
  Modo: TModoVisualizacaoBloquetos;
begin
  Aba := PageControl2.ActivePageIndex;
  //
  case Aba of
      0: Modo := istVisMensal;
      2: Modo := istVisProtEnt;
    else Modo := istVisPesquisa;
  end;
  ConfiguraJanela(Modo, PageControl1.ActivePageIndex);
end;

procedure TFmBloGeren.PCArrecadaChange(Sender: TObject);
begin
  if PCArrecada.ActivePageIndex = 1 then
    UBloquetos.ReopenArreFut(QrArreFutI, VAR_ModBloq_CliInt, 0, 0);
end;

procedure TFmBloGeren.PMArrecadaPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  Enab2 := (QrArre.State <> dsInactive) and (QrArre.RecordCount > 0);
  //
  Incluiitensbasedearrecadao1.Enabled := Enab;
  Novoitemdearrecadao1.Enabled        := Enab;
  Alteraarrecadao1.Enabled            := Enab and Enab2;
  Exclusodearrecadao1.Enabled         := Enab and Enab2;
end;

procedure TFmBloGeren.PMArreFutPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0) and (QrPrevEncerrado.Value = 0);
  Enab2 := (QrArreFutI.State <> dsInactive) and (QrArreFutI.RecordCount > 0);
  //
  Incluiarrecadaofutura1.Enabled := Enab;
  //
  Alteraarrecadaofutura1.Enabled := Enab and Enab2;
  Excluiarrecadaofutura1.Enabled := Enab and Enab2;
end;

procedure TFmBloGeren.PMBoleto2Popup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrBoletos.State <> dsInactive) and (QrBoletos.RecordCount > 0);
  //
  AlteraVencimentoSelecionados1.Enabled         := Enab;
  Alteravalor1.Enabled                          := Enab;
  Alteratextodaarrecadaoatual1.Enabled          := Enab;
  Desfazerboletosselecionados1.Enabled          := Enab;
  EditarprotocolodeentregaSelecionados1.Enabled := Enab;
  Gerenciar2.Enabled                            := Enab;
  Modificarlayoutdobloqueto1.Enabled            := Enab;
  GerarNFSeparaosboletosselecionados2.Enabled   := Enab;
end;

procedure TFmBloGeren.PMBoletoPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  Enab2 := (QrBoletos.State <> dsInactive) and (QrBoletos.RecordCount > 0);
  //
  Alteravencimento1.Enabled                   := Enab and Enab2;
  Desfazerboletos2.Enabled                    := Enab and Enab2;
  Editar1.Enabled                             := Enab and Enab2;
  Gerenciar1.Enabled                          := Enab and Enab2;
  GerarNFSeparaosboletosselecionados1.Enabled := Enab and Enab2;
end;

procedure TFmBloGeren.PmImprimePopup(Sender: TObject);
var
  Habil1, Habil2: Boolean;
begin
  Habil1 := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  Habil2 := (QrBoletos.State <> dsInactive) and (QrBoletos.RecordCount > 0);
  //
  ImprimeBloqSel.Enabled   := Habil1 and Habil2;
  ImprimeBloqTodos.Enabled := Habil1 and Habil2;
end;

procedure TFmBloGeren.PMOrcPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  //
  Altera1.Enabled    := Enab and (QrPrevEncerrado.Value = 0);
  TravaDestr.Enabled := Enab;
end;

procedure TFmBloGeren.PMPrebolPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  Enab2 := (QrBoletos.State <> dsInactive) and (QrBoletos.RecordCount > 0);
  //
  Boleto1.Enabled           := Enab and Enab2;
  AlteraVencimento2.Enabled := Enab and Enab2;
end;

procedure TFmBloGeren.PMProtocoloPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (DBloGeren.QrProtocoBol.State <> dsInactive) and
             (DBloGeren.QrProtocoBol.RecordCount > 0);
  Enab2 := TBGerados.TabIndex = 1;
  //
  Gera1.Enabled              := Enab and (TBGerados.TabIndex = 0);
  Desfazprotocolo1.Enabled   := Enab and Enab2;
  Localizaprotocolo1.Enabled := Enab and Enab2;
end;

procedure TFmBloGeren.T(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  Enab := (QrArre.State <> dsInactive) and (QrArre.RecordCount > 0);
  //
  BtAvanca2.Enabled := Enab;
  //
  if Enab then
    LaMesArr.Caption := ''
end;

procedure TFmBloGeren.TBGeradosChange(Sender: TObject);
begin
  ConfiguraBotoes(TBProtocolos.TabIndex);
  ReopenProt1(TBProtocolos.TabIndex, TBGerados.TabIndex);
end;

procedure TFmBloGeren.TBProtocolosChange(Sender: TObject);
begin
  ConfiguraBotoes(TBProtocolos.TabIndex);
  ReopenProt1(TBProtocolos.TabIndex, TBGerados.TabIndex);
end;

procedure TFmBloGeren.QrArreAfterOpen(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  Enab := (QrArre.State <> dsInactive) and (QrArre.RecordCount > 0);
  //
  BtAvanca2.Enabled := Enab;
  //
  if Enab then
    LaMesArr.Caption := 'Total de itens: ' + Geral.FF0(QrArre.RecordCount)
  else
    LaMesArr.Caption := '';
end;

procedure TFmBloGeren.QrArreAfterScroll(DataSet: TDataSet);
begin
  ReopenArreBol(0);
end;

procedure TFmBloGeren.QrArreBeforeClose(DataSet: TDataSet);
begin
  BtAvanca2.Enabled := False;
  //
  QrArreBol.Close;
  //
  LaMesArr.Caption := '';
end;

procedure TFmBloGeren.QrArreBolAfterScroll(DataSet: TDataSet);
begin
  ReopenArreIts(0);
end;

procedure TFmBloGeren.QrArreBolBeforeClose(DataSet: TDataSet);
begin
  QrArreIts.Close;
end;

procedure TFmBloGeren.QrArreFutACalcFields(DataSet: TDataSet);
begin
  QrArreFutAPERIODO_TXT.Value := dmkPF.PeriodoToMensal(QrArreFutAPeriodo.Value);
end;

procedure TFmBloGeren.QrArreFutICalcFields(DataSet: TDataSet);
begin
  QrArreFutIPERIODO_TXT.Value := dmkPF.PeriodoToMensal(QrArreFutIPeriodo.Value);
end;

procedure TFmBloGeren.ConfiguraTotalDeItensDeBoletos();
var
  Enab: Boolean;
  ModoSel: TModoVisualizacaoBloquetos;
begin
  Enab := (QrBoletos.State <> dsInactive) and (QrBoletos.RecordCount > 0);
  //
  if DBloGeren.QrBloOpcoes.FieldByName('VisPadrao').AsInteger = 0 then
    ModoSel := istVisMensal
  else
    ModoSel := istVisPesquisa;
  //
  if ModoSel = istVisMensal then
  begin
    if PageControl1.ActivePageIndex = 2 then
    begin
      if Enab then
        LaMesPreBol.Caption := 'Total de itens: ' + Geral.FF0(QrBoletos.RecordCount)
      else
        LaMesPreBol.Caption := '';
    end else
    if PageControl1.ActivePageIndex = 3 then
    begin
      if Enab then
        LaMesBol.Caption := 'Total de itens: ' + Geral.FF0(QrBoletos.RecordCount)
      else
        LaMesBol.Caption := '';
    end;
  end else
  if ModoSel = istVisPesquisa then
  begin
    if Enab then
      LaPesBol.Caption := 'Total de itens: ' + Geral.FF0(QrBoletos.RecordCount)
    else
      LaPesBol.Caption := '';
  end;
end;

procedure TFmBloGeren.QrBoletosAfterOpen(DataSet: TDataSet);
begin
  ConfiguraTotalDeItensDeBoletos();
end;

procedure TFmBloGeren.QrBoletosAfterScroll(DataSet: TDataSet);
begin
  ReopenBoletosIts();
  UBloqGerl.ReopenLeiBol(Dmod.MyDB, QrLeiBol, QrBoletosBoleto.Value);
end;

procedure TFmBloGeren.QrBoletosBeforeClose(DataSet: TDataSet);
begin
  QrBoletosIts.Close;
  //
  LaPesBol.Caption    := '';
  LaMesPreBol.Caption := '';
  LaMesBol.Caption    := '';
  //
  QrLeiBol.Close;
end;

procedure TFmBloGeren.QrBoletosCalcFields(DataSet: TDataSet);
begin
  QrBoletosVENCTO_TXT.Value := Geral.FDT(QrBoletosVencto.Value, 3);
end;

procedure TFmBloGeren.QrLeiBolCalcFields(DataSet: TDataSet);
var
  Movimento: Integer;
begin
  Movimento := Geral.IMV(QrLeiBolOcorrCodi.Value);
  QrLeiBolOCOR_TXT.Value := UBancos.CNABTipoDeMovimento(QrLeiBolBanco.Value,
                              ecnabRetorno, Movimento, QrLeiBolTamReg.Value,
                              QrLeiBolModalCobr.Value = 1, QrLeiBolLayoutRem.Value);
end;

procedure TFmBloGeren.QrPrevAfterClose(DataSet: TDataSet);
begin
  HabilitaBotoes;
end;

procedure TFmBloGeren.QrPrevAfterOpen(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  Enab := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  //
  HabilitaBotoes;
  //
  BtPesq.Enabled      := True;
  BtProtocolo.Enabled := True;
  BtAvanca1.Enabled   := Enab;
end;

procedure TFmBloGeren.QrPrevAfterScroll(DataSet: TDataSet);
var
  Aba: Integer;
begin
  Aba := PageControl1.ActivePageIndex;
  //
  ReopenArre(0);
  ReopenBoletos(istVisMensal, Aba, 0);
end;

procedure TFmBloGeren.QrPrevBeforeClose(DataSet: TDataSet);
begin
  BtPesq.Enabled      := False;
  BtProtocolo.Enabled := False;
  BtAvanca1.Enabled   := False;
  //
  QrArre.Close;
  QrSumARRE.Close;
  QrBoletos.Close;
end;

procedure TFmBloGeren.QrPrevCalcFields(DataSet: TDataSet);
var
  Valor: Double;
begin
  if QrPrevEncerrado.Value = 0 then
  begin
    QrPrevNOME_ENCERRADO.Value := 'ABERTO';
    DBText12.Font.Color := clGreen;
  end else begin
    QrPrevNOME_ENCERRADO.Value := 'ENCERRADO';
    DBText12.Font.Color := clRed;
  end;
  //
  QrPrevPERIODO_TXT.Value := dmkPF.MesEAnoDoPeriodoLongo(QrPrevPeriodo.Value);
  //
  DBloGeren.QrSumBol.Close;
  DBloGeren.QrSumBol.Params[0].AsInteger := QrPrevCodigo.Value;
  DBloGeren.QrSumBol.Open;
  Valor := 0;
  while not DBloGeren.QrSumBol.Eof do
  begin
    Valor := Valor + DBloGeren.QrSumBolVALOR.Value;
    DBloGeren.QrSumBol.Next;
  end;
  QrPrevTOT_BOL.Value := Valor;
  DBloGeren.QrSumBol.Close;
  //
  DBloGeren.QrSumPre.Close;
  DBloGeren.QrSumPre.Params[0].AsInteger := QrPrevCodigo.Value;
  DBloGeren.QrSumPre.Open;
  QrPrevTOT_PRE.Value := DBloGeren.QrSumPreVALOR.Value;
  Valor := 0;
  while not DBloGeren.QrSumPre.Eof do
  begin
    Valor := Valor + DBloGeren.QrSumPreVALOR.Value;
    DBloGeren.QrSumPre.Next;
  end;
  QrPrevTOT_PRE.Value := Valor;
  DBloGeren.QrSumPre.Close;
end;

procedure TFmBloGeren.QrProt1AfterScroll(DataSet: TDataSet);
begin
  ReopenProtocoBol;
  //
  LaProBol.Caption := 'Total de itens: ' + Geral.FF0(DBloGeren.QrProtocoBol.RecordCount);
end;

procedure TFmBloGeren.QrProt1BeforeClose(DataSet: TDataSet);
begin
  DBloGeren.QrProtocoBol.Close;
  //
  LaProBol.Caption := '';
end;

procedure TFmBloGeren.ReopenArre(Entidade: Integer);
begin
  QrArre.Close;
  QrArre.Params[0].AsInteger := QrPrevCodigo.Value;
  QrArre.Open;
  //
  if Entidade <> 0 then
    QrArre.Locate('Entidade', Entidade, []);
  //
  QrSumARRE.Close;
  QrSumARRE.Params[0].AsInteger := QrPrevCodigo.Value;
  QrSumARRE.Open;
end;

procedure TFmBloGeren.ReopenArreBol(Boleto: Integer);
begin
  QrArreBol.Close;
  QrArreBol.Params[00].AsInteger  := QrPrevCodigo.Value;
  QrArreBol.Params[01].AsInteger  := QrArreEntidade.Value;
  QrArreBol.Params[02].AsInteger  := QrArreCNAB_Cfg.Value;
  QrArreBol.Params[03].AsDateTime := QrArreVencto.Value;
  QrArreBol.Open;
  //
  if Boleto > 0 then
    QrArreBol.Locate('Boleto', Boleto, []);
end;

procedure TFmBloGeren.ReopenArreIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrArreIts, Dmod.MyDB, [
    'SELECT ',
    {$IfNDef sNFSe}
    'nsc.Nome NFSeSrvCad_TXT, ',
    {$Else}
    '"" NFSeSrvCad_TXT, ',
    {$EndIf}
    'ari.* ',
    'FROM arreits ari ',
    {$IfNDef sNFSe}
    'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo = ari.NFSeSrvCad ',
    {$EndIf}
    'WHERE ari.Codigo=' + Geral.FF0(QrPrevCodigo.Value),
    'AND ari.Entidade=' + Geral.FF0(QrArreEntidade.Value),
    'AND ari.CNAB_Cfg=' + Geral.FF0(QrArreCNAB_Cfg.Value),
    'AND ari.Boleto=' + Geral.FFI(QrArreBolBoleto.Value),
    'AND ari.Vencto="' + Geral.FDT(QrArreVencto.Value, 1) + '"',
    'ORDER BY Valor DESC ',
    '']);
  if Controle > 0 then
    QrArreIts.Locate('Controle', Controle, []);
end;

procedure TFmBloGeren.ReopenBoletos(Modo: TModoVisualizacaoBloquetos;
  Tipo, Controle: Integer);

  function ObtemFiltroSQLLancto(Lancto: Integer): String;
  var
    Boleto: Double;
    Codigo, Entidade, CNAB_Cfg: Integer;
    Vencto: TDate;
  begin
    Result := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT Boleto, Entidade, CNAB_Cfg, Vencto, Codigo ',
      'FROM arreits ',
      'WHERE Lancto=' + Geral.FF0(Lancto),
      '']);
    if QrLoc.RecordCount > 0 then
    begin
      CNAB_Cfg := QrLoc.FieldByName('CNAB_Cfg').AsInteger;
      Entidade := QrLoc.FieldByName('Entidade').AsInteger;
      Boleto   := QrLoc.FieldByName('Boleto').AsFloat;
      Vencto   := QrLoc.FieldByName('Vencto').AsDateTime;
      Codigo   := QrLoc.FieldByName('Codigo').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
        'SELECT Boleto, Entidade, CNAB_Cfg, Vencto ',
        'FROM arreits ',
        'WHERE CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
        'AND Entidade=' + Geral.FF0(Entidade),
        'AND Boleto=' + Geral.FFI(Boleto),
        'AND Vencto="' + Geral.FDT(Vencto, 1) + '"',
        'AND Codigo=' + Geral.FF0(Codigo),
        '']);
      if QrLoc.RecordCount <= 1 then
      begin
        Result := 'AND ari.Lancto=' + Geral.FF0(Lancto) + ' ';
      end else
      begin
        EdCNABCfg.ValueVariant  := CNAB_Cfg;
        CBCNABCfg.KeyValue      := CNAB_Cfg;
        EdEntidade.ValueVariant := Entidade;
        CBEntidade.KeyValue     := Entidade;
        EdBoleto.ValueVariant   := Boleto;
        CkDataVctIni.Checked    := True;
        CkDataVctFim.Checked    := True;
        TPDataVctIni.Date       := Vencto;
        TPDataVctFim.Date       := Vencto;
        EdLancto.ValueVariant   := 0;
        //
        Result := 'AND ari.CNAB_Cfg=' + Geral.FF0(CNAB_Cfg) + ' ' +
                  'AND ari.Entidade=' + Geral.FF0(Entidade) + ' ' +
                  'AND ari.Boleto=' + Geral.FFI(Boleto) + ' ' +
                  dmkPF.SQL_Periodo('AND ari.Vencto ', Vencto, Vencto, True, True) + ' ';
      end;
    end;
    QrLoc.Close;
  end;

var
  CNABCfg, Arre, Conta, Entidade, FatID, Lancto: Integer;
  ValorIni, ValorFim, FatNum, Boleto: Double;
  EmiIniCk, EmiFimCk, VctIniCk, VctFimCk, ValorCk: Boolean;
  EmiIniDt, EmiFimDt, VctIniDt, VctFimDt: TDateTime;
  SQLGroupBy: String;
begin
  try
    Screen.Cursor       := crHourGlass;
    FQrBloquetosFiltros := '';
    //
    if Modo = istVisPesquisa then //Pesquisa
    begin
      CNABCfg  := EdCNABCfg.ValueVariant;
      Arre     := EdArre.ValueVariant;
      Conta    := EdConta.ValueVariant;
      Entidade := EdEntidade.ValueVariant;
      EmiIniCk := CkDataEmiIni.Checked;
      EmiFimCk := CkDataEmiFim.Checked;
      VctIniCk := CkDataVctIni.Checked;
      VctFimCk := CkDataVctFim.Checked;
      ValorCk  := CkValor.Checked;
      EmiIniDt := TPDataEmiIni.Date;
      EmiFimDt := TPDataEmiFim.Date;
      VctIniDt := TPDataVctIni.Date;
      VctFimDt := TPDataVctFim.Date;
      ValorIni := EdValorIni.ValueVariant;
      ValorFim := EdValorFim.ValueVariant;
      FatID    := EdFatID_Cod.ValueVariant;
      FatNum   := EdFatNum.ValueVariant;
      Boleto   := EdBoleto.ValueVariant;
      Lancto   := EdLancto.ValueVariant;
      //
      FQrBloquetosFiltros := 'WHERE ari.Codigo <> 0 AND ari.Boleto <> 0 ';
      FQrBloquetosFiltros := FQrBloquetosFiltros + 'AND pre.Empresa=' + Geral.FF0(VAR_ModBloq_EntCliInt) + ' ';
      SQLGroupBy          := 'GROUP BY ari.CNAB_Cfg, ari.Boleto, ari.Entidade, pre.Codigo';
      //
      if Lancto <> 0 then
      begin
        FQrBloquetosFiltros := FQrBloquetosFiltros + ObtemFiltroSQLLancto(Lancto);
      end else
      begin
        if CNABCfg <> 0 then
          FQrBloquetosFiltros := FQrBloquetosFiltros + 'AND ari.CNAB_Cfg=' + Geral.FF0(CNABCfg) + ' ';
        if Entidade <> 0 then
          FQrBloquetosFiltros := FQrBloquetosFiltros + 'AND ari.Entidade=' + Geral.FF0(Entidade) + ' ';
        if Boleto <> 0 then
          FQrBloquetosFiltros := FQrBloquetosFiltros + 'AND ari.Boleto=' + Geral.FFI(Boleto) + ' ';
        //
        FQrBloquetosFiltros := FQrBloquetosFiltros + dmkPF.SQL_Periodo('AND ari.Vencto ', VctIniDt, VctFimDt, VctIniCk, VctFimCk) + ' ';
      end;
      if Arre <> 0 then
        FQrBloquetosFiltros := FQrBloquetosFiltros +
          'AND ari.BloArre=' + Geral.FF0(Arre) + ' ';
      if Conta <> 0 then
        FQrBloquetosFiltros := FQrBloquetosFiltros +
          'AND ari.Conta=' + Geral.FF0(Conta) + ' ';
      if FatID <> 0 then
        FQrBloquetosFiltros := FQrBloquetosFiltros + 'AND ari.FatID=' +
          Geral.FF0(FatID) + ' ';
      if FatNum <> 0 then
        FQrBloquetosFiltros := FQrBloquetosFiltros + 'AND ari.FatNum=' +
          Geral.FFI(FatNum) + ' ';
      //
      FQrBloquetosFiltros := FQrBloquetosFiltros +
        dmkPF.SQL_Periodo('AND ari.DataCad ', EmiIniDt, EmiFimDt, EmiIniCk,
        EmiFimCk) + ' ';
      //
      if ValorCk then
      begin
        if ValorFim <> 0 then //Entre
          FQrBloquetosFiltros := FQrBloquetosFiltros + 'AND ari.Valor >= ' +
            FloatToStr(ValorIni) + ' AND ari.Valor <= ' + FloatToStr(ValorFim) + ' '
        else
          FQrBloquetosFiltros := FQrBloquetosFiltros + 'AND ari.Valor = ' +
            FloatToStr(ValorIni) + ' ';
      end;
    end else
    if Modo = istVisMensal then //Mensal
    begin
      FQrBloquetosFiltros := 'WHERE ari.Codigo =' + Geral.FF0(QrPrevCodigo.Value) + ' ';
      //
      if Tipo = 2 then
      begin
        FQrBloquetosFiltros := FQrBloquetosFiltros + 'AND ari.Boleto = 0 ';
        SQLGroupBy          := 'GROUP BY ari.CNAB_Cfg, ari.Entidade, ari.Vencto, pre.Codigo';
      end else
      begin
        FQrBloquetosFiltros := FQrBloquetosFiltros + 'AND ari.Boleto <> 0 ';
        SQLGroupBy          := 'GROUP BY ari.CNAB_Cfg, ari.Boleto, ari.Entidade, pre.Codigo';
      end;
    end else
    begin
      QrBoletos.Close;
      Exit;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrBoletos, Dmod.MyDB, [
      'SELECT DISTINCT ari.Boleto, ari.Entidade, ari.Vencto, ',
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
      'SUM(ari.Valor) Valor, CONCAT_WS("-", ari.Boleto, ari.Entidade) BOLENT, ',
      '0 KGT, ari.CartEmiss, car.TipoDoc CAR_TIPODOC, car.Ativo CART_ATIVO, ',
      'cna.CedBanco, cna.JurosPerc, cna.MultaPerc, ari.CNAB_Cfg, pre.Empresa, ',
      'ari.Conta Genero, ari.Texto, ari.Controle, cna.Nome NOMECNAB_Cfg, ',
      'ari.Codigo, ari.Avulso, pre.Periodo, cna.Cedente, ari.FatID, ',
      'ari.FatNum, ari.ModelBloq, ari.Compe, ari.BloqFV, ari.DataCad, ',
      '( ',
      'SELECT COUNT(DISTINCT ppi.Codigo) + 0.000 ',
      'FROM proenprit ppi ',
      'LEFT JOIN proenpr per ON per.Codigo = ppi.Codigo ',
      'WHERE ppi.Entidade = ari.Entidade ',
      'AND (per.Finalidade = 0 OR per.Finalidade IS NULL) ',
      ') Envios, ',
      'ari.Aviso01, ari.Aviso02, ari.Aviso03, ari.Aviso04, ari.Aviso05, ',
      'ari.Aviso06, ari.Aviso07, ari.Aviso08, ari.Aviso09, ari.Aviso10, ',
      'pre.AvisoVerso, COUNT(ari.Codigo) + 0.000 TotIts ',
      'FROM arreits ari ',
      'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade ',
      'LEFT JOIN carteiras car ON car.Codigo=ari.CartEmiss ',
      'LEFT JOIN cnab_cfg cna ON cna.Codigo = ari.CNAB_Cfg ',
      'LEFT JOIN prev pre ON pre.Codigo = ari.Codigo ',
      FQrBloquetosFiltros,
      SQLGroupBy,
      'ORDER BY NOMECNAB_Cfg, ari.Boleto, NOMEENT ',
      '']);
    if Controle <> 0 then
      QrBoletos.Locate('Controle', Controle, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBloGeren.ReopenBoletosIts();
begin
  UBloquetos.ReopenBoletosIts(QrBoletosIts, QrBoletosBoleto.Value,
    QrBoletosEntidade.Value, QrBoletosCNAB_Cfg.Value, QrBoletosCodigo.Value,
    QrBoletosVencto.Value);
  //
  if PageControl2.ActivePageIndex = 0 then
  begin
    if PageControl1.ActivePageIndex = 4 then
      QrBoletosIts.Locate('TEXTO', FBolItsSim, [])
    else
      QrBoletosIts.Locate('TEXTO', FBolItsNao, []);
  end;
end;

procedure TFmBloGeren.ReopenProt1(AbaPro, AbaGerados: Integer);
{$IfNDef SemProtocolo}
var
  ProtTipo: Integer;
begin
  case AbaPro of
    0: //Documentos
      ProtTipo := 1;
    1: //E-mail
      ProtTipo := 2;
    2: //Cobran�a com registro
      ProtTipo := 4;
    else
      ProtTipo := -1;
  end;
  //
  if AbaPro = 2 then //Cobran�a com registro
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrProt1, Dmod.MyDB, [
      'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
      'FROM arreits ari ',
      'LEFT JOIN prev pre ON pre.Codigo = ari.Codigo ',
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = ari.CNAB_cfg ',
      'LEFT JOIN protocolos pro ON pro.Codigo = cfg.ProtocolCR ',
      'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
      FQrBloquetosFiltros,
      'AND pro.Tipo=' + Geral.FF0(ProtTipo),
      'AND pro.def_client=' + Geral.FF0(VAR_ModBloq_EntCliInt),
      'GROUP BY pro.Codigo ',
      'ORDER BY pro.Nome ',
      '']);
  end else
  begin
    if TBGerados.TabIndex = 1 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrProt1, Dmod.MyDB, [
        'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
        'FROM arreits ari ',
        'LEFT JOIN prev pre ON pre.Codigo=ari.Codigo ',
        'LEFT JOIN protpakits ppi ON (ppi.Docum=ari.Boleto AND ppi.Cliente=ari.Entidade ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=pre.Empresa AND ppi.ID_Cod4=ari.CNAB_Cfg) ',
        'LEFT JOIN protocolos pro ON pro.Codigo=ppi.Codigo ',
        'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
        FQrBloquetosFiltros,
        'AND pro.Tipo=' + Geral.FF0(ProtTipo),
        'AND pro.def_client=' + Geral.FF0(VAR_ModBloq_EntCliInt),
        'GROUP BY ppi.Codigo ',
        'ORDER BY pro.Nome ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrProt1, Dmod.MyDB, [
        'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
        'FROM arreits ari ',
        'LEFT JOIN prev pre ON pre.Codigo = ari.Codigo ',
        'LEFT JOIN proenprit ppi ON ppi.Entidade = ari.Entidade ',
        'LEFT JOIN proenpr ppr ON ppr.Codigo = ppi.Codigo ',
        'LEFT JOIN protocolos pro ON pro.Codigo = ppr.Protocolo ',
        'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
        FQrBloquetosFiltros,
        'AND pro.Tipo=' + Geral.FF0(ProtTipo),
        'AND pro.def_client=' + Geral.FF0(VAR_ModBloq_EntCliInt),
        'AND ppr.Finalidade = 0 ',
        'GROUP BY pro.Codigo ',
        'ORDER BY pro.Nome ',
        '']);
    end;
  end;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TFmBloGeren.ReopenProtocoBol;
var
  Aba, ArreIts: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  if (QrBoletosIts.State <> dsInactive) and (QrBoletosIts.RecordCount > 0) then
    ArreIts := QrBoletosItsControle.Value
  else
    ArreIts := 0;
  //
  try
    Aba := TBProtocolos.TabIndex;
    //
    case Aba of
      0:
        DBloGeren.ReopenQuery(1, QrProt1Codigo.Value, ArreIts, TBGerados.TabIndex, FQrBloquetosFiltros);
      1:
        DBloGeren.ReopenQuery(2, QrProt1Codigo.Value, ArreIts, TBGerados.TabIndex, FQrBloquetosFiltros);
      2:
        DBloGeren.ReopenQuery(4, QrProt1Codigo.Value, ArreIts, TBGerados.TabIndex, FQrBloquetosFiltros);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBloGeren.TravaDestPeriodo(Encerrado: Integer; Liberar: Boolean);
var
  Periodo, Codigo: Integer;
begin
  if not Liberar then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then
      Exit;
  end;
  Periodo := QrPrevPeriodo.Value;
  Codigo  := QrPrevCodigo.Value;
  //
  if Encerrado = 1 then
    Encerrado := 0
  else
    Encerrado := 1;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'prev', False,
    ['Encerrado'], ['Codigo'], [Encerrado], [Codigo], True)
  then
    LocPeriodo(Periodo, Periodo);
end;

procedure TFmBloGeren.TravaDestrClick(Sender: TObject);
begin
  TravaDestPeriodo(QrPrevEncerrado.Value, False);
end;

procedure TFmBloGeren.BtProtoGerClick(Sender: TObject);
{$IfNDef SemProtocolo}
var
  Tarefa, Lote: Integer;
{$EndIf}
begin
  {$IfNDef SemProtocolo}
    if PageControl2.ActivePageIndex = 2 then //Lotes protocolos
    begin
      Tarefa := QrProt1Codigo.Value;
      Lote   := DBloGeren.QrProtocoBolLOTE.Value;
    end else
    begin
      Tarefa := 0;
      Lote   := 0;
    end;
    ProtocoUnit.MostraProtoGer(True, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPager1, Tarefa, Lote);
  {$Else}
    dmkPF.InfoSemModulo(mdlappProtocolos);
  {$EndIf}
end;

procedure TFmBloGeren.BtProPadClick(Sender: TObject);
begin
  {$IfNDef SemProtocolo}
    ProtocoUnit.MostraProEnPr(0);
  {$Else}
    dmkPF.InfoSemModulo(mdlappProtocolos);
  {$EndIf}
end;

procedure TFmBloGeren.BtProtocoloClick(Sender: TObject);
begin
  FOrigem := istVisMensal;
  //
  ConfiguraJanela(istVisProtEnt, 2);
end;

procedure TFmBloGeren.BtRetornoClick(Sender: TObject);
begin
  UBloqGerl_Jan.MostraBloCNAB_Ret;
end;

procedure TFmBloGeren.Selecionados3Click(Sender: TObject);
begin
  DesfazerBoletos(istSelecionados, DBGradeBlo, istVisMensal);
end;

procedure TFmBloGeren.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmBloGeren.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmBloGeren.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmBloGeren.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmBloGeren.Todos3Click(Sender: TObject);
begin
  DesfazerBoletos(istTodos, DBGradeBlo, istVisMensal);
end;

procedure TFmBloGeren.Todosabertos1Click(Sender: TObject);
begin
{$IfNDef SemProtocolo}
  try
    TBProtocolos.Enabled   := False;
    BtPEBProtocolo.Enabled := False;
    //
    UnProtocolo.GeraProtocoloBoleto(istTodos, DBloGeren.QrProtocoBol,
      Dmod.QrUpd, DBloGeren.QrLoc, Dmod.MyDB, TDBGrid(DBG_ProtocoBol), PB1,
      VAR_ModBloq_EntCliInt);
    //
    ReopenProtocoBol;
  finally
    TBProtocolos.Enabled   := True;
    BtPEBProtocolo.Enabled := True;
  end;
{$Else}
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

function TFmBloGeren.Va(Para: TVaiPara): Boolean;
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrevPeriodo.Value, LaRegistro.Caption[2]);
  Result := LaRegistro.Caption <> VAR_PERIODO_NAO_LOC;
end;

end.


