unit CondGerArreFut;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, DBCtrls, ComCtrls, Variants,
  dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, UnDmkEnums,
  DmkDAC_PF;

type
  TFmCondGerArreFut = class(TForm)
    Panel1: TPanel;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    DsContas: TDataSource;
    Label2: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    LaDeb: TLabel;
    EdValor: TdmkEdit;
    CkContinuar: TCheckBox;
    LaDepto: TLabel;
    EdDepto: TdmkEditCB;
    CBDepto: TdmkDBLookupComboBox;
    QrDeptos: TmySQLQuery;
    DsDeptos: TDataSource;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    LaAno: TLabel;
    RichEdit1: TRichEdit;
    Label3: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    EdCNAB_Cfg: TdmkEditCB;
    CBCNAB_Cfg: TdmkDBLookupComboBox;
    Label4: TLabel;
    QrCNAB_Cfg: TmySQLQuery;
    DsCNAB_Cfg: TDataSource;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    QrLoc: TmySQLQuery;
    QrDeptosCodigo: TIntegerField;
    QrDeptosNome: TWideStringField;
    LaNFSeSrvCad: TLabel;
    EdNFSeSrvCad: TdmkEditCB;
    CBNFSeSrvCad: TdmkDBLookupComboBox;
    SBNFSeSrvCad: TSpeedButton;
    QrNFSeSrvCad: TmySQLQuery;
    QrNFSeSrvCadCodigo: TIntegerField;
    QrNFSeSrvCadNome: TWideStringField;
    DsNFSeSrvCad: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SBNFSeSrvCadClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCliInt, FEntCliInt, FArreFutControle: Integer;
  end;

  var
  FmCondGerArreFut: TFmCondGerArreFut;

implementation

uses Module, UnInternalConsts, UMySQLModule, UnMSgInt, UnMyObjects, MyDBCheck,
  {$IfNDef sNFSe} NFSe_PF_0000, {$EndIf}
  UnDmkProcFunc, UnBloquetos, UnBloqGerl, ModuleGeral, MyListas;

{$R *.DFM}

procedure TFmCondGerArreFut.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerArreFut.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerArreFut.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerArreFut.FormShow(Sender: TObject);
begin
  UBloqGerl.ReopenCNAB_Cfg(QrCNAB_Cfg, Dmod.MyDB, FEntCliInt);
end;

procedure TFmCondGerArreFut.SBNFSeSrvCadClick(Sender: TObject);
begin
  {$IfNDef sNFSe}
  VAR_CADASTRO := 0;
  //
  UnNFSe_PF_0000.MostraFormNFSeSrvCad(EdNFSeSrvCad.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdNFSeSrvCad, CBNFSeSrvCad, QrNFSeSrvCad, VAR_CADASTRO);
    //
    EdNFSeSrvCad.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmCondGerArreFut.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Periodo: Integer;
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if QrContasMensal.Value = 'V' then
    begin
      Periodo := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
      Mes     := dmkPF.MesEAnoDoPeriodoLongo(Periodo);
    end;
    if key=VK_F4 then EdDescricao.Text := QrContasNome.Value  + ' ' + Mes;
    if key=VK_F5 then EdDescricao.Text := QrContasNome2.Value + ' ' + Mes;
    if key=VK_F6 then EdDescricao.Text := QrContasNome3.Value + ' ' + Mes;
    //
    EdDescricao.SetFocus;
    EdDescricao.SelStart  := Length(EdDescricao.Text);
    EdDescricao.SelLength := 0;
  end;
end;

procedure TFmCondGerArreFut.BtOKClick(Sender: TObject);
var
  Conta, Controle, Depto, Periodo, CNAB_Cfg, NFSeSrvCad: Integer;
  Valor: Double;
  Texto, Msg: String;
begin
  Periodo    := dmkPF.PeriodoEncode(Geral.IMV(CBAno.Items[CBAno.ItemIndex]), CBMes.ItemIndex+1);
  Depto      := EdDepto.ValueVariant;
  Conta      := EdConta.ValueVariant;
  Valor      := EdValor.ValueVariant;
  Texto      := Trim(EdDescricao.Text);
  CNAB_Cfg   := EdCNAB_Cfg.ValueVariant;
  NFSeSrvCad := EdNFSeSrvCad.ValueVariant;
  //
  if VAR_KIND_DEPTO = kdUH then
  begin
    if MyObjects.FIC(Depto = 0, EdDepto, 'A unidade deve ser informada!') then Exit;
  end else
  begin
    if MyObjects.FIC(Depto = 0, EdDepto, 'A entidade deve ser informada!') then Exit;
  end;
  //
  if MyObjects.FIC(CNAB_Cfg = 0, EdCNAB_Cfg, 'A configura��o do boleto deve ser informada!') then Exit;
  if MyObjects.FIC(Conta = 0, EdConta, 'A conta deve ser informada!') then Exit;
  if MyObjects.FIC(Valor = 0, EdValor, 'O valor deve ser informado!') then Exit;
  if MyObjects.FIC(Texto = '', EdDescricao, 'O texto deve ser informado!') then Exit;
  //
  if Valor > 0 then
  begin
    if MyObjects.FIC(UpperCase(QrContasCredito.Value) = 'F', EdConta, 'A conta selecionada deve ser de cr�dito!') then Exit;
  end else
  begin
    if MyObjects.FIC(UpperCase(QrContasDebito.Value) = 'F', EdConta, 'A conta selecionada deve ser de d�bito!') then Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    if ImgTipo.SQLType = stIns then
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'ArreFut', 'ArreFut', 'Controle')
    else
      Controle := FArreFutControle;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ArreFut', False,
      ['Conta', 'Texto', 'Valor', 'Depto', 'CliInt', 'Periodo', 'CNAB_Cfg', 'NFSeSrvCad'], ['Controle'],
      [Conta, Texto, Valor, Depto, FCliInt, Periodo, CNAB_Cfg, NFSeSrvCad], [Controle], True) then
    begin
      if CkContinuar.Checked then
      begin
        ImgTipo.SQLType         := stIns;
        EdDepto.ValueVariant    := 0;
        CBDepto.KeyValue        := NULL;
        EdCNAB_Cfg.ValueVariant := 0;
        CBCNAB_Cfg.KeyValue     := Null;
        //
        Geral.MB_Aviso('Item inclu�do com sucesso!');
        //
        CBMes.SetFocus;
      end else
        Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGerArreFut.FormCreate(Sender: TObject);
var
  TemNFSe: Boolean;
begin
  ImgTipo.SQLType := stLok;
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, 0);
  //
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  //
  {$IfNDef sNFSe}
  UMyMod.AbreQuery(QrNFSeSrvCad, Dmod.MyDB);
  //
  TemNFSe := DBCheck.LiberaModulo(CO_DMKID_APP, dmkPF.ObtemSiglaModulo(mdlappNFSe),
               DModG.QrMaster.FieldByName('HabilModulos').AsString);
  {$Else}
  QrNFSeSrvCad.Close;
  //
  TemNFSe := False;
  {$EndIf}
  LaNFSeSrvCad.Visible := TemNFSe;
  EdNFSeSrvCad.Visible := TemNFSe;
  CBNFSeSrvCad.Visible := TemNFSe;
  SBNFSeSrvCad.Visible := TemNFSe;
  //
  if VAR_KIND_DEPTO = kdUH then
    LaDepto.Caption := 'Unidade:'
  else
    LaDepto.Caption := 'Entidade:';
end;

end.
