unit UnBloqGerl_Tabs;

{ Colocar no MyListas:

Uses UnBloqGerl_Tabs;


//
function TMyListas.CriaListaTabelas:

      BloqGerl_Tb.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
      BloqGerl_Tb.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:

    BloqGerl_Tb.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      BloqGerl_Tb.CarregaListaFRIndices(Tabela, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      BloqGerl_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos);
    BloqGerl_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  BloqGerl_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections, Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Dialogs, DB, (*DBTables,*) UnMyLinguas, Forms, mysqlDBTables,
  UnInternalConsts, dmkGeral, UnDmkEnums, DmkDAC_PF;

type
  TUnBloqGerl_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Database: TmySQLDatabase; Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
  end;

//const

var
  BloqGerl_Tabs: TUnBloqGerl_Tabs;

implementation

uses UMySQLModule;

function TUnBloqGerl_Tabs.CarregaListaTabelas(Database: TmySQLDatabase;
  Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, LowerCase('arrefut'), '');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnBloqGerl_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
end;

function TUnBloqGerl_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('xxxx') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
end;


function TUnBloqGerl_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('ArreFut') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TUnBloqGerl_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('ArreFut') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Depto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := TAB_ARI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Inclui';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Cfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFSeSrvCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnBloqGerl_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'ArreFut';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnBloqGerl_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  // BLO-GEREN-006 :: Per�odo
  New(FRJanelas);
  FRJanelas.ID        := 'BLO-GEREN-006';
  FRJanelas.Nome      := 'FmBloGerenLocper';
  FRJanelas.Descricao := 'Per�odo';
  FRJanelas.Modulo    := 'BLOQ';
  FLJanelas.Add(FRJanelas);
  //
  // BLO-GEREN-015 :: Retorno de Arquivos CNAB
  New(FRJanelas);
  FRJanelas.ID        := 'BLO-GEREN-015';
  FRJanelas.Nome      := 'FmBloCNAB_Ret';
  FRJanelas.Descricao := 'Retorno de Arquivos CNAB';
  FRJanelas.Modulo    := 'BLOQ';
  FLJanelas.Add(FRJanelas);
  //
  // BLO-PROTO-002 :: Boletos Sem Lote
  New(FRJanelas);
  FRJanelas.ID        := 'BLO-PROTO-002';
  FRJanelas.Nome      := 'FmBloSemlot';
  FRJanelas.Descricao := 'Boletos Sem Lote';
  FRJanelas.Modulo    := 'BLOQ';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-025 :: Item de Arrecada��o Futura
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-025';
  FRJanelas.Nome      := 'FmCondGerArreFut';
  FRJanelas.Descricao := 'Item de Arrecada��o Futura';
  FRJanelas.Modulo    := 'BLOQ';
  FLJanelas.Add(FRJanelas);
  //
  // GER-CONDM-026 :: Arrecada��es Pr�-agendadas
  New(FRJanelas);
  FRJanelas.ID        := 'GER-CONDM-026';
  FRJanelas.Nome      := 'FmCondGerArreFutA';
  FRJanelas.Descricao := 'Arrecada��es Pr�-agendadas';
  FRJanelas.Modulo    := 'BLOQ';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
