unit BloGerenLocper;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, DB, mySQLDbTables, UnDmkProcFunc,
  Variants, dmkImage, dmkGeral, dmkPermissoes, UnDmkEnums;

type
  TFmBloGerenLocper = class(TForm)
    Panel1: TPanel;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    LBPeriodos: TDBLookupListBox;
    QrPrev: TmySQLQuery;
    QrPrevPeriodo: TIntegerField;
    QrPrevNOME_PER: TWideStringField;
    DsPrev: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPrevCalcFields(DataSet: TDataSet);
    procedure LBPeriodosClick(Sender: TObject);
    procedure LBPeriodosDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure Localizar;
  public
    { Public declarations }
    FLocalizar: Boolean;
    FPeriodo  : Integer;
  end;

  var
  FmBloGerenLocper: TFmBloGerenLocper;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmBloGerenLocper.BtDesisteClick(Sender: TObject);
begin
  FLocalizar := False;
  Close;
end;

procedure TFmBloGerenLocper.BtOKClick(Sender: TObject);
begin
  Localizar;
end;

procedure TFmBloGerenLocper.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmBloGerenLocper.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FLocalizar := False;
end;

procedure TFmBloGerenLocper.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloGerenLocper.LBPeriodosClick(Sender: TObject);
begin
  BtOK.Enabled := LBPeriodos.KeyValue <> Null;
end;

procedure TFmBloGerenLocper.LBPeriodosDblClick(Sender: TObject);
begin
  Localizar;
end;

procedure TFmBloGerenLocper.Localizar;
begin
  FLocalizar := True;
  FPeriodo   := QrprevPeriodo.Value;
  //
  Close;
end;

procedure TFmBloGerenLocper.QrPrevCalcFields(DataSet: TDataSet);
begin
  QrPrevNOME_PER.Value := dmkPF.MesEAnoDoPeriodo(QrPrevPeriodo.Value);
end;

end.
