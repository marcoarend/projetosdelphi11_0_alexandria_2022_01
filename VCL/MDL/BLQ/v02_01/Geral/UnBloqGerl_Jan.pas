unit UnBloqGerl_Jan;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, dmkImage, Forms, Controls, Windows,
  SysUtils, ComCtrls, Grids, DBGrids, AdvToolBar, DB, UnDmkProcFunc, ExtCtrls,
  dmkDBGrid, Math, Dialogs, UnDmkEnums, Classes, dmkDBGridZTO, frxDBSet,
  frxClass, UnInternalConsts;

type
  TUnBloqGerl_Jan = class(TObject)
  private
  public
    function  LocalizarPeriodoAtual(CliInt: Integer; Tabela: String;
              Cond: Boolean = False): Integer;
    procedure MostraCNAB_Cfg(Codigo: Integer);
    procedure MostraBloCNAB_Ret;
    procedure MostraFmCondGerArreFut(SQLType: TSQLType; CliInt, EntCliInt: Integer;
              QrArreFut: TmySQLQuery; QueryDados: TMySQLQuery = nil;
              QueryDados2: TMySQLQuery = nil);
    procedure VerificaArreFut(QueryArreFutA, QueryArreFutI: TmySQLQuery;
              DataSourceArreFutA: TDataSource; CliInt, Codigo, Periodo: Integer;
              TabAriA: String);
  end;

var
  UBloqGerl_Jan: TUnBloqGerl_Jan;

implementation

uses MyDBCheck, UnMyObjects, BloGerenLocper, DmkDAC_PF, Module, ModuleGeral,
  CNAB_Cfg, BloCNAB_Ret, CondGerArreFut, CondGerArreFutA;

{ TUnBloqGerl_Jan }

function TUnBloqGerl_Jan.LocalizarPeriodoAtual(CliInt: Integer; Tabela: String;
  Cond: Boolean = False): Integer;
var
  Localizar: Boolean;
  Periodo: Integer;
begin
  Result := 0;
  //
  if DBCheck.CriaFm(TFmBloGerenLocper, FmBloGerenLocper, afmoNegarComAviso) then
  begin
    FmBloGerenLocper.FLocalizar := False;
    //
    if not Cond then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(FmBloGerenLocper.QrPrev, Dmod.MyDB, [
        'SELECT Periodo ',
        'FROM ' + Tabela,
        'WHERE Empresa=' + Geral.FF0(CliInt),
        'ORDER BY Periodo DESC ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(FmBloGerenLocper.QrPrev, Dmod.MyDB, [
        'SELECT Periodo ',
        'FROM ' + Tabela,
        'WHERE Cond=' + Geral.FF0(CliInt),
        'ORDER BY Periodo DESC ',
        '']);
    end;
    //
    FmBloGerenLocper.ShowModal;
    //
    Localizar := FmBloGerenLocper.FLocalizar;
    Periodo   := FmBloGerenLocper.FPeriodo;
    //
    FmBloGerenLocper.Destroy;
    Application.ProcessMessages;
    if Localizar then
      Result := Periodo;
  end;
end;

procedure TUnBloqGerl_Jan.MostraBloCNAB_Ret;
begin
{$IfNDef NO_CNAB}
 if DBCheck.CriaFm(TFmBloCNAB_Ret, FmBloCNAB_Ret, afmoNegarComAviso) then
  begin
    FmBloCNAB_Ret.ShowModal;
    FmBloCNAB_Ret.Destroy;
  end;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappCNAB);
{$EndIf}
end;

procedure TUnBloqGerl_Jan.MostraCNAB_Cfg(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCNAB_Cfg.LocCod(Codigo, Codigo);
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
  end;
end;

procedure TUnBloqGerl_Jan.MostraFmCondGerArreFut(SQLType: TSQLType; CliInt,
  EntCliInt: Integer; QrArreFut: TmySQLQuery; QueryDados: TMySQLQuery = nil;
  QueryDados2: TMySQLQuery = nil);
var
  Ano, Mes, Dia: Word;
  i: Integer;
begin
  if DBCheck.CriaFm(TFmCondGerArreFut, FmCondGerArreFut, afmoNegarComAviso) then
  begin
    FmCondGerArreFut.FCliInt         := CliInt;
    FmCondGerArreFut.FEntCliInt      := EntCliInt;
    FmCondGerArreFut.ImgTipo.SQLType := SQLType;
    //
    if VAR_KIND_DEPTO = kdUH then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(FmCondGerArreFut.QrDeptos, Dmod.MyDB, [
        'SELECT Unidade Nome, Conta Codigo ',
        'FROM condimov ',
        'WHERE Codigo=' + Geral.FF0(CliInt),
        'ORDER BY Andar, Unidade ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(FmCondGerArreFut.QrDeptos, Dmod.MyDB, [
        'SELECT IF(Tipo=0, RazaoSocial, Nome) Nome, Codigo ',
        'FROM entidades ',
        'WHERE Ativo = 1 ',
        'ORDER BY Nome ',
        '']);
    end;
    if SQLType = stUpd then
    begin
      Ano := dmkPF.AnoDoPeriodo(QrArreFut.FieldByName('Periodo').AsInteger);
      Mes := dmkPF.MesDoPeriodo(QrArreFut.FieldByName('Periodo').AsInteger);
      with FmCondGerArreFut do
      begin
        FArreFutControle          := QrArreFut.FieldByName('Controle').AsInteger;
        EdDepto.ValueVariant      := QrArreFut.FieldByName('Depto').AsInteger;
        CBDepto.KeyValue          := QrArreFut.FieldByName('Depto').AsInteger;
        EdCNAB_Cfg.ValueVariant   := QrArreFut.FieldByName('CNAB_Cfg').AsInteger;
        CBCNAB_Cfg.KeyValue       := QrArreFut.FieldByName('CNAB_Cfg').AsInteger;
        EdNFSeSrvCad.ValueVariant := QrArreFut.FieldByName('NFSeSrvCad').AsInteger;
        CBNFSeSrvCad.KeyValue     := QrArreFut.FieldByName('NFSeSrvCad').AsInteger;
        EdConta.ValueVariant      := QrArreFut.FieldByName('Conta').AsInteger;
        CBConta.KeyValue          := QrArreFut.FieldByName('Conta').AsInteger;
        EdValor.ValueVariant      := QrArreFut.FieldByName('Valor').AsFloat;
        EdDescricao.Text          := QrArreFut.FieldByName('Texto').AsString;
        //
        CkContinuar.Visible := False;
        CkContinuar.Checked := False;
        //
        for i := 0 to FmCondGerArreFut.CBAno.Items.Count - 1 do
        begin
          if Geral.IMV(FmCondGerArreFut.CBAno.Items[i]) = Ano then
            FmCondGerArreFut.CBAno.ItemIndex := i;
        end;
        FmCondGerArreFut.CBMes.ItemIndex := Mes-1;
      end;
    end else
    begin
      with FmCondGerArreFut do
      begin
        if ((QueryDados <> nil) and (QueryDados.Name = 'QrLeiItens')) and
          ((QueryDados2 <> nil) and (QueryDados2.Name = 'QrLei')) then
        begin
          DecodeDate(DmodG.ObtemAgora(), Ano, Mes, Dia);
          //
          if VAR_KIND_DEPTO = kdUH then
          begin
            EdDepto.ValueVariant := QueryDados.FieldByName('Apto').AsInteger;
            CBDepto.KeyValue     := QueryDados.FieldByName('Apto').AsInteger;
          end else
          begin
            EdDepto.ValueVariant := QueryDados.FieldByName('Cliente').AsInteger;
            CBDepto.KeyValue     := QueryDados.FieldByName('Cliente').AsInteger;
          end;
          //
          if Dmod.QrControle.FieldByName('CNABCtaJur').AsInteger > 0 then
          begin
            EdConta.ValueVariant := DmodG.QrControle.FieldByName('CNABCtaJur').AsInteger;
            CBConta.KeyValue     := DmodG.QrControle.FieldByName('CNABCtaJur').AsInteger;
          end else ;
          EdValor.ValueVariant := QueryDados2.FieldByName('DJM').AsFloat;
          EdDescricao.Text     := 'Diferen�a de juros ' + QueryDados.FieldByName('Mez_TXT').AsString +
                                  ' (boleto ' + FormatFloat('00000000', QueryDados2.FieldByName('IDNum').AsInteger)+')';
          //
          for i := 0 to FmCondGerArreFut.CBAno.Items.Count - 1 do
          begin
            if Geral.IMV(FmCondGerArreFut.CBAno.Items[i]) = Ano then
              FmCondGerArreFut.CBAno.ItemIndex := i;
          end;
          FmCondGerArreFut.CBMes.ItemIndex := Mes-1;
        end else
        if ((QueryDados <> nil) and (QueryDados.Name = 'QrLct')) and
          (QueryDados2 = nil) then
        begin
          DecodeDate(DmodG.ObtemAgora(), Ano, Mes, Dia);
          //
          EdConta.ValueVariant := QueryDados.FieldByName('Genero').AsInteger;
          CBConta.KeyValue     := QueryDados.FieldByName('Genero').AsInteger;
          //
          if QueryDados.FieldByName('Credito').AsFloat > 0 then
          begin
            if VAR_KIND_DEPTO = kdUH then
            begin
              EdDepto.ValueVariant := QueryDados.FieldByName('Depto').AsInteger;
              CBDepto.KeyValue     := QueryDados.FieldByName('Depto').AsInteger;
            end else
            begin
              EdDepto.ValueVariant := QueryDados.FieldByName('Cliente').AsInteger;
              CBDepto.KeyValue     := QueryDados.FieldByName('Cliente').AsInteger;
            end;
            EdValor.ValueVariant := Geral.FFT(QueryDados.FieldByName('Credito').AsFloat, 2, siPositivo);
          end else
          begin
            if VAR_KIND_DEPTO = kdUH then
            begin
              EdDepto.ValueVariant := QueryDados.FieldByName('Depto').AsInteger;
              CBDepto.KeyValue     := QueryDados.FieldByName('Depto').AsInteger;
            end else
            begin
              EdDepto.ValueVariant := QueryDados.FieldByName('Fornecedor').AsInteger;
              CBDepto.KeyValue     := QueryDados.FieldByName('Fornecedor').AsInteger;
            end;
            EdValor.ValueVariant := QueryDados.FieldByName('Debito').AsFloat * -1;
          end;
          EdDescricao.Text := QueryDados.FieldByName('Descricao').AsString;
          //
          for i := 0 to FmCondGerArreFut.CBAno.Items.Count - 1 do
          begin
            if Geral.IMV(FmCondGerArreFut.CBAno.Items[i]) = Ano then
              FmCondGerArreFut.CBAno.ItemIndex := i;
          end;
          FmCondGerArreFut.CBMes.ItemIndex := Mes-1;
        end;
        CkContinuar.Visible := True;
        CkContinuar.Checked := True;
      end;
    end;
    FmCondGerArreFut.ShowModal;
    FmCondGerArreFut.Destroy;
  end;
end;

procedure TUnBloqGerl_Jan.VerificaArreFut(QueryArreFutA,
  QueryArreFutI: TmySQLQuery; DataSourceArreFutA: TDataSource; CliInt, Codigo,
  Periodo: Integer; TabAriA: String);

  procedure MostraFmCondGerArreFutA();
  begin
    if DBCheck.CriaFm(TFmCondGerArreFutA, FmCondGerArreFutA, afmoNegarComAviso) then
    begin
      FmCondGerArreFutA.FQueryArreFutI      := QueryArreFutI;
      FmCondGerArreFutA.FQueryArreFutA      := QueryArreFutA;
      FmCondGerArreFutA.FDataSourceArreFutA := DataSourceArreFutA;
      FmCondGerArreFutA.FTabAriA            := TabAriA;
      FmCondGerArreFutA.FCodigo             := Codigo;
      FmCondGerArreFutA.ShowModal;
      FmCondGerArreFutA.Destroy;
    end;
  end;

begin
  if VAR_KIND_DEPTO = kdUH then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QueryArreFutA, Dmod.MyDB, [
      'SELECT IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPROP, ',
      'imv.Unidade UNIDADE, con.Nome NOMECONTA, imv.Propriet, ',
      'cfg.CartEmiss, arf.* ',
      'FROM arrefut arf ',
      'LEFT JOIN condimov  imv ON imv.Conta=arf.Depto ',
      'LEFT JOIN entidades prp ON prp.Codigo=imv.Propriet ',
      'LEFT JOIN contas    con ON con.Codigo=arf.Conta ',
      'LEFT JOIN cnab_cfg  cfg ON cfg.Codigo=arf.CNAB_Cfg ',
      'WHERE arf.' + TAB_ARI + '=0 ',
      'AND arf.CliInt=' + Geral.FF0(CliInt),
      'AND arf.Periodo=' + Geral.FF0(Periodo),
      'ORDER BY arf.Periodo DESC ',
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QueryArreFutA, Dmod.MyDB, [
      'SELECT IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEENT, ',
      'con.Nome NOMECONTA, cfg.CartEmiss, arf.* ',
      'FROM arrefut arf ',
      'LEFT JOIN entidades prp ON prp.Codigo=arf.Depto ',
      'LEFT JOIN contas    con ON con.Codigo=arf.Conta ',
      'LEFT JOIN cnab_cfg  cfg ON cfg.Codigo=arf.CNAB_Cfg ',
      'WHERE arf.' + TAB_ARI + '=0 ',
      'AND arf.CliInt=' + Geral.FF0(CliInt),
      'AND arf.Periodo=' + Geral.FF0(Periodo),
      'ORDER BY arf.Periodo DESC ',
      '']);
  end;
  if QueryArreFutA.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('Foram localizados ' +
      Geral.FF0(QueryArreFutA.RecordCount) + ' itens de arrecada��o com ' +
      'pr�-agendadamento para este per�odo ou per�odos anteriores que n�o ' +
      'foram inclu�dos ainda.' + sLineBreak + 'Deseja visualiz�-los agora?') = ID_YES
    then
      MostraFmCondGerArreFutA;
  end else
    Geral.MB_Aviso('N�o foram localizados itens de ' +
      'arrecada��o com pr�-agendadamento para este per�odo ou per�odos anteriores!');
end;

end.
