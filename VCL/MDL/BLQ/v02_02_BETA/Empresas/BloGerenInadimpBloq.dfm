object FmBloGerenInadimpBloq: TFmBloGerenInadimpBloq
  Left = 339
  Top = 185
  Caption = 'BLO-GEREN-014 :: Boletos Inadimplentes'
  ClientHeight = 879
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 687
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 732
      Top = 69
      Width = 13
      Height = 618
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 69
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 20
        Top = 10
        Width = 108
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Novo vencimento:'
      end
      object Label2: TLabel
        Left = 162
        Top = 10
        Width = 50
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '% Multa:'
      end
      object Label3: TLabel
        Left = 266
        Top = 10
        Width = 87
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '% Juros / m'#234's:'
      end
      object LaAviso: TLabel
        Left = 379
        Top = 34
        Width = 720
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        AutoSize = False
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TPVencto: TdmkEditDateTimePicker
        Left = 20
        Top = 30
        Width = 138
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 40440.407388738430000000
        Time = 40440.407388738430000000
        TabOrder = 0
        OnClick = TPVenctoClick
        OnChange = TPVenctoChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdMulta: TdmkEdit
        Left = 162
        Top = 30
        Width = 99
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2.000000000000000000
        ValWarn = False
        OnChange = EdMultaChange
      end
      object EdJuros: TdmkEdit
        Left = 266
        Top = 30
        Width = 98
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.000000000000000000
        ValWarn = False
        OnChange = EdJurosChange
      end
      object CkZerado: TCheckBox
        Left = 1113
        Top = 12
        Width = 119
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sem val/vcto.'
        TabOrder = 3
        Visible = False
      end
      object CkDesign: TCheckBox
        Left = 1113
        Top = 34
        Width = 119
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Design'
        TabOrder = 4
        Visible = False
      end
    end
    object DBGBloq: TdmkDBGridDAC
      Left = 0
      Top = 69
      Width = 732
      Height = 618
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      SQLFieldsToChange.Strings = (
        'Ativo')
      SQLIndexesOnUpdate.Strings = (
        'Controle'
        'FatNum')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Imp.'
          Width = 31
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cliente'
          Title.Caption = 'ID Entidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENT'
          Title.Caption = 'Entidade'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Boleto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NewVencto'
          Title.Caption = 'Novo Vct.'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PEND_VAL'
          Title.Caption = '$ A pagar'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEZ_TXT'
          Title.Caption = 'M'#234's'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vct. Original'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CREDITO'
          Title.Caption = 'Val. Original'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PAGO'
          Title.Caption = 'Pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Multa'
          Title.Caption = '$ Multa'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Juros'
          Title.Caption = '$ Juros'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 173
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEEMP'
          Title.Caption = 'Empresa'
          Width = 228
          Visible = True
        end>
      Color = clWindow
      DataSource = DsBloqInad
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
      SQLTable = 'bloqinadcli'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Imp.'
          Width = 31
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cliente'
          Title.Caption = 'ID Entidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENT'
          Title.Caption = 'Entidade'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Boleto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NewVencto'
          Title.Caption = 'Novo Vct.'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PEND_VAL'
          Title.Caption = '$ A pagar'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEZ_TXT'
          Title.Caption = 'M'#234's'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vct. Original'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CREDITO'
          Title.Caption = 'Val. Original'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PAGO'
          Title.Caption = 'Pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Multa'
          Title.Caption = '$ Multa'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Juros'
          Title.Caption = '$ Juros'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 173
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEEMP'
          Title.Caption = 'Empresa'
          Width = 228
          Visible = True
        end>
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 745
      Top = 69
      Width = 496
      Height = 618
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Columns = <
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'Conta'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Hist'#243'rico'
          Width = 257
          Visible = True
        end>
      Color = clWindow
      DataSource = DsBloqIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'Conta'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Hist'#243'rico'
          Width = 257
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 314
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Boletos Inadimplentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 314
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Boletos Inadimplentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 314
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Boletos Inadimplentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 746
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 800
    Width = 1241
    Height = 79
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 1059
        Top = 0
        Width = 178
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 4
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtRecalcula: TBitBtn
        Tag = 10064
        Left = 25
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Recalcula'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtRecalculaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 177
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 416
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 569
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtNenhumClick
      end
    end
  end
  object QrBloqInad: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrBloqInadBeforeClose
    AfterScroll = QrBloqInadAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM bloqinadcli'
      'WHERE Tipo = 0')
    Left = 284
    Top = 220
    object QrBloqInadData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBloqInadCREDITO: TFloatField
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadPAGO: TFloatField
      FieldName = 'PAGO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrBloqInadVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrBloqInadCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBloqInadControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrBloqInadJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadMulta: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object QrBloqInadFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBloqInadPEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBloqInadMEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Required = True
      Size = 7
    end
    object QrBloqInadVCTO_TXT: TWideStringField
      FieldName = 'VCTO_TXT'
      Size = 10
    end
    object QrBloqInadCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrBloqInadNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrBloqInadNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrBloqInadAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'clientes.Ativo'
      MaxValue = 1
    end
    object QrBloqInadCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrBloqInadNewVencto: TDateField
      FieldName = 'NewVencto'
    end
  end
  object DsBloqInad: TDataSource
    DataSet = QrBloqInad
    Left = 312
    Top = 220
  end
  object frxDsBoletos: TfrxDBDataset
    UserName = 'frxDsBoletos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Entidade=Entidade'
      'Vencto=Vencto'
      'NOMEENT=NOMEENT'
      'BOLENT=BOLENT'
      'KGT=KGT'
      'VENCTO_TXT=VENCTO_TXT'
      'Valor=Valor'
      'CartEmiss=CartEmiss'
      'CAR_TIPODOC=CAR_TIPODOC'
      'CART_ATIVO=CART_ATIVO'
      'CedBanco=CedBanco'
      'JurosPerc=JurosPerc'
      'TermoAceite=TermoAceite'
      'MultaPerc=MultaPerc'
      'CNAB_Cfg=CNAB_Cfg'
      'Empresa=Empresa'
      'Genero=Genero'
      'Texto=Texto'
      'Controle=Controle'
      'NOMECNAB_Cfg=NOMECNAB_Cfg'
      'Boleto=Boleto'
      'Codigo=Codigo'
      'Avulso=Avulso'
      'Periodo=Periodo'
      'FatID=FatID'
      'FatNum=FatNum'
      'ModelBloq=ModelBloq'
      'Compe=Compe'
      'BloqFV=BloqFV'
      'Aviso01=Aviso01'
      'Aviso02=Aviso02'
      'Aviso03=Aviso03'
      'Aviso04=Aviso04'
      'Aviso05=Aviso05'
      'Aviso06=Aviso06'
      'Aviso07=Aviso07'
      'Aviso08=Aviso08'
      'Aviso09=Aviso09'
      'Aviso10=Aviso10'
      'AvisoVerso=AvisoVerso'
      'DataCad=DataCad'
      'Cedente=Cedente'
      'ModalCobr=ModalCobr'
      'CedAgencia=CedAgencia'
      'CedPosto=CedPosto'
      'CedConta=CedConta'
      'CartNum=CartNum'
      'CodEmprBco=CodEmprBco'
      'TipoCobranca=TipoCobranca'
      'EspecieDoc=EspecieDoc'
      'CNAB=CNAB'
      'CtaCooper=CtaCooper'
      'LayoutRem=LayoutRem'
      'CorresBco=CorresBco'
      'CorresAge=CorresAge'
      'CedDAC_A=CedDAC_A'
      'CedDAC_C=CedDAC_C'
      'CorresCto=CorresCto'
      'IDCobranca=IDCobranca'
      'CART_IMP=CART_IMP'
      'DVB=DVB'
      'OperCodi=OperCodi')
    DataSet = QrBoletos
    BCDToCurrency = False
    Left = 52
    Top = 284
  end
  object QrBoletos: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrBoletosBeforeClose
    AfterScroll = QrBoletosAfterScroll
    Left = 80
    Top = 284
    object QrBoletosEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'arreits.Entidade'
    end
    object QrBoletosVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'arreits.Vencto'
    end
    object QrBoletosNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrBoletosBOLENT: TWideStringField
      FieldName = 'BOLENT'
      Required = True
      Size = 65
    end
    object QrBoletosKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrBoletosVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Calculated = True
    end
    object QrBoletosValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBoletosCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Origin = 'arreits.CartEmiss'
    end
    object QrBoletosCAR_TIPODOC: TSmallintField
      FieldName = 'CAR_TIPODOC'
      Origin = 'carteiras.TipoDoc'
    end
    object QrBoletosCART_ATIVO: TSmallintField
      FieldName = 'CART_ATIVO'
      Origin = 'carteiras.Ativo'
    end
    object QrBoletosCedBanco: TIntegerField
      FieldName = 'CedBanco'
      Origin = 'cnab_cfg.CedBanco'
    end
    object QrBoletosJurosPerc: TFloatField
      FieldName = 'JurosPerc'
      Origin = 'cnab_cfg.JurosPerc'
    end
    object QrBoletosTermoAceite: TIntegerField
      FieldName = 'TermoAceite'
    end
    object QrBoletosMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'cnab_cfg.MultaPerc'
    end
    object QrBoletosCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
      Origin = 'arreits.CNAB_Cfg'
    end
    object QrBoletosEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'prev.Empresa'
    end
    object QrBoletosGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'arreits.Conta'
    end
    object QrBoletosTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'arreits.Texto'
      Size = 50
    end
    object QrBoletosControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'arreits.Controle'
    end
    object QrBoletosNOMECNAB_Cfg: TWideStringField
      FieldName = 'NOMECNAB_Cfg'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrBoletosBoleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'arreits.Boleto'
    end
    object QrBoletosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBoletosAvulso: TSmallintField
      FieldName = 'Avulso'
      MaxValue = 1
    end
    object QrBoletosPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBoletosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrBoletosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrBoletosModelBloq: TIntegerField
      FieldName = 'ModelBloq'
    end
    object QrBoletosCompe: TSmallintField
      FieldName = 'Compe'
    end
    object QrBoletosBloqFV: TIntegerField
      FieldName = 'BloqFV'
    end
    object QrBoletosAviso01: TWideStringField
      FieldName = 'Aviso01'
      Origin = 'prev.Aviso01'
      Size = 50
    end
    object QrBoletosAviso02: TWideStringField
      FieldName = 'Aviso02'
      Origin = 'prev.Aviso02'
      Size = 50
    end
    object QrBoletosAviso03: TWideStringField
      FieldName = 'Aviso03'
      Origin = 'prev.Aviso03'
      Size = 50
    end
    object QrBoletosAviso04: TWideStringField
      FieldName = 'Aviso04'
      Origin = 'prev.Aviso04'
      Size = 50
    end
    object QrBoletosAviso05: TWideStringField
      FieldName = 'Aviso05'
      Origin = 'prev.Aviso05'
      Size = 50
    end
    object QrBoletosAviso06: TWideStringField
      FieldName = 'Aviso06'
      Origin = 'prev.Aviso06'
      Size = 50
    end
    object QrBoletosAviso07: TWideStringField
      FieldName = 'Aviso07'
      Origin = 'prev.Aviso07'
      Size = 50
    end
    object QrBoletosAviso08: TWideStringField
      FieldName = 'Aviso08'
      Origin = 'prev.Aviso08'
      Size = 50
    end
    object QrBoletosAviso09: TWideStringField
      FieldName = 'Aviso09'
      Origin = 'prev.Aviso09'
      Size = 50
    end
    object QrBoletosAviso10: TWideStringField
      FieldName = 'Aviso10'
      Origin = 'prev.Aviso10'
      Size = 50
    end
    object QrBoletosAvisoVerso: TWideStringField
      FieldName = 'AvisoVerso'
      Origin = 'prev.AvisoVerso'
      Size = 180
    end
    object QrBoletosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBoletosCedente: TIntegerField
      FieldName = 'Cedente'
    end
    object QrBoletosModalCobr: TSmallintField
      FieldName = 'ModalCobr'
    end
    object QrBoletosCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
    end
    object QrBoletosCedPosto: TIntegerField
      FieldName = 'CedPosto'
    end
    object QrBoletosCedConta: TWideStringField
      FieldName = 'CedConta'
      Size = 30
    end
    object QrBoletosCartNum: TWideStringField
      FieldName = 'CartNum'
      Size = 3
    end
    object QrBoletosCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
    end
    object QrBoletosTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
    end
    object QrBoletosEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
    end
    object QrBoletosCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrBoletosCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrBoletosLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrBoletosCorresBco: TIntegerField
      FieldName = 'CorresBco'
    end
    object QrBoletosCorresAge: TIntegerField
      FieldName = 'CorresAge'
    end
    object QrBoletosCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Size = 1
    end
    object QrBoletosCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Size = 1
    end
    object QrBoletosCorresCto: TWideStringField
      FieldName = 'CorresCto'
      Size = 30
    end
    object QrBoletosIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Size = 2
    end
    object QrBoletosCART_IMP: TWideStringField
      FieldName = 'CART_IMP'
      Size = 10
    end
    object QrBoletosDVB: TWideStringField
      FieldName = 'DVB'
      Size = 1
    end
    object QrBoletosOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Size = 3
    end
    object QrBoletosTexto01: TWideStringField
      FieldName = 'Texto01'
      Size = 100
    end
    object QrBoletosTexto02: TWideStringField
      FieldName = 'Texto02'
      Size = 100
    end
    object QrBoletosTexto03: TWideStringField
      FieldName = 'Texto03'
      Size = 100
    end
    object QrBoletosTexto04: TWideStringField
      FieldName = 'Texto04'
      Size = 100
    end
    object QrBoletosTexto05: TWideStringField
      FieldName = 'Texto05'
      Size = 100
    end
    object QrBoletosTexto06: TWideStringField
      FieldName = 'Texto06'
      Size = 100
    end
    object QrBoletosTexto07: TWideStringField
      FieldName = 'Texto07'
      Size = 100
    end
    object QrBoletosTexto08: TWideStringField
      FieldName = 'Texto08'
      Size = 100
    end
    object QrBoletosTexto09: TWideStringField
      FieldName = 'Texto09'
      Size = 100
    end
    object QrBoletosTexto10: TWideStringField
      FieldName = 'Texto10'
      Size = 100
    end
  end
  object QrBloqIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 916
    Top = 156
    object QrBloqItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBloqItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrBloqItsCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsBloqIts: TDataSource
    DataSet = QrBloqIts
    Left = 944
    Top = 156
  end
  object frxDsBoletosIts: TfrxDBDataset
    UserName = 'frxDsBoletosIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TEXTO=TEXTO'
      'VALOR=VALOR'
      'Vencto=Vencto'
      'Controle=Controle'
      'Lancto=Lancto'
      'CNAB_Cfg=CNAB_Cfg'
      'Entidade=Entidade'
      'Avulso=Avulso'
      'Boleto=Boleto'
      'NotaFiscal=NotaFiscal'
      'SerieNF=SerieNF'
      'FatNum=FatNum')
    DataSet = QrBoletosIts
    BCDToCurrency = False
    Left = 108
    Top = 212
  end
  object QrBoletosIts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 136
    Top = 212
    object QrBoletosItsTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Size = 50
    end
    object QrBoletosItsVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBoletosItsVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrBoletosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrBoletosItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrBoletosItsCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrBoletosItsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrBoletosItsAvulso: TIntegerField
      FieldName = 'Avulso'
    end
    object QrBoletosItsBoleto: TFloatField
      FieldName = 'Boleto'
    end
    object QrBoletosItsNotaFiscal: TFloatField
      FieldName = 'NotaFiscal'
    end
    object QrBoletosItsSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 5
    end
    object QrBoletosItsFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 448
    Top = 11
  end
end
