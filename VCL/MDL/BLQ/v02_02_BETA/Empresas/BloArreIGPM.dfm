object FmBloArreIGPM: TFmBloArreIGPM
  Left = 339
  Top = 185
  Caption = 'BLO-ARREC-003 :: Reajuste IGP-M'
  ClientHeight = 381
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 225
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 22
      Top = 15
      Width = 470
      Height = 130
      Caption = ' Dados para o reajuste: '
      TabOrder = 0
      object Label17: TLabel
        Left = 5
        Top = 82
        Width = 105
        Height = 13
        Caption = 'Valor a ser reajustado:'
      end
      object Label7: TLabel
        Left = 370
        Top = 101
        Width = 44
        Height = 13
        Caption = 'Parcelas:'
      end
      object GBIni: TGroupBox
        Left = 5
        Top = 15
        Width = 225
        Height = 64
        Caption = ' Per'#237'odo Inicial: '
        TabOrder = 0
        object Label32: TLabel
          Left = 4
          Top = 15
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object LaAnoI: TLabel
          Left = 158
          Top = 15
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object CBMesI: TComboBox
          Left = 4
          Top = 32
          Width = 150
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnChange = CBMesIChange
        end
        object CBAnoI: TComboBox
          Left = 158
          Top = 32
          Width = 64
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnChange = CBAnoIChange
        end
      end
      object GBFim: TGroupBox
        Left = 238
        Top = 15
        Width = 225
        Height = 64
        Caption = ' Per'#237'odo final: '
        TabOrder = 1
        object Label34: TLabel
          Left = 5
          Top = 15
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object LaAnoF: TLabel
          Left = 158
          Top = 15
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object CBMesF: TComboBox
          Left = 5
          Top = 32
          Width = 150
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnChange = CBMesFChange
        end
        object CBAnoF: TComboBox
          Left = 158
          Top = 32
          Width = 64
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnChange = CBAnoFChange
        end
      end
      object EdValor: TdmkEdit
        Left = 5
        Top = 98
        Width = 120
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdParcelas: TdmkEdit
        Left = 418
        Top = 98
        Width = 45
        Height = 21
        Alignment = taRightJustify
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object BtAtualiza: TBitBtn
        Tag = 22
        Left = 132
        Top = 82
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtAtualizaClick
      end
    end
    object GBDadosNew: TGroupBox
      Left = 22
      Top = 150
      Width = 342
      Height = 65
      Caption = ' Novos dados: '
      TabOrder = 1
      object Label1: TLabel
        Left = 215
        Top = 16
        Width = 55
        Height = 13
        Caption = 'Novo valor:'
      end
      object Label3: TLabel
        Left = 5
        Top = 15
        Width = 70
        Height = 13
        Caption = 'Per'#237'odo inicial:'
      end
      object Label4: TLabel
        Left = 110
        Top = 15
        Width = 63
        Height = 13
        Caption = 'Per'#237'odo final:'
      end
      object EdValorNew: TdmkEdit
        Left = 215
        Top = 32
        Width = 120
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDataINew: TdmkEditDateTimePicker
        Left = 5
        Top = 32
        Width = 100
        Height = 21
        Date = 39844.982392210650000000
        Time = 39844.982392210650000000
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DVencimento'
        UpdCampo = 'DVencimento'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPDataFNew: TdmkEditDateTimePicker
        Left = 110
        Top = 32
        Width = 100
        Height = 21
        Date = 39844.982392210650000000
        Time = 39844.982392210650000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DVencimento'
        UpdCampo = 'DVencimento'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 194
        Height = 32
        Caption = 'Reajuste IGP-M'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 194
        Height = 32
        Caption = 'Reajuste IGP-M'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 194
        Height = 32
        Caption = 'Reajuste IGP-M'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 317
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 2
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtIGPM: TBitBtn
        Tag = 494
        Left = 146
        Top = 3
        Width = 120
        Height = 40
        Caption = '&IGPM'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtIGPMClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 273
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrIGPM: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT AnoMes, IGPM_D'
      'FROM igpm'
      'WHERE AnoMes=:P0')
    Left = 402
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrIGPMAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrIGPMIGPM_D: TFloatField
      FieldName = 'IGPM_D'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 408
    Top = 11
  end
  object QrIGPMAnt: TMySQLQuery
    Database = Dmod.MyDB
    Left = 430
    Top = 208
  end
  object QrIGPMAtu: TMySQLQuery
    Database = Dmod.MyDB
    Left = 457
    Top = 208
  end
end
