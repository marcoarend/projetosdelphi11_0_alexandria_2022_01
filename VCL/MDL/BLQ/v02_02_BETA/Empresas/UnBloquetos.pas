unit UnBloquetos;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, ModuleGeral, dmkImage, Forms,
  Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids, DB, UnDmkProcFunc,
  ExtCtrls, dmkDBGrid, Math, Dialogs, UnDmkEnums, Classes, dmkDBGridZTO,
  frxDBSet, frxClass, UnInternalConsts, UnBloqGerl, UnProjGroup_Consts;

type
  TUnBloquetos = class(TObject)
  private
    {$IFDEF DEFINE_VARLCT}
    procedure CadastroBloGeren(Aba: Integer; InOwner: TWincontrol;
                Pager: TWinControl);
    {$ELSE}
    procedure CadastroBloGeren();
    {$ENDIF}
    procedure CadastroBloGerenInadImp(CliInt: Integer; InOwner: TWincontrol;
                Pager: TWinControl);
  public
    function  TextoExplicativoItemBoleto(EhConsumo, Casas: Integer; MedAnt,
                MedAtu, Consumo, UnidFat: Double; UnidLei, UnidImp: String;
                GeraTyp, CasRat, NaoImpLei: Integer; GeraFat: Double): String;
    function  ObtemNumeroDocumento(NumDocPad: Integer; FatNum, Boleto: Double;
                Lancto: Integer; TabLct: String): Double;
    function  AtualizaLayoutAvisosBoleto(Aviso01, Aviso02, Aviso03, Aviso04,
                Aviso05, Aviso06, Aviso07, Aviso08, Aviso09, Aviso10,
                AvisoVerso: String; ModelBloq, BloqFV, Compe, Codigo,
                Controle: Integer): Boolean;
    function  GeraProtocoloCR(ProtocoloCR, LoteCR, Entidade, Prev,
                CNAB_Cfg: Integer; Valor, NumBloq: Double; Vencto: String): Integer;
    function  AtualizaBloGerenNew(SQLType: TSQLType; CodigoPrev, Encerrado,
                Empresa, Periodo, ModelBloq, Compe, BloqFV: Integer; Aviso01,
                Aviso02, Aviso03, Aviso04, Aviso05, Aviso06, Aviso07, Aviso08,
                Aviso09, Aviso10, AvisoVerso: String; var Mensagem: String): Integer;
    function  AtualizaArrecadacao(SQLType: TSQLType; Codigo, ControleArre, Conta,
                BloArre, BloArreIts, Entidade, CartEmiss, Tipo, Config, NFSeSrvCad,
                Lancto, Avulso, ComoAdd, FatID, FatParcela: Integer;
                Vencto: String; Valor, FatNum, Boleto: Double; Texto: String;
                var Mensagem: String): Integer;
    function  AjustaVencimentoPeriodo(Periodo: Integer; Data: TDateTime): TDateTime;
    function  ExcluiArrecadacao(Codigo, Controle: InTeger; Todos: Boolean): Boolean;
    function  AlteraVencimentoPreFatura(Tipo: TselType; Progress: TProgressBar;
               Grade: TDBGrid; QueryBloquetos: TmySQLQuery): Boolean;
    function  AlteraVencimentoFatura(Tipo: TselType; Progress: TProgressBar;
                Grade: TDBGrid; QueryBloquetos: TmySQLQuery; TabLct: String): Boolean;
    function  DesfazerBoletosFatID(QueryLoc: TmySQLQuery;
                Database: TmySQLDatabase; FatID: Integer; FatNum: Double;
                TabLctA: String): Boolean;
    function  DesfazerFaturaAtual(Codigo, EntCliInt, Controle, Periodo, Lancto,
                Tipo, Config, Entidade, Avulso: Integer; Boleto: Double;
                TabLct: String; ImpedeSePosterior: Boolean;
                Modo: TModoVisualizacaoBloquetos; var MostrouMsg: Boolean): Boolean;
    function  VerificaAntesDeCancelar(Periodo, Tipo, Config: Integer; Boleto: Double): Boolean;
    function  VerificaSeFaturaEstaPaga(TabLct: String; Lancto: Integer): Boolean;
    function  VerificaSeBoletoExiste(Controle: Integer): Boolean;
    function  VerificaFatur_Cfg(const Carteira: Integer; var
                Tipo, Config: Integer): Integer; //No lugar da fun��o VerificaCNAB_Cfg
    function  VerificaArrecadacao(Entidade, Conta, BloArre, BloArreIts,
                Empresa, Periodo: Integer): Boolean;
    function  LocalizaPrev(Empresa, Periodo: Integer): Integer;
    function  EmiteBoletoAvulso(TabLctA: String; Grade: TdmkDBGrid;
                QrLcts: TmySQLQuery; EmiteLotProtocoloSeComRegistro: Boolean;
                FatNum: Double; Tipo, Config: Integer; var LoteProtoCR: Integer): Boolean;
    function  VerificaSeOpcoesForamConfiguradas(): Boolean;
    function  VerificaParcelas(SitCobr, MesI, MesF: Integer; AnoI, AnoF: String;
                HabilBot: Boolean): Double;
    function  AtivaDesativaArreIts(Controle, Ativo: Integer;
                MostraPergunta: Boolean = True): Boolean;
    function  CalculaPeriodoIniFimArrecadacao(SitCobr, ParcPerI,
                ParcPerF, Parc: Integer; DiaVencto: Double; DVencimento,
                DtaPrxRenw: TDateTime; var PeriodoIni, PeriodoFim,
                ExpiraEm: String; var Parcelas: Integer): Boolean; 
    function  VerificaSePeriodoEstaEncerrado(Codigo: Integer): Boolean;
    function  ObtemDocumProtPakIts(ProtPakIts: Integer): Double;
    function  ValidaPeriodo(Periodo: Integer; Inverso: Boolean = False): Integer;
    procedure MostraMensagemPeriodoEncerrado(Bloqueto: Double);
    procedure PreencheModelosBloq(RG: TRadioGroup);
    procedure BloquetosParcelasByFatNum(Empresa, FatID: Integer; FatNumLct,
                FatNumBlo: Double; TabLctA: String;
                EmiteLotProtocoloSeComRegistro: Boolean; Progress: TProgressBar);
    procedure ReajustarArrecadacaoIGPM(PeriodoIni, PeriodoFim, SitCobr,
                IDNum: Integer; Valor: Double);
    procedure VerificaArrencadacoesVencendo();
    procedure ReopenArre(QueryArre, QuerySumARRE: TmySQLQuery; Codigo, Entidade: Integer);
    procedure ReopenArreFut(QueryArreFutI: TmySQLQuery; CliInt, MesesAnt,
              Controle: Integer);
    //Abertura de Janelas
    function  CadastroDeBloOpcoes(): Boolean;
    procedure MostraBloArre(Codigo, Controle: Integer);
    procedure AlteraValorDeFaturasIts(PrevCod, EntCliInt, Controle, Lancto,
                Tipo, Config, Entidade: Integer; ValorArreIts, Boleto: Double;
                TabLct: String);
    procedure AteraDescricaoDeFaturasIts(PrevCod, EntCliInt, Controle, Lancto,
                Tipo, Config, Entidade: Integer; Boleto: Double; Texto, TabLct: String);
    function  ArrecadacoesVencidas_e_AVencer(Hoje: TDateTime;
                MostraForm: Boolean): Integer;
    procedure MostraBloGeren(Aba, FatID, Lancamento: Integer; FatNum: Double;
              PageControl: TPageControl; Pager: TWinControl);
    procedure MostraBloGerenInadImp(PageControl: TPageControl;
                Pager: TWinControl);
    function  ImprimeBloqueto(CkZerado: Boolean; TabLctA: String; QueryBoletos,
                QueryBoletosIts: TmySQLQuery; frxDsBoletos,
                frxDsBoletosIts: TfrxDBDataset; Quais: TselType;
                Como: TfrxImpComo; Arquivo: String;
                Filtro: TfrxCustomExportFilter; Grade: TDBGrid;
                TextoVia2: String = ''): TfrxReport;
    procedure ReopenBoletosIts(Query: TMySQLQuery; Boleto: Double; Entidade,
                Tipo, Config, Codigo: Integer; Vencto: TDate);
  end;

var
  UBloquetos: TUnBloquetos;

implementation

uses
  {$IfNDef SemProtocolo} Protocolo, UnProtocoUnit, {$EndIf}
  {$IfNDef NO_CNAB} UnBco_Rem, UnBancos, {$EndIf}
  Module, DmkDAC_PF, ModuleBloGeren, BloArrItsRnw, BloAvulso, BloGeren,
  BloOpcoes, BloArre, BloGerenInadImp, BloArreIGPM, BloGerenBaAVen,
  GetData, UnMyObjects, MyDBCheck, GetValor, BloImp, MyListas, UnGrl_Geral;

{ TUnBloquetos }

procedure TUnBloquetos.AteraDescricaoDeFaturasIts(PrevCod, EntCliInt, Controle,
  Lancto, Tipo, Config, Entidade: Integer; Boleto: Double; Texto, TabLct: String);
var
   Txt: String;
begin
  if MyObjects.FIC(VerificaSeFaturaEstaPaga(TabLct, Lancto), nil,
    'A fatura n�mero ' + Geral.FFI(Boleto) + ' n�o pode ser editada!' +
    sLineBreak + 'Motivo: H� pagamento(s) lan�ado(s) nela!')
  then
    Exit;
  //
  Txt := Texto;
  //
  if InputQuery('Descri��o', 'Defina uma descri��o', Txt) then
  begin
    if Geral.MB_Pergunta('Confirma a altera��o da descri��o para "' +
      Txt + '"?')  <> ID_YES
    then
      Exit;
    //
    if Controle <> 0 then
    begin
      if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE arreits SET Texto="' + Txt + '"',
        'WHERE Controle=' + Geral.FF0(Controle),
        '']) then
      begin
        if Tipo = 0 then
          UnProtocolo.AtualizaProtocolo('Texto', Txt, PrevCod, EntCliInt, Config,
            Entidade, Boleto, Dmod.MyDB);
      end;
    end;
    if Lancto <> 0 then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE ' + TabLct + ' SET Descricao="' + Txt + '"',
        'WHERE Controle=' + Geral.FF0(Lancto),
        '']);
    end;
  end;
end;

procedure TUnBloquetos.AlteraValorDeFaturasIts(PrevCod, EntCliInt, Controle,
  Lancto, Tipo, Config, Entidade: Integer; ValorArreIts, Boleto: Double; TabLct: String);
var
  Continua: Boolean;
  Valor: Double;
  GetValor: Variant;
begin
  if MyObjects.FIC(VerificaSeFaturaEstaPaga(TabLct, Lancto), nil,
    'A fatura n�mero ' + Geral.FFI(Boleto) + ' n�o pode ser editada!' +
    sLineBreak + 'Motivo: H� pagamento(s) lan�ado(s) nela!')
  then
    Exit;
  //
  if Tipo = 0 then
    Continua := UnProtocolo.LiberaAtualizacaoDeProtocoloBoleto(Dmod.MyDB, Boleto,
                  Entidade, PrevCod, EntCliInt, Config)
  else
    Continua := True;
  //
  if not Continua then
  begin
    if Geral.MB_Pergunta('J� foi gerada uma remessa para o boleto selecionado!' +
      sLineBreak + 'Deseja realmente alter�-lo?' + sLineBreak + sLineBreak +
      'ATEN��O: Antes de alter�-lo certifique-se que o boleto j� foi alterado junto ao banco!') = ID_YES then
    begin
      if not DBCheck.LiberaPelaSenhaBoss then
        Exit;
    end else
      Exit;
  end;
  //
  GetValor := 0;
  //
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, ValorArreIts,
    2, 0, '', '', True, 'Valor', 'Informe o valor: ', 0, GetValor) then
  begin
    if Geral.MB_Pergunta('Confirma a altera��o do valor para ' +
      Geral.TFT(GetValor, 2, siNegativo) + '?')  <> ID_YES
    then
      Exit;
    Valor := GetValor;
    //
    if Controle <> 0 then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'arreits', False,
        ['Valor'], ['Controle'], [Valor], [Controle], True) then
      begin
        if Tipo = 0 then
        begin
          UnProtocolo.AtualizaProtocolo('Valor', Geral.TFT(GetValor, 2, siNegativo),
            PrevCod, EntCliInt, Config, Entidade, Boleto, Dmod.MyDB);
        end;
      end;
    end;
    if Lancto <> 0 then
    begin
      if Valor < 0 then
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabLct, False,
          ['Debito'], ['Controle'], [Valor], [Lancto], True)
      else
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabLct, False,
          ['Credito'], ['Controle'], [Valor], [Lancto], True);
    end;
  end;
end;

function TUnBloquetos.AjustaVencimentoPeriodo(Periodo: Integer;
  Data: TDateTime): TDateTime;
var
  Dia, Mes, Ano: Word;
  Peri, VctoFeriado: Integer;
  NewVcto: TDateTime;
begin
  DecodeDate(Data, Ano, Mes, Dia);
  //
  UMyMod.AbreQuery(DBloGeren.QrBloOpcoes, Dmod.MyDB);
  //
  case DBloGeren.QrBloOpcoes.FieldByName('PerGerBol').AsInteger of
    0: //Anterior
      Peri := Periodo - 1;
    1: //Atual
      Peri := Periodo;
    2: //Pr�ximo
      Peri := Periodo + 1;
    else
      Peri := 0;
  end;
  VctoFeriado := DBloGeren.QrBloOpcoes.FieldByName('VctoFeriado').AsInteger;
  NewVcto     := Geral.PeriodoToDate(Peri, Dia, True);
  //
  if VctoFeriado = 1 then //Pular feriados e dias n�o �teis
    NewVcto := UMyMod.CalculaDataDeposito(NewVcto);
  //
  Result  := NewVcto;
end;

function TUnBloquetos.ArrecadacoesVencidas_e_AVencer(Hoje: TDateTime;
  MostraForm: Boolean): Integer;
var
  Periodo, DataFutura: String;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    DataFutura :=  Geral.FDT(Hoje + 30, 1);
    Periodo := Geral.FF0(Geral.Periodo2000(Hoje + 30));
    //
    if MostraForm = False then
    begin
      {$IfNDef sCNTR}
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT COUNT(*) ITENS',
        'FROM bloarreits its ',
        'LEFT JOIN contratos con ON con.Codigo = its.Contrato ',
        'WHERE its.Ativo = 1 ',
        'AND (',
        '    (   ',
        '         its.SitCobr = 2 ', // Programado
        '         AND ',
        '         its.ParcPerI <= ' + Periodo,
        '         AND ',
        '         its.ParcPerF <= ' + Periodo,
        '    )',
        '    OR',
        '    (',
        '        its.SitCobr = 3',   // Contrato
        '        AND',
        '        (  ',
        '        con.DtaPrxRenw BETWEEN "1900-01-01"  ',
        '        AND "' + DataFutura + '" ',
        '        ) ',
        '    )',
        ')',
        '']);
      {$Else}
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT COUNT(*) ITENS',
        'FROM bloarreits its ',
        'WHERE its.Ativo = 1 ',
        'AND (',
        '    (   ',
        '         its.SitCobr = 2 ', // Programado
        '         AND ',
        '         its.ParcPerI <= ' + Periodo,
        '         AND ',
        '         its.ParcPerF <= ' + Periodo,
        '    )',
        ')',
        '']);
      {$EndIf}
      Result := Qry.FieldByName('ITENS').AsInteger;
    end else
      Result := 1;
    //
    if MostraForm and (Result > 0) then
    begin
      if DBCheck.CriaFm(TFmBloArrItsRnw, FmBloArrItsRnw, afmoNegarComAviso) then
      begin
        FmBloArrItsRnw.FHoje := Hoje;
        FmBloArrItsRnw.ReopenBloArreIts(Hoje);
        FmBloArrItsRnw.ShowModal;
        FmBloArrItsRnw.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnBloquetos.AlteraVencimentoFatura(Tipo: TselType;
  Progress: TProgressBar; Grade: TDBGrid; QueryBloquetos: TmySQLQuery;
  TabLct: String): Boolean;

  procedure MostraMensagemEncerrado(Bloqueto: Double);
  begin
    Geral.MB_Aviso('Altera��o de vencimento cancelada para a fatura n�mero ' +
      Geral.FFI(Bloqueto) + sLineBreak + 'Motivo: Per�odo encerrado!');
  end;
  
  procedure AtualizaArreLct(BoletoNum: Double; Prev, Empresa, Entidade,
    Tipo, Config: Integer; TabLctA: String; OldData, Data: TDateTime);
  var
    Continua: Boolean;
    Controle, Lancto, EntCliInt: Integer;
    SQLCompl: String;
  begin
    if Tipo = 0 then
      Continua := UnProtocolo.LiberaAtualizacaoDeProtocoloBoleto(Dmod.MyDB,
                    BoletoNum, Entidade, Prev, Empresa, Config)
    else
      Continua := True;
    //
    if not Continua then
    begin
      if Geral.MB_Pergunta('J� foi gerada uma remessa para o boleto selecionado!' +
        sLineBreak + 'Deseja realmente alter�-lo?' + sLineBreak + sLineBreak +
        'ATEN��O: Antes de alter�-lo certifique-se que o boleto j� foi alterado junto ao banco!') = ID_YES then
      begin
        if not DBCheck.LiberaPelaSenhaBoss then
          Exit;
      end else
        Exit;
    end;
    //
    if Tipo = 0 then
      SQLCompl := 'AND ari.CNAB_Cfg=' + Geral.FF0(Config)
    else
      SQLCompl := 'AND ari.Fatur_Cfg=' + Geral.FF0(Config);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
      'SELECT ari.Lancto, ari.Controle, ari.Boleto, pre.Empresa ',
      'FROM arreits ari ',
      'LEFT JOIN prev pre ON pre.Codigo = ari.Codigo ',
      'WHERE ari.Boleto=' + Geral.FFI(BoletoNum),
      'AND ari.Entidade=' + Geral.FF0(Entidade),
      SQLCompl,
      'AND ari.Codigo=' + Geral.FF0(Prev),
      'AND ari.Vencto="' + Geral.FDT(OldData, 1) + '"',
      'ORDER BY VALOR DESC ',
      '']);
    DBloGeren.QrLoc.First;
    while not DBloGeren.QrLoc.Eof do
    begin
      Controle  := DBloGeren.QrLoc.FieldByName('Controle').AsInteger;
      Lancto    := DBloGeren.QrLoc.FieldByName('Lancto').AsInteger;
      EntCliInt := DBloGeren.QrLoc.FieldByName('Empresa').AsInteger;
      //
      if MyObjects.FIC(VerificaSeFaturaEstaPaga(TabLct, Lancto), nil,
        'A fatura n�mero ' + Geral.FFI(BoletoNum) + ' n�o pode ser editada!' +
        sLineBreak + 'Motivo: H� pagamento(s) lan�ado(s) nela!')
      then
        Exit;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'arreits', False, ['Vencto'],
        ['Controle'], [Geral.FDT(Data, 1)], [Controle], True) then
      begin
        if Tipo = 0 then
        begin
          UnProtocolo.AtualizaProtocolo('Vencto', Geral.FDT(Data, 1), Prev,
            EntCliInt, Config, Entidade, BoletoNum, Dmod.MyDB);
        end;
        //
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabLctA, False,
          ['Vencimento'], ['Controle'], [Geral.FDT(Data, 1)],
          [Lancto], True)
        then
          Geral.MB_Aviso('Falha ao alterar lan�amento financeiro da fatura ID n�mero: '
            + Geral.FF0(Controle));
      end else
        Geral.MB_Aviso('Falha ao alterar vencimento da fatura ID n�mero: ' +
          Geral.FF0(Controle));
      //
      DBloGeren.QrLoc.Next;
    end;
  end;
var
  i: Integer;
  TipCfg, Config, CNAB_Cfg, Fatur_Cfg: Integer;
  Data: TDateTime;
begin
  Result := True;
  //
  if not DBCheck.ObtemData(QueryBloquetos.FieldByName('Vencto').AsDateTime, Data, 0) then
    Exit;
  //
  if Geral.MB_Pergunta('Confirma a altera��o do vencimento para a data '
    + Geral.FDT(Data, 2) + '?') = ID_YES then
  begin
    try
      Screen.Cursor     := crHourGlass;
      Progress.Position := 0;
      //
      case Tipo of
        istSelecionados:
        begin
          if Grade.SelectedRows.Count > 1 then
          begin
            try
              Grade.Enabled := False;
              Progress.Max  := Grade.SelectedRows.Count;
              //
              with Grade.DataSource.DataSet do
              begin
                for i:= 0 to Grade.SelectedRows.Count-1 do
                begin
                  GotoBookmark(Grade.SelectedRows.Items[i]);
                  //
                  Progress.Position := Progress.Position + 1;
                  Progress.Update;
                  Application.ProcessMessages;
                  //
                  if not VerificaSePeriodoEstaEncerrado(
                    QueryBloquetos.FieldByName('Codigo').AsInteger) then
                  begin
                    CNAB_Cfg  := QueryBloquetos.FieldByName('CNAB_Cfg').AsInteger;
                    Fatur_Cfg := QueryBloquetos.FieldByName('Fatur_Cfg').AsInteger;
                    //
                    if CNAB_Cfg <> 0 then
                    begin
                      TipCfg := 0;
                      Config := CNAB_Cfg;
                    end else
                    begin
                      TipCfg := 1;
                      Config := Fatur_Cfg;
                    end;
                    //
                    AtualizaArreLct(QueryBloquetos.FieldByName('Boleto').AsFloat,
                      QueryBloquetos.FieldByName('Codigo').AsInteger,
                      QueryBloquetos.FieldByName('Empresa').AsInteger,
                      QueryBloquetos.FieldByName('Entidade').AsInteger, TipCfg,
                      Config, TabLct, QueryBloquetos.FieldByName('Vencto').AsDateTime,
                      Data);
                  end else
                    MostraMensagemEncerrado(
                      QueryBloquetos.FieldByName('Boleto').AsFloat);
                end;
              end;
            finally
              Grade.Enabled := True;
            end;
          end else
          begin
            if not VerificaSePeriodoEstaEncerrado(
              QueryBloquetos.FieldByName('Codigo').AsInteger) then
            begin
              CNAB_Cfg  := QueryBloquetos.FieldByName('CNAB_Cfg').AsInteger;
              Fatur_Cfg := QueryBloquetos.FieldByName('Fatur_Cfg').AsInteger;
              //
              if CNAB_Cfg <> 0 then
              begin
                TipCfg := 0;
                Config := CNAB_Cfg;
              end else
              begin
                TipCfg := 1;
                Config := Fatur_Cfg;
              end;
              //
              AtualizaArreLct(QueryBloquetos.FieldByName('Boleto').AsFloat,
                QueryBloquetos.FieldByName('Codigo').AsInteger,
                QueryBloquetos.FieldByName('Empresa').AsInteger,
                QueryBloquetos.FieldByName('Entidade').AsInteger, TipCfg,
                Config, TabLct, QueryBloquetos.FieldByName('Vencto').AsDateTime,
                Data);
            end else
              MostraMensagemEncerrado(QueryBloquetos.FieldByName('Boleto').AsFloat);
          end;
        end;
        istTodos:
        begin
          Progress.Max := QueryBloquetos.RecordCount;
          try
            Grade.Enabled := False;
            //
            QueryBloquetos.DisableControls;
            QueryBloquetos.First;
            //
            while not QueryBloquetos.Eof do
            begin
              Progress.Position := Progress.Position + 1;
              Progress.Update;
              Application.ProcessMessages;
              //
              if not VerificaSePeriodoEstaEncerrado(
                QueryBloquetos.FieldByName('Codigo').AsInteger) then
              begin
                CNAB_Cfg  := QueryBloquetos.FieldByName('CNAB_Cfg').AsInteger;
                Fatur_Cfg := QueryBloquetos.FieldByName('Fatur_Cfg').AsInteger;
                //
                if CNAB_Cfg <> 0 then
                begin
                  TipCfg := 0;
                  Config := CNAB_Cfg;
                end else
                begin
                  TipCfg := 1;
                  Config := Fatur_Cfg;
                end;
                //
                AtualizaArreLct(QueryBloquetos.FieldByName('Boleto').AsFloat,
                  QueryBloquetos.FieldByName('Codigo').AsInteger,
                  QueryBloquetos.FieldByName('Empresa').AsInteger,
                  QueryBloquetos.FieldByName('Entidade').AsInteger, TipCfg,
                  Config, TabLct, QueryBloquetos.FieldByName('Vencto').AsDateTime,
                  Data);
              end else
                MostraMensagemEncerrado(QueryBloquetos.FieldByName('Boleto').AsFloat);
              //                 
              QueryBloquetos.Next;
            end;
          finally
            Grade.Enabled := True;
            //
            QueryBloquetos.EnableControls;
          end;
        end;
      end;
    finally
      Progress.Position := 0;
      Screen.Cursor     := crDefault;
      Result            := True;
    end;
  end;
end;

function TUnBloquetos.AlteraVencimentoPreFatura(Tipo: TselType;
  Progress: TProgressBar; Grade: TDBGrid; QueryBloquetos: TmySQLQuery): Boolean;
var
  Itens, itxt, txt, MesTxt, NewVcto: String;
  i, MesValido: Integer;
  ValidouMes: Boolean;
  Data: TDateTime;
begin
  Result := False;
  //
  if not DBCheck.ObtemData(QueryBloquetos.FieldByName('Vencto').AsDateTime, Data, 0) then
    Exit;
  //
  Screen.Cursor     := crHourGlass;
  Progress.Position := 0;
  //
  Itens   := '';
  Itxt    := '';
  NewVcto := '';
  //
  case Tipo of
    istSelecionados:
    begin
      Progress.Max := Grade.SelectedRows.Count;
      //
      if Grade.SelectedRows.Count > 1 then
      begin
        try
          Grade.Enabled := False;
          //
          QueryBloquetos.DisableControls;
          //
          with Grade.DataSource.DataSet do
          begin
            for i:= 0 to Grade.SelectedRows.Count-1 do
            begin
              GotoBookmark(Grade.SelectedRows.Items[i]);
              //
              if Itens = '' then
                Itens := Geral.FF0(QueryBloquetos.FieldByName('Entidade').AsInteger)
              else
                Itens := Itens + ',' + Geral.FF0(QueryBloquetos.FieldByName('Entidade').AsInteger);
              //
              if itxt = '' then
                itxt := QueryBloquetos.FieldByName('NOMEENT').AsString
              else
                itxt := itxt + ',' + QueryBloquetos.FieldByName('NOMEENT').AsString;
              //
              Progress.Position := Progress.Position + 1;
              Progress.Update;
              Application.ProcessMessages;
            end;
            Txt := 'das entidades '+itxt;
          end;
        finally
          Grade.Enabled := True;
          //
          QueryBloquetos.EnableControls;
        end;
      end else
      begin
        Itens := Geral.FF0(QueryBloquetos.FieldByName('Entidade').AsInteger);
        Txt   := 'da entidade ' + QueryBloquetos.FieldByName('NOMEENT').AsString;
      end;
    end;
    istTodos:
    begin
      Progress.Max := Grade.SelectedRows.Count;
      //
      try
        Grade.Enabled := False;
        //
        QueryBloquetos.DisableControls;
        QueryBloquetos.First;
        //
        while not QueryBloquetos.Eof do
        begin
          if Itens = '' then
            Itens := Geral.FF0(QueryBloquetos.FieldByName('Entidade').AsInteger)
          else
            Itens := Itens + ',' + Geral.FF0(QueryBloquetos.FieldByName('Entidade').AsInteger);
          if itxt = '' then
            Itxt := QueryBloquetos.FieldByName('NOMEENT').AsString
          else
            Itxt := Itxt + ',' + QueryBloquetos.FieldByName('NOMEENT').AsString;
          //
          Progress.Position := Progress.Position + 1;
          //
          QueryBloquetos.Next;
        end;
      finally
        Grade.Enabled := True;
        //
        QueryBloquetos.EnableControls;
      end;
      Txt := 'de todas entidades deste per�odo';
    end;
  end;
  if Itens = '' then
    Geral.MB_Aviso('Nenhum item foi encontrado!')
  else if Geral.MB_Pergunta('Confirma a altera��o do vencimento ' + Txt +
    ' para a data ' + Geral.FDT(Data, 2) + '?') = ID_YES then
  begin
    ValidouMes := UBloqGerl.ValidaVencimentoPeriodo(Dmod.MyDB, DBloGeren.QrBloOpcoes,
                  QueryBloquetos.FieldByName('Periodo').AsInteger, Data, MesValido);
    MesTxt     := dmkPF.VerificaMes(MesValido, False);
    //
    if MyObjects.FIC(ValidouMes, nil, 'O vencimento deve ser correspondente ao m�s ' + MesTxt + '!') then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    NewVcto := Geral.FDT(Data, 1);
    // 
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE arreits SET Vencto="' + NewVcto + '"',
      'WHERE Boleto=0 AND Entidade in (' + Itens + ') ',
      'AND Codigo=' + Geral.FF0(QueryBloquetos.FieldByName('Codigo').AsInteger),
      '']);
  end;
  Progress.Position := 0;
  Screen.Cursor     := crDefault;
  Result            := True;
end;

function TUnBloquetos.AtivaDesativaArreIts(Controle, Ativo: Integer;
  MostraPergunta: Boolean = True): Boolean;
var
  Continua: Boolean;
  Txt: String;
begin
  Result := False;
  //
  if Ativo = 1 then
  begin
    Txt   := 'desativar';
    Ativo := 0;
  end else
  begin
    Txt   := 'ativar';
    Ativo := 1;
  end;
  Continua := True;
  //
  if MostraPergunta = True then
  begin
    if Geral.MB_Pergunta('Deseja ' + Txt +
      ' o item de arrecada��o selecionado?') <> ID_YES
    then
      Continua := False;
  end;
  if Continua = True then
  begin
    if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE bloarreits SET Ativo=' + Geral.FF0(Ativo),
      'WHERE Controle=' + Geral.FF0(Controle),
      ''])
    then
      Result := True;
  end;
end;

function TUnBloquetos.AtualizaArrecadacao(SQLType: TSQLType; Codigo,
  ControleArre, Conta, BloArre, BloArreIts, Entidade, CartEmiss, Tipo, Config,
  NFSeSrvCad, Lancto, Avulso, ComoAdd, FatID, FatParcela: Integer; Vencto: String;
  Valor, FatNum, Boleto: Double; Texto: String; var Mensagem: String): Integer;

  procedure ObtemCamposPrev(Codigo: Integer; var Aviso01, Aviso02, Aviso03,
    Aviso04, Aviso05, Aviso06, Aviso07, Aviso08, Aviso09, Aviso10,
    AvisoVerso: String; var ModelBloq, BloqFV, Compe: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
      'SELECT Aviso01, Aviso02, Aviso03, Aviso04, ',
      'Aviso05, Aviso06, Aviso07, Aviso08, Aviso09, ',
      'Aviso10, AvisoVerso, ModelBloq, BloqFV, Compe ',
      'FROM prev ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    Aviso01    := DBloGeren.QrLoc.FieldByName('Aviso01').AsString;
    Aviso02    := DBloGeren.QrLoc.FieldByName('Aviso02').AsString;
    Aviso03    := DBloGeren.QrLoc.FieldByName('Aviso03').AsString;
    Aviso04    := DBloGeren.QrLoc.FieldByName('Aviso04').AsString;
    Aviso05    := DBloGeren.QrLoc.FieldByName('Aviso05').AsString;
    Aviso06    := DBloGeren.QrLoc.FieldByName('Aviso06').AsString;
    Aviso07    := DBloGeren.QrLoc.FieldByName('Aviso07').AsString;
    Aviso08    := DBloGeren.QrLoc.FieldByName('Aviso08').AsString;
    Aviso09    := DBloGeren.QrLoc.FieldByName('Aviso09').AsString;
    Aviso10    := DBloGeren.QrLoc.FieldByName('Aviso10').AsString;
    AvisoVerso := DBloGeren.QrLoc.FieldByName('AvisoVerso').AsString;
    ModelBloq  := DBloGeren.QrLoc.FieldByName('ModelBloq').AsInteger;
    BloqFV     := DBloGeren.QrLoc.FieldByName('BloqFV').AsInteger;
    Compe      := DBloGeren.QrLoc.FieldByName('Compe').AsInteger;
  end;
var
  Controle, ModelBloq, BloqFV, Compe, CNAB_Cfg, Fatur_Cfg: Integer;
  Aviso01, Aviso02, Aviso03, Aviso04, Aviso05, Aviso06, Aviso07, Aviso08,
  Aviso09, Aviso10, AvisoVerso: String;
begin
  Result   := 0;
  Mensagem := '';
  //
  if Codigo = 0 then
  begin
    Mensagem := 'ID do per�odo n�o definido!';
    Exit;
  end;
  if Conta = 0 then
  begin
    Mensagem := 'Conta do plano de contas n�o definida!';
    Exit;
  end;
  if Entidade = 0 then
  begin
    Mensagem := 'Entidade n�o definida!';
    Exit;
  end;
  if CartEmiss = 0 then
  begin
    Mensagem := 'Carteira de emiss�o de fatura n�o definida!';
    Exit;
  end;
  if Config = 0 then
  begin
    Mensagem := 'Configura��o para gera��o de fatura n�o definida!';
    Exit;
  end;
  if Texto = '' then
  begin
    Mensagem := 'Texto da arrecada��o n�o definido!';
    Exit;
  end;
  if SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'arreits', 'arreits', 'Controle')
  end else
  begin
    if ControleArre = 0 then
    begin
      Mensagem := 'ID da arrecada��o n�o definida para o tipo altera��o!';
      Exit;
    end;
    Controle := ControleArre;
  end;
  if Mensagem = '' then
  begin
    ObtemCamposPrev(Codigo, Aviso01, Aviso02, Aviso03, Aviso04, Aviso05,
      Aviso06, Aviso07, Aviso08, Aviso09, Aviso10, AvisoVerso, ModelBloq,
      BloqFV, Compe);
    //
    if Tipo = 0 then
    begin
      CNAB_Cfg  := Config;
      Fatur_Cfg := 0;
    end else
    begin
      CNAB_Cfg  := 0;
      Fatur_Cfg := Config;
    end;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'arreits', False,
    [
      'Conta', 'BloArre', 'BloArreIts', 'Entidade', 'Valor', 'Texto',
      'CartEmiss', 'Boleto', 'CNAB_Cfg', 'Fatur_Cfg', 'NFSeSrvCad', 'Lancto',
      'Vencto', 'ComoAdd', 'Avulso', 'FatID', 'FatParcela', 'FatNum', 'Aviso01',
      'Aviso02', 'Aviso03', 'Aviso04', 'Aviso05', 'Aviso06', 'Aviso07',
      'Aviso08', 'Aviso09', 'Aviso10', 'AvisoVerso', 'ModelBloq', 'BloqFV',
      'Compe', 'Codigo'], ['Controle'],
    [
      Conta, BloArre, BloArreIts, Entidade, Valor, Texto,
      CartEmiss, Boleto, CNAB_Cfg, Fatur_Cfg, NFSeSrvCad, Lancto,
      Vencto, ComoAdd, Avulso, FatID, FatParcela, FatNum, Aviso01,
      Aviso02, Aviso03, Aviso04, Aviso05, Aviso06, Aviso07,
      Aviso08, Aviso09, Aviso10, AvisoVerso, ModelBloq, BloqFV,
      Compe, Codigo], [Controle], True)
    then
      Result := Controle;
  end;
end;

function TUnBloquetos.AtualizaBloGerenNew(SQLType: TSQLType; CodigoPrev,
  Encerrado, Empresa, Periodo, ModelBloq, Compe, BloqFV: Integer; Aviso01,
  Aviso02, Aviso03, Aviso04, Aviso05, Aviso06, Aviso07, Aviso08, Aviso09,
  Aviso10, AvisoVerso: String; var Mensagem: String): Integer;
var
  Codigo: Integer;
begin
  Result   := 0;
  Mensagem := '';
  //
  if SQLType = stIns then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
      'SELECT Periodo ',
      'FROM prev ',
      'WHERE Empresa=' + Geral.FF0(Empresa),
      'AND Periodo=' + Geral.FF0(Periodo),
      '']);
    if DBloGeren.QrLoc.FieldByName('Periodo').AsInteger = Periodo then
    begin
      Mensagem := 'Inclus�o de novo or�amento abortado!' + sLineBreak +
        'J� existe or�amento para esta empresa neste per�odo!';
      Exit;
    end;
  end;
  //
  if SQLType = stIns then 
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Prev', 'Prev', 'Codigo')
  else
    Codigo := CodigoPrev;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'prev', False,
  [
    'Encerrado', 'Empresa', 'Periodo', 'Aviso01', 'Aviso02', 'Aviso03',
    'Aviso04', 'Aviso05', 'Aviso06', 'Aviso07', 'Aviso08', 'Aviso09',
    'Aviso10', 'AvisoVerso', 'ModelBloq', 'Compe', 'BloqFV'
  ], ['Codigo'],
  [
    0, Empresa, Periodo, Aviso01, Aviso02, Aviso03,
    Aviso04, Aviso05, Aviso06, Aviso07, Aviso08, Aviso09,
    Aviso10, AvisoVerso, ModelBloq, Compe, BloqFV
  ], [Codigo], False)
  then
    Result := Codigo;
end;

function TUnBloquetos.AtualizaLayoutAvisosBoleto(Aviso01, Aviso02, Aviso03,
  Aviso04, Aviso05, Aviso06, Aviso07, Aviso08, Aviso09, Aviso10,
  AvisoVerso: String; ModelBloq, BloqFV, Compe, Codigo,
  Controle: Integer): Boolean;
begin
  Result := False;
  if Codigo <> 0 then
  begin
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE arreits SET ',
      'Aviso01="' + Aviso01 + '", Aviso02="' + Aviso02 + '", ',
      'Aviso03="' + Aviso03 + '", Aviso04="' + Aviso04 + '", ',
      'Aviso05="' + Aviso05 + '", Aviso06="' + Aviso06 + '", ',
      'Aviso07="' + Aviso07 + '", Aviso08="' + Aviso08 + '", ',
      'Aviso09="' + Aviso09 + '", Aviso10="' + Aviso10 + '", ',
      'AvisoVerso="' + AvisoVerso + '", ModelBloq=' + Geral.FF0(ModelBloq) + ', ',
      'BloqFV=' + Geral.FF0(BloqFV) + ', Compe=' + Geral.FF0(Compe),
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Fatur_Cfg=0 ',
      '']);
  end else
  if Controle <> 0 then
  begin
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE arreits SET ',
      'Aviso01="' + Aviso01 + '", Aviso02="' + Aviso02 + '", ',
      'Aviso03="' + Aviso03 + '", Aviso04="' + Aviso04 + '", ',
      'Aviso05="' + Aviso05 + '", Aviso06="' + Aviso06 + '", ',
      'Aviso07="' + Aviso07 + '", Aviso08="' + Aviso08 + '", ',
      'Aviso09="' + Aviso09 + '", Aviso10="' + Aviso10 + '", ',
      'AvisoVerso="' + AvisoVerso + '", ModelBloq=' + Geral.FF0(ModelBloq) + ', ',
      'BloqFV=' + Geral.FF0(BloqFV) + ', Compe=' + Geral.FF0(Compe),
      'WHERE Controle=' + Geral.FF0(Controle),
      'AND Fatur_Cfg=0 ',
      '']);
  end;
end;

function TUnBloquetos.ValidaPeriodo(Periodo: Integer; Inverso: Boolean = False): Integer;
var
  PerGerBol: Integer;
begin
  Result := 0;
  UMyMod.AbreQuery(DBloGeren.QrBloOpcoes, Dmod.MyDB);
  //
  if DBloGeren.QrBloOpcoes.RecordCount > 0 then
  begin
    PerGerBol := DBloGeren.QrBloOpcoes.FieldByName('PerGerBol').AsInteger;
    //
    if not Inverso then
    begin
      case PerGerBol of
        0: Result := Periodo - 1;
        1: Result := Periodo;
        2: Result := Periodo + 1;
      end;
    end else
    begin
      case PerGerBol of
        0: Result := Periodo + 1;
        1: Result := Periodo;
        2: Result := Periodo - 1;
      end;
    end;
  end;
end;

procedure TUnBloquetos.BloquetosParcelasByFatNum(Empresa, FatID: Integer;
  FatNumLct, FatNumBlo: Double; TabLctA: String;
  EmiteLotProtocoloSeComRegistro: Boolean; Progress: TProgressBar);
var
  TotCfgs, Tipo, Config, CodConfig, LoteCR: Integer;
  QrFat, QrPar: TmySQLQuery;
begin
  if not Grl_Geral.LiberaModulo(CO_DMKID_APP, Grl_Geral.ObtemSiglaModulo(mdlappBloquetos),
           DModG.QrMaster.FieldByName('HabilModulos').AsString) then
    Exit;
  //
  QrFat := TmySQLQuery.Create(Dmod);
  QrPar := TmySQLQuery.Create(Dmod);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFat, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabLctA,
    'WHERE CliInt=' + Geral.FF0(Empresa),
    'AND FatID=' + Geral.FF0(FatID),
    'AND FatNum=' + Geral.FFI(FatNumLct),
    '']);
  //
  if QrFat.RecordCount = 0 then
  begin
    Geral.MB_Erro('ERRO AO GERAR BOLETO!' + sLineBreak +
      'N�o foi poss�vel localizar as parcelas do faturamento n� ' +
      Geral.FFI(FatNumLct) + sLineBreak + 'FatID = ' + Geral.FF0(FatID)
      + ' da empresa ' + Geral.FF0(Empresa));
    //
    Exit;
  end;
  Config  := 0;
  TotCfgs := VerificaFatur_Cfg(QrFat.FieldByName('Carteira').AsInteger, Tipo, CodConfig);
  //
  if (TotCfgs = 1) and (CodConfig <> 0) and (Tipo >= 0) then
  begin
    Config := CodConfig;
  end else
  begin
    if DBCheck.CriaFm(TFmBloAvulso, FmBloAvulso, afmoNegarComAviso) then
    begin
      FmBloAvulso.FCarteira := QrFat.FieldByName('Carteira').AsInteger;
      FmBloAvulso.ShowModal;
      //
      Tipo      := FmBloAvulso.FTipo;
      CodConfig := FmBloAvulso.FConfig;
      //
      FmBloAvulso.Destroy;
      //
      if CodConfig <> 0 then
        Config := CodConfig;
    end;
  end;
  if (CodConfig = 0) or (Tipo < 0) then
  begin
    Geral.MB_Aviso('Emiss�o de fatura abortada!' + sLineBreak +
      'Motivo: Configura��o n�o definida!');
    Exit;
  end;
  //
  if Progress <> nil then
  begin
    Progress.Position := 0;
    Progress.Max      := QrFat.RecordCount;
    Progress.Visible  := True;
  end;
  QrFat.First;
  while not QrFat.Eof do
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPar, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabLctA,
    'WHERE Data="' + Geral.FDT(QrFat.FieldByName('Data').AsDateTime, 1) + '"',
    'AND Controle=' + Geral.FF0(QrFat.FieldByName('Controle').AsInteger),
    'AND Tipo=' + Geral.FF0(QrFat.FieldByName('Tipo').AsInteger),
    'AND Carteira=' + Geral.FF0(QrFat.FieldByName('Carteira').AsInteger),
    'AND Sub=' + Geral.FF0(QrFat.FieldByName('Sub').AsInteger),
    'ORDER BY FatParcela',
    '']);
    //
    if QrPar.RecordCount = 0 then
    begin
      Geral.MB_Erro('ERRO AO GERAR BOLETO!' + sLineBreak +
      'N�o foi poss�vel localizar a parcela ' +
      Geral.FF0(QrFat.FieldByName('FatParcela').AsInteger) +
      ' do faturamento n� ' +
      Geral.FFI(FatNumLct) + sLineBreak + 'FatID = ' + Geral.FF0(FatID)
      + ' da empresa ' + Geral.FF0(Empresa));
      //
      Exit;
    end else
    begin
      if not EmiteBoletoAvulso(TabLctA, nil, QrPar,
        EmiteLotProtocoloSeComRegistro, FatNumBlo, Tipo, Config, LoteCR)
      then
        Exit;
    end;
    //
    if Progress <> nil then
    begin
      Progress.Position := Progress.Position + 1;
      Progress.Update;
      Application.ProcessMessages;
    end;
    //
    QrFat.Next;
  end;
  Geral.MB_Aviso('Boletos gerados com sucesso!');
  //
  {$IfNDef SemProtocolo}
    if (EmiteLotProtocoloSeComRegistro) and (LoteCR <> 0) then
    begin
      if Geral.MB_Pergunta(
      'Deseja localizar o lote para enviar o arquivo de remessa ao banco?') = ID_YES
      then
        ProtocoUnit.MostraFormProtocolos(LoteCR, 0);
    end;
  {$Else}
    dmkPF.InfoSemModulo(mdlappProtocolos);
  {$EndIf}
  //
  if QrFat <> nil then
    QrFat.Free;
  if QrPar <> nil then
    QrPar.Free;
  //
  if Progress <> nil then
    Progress.Visible := False;
end;

{$IFDEF DEFINE_VARLCT}
procedure TUnBloquetos.CadastroBloGeren(Aba: Integer; InOwner: TWincontrol;
  Pager: TWinControl);
var
  Abertura: Integer;
begin
  if not VerificaSeOpcoesForamConfiguradas then
  begin
    CadastroDeBloOpcoes();
    Exit;
  end;
  if Aba in [0, 1] then
    Abertura := Aba
  else
    Abertura := DBloGeren.QrBloOpcoes.FieldByName('AbreEmTab').AsInteger;
  //
  if Abertura = 2 then //Escolhe
  begin
    Abertura := MyObjects.SelRadioGroup('Selecione o modelo', '', ['Janela', 'Aba'], 2);
  end;
  //
  case Abertura of
    0: //Janela
    begin
      if DBCheck.CriaFm(TFmBloGeren, FmBloGeren, afmoNegarComAviso) then
      begin
        FmBloGeren.ShowModal;
        FmBloGeren.Destroy;
        //
        FmBloGeren := nil;
      end;
    end;
    1: //Aba
    begin
      if FmBloGeren = nil then
        MyObjects.FormTDICria(TFmBloGeren, InOwner, Pager, True, True);
    end;
  end;
end;
{$ELSE}
procedure TUnBloquetos.CadastroBloGeren();
begin
//  if not DmodG.SelecionarCliInt(CliInt, True) then
//    Exit;
  //
  if DBCheck.CriaFm(TFmBloGeren, FmBloGeren, afmoNegarComAviso) then
  begin
    FmBloGeren.ShowModal;
    FmBloGeren.Destroy;
  end;
end;
{$ENDIF}

function TUnBloquetos.CadastroDeBloOpcoes(): Boolean;
begin
  Result := False;
  //
  if DBCheck.CriaFm(TFmBloOpcoes, FmBloOpcoes, afmoNegarComAviso) then
  begin
    FmBloOpcoes.ShowModal;
    //
    Result := FmBloOpcoes.FConfirmou;
    //
    FmBloOpcoes.Destroy;
  end;
end;

function TUnBloquetos.CalculaPeriodoIniFimArrecadacao(SitCobr, ParcPerI,
  ParcPerF, Parc: Integer; DiaVencto: Double; DVencimento, DtaPrxRenw: TDateTime;
  var PeriodoIni, PeriodoFim, ExpiraEm: String; var Parcelas: Integer): Boolean;
var
  Hoje, Data: TDateTime;
  PerI, PerF: Integer;
  Expirou: Boolean;
begin
  Hoje := DModG.ObtemAgora;
  //
  case SitCobr of
    1:  //Cont�nua (anual)
    begin
      PeriodoIni := dmkPF.VerificaMes(ParcPerI, False);
      PeriodoFim := dmkPF.VerificaMes(ParcPerF, False);
      ExpiraEm   := 'Cont�nua (Anual) n�o expira';
      Expirou    := False;
      Parcelas   := Parc;
    end;
    2: //Programada
    begin
      PeriodoIni := dmkPF.MesEAnoDoPeriodo(ParcPerI);
      PeriodoFim := dmkPF.MesEAnoDoPeriodo(ParcPerF);
      Data       := Geral.PeriodoToDate(ParcPerF, Trunc(DiaVencto), True);
      Data       := Data - Hoje;
      Data       := Floor(Data);
      Parcelas   := Parc;
      //
      if Data > 0 then
      begin
        ExpiraEm := 'Expira em ' + FloatToStr(Data) + ' dias';
        Expirou  := False;
      end else
      begin
        Data     := Data * -1;
        ExpiraEm := 'Expirou fazem ' + FloatToStr(Data) + ' dias';
        Expirou  := True;
      end;
    end;
    3: //Contratos
    begin
      PerI       := Geral.Periodo2000(DVencimento);
      PerF       := Geral.Periodo2000(DtaPrxRenw);
      PeriodoIni := dmkPF.MesEAnoDoPeriodo(PerI);
      PeriodoFim := dmkPF.MesEAnoDoPeriodo(PerF);
      Parcelas   := (PerF - PerI) + 1;
      //
      if DtaPrxRenw > 2 then
      begin
        Data := DtaPrxRenw - Hoje;
        Data := Floor(Data);
        //
        if Data > 0 then
        begin
          ExpiraEm := 'Expira em ' + FloatToStr(Data) + ' dias';
          Expirou  := False;
        end else
        begin
          Data     := Data * -1;
          ExpiraEm := 'Expirou fazem ' + FloatToStr(Data) + ' dias';
          Expirou  := True;
        end;
      end else
      begin
        ExpiraEm := 'Cont�nua (Anual) n�o expira';
        Expirou  := False;
      end;
    end else
    begin
      PeriodoIni := '';
      PeriodoFim := '';
      ExpiraEm   := 'Cont�nua (Anual) n�o expira';
      Expirou    := False;
      Parcelas   := Parc;
    end;
  end;
  Result := Expirou;
end;

procedure TUnBloquetos.MostraMensagemPeriodoEncerrado(Bloqueto: Double);
begin
  Geral.MB_Aviso('N�o foi poss�vel desfazer a fatura n�mero'
    + Geral.FFI(Bloqueto) + sLineBreak + 'Motivo: Per�odo encerrado!');
end;

function TUnBloquetos.DesfazerBoletosFatID(QueryLoc: TmySQLQuery;
  Database: TmySQLDatabase; FatID: Integer; FatNum: Double;
  TabLctA: String): Boolean;
var
  Periodo, Codigo, Controle, EntCliInt, Entidade, Lancto, CNAB_Cfg, Fatur_Cfg,
  Tipo, Config, Avulso: Integer;
  Boleto: Double;
  MostrouMsg: Boolean;
begin
  Result     := False;
  MostrouMsg := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
    'SELECT its.Controle, its.Codigo, its.Boleto, pre.Periodo, its.Lancto, ',
    'its.CNAB_Cfg, pre.Empresa, its.Entidade, its.Avulso ',
    'FROM arreits its ',
    'LEFT JOIN prev pre ON pre.Codigo = its.Codigo ',
    'WHERE its.FatID=' + Geral.FF0(FatID),
    'AND its.FatNum=' + FloatToStr(FatNum),
    '']);
  if QueryLoc.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('Deseja desfazer os boletos gerados?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        QueryLoc.First;
        while not QueryLoc.Eof do
        begin
          Codigo       := QueryLoc.FieldByName('Codigo').AsInteger;
          EntCliInt    := QueryLoc.FieldByName('Empresa').AsInteger;
          Boleto       := QueryLoc.FieldByName('Boleto').AsFloat;
          Controle     := QueryLoc.FieldByName('Controle').AsInteger;
          Periodo      := QueryLoc.FieldByName('Periodo').AsInteger;
          Entidade     := QueryLoc.FieldByName('Entidade').AsInteger;
          Lancto       := QueryLoc.FieldByName('Lancto').AsInteger;
          CNAB_Cfg     := QueryLoc.FieldByName('CNAB_Cfg').AsInteger;
          Fatur_Cfg    := QueryLoc.FieldByName('Fatur_Cfg').AsInteger;
          Avulso       := QueryLoc.FieldByName('Avulso').AsInteger;
          //
          if CNAB_Cfg <> 0 then
          begin
            Tipo   := 0;
            Config := CNAB_Cfg;
          end else
          begin
            Tipo   := 0;
            Config := Fatur_Cfg;
          end;
          //
          if not VerificaSePeriodoEstaEncerrado(Codigo) then
          begin
            if not DesfazerFaturaAtual(Codigo, EntCliInt, Controle, Periodo,
              Lancto, Tipo, Config, Entidade, Avulso, Boleto, TabLctA, False,
              istVisPesquisa, MostrouMsg) then
            begin
              Geral.MB_Aviso('N�o foi poss�vel desfazer o boleto n�mero ' + Geral.FFI(Boleto));
              Result := False;
              Exit;
            end else
              Result := True;
          end else
          begin
            MostraMensagemPeriodoEncerrado(Boleto);
            Exit;
          end;
          //
          QueryLoc.Next;
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end else
    Result := True;
end;

function TUnBloquetos.DesfazerFaturaAtual(Codigo, EntCliInt, Controle, Periodo,
  Lancto, Tipo, Config, Entidade, Avulso: Integer; Boleto: Double; TabLct: String;
  ImpedeSePosterior: Boolean; Modo: TModoVisualizacaoBloquetos;
  var MostrouMsg: Boolean): Boolean;
var
  Libera: Boolean;
begin
  Result := False;
  //
  if ImpedeSePosterior then
  begin
    Libera := VerificaAntesDeCancelar(Periodo, Tipo, Config, Boleto);
    if not Libera then
      Geral.MB_Aviso('A fatura n�mero ' + Geral.FFI(Boleto) + ' n�o pode ser exclu�da!' +
        sLineBreak + 'Motivo: J� foram geradas faturas em per�odos posteriores!');
  end else
    Libera := True;
  //
  if Libera then
  begin
    if MyObjects.FIC(VerificaSeFaturaEstaPaga(TabLct, Lancto), nil,
      'A fatura n�mero ' + Geral.FFI(Boleto) + ' n�o pode ser exclu�da!' +
      sLineBreak + 'Motivo: H� pagamento(s) lan�ado(s) nela!')
    then
      Exit;
    if Tipo = 0 then //Boleto
    begin
      if MyObjects.FIC(UnProtocolo.VerificaSeLoteCRFoiGerado(Dmod.MyDB, Codigo,
        EntCliInt, Config, Entidade, Boleto), nil,
        'A boleto n�mero ' + Geral.FFI(Boleto) + ' n�o pode ser exclu�do!' +
        sLineBreak + 'Motivo: Foi gerado um arquivo remessa para ele!')
      then
        Exit;
    end;
    if Tipo = 0 then
    begin
      if MyObjects.FIC(UnProtocolo.VerificaSeProtocoloRetornou(Dmod.MyDB, Codigo,
        EntCliInt, Config, Entidade, Boleto), nil, 'O boleto n�mero ' + Geral.FFI(Boleto) +
        ' n�o pode ser exclu�do!' + sLineBreak + 'Motivo: O protocolo foi marcado com data de retorno!')
      then
        Exit;
    end;
    //
    if Modo = istVisPesquisa then
    begin
{$IfNDef SemProtocolo}
      UnProtocolo.ExcluiLoteProtocoloBoleto(Dmod.MyDB, 0, Codigo, EntCliInt,
        Controle, 'arreits');
{$EndIf}
      UMyMod.ExcluiRegistroInt1('', 'arreits', 'Controle', Controle, Dmod.MyDB);
    end else
    begin
{$IfNDef SemProtocolo}
      UnProtocolo.ExcluiLoteProtocoloBoleto(Dmod.MyDB, 0, Codigo, EntCliInt,
        Controle, 'arreits');
{$EndIf}
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE arreits SET Boleto=0, Lancto=0 WHERE Controle=' + Geral.FF0(Controle),
        '']);
    end;
    if Avulso = 0 then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'DELETE FROM '+ TabLct +' WHERE Controle=' + Geral.FF0(Lancto),
        '']);
    end else
    begin
      if not MostrouMsg then
      begin
        Geral.MB_Aviso('ATEN��O: Os lan�amentos referentes a faturas avulsas ' +
          sLineBreak + 'devem ser exclu�dos manualmente ou atrav�s de sua janela de origem!');
        MostrouMsg := True;
      end;
    end;
    //
    Result := True;
  end;
end;

function TUnBloquetos.EmiteBoletoAvulso(TabLctA: String; Grade: TdmkDBGrid;
  QrLcts: TmySQLQuery; EmiteLotProtocoloSeComRegistro: Boolean; FatNum: Double;
  Tipo, Config: Integer; var LoteProtoCR: Integer): Boolean;

  function ValidaDadosBoleto(Controle, FatID, Terceiro: Integer; Credito,
    Debito: Double; Vencto: TDateTime): Boolean;
  begin
    Result := False;
    //
    if MyObjects.FIC(Controle = 0, nil, 'Nenhum item foi selecionado!') then
      Exit;
    if MyObjects.FIC(VerificaSeBoletoExiste(Controle), nil,
      'J� foi emitido uma fatura para o lan�amento ID n�mero ' +
      Geral.FF0(Controle) + '!')
    then
      Exit;
    if MyObjects.FIC((Credito - Debito) <= 0, nil,
      'A soma dos valores dos lan�amentos selecionados deve ser maior que 0 (Zero)!')
    then
      Exit;
    if MyObjects.FIC(VerificaSeFaturaEstaPaga(TabLctA, Controle), nil,
      'O lan�amento selecionado n�o deve estar quitado!')
    then
      Exit;
    if MyObjects.FIC(VerificaSeBoletoExiste(QrLcts.FieldByName('Controle').AsInteger),
      nil, 'Este lan�amento j� possui uma fatura gerada!')
    then
      Exit;
    if MyObjects.FIC(FatID = FArre_FatID, nil, 'Voc� deve selecionar apenas lan�amentos que n�o sejam faturas!') then Exit;
    if MyObjects.FIC(Terceiro = 0, nil, 'Terceiro n�o definido!') then Exit;
    if MyObjects.FIC(Vencto = 0, nil, 'Vencimento n�o definido!') then Exit;
    //
    Result := True;
  end;

  function InsereBloArre(Lancto, Conta, Entidade, Tipo, Config, CartEmiss,
    ProtocoloCR, Prev, FatID, FatParcela, LoteCR: Integer; Valor, NumBloq,
    FatNum: Double; Descri, Vencto: String): Boolean;
  var
    Msg: String;
    Controle: Integer;
  begin
    Result   := False;
    Controle := AtualizaArrecadacao(stIns, Prev, 0, Conta, 0, 0, Entidade,
                  CartEmiss, Tipo, Config, 0, Lancto, 1, 0, FatID, FatParcela, Vencto,
                  Valor, FatNum, NumBloq, Descri, Msg);
    if Controle <> 0 then
    begin
      if Tipo = 0 then
      begin
        if GeraProtocoloCR(ProtocoloCR, LoteCR, Entidade, Prev, Config, Valor,
          NumBloq, Vencto) <> 0
        then
          Result := True;
      end else
        Result := True;
    end;
  end;

  function GeraBoleto(Tipo, Config, Prev, Periodo, Terceiro, CliInt: Integer;
    FatNum: Double): Boolean;
  var
    i, Conta, Carteira, Lancto, FatID, FatParcela, ProtocoloCR: Integer;
    NumBloq, Valor: Double;
    Descricao, Vencto, Duplicata: String;
    QueryBoleto: TmySQLQuery;
  begin
    Result      := False;
    QueryBoleto := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QueryBoleto, Dmod.MyDB, [
        'SELECT ',
        Geral.FF0(Config) + ' CNAB_Cfg, ',
        Geral.FF0(Config) + ' Fatur_Cfg, ',
        Geral.FF0(Terceiro) + ' Entidade ',
        '']);
      Vencto  := Geral.FDT(QrLcts.FieldByName('Vencimento').AsDateTime, 1); //Todos lan�amentos devem ter o mesmo vencimento!

      if Tipo = 0 then
        NumBloq := UBloqGerl.GeraBloquetoAtual(Periodo, CliInt,
                     QrLcts.FieldByName('Vencimento').AsDateTime, False, TabLctA,
                     'arreits', '', QueryBoleto, nil, Dmod.MyDB, True, ProtocoloCR)
      else
        NumBloq := UBloqGerl.GeraFaturaAtual(Periodo, CliInt,
                     QrLcts.FieldByName('Vencimento').AsDateTime, False, TabLctA,
                     'arreits', '', QueryBoleto, nil, Dmod.MyDB, True);
      //
      if NumBloq <> 0 then
      begin
        if (ProtocoloCR <> 0) and (LoteProtoCR = 0) and (Tipo = 0)
        then
{$IfNDef SemProtocolo}
          LoteProtoCR := UnProtocolo.GeraLoteProtocolo(Dmod.MyDB, ProtocoloCR, Config, 4);
{$Else}
          LoteProtoCR := 0;
{$EndIf}

        //
        if (Grade <> nil) and (Grade.SelectedRows.Count > 1) then
        begin
          with Grade.DataSource.DataSet do
          for i := 0 to Grade.SelectedRows.Count - 1 do
          begin
            GotoBookmark(Grade.SelectedRows.Items[i]);
            //
            Conta      := QrLcts.FieldByName('Genero').AsInteger;
            Descricao  := QrLcts.FieldByName('Descricao').AsString;
            Valor      := QrLcts.FieldByName('Credito').AsFloat - QrLcts.FieldByName('Debito').AsFloat;
            Carteira   := QrLcts.FieldByName('Carteira').AsInteger;
            Lancto     := QrLcts.FieldByName('Controle').AsInteger;
            FatID      := QrLcts.FieldByName('FatID').AsInteger;
            FatParcela := QrLcts.FieldByName('FatParcela').AsInteger;
            Duplicata  := QrLcts.FieldByName('Duplicata').AsString;
            //
            InsereBloArre(Lancto, Conta, Terceiro, Tipo, Config, Carteira,
              ProtocoloCR, Prev, FatID, FatParcela, LoteProtoCR, Valor, NumBloq,
              QrLcts.FieldByName('FatNum').AsFloat, Descricao, Vencto);
          end;
        end else
        begin
          Conta      := QrLcts.FieldByName('Genero').AsInteger;
          Descricao  := QrLcts.FieldByName('Descricao').AsString;
          Vencto     := Geral.FDT(QrLcts.FieldByName('Vencimento').AsDateTime, 1);
          Valor      := QrLcts.FieldByName('Credito').AsFloat - QrLcts.FieldByName('Debito').AsFloat;
          Carteira   := QrLcts.FieldByName('Carteira').AsInteger;
          Lancto     := QrLcts.FieldByName('Controle').AsInteger;
          FatID      := QrLcts.FieldByName('FatID').AsInteger;
          FatParcela := QrLcts.FieldByName('FatParcela').AsInteger;
          Duplicata  := QrLcts.FieldByName('Duplicata').AsString;
          //
          InsereBloArre(Lancto, Conta, Terceiro, Tipo, Config, Carteira, ProtocoloCR, Prev,
            FatID, FatParcela, LoteProtoCR, Valor, NumBloq, FatNum, Descricao, Vencto);
        end;
        Result := True;
      end;
    finally
      QueryBoleto.Free;
    end;
  end;
var
  i, Terceiro, IniTerceiro, FatID, Peri, Controle, CliInt, Prev, TotConfig,
  CodConfig, CodTipo: Integer;
  Credito, Debito: Double;
  Vencto, IniVencto: TDateTime;
  Msg: String;
begin
  Result    := False;
  CodConfig := 0;
  //
  if Grade = nil then
  begin
    FatID    := QrLcts.FieldByName('FatID').AsInteger;
    Credito  := QrLcts.FieldByName('Credito').AsFloat;
    Debito   := QrLcts.FieldByName('Debito').AsFloat;
    Vencto   := QrLcts.FieldByName('Vencimento').AsDateTime;
    Terceiro := QrLcts.FieldByName('Cliente').AsInteger;
    Controle := QrLcts.FieldByName('Controle').AsInteger;
    //
    if Terceiro = 0 then
      Terceiro := QrLcts.FieldByName('Fornecedor').AsInteger;
    //
    if not ValidaDadosBoleto(Controle, FatID, Terceiro, Credito, Debito, Vencto) then
      Exit;
    (*
    if MyObjects.FIC(VerificaSeBoletoExiste(Controle), nil,
      'J� foi emitido uma fatura para o lan�amento ID n�mero ' +
      Geral.FF0(Controle) + '!')
    then
      Exit;
    if MyObjects.FIC((Credito - Debito) <= 0, nil,
      'A soma dos valores dos lan�amentos selecionados deve ser maior que 0 (Zero)!')
    then
      Exit;
    if MyObjects.FIC(VerificaSeFaturaEstaPaga(TabLctA, Controle), nil,
      'O lan�amento selecionado n�o deve estar quitado!')
    then
      Exit;
    if MyObjects.FIC(VerificaSeBoletoExiste(QrLcts.FieldByName('Controle').AsInteger),
      nil, 'Este lan�amento j� possui uma fatura gerada!')
    then
      Exit;
    if MyObjects.FIC(FatID = FArre_FatID, nil, 'Voc� deve selecionar apenas lan�amentos que n�o sejam faturas!') then Exit;
    if MyObjects.FIC(Terceiro = 0, nil, 'Terceiro n�o definido!') then Exit;
    if MyObjects.FIC(Vencto = 0, nil, 'Vencimento n�o definido!') then Exit;
    *)
  end else
  begin
    if Grade.SelectedRows.Count > 1 then
    begin
      Credito  := 0;
      Debito   := 0;
      Terceiro := 0;
      Vencto   := 0;
      //
      //Verifica antes de emitir
      with Grade.DataSource.DataSet do
      for i := 0 to Grade.SelectedRows.Count - 1 do
      begin
        GotoBookmark(Grade.SelectedRows.Items[i]);
        //
        FatID    := QrLcts.FieldByName('FatID').AsInteger;
        Credito  := Credito + QrLcts.FieldByName('Credito').AsFloat;
        Debito   := Debito + QrLcts.FieldByName('Debito').AsFloat;
        Vencto   := QrLcts.FieldByName('Vencimento').AsDateTime;
        Terceiro := QrLcts.FieldByName('Cliente').AsInteger;
        Controle := QrLcts.FieldByName('Controle').AsInteger;
        //
        if Terceiro = 0 then
          Terceiro := QrLcts.FieldByName('Fornecedor').AsInteger;
        //
        if not ValidaDadosBoleto(Controle, FatID, Terceiro, Credito, Debito, Vencto) then
          Exit;
        //
        if i = 0 then
        begin
          IniVencto   := QrLcts.FieldByName('Vencimento').AsDateTime;
          IniTerceiro := QrLcts.FieldByName('Cliente').AsInteger;
          //
          if IniTerceiro = 0 then
            IniTerceiro := QrLcts.FieldByName('Fornecedor').AsInteger;
        end;
        if MyObjects.FIC(Terceiro <> IniTerceiro, nil,
          'Todos os lan�amentos selecionados devem pertencer ao mesmo terceiro!')
        then
          Exit;
        if MyObjects.FIC(Vencto <> IniVencto, nil,
          'Todos os lan�amentos selecionados devem ter o mesmo vencimento!')
        then
          Exit;
        (*
        if i > 0 then
        begin
          if MyObjects.FIC(Terceiro = 0, nil,
            'Todos os lan�amentos selecionados devem pertencer ao mesmo terceiro!')
          then
            Exit;
          if MyObjects.FIC(Vencto <> QrLcts.FieldByName('Vencimento').AsDateTime,
            nil, 'Todos os lan�amentos selecionados devem ter o mesmo vencimento!')
          then
            Exit;
          if MyObjects.FIC(VerificaSeBoletoExiste(QrLcts.FieldByName('Controle').AsInteger),
            nil, 'Este lan�amento j� possui uma fatura para o lan�amento selecionado!')
          then
            Exit;
        end;
        if MyObjects.FIC(VerificaSeFaturaEstaPaga(TabLctA, Controle), nil,
          'Todos os lan�amentos selecionados n�o devem estar quitados!')
        then
          Exit;
        //
        if MyObjects.FIC(FatID = FArre_FatID, nil, 'Voc� deve selecionar apenas lan�amentos que n�o sejam faturas!') then Exit;
        if MyObjects.FIC(Terceiro = 0, nil, 'Terceiro n�o definido!') then Exit;
        if MyObjects.FIC(Vencto = 0, nil, 'Vencimento n�o definido!') then Exit;
        //
        if i = (Grade.SelectedRows.Count - 1) then
        begin
          if MyObjects.FIC((Credito - Debito) <= 0, nil,
            'A soma dos valores dos lan�amentos selecionados deve ser maior que 0 (Zero)!')
          then
            Exit;
        end;
        *)
      end;
    end else
    begin
      FatID    := QrLcts.FieldByName('FatID').AsInteger;
      Credito  := QrLcts.FieldByName('Credito').AsFloat;
      Debito   := QrLcts.FieldByName('Debito').AsFloat;
      Vencto   := QrLcts.FieldByName('Vencimento').AsDateTime;
      Terceiro := QrLcts.FieldByName('Cliente').AsInteger;
      Controle := QrLcts.FieldByName('Controle').AsInteger;
      //
      if Terceiro = 0 then
        Terceiro := QrLcts.FieldByName('Fornecedor').AsInteger;
      //
      if not ValidaDadosBoleto(Controle, FatID, Terceiro, Credito, Debito, Vencto) then
        Exit;
      (*
      if MyObjects.FIC(Controle = 0, nil, 'Nenhum item foi selecionado!') then Exit;
      //
      if MyObjects.FIC(VerificaSeBoletoExiste(Controle), nil,
        'J� foi emitido uma fatura para o lan�amento ID n�mero ' +
        Geral.FF0(Controle) + '!')
      then
        Exit;
      if MyObjects.FIC((Credito - Debito) <= 0, nil,
        'A soma dos valores dos lan�amentos selecionados deve ser maior que 0 (Zero)!')
      then
        Exit;
      if MyObjects.FIC(VerificaSeFaturaEstaPaga(TabLctA, Controle), nil,
        'O lan�amento selecionado n�o deve estar quitado!')
      then
        Exit;
      if MyObjects.FIC(VerificaSeBoletoExiste(QrLcts.FieldByName('Controle').AsInteger),
        nil, 'Este lan�amento j� possui uma fatura gerada!')
      then
        Exit;
      if MyObjects.FIC(FatID = FArre_FatID, nil, 'Voc� deve selecionar apenas lan�amentos que n�o sejam faturas!') then Exit;
      if MyObjects.FIC(Terceiro = 0, nil, 'Terceiro n�o definido!') then Exit;
      if MyObjects.FIC(Vencto = 0, nil, 'Vencimento n�o definido!') then Exit;
      *)
    end;
  end;
  DBloGeren.ReopenBloOpcoes;
  //
  if Config <> 0 then
  begin
    TotConfig := 1;
    CodConfig := Config;
    CodTipo   := Tipo;
  end else
  begin
    TotConfig := VerificaFatur_Cfg(QrLcts.FieldByName('Carteira').AsInteger, CodTipo, CodConfig);
  end;
  //
  Peri   := Geral.DTP(QrLcts.FieldByName('Vencimento').AsDateTime) - 1;
  CliInt := QrLcts.FieldByName('CliInt').AsInteger;
  //
  case DBloGeren.QrBloOpcoes.FieldByName('PerGerBol').AsInteger of
    0: Peri := Peri - 1; //Anterior
    1: Peri := Peri;     //Atual
    2: Peri := Peri + 1; //Pr�ximo   
  end;
  Prev := LocalizaPrev(CliInt, Peri);
  //
  if MyObjects.FIC(TotConfig = 0, nil,
    'N�o existe configura��o para a carteira n� ' +
    Geral.FF0(QrLcts.FieldByName('Carteira').AsInteger)) then Exit;
  //
  if Prev = 0 then
  begin
    Prev := AtualizaBloGerenNew(stIns, 0, 0, CliInt, Peri,
      DBloGeren.QrBloOpcoes.FieldByName('ModelBloq').AsInteger,
      DBloGeren.QrBloOpcoes.FieldByName('Compe').AsInteger,
      DBloGeren.QrBloOpcoes.FieldByName('BloqFV').AsInteger,
      '', '', '', '', '', '', '', '', '', '', '', Msg);
    if Prev = 0 then
    begin
      Geral.MB_Aviso(Msg);
      Exit;
    end;
  end;
  if Prev > 0 then
  begin
    if (TotConfig = 1) and (CodConfig <> 0) and (CodTipo >= 0) then
    begin
      Result := GeraBoleto(CodTipo, CodConfig, Prev, Peri, Terceiro, CliInt, FatNum);
    end else
    begin
      if DBCheck.CriaFm(TFmBloAvulso, FmBloAvulso, afmoNegarComAviso) then
      begin
        FmBloAvulso.FCarteira := QrLcts.FieldByName('Carteira').AsInteger;
        FmBloAvulso.ShowModal;
        //
        CodTipo   := FmBloAvulso.FTipo;
        CodConfig := FmBloAvulso.FConfig;
        //
        FmBloAvulso.Destroy;
        //
        if CodConfig <> 0 then
          Result := GeraBoleto(CodTipo, CodConfig, Prev, Peri, Terceiro, CliInt, FatNum);
      end;
    end;
  end else
    Geral.MB_Aviso('Inclus�o abortada!' + sLineBreak + 'Per�odo n�o definido!');
end;

function TUnBloquetos.ExcluiArrecadacao(Codigo, Controle: Integer; Todos: Boolean): Boolean;

  function LocalizaArreIts(Controle: Integer; var Bloqueto: Double;
    var Texto: String): Boolean;
  begin
    Bloqueto := 0;
    Texto    := '';
    Result   := False;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
      'SELECT Boleto, Texto ',
      'FROM arreits ',
      'WHERE Controle=' + Geral.FF0(Controle),
      '']);
    if DBloGeren.QrLoc.RecordCount > 0 then
    begin
      Bloqueto := DBloGeren.QrLoc.FieldByName('Boleto').AsFloat;
      Texto    := DBloGeren.QrLoc.FieldByName('Texto').AsString;
      Result   := True;
    end;
  end;

var
  Bloq: Double;
  Txt: String;
begin
  Result := False;
  //
  if Todos then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
      'SELECT Boleto ',
      'FROM arreits ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Boleto<>0 ',
      '']);
    if DBloGeren.QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso(
      'Exclus�o cancelada! J� foram gerados boletos para estas arrecada��es!'
      + sLineBreak +
      'Desfa�a os boletos referentes a estas arrecada��es e tente novamente!');
      Exit;
    end;
    if Geral.MB_Pergunta('Confirma a EXCLUS�O de TODOS itens de ' +
      'arrecada��o deste per�odo?') = ID_YES then
    begin
      //Volta arrefut
      UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
        'SELECT Controle ',
        'FROM arreits ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        'AND ComoAdd=2 ',
        '']);
      if DBloGeren.QrLoc.RecordCount > 0 then
      begin
        while not DBloGeren.QrLoc.EOF do
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
            'UPDATE arrefut SET ' + TAB_ARI + '=0 ',
            'WHERE ' + TAB_ARI + '=' + Geral.FF0(DBloGeren.QrLoc.FieldByName('Controle').AsInteger),
            '']);
          //
          DBloGeren.QrLoc.Next;
        end;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'DELETE FROM arreits ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      Result := True;
    end;
  end else
  begin
    if LocalizaArreIts(Controle, Bloq, Txt) then
    begin
      if Bloq > 0 then
      begin
        Geral.MB_Aviso(
        'Exclus�o cancelada! J� foi gerado uma fatura para esta arrecada��o!'
        + sLineBreak +
        'Desfa�a a fatura referente a esta arrecada��o e tente novamente!');
        Exit;
      end;
      if Geral.MB_Pergunta('Confirma a exclus�o do item "' + Txt + '"? ') = ID_YES then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
          'SELECT Controle ',
          'FROM arreits ',
          'WHERE Controle=' + Geral.FF0(Controle),
          'AND ComoAdd=2 ',
          '']);
        if DBloGeren.QrLoc.RecordCount > 0 then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
            'UPDATE arrefut SET ' + TAB_ARI + '=0 ',
            'WHERE ' + TAB_ARI + '=' + Geral.FF0(DBloGeren.QrLoc.FieldByName('Controle').AsInteger),
            '']);
        end;
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'DELETE FROM arreits WHERE Controle=' + Geral.FF0(Controle),
          '']);
        //
        Result := True;
      end;
    end;
  end;
end;

function TUnBloquetos.GeraProtocoloCR(ProtocoloCR, LoteCR, Entidade, Prev,
  CNAB_Cfg: Integer; Valor, NumBloq: Double; Vencto: String): Integer;
var
  Agora: TDateTime;
  LimiteSai, LimiteRem, LimiteRet: TDate;
  Conta, Periodo, CliEnti: Integer;
  MoraDiaVal, MultaVal, MoraPer, MultaPer: Double;
  LimiteSai_Txt, LimiteRem_Txt, LimiteRet_Txt: String;
begin
{$IfNDef SemProtocolo}
  Result    := 0;
  Agora     := DModG.ObtemAgora();
  LimiteSai := Agora + 1;
  LimiteRem := Agora + 1;
  LimiteRet := Agora + 4;
  //
  LimiteSai_Txt := Geral.FDT(LimiteSai, 1);
  LimiteRem_Txt := Geral.FDT(LimiteRem, 1);
  LimiteRet_Txt := Geral.FDT(LimiteRet, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
    'SELECT Periodo, Empresa ',
    'FROM prev ',
    'WHERE Codigo=' + Geral.FF0(Prev),
    '']);
  if DBloGeren.QrLoc.RecordCount > 0 then
  begin
    CliEnti := DBloGeren.QrLoc.FieldByName('Empresa').AsInteger;
    Periodo := DBloGeren.QrLoc.FieldByName('Periodo').AsInteger
  end else
  begin
    CliEnti := 0;
    Periodo := 0;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
    'SELECT cfg.MultaPerc, cfg.JurosPerc',
    'FROM protocopak pak',
    'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=pak.CNAB_Cfg',
    'WHERE pak.Controle=:P0',
    '']);
  MoraPer    := DBloGeren.QrLoc.FieldByName('JurosPerc').AsFloat;
  MultaPer   := DBloGeren.QrLoc.FieldByName('MultaPerc').AsFloat;
  MoraDiaVal := Round(Valor * (MoraPer / 30)) / 100;
  MultaVal   := Round(Valor * MultaPer) / 100;
  //
  Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakits', False,
    [
      'Codigo', 'Controle', 'Link_ID', 'CliInt', 'Cliente', 'Fornece',
      'Periodo', 'Lancto', 'Docum', 'Depto', 'ID_Cod1', 'ID_Cod2',
      'ID_Cod3', 'ID_Cod4', 'Retorna', 'Vencto', 'Valor', 'MoraDiaVal',
      'MultaVal', 'Cedente', 'LimiteSai', 'LimiteRem', 'LimiteRet'
    ], ['Conta'], [
      ProtocoloCR, LoteCR, VAR_TIPO_LINK_ID_01_GENERICO, CliEnti, Entidade, 0,
      Periodo, 0, NumBloq, 0, Prev, CliEnti,
      0, CNAB_Cfg, 0, Vencto, Valor, MoraDiaVal,
      MultaVal, 0, LimiteSai_Txt, LimiteRem_Txt, LimiteRet_Txt
    ], [Conta], True) then
  begin
    Result := Conta;
  end;
{$Else}
  Result := 0;
{$EndIf}
end;

function TUnBloquetos.ImprimeBloqueto(CkZerado: Boolean; TabLctA: String;
  QueryBoletos, QueryBoletosIts: TmySQLQuery; frxDsBoletos,
  frxDsBoletosIts: TfrxDBDataset; Quais: TselType; Como: TfrxImpComo;
  Arquivo: String; Filtro: TfrxCustomExportFilter; Grade: TDBGrid;
  TextoVia2: String): TfrxReport;
begin
  Application.CreateForm(TFmBloImp, FmBloImp);
  //
  Result := FmBloImp.ImprimeBloq(CkZerado, TabLctA, QueryBoletos,
              QueryBoletosIts, frxDsBoletos, frxDsBoletosIts, Quais, Como,
              Arquivo, Filtro, Grade, TextoVia2);
  //
  FmBloImp.Destroy;
end;

function TUnBloquetos.LocalizaPrev(Empresa, Periodo: Integer): Integer;
begin
  DBloGeren.QrLoc.Close;
  DBloGeren.QrLoc.SQL.Clear;
  DBloGeren.QrLoc.SQL.Add('SELECT Codigo');
  DBloGeren.QrLoc.SQL.Add('FROM prev');
  DBloGeren.QrLoc.SQL.Add('WHERE Empresa=:P0');
  DBloGeren.QrLoc.SQL.Add('AND Periodo=:P1');
  DBloGeren.QrLoc.Params[0].AsInteger := Empresa;
  DBloGeren.QrLoc.Params[1].AsInteger := Periodo;
  UnDmkDAC_PF.AbreQuery(DBloGeren.QrLoc, Dmod.MyDB);
  if DBloGeren.QrLoc.RecordCount = 0 then
    Result := 0
  else
    Result := DBloGeren.QrLoc.FieldByName('Codigo').AsInteger;
end;

procedure TUnBloquetos.MostraBloArre(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmBloArre, FmBloArre, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmBloArre.LocCod(Codigo, Codigo);
    if Controle <> 0 then
      FmBloArre.ReopenBloArreIts(Controle);
    FmBloArre.ShowModal;
    FmBloArre.Destroy;
  end;
end;

procedure TUnBloquetos.MostraBloGeren(Aba, FatID, Lancamento: Integer;
  FatNum: Double; PageControl: TPageControl; Pager: TWinControl);
begin
  VAR_ModBloq_FatID  := FatID;
  VAR_ModBloq_FatNum := FatNum;
  VAR_ModBloq_Lancto := Lancamento;
  //
  if UBloqGerl.DefineVarsBloGeren(VAR_ModBloq_EntCliInt, VAR_ModBloq_CliInt,
    VAR_ModBloq_TabLctA, VAR_ModBloq_TabLctB, VAR_ModBloq_TabLctD,
    VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA, VAR_ModBloq_TabPriA,
    VAR_ModBloq_TabPrvA) then
  begin
    CadastroBloGeren(Aba, PageControl, Pager);
  end;
end;

procedure TUnBloquetos.MostraBloGerenInadImp(PageControl: TPageControl;
  Pager: TWinControl);
begin
  if UBloqGerl.DefineVarsBloGeren(VAR_ModBloq_EntCliInt, VAR_ModBloq_CliInt,
    VAR_ModBloq_TabLctA, VAR_ModBloq_TabLctB, VAR_ModBloq_TabLctD,
    VAR_ModBloq_TabAriA, VAR_ModBloq_TabCnsA, VAR_ModBloq_TabPriA,
    VAR_ModBloq_TabPrvA) then
  begin
    CadastroBloGerenInadImp(DModG.QrFiliLogFilial.Value, PageControl, Pager);
  end;
end;

procedure TUnBloquetos.CadastroBloGerenInadImp(CliInt: Integer;
  InOwner: TWincontrol; Pager: TWinControl);
var
  Abertura: Integer;
  Hoje: TDateTime;
  Form: TForm;
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  DBloGeren.ReopenBloOpcoes;
  //
  Hoje     := DModG.ObtemAgora;
  Abertura := DBloGeren.QrBloOpcoes.FieldByName('AbreEmTab').AsInteger;
  //
  if Abertura = 2 then //Escolhe
  begin
    Abertura := MyObjects.SelRadioGroup('Selecione o modelo', '', ['Janela', 'Aba'], 2);
  end;
  //
  case Abertura of
    0: //Janela
    begin
      if DBCheck.CriaFm(TFmBloGerenInadImp, FmBloGerenInadImp, afmoNegarComAviso) then
      begin
        if CliInt <> 0 then
        begin
          FmBloGerenInadimp.EdEmpresa.ValueVariant := CliInt;
          FmBloGerenInadimp.CBEmpresa.KeyValue     := CliInt;
        end;
        FmBloGerenInadimp.CkIniVct.Checked := True;
        FmBloGerenInadimp.CkFimVct.Checked := True;
        FmBloGerenInadimp.TPIniVct.Date    := Geral.PrimeiroDiaDoMes(Hoje);
        FmBloGerenInadimp.TPFimVct.Date    := Geral.UltimoDiaDoMes(Hoje);
        //
        FmBloGerenInadImp.ShowModal;
        FmBloGerenInadImp.Destroy;
        //
        FmBloGerenInadImp := nil;
      end;
    end;
    1: //Aba
    begin
      if FmBloGerenInadImp = nil then
      begin
        Form := MyObjects.FormTDICria(TFmBloGerenInadImp, InOwner, Pager, True, True);
        //
        if CliInt <> 0 then
        begin
          TFmBloGerenInadImp(Form).EdEmpresa.ValueVariant := CliInt;
          TFmBloGerenInadImp(Form).CBEmpresa.KeyValue     := CliInt;
        end;
        TFmBloGerenInadImp(Form).CkIniVct.Checked := True;
        TFmBloGerenInadImp(Form).CkFimVct.Checked := True;
        TFmBloGerenInadImp(Form).TPIniVct.Date    := Geral.PrimeiroDiaDoMes(Hoje);
        TFmBloGerenInadImp(Form).TPFimVct.Date    := Geral.UltimoDiaDoMes(Hoje);
      end;
    end;
  end;
end;

function TUnBloquetos.ObtemDocumProtPakIts(ProtPakIts: Integer): Double;
var
  Perio, FatID: Integer;
  Docum, FatNum: Double;
begin
  DBloGeren.ReopenBloOpcoes;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
    'SELECT Docum, ID_Cod1, ID_Cod2 ',
    'FROM protpakits ',
    'WHERE Conta=' + Geral.FF0(ProtPakIts),
    '']);
  FatID := DBloGeren.QrLoc.FieldByName('ID_Cod2').AsInteger;
  Docum := DBloGeren.QrLoc.FieldByName('Docum').AsFloat;
  Perio := DBloGeren.QrLoc.FieldByName('ID_Cod1').AsInteger;
  //
  if (DBloGeren.QrBloOpcoes.FieldByName('NumDocPad').AsInteger = 0) or
    (FatID <> FArre_FatID) then
  begin
    Result := Docum;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
      'SELECT its.FatNum ',
      'FROM arreits its ',
      'WHERE its.Codigo=' + Geral.FF0(Perio),
      'AND its.Boleto=' + Geral.FFI(Docum),
      '']);
    FatNum := DBloGeren.QrLoc.FieldByName('FatNum').AsFloat;
    //
    if FatNum <> 0 then
      Result := FatNum
    else
      Result := Docum;
  end;
end;

function TUnBloquetos.ObtemNumeroDocumento(NumDocPad: Integer; FatNum,
  Boleto: Double; Lancto: Integer; TabLct: String): Double;

  function ObtemNotaFiscalDeLancamento(): Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
      'SELECT NotaFiscal ',
      'FROM ' + TabLct,
      'WHERE Controle=' + Geral.FF0(Lancto),
      '']);
    Result := DBloGeren.QrLoc.FieldByName('NotaFiscal').AsInteger;
  end;
var
  NotaFiscal: Integer;
begin
  case NumDocPad of
    1: //N�mero da fatura
    begin
      if FatNum <> 0 then
        Result := FatNum
      else
        Result := Boleto;
    end;
    2: //N�mero da nota fiscal
    begin
      NotaFiscal := ObtemNotaFiscalDeLancamento();
      //
      if NotaFiscal <> 0 then
        Result := NotaFiscal
      else
        Result := Boleto;
    end;
    else //N�mero do boleto
    begin
      Result := Boleto;
    end;
  end;
end;

procedure TUnBloquetos.PreencheModelosBloq(RG: TRadioGroup);
begin
  RG.Items.Clear;
  {0} RG.Items.Add('? - Definir no momento da impress�o');
  {1} RG.Items.Add('A - 3 colunas fonte tamanho 8 sem rodap�');
  {2} RG.Items.Add('B - 1 coluna fonte tamanho 8 sem rodap�');
  {3} RG.Items.Add('E - Somente boleto (nenhum n�vel)');
end;

procedure TUnBloquetos.ReajustarArrecadacaoIGPM(PeriodoIni, PeriodoFim,
  SitCobr, IDNum: Integer; Valor: Double);
begin
  if DBCheck.CriaFm(TFmBloArreIGPM, FmBloArreIGPM, afmoNegarComAviso) then
  begin
    FmBloArreIGPM.FIDNum    := IDNum;
    FmBloArreIGPM.FValor    := Valor;
    FmBloArreIGPM.FPeriodoI := PeriodoIni;
    FmBloArreIGPM.FPeriodoF := PeriodoFim;
    FmBloArreIGPM.FSitCobr  := SitCobr;
    //
    FmBloArreIGPM.ShowModal;
    FmBloArreIGPM.Destroy;
  end;
end;

procedure TUnBloquetos.ReopenArre(QueryArre, QuerySumARRE: TmySQLQuery;
  Codigo, Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryArre, Dmod.MyDB, [
    'SELECT ari.Vencto, ari.Entidade, SUM(ari.Valor) Valor, ',
    'IF(ari.CNAB_Cfg<>0, cna.Nome, fat.Nome) NOME_Cfg, ',
    'CASE WHEN ent.Tipo=0 ',
    'THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
    'ari.Boleto, ari.CNAB_Cfg, ari.Fatur_Cfg, ',
    'IF(ari.CNAB_Cfg<>0, 0, 1) + 0.000 Tipo, ',
    'IF(ari.CNAB_Cfg<>0, ari.CNAB_Cfg, ari.Fatur_Cfg) + 0.000 Config ',
    'FROM arreits ari ',
    'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade ',
    'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
    'LEFT JOIN faturcfg fat ON fat.Codigo=ari.Fatur_Cfg ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    'GROUP BY Tipo, Config, ari.Entidade, ari.Vencto ',
    'ORDER BY  NOME_Cfg, NOMEENT ',
    '']);
  if Entidade <> 0 then
    QueryArre.Locate('Entidade', Entidade, []);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QuerySumARRE, Dmod.MyDB, [
    'SELECT SUM(ari.Valor) Valor ',
    'FROM arreits ari ',
    'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    '']);
end;

procedure TUnBloquetos.ReopenArreFut(QueryArreFutI: TmySQLQuery; CliInt,
  MesesAnt, Controle: Integer);
var
  Loc, Peri: Integer;
begin
  Peri := Geral.Periodo2000(DModG.ObtemAgora());
  Loc  := Controle;
  //
  if Controle < 1 then
    if QueryArreFutI.State = dsBrowse then
      Loc := QueryArreFutI.FieldByName('Controle').AsInteger;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryArreFutI, Dmod.MyDB, [
    'SELECT IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEENT, ',
    'con.Nome NOMECONTA, ',
    {$IfNDef sNFSe}
    'nsc.Nome NFSeSrvCad_TXT, ',
    {$Else}
    '"" NFSeSrvCad_TXT, ',
    {$EndIf}
    'IF(arf.CNAB_Cfg<>0, cna.Nome, fat.Nome) NOME_Cfg, ',
    'arf.* ',
    'FROM arrefut arf ',
    'LEFT JOIN entidades prp ON prp.Codigo=arf.Depto ',
    'LEFT JOIN contas    con ON con.Codigo=arf.Conta ',
    'LEFT JOIN cnab_cfg  cna ON cna.Codigo=arf.CNAB_Cfg ',
    'LEFT JOIN faturcfg  fat ON fat.Codigo=arf.Fatur_Cfg ',
    {$IfNDef sNFSe}
    'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo = arf.NFSeSrvCad ',
    {$EndIf}
    'WHERE arf.' + TAB_ARI + '=0 ',
    'AND arf.CliInt=' + Geral.FF0(CliInt),
    'AND arf.Periodo>=' + Geral.FF0(Peri - MesesAnt),
    'ORDER BY arf.Periodo DESC ',
    '']);
  //
  if Loc > 0 then
    QueryArreFutI.Locate('Controle', Loc, []);
end;

procedure TUnBloquetos.ReopenBoletosIts(Query: TMySQLQuery; Boleto: Double;
  Entidade, Tipo, Config, Codigo: Integer; Vencto: TDate);
var
  SQL_Vcto, Campo: String;
begin
  if Tipo = 0 then //CNAB_Cfg
    Campo := 'CNAB_Cfg'
  else
    Campo := 'Fatur_Cfg';
  //
  if Vencto > 0 then
    SQL_Vcto := 'AND ari.Vencto="' + Geral.FDT(Vencto, 1) + '"'
  else
    SQL_Vcto := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT ari.Texto TEXTO, ari.Valor VALOR, ari.NFSeSrvCad, ',
    'ari.Entidade, ari.Vencto, ari.Controle, ari.Lancto, ari.BloArreIts, ',
    'ari.CNAB_Cfg, ari.Avulso, ari.Boleto, ari.FatNum, ari.Conta Genero, ',
    'IF(lca.Controle IS NULL, IF(lcb.Controle IS NULL, IF(lcd.Controle IS NULL, "", lcd.SerieNF), lcb.SerieNF), lca.SerieNF) SerieNF, ',
    'IF(lca.Controle IS NULL, IF(lcb.Controle IS NULL, IF(lcd.Controle IS NULL, 0, lcd.NotaFiscal), lcb.NotaFiscal), lca.NotaFiscal) + 0.000 NotaFiscal ',
    'FROM arreits ari ',
    'LEFT JOIN ' + VAR_ModBloq_TabLctA + ' lca ON lca.Controle = ari.Lancto ',
    'LEFT JOIN ' + VAR_ModBloq_TabLctB + ' lcb ON lcb.Controle = ari.Lancto ',
    'LEFT JOIN ' + VAR_ModBloq_TabLctD + ' lcd ON lcd.Controle = ari.Lancto ',
    'WHERE ari.Boleto=' + Geral.FFI(Boleto),
    'AND ari.Entidade=' + Geral.FF0(Entidade),
    'AND ari.' + Campo + '=' + Geral.FF0(Config),
    'AND ari.Codigo=' + Geral.FF0(Codigo),
    SQL_Vcto,
    'ORDER BY VALOR DESC ',
    '']);
end;

function TUnBloquetos.TextoExplicativoItemBoleto(EhConsumo, Casas: Integer;
  MedAnt, MedAtu, Consumo, UnidFat: Double; UnidLei, UnidImp: String; GeraTyp,
  CasRat, NaoImpLei: Integer; GeraFat: Double): String;
begin
  //Compatibilidade
  Result := '';
end;

function TUnBloquetos.VerificaAntesDeCancelar(Periodo, Tipo, Config: Integer;
  Boleto: Double): Boolean;
var
  CampoConfig: String;
begin
  if Tipo = 0 then
    CampoConfig := 'CNAB_Cfg'
  else
    CampoConfig := 'Fatur_Cfg';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrPesqMB, Dmod.MyDB, [
    'SELECT MAX(ari.Boleto) Boleto ',
    'FROM arreits ari ',
    'LEFT JOIN prev prv ON  prv.Codigo=ari.Codigo ',
    'WHERE prv.Periodo>' + Geral.FF0(Periodo),
    'AND ari.' + CampoConfig + '=' + Geral.FF0(Config),
    '']);
  //
  if DBloGeren.QrPesqMBBoleto.Value > Boleto then
    Result := False
  else
    Result := True;
end;

function TUnBloquetos.VerificaArrecadacao(Entidade, Conta, BloArre, BloArreIts,
  Empresa, Periodo: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLocArre, Dmod.MyDB, [
    'SELECT its.Controle ',
    'FROM arreits its ',
    'LEFT JOIN prev pre ON pre.Codigo = its.Codigo ',
    'WHERE its.BloArre=' + Geral.FF0(BloArre),
    'AND its.BloArreIts=' + Geral.FF0(BloArreIts),
    'AND pre.Empresa=' + Geral.FF0(Empresa),
    'AND pre.Periodo=' + Geral.FF0(Periodo),
    '']);
  if DBloGeren.QrLocArre.RecordCount > 0 then
    Result := False
  else
    Result := True;
end;

procedure TUnBloquetos.VerificaArrencadacoesVencendo;
begin
  if DBloGeren.ReopenBloArreItsVen <> 0 then
  begin
    if DBCheck.CriaFm(TFmBloGerenBaAVen, FmBloGerenBaAVen, afmoNegarComAviso) then
    begin
      FmBloGerenBaAVen.ShowModal;
      FmBloGerenBaAVen.Destroy;
    end;
  end;
end;

function TUnBloquetos.VerificaFatur_Cfg(const Carteira: Integer;
  var Tipo, Config: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
    'SELECT 0 Tipo, Codigo ',
    'FROM cnab_cfg ',
    'WHERE CartEmiss=' + Geral.FF0(Carteira),
    ' ',
    'UNION ',
    ' ',
    'SELECT 1 Tipo, Codigo ',
    'FROM faturcfg ',
    'WHERE Carteira=' + Geral.FF0(Carteira),
    '']);
  if DBloGeren.QrLoc.RecordCount = 1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM cnab_cfg ',
      'WHERE CartEmiss=' + Geral.FF0(Carteira),
      '']);
    //
    if DBloGeren.QrLoc.RecordCount = 1 then
    begin
      Tipo   := 0;
      Config := DBloGeren.QrLoc.FieldByName('Codigo').AsInteger;
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
        'SELECT Codigo ',
        'FROM faturcfg ',
        'WHERE Carteira=' + Geral.FF0(Carteira),
        '']);
      if DBloGeren.QrLoc.RecordCount = 1 then
      begin
        Tipo   := 1;
        Config := DBloGeren.QrLoc.FieldByName('Codigo').AsInteger;
      end else
      begin
        Tipo   := -1;
        Config := 0;
      end;
    end;
  end else
  begin
    Tipo   := -1;
    Config := 0;
  end;
  //
  Result := DBloGeren.QrLoc.RecordCount;
  //
  DBloGeren.QrLoc.Close;
end;

function TUnBloquetos.VerificaParcelas(SitCobr, MesI, MesF: Integer; AnoI,
  AnoF: String; HabilBot: Boolean): Double;
var
  Ano, ParcPerI, ParcPerF, pp: Integer;
begin
  Result := 0;
  //
  ParcPerI := MesI + 1;
  ParcPerF := MesF + 1;
  //
  if SitCobr = 2 then
  begin
    Ano      := Geral.IMV(AnoI);
    ParcPerI := ((Ano - 2000) * 12) + ParcPerI;
  end;
  if SitCobr = 2 then
  begin
    Ano      := Geral.IMV(AnoF);
    ParcPerF := ((Ano - 2000) * 12) + ParcPerF;
    pp       := ParcPerF - ParcPerI + 1;
    //
    Result := pp;
  end;
end;

function TUnBloquetos.VerificaSeFaturaEstaPaga(TabLct: String;
  Lancto: Integer): Boolean;
begin
  //True  = Pago
  //False = Em aberto
  //
  DBloGeren.QrLoc.Close;
  DBloGeren.QrLoc.SQL.Clear;
  DBloGeren.QrLoc.SQL.Add('SELECT *');
  DBloGeren.QrLoc.SQL.Add('FROM ' + TabLct);
  DBloGeren.QrLoc.SQL.Add('WHERE Controle = ' + Geral.FF0(Lancto));
  UnDmkDAC_PF.AbreQuery(DBloGeren.QrLoc, Dmod.MyDB);
  //
  if DBloGeren.QrLoc.RecordCount > 0 then
  begin
    DBloGeren.QrLoc.Close;
    DBloGeren.QrLoc.SQL.Clear;
    DBloGeren.QrLoc.SQL.Add('SELECT *');
    DBloGeren.QrLoc.SQL.Add('FROM ' + TabLct);
    DBloGeren.QrLoc.SQL.Add('WHERE Sit < 2');
    DBloGeren.QrLoc.SQL.Add('AND Pago = 0');
    DBloGeren.QrLoc.SQL.Add('AND Controle = ' + Geral.FF0(Lancto));
    UnDmkDAC_PF.AbreQuery(DBloGeren.QrLoc, Dmod.MyDB);
    if DBloGeren.QrLoc.RecordCount > 0 then
      Result := False
    else
      Result := True;
  end else
    Result := False;
end;

function TUnBloquetos.VerificaSeBoletoExiste(Controle: Integer): Boolean;
begin
  //True  = Existe
  //False = N�o existe
  //
  DBloGeren.QrLoc.Close;
  DBloGeren.QrLoc.SQL.Clear;
  DBloGeren.QrLoc.SQL.Add('SELECT *');
  DBloGeren.QrLoc.SQL.Add('FROM arreits');
  DBloGeren.QrLoc.SQL.Add('WHERE Lancto=:P0');
  DBloGeren.QrLoc.Params[0].AsInteger := Controle;
  UMyMod.AbreQuery(DBloGeren.QrLoc, Dmod.MyDB);
  if DBloGeren.QrLoc.RecordCount > 0 then
    Result := True
  else
    Result := False;
end;

function TUnBloquetos.VerificaSeOpcoesForamConfiguradas: Boolean;
begin
  DBloGeren.ReopenBloOpcoes;
  //
  if DBloGeren.QrBloOpcoes.FieldByName('Configurado').AsInteger = 0 then
    Result := False
  else
    Result := True;  
end;

function TUnBloquetos.VerificaSePeriodoEstaEncerrado(Codigo: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DBloGeren.QrLoc, Dmod.MyDB, [
    'SELECT Encerrado ',
    'FROM prev ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
  Result := Geral.IntToBool(DBloGeren.QrLoc.FieldByName('Encerrado').AsInteger);
end;

end.
