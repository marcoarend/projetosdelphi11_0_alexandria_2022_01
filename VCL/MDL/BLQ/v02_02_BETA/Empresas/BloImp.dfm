object FmBloImp: TFmBloImp
  Left = 0
  Top = 0
  ClientHeight = 303
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 17
  object frxPDFExport: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'Dermatek'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 47
    Top = 31
  end
  object frxVerso: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39113.610607638900000000
    ReportOptions.LastChange = 39113.610607638900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  PageHeader1.Height  := <VARF_BLQ_AltuHeader>;'
      '  MasterData1.Height  := <VARF_BLQ_AltuRotulo>;'
      ''
      '  Memo1.Height        := <VARF_BLQ_AltuDestin>;'
      '  Memo1.Left          := <VARF_BLQ_MEsqDestin>;'
      '  Memo1.Width         := <VARF_BLQ_LargDestin>;'
      '  Memo1.Top           := <VARF_BLQ_TopoDestin>;'
      ''
      '  Memo2.Height        := <VARF_BLQ_AltuAvisoV>;'
      '  Memo2.Left          := <VARF_BLQ_MEsqAvisoV>;'
      '  Memo2.Width         := <VARF_BLQ_LargAvisoV>;'
      'end.')
    OnGetValue = frxVersoGetValue
    Left = 75
    Top = 31
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = DModG.frxDsEndereco2
        DataSetName = 'frxDsEndereco2'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 83.149660000000000000
        Width = 793.701300000000000000
        RowCount = 1
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Top = 37.795300000000000000
          Width = 718.110700000000000000
          Height = 45.354360000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]'
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Width = 718.110700000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_AVISOVERSO]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
      end
    end
  end
  object frxBloqA: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 42473.752164606480000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo147OnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  Txt: String;                                '
      'begin'
      '  if <frxDsBoletosIts."FatNum"> = 0 then'
      '    Txt := <frxDsBoletosIts."Texto">'
      '  else                               '
      
        '    Txt := <frxDsBoletosIts."Texto"> + '#39' '#39' + FormatFloat('#39'0'#39', <f' +
        'rxDsBoletosIts."FatNum">);              '
      '  //    '
      
        '  if (<VAR_InfNFseArreIts> = 1) and (<frxDsBoletosIts."NotaFisca' +
        'l"> <> 0) then'
      
        '    Memo60.Text := Txt + '#39' NF: '#39' + FormatFloat('#39'0'#39', <frxDsBoleto' +
        'sIts."NotaFiscal">) + '#39' S'#233'rie: '#39' + <frxDsBoletosIts."SerieNF">'
      '  else'
      
        '    Memo60.Text := Txt;                                         ' +
        '                '
      'end;'
      '  '
      'begin  '
      '  if <LogoFilialExiste> = True then'
      '    Picture1.LoadFromFile(<LogoFilialPath>);'
      '  '
      '  if <LogoBancoExiste> = True then'
      '  begin'
      
        '    Picture_Bco2.LoadFromFile(<LogoBancoPath>);                 ' +
        '                                                            '
      '    MeLogo2.Visible := False;'
      '  end else begin           '
      '    MeLogo2.Visible := True;'
      '  end;'
      '    '
      '  MeHomologacao.Visible := <VARF_STATUS>;'
      '  MeVia2.Visible        := <VAR_MOSTRA_VIA2>;              '
      'end.')
    OnGetValue = frxBloqE1GetValue
    Left = 103
    Top = 31
    Datasets = <
      item
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = DModG.frxDsEndereco2
        DataSetName = 'frxDsEndereco2'
      end>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Width = 0.100000000000000000
      object Line13: TfrxLineView
        Left = 3.779530000000000000
        Top = 721.110700000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Style = fsDash
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Picture_Bco2: TfrxPictureView
        Left = 52.913420000000000000
        Top = 721.890230000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo101: TfrxMemoView
        Left = 602.834645670000000000
        Top = 842.835190000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo85: TfrxMemoView
        Left = 602.834645670000000000
        Top = 763.465060000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object BarCode1: TfrxBarCodeView
        Left = 90.708661420000000000
        Top = 1062.047644410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Rotation = 0
        ShowText = False
        TestLine = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Line14: TfrxLineView
        Left = 52.913420000000000000
        Top = 763.465060000000000000
        Width = 687.874015750000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 52.913420000000000000
        Top = 795.590949060000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 52.913420000000000000
        Top = 820.158010000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 52.913420000000000000
        Top = 842.835190000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 226.771653540000000000
        Top = 740.787880000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 294.803152050000000000
        Top = 740.787880000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 602.834645670000000000
        Top = 763.465060000000000000
        Height = 230.551330000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 430.866420000000000000
        Top = 820.158010000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 370.393940000000000000
        Top = 820.158010000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line23: TfrxLineView
        Left = 291.023810000000000000
        Top = 820.158010000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 228.661417320000000000
        Top = 842.835190000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 160.629921260000000000
        Top = 820.158010000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 52.913420000000000000
        Top = 869.291900000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line27: TfrxLineView
        Left = 52.913420000000000000
        Top = 994.016145910000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line28: TfrxLineView
        Left = 52.913420000000000000
        Top = 1054.488586850000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo82: TfrxMemoView
        Left = 226.771800000000000000
        Top = 740.787880000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo83: TfrxMemoView
        Left = 294.803340000000000000
        Top = 740.787880000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]  ')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo84: TfrxMemoView
        Left = 52.913420000000000000
        Top = 763.465060000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo86: TfrxMemoView
        Left = 52.913420000000000000
        Top = 797.480830000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        Left = 52.913420000000000000
        Top = 820.158010000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo89: TfrxMemoView
        Left = 52.913420000000000000
        Top = 842.835190000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo90: TfrxMemoView
        Left = 166.299320000000000000
        Top = 820.158010000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo91: TfrxMemoView
        Left = 294.803340000000000000
        Top = 820.158010000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo92: TfrxMemoView
        Left = 374.173470000000000000
        Top = 820.158010000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo93: TfrxMemoView
        Left = 434.645950000000000000
        Top = 820.158010000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo94: TfrxMemoView
        Left = 166.299320000000000000
        Top = 842.835190000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo95: TfrxMemoView
        Left = 230.551330000000000000
        Top = 842.835190000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo96: TfrxMemoView
        Left = 294.803340000000000000
        Top = 842.835190000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo97: TfrxMemoView
        Left = 434.645950000000000000
        Top = 842.835190000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo98: TfrxMemoView
        Left = 427.086890000000000000
        Top = 852.283864880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Fill.BackColor = clWhite
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo99: TfrxMemoView
        Left = 604.724800000000000000
        Top = 797.480830000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo100: TfrxMemoView
        Left = 604.724800000000000000
        Top = 820.158010000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo102: TfrxMemoView
        Left = 604.724800000000000000
        Top = 869.291900000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo103: TfrxMemoView
        Left = 604.724800000000000000
        Top = 895.748610000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo104: TfrxMemoView
        Left = 604.724800000000000000
        Top = 922.205320000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo105: TfrxMemoView
        Left = 604.724800000000000000
        Top = 944.882500000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo106: TfrxMemoView
        Left = 604.724800000000000000
        Top = 971.339210000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line29: TfrxLineView
        Left = 602.834645670000000000
        Top = 893.858665590000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line30: TfrxLineView
        Left = 602.834645670000000000
        Top = 918.425594720000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line31: TfrxLineView
        Left = 602.834645670000000000
        Top = 942.992523860000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line32: TfrxLineView
        Left = 602.834645670000000000
        Top = 967.559452990000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo109: TfrxMemoView
        Left = 604.724800000000000000
        Top = 1058.268400000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo110: TfrxMemoView
        Left = 604.724800000000000000
        Top = 1039.370750000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo111: TfrxMemoView
        Left = 52.913420000000000000
        Top = 870.047644410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object MeLogo2: TfrxMemoView
        Left = 52.913420000000000000
        Top = 733.228820000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo107: TfrxMemoView
        Left = 170.078850000000000000
        Top = 994.016390000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo126: TfrxMemoView
        Left = 226.771800000000000000
        Top = 994.016390000000000000
        Width = 374.173470000000000000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo1: TfrxMemoView
        Left = 56.692950000000000000
        Top = 903.307655350000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_03]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo2: TfrxMemoView
        Left = 56.692950000000000000
        Top = 880.630490000000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_01]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo10: TfrxMemoView
        Left = 56.692950000000000000
        Top = 891.969072670000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_02]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo24: TfrxMemoView
        Left = 56.692950000000000000
        Top = 914.646238030000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_04]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo25: TfrxMemoView
        Left = 56.692950000000000000
        Top = 925.984823150000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_05]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo26: TfrxMemoView
        Left = 56.692950000000000000
        Top = 937.323403380000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_06]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo27: TfrxMemoView
        Left = 56.692950000000000000
        Top = 948.661986060000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_07]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo28: TfrxMemoView
        Left = 56.692950000000000000
        Top = 960.000568740000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_08]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo29: TfrxMemoView
        Left = 56.692950000000000000
        Top = 971.339502910000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_09]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo30: TfrxMemoView
        Left = 56.692950000000000000
        Top = 982.678085590000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_10]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo31: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1020.473100000000000000
        Width = 430.866420000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."E_ALL"]')
        ParentFont = False
      end
      object Memo32: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1005.354980000000000000
        Width = 430.866420000000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."NOME_ENT"] :: [frxDsEndereco."CNPJ_CPF"]')
        ParentFont = False
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 37.795275590000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object MeHomologacao: TfrxMemoView
          Tag = 2
          Left = 52.913420000000000000
          Top = 22.677180000000000000
          Width = 691.653990000000000000
          Height = 676.535870000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -64
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Boleto '
            'em '
            'homologa'#231#227'o')
          ParentFont = False
          Rotation = 45
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 52.913420000000000000
          Top = 23.433070870000000000
          Width = 691.653543310000000000
          Height = 14.362204720000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"] :: [frxDsEndereco."CNPJ_CPF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 52.913420000000000000
          Top = 6.803149606299212000
          Width = 702.992133310000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'RECIBO DO SACADO')
          ParentFont = False
          VAlign = vaBottom
        end
        object Shape2: TfrxShapeView
          Left = 52.913420000000000000
          Top = 37.795300000000000000
          Width = 230.551185980000000000
          Height = 665.196850390000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape1: TfrxShapeView
          Left = 514.016080000000000000
          Top = 37.795324410000000000
          Width = 230.551181100000000000
          Height = 665.196850390000000000
          Frame.Width = 0.100000000000000000
        end
        object MeVia2: TfrxMemoView
          Left = 52.913420000000000000
          Top = 37.795300000000000000
          Width = 691.653543310000000000
          Height = 14.362204720000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold, fsItalic]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_TXT_VIA2]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxShapeView1: TfrxShapeView
          Left = 283.464750000000000000
          Top = 37.795324410000000000
          Width = 230.551185980000000000
          Height = 665.196850390000000000
          Frame.Width = 0.100000000000000000
        end
      end
      object Memo149: TfrxMemoView
        Left = 514.016080000000000000
        Top = 646.299654410000000000
        Width = 75.590600000000000000
        Height = 15.118112680000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftRight, ftBottom]
        Frame.Width = 0.100000000000000000
        Fill.BackColor = 14803425
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo150: TfrxMemoView
        Left = 514.016080000000000000
        Top = 642.520124410000000000
        Width = 75.590600000000000000
        Height = 5.669291340000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop]
        Frame.Width = 0.100000000000000000
        Fill.BackColor = 14803425
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo151: TfrxMemoView
        Left = 589.606680000000000000
        Top = 642.520124410000000000
        Width = 52.913420000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Fill.BackColor = 14803425
        Memo.UTF8W = (
          'Valor do documento')
        ParentFont = False
        WordWrap = False
      end
      object Line34: TfrxLineView
        Left = 514.016080000000000000
        Top = 680.315378030000000000
        Width = 230.551283620000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line35: TfrxLineView
        Left = 514.016080000000000000
        Top = 661.417740240000000000
        Width = 230.551227480000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line36: TfrxLineView
        Left = 514.016080000000000000
        Top = 642.520102440000000000
        Width = 230.551227480000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo152: TfrxMemoView
        Left = 514.016080000000000000
        Top = 661.417774410000000000
        Width = 230.551330000000000000
        Height = 7.559060000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo153: TfrxMemoView
        Left = 514.016080000000000000
        Top = 680.315424410000000000
        Width = 45.354360000000000000
        Height = 7.559060000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo154: TfrxMemoView
        Left = 642.520100000000000000
        Top = 642.520124410000000000
        Width = 94.488250000000000000
        Height = 18.897642680000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Fill.BackColor = 14803425
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Valor"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo155: TfrxMemoView
        Left = 514.016080000000000000
        Top = 668.976834410000000000
        Width = 222.992270000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."AgContaCed"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo156: TfrxMemoView
        Left = 514.016080000000000000
        Top = 687.874484410000000000
        Width = 222.992270000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo161: TfrxMemoView
        Left = 737.008350000000000000
        Top = 642.520124410000000000
        Width = 7.559060000000000000
        Height = 18.897642680000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Fill.BackColor = 14803425
        HAlign = haRight
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo11: TfrxMemoView
        Left = 52.913420000000000000
        Top = 706.772134410000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'www.dermatek.net.br  - Software customizado')
        ParentFont = False
      end
      object Memo144: TfrxMemoView
        Left = 491.338900000000000000
        Top = 706.772134410000000000
        Width = 86.929190000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Modelo A')
        ParentFont = False
      end
      object Memo81: TfrxMemoView
        Left = 744.567410000000000000
        Top = 589.606704410000000000
        Width = 15.118120000000000000
        Height = 120.944960000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'AUTENTICA'#199#195'O MEC'#194'NICA')
        ParentFont = False
        Rotation = 90
      end
      object Memo157: TfrxMemoView
        Left = 514.016080000000000000
        Top = 529.354694410001000000
        Width = 230.551181100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_MENSAGEM_1]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo158: TfrxMemoView
        Left = 514.016080000000000000
        Top = 540.693277080001000000
        Width = 230.551181100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_MENSAGEM_2]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo159: TfrxMemoView
        Left = 514.016080000000000000
        Top = 552.031859760001000000
        Width = 230.551181100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_MENSAGEM_3]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo160: TfrxMemoView
        Left = 514.016080000000000000
        Top = 563.370442440001000000
        Width = 230.551181100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_MENSAGEM_4]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo167: TfrxMemoView
        Left = 514.016080000000000000
        Top = 574.709025120001000000
        Width = 230.551181100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_MENSAGEM_5]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo168: TfrxMemoView
        Left = 514.016080000000000000
        Top = 586.047607790001000000
        Width = 230.551181100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_MENSAGEM_6]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo169: TfrxMemoView
        Left = 514.016080000000000000
        Top = 597.386190470001000000
        Width = 230.551181100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_MENSAGEM_7]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo170: TfrxMemoView
        Left = 514.016080000000000000
        Top = 608.724773150001000000
        Width = 230.551330000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_MENSAGEM_8]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo171: TfrxMemoView
        Left = 514.016080000000000000
        Top = 620.063355830001000000
        Width = 230.551330000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_MENSAGEM_9]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo172: TfrxMemoView
        Left = 514.016080000000000000
        Top = 631.401938500001000000
        Width = 230.551330000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VARF_MENSAGEM_10]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo118: TfrxMemoView
        Left = 294.803340000000000000
        Top = 829.228746770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieDoc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo119: TfrxMemoView
        Left = 374.173470000000000000
        Top = 829.228746770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."ACEITETIT_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo120: TfrxMemoView
        Left = 434.645950000000000000
        Top = 829.228746770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."DataCad"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo124: TfrxMemoView
        Left = 604.724800000000000000
        Top = 829.228746770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo114: TfrxMemoView
        Left = 602.834645670000000000
        Top = 851.905912130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Valor"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo121: TfrxMemoView
        Left = 604.724800000000000000
        Top = 805.039890000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."AgContaCed"]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo113: TfrxMemoView
        Left = 602.834645670000000000
        Top = 772.535833390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo117: TfrxMemoView
        Left = 162.519790000000000000
        Top = 829.228746770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_NUMDOC]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo122: TfrxMemoView
        Left = 166.299320000000000000
        Top = 851.905912130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."CART_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo123: TfrxMemoView
        Left = 230.551330000000000000
        Top = 851.905912130000000000
        Width = 56.692950000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieTxt"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo116: TfrxMemoView
        Left = 56.692950000000000000
        Top = 829.228746770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."DataCad"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo115: TfrxMemoView
        Left = 56.692950000000000000
        Top = 805.039890000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo88: TfrxMemoView
        Left = 56.692950000000000000
        Top = 774.803650000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo6: TfrxMemoView
        Left = 56.692950000000000000
        Top = 851.905511810000000000
        Width = 102.047310000000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NPrinBc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo7: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1043.150280000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacador / avalista:')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo8: TfrxMemoView
        Left = 226.771800000000000000
        Top = 1043.150280000000000000
        Width = 374.173470000000000000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."SacAvaNome"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      Columns = 2
      ColumnWidth = 65.000000000000000000
      ColumnPositions.Strings = (
        '0'
        '61'
        '122')
      PrintOnPreviousPage = True
      object Header3: TfrxHeader
        FillType = ftBrush
        Height = 136.062992130000000000
        Top = 18.897650000000000000
        Width = 245.669450000000000000
        object Memo145: TfrxMemoView
          Left = 52.913420000000000000
          Top = 109.606299210000000000
          Width = 230.551330000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14803425
          HAlign = haCenter
          Memo.UTF8W = (
            'COMPOSI'#199#195'O DA ARRECADA'#199#195'O - [VARF_MESANOA] ')
          ParentFont = False
          WordWrap = False
        end
        object Memo146: TfrxMemoView
          Left = 52.913420000000000000
          Top = 122.834645670000000000
          Width = 230.551188430000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Style = fsDot
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBoletos."NOMEENT"]')
          ParentFont = False
          WordWrap = False
        end
        object Picture1: TfrxPictureView
          Left = 56.692913390000000000
          Top = 3.779527559999998000
          Width = 188.976434090000000000
          Height = 102.047310000000000000
          Center = True
          Frame.Width = 0.500000000000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object MDBoletosIts: TfrxMasterData
        FillType = ftBrush
        Height = 10.204724410000000000
        Top = 177.637910000000000000
        Width = 245.669450000000000000
        RowCount = 0
        Stretched = True
        object Memo147: TfrxMemoView
          Left = 52.913420000000000000
          Width = 181.417440000000000000
          Height = 10.204724410000000000
          OnBeforePrint = 'Memo147OnBeforePrint'
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo148: TfrxMemoView
          Left = 234.330860000000000000
          Width = 45.354335590000000000
          Height = 10.204724410000000000
          OnBeforePrint = 'Memo148OnBeforePrint'
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletosIts."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 211.653680000000000000
        Width = 245.669450000000000000
        object Line1: TfrxLineView
          Left = 55.913420000000000000
          Width = 222.992270000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo12: TfrxMemoView
          Left = 52.913420000000000000
          Width = 226.771800000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletos."Valor"]')
          ParentFont = False
        end
      end
    end
  end
  object frxBloqB: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 42473.756203449080000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo60OnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  Txt: String;                                '
      'begin'
      '  Txt := '#39#39';'
      '  //        '
      '  if <frxDsBoletosIts."FatNum"> = 0 then'
      '    Txt := <frxDsBoletosIts."Texto">'
      '  else                               '
      
        '    Txt := <frxDsBoletosIts."Texto"> + '#39' '#39' + FormatFloat('#39'0'#39', <f' +
        'rxDsBoletosIts."FatNum">);              '
      '  //    '
      
        '  if (<VAR_InfNFseArreIts> = 1) and (<frxDsBoletosIts."NotaFisca' +
        'l"> <> 0) then'
      
        '    Memo60.Text := Txt + '#39' NF: '#39' + FormatFloat('#39'0'#39', <frxDsBoleto' +
        'sIts."NotaFiscal">) + '#39' S'#233'rie: '#39' + <frxDsBoletosIts."SerieNF">'
      '  else'
      '    Memo60.Text := Txt;                   '
      'end;'
      ''
      'begin'
      '  if <LogoFilialExiste> = True then'
      '    Picture3.LoadFromFile(<LogoFilialPath>);'
      '  '
      '  if <LogoBancoExiste> = True then'
      '  begin'
      '    Picture1.LoadFromFile(<LogoBancoPath>);'
      
        '    Picture2.LoadFromFile(<LogoBancoPath>);                     ' +
        '                                                        '
      '    MeLogo1.Visible := False;'
      '    MeLogo2.Visible := False;'
      '  end else begin           '
      '    MeLogo1.Visible := True;'
      '    MeLogo2.Visible := True;'
      '  end;      '
      ''
      '  MeHomologacao.Visible := <VARF_STATUS>;'
      '  MeVia2.Visible        := <VAR_MOSTRA_VIA2>;              '
      'end.'
      '    ')
    OnGetValue = frxBloqE1GetValue
    Left = 131
    Top = 31
    Datasets = <
      item
        DataSet = FmBloGeren.frxDsBoletos
        DataSetName = 'frxDsBoletos'
      end
      item
        DataSet = FmBloGeren.frxDsBoletosIts
        DataSetName = 'frxDsBoletosIts'
      end
      item
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = DModG.frxDsEndereco2
        DataSetName = 'frxDsEndereco2'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Width = 0.100000000000000000
      object Picture1: TfrxPictureView
        Left = 52.913420000000000000
        Top = 718.110700000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo101: TfrxMemoView
        Left = 602.834645670000000000
        Top = 839.055660000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo85: TfrxMemoView
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line13: TfrxLineView
        Left = 3.779530000000000000
        Top = 718.110700000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Style = fsDash
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo81: TfrxMemoView
        Left = 578.268090000000000000
        Top = 706.772110000000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'AUTENTICA'#199#195'O MEC'#194'NICA')
        ParentFont = False
      end
      object BarCode1: TfrxBarCodeView
        Left = 90.708661417322800000
        Top = 1058.268114410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Rotation = 0
        ShowText = False
        TestLine = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Line14: TfrxLineView
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 687.874015750000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 52.913420000000000000
        Top = 791.811419060000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 226.771653540000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 294.803152050000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Height = 230.551330000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 430.866420000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 370.393940000000000000
        Top = 816.378480000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line23: TfrxLineView
        Left = 291.023810000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 228.661417320000000000
        Top = 839.055660000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 160.629921260000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 52.913420000000000000
        Top = 865.512370000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line27: TfrxLineView
        Left = 52.913420000000000000
        Top = 990.236615910000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line28: TfrxLineView
        Left = 52.913420000000000000
        Top = 1050.709056850000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo82: TfrxMemoView
        Left = 226.771800000000000000
        Top = 737.008350000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo83: TfrxMemoView
        Left = 294.803340000000000000
        Top = 737.008350000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]  ')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo84: TfrxMemoView
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo86: TfrxMemoView
        Left = 52.913420000000000000
        Top = 793.701300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo89: TfrxMemoView
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo90: TfrxMemoView
        Left = 166.299320000000000000
        Top = 816.378480000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo91: TfrxMemoView
        Left = 294.803340000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo92: TfrxMemoView
        Left = 374.173470000000000000
        Top = 816.378480000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo93: TfrxMemoView
        Left = 434.645950000000000000
        Top = 816.378480000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo94: TfrxMemoView
        Left = 166.299320000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo95: TfrxMemoView
        Left = 230.551330000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo96: TfrxMemoView
        Left = 294.803340000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo97: TfrxMemoView
        Left = 434.645950000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo98: TfrxMemoView
        Left = 427.086890000000000000
        Top = 848.504334880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Fill.BackColor = clWhite
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo99: TfrxMemoView
        Left = 604.724800000000000000
        Top = 793.701300000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo100: TfrxMemoView
        Left = 604.724800000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo102: TfrxMemoView
        Left = 604.724800000000000000
        Top = 865.512370000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo103: TfrxMemoView
        Left = 604.724800000000000000
        Top = 891.969080000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo104: TfrxMemoView
        Left = 604.724800000000000000
        Top = 918.425790000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo105: TfrxMemoView
        Left = 604.724800000000000000
        Top = 941.102970000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo106: TfrxMemoView
        Left = 604.724800000000000000
        Top = 967.559680000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line29: TfrxLineView
        Left = 602.834645670000000000
        Top = 890.079135590000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line30: TfrxLineView
        Left = 602.834645670000000000
        Top = 914.646064720000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line31: TfrxLineView
        Left = 602.834645670000000000
        Top = 939.212993860000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line32: TfrxLineView
        Left = 602.834645670000000000
        Top = 963.779922990000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo109: TfrxMemoView
        Left = 604.724800000000000000
        Top = 1054.488870000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo110: TfrxMemoView
        Left = 604.724800000000000000
        Top = 1035.591220000000000000
        Width = 56.692950000000010000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo111: TfrxMemoView
        Left = 52.913420000000000000
        Top = 866.268114410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object MeLogo1: TfrxMemoView
        Left = 52.913420000000000000
        Top = 729.449290000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo_049: TfrxMemoView
        Left = 170.078850000000000000
        Top = 990.236860000000000000
        Width = 56.692950000000010000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo34: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1014.803149610000000000
        Width = 430.866420000000000000
        Height = 24.566929130000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."E_ALL"]')
        ParentFont = False
      end
      object Memo56: TfrxMemoView
        Left = 170.078850000000000000
        Top = 999.685039370078700000
        Width = 430.866420000000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."NOME_ENT"] :: [frxDsEndereco."CNPJ_CPF"]')
        ParentFont = False
        WordWrap = False
      end
      object Picture2: TfrxPictureView
        Left = 56.692950000000000000
        Top = 37.795300000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo10: TfrxMemoView
        Left = 230.551330000000000000
        Top = 56.692950000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo54: TfrxMemoView
        Left = 298.582870000000000000
        Top = 56.692950000000010000
        Width = 442.205010000000000000
        Height = 18.897650000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]  ')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object MeLogo2: TfrxMemoView
        Left = 56.692950000000000000
        Top = 49.133890000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo57: TfrxMemoView
        Left = 566.929133860000000000
        Top = 79.370130000000000000
        Width = 173.858253070000000000
        Height = 9.448818899999999000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          'Motivos de n'#227'o entrega (Para uso da empresa entregadora)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo58: TfrxMemoView
        Left = 566.929133860000000000
        Top = 88.818897640000000000
        Width = 56.692913390000000000
        Height = 58.582677170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '(   ) Ausente'
          '(   ) Falecido'
          '(   ) Recusado'
          '(   ) Mudou-se')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo59: TfrxMemoView
        Left = 623.622047240000000000
        Top = 88.818897640000000000
        Width = 117.165351890000000000
        Height = 58.582677170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '(   ) Endere'#231'o insuficiente'
          '(   ) N'#227'o existe o n'#186' indicado'
          '(   ) Desconhecido'
          '(   ) Outros (especificar no verso)')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo65: TfrxMemoView
        Left = 430.866420000000000000
        Top = 79.370130000000000000
        Width = 136.062992130000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftTop]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo66: TfrxMemoView
        Left = 430.866420000000000000
        Top = 86.929190000000000000
        Width = 136.062992130000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."AgContaCed"]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo67: TfrxMemoView
        Left = 56.692950000000000000
        Top = 79.370130000000000000
        Width = 374.173228350000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftTop]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo68: TfrxMemoView
        Left = 56.692950000000000000
        Top = 86.929133860000000000
        Width = 374.173470000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo69: TfrxMemoView
        Left = 430.866420000000000000
        Top = 102.047310000000000000
        Width = 136.062992130000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo70: TfrxMemoView
        Left = 430.866420000000000000
        Top = 109.606299210000000000
        Width = 136.062992130000000000
        Height = 15.118110240000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo71: TfrxMemoView
        Left = 430.866420000000000000
        Top = 132.283464570000000000
        Width = 136.062992130000000000
        Height = 15.118110240000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Valor"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo72: TfrxMemoView
        Left = 430.866420000000000000
        Top = 124.724409450000000000
        Width = 136.062992130000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo73: TfrxMemoView
        Left = 56.692950000000000000
        Top = 102.047310000000000000
        Width = 374.173228350000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftTop]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo74: TfrxMemoView
        Left = 56.692950000000000000
        Top = 109.606313860000000000
        Width = 374.173470000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '[frxDsEndereco."NOME_ENT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo75: TfrxMemoView
        Left = 292.913385830000000000
        Top = 132.283464570000000000
        Width = 137.952755910000000000
        Height = 15.118110240000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo76: TfrxMemoView
        Left = 292.913385830000000000
        Top = 124.724490000000000000
        Width = 137.952755910000000000
        Height = 7.559055120000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo77: TfrxMemoView
        Left = 164.409448820000000000
        Top = 124.724490000000000000
        Width = 128.504020000000000000
        Height = 7.559055120000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo78: TfrxMemoView
        Left = 164.409448820000000000
        Top = 132.283464570000000000
        Width = 128.504020000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_NUMDOC]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo79: TfrxMemoView
        Left = 56.692950000000000000
        Top = 124.724490000000000000
        Width = 107.716535433071000000
        Height = 7.559055120000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo80: TfrxMemoView
        Left = 56.692950000000010000
        Top = 132.283464570000000000
        Width = 107.716535430000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieTxt"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo107: TfrxMemoView
        Left = 56.692950000000000000
        Top = 147.401670000000000000
        Width = 107.716535430000000000
        Height = 22.677165350000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          'Recebi(emos) o boleto/t'#237'tulo'
          'com as caracter'#237'sticas acima')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo108: TfrxMemoView
        Left = 164.409448820000000000
        Top = 147.401670000000000000
        Width = 128.504020000000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Data')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo112: TfrxMemoView
        Left = 164.409448820000000000
        Top = 154.960644570000000000
        Width = 128.504020000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo125: TfrxMemoView
        Left = 292.913385830000000000
        Top = 147.401670000000000000
        Width = 274.015748031496000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Assinatura')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo126: TfrxMemoView
        Left = 292.913385830000000000
        Top = 154.960644570000000000
        Width = 274.015748031496000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo135: TfrxMemoView
        Left = 566.929499999999900000
        Top = 147.401670000000000000
        Width = 173.858380000000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Nome')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo136: TfrxMemoView
        Left = 566.929499999999900000
        Top = 154.960644570000000000
        Width = 173.858380000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line41: TfrxLineView
        Left = 3.779530000000000000
        Top = 173.858380000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Style = fsDash
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo149: TfrxMemoView
        Left = 532.913730000000000000
        Top = 600.945270000000000000
        Width = 94.488250000000000000
        Height = 18.897637800000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftRight, ftBottom]
        Frame.Width = 0.100000000000000000
        Fill.BackColor = 14803425
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo150: TfrxMemoView
        Left = 532.913730000000000000
        Top = 593.386210000000000000
        Width = 94.488188980000000000
        Height = 7.559055120000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop]
        Frame.Width = 0.100000000000000000
        Fill.BackColor = 14803425
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo151: TfrxMemoView
        Left = 627.401980000000000000
        Top = 593.386210000000000000
        Width = 113.385900000000000000
        Height = 7.559055120000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop]
        Frame.Width = 0.500000000000000000
        Fill.BackColor = 14803425
        Memo.UTF8W = (
          'Valor do documento')
        ParentFont = False
        WordWrap = False
      end
      object Memo154: TfrxMemoView
        Left = 627.401980000000000000
        Top = 600.945270000000000000
        Width = 113.385826770000000000
        Height = 18.897642680000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        Fill.BackColor = 14803425
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Valor"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Picture3: TfrxPictureView
        Left = 56.692950000000000000
        Top = 181.417322830000000000
        Width = 188.976434090000000000
        Height = 102.047310000000000000
        Center = True
        Frame.Width = 0.500000000000000000
        HightQuality = True
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo1: TfrxMemoView
        Left = 249.448980000000000000
        Top = 219.212598430000000000
        Width = 491.338900000000000000
        Height = 34.015755350000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco2."E_ALL"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo2: TfrxMemoView
        Left = 249.448980000000000000
        Top = 181.417440000000000000
        Width = 491.338900000000000000
        Height = 37.795275590000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco2."NOME_ENT"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo3: TfrxMemoView
        Left = 249.448980000000000000
        Top = 253.228510000000000000
        Width = 491.338900000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '[VARF_TELEFONE_CLIINT]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo4: TfrxMemoView
        Left = 249.448980000000000000
        Top = 268.346630000000000000
        Width = 491.338900000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '[VARF_SITE_CLIINT]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo157: TfrxMemoView
        Left = 64.252010000000000000
        Top = 593.386210000000000000
        Width = 461.102511100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        Frame.Typ = [ftRight, ftTop]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '[VARF_MENSAGEM_1]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo158: TfrxMemoView
        Left = 64.252010000000000000
        Top = 604.724792670000000000
        Width = 461.102511100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[VARF_MENSAGEM_2]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo159: TfrxMemoView
        Left = 64.252010000000000000
        Top = 616.063375350000000000
        Width = 461.102511100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[VARF_MENSAGEM_3]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo160: TfrxMemoView
        Left = 64.252010000000000000
        Top = 627.401958030000000000
        Width = 461.102511100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[VARF_MENSAGEM_4]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo167: TfrxMemoView
        Left = 64.252010000000000000
        Top = 638.740540710000000000
        Width = 461.102511100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[VARF_MENSAGEM_5]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo168: TfrxMemoView
        Left = 64.252010000000000000
        Top = 650.079123380000000000
        Width = 461.102511100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[VARF_MENSAGEM_6]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo169: TfrxMemoView
        Left = 64.252010000000000000
        Top = 661.417706060000000000
        Width = 461.102511100000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[VARF_MENSAGEM_7]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo170: TfrxMemoView
        Left = 64.252010000000000000
        Top = 672.756288740000000000
        Width = 461.102660000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[VARF_MENSAGEM_8]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo171: TfrxMemoView
        Left = 64.252010000000000000
        Top = 684.094871420000000000
        Width = 461.102660000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[VARF_MENSAGEM_9]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo172: TfrxMemoView
        Left = 64.252010000000000000
        Top = 695.433454090000000000
        Width = 461.102660000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        Frame.Typ = [ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '[VARF_MENSAGEM_10]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo5: TfrxMemoView
        Left = 52.913420000000000000
        Top = 593.386210000000000000
        Width = 11.338582680000000000
        Height = 113.385892680000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Frame.Typ = [ftRight]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          'Avisos')
        ParentFont = False
        Rotation = 90
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo6: TfrxMemoView
        Left = 532.913730000000000000
        Top = 653.858690000000000000
        Width = 207.874062130000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo7: TfrxMemoView
        Left = 532.913730000000000000
        Top = 661.417679210000000000
        Width = 207.874062130000000000
        Height = 22.677165350000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = [ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo8: TfrxMemoView
        Left = 532.913730000000000000
        Top = 619.842920000000000000
        Width = 207.874062130000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo9: TfrxMemoView
        Left = 532.913730000000000000
        Top = 627.401980000000000000
        Width = 207.874062130000000000
        Height = 26.456692910000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = [ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."AgContaCed"]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo11: TfrxMemoView
        Left = 532.913730000000000000
        Top = 684.094930000000000000
        Width = 207.874150000000000000
        Height = 7.559055120000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo12: TfrxMemoView
        Left = 532.913730000000000000
        Top = 691.653904570000000000
        Width = 207.874150000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NUMDOC]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        Left = 52.913420000000000000
        Top = 706.772110000000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'www.dermatek.net.br  - Software customizado')
        ParentFont = False
      end
      object Memo144: TfrxMemoView
        Left = 491.338900000000000000
        Top = 706.772110000000000000
        Width = 86.929190000000000000
        Height = 11.338590000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Modelo B')
        ParentFont = False
      end
      object Memo24: TfrxMemoView
        Left = 56.692950000000000000
        Top = 899.528125350000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_03]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo25: TfrxMemoView
        Left = 56.692950000000010000
        Top = 876.850960000000000000
        Width = 544.252320000000100000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_01]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo26: TfrxMemoView
        Left = 56.692950000000000000
        Top = 888.189542670000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_02]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo27: TfrxMemoView
        Left = 56.692950000000010000
        Top = 910.866708030000000000
        Width = 544.252320000000100000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_04]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo28: TfrxMemoView
        Left = 56.692950000000000000
        Top = 922.205293150000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_05]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo29: TfrxMemoView
        Left = 56.692950000000000000
        Top = 933.543873380000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_06]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo30: TfrxMemoView
        Left = 56.692950000000000000
        Top = 944.882456060000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_07]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo31: TfrxMemoView
        Left = 56.692950000000000000
        Top = 956.221038740000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_08]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo32: TfrxMemoView
        Left = 56.692950000000000000
        Top = 967.559972910000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_09]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo33: TfrxMemoView
        Left = 56.692950000000000000
        Top = 978.898555590000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_10]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo16: TfrxMemoView
        Left = 56.692950000000010000
        Top = 848.125984250000000000
        Width = 102.047310000000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NPrinBc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo122: TfrxMemoView
        Left = 166.299320000000000000
        Top = 848.126382130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."CART_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo123: TfrxMemoView
        Left = 230.551330000000000000
        Top = 848.126382130000000000
        Width = 56.692950000000010000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieTxt"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo120: TfrxMemoView
        Left = 434.645950000000000000
        Top = 825.449216770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."DataCad"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo124: TfrxMemoView
        Left = 604.724800000000000000
        Top = 825.449216770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo114: TfrxMemoView
        Left = 602.834645670000000000
        Top = 848.126382130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Valor"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo121: TfrxMemoView
        Left = 604.724800000000000000
        Top = 801.260360000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."AgContaCed"]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo113: TfrxMemoView
        Left = 602.834645670000000000
        Top = 768.756303390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo119: TfrxMemoView
        Left = 374.173470000000000000
        Top = 825.449216770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."ACEITETIT_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo118: TfrxMemoView
        Left = 294.803340000000000000
        Top = 825.449216770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieDoc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo117: TfrxMemoView
        Left = 162.519790000000000000
        Top = 825.449216770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_NUMDOC]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo116: TfrxMemoView
        Left = 56.692950000000010000
        Top = 825.449216770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."DataCad"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo115: TfrxMemoView
        Left = 56.692950000000010000
        Top = 801.260360000000000000
        Width = 544.252320000000100000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo88: TfrxMemoView
        Left = 56.692950000000010000
        Top = 771.024120000000000000
        Width = 544.252320000000100000
        Height = 18.897650000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1039.370750000000000000
        Width = 56.692950000000010000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacador / avalista:')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo18: TfrxMemoView
        Left = 226.771800000000000000
        Top = 1039.370750000000000000
        Width = 374.173470000000000000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."SacAvaNome"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object MeVia2: TfrxMemoView
        Left = 64.252010000000000000
        Top = 579.023622047244000000
        Width = 676.535423310000000000
        Height = 14.362204720000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold, fsItalic]
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[VAR_TXT_VIA2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object MeHomologacao: TfrxMemoView
        Tag = 2
        Left = 56.692950000000010000
        Top = 37.795300000000000000
        Width = 684.094930000000000000
        Height = 680.315400000000000000
        Visible = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15000804
        Font.Height = -64
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'Boleto '
          'em '
          'homologa'#231#227'o')
        ParentFont = False
        Rotation = 45
        VAlign = vaCenter
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 154.000000000000000000
      PaperSize = 256
      Columns = 2
      ColumnWidth = 105.000000000000000000
      ColumnPositions.Strings = (
        '0'
        '105')
      LargeDesignHeight = True
      PrintOnPreviousPage = True
      object MDBoletosIts: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 362.834880000000000000
        Width = 396.850650000000000000
        DataSet = FmBloGeren.frxDsBoletosIts
        DataSetName = 'frxDsBoletosIts'
        RowCount = 0
        Stretched = True
        object Memo60: TfrxMemoView
          Left = 56.692950000000010000
          Width = 279.685049130000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'Memo60OnBeforePrint'
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 336.378170000000000000
          Width = 66.141732280000000000
          Height = 15.118110240000000000
          StretchMode = smMaxHeight
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletosIts."Valor"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 321.260040240000000000
        Top = 18.897650000000000000
        Width = 396.850650000000000000
        object Memo62: TfrxMemoView
          Left = 56.692913390000000000
          Top = 291.023622050000000000
          Width = 345.826771650000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'COMPOSI'#199#195'O DA ARRECADA'#199#195'O')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          Left = 56.692950000000010000
          Top = 306.141930000000000000
          Width = 345.826771650000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Style = fsDot
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsBoletos."NOMEENT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 400.630180000000000000
        Width = 396.850650000000000000
        object Memo64: TfrxMemoView
          Left = 56.692950000000010000
          Width = 345.826952280000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBoletos."Valor"]')
          ParentFont = False
        end
      end
    end
  end
  object frxBloqE1: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 41591.755609803240000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoBancoExiste> = True then'
      '  begin'
      '    Picture_Bco1.LoadFromFile(<LogoBancoPath>);'
      
        '    Picture_Bco2.LoadFromFile(<LogoBancoPath>);                 ' +
        '                                                            '
      
        '    Picture_Bco3.LoadFromFile(<LogoBancoPath>);                 ' +
        '                                                            '
      '    MeLogo1.Visible := False;'
      '    MeLogo2.Visible := False;'
      '    MeLogo3.Visible := False;'
      '  end else begin           '
      '    MeLogo1.Visible := True;'
      '    MeLogo2.Visible := True;'
      '    MeLogo3.Visible := True;'
      '  end;'
      '  MeHomologacao.Visible := <VARF_STATUS>;       '
      '  MeVia2.Visible        := <VAR_MOSTRA_VIA2>;              '
      'end.')
    OnGetValue = frxBloqE1GetValue
    Left = 159
    Top = 31
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = DModG.frxDsEndereco2
        DataSetName = 'frxDsEndereco2'
      end
      item
      end>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -64
      Font.Name = 'Arial'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Width = 0.100000000000000000
      object Picture_Bco2: TfrxPictureView
        Left = 52.913420000000000000
        Top = 718.110700000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo101: TfrxMemoView
        Left = 602.834645670000000000
        Top = 839.055660000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo85: TfrxMemoView
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line13: TfrxLineView
        Left = 3.779530000000000000
        Top = 718.110700000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Style = fsDash
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo81: TfrxMemoView
        Left = 578.268090000000000000
        Top = 706.772110000000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'AUTENTICA'#199#195'O MEC'#194'NICA')
        ParentFont = False
      end
      object BarCode1: TfrxBarCodeView
        Left = 90.708661420000000000
        Top = 1058.268114410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Rotation = 0
        ShowText = False
        TestLine = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Line14: TfrxLineView
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 687.874015750000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 52.913420000000000000
        Top = 791.811419060000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 226.771653540000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 294.803152050000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Height = 230.551330000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 430.866420000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 370.393940000000000000
        Top = 816.378480000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line23: TfrxLineView
        Left = 291.023810000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 228.661417320000000000
        Top = 839.055660000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 160.629921260000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 52.913420000000000000
        Top = 865.512370000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line27: TfrxLineView
        Left = 52.913420000000000000
        Top = 990.236615910000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line28: TfrxLineView
        Left = 52.913420000000000000
        Top = 1050.709056850000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo82: TfrxMemoView
        Left = 226.771800000000000000
        Top = 737.008350000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo83: TfrxMemoView
        Left = 294.803340000000000000
        Top = 737.008350000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]  ')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo84: TfrxMemoView
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo86: TfrxMemoView
        Left = 52.913420000000000000
        Top = 793.701300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo88: TfrxMemoView
        Left = 56.692950000000000000
        Top = 771.024120000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo89: TfrxMemoView
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo90: TfrxMemoView
        Left = 166.299320000000000000
        Top = 816.378480000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo91: TfrxMemoView
        Left = 294.803340000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo92: TfrxMemoView
        Left = 374.173470000000000000
        Top = 816.378480000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo93: TfrxMemoView
        Left = 434.645950000000000000
        Top = 816.378480000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo94: TfrxMemoView
        Left = 166.299320000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo95: TfrxMemoView
        Left = 230.551330000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo96: TfrxMemoView
        Left = 294.803340000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo97: TfrxMemoView
        Left = 434.645950000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo98: TfrxMemoView
        Left = 427.086890000000000000
        Top = 848.504334880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Fill.BackColor = clWhite
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo99: TfrxMemoView
        Left = 604.724800000000000000
        Top = 793.701300000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo100: TfrxMemoView
        Left = 604.724800000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo102: TfrxMemoView
        Left = 604.724800000000000000
        Top = 865.512370000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo103: TfrxMemoView
        Left = 604.724800000000000000
        Top = 891.969080000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo104: TfrxMemoView
        Left = 604.724800000000000000
        Top = 918.425790000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo105: TfrxMemoView
        Left = 604.724800000000000000
        Top = 941.102970000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo106: TfrxMemoView
        Left = 604.724800000000000000
        Top = 967.559680000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line29: TfrxLineView
        Left = 602.834645670000000000
        Top = 890.079135590000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line30: TfrxLineView
        Left = 602.834645670000000000
        Top = 914.646064720000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line31: TfrxLineView
        Left = 602.834645670000000000
        Top = 939.212993860000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line32: TfrxLineView
        Left = 602.834645670000000000
        Top = 963.779922990000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo109: TfrxMemoView
        Left = 604.724800000000000000
        Top = 1054.488870000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo110: TfrxMemoView
        Left = 604.724800000000000000
        Top = 1035.591220000000000000
        Width = 56.692950000000010000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo111: TfrxMemoView
        Left = 52.913420000000000000
        Top = 866.268114410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo115: TfrxMemoView
        Left = 56.692950000000000000
        Top = 801.260360000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object MeLogo2: TfrxMemoView
        Left = 52.913420000000000000
        Top = 729.449290000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo107: TfrxMemoView
        Left = 170.078850000000000000
        Top = 990.236860000000000000
        Width = 56.692950000000010000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo126: TfrxMemoView
        Left = 226.771800000000000000
        Top = 990.236860000000000000
        Width = 374.173470000000000000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo32: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1016.693570000000000000
        Width = 430.866420000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."E_ALL"]')
        ParentFont = False
      end
      object Memo34: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1001.575450000000000000
        Width = 430.866420000000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."NOME_ENT"] :: [frxDsEndereco."CNPJ_CPF"]')
        ParentFont = False
      end
      object Memo1: TfrxMemoView
        Left = 55.913420000000000000
        Top = 899.528125350000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_03]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo46: TfrxMemoView
        Left = 55.913420000000000000
        Top = 876.850960000000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_01]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo47: TfrxMemoView
        Left = 55.913420000000000000
        Top = 888.189542670000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_02]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo48: TfrxMemoView
        Left = 55.913420000000000000
        Top = 910.866708030000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_04]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo49: TfrxMemoView
        Left = 55.913420000000000000
        Top = 922.205293150000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_05]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo50: TfrxMemoView
        Left = 55.913420000000000000
        Top = 933.543873380000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_06]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo51: TfrxMemoView
        Left = 55.913420000000000000
        Top = 944.882456060000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_07]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo52: TfrxMemoView
        Left = 55.913420000000000000
        Top = 956.221038740000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_08]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo10: TfrxMemoView
        Left = 55.913420000000000000
        Top = 967.559972910000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_09]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo56: TfrxMemoView
        Left = 55.913420000000000000
        Top = 978.898555590000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_10]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Picture_Bco1: TfrxPictureView
        Left = 245.669450000000000000
        Top = 578.268090000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        Visible = False
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object MeLogo1: TfrxMemoView
        Left = 245.669450000000000000
        Top = 589.606680000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        Visible = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        Left = 52.913420000000000000
        Top = 706.772110000000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'www.dermatek.net.br  - Software customizado')
        ParentFont = False
      end
      object Memo144: TfrxMemoView
        Left = 491.338900000000000000
        Top = 706.772110000000000000
        Width = 86.929190000000000000
        Height = 11.338590000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Modelo E - 1 via')
        ParentFont = False
      end
      object Picture_Bco3: TfrxPictureView
        Left = 56.692950000000000000
        Top = 578.268090000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        Visible = False
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object MeLogo3: TfrxMemoView
        Left = 56.692950000000000000
        Top = 589.606680000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        Visible = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo113: TfrxMemoView
        Left = 602.834645670000000000
        Top = 768.756303390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo121: TfrxMemoView
        Left = 604.724800000000000000
        Top = 801.260360000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."AgContaCed"]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo124: TfrxMemoView
        Left = 604.724800000000000000
        Top = 825.449216770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo114: TfrxMemoView
        Left = 602.834645670000000000
        Top = 848.126382130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Valor"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo120: TfrxMemoView
        Left = 434.645950000000000000
        Top = 825.449216770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."DataCad"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo118: TfrxMemoView
        Left = 294.803340000000000000
        Top = 825.449216770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieDoc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo119: TfrxMemoView
        Left = 374.173470000000000000
        Top = 825.449216770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."ACEITETIT_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo123: TfrxMemoView
        Left = 230.551330000000000000
        Top = 848.126382130000000000
        Width = 56.692950000000010000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieTxt"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo122: TfrxMemoView
        Left = 166.299320000000000000
        Top = 848.126382130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."CART_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo117: TfrxMemoView
        Left = 162.519790000000000000
        Top = 825.449216770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_NUMDOC]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo116: TfrxMemoView
        Left = 56.692950000000010000
        Top = 825.449216770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."DataCad"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo4: TfrxMemoView
        Left = 56.692950000000010000
        Top = 848.125984250000000000
        Width = 102.047310000000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NPrinBc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1039.370750000000000000
        Width = 56.692950000000010000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacador / avalista:')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo18: TfrxMemoView
        Left = 226.771800000000000000
        Top = 1039.370750000000000000
        Width = 374.173470000000000000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."SacAvaNome"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object MeVia2: TfrxMemoView
        Left = 52.913420000000000000
        Top = 691.653990000000000000
        Width = 687.874013310000000000
        Height = 14.362204720000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold, fsItalic]
        Frame.Width = 0.100000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[VAR_TXT_VIA2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object MeHomologacao: TfrxMemoView
        Tag = 2
        Left = 52.913420000000000000
        Top = 41.574830000000000000
        Width = 691.653990000000000000
        Height = 676.535870000000000000
        Visible = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15000804
        Font.Height = -64
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'Boleto '
          'em '
          'homologa'#231#227'o')
        ParentFont = False
        Rotation = 45
        VAlign = vaCenter
      end
    end
  end
  object frxBloqE2: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 40897.760788298610000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoBancoExiste> = True then'
      '  begin'
      '    Picture_Bco1.LoadFromFile(<LogoBancoPath>);'
      
        '    Picture_Bco2.LoadFromFile(<LogoBancoPath>);                 ' +
        '                                                            '
      
        '    Picture_Bco3.LoadFromFile(<LogoBancoPath>);                 ' +
        '                                                            '
      '    MeLogo1.Visible := False;'
      '    MeLogo2.Visible := False;'
      '    MeLogo3.Visible := False;'
      '  end else begin           '
      '    MeLogo1.Visible := True;'
      '    MeLogo2.Visible := False;'
      '    MeLogo3.Visible := False;'
      '  end;'
      '  MeHomologacao.Visible := <VARF_STATUS>;      '
      '  MeVia2.Visible        := <VAR_MOSTRA_VIA2>;              '
      'end.')
    OnGetValue = frxBloqE1GetValue
    Left = 187
    Top = 31
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = DModG.frxDsEndereco2
        DataSetName = 'frxDsEndereco2'
      end
      item
      end>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Width = 0.100000000000000000
      object Memo114: TfrxMemoView
        Left = 602.834645670000000000
        Top = 848.126382130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Valor"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Picture_Bco2: TfrxPictureView
        Left = 52.913420000000000000
        Top = 718.110700000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo113: TfrxMemoView
        Left = 602.834645670000000000
        Top = 768.756303390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo101: TfrxMemoView
        Left = 602.834645670000000000
        Top = 839.055660000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo85: TfrxMemoView
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line13: TfrxLineView
        Left = 3.779530000000000000
        Top = 718.110700000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Style = fsDash
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo81: TfrxMemoView
        Left = 578.268090000000000000
        Top = 706.772110000000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'AUTENTICA'#199#195'O MEC'#194'NICA')
        ParentFont = False
      end
      object BarCode1: TfrxBarCodeView
        Left = 90.708661417322800000
        Top = 1058.268114410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Rotation = 0
        ShowText = False
        TestLine = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Line14: TfrxLineView
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 687.874015750000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 52.913420000000000000
        Top = 791.811419060000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 226.771653540000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 294.803152050000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Height = 230.551330000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 430.866420000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 370.393940000000000000
        Top = 816.378480000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line23: TfrxLineView
        Left = 291.023810000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 228.661417320000000000
        Top = 839.055660000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 160.629921260000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 52.913420000000000000
        Top = 865.512370000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line27: TfrxLineView
        Left = 52.913420000000000000
        Top = 990.236615910000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line28: TfrxLineView
        Left = 52.913420000000000000
        Top = 1050.709056850000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo82: TfrxMemoView
        Left = 226.771800000000000000
        Top = 737.008350000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo83: TfrxMemoView
        Left = 294.803340000000000000
        Top = 737.008350000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]  ')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo84: TfrxMemoView
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo86: TfrxMemoView
        Left = 52.913420000000000000
        Top = 793.701300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo88: TfrxMemoView
        Left = 56.692950000000000000
        Top = 771.024120000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo89: TfrxMemoView
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo90: TfrxMemoView
        Left = 166.299320000000000000
        Top = 816.378480000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo91: TfrxMemoView
        Left = 294.803340000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo92: TfrxMemoView
        Left = 374.173470000000000000
        Top = 816.378480000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo93: TfrxMemoView
        Left = 434.645950000000000000
        Top = 816.378480000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo94: TfrxMemoView
        Left = 166.299320000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo95: TfrxMemoView
        Left = 230.551330000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo96: TfrxMemoView
        Left = 294.803340000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo97: TfrxMemoView
        Left = 434.645950000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo98: TfrxMemoView
        Left = 427.086890000000000000
        Top = 848.504334880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Fill.BackColor = clWhite
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo99: TfrxMemoView
        Left = 604.724800000000000000
        Top = 793.701300000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo100: TfrxMemoView
        Left = 604.724800000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo102: TfrxMemoView
        Left = 604.724800000000000000
        Top = 865.512370000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo103: TfrxMemoView
        Left = 604.724800000000000000
        Top = 891.969080000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo104: TfrxMemoView
        Left = 604.724800000000000000
        Top = 918.425790000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo105: TfrxMemoView
        Left = 604.724800000000000000
        Top = 941.102970000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo106: TfrxMemoView
        Left = 604.724800000000000000
        Top = 967.559680000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line29: TfrxLineView
        Left = 602.834645670000000000
        Top = 890.079135590000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line30: TfrxLineView
        Left = 602.834645670000000000
        Top = 914.646064720000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line31: TfrxLineView
        Left = 602.834645670000000000
        Top = 939.212993860000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line32: TfrxLineView
        Left = 602.834645670000000000
        Top = 963.779922990000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo109: TfrxMemoView
        Left = 604.724800000000000000
        Top = 1054.488870000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo110: TfrxMemoView
        Left = 604.724800000000000000
        Top = 1035.591220000000000000
        Width = 56.692950000000010000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo111: TfrxMemoView
        Left = 52.913420000000000000
        Top = 866.268114410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo115: TfrxMemoView
        Left = 56.692950000000000000
        Top = 801.260360000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo116: TfrxMemoView
        Left = 56.692950000000010000
        Top = 825.449216770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."DataCad"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo117: TfrxMemoView
        Left = 162.519790000000000000
        Top = 825.449216770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_NUMDOC]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo118: TfrxMemoView
        Left = 294.803340000000000000
        Top = 825.449216770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieDoc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo119: TfrxMemoView
        Left = 374.173470000000000000
        Top = 825.449216770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."ACEITETIT_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo120: TfrxMemoView
        Left = 434.645950000000000000
        Top = 825.449216770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."DataCad"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo122: TfrxMemoView
        Left = 166.299320000000000000
        Top = 848.126382130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."CART_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo123: TfrxMemoView
        Left = 230.551330000000000000
        Top = 848.126382130000000000
        Width = 56.692950000000010000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieTxt"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo124: TfrxMemoView
        Left = 604.724800000000000000
        Top = 825.449216770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo121: TfrxMemoView
        Left = 604.724800000000000000
        Top = 801.260360000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."AgContaCed"]')
        ParentFont = False
        VAlign = vaBottom
      end
      object MeLogo2: TfrxMemoView
        Left = 52.913420000000000000
        Top = 729.449290000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Picture_Bco1: TfrxPictureView
        Left = 52.913420000000000000
        Top = 294.803340000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo3: TfrxMemoView
        Left = 602.834645670000000000
        Top = 345.448943390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo4: TfrxMemoView
        Left = 602.834645670000000000
        Top = 415.748300000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo5: TfrxMemoView
        Left = 602.834645670000000000
        Top = 336.378170000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line1: TfrxLineView
        Left = 3.779530000000000000
        Top = 294.803340000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo6: TfrxMemoView
        Left = 578.268090000000000000
        Top = 279.685220000000000000
        Width = 162.519790000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          'SEGUNDA VIA')
        ParentFont = False
      end
      object Line2: TfrxLineView
        Left = 52.913420000000000000
        Top = 336.378170000000000000
        Width = 687.874015750000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line3: TfrxLineView
        Left = 52.913420000000000000
        Top = 368.504059060000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line4: TfrxLineView
        Left = 52.913420000000000000
        Top = 393.071120000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line5: TfrxLineView
        Left = 52.913420000000000000
        Top = 415.748300000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line6: TfrxLineView
        Left = 226.771653540000000000
        Top = 313.700990000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line7: TfrxLineView
        Left = 294.803152050000000000
        Top = 313.700990000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line8: TfrxLineView
        Left = 602.834645670000000000
        Top = 336.378170000000000000
        Height = 230.551330000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line9: TfrxLineView
        Left = 430.866420000000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line10: TfrxLineView
        Left = 370.393940000000000000
        Top = 393.071120000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line11: TfrxLineView
        Left = 291.023810000000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line12: TfrxLineView
        Left = 228.661417320000000000
        Top = 415.748300000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line33: TfrxLineView
        Left = 160.629921260000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line34: TfrxLineView
        Left = 52.913420000000000000
        Top = 442.205010000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line35: TfrxLineView
        Left = 52.913420000000000000
        Top = 566.929255910000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line36: TfrxLineView
        Left = 52.913420000000000000
        Top = 627.401696850000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo7: TfrxMemoView
        Left = 226.771800000000000000
        Top = 313.700990000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo8: TfrxMemoView
        Left = 294.803340000000000000
        Top = 313.700990000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]  ')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo9: TfrxMemoView
        Left = 52.913420000000000000
        Top = 336.378170000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo11: TfrxMemoView
        Left = 52.913420000000000000
        Top = 370.393940000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo12: TfrxMemoView
        Left = 52.913420000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        Left = 56.692950000000000000
        Top = 347.716760000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo14: TfrxMemoView
        Left = 52.913420000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo15: TfrxMemoView
        Left = 166.299320000000000000
        Top = 393.071120000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo16: TfrxMemoView
        Left = 294.803340000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        Left = 374.173470000000000000
        Top = 393.071120000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo18: TfrxMemoView
        Left = 434.645950000000000000
        Top = 393.071120000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo19: TfrxMemoView
        Left = 166.299320000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo20: TfrxMemoView
        Left = 230.551330000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo21: TfrxMemoView
        Left = 294.803340000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo22: TfrxMemoView
        Left = 434.645950000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo23: TfrxMemoView
        Left = 427.086890000000000000
        Top = 425.196974880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Fill.BackColor = clWhite
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo24: TfrxMemoView
        Left = 604.724800000000000000
        Top = 370.393940000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo25: TfrxMemoView
        Left = 604.724800000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo26: TfrxMemoView
        Left = 604.724800000000000000
        Top = 442.205010000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo27: TfrxMemoView
        Left = 604.724800000000000000
        Top = 468.661720000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo28: TfrxMemoView
        Left = 604.724800000000000000
        Top = 495.118430000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo29: TfrxMemoView
        Left = 604.724800000000000000
        Top = 517.795610000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo30: TfrxMemoView
        Left = 604.724800000000000000
        Top = 544.252320000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line37: TfrxLineView
        Left = 602.834645670000000000
        Top = 466.771775590000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line38: TfrxLineView
        Left = 602.834645670000000000
        Top = 491.338704720000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line39: TfrxLineView
        Left = 602.834645670000000000
        Top = 515.905633860000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line40: TfrxLineView
        Left = 602.834645670000000000
        Top = 540.472562990000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo31: TfrxMemoView
        Left = 170.078850000000000000
        Top = 566.929500000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo33: TfrxMemoView
        Left = 604.724800000000000000
        Top = 631.181510000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo34: TfrxMemoView
        Left = 604.724800000000000000
        Top = 612.283860000000000000
        Width = 56.692950000000010000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo35: TfrxMemoView
        Left = 52.913420000000000000
        Top = 442.960754410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo36: TfrxMemoView
        Left = 56.692950000000000000
        Top = 377.953000000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo53: TfrxMemoView
        Left = 604.724800000000000000
        Top = 377.953000000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."AgContaCed"]')
        ParentFont = False
        VAlign = vaBottom
      end
      object MeLogo1: TfrxMemoView
        Left = 52.913420000000000000
        Top = 306.141930000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo58: TfrxMemoView
        Left = 52.913420000000000000
        Top = 181.417440000000000000
        Width = 687.874460000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          'Boleto em duas vias')
        ParentFont = False
      end
      object Memo107: TfrxMemoView
        Left = 170.078850000000000000
        Top = 990.236860000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo45: TfrxMemoView
        Left = 226.771800000000000000
        Top = 566.929499999999900000
        Width = 374.173470000000000000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo126: TfrxMemoView
        Left = 226.771800000000000000
        Top = 990.236860000000000000
        Width = 374.173470000000000000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo1: TfrxMemoView
        Left = 56.913420000000000000
        Top = 476.220765350000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_03]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo46: TfrxMemoView
        Left = 56.913420000000000000
        Top = 453.543600000000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_01]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo47: TfrxMemoView
        Left = 56.913420000000000000
        Top = 464.882182670000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_02]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo48: TfrxMemoView
        Left = 56.913420000000000000
        Top = 487.559348030000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_04]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo49: TfrxMemoView
        Left = 56.913420000000000000
        Top = 498.897933150000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_05]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo50: TfrxMemoView
        Left = 56.913420000000000000
        Top = 510.236513380000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_06]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo51: TfrxMemoView
        Left = 56.913420000000000000
        Top = 521.575096060000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_07]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo52: TfrxMemoView
        Left = 56.913420000000000000
        Top = 532.913678740000000000
        Width = 544.252320000000100000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_08]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo61: TfrxMemoView
        Left = 56.913420000000000000
        Top = 544.252612910000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_09]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo62: TfrxMemoView
        Left = 56.913420000000000000
        Top = 555.591195590000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_10]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo32: TfrxMemoView
        Left = 170.078850000000000000
        Top = 593.386210000000000000
        Width = 430.866420000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."E_ALL"]')
        ParentFont = False
      end
      object Memo55: TfrxMemoView
        Left = 170.078850000000000000
        Top = 578.268090000000000000
        Width = 430.866420000000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."NOME_ENT"] :: [frxDsEndereco."CNPJ_CPF"]')
        ParentFont = False
      end
      object Memo56: TfrxMemoView
        Left = 56.692950000000000000
        Top = 899.528125350000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_03]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo57: TfrxMemoView
        Left = 56.692950000000000000
        Top = 876.850960000000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_01]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo59: TfrxMemoView
        Left = 56.692950000000000000
        Top = 888.189542670000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_02]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo63: TfrxMemoView
        Left = 56.692950000000000000
        Top = 910.866708030000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_04]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo64: TfrxMemoView
        Left = 56.692950000000000000
        Top = 922.205293150000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_05]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo65: TfrxMemoView
        Left = 56.692950000000000000
        Top = 933.543873380000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_06]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo66: TfrxMemoView
        Left = 56.692950000000000000
        Top = 944.882456060000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_07]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo67: TfrxMemoView
        Left = 56.692950000000000000
        Top = 956.221038740000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_08]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo68: TfrxMemoView
        Left = 56.692950000000000000
        Top = 967.559972910000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_09]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo69: TfrxMemoView
        Left = 56.692950000000000000
        Top = 978.898555590000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_10]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo10: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1016.693570000000000000
        Width = 430.866420000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."E_ALL"]')
        ParentFont = False
      end
      object Memo60: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1001.575450000000000000
        Width = 430.866420000000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."NOME_ENT"] :: [frxDsEndereco."CNPJ_CPF"]')
        ParentFont = False
      end
      object Picture_Bco3: TfrxPictureView
        Left = 56.692950000000000000
        Top = 37.795300000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo54: TfrxMemoView
        Left = 230.551330000000000000
        Top = 56.692950000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo70: TfrxMemoView
        Left = 298.582870000000000000
        Top = 56.692950000000010000
        Width = 442.205010000000000000
        Height = 18.897650000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]  ')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object MeLogo3: TfrxMemoView
        Left = 56.692950000000000000
        Top = 49.133890000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo72: TfrxMemoView
        Left = 566.929133860000000000
        Top = 79.370130000000000000
        Width = 173.858253070000000000
        Height = 9.448818899999999000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          'Motivos de n'#227'o entrega (Para uso da empresa entregadora)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo73: TfrxMemoView
        Left = 566.929133860000000000
        Top = 88.818897640000000000
        Width = 56.692913390000000000
        Height = 58.582677170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '(   ) Ausente'
          '(   ) Falecido'
          '(   ) Recusado'
          '(   ) Mudou-se')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo74: TfrxMemoView
        Left = 623.622047240000000000
        Top = 88.818897640000000000
        Width = 117.165351890000000000
        Height = 58.582677170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '(   ) Endere'#231'o insuficiente'
          '(   ) N'#227'o existe o n'#186' indicado'
          '(   ) Desconhecido'
          '(   ) Outros (especificar no verso)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo75: TfrxMemoView
        Left = 430.866420000000000000
        Top = 79.370130000000000000
        Width = 136.062992130000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft, ftTop]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo76: TfrxMemoView
        Left = 430.866420000000000000
        Top = 86.929190000000000000
        Width = 136.062992130000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."AgContaCed"]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo77: TfrxMemoView
        Left = 56.692950000000000000
        Top = 79.370130000000000000
        Width = 374.173228350000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftTop]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo78: TfrxMemoView
        Left = 56.692950000000000000
        Top = 86.929133860000000000
        Width = 374.173470000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo79: TfrxMemoView
        Left = 430.866420000000000000
        Top = 102.047310000000000000
        Width = 136.062992130000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo80: TfrxMemoView
        Left = 430.866420000000000000
        Top = 109.606299210000000000
        Width = 136.062992130000000000
        Height = 15.118110240000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo108: TfrxMemoView
        Left = 430.866420000000000000
        Top = 132.283464570000000000
        Width = 136.062992130000000000
        Height = 15.118110240000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Valor"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo112: TfrxMemoView
        Left = 430.866420000000000000
        Top = 124.724409450000000000
        Width = 136.062992130000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo125: TfrxMemoView
        Left = 56.692950000000000000
        Top = 102.047310000000000000
        Width = 374.173228350000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftTop]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo127: TfrxMemoView
        Left = 56.692950000000000000
        Top = 109.606313860000000000
        Width = 374.173470000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          '[frxDsEndereco."NOME_ENT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo128: TfrxMemoView
        Left = 292.913385830000000000
        Top = 132.283464570000000000
        Width = 137.952755910000000000
        Height = 15.118110240000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo129: TfrxMemoView
        Left = 292.913385830000000000
        Top = 124.724490000000000000
        Width = 137.952755910000000000
        Height = 7.559055120000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo130: TfrxMemoView
        Left = 164.409448820000000000
        Top = 124.724490000000000000
        Width = 128.504020000000000000
        Height = 7.559055120000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftLeft]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo131: TfrxMemoView
        Left = 164.409448820000000000
        Top = 132.283464570000000000
        Width = 128.504020000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_NUMDOC]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo132: TfrxMemoView
        Left = 56.692950000000000000
        Top = 124.724490000000000000
        Width = 107.716535430000000000
        Height = 7.559055120000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo133: TfrxMemoView
        Left = 56.692950000000010000
        Top = 132.283464570000000000
        Width = 107.716535430000000000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieTxt"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo134: TfrxMemoView
        Left = 56.692950000000000000
        Top = 147.401670000000000000
        Width = 107.716535430000000000
        Height = 22.677165350000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          'Recebi(emos) o boleto/t'#237'tulo'
          'com as caracter'#237'sticas acima')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo135: TfrxMemoView
        Left = 164.409448820000000000
        Top = 147.401670000000000000
        Width = 128.504020000000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Data')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo136: TfrxMemoView
        Left = 164.409448820000000000
        Top = 154.960644570000000000
        Width = 128.504020000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo137: TfrxMemoView
        Left = 292.913385830000000000
        Top = 147.401670000000000000
        Width = 274.015748030000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = [ftRight]
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Assinatura')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo138: TfrxMemoView
        Left = 292.913385830000000000
        Top = 154.960644570000000000
        Width = 274.015748030000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftRight, ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo139: TfrxMemoView
        Left = 566.929499999999900000
        Top = 147.401670000000000000
        Width = 173.858380000000000000
        Height = 7.559055120000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Width = 0.500000000000000000
        Memo.UTF8W = (
          'Nome')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo140: TfrxMemoView
        Left = 566.929499999999900000
        Top = 154.960644570000000000
        Width = 173.858380000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 0.500000000000000000
        HAlign = haCenter
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line41: TfrxLineView
        Left = 3.779530000000000000
        Top = 173.858380000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Style = fsDash
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo141: TfrxMemoView
        Left = 52.913420000000000000
        Top = 706.772110000000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'www.dermatek.net.br  - Software customizado')
        ParentFont = False
      end
      object Memo144: TfrxMemoView
        Left = 491.338900000000000000
        Top = 706.772110000000000000
        Width = 86.929190000000000000
        Height = 11.338590000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Modelo E - 2 vias')
        ParentFont = False
      end
      object Memo71: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1039.370750000000000000
        Width = 56.692950000000010000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacador / avalista:')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo142: TfrxMemoView
        Left = 226.771800000000000000
        Top = 1039.370750000000000000
        Width = 374.173470000000000000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."SacAvaNome"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo143: TfrxMemoView
        Left = 56.692950000000010000
        Top = 846.614720000000000000
        Width = 102.047310000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NPrinBc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo147: TfrxMemoView
        Left = 170.078850000000000000
        Top = 616.063390000000000000
        Width = 56.692950000000010000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacador / avalista:')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo148: TfrxMemoView
        Left = 226.771800000000000000
        Top = 616.063390000000000000
        Width = 374.173470000000000000
        Height = 9.070866140000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."SacAvaNome"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo149: TfrxMemoView
        Left = 56.692950000000010000
        Top = 424.818897640000000000
        Width = 102.047310000000000000
        Height = 15.118110240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."NPrinBc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo42: TfrxMemoView
        Left = 166.299320000000000000
        Top = 424.819022130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."CART_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo43: TfrxMemoView
        Left = 230.551330000000000000
        Top = 424.819022130000000000
        Width = 56.692950000000010000
        Height = 15.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieTxt"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo2: TfrxMemoView
        Left = 602.834645670000000000
        Top = 424.819022130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Valor"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo44: TfrxMemoView
        Left = 604.724800000000000000
        Top = 402.141856770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo41: TfrxMemoView
        Left = 434.645950000000000000
        Top = 402.141856770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."DataCad"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo40: TfrxMemoView
        Left = 374.173470000000000000
        Top = 402.141856770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."ACEITETIT_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo39: TfrxMemoView
        Left = 294.803340000000000000
        Top = 402.141856770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCNAB_Cfg."EspecieDoc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo38: TfrxMemoView
        Left = 162.519790000000000000
        Top = 402.141856770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_NUMDOC]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo37: TfrxMemoView
        Left = 56.692950000000010000
        Top = 402.141856770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."DataCad"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object MeVia2: TfrxMemoView
        Left = 52.913420000000000000
        Top = 200.315090000000000000
        Width = 687.874013310000000000
        Height = 14.362204720000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold, fsItalic]
        Frame.Width = 0.100000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[VAR_TXT_VIA2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object MeHomologacao: TfrxMemoView
        Tag = 2
        Left = 56.692950000000010000
        Top = 37.795300000000000000
        Width = 684.094930000000000000
        Height = 676.535870000000000000
        Visible = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15000804
        Font.Height = -64
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'Boleto '
          'em '
          'homologa'#231#227'o')
        ParentFont = False
        Rotation = 45
        VAlign = vaCenter
      end
    end
  end
  object frxBloq: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39911.688433344920000000
    ReportOptions.LastChange = 39911.688433344920000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 216
    Top = 31
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
end
