unit BloAvulso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, dmkPermissoes, UnDmkEnums, dmkRadioGroup, Variants;

type
  TFmBloAvulso = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrCNAB_cfg: TmySQLQuery;
    QrCNAB_cfgCodigo: TIntegerField;
    QrCNAB_cfgNome: TWideStringField;
    DsCNAB_cfg: TDataSource;
    Label37: TLabel;
    EdCNAB_cfg: TdmkEditCB;
    CBCNAB_cfg: TdmkDBLookupComboBox;
    SpeedButton2: TSpeedButton;
    dmkPermissoes1: TdmkPermissoes;
    RgTipo: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RgTipoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCNAB_Cfg(Tipo, Carteira: Integer);
  public
    { Public declarations }
    FTipo, FConfig, FCarteira: Integer;
  end;

  var
  FmBloAvulso: TFmBloAvulso;

implementation

uses UnMyObjects, Module, CNAB_Cfg, MyDBCheck, UMySQLModule, ModuleBloGeren,
  UnBloqGerl, UnBloqGerl_Jan, DmkDAC_PF;

{$R *.DFM}

procedure TFmBloAvulso.BtOKClick(Sender: TObject);
var
  Tipo, Config: Integer;
begin
  Tipo   := RGTipo.ItemIndex;
  Config := EdCNAB_Cfg.ValueVariant;
  //
  if MyObjects.FIC(Tipo < 0, RGTipo, 'Informe o tipo de fatura!') then Exit;
  if MyObjects.FIC(Config = 0, EdCNAB_Cfg, 'Informe a configura��o!') then Exit;
  //
  FTipo   := Tipo;
  FConfig := Config;
  //
  Close;
end;

procedure TFmBloAvulso.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloAvulso.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBloAvulso.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBCNAB_cfg.ListSource := DsCNAB_cfg;
  //
  RgTipo.Items.Clear;
  RgTipo.Items.AddStrings(UBloqGerl.ObtemListaTipoFatura());
  //
  FTipo   := -1;
  FConfig := 0;
end;

procedure TFmBloAvulso.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloAvulso.ReopenCNAB_Cfg(Tipo, Carteira: Integer);
begin
  case Tipo of
    0: //Boletos
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCNAB_cfg, Dmod.MyDB, [
        'SELECT Codigo, Nome ',
        'FROM cnab_cfg ',
        'WHERE Ativo = 1 ',
        'AND CartEmiss=' + Geral.FF0(Carteira),
        'ORDER BY Nome ',
        '']);
    end;
    1: //Fatura avulsa
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCNAB_cfg, Dmod.MyDB, [
        'SELECT Codigo, Nome ',
        'FROM faturcfg ',
        'WHERE Ativo = 1 ',
        'AND Carteira=' + Geral.FF0(Carteira),
        'ORDER BY Nome ',
        '']);
    end
    else
      QrCNAB_cfg.Close;
  end;
end;

procedure TFmBloAvulso.RgTipoClick(Sender: TObject);
var
  Tipo: Integer;
begin
  Tipo := RGTipo.ItemIndex;
  //
  ReopenCNAB_Cfg(Tipo, FCarteira);
  //
  EdCNAB_Cfg.ValueVariant := 0;
  CBCNAB_Cfg.KeyValue     := Null;
end;

procedure TFmBloAvulso.SpeedButton2Click(Sender: TObject);
var
  Tipo, CNAB_Cfg: Integer;
begin
  VAR_CADASTRO := 0;
  Tipo         := RGTipo.ItemIndex;
  CNAB_Cfg     := EdCNAB_Cfg.ValueVariant;
  //
  case Tipo of
    0:
      UBloqGerl_Jan.CadastroCNAB_Cfg(CNAB_Cfg);
    1:
      UBloqGerl_Jan.MostraFaturCfg(CNAB_Cfg);
    else
    begin
      Geral.MB_Aviso('Tipo de fatura n�o informada!');
      Exit;
    end;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    ReopenCNAB_Cfg(Tipo, FCarteira);
    //
    UMyMod.SetaCodigoPesquisado(EdCNAB_Cfg, CBCNAB_Cfg, QrCNAB_cfg, VAR_CADASTRO);
    CBCNAB_Cfg.SetFocus;
  end;
end;

end.
