unit BloArrItsRnw;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, dmkDBGrid,
  mySQLDbTables;

type
  TFmBloArrItsRnw = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBGBloArreIts: TdmkDBGridZTO;
    QrBloArreIts: TmySQLQuery;
    QrBloArreItsNOMEENT: TWideStringField;
    QrBloArreItsCodigo: TIntegerField;
    QrBloArreItsControle: TIntegerField;
    QrBloArreItsEntidade: TIntegerField;
    QrBloArreItsValor: TFloatField;
    QrBloArreItsSitCobr: TIntegerField;
    QrBloArreItsParcelas: TIntegerField;
    QrBloArreItsParcPerI: TIntegerField;
    QrBloArreItsParcPerF: TIntegerField;
    QrBloArreItsInfoParc: TSmallintField;
    QrBloArreItsArredonda: TFloatField;
    QrBloArreItsLk: TIntegerField;
    QrBloArreItsDataCad: TDateField;
    QrBloArreItsDataAlt: TDateField;
    QrBloArreItsUserCad: TIntegerField;
    QrBloArreItsUserAlt: TIntegerField;
    QrBloArreItsAlterWeb: TSmallintField;
    QrBloArreItsAtivo: TSmallintField;
    QrBloArreItsValorInf: TSmallintField;
    QrBloArreItsFINAL: TWideStringField;
    QrBloArreItsINICIO: TWideStringField;
    QrBloArreItsCnab_Cfg: TIntegerField;
    QrBloArreItsDiaVencto: TIntegerField;
    QrBloArreItsEXPIROU: TBooleanField;
    QrBloArreItsEXPIRAEM: TWideStringField;
    QrBloArreItsEmpresa: TIntegerField;
    QrBloArreItsNOMEEMP: TWideStringField;
    QrBloArreItsFilial: TIntegerField;
    QrBloArreItsContrato: TIntegerField;
    QrBloArreItsValor_TXT: TFloatField;
    QrBloArreItsDVencimento: TDateField;
    QrBloArreItsDtaPrxRenw: TDateField;
    QrBloArreItsSitCobr_TXT: TWideStringField;
    QrBloArreItsNOMECFG: TWideStringField;
    QrBloArreItsDiaVcto_TXT: TFloatField;
    QrBloArreItsParc_Calc: TIntegerField;
    QrBloArreItsTexto: TWideStringField;
    QrBloArreItsInfoParc_TXT: TWideStringField;
    QrBloArreItsFANENT: TWideStringField;
    QrBloArreItsDOCENT: TWideStringField;
    DsBloArreIts: TDataSource;
    LaTotal: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGBloArreItsDblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrBloArreItsAfterScroll(DataSet: TDataSet);
    procedure QrBloArreItsBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure MostraBloArre();
  public
    { Public declarations }
    FHoje: TDateTime;
    procedure ReopenBloArreIts(Hoje: TDateTime);
  end;

  var
  FmBloArrItsRnw: TFmBloArrItsRnw;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF,
  {$IfNDef SNoti} UnitNotificacoesEdit, {$EndIf}
  UnBloquetos;

{$R *.DFM}

procedure TFmBloArrItsRnw.BtOKClick(Sender: TObject);
begin
  MostraBloArre();
end;

procedure TFmBloArrItsRnw.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloArrItsRnw.MostraBloArre();
var
  Codigo, Controle: Integer;
begin
  Codigo   := QrBloArreItsCodigo.Value;
  Controle := QrBloArreItsControle.Value;
  //
  UBloquetos.MostraBloArre(Codigo, Controle);
  //
  if FHoje <> 0 then
    ReopenBloArreIts(FHoje);
end;

procedure TFmBloArrItsRnw.QrBloArreItsAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrBloArreIts.RecordCount)
end;

procedure TFmBloArrItsRnw.QrBloArreItsBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmBloArrItsRnw.DBGBloArreItsDblClick(Sender: TObject);
begin
  MostraBloArre();
end;

procedure TFmBloArrItsRnw.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBloArrItsRnw.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FHoje := 0;
end;

procedure TFmBloArrItsRnw.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloArrItsRnw.ReopenBloArreIts(Hoje: TDateTime);
var
  Periodo, DataFutura: String;
begin
  DataFutura :=  Geral.FDT(Hoje + 30, 1);
  Periodo := Geral.FF0(Geral.Periodo2000(Hoje + 30));
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBloArreIts, Dmod.MyDB, [
    'SELECT  ',
    'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
    'IF(ent.Tipo=0, ent.Fantasia, ent.Apelido) FANENT, ',
    'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCENT, ',
    'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP, ',
    'cfg.Nome NOMECFG,  ',
    'CASE its.SitCobr  ',
    'WHEN 0 THEN "N�o cobrar"  ',
    'WHEN 1 THEN "Cont�nua (Anual)"  ',
    'WHEN 2 THEN "Programada"  ',
    'WHEN 3 THEN "Contrato" END SitCobr_TXT,  ',
    'enb.Filial, IF(its.InfoParc = 0, "N�o", "Sim") InfoParc_TXT, ',
    'IF(its.Contrato<>0, con.ddMesVcto, its.DiaVencto) + 0.000 DiaVcto_TXT, ',
    'IF(its.Contrato<>0, con.ValorMes, its.Valor) Valor_TXT, ',
    'con.DVencimento, con.DtaPrxRenw, its.* ',
    'FROM bloarreits its ',
    'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade ',
    'LEFT JOIN entidades enb ON enb.Codigo = its.Empresa ',
    'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = its.CNAB_Cfg ',
    'LEFT JOIN contratos con ON con.Codigo = its.Contrato ',
    'WHERE its.Ativo = 1 ',
    'AND (',
    '    (   ',
    '         its.SitCobr = 2 ', // Programado
    '         AND ',
    '         its.ParcPerI <= ' + Periodo,
    '         AND ',
    '         its.ParcPerF <= ' + Periodo,
    '    )',
    '    OR',
    '    (',
    '        its.SitCobr = 3',   // Contrato
    '        AND',
    '        (  ',
    '        con.DtaPrxRenw BETWEEN "1900-01-01"  ',
    '        AND "' + DataFutura + '" ',
    '        ) ',
    '    )',
    ')',
    '']);
  {$IfNDef SNoti}
  if QrBloArreIts.RecordCount > 0 then
    UnNotificacoesEdit.CriaNotificacaoPublica(Dmod.MyDB, nftBlqVenAVen,
      DmodG.ObtemAgora(True))
  else
    UnNotificacoesEdit.ExcluiNotificacao(Dmod.MyDB, Integer(nftBlqVenAVen));
  {$EndIf}
end;

end.
