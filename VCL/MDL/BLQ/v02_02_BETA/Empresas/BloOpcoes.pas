unit BloOpcoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkValUsu, DB, mySQLDbTables,
  dmkGeral, ComCtrls, dmkRadioGroup, dmkImage, dmkPermissoes, UnDmkEnums,
  dmkCheckBox, UnDmkProcFunc;

type
  TFmBloOpcoes = class(TForm)
    QrEntiTipCto: TmySQLQuery;
    QrEntiTipCtoCodigo: TIntegerField;
    QrEntiTipCtoCodUsu: TIntegerField;
    QrEntiTipCtoNome: TWideStringField;
    DsEntiTipCto: TDataSource;
    VUEntiTipCto: TdmkValUsu;
    QrBloOpcoes: TmySQLQuery;
    DsBloOpcoes: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    RGModelBloq: TdmkRadioGroup;
    RGCompe: TdmkRadioGroup;
    RGBloqFV: TdmkRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    RGPerGerBol: TRadioGroup;
    RGAbreEmTab: TRadioGroup;
    RGVisPadrao: TRadioGroup;
    dmkPermissoes1: TdmkPermissoes;
    RGNumDocPad: TRadioGroup;
    Label3: TLabel;
    RGVctoFeriado: TRadioGroup;
    Label4: TLabel;
    EdPreMailReaj: TdmkEditCB;
    CBPreMailReaj: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrPreEmail: TmySQLQuery;
    IntegerField1: TIntegerField;
    DsPreEmail: TDataSource;
    QrPreEmailNome: TWideStringField;
    QrPreEmailRe: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField1: TWideStringField;
    DsPreEmailRe: TDataSource;
    CkInfNFseArreIts: TCheckBox;
    TabSheet3: TTabSheet;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    EdMaxDias: TdmkEdit;
    EdDdAutConfMail: TdmkEdit;
    Label20: TLabel;
    Label19: TLabel;
    SpeedButton2: TSpeedButton;
    CBPreMailReenv: TdmkDBLookupComboBox;
    EdPreMailReenv: TdmkEditCB;
    Label5: TLabel;
    Label98: TLabel;
    EdEntiTipCto: TdmkEditCB;
    CBEntiTipCto: TdmkDBLookupComboBox;
    SpeedButton15: TSpeedButton;
    TabSheet4: TTabSheet;
    Panel5: TPanel;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    SBWhatsApp_EntiTipCto: TSpeedButton;
    EdWhatsApp_EntiTipCto: TdmkEditCB;
    CBWhatsApp_EntiTipCto: TdmkDBLookupComboBox;
    GroupBox4: TGroupBox;
    MeWhatsApp_Msg: TMemo;
    QrEntiTipCtoWA: TmySQLQuery;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    StringField2: TWideStringField;
    DsEntiTipCtoWA: TDataSource;
    VUWhatsApp_EntiTipCto: TdmkValUsu;
    LVItens: TListView;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton15Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGModelBloqClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SBWhatsApp_EntiTipCtoClick(Sender: TObject);
    procedure LVItensDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConfirmou: Boolean;
  end;

  var
  FmBloOpcoes: TFmBloOpcoes;

implementation

uses
{$IfNDef NO_USE_EMAILDMK} PreEmail, {$EndIf}
 UnEntities, UMySQLModule, Module, UnInternalConsts, MyDBCheck, UnMyObjects,
  UnBloquetos, ModuleBloGeren, DmkDAC_PF, UnitDmkTags;

{$R *.DFM}

procedure TFmBloOpcoes.BtOKClick(Sender: TObject);
var
  Codigo, Compe, Configurado, InfNFseArreIts, EntiTipCto, WhatsApp_EntiTipCto: Integer;
  WhatsApp_Msg: String;
begin
  DBloGeren.ReopenBloOpcoes;
  //
  Codigo         := QrBloOpcoes.FieldByName('Codigo').AsInteger;
  Configurado    := 1;
  InfNFseArreIts := Geral.BoolToInt(CkInfNFseArreIts.Checked);
  EntiTipCto     := EdEntiTipCto.ValueVariant;
  //
  if EntiTipCto <> 0 then
    EntiTipCto := VUEntiTipCto.ValueVariant;  
  //
  if RGCompe.Visible then
    Compe := RGCompe.ItemIndex
  else
    Compe := 0;
  //
  if (RGModelBloq.ItemIndex = 2) and (RGCompe.Visible = True) and (Compe < 1) then
  begin
    Geral.MB_Aviso('Defina a ficha de compensação!');
    PageControl1.ActivePageIndex := 1;
    RGCompe.SetFocus;
    Exit;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'bloopcoes', False,
    ['EntiTipCto', 'DdAutConfMail', 'MaxDias', 'ModelBloq', 'Compe', 'BloqFV',
    'PerGerBol', 'AbreEmTab', 'VisPadrao', 'Configurado', 'NumDocPad',
    'VctoFeriado', 'PreMailReaj', 'PreMailReenv', 'InfNFseArreIts',
    'WhatsApp_EntiTipCto', 'WhatsApp_Msg'], ['Codigo'],
    [EntiTipCto, EdDdAutConfMail.ValueVariant,
    EdMaxDias.ValueVariant, RGModelBloq.ItemIndex, Compe, RGBloqFV.ItemIndex,
    RGPerGerBol.ItemIndex, RGAbreEmTab.ItemIndex, RGVisPadrao.ItemIndex,
    Configurado, RGNumDocPad.ItemIndex, RGVctoFeriado.ItemIndex,
    EdPreMailReaj.ValueVariant, EdPreMailReenv.ValueVariant, InfNFseArreIts,
    VUWhatsApp_EntiTipCto.ValueVariant, MeWhatsApp_Msg.Text],
    [Codigo], True) then
  begin
    UnDmkDAC_PF.AbreQueryApenas(QrBloOpcoes);
    //
    FConfirmou := True;
    Close;
  end;
end;

procedure TFmBloOpcoes.BtSaidaClick(Sender: TObject);
begin
  FConfirmou := False;
  Close;
end;

procedure TFmBloOpcoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmBloOpcoes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  //
  UBloquetos.PreencheModelosBloq(RGModelBloq);
  //
  UMyMod.AbreQuery(QrBloOpcoes, DMod.MyDB);
  UMyMod.AbreQuery(QrEntiTipCto, DMod.MyDB);
  UMyMod.AbreQuery(QrEntiTipCtoWA, DMod.MyDB);
{$IfNDef NO_USE_EMAILDMK}
  UMyMod.AbreQuery(QrPreEmail, DMod.MyDB);
  UMyMod.AbreQuery(QrPreEmailRe, DMod.MyDB);
{$EndIf}
  //
  VUEntiTipCto.ValueVariant  := QrBloOpcoes.FieldByName('EntiTipCto').AsInteger;
  EdPreMailReaj.ValueVariant := QrBloOpcoes.FieldByName('PreMailReaj').AsInteger;
  CBPreMailReaj.KeyValue     := QrBloOpcoes.FieldByName('PreMailReaj').AsInteger;
  RGPerGerBol.ItemIndex      := QrBloOpcoes.FieldByName('PerGerBol').AsInteger;
  RGVisPadrao.ItemIndex      := QrBloOpcoes.FieldByName('VisPadrao').AsInteger;
  RGAbreEmTab.ItemIndex      := QrBloOpcoes.FieldByName('AbreEmTab').AsInteger;
  RGVctoFeriado.ItemIndex    := QrBloOpcoes.FieldByName('VctoFeriado').AsInteger;
  RGNumDocPad.ItemIndex      := QrBloOpcoes.FieldByName('NumDocPad').AsInteger;
  CkInfNFseArreIts.Checked   := Geral.IntToBool(QrBloOpcoes.FieldByName('InfNFseArreIts').AsInteger);
  //
  RGModelBloq.ItemIndex := QrBloOpcoes.FieldByName('ModelBloq').AsInteger;
  RGCompe.ItemIndex     := QrBloOpcoes.FieldByName('Compe').AsInteger;
  RGBloqFV.ItemIndex    := QrBloOpcoes.FieldByName('BloqFV').AsInteger;
  //
  EdMaxDias.ValueVariant       := QrBloOpcoes.FieldByName('MaxDias').AsInteger;
  EdDdAutConfMail.ValueVariant := QrBloOpcoes.FieldByName('DdAutConfMail').AsInteger;
  EdPreMailReenv.ValueVariant  := QrBloOpcoes.FieldByName('PreMailReenv').AsInteger;
  CBPreMailReenv.KeyValue      := QrBloOpcoes.FieldByName('PreMailReenv').AsInteger;
  //
  EdWhatsApp_EntiTipCto.ValueVariant := QrBloOpcoes.FieldByName('WhatsApp_EntiTipCto').AsInteger;
  CBWhatsApp_EntiTipCto.KeyValue     := QrBloOpcoes.FieldByName('WhatsApp_EntiTipCto').AsInteger;
  MeWhatsApp_Msg.Text                := QrBloOpcoes.FieldByName('WhatsApp_Msg').AsString;
  //
  UnDmkTags.CarregaWhatsAppBolTags(LVItens);
end;

procedure TFmBloOpcoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloOpcoes.LVItensDblClick(Sender: TObject);
begin
  UnDmkTags.AdicionaTag(LVItens, MeWhatsApp_Msg);
end;

procedure TFmBloOpcoes.RGModelBloqClick(Sender: TObject);
begin
  if RGModelBloq.ItemIndex = 2 then
    RGCompe.Visible := True
  else
    RGCompe.Visible := False;
end;

procedure TFmBloOpcoes.SBWhatsApp_EntiTipCtoClick(Sender: TObject);
begin
  Entities.MostraFormMostraEntiTipCto(VUWhatsApp_EntiTipCto.ValueVariant,
    EdWhatsApp_EntiTipCto, CBWhatsApp_EntiTipCto, QrEntiTipCtoWA);
end;

procedure TFmBloOpcoes.SpeedButton15Click(Sender: TObject);
begin
  Entities.MostraFormMostraEntiTipCto(VUEntiTipCto.ValueVariant,
    EdEntiTipCto, CBEntiTipCto, QrEntiTipCto);
end;

procedure TFmBloOpcoes.SpeedButton1Click(Sender: TObject);
{$IfNDef NO_USE_EMAILDMK}
var
  PreEmail: Integer;
begin
  VAR_CADASTRO := 0;
  PreEmail     := EdPreMailReaj.ValueVariant;
  //
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    if PreEmail <> 0 then
      FmPreEmail.LocCod(PreEmail, PreEmail);
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodigoPesquisado(EdPreMailReaj, CBPreMailReaj, QrPreEmail, VAR_CADASTRO);
      EdPreMailReaj.SetFocus;
    end;
  end;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappEmail);
{$EndIf}
end;

procedure TFmBloOpcoes.SpeedButton2Click(Sender: TObject);
{$IfNDef NO_USE_EMAILDMK}
var
  PreEmail: Integer;
begin
  VAR_CADASTRO := 0;
  PreEmail     := EdPreMailReenv.ValueVariant;
  //
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    if PreEmail <> 0 then
      FmPreEmail.LocCod(PreEmail, PreEmail);
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodigoPesquisado(EdPreMailReenv, CBPreMailReenv, QrPreEmailRe, VAR_CADASTRO);
      EdPreMailReenv.SetFocus;
    end;
  end;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappEmail);
{$EndIf}
end;

end.
