unit ModuleBloGeren;

interface

uses
  SysUtils, Classes, DB, mySQLDbTables, Dialogs, Forms, Windows,
  DBGrids, frxClass, dmkGeral, frxDBSet, Controls, Variants, DmkDAC_PF,
  dmkDBGrid, ComCtrls, UnDmkProcFunc, UnDmkEnums;

type
  TDBloGeren = class(TDataModule)
    QrNIA: TmySQLQuery;
    QrNIACodigo: TIntegerField;
    QrNIANome: TWideStringField;
    QrNIAConta: TIntegerField;
    QrNIAValor: TFloatField;
    QrNIASitCobr: TIntegerField;
    QrNIAParcelas: TIntegerField;
    QrNIAParcPerI: TIntegerField;
    QrNIAParcPerF: TIntegerField;
    QrNIAInfoParc: TSmallintField;
    QrNIATexto: TWideStringField;
    QrNIAControle: TIntegerField;
    QrNIAArredonda: TFloatField;
    QrNIAEntidade: TIntegerField;
    QrNIANOMEENT: TWideStringField;
    QrNIACNAB_Cfg: TIntegerField;
    QrLocArre: TmySQLQuery;
    QrNIAValorInf: TSmallintField;
    QrPesqMB: TmySQLQuery;
    QrSumBol: TmySQLQuery;
    QrSumBolVALOR: TFloatField;
    QrSumPre: TmySQLQuery;
    QrSumPreVALOR: TFloatField;
    QrLoc: TmySQLQuery;
    QrPesqMBBoleto: TFloatField;
    QrBloOpcoes: TmySQLQuery;
    QrNIADVencimento: TDateField;
    QrNIADtaPrxRenw: TDateField;
    QrNIADiaVencto: TFloatField;
    QrCNABcfg: TmySQLQuery;
    QrCNABcfgCodigo: TIntegerField;
    QrCNABcfgNome: TWideStringField;
    DsCNABcfg: TDataSource;
    QrContas: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    QrArrePesq: TmySQLQuery;
    QrArrePesqCodigo: TIntegerField;
    QrArrePesqNome: TWideStringField;
    DsArrePesq: TDataSource;
    QrEntidades: TmySQLQuery;
    IntegerField2: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    QrBloArreItsVen: TmySQLQuery;
    QrBloArreItsVenNOMEENT: TWideStringField;
    QrBloArreItsVenCodigo: TIntegerField;
    QrBloArreItsVenControle: TIntegerField;
    QrBloArreItsVenEntidade: TIntegerField;
    QrBloArreItsVenValor: TFloatField;
    QrBloArreItsVenSitCobr: TIntegerField;
    QrBloArreItsVenParcelas: TIntegerField;
    QrBloArreItsVenParcPerI: TIntegerField;
    QrBloArreItsVenParcPerF: TIntegerField;
    QrBloArreItsVenInfoParc: TSmallintField;
    QrBloArreItsVenArredonda: TFloatField;
    QrBloArreItsVenLk: TIntegerField;
    QrBloArreItsVenDataCad: TDateField;
    QrBloArreItsVenDataAlt: TDateField;
    QrBloArreItsVenUserCad: TIntegerField;
    QrBloArreItsVenUserAlt: TIntegerField;
    QrBloArreItsVenAlterWeb: TSmallintField;
    QrBloArreItsVenAtivo: TSmallintField;
    QrBloArreItsVenValorInf: TSmallintField;
    QrBloArreItsVenFINAL: TWideStringField;
    QrBloArreItsVenINICIO: TWideStringField;
    QrBloArreItsVenCnab_Cfg: TIntegerField;
    QrBloArreItsVenDiaVencto: TIntegerField;
    QrBloArreItsVenEXPIROU: TBooleanField;
    QrBloArreItsVenEXPIRAEM: TWideStringField;
    QrBloArreItsVenEmpresa: TIntegerField;
    QrBloArreItsVenNOMEEMP: TWideStringField;
    QrBloArreItsVenFilial: TIntegerField;
    QrBloArreItsVenContrato: TIntegerField;
    QrBloArreItsVenValor_TXT: TFloatField;
    QrBloArreItsVenSitCobr_TXT: TWideStringField;
    QrBloArreItsVenNOMECFG: TWideStringField;
    QrBloArreItsVenDiaVcto_TXT: TFloatField;
    QrBloArreItsVenParc_Calc: TIntegerField;
    QrBloArreItsVenTexto: TWideStringField;
    DsBloArreItsVen: TDataSource;
    QrBloArreItsVenDVencimento: TDateField;
    QrBloArreItsVenDtaPrxRenw: TDateField;
    QrBloArreItsVenInfoParc_TXT: TWideStringField;
    QrBloArreItsVenNOMEARRECAD: TWideStringField;
    DsProtocoBol: TDataSource;
    QrArre: TmySQLQuery;
    QrArreNomeArre: TWideStringField;
    QrArreNomeEnti: TWideStringField;
    QrArreNomeContrato: TWideStringField;
    QrArreBloArre: TIntegerField;
    QrArreBloArreIts: TIntegerField;
    QrArreCodContrato: TIntegerField;
    QrArreAdiciona: TSmallintField;
    QrArreControle: TIntegerField;
    DsArre: TDataSource;
    DsArreIts: TDataSource;
    QrArreIts: TmySQLQuery;
    QrArreItsAdiciona: TSmallintField;
    QrArreItsValor: TFloatField;
    QrArreItsControle: TIntegerField;
    QrArreItsDiaVencto: TIntegerField;
    QrArreItsPeriodo: TIntegerField;
    QrArreItsJurosPerc: TFloatField;
    QrArreItsMultaPerc: TFloatField;
    QrArreItsEntidade: TIntegerField;
    QrArreItsCartEmiss: TIntegerField;
    QrArreItsConta: TIntegerField;
    QrArreItsBloArre: TIntegerField;
    QrArreItsBloArreIts: TIntegerField;
    QrArreItsCNAB_Cfg: TIntegerField;
    QrLocArreAll: TmySQLQuery;
    QrProtocoBol: TmySQLQuery;
    QrProtocoBolVencto: TDateField;
    QrProtocoBolEntidade: TIntegerField;
    QrProtocoBolValor: TFloatField;
    QrProtocoBolNOMEENT: TWideStringField;
    QrProtocoBolBoleto: TFloatField;
    QrProtocoBolCNAB_Cfg: TIntegerField;
    QrProtocoBolPROTOCOD: TIntegerField;
    QrProtocoBolPROTOCOLO: TIntegerField;
    QrProtocoBolTAREFA: TWideStringField;
    QrProtocoBolDELIVER: TWideStringField;
    QrProtocoBolLOTE: TIntegerField;
    QrProtocoBolVENCTO_PROT: TDateField;
    QrProtocoBolMULTAVAL: TFloatField;
    QrProtocoBolPREVCOD: TIntegerField;
    QrProtocoBolPeriodo: TIntegerField;
    QrProtocoBolMORADIAVAL: TFloatField;
    QrProtocoBolProtocoPak: TIntegerField;
    QrProtocoBolTipoProt: TIntegerField;
    QrProtocoBolEmail: TWideStringField;
    QrProtocoBolEMeio_ID: TFloatField;
    QrProtocoBolArreits: TIntegerField;
    QrProtocoBolDataE_Txt: TWideStringField;
    QrProtocoBolDataD_Txt: TWideStringField;
    QrProtocoBolDataE: TDateField;
    QrProtocoBolDataD: TDateTimeField;
    QrProtocoBolCodigo: TIntegerField;
    QrProtocoBolEmpresa: TIntegerField;
    QrArreCNAB_Cfg: TIntegerField;
    QrArreItsControleTmp: TIntegerField;
    QrArreItsLancto: TIntegerField;
    QrArreItsTextoTmp: TWideStringField;
    QrArreItsTEXTO: TWideStringField;
    QrArreItsGenero: TIntegerField;
    QrArreItsCodigo: TIntegerField;
    QrNIANFSeSrvCad: TIntegerField;
    QrArreItsNFSeSrvCad: TIntegerField;
    QrArreItsFatur_Cfg: TIntegerField;
    QrNIACartEmiss: TFloatField;
    QrNIAFatur_Cfg: TIntegerField;
    QrProtocoBolNOME_Cfg: TWideStringField;
    QrProtocoBolJurosPerc: TFloatField;
    QrProtocoBolMultaPerc: TFloatField;
    QrProtocoBolFatur_Cfg: TIntegerField;
    QrArreFatur_Cfg: TIntegerField;
    QrArreItsVencto: TDateField;
    QrArreNOME_Cfg: TWideStringField;
    QrArreEntidade: TIntegerField;
    QrProtocoBolNOMEENT_MAIL: TWideStringField;
    QrProtocoBolId_Contato: TFloatField;
    QrProtocoBolTelefone: TWideStringField;
    QrProtocoBolContato: TWideStringField;
    QrProtocoBolTelefone_TXT: TWideStringField;
    QrProtocoBolModalCobr: TSmallintField;
    QrProtocoBolCedBanco: TIntegerField;
    QrProtocoBolCedAgencia: TIntegerField;
    QrProtocoBolCedPosto: TIntegerField;
    QrProtocoBolCedConta: TWideStringField;
    QrProtocoBolCartNum: TWideStringField;
    QrProtocoBolIDCobranca: TWideStringField;
    QrProtocoBolCodEmprBco: TWideStringField;
    QrProtocoBolTipoCobranca: TIntegerField;
    QrProtocoBolEspecieDoc: TWideStringField;
    QrProtocoBolCNAB: TIntegerField;
    QrProtocoBolCtaCooper: TWideStringField;
    QrProtocoBolLayoutRem: TWideStringField;
    QrProtocoBolCorresBco: TIntegerField;
    QrProtocoBolCorresAge: TIntegerField;
    QrProtocoBolCedDAC_A: TWideStringField;
    QrProtocoBolCedDAC_C: TWideStringField;
    QrProtocoBolCorresCto: TWideStringField;
    QrProtocoBolCART_IMP: TWideStringField;
    QrProtocoBolOperCodi: TWideStringField;
    QrProtocoBolVENCTO_TXT: TWideStringField;
    QrProtocoBolLinhaDigitavel_TXT: TWideStringField;
    procedure QrBloArreItsVenCalcFields(DataSet: TDataSet);
    procedure QrArreAfterScroll(DataSet: TDataSet);
    procedure QrArreBeforeClose(DataSet: TDataSet);
    procedure QrProtocoBolCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    FBloArreAll: String;
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    procedure ReopenNIA(CliInt: Integer; Todos: Boolean);
    procedure ReopenBloOpcoes();
    procedure ReopenQuery(TipoProtocolo, Protocolo, ArreIts, AbaGerados: Integer;
                QueryBloquetosFiltros: String);
    procedure AtualizaSelecionadoArrPeri(BloArre: String; BloArreIts,
                Controle: Integer);
    procedure AtualizaTodosArrPeri(BloArre: String; BloArreIts: Integer;
                Todos: Boolean);
    procedure ReopenArre(Controle: Integer; BloArre: String);
    procedure ReopenArreIts(Controle: Integer);
    procedure ConfiguraArrecadacoes(CliInt: Integer; Progress: TProgressBar;
                BloArreAll: String);
    procedure ReopenArrePesq();
    function  ReopenBloArreItsVen(): Integer;
  end;

var
  DBloGeren: TDBloGeren;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses Module, UnBancos, ModuleGeral, UnGotoy, MyDBCheck, UnInternalConsts,
  UnBco_Rem, UnFinanceiro, UCreate, UMySQLModule, UnMyObjects, GetValor,
  BloGerenNew, UnBloquetos, UnBloqGerl;

{$R *.dfm}

{ TDBloGeren }

procedure TDBloGeren.AtualizaSelecionadoArrPeri(BloArre: String; BloArreIts,
  Controle: Integer);
var
  Sel: Integer;
begin
  if BloArreIts <> 0 then
  begin
    Sel := QrArreAdiciona.Value;
    if Sel = 0 then
      Sel := 1
    else
      Sel := 0;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, BloArre, False, ['Adiciona'],
      ['BloArreIts'], [Sel], [BloArreIts], False);
  end else
  if Controle <> 0 then
  begin
    Sel := QrArreItsAdiciona.Value;
    if Sel = 0 then
      Sel := 1
    else
      Sel := 0;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, BloArre, False, ['Adiciona'],
      ['Controle'], [Sel], [Controle], False);
  end;
end;

procedure TDBloGeren.AtualizaTodosArrPeri(BloArre: String; BloArreIts: Integer;
  Todos: Boolean);
var
  Sel: Integer;
begin
  Sel := Geral.BoolToInt(Todos);
  //
  if BloArreIts <> 0 then
  begin
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, BloArre, False, ['Adiciona'],
      ['BloArreIts'], [Sel], [BloArreIts], False);
    //
    ReopenArreIts(0);
  end else
  begin
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, BloArre, False, ['Adiciona'],
      [], [Sel], [], False);
    //
    ReopenArre(0, BloArre);
  end;
end;

procedure TDBloGeren.ConfiguraArrecadacoes(CliInt: Integer;
  Progress: TProgressBar; BloArreAll: String);
var
  MesI, MesF, MesDif, MesT, AnoT, a, b, c: Word;  Adiciona, Seq,
  ArreInt, Periodo, i, Controle, DiaVcto: Integer;
  Texto, Titulo: String;
  Vencto: TDateTime;
  Val1: Double;
  ResVar: Variant;
begin
  try
    Screen.Cursor := crHourGlass;
    Seq           := 0;
    Adiciona      := 0;
    MesF          := 0;
    MesI          := 0;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DELETE FROM ' + BloArreAll);
    DmodG.QrUpdPID1.ExecSQL;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + BloArreAll + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Conta=:P0, BloArre=:P1, BloArreIts=:P2, Valor=:P3, ');
    DModG.QrUpdPID1.SQL.Add('Texto=:P4, Adiciona=:P5, Seq=:P6, Entidade=:P7, ');
    DModG.QrUpdPID1.SQL.Add('NomeEnti=:P8, CartEmiss=:P9, CNAB_Cfg=:P10, ');
    DModG.QrUpdPID1.SQL.Add('Fatur_Cfg=:P11, NFSeSrvCad=:P12, DiaVencto=:P13, ');
    DModG.QrUpdPID1.SQL.Add('Vencto=:P14, Periodo=:P15, Controle=:P16');
    //
    ReopenNIA(CliInt, True);
    //
    Progress.Position := 0;
    Progress.Max      := QrNIA.RecordCount;
    // Cobran�as Sobre arrecada��es.
    while not QrNIA.Eof do
    begin
      case QrNIASitCobr.Value of
        2: //Cobran�a Programada
        begin
          MesI := QrNIAParcPerI.Value;
          MesF := QrNIAParcPerF.Value;
        end;
        3: //Contratos
        begin
          MesI := Geral.Periodo2000(QrNIADVencimento.Value);
          MesF := Geral.Periodo2000(QrNIADtaPrxRenw.Value);
        end;
      end;
      MesDif  := (MesF - MesI) + 1;
      Periodo := MesI;
      for i := 0 to MesDif - 1 do
      begin
        if UBloquetos.VerificaArrecadacao(QrNIAEntidade.Value,
          QrNIAConta.Value, QrNIACodigo.Value,
          QrNIAControle.Value, CliInt, Periodo) then
        begin
          if QrNIAValorInf.Value = 1 then
          begin
            if QrNIATexto.Value <> '' then
              Titulo := '(' + QrNIATexto.Value + ')'
            else
              Titulo := '';
            if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
              QrNIAValor.Value, 2, 0, '', '', True,
              QrNIANome.Value, 'Informe o valor: ' + Titulo, 0, ResVar)
            then
              Val1 := ResVar
            else
              Val1 := 0;
          end else
            Val1 := QrNIAValor.Value;
          //
          // Arredonda valor conforme configurado no ArreBaI
          if QrNIAArredonda.Value > 0 then
          begin
            ArreInt := Trunc((Val1 + (QrNIAArredonda.Value * 0.99)) /
              QrNIAArredonda.Value);
            Val1 := ArreInt * QrNIAArredonda.Value;
          end else Val1 := Trunc(Val1 * 100) / 100;
          //
          if Trim(QrNIATexto.Value) <> '' then
            Texto := QrNIATexto.Value
          else
            Texto := QrNIANome.Value;
          if QrNIAInfoParc.Value = 1 then
          begin
            a := 0;
            b := 0;
            c := 0;
            case QrNIASitCobr.Value of
              1: // Cobran�a Cont�nua (Anual)
              begin
                dmkPF.PeriodoDecode(Periodo, AnoT, MesT);
                a := QrNIAParcPerI.Value;
                c := QrNIAParcPerF.Value;
                if c < a then Inc(c, 12);
                b := MesT;
                if b < a then Inc(b, 12);
              end;
              2: // Cobran�a Programada
              begin
                a := QrNIAParcPerI.Value;
                b := Periodo;
                c := QrNIAParcPerF.Value;
              end;
              3: // Contrato
              begin
                a := Geral.Periodo2000(QrNIADVencimento.Value);
                b := Periodo;
                c := Geral.Periodo2000(QrNIADtaPrxRenw.Value);
              end;
            end;
            Texto := FormatFloat('00', b-a+1) + '/' + FormatFloat('00', c-a+1) +
              ' - ' + Texto;
          end;
          DiaVcto  := Trunc(QrNIADiaVencto.Value);
          Vencto   := Geral.PeriodoToDate(Periodo, DiaVcto, True);
          Vencto   := UBloquetos.AjustaVencimentoPeriodo(Periodo, Vencto);
          Controle := UMyMod.BuscaNovoCodigo_Int(DModG.QrUpdPID2, BloArreAll,
                        'Controle', [], [], stIns, 0, siPositivo, nil);
          //
          DModG.QrUpdPID1.Params[00].AsInteger := QrNIAConta.Value;
          DModG.QrUpdPID1.Params[01].AsInteger := QrNIACodigo.Value;
          DModG.QrUpdPID1.Params[02].AsInteger := QrNIAControle.Value;
          DModG.QrUpdPID1.Params[03].AsFloat   := Val1;
          DModG.QrUpdPID1.Params[04].AsString  := Texto;
          DModG.QrUpdPID1.Params[05].AsInteger := Adiciona;
          DModG.QrUpdPID1.Params[06].AsInteger := Seq;
          DModG.QrUpdPID1.Params[07].AsInteger := QrNIAEntidade.Value;
          DModG.QrUpdPID1.Params[08].AsString  := QrNIANOMEENT.Value;
          DModG.QrUpdPID1.Params[09].AsInteger := Trunc(QrNIACartEmiss.Value);
          DModG.QrUpdPID1.Params[10].AsInteger := QrNIACNAB_Cfg.Value;
          DModG.QrUpdPID1.Params[11].AsInteger := QrNIAFatur_Cfg.Value;
          DModG.QrUpdPID1.Params[12].AsInteger := QrNIANFSeSrvCad.Value;
          DModG.QrUpdPID1.Params[13].AsInteger := DiaVcto;
          DModG.QrUpdPID1.Params[14].AsString  := Geral.FDT(Vencto, 1);
          DModG.QrUpdPID1.Params[15].AsInteger := Periodo;
          DModG.QrUpdPID1.Params[16].AsInteger := Controle;
          DmodG.QrUpdPID1.ExecSQL;
        end;
        Periodo := Periodo + 1;
      end;
      Progress.Position := Progress.Position + 1;
      Progress.Update;
      Application.ProcessMessages;
      //
      QrNIA.Next;
    end;
  finally
    Progress.Position := 0;
    Screen.Cursor     := crDefault;
  end;
end;

procedure TDBloGeren.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

procedure TDBloGeren.QrArreAfterScroll(DataSet: TDataSet);
begin
  ReopenArreIts(0);
end;

procedure TDBloGeren.QrArreBeforeClose(DataSet: TDataSet);
begin
  QrArreIts.Close;
end;

procedure TDBloGeren.QrBloArreItsVenCalcFields(DataSet: TDataSet);
var
  Expirou: Boolean;
  Parcelas: Integer;
  ExpiraEm, Ini, Fim: String;
begin
  Parcelas := 0;
  Expirou  := UBloquetos.CalculaPeriodoIniFimArrecadacao(
    QrBloArreItsVenSitCobr.Value, QrBloArreItsVenParcPerI.Value,
    QrBloArreItsVenParcPerF.Value, QrBloArreItsVenParcelas.Value,
    QrBloArreItsVenDiaVcto_TXT.Value, QrBloArreItsVenDVencimento.Value,
    QrBloArreItsVenDtaPrxRenw.Value, Ini, Fim, ExpiraEm, Parcelas);
  //
  QrBloArreItsVenINICIO.Value    := Ini;
  QrBloArreItsVenFINAL.Value     := Fim;
  QrBloArreItsVenEXPIRAEM.Value  := ExpiraEm;
  QrBloArreItsVenEXPIROU.Value   := Expirou;
  QrBloArreItsVenParc_Calc.Value := Parcelas;
end;

procedure TDBloGeren.QrProtocoBolCalcFields(DataSet: TDataSet);
var
  NossoNumero, NossoNumero_Txt, CodigoDeCobranca, LinhaDigital: String;
begin
  QrProtocoBolTelefone_TXT.Value := Geral.FormataTelefone_TT_Curto(QrProtocoBolTelefone.Value);
  //
  if (QrProtocoBolCNAB_Cfg.AsInteger <> 0) and
    (QrProtocoBolBoleto.AsFloat <> 0) then
  begin
    UBancos.GeraNossoNumero(
      QrProtocoBolModalCobr.AsInteger,
      QrProtocoBolCedBanco.AsInteger,
      QrProtocoBolCedAgencia.AsInteger,
      QrProtocoBolCedPosto.AsInteger,
      QrProtocoBolBoleto.AsFloat,
      QrProtocoBolCedConta.AsString,
      QrProtocoBolCartNum.AsString,
      QrProtocoBolIDCobranca.AsString,
      Geral.SoNumero_TT(QrProtocoBolCodEmprBco.AsString),
      QrProtocoBolVencto.AsDateTime,
      QrProtocoBolTipoCobranca.AsInteger,
      QrProtocoBolEspecieDoc.AsString,
      QrProtocoBolCNAB.AsInteger,
      QrProtocoBolCtaCooper.AsString,
      QrProtocoBolLayoutRem.AsString,
      NossoNumero_Txt, NossoNumero);
    //
    CodigoDeCobranca := UBancos.CodigoDeBarra_BoletoDeCobranca(
                          QrProtocoBolCedBanco.AsInteger,
                          QrProtocoBolCedAgencia.AsInteger,
                          QrProtocoBolCorresBco.AsInteger,
                          QrProtocoBolCorresAge.AsInteger,
                          QrProtocoBolCedDAC_A.AsString,
                          QrProtocoBolCedPosto.AsInteger,
                          QrProtocoBolCedConta.AsString,
                          QrProtocoBolCedDAC_C.AsString,
                          QrProtocoBolCorresCto.AsString,
                          9, 3, 1, NossoNumero,
                          QrProtocoBolCodEmprBco.AsString,
                          QrProtocoBolCartNum.AsString,
                          QrProtocoBolCART_IMP.AsString,
                          QrProtocoBolIDCobranca.AsString,
                          QrProtocoBolOperCodi.AsString,
                          QrProtocoBolVencto.AsDateTime,
                          QrProtocoBolValor.AsFloat,
                          0, 0, not False,
                          QrProtocoBolModalCobr.AsInteger,
                          QrProtocoBolLayoutRem.AsString);
    LinhaDigital := UBancos.LinhaDigitavel_BoletoDeCobranca(CodigoDeCobranca);
    LinhaDigital := Geral.SoNumero_TT(LinhaDigital);
  end else
    LinhaDigital := '';
  //
  QrProtocoBolVENCTO_TXT.Value         := Geral.FDT(QrProtocoBolVencto.Value, 3);
  QrProtocoBolLinhaDigitavel_TXT.Value := LinhaDigital;
end;

procedure TDBloGeren.ReopenArrePesq();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrArrePesq, Dmod.MyDB, [
    'SELECT bar.Codigo, bar.Nome ',
    'FROM bloarre bar ',
    'LEFT JOIN bloarreits its  ON its.Codigo = bar.Codigo ',
    'WHERE bar.Ativo = 1 ',
    'AND its.Empresa = ' + Geral.FF0(VAR_ModBloq_EntCliInt),
    'GROUP BY bar.Codigo ',
    'ORDER BY bar.Nome ',
    '']);
end;

procedure TDBloGeren.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TDBloGeren.ReopenArre(Controle: Integer; BloArre: String);
begin
  FBloArreAll := BloArre; 
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrArre, DModG.MyPID_DB, [
    'SELECT arr.Nome NomeArre, tmp.NomeEnti, tmp.Fatur_Cfg, tmp.CNAB_Cfg, ',
{$IfNDef sCNTR}
    'con.Nome NomeContrato, con.Codigo CodContrato, ',
{$Else}
    '"" NomeContrato, "" CodContrato, ',
{$EndIF}
    'tmp.BloArre, tmp.BloArreIts, tmp.Adiciona, tmp.Controle, ',
    'IF(tmp.CNAB_Cfg<>0, cfg.Nome, fcf.Nome) NOME_Cfg, tmp.Entidade ',
    'FROM ' + BloArre + ' tmp ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.bloarre arr ON arr.Codigo = tmp.BloArre ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.bloarreits its ON its.Controle = tmp.BloArreIts ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.faturcfg fcf ON fcf.Codigo = tmp.Fatur_Cfg ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.cnab_cfg cfg ON cfg.Codigo = tmp.Cnab_Cfg ',
{$IfNDef sCNTR}
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.contratos con ON con.Codigo = its.Contrato ',
{$EndIF}
    'GROUP BY tmp.BloArreIts ',
    'ORDER BY NomeArre, NomeEnti ',
    '']);
  if Controle <> 0 then
    QrArre.Locate('Controle', Controle, []);
end;

procedure TDBloGeren.ReopenArreIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrArreIts, DModG.MyPID_DB, [
    'SELECT tmp.Adiciona, tmp.Texto TextoTmp, tmp.Valor, tmp.Controle ControleTmp, ',
    'tmp.CartEmiss, tmp.Periodo, tmp.DiaVencto, tmp.Entidade, tmp.Conta, tmp.BloArre, ',
    'tmp.BloArreIts, tmp.CNAB_Cfg, tmp.Fatur_Cfg, tmp.NFSeSrvCad, ari.Codigo, ari.Controle, ',
    'ari.Lancto, ari.Texto TEXTO, ari.Conta Genero, tmp.Vencto, ',
    'IF(ari.CNAB_Cfg<>0, cfg.JurosPerc, fcf.JurosPerc) JurosPerc, ',
    'IF(ari.CNAB_Cfg<>0, cfg.MultaPerc, fcf.MultaPerc) MultaPerc ',
    'FROM ' + FBloArreAll + ' tmp ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.arreits ari ON ari.Controle = tmp.IDArre ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.bloarreits its ON its.Controle = tmp.BloArreIts ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.faturcfg fcf ON fcf.Codigo = its.Fatur_Cfg ',
    'LEFT JOIN ' + LowerCase(TMeuDB) + '.cnab_cfg cfg ON cfg.Codigo = its.Cnab_Cfg ',
    'WHERE tmp.BloArre=' + Geral.FF0(QrArreBloArre.Value),
    'AND tmp.BloArreIts=' + Geral.FF0(QrArreBloArreIts.Value),
    'ORDER BY tmp.Texto, tmp.Valor ',
    '']);
  if Controle <> 0 then
    QrArreIts.Locate('Controle', Controle, []);
end;

function TDBloGeren.ReopenBloArreItsVen(): Integer;
var
  Periodo: Integer;
  Hoje: TDateTime;
begin
  Hoje    := DModG.ObtemAgora;
  Periodo := Geral.Periodo2000(Hoje);
  Periodo := Periodo + 1; //M�s posterior
  //
(*
  QrBloArreItsVen.Close;
  QrBloArreItsVen.Database := Dmod.MyDB;
  QrBloArreItsVen.Params[0].AsInteger  := Periodo;
  QrBloArreItsVen.Params[1].AsDateTime := Hoje;
  QrBloArreItsVen. O p e n;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrBloArreItsVen, Dmod.MyDB, [
  'SELECT IF(its.InfoParc = 0, "N�o", "Sim") InfoParc_TXT, ',
  'are.Nome NOMEARRECAD, ',
  'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
  'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP, ',
  'cfg.Nome NOMECFG, "Programada" SitCobr_TXT, enb.Filial, ',
  'its.DiaVencto + 0.000 DiaVcto_TXT, its.Valor Valor_TXT, ',
  'its.DataCad DVencimento, its.DataCad DtaPrxRenw, its.* ',
  'FROM bloarreits its ',
  'LEFT JOIN bloarre are ON are.Codigo = its.Codigo ',
  'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade ',
  'LEFT JOIN entidades enb ON enb.Codigo = its.Empresa ',
  'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = its.CNAB_Cfg ',
  'WHERE its.Contrato=0 ',
  'AND its.Ativo=1 ',
  'AND its.ParcPerF<=' + Geral.FF0(Periodo),
  'AND its.SitCobr=2 ',
  ' ',
  'UNION ',
  ' ',
  'SELECT IF(its.InfoParc = 0, "N�o", "Sim") InfoParc_TXT, ',
  'are.Nome NOMEARRECAD, ',
  'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
  'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP, ',
  'cfg.Nome NOMECFG, "Contrato" SitCobr_TXT, enb.Filial, ',
{$IfNDef sCNTR}
  'con.ddMesVcto + 0.000 DiaVcto_TXT, con.ValorMes Valor_TXT, ',
  'con.DVencimento, con.DtaPrxRenw, ',
{$Else}
  '0.000 DiaVcto_TXT ,0.000 Valor_TXT, ',
  'DATE("1899-12-30") DVencimento, DATE("1899-12-30") DtaPrxRenw,  ',
{$EndIF}
  'its.* ',
  'FROM bloarreits its ',
  'LEFT JOIN bloarre are ON are.Codigo = its.Codigo ',
  'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade ',
  'LEFT JOIN entidades enb ON enb.Codigo = its.Empresa ',
  'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = its.CNAB_Cfg ',
{$IfNDef sCNTR}
  'LEFT JOIN contratos con ON con.Codigo = its.Contrato',
{$EndIF}
  'WHERE its.Contrato<>0 ',
  'AND its.Ativo=1 ',
{$IfNDef sCNTR}
  'AND con.Ativo=1 ',
  'AND con.DtaPrxRenw<="' + Geral.FDT(Hoje, 1) + '"',
{$EndIF}
  'AND its.SitCobr=3 ',
  '']);
  //
  Result := QrBloArreItsVen.RecordCount;
end;

procedure TDBloGeren.ReopenBloOpcoes;
begin
  UMyMod.AbreQuery(QrBloOpcoes, Dmod.MyDB);
end;

procedure TDBloGeren.ReopenNIA(CLiInt: Integer; Todos: Boolean);
var
  SQL: String;
begin
  if Todos then
    SQL := 'AND its.SitCobr IN (2, 3)'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNIA, Dmod.MyDB, [
    'SELECT arr.Codigo, arr.Nome, arr.Conta, ',
{$IfNDef sCNTR}
    'IF(its.Contrato<>0, con.ValorMes, its.Valor) Valor, ',
    'IF(its.Contrato<>0, con.ddMesVcto, its.DiaVencto) + 0.000 DiaVencto, ',
{$else}
    '(its.Valor) Valor, ',
    '(its.DiaVencto) + 0.000 DiaVencto, ',
{$EndIf}
    'its.SitCobr, its.Parcelas, its.ParcPerI, its.ParcPerF, ',
    'its.InfoParc, its.Texto, its.Controle, its.Arredonda, its.Entidade, ',
    'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
    'its.CNAB_Cfg, its.Fatur_Cfg, its.NFSeSrvCad, its.ValorInf, ',
    'IF(its.CNAB_Cfg<>0, cna.CartEmiss, fcf.Carteira) + 0.000 CartEmiss, ',
{$IfNDef sCNTR}
    'con.DVencimento, con.DtaPrxRenw ',
{$else}
    'DATE("1899-12-30") DVencimento, DATE("1899-12-30") DtaPrxRenw ',
{$EndIf}
    'FROM bloarre arr ',
    'LEFT JOIN bloarreits its ON its.Codigo = arr.Codigo ',
    'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade ',
    'LEFT JOIN cnab_cfg cna ON cna.Codigo = its.CNAB_Cfg ',
    'LEFT JOIN faturcfg fcf ON fcf.Codigo = its.Fatur_Cfg ',
{$IfNDef sCNTR}
    'LEFT JOIN contratos con ON con.Codigo = its.Contrato ',
{$EndIf}
    'WHERE arr.Codigo > 0 ',
    'AND its.Ativo = 1 ',
    'AND its.Empresa=' + Geral.FF0(CliInt),
    SQL,
    '']);
end;

procedure TDBloGeren.ReopenQuery(TipoProtocolo, Protocolo, ArreIts, AbaGerados: Integer;
  QueryBloquetosFiltros: String);
var
  TipoEmail, TipoTel: Integer;
  SQLCompl: String;
begin
  if TipoProtocolo = 2 then //E-mail
  begin
    ReopenBloOpcoes;
    //
    TipoEmail := QrBloOpcoes.FieldByName('EntiTipCto').AsInteger;
    TipoTel   := QrBloOpcoes.FieldByName('WhatsApp_EntiTipCto').AsInteger;
    //
    if MyObjects.FIC(TipoEmail = 0, nil, 'Tipo de email para envio de boleto n�o definido nas op��es de bloquetos!') then
    begin
      QrProtocoBol.Close;
      Exit;
    end;
    //
    if AbaGerados = 1 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrProtocoBol, Dmod.MyDB, [
        'SELECT DISTINCT ari.Boleto, pre.Codigo, pre.Empresa, ari.Entidade, ',
        'ari.Vencto, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
        'IF(IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido) <> "", ',
        'IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido), ',
        'IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome)) NOMEENT_MAIL, ',
        'ptc.Codigo PROTOCOD, ppi.Controle ProtocoPak, ptc.Tipo TipoProt,  ',
        'CONCAT_WS("-", ari.Boleto, ari.Entidade) BOLENT, ',
        'SUM(ari.Valor) Valor, cna.Nome NOME_Cfg, ari.Fatur_Cfg, ari.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, enm.Conta + 0.000 EMeio_ID, enm.Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE <= "1899-12-30", "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD <= "1899-12-30", "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y %H:%i")) DataD_Txt,  ',
        'cna.JurosPerc, cna.MultaPerc, eco.Controle + 0.000 Id_Contato, ',
        'IF(tel.EntiTipCto = 0, "", tel.Telefone) Telefone, eco.Nome Contato, ',
        'cna.ModalCobr, cna.CedBanco, cna.CedAgencia, cna.CedPosto, cna.CedConta, ',
        'cna.CartNum, cna.IDCobranca, cna.CodEmprBco, cna.TipoCobranca, ',
        'cna.EspecieDoc, cna.CNAB, cna.CtaCooper, cna.LayoutRem, cna.CorresBco, ',
        'cna.CorresAge, cna.CedDAC_A, cna.CedDAC_C, cna.CorresCto, cna.OperCodi, ',
        'IF(cna.CartTxt <> "", cna.CartTxt, cna.CartNum) CART_IMP ',
        'FROM entimail enm ',
        'LEFT JOIN enticontat eco ON eco.Controle = enm.Controle ',
        'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
        'LEFT JOIN entitel tel ON (tel.Controle=enm.Controle AND tel.EntiTipCto='+ Geral.FF0(TipoTel) +') ',
        'LEFT JOIN entidades ent ON ent.Codigo = ece.Codigo ',
        'LEFT JOIN arreits ari ON ari.Entidade = ent.Codigo ',
        'LEFT JOIN prev pre ON pre.Codigo=ari.Codigo ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=' + Geral.FF0(Protocolo),
        'AND ppi.Docum=ari.Boleto AND ppi.Cliente=ari.Entidade ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=pre.Empresa ',
        'AND ppi.ID_Cod3=enm.Conta AND ppi.ID_Cod4=ari.CNAB_Cfg) ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        QueryBloquetosFiltros,
        'AND enm.EntiTipCto=' + Geral.FF0(TipoEmail),
        'AND ari.Boleto > 0 ',
        'AND ari.CNAB_Cfg <> 0 ',
        'AND ppi.Conta <> 0 ',
        'GROUP BY tel.Controle, ptc.Codigo, enm.Email, ari.CNAB_Cfg, ari.Boleto, ari.Entidade, pre.Codigo, ari.Vencto ',
        'ORDER BY NOMEENT, ari.Boleto, enm.Email ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrProtocoBol, Dmod.MyDB, [
        'SELECT DISTINCT ari.Boleto, pre.Codigo, pre.Empresa, ari.Entidade, ari.Vencto, ',
        'IF(IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido) <> "", ',
        'IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido), ',
        'IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome)) NOMEENT_MAIL, ',
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
        'ELSE ent.Nome END NOMEENT, ptc.Codigo PROTOCOD, ',
        'ppi.Controle ProtocoPak, ptc.Tipo TipoProt,  ',
        'CONCAT_WS("-", ari.Boleto, ari.Entidade) BOLENT, ',
        'SUM(ari.Valor) Valor, cna.Nome NOME_Cfg, ari.Fatur_Cfg, ari.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, enm.Conta + 0.000 EMeio_ID, enm.Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE <= "1899-12-30", "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD <= "1899-12-30", "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y %H:%i")) DataD_Txt, ',
        'cna.JurosPerc, cna.MultaPerc, eco.Controle + 0.000 Id_Contato, ',
        'IF(tel.EntiTipCto = 0, "", tel.Telefone) Telefone, eco.Nome Contato, ',
        'cna.ModalCobr, cna.CedBanco, cna.CedAgencia, cna.CedPosto, cna.CedConta, ',
        'cna.CartNum, cna.IDCobranca, cna.CodEmprBco, cna.TipoCobranca, ',
        'cna.EspecieDoc, cna.CNAB, cna.CtaCooper, cna.LayoutRem, cna.CorresBco, ',
        'cna.CorresAge, cna.CedDAC_A, cna.CedDAC_C, cna.CorresCto, cna.OperCodi, ',
        'IF(cna.CartTxt <> "", cna.CartTxt, cna.CartNum) CART_IMP ',
        'FROM entimail enm ',
        'LEFT JOIN enticontat eco ON eco.Controle = enm.Controle ',
        'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
        'LEFT JOIN entitel tel ON (tel.Controle=enm.Controle AND tel.EntiTipCto='+ Geral.FF0(TipoTel) +') ',
        'LEFT JOIN entidades ent ON ent.Codigo = ece.Codigo ',
        'LEFT JOIN arreits ari ON ari.Entidade = ent.Codigo ',
        'LEFT JOIN prev pre ON pre.Codigo=ari.Codigo ',
        'LEFT JOIN proenprit pep ON pep.Entidade=ari.Entidade ',
        'LEFT JOIN proenpr per ON per.Codigo=pep.Codigo ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=per.Protocolo ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=ptc.Codigo ',
        'AND ppi.Docum=ari.Boleto AND ppi.Cliente=ari.Entidade ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=pre.Empresa ',
        'AND ppi.ID_Cod3=enm.Conta AND ppi.ID_Cod4=ari.CNAB_Cfg) ',
        QueryBloquetosFiltros,
        'AND enm.EntiTipCto=' + Geral.FF0(TipoEmail),
        'AND ari.Boleto > 0 ',
        'AND ari.CNAB_Cfg <> 0 ',
        'AND ppi.Conta IS NULL ',
        'AND ptc.Codigo=' + Geral.FF0(Protocolo),
        'GROUP BY tel.Controle, ptc.Codigo, enm.Email, ari.CNAB_Cfg, ari.Boleto, ari.Entidade, pre.Codigo, ari.Vencto ',
        'ORDER BY NOMEENT, ari.Boleto, enm.Email ',
        '']);
    end;
  end else
  if TipoProtocolo = 4 then //CR
  begin
    if AbaGerados = 1 then
      SQLCompl := 'AND ppi.Conta <> 0'
    else
      SQLCompl := 'AND ppi.Conta IS NULL';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrProtocoBol, Dmod.MyDB, [
      'SELECT DISTINCT ari.Boleto, pre.Codigo, pre.Empresa, ari.Entidade, ari.Vencto, ',
      'IF(IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido) <> "", ',
      'IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido), ',
      'IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome)) NOMEENT_MAIL, ',
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
      'ELSE ent.Nome END NOMEENT, ptc.Codigo PROTOCOD,  ',
      'ppi.Controle ProtocoPak, ptc.Tipo TipoProt,  ',
      'CONCAT_WS("-", ari.Boleto, ari.Entidade) BOLENT, ',
      'SUM(ari.Valor) Valor, cna.Nome NOME_Cfg, ari.Fatur_Cfg, ari.CNAB_Cfg, ',
      'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, 0 + 0.000 EMeio_ID, "" Email, ',
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ',
      'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
      'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
      'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
      'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
      'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DataD_Txt, ',
      'cna.JurosPerc, cna.MultaPerc, 0 + 0.000 Id_Contato, "" Telefone, "" Contato, ',
      'cna.ModalCobr, cna.CedBanco, cna.CedAgencia, cna.CedPosto, cna.CedConta, ',
      'cna.CartNum, cna.IDCobranca, cna.CodEmprBco, cna.TipoCobranca, ',
      'cna.EspecieDoc, cna.CNAB, cna.CtaCooper, cna.LayoutRem, cna.CorresBco, ',
      'cna.CorresAge, cna.CedDAC_A, cna.CedDAC_C, cna.CorresCto, cna.OperCodi, ',
      'IF(cna.CartTxt <> "", cna.CartTxt, cna.CartNum) CART_IMP ',
      'FROM arreits ari ',
      'LEFT JOIN prev pre ON pre.Codigo=ari.Codigo ',
      'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade ',
      'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
      'LEFT JOIN protocolos ptc ON ptc.Codigo=cna.ProtocolCR ',
      'LEFT JOIN protpakits ppi ON (ppi.Codigo=ptc.Codigo ',
      'AND ppi.Docum=ari.Boleto AND ppi.Cliente=ari.Entidade ',
      'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=pre.Empresa AND ppi.ID_Cod4=ari.CNAB_Cfg) ',
      'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
      QueryBloquetosFiltros,
      'AND ari.Boleto > 0 ',
      'AND ari.CNAB_Cfg <> 0 ',
      'AND ptc.Codigo=' + Geral.FF0(Protocolo),
      SQLCompl,
      'GROUP BY ari.CNAB_Cfg, ari.Boleto, ari.Entidade, pre.Codigo, ari.Vencto',
      'ORDER BY NOMEENT, Boleto ',
      '']);
  end else
  begin
    if AbaGerados = 1 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrProtocoBol, Dmod.MyDB, [
        'SELECT DISTINCT ari.Boleto, pre.Codigo, pre.Empresa, ari.Entidade, ari.Vencto, ',
        'IF(IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido) <> "", ',
        'IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido), ',
        'IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome)) NOMEENT_MAIL, ',
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
        'ELSE ent.Nome END NOMEENT, ptc.Codigo PROTOCOD,  ',
        'ppi.Controle ProtocoPak, ptc.Tipo TipoProt,  ',
        'CONCAT_WS("-", ari.Boleto, ari.Entidade) BOLENT, ',
        'SUM(ari.Valor) Valor, ari.Fatur_Cfg, ari.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, 0 + 0.000 EMeio_ID, "" Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DataD_Txt, ',
        'IF(ari.CNAB_Cfg<>0, cna.Nome, fcf.Nome) NOME_Cfg, ',
        'IF(ari.CNAB_Cfg<>0, 0, 1) + 0.000 Tipo, ',
        'IF(ari.CNAB_Cfg<>0, ari.CNAB_Cfg, ari.Fatur_Cfg) + 0.000 Config, ',
        'IF(ari.CNAB_Cfg<>0, cna.JurosPerc, fcf.JurosPerc) JurosPerc, ',
        'IF(ari.CNAB_Cfg<>0, cna.MultaPerc, fcf.MultaPerc) MultaPerc, ',
        '0 + 0.000 Id_Contato, "" Telefone, "" Contato, ',
        'cna.ModalCobr, cna.CedBanco, cna.CedAgencia, cna.CedPosto, cna.CedConta, ',
        'cna.CartNum, cna.IDCobranca, cna.CodEmprBco, cna.TipoCobranca, ',
        'cna.EspecieDoc, cna.CNAB, cna.CtaCooper, cna.LayoutRem, cna.CorresBco, ',
        'cna.CorresAge, cna.CedDAC_A, cna.CedDAC_C, cna.CorresCto, cna.OperCodi, ',
        'IF(cna.CartTxt <> "", cna.CartTxt, cna.CartNum) CART_IMP ',
        'FROM arreits ari ',
        'LEFT JOIN prev pre ON pre.Codigo=ari.Codigo ',
        'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
        'LEFT JOIN faturcfg fcf ON fcf.Codigo = ari.Fatur_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=' + Geral.FF0(Protocolo),
        'AND ppi.Docum=ari.Boleto AND ppi.Cliente=ari.Entidade ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=pre.Empresa AND ppi.ID_Cod4=IF(ari.CNAB_Cfg<>0, ari.CNAB_Cfg, ari.Fatur_Cfg)) ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        QueryBloquetosFiltros,
        'AND ari.Boleto > 0 ',
        'AND ppi.Conta <> 0',
        'GROUP BY ptc.Codigo, Tipo, Config, ari.Boleto, ari.Entidade, pre.Codigo, ari.Vencto ',
        'ORDER BY NOMEENT, Boleto ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrProtocoBol, Dmod.MyDB, [
        'SELECT DISTINCT ari.Boleto, pre.Codigo, pre.Empresa, ari.Entidade, ari.Vencto, ',
        'IF(IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido) <> "", ',
        'IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido), ',
        'IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome)) NOMEENT_MAIL, ',
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
        'ELSE ent.Nome END NOMEENT, ptc.Codigo PROTOCOD,  ',
        'ppi.Controle ProtocoPak, ptc.Tipo TipoProt,  ',
        'CONCAT_WS("-", ari.Boleto, ari.Entidade) BOLENT, ',
        'SUM(ari.Valor) Valor, ari.Fatur_Cfg, ari.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, 0 + 0.000 EMeio_ID, "" Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DataD_Txt, ',
        'IF(ari.CNAB_Cfg<>0, cna.Nome, fcf.Nome) NOME_Cfg, ',
        'IF(ari.CNAB_Cfg<>0, 0, 1) + 0.000 Tipo, ',
        'IF(ari.CNAB_Cfg<>0, ari.CNAB_Cfg, ari.Fatur_Cfg) + 0.000 Config, ',
        'IF(ari.CNAB_Cfg<>0, cna.JurosPerc, fcf.JurosPerc) JurosPerc, ',
        'IF(ari.CNAB_Cfg<>0, cna.MultaPerc, fcf.MultaPerc) MultaPerc, ',
        '0 + 0.000 Id_Contato, "" Telefone, "" Contato, ',
        'cna.ModalCobr, cna.CedBanco, cna.CedAgencia, cna.CedPosto, cna.CedConta, ',
        'cna.CartNum, cna.IDCobranca, cna.CodEmprBco, cna.TipoCobranca, ',
        'cna.EspecieDoc, cna.CNAB, cna.CtaCooper, cna.LayoutRem, cna.CorresBco, ',
        'cna.CorresAge, cna.CedDAC_A, cna.CedDAC_C, cna.CorresCto, cna.OperCodi, ',
        'IF(cna.CartTxt <> "", cna.CartTxt, cna.CartNum) CART_IMP ',
        'FROM arreits ari ',
        'LEFT JOIN prev pre ON pre.Codigo=ari.Codigo ',
        'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade ',
        'LEFT JOIN proenprit pep ON pep.Entidade=ari.Entidade ',
        'LEFT JOIN proenpr per ON per.Codigo=pep.Codigo ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=per.Protocolo ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
        'LEFT JOIN faturcfg fcf ON fcf.Codigo = ari.Fatur_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=ptc.Codigo ',
        'AND ppi.Docum=ari.Boleto AND ppi.Cliente=ari.Entidade ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=pre.Empresa AND ppi.ID_Cod4=IF(ari.CNAB_Cfg<>0, ari.CNAB_Cfg, ari.Fatur_Cfg)) ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        QueryBloquetosFiltros,
        'AND ari.Boleto > 0 ',
        'AND ptc.Codigo=' + Geral.FF0(Protocolo),
        'AND ppi.Conta IS NULL ',
        'GROUP BY ptc.Codigo, Tipo, Config, ari.Boleto, ari.Entidade, pre.Codigo, ari.Vencto ',
        'ORDER BY NOMEENT, Boleto ',
        '']);
    end;
  end;
  //dmkPF.LeMeuTexto(QrProtocoBol.SQL.Text);
  if ArreIts <> 0 then
    QrProtocoBol.Locate('ArreIts', ArreIts, []);
end;

procedure TDBloGeren.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

end.
