object FmBloArre: TFmBloArre
  Left = 368
  Top = 194
  Caption = 'BLO-ARREC-001 :: Cadastro de Arrecada'#231#245'es Bases'
  ClientHeight = 554
  ClientWidth = 949
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 94
    Width = 949
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 949
      Height = 102
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label9: TLabel
        Left = 64
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 558
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        FocusControl = DBEdNome
      end
      object LaForneceI: TLabel
        Left = 4
        Top = 46
        Width = 80
        Height = 13
        Caption = 'Conta vinculada:'
      end
      object SpeedButton5: TSpeedButton
        Left = 636
        Top = 63
        Width = 21
        Height = 21
        Hint = 'Inclui item de carteira'
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 55
        Height = 20
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 64
        Top = 20
        Width = 488
        Height = 20
        MaxLength = 40
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSigla: TdmkEdit
        Left = 558
        Top = 20
        Width = 99
        Height = 20
        MaxLength = 15
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Sigla'
        UpdCampo = 'Sigla'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CBConta: TdmkDBLookupComboBox
        Left = 59
        Top = 63
        Width = 573
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 4
        dmkEditCB = EdConta
        QryCampo = 'Conta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdConta: TdmkEditCB
        Left = 4
        Top = 63
        Width = 55
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Conta'
        UpdCampo = 'Conta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 397
      Width = 949
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 396
      object Panel4: TPanel
        Left = 2
        Top = 14
        Width = 945
        Height = 48
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 803
          Top = 0
          Width = 143
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 118
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 2
          Width = 118
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 94
    Width = 949
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 949
      Height = 94
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 64
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 558
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        FocusControl = dmkDBEdit1
      end
      object Label6: TLabel
        Left = 4
        Top = 47
        Width = 80
        Height = 13
        Caption = 'Conta vinculada:'
        FocusControl = dmkDBEdit2
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 4
        Top = 20
        Width = 55
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsBloArre
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 64
        Top = 20
        Width = 490
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsBloArre
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 558
        Top = 20
        Width = 99
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Sigla'
        DataSource = DsBloArre
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 4
        Top = 63
        Width = 653
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMECONTA'
        DataSource = DsBloArre
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object TabControl1: TTabControl
      Left = 0
      Top = 94
      Width = 949
      Height = 191
      Align = alTop
      MultiLine = True
      TabHeight = 20
      TabOrder = 1
      Tabs.Strings = (
        'Ativos'
        'Inativos')
      TabIndex = 0
      OnChange = TabControl1Change
      ExplicitWidth = 1186
      object LaTotal: TLabel
        Left = 4
        Top = 174
        Width = 941
        Height = 13
        Align = alBottom
        ExplicitWidth = 3
      end
      object DBGBloArreIts: TdmkDBGrid
        Left = 4
        Top = 26
        Width = 941
        Height = 148
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EXPIRAEM'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'T'#233'rmino'
            Width = 96
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Entidade'
            Title.Caption = 'ID Entidade'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEENT'
            Title.Caption = 'Entidade (Raz'#227'o social / Nome)'
            Width = 152
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FANENT'
            Title.Caption = 'Entidade (Fantasia / Apelido)'
            Width = 152
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DOCENT'
            Title.Caption = 'Entidade (CNPJ / CPF)'
            Width = 96
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SitCobr_TXT'
            Title.Caption = 'Sit. de cobran'#231'a'
            Width = 83
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Parc_Calc'
            Title.Caption = 'Parc.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Contrato'
            Title.Caption = 'ID Contrato'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DiaVcto_TXT'
            Title.Caption = 'Dia do vencto.'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INICIO'
            Title.Caption = 'In'#237'cio'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FINAL'
            Title.Caption = 'Final'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor_TXT'
            Title.Caption = 'Valor'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'InfoParc_TXT'
            Title.Caption = 'Info parc.'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Texto'
            Title.Caption = 'Descri'#231#227'o substituta'
            Width = 111
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Arredonda'
            Title.Caption = 'Arredondamento'
            Width = 84
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEEMP'
            Title.Caption = 'Empresa'
            Width = 152
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIPCFG'
            Title.Caption = 'Tipo de documento'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECFG'
            Title.Caption = 'Configura'#231#227'o'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NFSeSrvCad_TXT'
            Title.Caption = 'Regra Fiscal'
            Width = 120
            Visible = True
          end>
        Color = clWindow
        Ctl3D = True
        DataSource = DsBloArreIts
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = DBGBloArreItsDrawColumnCell
        FieldsCalcToOrder.Strings = (
          'EXPIRAEM=ParcPerF'
          'InfoParc_TXT=InfoParc'
          'Parc_Calc=Parcelas'
          'INICIO=ParcPerI'
          'FINAL=ParcPerF')
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EXPIRAEM'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'T'#233'rmino'
            Width = 96
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Entidade'
            Title.Caption = 'ID Entidade'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEENT'
            Title.Caption = 'Entidade (Raz'#227'o social / Nome)'
            Width = 152
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FANENT'
            Title.Caption = 'Entidade (Fantasia / Apelido)'
            Width = 152
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DOCENT'
            Title.Caption = 'Entidade (CNPJ / CPF)'
            Width = 96
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SitCobr_TXT'
            Title.Caption = 'Sit. de cobran'#231'a'
            Width = 83
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Parc_Calc'
            Title.Caption = 'Parc.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Contrato'
            Title.Caption = 'ID Contrato'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DiaVcto_TXT'
            Title.Caption = 'Dia do vencto.'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INICIO'
            Title.Caption = 'In'#237'cio'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FINAL'
            Title.Caption = 'Final'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor_TXT'
            Title.Caption = 'Valor'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'InfoParc_TXT'
            Title.Caption = 'Info parc.'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Texto'
            Title.Caption = 'Descri'#231#227'o substituta'
            Width = 111
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Arredonda'
            Title.Caption = 'Arredondamento'
            Width = 84
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEEMP'
            Title.Caption = 'Empresa'
            Width = 152
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIPCFG'
            Title.Caption = 'Tipo de documento'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECFG'
            Title.Caption = 'Configura'#231#227'o'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NFSeSrvCad_TXT'
            Title.Caption = 'Regra Fiscal'
            Width = 120
            Visible = True
          end>
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 397
      Width = 949
      Height = 63
      Align = alBottom
      TabOrder = 2
      ExplicitTop = 396
      object Panel5: TPanel
        Left = 2
        Top = 14
        Width = 169
        Height = 48
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 126
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 47
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 171
        Top = 14
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 434
        Top = 14
        Width = 513
        Height = 48
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 382
          Top = 0
          Width = 131
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 118
            Height = 39
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtBloArre: TBitBtn
          Tag = 10004
          Left = 4
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Arrecad.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtBloArreClick
        end
        object BtEntidade: TBitBtn
          Tag = 132
          Left = 126
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Entidade'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtEntidadeClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 949
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 902
      Top = 0
      Width = 47
      Height = 51
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 213
      Height = 51
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 213
      Top = 0
      Width = 689
      Height = 51
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 384
        Height = 31
        Caption = 'Cadastro de Arrecada'#231#245'es Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 384
        Height = 31
        Caption = 'Cadastro de Arrecada'#231#245'es Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 384
        Height = 31
        Caption = 'Cadastro de Arrecada'#231#245'es Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 949
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 14
      Width = 945
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsBloArre: TDataSource
    DataSet = QrBloArre
    Left = 40
    Top = 12
  end
  object QrBloArre: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrBloArreBeforeOpen
    AfterOpen = QrBloArreAfterOpen
    BeforeClose = QrBloArreBeforeClose
    AfterScroll = QrBloArreAfterScroll
    SQL.Strings = (
      'SELECT con.Nome NOMECONTA, blo.*'
      'FROM bloarre blo'
      'LEFT JOIN contas con ON con.Codigo = blo.Conta'
      'WHERE blo.Codigo > 0 ')
    Left = 12
    Top = 12
    object QrBloArreCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBloArreConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrBloArreNome: TWideStringField
      FieldName = 'Nome'
      Size = 40
    end
    object QrBloArreSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 15
    end
    object QrBloArreLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBloArreDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBloArreDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBloArreUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBloArreUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBloArreAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrBloArreAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBloArreNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Inclui1
    CanIns02 = Adicionaentidadearrecadaoselecionada1
    CanUpd01 = ReajustarutilizandoIGPM1
    CanUpd02 = Editaarrecadaoselecionada1
    CanUpd03 = Desativararrecadaoselecionada1
    CanDel01 = Exclui1
    CanDel02 = Retiraentidadedaarrecadaoselecionada1
    Left = 144
    Top = 12
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Credito="V"'
      'ORDER BY Nome')
    Left = 748
    Top = 8
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 776
    Top = 8
  end
  object PMBloArre: TPopupMenu
    OnPopup = PMBloArrePopup
    Left = 172
    Top = 12
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
  object PMEntidade: TPopupMenu
    OnPopup = PMEntidadePopup
    Left = 200
    Top = 12
    object Adicionaentidadearrecadaoselecionada1: TMenuItem
      Caption = '&Inclui entidade '#224' arrecada'#231#227'o selecionada'
      OnClick = Adicionaentidadearrecadaoselecionada1Click
    end
    object Editaarrecadaoselecionada1: TMenuItem
      Caption = '&Altera arrecada'#231#227'o selecionada'
      OnClick = Editaarrecadaoselecionada1Click
    end
    object Desativararrecadaoselecionada1: TMenuItem
      Caption = '&Desativar / ativar arrecada'#231#227'o selecionada'
      OnClick = Desativararrecadaoselecionada1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ReajustarutilizandoIGPM1: TMenuItem
      Caption = '&Reajustar utilizando IGP-M'
      OnClick = ReajustarutilizandoIGPM1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Retiraentidadedaarrecadaoselecionada1: TMenuItem
      Caption = '&Exclui entidade da arrecada'#231#227'o selecionada'
      OnClick = Retiraentidadedaarrecadaoselecionada1Click
    end
  end
  object QrBloArreIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrBloArreItsBeforeClose
    AfterScroll = QrBloArreItsAfterScroll
    OnCalcFields = QrBloArreItsCalcFields
    SQL.Strings = (
      'SELECT '
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.Fantasia, ent.Apelido) FANENT,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCENT,'
      'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP,'
      'cfg.Nome NOMECFG, '
      'CASE its.SitCobr '
      'WHEN 0 THEN "N'#227'o cobrar" '
      'WHEN 1 THEN "Cont'#237'nua (Anual)" '
      'WHEN 2 THEN "Programada" '
      'WHEN 3 THEN "Contrato" END SitCobr_TXT, '
      'enb.Filial, IF(its.InfoParc = 0, "N'#227'o", "Sim") InfoParc_TXT,'
      
        'IF(its.Contrato<>0, con.ddMesVcto, its.DiaVencto) + 0.000 DiaVct' +
        'o_TXT,'
      'IF(its.Contrato<>0, con.ValorMes, its.Valor) Valor_TXT,'
      'con.DVencimento, con.DtaPrxRenw, its.*'
      'FROM bloarreits its'
      'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade'
      'LEFT JOIN entidades enb ON enb.Codigo = its.Empresa'
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = its.CNAB_Cfg'
      'LEFT JOIN contratos con ON con.Codigo = its.Contrato'
      'WHERE its.Codigo=:P0'
      'AND its.Ativo=:P1')
    Left = 68
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBloArreItsNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrBloArreItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'bloarreits.Codigo'
    end
    object QrBloArreItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'bloarreits.Controle'
    end
    object QrBloArreItsEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'bloarreits.Entidade'
    end
    object QrBloArreItsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'bloarreits.Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBloArreItsSitCobr: TIntegerField
      FieldName = 'SitCobr'
      Origin = 'bloarreits.SitCobr'
    end
    object QrBloArreItsParcelas: TIntegerField
      FieldName = 'Parcelas'
      Origin = 'bloarreits.Parcelas'
    end
    object QrBloArreItsParcPerI: TIntegerField
      FieldName = 'ParcPerI'
      Origin = 'bloarreits.ParcPerI'
    end
    object QrBloArreItsParcPerF: TIntegerField
      FieldName = 'ParcPerF'
      Origin = 'bloarreits.ParcPerF'
    end
    object QrBloArreItsInfoParc: TSmallintField
      FieldName = 'InfoParc'
      Origin = 'bloarreits.InfoParc'
    end
    object QrBloArreItsArredonda: TFloatField
      FieldName = 'Arredonda'
      Origin = 'bloarreits.Arredonda'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBloArreItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'bloarreits.Lk'
    end
    object QrBloArreItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'bloarreits.DataCad'
    end
    object QrBloArreItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'bloarreits.DataAlt'
    end
    object QrBloArreItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'bloarreits.UserCad'
    end
    object QrBloArreItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'bloarreits.UserAlt'
    end
    object QrBloArreItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'bloarreits.AlterWeb'
    end
    object QrBloArreItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'bloarreits.Ativo'
    end
    object QrBloArreItsValorInf: TSmallintField
      FieldName = 'ValorInf'
      Origin = 'bloarreits.ValorInf'
    end
    object QrBloArreItsFINAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Calculated = True
    end
    object QrBloArreItsINICIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'INICIO'
      Calculated = True
    end
    object QrBloArreItsCnab_Cfg: TIntegerField
      FieldName = 'Cnab_Cfg'
    end
    object QrBloArreItsDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
    end
    object QrBloArreItsEXPIROU: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'EXPIROU'
      Calculated = True
    end
    object QrBloArreItsEXPIRAEM: TWideStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'EXPIRAEM'
      Size = 50
      Calculated = True
    end
    object QrBloArreItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrBloArreItsNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrBloArreItsFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrBloArreItsContrato: TIntegerField
      FieldName = 'Contrato'
    end
    object QrBloArreItsValor_TXT: TFloatField
      FieldName = 'Valor_TXT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBloArreItsDVencimento: TDateField
      FieldName = 'DVencimento'
    end
    object QrBloArreItsDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
    end
    object QrBloArreItsSitCobr_TXT: TWideStringField
      FieldName = 'SitCobr_TXT'
    end
    object QrBloArreItsNOMECFG: TWideStringField
      FieldName = 'NOMECFG'
      Size = 50
    end
    object QrBloArreItsDiaVcto_TXT: TFloatField
      FieldName = 'DiaVcto_TXT'
    end
    object QrBloArreItsParc_Calc: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Parc_Calc'
      Calculated = True
    end
    object QrBloArreItsTexto: TWideStringField
      FieldName = 'Texto'
      Size = 100
    end
    object QrBloArreItsInfoParc_TXT: TWideStringField
      FieldName = 'InfoParc_TXT'
      Size = 3
    end
    object QrBloArreItsFANENT: TWideStringField
      FieldName = 'FANENT'
      Size = 60
    end
    object QrBloArreItsDOCENT: TWideStringField
      FieldName = 'DOCENT'
      Size = 18
    end
    object QrBloArreItsNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrBloArreItsNFSeSrvCad_TXT: TWideStringField
      FieldName = 'NFSeSrvCad_TXT'
      Size = 255
    end
    object QrBloArreItsTIPCFG: TWideStringField
      FieldName = 'TIPCFG'
      Size = 30
    end
    object QrBloArreItsFatur_Cfg: TIntegerField
      FieldName = 'Fatur_Cfg'
    end
  end
  object DsBloArreIts: TDataSource
    DataSet = QrBloArreIts
    Left = 96
    Top = 12
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 720
    Top = 8
  end
end
