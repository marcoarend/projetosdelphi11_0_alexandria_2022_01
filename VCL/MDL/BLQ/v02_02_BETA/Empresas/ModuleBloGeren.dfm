object DBloGeren: TDBloGeren
  OldCreateOrder = False
  Height = 379
  Width = 644
  object QrNIA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT arr.Codigo, arr.Nome, arr.Conta, '
      'IF(its.Contrato<>0, con.ValorMes, its.Valor) Valor,'
      
        'IF(its.Contrato<>0, con.ddMesVcto, its.DiaVencto) + 0.000 DiaVen' +
        'cto,'
      'its.SitCobr, its.Parcelas, its.ParcPerI, its.ParcPerF, '
      
        'its.InfoParc, its.Texto, its.Controle, its.Arredonda, its.Entida' +
        'de, '
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,  '
      'its.CNAB_Cfg, cna.CartEmiss, its.ValorInf,'
      'con.DVencimento, con.DtaPrxRenw'
      'FROM bloarre arr'
      'LEFT JOIN bloarreits its ON its.Codigo = arr.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade'
      'LEFT JOIN cnab_cfg cna ON cna.Codigo = its.CNAB_Cfg'
      'LEFT JOIN contratos con ON con.Codigo = its.Contrato'
      'WHERE arr.Codigo > 0'
      'AND its.Ativo = 1'
      'AND its.Empresa=:P0')
    Left = 17
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNIACodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'bloarre.Codigo'
    end
    object QrNIANome: TWideStringField
      FieldName = 'Nome'
      Origin = 'bloarre.Nome'
      Size = 40
    end
    object QrNIAConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'bloarre.Conta'
    end
    object QrNIAValor: TFloatField
      FieldName = 'Valor'
      Origin = 'bloarreits.Valor'
    end
    object QrNIASitCobr: TIntegerField
      FieldName = 'SitCobr'
      Origin = 'bloarreits.SitCobr'
    end
    object QrNIAParcelas: TIntegerField
      FieldName = 'Parcelas'
      Origin = 'bloarreits.Parcelas'
    end
    object QrNIAParcPerI: TIntegerField
      FieldName = 'ParcPerI'
      Origin = 'bloarreits.ParcPerI'
    end
    object QrNIAParcPerF: TIntegerField
      FieldName = 'ParcPerF'
      Origin = 'bloarreits.ParcPerF'
    end
    object QrNIAInfoParc: TSmallintField
      FieldName = 'InfoParc'
      Origin = 'bloarreits.InfoParc'
    end
    object QrNIATexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'bloarreits.Texto'
      Size = 40
    end
    object QrNIAControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'bloarreits.Controle'
      Required = True
    end
    object QrNIAArredonda: TFloatField
      FieldName = 'Arredonda'
      Origin = 'bloarreits.Arredonda'
    end
    object QrNIAEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'bloarreits.Entidade'
    end
    object QrNIANOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrNIACNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
      Origin = 'bloarreits.Cnab_Cfg'
    end
    object QrNIAValorInf: TSmallintField
      FieldName = 'ValorInf'
      Origin = 'bloarreits.ValorInf'
    end
    object QrNIADVencimento: TDateField
      FieldName = 'DVencimento'
    end
    object QrNIADtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
    end
    object QrNIADiaVencto: TFloatField
      FieldName = 'DiaVencto'
    end
    object QrNIANFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrNIACartEmiss: TFloatField
      FieldName = 'CartEmiss'
    end
    object QrNIAFatur_Cfg: TIntegerField
      FieldName = 'Fatur_Cfg'
    end
  end
  object QrLocArre: TMySQLQuery
    Database = Dmod.MyDB
    Left = 72
    Top = 10
  end
  object QrPesqMB: TMySQLQuery
    Database = Dmod.MyDB
    Left = 129
    Top = 58
    object QrPesqMBBoleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'Boleto'
    end
  end
  object QrSumBol: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) VALOR'
      'FROM arreits ari'
      'WHERE ari.Codigo=:P0'
      'AND ari.Boleto <> 0')
    Left = 17
    Top = 59
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumBolVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object QrSumPre: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) VALOR'
      'FROM arreits ari'
      'WHERE ari.Codigo=:P0'
      'AND ari.Boleto = 0')
    Left = 71
    Top = 59
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPreVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 177
    Top = 59
  end
  object QrBloOpcoes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM bloopcoes')
    Left = 231
    Top = 59
  end
  object QrCNABcfg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cnab_cfg'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 39
    Top = 210
    object QrCNABcfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNABcfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCNABcfg: TDataSource
    DataSet = QrCNABcfg
    Left = 67
    Top = 210
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 103
    Top = 210
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 131
    Top = 210
  end
  object QrArrePesq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bloarre'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 167
    Top = 210
    object QrArrePesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrArrePesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 40
    end
  end
  object DsArrePesq: TDataSource
    DataSet = QrArrePesq
    Left = 195
    Top = 210
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) Nome'
      'FROM entidades'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 231
    Top = 210
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 259
    Top = 210
  end
  object QrBloArreItsVen: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrBloArreItsVenCalcFields
    SQL.Strings = (
      'SELECT IF(its.InfoParc = 0, "N'#227'o", "Sim") InfoParc_TXT,'
      'are.Nome NOMEARRECAD,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP,'
      'cfg.Nome NOMECFG, "Programada" SitCobr_TXT, enb.Filial, '
      'its.DiaVencto + 0.000 DiaVcto_TXT, its.Valor Valor_TXT, '
      'its.DataCad DVencimento, its.DataCad DtaPrxRenw, its.*'
      'FROM bloarreits its'
      'LEFT JOIN bloarre are ON are.Codigo = its.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade'
      'LEFT JOIN entidades enb ON enb.Codigo = its.Empresa'
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = its.CNAB_Cfg'
      'WHERE its.Contrato=0'
      'AND its.Ativo=1'
      'AND its.ParcPerF<=:P0'
      'AND its.SitCobr=2'
      ''
      'UNION'
      ''
      'SELECT IF(its.InfoParc = 0, "N'#227'o", "Sim") InfoParc_TXT,'
      'are.Nome NOMEARRECAD,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP,'
      'cfg.Nome NOMECFG, "Contrato" SitCobr_TXT, enb.Filial, '
      'con.ddMesVcto + 0.000 DiaVcto_TXT, con.ValorMes Valor_TXT, '
      'con.DVencimento, con.DtaPrxRenw, its.*'
      'FROM bloarreits its'
      'LEFT JOIN bloarre are ON are.Codigo = its.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade'
      'LEFT JOIN entidades enb ON enb.Codigo = its.Empresa'
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = its.CNAB_Cfg'
      'LEFT JOIN contratos con ON con.Codigo = its.Contrato'
      'WHERE its.Contrato<>0'
      'AND its.Ativo=1'
      'AND con.Ativo=1'
      'AND con.DtaPrxRenw<=:P1'
      'AND its.SitCobr=3')
    Left = 420
    Top = 108
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBloArreItsVenNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrBloArreItsVenCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'bloarreits.Codigo'
    end
    object QrBloArreItsVenControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'bloarreits.Controle'
    end
    object QrBloArreItsVenEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'bloarreits.Entidade'
    end
    object QrBloArreItsVenValor: TFloatField
      FieldName = 'Valor'
      Origin = 'bloarreits.Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBloArreItsVenSitCobr: TIntegerField
      FieldName = 'SitCobr'
      Origin = 'bloarreits.SitCobr'
    end
    object QrBloArreItsVenParcelas: TIntegerField
      FieldName = 'Parcelas'
      Origin = 'bloarreits.Parcelas'
    end
    object QrBloArreItsVenParcPerI: TIntegerField
      FieldName = 'ParcPerI'
      Origin = 'bloarreits.ParcPerI'
    end
    object QrBloArreItsVenParcPerF: TIntegerField
      FieldName = 'ParcPerF'
      Origin = 'bloarreits.ParcPerF'
    end
    object QrBloArreItsVenInfoParc: TSmallintField
      FieldName = 'InfoParc'
      Origin = 'bloarreits.InfoParc'
    end
    object QrBloArreItsVenArredonda: TFloatField
      FieldName = 'Arredonda'
      Origin = 'bloarreits.Arredonda'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBloArreItsVenLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'bloarreits.Lk'
    end
    object QrBloArreItsVenDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'bloarreits.DataCad'
    end
    object QrBloArreItsVenDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'bloarreits.DataAlt'
    end
    object QrBloArreItsVenUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'bloarreits.UserCad'
    end
    object QrBloArreItsVenUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'bloarreits.UserAlt'
    end
    object QrBloArreItsVenAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'bloarreits.AlterWeb'
    end
    object QrBloArreItsVenAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'bloarreits.Ativo'
    end
    object QrBloArreItsVenValorInf: TSmallintField
      FieldName = 'ValorInf'
      Origin = 'bloarreits.ValorInf'
    end
    object QrBloArreItsVenFINAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Calculated = True
    end
    object QrBloArreItsVenINICIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'INICIO'
      Calculated = True
    end
    object QrBloArreItsVenCnab_Cfg: TIntegerField
      FieldName = 'Cnab_Cfg'
    end
    object QrBloArreItsVenDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
    end
    object QrBloArreItsVenEXPIROU: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'EXPIROU'
      Calculated = True
    end
    object QrBloArreItsVenEXPIRAEM: TWideStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'EXPIRAEM'
      Size = 50
      Calculated = True
    end
    object QrBloArreItsVenEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrBloArreItsVenNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrBloArreItsVenFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrBloArreItsVenContrato: TIntegerField
      FieldName = 'Contrato'
    end
    object QrBloArreItsVenValor_TXT: TFloatField
      FieldName = 'Valor_TXT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBloArreItsVenSitCobr_TXT: TWideStringField
      FieldName = 'SitCobr_TXT'
    end
    object QrBloArreItsVenNOMECFG: TWideStringField
      FieldName = 'NOMECFG'
      Size = 50
    end
    object QrBloArreItsVenDiaVcto_TXT: TFloatField
      FieldName = 'DiaVcto_TXT'
    end
    object QrBloArreItsVenParc_Calc: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Parc_Calc'
      Calculated = True
    end
    object QrBloArreItsVenDVencimento: TDateField
      FieldName = 'DVencimento'
    end
    object QrBloArreItsVenDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
    end
    object QrBloArreItsVenTexto: TWideStringField
      FieldName = 'Texto'
      Size = 100
    end
    object QrBloArreItsVenInfoParc_TXT: TWideStringField
      FieldName = 'InfoParc_TXT'
      Size = 3
    end
    object QrBloArreItsVenNOMEARRECAD: TWideStringField
      FieldName = 'NOMEARRECAD'
      Size = 40
    end
  end
  object DsBloArreItsVen: TDataSource
    DataSet = QrBloArreItsVen
    Left = 448
    Top = 108
  end
  object DsProtocoBol: TDataSource
    DataSet = QrProtocoBol
    Left = 440
    Top = 260
  end
  object QrArre: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrArreBeforeClose
    AfterScroll = QrArreAfterScroll
    Left = 383
    Top = 194
    object QrArreNomeArre: TWideStringField
      FieldName = 'NomeArre'
      Size = 40
    end
    object QrArreNomeEnti: TWideStringField
      FieldName = 'NomeEnti'
      Size = 100
    end
    object QrArreNomeContrato: TWideStringField
      FieldName = 'NomeContrato'
      Size = 255
    end
    object QrArreBloArre: TIntegerField
      FieldName = 'BloArre'
    end
    object QrArreBloArreIts: TIntegerField
      FieldName = 'BloArreIts'
    end
    object QrArreCodContrato: TIntegerField
      FieldName = 'CodContrato'
    end
    object QrArreAdiciona: TSmallintField
      FieldName = 'Adiciona'
      MaxValue = 1
    end
    object QrArreControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrArreCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrArreFatur_Cfg: TIntegerField
      FieldName = 'Fatur_Cfg'
    end
    object QrArreNOME_Cfg: TWideStringField
      FieldName = 'NOME_Cfg'
      Size = 50
    end
    object QrArreEntidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object DsArre: TDataSource
    DataSet = QrArre
    Left = 411
    Top = 194
  end
  object DsArreIts: TDataSource
    DataSet = QrArreIts
    Left = 467
    Top = 194
  end
  object QrArreIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Adiciona, Texto, Valor'
      'FROM bloarretmp '
      'WHERE BloArre=:P0'
      'AND BloArreIts=:P1'
      'ORDER BY Texto, Valor')
    Left = 439
    Top = 194
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrArreItsAdiciona: TSmallintField
      FieldName = 'Adiciona'
      MaxValue = 1
    end
    object QrArreItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrArreItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrArreItsDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
    end
    object QrArreItsPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrArreItsJurosPerc: TFloatField
      FieldName = 'JurosPerc'
    end
    object QrArreItsMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrArreItsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrArreItsCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrArreItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrArreItsBloArre: TIntegerField
      FieldName = 'BloArre'
    end
    object QrArreItsBloArreIts: TIntegerField
      FieldName = 'BloArreIts'
    end
    object QrArreItsCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrArreItsControleTmp: TIntegerField
      FieldName = 'ControleTmp'
    end
    object QrArreItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrArreItsTextoTmp: TWideStringField
      FieldName = 'TextoTmp'
      Size = 40
    end
    object QrArreItsTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Size = 100
    end
    object QrArreItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrArreItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrArreItsNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrArreItsFatur_Cfg: TIntegerField
      FieldName = 'Fatur_Cfg'
    end
    object QrArreItsVencto: TDateField
      FieldName = 'Vencto'
    end
  end
  object QrLocArreAll: TMySQLQuery
    Database = Dmod.MyDB
    Left = 377
    Top = 27
  end
  object QrProtocoBol: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrProtocoBolCalcFields
    SQL.Strings = (
      'SELECT ari.Vencto, ari.Entidade, SUM(ari.Valor) Valor,'
      'cna.Nome NOMECNAB_Cfg, CASE WHEN ent.Tipo=0'
      'THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT,'
      'ari.Boleto, ari.CNAB_Cfg, ari.Protocolo PROTOCOD,'
      'ppi.Conta PROTOCOLO, ptc.Nome TAREFA,'
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER,'
      'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, '
      'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, '
      'ppi.MultaVal MULTAVAL,  pre.Codigo PREVCOD, pre.Periodo'
      'FROM arreits ari'
      'LEFT JOIN prev pre ON pre.Codigo=ari.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade'
      'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ari.Protocolo'
      'LEFT JOIN protpakits ppi ON ppi.Docum=ari.Boleto'
      'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender'
      'WHERE ari.Codigo=:P0'
      'AND ptc.Def_Client=:P1'
      'GROUP BY ari.CNAB_Cfg, ari.Entidade, ari.Vencto'
      'ORDER BY  NOMECNAB_Cfg, NOMEENT')
    Left = 412
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrProtocoBolVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProtocoBolEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrProtocoBolValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProtocoBolNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrProtocoBolBoleto: TFloatField
      FieldName = 'Boleto'
    end
    object QrProtocoBolCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrProtocoBolPROTOCOD: TIntegerField
      FieldName = 'PROTOCOD'
    end
    object QrProtocoBolPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      DisplayFormat = '000000;-000000;'
    end
    object QrProtocoBolTAREFA: TWideStringField
      FieldName = 'TAREFA'
      Size = 50
    end
    object QrProtocoBolDELIVER: TWideStringField
      FieldName = 'DELIVER'
      Size = 100
    end
    object QrProtocoBolLOTE: TIntegerField
      FieldName = 'LOTE'
      DisplayFormat = '000000;-000000;'
    end
    object QrProtocoBolVENCTO_PROT: TDateField
      FieldName = 'VENCTO_PROT'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProtocoBolMULTAVAL: TFloatField
      FieldName = 'MULTAVAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProtocoBolPREVCOD: TIntegerField
      FieldName = 'PREVCOD'
    end
    object QrProtocoBolPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrProtocoBolMORADIAVAL: TFloatField
      FieldName = 'MORADIAVAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProtocoBolProtocoPak: TIntegerField
      FieldName = 'ProtocoPak'
    end
    object QrProtocoBolTipoProt: TIntegerField
      FieldName = 'TipoProt'
    end
    object QrProtocoBolEmail: TWideStringField
      FieldName = 'Email'
      Size = 255
    end
    object QrProtocoBolEMeio_ID: TFloatField
      FieldName = 'EMeio_ID'
    end
    object QrProtocoBolArreits: TIntegerField
      FieldName = 'Arreits'
    end
    object QrProtocoBolDataE_Txt: TWideStringField
      FieldName = 'DataE_Txt'
      Size = 10
    end
    object QrProtocoBolDataD_Txt: TWideStringField
      DisplayWidth = 16
      FieldName = 'DataD_Txt'
      Size = 16
    end
    object QrProtocoBolDataE: TDateField
      FieldName = 'DataE'
    end
    object QrProtocoBolDataD: TDateTimeField
      FieldName = 'DataD'
    end
    object QrProtocoBolCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProtocoBolEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrProtocoBolNOME_Cfg: TWideStringField
      FieldName = 'NOME_Cfg'
      Size = 50
    end
    object QrProtocoBolJurosPerc: TFloatField
      FieldName = 'JurosPerc'
    end
    object QrProtocoBolMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrProtocoBolFatur_Cfg: TIntegerField
      FieldName = 'Fatur_Cfg'
    end
    object QrProtocoBolNOMEENT_MAIL: TWideStringField
      FieldName = 'NOMEENT_MAIL'
      Size = 100
    end
    object QrProtocoBolId_Contato: TFloatField
      FieldName = 'Id_Contato'
    end
    object QrProtocoBolTelefone: TWideStringField
      FieldName = 'Telefone'
    end
    object QrProtocoBolContato: TWideStringField
      FieldName = 'Contato'
      Size = 30
    end
    object QrProtocoBolTelefone_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Telefone_TXT'
      Size = 30
      Calculated = True
    end
    object QrProtocoBolModalCobr: TSmallintField
      FieldName = 'ModalCobr'
    end
    object QrProtocoBolCedBanco: TIntegerField
      FieldName = 'CedBanco'
    end
    object QrProtocoBolCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
    end
    object QrProtocoBolCedPosto: TIntegerField
      FieldName = 'CedPosto'
    end
    object QrProtocoBolCedConta: TWideStringField
      FieldName = 'CedConta'
      Size = 30
    end
    object QrProtocoBolCartNum: TWideStringField
      FieldName = 'CartNum'
      Size = 3
    end
    object QrProtocoBolIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Size = 2
    end
    object QrProtocoBolCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
    end
    object QrProtocoBolTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
    end
    object QrProtocoBolEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
    end
    object QrProtocoBolCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrProtocoBolCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrProtocoBolLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrProtocoBolCorresBco: TIntegerField
      FieldName = 'CorresBco'
    end
    object QrProtocoBolCorresAge: TIntegerField
      FieldName = 'CorresAge'
    end
    object QrProtocoBolCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Size = 1
    end
    object QrProtocoBolCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Size = 1
    end
    object QrProtocoBolCorresCto: TWideStringField
      FieldName = 'CorresCto'
      Size = 30
    end
    object QrProtocoBolCART_IMP: TWideStringField
      FieldName = 'CART_IMP'
      Size = 10
    end
    object QrProtocoBolOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Size = 3
    end
    object QrProtocoBolVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Calculated = True
    end
    object QrProtocoBolLinhaDigitavel_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LinhaDigitavel_TXT'
      Size = 100
      Calculated = True
    end
  end
end
