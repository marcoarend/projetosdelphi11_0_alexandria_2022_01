object FmBloGerenBaA: TFmBloGerenBaA
  Left = 339
  Top = 185
  Caption = 'BLO-GEREN-004 :: Adi'#231#227'o de Contas Bases no Or'#231'amento'
  ClientHeight = 489
  ClientWidth = 864
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 864
    Height = 333
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object StaticText2: TStaticText
      Left = 0
      Top = 17
      Width = 864
      Height = 28
      Align = alTop
      Alignment = taCenter
      Caption = 'StaticText2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -21
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 0
      Top = 45
      Width = 864
      Height = 288
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Adiciona'
          Title.Caption = 'Ok'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Texto a ser impresso'
          Width = 160
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTABASE'
          Title.Caption = 'Conta'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeEnti'
          Title.Caption = 'Entidade'
          Width = 405
          Visible = True
        end>
      Color = clWindow
      DataSource = DsBloArre
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGrid1CellClick
      FieldsCalcToOrder.Strings = (
        'NOMECONTABASE=BloArre')
      Columns = <
        item
          Expanded = False
          FieldName = 'Adiciona'
          Title.Caption = 'Ok'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Texto a ser impresso'
          Width = 160
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTABASE'
          Title.Caption = 'Conta'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeEnti'
          Title.Caption = 'Entidade'
          Width = 405
          Visible = True
        end>
    end
    object PB1: TProgressBar
      Left = 0
      Top = 0
      Width = 864
      Height = 17
      Align = alTop
      TabOrder = 2
      Visible = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 864
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 816
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 768
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 381
    Width = 864
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 425
    Width = 864
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 716
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 4
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn2: TBitBtn
        Tag = 127
        Left = 204
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn2Click
      end
      object BitBtn3: TBitBtn
        Tag = 128
        Left = 328
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BitBtn3Click
      end
      object BitBtn1: TBitBtn
        Tag = 12
        Left = 496
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BitBtn1Click
      end
    end
  end
  object TbBloArre: TmySQLTable
    Database = Dmod.ZZDB
    AfterInsert = TbBloArreAfterInsert
    TableName = 'bloarretmp'
    Left = 8
    Top = 8
    object TbBloArreNOMECONTABASE: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECONTABASE'
      LookupDataSet = QrBloArr
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'BloArre'
      Size = 50
      Lookup = True
    end
    object TbBloArreSeq: TIntegerField
      FieldName = 'Seq'
    end
    object TbBloArreConta: TIntegerField
      FieldName = 'Conta'
    end
    object TbBloArreBloArre: TIntegerField
      FieldName = 'BloArre'
    end
    object TbBloArreBloArreIts: TIntegerField
      FieldName = 'BloArreIts'
    end
    object TbBloArreValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object TbBloArreTexto: TWideStringField
      FieldName = 'Texto'
      Size = 40
    end
    object TbBloArreAdiciona: TSmallintField
      FieldName = 'Adiciona'
      MaxValue = 1
    end
    object TbBloArreEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object TbBloArreNomeEnti: TWideStringField
      FieldName = 'NomeEnti'
      Size = 100
    end
    object TbBloArreCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object TbBloArreCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object TbBloArreDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
    end
    object TbBloArreNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object TbBloArreFatur_Cfg: TIntegerField
      FieldName = 'Fatur_Cfg'
    end
  end
  object DsBloArre: TDataSource
    DataSet = TbBloArre
    Left = 36
    Top = 8
  end
  object QrBloArr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Conta, Nome'
      'FROM bloarre'
      'WHERE Codigo > 0 ')
    Left = 64
    Top = 8
    object QrBloArrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBloArrConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrBloArrNome: TWideStringField
      FieldName = 'Nome'
      Size = 40
    end
  end
  object DsBloArr: TDataSource
    DataSet = QrBloArr
    Left = 92
    Top = 8
  end
  object QrConta: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT COUNT(Valor) Zeros'
      'FROM bloarretmp'
      'WHERE Valor < 0.01')
    Left = 704
    Top = 8
    object QrContaZeros: TLargeintField
      FieldName = 'Zeros'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 448
    Top = 11
  end
end
