object FmBloArreIts: TFmBloArreIts
  Left = 339
  Top = 185
  Caption = 'BLO-ARREC-002 :: Item de Arrecada'#231#227'o Base'
  ClientHeight = 682
  ClientWidth = 641
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 641
    Height = 529
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label12: TLabel
      Left = 11
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label1: TLabel
      Left = 11
      Top = 135
      Width = 45
      Height = 13
      Caption = 'Entidade:'
    end
    object SpeedButton7: TSpeedButton
      Left = 472
      Top = 150
      Width = 23
      Height = 21
      Caption = '...'
      OnClick = SpeedButton7Click
    end
    object Label2: TLabel
      Left = 11
      Top = 273
      Width = 340
      Height = 13
      Caption = 
        'Texto a ser usado no lugar do texto base (deixe vazio para usar ' +
        'o base):'
    end
    object Label19: TLabel
      Left = 411
      Top = 273
      Width = 81
      Height = 13
      Caption = 'Arredondamento:'
    end
    object Label4: TLabel
      Left = 85
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object LaContrato: TLabel
      Left = 11
      Top = 180
      Width = 414
      Height = 13
      Caption = 
        'Contrato (Antes de selecionar o contrato '#233' necess'#225'rio selecionar' +
        ' a empresa e o cliente):'
    end
    object SBContrato: TSpeedButton
      Left = 472
      Top = 197
      Width = 23
      Height = 21
      Caption = '...'
      OnClick = SBContratoClick
    end
    object Label3: TLabel
      Left = 11
      Top = 91
      Width = 66
      Height = 13
      Caption = 'Configura'#231#227'o:'
    end
    object SpeedButton1: TSpeedButton
      Left = 472
      Top = 107
      Width = 23
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object LaNFSeSrvCad: TLabel
      Left = 11
      Top = 227
      Width = 62
      Height = 13
      Caption = 'Regra Fiscal:'
    end
    object SBNFSeSrvCad: TSpeedButton
      Left = 472
      Top = 244
      Width = 23
      Height = 21
      Caption = '...'
      OnClick = SBNFSeSrvCadClick
    end
    object EdCodigo: TdmkEdit
      Left = 11
      Top = 20
      Width = 70
      Height = 20
      Alignment = taRightJustify
      Color = clInactiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGSitCobr: TRadioGroup
      Left = 501
      Top = 97
      Width = 128
      Height = 83
      Caption = ' Forma de cobran'#231'a: '
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o cobrar'
        'Cont'#237'nua (Anual)'
        'Programada')
      TabOrder = 18
      OnClick = RGSitCobrClick
    end
    object GBPer: TGroupBox
      Left = 11
      Top = 431
      Width = 484
      Height = 88
      Caption = 
        ' Per'#237'odo de cobran'#231'a programada (m'#234's e ano) ou cont'#237'nua (s'#243' m'#234's)' +
        ': '
      TabOrder = 19
      Visible = False
      object GBIni: TGroupBox
        Left = 5
        Top = 17
        Width = 237
        Height = 63
        Caption = ' Per'#237'odo Inicial: '
        TabOrder = 0
        object Label32: TLabel
          Left = 4
          Top = 14
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object LaAnoI: TLabel
          Left = 168
          Top = 14
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object CBMesI: TComboBox
          Left = 5
          Top = 31
          Width = 160
          Height = 24
          AutoDropDown = True
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnChange = CBMesIChange
        end
        object CBAnoI: TComboBox
          Left = 168
          Top = 31
          Width = 63
          Height = 24
          AutoDropDown = True
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnChange = CBAnoIChange
        end
      end
      object GBFim: TGroupBox
        Left = 244
        Top = 17
        Width = 237
        Height = 63
        Caption = ' Per'#237'odo final: '
        TabOrder = 1
        object Label34: TLabel
          Left = 5
          Top = 14
          Width = 23
          Height = 13
          Caption = 'M'#234's:'
        end
        object LaAnoF: TLabel
          Left = 168
          Top = 14
          Width = 22
          Height = 13
          Caption = 'Ano:'
        end
        object CBMesF: TComboBox
          Left = 5
          Top = 31
          Width = 160
          Height = 24
          AutoDropDown = True
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnChange = CBMesFChange
        end
        object CBAnoF: TComboBox
          Left = 168
          Top = 31
          Width = 63
          Height = 24
          AutoDropDown = True
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnChange = CBAnoFChange
        end
      end
    end
    object GbPro: TGroupBox
      Left = 501
      Top = 190
      Width = 128
      Height = 56
      Caption = ' Cobran'#231'a programada: '
      TabOrder = 20
      Visible = False
      object Label7: TLabel
        Left = 16
        Top = 27
        Width = 44
        Height = 13
        Caption = 'Parcelas:'
      end
      object EdParcelas: TdmkEdit
        Left = 65
        Top = 25
        Width = 45
        Height = 21
        Alignment = taRightJustify
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object EdEntidade: TdmkEditCB
      Left = 11
      Top = 150
      Width = 55
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEntidadeChange
      DBLookupComboBox = CBEntidade
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBEntidade: TdmkDBLookupComboBox
      Left = 66
      Top = 150
      Width = 404
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENT'
      ListSource = DsEntidades
      TabOrder = 7
      dmkEditCB = EdEntidade
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object GBValor: TGroupBox
      Left = 501
      Top = 4
      Width = 128
      Height = 85
      Caption = ' Valor para c'#225'lculo: '
      TabOrder = 17
      object Label17: TLabel
        Left = 18
        Top = 20
        Width = 53
        Height = 13
        Caption = 'Valor base:'
      end
      object CkValor: TCheckBox
        Left = 18
        Top = 59
        Width = 92
        Height = 17
        Caption = 'Solicitar o valor.'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
      object EdValor: TdmkEdit
        Left = 18
        Top = 35
        Width = 96
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object EdTexto: TdmkEdit
      Left = 11
      Top = 290
      Width = 395
      Height = 20
      MaxLength = 100
      TabOrder = 12
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CkInfoParc: TCheckBox
      Left = 11
      Top = 316
      Width = 264
      Height = 17
      Caption = 'Informar n'#250'mero da parcela / total de parcelas.'
      TabOrder = 14
    end
    object EdArredonda: TdmkEdit
      Left = 411
      Top = 290
      Width = 84
      Height = 20
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,01'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.010000000000000000
      ValWarn = False
    end
    object RGDiaVencto: TRadioGroup
      Left = 11
      Top = 338
      Width = 484
      Height = 88
      Caption = ' Dia do m'#234's do vencimento: '
      Columns = 7
      ItemIndex = 4
      Items.Strings = (
        '01'
        '02'
        '03'
        '04'
        '05'
        '06'
        '07'
        '08'
        '09'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28'
        '29'
        '30'
        '31')
      TabOrder = 16
    end
    object CkAtivo: TCheckBox
      Left = 446
      Top = 316
      Width = 49
      Height = 17
      BiDiMode = bdLeftToRight
      Caption = 'Ativo'
      Checked = True
      ParentBiDiMode = False
      State = cbChecked
      TabOrder = 15
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 140
      Top = 20
      Width = 355
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 2
      dmkEditCB = EdEmpresa
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdEmpresa: TdmkEditCB
      Left = 85
      Top = 20
      Width = 55
      Height = 20
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEmpresaChange
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdContrato: TdmkEditCB
      Left = 11
      Top = 197
      Width = 55
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdContratoChange
      DBLookupComboBox = CBContrato
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBContrato: TdmkDBLookupComboBox
      Left = 66
      Top = 197
      Width = 404
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContratos
      TabOrder = 9
      dmkEditCB = EdContrato
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object CBCNABConf: TdmkDBLookupComboBox
      Left = 66
      Top = 107
      Width = 404
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCnab_Cfg
      TabOrder = 5
      dmkEditCB = EdCNABConf
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCNABConf: TdmkEditCB
      Left = 11
      Top = 107
      Width = 55
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCNABConf
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdNFSeSrvCad: TdmkEditCB
      Left = 11
      Top = 244
      Width = 55
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdContratoChange
      DBLookupComboBox = CBNFSeSrvCad
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBNFSeSrvCad: TdmkDBLookupComboBox
      Left = 66
      Top = 244
      Width = 404
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsNFSeSrvCad
      TabOrder = 11
      dmkEditCB = EdNFSeSrvCad
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object RgTipo: TdmkRadioGroup
      Left = 11
      Top = 50
      Width = 484
      Height = 36
      Caption = 'Tipo de fatura'
      Columns = 2
      Items.Strings = (
        'Item 0'
        'Item 1')
      TabOrder = 3
      TabStop = True
      OnClick = RgTipoClick
      UpdType = utYes
      OldValor = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 641
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 594
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 547
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 302
        Height = 31
        Caption = 'Item de Arrecada'#231#227'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 302
        Height = 31
        Caption = 'Item de Arrecada'#231#227'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 302
        Height = 31
        Caption = 'Item de Arrecada'#231#227'o Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 576
    Width = 641
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 637
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 619
    Width = 641
    Height = 63
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 14
      Width = 637
      Height = 48
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 496
        Top = 0
        Width = 142
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 18
        Top = 3
        Width = 119
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT'
      'FROM entidades ent'
      'WHERE Codigo > 0'
      'ORDER BY NOMEENT')
    Left = 341
    Top = 448
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 369
    Top = 448
  end
  object QrCnab_Cfg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM  cnab_cfg'
      'WHERE Codigo > 0'
      'AND Ativo = 1'
      'ORDER BY Nome')
    Left = 397
    Top = 448
    object QrCnab_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCnab_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCnab_Cfg: TDataSource
    DataSet = QrCnab_Cfg
    Left = 425
    Top = 448
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 453
    Top = 448
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 482
    Top = 448
  end
  object QrContratos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Contratada, Contratante, '
      'DVencimento, DtaPrxRenw, ValorMes, ddMesVcto'
      'FROM contratos'
      'WHERE DtaCntrFim < 2'
      'AND Ativo = 1'
      'AND Contratada=:P0'
      'AND Contratante=:P1')
    Left = 510
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contratos.Codigo'
    end
    object QrContratosContratada: TIntegerField
      FieldName = 'Contratada'
      Origin = 'contratos.Contratada'
    end
    object QrContratosNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contratos.Nome'
      Size = 255
    end
    object QrContratosDVencimento: TDateField
      FieldName = 'DVencimento'
      Origin = 'contratos.DVencimento'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosValorMes: TFloatField
      FieldName = 'ValorMes'
      Origin = 'contratos.ValorMes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrContratosddMesVcto: TSmallintField
      FieldName = 'ddMesVcto'
      Origin = 'contratos.ddMesVcto'
    end
    object QrContratosContratante: TIntegerField
      FieldName = 'Contratante'
      Origin = 'contratos.Contratante'
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 538
    Top = 448
  end
  object QrNFSeSrvCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM nfsesrvcad'
      'ORDER BY Nome')
    Left = 566
    Top = 448
    object QrNFSeSrvCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeSrvCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsNFSeSrvCad: TDataSource
    DataSet = QrNFSeSrvCad
    Left = 594
    Top = 448
  end
end
