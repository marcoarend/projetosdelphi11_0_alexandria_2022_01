object FmBloAvulso: TFmBloAvulso
  Left = 339
  Top = 185
  Caption = 'BLO-GEREN-008 :: Fatura Avulsa'
  ClientHeight = 272
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 554
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 506
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 458
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 167
        Height = 32
        Caption = 'Fatura Avulsa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 167
        Height = 32
        Caption = 'Fatura Avulsa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 167
        Height = 32
        Caption = 'Fatura Avulsa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 554
    Height = 110
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 63
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 554
      Height = 110
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 63
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 554
        Height = 110
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 63
        object Label37: TLabel
          Left = 12
          Top = 56
          Width = 66
          Height = 13
          Caption = 'Configura'#231#227'o:'
        end
        object SpeedButton2: TSpeedButton
          Left = 520
          Top = 73
          Width = 21
          Height = 21
          Hint = 'Inclui item de carteira'
          Caption = '...'
          OnClick = SpeedButton2Click
        end
        object EdCNAB_cfg: TdmkEditCB
          Left = 12
          Top = 73
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCNAB_cfg
          IgnoraDBLookupComboBox = False
        end
        object CBCNAB_cfg: TdmkDBLookupComboBox
          Left = 68
          Top = 73
          Width = 450
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCNAB_cfg
          TabOrder = 1
          dmkEditCB = EdCNAB_cfg
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object RgTipo: TdmkRadioGroup
          Left = 12
          Top = 10
          Width = 528
          Height = 37
          Caption = 'Tipo de fatura'
          Columns = 2
          Items.Strings = (
            'Item 0'
            'Item 1')
          TabOrder = 2
          OnClick = RgTipoClick
          UpdType = utYes
          OldValor = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 158
    Width = 554
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 111
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 550
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 202
    Width = 554
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 155
    object PnSaiDesis: TPanel
      Left = 408
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 406
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrCNAB_cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM cnab_cfg'
      'WHERE Ativo = 1'
      'AND CartEmiss=:P0'
      'ORDER BY Nome')
    Left = 336
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_cfgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_cfg.Codigo'
    end
    object QrCNAB_cfgNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
  end
  object DsCNAB_cfg: TDataSource
    DataSet = QrCNAB_cfg
    Left = 364
    Top = 10
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 448
    Top = 11
  end
end
