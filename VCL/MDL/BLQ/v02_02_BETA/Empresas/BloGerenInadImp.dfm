object FmBloGerenInadimp: TFmBloGerenInadimp
  Left = 199
  Top = 170
  Caption = 'BLO-GEREN-007 :: Pesquisa Pagamentos'
  ClientHeight = 531
  ClientWidth = 791
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 791
    Height = 377
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 993
    ExplicitHeight = 519
    object Splitter2: TSplitter
      Left = 0
      Top = 160
      Width = 791
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitWidth = 993
    end
    object PnPesq1: TPanel
      Left = 0
      Top = 163
      Width = 791
      Height = 214
      Align = alClient
      TabOrder = 0
      Visible = False
      ExplicitWidth = 993
      ExplicitHeight = 356
      object Splitter1: TSplitter
        Left = 642
        Top = 1
        Width = 11
        Height = 354
        Align = alRight
      end
      object dmkDBGPesq: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 641
        Height = 354
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Data'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_Cfg'
            Title.Caption = 'Configura'#231#227'o'
            Width = 138
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Boleto'
            Title.Caption = 'Fatura'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CREDITO'
            Title.Caption = 'Valor'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MEZ_TXT'
            Title.Caption = 'M'#234's'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VCTO_TXT'
            Title.Caption = 'Vencto'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PAGO'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SALDO'
            Title.Caption = 'Saldo'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cliente'
            Title.Caption = 'ID Entidade'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEENT'
            Title.Caption = 'Entidade'
            Width = 182
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEEMP'
            Title.Caption = 'Empresa'
            Width = 182
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Width = 59
            Visible = True
          end>
        Color = clWindow
        DataSource = DsPesq3
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Data'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_Cfg'
            Title.Caption = 'Configura'#231#227'o'
            Width = 138
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Boleto'
            Title.Caption = 'Fatura'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CREDITO'
            Title.Caption = 'Valor'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MEZ_TXT'
            Title.Caption = 'M'#234's'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VCTO_TXT'
            Title.Caption = 'Vencto'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PAGO'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SALDO'
            Title.Caption = 'Saldo'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cliente'
            Title.Caption = 'ID Entidade'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEENT'
            Title.Caption = 'Entidade'
            Width = 182
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEEMP'
            Title.Caption = 'Empresa'
            Width = 182
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Width = 59
            Visible = True
          end>
      end
      object PnPesq2: TPanel
        Left = 653
        Top = 1
        Width = 339
        Height = 354
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 339
          Height = 354
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Genero'
              Title.Caption = 'Conta'
              Width = 35
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Valor'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Hist'#243'rico'
              Width = 563
              Visible = True
            end>
          Color = clWindow
          DataSource = DsIts3
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Genero'
              Title.Caption = 'Conta'
              Width = 35
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Valor'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Hist'#243'rico'
              Width = 563
              Visible = True
            end>
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 791
      Height = 160
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 993
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 916
        Height = 160
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 916
          Height = 160
          Align = alClient
          Caption = ' Configura'#231#227'o da pesquisa: '
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 764
            Top = 8
            Width = 139
            Height = 59
            Caption = ' Per'#237'odo de compet'#234'ncia:'
            TabOrder = 2
            object Label6: TLabel
              Left = 63
              Top = 20
              Width = 6
              Height = 13
              Caption = #224
            end
            object EdMezIni: TEdit
              Left = 8
              Top = 16
              Width = 52
              Height = 21
              TabOrder = 0
              OnExit = EdMezIniExit
              OnKeyDown = EdMezIniKeyDown
            end
            object EdMezFim: TEdit
              Left = 75
              Top = 16
              Width = 52
              Height = 21
              TabOrder = 1
              OnExit = EdMezFimExit
              OnKeyDown = EdMezFimKeyDown
            end
            object CkNaoMensais: TCheckBox
              Left = 8
              Top = 38
              Width = 119
              Height = 16
              Caption = 'Permite n'#227'o mensais.'
              Checked = True
              State = cbChecked
              TabOrder = 2
            end
          end
          object GBEmissao: TGroupBox
            Left = 541
            Top = 8
            Width = 216
            Height = 60
            Caption = ' Per'#237'odo de emiss'#227'o: '
            TabOrder = 0
            object CkIniDta: TCheckBox
              Left = 8
              Top = 14
              Width = 98
              Height = 16
              Caption = 'Data inicial:'
              TabOrder = 0
            end
            object TPIniDta: TdmkEditDateTimePicker
              Left = 8
              Top = 31
              Width = 98
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.000000000000000000
              Time = 0.777157974502188200
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object CkFimDta: TCheckBox
              Left = 114
              Top = 14
              Width = 98
              Height = 16
              Caption = 'Data final:'
              TabOrder = 2
            end
            object TPFimDta: TdmkEditDateTimePicker
              Left = 114
              Top = 31
              Width = 98
              Height = 21
              Date = 37636.000000000000000000
              Time = 0.777203761601413100
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
          object GBVencto: TGroupBox
            Left = 541
            Top = 72
            Width = 362
            Height = 64
            Caption = ' Per'#237'odo de vencimento: '
            TabOrder = 1
            object CkIniVct: TCheckBox
              Left = 8
              Top = 14
              Width = 98
              Height = 16
              Caption = 'Data inicial:'
              TabOrder = 0
            end
            object TPIniVct: TdmkEditDateTimePicker
              Left = 8
              Top = 31
              Width = 98
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.000000000000000000
              Time = 0.777157974502188200
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object CkFimVct: TCheckBox
              Left = 114
              Top = 14
              Width = 98
              Height = 16
              Caption = 'Data final:'
              TabOrder = 2
            end
            object TPFimVct: TdmkEditDateTimePicker
              Left = 114
              Top = 31
              Width = 98
              Height = 21
              Date = 37636.000000000000000000
              Time = 0.777203761601413100
              TabOrder = 3
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object CkFimPgt: TCheckBox
              Left = 218
              Top = 14
              Width = 99
              Height = 16
              Caption = 'Data pesquisa:'
              TabOrder = 4
              Visible = False
            end
            object TPFimPgt: TdmkEditDateTimePicker
              Left = 218
              Top = 31
              Width = 99
              Height = 21
              Date = 37636.000000000000000000
              Time = 0.777203761601413100
              TabOrder = 5
              Visible = False
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
          object PageControl1: TPageControl
            Left = 2
            Top = 15
            Width = 535
            Height = 143
            ActivePage = TabSheet1
            Align = alLeft
            TabOrder = 3
            ExplicitTop = 18
            ExplicitHeight = 140
            object TabSheet1: TTabSheet
              Caption = 'Filtros'
              object Panel11: TPanel
                Left = 0
                Top = 0
                Width = 527
                Height = 115
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                ExplicitWidth = 529
                ExplicitHeight = 119
                object Label2: TLabel
                  Left = 4
                  Top = 0
                  Width = 44
                  Height = 13
                  Caption = 'Empresa:'
                end
                object Label1: TLabel
                  Left = 4
                  Top = 39
                  Width = 45
                  Height = 13
                  Caption = 'Entidade:'
                end
                object EdEmpresa: TdmkEditCB
                  Left = 4
                  Top = 16
                  Width = 55
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdEmpresaChange
                  DBLookupComboBox = CBEmpresa
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBEmpresa: TdmkDBLookupComboBox
                  Left = 59
                  Top = 16
                  Width = 463
                  Height = 21
                  Color = clWhite
                  KeyField = 'Filial'
                  ListField = 'NOMEFILIAL'
                  ListSource = DModG.DsEmpresas
                  TabOrder = 1
                  dmkEditCB = EdEmpresa
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object EdEntidade: TdmkEditCB
                  Left = 4
                  Top = 55
                  Width = 55
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdEntidadeChange
                  DBLookupComboBox = CBEntidade
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBEntidade: TdmkDBLookupComboBox
                  Left = 59
                  Top = 55
                  Width = 463
                  Height = 21
                  Color = clWhite
                  KeyField = 'Codigo'
                  ListField = 'NOMEENT'
                  ListSource = DsEntidades
                  TabOrder = 3
                  dmkEditCB = EdEntidade
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object RGSituacao: TRadioGroup
                  Left = 4
                  Top = 80
                  Width = 518
                  Height = 36
                  Caption = ' Situa'#231#227'o: '
                  Columns = 2
                  ItemIndex = 1
                  Items.Strings = (
                    'Abertos'
                    'Atrasados')
                  TabOrder = 4
                  OnClick = RGSituacaoClick
                end
              end
            end
            object TabSheet2: TTabSheet
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Caption = 'Configura'#231#245'es / Ordena'#231#227'o'
              ImageIndex = 1
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 527
                Height = 115
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitHeight = 109
                object RGGrupos: TRadioGroup
                  Left = 0
                  Top = 0
                  Width = 68
                  Height = 115
                  Align = alLeft
                  Caption = ' Grupos: '
                  ItemIndex = 1
                  Items.Strings = (
                    '0'
                    '1'
                    '2'
                    '3')
                  TabOrder = 0
                  OnClick = RGGruposClick
                  ExplicitHeight = 109
                end
                object RGOrdem1: TRadioGroup
                  Left = 68
                  Top = 0
                  Width = 92
                  Height = 115
                  Align = alLeft
                  Caption = ' Ordem 1: '
                  ItemIndex = 3
                  Items.Strings = (
                    'Vencimento'
                    'Empresa'
                    'Entidade'
                    'Compet'#234'ncia'
                    'Emiss'#227'o')
                  TabOrder = 1
                  OnClick = RGOrdem1Click
                  ExplicitHeight = 109
                end
                object RGOrdem2: TRadioGroup
                  Left = 160
                  Top = 0
                  Width = 92
                  Height = 115
                  Align = alLeft
                  Caption = ' Ordem 2: '
                  ItemIndex = 4
                  Items.Strings = (
                    'Vencimento'
                    'Empresa'
                    'Entidade'
                    'Compet'#234'ncia'
                    'Emiss'#227'o')
                  TabOrder = 2
                  OnClick = RGOrdem2Click
                  ExplicitHeight = 109
                end
                object RGOrdem3: TRadioGroup
                  Left = 252
                  Top = 0
                  Width = 92
                  Height = 115
                  Align = alLeft
                  Caption = ' Ordem 3: '
                  ItemIndex = 0
                  Items.Strings = (
                    'Vencimento'
                    'Empresa'
                    'Entidade'
                    'Compet'#234'ncia'
                    'Emiss'#227'o')
                  TabOrder = 3
                  OnClick = RGOrdem3Click
                  ExplicitHeight = 109
                end
                object RGOrdem4: TRadioGroup
                  Left = 344
                  Top = 0
                  Width = 92
                  Height = 115
                  Align = alLeft
                  Caption = ' Ordem 4: '
                  ItemIndex = 4
                  Items.Strings = (
                    'Vencimento'
                    'Empresa'
                    'Entidade'
                    'Compet'#234'ncia'
                    'Emiss'#227'o')
                  TabOrder = 4
                  OnClick = RGOrdem4Click
                  ExplicitHeight = 109
                end
                object Panel9: TPanel
                  Left = 436
                  Top = 0
                  Width = 87
                  Height = 115
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 5
                  ExplicitHeight = 109
                  object RGSomas: TRadioGroup
                    Left = 0
                    Top = 0
                    Width = 87
                    Height = 64
                    Align = alTop
                    Caption = ' Somas: '
                    ItemIndex = 1
                    Items.Strings = (
                      'Nenhuma'
                      'Boletos')
                    TabOrder = 0
                  end
                  object RGImpressao: TRadioGroup
                    Left = 0
                    Top = 64
                    Width = 87
                    Height = 51
                    Align = alClient
                    Caption = ' Impress'#227'o: '
                    Enabled = False
                    ItemIndex = 0
                    Items.Strings = (
                      'Anal'#237'tica'
                      'Sint'#233'tica')
                    TabOrder = 1
                    ExplicitHeight = 45
                  end
                end
              end
            end
          end
        end
      end
      object Memo1: TMemo
        Left = 916
        Top = 0
        Width = 77
        Height = 160
        Align = alClient
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 791
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 993
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 899
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 257
        Height = 31
        Caption = 'Pesquisa Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 257
        Height = 31
        Caption = 'Pesquisa Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 257
        Height = 31
        Caption = 'Pesquisa Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 424
    Width = 791
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 566
    ExplicitWidth = 993
    object Panel8: TPanel
      Left = 2
      Top = 14
      Width = 989
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 468
    Width = 791
    Height = 63
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 610
    ExplicitWidth = 993
    object Panel12: TPanel
      Left = 2
      Top = 14
      Width = 989
      Height = 48
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 847
        Top = 0
        Width = 143
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtPesquisa: TBitBtn
        Tag = 22
        Left = 18
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 142
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtMulJur: TBitBtn
        Left = 265
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Multa/Juros'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtMulJurClick
      end
      object GroupBox3: TGroupBox
        Left = 390
        Top = 0
        Width = 412
        Height = 42
        Caption = ' Totais da pesquisa: '
        TabOrder = 4
        object Label5: TLabel
          Left = 138
          Top = 20
          Width = 28
          Height = 13
          Caption = 'Pago:'
          FocusControl = DBEdit2
        end
        object Label7: TLabel
          Left = 274
          Top = 20
          Width = 30
          Height = 13
          Caption = 'Saldo:'
          FocusControl = DBEdit3
        end
        object Label4: TLabel
          Left = 6
          Top = 20
          Width = 27
          Height = 13
          Caption = 'Valor:'
          FocusControl = DBEdit1
        end
        object DBEdit2: TDBEdit
          Left = 177
          Top = 18
          Width = 90
          Height = 21
          DataField = 'PAGO'
          DataSource = DsTot3
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 314
          Top = 18
          Width = 90
          Height = 21
          DataField = 'SALDO'
          DataSource = DsTot3
          TabOrder = 1
        end
        object DBEdit1: TDBEdit
          Left = 42
          Top = 18
          Width = 90
          Height = 21
          DataField = 'VALOR'
          DataSource = DsTot3
          TabOrder = 2
        end
      end
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT'
      'FROM entidades ent'
      'WHERE ent.Codigo > 0'
      'ORDER BY NOMEENT'
      '')
    Left = 69
    Top = 9
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEntidadesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 97
    Top = 9
  end
  object QrPesq3: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesq3AfterOpen
    BeforeClose = QrPesq3BeforeClose
    AfterScroll = QrPesq3AfterScroll
    Left = 740
    Top = 544
    object QrPesq3Data: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3CREDITO: TFloatField
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesq3PAGO: TFloatField
      FieldName = 'PAGO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesq3SALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesq3Mez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPesq3Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPesq3Compensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPesq3Juros: TFloatField
      FieldName = 'Juros'
    end
    object QrPesq3Multa: TFloatField
      FieldName = 'Multa'
    end
    object QrPesq3TOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object QrPesq3FatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPesq3PEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
    end
    object QrPesq3MEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Required = True
      Size = 7
    end
    object QrPesq3VCTO_TXT: TWideStringField
      FieldName = 'VCTO_TXT'
      Size = 10
    end
    object QrPesq3CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPesq3NOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrPesq3NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrPesq3Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesq3NOME_Cfg: TWideStringField
      FieldName = 'NOME_Cfg'
      Size = 50
    end
    object QrPesq3Boleto: TFloatField
      FieldName = 'Boleto'
    end
  end
  object DsPesq3: TDataSource
    DataSet = QrPesq3
    Left = 768
    Top = 544
  end
  object QrTot3: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTot3CalcFields
    SQL.Strings = (
      'SELECT SUM(lan.Credito) VALOR,'
      ''
      '  SUM((SELECT SUM(la3.Credito)'
      '  FROM lct la3'
      '  WHERE la3.ID_Pgto = lan.Controle'
      '  AND la3.Data <= "2007-01-31")) PAGO'
      ''
      'FROM lct lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN condimov  imv ON imv.Conta=lan.Depto'
      ''
      'WHERE FatID=600'
      ''
      'AND lan.Tipo=2'
      'AND car.ForneceI=38'
      ' AND lan.Mez > 0 '
      'AND lan.Vencimento  <= "2007-01-31"'
      'AND ( '
      '  (lan.Compensado > "2007-01-31")'
      '  OR (lan.Compensado = 0)'
      '  )'
      ''
      'ORDER BY imv.Unidade, lan.Data')
    Left = 740
    Top = 648
    object QrTot3VALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTot3PAGO: TFloatField
      FieldName = 'PAGO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTot3SALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsTot3: TDataSource
    DataSet = QrTot3
    Left = 768
    Top = 648
  end
  object frxPend_s0i0: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 39141.750700231500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  PageHeader1.Visible := True;'
      'end;'
      ''
      'procedure ReportTitle1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  PageHeader1.Visible := False;'
      'end;'
      ''
      'begin'
      '  if <LogoAdmiExiste> = True then'
      '    Picture1.LoadFromFile(<LogoAdmiCaminho>);'
      ''
      
        '  if <VARF_MAXGRUPO> > 0 then GroupHeader1.Visible := True else ' +
        'GroupHeader1.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 1 then GroupHeader2.Visible := True else ' +
        'GroupHeader2.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 3 then GroupHeader4.Visible := True else ' +
        'GroupHeader4.Visible := False;'
      ''
      
        '  if <VARF_MAXGRUPO> > 0 then GroupFooter1.Visible := True else ' +
        'GroupFooter1.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 1 then GroupFooter2.Visible := True else ' +
        'GroupFooter2.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 3 then GroupFooter4.Visible := True else ' +
        'GroupFooter4.Visible := False;'
      ''
      '  GroupHeader1.Condition := <VARF_GRUPO1>;'
      '  Memo27.Memo.Text := <VARF_HEADR1>;'
      '  Memo35.Memo.Text := <VARF_FOOTR1>;'
      ''
      '  GroupHeader2.Condition := <VARF_GRUPO2>;'
      '  Memo28.Memo.Text := <VARF_HEADR2>;'
      '  Memo34.Memo.Text := <VARF_FOOTR2>;'
      ''
      '  GroupHeader4.Condition := <VARF_GRUPO4>;'
      '  Memo30.Memo.Text := <VARF_HEADR4>;'
      '  Memo46.Memo.Text := <VARF_FOOTR4>;'
      ''
      '  Memo39.Memo.Text := <VARF_ID_VAL>;'
      'end.')
    OnGetValue = frxPend_s0i0GetValue
    Left = 741
    Top = 485
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPesq3
        DataSetName = 'frxDsPesq3'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 168.189076460000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        OnAfterPrint = 'ReportTitle1OnAfterPrint'
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 1.220470000000000000
          Top = 46.559060000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858267720000000000
          Top = 45.409400000000010000
          Width = 575.338590000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858267720000000000
          Top = 60.472480000000000000
          Width = 574.488560000000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Top = 113.385900000000000000
          Width = 170.928880000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 94.488250000000000000
          Width = 574.488560000000000000
          Height = 15.118105350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_PERIODO_VCT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 109.606370000000000000
          Width = 574.488560000000000000
          Height = 15.118105350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_PERIODO_DTA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 124.724490000000000000
          Width = 574.488560000000000000
          Height = 15.118105350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_PERIODO_PSQ]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858492280000000000
          Top = 139.842610000000000000
          Width = 574.488560000000000000
          Height = 15.118105350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_PERIODO_CPT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 79.370130000000000000
          Width = 574.488560000000000000
          Height = 15.118105350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_ENTIDADE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 748.346940000000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 154.960730000000000000
          Width = 113.385900000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 672.756340000000000000
          Top = 154.960730000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 154.960730000000000000
          Width = 181.417440000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Entidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 154.960730000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 154.960730000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 154.960730000000000000
          Width = 52.913380940000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Top = 154.960730000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 154.960730000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Compet.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Top = 154.960730000000000000
          Width = 45.354360000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_ID_TIT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 154.960730000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 154.960730000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 154.960730000000000000
          Width = 52.913380940000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pendente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 13.228346460000000000
        ParentFont = False
        Top = 476.220780000000000000
        Width = 755.906000000000000000
        DataSet = frxDsPesq3
        DataSetName = 'frxDsPesq3'
        RowCount = 0
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 113.385900000000000000
          Height = 13.228346460000000000
          DataField = 'NOMEEMP'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq3."NOMEEMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 181.417440000000000000
          Height = 13.228346460000000000
          DataField = 'NOMEENT'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq3."NOMEENT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 672.756340000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataField = 'Data'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq3."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataField = 'Credito'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."Credito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataField = 'PAGO'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."PAGO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataField = 'PEND_VAL'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."PEND_VAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'VCTO_TXT'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq3."VCTO_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'MEZ_TXT'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPesq3."MEZ_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Width = 45.354360000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '?????')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataField = 'Multa'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."Multa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataField = 'Juros'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."Juros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataField = 'TOTAL'
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq3."TOTAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 69.921296460000000000
        Top = 211.653680000000000000
        Visible = False
        Width = 755.906000000000000000
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 748.346940000000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PEND'#202'NCIAS DE CLIENTES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 56.692949999999990000
          Width = 113.385900000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 672.756340000000000000
          Top = 56.692949999999990000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 56.692949999999990000
          Width = 181.417440000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Entidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 56.692949999999990000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 56.692949999999990000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 56.692949999999990000
          Width = 52.913380940000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692949999999990000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 56.692949999999990000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Compet.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 710.551640000000000000
          Top = 56.692949999999990000
          Width = 45.354360000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_ID_TIT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 56.692949999999990000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 56.692949999999990000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 56.692949999999990000
          Width = 52.913380940000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pendente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 343.937230000000000000
        Width = 755.906000000000000000
        OnAfterPrint = 'GroupHeader1OnAfterPrint'
        Condition = 'frxDsPesq3."NOMEEMP"'
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779530000000022000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa: [frxDsPesq3."NOMEEMPCOND"]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 389.291590000000000000
        Width = 755.906000000000000000
        Condition = 'frxDsPesq3."NOMEENT"'
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 3.779530000000022000
          Width = 702.992580000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Entidade: [frxDsPesq3."NOMEENT"]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 434.645950000000000000
        Width = 755.906000000000000000
        Condition = 'frxDsPesq3."Vencimento"'
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Vencimento: [frxDsPesq3."VCTO_TXT"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 604.724800000000000000
        Width = 755.906000000000000000
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 362.834545590000000000
          Height = 20.787406460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Sub-total Empresa: [frxDsPesq3."NOMEEMP"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PAGO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PEND_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Multa">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Juros">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 559.370440000000000000
        Width = 755.906000000000000000
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 362.834545590000000000
          Height = 20.787406460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Sub-total Entidade: [frxDsPesq3."NOMEENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PAGO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PEND_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Multa">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Juros">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 514.016080000000000000
        Width = 755.906000000000000000
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 362.834513860000000000
          Height = 20.787406460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Sub-total Vencimento: [frxDsPesq3."VCTO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PAGO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PEND_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Multa">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Juros">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 56.692950000000000000
        Top = 733.228820000000000000
        Width = 755.906000000000000000
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 563.149970000000000000
          Top = 3.779530000000022000
          Width = 164.000000000000000000
          Height = 12.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina[Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 687.874460000000000000
        Width = 755.906000000000000000
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779530000000022000
          Width = 332.598305590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'SOMA TOTAL DA PESQUISA:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PAGO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."PEND_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Multa">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."Juros">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 3.779530000000022000
          Width = 52.913420000000000000
          Height = 13.228346460000000000
          DataSet = frxDsPesq3
          DataSetName = 'frxDsPesq3'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq3."TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPesq3: TfrxDBDataset
    UserName = 'frxDsPesq3'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'CREDITO=CREDITO'
      'PAGO=PAGO'
      'SALDO=SALDO'
      'Mez=Mez'
      'Vencimento=Vencimento'
      'Compensado=Compensado'
      'Controle=Controle'
      'Juros=Juros'
      'Multa=Multa'
      'TOTAL=TOTAL'
      'FatNum=FatNum'
      'PEND_VAL=PEND_VAL'
      'MEZ_TXT=MEZ_TXT'
      'VCTO_TXT=VCTO_TXT'
      'CliInt=CliInt'
      'NOMEEMP=NOMEEMP'
      'NOMEENT=NOMEENT'
      'Cliente=Cliente'
      'NOME_Cfg=NOME_Cfg'
      'Boleto=Boleto')
    DataSet = QrPesq3
    BCDToCurrency = False
    
    Left = 769
    Top = 485
  end
  object PMMulJur: TPopupMenu
    Left = 260
    Top = 424
    object AlterapercentualdeMulta1: TMenuItem
      Caption = 'Altera percentual de &Multa'
      OnClick = AlterapercentualdeMulta1Click
    end
    object AlterapercentualdeJurosmensais1: TMenuItem
      Caption = '&Altera percentual de &Juros mensais'
      OnClick = AlterapercentualdeJurosmensais1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 136
    Top = 480
    object Pendnciasdeclientes1: TMenuItem
      Caption = 'Pend'#234'ncias de clientes'
      OnClick = Pendnciasdeclientes1Click
    end
    object Segundaviadebloquetos1: TMenuItem
      Caption = 'Segunda via de boletos'
      OnClick = Segundaviadebloquetos1Click
    end
  end
  object frxCondE2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 39342.853423206000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 508
    Top = 576
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
      end
      item
      end>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      Frame.Width = 0.100000000000000000
      MirrorMode = []
      object Memo129: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 904.063390000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO3]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo114: TfrxMemoView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 848.126382130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."SUB_TOT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Picture2: TfrxPictureView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 718.110700000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        Frame.Typ = []
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo113: TfrxMemoView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 768.756303390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo101: TfrxMemoView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 839.055660000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo85: TfrxMemoView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line13: TfrxLineView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 718.110700000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Style = fsDash
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo81: TfrxMemoView
        AllowVectorExport = True
        Left = 578.268090000000000000
        Top = 706.772110000000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'AUTENTICA'#199#195'O MEC'#194'NICA')
        ParentFont = False
      end
      object BarCode1: TfrxBarCodeView
        AllowVectorExport = True
        Left = 90.708661417322800000
        Top = 1058.268114410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Frame.Typ = []
        Rotation = 0
        ShowText = False
        TestLine = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ColorBar = clBlack
      end
      object Line14: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 687.874015750000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 791.811419060000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        AllowVectorExport = True
        Left = 226.771653540000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        AllowVectorExport = True
        Left = 294.803152050000000000
        Top = 737.008350000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 759.685530000000000000
        Height = 230.551330000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        AllowVectorExport = True
        Left = 430.866420000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        AllowVectorExport = True
        Left = 370.393940000000000000
        Top = 816.378480000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line23: TfrxLineView
        AllowVectorExport = True
        Left = 291.023810000000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        AllowVectorExport = True
        Left = 228.661417320000000000
        Top = 839.055660000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        AllowVectorExport = True
        Left = 160.629921260000000000
        Top = 816.378480000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 865.512370000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line27: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 990.236615910000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line28: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 1050.709056850000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo82: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 737.008350000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo83: TfrxMemoView
        AllowVectorExport = True
        Left = 294.803340000000000000
        Top = 737.008350000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo84: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 759.685530000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo86: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 793.701300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo88: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 771.024120000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDsCond."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo89: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo90: TfrxMemoView
        AllowVectorExport = True
        Left = 166.299320000000000000
        Top = 816.378480000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo91: TfrxMemoView
        AllowVectorExport = True
        Left = 294.803340000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo92: TfrxMemoView
        AllowVectorExport = True
        Left = 374.173470000000000000
        Top = 816.378480000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo93: TfrxMemoView
        AllowVectorExport = True
        Left = 434.645950000000000000
        Top = 816.378480000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo94: TfrxMemoView
        AllowVectorExport = True
        Left = 166.299320000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo95: TfrxMemoView
        AllowVectorExport = True
        Left = 230.551330000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo96: TfrxMemoView
        AllowVectorExport = True
        Left = 294.803340000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo97: TfrxMemoView
        AllowVectorExport = True
        Left = 434.645950000000000000
        Top = 839.055660000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo98: TfrxMemoView
        AllowVectorExport = True
        Left = 427.086890000000000000
        Top = 848.504334880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Fill.BackColor = clWhite
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo99: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 793.701300000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo100: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 816.378480000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo102: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 865.512370000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo103: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 891.969080000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo104: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 918.425790000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo105: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 941.102970000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo106: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 967.559680000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line29: TfrxLineView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 890.079135590000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line30: TfrxLineView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 914.646064720000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line31: TfrxLineView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 939.212993860000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line32: TfrxLineView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 963.779922990000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo109: TfrxMemoView
        AllowVectorExport = True
        Left = 589.606680000000000000
        Top = 1054.488870000000000000
        Width = 151.181200000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo110: TfrxMemoView
        AllowVectorExport = True
        Left = 619.842920000000000000
        Top = 1035.591220000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo111: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 866.268114410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo115: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 801.260360000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDsCond."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo116: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 825.449216770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo117: TfrxMemoView
        AllowVectorExport = True
        Left = 162.519790000000000000
        Top = 825.449216770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."Boleto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo118: TfrxMemoView
        AllowVectorExport = True
        Left = 294.803340000000000000
        Top = 825.449216770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."EspecieDoc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo119: TfrxMemoView
        AllowVectorExport = True
        Left = 374.173470000000000000
        Top = 825.449216770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."Aceite"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo120: TfrxMemoView
        AllowVectorExport = True
        Left = 434.645950000000000000
        Top = 825.449216770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo122: TfrxMemoView
        AllowVectorExport = True
        Left = 166.299320000000000000
        Top = 848.126382130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."Carteira"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo123: TfrxMemoView
        AllowVectorExport = True
        Left = 230.551330000000000000
        Top = 848.126382130000000000
        Width = 56.692950000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."EspecieVal"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo124: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 825.449216770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo127: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 875.338980550000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO1]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo128: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 889.701185280000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo130: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 918.425790000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO4]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo131: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 932.787799450000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO5]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo132: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 947.150004170000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO6]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo133: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 961.512208900000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO7]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo134: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 975.874413620000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO8]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo121: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 801.260360000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_AGCodCed]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo135: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 729.449290000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDsCond."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object frxMemoView1: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 480.756030000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO3]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo2: TfrxMemoView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 424.819022130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."SUB_TOT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Picture1: TfrxPictureView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 294.803340000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        Frame.Typ = []
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo3: TfrxMemoView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 345.448943390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsBoletos."Vencto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo4: TfrxMemoView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 415.748300000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo5: TfrxMemoView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 336.378170000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line1: TfrxLineView
        AllowVectorExport = True
        Left = 3.779530000000000000
        Top = 294.803340000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo6: TfrxMemoView
        AllowVectorExport = True
        Left = 578.268090000000000000
        Top = 279.685220000000000000
        Width = 162.519790000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'SEGUNDA VIA')
        ParentFont = False
      end
      object BarCode2: TfrxBarCodeView
        AllowVectorExport = True
        Left = 90.708661420000000000
        Top = 634.960754410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Frame.Typ = []
        Rotation = 0
        ShowText = False
        TestLine = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ColorBar = clBlack
      end
      object Line2: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 336.378170000000000000
        Width = 687.874015750000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line3: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 368.504059060000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line4: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 393.071120000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line5: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 415.748300000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line6: TfrxLineView
        AllowVectorExport = True
        Left = 226.771653540000000000
        Top = 313.700990000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line7: TfrxLineView
        AllowVectorExport = True
        Left = 294.803152050000000000
        Top = 313.700990000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line8: TfrxLineView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 336.378170000000000000
        Height = 230.551330000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line9: TfrxLineView
        AllowVectorExport = True
        Left = 430.866420000000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line10: TfrxLineView
        AllowVectorExport = True
        Left = 370.393940000000000000
        Top = 393.071120000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line11: TfrxLineView
        AllowVectorExport = True
        Left = 291.023810000000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line12: TfrxLineView
        AllowVectorExport = True
        Left = 228.661417320000000000
        Top = 415.748300000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line33: TfrxLineView
        AllowVectorExport = True
        Left = 160.629921260000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line34: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 442.205010000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line35: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 566.929255910000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line36: TfrxLineView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 627.401696850000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo7: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 313.700990000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo8: TfrxMemoView
        AllowVectorExport = True
        Left = 294.803340000000000000
        Top = 313.700990000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo9: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 336.378170000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo11: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 370.393940000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo12: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 347.716760000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDsCond."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo14: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo15: TfrxMemoView
        AllowVectorExport = True
        Left = 166.299320000000000000
        Top = 393.071120000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo16: TfrxMemoView
        AllowVectorExport = True
        Left = 294.803340000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        AllowVectorExport = True
        Left = 374.173470000000000000
        Top = 393.071120000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo18: TfrxMemoView
        AllowVectorExport = True
        Left = 434.645950000000000000
        Top = 393.071120000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo19: TfrxMemoView
        AllowVectorExport = True
        Left = 166.299320000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo20: TfrxMemoView
        AllowVectorExport = True
        Left = 230.551330000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo21: TfrxMemoView
        AllowVectorExport = True
        Left = 294.803340000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo22: TfrxMemoView
        AllowVectorExport = True
        Left = 434.645950000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo23: TfrxMemoView
        AllowVectorExport = True
        Left = 427.086890000000000000
        Top = 425.196974880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Fill.BackColor = clWhite
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo24: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 370.393940000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo25: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo26: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 442.205010000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo27: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 468.661720000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo28: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 495.118430000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo29: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 517.795610000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo30: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 544.252320000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line37: TfrxLineView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 466.771775590000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line38: TfrxLineView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 491.338704720000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line39: TfrxLineView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 515.905633860000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line40: TfrxLineView
        AllowVectorExport = True
        Left = 602.834645670000000000
        Top = 540.472562990000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo33: TfrxMemoView
        AllowVectorExport = True
        Left = 589.606680000000000000
        Top = 631.181510000000000000
        Width = 151.181200000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo35: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 442.960754410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo36: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 377.953000000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDsCond."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo37: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 402.141856770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo38: TfrxMemoView
        AllowVectorExport = True
        Left = 162.519790000000000000
        Top = 402.141856770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsBoletos."Boleto"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo39: TfrxMemoView
        AllowVectorExport = True
        Left = 294.803340000000000000
        Top = 402.141856770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."EspecieDoc"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo40: TfrxMemoView
        AllowVectorExport = True
        Left = 374.173470000000000000
        Top = 402.141856770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."Aceite"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo41: TfrxMemoView
        AllowVectorExport = True
        Left = 434.645950000000000000
        Top = 402.141856770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo42: TfrxMemoView
        AllowVectorExport = True
        Left = 166.299320000000000000
        Top = 424.819022130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."Carteira"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo43: TfrxMemoView
        AllowVectorExport = True
        Left = 230.551330000000000000
        Top = 424.819022130000000000
        Width = 56.692950000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCond."EspecieVal"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo44: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 402.141856770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo46: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 452.031620550000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO1]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo47: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 466.393825280000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO2]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo48: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 495.118430000000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO4]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo49: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 509.480439450000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO5]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo50: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 523.842644170000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO6]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo51: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 538.204848900000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO7]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo52: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 552.567053620000000000
        Width = 544.252320000000000000
        Height = 14.362204720000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO8]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo53: TfrxMemoView
        AllowVectorExport = True
        Left = 604.724800000000000000
        Top = 377.953000000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_AGCodCed]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo54: TfrxMemoView
        AllowVectorExport = True
        Left = 52.913420000000000000
        Top = 306.141930000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDsCond."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo58: TfrxMemoView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 37.795300000000000000
        Width = 684.094930000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Memo.UTF8W = (
          'Boleto em duas vias')
        ParentFont = False
      end
      object Memo108: TfrxMemoView
        AllowVectorExport = True
        Left = 170.078850000000000000
        Top = 1041.259842520000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Sacador / Avalista')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo125: TfrxMemoView
        AllowVectorExport = True
        Left = 170.078850000000000000
        Top = 1020.473100000000000000
        Width = 449.764070000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDsInquilino."E_ALL"]')
        ParentFont = False
      end
      object Memo136: TfrxMemoView
        AllowVectorExport = True
        Left = 170.078850000000000000
        Top = 1005.354980000000000000
        Width = 449.764070000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDsInquilino."PROPRI_E_MORADOR"]')
        ParentFont = False
        WordWrap = False
      end
      object Memo10: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 1041.259842520000000000
        Width = 393.071120000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDsCond."NOMESAC_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Picture3: TfrxPictureView
        AllowVectorExport = True
        Left = 56.692950000000000000
        Top = 60.472480000000000000
        Width = 168.188976380000000000
        Height = 68.031496060000000000
        Frame.Typ = []
        HightQuality = True
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo_049: TfrxMemoView
        AllowVectorExport = True
        Left = 170.078850000000000000
        Top = 990.236860000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo_050: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 990.236860000000000000
        Width = 260.787401574803200000
        Height = 15.118112680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCond."NOMECLI"]  ')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo_055: TfrxMemoView
        AllowVectorExport = True
        Left = 491.338900000000000000
        Top = 990.236860000000000000
        Width = 173.858380000000000000
        Height = 30.236232680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'Unidade: [frxDsBoletos."Unidade"]')
        ParentFont = False
        WordWrap = False
      end
      object Memo31: TfrxMemoView
        AllowVectorExport = True
        Left = 619.842920000000000000
        Top = 612.283860000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo32: TfrxMemoView
        AllowVectorExport = True
        Left = 170.078850000000000000
        Top = 597.165740000000000000
        Width = 449.764070000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDsInquilino."E_ALL"]')
        ParentFont = False
      end
      object Memo34: TfrxMemoView
        AllowVectorExport = True
        Left = 170.078850000000000000
        Top = 582.047620000000000000
        Width = 449.764070000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          '[frxDsInquilino."PROPRI_E_MORADOR"]')
        ParentFont = False
        WordWrap = False
      end
      object Memo45: TfrxMemoView
        AllowVectorExport = True
        Left = 170.078850000000000000
        Top = 566.929500000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo55: TfrxMemoView
        AllowVectorExport = True
        Left = 226.771800000000000000
        Top = 566.929500000000000000
        Width = 309.921460000000000000
        Height = 15.118112680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCond."NOMECLI"]  ')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo56: TfrxMemoView
        AllowVectorExport = True
        Left = 536.693260000000000000
        Top = 566.929500000000000000
        Width = 128.504020000000000000
        Height = 30.236232680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        Frame.Typ = []
        HAlign = haRight
        Memo.UTF8W = (
          'Unidade: [frxDsBoletos."Unidade"]')
        ParentFont = False
        WordWrap = False
      end
    end
  end
  object QrIts3: TMySQLQuery
    Database = Dmod.MyDB
    Left = 740
    Top = 596
    object QrIts3Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrIts3Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrIts3Credito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrIts3Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsIts3: TDataSource
    DataSet = QrIts3
    Left = 768
    Top = 596
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 448
    Top = 11
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 420
    Top = 11
  end
end
