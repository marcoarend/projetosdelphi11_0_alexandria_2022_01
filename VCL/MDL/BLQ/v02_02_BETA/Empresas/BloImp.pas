unit BloImp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, Vcl.DBGrids,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass,
  frxExportPDF, Data.DB, mySQLDbTables, dmkGeral, UnDmkEnums, frxDBSet;

type
  TFmBloImp = class(TForm)
    frxPDFExport: TfrxPDFExport;
    frxVerso: TfrxReport;
    frxBloqA: TfrxReport;
    frxBloqB: TfrxReport;
    frxBloqE1: TfrxReport;
    frxBloqE2: TfrxReport;
    frxBloq: TfrxReport;
    procedure frxBloqAGetValue(const VarName: string; var Value: Variant);
    procedure frxBloqBGetValue(const VarName: string; var Value: Variant);
    procedure frxBloqE1GetValue(const VarName: string; var Value: Variant);
    procedure frxBloqE2GetValue(const VarName: string; var Value: Variant);
    procedure frxVersoGetValue(const VarName: string; var Value: Variant);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FCkZerado: Boolean;
    FTextoVia2, FTabLctA, FNossoNumero: String;
    FQueryBoletos, FQueryBoletosIts: TmySQLQuery;
    F_frxDsBoletos, F_frxDsBoletosIts: TfrxDBDataset;
    procedure BloqImpVersoGetValue(const VarName: string; var Value: Variant);
    procedure BloqImpGetValue(const VarName: string; var Value: Variant);
    procedure ConfiguraMasterData(frxReport: TfrxReport);
  public
    { Public declarations }
    function  ImprimeBloq(CkZerado: Boolean; TabLctA: String; QueryBoletos,
              QueryBoletosIts: TmySQLQuery; frxDsBoletos,
              frxDsBoletosIts: TfrxDBDataset; Quais: TselType; Como: TfrxImpComo;
              Arquivo: String; Filtro: TfrxCustomExportFilter;
              Grade: TDBGrid; TextoVia2: String): TfrxReport;
  end;

var
  FmBloImp: TFmBloImp;

implementation

uses UnInternalConsts, UnBancos, MyGlyfs, UnBloquetos, UnDmkProcFunc,
  ModuleBco, ModuleGeral, UnMyObjects, Meufrx, Module, UMySQLModule,
  ModBloqGerl, ModuleBloGeren, UnBloqGerl;

{$R *.dfm}

function TFmBloImp.ImprimeBloq(CkZerado: Boolean; TabLctA: String; QueryBoletos,
  QueryBoletosIts: TmySQLQuery; frxDsBoletos, frxDsBoletosIts: TfrxDBDataset;
  Quais: TselType; Como: TfrxImpComo; Arquivo: String;
  Filtro: TfrxCustomExportFilter; Grade: TDBGrid; TextoVia2: String): TfrxReport;
var
  prim, FVBol: Boolean;
  frxRep: TfrxReport;
  i: Integer;

  function GeraBloq(ModelBloq, Compe, CNAB_Cfg, Entidade: Integer): Boolean;
  var
    s: TMemoryStream;
    frxAtu: TfrxReport;
  begin
    if CNAB_Cfg = 0 then  //Fatura avulsa n�o imprime
    begin
      Result := True;
      Exit;
    end;
    //
    Result := False;
    try
      frxAtu := nil;
      //
      if not UBloqGerl.LiberaImpressaoBoletosCR(Dmod.MyDB, QueryBoletos) then
      begin
        Geral.MB_Aviso('A fatura n� ' +
          Geral.FFI(QueryBoletos.FieldByName('Boleto').AsFloat) +
          ' est� aguardando aprova��o do banco e n�o poder� ser impresso!');
        Exit;
      end;
      //
      DBloqGerl.ReopenCNAB_Cfg(CNAB_Cfg);
      DModG.ReopenEndereco(Entidade);
      DModG.ReopenEndereco2(DModG.QrFiliLogCodigo.Value);
      //
      case ModelBloq of
        0: frxAtu := frxVerso;
        1: frxAtu := frxBloqA;
        2: frxAtu := frxBloqB;
        3:
        begin
          case Compe of
            1: frxAtu := frxBloqE1;
            2: frxAtu := frxBloqE2;
          end;
        end;
      end;
      //
      frxRep := frxBloq;
      //
      frxBloq.OnGetValue         := frxAtu.OnGetValue;
      frxBloq.OnUserFunction     := frxAtu.OnUserFunction;
      frxBloq.ReportOptions.Name := 'Boletos';
      //
      if ModelBloq <> 0 then
      begin
        frxAtu.Variables['MeuLogoExiste']  := VAR_MEULOGOEXISTE;
        frxAtu.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
        frxAtu.Variables['LogoNFExiste']   := VAR_LOGONFEXISTE;
        frxAtu.Variables['LogoNFCaminho']  := QuotedStr(VAR_LOGONFPATH);
      end;
      //
      s := TMemoryStream.Create;
      frxAtu.SaveToStream(s);
      s.Position := 0;
      frxBloq.LoadFromStream(s);
      //
      //
      MyObjects.frxDefineDataSets(frxBloq, [
        F_frxDsBoletos,
        F_frxDsBoletosIts,
        DBloqGerl.frxDsCNAB_Cfg,
        DModG.frxDsEndereco,
        DModG.frxDsEndereco2,
        DModG.frxDsMaster
        ]);
      //
      ConfiguraMasterData(frxBloq);
      //
      if not prim then
      begin
        frxBloq.PrepareReport(True);
        prim := True;
      end else
        frxBloq.PrepareReport(False);
      Result := True;
    except
      Result := False;
    end;
  end;

  function frxExporta(Titulo: string; Quais: TSelType; Arquivo: String;
    Filtro: TfrxCustomExportFilter): Boolean;

    function frxGravaAtual(frx: TfrxReport; Titulo, Arquivo: String;
      Filtro: TfrxCustomExportFilter): Boolean;
    begin
      Result := frx.Export(Filtro);
    end;

  var
    i: Integer;
  begin
    Result := True;
    case Quais of
      istTodos:
      begin
        FQueryBoletos.First;
        while not FQueryBoletos.Eof do
        begin
          if Result then
          begin
            Result := GeraBloq(FQueryBoletos.FieldByName('ModelBloq').AsInteger,
                        FQueryBoletos.FieldByName('Compe').AsInteger,
                        FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger,
                        FQueryBoletos.FieldByName('Entidade').AsInteger);
            if Result then
              Result := frxGravaAtual(frxBloq, Titulo, Arquivo, Filtro);
          end;
          FQueryBoletos.Next;
        end;
      end;
      istSelecionados:
      begin
        if Grade = nil then
          Geral.MB_Erro('Grade n�o definida!')
        else
        begin
          with Grade.DataSource.DataSet do
          for i:= 0 to Grade.SelectedRows.Count-1 do
          begin
            GotoBookmark(Grade.SelectedRows.Items[i]);
            Result := GeraBloq(FQueryBoletos.FieldByName('ModelBloq').AsInteger,
                        FQueryBoletos.FieldByName('Compe').AsInteger,
                        FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger,
                        FQueryBoletos.FieldByName('Entidade').AsInteger);
            if Result then
              Result := frxGravaAtual(frxBloq, Titulo, Arquivo, Filtro);
          end;
        end;
      end;
      istAtual:
      begin
        Result := GeraBloq(FQueryBoletos.FieldByName('ModelBloq').AsInteger,
                    FQueryBoletos.FieldByName('Compe').AsInteger,
                    FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger,
                    FQueryBoletos.FieldByName('Entidade').AsInteger);
        if Result then
          Result := frxGravaAtual(frxBloq, Titulo, Arquivo, Filtro);
      end;
    end;
  end;

  procedure ImprimeBoleto;
  begin
    Application.CreateForm(TFmMeuFrx, FmMeuFrx);
    frxBloq.Preview := FmMeufrx.PvVer;
    //
    FmMeufrx.PvVer.OutlineWidth := frxBloq.PreviewOptions.OutlineWidth;
    FmMeufrx.PvVer.Zoom         := frxBloq.PreviewOptions.Zoom;
    FmMeufrx.PvVer.ZoomMode     := frxBloq.PreviewOptions.ZoomMode;
    FmMeufrx.UpdateZoom;
    //
    FmMeufrx.ShowModal;
    FmMeufrx.Destroy;
  end;

begin
  FCkZerado         := CkZerado;
  FTabLctA          := TabLctA;
  FQueryBoletos     := QueryBoletos;
  FQueryBoletosIts  := QueryBoletosIts;
  F_frxDsBoletos    := frxDsBoletos;
  F_frxDsBoletosIts := frxDsBoletosIts;
  FTextoVia2        := TextoVia2;
  //
  frxRep := nil;
  prim   := False;
  FVBol  := False;
  //
  frxBloq.Clear;
  //
  case Quais of
    istAtual:
    begin
      case Como of
        ficMostra: GeraBloq(FQueryBoletos.FieldByName('ModelBloq').AsInteger,
                     FQueryBoletos.FieldByName('Compe').AsInteger,
                     FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger,
                     FQueryBoletos.FieldByName('Entidade').AsInteger);
        ficExporta: frxExporta('Boletos', Quais, Arquivo, Filtro);
      end;
    end;
    istSelecionados:
    begin
      if Grade <> nil then
      begin
        if Grade.SelectedRows.Count > 1 then
        begin
          with Grade.DataSource.DataSet do
          for i:= 0 to Grade.SelectedRows.Count - 1 do
          begin
            GotoBookmark(Grade.SelectedRows.Items[i]);
            case Como of
              ficMostra: GeraBloq(FQueryBoletos.FieldByName('ModelBloq').AsInteger,
                           FQueryBoletos.FieldByName('Compe').AsInteger,
                           FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger,
                           FQueryBoletos.FieldByName('Entidade').AsInteger);
              ficExporta: frxExporta('Boletos', Quais, Arquivo, Filtro);
            end;
          end;
        end else
        begin
          case Como of
            ficMostra: GeraBloq(FQueryBoletos.FieldByName('ModelBloq').AsInteger,
                         FQueryBoletos.FieldByName('Compe').AsInteger,
                         FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger,
                         FQueryBoletos.FieldByName('Entidade').AsInteger);
            ficExporta: frxExporta('Boletos', Quais, Arquivo, Filtro);
          end;
        end;
      end else
        Geral.MB_Erro('Grade n�o definida!');
    end;
    istTodos:
    begin
      FQueryBoletos.First;
      while not FQueryBoletos.Eof do
      begin
        case Como of
          ficMostra: GeraBloq(FQueryBoletos.FieldByName('ModelBloq').AsInteger,
                       FQueryBoletos.FieldByName('Compe').AsInteger,
                       FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger,
                       FQueryBoletos.FieldByName('Entidade').AsInteger);
          ficExporta: frxExporta('Boletos', Quais, Arquivo, Filtro);
        end;
        //
        FQueryBoletos.Next;
      end;
    end;
  end;
  if Como = ficMostra then
    ImprimeBoleto;
  //
  //Verso
  prim := False;
  frxBloq.Clear;
  case Quais of
    istAtual:
    begin
      if (FQueryBoletos.FieldByName('BloqFV').AsInteger = 1) and (Como <> ficExporta) then
        FVBol := GeraBloq(0, FQueryBoletos.FieldByName('Compe').AsInteger,
                 FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger,
                 FQueryBoletos.FieldByName('Entidade').AsInteger);
    end;
    istSelecionados:
    begin
      if Grade <> nil then
      begin
        if Grade.SelectedRows.Count > 1 then
        begin
          with Grade.DataSource.DataSet do
          for i:= 0 to Grade.SelectedRows.Count - 1 do
          begin
            GotoBookmark(Grade.SelectedRows.Items[i]);
            if (FQueryBoletos.FieldByName('BloqFV').AsInteger = 1) and (Como <> ficExporta) then
              FVBol := GeraBloq(0, FQueryBoletos.FieldByName('Compe').AsInteger,
                       FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger,
                       FQueryBoletos.FieldByName('Entidade').AsInteger);
          end;
        end else
        begin
          if (FQueryBoletos.FieldByName('BloqFV').AsInteger = 1) and (Como <> ficExporta) then
            FVBol := GeraBloq(0, FQueryBoletos.FieldByName('Compe').AsInteger,
                     FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger,
                     FQueryBoletos.FieldByName('Entidade').AsInteger);
        end;
      end else
        Geral.MB_Erro('Grade n�o definida!');
    end;
    istTodos:
    begin
      FQueryBoletos.First;
      while not FQueryBoletos.Eof do
      begin
        if (FQueryBoletos.FieldByName('BloqFV').AsInteger = 1) and (Como <> ficExporta) then
          FVBol := GeraBloq(0, FQueryBoletos.FieldByName('Compe').AsInteger,
                   FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger,
                   FQueryBoletos.FieldByName('Entidade').AsInteger);
        //
        FQueryBoletos.Next;
      end;
    end;
  end;
  if FVBol then
    ImprimeBoleto;
  //
  if frxRep = nil then
    Result := nil
  else
    Result := frxBloq;
end;

procedure TFmBloImp.BloqImpGetValue(const VarName: string; var Value: Variant);
var
  Banco: Integer;
  DVB, NossoNumero_Txt: String;
  V, M, J, C, P: Double;
  GeraNossoGeral, CodigoDeCobranca, LinhaDigital: String;
  Status: Boolean;
begin
  P      := 0;
  C      := 0;
  V      := FQueryBoletos.FieldByName('Valor').AsFloat;
  M      := FQueryBoletos.FieldByName('MultaPerc').AsFloat;
  J      := FQueryBoletos.FieldByName('JurosPerc').AsFloat;
  Status := FQueryBoletos.FieldByName('TermoAceite').AsInteger = 0;
  //
  UBancos.GeraNossoNumero(
    FQueryBoletos.FieldByName('ModalCobr').AsInteger,
    FQueryBoletos.FieldByName('CedBanco').AsInteger,
    FQueryBoletos.FieldByName('CedAgencia').AsInteger,
    FQueryBoletos.FieldByName('CedPosto').AsInteger,
    FQueryBoletos.FieldByName('Boleto').AsFloat,
    FQueryBoletos.FieldByName('CedConta').AsString,
    FQueryBoletos.FieldByName('CartNum').AsString,
    FQueryBoletos.FieldByName('IDCobranca').AsString,
    Geral.SoNumero_TT(FQueryBoletos.FieldByName('CodEmprBco').AsString),
    FQueryBoletos.FieldByName('Vencto').AsDateTime,
    FQueryBoletos.FieldByName('TipoCobranca').AsInteger,
    FQueryBoletos.FieldByName('EspecieDoc').AsString,
    FQueryBoletos.FieldByName('CNAB').AsInteger,
    FQueryBoletos.FieldByName('CtaCooper').AsString,
    FQueryBoletos.FieldByName('LayoutRem').AsString,
    NossoNumero_Txt, FNossoNumero);
  //
  GeraNossoGeral   := NossoNumero_Txt;
  CodigoDeCobranca := UBancos.CodigoDeBarra_BoletoDeCobranca(
                        FQueryBoletos.FieldByName('CedBanco').AsInteger,
                        FQueryBoletos.FieldByName('CedAgencia').AsInteger,
                        FQueryBoletos.FieldByName('CorresBco').AsInteger,
                        FQueryBoletos.FieldByName('CorresAge').AsInteger,
                        FQueryBoletos.FieldByName('CedDAC_A').AsString,
                        FQueryBoletos.FieldByName('CedPosto').AsInteger,
                        FQueryBoletos.FieldByName('CedConta').AsString,
                        FQueryBoletos.FieldByName('CedDAC_C').AsString,
                        FQueryBoletos.FieldByName('CorresCto').AsString,
                        9, 3, 1, FNossoNumero,
                        FQueryBoletos.FieldByName('CodEmprBco').AsString,
                        FQueryBoletos.FieldByName('CartNum').AsString,
                        FQueryBoletos.FieldByName('CART_IMP').AsString,
                        FQueryBoletos.FieldByName('IDCobranca').AsString,
                        FQueryBoletos.FieldByName('OperCodi').AsString,
                        FQueryBoletos.FieldByName('Vencto').AsDateTime,
                        FQueryBoletos.FieldByName('Valor').AsFloat, 0, 0, not FCkZerado,
                        FQueryBoletos.FieldByName('ModalCobr').AsInteger,
                        FQueryBoletos.FieldByName('LayoutRem').AsString);
  LinhaDigital := UBancos.LinhaDigitavel_BoletoDeCobranca(CodigoDeCobranca);
  //
  if (FQueryBoletos.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
    or (FQueryBoletos.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
  then
    FmMyGlyfs.PreparaLogoBanco(FQueryBoletos.FieldByName('CorresBco').AsInteger)
  else
    FmMyGlyfs.PreparaLogoBanco(FQueryBoletos.FieldByName('CedBanco').AsInteger);
  //
  if AnsiCompareText(VarName, 'VARF_NossoNumero') = 0 then
  begin
    Value := GeraNossoGeral;
  end else
  if AnsiCompareText(VarName, 'VAX') = 0 then
  begin
    if (FQueryBoletos.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (FQueryBoletos.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015) then
    begin
      Banco := FQueryBoletos.FieldByName('CorresBco').AsInteger;
      //
      DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end else
    begin
      Banco := FQueryBoletos.FieldByName('CedBanco').AsInteger;
      //
      if FQueryBoletos.FieldByName('DVB').AsString <> '?' then
        DVB :=  FQueryBoletos.FieldByName('DVB').AsString
      else
        DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end;
    Value := FormatFloat('000', Banco) + '-' + DVB;
  end else
  if AnsiCompareText(VarName, 'VARF_CODIGOBARRAS') = 0 then
  begin
    Value := CodigoDeCobranca;
  end else
  if AnsiCompareText(VarName, 'VARF_LINHADIGITAVEL') = 0 then
  begin
    Value := LinhaDigital;
  end else
  if AnsiCompareText(VarName, 'VARF_NUMDOC') = 0 then
  begin
    Value := UBloquetos.ObtemNumeroDocumento(
               DBloGeren.QrBloOpcoes.FieldByName('NumDocPad').AsInteger,
               FQueryBoletos.FieldByName('FatNum').AsFloat,
               FQueryBoletos.FieldByName('Boleto').AsFloat,
               FQueryBoletosIts.FieldByName('Lancto').AsInteger,
               FTabLctA)
  end else
  if AnsiCompareText(VarName, 'VARF_MESANOA') = 0 then
    Value := dmkPF.MesEAnoDoPeriodo(UBloquetos.ValidaPeriodo(
               FQueryBoletos.FieldByName('Periodo').AsInteger))
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_01') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(FQueryBoletos.FieldByName('Texto01').AsString, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_02') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(FQueryBoletos.FieldByName('Texto02').AsString, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_03') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(FQueryBoletos.FieldByName('Texto03').AsString, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_04') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(FQueryBoletos.FieldByName('Texto04').AsString, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_05') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(FQueryBoletos.FieldByName('Texto05').AsString, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_06') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(FQueryBoletos.FieldByName('Texto06').AsString, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_07') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(FQueryBoletos.FieldByName('Texto07').AsString, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_08') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(FQueryBoletos.FieldByName('Texto08').AsString, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_09') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(FQueryBoletos.FieldByName('Texto09').AsString, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VAR_INSTRUCAO_10') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(FQueryBoletos.FieldByName('Texto10').AsString, V, M, J, C, P)
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_1') = 0 then
    Value := FQueryBoletos.FieldByName('Aviso01').AsString
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_2') = 0 then
    Value := FQueryBoletos.FieldByName('Aviso02').AsString
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_3') = 0 then
    Value := FQueryBoletos.FieldByName('Aviso03').AsString
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_4') = 0 then
    Value := FQueryBoletos.FieldByName('Aviso04').AsString
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_5') = 0 then
    Value := FQueryBoletos.FieldByName('Aviso05').AsString
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_6') = 0 then
    Value := FQueryBoletos.FieldByName('Aviso06').AsString
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_7') = 0 then
    Value := FQueryBoletos.FieldByName('Aviso07').AsString
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_8') = 0 then
    Value := FQueryBoletos.FieldByName('Aviso08').AsString
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_9') = 0 then
    Value := FQueryBoletos.FieldByName('Aviso09').AsString
  else
  if AnsiCompareText(VarName, 'VARF_MENSAGEM_10') = 0 then
    Value := FQueryBoletos.FieldByName('Aviso10').AsString
  else
  if AnsiCompareText(VarName, 'LogoBancoExiste') = 0 then
  begin
    if (FQueryBoletos.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (FQueryBoletos.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      Value := FmMyGlyfs.LogoBancoExiste(FQueryBoletos.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.LogoBancoExiste(FQueryBoletos.FieldByName('CedBanco').AsInteger);
  end
  else
  if AnsiCompareText(VarName, 'LogoBancoPath') = 0 then
  begin
    if (FQueryBoletos.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (FQueryBoletos.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      Value := FmMyGlyfs.CaminhoLogoBanco(FQueryBoletos.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.CaminhoLogoBanco(FQueryBoletos.FieldByName('CedBanco').AsInteger);
  end else
  if AnsiCompareText(VarName, 'LogoFilialExiste') = 0 then
    Value := FileExists(DModG.QrParamsEmpLogo3x1.Value)
  else
  if AnsiCompareText(VarName, 'LogoFilialPath') = 0 then
    Value := DModG.QrParamsEmpLogo3x1.Value
  else
  if AnsiCompareText(VarName, 'VARF_TELEFONE_CLIINT') = 0 then
  begin
    if DModG.QrEndereco2TE1_TXT.Value <> '' then
      Value := 'Telefone: ' + DModG.QrEndereco2TE1_TXT.Value
    else
      Value := '';
  end else
  if AnsiCompareText(VarName, 'VARF_SITE_CLIINT') = 0 then
  begin
    if DModG.QrEndereco2SITE.Value <> '' then
      Value := 'Site: ' + DModG.QrEndereco2SITE.Value
    else
      Value := '';
  end else
  if AnsiCompareText(VarName, 'VAR_TXT_VIA2') = 0 then
    Value := FTextoVia2
  else 
  if AnsiCompareText(VarName, 'VAR_MOSTRA_VIA2') = 0 then
  begin
    if FTextoVia2 = '' then
      Value := False
    else
      Value := True;
  end   
  else
  if AnsiCompareText(VarName, 'VARF_STATUS') = 0 then
    Value := Status
  else
  if AnsiCompareText(VarName, 'VAR_InfNFseArreIts') = 0 then
    Value := DBloGeren.QrBloOpcoes.FieldByName('InfNFseArreIts').AsInteger;
end;

procedure TFmBloImp.frxBloqAGetValue(const VarName: string; var Value: Variant);
begin
  BloqImpGetValue(VarName, Value);
end;

procedure TFmBloImp.frxBloqBGetValue(const VarName: string; var Value: Variant);
begin
  BloqImpGetValue(VarName, Value);
end;

procedure TFmBloImp.frxBloqE1GetValue(const VarName: string;
  var Value: Variant);
begin
  BloqImpGetValue(VarName, Value);
end;

procedure TFmBloImp.frxBloqE2GetValue(const VarName: string;
  var Value: Variant);
begin
  BloqImpGetValue(VarName, Value);
end;

procedure TFmBloImp.frxVersoGetValue(const VarName: string; var Value: Variant);
begin
  BloqImpVersoGetValue(VarName, Value);
end;

procedure TFmBloImp.BloqImpVersoGetValue(const VarName: string; var Value: Variant);
var
  Calculo: Integer;
begin
  if AnsiCompareText(VarName, 'VARF_BLQ_AltuHeader') = 0 then
    Value := Int(Dmod.QrControle.FieldByName('BLQ_TopoAvisoV').AsInteger / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuRotulo') = 0 then
  begin
    Calculo := 26000 (*- 4500*) - Dmod.QrControle.FieldByName('BLQ_TopoAvisoV').AsInteger;
    Value := Int(Calculo / VAR_frCM);
  end
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuDestin') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_AltuDestin').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_MEsqDestin') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_MEsqDestin').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_LargDestin') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_LargDestin').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_TopoDestin') = 0 then
  begin
    Calculo := Dmod.QrControle.FieldByName('BLQ_TopoDestin').AsInteger -
               Dmod.QrControle.FieldByName('BLQ_TopoAvisoV').Value;
    Value := Int(Calculo / VAR_frCM);
  end
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuAvisoV') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_AltuAvisoV').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_MEsqAvisoV') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_MEsqAvisoV').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_LargAvisoV') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_LargAvisoV').AsInteger / VAR_frCM
  //
  else if AnsiCompareText(VarName, 'VARF_NOMEPROPRIET') = 0 then
    Value := FQueryBoletos.FieldByName('NOMEENT').AsString
  else if AnsiCompareText(VarName, 'VARF_AVISOVERSO') = 0 then
    Value := FQueryBoletos.FieldByName('AvisoVerso').AsString;
end;

procedure TFmBloImp.ConfiguraMasterData(frxReport: TfrxReport);
var
  i, Tot: Integer;
begin
  Tot := frxReport.ComponentCount;
  //
  for i := 0 to Tot - 1 do
  begin
    if UpperCase(frxReport.Components[i].Name) = 'MDBOLETOSITS' then
    begin
      if (frxReport.Components[i] is TfrxMasterData) then
        TfrxMasterData(frxReport.Components[i]).DataSet := F_frxDsBoletosIts;
    end;
  end;
end;

procedure TFmBloImp.FormCreate(Sender: TObject);
begin
  DBloGeren.ReopenBloOpcoes;
end;

end.
