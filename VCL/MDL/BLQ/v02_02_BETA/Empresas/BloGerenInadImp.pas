unit BloGerenInadImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, Db, mySQLDbTables, Grids,
  DBGrids, dmkDBGrid, Mask, ComCtrls, frxClass, frxDBSet, Variants, Menus,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UMySQLModule, dmkGeral,
  dmkEditDateTimePicker, UnDmkProcFunc, dmkImage, dmkPermissoes, UnDmkEnums,
  dmkCompoStore, DmkDAC_PF, UnProjGroup_Consts;

type
  TFmBloGerenInadImp = class(TForm)
    Panel1: TPanel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    PnPesq1: TPanel;
    dmkDBGPesq: TdmkDBGrid;
    PnPesq2: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Splitter1: TSplitter;
    QrPesq3: TmySQLQuery;
    DsPesq3: TDataSource;
    QrTot3: TmySQLQuery;
    DsTot3: TDataSource;
    QrPesq3Data: TDateField;
    QrPesq3PAGO: TFloatField;
    QrPesq3SALDO: TFloatField;
    QrPesq3Mez: TIntegerField;
    QrPesq3Vencimento: TDateField;
    QrPesq3Compensado: TDateField;
    QrPesq3Controle: TIntegerField;
    QrTot3VALOR: TFloatField;
    QrTot3PAGO: TFloatField;
    QrTot3SALDO: TFloatField;
    Panel3: TPanel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    EdMezIni: TEdit;
    EdMezFim: TEdit;
    CkNaoMensais: TCheckBox;
    GBEmissao: TGroupBox;
    CkIniDta: TCheckBox;
    TPIniDta: TdmkEditDateTimePicker;
    CkFimDta: TCheckBox;
    TPFimDta: TdmkEditDateTimePicker;
    GBVencto: TGroupBox;
    CkIniVct: TCheckBox;
    TPIniVct: TdmkEditDateTimePicker;
    CkFimVct: TCheckBox;
    TPFimVct: TdmkEditDateTimePicker;
    CkFimPgt: TCheckBox;
    TPFimPgt: TdmkEditDateTimePicker;
    Memo1: TMemo;
    frxPend_s0i0: TfrxReport;
    frxDsPesq3: TfrxDBDataset;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Splitter2: TSplitter;
    QrPesq3Juros: TFloatField;
    QrPesq3Multa: TFloatField;
    QrPesq3TOTAL: TFloatField;
    QrPesq3FatNum: TFloatField;
    QrPesq3PEND_VAL: TFloatField;
    QrPesq3MEZ_TXT: TWideStringField;
    QrPesq3VCTO_TXT: TWideStringField;
    QrPesq3CREDITO: TFloatField;
    PMMulJur: TPopupMenu;
    AlterapercentualdeMulta1: TMenuItem;
    AlterapercentualdeJurosmensais1: TMenuItem;
    QrPesq3CliInt: TIntegerField;
    Panel11: TPanel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    PMImprime: TPopupMenu;
    Pendnciasdeclientes1: TMenuItem;
    Segundaviadebloquetos1: TMenuItem;
    frxCondE2: TfrxReport;
    QrIts3: TmySQLQuery;
    DsIts3: TDataSource;
    QrIts3Genero: TIntegerField;
    QrIts3Descricao: TWideStringField;
    QrIts3Credito: TFloatField;
    QrEntidadesCodUsu: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    QrPesq3NOMEEMP: TWideStringField;
    QrPesq3NOMEENT: TWideStringField;
    QrPesq3Cliente: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel12: TPanel;
    PnSaiDesis: TPanel;
    BtPesquisa: TBitBtn;
    BtImprime: TBitBtn;
    BtMulJur: TBitBtn;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    QrPesq3NOME_Cfg: TWideStringField;
    QrPesq3Boleto: TFloatField;
    QrIts3Controle: TIntegerField;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label7: TLabel;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit1: TDBEdit;
    TabSheet2: TTabSheet;
    RGSituacao: TRadioGroup;
    Panel7: TPanel;
    RGGrupos: TRadioGroup;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    Panel9: TPanel;
    RGSomas: TRadioGroup;
    RGImpressao: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdCondImovChange(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure EdMezIniExit(Sender: TObject);
    procedure EdMezIniKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMezFimExit(Sender: TObject);
    procedure EdMezFimKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrTot3CalcFields(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxPend_s0i0GetValue(const VarName: String;
      var Value: Variant);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure RGOrdem3Click(Sender: TObject);
    procedure RGOrdem4Click(Sender: TObject);
    procedure EdCondGriChange(Sender: TObject);
    procedure QrPesq3AfterOpen(DataSet: TDataSet);
    procedure QrPesq3BeforeClose(DataSet: TDataSet);
    procedure BtMulJurClick(Sender: TObject);
    procedure AlterapercentualdeJurosmensais1Click(Sender: TObject);
    procedure AlterapercentualdeMulta1Click(Sender: TObject);
    procedure Pendnciasdeclientes1Click(Sender: TObject);
    procedure Segundaviadebloquetos1Click(Sender: TObject);
    procedure QrPesq3AfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure RGGruposClick(Sender: TObject);
    procedure RGSituacaoClick(Sender: TObject);
  private
    { Private declarations }
    FDtaPgt, FTabLctA, FTmpBloqInadCli: String;
    FTemDtP: Boolean;
    function  AlteraMulta(): Boolean;
    function  AlteraJuros(): Boolean;
    procedure ComplementaQuery3(Query: TMySQLQuery);
    procedure Pesquisa3();
    procedure SetaEImprimeFrx(Frx: TfrxReport; Titulo: String);
  public
    { Public declarations }
  end;

  var
  FmBloGerenInadimp: TFmBloGerenInadImp;

implementation

uses Module, Principal, UnInternalConsts, MyDBCheck, UnFinanceiro, UCreate,
  ModuleGeral, BloGerenInadimpBloq, UnMyObjects, MyListas, MyGlyfs;

{$R *.DFM}

var
  HEADERS: array of String;

const
  Ordens: array[0..4] of String = ('Vencimento', 'NOMEEMP', 'NOMEENT', 'Mez', 'Data');
  _Labels_: array[0..4] of String = ('Vencimento', 'NOMEEMP', 'NOMEENT', 'MEZ_TXT', 'Data');
                                     
procedure TFmBloGerenInadImp.BtSaidaClick(Sender: TObject);
begin
  if TFmBloGerenInadImp(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmBloGerenInadImp.FormActivate(Sender: TObject);
begin
  if TFmBloGerenInadImp(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmBloGerenInadImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloGerenInadImp.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmBloGerenInadImp.EdEmpresaChange(Sender: TObject);
begin
  {$IFDEF DEFINE_VARLCT}
    FTabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, DModG.QrEmpresasFilial.Value);
  {$ELSE}
    FTabLctA := VAR_LCT;
  {$ENDIF}
  PnPesq1.Visible := False;
  BtImprime.Enabled := False;
end;

procedure TFmBloGerenInadImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  Memo1.Visible := False;
  //
  SetLength(Headers, 6);
  HEADERS[0] := 'Vencimento';
  HEADERS[1] := 'Empresa';
  HEADERS[2] := 'Entidade';
  HEADERS[3] := 'Compet�ncia';
  HEADERS[4] := 'Emiss�o';
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  QrEntidades.Open;
  //
  TPIniDta.Date := Date;
  TPFimDta.Date := Date;
  //
  TPIniVct.Date := Date;
  TPFimVct.Date := Date;
  //
  TPFimPgt.Date := Date;
  //
  PageControl1.ActivePageIndex := 0;
  //
  CkFimPgt.Visible  := True;
  TPFimPgt.Visible  := True;
  //
  CkIniVct.Checked := False;
  CkIniDta.Checked := False;
  CkFimDta.Checked := False;
end;

procedure TFmBloGerenInadImp.EdEntidadeChange(Sender: TObject);
begin
  PnPesq1.Visible   := False;
  BtImprime.Enabled := False;
end;

procedure TFmBloGerenInadImp.EdCondImovChange(Sender: TObject);
begin
  PnPesq1.Visible   := False;
  BtImprime.Enabled := False;
end;

procedure TFmBloGerenInadImp.EdMezIniExit(Sender: TObject);
begin
  EdMezIni.Text := Geral.TST(EdMezIni.Text, False);
end;

procedure TFmBloGerenInadImp.EdMezIniKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Periodo: Integer;
begin
  if key in ([VK_DOWN, VK_UP, VK_F5, VK_F6]) then
  begin
    if (EdMezIni.Text = '') or (key=VK_F5) then
      Periodo := Geral.Periodo2000(Date)
    else begin
      Periodo := dmkPF.MensalToPeriodo(EdMezIni.Text);
      if key=VK_DOWN then Periodo := Periodo -1;
      if key=VK_UP   then Periodo := Periodo +1;
      if key=VK_F4   then Periodo := Periodo -1;
      if key=VK_F5   then Periodo := Periodo   ;
    end;
    EdMezIni.Text := dmkPF.PeriodoToMensal(Periodo);
  end;
end;

procedure TFmBloGerenInadImp.EdMezFimExit(Sender: TObject);
begin
  EdMezFim.Text := Geral.TST(EdMezFim.Text, False);
end;

procedure TFmBloGerenInadImp.EdMezFimKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Periodo: Integer;
begin
  if key in ([VK_DOWN, VK_UP, VK_F5, VK_F6]) then
  begin
    if (EdMezFim.Text = '') or (key=VK_F5) then
      Periodo := Geral.Periodo2000(Date)
    else begin
      Periodo := dmkPF.MensalToPeriodo(EdMezFim.Text);
      if key=VK_DOWN then Periodo := Periodo -1;
      if key=VK_UP   then Periodo := Periodo +1;
      if key=VK_F4   then Periodo := Periodo -1;
      if key=VK_F5   then Periodo := Periodo   ;
    end;
    EdMezFim.Text := dmkPF.PeriodoToMensal(Periodo);
  end;
end;

procedure TFmBloGerenInadImp.BtPesquisaClick(Sender: TObject);
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Empresa n�o definida!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Memo1.Lines.Clear;
    //
    Pesquisa3();
    dmkDBGPesq.DataSource := DsPesq3;
    DBEdit1.DataSource    := DsTot3;
    DBEdit2.DataSource    := DsTot3;
    DBEdit3.DataSource    := DsTot3;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBloGerenInadImp.ComplementaQuery3(Query: TMySQLQuery);
var
  CodEmp, CodEnt: Integer;
  Txt: String;
begin
  Query.SQL.Add('FROM arreits its ');
  Query.SQL.Add('LEFT JOIN ' + FTabLctA + ' lan ON lan.Controle = its.Lancto');
  Query.SQL.Add('LEFT JOIN carteiras car ON car.Codigo = lan.Carteira');
  Query.SQL.Add('LEFT JOIN entidades emp ON emp.Codigo = car.ForneceI');
  Query.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = lan.Cliente');
  Query.SQL.Add('LEFT JOIN cnab_cfg cna ON cna.Codigo = its.CNAB_Cfg');
  Query.SQL.Add('LEFT JOIN faturcfg fcf ON fcf.Codigo = its.Fatur_Cfg');
  Query.SQL.Add('LEFT JOIN prev pre ON pre.Codigo = its.Codigo');
  //
  // Reparcelados n�o s�o d�bitos
  Query.SQL.Add('WHERE lan.Reparcel=0');
  Query.SQL.Add('AND lan.Tipo=2');
  Query.SQL.Add('AND its.Controle <> 0');
  Query.SQL.Add('AND lan.FatNum>0');
  Query.SQL.Add('AND car.ForneceI="' + Geral.FF0(DModG.QrEmpresasCodigo.Value) + '"');
  Query.SQL.Add('');
  //
  CodEmp := Geral.IMV(EdEmpresa.Text);
  if CodEmp <> 0 then
    CodEmp := DModG.QrEmpresasCodigo.Value
  else
    CodEmp := 0;
  if CodEmp > 0 then
    Query.SQL.Add('AND car.ForneceI=' + FormatFloat('0', CodEmp));
  //
  CodEnt := Geral.IMV(EdEntidade.ValueVariant);
  if CodEnt > 0 then
    CodEnt := QrEntidadesCodigo.Value
  else
    CodEnt := 0;
  if CodEnt > 0 then
    Query.SQL.Add('AND lan.Cliente=' + FormatFloat('0', CodEnt));
  //
  Query.SQL.Add(Txt);
  //
  Query.SQL.Add(dmkPF.SQL_Mensal('lan.Mez', EdMezIni.Text, EdMezFim.Text,
    CkNaoMensais.Checked));

  Query.SQL.Add(dmkPF.SQL_Periodo('AND lan.Data ', TPIniDta.Date,
    TPFimDta.Date,
    CkIniDta.Checked and GBEmissao.Visible,
    CkFimDta.Checked and GBEmissao.Visible));

  Query.SQL.Add(dmkPF.SQL_Periodo('AND lan.Vencimento ', TPIniVct.Date,
    TPFimVct.Date,
    CkIniVct.Checked and GBVencto.Visible,
    CkFimVct.Checked and GBVencto.Visible));

  (* Data que deseja ver os d�bitos ou hoje se n�o definido*)
  if FTemDtP then
    Query.SQL.Add(
    'AND (' +
    '  (lan.Compensado > "' + FDtaPgt + '")' +
    '  OR (lan.Compensado < 2)' +
    '  )')
  else
    Query.SQL.Add(
    'AND ' +
    '   (lan.Compensado < 2)' +
    '  ');
  Query.SQL.Add('');
  Query.SQL.Add('');
end;

procedure TFmBloGerenInadImp.Pendnciasdeclientes1Click(Sender: TObject);
begin
  if RGSituacao.ItemIndex = 0 then
  begin
    SetaEImprimeFrx(frxPend_s0i0, 'Faturas em aberto')
  end else
  begin
    if not AlteraMulta() then Exit;
    if not AlteraJuros() then Exit;
    //
    SetaEImprimeFrx(frxPend_s0i0, 'Pend�ncias de clientes');
  end;
end;

procedure TFmBloGerenInadImp.Pesquisa3();
  function OrdemPesq: String;
  begin
    Result := 'ORDER BY ' +
    Ordens[RGOrdem1.ItemIndex] + ',' +
    Ordens[RGOrdem2.ItemIndex] + ',' +
    Ordens[RGOrdem3.ItemIndex] + ',' +
    Ordens[RGOrdem4.ItemIndex];
  end;
var
  DataAtual: String;
begin
  DataAtual := Geral.FDT(DModG.ObtemAgora, 1);
  //
  (* Data que deseja ver os d�bitos ou hoje se n�o definido*)
  QrPesq3.Close;
  if CkFimPgt.Checked and GBVencto.Visible then
  begin
    FTemDtP := True;
    FDtaPgt := Geral.FDT(TPFimPgt.Date, 1);
    if RGSomas.ItemIndex = 1 then
    begin
      Geral.MB_Aviso('N�o � poss�vel ao mesmo tempo somar ' +
      '(faturas) e selecionar uma data limite para pesquisa!');
      Exit;
    end;
  end else begin
    FTemDtP := False;
    FDtaPgt := Geral.FDT(Date, 1);
  end;
  //
  QrPesq3.SQL.Clear;
  QrPesq3.SQL.Add('SELECT lan.Cliente, lan.Data, lan.CliInt,');
  case RGSomas.ItemIndex of
    0:
    begin
      QrPesq3.SQL.Add('lan.Credito, lan.Pago,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS("' + DataAtual + '") - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000, 2), 0) Juros,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 0) Multa,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS("' + DataAtual + '") - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito TOTAL,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS("' + DataAtual + '") - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago SALDO,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS("' + DataAtual + '") - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago <0,0,');
      QrPesq3.SQL.Add('IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS("' + DataAtual + '") - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) PEND_VAL,');
      QrPesq3.SQL.Add('');
    end;
    1:
    begin
      QrPesq3.SQL.Add('SUM(lan.Credito) CREDITO, SUM(lan.Pago) PAGO,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('SUM(IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS("' + DataAtual + '") - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000, 2), 0)) Juros,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('SUM(IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 0)) Multa,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('SUM(IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS("' + DataAtual + '") - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito) TOTAL,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('SUM(IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS("' + DataAtual + '") - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) SALDO,');
      QrPesq3.SQL.Add('');
      QrPesq3.SQL.Add('IF(SUM(IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS("' + DataAtual + '") - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) <0,0,');
      QrPesq3.SQL.Add('SUM(IF(TO_DAYS("' + DataAtual + '")-TO_DAYS(lan.Vencimento) > 0,');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * (TO_DAYS("' + DataAtual + '") - TO_DAYS(lan.Vencimento)) *');
      QrPesq3.SQL.Add('  lan.MoraDia  / 3000 +');
      QrPesq3.SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago)) PEND_VAL,');
      QrPesq3.SQL.Add('');
    end;
    else
    begin
      QrPesq3.SQL.Add('?credito?');
      QrPesq3.SQL.Add('?Descricao?');
    end;
  end;
  QrPesq3.SQL.Add('lan.Mez, CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4)) MEZ_TXT,');
  QrPesq3.SQL.Add('lan.Vencimento, lan.Compensado, lan.Controle, lan.FatNum,');
  QrPesq3.SQL.Add('IF(lan.Vencimento=0, "", DATE_FORMAT(lan.Vencimento, "%d/%m/%y")) VCTO_TXT,');
  QrPesq3.SQL.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,');
  QrPesq3.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
  QrPesq3.SQL.Add('IF(its.CNAB_Cfg<>0, cna.Nome, fcf.Nome) NOME_Cfg,');
  QrPesq3.SQL.Add('lan.Ativo, lan.Vencimento NewVencto, its.Boleto, '); // Para segunda via de boleto
  QrPesq3.SQL.Add('IF(its.CNAB_Cfg<>0, 0, 1) + 0.000 Tipo, ');
  QrPesq3.SQL.Add('IF(its.CNAB_Cfg<>0, its.CNAB_Cfg, its.Fatur_Cfg) + 0.000 Config ');

  ComplementaQuery3(QrPesq3);

  if RGSituacao.ItemIndex = 1 then
  begin
    QrPesq3.SQL.Add('AND lan.Vencimento < "' + DataAtual + '"');
  end else
  begin
    QrPesq3.SQL.Add('AND lan.Vencimento >= "' + DataAtual + '"');
  end;

  case RGSomas.ItemIndex of
    0: QrPesq3.SQL.Add('');
    1: QrPesq3.SQL.Add('GROUP BY lan.ForneceI, Tipo, Config, its.Boleto, lan.Depto, its.Entidade, pre.Codigo, lan.Mez, lan.Vencimento');
    else QrPesq3.SQL.Add('?GROUP BY?');
  end;
  QrPesq3.SQL.Add(OrdemPesq);
  UMyMod.AbreQuery(QrPesq3, Dmod.MyDB);
  //
  PnPesq1.Visible := True;
end;

procedure TFmBloGerenInadImp.QrTot3CalcFields(DataSet: TDataSet);
begin
  QrTot3SALDO.Value := QrTot3VALOR.Value - QrTot3PAGO.Value;
end;

function TFmBloGerenInadImp.AlteraJuros(): Boolean;
var
  PercNum: Double;
  PercTxt: String;
begin
  Result  := False;
  PercTxt := '1,00';
  //
  if (QrPesq3.State <> dsInactive) and (QrPesq3.RecordCount > 0) then
  begin
    if InputQuery('Novo Percentual de Juros Mensais',
      'Informe o novo percentual de juros mensais', PercTxt) then
    begin
      PercNum := Geral.DMV(PercTxt);
      PercTxt := Geral.FFT(PercNum, 2, siPositivo);
      //
      if Geral.MB_Pergunta('Confirma a altera��o do percentual de juros ' +
        'mensais de todos itens pesquisados para ' + PercTxt + '%?') = ID_YES then
      begin
        Screen.Cursor := crHourGlass;
        try
          QrPesq3.First;
          while not QrPesq3.Eof do
          begin
            if (QrIts3.State <> dsInactive) and (QrIts3.RecordCount > 0) then
            begin
              QrIts3.First;
              while not QrIts3.Eof do
              begin
                UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False,
                  ['MoraDia'], ['CliInt', 'Controle'],
                  [PercNum], [QrPesq3CliInt.Value, QrIts3Controle.Value],
                  True, '', FTabLctA);
                //
                QrIts3.Next;
              end;
            end;
            QrPesq3.Next;
          end;
          Result := True;
        finally
          QrPesq3.Close;
          QrPesq3.Open;
          Screen.Cursor := crDefault;
        end;
      end;
    end;
  end;
end;

procedure TFmBloGerenInadImp.AlterapercentualdeJurosmensais1Click(Sender: TObject);
begin
  AlteraJuros();
end;

function TFmBloGerenInadImp.AlteraMulta(): Boolean;
var
  PercNum: Double;
  PercTxt: String;
begin
  Result  := False;
  PercTxt := '2,00';
  //
  if (QrPesq3.State <> dsInactive) and (QrPesq3.RecordCount > 0) then
  begin
    if InputQuery('Novo Percentual de Multa',
      'Informe o novo percentual multa:', PercTxt) then
    begin
      PercNum := Geral.DMV(PercTxt);
      PercTxt := Geral.FFT(PercNum, 2, siPositivo);
      //
      if Geral.MB_Pergunta('Confirma a altera��o do percentual de ' +
        'multa de todos itens pesquisados para ' + PercTxt + '%?') = ID_YES then
      begin
        Screen.Cursor := crHourGlass;
        try
          QrPesq3.First;
          while not QrPesq3.Eof do
          begin
            if (QrIts3.State <> dsInactive) and (QrIts3.RecordCount > 0) then
            begin
              QrIts3.First;
              while not QrIts3.Eof do
              begin
                UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False,
                  ['Multa'], ['CliInt', 'Controle'],
                  [PercNum], [QrPesq3CliInt.Value, QrIts3Controle.Value],
                  True, '', FTabLctA);
                //
                QrIts3.Next;
              end;
            end;
            QrPesq3.Next;
          end;
          Result := True;
        finally
          QrPesq3.Close;
          QrPesq3.Open;
          Screen.Cursor := crDefault;
        end;
      end;
    end;
  end;
end;

procedure TFmBloGerenInadImp.AlterapercentualdeMulta1Click(Sender: TObject);
begin
  AlteraMulta();
end;

procedure TFmBloGerenInadImp.BtMulJurClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMulJur, BtMulJur);
end;

procedure TFmBloGerenInadImp.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmBloGerenInadImp.Segundaviadebloquetos1Click(Sender: TObject);
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa pesquise novamente!') then
    Exit;
  //
  if DBCheck.CriaFm(TFmBloGerenInadImpBloq, FmBloGerenInadimpBloq, afmoNegarComAviso) then
  begin
    FTmpBloqInadCli                       := UCriar.RecriaTempTable('bloqinadcli', DModG.QrUpdPID1, False);
    FmBloGerenInadimpBloq.FTabLctA        := FTabLctA;
    FmBloGerenInadimpBloq.FTmpBloqInadCli := FTmpBloqInadCli;
    FmBloGerenInadimpBloq.FEmpresa        := DModG.QrEmpresasCodigo.Value;
    //
    DmodG.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add(DELETE_FROM + VAR_MyPID_DB_NOME + '.' + FTmpBloqInadCli + ';');
    Dmod.QrAux.SQL.Add('INSERT INTO ' + VAR_MyPID_DB_NOME + '.' + FTmpBloqInadCli + ' ');
    Dmod.QrAux.SQL.Add(QrPesq3.SQL.Text);
    Dmod.QrAux.Params := QrPesq3.Params;
    Dmod.QrAux.ExecSQL;
    //
    FmBloGerenInadimpBloq.ShowModal;
    FmBloGerenInadimpBloq.Destroy;
  end;
end;

procedure TFmBloGerenInadImp.SetaEImprimeFrx(Frx: TfrxReport; Titulo: String);
  function Footer(Indice: Integer): String;
  begin
    Result := 'Sub-total ' + Headers[Indice];
  end;
begin
  frx.Variables['LogoAdmiExiste']  := FileExists(Dmod.QrControle.FieldByName('MeuLogoPath').AsString);
  frx.Variables['LogoAdmiCaminho'] := QuotedStr(Dmod.QrControle.FieldByName('MeuLogoPath').AsString);
  frx.Variables['VARF_MAXGRUPO']   := RGGrupos.ItemIndex;

  frx.Variables['VARF_GRUPO1']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR1']   := ''''+Headers[RGOrdem1.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem1.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR1']   := ''''+Footer(RGOrdem1.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  frx.Variables['VARF_GRUPO2']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem2.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR2']   := ''''+Headers[RGOrdem2.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem2.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR2']   := ''''+Footer(RGOrdem2.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem2.ItemIndex]+'"]''';

  frx.Variables['VARF_GRUPO3']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem3.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR3']   := ''''+Headers[RGOrdem3.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem3.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR3']   := ''''+Footer(RGOrdem1.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  frx.Variables['VARF_GRUPO4']   := ''''+'frxDsPesq3."'+Ordens[RGOrdem4.ItemIndex]+'"''';
  frx.Variables['VARF_HEADR4']   := ''''+Headers[RGOrdem4.ItemIndex]+': [frxDsPesq3."'+_labels_[RGOrdem4.ItemIndex]+'"]''';
  frx.Variables['VARF_FOOTR4']   := ''''+Footer(RGOrdem1.ItemIndex)+': [frxDsPesq3."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  case RGSomas.ItemIndex of
    0: frx.Variables['VARF_ID_VAL'] := '''[frxDsPesq3."Controle"]''';
    1: frx.Variables['VARF_ID_VAL'] := '''[frxDsPesq3."FatNum"]''';
    else frx.Variables['VARF_ID_VAL'] := '';
  end;

  MyObjects.frxMostra(frx, Titulo);
end;

procedure TFmBloGerenInadImp.frxPend_s0i0GetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO_VCT') = 0 then
    Value := dmkPF.PeriodoImp2(TPIniVct.Date, TPFimVct.Date,
      CkIniVct.Checked, CkFimVct.Checked, 'Per�odo de vencimento: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_PERIODO_DTA') = 0 then
    Value := dmkPF.PeriodoImp2(TPIniDta.Date, TPFimDta.Date,
      CkIniDta.Checked, CkFimDta.Checked, 'Per�odo de emiss�o: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_PERIODO_PSQ') = 0 then
    Value := dmkPF.PeriodoImp2(0, TPFimPgt.Date,
      False, CkFimPgt.Checked, 'Data da pesquisa: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_PERIODO_CPT') = 0 then
    Value := dmkPF.PeriodoImpMezAno(EdMezIni.Text, EdMezFim.Text,
      'Per�odo de compet�ncia: ', '', '')
  else if AnsiCompareText(VarName, 'VARF_ID_TIT') = 0 then
  begin
    case RGSomas.ItemIndex of
      0: Value := 'Controle';
      1: Value := 'Fatura';
      else Value := '';
    end;
  end
  else if AnsiCompareText(VarName, 'VARF_TITULO') = 0 then
  begin
    case RGSituacao.ItemIndex of
      0:
      begin
        if CO_DMKID_APP = 4 then //Syndi2
          Value := 'TODOS VALORES ABERTOS DE COND�MINOS'
        else
          Value := 'TODOS VALORES ABERTOS DE CLIENTES';
      end;
      1:
      begin
        if CO_DMKID_APP = 4 then //Syndi2
          Value := 'PEND�NCIAS DE COND�MINOS'
        else
          Value := 'PEND�NCIAS DE CLIENTES';
      end
      else Value := RGSituacao.Items[RGSituacao.ItemIndex];
    end;
  end
  else begin
    case PageControl1.ActivePageIndex of
      0:
      begin
        if AnsiCompareText(VarName, 'VARF_EMPRESA') = 0 then
          Value := dmkPF.ParValueCodTxt('Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant)
        else if AnsiCompareText(VarName, 'VARF_ENTIDADE') = 0 then
          Value := dmkPF.ParValueCodTxt('Entidade: ', CBEntidade.Text, EdEntidade.ValueVariant)
      end;
      1:
      begin
        case RGSomas.ItemIndex of
          0:
          begin
            if AnsiCompareText(VarName, 'VARF_EMPRESA') = 0 then
              Value := dmkPF.ParValueCodTxt('Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant)
            else if AnsiCompareText(VarName, 'VARF_ENTIDADE') = 0 then
              Value := dmkPF.ParValueCodTxt('Entidade: ', CBEntidade.Text, EdEntidade.ValueVariant)
          end;
          1:
          begin
            if AnsiCompareText(VarName, 'VARF_EMPRESA') = 0 then
              Value := dmkPF.ParValueCodTxt('Grupo de pesquisa: ', CBEmpresa.Text, EdEmpresa.ValueVariant)
            else if AnsiCompareText(VarName, 'VARF_ENTIDADE') = 0 then
              Value := ''
          end;
        end;
      end;
    end;
  end;
end;

procedure TFmBloGerenInadImp.RGGruposClick(Sender: TObject);
begin
  PnPesq1.Visible   := False;
  BtImprime.Enabled := False;
end;

procedure TFmBloGerenInadImp.RGOrdem1Click(Sender: TObject);
begin
  PnPesq1.Visible   := False;
  BtImprime.Enabled := False;
end;

procedure TFmBloGerenInadImp.RGOrdem2Click(Sender: TObject);
begin
  PnPesq1.Visible   := False;
  BtImprime.Enabled := False;
end;

procedure TFmBloGerenInadImp.RGOrdem3Click(Sender: TObject);
begin
  PnPesq1.Visible   := False;
  BtImprime.Enabled := False;
end;

procedure TFmBloGerenInadImp.RGOrdem4Click(Sender: TObject);
begin
  PnPesq1.Visible   := False;
  BtImprime.Enabled := False;
end;

procedure TFmBloGerenInadImp.RGSituacaoClick(Sender: TObject);
begin
  PnPesq1.Visible   := False;
  BtImprime.Enabled := False;
end;

procedure TFmBloGerenInadImp.EdCondGriChange(Sender: TObject);
begin
  PnPesq1.Visible   := False;
  BtImprime.Enabled := False;
end;

procedure TFmBloGerenInadImp.QrPesq3AfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := QrPesq3.RecordCount > 0;
  BtMulJur.Enabled  := QrPesq3.RecordCount > 0;
end;

procedure TFmBloGerenInadImp.QrPesq3AfterScroll(DataSet: TDataSet);
var
  Cliente, FatNum, Mez, Vencto: String;
begin
  Cliente := FormatFloat('0', QrPesq3Cliente.Value);
  FatNum  := FormatFloat('0', QrPesq3FatNum.Value);
  Mez     := FormatFloat('0', QrPesq3Mez.Value);
  Vencto  := Geral.FDT(QrPesq3Vencimento.Value, 1);
  //
  QrIts3.Close;
  QrIts3.SQL.Clear;
  QrIts3.SQL.Add('SELECT lct.Genero, lct.Descricao, lct.Credito, lct.Controle');
  QrIts3.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrIts3.SQL.Add('WHERE lct.Cliente=' + Cliente);
  QrIts3.SQL.Add('AND lct.FatNum=' + FatNum);
  QrIts3.SQL.Add('AND lct.Mez=' + Mez);
  QrIts3.SQL.Add('AND lct.Vencimento="' + Vencto + '"');
  QrIts3.Open;
end;

procedure TFmBloGerenInadImp.QrPesq3BeforeClose(DataSet: TDataSet);
begin
  QrIts3.Close;
  BtImprime.Enabled := False;
end;

end.

