object FmBloOpcoes: TFmBloOpcoes
  Left = 339
  Top = 185
  Caption = 'BLO-OPCAO-001 :: Op'#231#245'es Faturas'
  ClientHeight = 615
  ClientWidth = 940
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PageControl1: TPageControl
    Left = 0
    Top = 59
    Width = 940
    Height = 424
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = TabSheet4
    Align = alClient
    TabHeight = 25
    TabOrder = 0
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Geral'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 932
        Height = 389
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label3: TLabel
          Left = 10
          Top = 228
          Width = 472
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Caso n'#227'o haja n'#250'mero de fatura ou nota fiscal ser'#225' utilizado o n' +
            #250'mero do boleto'
        end
        object GroupBox1: TGroupBox
          Left = 10
          Top = 14
          Width = 576
          Height = 139
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Envio de e-mail de boleto: '
          TabOrder = 0
          object Label4: TLabel
            Left = 10
            Top = 82
            Width = 256
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pr'#233' e-mail para envio de aviso de reajuste:'
          end
          object SpeedButton1: TSpeedButton
            Left = 542
            Top = 106
            Width = 25
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object Label98: TLabel
            Left = 10
            Top = 23
            Width = 217
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo de e-mail para envio de boleto:'
          end
          object SpeedButton15: TSpeedButton
            Left = 542
            Top = 47
            Width = 25
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton15Click
          end
          object EdPreMailReaj: TdmkEditCB
            Left = 10
            Top = 106
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PreMailReaj'
            UpdCampo = 'PreMailReaj'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPreMailReaj
            IgnoraDBLookupComboBox = False
          end
          object CBPreMailReaj: TdmkDBLookupComboBox
            Left = 80
            Top = 106
            Width = 455
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPreEmail
            TabOrder = 3
            dmkEditCB = EdPreMailReaj
            QryCampo = 'PreMailReaj'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEntiTipCto: TdmkEditCB
            Left = 10
            Top = 47
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEntiTipCto
            IgnoraDBLookupComboBox = False
          end
          object CBEntiTipCto: TdmkDBLookupComboBox
            Left = 81
            Top = 47
            Width = 454
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsEntiTipCto
            TabOrder = 1
            dmkEditCB = EdEntiTipCto
            QryCampo = 'EntiTipCto'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object RGPerGerBol: TRadioGroup
          Left = 593
          Top = 14
          Width = 326
          Height = 55
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Per'#237'odo no gerenciamento das faturas: '
          Columns = 3
          ItemIndex = 1
          Items.Strings = (
            'Anterior'
            'Atual'
            'Pr'#243'ximo')
          TabOrder = 2
        end
        object RGAbreEmTab: TRadioGroup
          Left = 593
          Top = 139
          Width = 326
          Height = 99
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Modo de abertura da janela de geren. de faturas'
          ItemIndex = 0
          Items.Strings = (
            'Janela'
            'Aba'
            'Escolher na abertura')
          TabOrder = 4
        end
        object RGVisPadrao: TRadioGroup
          Left = 593
          Top = 76
          Width = 326
          Height = 56
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Modo de visualiza'#231#227'o padr'#227'o'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Mensal'
            'Pesquisa')
          TabOrder = 3
        end
        object RGNumDocPad: TRadioGroup
          Left = 10
          Top = 159
          Width = 576
          Height = 62
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' N'#250'mero do documento no boleto'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'N'#250'mero do boleto'
            'N'#250'mero da fatura'
            'N'#250'mero da nota fiscal')
          TabOrder = 1
        end
        object RGVctoFeriado: TRadioGroup
          Left = 593
          Top = 245
          Width = 326
          Height = 85
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Vencimento da fatura'
          ItemIndex = 0
          Items.Strings = (
            'Utilizar a data selecionada'
            'Pular feriados e dias n'#227'o '#250'teis')
          TabOrder = 5
        end
        object CkInfNFseArreIts: TCheckBox
          Left = 10
          Top = 253
          Width = 576
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Informar n'#250'mero da nota fiscal quando tiver nos itens do boleto'
          TabOrder = 6
        end
      end
    end
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Modelo padr'#227'o'
      ImageIndex = 1
      object RGModelBloq: TdmkRadioGroup
        Left = 0
        Top = 0
        Width = 932
        Height = 291
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Modelo de impress'#227'o do boleto: '
        Items.Strings = (
          'ModuleBloGeren.PreencheModelosBloq')
        TabOrder = 0
        OnClick = RGModelBloqClick
        QryCampo = 'ModelBloq'
        UpdCampo = 'ModelBloq'
        UpdType = utYes
        OldValor = 0
      end
      object RGCompe: TdmkRadioGroup
        Left = 0
        Top = 291
        Width = 932
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Caption = ' Ficha de compensa'#231#227'o (modelo E): '
        Columns = 3
        Items.Strings = (
          '??'
          '1 (uma via)'
          '2 (duas vias)')
        TabOrder = 1
        QryCampo = 'Compe'
        UpdCampo = 'Compe'
        UpdType = utYes
        OldValor = 0
      end
      object RGBloqFV: TdmkRadioGroup
        Left = 0
        Top = 340
        Width = 932
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Caption = 'Frente e verso?'
        Columns = 2
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 2
        QryCampo = 'BloqFV'
        UpdCampo = 'BloqFV'
        UpdType = utYes
        OldValor = 0
      end
    end
    object TabSheet3: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'WEB'
      ImageIndex = 2
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 932
        Height = 389
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 12
          Top = 12
          Width = 579
          Height = 166
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Configura'#231#245'es base de dados WEB'
          TabOrder = 0
          object Label1: TLabel
            Left = 10
            Top = 28
            Width = 142
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade m'#225'xima de '
          end
          object Label2: TLabel
            Left = 256
            Top = 28
            Width = 295
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'dias para a configura'#231#227'o do vencimento da fatura'
          end
          object Label20: TLabel
            Left = 374
            Top = 64
            Width = 145
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'dias do seu vencimento.'
          end
          object Label19: TLabel
            Left = 10
            Top = 64
            Width = 254
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Considerar que o e-mail foi recebido ap'#243's '
          end
          object SpeedButton2: TSpeedButton
            Left = 542
            Top = 126
            Width = 25
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object Label5: TLabel
            Left = 10
            Top = 102
            Width = 497
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'Pr'#233' e-mail para re-envio de aviso da falta de confirma'#231#227'o do rec' +
              'ebimento da fatura:'
          end
          object EdMaxDias: TdmkEdit
            Left = 153
            Top = 25
            Width = 99
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '60'
            QryCampo = 'MaxDias'
            UpdCampo = 'MaxDias'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 60
            ValWarn = False
          end
          object EdDdAutConfMail: TdmkEdit
            Left = 268
            Top = 60
            Width = 99
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '60'
            QryCampo = 'DdAutConfMail'
            UpdCampo = 'DdAutConfMail'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 60
            ValWarn = False
          end
          object CBPreMailReenv: TdmkDBLookupComboBox
            Left = 80
            Top = 126
            Width = 455
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPreEmailRe
            TabOrder = 3
            dmkEditCB = EdPreMailReenv
            QryCampo = 'PreMailReenv'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPreMailReenv: TdmkEditCB
            Left = 10
            Top = 126
            Width = 69
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PreMailReenv'
            UpdCampo = 'PreMailReenv'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPreMailReenv
            IgnoraDBLookupComboBox = False
          end
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'WhatsApp'
      ImageIndex = 3
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 932
        Height = 389
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox3: TGroupBox
          Left = 0
          Top = 0
          Width = 932
          Height = 90
          Align = alTop
          Caption = ' Envio de WhatsApp de boleto: '
          TabOrder = 0
          object Label6: TLabel
            Left = 12
            Top = 23
            Width = 228
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo de telefone para envio de boleto:'
          end
          object SBWhatsApp_EntiTipCto: TSpeedButton
            Left = 542
            Top = 47
            Width = 25
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SBWhatsApp_EntiTipCtoClick
          end
          object EdWhatsApp_EntiTipCto: TdmkEditCB
            Left = 12
            Top = 47
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBWhatsApp_EntiTipCto
            IgnoraDBLookupComboBox = False
          end
          object CBWhatsApp_EntiTipCto: TdmkDBLookupComboBox
            Left = 83
            Top = 47
            Width = 454
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsEntiTipCtoWA
            TabOrder = 1
            dmkEditCB = EdWhatsApp_EntiTipCto
            QryCampo = 'EntiTipCto'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object GroupBox4: TGroupBox
          Left = 0
          Top = 90
          Width = 932
          Height = 299
          Align = alClient
          Caption = 'Mensagem'
          TabOrder = 1
          object MeWhatsApp_Msg: TMemo
            Left = 2
            Top = 18
            Width = 643
            Height = 279
            Align = alClient
            TabOrder = 0
            ExplicitWidth = 928
          end
          object LVItens: TListView
            Left = 645
            Top = 18
            Width = 285
            Height = 279
            Align = alRight
            Columns = <>
            TabOrder = 1
            OnDblClick = LVItensDblClick
            ExplicitLeft = 650
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 940
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 881
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 822
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 230
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es Faturas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 230
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es Faturas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 230
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es Faturas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 483
    Width = 940
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 936
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 537
    Width = 940
    Height = 78
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 18
      Width = 936
      Height = 58
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 759
        Top = 0
        Width = 177
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 22
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntiTipCto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM entitipcto'
      'WHERE Ativo = 1'
      'AND Tipo IN (0,2)'
      'ORDER BY Nome')
    Left = 476
    Top = 8
    object QrEntiTipCtoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiTipCtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntiTipCtoNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiTipCto: TDataSource
    DataSet = QrEntiTipCto
    Left = 504
    Top = 8
  end
  object VUEntiTipCto: TdmkValUsu
    dmkEditCB = EdEntiTipCto
    Panel = Panel1
    QryCampo = 'EntiTipCto'
    UpdCampo = 'EntiTipCto'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 532
    Top = 8
  end
  object QrBloOpcoes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM bloopcoes')
    Left = 420
    Top = 8
  end
  object DsBloOpcoes: TDataSource
    DataSet = QrBloOpcoes
    Left = 448
    Top = 8
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 616
    Top = 8
  end
  object QrPreEmail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 560
    Top = 8
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreEmailNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreEmail: TDataSource
    DataSet = QrPreEmail
    Left = 588
    Top = 8
  end
  object QrPreEmailRe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 644
    Top = 8
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreEmailRe: TDataSource
    DataSet = QrPreEmailRe
    Left = 672
    Top = 8
  end
  object QrEntiTipCtoWA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM entitipcto'
      'WHERE Ativo = 1'
      'AND Tipo IN (0,1)'
      'ORDER BY Nome')
    Left = 700
    Top = 8
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object IntegerField4: TIntegerField
      FieldName = 'CodUsu'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiTipCtoWA: TDataSource
    DataSet = QrEntiTipCtoWA
    Left = 728
    Top = 8
  end
  object VUWhatsApp_EntiTipCto: TdmkValUsu
    dmkEditCB = EdWhatsApp_EntiTipCto
    Panel = Panel5
    QryCampo = 'WhatsApp_EntiTipCto'
    UpdCampo = 'WhatsApp_EntiTipCto'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 756
    Top = 8
  end
end
