object FmBloSemLot: TFmBloSemLot
  Left = 339
  Top = 185
  Caption = 'BLO-PROTO-002 :: Boletos Sem Lote'
  ClientHeight = 500
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 215
        Height = 32
        Caption = 'Boletos Sem Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 215
        Height = 32
        Caption = 'Boletos Sem Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 215
        Height = 32
        Caption = 'Boletos Sem Lote'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 338
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 338
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GBConfigPesq: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 65
        Align = alTop
        Caption = ' Configura'#231#227'o de pesquisa: '
        TabOrder = 0
        object PnConfigPesq: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label2: TLabel
            Left = 8
            Top = 4
            Width = 259
            Height = 13
            Caption = 'Configura'#231#227'o para gera'#231#227'o do arquivo remessa CNAB:'
          end
          object SBReabre: TSpeedButton
            Left = 532
            Top = 20
            Width = 21
            Height = 21
            Caption = '>'
            OnClick = SBReabreClick
          end
          object EdCNAB_Cfg: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCNAB_CfgChange
            DBLookupComboBox = CBCNAB_Cfg
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCNAB_Cfg: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 465
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCNAB_Cfg
            TabOrder = 1
            OnClick = CBCNAB_CfgClick
            dmkEditCB = EdCNAB_Cfg
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
      object DBGArreIts: TDBGrid
        Left = 0
        Top = 65
        Width = 784
        Height = 273
        Align = alClient
        DataSource = DsArreIts
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Entidade'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_ENT'
            Title.Caption = 'Nome do Sacado'
            Width = 382
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Boleto'
            Width = 98
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Lancto'
            Width = 68
            Visible = True
          end>
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 386
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 430
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTudo: TBitBtn
        Tag = 127
        Left = 284
        Top = 4
        Width = 100
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTudoClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 394
        Top = 4
        Width = 100
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNenhumClick
      end
    end
  end
  object QrCNAB_Cfg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cnab_cfg'
      'ORDER BY Nome')
    Left = 8
    Top = 60
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 36
    Top = 60
  end
  object QrArreIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrArreItsAfterOpen
    BeforeClose = QrArreItsBeforeClose
    SQL.Strings = (
      'SELECT ari.*, prv.Empresa, prv.Periodo, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM arreits ari'
      'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade'
      'LEFT JOIN prev prv ON prv.Codigo=ari.Codigo'
      'WHERE ari.Protocolo=0'
      'AND ari.CNAB_Cfg=1'
      'ORDER BY Boleto')
    Left = 381
    Top = 312
    object QrArreItsNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrArreItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrArreItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrArreItsBloArre: TIntegerField
      FieldName = 'BloArre'
    end
    object QrArreItsBloArreIts: TIntegerField
      FieldName = 'BloArreIts'
    end
    object QrArreItsBoleto: TFloatField
      FieldName = 'Boleto'
    end
    object QrArreItsCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrArreItsCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrArreItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrArreItsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrArreItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrArreItsNum: TIntegerField
      FieldName = 'Num'
    end
    object QrArreItsTexto: TWideStringField
      FieldName = 'Texto'
      Size = 50
    end
    object QrArreItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrArreItsVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrArreItsProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrArreItsAvulso: TSmallintField
      FieldName = 'Avulso'
    end
    object QrArreItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrArreItsPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
  object DsArreIts: TDataSource
    DataSet = QrArreIts
    Left = 409
    Top = 312
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 448
    Top = 11
  end
end
