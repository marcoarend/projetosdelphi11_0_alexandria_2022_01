unit BloConfReceb;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  Xml.xmldom, Datasnap.Provider, dmkDBGrid, Datasnap.DBClient,
  Xml.XMLIntf, Xml.Win.msxmldom, Xmlxform;

type
  TFmBloConfReceb = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    XMLTransformProvider1: TXMLTransformProvider;
    CdRest: TClientDataSet;
    DsRest: TDataSource;
    DBGrid1: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CarregaDados();
  public
    { Public declarations }
    FId_Periodo, FId_Cfg_Fatura, FId_Cliente: Integer;
    FNumero_Fatura: Double;
  end;

  var
  FmBloConfReceb: TFmBloConfReceb;

implementation

uses UnMyObjects, UnGrl_REST;

{$R *.DFM}

procedure TFmBloConfReceb.CarregaDados();
var
  Par1, Par2, Par3, Par4: THttpParam;
  Res: String;
  ResCod: Integer;
begin
  Par1.Nome         := 'id_periodo';
  Par1.Valor        := Geral.FF0(FId_Periodo);
  Par1.ACharse      := '';
  Par1.AContentType := '';
  //
  Par2.Nome         := 'id_cfg_fatura';
  Par2.Valor        := Geral.FF0(FId_Cfg_Fatura);
  Par2.ACharse      := '';
  Par2.AContentType := '';
  //
  Par3.Nome         := 'id_cliente';
  Par3.Valor        := Geral.FF0(FId_cliente);
  Par3.ACharse      := '';
  Par3.AContentType := '';
  //
  Par4.Nome         := 'numero_fatura';
  Par4.Valor        := Geral.FFI(FNumero_Fatura);
  Par4.ACharse      := '';
  Par4.AContentType := '';
  //
  ResCod := Grl_REST.Rest_Dermatek_Post('faturas', 'faturas_confirmacao_de_recebimento',
              trtXML, [Par1, Par2, Par3, Par4], Res);
  //
  if ResCod = 200 then
  begin
    Grl_REST.Rest_XmlToClientDataSet(XMLTransformProvider1,
      'conf_recebimento', Res);
    CdRest.Active := True;
  end else
    Geral.MB_Aviso('Falha ao carregar dados!');
end;

procedure TFmBloConfReceb.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloConfReceb.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  CarregaDados();
end;

procedure TFmBloConfReceb.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CdRest.Active       := False;
  CdRest.ProviderName := XMLTransformProvider1.Name;
  //
  FId_Periodo    := 0;
  FId_Cfg_Fatura := 0;
  FId_Cliente    := 0;
  FNumero_Fatura := 0;
end;

procedure TFmBloConfReceb.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
