object FmBloGerenLocper: TFmBloGerenLocper
  Left = 339
  Top = 185
  BorderStyle = bsDialog
  Caption = 'BLO-GEREN-006 :: Per'#237'odo'
  ClientHeight = 345
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 394
    Height = 189
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object StaticText1: TStaticText
      Left = 0
      Top = 0
      Width = 394
      Height = 20
      Align = alTop
      Alignment = taCenter
      Caption = 'Per'#237'odo atual n'#227'o localizado!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object StaticText2: TStaticText
      Left = 0
      Top = 20
      Width = 394
      Height = 20
      Align = alTop
      Alignment = taCenter
      Caption = 'Selecione o per'#237'odo desejado:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 12615680
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object LBPeriodos: TDBLookupListBox
      Left = 0
      Top = 40
      Width = 394
      Height = 147
      Align = alClient
      KeyField = 'Periodo'
      ListField = 'NOME_PER'
      ListSource = DsPrev
      TabOrder = 2
      OnClick = LBPeriodosClick
      OnDblClick = LBPeriodosDblClick
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 394
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 346
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 298
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 92
        Height = 32
        Caption = 'Per'#237'odo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 92
        Height = 32
        Caption = 'Per'#237'odo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 92
        Height = 32
        Caption = 'Per'#237'odo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 237
    Width = 394
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 390
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 281
    Width = 394
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 390
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 246
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object QrPrev: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPrevCalcFields
    SQL.Strings = (
      'SELECT Periodo'
      'FROM Prev'
      'WHERE Empresa=:P0'
      'ORDER BY Periodo DESC')
    Left = 304
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrevPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrPrevNOME_PER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_PER'
      Calculated = True
    end
  end
  object DsPrev: TDataSource
    DataSet = QrPrev
    Left = 332
    Top = 12
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 192
    Top = 11
  end
end
