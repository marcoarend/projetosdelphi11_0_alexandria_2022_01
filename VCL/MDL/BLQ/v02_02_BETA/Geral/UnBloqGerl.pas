unit UnBloqGerl;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, ModuleGeral, dmkImage, Forms,
  Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids, DB, UnDmkProcFunc,
  ExtCtrls, dmkDBGrid, Math, Dialogs, UnDmkEnums, Classes, dmkDBGridZTO,
  frxDBSet, frxClass, UnInternalConsts, Variants;

type
  TModoVisualizacaoBloquetos = (istVisNenhum, istVisMensal, istVisPesquisa, istVisProtEnt, istArrPeri);
  TUnBloqGerl = class(TObject)
  private
    function  ObtemProximoNossoNumeroFatura(QryFatur_Cfg, QryUpd: TmySQLQuery;
              DataBase: TmySQLDatabase; Fatur_Cfg: Integer): Double;
  public
    function  InsereBloProto(DataBase: TmySQLDatabase; QueryUpd: TmySQLQuery;
              Codigo, Controle, Tarefa: Integer; Tabela: String): Integer;
    function  EncerraPeriodo(QueryUpd, QueryPrev: TmySQLQuery; TabelaPrvA: String): Integer;
    function  DefineVarsBloGeren(var EntCliInt, CliInt: Integer; var TabLctA,
              TabLctB, TabLctD, TabAriA, TabCnsA, TabPriA, TabPrvA: String): Boolean;
    function  PeriodoExiste(Periodo: Integer; Tabela: String;
              DataBase: TmySQLDatabase): Boolean;
    function  GeraBloquetoAtual(Periodo, EntCliInt: Integer;
              Vencimento: TDate; AtualizaAreeIts: Boolean; TabLctA, TabAriA,
              TabCnsA: String; QueryBoletos, QueryBoletosIts: TmySQLQuery;
              DataBase: TmySQLDatabase; GeraPeriodo: Boolean;
              var ProtocoloCR: Integer): Double;
    function  GeraFaturaAtual(Periodo, EntCliInt: Integer;
              Vencimento: TDate; AtualizaAreeIts: Boolean; TabLctA, TabAriA,
              TabCnsA: String; QueryBoletos, QueryBoletosIts: TmySQLQuery;
              DataBase: TmySQLDatabase; GeraPeriodo: Boolean): Double;
    function  ValidaVencimentoPeriodo(const DataBase: TmySQLDatabase;
              const QueryBloOpcoes: TmySQLQuery; const Periodo: Integer;
              const Data: TDateTime; var MesValido: Integer): Boolean;
    function  LiberaImpressaoBoletosCR(DataBase: TmySQLDatabase;
              QueryBoletos: TmySQLQuery): Boolean;
    //function  ObtemProximoNumeroFatura(Database: TmySQLDatabase): Integer;
    procedure ReopenLeiBol(DataBase: TmySQLDatabase;
              QueryBoletos: TmySQLQuery; Boleto: Double);
    procedure ReopenCNAB_Cfg(Query: TmySQLQuery; Database: TmySQLDatabase;
              Cliente, Tipo: Integer; Codigo: Integer = 0);
    function  ObtemNomeTipoFatura(Tipo: Integer): String;
    function  ObtemListaTipoFatura(): TStringList;
    {$IfNDef sNFSe}
    function  CriaNFSeFatura(Empresa, IncreCompet: Integer; Progress: TProgressBar;
                Grade: TDBGrid; QueryBoletos, QueryBoletosIts, QueryUpd,
                QueryLoc: TmySQLQuery; DataBase: TmySQLDatabase): Integer;
    {$EndIf}
  end;

var
  UBloqGerl: TUnBloqGerl;
  //
  VAR_ModBloq_EntCliInt, VAR_ModBloq_CliInt, VAR_ModBloq_Peri,
  VAR_ModBloq_FatID, VAR_ModBloq_Lancto: Integer;
  VAR_ModBloq_TabAriA, VAR_ModBloq_TabPrvA, VAR_ModBloq_TabCnsA,
  VAR_ModBloq_TabPriA, VAR_ModBloq_TabLctA, VAR_ModBloq_TabLctB,
  VAR_ModBloq_TabLctD: String;
  VAR_ModBloq_FatNum: Double;

const
  FArre_FatID = VAR_FATID_0600;
  FCons_FatID = VAR_FATID_0601;
  CO_BLQ_FaturTip: array[0..1] of string = ('Boleto', 'Fatura Avulsa');


implementation

uses
{$IfNDef SemProtocolo} Protocolo, {$EndIf}
{$IfNDef NO_CNAB} UnBco_Rem, UnBancos, {$EndIf}
{$IfNDef NO_FINANCEIRO} UnFinanceiro, {$EndIf}
{$IfNDef sNFSe}NFSe_PF_0201, NFSe_PF_0000, {$EndIf}
DmkDAC_PF, UnMyObjects, MyDBCheck, UnBloquetos, UnDmkWeb;

{ TUnBloqGerl }

function TUnBloqGerl.ValidaVencimentoPeriodo(const DataBase: TmySQLDatabase;
  const QueryBloOpcoes: TmySQLQuery; const Periodo: Integer;
  const Data: TDateTime; var MesValido: Integer): Boolean;
var
  //Mes: String;
  DataPeri, Peri: Integer;
begin
  if VAR_KIND_DEPTO = kdUH then
  begin
    Peri := Periodo + 1; //� fixo
  end else
  begin
    UMyMod.AbreQuery(QueryBloOpcoes, DataBase);
    //
    case QueryBloOpcoes.FieldByName('PerGerBol').AsInteger of
      0: //Anterior
        Peri := Periodo - 1;
      1: //Atual
        Peri := Periodo;
      2: //Pr�ximo
        Peri := Periodo + 1;
      else
        Peri := 0;
    end;
  end;
  DataPeri  := Geral.Periodo2000(Data);
  MesValido := dmkPF.MesDoPeriodo(Peri);
  //
  if DataPeri <> Peri then
    Result := True //Per�odo inv�lido
  else
    Result := False; //Per�odo v�lido
end;

procedure TUnBloqGerl.ReopenCNAB_Cfg(Query: TmySQLQuery;
  Database: TmySQLDatabase; Cliente, Tipo: Integer; Codigo: Integer = 0);
var
  SQL: String;
begin
  case Tipo of
    0: //Boletos
    begin
      if Codigo = 0 then
        SQL := 'AND (Cedente=' + Geral.FF0(Cliente) + ' OR SacadAvali=' + Geral.FF0(Cliente) + ')'
      else
        SQL := 'AND Codigo=' + Geral.FF0(Codigo);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
        'SELECT Codigo, Nome, CartEmiss ',
        'FROM cnab_cfg ',
        'WHERE Codigo > 0 ',
        'AND Ativo = 1 ',
        SQL,
        'ORDER BY Nome ',
        '']);
    end;
    1: //Fatura avulsa
    begin
      if Codigo = 0 then
        SQL := ''
      else
        SQL := 'AND Codigo=' + Geral.FF0(Codigo);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
        'SELECT Codigo, Nome, Carteira CartEmiss ',
        'FROM  faturcfg ',
        'WHERE Codigo > 0 ',
        'AND Ativo = 1 ',
        'AND Empresa=' + Geral.FF0(Cliente),
        SQL,
        'ORDER BY Nome ',
        '']);
    end;
    else
      Query.Close;
  end;
end;

procedure TUnBloqGerl.ReopenLeiBol(DataBase: TmySQLDatabase;
  QueryBoletos: TmySQLQuery; Boleto: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryBoletos, DataBase, [
    'SELECT cfg.ModalCobr, cfg.LayoutRem, cfg.CNAB, lei.*  ',
    'FROM cnab_lei lei ',
    'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=lei.Diretorio ',
    'WHERE lei.ID_Link=' + Geral.FFI(Boleto),
    ' ',
    '']);
end;

function TUnBloqGerl.EncerraPeriodo(QueryUpd, QueryPrev: TmySQLQuery;
  TabelaPrvA: String): Integer;
var
  Encerrado, Codigo, Periodo: Integer;
begin
  Result := 0;
  //
  Encerrado := QueryPrev.FieldByName('Encerrado').AsInteger;
  Codigo    := QueryPrev.FieldByName('Codigo').AsInteger;
  Periodo   := QueryPrev.FieldByName('Periodo').AsInteger;
  //
  if MyObjects.FIC(Codigo = 0, nil, 'ID do per�odo n�o definido!') then Exit;
  if MyObjects.FIC(Periodo = 0, nil, 'Per�odo n�o definido!') then Exit;
  //
  if Encerrado = 1 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    Encerrado := 0;
  end else
    Encerrado := 1;
  //
  if UMyMod.SQLInsUpd(QueryUpd, stUpd, TabelaPrvA, False,
    ['Encerrado'], ['Codigo'], [Encerrado], [Codigo], True) then
  begin
    Result := Periodo;
  end;
end;

function TUnBloqGerl.DefineVarsBloGeren(var EntCliInt, CliInt: Integer; var TabLctA,
  TabLctB, TabLctD, TabAriA, TabCnsA, TabPriA, TabPrvA: String): Boolean;
var
  SelecionouEmpresa, Validou: Boolean;
  Enti, CliIn: Integer;
  TbLctA: String;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
begin
  Result    := False;
  Enti      := 0;
  CliIn     := 0;
  EntCliInt := 0;
  CliInt    := 0;
  TabLctA   := '';
  TbLctB    := '';
  TbLctD    := '';
  //
  begin
// Usa no lct antigo?
    SelecionouEmpresa := True;
    Enti  := -11;
    CliIn := 1;
  end;
{$IFDEF DEFINE_VARLCT}
{$IfNDef NO_FINANCEIRO}
  SelecionouEmpresa := DModG.SelecionaEmpresa(sllLivre);
  if SelecionouEmpresa then
  begin
    Enti  := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    CliIn := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
  end else
    Exit;
{$ENDIF}
{$ENDIF}
  //
  if (Enti <> 0) and (CliIn <> 0) then
  begin
    {$IFDEF DEFINE_VARLCT}
    DModG.Def_EM_ABD(TMeuDB, Enti, CliIn, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
    {$ELSE}
    TbLctA := VAR_LCT;
    {$ENDIF}
    //
    EntCliInt := Enti;
    CliInt    := CliIn;
    TabLctA   := TbLctA;
    TabLctB   := TbLctB;
    TabLctD   := TbLctD;
    //
    if VAR_KIND_DEPTO = kdUH then
    begin
      TabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CliInt);
      TabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, CliInt);
      TabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, CliInt);
      TabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, CliInt);
      //
      Validou := (TabAriA <> '') and (TabCnsA <> '') and (TabPriA <> '') and
                 (TabPrvA <> '');
    end else
    begin
      TabAriA := 'arreits';
      TabCnsA := ''; //N�o tem
      TabPriA := ''; //N�o tem
      TabPrvA := 'prev';
      //
      Validou := (TabAriA <> '') and (TabPrvA <> '');
    end;
    //
    Result := (EntCliInt <> 0) and (CliInt <> 0) and (TabLctA <> '') and
              (TabLctB <> '') and (TabLctD <> '') and Validou;
  end;
end;

function TUnBloqGerl.GeraBloquetoAtual(Periodo, EntCliInt: Integer;
  Vencimento: TDate; AtualizaAreeIts: Boolean; TabLctA, TabAriA, TabCnsA: String;
  QueryBoletos, QueryBoletosIts: TmySQLQuery; DataBase: TmySQLDatabase;
  GeraPeriodo: Boolean; var ProtocoloCR: Integer): Double;

  function NaoContinuaPelaCarteira(Carteira, TipoDoc, Ativo: Integer): Boolean;
  var
    NumCartEmiss: String;
  begin
    Result := True;
    //
    NumCartEmiss := Geral.FF0(Carteira);
    //
    if Carteira = 0 then
    begin
      Geral.MB_Aviso('A carteira de emiss�o n�o foi definida! A��o abortada!');
      Exit;
    end;
    if TipoDoc <> 5 then
    begin
      if Geral.MB_Pergunta('A carteira de emiss�o n� ' + NumCartEmiss +
        ' n�o est� configurada como "Tipo de documento" = bloqueto!' + sLineBreak +
        'Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
    if Ativo <> 1 then
    begin
      if Geral.MB_Pergunta('A carteira de emiss�o (n� ' +
        NumCartEmiss + ') n�o est� configurada como ativa!' + sLineBreak +
        'Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
    Result := False;
  end;

  function InsereLancto(Vencto: TDateTime; Boleto, Valor, JurosPerc,
    MultaPerc: Double; Empresa, Entidade, CartEmiss, Genero, Apto, Controle,
    FatID: Integer; Descri, TabelaUpd: String; Avulso: Boolean): Integer;
  {$IfNDef NO_FINANCEIRO}
  var
    Res: Boolean;
    Qry: TmySQLQuery;
    SQLCompl: String;
  begin
    Result := 0;
    Qry    := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      UFinanceiro.LancamentoDefaultVARS;
      //
      FLAN_Data       := Geral.FDT(DModG.ObtemAgora(), 1);
      FLAN_Tipo       := 2;
      FLAN_Documento  := Trunc(Boleto);
      //
      if Valor > 0 then
        FLAN_Credito := Valor
      else
        FLAN_Debito := Valor * -1;
      //
      FLAN_MoraDia    := JurosPerc;
      FLAN_Multa      := MultaPerc;
      FLAN_Carteira   := CartEmiss;
      FLAN_Genero     := Genero;
      FLAN_Cliente    := Entidade;
      FLAN_CliInt     := Empresa;
      FLAN_Depto      := Apto;
      //
      if VAR_KIND_DEPTO = kdUH then
        FLAN_ForneceI   := Entidade;
      //
      FLAN_Vencimento := Geral.FDT(Vencto, 1);
      FLAN_Mez        := dmkPF.PeriodoToAnoMes(Periodo);
      FLAN_FatID      := FatID;
      FLAN_FatNum     := Trunc(Boleto);
      FLAN_FatParcela := 1;
      //
      if VAR_KIND_DEPTO = kdUH then
      begin
        FLAN_Descricao  := Descri;
      end else
      begin
        if Avulso then
          FLAN_Descricao := Descri
        else
          FLAN_Descricao := Descri + ' - ' + dmkPF.MesEAnoDoPeriodo(Periodo);
      end;
      //
      FLAN_Controle := UMyMod.BuscaEmLivreY(DataBase, 'Livres', 'Controle',
                         TabLctA, LAN_CTOS, 'Controle');
      //
      {$IFDEF DEFINE_VARLCT}
        Res := UFinanceiro.InsereLancamento(TabLctA);
      {$ELSE}
        Res := UFinanceiro.InsereLancamento;
      {$ENDIF}
      //
      if Res then
      begin
        if not Avulso then
        begin
          if VAR_KIND_DEPTO = kdUH then
            SQLCompl := ', Vencto="' + Geral.FDT(Vencto, 1) + '"'
          else
            SQLCompl := '';
          //
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DataBase, [
            'UPDATE ' + TabelaUpd + ' SET ',
            'Lancto=' + Geral.FF0(FLAN_Controle),
            SQLCompl,
            'WHERE Controle=' + Geral.FF0(Controle),
            '']);
        end;
        Result := FLAN_Controle;
      end;
    finally
      Qry.Free;
    end;
  {$Else}
  begin
    dmkPF.InfoSemModulo(mdlappFinanceiro);
  {$EndIf}
  end;

var
  CNAB_Cfg, Controle, Lancto: Integer;
  NumBloq: Double;
  NossoNumero, NossoNumero_Rem, TabelaUpd, Descri: String;
  QryCNAB_Cfg, QryUpd, QryAux: TmySQLQuery;
begin
  Result := 0;
  //
  if MyObjects.FIC(QueryBoletos = nil, nil, 'Query Boletos n�o definida!') then Exit;
  //
  if AtualizaAreeIts = True then
    if MyObjects.FIC(QueryBoletosIts = nil, nil, 'Query BoletosIts n�o definida!') then Exit;
  //
  QryCNAB_Cfg := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd      := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryAux      := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  //
  QryUpd.DataBase := DataBase;
  try
    CNAB_Cfg := QueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
    NumBloq  := UBco_Rem.ObtemProximoNossoNumero(CNAB_Cfg, False);
    //
    if MyObjects.FIC(CNAB_Cfg = 0, nil, 'Configura��o de boleto n�o definida!') then Exit;
    if MyObjects.FIC(Vencimento < 2, nil, 'Vencimento de boleto n�o definido!') then Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QryCNAB_Cfg, DataBase, [
      'SELECT bco.Nome NOMEBANCO, bco.DVB,  cfg.*, ',
      'IF(cfg.Cedente=0,"",IF(cfg.CedNome <> "", cfg.CedNome, ',
      'IF(ced.Tipo=0,ced.RazaoSocial,ced.Nome))) NOMECED_IMP, ',
      'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CartNum, ',
      'IF(cfg.AceiteTit = 1, "S", "N") ACEITETIT_TXT, ',
      'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CART_IMP, ',
      'cfg.CartEmiss, car.TipoDoc, car.Ativo ',
      'FROM cnab_cfg cfg ',
      'LEFT JOIN carteiras car ON car.Codigo=cfg.CartEmiss ',
      'LEFT JOIN bancos bco ON bco.Codigo=cfg.CedBanco ',
      'LEFT JOIN entidades ced ON ced.Codigo=cfg.Cedente ',
      'WHERE cfg.Codigo=' + Geral.FF0(CNAB_Cfg),
      '']);
    //
    if VAR_KIND_DEPTO = kdUH then
    begin
      if NaoContinuaPelaCarteira(
        QryCNAB_Cfg.FieldByName('CartEmiss').AsInteger,
        QryCNAB_Cfg.FieldByName('TipoDoc').AsInteger,
        QryCNAB_Cfg.FieldByName('Ativo').AsInteger) = True then Exit;
    end;
    //
    if (UBancos.GeraNossoNumero(
      QryCNAB_Cfg.FieldByName('TipoCart').AsInteger,
      QryCNAB_Cfg.FieldByName('CedBanco').AsInteger,
      QryCNAB_Cfg.FieldByName('CedAgencia').AsInteger,
      QryCNAB_Cfg.FieldByName('CedPosto').AsInteger,
      NumBloq,
      QryCNAB_Cfg.FieldByName('CedConta').AsString,
      QryCNAB_Cfg.FieldByName('CartNum').AsString,
      QryCNAB_Cfg.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(QryCNAB_Cfg.FieldByName('CodEmprBco').AsString),
      Vencimento,
      QryCNAB_Cfg.FieldByName('TipoCobranca').AsInteger,
      QryCNAB_Cfg.FieldByName('EspecieDoc').AsString,
      QryCNAB_Cfg.FieldByName('CNAB').AsInteger,
      QryCNAB_Cfg.FieldByName('CtaCooper').AsString,
      QryCNAB_Cfg.FieldByName('LayoutRem').AsString,
      NossoNumero, NossoNumero_Rem)) then
    begin
      {$IfNDef SemProtocolo}
        ProtocoloCR := UnProtocolo.LocalizaProtocoloCR(DataBase, CNAB_Cfg);
      {$Else}
        ProtocoloCR := 0;
      {$EndIf}
      //
      if (GeraPeriodo = True) and (VAR_KIND_DEPTO <> kdUH) then
      begin
        if AtualizaAreeIts then
        begin
          if QueryBoletosIts.RecordCount > 0 then
          begin
            Controle := QueryBoletosIts.FieldByName('Controle').AsInteger;
            Lancto   := QueryBoletosIts.FieldByName('Lancto').AsInteger;
            //
            UMyMod.SQLInsUpd(QryUpd, stUpd, TabAriA, False,
              ['Boleto'], ['Controle'], [NumBloq], [Controle], True);
            //
            if Lancto = 0 then
            begin
              Descri := QueryBoletosIts.FieldByName('TEXTO').AsString;
              //
              if InsereLancto(Vencimento, NumBloq,
                   QueryBoletosIts.FieldByName('VALOR').AsFloat,
                   QryCNAB_Cfg.FieldByName('JurosPerc').AsFloat,
                   QryCNAB_Cfg.FieldByName('MultaPerc').AsFloat, EntCliInt,
                   QueryBoletosIts.FieldByName('Entidade').AsInteger,
                   QryCNAB_Cfg.FieldByName('CartEmiss').AsInteger,
                   QueryBoletosIts.FieldByName('Genero').AsInteger,
                   0, Controle,
                   FArre_FatID, Descri, TabAriA, False) = 0 then
                begin
                  Result := 0;
                  Exit;
                end else
                  Result := NumBloq;
            end;
          end;
        end else
          Result := NumBloq;
      end else
      begin
        if AtualizaAreeIts then
        begin
          if QueryBoletosIts.RecordCount > 0 then
          begin
            QueryBoletosIts.First;
            //
            while not QueryBoletosIts.EOF do
            begin
              Controle := QueryBoletosIts.FieldByName('Controle').AsInteger;
              Lancto   := QueryBoletosIts.FieldByName('Lancto').AsInteger;
              //
              if VAR_KIND_DEPTO = kdUH then
                TabelaUpd := QueryBoletosIts.FieldByName('TabelaOrig').AsString
              else
                TabelaUpd := TabAriA;
              //
              UMyMod.SQLInsUpd(QryUpd, stUpd, TabelaUpd, False,
                ['Boleto'], ['Controle'], [NumBloq], [Controle], True);
              //
              //Cria os lan�amentos financeiros
              if VAR_KIND_DEPTO = kdUH then
              begin
                if LowerCase(TabAriA) = LowerCase(TabelaUpd) then //Arrecada��es
                begin
                  //Arrecada��o
                  if Lancto = 0 then
                  begin
                    Descri := QueryBoletosIts.FieldByName('TEXTO').AsString + ' - ' +
                                dmkPF.MesEAnoDoPeriodo(Periodo) + ' - ' +
                                QueryBoletos.FieldByName('Unidade').AsString;
                    //
                    if InsereLancto(Vencimento, NumBloq,
                         QueryBoletosIts.FieldByName('VALOR').AsFloat,
                         QryCNAB_Cfg.FieldByName('JurosPerc').AsFloat,
                         QryCNAB_Cfg.FieldByName('MultaPerc').AsFloat, EntCliInt,
                         QueryBoletos.FieldByName('Propriet').AsInteger,
                         QryCNAB_Cfg.FieldByName('CartEmiss').AsInteger,
                         QueryBoletosIts.FieldByName('Genero').AsInteger,
                         QueryBoletos.FieldByName('Apto').AsInteger, Controle,
                         FArre_FatID, Descri, TabelaUpd, False) = 0 then
                    begin
                      Result := 0;
                      Exit;
                    end else
                      Result := NumBloq;
                  end;
                end else
                if LowerCase(TabCnsA) = LowerCase(TabelaUpd) then //Consumo por leitura
                begin
                  if Lancto = 0 then
                  begin
                    Descri := QueryBoletosIts.FieldByName('TEXTO').AsString + ' (' +
                                UBloquetos.TextoExplicativoItemBoleto(1,
                                  QueryBoletosIts.FieldByName('Casas').AsInteger,
                                  QueryBoletosIts.FieldByName('MedAnt').AsFloat,
                                  QueryBoletosIts.FieldByName('MedAtu').AsFloat,
                                  QueryBoletosIts.FieldByName('Consumo').AsFloat,
                                  QueryBoletosIts.FieldByName('UnidFat').AsFloat,
                                  QueryBoletosIts.FieldByName('UnidLei').AsString,
                                  QueryBoletosIts.FieldByName('UnidImp').AsString,
                                  QueryBoletosIts.FieldByName('GeraTyp').AsInteger,
                                  QueryBoletosIts.FieldByName('CasRat').AsInteger,
                                  QueryBoletosIts.FieldByName('NaoImpLei').AsInteger,
                                  QueryBoletosIts.FieldByName('GeraFat').AsFloat);

                    //
                    if InsereLancto(Vencimento, NumBloq,
                         QueryBoletosIts.FieldByName('VALOR').AsFloat,
                         QryCNAB_Cfg.FieldByName('JurosPerc').AsFloat,
                         QryCNAB_Cfg.FieldByName('MultaPerc').AsFloat, EntCliInt,
                         QueryBoletos.FieldByName('Propriet').AsInteger,
                         QryCNAB_Cfg.FieldByName('CartEmiss').AsInteger,
                         QueryBoletosIts.FieldByName('Genero').AsInteger,
                         QueryBoletos.FieldByName('Apto').AsInteger, Controle,
                         FCons_FatID, Descri, TabelaUpd, False) = 0 then
                    begin
                      Result := 0;
                      Exit;
                    end else
                      Result := NumBloq;
                  end;
                end else
                begin
                  Geral.MB_Aviso('Tabel inv�lida para inser��o de lan�amentos financeiros!');
                  Exit;
                end;
              end else
              begin
                if Lancto = 0 then
                begin
                  Descri := QueryBoletosIts.FieldByName('TEXTO').AsString;
                  //
                  if InsereLancto(Vencimento, NumBloq,
                       QueryBoletosIts.FieldByName('VALOR').AsFloat,
                       QryCNAB_Cfg.FieldByName('JurosPerc').AsFloat,
                       QryCNAB_Cfg.FieldByName('MultaPerc').AsFloat, EntCliInt,
                       QueryBoletosIts.FieldByName('Entidade').AsInteger,
                       QryCNAB_Cfg.FieldByName('CartEmiss').AsInteger,
                       QueryBoletosIts.FieldByName('Genero').AsInteger,
                       0, Controle,
                       FArre_FatID, Descri, TabelaUpd, False) = 0 then
                    begin
                      Result := 0;
                      Exit;
                    end else
                      Result := NumBloq;
                end;
              end;
              QueryBoletosIts.Next;
            end
          end;
        end else
          Result := NumBloq;
      end;
    end;
  finally
    QryCNAB_Cfg.Free;
    QryUpd.Free;
    QryAux.Free;
  end;
end;

function TUnBloqGerl.GeraFaturaAtual(Periodo, EntCliInt: Integer;
  Vencimento: TDate; AtualizaAreeIts: Boolean; TabLctA, TabAriA, TabCnsA: String;
  QueryBoletos, QueryBoletosIts: TmySQLQuery; DataBase: TmySQLDatabase;
  GeraPeriodo: Boolean): Double;

  function NaoContinuaPelaCarteira(Carteira, Ativo: Integer): Boolean;
  var
    NumCartEmiss: String;
  begin
    Result := True;
    //
    NumCartEmiss := Geral.FF0(Carteira);
    //
    if Carteira = 0 then
    begin
      Geral.MB_Aviso('A carteira de emiss�o n�o foi definida! A��o abortada!');
      Exit;
    end;
    if Ativo <> 1 then
    begin
      if Geral.MB_Pergunta('A carteira de emiss�o (n� ' +
        NumCartEmiss + ') n�o est� configurada como ativa!' + sLineBreak +
        'Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
    end;
    Result := False;
  end;

  function InsereLancto(Vencto: TDateTime; Boleto, Valor, JurosPerc,
    MultaPerc: Double; Empresa, Entidade, CartEmiss, Genero, Apto, Controle,
    FatID: Integer; Descri, TabelaUpd: String; Avulso: Boolean): Integer;
  {$IfNDef NO_FINANCEIRO}
  var
    Res: Boolean;
    Qry: TmySQLQuery;
    SQLCompl: String;
  begin
    Result := 0;
    Qry    := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      UFinanceiro.LancamentoDefaultVARS;
      //
      FLAN_Data       := Geral.FDT(DModG.ObtemAgora(), 1);
      FLAN_Tipo       := 2;
      FLAN_Documento  := Trunc(Boleto);
      //
      if Valor > 0 then
        FLAN_Credito := Valor
      else
        FLAN_Debito := Valor * -1;
      //
      FLAN_MoraDia    := JurosPerc;
      FLAN_Multa      := MultaPerc;
      FLAN_Carteira   := CartEmiss;
      FLAN_Genero     := Genero;
      FLAN_Cliente    := Entidade;
      FLAN_CliInt     := Empresa;
      FLAN_Depto      := Apto;
      //
      if VAR_KIND_DEPTO = kdUH then
        FLAN_ForneceI   := Entidade;
      //
      FLAN_Vencimento := Geral.FDT(Vencto, 1);
      FLAN_Mez        := dmkPF.PeriodoToAnoMes(Periodo);
      FLAN_FatID      := FatID;
      FLAN_FatNum     := Trunc(Boleto);
      FLAN_FatParcela := 1;
      //
      if VAR_KIND_DEPTO = kdUH then
      begin
        FLAN_Descricao  := Descri;
      end else
      begin
        if Avulso then
          FLAN_Descricao := Descri
        else
          FLAN_Descricao := Descri + ' - ' + dmkPF.MesEAnoDoPeriodo(Periodo);
      end;
      //
      FLAN_Controle := UMyMod.BuscaEmLivreY(DataBase, 'Livres', 'Controle',
                         TabLctA, LAN_CTOS, 'Controle');
      //
      {$IFDEF DEFINE_VARLCT}
        Res := UFinanceiro.InsereLancamento(TabLctA);
      {$ELSE}
        Res := UFinanceiro.InsereLancamento;
      {$ENDIF}
      //
      if Res then
      begin
        if not Avulso then
        begin
          if VAR_KIND_DEPTO = kdUH then
            SQLCompl := ', Vencto="' + Geral.FDT(Vencto, 1) + '"'
          else
            SQLCompl := '';
          //
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DataBase, [
            'UPDATE ' + TabelaUpd + ' SET ',
            'Lancto=' + Geral.FF0(FLAN_Controle),
            SQLCompl,
            'WHERE Controle=' + Geral.FF0(Controle),
            '']);
        end;
        Result := FLAN_Controle;
      end;
    finally
      Qry.Free;
    end;
  {$Else}
  begin
    dmkPF.InfoSemModulo(mdlappFinanceiro);
  {$EndIf}
  end;

var
  Fatur_Cfg, Controle, Lancto: Integer;
  NumBloq: Double;
  NossoNumero, NossoNumero_Rem, TabelaUpd, Descri: String;
  QryFatur_Cfg, QryUpd, QryAux: TmySQLQuery;
begin
  Result := 0;
  //
  if MyObjects.FIC(QueryBoletos = nil, nil, 'Query Boletos n�o definida!') then Exit;
  //
  if AtualizaAreeIts = True then
    if MyObjects.FIC(QueryBoletosIts = nil, nil, 'Query BoletosIts n�o definida!') then Exit;
  //
  QryFatur_Cfg := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd       := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryAux       := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  //
  QryUpd.DataBase := DataBase;
  try
    Fatur_Cfg := QueryBoletos.FieldByName('Fatur_Cfg').AsInteger;
    NumBloq   := ObtemProximoNossoNumeroFatura(QryFatur_Cfg, QryUpd, DataBase, Fatur_Cfg);
    //
    if MyObjects.FIC(Fatur_Cfg = 0, nil, 'Configura��o de fatura n�o definida!') then Exit;
    if MyObjects.FIC(Vencimento < 2, nil, 'Vencimento da fatura n�o definido!') then Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QryFatur_Cfg, DataBase, [
      'SELECT cfg.Carteira, cfg.Ativo, cfg.JurosPerc, ',
      'cfg.MultaPerc, cfg.Carteira ',
      'FROM faturcfg cfg ',
      'WHERE cfg.Codigo=' + Geral.FF0(Fatur_Cfg),
      '']);
    //
    if VAR_KIND_DEPTO = kdUH then
    begin
      if NaoContinuaPelaCarteira(QryFatur_Cfg.FieldByName('CartEmiss').AsInteger,
        QryFatur_Cfg.FieldByName('Ativo').AsInteger) = True
      then
        Exit;
    end;
    if (GeraPeriodo = True) and (VAR_KIND_DEPTO <> kdUH) then
    begin
      if AtualizaAreeIts then
      begin
        if QueryBoletosIts.RecordCount > 0 then
        begin
          Controle := QueryBoletosIts.FieldByName('Controle').AsInteger;
          Lancto   := QueryBoletosIts.FieldByName('Lancto').AsInteger;
          //
          UMyMod.SQLInsUpd(QryUpd, stUpd, TabAriA, False,
            ['Boleto'], ['Controle'], [NumBloq], [Controle], True);
          //
          if Lancto = 0 then
          begin
            Descri := QueryBoletosIts.FieldByName('TEXTO').AsString;
            //
            if InsereLancto(Vencimento, NumBloq,
                 QueryBoletosIts.FieldByName('VALOR').AsFloat,
                 QryFatur_Cfg.FieldByName('JurosPerc').AsFloat,
                 QryFatur_Cfg.FieldByName('MultaPerc').AsFloat, EntCliInt,
                 QueryBoletosIts.FieldByName('Entidade').AsInteger,
                 QryFatur_Cfg.FieldByName('Carteira').AsInteger,
                 QueryBoletosIts.FieldByName('Genero').AsInteger,
                 0, Controle,
                 FArre_FatID, Descri, TabAriA, False) = 0 then
              begin
                Result := 0;
                Exit;
              end else
                Result := NumBloq;
          end;
        end;
      end else
        Result := NumBloq;
    end else
    begin
      if AtualizaAreeIts then
      begin
        if QueryBoletosIts.RecordCount > 0 then
        begin
          QueryBoletosIts.First;
          //
          while not QueryBoletosIts.EOF do
          begin
            Controle := QueryBoletosIts.FieldByName('Controle').AsInteger;
            Lancto   := QueryBoletosIts.FieldByName('Lancto').AsInteger;
            //
            if VAR_KIND_DEPTO = kdUH then
              TabelaUpd := QueryBoletosIts.FieldByName('TabelaOrig').AsString
            else
              TabelaUpd := TabAriA;
            //
            UMyMod.SQLInsUpd(QryUpd, stUpd, TabelaUpd, False,
              ['Boleto'], ['Controle'], [NumBloq], [Controle], True);
            //
            //Cria os lan�amentos financeiros
            if VAR_KIND_DEPTO = kdUH then
            begin
              if LowerCase(TabAriA) = LowerCase(TabelaUpd) then //Arrecada��es
              begin
                //Arrecada��o
                if Lancto = 0 then
                begin
                  Descri := QueryBoletosIts.FieldByName('TEXTO').AsString + ' - ' +
                              dmkPF.MesEAnoDoPeriodo(Periodo) + ' - ' +
                              QueryBoletos.FieldByName('Unidade').AsString;
                  //
                  if InsereLancto(Vencimento, NumBloq,
                       QueryBoletosIts.FieldByName('VALOR').AsFloat,
                       QryFatur_Cfg.FieldByName('JurosPerc').AsFloat,
                       QryFatur_Cfg.FieldByName('MultaPerc').AsFloat, EntCliInt,
                       QueryBoletos.FieldByName('Propriet').AsInteger,
                       QryFatur_Cfg.FieldByName('Carteira').AsInteger,
                       QueryBoletosIts.FieldByName('Genero').AsInteger,
                       QueryBoletos.FieldByName('Apto').AsInteger, Controle,
                       FArre_FatID, Descri, TabelaUpd, False) = 0 then
                  begin
                    Result := 0;
                    Exit;
                  end else
                    Result := NumBloq;
                end;
              end else
              if LowerCase(TabCnsA) = LowerCase(TabelaUpd) then //Consumo por leitura
              begin
                if Lancto = 0 then
                begin
                  Descri := QueryBoletosIts.FieldByName('TEXTO').AsString + ' (' +
                              UBloquetos.TextoExplicativoItemBoleto(1,
                                QueryBoletosIts.FieldByName('Casas').AsInteger,
                                QueryBoletosIts.FieldByName('MedAnt').AsFloat,
                                QueryBoletosIts.FieldByName('MedAtu').AsFloat,
                                QueryBoletosIts.FieldByName('Consumo').AsFloat,
                                QueryBoletosIts.FieldByName('UnidFat').AsFloat,
                                QueryBoletosIts.FieldByName('UnidLei').AsString,
                                QueryBoletosIts.FieldByName('UnidImp').AsString,
                                QueryBoletosIts.FieldByName('GeraTyp').AsInteger,
                                QueryBoletosIts.FieldByName('CasRat').AsInteger,
                                QueryBoletosIts.FieldByName('NaoImpLei').AsInteger,
                                QueryBoletosIts.FieldByName('GeraFat').AsFloat);

                  //
                  if InsereLancto(Vencimento, NumBloq,
                       QueryBoletosIts.FieldByName('VALOR').AsFloat,
                       QryFatur_Cfg.FieldByName('JurosPerc').AsFloat,
                       QryFatur_Cfg.FieldByName('MultaPerc').AsFloat, EntCliInt,
                       QueryBoletos.FieldByName('Propriet').AsInteger,
                       QryFatur_Cfg.FieldByName('Carteira').AsInteger,
                       QueryBoletosIts.FieldByName('Genero').AsInteger,
                       QueryBoletos.FieldByName('Apto').AsInteger, Controle,
                       FCons_FatID, Descri, TabelaUpd, False) = 0 then
                  begin
                    Result := 0;
                    Exit;
                  end else
                    Result := NumBloq;
                end;
              end else
              begin
                Geral.MB_Aviso('Tabel inv�lida para inser��o de lan�amentos financeiros!');
                Exit;
              end;
            end else
            begin
              if Lancto = 0 then
              begin
                Descri := QueryBoletosIts.FieldByName('TEXTO').AsString;
                //
                if InsereLancto(Vencimento, NumBloq,
                     QueryBoletosIts.FieldByName('VALOR').AsFloat,
                     QryFatur_Cfg.FieldByName('JurosPerc').AsFloat,
                     QryFatur_Cfg.FieldByName('MultaPerc').AsFloat, EntCliInt,
                     QueryBoletosIts.FieldByName('Entidade').AsInteger,
                     QryFatur_Cfg.FieldByName('Carteira').AsInteger,
                     QueryBoletosIts.FieldByName('Genero').AsInteger,
                     0, Controle,
                     FArre_FatID, Descri, TabelaUpd, False) = 0 then
                  begin
                    Result := 0;
                    Exit;
                  end else
                    Result := NumBloq;
              end;
            end;
            QueryBoletosIts.Next;
          end
        end;
      end else
        Result := NumBloq;
    end;
  finally
    QryFatur_Cfg.Free;
    QryUpd.Free;
    QryAux.Free;
  end;
end;

function TUnBloqGerl.InsereBloProto(DataBase: TmySQLDatabase; QueryUpd: TmySQLQuery;
  Codigo, Controle, Tarefa: Integer; Tabela: String): Integer;
var
  Conta: Integer;
begin
  Result := 0;
  Conta  := UMyMod.BuscaEmLivreY(DataBase, 'Livres', 'Controle', 'bloproto', 'bloproto', 'Conta');
  //
  if Conta <> 0 then
  begin
    if UMyMod.SQLInsUpd(QueryUpd, stIns, 'bloproto', False,
      ['Tabela', 'Protocolo', 'Codigo', 'Controle'], ['Conta'],
      [Tabela, Tarefa, Codigo, Controle], [Conta], True)
    then
      Result := Conta;
  end;
end;

function TUnBloqGerl.LiberaImpressaoBoletosCR(DataBase: TmySQLDatabase;
  QueryBoletos: TmySQLQuery): Boolean;
var
  Qry: TmySQLQuery;
  Apto, Entidade: Integer;
begin
  if (QueryBoletos.State <> dsInactive) and (QueryBoletos.RecordCount > 0) then
  begin
    Qry := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      if VAR_KIND_DEPTO = kdUH then
      begin
        Apto := QueryBoletos.FieldByName('Apto').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
          'SELECT ppi.DataE, pro.Def_Retorn ',
          'FROM protpakits ppi ',
          'LEFT JOIN protocolos pro ON pro.Codigo = ppi.Codigo ',
          'WHERE ppi.Docum=' + Geral.FFI(QueryBoletos.FieldByName('Boleto').AsFloat),
          'AND ppi.Depto=' + Geral.FF0(Apto),
          'AND ppi.ID_Cod1=' + Geral.FF0(QueryBoletos.FieldByName('Codigo').AsInteger),
          'AND ppi.ID_Cod2=' + Geral.FF0(QueryBoletos.FieldByName('Empresa').AsInteger),
          'AND ppi.ID_Cod4=' + Geral.FF0(QueryBoletos.FieldByName('CNAB_Cfg').AsInteger),
          'AND pro.Tipo = 4 ', //CR
          '']);
      end else
      begin
        Entidade := QueryBoletos.FieldByName('Entidade').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
          'SELECT ppi.DataE, pro.Def_Retorn ',
          'FROM protpakits ppi ',
          'LEFT JOIN protocolos pro ON pro.Codigo = ppi.Codigo ',
          'WHERE ppi.Docum=' + Geral.FFI(QueryBoletos.FieldByName('Boleto').AsFloat),
          'AND ppi.Cliente=' + Geral.FF0(Entidade),
          'AND ppi.ID_Cod1=' + Geral.FF0(QueryBoletos.FieldByName('Codigo').AsInteger),
          'AND ppi.ID_Cod2=' + Geral.FF0(QueryBoletos.FieldByName('Empresa').AsInteger),
          'AND ppi.ID_Cod4=' + Geral.FF0(QueryBoletos.FieldByName('CNAB_Cfg').AsInteger),
          'AND pro.Tipo = 4 ', //CR
          '']);
      end;
      if Qry.RecordCount > 0 then
      begin
        if Qry.FieldByName('Def_Retorn').AsInteger = 1 then //Verifica retorno
        begin
          if Qry.FieldByName('DataE').AsDateTime < 2 then
            Result := False
          else
            Result := True;
        end else
          Result := True; //N�o verifica retorno
      end else
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
          'SELECT pro.Def_Retorn ',
          'FROM cnab_cfg cfg ',
          'LEFT JOIN protocolos pro ON pro.Codigo = cfg.ProtocolCR ',
          'WHERE cfg.Codigo=' + Geral.FF0(QueryBoletos.FieldByName('CNAB_Cfg').AsInteger),
          '']);
        if (Qry.RecordCount > 0) and (Qry.FieldByName('Def_Retorn').AsInteger = 1) then
        begin
          Result := False; //Com registro
        end else
          Result := True; //Sem registro
      end;
    finally
      Qry.Free;
    end;
  end else
    Result := False;
end;

function TUnBloqGerl.ObtemListaTipoFatura: TStringList;
var
  i: Integer;
  Nome: String;
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    for I := Low(CO_BLQ_FaturTip) to High(CO_BLQ_FaturTip) do
    begin
      Nome := ObtemNomeTipoFatura(i);
      //
      Lista.Add(Nome);
    end;
  finally
    Result := Lista;
  end;
end;

function TUnBloqGerl.ObtemNomeTipoFatura(Tipo: Integer): String;
begin
  case Tipo of
    0:
      Result := CO_BLQ_FaturTip[0];
    1:
      Result := CO_BLQ_FaturTip[1];
    else
      Result := 'N�o definido!';
  end;
end;

function TUnBloqGerl.ObtemProximoNossoNumeroFatura(QryFatur_Cfg,
  QryUpd: TmySQLQuery; DataBase: TmySQLDatabase; Fatur_Cfg: Integer): Double;
var
  BloqAtual: Double;
begin
  Result := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QryFatur_Cfg, DataBase, [
    'SELECT cfg.* ',
    'FROM faturcfg cfg ',
    'WHERE cfg.Codigo=' + Geral.FF0(Fatur_Cfg),
    '']);
  if QryFatur_Cfg.RecordCount = 0 then
  begin
    Geral.MB_Aviso('A configura��o sob o n�mero de cadastro '
      + Geral.FF0(Fatur_Cfg) + ' n�o foi localizada!');
    Exit;
  end;
  BloqAtual := QryFatur_Cfg.FieldByName('LastFatNum').AsFloat + 1;
  //
  QryUpd.Close;
  QryUpd.SQL.Clear;
  QryUpd.SQL.Add('LOCK TABLES faturcfg WRITE');
  QryUpd.ExecSQL;
  //
  QryUpd.SQL.Clear;
  QryUpd.SQL.Add('UPDATE faturcfg SET LastFatNum=:P0');
  QryUpd.SQL.Add('WHERE Codigo=:P1 ');
  QryUpd.Params[00].AsFloat   := BloqAtual;
  QryUpd.Params[01].AsInteger := Fatur_Cfg;
  QryUpd.ExecSQL;
  //
  QryUpd.Close;
  QryUpd.SQL.Clear;
  QryUpd.SQL.Add('UNLOCK TABLES ');
  QryUpd.ExecSQL;
  //
  Result := BloqAtual;
end;

(*
function TUnBloqGerl.ObtemProximoNumeroFatura(Database: TmySQLDatabase): Integer;
var
  Qry: TmySQLQuery;
  Fatura: Integer;
begin
  Qry := TmySQLQuery.Create(TDataModule(Database.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
      'SELECT LastFatura ',
      'FROM blofatura ',
      '']);
    Fatura := Qry.FieldByName('LastFatura').AsInteger + 1;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Database, [
      'UPDATE blofatura SET LastFatura=' + Geral.FF0(Fatura),
      '']);
    Result := Fatura;
  finally
    Qry.Free;
  end;
end;
*)

function TUnBloqGerl.PeriodoExiste(Periodo: Integer; Tabela: String;
  DataBase: TmySQLDatabase): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry    := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
      'SELECT Codigo ',
      'FROM ' + Tabela,
      'WHERE Periodo=' + Geral.FF0(Periodo),
      '']);
    if Qry.RecordCount > 0 then
      Result := True;
  finally
    Qry.Free;
  end;
end;

{$IfNDef sNFSe}
function TUnBloqGerl.CriaNFSeFatura(Empresa, IncreCompet: Integer;
  Progress: TProgressBar; Grade: TDBGrid; QueryBoletos, QueryBoletosIts,
  QueryUpd, QueryLoc: TmySQLQuery; DataBase: TmySQLDatabase): Integer;

  procedure ReopenNFSeSrvCad(Qry: TmySQLQuery; Regra: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
      'SELECT * ',
      'FROM nfsesrvcad ',
      'WHERE Codigo=' + Geral.FF0(Regra),
      '']);
  end;

  function SelecionaNFSeSrvCad(Controle, BloArreIts: Integer): Integer;
  const
    Aviso  = '...';
    Titulo = 'Sele��o de regra fiscal';
    Prompt = 'Informe a regra fiscal: [F7 para pesquisar]';
    Campo  = 'Descricao';
  var
    Codigo: Variant;
    PesqSQL: String;
  begin
    Result := 0;
    Codigo := 0;
    //
    PesqSQL := Geral.ATS([
      'SELECT Codigo, Nome ' + Campo,
      'FROM nfsesrvcad ',
      'WHERE Codigo <> 0',
      'AND Ativo = 1',
      'ORDER BY ' + Campo,
      '']);
    //
    Codigo := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo,
                Codigo, [PesqSQL], DataBase, True);
    //
    if Codigo <> Null then
    begin
      if Controle <> 0 then
      begin
        UMyMod.SQLInsUpd(QueryUpd, stUpd, 'arreits', False,
          ['NFSeSrvCad'], ['Controle'], [Codigo], [Controle], False);
      end;
      //
      if BloArreIts <> 0 then
      begin
        if Geral.MB_Pergunta('Deseja definir esta regra como padr�o ' +
          'para a arrecada��o base referente a este item?') = ID_YES then
        begin
          UMyMod.SQLInsUpd(QueryUpd, stUpd, 'bloarreits', False,
            ['NFSeSrvCad'], ['Controle'], [Codigo], [BloArreIts], False);
        end;
      end;
      Result := Codigo;
    end;
  end;

  function VerificaSeRPSExiste(Lancto: Integer): Integer;
  begin
    Result := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, DataBase, [
      'SELECT Codigo ',
      'FROM nfsedpscab ',
      'WHERE FatID=' + Geral.FF0(VAR_FATID_0202),
      'AND Lancto=' + Geral.FF0(Lancto),
      '']);
    if QueryLoc.RecordCount > 0 then
      Result := QueryLoc.FieldByName('Codigo').AsInteger;
    QueryLoc.Close;
  end;

  function CriaNFSe(Empresa, NFSeFatCab, Cliente, NFSeSrvCad, Lancto: Integer;
    Valor: Double): Integer;
  var
    Qry: TmySQLQuery;
    RpsDataEmissao, Competencia, Hoje: TDateTime;
    Agora: TTime;
    fontTotTrib: TIBPTaxFont;
    tabTotTrib: TIBPTaxTabs;
    tpAliqTotTrib: TIBPTaxOrig;
    iTotTrib, CodAtual, Intermediario, RpsIDNumero, RpsIDTipo, RpsStatus,
    SubstNumero, SubstTipo, IssRetido, ResponsavelRetencao, ExigibilidadeISS,
    QuemPagaIss, MunicipioIncidencia, RegimeEspecialTributacao,
    OptanteSimplesNacional, IncentivoFiscal: Integer;
    ValorServicos, ValorDeducoes, DescontoIncondicionado, DescontoCondicionado,
    VS, vTotTrib, vBasTrib, pTotTrib, ValorPis, ValorCofins, ValorInss, ValorIr,
    ValorCsll, OutrasRetencoes, ValorIss, Aliquota: Double;
    RpsIDSerie, SubstSerie, ItemListaServico, CodigoCnae,
    CodigoTributacaoMunicipio, Discriminacao, verTotTrib, NumeroProcesso,
    ConstrucaoCivilCodigoObra, ConstrucaoCivilArt, Id, RpsHoraEmissao: String;
    Desistiu: Boolean;
  begin
    Result := 0;
    //
    if MyObjects.FIC(NFSeFatCab = 0, nil, 'A��o abortada!' + sLineBreak + 'Motivo: Lote n�o definido!') then Exit;
    //
    Qry := TmySQLQuery.Create(DataBase.Owner);
    try
      DmodG.ObtemDataHora(Hoje, Agora);
      ReopenNFSeSrvCad(Qry, NFSeSrvCad);
      //
      (*
      if MyObjects.FIC(Qry.RecordCount = 0, nil, 'A��o abortada!' + sLineBreak + 'Motivo: Regra fiscal n�o definida!') then Exit;
      Apenas n�o gera NFS-e deixar continuar! Entende-se que n�o tem nota fiscal
      *)
      if Qry.RecordCount = 0 then
      begin
        Result := -1;
        Exit;
      end;
      //
      Desistiu                  := False;
      CodAtual                  := 0;
      RpsDataEmissao            := Hoje;
      Competencia               := IncMonth(RpsDataEmissao, IncreCompet);
      RpsIDNumero               := 0;
      RpsIDSerie                := '';
      SubstSerie                := '';
      RpsIDTipo                 := Qry.FieldByName('RpsTipo').AsInteger;
      RpsStatus                 := tsStatusRps_Normal;
      SubstNumero               := 0;
      SubstTipo                 := 0;
      ResponsavelRetencao       := Qry.FieldByName('ResponsavelRetencao').AsInteger;
      if ResponsavelRetencao = -1 then
        IssRetido := 2
      else
        IssRetido := 1;
      ItemListaServico          := Qry.FieldByName('ItemListaServico').AsString;
      CodigoCnae                := Qry.FieldByName('CodigoCnae').AsString;
      CodigoTributacaoMunicipio := Qry.FieldByName('CodigoTributacaoMunicipio').AsString;
      ExigibilidadeISS          := Qry.FieldByName('ExigibilidadeIss').AsInteger;
      QuemPagaISS               := Qry.FieldByName('QuemPagaISS').AsInteger;
      MunicipioIncidencia       := -1;
      ValorServicos             := Valor;
      VS                        := ValorServicos;
      ValorDeducoes             := 0;
      ValorPis                  := UnNFSe_PF_0201.CI(VS, Qry.FieldByName('AlqPIS').AsFloat);
      ValorCofins               := UnNFSe_PF_0201.CI(VS, Qry.FieldByName('AlqCOFINS').AsFloat);
      ValorInss                 := UnNFSe_PF_0201.CI(VS, Qry.FieldByName('AlqINSS').AsFloat);
      ValorIr                   := UnNFSe_PF_0201.CI(VS, Qry.FieldByName('AlqIR').AsFloat);
      ValorCsll                 := UnNFSe_PF_0201.CI(VS, Qry.FieldByName('AlqCSLL').AsFloat);
      OutrasRetencoes           := UnNFSe_PF_0201.CI(VS, Qry.FieldByName('AlqOutrasRetencoes').AsFloat);
      ValorIss                  := UnNFSe_PF_0201.CI(VS, Qry.FieldByName('Aliquota').AsFloat);
      Aliquota                  := Qry.FieldByName('Aliquota').AsFloat;
      DescontoIncondicionado    := 0;
      DescontoCondicionado      := 0;
      NumeroProcesso            := Qry.FieldByName('NumeroProcesso').AsString;
      ConstrucaoCivilCodigoObra := '';
      ConstrucaoCivilArt        := '';
      RpsHoraEmissao            := Geral.FDT(Agora, 100);
      Intermediario             := 0;
      RegimeEspecialTributacao  := Qry.FieldByName('RegimeEspecialTributacao').AsInteger;
      OptanteSimplesNacional    := Qry.FieldByName('OptanteSimplesNacional').AsInteger;
      IncentivoFiscal           := Qry.FieldByName('IncentivoFiscal').AsInteger;
      //
      // INICIO LEI N� 12.741 - Lei da Transparencia
      Discriminacao := UnNFSe_PF_0201.CalculaDiscriminacao(VS, ValorDeducoes,
                         DescontoIncondicionado, Qry.FieldByName('indFinal').AsInteger,
                         Qry.FieldByName('Importado').AsInteger,
                         Qry.FieldByName('ItemListaServico').AsString,
                         Qry.FieldByName('Discriminacao').AsString, iTotTrib, vTotTrib,
                         vBasTrib, pTotTrib, fontTotTrib, tabTotTrib, verTotTrib,
                         tpAliqTotTrib, Desistiu);
      // FIM LEI N� 12.741 - Lei da Transparencia
      if Desistiu then
        Exit;
      //
      Result := UnNFSe_PF_0000.InsUpdDeclaracaoPrestacaoServico(stIns,
                  NFSeFatCab, RpsDataEmissao, Competencia, RpsIDSerie,
                  SubstSerie, ItemListaServico, CodigoCnae,
                  CodigoTributacaoMunicipio, Discriminacao, NumeroProcesso,
                  ConstrucaoCivilCodigoObra, ConstrucaoCivilArt, Id,
                  RpsHoraEmissao, CodAtual, Empresa, Cliente, Intermediario,
                  RpsIDNumero, RpsIDTipo, RpsStatus, SubstNumero, SubstTipo,
                  IssRetido, ResponsavelRetencao, ExigibilidadeISS, QuemPagaISS,
                  MunicipioIncidencia, RegimeEspecialTributacao,
                  OptanteSimplesNacional, IncentivoFiscal, NFSeSrvCad,
                  ValorServicos, ValorDeducoes, ValorPis, ValorCofins, ValorInss,
                  ValorIr, ValorCsll, OutrasRetencoes, ValorIss, Aliquota,
                  DescontoIncondicionado, DescontoCondicionado, 0, iTotTrib,
                  vTotTrib, vBasTrib, pTotTrib, fontTotTrib, tabTotTrib,
                  verTotTrib, tpAliqTotTrib);
      //
      if Result <> 0 then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, DataBase, [
          'UPDATE nfsedpscab SET ',
          'FatID=' + Geral.FF0(VAR_FATID_0202) + ', ',
          'Lancto=' + Geral.FF0(Lancto),
          'WHERE Codigo=' + Geral.FF0(Result),
          '']);
      end;
    finally
      Qry.Free;
    end;
  end;

var
  NFSeFtCb, NFSeFatCab, Entidade, NFSeSrvCad, Codigo, i, Lote: Integer;
  Boleto, NotaFiscal: Double;
  Vencto: TDate;
  Competen: String;
  Res: Boolean;
begin
  Result     := 0;
  Res        := False;
  NFSeFatCab := 0;
  Competen   := Geral.FDT(Date, 14);
  //
  if MyObjects.FIC(Empresa = 0, nil, 'A��o abortada!' + sLineBreak + 'Motivo: Empresa n�o definida!') then Exit;
  if MyObjects.FIC(Competen = '', nil, 'A��o abortada!' + sLineBreak + 'Motivo: M�s n�o definido!') then Exit;
  //
  try
    Progress.Position := 0;
    //
    if Grade.SelectedRows.Count > 1 then
    begin
      //Selecionados
      try
        Grade.Enabled := False;
        Progress.Max  := Grade.SelectedRows.Count;
        Lote          := 0;
        //
        with Grade.DataSource.DataSet do
        begin
          for i:= 0 to Grade.SelectedRows.Count-1 do
          begin
            GotoBookmark(Grade.SelectedRows.Items[i]);
            //
            Progress.Position := Progress.Position + 1;
            Progress.Update;
            Application.ProcessMessages;
            //
            Boleto   := QueryBoletos.FieldByName('Boleto').AsFloat;
            Entidade := QueryBoletos.FieldByName('Entidade').AsInteger;
            Codigo   := QueryBoletos.FieldByName('Codigo').AsInteger;
            Vencto   := QueryBoletos.FieldByName('Vencto').AsDateTime;
            //
            if QueryBoletosIts.RecordCount > 0 then
            begin
              QueryBoletosIts.First;
              //
              while not QueryBoletosIts.Eof do
              begin
                NotaFiscal := QueryBoletosIts.FieldByName('NotaFiscal').AsFloat;
                //
                if NotaFiscal = 0 then
                begin
                  NFSeFtCb := VerificaSeRPSExiste(QueryBoletosIts.FieldByName('Lancto').AsInteger);
                  //
                  if NFSeFtCb = 0 then
                  begin
                    if Lote = 0 then
                    begin
                      NFSeFatCab := UnNFSe_PF_0201.InsUpdNFSeFatCab(0, Empresa, 0, Competen, '', 0, 0, stIns);
                      Lote       := NFSeFatCab;
                    end;
                    //
                    if NFSeFatCab = 0 then
                    begin
                      Geral.MB_Aviso('A��o abortada!' + sLineBreak + 'Motivo: Falha ao gerar lote!');
                      Exit;
                    end;
                    //
                    NFSeSrvCad := QueryBoletosIts.FieldByName('NFSeSrvCad').AsInteger;
                    //
                    if NFSeSrvCad = 0 then
                      NFSeSrvCad := SelecionaNFSeSrvCad(
                                      QueryBoletosIts.FieldByName('Controle').AsInteger,
                                      QueryBoletosIts.FieldByName('BloArreIts').AsInteger);
                    //
                    Res := CriaNFSe(Empresa, NFSeFatCab, Entidade, NFSeSrvCad,
                             QueryBoletosIts.FieldByName('Lancto').AsInteger,
                             QueryBoletosIts.FieldByName('Valor').AsFloat) <> 0;
                    //
                    if not Res then
                      Exit;
                  end else
                    Result := NFSeFtCb;
                end;
                //
                QueryBoletosIts.Next;
              end;
            end;
          end;
        end;
      finally
        Grade.Enabled := True;
      end;
    end else
    begin
      //�nico
      Boleto   := QueryBoletos.FieldByName('Boleto').AsFloat;
      Entidade := QueryBoletos.FieldByName('Entidade').AsInteger;
      Codigo   := QueryBoletos.FieldByName('Codigo').AsInteger;
      Vencto   := QueryBoletos.FieldByName('Vencto').AsDateTime;
      //
      if QueryBoletosIts.RecordCount > 0 then
      begin
        QueryBoletosIts.First;
        //
        Lote := 0;
        //
        while not QueryBoletosIts.Eof do
        begin
          NotaFiscal := QueryBoletosIts.FieldByName('NotaFiscal').AsFloat;
          //
          if NotaFiscal = 0 then
          begin
            NFSeFtCb := VerificaSeRPSExiste(QueryBoletosIts.FieldByName('Lancto').AsInteger);
            //
            if NFSeFtCb = 0 then
            begin
              if Lote = 0 then
              begin
                NFSeFatCab := UnNFSe_PF_0201.InsUpdNFSeFatCab(0, Empresa, 0, Competen, '', 0, 0, stIns);
                Lote       := NFSeFatCab;
              end;
              //
              if NFSeFatCab = 0 then
              begin
                Geral.MB_Aviso('A��o abortada!' + sLineBreak + 'Motivo: Falha ao gerar lote!');
                Exit;
              end;
              //
              NFSeSrvCad := QueryBoletosIts.FieldByName('NFSeSrvCad').AsInteger;
              //
              if NFSeSrvCad = 0 then
                NFSeSrvCad := SelecionaNFSeSrvCad(
                                QueryBoletosIts.FieldByName('Controle').AsInteger,
                                QueryBoletosIts.FieldByName('BloArreIts').AsInteger);
              //
              Res := CriaNFSe(Empresa, NFSeFatCab, Entidade, NFSeSrvCad,
                       QueryBoletosIts.FieldByName('Lancto').AsInteger,
                       QueryBoletosIts.FieldByName('Valor').AsFloat) <> 0;
            end else
              Result := NFSeFtCb;
          end;
          //
          QueryBoletosIts.Next;
        end;
      end;
    end;
  finally
    Progress.Position := 0;
  end;
  //
  if Res = True then
    Result := NFSeFatCab;
end;
{$EndIf}

end.
