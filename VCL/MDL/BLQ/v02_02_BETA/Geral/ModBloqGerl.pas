unit ModBloqGerl;

interface

uses
  System.SysUtils, System.Classes, DB, mySQLDbTables, Dialogs, Forms, Windows,
  DBGrids, frxClass, dmkGeral, frxDBSet, Controls, Variants, DmkDAC_PF,
  dmkDBGrid, ComCtrls, UnDmkProcFunc, UnDmkEnums;

type
  TDBloqGerl = class(TDataModule)
    frxDsCNAB_Cfg: TfrxDBDataset;
    QrCNAB_Cfg: TmySQLQuery;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    QrCNAB_CfgCedente: TIntegerField;
    QrCNAB_CfgSacadAvali: TIntegerField;
    QrCNAB_CfgCedBanco: TIntegerField;
    QrCNAB_CfgCedAgencia: TIntegerField;
    QrCNAB_CfgCedConta: TWideStringField;
    QrCNAB_CfgCedDAC_A: TWideStringField;
    QrCNAB_CfgCedDAC_C: TWideStringField;
    QrCNAB_CfgCedDAC_AC: TWideStringField;
    QrCNAB_CfgCedNome: TWideStringField;
    QrCNAB_CfgCedPosto: TIntegerField;
    QrCNAB_CfgSacAvaNome: TWideStringField;
    QrCNAB_CfgTipoCart: TSmallintField;
    QrCNAB_CfgCartNum: TWideStringField;
    QrCNAB_CfgCartCod: TWideStringField;
    QrCNAB_CfgEspecieTit: TWideStringField;
    QrCNAB_CfgEspecieDoc: TWideStringField;
    QrCNAB_CfgAceiteTit: TSmallintField;
    QrCNAB_CfgInstrCobr1: TWideStringField;
    QrCNAB_CfgInstrCobr2: TWideStringField;
    QrCNAB_CfgInstrDias: TSmallintField;
    QrCNAB_CfgCodEmprBco: TWideStringField;
    QrCNAB_CfgJurosTipo: TSmallintField;
    QrCNAB_CfgJurosPerc: TFloatField;
    QrCNAB_CfgJurosDias: TSmallintField;
    QrCNAB_CfgMultaTipo: TSmallintField;
    QrCNAB_CfgMultaPerc: TFloatField;
    QrCNAB_CfgMultaDias: TSmallintField;
    QrCNAB_CfgTexto01: TWideStringField;
    QrCNAB_CfgTexto02: TWideStringField;
    QrCNAB_CfgTexto03: TWideStringField;
    QrCNAB_CfgTexto04: TWideStringField;
    QrCNAB_CfgTexto05: TWideStringField;
    QrCNAB_CfgTexto06: TWideStringField;
    QrCNAB_CfgTexto07: TWideStringField;
    QrCNAB_CfgTexto08: TWideStringField;
    QrCNAB_CfgTexto09: TWideStringField;
    QrCNAB_CfgTexto10: TWideStringField;
    QrCNAB_CfgCNAB: TIntegerField;
    QrCNAB_CfgEnvEmeio: TSmallintField;
    QrCNAB_CfgDiretorio: TWideStringField;
    QrCNAB_CfgQuemPrint: TWideStringField;
    QrCNAB_Cfg_237Mens1: TWideStringField;
    QrCNAB_Cfg_237Mens2: TWideStringField;
    QrCNAB_CfgCodOculto: TWideStringField;
    QrCNAB_CfgSeqArq: TIntegerField;
    QrCNAB_CfgLastNosNum: TLargeintField;
    QrCNAB_CfgLocalPag: TWideStringField;
    QrCNAB_CfgEspecieVal: TWideStringField;
    QrCNAB_CfgOperCodi: TWideStringField;
    QrCNAB_CfgIDCobranca: TWideStringField;
    QrCNAB_CfgAgContaCed: TWideStringField;
    QrCNAB_CfgDirRetorno: TWideStringField;
    QrCNAB_CfgCartRetorno: TIntegerField;
    QrCNAB_CfgPosicoesBB: TSmallintField;
    QrCNAB_CfgIndicatBB: TWideStringField;
    QrCNAB_CfgTipoCobrBB: TWideStringField;
    QrCNAB_CfgVariacao: TIntegerField;
    QrCNAB_CfgComando: TIntegerField;
    QrCNAB_CfgDdProtesBB: TIntegerField;
    QrCNAB_CfgMsg40posBB: TWideStringField;
    QrCNAB_CfgQuemDistrb: TWideStringField;
    QrCNAB_CfgDesco1Cod: TSmallintField;
    QrCNAB_CfgDesco1Dds: TIntegerField;
    QrCNAB_CfgDesco1Fat: TFloatField;
    QrCNAB_CfgDesco2Cod: TSmallintField;
    QrCNAB_CfgDesco2Dds: TIntegerField;
    QrCNAB_CfgDesco2Fat: TFloatField;
    QrCNAB_CfgDesco3Cod: TSmallintField;
    QrCNAB_CfgDesco3Dds: TIntegerField;
    QrCNAB_CfgDesco3Fat: TFloatField;
    QrCNAB_CfgProtesCod: TSmallintField;
    QrCNAB_CfgProtesDds: TSmallintField;
    QrCNAB_CfgBxaDevCod: TSmallintField;
    QrCNAB_CfgBxaDevDds: TIntegerField;
    QrCNAB_CfgMoedaCod: TWideStringField;
    QrCNAB_CfgContrato: TFloatField;
    QrCNAB_CfgConvCartCobr: TWideStringField;
    QrCNAB_CfgConvVariCart: TWideStringField;
    QrCNAB_CfgInfNossoNum: TSmallintField;
    QrCNAB_CfgCodLidrBco: TWideStringField;
    QrCNAB_CfgLk: TIntegerField;
    QrCNAB_CfgDataCad: TDateField;
    QrCNAB_CfgDataAlt: TDateField;
    QrCNAB_CfgUserCad: TIntegerField;
    QrCNAB_CfgUserAlt: TIntegerField;
    QrCNAB_CfgAlterWeb: TSmallintField;
    QrCNAB_CfgAtivo: TSmallintField;
    QrCNAB_CfgCartEmiss: TIntegerField;
    QrCNAB_CfgDVB: TWideStringField;
    QrCNAB_CfgNOMEBANCO: TWideStringField;
    QrCNAB_CfgNOMECED_IMP: TWideStringField;
    QrCNAB_CfgTipoCobranca: TIntegerField;
    QrCNAB_CfgEspecieTxt: TWideStringField;
    QrCNAB_CfgCartTxt: TWideStringField;
    QrCNAB_CfgLayoutRem: TWideStringField;
    QrCNAB_CfgLayoutRet: TWideStringField;
    QrCNAB_CfgConcatCod: TWideStringField;
    QrCNAB_CfgComplmCod: TWideStringField;
    QrCNAB_CfgNaoRecebDd: TSmallintField;
    QrCNAB_CfgTipBloqUso: TWideStringField;
    QrCNAB_CfgNumVersaoI3: TIntegerField;
    QrCNAB_CfgModalCobr: TSmallintField;
    QrCNAB_CfgCtaCooper: TWideStringField;
    QrCNAB_CfgTermoAceite: TIntegerField;
    QrCNAB_CfgCorresBco: TIntegerField;
    QrCNAB_CfgCorresAge: TIntegerField;
    QrCNAB_CfgCorresCto: TWideStringField;
    QrCNAB_CfgNPrinBc: TWideStringField;
    QrCNAB_CfgACEITETIT_TXT: TWideStringField;
    QrCNAB_CfgCART_IMP: TWideStringField;
    QrCNAB_CfgCorreio_TXT: TWideStringField;
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    //
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    procedure ReopenCNAB_Cfg(Codigo: Integer);
  end;

var
  DBloqGerl: TDBloqGerl;

implementation

uses Module, UMySQLModule;

{$R *.dfm}

{ TDBloqGerl }

procedure TDBloqGerl.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

procedure TDBloqGerl.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TDBloqGerl.ReopenCNAB_Cfg(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCNAB_Cfg, Dmod.MyDB, [
    'SELECT bco.Nome NOMEBANCO, bco.DVB,  cfg.*, ',
    'IF(cfg.Cedente=0,"",IF(cfg.CedNome <> "", cfg.CedNome, IF(ced.Tipo=0,ced.RazaoSocial,ced.Nome))) NOMECED_IMP, ',
    'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CartNum, ',
    'IF(cfg.AceiteTit = 1, "S", "N") ACEITETIT_TXT, ',
    'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CART_IMP, ',
    'IF(cfg.Correio = 1, "S", "N") Correio_TXT ',
    'FROM cnab_cfg cfg ',
    'LEFT JOIN bancos bco ON bco.Codigo=cfg.CedBanco ',
    'LEFT JOIN entidades ced ON ced.Codigo=cfg.Cedente ',
    'WHERE cfg.Codigo=' + Geral.FF0(Codigo),
    '']);
end;

procedure TDBloqGerl.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

end.
