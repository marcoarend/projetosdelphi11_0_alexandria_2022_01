unit BloSemLot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkPermissoes, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmBloSemLot = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBConfigPesq: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnConfigPesq: TPanel;
    DBGArreIts: TDBGrid;
    QrCNAB_Cfg: TmySQLQuery;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    DsCNAB_Cfg: TDataSource;
    Label2: TLabel;
    EdCNAB_Cfg: TdmkEditCB;
    CBCNAB_Cfg: TdmkDBLookupComboBox;
    SBReabre: TSpeedButton;
    QrArreIts: TmySQLQuery;
    DsArreIts: TDataSource;
    QrArreItsNO_ENT: TWideStringField;
    QrArreItsCodigo: TIntegerField;
    QrArreItsControle: TIntegerField;
    QrArreItsBloArre: TIntegerField;
    QrArreItsBloArreIts: TIntegerField;
    QrArreItsBoleto: TFloatField;
    QrArreItsCartEmiss: TIntegerField;
    QrArreItsCNAB_Cfg: TIntegerField;
    QrArreItsConta: TIntegerField;
    QrArreItsEntidade: TIntegerField;
    QrArreItsLancto: TIntegerField;
    QrArreItsNum: TIntegerField;
    QrArreItsTexto: TWideStringField;
    QrArreItsValor: TFloatField;
    QrArreItsVencto: TDateField;
    QrArreItsProtocolo: TIntegerField;
    QrArreItsAvulso: TSmallintField;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    QrArreItsEmpresa: TIntegerField;
    QrArreItsPeriodo: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCNAB_CfgChange(Sender: TObject);
    procedure CBCNAB_CfgClick(Sender: TObject);
    procedure SBReabreClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrArreItsBeforeClose(DataSet: TDataSet);
    procedure QrArreItsAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    procedure DesfazPesquisa();
  public
    { Public declarations }
    FProtocoPakCodigo, FProtocoPakControle: Integer;
    FQrIts: TmySQLQuery;
    //
    procedure ReopenArreIts();
    procedure ReopenCNAB_Cfg(Protocolo: Integer);
  end;

  var
  FmBloSemLot: TFmBloSemLot;

implementation

uses Protocolo, UnMyObjects, Module, UMySQLModule, DmkDAC_PF, MyListas;

{$R *.DFM}

procedure TFmBloSemLot.BtNenhumClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(DBGArreIts, False);
end;

procedure TFmBloSemLot.BtOKClick(Sender: TObject);
  procedure AdicionaAtual();
  var
    //DataE, DataD, SerieCH, Texto, LimiteSai, LimiteRem, LimiteRet, DataSai, DataRec, DataRet,
    Vencto: String;
    Codigo, Controle, Conta, Link_ID, CliInt, Cliente, Cedente, Fornece,
    Periodo, Lancto, Depto, (*Retorna, Cancelado, Motivo,*)
    ID_Cod1, ID_Cod2, ID_Cod3, ID_Cod4
    (*, ComoConf, Manual, Saiu, Recebeu, Retornou*): Integer;
    Docum, Valor(*, MoraDiaVal, MultaVal*): Double;
    //
    Protocolo, protocopak, protpakits: Integer;
  begin
    Codigo         := FProtocoPakCodigo;
    Controle       := FProtocoPakControle;
    Conta          := 0; // Abaixo
    Link_ID        := VAR_TIPO_LINK_ID_02_FATURA_PROD_SERV;
    CliInt         := QrArreItsEmpresa.Value;
    Cliente        := QrArreItsEntidade.Value;
    Cedente        := 0;
    Fornece        := 0;
    Periodo        := QrArreItsPeriodo.Value;
    Lancto         := QrArreItsLancto.Value;
    Docum          := QrArreItsBoleto.Value;
    //
    if VAR_KIND_DEPTO = kdUH then
      Depto := QrArreIts.FieldByName('Apto').AsInteger
    else
      Depto := 0;
    {
    DataE          := 0;
    DataD          := 0;
    Retorna        := 0;
    Cancelado      := 0;
    Motivo         := ;
    }
    ID_Cod1        := QrArreItsCodigo.Value; // Prev
    ID_Cod2        := QrArreItsEmpresa.Value; // Empresa
    ID_Cod3        := QrArreItsControle.Value; // ArreIts.Controle;
    ID_Cod4        := 0;
    Vencto         := Geral.FDT(QrArreItsVencto.Value, 1);
    Valor          := QrArreItsValor.Value;
    {
    MoraDiaVal     := 0;
    MultaVal       := 0;
    ComoConf       := 0;
    SerieCH        := '';
    Manual         := ;
    Texto          := ;
    Saiu           := ;
    Recebeu        := ;
    Retornou       := ;
    LimiteSai      := ;
    LimiteRem      := ;
    LimiteRet      := ;
    DataSai        := ;
    DataRec        := ;
    DataRet        := ;
    }
    //
    Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'Conta', stIns, Conta);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakits', False, [
    'Codigo', 'Controle', 'Link_ID',
    'CliInt', 'Cliente', 'Cedente',
    'Fornece', 'Periodo', 'Lancto',
    'Docum', 'Depto', (*'DataE',
    'DataD', 'Retorna', 'Cancelado',
    'Motivo',*) 'ID_Cod1', 'ID_Cod2',
    'ID_Cod3', 'ID_Cod4', 'Vencto',
    'Valor'(*, 'MoraDiaVal', 'MultaVal',
    'ComoConf', 'SerieCH', 'Manual',
    'Texto', 'Saiu', 'Recebeu',
    'Retornou', 'LimiteSai', 'LimiteRem',
    'LimiteRet', 'DataSai', 'DataRec',
    'DataRet'*)], [
    'Conta'], [
    Codigo, Controle, Link_ID,
    CliInt, Cliente, Cedente,
    Fornece, Periodo, Lancto,
    Docum, Depto, (*DataE,
    DataD, Retorna, Cancelado,
    Motivo,*) ID_Cod1, ID_Cod2,
    ID_Cod3, ID_Cod4, Vencto,
    Valor(*, MoraDiaVal, MultaVal,
    ComoConf, SerieCH, Manual,
    Texto, Saiu, Recebeu,
    Retornou, LimiteSai, LimiteRem,
    LimiteRet, DataSai, DataRec,
    DataRet*)], [
    Conta], True) then
    begin
      Protocolo := FProtocoPakCodigo;
      Controle := QrArreItsControle.Value;
      protocopak := FProtocoPakControle;
      protpakits := Conta;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'arreits', False, [
      'Protocolo', 'protocopak', 'protpakits'
      ], ['Controle'], [
      Protocolo, protocopak, protpakits], [Controle], True);
    end;
  end;
var
  Itens, N: Integer;
  Msg: String;
begin
  Itens := DBGArreIts.SelectedRows.Count;
  if Itens > 1 then
    Msg := 's ' + IntToStr(Itens) + ' boletos selecionados?'
  else
    Msg := ' boleto selecionado?';
  if Geral.MensagemBox('Confirma a adi��o ao lote do' + Msg,
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if Itens > 1 then
    begin
      with DBGArreIts.DataSource.DataSet do
      for N := 0 to DBGArreIts.SelectedRows.Count-1 do
      begin
        GotoBookmark(DBGArreIts.SelectedRows.Items[N]);
        AdicionaAtual();
      end;
    end else
      AdicionaAtual();
    //
    if FQrIts <> nil then
    begin
      UMyMod.AbreQuery(FQrIts, Dmod.MyDB);
    end;
  end;
  //
  Close;
end;

procedure TFmBloSemLot.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloSemLot.BtTudoClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(DBGArreIts, True);
end;

procedure TFmBloSemLot.CBCNAB_CfgClick(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmBloSemLot.DesfazPesquisa();
begin
  QrArreIts.Close;
end;

procedure TFmBloSemLot.EdCNAB_CfgChange(Sender: TObject);
begin
  DesfazPesquisa();
end;

procedure TFmBloSemLot.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBloSemLot.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmBloSemLot.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBloSemLot.QrArreItsAfterOpen(DataSet: TDataSet);
begin
  BtOK.Enabled := QrArreIts.RecordCount > 0;
end;

procedure TFmBloSemLot.QrArreItsBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

procedure TFmBloSemLot.ReopenArreIts();
begin
  // Mudar para mostrar apenas os n�o gerados mas do jeito novo
  UnDmkDAC_PF.AbreMySQLQuery0(QrArreIts, Dmod.MyDB, [
    'SELECT ari.*, prv.Empresa, prv.Periodo,  ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
    'FROM arreits ari ',
    'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade ',
    'LEFT JOIN prev prv ON prv.Codigo=ari.Codigo ',
    'WHERE ari.ProtocoloCR=0 ',
    'AND ari.CNAB_Cfg=' + Geral.FF0(EdCNAB_Cfg.ValueVariant),
    'ORDER BY Boleto ',
    '']);
end;

procedure TFmBloSemLot.ReopenCNAB_Cfg(Protocolo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCNAB_Cfg, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM cnab_cfg ',
    'WHERE ProtocolCR=' + Geral.FF0(Protocolo),
    'ORDER BY Nome ',
    '']);
end;

procedure TFmBloSemLot.SBReabreClick(Sender: TObject);
begin
  ReopenArreIts();
end;

end.
