unit FaturCfg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkEditCB, dmkDBLookupComboBox, dmkValUsu;

type
  TFmFaturCfg = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrFaturCfg: TmySQLQuery;
    QrFaturCfgCodigo: TIntegerField;
    QrFaturCfgNome: TWideStringField;
    DsFaturCfg: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    DsCarteiras: TDataSource;
    CBCarteira: TdmkDBLookupComboBox;
    EdCarteira: TdmkEditCB;
    LaCarteira: TLabel;
    SpeedButton5: TSpeedButton;
    CkAtivo: TCheckBox;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VuEmpresa: TdmkValUsu;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    Label5: TLabel;
    DBCheckBox2: TDBCheckBox;
    QrFaturCfgEmpresa: TIntegerField;
    QrFaturCfgAtivo: TSmallintField;
    QrFaturCfgCarteira: TIntegerField;
    QrFaturCfgNOMEEMP: TWideStringField;
    QrFaturCfgNOMECART: TWideStringField;
    GBMulta: TGroupBox;
    Label35: TLabel;
    EdMultaPerc: TdmkEdit;
    EdJurosPerc: TdmkEdit;
    Label61: TLabel;
    QrFaturCfgMultaPerc: TFloatField;
    QrFaturCfgJurosPerc: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFaturCfgAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFaturCfgBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenCarteiras(Empresa: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmFaturCfg: TFmFaturCfg;
const
  FFormatFloat = '00000';

implementation

uses
  {$IfNDef NO_FINANCEIRO} UnFinanceiroJan, {$EndIf}
  UnMyObjects, Module, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFaturCfg.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFaturCfg.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFaturCfgCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFaturCfg.DefParams;
begin
  VAR_GOTOTABELA := 'faturcfg';
  VAR_GOTOMYSQLTABLE := QrFaturCfg;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cfg.*, ');
  {$IfNDef NO_FINANCEIRO}
  VAR_SQLx.Add('car.Nome NOMECART,');
  {$Else}
  VAR_SQLx.Add('"" NOMECART,');
  {$EndIf}
  VAR_SQLx.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEEMP');
  VAR_SQLx.Add('FROM faturcfg cfg');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = cfg.Empresa');
  {$IfNDef NO_FINANCEIRO}
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo = cfg.Carteira');
  {$EndIf}
  VAR_SQLx.Add('WHERE cfg.Codigo > 0');
  //
  VAR_SQL1.Add('AND cfg.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND cfg.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cfg.Nome Like :P0');
  //
end;

procedure TFmFaturCfg.EdEmpresaChange(Sender: TObject);
var
  Empresa: Integer;
begin
  Empresa := VUEmpresa.ValueVariant;
  //
  ReopenCarteiras(Empresa);
end;

procedure TFmFaturCfg.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFaturCfg.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFaturCfg.ReopenCarteiras(Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarteiras, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM carteiras ',
    'WHERE ForneceI=' + Geral.FF0(Empresa),
    'AND Ativo=1',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmFaturCfg.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFaturCfg.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFaturCfg.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFaturCfg.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFaturCfg.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFaturCfg.SpeedButton5Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  Carteira: Integer;
begin
  VAR_CADASTRO := 0;
  Carteira     := EdCarteira.ValueVariant;
  //
  FinanceiroJan.CadastroDeCarteiras(Carteira);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdCarteira, CBCarteira, QrCarteiras, VAR_CADASTRO);
    CBCarteira.SetFocus;
  end;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappFinanceiro);
{$EndIf}
end;

procedure TFmFaturCfg.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFaturCfg.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFaturCfg, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'faturcfg');
end;

procedure TFmFaturCfg.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFaturCfgCodigo.Value;
  Close;
end;

procedure TFmFaturCfg.BtConfirmaClick(Sender: TObject);
var
  Codigo, Empresa, Carteira: Integer;
  Nome: String;
begin
  Nome     := EdNome.ValueVariant;
  Empresa  := EdEmpresa.ValueVariant;
  Carteira := EdCarteira.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a empresa!') then Exit;
  if MyObjects.FIC(Carteira = 0, EdCarteira, 'Defina um carteira!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('faturcfg', 'Codigo', '', '', tsPos,
              ImgTipo.SQLType, QrFaturCfgCodigo.Value);
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'faturcfg', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmFaturCfg.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'faturcfg', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFaturCfg.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFaturCfg, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'faturcfg');
  //
  EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
end;

procedure TFmFaturCfg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmFaturCfg.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFaturCfgCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFaturCfg.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmFaturCfg.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFaturCfg.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmFaturCfg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFaturCfg.QrFaturCfgAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFaturCfg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFaturCfg.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFaturCfgCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'faturcfg', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFaturCfg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFaturCfg.QrFaturCfgBeforeOpen(DataSet: TDataSet);
begin
  QrFaturCfgCodigo.DisplayFormat := FFormatFloat;
end;

end.

