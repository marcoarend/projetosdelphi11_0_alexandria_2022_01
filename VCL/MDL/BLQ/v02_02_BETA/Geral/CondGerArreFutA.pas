unit CondGerArreFutA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkImage, UnDmkEnums,
  Data.DB, mySQLDbTables, DmkDAC_PF;

type
  THackDBGrid = class(TDBGrid);
  TFmCondGerArreFutA = class(TForm)
    Panel1: TPanel;
    DBGEmpresas: TDBGrid;
    Panel3: TPanel;
    CkNaoArreRisco: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BtSaida: TBitBtn;
    QrLoc: TmySQLQuery;
    DBGCondominios: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure DBGEmpresasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGEmpresasCellClick(Column: TColumn);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGCondominiosCellClick(Column: TColumn);
    procedure DBGCondominiosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    procedure SetaTodos(Marca: Boolean);
  public
    { Public declarations }
    FCodigo: Integer;
    FTabAriA: String;
    FQueryArreFutA, FQueryArreFutI: TmySQLQuery;
    FDataSourceArreFutA: TDataSource;
  end;

  var
  FmCondGerArreFutA: TFmCondGerArreFutA;

implementation

uses Module, MyVCLSkin, UMySQLModule, UnInternalConsts, UnMyObjects, UnBloquetos;

{$R *.DFM}

procedure TFmCondGerArreFutA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerArreFutA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerArreFutA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCondGerArreFutA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerArreFutA.FormShow(Sender: TObject);
begin
  if VAR_KIND_DEPTO = kdUH then
  begin
    DBGEmpresas.Visible       := False;
    DBGCondominios.Visible    := True;
    DBGCondominios.Align      := alClient;
    DBGCondominios.DataSource := FDataSourceArreFutA;
  end else
  begin
    DBGCondominios.Visible := False;
    DBGEmpresas.Visible    := True;
    DBGEmpresas.Align      := alClient;
    DBGEmpresas.DataSource := FDataSourceArreFutA;
  end;
end;

procedure TFmCondGerArreFutA.BitBtn2Click(Sender: TObject);
begin
  SetaTodos(True);
end;

procedure TFmCondGerArreFutA.BitBtn3Click(Sender: TObject);
begin
  SetaTodos(False);
end;

procedure TFmCondGerArreFutA.SetaTodos(Marca: Boolean);
var
  Controle, Status: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Controle := FQueryArreFutA.FieldByName('Controle').AsInteger;
    Status   := Geral.BoolToInt(Marca);
    //
    FQueryArreFutA.DisableControls;
    //
    FQueryArreFutA.First;
    while not FQueryArreFutA.Eof do
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE ArreFut SET ',
        'Inclui=' + Geral.FF0(Status),
        'WHERE Controle=' + Geral.FF0(FQueryArreFutA.FieldByName('Controle').AsInteger),
        '']);
      //
      FQueryArreFutA.Next;
    end;
  finally
    FQueryArreFutA.EnableControls;
    UMyMod.AbreQuery(FQueryArreFutA, Dmod.MyDB);
    FQueryArreFutA.Locate('Controle', Controle, []);
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGerArreFutA.DBGEmpresasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Inclui' then
    MeuVCLSkin.DrawGrid(DBGEmpresas, Rect, 1, FQueryArreFutA.FieldByName('Inclui').AsInteger);
end;

procedure TFmCondGerArreFutA.DBGCondominiosCellClick(Column: TColumn);
var
  Campo: String;
  Controle, Status: Integer;
begin
  Campo := UpperCase(DBGCondominios.Columns[THackDBGrid(DBGCondominios).Col -1].FieldName);
  //
  if UpperCase(Campo) = UpperCase('Inclui') then
  begin
    Screen.Cursor := crHourGlass;
    try
      Controle := FQueryArreFutA.FieldByName('Controle').AsInteger;
      //
      if FQueryArreFutA.FieldByName('Inclui').AsInteger = 0 then
        Status := 1
      else
        Status := 0;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE ArreFut SET ',
        'Inclui=' + Geral.FF0(Status),
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
      //
      UMyMod.AbreQuery(FQueryArreFutA, Dmod.MyDB);
      FQueryArreFutA.Locate('Controle', Controle, []);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmCondGerArreFutA.DBGCondominiosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Inclui' then
    MeuVCLSkin.DrawGrid(DBGCondominios, Rect, 1, FQueryArreFutA.FieldByName('Inclui').AsInteger);
end;

procedure TFmCondGerArreFutA.DBGEmpresasCellClick(Column: TColumn);
var
  Campo: String;
  Controle, Status: Integer;
begin
  Campo := UpperCase(DBGEmpresas.Columns[THackDBGrid(DBGEmpresas).Col -1].FieldName);
  //
  if UpperCase(Campo) = UpperCase('Inclui') then
  begin
    Screen.Cursor := crHourGlass;
    try
      Controle := FQueryArreFutA.FieldByName('Controle').AsInteger;
      //
      if FQueryArreFutA.FieldByName('Inclui').AsInteger = 0 then
        Status := 1
      else
        Status := 0;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE ArreFut SET ',
        'Inclui=' + Geral.FF0(Status),
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
      //
      UMyMod.AbreQuery(FQueryArreFutA, Dmod.MyDB);
      FQueryArreFutA.Locate('Controle', Controle, []);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmCondGerArreFutA.BtOKClick(Sender: TObject);
var
  Controle, Contar, CNAB_Cfg, Fatur_Cfg, Tipo, Config: Integer;
  Msg, MsgVal: String;
begin
  Screen.Cursor := crHourGlass;
  try
    FQueryArreFutA.First;
    //
    Contar := 0;
    MsgVal := '';
    //
    while not FQueryArreFutA.Eof do
    begin
      if FQueryArreFutA.FieldByName('Inclui').AsInteger = 1 then
      begin
        if VAR_KIND_DEPTO = kdUH then
        begin
          Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                      FTabAriA, TAB_ARI, 'Controle');
          //
          CNAB_Cfg  := FQueryArreFutA.FieldByName('CNAB_Cfg').AsInteger;
          Fatur_Cfg := FQueryArreFutA.FieldByName('Fatur_Cfg').AsInteger;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, FTabAriA, False,
            ['ComoAdd',
            'Conta',
            'Valor',
            'Texto',
            'ArreBaC',
            'ArreBaI',
            'Apto',
            'Propriet',
            'NaoArreRisco',
            'CNAB_Cfg',
            'Fatur_Cfg',
            'NFSeSrvCad',
            'Codigo'], ['Controle'],
            [2,
            FQueryArreFutA.FieldByName('Conta').AsInteger,
            FQueryArreFutA.FieldByName('Valor').AsFloat,
            FQueryArreFutA.FieldByName('Texto').AsString,
            0,
            0,
            FQueryArreFutA.FieldByName('Depto').AsInteger,
            FQueryArreFutA.FieldByName('Propriet').AsInteger,
            Geral.BoolToInt(CkNaoArreRisco.Checked),
            CNAB_Cfg,
            Fatur_Cfg,
            FQueryArreFutA.FieldByName('NFSeSrvCad').AsInteger,
            FCodigo], [Controle], True) then
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(DMod.QrUpdM, Dmod.MyDB, [
              'UPDATE ArreFut SET ',
              TAB_ARI + '=' + Geral.FF0(Controle),
              'WHERE Controle=' + Geral.FF0(FQueryArreFutA.FieldByName('Controle').AsInteger),
              '']);
            Contar := Contar + 1;
          end;
        end else
        begin
          CNAB_Cfg  := FQueryArreFutA.FieldByName('CNAB_Cfg').AsInteger;
          Fatur_Cfg := FQueryArreFutA.FieldByName('Fatur_Cfg').AsInteger;
          //
          if CNAB_Cfg <> 0 then
          begin
            Tipo   := 0;
            Config := CNAB_Cfg;
          end else
          begin
            Tipo   := 1;
            Config := Fatur_Cfg;
          end;
          Controle := UBloquetos.AtualizaArrecadacao(stIns, FCodigo, 0,
                        FQueryArreFutA.FieldByName('Conta').AsInteger, 0, 0,
                        FQueryArreFutA.FieldByName('Depto').AsInteger,
                        FQueryArreFutA.FieldByName('CartEmiss').AsInteger,
                        Tipo, Config,
                        FQueryArreFutA.FieldByName('NFSeSrvCad').AsInteger,
                        0, 0, 2, 0, 0, '0000-00-00',
                        FQueryArreFutA.FieldByName('Valor').AsFloat, 0, 0,
                        FQueryArreFutA.FieldByName('Texto').AsString, Msg);
          if Controle <> 0 then          
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(DMod.QrUpd, Dmod.MyDB, [
              'UPDATE ArreFut SET ',
              TAB_ARI + '=' + Geral.FF0(Controle),
              'WHERE Controle=' + Geral.FF0(FQueryArreFutA.FieldByName('Controle').AsInteger),
              '']);
            Contar := Contar + 1;
          end;
        end;
      end;
      FQueryArreFutA.Next;
    end;
    if MsgVal <> '' then
      Geral.MB_Aviso(MsgVal);
    //
    UMyMod.AbreQuery(FQueryArreFutA, Dmod.MyDB);
    UMyMod.AbreQuery(FQueryArreFutI, Dmod.MyDB);
    //
    case Contar of
        0: Msg := 'Nenhum item foi inclu�do!';
        1: Msg := 'Um item foi inclu�do!';
      else Msg := Geral.FF0(Contar) + ' itens foram inclu�dos!';
    end;
    Geral.MB_Aviso(Msg);
    //
    if FQueryArreFutA.RecordCount = 0 then
      Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
