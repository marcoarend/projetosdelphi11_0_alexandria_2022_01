object DBloqGerl: TDBloqGerl
  OldCreateOrder = False
  Height = 274
  Width = 367
  object frxDsCNAB_Cfg: TfrxDBDataset
    UserName = 'frxDsCNAB_Cfg'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'Cedente=Cedente'
      'SacadAvali=SacadAvali'
      'CedBanco=CedBanco'
      'CedAgencia=CedAgencia'
      'CedConta=CedConta'
      'CedDAC_A=CedDAC_A'
      'CedDAC_C=CedDAC_C'
      'CedDAC_AC=CedDAC_AC'
      'CedNome=CedNome'
      'CedPosto=CedPosto'
      'SacAvaNome=SacAvaNome'
      'TipoCart=TipoCart'
      'CartNum=CartNum'
      'CartCod=CartCod'
      'EspecieTit=EspecieTit'
      'EspecieDoc=EspecieDoc'
      'AceiteTit=AceiteTit'
      'InstrCobr1=InstrCobr1'
      'InstrCobr2=InstrCobr2'
      'InstrDias=InstrDias'
      'CodEmprBco=CodEmprBco'
      'JurosTipo=JurosTipo'
      'JurosPerc=JurosPerc'
      'JurosDias=JurosDias'
      'MultaTipo=MultaTipo'
      'MultaPerc=MultaPerc'
      'MultaDias=MultaDias'
      'Texto01=Texto01'
      'Texto02=Texto02'
      'Texto03=Texto03'
      'Texto04=Texto04'
      'Texto05=Texto05'
      'Texto06=Texto06'
      'Texto07=Texto07'
      'Texto08=Texto08'
      'Texto09=Texto09'
      'Texto10=Texto10'
      'CNAB=CNAB'
      'EnvEmeio=EnvEmeio'
      'Diretorio=Diretorio'
      'QuemPrint=QuemPrint'
      '_237Mens1=_237Mens1'
      '_237Mens2=_237Mens2'
      'CodOculto=CodOculto'
      'SeqArq=SeqArq'
      'LastNosNum=LastNosNum'
      'LocalPag=LocalPag'
      'EspecieVal=EspecieVal'
      'OperCodi=OperCodi'
      'IDCobranca=IDCobranca'
      'AgContaCed=AgContaCed'
      'DirRetorno=DirRetorno'
      'CartRetorno=CartRetorno'
      'PosicoesBB=PosicoesBB'
      'IndicatBB=IndicatBB'
      'TipoCobrBB=TipoCobrBB'
      'Variacao=Variacao'
      'Comando=Comando'
      'DdProtesBB=DdProtesBB'
      'Msg40posBB=Msg40posBB'
      'QuemDistrb=QuemDistrb'
      'Desco1Cod=Desco1Cod'
      'Desco1Dds=Desco1Dds'
      'Desco1Fat=Desco1Fat'
      'Desco2Cod=Desco2Cod'
      'Desco2Dds=Desco2Dds'
      'Desco2Fat=Desco2Fat'
      'Desco3Cod=Desco3Cod'
      'Desco3Dds=Desco3Dds'
      'Desco3Fat=Desco3Fat'
      'ProtesCod=ProtesCod'
      'ProtesDds=ProtesDds'
      'BxaDevCod=BxaDevCod'
      'BxaDevDds=BxaDevDds'
      'MoedaCod=MoedaCod'
      'Contrato=Contrato'
      'ConvCartCobr=ConvCartCobr'
      'ConvVariCart=ConvVariCart'
      'InfNossoNum=InfNossoNum'
      'CodLidrBco=CodLidrBco'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'CartEmiss=CartEmiss'
      'DVB=DVB'
      'NOMEBANCO=NOMEBANCO'
      'NOMECED_IMP=NOMECED_IMP'
      'TipoCobranca=TipoCobranca'
      'EspecieTxt=EspecieTxt'
      'CartTxt=CartTxt'
      'LayoutRem=LayoutRem'
      'LayoutRet=LayoutRet'
      'ConcatCod=ConcatCod'
      'ComplmCod=ComplmCod'
      'NaoRecebDd=NaoRecebDd'
      'TipBloqUso=TipBloqUso'
      'NumVersaoI3=NumVersaoI3'
      'ModalCobr=ModalCobr'
      'CtaCooper=CtaCooper'
      'TermoAceite=TermoAceite'
      'CorresBco=CorresBco'
      'CorresAge=CorresAge'
      'CorresCto=CorresCto'
      'NPrinBc=NPrinBc'
      'ACEITETIT_TXT=ACEITETIT_TXT'
      'CART_IMP=CART_IMP'
      'Correio_TXT=Correio_TXT')
    DataSet = QrCNAB_Cfg
    BCDToCurrency = False
    Left = 174
    Top = 27
  end
  object QrCNAB_Cfg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT bco.Nome NOMEBANCO, bco.DVB,  cfg.*,'
      
        'IF(cfg.Cedente=0,"",IF(cfg.CedNome <> "", cfg.CedNome, IF(ced.Ti' +
        'po=0,ced.RazaoSocial,ced.Nome))) NOMECED_IMP,'
      'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CartNum,'
      'IF(cfg.AceiteTit = 1, "S", "N") ACEITETIT_TXT,'
      'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CART_IMP,'
      'IF(cfg.Correio = 1, "S", "N") Correio_TXT'
      'FROM cnab_cfg cfg'
      'LEFT JOIN bancos bco ON bco.Codigo=cfg.CedBanco'
      'LEFT JOIN entidades ced ON ced.Codigo=cfg.Cedente'
      'WHERE cfg.Codigo=:P0')
    Left = 146
    Top = 27
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_cfg.Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrCNAB_CfgCedente: TIntegerField
      FieldName = 'Cedente'
      Origin = 'cnab_cfg.Cedente'
    end
    object QrCNAB_CfgSacadAvali: TIntegerField
      FieldName = 'SacadAvali'
      Origin = 'cnab_cfg.SacadAvali'
    end
    object QrCNAB_CfgCedBanco: TIntegerField
      FieldName = 'CedBanco'
      Origin = 'cnab_cfg.CedBanco'
    end
    object QrCNAB_CfgCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
      Origin = 'cnab_cfg.CedAgencia'
    end
    object QrCNAB_CfgCedConta: TWideStringField
      FieldName = 'CedConta'
      Origin = 'cnab_cfg.CedConta'
      Size = 30
    end
    object QrCNAB_CfgCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Origin = 'cnab_cfg.CedDAC_A'
      Size = 1
    end
    object QrCNAB_CfgCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Origin = 'cnab_cfg.CedDAC_C'
      Size = 1
    end
    object QrCNAB_CfgCedDAC_AC: TWideStringField
      FieldName = 'CedDAC_AC'
      Origin = 'cnab_cfg.CedDAC_AC'
      Size = 1
    end
    object QrCNAB_CfgCedNome: TWideStringField
      FieldName = 'CedNome'
      Origin = 'cnab_cfg.CedNome'
      Size = 100
    end
    object QrCNAB_CfgCedPosto: TIntegerField
      FieldName = 'CedPosto'
      Origin = 'cnab_cfg.CedPosto'
    end
    object QrCNAB_CfgSacAvaNome: TWideStringField
      FieldName = 'SacAvaNome'
      Origin = 'cnab_cfg.SacAvaNome'
      Size = 100
    end
    object QrCNAB_CfgTipoCart: TSmallintField
      FieldName = 'TipoCart'
      Origin = 'cnab_cfg.TipoCart'
    end
    object QrCNAB_CfgCartNum: TWideStringField
      FieldName = 'CartNum'
      Origin = 'cnab_cfg.CartNum'
      Size = 3
    end
    object QrCNAB_CfgCartCod: TWideStringField
      FieldName = 'CartCod'
      Origin = 'cnab_cfg.CartCod'
      Size = 3
    end
    object QrCNAB_CfgEspecieTit: TWideStringField
      FieldName = 'EspecieTit'
      Origin = 'cnab_cfg.EspecieTit'
      Size = 6
    end
    object QrCNAB_CfgEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
      Origin = 'cnab_cfg.EspecieDoc'
    end
    object QrCNAB_CfgAceiteTit: TSmallintField
      FieldName = 'AceiteTit'
      Origin = 'cnab_cfg.AceiteTit'
    end
    object QrCNAB_CfgInstrCobr1: TWideStringField
      FieldName = 'InstrCobr1'
      Origin = 'cnab_cfg.InstrCobr1'
      Size = 2
    end
    object QrCNAB_CfgInstrCobr2: TWideStringField
      FieldName = 'InstrCobr2'
      Origin = 'cnab_cfg.InstrCobr2'
      Size = 2
    end
    object QrCNAB_CfgInstrDias: TSmallintField
      FieldName = 'InstrDias'
      Origin = 'cnab_cfg.InstrDias'
    end
    object QrCNAB_CfgCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
      Origin = 'cnab_cfg.CodEmprBco'
    end
    object QrCNAB_CfgJurosTipo: TSmallintField
      FieldName = 'JurosTipo'
      Origin = 'cnab_cfg.JurosTipo'
    end
    object QrCNAB_CfgJurosPerc: TFloatField
      FieldName = 'JurosPerc'
      Origin = 'cnab_cfg.JurosPerc'
    end
    object QrCNAB_CfgJurosDias: TSmallintField
      FieldName = 'JurosDias'
      Origin = 'cnab_cfg.JurosDias'
    end
    object QrCNAB_CfgMultaTipo: TSmallintField
      FieldName = 'MultaTipo'
      Origin = 'cnab_cfg.MultaTipo'
    end
    object QrCNAB_CfgMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'cnab_cfg.MultaPerc'
    end
    object QrCNAB_CfgMultaDias: TSmallintField
      FieldName = 'MultaDias'
      Origin = 'cnab_cfg.MultaDias'
    end
    object QrCNAB_CfgTexto01: TWideStringField
      FieldName = 'Texto01'
      Origin = 'cnab_cfg.Texto01'
      Size = 100
    end
    object QrCNAB_CfgTexto02: TWideStringField
      FieldName = 'Texto02'
      Origin = 'cnab_cfg.Texto02'
      Size = 100
    end
    object QrCNAB_CfgTexto03: TWideStringField
      FieldName = 'Texto03'
      Origin = 'cnab_cfg.Texto03'
      Size = 100
    end
    object QrCNAB_CfgTexto04: TWideStringField
      FieldName = 'Texto04'
      Origin = 'cnab_cfg.Texto04'
      Size = 100
    end
    object QrCNAB_CfgTexto05: TWideStringField
      FieldName = 'Texto05'
      Origin = 'cnab_cfg.Texto05'
      Size = 100
    end
    object QrCNAB_CfgTexto06: TWideStringField
      FieldName = 'Texto06'
      Origin = 'cnab_cfg.Texto06'
      Size = 100
    end
    object QrCNAB_CfgTexto07: TWideStringField
      FieldName = 'Texto07'
      Origin = 'cnab_cfg.Texto07'
      Size = 100
    end
    object QrCNAB_CfgTexto08: TWideStringField
      FieldName = 'Texto08'
      Origin = 'cnab_cfg.Texto08'
      Size = 100
    end
    object QrCNAB_CfgTexto09: TWideStringField
      FieldName = 'Texto09'
      Origin = 'cnab_cfg.Texto09'
      Size = 100
    end
    object QrCNAB_CfgTexto10: TWideStringField
      FieldName = 'Texto10'
      Origin = 'cnab_cfg.Texto10'
      Size = 100
    end
    object QrCNAB_CfgCNAB: TIntegerField
      FieldName = 'CNAB'
      Origin = 'cnab_cfg.CNAB'
    end
    object QrCNAB_CfgEnvEmeio: TSmallintField
      FieldName = 'EnvEmeio'
      Origin = 'cnab_cfg.EnvEmeio'
    end
    object QrCNAB_CfgDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Origin = 'cnab_cfg.Diretorio'
      Size = 255
    end
    object QrCNAB_CfgQuemPrint: TWideStringField
      FieldName = 'QuemPrint'
      Origin = 'cnab_cfg.QuemPrint'
      Size = 1
    end
    object QrCNAB_Cfg_237Mens1: TWideStringField
      FieldName = '_237Mens1'
      Origin = 'cnab_cfg._237Mens1'
      Size = 12
    end
    object QrCNAB_Cfg_237Mens2: TWideStringField
      FieldName = '_237Mens2'
      Origin = 'cnab_cfg._237Mens2'
      Size = 60
    end
    object QrCNAB_CfgCodOculto: TWideStringField
      FieldName = 'CodOculto'
      Origin = 'cnab_cfg.CodOculto'
    end
    object QrCNAB_CfgSeqArq: TIntegerField
      FieldName = 'SeqArq'
      Origin = 'cnab_cfg.SeqArq'
    end
    object QrCNAB_CfgLastNosNum: TLargeintField
      FieldName = 'LastNosNum'
      Origin = 'cnab_cfg.LastNosNum'
    end
    object QrCNAB_CfgLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Origin = 'cnab_cfg.LocalPag'
      Size = 127
    end
    object QrCNAB_CfgEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Origin = 'cnab_cfg.EspecieVal'
      Size = 5
    end
    object QrCNAB_CfgOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Origin = 'cnab_cfg.OperCodi'
      Size = 3
    end
    object QrCNAB_CfgIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Origin = 'cnab_cfg.IDCobranca'
      Size = 2
    end
    object QrCNAB_CfgAgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Origin = 'cnab_cfg.AgContaCed'
      Size = 40
    end
    object QrCNAB_CfgDirRetorno: TWideStringField
      FieldName = 'DirRetorno'
      Origin = 'cnab_cfg.DirRetorno'
      Size = 255
    end
    object QrCNAB_CfgCartRetorno: TIntegerField
      FieldName = 'CartRetorno'
      Origin = 'cnab_cfg.CartRetorno'
    end
    object QrCNAB_CfgPosicoesBB: TSmallintField
      FieldName = 'PosicoesBB'
      Origin = 'cnab_cfg.PosicoesBB'
    end
    object QrCNAB_CfgIndicatBB: TWideStringField
      FieldName = 'IndicatBB'
      Origin = 'cnab_cfg.IndicatBB'
      Size = 1
    end
    object QrCNAB_CfgTipoCobrBB: TWideStringField
      FieldName = 'TipoCobrBB'
      Origin = 'cnab_cfg.TipoCobrBB'
      Size = 5
    end
    object QrCNAB_CfgVariacao: TIntegerField
      FieldName = 'Variacao'
      Origin = 'cnab_cfg.Variacao'
    end
    object QrCNAB_CfgComando: TIntegerField
      FieldName = 'Comando'
      Origin = 'cnab_cfg.Comando'
    end
    object QrCNAB_CfgDdProtesBB: TIntegerField
      FieldName = 'DdProtesBB'
      Origin = 'cnab_cfg.DdProtesBB'
    end
    object QrCNAB_CfgMsg40posBB: TWideStringField
      FieldName = 'Msg40posBB'
      Origin = 'cnab_cfg.Msg40posBB'
      Size = 40
    end
    object QrCNAB_CfgQuemDistrb: TWideStringField
      FieldName = 'QuemDistrb'
      Origin = 'cnab_cfg.QuemDistrb'
      Size = 1
    end
    object QrCNAB_CfgDesco1Cod: TSmallintField
      FieldName = 'Desco1Cod'
      Origin = 'cnab_cfg.Desco1Cod'
    end
    object QrCNAB_CfgDesco1Dds: TIntegerField
      FieldName = 'Desco1Dds'
      Origin = 'cnab_cfg.Desco1Dds'
    end
    object QrCNAB_CfgDesco1Fat: TFloatField
      FieldName = 'Desco1Fat'
      Origin = 'cnab_cfg.Desco1Fat'
    end
    object QrCNAB_CfgDesco2Cod: TSmallintField
      FieldName = 'Desco2Cod'
      Origin = 'cnab_cfg.Desco2Cod'
    end
    object QrCNAB_CfgDesco2Dds: TIntegerField
      FieldName = 'Desco2Dds'
      Origin = 'cnab_cfg.Desco2Dds'
    end
    object QrCNAB_CfgDesco2Fat: TFloatField
      FieldName = 'Desco2Fat'
      Origin = 'cnab_cfg.Desco2Fat'
    end
    object QrCNAB_CfgDesco3Cod: TSmallintField
      FieldName = 'Desco3Cod'
      Origin = 'cnab_cfg.Desco3Cod'
    end
    object QrCNAB_CfgDesco3Dds: TIntegerField
      FieldName = 'Desco3Dds'
      Origin = 'cnab_cfg.Desco3Dds'
    end
    object QrCNAB_CfgDesco3Fat: TFloatField
      FieldName = 'Desco3Fat'
      Origin = 'cnab_cfg.Desco3Fat'
    end
    object QrCNAB_CfgProtesCod: TSmallintField
      FieldName = 'ProtesCod'
      Origin = 'cnab_cfg.ProtesCod'
    end
    object QrCNAB_CfgProtesDds: TSmallintField
      FieldName = 'ProtesDds'
      Origin = 'cnab_cfg.ProtesDds'
    end
    object QrCNAB_CfgBxaDevCod: TSmallintField
      FieldName = 'BxaDevCod'
      Origin = 'cnab_cfg.BxaDevCod'
    end
    object QrCNAB_CfgBxaDevDds: TIntegerField
      FieldName = 'BxaDevDds'
      Origin = 'cnab_cfg.BxaDevDds'
    end
    object QrCNAB_CfgMoedaCod: TWideStringField
      FieldName = 'MoedaCod'
      Origin = 'cnab_cfg.MoedaCod'
    end
    object QrCNAB_CfgContrato: TFloatField
      FieldName = 'Contrato'
      Origin = 'cnab_cfg.Contrato'
    end
    object QrCNAB_CfgConvCartCobr: TWideStringField
      FieldName = 'ConvCartCobr'
      Origin = 'cnab_cfg.ConvCartCobr'
      Size = 2
    end
    object QrCNAB_CfgConvVariCart: TWideStringField
      FieldName = 'ConvVariCart'
      Origin = 'cnab_cfg.ConvVariCart'
      Size = 3
    end
    object QrCNAB_CfgInfNossoNum: TSmallintField
      FieldName = 'InfNossoNum'
      Origin = 'cnab_cfg.InfNossoNum'
    end
    object QrCNAB_CfgCodLidrBco: TWideStringField
      FieldName = 'CodLidrBco'
      Origin = 'cnab_cfg.CodLidrBco'
    end
    object QrCNAB_CfgLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cnab_cfg.Lk'
    end
    object QrCNAB_CfgDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cnab_cfg.DataCad'
    end
    object QrCNAB_CfgDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cnab_cfg.DataAlt'
    end
    object QrCNAB_CfgUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cnab_cfg.UserCad'
    end
    object QrCNAB_CfgUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cnab_cfg.UserAlt'
    end
    object QrCNAB_CfgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'cnab_cfg.AlterWeb'
    end
    object QrCNAB_CfgAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cnab_cfg.Ativo'
    end
    object QrCNAB_CfgCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Origin = 'cnab_cfg.CartEmiss'
    end
    object QrCNAB_CfgDVB: TWideStringField
      FieldName = 'DVB'
      Origin = 'bancos.DVB'
      Size = 1
    end
    object QrCNAB_CfgNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'bancos.Nome'
      Size = 100
    end
    object QrCNAB_CfgNOMECED_IMP: TWideStringField
      FieldName = 'NOMECED_IMP'
      Size = 100
    end
    object QrCNAB_CfgTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
    end
    object QrCNAB_CfgEspecieTxt: TWideStringField
      FieldName = 'EspecieTxt'
    end
    object QrCNAB_CfgCartTxt: TWideStringField
      FieldName = 'CartTxt'
      Size = 10
    end
    object QrCNAB_CfgLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrCNAB_CfgLayoutRet: TWideStringField
      FieldName = 'LayoutRet'
      Size = 50
    end
    object QrCNAB_CfgConcatCod: TWideStringField
      FieldName = 'ConcatCod'
      Size = 50
    end
    object QrCNAB_CfgComplmCod: TWideStringField
      FieldName = 'ComplmCod'
      Size = 10
    end
    object QrCNAB_CfgNaoRecebDd: TSmallintField
      FieldName = 'NaoRecebDd'
    end
    object QrCNAB_CfgTipBloqUso: TWideStringField
      FieldName = 'TipBloqUso'
      Size = 1
    end
    object QrCNAB_CfgNumVersaoI3: TIntegerField
      FieldName = 'NumVersaoI3'
    end
    object QrCNAB_CfgModalCobr: TSmallintField
      FieldName = 'ModalCobr'
    end
    object QrCNAB_CfgCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrCNAB_CfgTermoAceite: TIntegerField
      FieldName = 'TermoAceite'
    end
    object QrCNAB_CfgCorresBco: TIntegerField
      FieldName = 'CorresBco'
    end
    object QrCNAB_CfgCorresAge: TIntegerField
      FieldName = 'CorresAge'
    end
    object QrCNAB_CfgCorresCto: TWideStringField
      FieldName = 'CorresCto'
      Size = 30
    end
    object QrCNAB_CfgNPrinBc: TWideStringField
      FieldName = 'NPrinBc'
      Size = 30
    end
    object QrCNAB_CfgACEITETIT_TXT: TWideStringField
      FieldName = 'ACEITETIT_TXT'
      Size = 1
    end
    object QrCNAB_CfgCART_IMP: TWideStringField
      FieldName = 'CART_IMP'
      Size = 10
    end
    object QrCNAB_CfgCorreio_TXT: TWideStringField
      FieldName = 'Correio_TXT'
      Size = 1
    end
  end
end
