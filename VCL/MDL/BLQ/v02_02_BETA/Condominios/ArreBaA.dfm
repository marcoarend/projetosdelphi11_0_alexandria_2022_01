object FmArreBaA: TFmArreBaA
  Left = 306
  Top = 185
  Caption = 'GER-CONDM-022 :: Adi'#231#227'o de Contas Bases no Or'#231'amento'
  ClientHeight = 494
  ClientWidth = 909
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 909
    Height = 332
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object StaticText2: TStaticText
      Left = 0
      Top = 0
      Width = 909
      Height = 28
      Align = alTop
      Alignment = taCenter
      Caption = 'StaticText2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -21
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 28
      Width = 420
      Height = 304
      Align = alLeft
      DataSource = DsArreBaA
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnDrawColumnCell = DBGrid1DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Adiciona'
          ReadOnly = True
          Title.Caption = 'OK'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          ReadOnly = True
          Title.Caption = 'Texto a ser impresso'
          Width = 160
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          ReadOnly = True
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTABASE'
          ReadOnly = True
          Title.Caption = 'Conta'
          Width = 140
          Visible = True
        end>
    end
    object DBGrid2: TDBGrid
      Left = 420
      Top = 28
      Width = 489
      Height = 304
      Align = alClient
      DataSource = DsArreBaAUni
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid2CellClick
      OnDrawColumnCell = DBGrid2DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Adiciona'
          ReadOnly = True
          Title.Caption = 'OK'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          ReadOnly = True
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomePropriet'
          ReadOnly = True
          Title.Caption = 'Propriet'#225'rio'
          Width = 211
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TextoCota'
          Title.Caption = 'Descri'#231#227'o'
          Width = 120
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 909
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 861
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 813
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 480
        Height = 32
        Caption = 'Adi'#231#227'o de Contas Bases no Or'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 380
    Width = 909
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 905
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 437
        Height = 16
        Caption = 
          '***   Edit'#225'veis  clicando no bot'#227'o de edi'#231#227'o que aparece ao clic' +
          'ar na c'#233'lula.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 437
        Height = 16
        Caption = 
          '***   Edit'#225'veis  clicando no bot'#227'o de edi'#231#227'o que aparece ao clic' +
          'ar na c'#233'lula.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 424
    Width = 909
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 905
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 761
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 4
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn2: TBitBtn
        Tag = 127
        Left = 145
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn2Click
      end
      object BitBtn3: TBitBtn
        Tag = 128
        Left = 270
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BitBtn3Click
      end
      object BitBtn1: TBitBtn
        Tag = 12
        Left = 395
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BitBtn1Click
      end
      object BitBtn4: TBitBtn
        Tag = 335
        Left = 520
        Top = 3
        Width = 180
        Height = 40
        Caption = '&Arrecada'#231#227'o base'
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BitBtn4Click
      end
    end
  end
  object TbArreBaA: TmySQLTable
    Database = DModG.MyPID_DB
    AfterInsert = TbArreBaAAfterInsert
    BeforePost = TbArreBaABeforePost
    AfterScroll = TbArreBaAAfterScroll
    TableName = 'ArreBaA'
    Left = 8
    Top = 8
    object TbArreBaAConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'ArreBaA.Conta'
    end
    object TbArreBaAArreBaI: TIntegerField
      FieldName = 'ArreBaI'
      Origin = 'ArreBaA.ArreBaI'
    end
    object TbArreBaAArreBaC: TIntegerField
      FieldName = 'ArreBaC'
      Origin = 'ArreBaA.ArreBaC'
    end
    object TbArreBaAValor: TFloatField
      FieldName = 'Valor'
      Origin = 'ArreBaA.Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object TbArreBaATexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'ArreBaA.Texto'
      Size = 40
    end
    object TbArreBaAAdiciona: TSmallintField
      FieldName = 'Adiciona'
      Origin = 'ArreBaA.Adiciona'
      MaxValue = 1
    end
    object TbArreBaANOMECONTABASE: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECONTABASE'
      LookupDataSet = QrArreBaC
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'ArreBaC'
      Size = 50
      Lookup = True
    end
    object TbArreBaASeq: TIntegerField
      FieldName = 'Seq'
    end
    object TbArreBaACNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object TbArreBaANFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object TbArreBaAFatur_Cfg: TIntegerField
      FieldName = 'Fatur_Cfg'
    end
  end
  object DsArreBaA: TDataSource
    DataSet = TbArreBaA
    Left = 36
    Top = 8
  end
  object QrArreBaC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Conta, Nome'
      'FROM arrebac')
    Left = 120
    Top = 8
    object QrArreBaCCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'Arrebac.Codigo'
    end
    object QrArreBaCConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'Arrebac.Conta'
    end
    object QrArreBaCNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'Arrebac.Nome'
      Size = 40
    end
  end
  object DsArreBaC: TDataSource
    DataSet = QrArreBaC
    Left = 148
    Top = 8
  end
  object QrConta: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT COUNT(Valor) Zeros'
      'FROM arreban'
      'WHERE Valor < 0.01')
    Left = 704
    Top = 12
    object QrContaZeros: TLargeintField
      FieldName = 'Zeros'
      Required = True
    end
  end
  object TbArreBaAUni: TmySQLTable
    Database = DModG.MyPID_DB
    Filtered = True
    AfterInsert = TbArreBaAAfterInsert
    BeforePost = TbArreBaAUniBeforePost
    TableName = 'arrebaauni'
    Left = 64
    Top = 8
    object TbArreBaAUniSeq: TIntegerField
      Alignment = taCenter
      FieldName = 'Seq'
      Origin = 'arrebaauni.Seq'
    end
    object TbArreBaAUniApto: TIntegerField
      FieldName = 'Apto'
      Origin = 'arrebaauni.Apto'
    end
    object TbArreBaAUniUnidade: TWideStringField
      Alignment = taCenter
      FieldName = 'Unidade'
      Origin = 'arrebaauni.Unidade'
      Size = 10
    end
    object TbArreBaAUniValor: TFloatField
      FieldName = 'Valor'
      Origin = 'arrebaauni.Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object TbArreBaAUniAdiciona: TSmallintField
      FieldName = 'Adiciona'
      Origin = 'arrebaauni.Adiciona'
    end
    object TbArreBaAUniPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'arrebaauni.Propriet'
    end
    object TbArreBaAUniNomePropriet: TWideStringField
      FieldName = 'NomePropriet'
      Origin = 'arrebaauni.NomePropriet'
      Size = 100
    end
    object TbArreBaAUniTextoCota: TWideStringField
      FieldName = 'TextoCota'
      Origin = 'arrebaauni.TextoCota'
      Size = 30
    end
    object TbArreBaAUniNaoArreSobre: TIntegerField
      FieldName = 'NaoArreSobre'
      Origin = 'arrebaauni.NaoArreSobre'
    end
    object TbArreBaAUniCalculo: TSmallintField
      FieldName = 'Calculo'
      Origin = 'arrebaauni.Calculo'
    end
    object TbArreBaAUniCotas: TFloatField
      FieldName = 'Cotas'
      Origin = 'arrebaauni.Cotas'
    end
    object TbArreBaAUniDescriCota: TWideStringField
      FieldName = 'DescriCota'
      Origin = 'arrebaauni.DescriCota'
      Size = 10
    end
    object TbArreBaAUniNaoArreRisco: TIntegerField
      FieldName = 'NaoArreRisco'
    end
  end
  object DsArreBaAUni: TDataSource
    DataSet = TbArreBaAUni
    Left = 92
    Top = 8
  end
  object QrDuplic: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT COUNT(aba.Conta) ITENS, aba.Conta, aba.Valor'
      'FROM arrebaa aba'
      'GROUP BY aba.Conta, aba.Valor'
      'ORDER BY ITENS Desc')
    Left = 732
    Top = 12
    object QrDuplicITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrDuplicConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrDuplicValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 675
    Top = 12
  end
end
