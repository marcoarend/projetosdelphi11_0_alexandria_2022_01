object FmArreBaC: TFmArreBaC
  Left = 367
  Top = 168
  Caption = 'CAD-ARREC-001 :: Cadastro de Arrecada'#231#245'es Bases'
  ClientHeight = 898
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCond: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 780
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 1241
      Height = 671
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Ctl3D = True
      ParentCtl3D = False
      TabOrder = 0
      object PnCondDados: TPanel
        Left = 2
        Top = 18
        Width = 1237
        Height = 651
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        ExplicitWidth = 1236
        ExplicitHeight = 650
        object Label6: TLabel
          Left = 10
          Top = 59
          Width = 75
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Condom'#237'nio:'
        end
        object Label4: TLabel
          Left = 10
          Top = 10
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit02
        end
        object Label5: TLabel
          Left = 138
          Top = 10
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdit03
        end
        object Label8: TLabel
          Left = 689
          Top = 271
          Width = 128
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor (p/ cond'#244'mino):'
        end
        object Label11: TLabel
          Left = 10
          Top = 379
          Width = 430
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Texto a ser usado no lugar do texto base (deixe vazio para usar ' +
            'o base):'
        end
        object Label12: TLabel
          Left = 826
          Top = 271
          Width = 89
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Porcentagem*:'
        end
        object Label16: TLabel
          Left = 10
          Top = 551
          Width = 113
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o da cota:'
        end
        object Label19: TLabel
          Left = 162
          Top = 551
          Width = 104
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Arredondamento:'
        end
        object Label20: TLabel
          Left = 271
          Top = 551
          Width = 192
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Lista de unidades habitacionais:'
        end
        object Label23: TLabel
          Left = 10
          Top = 160
          Width = 83
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Configura'#231#227'o:'
        end
        object SpeedButton6: TSpeedButton
          Left = 532
          Top = 180
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton6Click
        end
        object LaNFSeSrvCad: TLabel
          Left = 10
          Top = 210
          Width = 80
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Regra Fiscal:'
        end
        object SBNFSeSrvCad: TSpeedButton
          Left = 532
          Top = 231
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SBNFSeSrvCadClick
        end
        object EdEmpresa: TdmkEditCB
          Left = 10
          Top = 79
          Width = 50
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 64
          Top = 79
          Width = 494
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 3
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object DBEdit02: TDBEdit
          Left = 10
          Top = 30
          Width = 123
          Height = 21
          Hint = 'N'#186' do banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsArreBaC
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdit03: TDBEdit
          Left = 138
          Top = 30
          Width = 420
          Height = 21
          Hint = 'Nome do banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsArreBaC
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 30
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object RGSitCobr: TRadioGroup
          Left = 565
          Top = 4
          Width = 144
          Height = 204
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Forma de cobran'#231'a: '
          ItemIndex = 0
          Items.Strings = (
            'N'#227'o cobrar'
            'Cont'#237'nua (Anual)'
            'Programada')
          TabOrder = 9
          OnClick = RGSitCobrClick
        end
        object GbPro: TGroupBox
          Left = 689
          Top = 320
          Width = 267
          Height = 55
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Cobran'#231'a programada: '
          TabOrder = 16
          Visible = False
          object Label7: TLabel
            Left = 15
            Top = 25
            Width = 57
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Parcelas:'
          end
          object EdParcelas: TdmkEdit
            Left = 79
            Top = 20
            Width = 50
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object GBPer: TGroupBox
          Left = 10
          Top = 266
          Width = 493
          Height = 109
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            ' Per'#237'odo de cobran'#231'a programada (m'#234's e ano) ou cont'#237'nua (s'#243' m'#234's)' +
            ': '
          TabOrder = 12
          Visible = False
          object GBIni: TGroupBox
            Left = 6
            Top = 21
            Width = 241
            Height = 79
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Per'#237'odo Inicial: '
            TabOrder = 0
            object Label32: TLabel
              Left = 5
              Top = 18
              Width = 29
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'M'#234's:'
            end
            object LaAnoI: TLabel
              Left = 158
              Top = 18
              Width = 27
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Ano:'
            end
            object CBMesI: TComboBox
              Left = 6
              Top = 39
              Width = 148
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Style = csDropDownList
              Color = clWhite
              DropDownCount = 12
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 7622183
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
            object CBAnoI: TComboBox
              Left = 156
              Top = 39
              Width = 79
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Style = csDropDownList
              Color = clWhite
              DropDownCount = 3
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 7622183
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object GBFim: TGroupBox
            Left = 247
            Top = 21
            Width = 242
            Height = 79
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Per'#237'odo final: '
            TabOrder = 1
            object Label34: TLabel
              Left = 5
              Top = 18
              Width = 29
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'M'#234's:'
            end
            object LaAnoF: TLabel
              Left = 158
              Top = 18
              Width = 27
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Ano:'
            end
            object CBMesF: TComboBox
              Left = 6
              Top = 39
              Width = 148
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Style = csDropDownList
              Color = clWhite
              DropDownCount = 12
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 7622183
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
            object CBAnoF: TComboBox
              Left = 156
              Top = 39
              Width = 79
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Style = csDropDownList
              Color = clWhite
              DropDownCount = 3
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 7622183
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
        end
        object EdValor: TdmkEdit
          Left = 689
          Top = 290
          Width = 128
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdTexto: TdmkEdit
          Left = 10
          Top = 399
          Width = 424
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 17
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkInfoParc: TCheckBox
          Left = 10
          Top = 428
          Width = 301
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Informar n'#250'mero da parcela / total de parcelas.'
          TabOrder = 18
        end
        object RGFator: TRadioGroup
          Left = 713
          Top = 4
          Width = 119
          Height = 204
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Fator*: '
          ItemIndex = 1
          Items.Strings = (
            'Valor'
            'Percentual')
          TabOrder = 10
        end
        object EdPercent: TdmkEdit
          Left = 826
          Top = 290
          Width = 129
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 15
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGDeQuem: TRadioGroup
          Left = 10
          Top = 458
          Width = 139
          Height = 90
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Cobrar de quem: '
          ItemIndex = 0
          Items.Strings = (
            'Todos **'
            'Selecionados')
          TabOrder = 21
        end
        object RGDoQue: TRadioGroup
          Left = 665
          Top = 458
          Width = 291
          Height = 90
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Cobrar sobre o qu'#234': '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Provis'#245'es'
            'Consumos'
            'Ambos'
            'Valor para c'#225'lculo'
            'Arrecada'#231#245'es***')
          TabOrder = 24
        end
        object RGComoCobra: TRadioGroup
          Left = 325
          Top = 458
          Width = 336
          Height = 90
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Forma de cobran'#231'a: '
          ItemIndex = 0
          Items.Strings = (
            'Aplicar o percentual ou fator direto sobre a base. '
            'Incluir o pr'#243'prio valor gerado na base c'#225'lculo. ')
          TabOrder = 23
        end
        object dmkEdArredonda: TdmkEdit
          Left = 162
          Top = 571
          Width = 105
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 26
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,01'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.010000000000000000
          ValWarn = False
        end
        object RGCalculo: TRadioGroup
          Left = 153
          Top = 458
          Width = 168
          Height = 90
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '  C'#225'lculo selecionados: '
          ItemIndex = 0
          Items.Strings = (
            'Rateio'
            'Percentual'
            'Cotas')
          TabOrder = 22
        end
        object dmkEdListaBaU: TdmkEditCB
          Left = 271
          Top = 571
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 27
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = dmkCBListaBaU
          IgnoraDBLookupComboBox = False
        end
        object dmkCBListaBaU: TdmkDBLookupComboBox
          Left = 345
          Top = 571
          Width = 610
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Controle'
          ListField = 'Nome'
          ListSource = DsListaBaU
          TabOrder = 28
          dmkEditCB = dmkEdListaBaU
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdDescriCota: TEdit
          Left = 10
          Top = 571
          Width = 149
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 25
          Text = 'Cotas'
        end
        object GroupBox2: TGroupBox
          Left = 507
          Top = 266
          Width = 179
          Height = 109
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Valor para c'#225'lculo: '
          TabOrder = 13
          object Label17: TLabel
            Left = 10
            Top = 30
            Width = 69
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor base:'
          end
          object CkValFracAsk: TCheckBox
            Left = 10
            Top = 79
            Width = 134
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Solicitar o valor.'
            Checked = True
            State = cbChecked
            TabOrder = 1
          end
          object EdValFracDef: TdmkEdit
            Left = 10
            Top = 49
            Width = 119
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object RGPartilha: TRadioGroup
          Left = 836
          Top = 4
          Width = 119
          Height = 204
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Partilha: '
          ItemIndex = 0
          Items.Strings = (
            'Equitativa'
            'Fra'#231#227'o ideal'
            'N'#186' moradores')
          TabOrder = 11
        end
        object CkInfoRelArr: TdmkCheckBox
          Left = 10
          Top = 601
          Width = 503
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Informa selecionados no "Relat'#243'rio de Arrecada'#231#245'es por Lista de ' +
            'Cond'#244'minos".'
          TabOrder = 29
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object RGDestCobra: TdmkRadioGroup
          Left = 724
          Top = 379
          Width = 232
          Height = 75
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Destino da cobran'#231'a: '
          ItemIndex = 0
          Items.Strings = (
            'Outras despesas condom'#237'niais.'
            'Taxa administrativa de cobran'#231'a.')
          TabOrder = 20
          UpdType = utYes
          OldValor = 0
        end
        object GroupBox5: TGroupBox
          Left = 441
          Top = 379
          Width = 277
          Height = 75
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' N'#227'o incluir na base de c'#225'lculo de: '
          TabOrder = 19
          object CkNaoArreSobre: TCheckBox
            Left = 10
            Top = 20
            Width = 252
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Despesas condominiais. (Deprecado?)'
            TabOrder = 0
          end
          object CkNaoArreRisco: TCheckBox
            Left = 10
            Top = 44
            Width = 257
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Taxa administrativa de cobran'#231'a.'
            TabOrder = 1
          end
        end
        object EdCNAB_Cfg: TdmkEditCB
          Left = 10
          Top = 180
          Width = 50
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCNAB_Cfg
          IgnoraDBLookupComboBox = False
        end
        object CBCNAB_Cfg: TdmkDBLookupComboBox
          Left = 64
          Top = 180
          Width = 462
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCNAB_Cfg
          TabOrder = 6
          dmkEditCB = EdCNAB_Cfg
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdNFSeSrvCad: TdmkEditCB
          Left = 10
          Top = 231
          Width = 50
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBNFSeSrvCad
          IgnoraDBLookupComboBox = False
        end
        object CBNFSeSrvCad: TdmkDBLookupComboBox
          Left = 64
          Top = 231
          Width = 462
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsNFSeSrvCad
          TabOrder = 8
          dmkEditCB = EdNFSeSrvCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object RgTipo: TdmkRadioGroup
          Left = 10
          Top = 111
          Width = 548
          Height = 45
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tipo de fatura'
          Columns = 2
          Items.Strings = (
            'Item 0'
            'Item 1')
          TabOrder = 4
          TabStop = True
          OnClick = RgTipoClick
          UpdType = utYes
          OldValor = 0
        end
      end
    end
    object GroupBox6: TGroupBox
      Left = 0
      Top = 694
      Width = 1241
      Height = 86
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel10: TPanel
        Left = 2
        Top = 18
        Width = 1236
        Height = 66
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label13: TLabel
          Left = 172
          Top = 2
          Width = 481
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            '* Fator percentual: calculado sobre total de provis'#245'es rateado e' +
            'ntre condominos.'
        end
        object Label14: TLabel
          Left = 181
          Top = 20
          Width = 380
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Exemplo: Quota condominial - 100%    -    Fundo de reserva 10%.'
        end
        object Label15: TLabel
          Left = 172
          Top = 42
          Width = 581
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            '** O im'#243'vel precisa estar com o "Rateia itens base de arrecada'#231#227 +
            'o" confirmado em seu cadastro.'
        end
        object Label22: TLabel
          Left = 684
          Top = 5
          Width = 381
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '*** A base ser'#225' constitu'#237'da pelas arrecada'#231#245'es rec'#233'm geradas.'
        end
        object Panel12: TPanel
          Left = 1058
          Top = 0
          Width = 178
          Height = 65
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn2: TBitBtn
            Tag = 15
            Left = 10
            Top = 4
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn2Click
          end
        end
        object BtConfCond: TBitBtn
          Tag = 14
          Left = 10
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfCondClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 780
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 694
      Width = 1241
      Height = 86
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel11: TPanel
        Left = 2
        Top = 18
        Width = 1236
        Height = 66
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 1058
          Top = 0
          Width = 178
          Height = 65
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 5
            Top = 4
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 10
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 0
      Width = 1241
      Height = 143
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      object PainelEdit: TPanel
        Left = 2
        Top = 18
        Width = 1237
        Height = 123
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        ExplicitWidth = 1236
        ExplicitHeight = 122
        object Label9: TLabel
          Left = 20
          Top = 10
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo:'
        end
        object Label10: TLabel
          Left = 148
          Top = 10
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
        end
        object LaForneceI: TLabel
          Left = 20
          Top = 59
          Width = 99
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Conta vinculada:'
        end
        object Label18: TLabel
          Left = 542
          Top = 10
          Width = 34
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Sigla:'
        end
        object SpeedButton5: TSpeedButton
          Left = 665
          Top = 79
          Width = 25
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object EdCodigo: TdmkEdit
          Left = 20
          Top = 30
          Width = 123
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 2
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome: TEdit
          Left = 148
          Top = 30
          Width = 390
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          TabOrder = 1
        end
        object EdConta: TdmkEditCB
          Left = 20
          Top = 79
          Width = 50
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBConta
          IgnoraDBLookupComboBox = False
        end
        object CBConta: TdmkDBLookupComboBox
          Left = 74
          Top = 79
          Width = 584
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContas
          TabOrder = 4
          dmkEditCB = EdConta
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdSigla: TEdit
          Left = 542
          Top = 30
          Width = 148
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 15
          TabOrder = 2
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 780
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    object GBCntrl: TGroupBox
      Left = 0
      Top = 702
      Width = 1241
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 35
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 330
        Top = 18
        Width = 908
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel8: TPanel
          Left = 745
          Top = 0
          Width = 163
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtArrecad: TBitBtn
          Tag = 10004
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Arrecad.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtArrecadClick
        end
        object BtCondom: TBitBtn
          Tag = 10003
          Left = 158
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Condom.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtCondomClick
        end
        object BtUnidades: TBitBtn
          Tag = 10003
          Left = 310
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Unidades'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtUnidadesClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 1241
      Height = 572
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alTop
      TabHeight = 25
      TabOrder = 1
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter3: TSplitter
          Left = 978
          Top = 0
          Width = 13
          Height = 529
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
        end
        object Panel4: TPanel
          Left = 991
          Top = 0
          Width = 240
          Height = 529
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          Caption = 'Panel1'
          ParentBackground = False
          TabOrder = 0
          object DBText1: TDBText
            Left = 0
            Top = 42
            Width = 240
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            DataField = 'NOMELISTABAU'
            DataSource = DsArreBaI
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clActiveCaption
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Splitter1: TSplitter
            Left = 0
            Top = 112
            Width = 240
            Height = 4
            Cursor = crVSplit
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
          end
          object DBGUnid: TdmkDBGrid
            Left = 0
            Top = 116
            Width = 240
            Height = 413
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Unidade'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Percent'
                Title.Caption = 'Percentual'
                Width = 84
                Visible = True
              end>
            Color = clWindow
            DataSource = DsArreBaU
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGUnidDblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Unidade'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Percent'
                Title.Caption = 'Percentual'
                Width = 84
                Visible = True
              end>
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 21
            Width = 242
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = 'Arrecada'#231#227'o base (se for outra origem):'
            TabOrder = 1
          end
          object DBGrid1: TdmkDBGrid
            Left = 0
            Top = 63
            Width = 240
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Columns = <
              item
                Expanded = False
                FieldName = 'ITENS'
                Title.Caption = 'Im'#243'veis'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Percent'
                Title.Caption = 'Soma Qtde'
                Width = 84
                Visible = True
              end>
            Color = clWindow
            DataSource = DsSABaU
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'ITENS'
                Title.Caption = 'Im'#243'veis'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Percent'
                Title.Caption = 'Soma Qtde'
                Width = 84
                Visible = True
              end>
          end
          object StaticText4: TStaticText
            Left = 0
            Top = 0
            Width = 71
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Unidades'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 3
          end
        end
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 978
          Height = 529
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object PainelData: TPanel
            Left = 0
            Top = 21
            Width = 978
            Height = 64
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            Enabled = False
            ParentBackground = False
            TabOrder = 0
            object Label1: TLabel
              Left = 20
              Top = 10
              Width = 47
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'C'#243'digo:'
              FocusControl = DBEdCodigo
            end
            object Label2: TLabel
              Left = 148
              Top = 10
              Width = 65
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Descri'#231#227'o:'
              FocusControl = DBEdNome
            end
            object Label3: TLabel
              Left = 546
              Top = 10
              Width = 99
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Conta vinculada:'
            end
            object DBEdCodigo: TDBEdit
              Left = 20
              Top = 30
              Width = 123
              Height = 21
              Hint = 'N'#186' do banco'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabStop = False
              DataField = 'Codigo'
              DataSource = DsArreBaC
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 8281908
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
            end
            object DBEdNome: TDBEdit
              Left = 148
              Top = 30
              Width = 395
              Height = 21
              Hint = 'Nome do banco'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Color = clWhite
              DataField = 'Nome'
              DataSource = DsArreBaC
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 30
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
            end
            object DBEdit01: TDBEdit
              Left = 546
              Top = 30
              Width = 415
              Height = 21
              Hint = 'Nome do banco'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Color = clWhite
              DataField = 'NOMECONTA'
              DataSource = DsArreBaC
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 30
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
            end
          end
          object DBGCond: TdmkDBGrid
            Left = 0
            Top = 106
            Width = 978
            Height = 423
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOND'
                Title.Caption = 'Condom'#237'nio'
                Width = 192
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_CALCULO'
                Title.Caption = 'C'#225'lculo unidades'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESITCOBR'
                Title.Caption = 'Sit. de cobran'#231'a'
                Width = 84
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEFATOR'
                Title.Caption = 'Fator'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Parcelas'
                Title.Caption = 'Parc.'
                Width = 28
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'INICIO'
                Title.Caption = 'In'#237'cio'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FINAL'
                Title.Caption = 'Final'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Percent'
                Title.Caption = 'Percentual'
                Width = 55
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'InfoParc_TXT'
                Title.Caption = 'InfoParc'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEDEQUEM'
                Title.Caption = 'De quem'
                Width = 73
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Texto'
                Title.Caption = 'Descri'#231#227'o substituta'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Arredonda'
                Title.Caption = 'Arredondamento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TIPCFG'
                Title.Caption = 'Tipo de documento'
                Width = 110
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECFG'
                Title.Caption = 'Configura'#231#227'o'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFSeSrvCad_TXT'
                Title.Caption = 'Regra Fiscal'
                Width = 150
                Visible = True
              end>
            Color = clWindow
            DataSource = DsArreBaI
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGCondCellClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOND'
                Title.Caption = 'Condom'#237'nio'
                Width = 192
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_CALCULO'
                Title.Caption = 'C'#225'lculo unidades'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESITCOBR'
                Title.Caption = 'Sit. de cobran'#231'a'
                Width = 84
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEFATOR'
                Title.Caption = 'Fator'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Parcelas'
                Title.Caption = 'Parc.'
                Width = 28
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'INICIO'
                Title.Caption = 'In'#237'cio'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FINAL'
                Title.Caption = 'Final'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Percent'
                Title.Caption = 'Percentual'
                Width = 55
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'InfoParc_TXT'
                Title.Caption = 'InfoParc'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEDEQUEM'
                Title.Caption = 'De quem'
                Width = 73
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Texto'
                Title.Caption = 'Descri'#231#227'o substituta'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Arredonda'
                Title.Caption = 'Arredondamento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TIPCFG'
                Title.Caption = 'Tipo de documento'
                Width = 110
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECFG'
                Title.Caption = 'Configura'#231#227'o'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFSeSrvCad_TXT'
                Title.Caption = 'Regra Fiscal'
                Width = 150
                Visible = True
              end>
          end
          object StaticText2: TStaticText
            Left = 0
            Top = 0
            Width = 94
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Arrecada'#231#227'o'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 2
          end
          object StaticText3: TStaticText
            Left = 0
            Top = 85
            Width = 94
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Condom'#237'nios'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pesquisa'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter2: TSplitter
          Left = 615
          Top = 0
          Width = 13
          Height = 529
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 615
          Height = 529
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGCondominios: TdmkDBGrid
            Left = 0
            Top = 55
            Width = 615
            Height = 474
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_CND'
                Title.Caption = 'Descri'#231#227'o'
                Width = 400
                Visible = True
              end>
            Color = clWindow
            DataSource = DsListaCond
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_CND'
                Title.Caption = 'Descri'#231#227'o'
                Width = 400
                Visible = True
              end>
          end
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 615
            Height = 55
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Label21: TLabel
              Left = 5
              Top = 5
              Width = 212
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Pesquisa de condom'#237'nio por nome:'
            end
            object EdPesqCond: TEdit
              Left = 5
              Top = 25
              Width = 576
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 0
              OnChange = EdPesqCondChange
            end
          end
        end
        object DBGItensCond: TdmkDBGrid
          Left = 628
          Top = 0
          Width = 603
          Height = 529
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 278
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sigla'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsListaItens
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGItensCondCellClick
          OnDblClick = DBGItensCondDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 278
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sigla'
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 916
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 478
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Arrecada'#231#245'es Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 478
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Arrecada'#231#245'es Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 478
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Arrecada'#231#245'es Bases'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 64
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel9: TPanel
      Left = 2
      Top = 18
      Width = 1236
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso2: TLabel
        Left = 16
        Top = 2
        Width = 692
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Na aba Pesquisa d'#234' um duplo clique na grade onde mostram as arre' +
          'cada'#231#245'es para localiz'#225'-la.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso1: TLabel
        Left = 15
        Top = 1
        Width = 692
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Na aba Pesquisa d'#234' um duplo clique na grade onde mostram as arre' +
          'cada'#231#245'es para localiz'#225'-la.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsArreBaC: TDataSource
    DataSet = QrArreBaC
    Left = 280
    Top = 608
  end
  object QrArreBaC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrArreBaCBeforeOpen
    AfterOpen = QrArreBaCAfterOpen
    BeforeClose = QrArreBaCBeforeClose
    AfterScroll = QrArreBaCAfterScroll
    SQL.Strings = (
      'SELECT con.Nome NOMECONTA, pbc.* '
      'FROM arrebac pbc'
      'LEFT JOIN contas con ON con.Codigo=pbc.Conta'
      'WHERE pbc.Codigo > 0')
    Left = 252
    Top = 608
    object QrArreBaCCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ArreBaC.Codigo'
    end
    object QrArreBaCConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'ArreBaC.Conta'
    end
    object QrArreBaCLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'ArreBaC.Lk'
    end
    object QrArreBaCDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'ArreBaC.DataCad'
    end
    object QrArreBaCDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'ArreBaC.DataAlt'
    end
    object QrArreBaCUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'ArreBaC.UserCad'
    end
    object QrArreBaCUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'ArreBaC.UserAlt'
    end
    object QrArreBaCNome: TWideStringField
      DisplayWidth = 40
      FieldName = 'Nome'
      Origin = 'ArreBaC.Nome'
      Size = 40
    end
    object QrArreBaCNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrArreBaCSigla: TWideStringField
      DisplayWidth = 15
      FieldName = 'Sigla'
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Credito="V"'
      'ORDER BY Nome')
    Left = 188
    Top = 608
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 216
    Top = 608
  end
  object QrArreBaI: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrArreBaIAfterOpen
    BeforeClose = QrArreBaIBeforeClose
    AfterScroll = QrArreBaIAfterScroll
    OnCalcFields = QrArreBaICalcFields
    SQL.Strings = (
      'SELECT ELT(pbi.Calculo+1, '#39'Rateio'#39', '#39'Percentual'#39', '#39'Cotas'#39','#39'?'#39') '
      
        'NOME_CALCULO, IF(pbi.ListaBaU<>0, bc2.Nome, "[------]") NOMELIST' +
        'ABAU,'
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial '
      'ELSE ent.Nome END NOMECOND, pbi.*'
      'FROM arrebai pbi'
      'LEFT JOIN cond      cnd ON cnd.Codigo=pbi.Cond'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'LEFT JOIN arrebai   bi2 ON bi2.Controle=pbi.ListaBaU'
      'LEFT JOIN arrebac   bc2 ON bc2.Codigo=bi2.Codigo'
      'WHERE pbi.Codigo=:P0')
    Left = 308
    Top = 608
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArreBaINOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
    object QrArreBaICodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'arrebai.Codigo'
    end
    object QrArreBaIControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'arrebai.Controle'
    end
    object QrArreBaICond: TIntegerField
      FieldName = 'Cond'
      Origin = 'arrebai.Cond'
    end
    object QrArreBaIValor: TFloatField
      FieldName = 'Valor'
      Origin = 'arrebai.Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrArreBaITexto: TWideStringField
      DisplayWidth = 50
      FieldName = 'Texto'
      Origin = 'arrebai.Texto'
      Size = 50
    end
    object QrArreBaISitCobr: TIntegerField
      FieldName = 'SitCobr'
      Origin = 'arrebai.SitCobr'
    end
    object QrArreBaIParcelas: TIntegerField
      FieldName = 'Parcelas'
      Origin = 'arrebai.Parcelas'
      DisplayFormat = '0;-0; '
    end
    object QrArreBaIParcPerI: TIntegerField
      FieldName = 'ParcPerI'
      Origin = 'arrebai.ParcPerI'
    end
    object QrArreBaIParcPerF: TIntegerField
      FieldName = 'ParcPerF'
      Origin = 'arrebai.ParcPerF'
    end
    object QrArreBaILk: TIntegerField
      FieldName = 'Lk'
      Origin = 'arrebai.Lk'
    end
    object QrArreBaIDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'arrebai.DataCad'
    end
    object QrArreBaIDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'arrebai.DataAlt'
    end
    object QrArreBaIUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'arrebai.UserCad'
    end
    object QrArreBaIUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'arrebai.UserAlt'
    end
    object QrArreBaINOMESITCOBR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESITCOBR'
      Size = 50
      Calculated = True
    end
    object QrArreBaIINICIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'INICIO'
      Size = 30
      Calculated = True
    end
    object QrArreBaIFINAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Size = 30
      Calculated = True
    end
    object QrArreBaIInfoParc: TIntegerField
      FieldName = 'InfoParc'
      Origin = 'arrebai.InfoParc'
    end
    object QrArreBaIInfoParc_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'InfoParc_TXT'
      Size = 3
      Calculated = True
    end
    object QrArreBaIFator: TSmallintField
      FieldName = 'Fator'
      Origin = 'arrebai.Fator'
      Required = True
    end
    object QrArreBaIPercent: TFloatField
      FieldName = 'Percent'
      Origin = 'arrebai.Percent'
      Required = True
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrArreBaINOMEFATOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEFATOR'
      Size = 15
      Calculated = True
    end
    object QrArreBaIDeQuem: TSmallintField
      FieldName = 'DeQuem'
      Origin = 'arrebai.DeQuem'
      Required = True
    end
    object QrArreBaINOMEDEQUEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEDEQUEM'
      Size = 15
      Calculated = True
    end
    object QrArreBaIDoQue: TSmallintField
      FieldName = 'DoQue'
      Origin = 'arrebai.DoQue'
      Required = True
    end
    object QrArreBaIComoCobra: TSmallintField
      FieldName = 'ComoCobra'
      Origin = 'arrebai.ComoCobra'
      Required = True
    end
    object QrArreBaINaoArreSobre: TSmallintField
      FieldName = 'NaoArreSobre'
      Origin = 'arrebai.NaoArreSobre'
      Required = True
    end
    object QrArreBaIArredonda: TFloatField
      DisplayWidth = 15
      FieldName = 'Arredonda'
      Required = True
      DisplayFormat = '0.00;-0.00; '
    end
    object QrArreBaICalculo: TSmallintField
      FieldName = 'Calculo'
      Required = True
    end
    object QrArreBaINOME_CALCULO: TWideStringField
      FieldName = 'NOME_CALCULO'
      Size = 10
    end
    object QrArreBaIListaBaU: TIntegerField
      FieldName = 'ListaBaU'
    end
    object QrArreBaINOMELISTABAU: TWideStringField
      FieldName = 'NOMELISTABAU'
      Size = 40
    end
    object QrArreBaIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrArreBaIAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrArreBaIDescriCota: TWideStringField
      FieldName = 'DescriCota'
      Required = True
      Size = 10
    end
    object QrArreBaIValFracDef: TFloatField
      FieldName = 'ValFracDef'
      Required = True
    end
    object QrArreBaIValFracAsk: TSmallintField
      FieldName = 'ValFracAsk'
      Required = True
    end
    object QrArreBaIPartilha: TSmallintField
      FieldName = 'Partilha'
    end
    object QrArreBaIInfoRelArr: TSmallintField
      FieldName = 'InfoRelArr'
      Required = True
    end
    object QrArreBaINaoArreRisco: TSmallintField
      FieldName = 'NaoArreRisco'
    end
    object QrArreBaIDestCobra: TSmallintField
      FieldName = 'DestCobra'
    end
    object QrArreBaICNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrArreBaINFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrArreBaINOMECFG: TWideStringField
      FieldName = 'NOMECFG'
      Size = 50
    end
    object QrArreBaITIPCFG: TWideStringField
      FieldName = 'TIPCFG'
      Size = 30
    end
    object QrArreBaIFatur_Cfg: TIntegerField
      FieldName = 'Fatur_Cfg'
    end
    object QrArreBaINFSeSrvCad_TXT: TWideStringField
      FieldName = 'NFSeSrvCad_TXT'
      Size = 255
    end
  end
  object DsArreBaI: TDataSource
    DataSet = QrArreBaI
    Left = 336
    Top = 608
  end
  object PMArrecad: TPopupMenu
    OnPopup = PMArrecadPopup
    Left = 780
    Top = 568
    object Incluiarrecadao1: TMenuItem
      Caption = '&Inclui arrecada'#231#227'o'
      OnClick = Incluiarrecadao1Click
    end
    object Alteraarrecadao1: TMenuItem
      Caption = '&Altera arrecada'#231#227'o'
      OnClick = Alteraarrecadao1Click
    end
    object Excluiarrecadao1: TMenuItem
      Caption = '&Exclui arrecada'#231#227'o'
      Enabled = False
    end
  end
  object PMCondom: TPopupMenu
    OnPopup = PMCondomPopup
    Left = 808
    Top = 568
    object AdicionaCondomnioarrecadaoselecionada1: TMenuItem
      Caption = '&Adiciona condom'#237'nio '#224' arrecada'#231#227'o selecionada'
      OnClick = AdicionaCondomnioarrecadaoselecionada1Click
    end
    object Editaarrecadaoselecionada1: TMenuItem
      Caption = '&Edita arrecada'#231#227'o selecionada'
      OnClick = Editaarrecadaoselecionada1Click
    end
    object Retiraocondominiodaarrecadaoselecionada1: TMenuItem
      Caption = '&Retira o condominio da arrecada'#231#227'o selecionada'
      OnClick = Retiraocondominiodaarrecadaoselecionada1Click
    end
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = Timer1Timer
    Left = 864
    Top = 568
  end
  object PMUnidades: TPopupMenu
    OnPopup = PMUnidadesPopup
    Left = 836
    Top = 568
    object Incluiunidade1: TMenuItem
      Caption = '&Inclui mais uma unidade'
      OnClick = Incluiunidade1Click
    end
    object Incluidiversasunidades1: TMenuItem
      Caption = 'Inclui &diversas unidades'
      OnClick = Incluidiversasunidades1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Alteraunidadeatual1: TMenuItem
      Caption = '&Altera unidade atual'
      OnClick = Alteraunidadeatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Retiraunidade1: TMenuItem
      Caption = '&Retira unidade'
      OnClick = Retiraunidade1Click
    end
    object Retiradiversasunidades1: TMenuItem
      Caption = 'Retira diversas unidade&s'
      OnClick = Retiradiversasunidades1Click
    end
  end
  object QrArreBaU: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cni.Unidade, aru.* '
      'FROM arrebau aru'
      'LEFT JOIN condimov cni ON cni.Conta=aru.Apto'
      'WHERE aru.Controle=:P0'
      'ORDER BY cni.Unidade')
    Left = 588
    Top = 608
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArreBaUUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrArreBaUCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrArreBaUControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrArreBaUConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrArreBaUApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrArreBaULk: TIntegerField
      FieldName = 'Lk'
    end
    object QrArreBaUDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrArreBaUDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrArreBaUUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrArreBaUUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrArreBaUAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrArreBaUAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrArreBaUPercent: TFloatField
      FieldName = 'Percent'
      Required = True
      DisplayFormat = '0.000000;-0.000000; '
    end
  end
  object DsArreBaU: TDataSource
    DataSet = QrArreBaU
    Left = 616
    Top = 608
  end
  object QrAptos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrAptosCalcFields
    SQL.Strings = (
      
        'SELECT cni.Andar, cni.Conta, cni.Unidade, CASE WHEN ent.Tipo=0 T' +
        'HEN'
      'ent.RazaoSocial ELSE ent.Nome END NOMEDONO, aru.Apto Sel'
      'FROM condimov cni'
      'LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet'
      'LEFT JOIN arrebau aru ON aru.Apto=cni.Conta AND aru.Controle=:P0'
      'WHERE cni.Codigo=:P1'
      'ORDER BY cni.Unidade')
    Left = 648
    Top = 608
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAptosAndar: TIntegerField
      FieldName = 'Andar'
      Required = True
    end
    object QrAptosConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrAptosUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrAptosNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrAptosSel: TIntegerField
      FieldName = 'Sel'
    end
    object QrAptosSELECIONADO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SELECIONADO'
      Calculated = True
    end
  end
  object QrListaBaU: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT abi.Controle, abc.Nome'
      'FROM arrebau abu'
      'LEFT JOIN arrebac abc ON abc.Codigo=abu.Codigo'
      'LEFT JOIN arrebai abi ON abi.Controle=abu.Controle'
      'WHERE abi.Cond=:P0'
      'ORDER BY abc.Nome')
    Left = 896
    Top = 568
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrListaBaUControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrListaBaUNome: TWideStringField
      FieldName = 'Nome'
      Size = 40
    end
  end
  object DsListaBaU: TDataSource
    DataSet = QrListaBaU
    Left = 924
    Top = 568
  end
  object QrSABaU: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(aru.Percent) Percent, '
      'Count(aru.Conta) ITENS'
      'FROM arrebau aru'
      'WHERE aru.Controle=:P0'
      '')
    Left = 844
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSABaUPercent: TFloatField
      FieldName = 'Percent'
    end
    object QrSABaUITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsSABaU: TDataSource
    DataSet = QrSABaU
    Left = 872
    Top = 348
  end
  object QrListaCond: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrListaCondAfterScroll
    SQL.Strings = (
      'SELECT cnd.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CND'
      'FROM cond cnd'
      'LEFT JOIN Entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE :P0'
      'ORDER BY NOME_CND'
      ''
      '')
    Left = 648
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrListaCondCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaCondNOME_CND: TWideStringField
      FieldName = 'NOME_CND'
      Size = 100
    end
  end
  object DsListaCond: TDataSource
    DataSet = QrListaCond
    Left = 676
    Top = 12
  end
  object DsListaItens: TDataSource
    DataSet = QrListaItens
    Left = 732
    Top = 12
  end
  object QrListaItens: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT abc.Codigo, abc.Sigla, abc.Nome'
      'FROM arrebai abi'
      'LEFT JOIN arrebac abc ON abc.Codigo=abi.Codigo'
      'WHERE Cond=:P0'
      'ORDER BY Nome')
    Left = 704
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrListaItensCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaItensSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 15
    end
    object QrListaItensNome: TWideStringField
      FieldName = 'Nome'
      Size = 40
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrListaCondAfterScroll
    Left = 920
    Top = 164
  end
  object QrCNAB_Cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT abc.Codigo, abc.Sigla, abc.Nome'
      'FROM arrebai abi'
      'LEFT JOIN arrebac abc ON abc.Codigo=abi.Codigo'
      'WHERE Cond=:P0'
      'ORDER BY Nome')
    Left = 864
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 892
    Top = 164
  end
  object QrNFSeSrvCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM nfsesrvcad'
      'ORDER BY Nome')
    Left = 830
    Top = 440
    object QrNFSeSrvCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeSrvCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsNFSeSrvCad: TDataSource
    DataSet = QrNFSeSrvCad
    Left = 858
    Top = 440
  end
end
