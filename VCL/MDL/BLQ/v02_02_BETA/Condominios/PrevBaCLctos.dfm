object FmPrevBaCLctos: TFmPrevBaCLctos
  Left = 339
  Top = 185
  Caption = 'CAD-PROVI-002 :: Agendamento de Provis'#227'o'
  ClientHeight = 447
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 285
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 351
    object DBGrid1: TdmkDBGrid
      Left = 0
      Top = 89
      Width = 784
      Height = 196
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'SerieDoc'
          Title.Caption = 'S'#233'rie / Docum.'
          Width = 78
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NF'
          Title.Caption = 'N.F.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Hist'#243'rico'
          Width = 286
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeCliFor'
          Title.Caption = 'Cliente / Fornecedor'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Lancto'
          Width = 56
          Visible = True
        end>
      Color = clWindow
      DataSource = DsAgeLct
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'SerieDoc'
          Title.Caption = 'S'#233'rie / Docum.'
          Width = 78
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NF'
          Title.Caption = 'N.F.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Hist'#243'rico'
          Width = 286
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeCliFor'
          Title.Caption = 'Cliente / Fornecedor'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Lancto'
          Width = 56
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 89
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 70
        Height = 13
        Caption = 'Provis'#227'o base:'
      end
      object Label2: TLabel
        Left = 688
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object SpeedButton1: TSpeedButton
        Left = 660
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object Label11: TLabel
        Left = 8
        Top = 44
        Width = 196
        Height = 13
        Caption = 'Texto a ser usado no lugar do texto base:'
      end
      object EdPrevBaC: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPrevBac
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPrevBac: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 597
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPrevBac
        TabOrder = 1
        dmkEditCB = EdPrevBaC
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdValor: TdmkEdit
        Left = 688
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdTexto: TdmkEdit
        Left = 8
        Top = 60
        Width = 761
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 318
        Height = 32
        Caption = 'Agendamento de Provis'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 318
        Height = 32
        Caption = 'Agendamento de Provis'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 318
        Height = 32
        Caption = 'Agendamento de Provis'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 333
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 377
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 230
    ExplicitWidth = 635
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object DsAgeLct: TDataSource
    DataSet = QrAgeLct
    Left = 160
    Top = 12
  end
  object QrPrevBaC: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Conta, Nome '
      'FROM prevbac'
      'ORDER BY Nome')
    Left = 72
    Top = 12
    object QrPrevBaCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrevBaCNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPrevBaCConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object DsPrevBac: TDataSource
    DataSet = QrPrevBaC
    Left = 100
    Top = 12
  end
  object QrPesq: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM prevbailct'
      'WHERE Lancto=:P0'
      #10
      ''
      '')
    Left = 132
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrAgeLct: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * FROM agelct;')
    Left = 232
    Top = 200
    object QrAgeLctControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAgeLctSerieDoc: TWideStringField
      FieldName = 'SerieDoc'
      Size = 30
    end
    object QrAgeLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrAgeLctNomeCliFor: TWideStringField
      FieldName = 'NomeCliFor'
      Size = 100
    end
    object QrAgeLctDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrAgeLctNF: TIntegerField
      FieldName = 'NF'
    end
  end
end
