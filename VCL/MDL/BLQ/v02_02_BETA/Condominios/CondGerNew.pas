unit CondGerNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnDmkProcFunc, Db, mySQLDbTables, dmkGeral,
  dmkImage, UnDmkEnums;

type
  TFmCondGerNew = class(TForm)
    Panel1: TPanel;
    Label32: TLabel;
    CBMes: TComboBox;
    LaAnoI: TLabel;
    CBAno: TComboBox;
    QrPesq: TmySQLQuery;
    QrPesqPeriodo: TIntegerField;
    QrCond: TmySQLQuery;
    QrCondModelBloq: TSmallintField;
    QrCondConfigBol: TIntegerField;
    CkModBol: TCheckBox;
    QrCondModBol: TmySQLQuery;
    QrCondModBolApto: TIntegerField;
    QrCondModBolModelBloq: TSmallintField;
    QrCondModBolConfigBol: TIntegerField;
    QrCondModBolBalAgrMens: TSmallintField;
    QrCondModBolCompe: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    CkBloqFV: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrCond();
    procedure ReopenQrPesq(Periodo: Integer);
    procedure ReopenQrCondModBol();
  public
    { Public declarations }
    FPeriodo, FCliInt: Integer;
    FTabela: String;
  end;

  var
    FmCondGerNew: TFmCondGerNew;

implementation

uses Module, UnMsgInt, UMySQLModule, UnInternalConsts, UnMyObjects, DmkDAC_PF;

{$R *.DFM}

procedure TFmCondGerNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerNew.FormShow(Sender: TObject);
var
  i: Integer;
  Ano, Mes: Word;
begin
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  //
  ReopenQrCond;
  ReopenQrPesq(0);
  //
  if QrPesqPeriodo.Value > 0 then
  begin
    dmkPF.MesEAnoDePeriodoShort(QrPesqPeriodo.Value + 1, Mes, Ano);
    for i := 0 to CBAno.Items.Count do
    begin
      if CBAno.Items[i] = IntToStr(Ano) then
      begin
        CBAno.ItemIndex := i;
        Break;
      end;
    end;
    CBMes.ItemIndex := Mes -1;
  end;
end;

procedure TFmCondGerNew.ReopenQrCond;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCond, Dmod.MyDB, [
    'SELECT ModelBloq, ConfigBol ',
    'FROM cond ',
    'WHERE Codigo=' + Geral.FF0(FCliInt),
    '']);
end;

procedure TFmCondGerNew.ReopenQrCondModBol;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCondModBol, Dmod.MyDB, [
    'SELECT Apto, ModelBloq, ConfigBol, BalAgrMens, Compe ',
    'FROM condmodbol ',
    'WHERE Codigo=' + Geral.FF0(FCliInt),
    '']);
end;

procedure TFmCondGerNew.ReopenQrPesq(Periodo: Integer);
var
  SQLPeriodo: String;
begin
  if Periodo <> 0 then
    SQLPeriodo := 'AND Periodo=' + Geral.FF0(Periodo)
  else
    SQLPeriodo := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    'SELECT Max(Periodo) Periodo ',
    'FROM ' + FTabela,
    'WHERE Cond=' + Geral.FF0(FCliInt),
    SQLPeriodo,
    '']);
end;

procedure TFmCondGerNew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCliInt := 0;
  FTabela := '';
end;

procedure TFmCondGerNew.BtOKClick(Sender: TObject);
var
  Codigo, Periodo, BloqFV: Integer;
begin
  Periodo := dmkPF.PeriodoEncode(Geral.IMV(CBAno.Items[CBAno.ItemIndex]), CBMes.ItemIndex+1);
  BloqFV  := Geral.BoolToInt(CkBloqFV.Checked);
  //
  if MyObjects.FIC(FCliInt = 0, nil, 'Condom�nio n�o informado!') then Exit;
  if MyObjects.FIC(FTabela = '', nil, 'Tabela n�o informada!') then Exit;
  //
  ReopenQrPesq(Periodo);
  //
  if QrPesqPeriodo.Value = Periodo then
  begin
    Geral.MB_Aviso('Inclus�o de novo or�amento abortado!' + sLineBreak +
      'J� existe or�amento para este condom�nio neste per�odo!');
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', FTabela, TAB_PRV, 'Codigo');
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, FTabela, False,
    ['Encerrado', 'Cond', 'Periodo', 'ModelBloq', 'ConfigBol', 'BloqFV'], ['Codigo'],
    [0, FCliInt, Periodo, QrCondModelBloq.Value, QrCondConfigBol.Value, BloqFV], [Codigo], True) then
  begin
    //Inclui itens CondModBol
    if CkModBol.Checked then
    begin
      ReopenQrCondModBol();
      //
      if QrCondModBol.RecordCount > 0 then
      begin
        QrCondModBol.First;
        while not QrCondModBol.Eof do
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prevmodbol', False,
            ['ModelBloq', 'ConfigBol', 'BalAgrMens', 'Compe'], ['Codigo', 'Apto'],
            [QrCondModBolModelBloq.Value, QrCondModBolConfigBol.Value,
            QrCondModBolBalAgrMens.Value, QrCondModBolCompe.Value], [Codigo,
            QrCondModBolApto.Value], True);
          //
          QrCondModBol.Next;
        end;
      end;
    end;
    FPeriodo := Periodo;
    Close;
  end;
end;

end.

