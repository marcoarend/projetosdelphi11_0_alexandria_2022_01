unit BloImp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, Vcl.DBGrids,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  Vcl.ComCtrls, mySQLDbTables, frxDBSet, frxClass, frxExportPDF, dmkGeral,
  UnDmkEnums, frxExportBaseDialog;

type
  TFmBloImp = class(TForm)
    frxCondR2: TfrxReport;
    frxCondR3: TfrxReport;
    frxCondR4: TfrxReport;
    frxVerso: TfrxReport;
    frxCondB: TfrxReport;
    frxCondA: TfrxReport;
    frxCondC: TfrxReport;
    frxCondD: TfrxReport;
    frxCondE1: TfrxReport;
    frxCondE2: TfrxReport;
    frxCondG: TfrxReport;
    frxCondIB: TfrxReport;
    frxCondIR: TfrxReport;
    frxCondH6: TfrxReport;
    frxCondH5: TfrxReport;
    frxCondH4: TfrxReport;
    frxCondH3: TfrxReport;
    frxCondH2: TfrxReport;
    frxCondH1: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    frxBloq: TfrxReport;
    QrAptoModBloq: TmySQLQuery;
    QrAptoModBloqModelBloq: TSmallintField;
    QrAptoModBloqConfigBol: TIntegerField;
    QrAptoModBloqBalAgrMens: TSmallintField;
    QrAptoModBloqCompe: TSmallintField;
    QrAptoModBloqColunas: TSmallintField;
    QrCarts: TmySQLQuery;
    QrCartsCarteira: TIntegerField;
    QrCartsNOMECART: TWideStringField;
    QrCartsInicial: TFloatField;
    QrCartsANT_CRED: TFloatField;
    QrCartsANT_DEB: TFloatField;
    QrCartsATU_CRED: TFloatField;
    QrCartsATU_DEB: TFloatField;
    QrCartsTRANSF: TFloatField;
    QrCartsANTERIOR: TFloatField;
    QrCartsRECEITAS: TFloatField;
    QrCartsDESPESAS: TFloatField;
    QrCartsSALDOMES: TFloatField;
    QrCartsFINAL: TFloatField;
    QrCartsTRF_CRED: TFloatField;
    QrCartsTRF_DEBI: TFloatField;
    frxDsCarts: TfrxDBDataset;
    frxDsCond: TfrxDBDataset;
    QrCond: TmySQLQuery;
    QrCondNOMECLI: TWideStringField;
    QrCondCodigo: TIntegerField;
    QrCondLk: TIntegerField;
    QrCondDataCad: TDateField;
    QrCondDataAlt: TDateField;
    QrCondUserCad: TIntegerField;
    QrCondUserAlt: TIntegerField;
    QrCondCliente: TIntegerField;
    QrCondObserv: TWideMemoField;
    QrCondAndares: TIntegerField;
    QrCondTotApt: TIntegerField;
    QrCondDiaVencto: TIntegerField;
    QrCondBcoLogoPath: TWideStringField;
    QrCondCliLogoPath: TWideStringField;
    QrCondCIDADE: TWideStringField;
    QrCondNOMEUF: TWideStringField;
    QrCondModelBloq: TSmallintField;
    QrCondMIB: TIntegerField;
    QrCondMBB: TSmallintField;
    QrCondMRB: TSmallintField;
    QrCondPBB: TSmallintField;
    QrCondMSB: TSmallintField;
    QrCondPSB: TSmallintField;
    QrCondMPB: TSmallintField;
    QrCondMAB: TSmallintField;
    QrCondAlterWeb: TSmallintField;
    QrCondPwdSite: TSmallintField;
    QrCondProLaINSSr: TFloatField;
    QrCondProLaINSSp: TFloatField;
    QrCondAtivo: TSmallintField;
    QrCondConfigBol: TIntegerField;
    QrCondMSP: TSmallintField;
    QrCondOcultaBloq: TSmallintField;
    QrCondSomaFracao: TFloatField;
    QrCondSigla: TWideStringField;
    QrCondBalAgrMens: TSmallintField;
    QrCondCompe: TSmallintField;
    QrCondDOCNUM: TWideStringField;
    QrCondDOCNUM_TXT: TWideStringField;
    QrCondHideCompe: TSmallintField;
    QrCondUF: TFloatField;
    QrConfigBol: TmySQLQuery;
    QrConfigBolCodigo: TIntegerField;
    QrConfigBolNome: TWideStringField;
    QrConfigBolTitBTxtSiz: TSmallintField;
    QrConfigBolTitBTxtFmt: TSmallintField;
    QrConfigBolTitBMrgSup: TIntegerField;
    QrConfigBolTitBLinAlt: TIntegerField;
    QrConfigBolTitBTxtTit: TWideStringField;
    QrConfigBolTitBPerFmt: TSmallintField;
    QrConfigBolNiv1TxtSiz: TSmallintField;
    QrConfigBolNiv1ValSiz: TSmallintField;
    QrConfigBolNiv1TxtFmt: TSmallintField;
    QrConfigBolNiv1ValFmt: TSmallintField;
    QrConfigBolNiv1ValLar: TIntegerField;
    QrConfigBolNiv1LinAlt: TIntegerField;
    QrConfigBolCxaSTxtSiz: TSmallintField;
    QrConfigBolCxaSValSiz: TSmallintField;
    QrConfigBolCxaSTxtFmt: TSmallintField;
    QrConfigBolCxaSValFmt: TSmallintField;
    QrConfigBolCxaSTxtAlt: TIntegerField;
    QrConfigBolCxaSValAlt: TIntegerField;
    QrConfigBolCxaSMrgSup: TIntegerField;
    QrConfigBolCxaSIniTam: TFloatField;
    QrConfigBolCxaSCreTam: TFloatField;
    QrConfigBolCxaSDebTam: TFloatField;
    QrConfigBolCxaSMovTam: TFloatField;
    QrConfigBolCxaSFimTam: TFloatField;
    QrConfigBolCxaSIniTxt: TWideStringField;
    QrConfigBolCxaSCreTxt: TWideStringField;
    QrConfigBolCxaSDebTxt: TWideStringField;
    QrConfigBolCxaSMovTxt: TWideStringField;
    QrConfigBolCxaSFimTxt: TWideStringField;
    QrConfigBolCxaSMrgInf: TIntegerField;
    QrConfigBolTit2TxtSiz: TSmallintField;
    QrConfigBolTit2TxtFmt: TSmallintField;
    QrConfigBolTit2MrgSup: TIntegerField;
    QrConfigBolTit2LinAlt: TIntegerField;
    QrConfigBolSom2TxtSiz: TSmallintField;
    QrConfigBolSom2ValSiz: TSmallintField;
    QrConfigBolSom2TxtFmt: TSmallintField;
    QrConfigBolSom2ValFmt: TSmallintField;
    QrConfigBolSom2ValLar: TIntegerField;
    QrConfigBolSom2LinAlt: TIntegerField;
    QrConfigBolTit3TxtSiz: TSmallintField;
    QrConfigBolTit3TxtFmt: TSmallintField;
    QrConfigBolTit3MrgSup: TIntegerField;
    QrConfigBolTit3LinAlt: TIntegerField;
    QrConfigBolSom3TxtSiz: TSmallintField;
    QrConfigBolSom3ValSiz: TSmallintField;
    QrConfigBolSom3TxtFmt: TSmallintField;
    QrConfigBolSom3ValFmt: TSmallintField;
    QrConfigBolSom3ValLar: TIntegerField;
    QrConfigBolSom3LinAlt: TIntegerField;
    QrConfigBolTit4TxtSiz: TSmallintField;
    QrConfigBolTit4TxtFmt: TSmallintField;
    QrConfigBolTit4MrgSup: TIntegerField;
    QrConfigBolTit4LinAlt: TIntegerField;
    QrConfigBolSom4LinAlt: TIntegerField;
    QrConfigBolTitRTxtSiz: TSmallintField;
    QrConfigBolTitRTxtFmt: TSmallintField;
    QrConfigBolTitRMrgSup: TIntegerField;
    QrConfigBolTitRLinAlt: TIntegerField;
    QrConfigBolTitRTxtTit: TWideStringField;
    QrConfigBolTitRPerFmt: TSmallintField;
    QrConfigBolTitCTxtSiz: TSmallintField;
    QrConfigBolTitCTxtFmt: TSmallintField;
    QrConfigBolTitCMrgSup: TIntegerField;
    QrConfigBolTitCLinAlt: TIntegerField;
    QrConfigBolTitCTxtTit: TWideStringField;
    QrConfigBolTitCPerFmt: TSmallintField;
    QrConfigBolSdoCTxtSiz: TSmallintField;
    QrConfigBolSdoCTxtFmt: TSmallintField;
    QrConfigBolSdoCLinAlt: TIntegerField;
    QrConfigBolTitITxtSiz: TSmallintField;
    QrConfigBolTitITxtFmt: TSmallintField;
    QrConfigBolTitIMrgSup: TIntegerField;
    QrConfigBolTitILinAlt: TIntegerField;
    QrConfigBolTitITxtTit: TWideStringField;
    QrConfigBolSdoITxMSiz: TSmallintField;
    QrConfigBolSdoITxMFmt: TSmallintField;
    QrConfigBolSdoIVaMSiz: TSmallintField;
    QrConfigBolSdoIVaMFmt: TSmallintField;
    QrConfigBolSdoILiMAlt: TIntegerField;
    QrConfigBolSdoITxMTxt: TWideStringField;
    QrConfigBolSdoILaMVal: TIntegerField;
    QrConfigBolSdoITxTSiz: TSmallintField;
    QrConfigBolSdoITxTFmt: TSmallintField;
    QrConfigBolSdoIVaTSiz: TSmallintField;
    QrConfigBolSdoIVaTFmt: TSmallintField;
    QrConfigBolSdoILiTAlt: TIntegerField;
    QrConfigBolSdoITxTTxt: TWideStringField;
    QrConfigBolSdoILaTVal: TIntegerField;
    QrConfigBolCtaPTxtSiz: TSmallintField;
    QrConfigBolCtaPTxtFmt: TSmallintField;
    QrConfigBolCtaPValSiz: TSmallintField;
    QrConfigBolCtaPValFmt: TSmallintField;
    QrConfigBolCtaPLinAlt: TIntegerField;
    QrConfigBolCtaPLarVal: TIntegerField;
    QrConfigBolSgrPTitSiz: TSmallintField;
    QrConfigBolSgrPTitFmt: TSmallintField;
    QrConfigBolSgrPTitAlt: TIntegerField;
    QrConfigBolSgrPSumSiz: TSmallintField;
    QrConfigBolSgrPSumFmt: TSmallintField;
    QrConfigBolSgrPSumAlt: TIntegerField;
    QrConfigBolSgrPLarVal: TIntegerField;
    QrConfigBolPrvPTitSiz: TSmallintField;
    QrConfigBolPrvPTitFmt: TSmallintField;
    QrConfigBolPrvPTitAlt: TIntegerField;
    QrConfigBolPrvPTitStr: TWideStringField;
    QrConfigBolPrvPMrgSup: TIntegerField;
    QrConfigBolSumPTxtSiz: TSmallintField;
    QrConfigBolSumPTxtFmt: TSmallintField;
    QrConfigBolSumPValSiz: TSmallintField;
    QrConfigBolSumPValFmt: TSmallintField;
    QrConfigBolSumPLinAlt: TIntegerField;
    QrConfigBolSumPValLar: TIntegerField;
    QrConfigBolSumPTxtStr: TWideStringField;
    QrConfigBolCtaATxtSiz: TSmallintField;
    QrConfigBolCtaATxtFmt: TSmallintField;
    QrConfigBolCtaAValSiz: TSmallintField;
    QrConfigBolCtaAValFmt: TSmallintField;
    QrConfigBolCtaALinAlt: TIntegerField;
    QrConfigBolCtaALarVal: TIntegerField;
    QrConfigBolTitATitSiz: TSmallintField;
    QrConfigBolTitATitFmt: TSmallintField;
    QrConfigBolTitATitAlt: TIntegerField;
    QrConfigBolTitATitStr: TWideStringField;
    QrConfigBolTitAMrgSup: TIntegerField;
    QrConfigBolTitAAptSiz: TSmallintField;
    QrConfigBolTitAAptFmt: TSmallintField;
    QrConfigBolTitAAptAlt: TIntegerField;
    QrConfigBolTitAAptSup: TIntegerField;
    QrConfigBolTitASumSiz: TSmallintField;
    QrConfigBolTitASumFmt: TSmallintField;
    QrConfigBolTitASumAlt: TIntegerField;
    QrConfigBolLk: TIntegerField;
    QrConfigBolDataCad: TDateField;
    QrConfigBolDataAlt: TDateField;
    QrConfigBolUserCad: TIntegerField;
    QrConfigBolUserAlt: TIntegerField;
    QrConfigBolAlterWeb: TSmallintField;
    QrConfigBolAtivo: TSmallintField;
    QrConfigBolMeuLogoImp: TSmallintField;
    QrConfigBolMeuLogoAlt: TIntegerField;
    QrConfigBolMeuLogoLar: TIntegerField;
    QrConfigBolMeuLogoArq: TWideStringField;
    QrConfigBolColunas: TSmallintField;
    QrConfigBolNiv1Grades: TSmallintField;
    QrConfigBolCxaTrnsTxt: TWideStringField;
    QrConfigBolTit8TxtSiz: TSmallintField;
    QrConfigBolTit8TxtFmt: TSmallintField;
    QrConfigBolTit8MrgSup: TIntegerField;
    QrConfigBolTit8LinAlt: TIntegerField;
    QrConfigBolNiv8TxtSiz: TSmallintField;
    QrConfigBolNiv8ValSiz: TSmallintField;
    QrConfigBolNiv8Grades: TSmallintField;
    QrConfigBolNiv8TxtFmt: TSmallintField;
    QrConfigBolNiv8ValFmt: TSmallintField;
    QrConfigBolNiv8ValLar: TIntegerField;
    QrConfigBolNiv8LinAlt: TIntegerField;
    QrConfigBolSom8TxtSiz: TSmallintField;
    QrConfigBolSom8ValSiz: TSmallintField;
    QrConfigBolSom8TxtFmt: TSmallintField;
    QrConfigBolSom8ValFmt: TSmallintField;
    QrConfigBolSom8ValLar: TIntegerField;
    QrConfigBolTitAAptTex: TWideMemoField;
    QrConfigBolTitWebTxt: TWideStringField;
    QrConfigBolSom8LinAlt: TIntegerField;
    QrConfigBolMesCompet: TSmallintField;
    frxDsConfigBol: TfrxDBDataset;
    QrInquilino: TmySQLQuery;
    QrInquilinoNATAL_TXT: TWideStringField;
    QrInquilinoTE1_TXT: TWideStringField;
    QrInquilinoFAX_TXT: TWideStringField;
    QrInquilinoCNPJ_TXT: TWideStringField;
    QrInquilinoE_ALL: TWideStringField;
    QrInquilinoNUMERO_TXT: TWideStringField;
    QrInquilinoECEP_TXT: TWideStringField;
    QrInquilinoPROPRI_E_MORADOR: TWideStringField;
    QrInquilinoUnidade: TWideStringField;
    QrInquilinoPrefixoUH: TWideStringField;
    QrInquilinoLNR: TWideStringField;
    QrInquilinoLN2: TWideStringField;
    QrInquilinoBloqEndTip: TSmallintField;
    QrInquilinoUsuario: TIntegerField;
    QrInquilinoCliente: TIntegerField;
    QrInquilinoPropriet: TIntegerField;
    QrInquilinoEnderLin1: TWideStringField;
    QrInquilinoEnderLin2: TWideStringField;
    QrInquilinoEnderNome: TWideStringField;
    QrInquilinoCNPJ_CPF_USUARIO: TWideStringField;
    QrInquilinoNOMETIPO_USUARIO: TWideStringField;
    QrInquilinoNO_USUARIO: TWideStringField;
    frxDsInquilino: TfrxDBDataset;
    QrIC: TmySQLQuery;
    QrICCodigo: TIntegerField;
    QrICTipo: TSmallintField;
    QrICNOMEENT: TWideStringField;
    QrICCNPJ_CPF: TWideStringField;
    QrICIE_RG: TWideStringField;
    QrICNIRE_: TWideStringField;
    QrICRUA: TWideStringField;
    QrICCOMPL: TWideStringField;
    QrICBAIRRO: TWideStringField;
    QrICCIDADE: TWideStringField;
    QrICNOMELOGRAD: TWideStringField;
    QrICNOMEUF: TWideStringField;
    QrICPais: TWideStringField;
    QrICTE1: TWideStringField;
    QrICFAX: TWideStringField;
    QrICNOMETIPO: TWideStringField;
    QrICNUMERO: TFloatField;
    QrICLograd: TFloatField;
    QrICCEP: TFloatField;
    QrDonoUH: TmySQLQuery;
    QrDonoUHNOMEPROPRIET: TWideStringField;
    QrDonoUHCNPJ_CPF: TWideStringField;
    QrDonoUHCNPJ_CPF_TXT: TWideStringField;
    QrDonoUHNOMETIPO: TWideStringField;
    QrMov: TmySQLQuery;
    QrMovNOMEGRUPO: TWideStringField;
    QrMovGrupo: TIntegerField;
    QrMovNOMESUBGRUPO: TWideStringField;
    QrMovSubGrupo: TIntegerField;
    QrMovNOMECONTA: TWideStringField;
    QrMovGenero: TIntegerField;
    QrMovMez: TIntegerField;
    QrMovValor: TFloatField;
    QrMovNOMECONTA_EXT: TWideStringField;
    QrMovNOMEPLANO: TWideStringField;
    QrMovNOMECONJUNTO: TWideStringField;
    QrMovConjunto: TIntegerField;
    QrMovCJ_CRED: TFloatField;
    QrMovCJ_DEBI: TFloatField;
    QrMovCJ_SALF: TFloatField;
    QrMovCJ_SALI: TFloatField;
    QrMovCJ_SALM: TFloatField;
    QrMovSubPgto1: TIntegerField;
    frxDsMov: TfrxDBDataset;
    QrMov3: TmySQLQuery;
    QrMov3NOMEPLANO: TWideStringField;
    QrMov3NOMECONJUNTO: TWideStringField;
    QrMov3Conjunto: TIntegerField;
    QrMov3NOMEGRUPO: TWideStringField;
    QrMov3Grupo: TIntegerField;
    QrMov3NOMESUBGRUPO: TWideStringField;
    QrMov3SubGrupo: TIntegerField;
    QrMov3NOMECONTA: TWideStringField;
    QrMov3Genero: TIntegerField;
    QrMov3Mez: TIntegerField;
    QrMov3Valor: TFloatField;
    QrMov3SDOANT: TFloatField;
    QrMov3SUMCRE: TFloatField;
    QrMov3SUMDEB: TFloatField;
    QrMov3SDOFIM: TFloatField;
    QrMov3SDOMES: TFloatField;
    QrMov3NOMECONTA_EXT: TWideStringField;
    QrMov3SubPgto1: TIntegerField;
    QrMov3CJT_COD: TIntegerField;
    QrMov3CJT_ANT: TFloatField;
    QrMov3CJT_CRE: TFloatField;
    QrMov3CJT_DEB: TFloatField;
    QrMov3CJT_FIM: TFloatField;
    QrMov3CJT_MOV: TFloatField;
    QrMov3SGR_COD: TIntegerField;
    QrMov3SGR_ANT: TFloatField;
    QrMov3SGR_CRE: TFloatField;
    QrMov3SGR_DEB: TFloatField;
    QrMov3SGR_FIM: TFloatField;
    QrMov3SGR_MOV: TFloatField;
    QrMov3GRU_COD: TIntegerField;
    QrMov3GRU_ANT: TFloatField;
    QrMov3GRU_CRE: TFloatField;
    QrMov3GRU_DEB: TFloatField;
    QrMov3GRU_MOV: TFloatField;
    QrMov3GRU_FIM: TFloatField;
    frxDsMov3: TfrxDBDataset;
    QrSdoCjt: TmySQLQuery;
    QrSdoCjtCodigo: TIntegerField;
    QrSdoCjtSumCre: TFloatField;
    QrSdoCjtSumDeb: TFloatField;
    QrSdoCjtSdoFim: TFloatField;
    QrSdoCjtValor: TFloatField;
    QrSdoCjtSdoAnt: TFloatField;
    QrSdoCjtNOME_CJT: TWideStringField;
    frxDsSdoCjt: TfrxDBDataset;
    QrPendU: TmySQLQuery;
    QrPendUSALDO: TFloatField;
    QrPendT: TmySQLQuery;
    QrPendTSALDO: TFloatField;
    QrCartsIni: TmySQLQuery;
    QrCartsIniCredito: TFloatField;
    QrCartsIniDebito: TFloatField;
    QrCartsIniCarteira: TIntegerField;
    QrCartsFim: TmySQLQuery;
    QrCartsFimCredito: TFloatField;
    QrCartsFimDebito: TFloatField;
    QrCartsFimCarteira: TIntegerField;
    QrCartsTrf: TmySQLQuery;
    QrCartsTrfValor: TFloatField;
    QrCartsTrfCarteira: TIntegerField;
    QrCartsTrfCRED: TFloatField;
    QrCartsTrfDEB: TFloatField;
    QrCartN: TmySQLQuery;
    QrCartNANT_CRED: TFloatField;
    QrCartNANT_DEB: TFloatField;
    QrCartNATU_CRED: TFloatField;
    QrCartNATU_DEB: TFloatField;
    QrCartNTRANSF: TFloatField;
    QrCartNANTERIOR: TFloatField;
    QrCartNRECEITAS: TFloatField;
    QrCartNDESPESAS: TFloatField;
    QrCartNSALDOMES: TFloatField;
    QrCartNFINAL: TFloatField;
    QrCartNCarteira: TIntegerField;
    QrCartNNOMECART: TWideStringField;
    QrCartNInicial: TFloatField;
    QrCjSdo: TmySQLQuery;
    QrCjSdoCodigo: TIntegerField;
    QrCjSdoSdoAtu: TFloatField;
    QrCJMov: TmySQLQuery;
    QrCJMovConjunto: TIntegerField;
    QrCJMovCredito: TFloatField;
    QrCJMovDebito: TFloatField;
    QrCJMovValor: TFloatField;
    QrSaldosNiv: TmySQLQuery;
    QrSaldosNivNivel: TIntegerField;
    QrSaldosNivCO_Cta: TIntegerField;
    QrSaldosNivCO_SGr: TIntegerField;
    QrSaldosNivCO_Gru: TIntegerField;
    QrSaldosNivCO_Cjt: TIntegerField;
    QrSaldosNivCO_Pla: TIntegerField;
    QrSaldosNivNO_Cta: TWideStringField;
    QrSaldosNivNO_SGr: TWideStringField;
    QrSaldosNivNO_Gru: TWideStringField;
    QrSaldosNivNO_Cjt: TWideStringField;
    QrSaldosNivNO_Pla: TWideStringField;
    QrSaldosNivOL_Cta: TIntegerField;
    QrSaldosNivOL_SGr: TIntegerField;
    QrSaldosNivOL_Gru: TIntegerField;
    QrSaldosNivOL_Cjt: TIntegerField;
    QrSaldosNivOL_Pla: TIntegerField;
    QrSaldosNivSdoAnt: TFloatField;
    QrSaldosNivCredito: TFloatField;
    QrSaldosNivDebito: TFloatField;
    QrSaldosNivMovim: TFloatField;
    QrSaldosNivEX_Cta: TIntegerField;
    QrSaldosNivEX_SGr: TIntegerField;
    QrSaldosNivEX_Gru: TIntegerField;
    QrSaldosNivEX_Cjt: TIntegerField;
    QrSaldosNivEX_Pla: TIntegerField;
    QrSaldosNivID_SEQ: TIntegerField;
    QrSaldosNivAtivo: TSmallintField;
    QrAptoModBloqApto: TIntegerField;
    QrSdoGru: TmySQLQuery;
    QrSdoGruCodigo: TIntegerField;
    QrSdoGruSdoAnt: TFloatField;
    QrSdoGruSumCre: TFloatField;
    QrSdoGruSumDeb: TFloatField;
    QrSdoGruSdoFim: TFloatField;
    QrSdoGruValor: TFloatField;
    QrSdoSgr: TmySQLQuery;
    QrSdoSgrCodigo: TIntegerField;
    QrSdoSgrSumCre: TFloatField;
    QrSdoSgrSumDeb: TFloatField;
    QrSdoSgrSdoFim: TFloatField;
    QrSdoSgrValor: TFloatField;
    QrSdoSgrSdoAnt: TFloatField;
    QrCtasSdo: TmySQLQuery;
    QrCtasSdoCodigo: TIntegerField;
    QrCtasSdoSdoAnt: TFloatField;
    QrCtasSdoSumCre: TFloatField;
    QrCtasSdoSumDeb: TFloatField;
    QrCtasSdoSdoFim: TFloatField;
    QrCtasSdoValor: TFloatField;
    procedure QrCartsCalcFields(DataSet: TDataSet);
    procedure QrCondCalcFields(DataSet: TDataSet);
    procedure QrInquilinoCalcFields(DataSet: TDataSet);
    function frxCondH1UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondH2UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondH3UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondH4UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondH5UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondH6UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondIRUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondIBUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondGUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondE2UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondE1UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondDUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondCUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondBUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondAUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondR2UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondR3UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    function frxCondR4UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure frxCondAGetValue(const VarName: string; var Value: Variant);
    procedure frxCondBGetValue(const VarName: string; var Value: Variant);
    procedure frxCondCGetValue(const VarName: string; var Value: Variant);
    procedure frxCondDGetValue(const VarName: string; var Value: Variant);
    procedure frxCondE1GetValue(const VarName: string; var Value: Variant);
    procedure frxCondE2GetValue(const VarName: string; var Value: Variant);
    procedure frxCondGGetValue(const VarName: string; var Value: Variant);
    procedure frxCondIBGetValue(const VarName: string; var Value: Variant);
    procedure frxCondIRGetValue(const VarName: string; var Value: Variant);
    procedure frxCondR2GetValue(const VarName: string; var Value: Variant);
    procedure frxCondR3GetValue(const VarName: string; var Value: Variant);
    procedure frxCondR4GetValue(const VarName: string; var Value: Variant);
    procedure frxCondH1GetValue(const VarName: string; var Value: Variant);
    procedure frxCondH2GetValue(const VarName: string; var Value: Variant);
    procedure frxCondH3GetValue(const VarName: string; var Value: Variant);
    procedure frxCondH4GetValue(const VarName: string; var Value: Variant);
    procedure frxCondH5GetValue(const VarName: string; var Value: Variant);
    procedure frxCondH6GetValue(const VarName: string; var Value: Variant);
    procedure QrCartNCalcFields(DataSet: TDataSet);
    procedure QrMovCalcFields(DataSet: TDataSet);
    procedure QrMovBeforeClose(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure frxVersoGetValue(const VarName: string; var Value: Variant);
    procedure QrMov3CalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FBalAgrMens, FColunas: Integer;
    FResumo_SaldoAnt, FResumo_Receitas, FResumo_Despesas, FResumo_SaldoMes,
    FResumo_SaldoTrf: Double;
    FCkZerado: Boolean;
    FTabLctA, FTabLctB, FTabLctD, FNossoNumero: String;
    FQueryBloOpcoes, FQueryPrev, FQueryBoletos, FQueryBoletosIts: TmySQLQuery;
    F_frxDsBoletos, F_frxDsBoletosIts, F_frxDsPRI: TfrxDBDataset;
    function  DefineModeloBoleto(const Escolher: Boolean; const DataI,
              DataF: String; const Apto: Integer; var ModelBloq, ConfigBol,
              Colunas, BalAgrMens, Compe: Integer): Boolean;
    function  frxUserFunction(const MethodName: string; var Params: Variant): Variant;
    function  TraduzInstrucaoArrecadacao(Instrucao, Propriet, Unidade, Empresa,
              EmpresaCNPJ: String; FracaoIdeal: Double): String;
    function  ObtemValorQrBoletosIts(Item, Tipo: Integer): String;
    function  FormataPeriodo(Periodo, FmtType: Integer): String;
    procedure frxGetValue(const VarName: string; var Value: Variant);
    procedure ConfiguraMasterData(frxReport: TfrxReport);
    procedure ReabreTabelasBoleto(Codigo, CliInt, EntCliInt, Periodo: Integer;
              var DataI, DataF: String);
    procedure ReopenEnderecoInquilino();
    procedure DefineSaldoResumo;
    procedure ReopenPendU(Entidade, Apto: Integer);
    procedure ReopenResumo();
  public
    { Public declarations }
    function  ImprimeBloq(CkZerado, Escolher: Boolean; TabLctA, TabLctB,
              TabLctD: String; CliInt, EntCliInt: Integer; QueryPrev,
              QueryBoletos, QueryBoletosIts, QueryBloOpcoes: TmySQLQuery;
              frxDsPRI, frxDsBoletos, frxDsBoletosIts: TfrxDBDataset;
              Quais: TselType; Como: TfrxImpComo; Arquivo: String;
              Filtro: TfrxCustomExportFilter; Grade: TDBGrid): TfrxReport;
  end;

var
  FmBloImp: TFmBloImp;

implementation

uses UnBloqGerl, Module, ModuleGeral, ModBloqGerl, UnMyObjects, MeuFrx,
  DmkDAC_PF, UnBloquetos_Jan, UnInternalConsts, UnFinanceiro, UMySQLModule,
  UnDmkProcFunc, UnBancos, MyGlyfs, ModuleBco, Principal, ModuleFin;

{$R *.dfm}

{ TFmBloImp }

procedure TFmBloImp.ConfiguraMasterData(frxReport: TfrxReport);
var
  I: Integer;
  MD_ARR, MD_U_CJT, MD_Cta, MD_C_C, MD_C_C_2, MD_PRV: TfrxMasterData;
begin
  for I := 0 to frxReport.Datasets.Count -1 do
    frxReport.DataSets.Items[I].DataSet.Enabled := True;
  //
  MD_ARR := frxReport.FindObject('MD_ARR') as TfrxMasterData;
  if MD_ARR <> nil then
    MD_ARR.Dataset := F_frxDsBoletosIts;
  //
  MD_U_CJT := frxReport.FindObject('MD_U_CJT') as TfrxMasterData;
  if MD_U_CJT <> nil then
    MD_U_CJT.DataSet := frxDsSdoCjt;
  //
  MD_Cta := frxReport.FindObject('MD_Cta') as TfrxMasterData;
  if MD_Cta <> nil then
    MD_Cta.DataSet := frxDsMov3;
  //
  MD_C_C := frxReport.FindObject('MD_C_C') as TfrxMasterData;
  if MD_C_C <> nil then
    MD_C_C.DataSet := frxDsCarts;
  //
  MD_C_C_2 := frxReport.FindObject('MD_C_C_2') as TfrxMasterData;
  if MD_C_C_2 <> nil then
    MD_C_C_2.DataSet := frxDsCarts;
  //
  MD_PRV := frxReport.FindObject('MD_PRV') as TfrxMasterData;
  if MD_PRV <> nil then
    MD_PRV.DataSet := F_frxDsPRI;
end;

function TFmBloImp.DefineModeloBoleto(const Escolher: Boolean; const DataI,
  DataF: String; const Apto: Integer; var ModelBloq, ConfigBol, Colunas,
  BalAgrMens, Compe: Integer): Boolean;
begin
  Result   := False;
  FColunas := 0;
  //
  if Escolher = True then
  begin
    UBloquetos_Jan.MostraCondGerModBol(ModelBloq, ConfigBol, BalAgrMens, Colunas,
      Compe);
  end else
  begin
    if (ModelBloq <> -1) and (QrAptoModBloq.Locate('Apto', Apto, [])) then
    begin
      ModelBloq  := QrAptoModBloq.FieldByName('ModelBloq').AsInteger;
      Colunas    := QrAptoModBloq.FieldByName('Colunas').AsInteger;
      BalAgrMens := QrAptoModBloq.FieldByName('BalAgrMens').AsInteger;
      Compe      := QrAptoModBloq.FieldByName('Compe').AsInteger;
      ConfigBol  := QrAptoModBloq.FieldByName('ConfigBol').AsInteger;
    end;
  end;
  Result := ModelBloq <> 0;
  //
  if Result then
  begin
    FColunas    := Colunas;
    FBalAgrMens := BalAgrMens;
    //
    if (ModelBloq in ([8,9,10,11])) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrConfigBol, Dmod.MyDB, [
        'SELECT * ',
        'FROM configbol ',
        'WHERE Codigo=' + Geral.FF0(ConfigBol),
        '']);
      if QrConfigBol.RecordCount = 0 then
      begin
        Result := False;
        //
        Geral.MB_Aviso('N�o foi definida nenhuma configura��o ' +
          'de impress�o para o formato selecionado!');
      end;
    end;
    ReopenEnderecoInquilino();
    //
    QrMov3.Close;
    QrMov3.SQL.Clear;
    QrMov3.SQL.Add('SELECT pla.Nome NOMEPLANO, cjt.Nome NOMECONJUNTO,');
    QrMov3.SQL.Add('gru.Conjunto, gru.Nome NOMEGRUPO,');
    QrMov3.SQL.Add('sgr.Grupo, sgr.Nome NOMESUBGRUPO,');
    QrMov3.SQL.Add('con.SubGrupo, con.Nome NOMECONTA,');
    QrMov3.SQL.Add('lan.Genero, lan.Mez,');
    QrMov3.SQL.Add('SUM(lan.Credito - lan.Debito) Valor, lan.SubPgto1');
    QrMov3.SQL.Add('FROM ' + FTabLctA + ' lan');
    QrMov3.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrMov3.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lan.Genero');
    QrMov3.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrMov3.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
    QrMov3.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrMov3.SQL.Add('LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano');
    QrMov3.SQL.Add('');
    QrMov3.SQL.Add('WHERE car.ForneceI=:P0');
    QrMov3.SQL.Add('AND car.Tipo < 2');
    QrMov3.SQL.Add('AND lan.Genero>0');
    QrMov3.SQL.Add('AND lan.Data BETWEEN :P1 AND :P2');
    QrMov3.SQL.Add('GROUP BY lan.Genero');
    //
    if BalAgrMens = 1 then
      QrMov3.SQL.Add(', lan.Mez /*, lan.Subpgto1*/');
    //
    QrMov3.SQL.Add('ORDER BY pla.OrdemLista, NOMEPLANO,');
    QrMov3.SQL.Add('                    cjt.OrdemLista, NOMECONJUNTO,');
    QrMov3.SQL.Add('                    gru.OrdemLista, NOMEGRUPO,');
    QrMov3.SQL.Add('                    sgr.OrdemLista, NOMESUBGRUPO,');
    QrMov3.SQL.Add('                    con.OrdemLista, NOMECONTA, lan.Mez');
    QrMov3.SQL.Add('');
    QrMov3.Params[00].AsInteger := QrCondCliente.Value;
    QrMov3.Params[01].AsString  := DataI;
    QrMov3.Params[02].AsString  := DataF;
    UMyMod.AbreQuery(QrMov3, Dmod.MyDB);
  end;
end;

procedure TFmBloImp.frxGetValue(const VarName: string; var Value: Variant);
var
  DVB, LocalEData, UH, NossoNumero_Rem: String;
  ModelBloq, Banco: Integer;
  Status: Boolean;
begin
  Status    := DBloqGerl.QrCNAB_CfgTermoAceite.Value = 0;
  //
  if AnsiCompareText(VarName, 'PERIODODATE_PBB') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3,
    dmkPF.PrimeiroDiaDoPeriodo_Date(
    FQueryPrev.FieldByName('Periodo').AsInteger - 1 + QrCondPBB.Value)) + ' a ' +
    FormatDateTime(VAR_FORMATDATE3,
    dmkPF.UltimoDiaDoPeriodo_Date(
    FQueryPrev.FieldByName('Periodo').AsInteger - 1 + QrCondPBB.Value)) else
  if AnsiCompareText(VarName, 'PERIODODATE_PSB') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3,
    dmkPF.PrimeiroDiaDoPeriodo_Date(
    FQueryPrev.FieldByName('Periodo').AsInteger - 1 + QrCondPSB.Value)) + ' a ' +
    FormatDateTime(VAR_FORMATDATE3,
    dmkPF.UltimoDiaDoPeriodo_Date(
    FQueryPrev.FieldByName('Periodo').AsInteger - 1 + QrCondPSB.Value))
  else if AnsiCompareText(VarName, 'VARF_FIM_PREVISAO') = 0 then
  begin
    Value := 0;//N�o usa
  end
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_TITULO') = 0 then
    Value := TraduzInstrucaoArrecadacao(
      QrConfigBolTitAAptTex.Value, QrInquilinoPROPRI_E_MORADOR.Value,
      FQueryBoletos.FieldByName('Unidade').AsString, QrCondNOMECLI.Value,
      QrCondDOCNUM_TXT.Value, FQueryBoletos.FieldByName('FracaoIdeal').AsFloat)
  else if AnsiCompareText(VarName, 'VARF_HideCompe') = 0 then
    Value := QrCondHideCompe.Value
  else if AnsiCompareText(VarName, 'VARF_MESANO') = 0 then
    Value := dmkPF.MesEAnoDoPeriodo(FQueryPrev.FieldByName('Periodo').AsInteger)
  else if AnsiCompareText(VarName, 'VARF_MESANOA') = 0 then
  begin
    if FQueryPrev.FieldByName('ModelBloq').AsInteger in [8, 11] then //Modelos configur�veis seta o per�odo na configura��o
      Value := dmkPF.MesEAnoDoPeriodo(FQueryPrev.FieldByName('Periodo').AsInteger + QrConfigBolMesCompet.Value - 1)
    else
      Value := dmkPF.MesEAnoDoPeriodo(FQueryPrev.FieldByName('Periodo').AsInteger + QrCondPBB.Value - 1);
  end
  else if AnsiCompareText(VarName, 'VARF_PROXIMOPERIODO') = 0 then
  begin
    if FQueryPrev.FieldByName('ModelBloq').AsInteger in [8, 11] then //Modelos configur�veis seta o per�odo na configura��o
      Value := dmkPF.MesEAnoDoPeriodo(FQueryPrev.FieldByName('Periodo').AsInteger + QrConfigBolMesCompet.Value - 1)
    else
      Value := dmkPF.MesEAnoDoPeriodo(FQueryPrev.FieldByName('Periodo').AsInteger + QrCondPBB.Value - 1);
  end
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_1') = 0 then
    Value := ObtemValorQrBoletosIts(1, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_2') = 0 then
    Value := ObtemValorQrBoletosIts(2, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_3') = 0 then
    Value := ObtemValorQrBoletosIts(3, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_4') = 0 then
    Value := ObtemValorQrBoletosIts(4, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_5') = 0 then
    Value := ObtemValorQrBoletosIts(5, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_6') = 0 then
    Value := ObtemValorQrBoletosIts(6, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_7') = 0 then
    Value := ObtemValorQrBoletosIts(7, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_8') = 0 then
    Value := ObtemValorQrBoletosIts(8, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_9') = 0 then
    Value := ObtemValorQrBoletosIts(9, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_10') = 0 then
    Value := ObtemValorQrBoletosIts(10, 0)

  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_1') = 0 then
    Value := ObtemValorQrBoletosIts(1, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_2') = 0 then
    Value := ObtemValorQrBoletosIts(2, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_3') = 0 then
    Value := ObtemValorQrBoletosIts(3, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_4') = 0 then
    Value := ObtemValorQrBoletosIts(4, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_5') = 0 then
    Value := ObtemValorQrBoletosIts(5, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_6') = 0 then
    Value := ObtemValorQrBoletosIts(6, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_7') = 0 then
    Value := ObtemValorQrBoletosIts(7, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_8') = 0 then
    Value := ObtemValorQrBoletosIts(8, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_9') = 0 then
    Value := ObtemValorQrBoletosIts(9, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_10') = 0 then
    Value := ObtemValorQrBoletosIts(10, 1)
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_1') = 0 then
    Value := FQueryPrev.FieldByName('Aviso01').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_2') = 0 then
    Value := FQueryPrev.FieldByName('Aviso02').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_3') = 0 then
    Value := FQueryPrev.FieldByName('Aviso03').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_4') = 0 then
    Value := FQueryPrev.FieldByName('Aviso04').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_5') = 0 then
    Value := FQueryPrev.FieldByName('Aviso05').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_6') = 0 then
    Value := FQueryPrev.FieldByName('Aviso06').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_7') = 0 then
    Value := FQueryPrev.FieldByName('Aviso07').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_8') = 0 then
    Value := FQueryPrev.FieldByName('Aviso08').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_9') = 0 then
    Value := FQueryPrev.FieldByName('Aviso09').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_10') = 0 then
    Value := FQueryPrev.FieldByName('Aviso10').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_11') = 0 then
    Value := FQueryPrev.FieldByName('Aviso11').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_12') = 0 then
    Value := FQueryPrev.FieldByName('Aviso12').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_13') = 0 then
    Value := FQueryPrev.FieldByName('Aviso13').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_14') = 0 then
    Value := FQueryPrev.FieldByName('Aviso14').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_15') = 0 then
    Value := FQueryPrev.FieldByName('Aviso15').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_16') = 0 then
    Value := FQueryPrev.FieldByName('Aviso16').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_17') = 0 then
    Value := FQueryPrev.FieldByName('Aviso17').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_18') = 0 then
    Value := FQueryPrev.FieldByName('Aviso18').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_19') = 0 then
    Value := FQueryPrev.FieldByName('Aviso19').AsString
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_20') = 0 then
    Value := FQueryPrev.FieldByName('Aviso20').AsString
  else if AnsiCompareText(VarName, 'VARF_Resumo_SaldoAnt') = 0 then
    Value := FResumo_SaldoAnt
  else if AnsiCompareText(VarName, 'VARF_Resumo_Receitas') = 0 then
    Value := FResumo_Receitas
  else if AnsiCompareText(VarName, 'VARF_Resumo_Despesas') = 0 then
    Value := FResumo_Despesas
  else if AnsiCompareText(VarName, 'VARF_Resumo_SaldoMes') = 0 then
    Value := FResumo_SaldoMes
  else if AnsiCompareText(VarName, 'VARF_Resumo_SaldoTrf') = 0 then
    Value := FResumo_SaldoTrf
  else if AnsiCompareText(VarName, 'VARF_INADIMP_U') = 0 then
  begin
    // 2010-10-26
    ReopenPendU(FQueryPrev.FieldByName('CondCli').AsInteger,
      FQueryBoletos.FieldByName('Apto').AsInteger);
    //
    Value := - QrPendUSALDO.Value;
  end else if AnsiCompareText(VarName, 'VARF_INADIMP_T') = 0 then
  begin
    Value := - QrPendTSALDO.Value;
  end
  else if AnsiCompareText(VarName, 'VARF_MBB') = 0 then
    Value := QrCondMBB.Value
  else if AnsiCompareText(VarName, 'VARF_MRB') = 0 then
    Value := QrCondMRB.Value
  else if AnsiCompareText(VarName, 'VARF_MSB') = 0 then
    Value := QrCondMSB.Value
  else if AnsiCompareText(VarName, 'VARF_MSP') = 0 then
    Value := QrCondMSP.Value
  else if AnsiCompareText(VarName, 'VARF_MIB') = 0 then
    Value := QrCondMIB.Value
  else if AnsiCompareText(VarName, 'VARF_MPB') = 0 then
    Value := QrCondMPB.Value
  else if AnsiCompareText(VarName, 'VARF_MAB') = 0 then
    Value := QrCondMAB.Value
  else if AnsiCompareText(VarName, 'VARF_URL') = 0 then
    Value := DmodG.QrOpcoesGerl.FieldByName('Web_MyURL').AsString
  else if AnsiCompareText(VarName, 'VARF_URL') = 0 then
  begin
    Value := 'Meu cond. na web: ' + DmodG.QrOpcoesGerl.FieldByName('Web_MyURL').AsString;
  end
  else if AnsiCompareText(VarName, 'VARF_WEB') = 0 then
  begin
    Value := '';
  end
  else if AnsiCompareText(VarName, 'VARF_WEB_FULL') = 0 then
  begin
    Value := 'Meu condom�nio na internet: ' + sLineBreak + DmodG.QrOpcoesGerl.FieldByName('Web_MyURL').AsString;
  end
  else if AnsiCompareText(VarName, 'PERIODODATE_PBB_H1') = 0 then
    Value := FormataPeriodo(FQueryPrev.FieldByName('Periodo').AsInteger - 1 + QrCondPBB.Value,
      QrConfigBolTitBPerFmt.Value)
  else if AnsiCompareText(VarName, 'PERIODODATE_PBB_H2') = 0 then
    Value := FormataPeriodo(FQueryPrev.FieldByName('Periodo').AsInteger - 1 + QrCondPBB.Value,
      QrConfigBolTitRPerFmt.Value)
  else if AnsiCompareText(VarName, 'PERIODODATE_PSB_H1') = 0 then
    Value := FormataPeriodo(FQueryPrev.FieldByName('Periodo').AsInteger - 1 + QrCondPBB.Value,
      QrConfigBolTitCPerFmt.Value)
  else if AnsiCompareText(VarName, 'MeuLogoBolExiste') = 0 then
    Value := FileExists(QrConfigBolMeuLogoArq.Value)
  else if AnsiCompareText(VarName, 'MeuLogoImp') = 0 then
    Value := QrConfigBolMeuLogoImp.Value


  // frxCondRx
  else if AnsiCompareText(VarName, 'VAR_ENOMEDONO') = 0 then
    Value := DmodG.QrEnderecoNOME_ENT.Value
  else if AnsiCompareText(VarName, 'VAR_ECNPJ') = 0 then
    Value := DmodG.QrEnderecoCNPJ_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_ENOMERUA') = 0 then
    Value := DmodG.QrEnderecoNOMELOGRAD.Value + ' ' + DmodG.QrEnderecoRUA.Value
  else if AnsiCompareText(VarName, 'VAR_ENUMERO') = 0 then
    Value := 'N� ' + DmodG.QrEnderecoNUMERO_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_ECOMPL') = 0 then
    Value := 'Compl.: ' + DmodG.QrEnderecoCOMPL.Value
  else if AnsiCompareText(VarName, 'VAR_EBAIRRO') = 0 then
    Value := 'Bairro: ' + DmodG.QrEnderecoBAIRRO.Value
  else if AnsiCompareText(VarName, 'VAR_ECIDADE') = 0 then
    Value := 'Cidade: ' + DmodG.QrEnderecoCIDADE.Value
  else if AnsiCompareText(VarName, 'VAR_EUF') = 0 then
    Value := 'UF: ' + DmodG.QrEnderecoNOMEUF.Value
  else if AnsiCompareText(VarName, 'VAR_ECEP') = 0 then
    Value := 'CEP: ' + Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value)
  else if AnsiCompareText(VarName, 'VAR_EPAIS') = 0 then
    Value := 'Pa�s: ' + DmodG.QrEnderecoPais.Value
  else if AnsiCompareText(VarName, 'VAR_ETEL') = 0 then
    Value := 'Tel.: ' + DmodG.QrEnderecoTE1_TXT.Value
  //
  else if AnsiCompareText(VarName, 'VAR_EXTENSO') = 0 then
    Value := dmkPF.ExtensoMoney(Geral.FFT(
      FQueryBoletos.FieldByName('SUB_TOT').AsFloat, 2, siPositivo))
  else if AnsiCompareText(VarName, 'VAR_REFERENTE') = 0 then
  begin
    if Trim(QrInquilinoPrefixoUH.Value) <>  '' then
      UH := QrInquilinoPrefixoUH.Value;
    if Trim(QrInquilinoUnidade.Value) <>  '' then
      UH := UH + ' ' + QrInquilinoUnidade.Value;
    Value := UpperCase('Pagamento da quota condominial - ' +
      FormatDateTime('mmm"/"yy', FQueryBoletos.FieldByName('Vencto').AsDateTime) +
      ' - ' + UH + ' - Com vencimento em ' +
      FormatDateTime('dd" de "mmmm" de "yyyy', FQueryBoletos.FieldByName('Vencto').AsDateTime)
      + '.')
  end
  else if AnsiCompareText(VarName, 'VAR_BNOME') = 0 then
    Value := DmodG.QrEndereco2NOME_ENT.Value
  else if AnsiCompareText(VarName, 'VAR_BCNPJ') = 0 then
    Value := DmodG.QrEndereco2CNPJ_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_BNOMERUA') = 0 then
    Value := DmodG.QrEndereco2NOMELOGRAD.Value + ' ' + DmodG.QrEndereco2RUA.Value
  else if AnsiCompareText(VarName, 'VAR_BNUMERO') = 0 then
    Value := 'N� ' + DmodG.QrEndereco2NUMERO_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_BCOMPL') = 0 then
    Value := 'Compl.: ' + DmodG.QrEndereco2COMPL.Value
  else if AnsiCompareText(VarName, 'VAR_BBAIRRO') = 0 then
    Value := 'Bairro: ' + DmodG.QrEndereco2BAIRRO.Value
  else if AnsiCompareText(VarName, 'VAR_BCIDADE') = 0 then
    Value := 'Cidade: ' + DmodG.QrEndereco2CIDADE.Value
  else if AnsiCompareText(VarName, 'VAR_BUF') = 0 then
    Value := 'UF: ' + DmodG.QrEndereco2NOMEUF.Value
  else if AnsiCompareText(VarName, 'VAR_BCEP') = 0 then
    Value := 'CEP: ' + Geral.FormataCEP_NT(DmodG.QrEndereco2CEP.Value)
  else if AnsiCompareText(VarName, 'VAR_BPAIS') = 0 then
    Value := 'Pa�s: ' + DmodG.QrEndereco2Pais.Value
  else if AnsiCompareText(VarName, 'VAR_BTEL') = 0 then
    Value := 'Tel.: ' + DmodG.QrEndereco2TE1_TXT.Value
  //
  else if AnsiCompareText(VarName, 'VAR_LOCAL') = 0 then
    Value := DmodG.QrEndereco2CIDADE.Value
  else if AnsiCompareText(VarName, 'VAR_LOCALDATA') = 0 then
  begin
    LocalEData := Geral.Maiusculas(
      FormatDateTime('dddd, dd" de "mmmm" de "yyyy', Now),
      Geral.EhMinusculas(DmodG.QrEndereco2CIDADE.Value, False));
    if DmodG.QrEndereco2CIDADE.Value <> '' then LocalEData :=
      DmodG.QrEndereco2CIDADE.Value + ', ' + LocalEData;
    //
    Value := LocalEData;
  end
  else if AnsiCompareText(VarName, 'VARF_COLUNASH') = 0 then
  begin
    // Testar este. Caso n�o funcione usar o abaixo (mais lento?)
    Value := FColunas;
    (*
    if DCond.QrPrevModBol.Locate('Apto', DmBloq.QrBoletosApto.Value, []) then
      ModelBloq := DCond.QrPrevModBolModelBloq.Value
    else
      ModelBloq := DmCond.QrCondModelBloq.Value;
    //
    DCond.QrCB.Close;
    DCond.QrCB.Params[0].AsInteger := ModelBloq;
    DCond.QrCB.Open;
    //
    Value := DCond.QrCBColunas.Value; // Parei aqui! est� correto?
    *)
  end

  // In�cio da Ficha de compensa��o
  else if AnsiCompareText(VarName, 'VARF_AGCodCed') = 0 then
    Value := DBloqGerl.QrCNAB_Cfg.FieldByName('AgContaCed').AsString
  else if AnsiCompareText(VarName, 'VARF_NossoNumero') = 0 then
  begin
    UBancos.GeraNossoNumero(
      DBloqGerl.QrCNAB_Cfg.FieldByName('ModalCobr').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedBanco').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedAgencia').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedPosto').AsInteger,
      FQueryBoletos.FieldByName('BLOQUETO').AsFloat,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedConta').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CartNum').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(DBloqGerl.QrCNAB_Cfg.FieldByName('CodEmprBco').AsString),
      FQueryBoletos.FieldByName('Vencto').AsDateTime,
      DBloqGerl.QrCNAB_Cfg.FieldByName('TipoCobranca').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('EspecieDoc').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CNAB').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CtaCooper').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('LayoutRem').AsString,
      FNossoNumero, NossoNumero_Rem);
    //
    Value := FNossoNumero;
  end else if AnsiCompareText(VarName, 'VARF_CODIGOBARRAS') = 0 then
  begin
    UBancos.GeraNossoNumero(
      DBloqGerl.QrCNAB_Cfg.FieldByName('ModalCobr').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedBanco').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedAgencia').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedPosto').AsInteger,
      FQueryBoletos.FieldByName('BLOQUETO').AsFloat,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedConta').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CartNum').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(DBloqGerl.QrCNAB_Cfg.FieldByName('CodEmprBco').AsString),
      FQueryBoletos.FieldByName('Vencto').AsDateTime,
      DBloqGerl.QrCNAB_Cfg.FieldByName('TipoCobranca').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('EspecieDoc').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CNAB').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CtaCooper').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('LayoutRem').AsString,
      FNossoNumero, NossoNumero_Rem);
    //
    Value := UBancos.CodigoDeBarra_BoletoDeCobranca(
               DBloqGerl.QrCNAB_Cfg.FieldByName('CedBanco').AsInteger,
               DBloqGerl.QrCNAB_Cfg.FieldByName('CedAgencia').AsInteger,
               DBloqGerl.QrCNAB_Cfg.FieldByName('CorresBco').AsInteger,
               DBloqGerl.QrCNAB_Cfg.FieldByName('CorresAge').AsInteger,
               DBloqGerl.QrCNAB_Cfg.FieldByName('CedDAC_A').AsString,
               DBloqGerl.QrCNAB_Cfg.FieldByName('CedPosto').AsInteger,
               DBloqGerl.QrCNAB_Cfg.FieldByName('CedConta').AsString,
               DBloqGerl.QrCNAB_Cfg.FieldByName('CedDAC_C').AsString,
               DBloqGerl.QrCNAB_Cfg.FieldByName('CorresCto').AsString,
               9, 3, 1, FNossoNumero,
               DBloqGerl.QrCNAB_Cfg.FieldByName('CodEmprBco').AsString,
               DBloqGerl.QrCNAB_Cfg.FieldByName('CartNum').AsString,
               DBloqGerl.QrCNAB_Cfg.FieldByName('CART_IMP').AsString,
               DBloqGerl.QrCNAB_Cfg.FieldByName('IDCobranca').AsString,
               DBloqGerl.QrCNAB_Cfg.FieldByName('OperCodi').AsString,
               FQueryBoletos.FieldByName('Vencto').AsDateTime,
               FQueryBoletos.FieldByName('SUB_TOT').AsFloat,
               0, 0, not FCkZerado,
               DBloqGerl.QrCNAB_Cfg.FieldByName('ModalCobr').AsInteger,
               DBloqGerl.QrCNAB_Cfg.FieldByName('LayoutRem').AsString)
  //
  end else if AnsiCompareText(VarName, 'VAX') = 0 then
  begin
    if (DBloqGerl.QrCNAB_Cfg.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (DBloqGerl.QrCNAB_Cfg.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015) then
    begin
      Banco := DBloqGerl.QrCNAB_Cfg.FieldByName('CorresBco').AsInteger;
      //
      DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end else
    begin
      Banco := DBloqGerl.QrCNAB_Cfg.FieldByName('CedBanco').AsInteger;
      //
      if DBloqGerl.QrCNAB_Cfg.FieldByName('DVB').AsString <> '?' then
        DVB :=  DBloqGerl.QrCNAB_Cfg.FieldByName('DVB').AsString
      else
        DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end;
    Value := FormatFloat('000', Banco) + '-' + DVB;
  end
  else if AnsiCompareText(VarName, 'VARF_LINHADIGITAVEL') = 0 then
  begin
    UBancos.GeraNossoNumero(
      DBloqGerl.QrCNAB_Cfg.FieldByName('ModalCobr').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedBanco').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedAgencia').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedPosto').AsInteger,
      FQueryBoletos.FieldByName('BLOQUETO').AsFloat,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CedConta').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CartNum').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(DBloqGerl.QrCNAB_Cfg.FieldByName('CodEmprBco').AsString),
      FQueryBoletos.FieldByName('Vencto').AsDateTime,
      DBloqGerl.QrCNAB_Cfg.FieldByName('TipoCobranca').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('EspecieDoc').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CNAB').AsInteger,
      DBloqGerl.QrCNAB_Cfg.FieldByName('CtaCooper').AsString,
      DBloqGerl.QrCNAB_Cfg.FieldByName('LayoutRem').AsString,
      FNossoNumero, NossoNumero_Rem);
    //
    Value := '';
    Value := UBancos.LinhaDigitavel_BoletoDeCobranca(
               UBancos.CodigoDeBarra_BoletoDeCobranca(
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CedBanco').AsInteger,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CedAgencia').AsInteger,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CorresBco').AsInteger,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CorresAge').AsInteger,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CedDAC_A').AsString,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CedPosto').AsInteger,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CedConta').AsString,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CedDAC_C').AsString,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CorresCto').AsString,
                 9, 3, 1, FNossoNumero,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CodEmprBco').AsString,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CartNum').AsString,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('CART_IMP').AsString,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('IDCobranca').AsString,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('OperCodi').AsString,
                 FQueryBoletos.FieldByName('Vencto').AsDateTime,
                 FQueryBoletos.FieldByName('SUB_TOT').AsFloat,
                 0, 0, not FCkZerado,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('ModalCobr').AsInteger,
                 DBloqGerl.QrCNAB_Cfg.FieldByName('LayoutRem').AsString))
  end else if AnsiCompareText(VarName, 'LogoBancoExiste') = 0 then
  begin
    if (DBloqGerl.QrCNAB_Cfg.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (DBloqGerl.QrCNAB_Cfg.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      Value := FmMyGlyfs.LogoBancoExiste(DBloqGerl.QrCNAB_Cfg.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.LogoBancoExiste(DBloqGerl.QrCNAB_Cfg.FieldByName('CedBanco').AsInteger);
  end
  else if AnsiCompareText(VarName, 'LogoBancoPath') = 0 then
  begin
    if (DBloqGerl.QrCNAB_Cfg.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (DBloqGerl.QrCNAB_Cfg.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      Value := FmMyGlyfs.CaminhoLogoBanco(DBloqGerl.QrCNAB_Cfg.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.CaminhoLogoBanco(DBloqGerl.QrCNAB_Cfg.FieldByName('CedBanco').AsInteger);
  end
  else if AnsiCompareText(VarName, 'VAR_VALORMMULTA') = 0 then
  begin
    Value := (DBloqGerl.QrCNAB_Cfg.FieldByName('MultaPerc').AsFloat *
              FQueryBoletos.FieldByName('SUB_TOT').AsFloat / 100) +
              FQueryBoletos.FieldByName('SUB_TOT').AsFloat;
    Value := Geral.FFT(Value, 2, siPositivo);
  end
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO1') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(
               DBloqGerl.QrCNAB_Cfg.FieldByName('Texto01').AsString,
               FQueryBoletos.FieldByName('SUB_TOT').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('MultaPerc').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('JurosPerc').AsFloat, 0, 0)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO2') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(
               DBloqGerl.QrCNAB_Cfg.FieldByName('Texto02').AsString,
               FQueryBoletos.FieldByName('SUB_TOT').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('MultaPerc').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('JurosPerc').AsFloat, 0, 0)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO3') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(
               DBloqGerl.QrCNAB_Cfg.FieldByName('Texto03').AsString,
               FQueryBoletos.FieldByName('SUB_TOT').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('MultaPerc').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('JurosPerc').AsFloat, 0, 0)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO4') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(
               DBloqGerl.QrCNAB_Cfg.FieldByName('Texto04').AsString,
               FQueryBoletos.FieldByName('SUB_TOT').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('MultaPerc').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('JurosPerc').AsFloat, 0, 0)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO5') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(
               DBloqGerl.QrCNAB_Cfg.FieldByName('Texto05').AsString,
               FQueryBoletos.FieldByName('SUB_TOT').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('MultaPerc').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('JurosPerc').AsFloat, 0, 0)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO6') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(
               DBloqGerl.QrCNAB_Cfg.FieldByName('Texto06').AsString,
               FQueryBoletos.FieldByName('SUB_TOT').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('MultaPerc').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('JurosPerc').AsFloat, 0, 0)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO7') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(
               DBloqGerl.QrCNAB_Cfg.FieldByName('Texto07').AsString,
               FQueryBoletos.FieldByName('SUB_TOT').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('MultaPerc').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('JurosPerc').AsFloat, 0, 0)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO8') = 0 then
    Value := DmBco.TraduzInstrucaoBloqueto(
               DBloqGerl.QrCNAB_Cfg.FieldByName('Texto08').AsString,
               FQueryBoletos.FieldByName('SUB_TOT').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('MultaPerc').AsFloat,
               DBloqGerl.QrCNAB_Cfg.FieldByName('JurosPerc').AsFloat, 0, 0)
  // Fim Ficha de compensa��o
  else if AnsiCompareText(VarName, 'VARF_TemModuloWEB') = 0 then
    Value := FmPrincipal.FTemModuloWEB
  else if AnsiCompareText(VarName, 'VARF_STATUS') = 0 then
    Value := Status;
end;

procedure TFmBloImp.frxCondAGetValue(const VarName: string; var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondAUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondBGetValue(const VarName: string; var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondBUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondCGetValue(const VarName: string; var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondCUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondDGetValue(const VarName: string; var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondDUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondE1GetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondE1UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondE2GetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondE2UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondGGetValue(const VarName: string; var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondGUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondH1GetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondH1UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondH2GetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondH2UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondH3GetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondH3UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondH4GetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondH4UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondH5GetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondH5UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondH6GetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondH6UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondIBGetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondIBUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondIRGetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondIRUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondR2GetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondR2UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondR3GetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondR3UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

procedure TFmBloImp.frxCondR4GetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

function TFmBloImp.frxCondR4UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  frxUserFunction(MethodName, Params);
end;

function TFmBloImp.frxUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if MethodName = 'VARF_MBB' then
    Params := QrCondMBB.Value else
  if MethodName = 'VARF_MRB' then
    Params := QrCondMRB.Value else
  if MethodName = 'VARF_MSB' then
    Params := QrCondMSB.Value else
  if MethodName = 'VARF_MSP' then
    Params := QrCondMSP.Value else
  if MethodName = 'VARF_MIB' then
    Params := QrCondMIB.Value else
  if MethodName = 'VARF_MPB' then
    Params := QrCondMPB.Value else
  if MethodName = 'VARF_MAB' then
    Params := QrCondMAB.Value
end;

procedure TFmBloImp.frxVersoGetValue(const VarName: string; var Value: Variant);
var
  Calculo: Integer;
begin
  if AnsiCompareText(VarName, 'VARF_BLQ_AltuHeader') = 0 then
    Value := Int(FQueryBloOpcoes.FieldByName('BLQ_TopoAvisoV').AsInteger / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuRotulo') = 0 then
  begin
    Calculo := 26000 (*- 4500*) - FQueryBloOpcoes.FieldByName('BLQ_TopoAvisoV').AsInteger;
    Value := Int(Calculo / VAR_frCM);
  end
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuDestin') = 0 then
    Value := FQueryBloOpcoes.FieldByName('BLQ_AltuDestin').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_MEsqDestin') = 0 then
    Value := FQueryBloOpcoes.FieldByName('BLQ_MEsqDestin').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_LargDestin') = 0 then
    Value := FQueryBloOpcoes.FieldByName('BLQ_LargDestin').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_TopoDestin') = 0 then
  begin
    Calculo := FQueryBloOpcoes.FieldByName('BLQ_TopoDestin').AsInteger -
               FQueryBloOpcoes.FieldByName('BLQ_TopoAvisoV').AsInteger;
    Value := Int(Calculo / VAR_frCM);
  end
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuAvisoV') = 0 then
    Value := FQueryBloOpcoes.FieldByName('BLQ_AltuAvisoV').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_MEsqAvisoV') = 0 then
    Value := FQueryBloOpcoes.FieldByName('BLQ_MEsqAvisoV').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_LargAvisoV') = 0 then
    Value := FQueryBloOpcoes.FieldByName('BLQ_LargAvisoV').AsInteger / VAR_frCM
  //
  else if AnsiCompareText(VarName, 'VARF_NOMEPROPRIET') = 0 then
    Value := FQueryBoletos.FieldByName('NOMEPROPRIET').AsString
  else if AnsiCompareText(VarName, 'VARF_LNR') = 0 then
    Value := QrInquilinoLNR.Value
  else if AnsiCompareText(VarName, 'VARF_LN2') = 0 then
    Value := QrInquilinoLN2.Value
  else if AnsiCompareText(VarName, 'VARF_AVISOVERSO') = 0 then
    Value := FQueryPrev.FieldByName('AvisoVerso').AsString;
end;

function TFmBloImp.ImprimeBloq(CkZerado, Escolher: Boolean; TabLctA, TabLctB,
  TabLctD: String; CliInt, EntCliInt: Integer; QueryPrev, QueryBoletos,
  QueryBoletosIts, QueryBloOpcoes: TmySQLQuery; frxDsPRI, frxDsBoletos,
  frxDsBoletosIts: TfrxDBDataset; Quais: TselType; Como: TfrxImpComo;
  Arquivo: String; Filtro: TfrxCustomExportFilter; Grade: TDBGrid): TfrxReport;
var
  prim, FVBol, Continua: Boolean;
  frxRep: TfrxReport;
  DataI, DataF: String;
  i, ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg, Codigo, Periodo: Integer;

  function GeraBloq(var ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe: Integer;
    const CNAB_Cfg: Integer): Boolean;
  var
    s: TMemoryStream;
    frxAtu: TfrxReport;
  begin
    if CNAB_Cfg = 0 then  //Fatura avulsa n�o imprime
    begin
      Result := True;
      Exit;
    end;
    //
    Result := False;
    try
      frxAtu := nil;
      //
      if not UBloqGerl.LiberaImpressaoBoletosCR(Dmod.MyDB, QueryBoletos) then
      begin
        Geral.MB_Aviso('O boleto n� ' +
          Geral.FFI(QueryBoletos.FieldByName('Boleto').AsFloat) +
          ' est� aguardando aprova��o do banco e n�o poder� ser impresso!');
        Exit;
      end;
      //
      DBloqGerl.ReopenCNAB_Cfg(CNAB_Cfg);
      (*
      DModG.ReopenEndereco(Entidade);
      DModG.ReopenEndereco2(DModG.QrFiliLogCodigo.Value);
      *)
      //
      if not DefineModeloBoleto(Escolher, DataI, DataF,
        FQueryBoletos.FieldByName('Apto').AsInteger, ModelBloq, ConfigBol,
        Colunas, BalAgrMens, Compe) then
      begin
        Geral.MB_Aviso('Modelo de boleto n�o definido!');
        Abort;
      end;
      case ModelBloq of
       -1: frxAtu := frxVerso;
        1: frxAtu := frxCondA;
        2: frxAtu := frxCondB;
        3: frxAtu := frxCondC;
        4: frxAtu := frxCondD;
        5:
        begin
          case Compe of
            1: frxAtu := frxCondE1;
            2: frxAtu := frxCondE2;
          end;
        end;
        6: frxAtu := frxCondG;
        7: frxAtu := frxCondG;
        8:
        begin
          case Colunas of
            1: frxAtu := frxCondH1;
            2: frxAtu := frxCondH2;
            3: frxAtu := frxCondH3;
            4: frxAtu := frxCondH4;
            5: frxAtu := frxCondH5;
            6: frxAtu := frxCondH6;
          end;
        end;
        9:
        begin
          case Colunas of
            2: frxAtu := frxCondR2;
            3: frxAtu := frxCondR3;
            4: frxAtu := frxCondR4;
          end;
        end;
        10:
        begin
          case Colunas of
            1: frxAtu := frxCondIB;
          end;
        end;
        11:
        begin
          case Colunas of
            1: frxAtu := frxCondIR;
          end;
        end;
      end;
      //
      frxRep := frxBloq;
      //
      frxBloq.OnGetValue         := frxAtu.OnGetValue;
      frxBloq.OnUserFunction     := frxAtu.OnUserFunction;
      frxBloq.ReportOptions.Name := 'Boletos';
      //
      if ModelBloq <> 0 then
      begin
        frxAtu.Variables['MeuLogoExiste']  := VAR_MEULOGOEXISTE;
        frxAtu.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
        frxAtu.Variables['LogoNFExiste']   := VAR_LOGONFEXISTE;
        frxAtu.Variables['LogoNFCaminho']  := QuotedStr(VAR_LOGONFPATH);
      end;
      //
      s := TMemoryStream.Create;
      frxAtu.SaveToStream(s);
      s.Position := 0;
      frxBloq.LoadFromStream(s);
      //
      //
      MyObjects.frxDefineDataSets(frxBloq, [
        F_frxDsBoletos,
        F_frxDsBoletosIts,
        frxDsCarts,
        frxDsCond,
        DBloqGerl.frxDsCNAB_Cfg,
        frxDsConfigBol,
        DMod.frxDsDono,
        DModG.frxDsEndereco,
        DModG.frxDsEndereco2,
        frxDsInquilino,
        DModG.frxDsMaster,
        frxDsMov,
        frxDsMov3,
        frxDsPRI,
        frxDsSdoCjt
      ], False);
      //
      ConfiguraMasterData(frxBloq);
      //
      if not prim then
      begin
        frxBloq.PrepareReport(True);
        prim := True;
      end else
        frxBloq.PrepareReport(False);
      Result := True;
    except
      Result := False;
    end;
  end;

  function GeraBloq2(var ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe: Integer;
    const CNAB_Cfg: Integer): Boolean;
  var
    s: TMemoryStream;
    frxAtu: TfrxReport;
  begin
    Result := False;
    try
      frxAtu := nil;
      //
      if not UBloqGerl.LiberaImpressaoBoletosCR(Dmod.MyDB, QueryBoletos) then
      begin
        Geral.MB_Aviso('O boleto n� ' +
          Geral.FFI(QueryBoletos.FieldByName('Boleto').AsFloat) +
          ' est� aguardando aprova��o do banco e n�o poder� ser impresso!');
        Exit;
      end;
      //
      DBloqGerl.ReopenCNAB_Cfg(CNAB_Cfg);
      (*
      DModG.ReopenEndereco(Entidade);
      DModG.ReopenEndereco2(DModG.QrFiliLogCodigo.Value);
      *)
      //
      if not DefineModeloBoleto(Escolher, DataI, DataF,
        FQueryBoletos.FieldByName('Apto').AsInteger, ModelBloq, ConfigBol,
        Colunas, BalAgrMens, Compe) then
      begin
        Geral.MB_Aviso('Modelo de boleto n�o definido!');
        Abort;
      end;
      case ModelBloq of
       -1: frxAtu := frxVerso;
        1: frxAtu := frxCondA;
        2: frxAtu := frxCondB;
        3: frxAtu := frxCondC;
        4: frxAtu := frxCondD;
        5:
        begin
          case Compe of
            1: frxAtu := frxCondE1;
            2: frxAtu := frxCondE2;
          end;
        end;
        6: frxAtu := frxCondG;
        7: frxAtu := frxCondG;
        8:
        begin
          case Colunas of
            1: frxAtu := frxCondH1;
            2: frxAtu := frxCondH2;
            3: frxAtu := frxCondH3;
            4: frxAtu := frxCondH4;
            5: frxAtu := frxCondH5;
            6: frxAtu := frxCondH6;
          end;
        end;
        9:
        begin
          case Colunas of
            2: frxAtu := frxCondR2;
            3: frxAtu := frxCondR3;
            4: frxAtu := frxCondR4;
          end;
        end;
        10:
        begin
          case Colunas of
            1: frxAtu := frxCondIB;
          end;
        end;
        11:
        begin
          case Colunas of
            1: frxAtu := frxCondIR;
          end;
        end;
      end;
      //
      frxRep := frxBloq;
      //
      frxBloq.OnGetValue         := frxAtu.OnGetValue;
      frxBloq.OnUserFunction     := frxAtu.OnUserFunction;
      frxBloq.ReportOptions.Name := 'Boletos';
      //
      if ModelBloq <> 0 then
      begin
        frxAtu.Variables['MeuLogoExiste']  := VAR_MEULOGOEXISTE;
        frxAtu.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
        frxAtu.Variables['LogoNFExiste']   := VAR_LOGONFEXISTE;
        frxAtu.Variables['LogoNFCaminho']  := QuotedStr(VAR_LOGONFPATH);
      end;
      //
      frxAtu.PrepareReport(True);
      //
      frxBloq.PreviewPages.AddFrom(frxAtu);
      //
      Result := True;
    except
      Result := False;
    end;
  end;

  function frxExporta(Titulo: string; Quais: TSelType; Arquivo: String;
    Filtro: TfrxCustomExportFilter): Boolean;

    function frxGravaAtual(frx: TfrxReport; Titulo, Arquivo: String;
      Filtro: TfrxCustomExportFilter): Boolean;
    begin
      Result := frx.Export(Filtro);
    end;

  var
    i, ModelBloq, Colunas, BalAgrMens, ConfigBol, Compe, CNAB_Cfg: Integer;
  begin
    Result := True;
    case Quais of
      istTodos:
      begin
        FQueryBoletos.First;
        while not FQueryBoletos.Eof do
        begin
          if Result then
          begin
            ModelBloq  := FQueryBoletos.FieldByName('ModelBloq').AsInteger;
            Colunas    := FQueryBoletos.FieldByName('Colunas').AsInteger;
            BalAgrMens := FQueryBoletos.FieldByName('BalAgrMens').AsInteger;
            Compe      := FQueryBoletos.FieldByName('Compe').AsInteger;
            CNAB_Cfg   := FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
            ConfigBol  := FQueryBoletos.FieldByName('ConfigBol').AsInteger;
            Result     := GeraBloq(ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg);
            //
            if Result then
              Result := frxGravaAtual(frxBloq, Titulo, Arquivo, Filtro);
          end;
          FQueryBoletos.Next;
        end;
      end;
      istSelecionados:
      begin
        if Grade = nil then
          Geral.MB_Erro('Grade n�o definida!')
        else
        begin
          with Grade.DataSource.DataSet do
          for i:= 0 to Grade.SelectedRows.Count-1 do
          begin
            //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
            GotoBookmark(Grade.SelectedRows.Items[i]);
            //
            ModelBloq  := FQueryBoletos.FieldByName('ModelBloq').AsInteger;
            Colunas    := FQueryBoletos.FieldByName('Colunas').AsInteger;
            BalAgrMens := FQueryBoletos.FieldByName('BalAgrMens').AsInteger;
            Compe      := FQueryBoletos.FieldByName('Compe').AsInteger;
            CNAB_Cfg   := FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
            ConfigBol  := FQueryBoletos.FieldByName('ConfigBol').AsInteger;
            Result     := GeraBloq(ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg);
            //
            if Result then
              Result := frxGravaAtual(frxBloq, Titulo, Arquivo, Filtro);
          end;
        end;
      end;
      istAtual:
      begin
        ModelBloq  := FQueryBoletos.FieldByName('ModelBloq').AsInteger;
        Colunas    := FQueryBoletos.FieldByName('Colunas').AsInteger;
        BalAgrMens := FQueryBoletos.FieldByName('BalAgrMens').AsInteger;
        Compe      := FQueryBoletos.FieldByName('Compe').AsInteger;
        CNAB_Cfg   := FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
        ConfigBol  := FQueryBoletos.FieldByName('ConfigBol').AsInteger;
        Result     := GeraBloq(ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg);
        //
        if Result then
          Result := frxGravaAtual(frxBloq, Titulo, Arquivo, Filtro);
      end;
    end;
  end;

  procedure ImprimeBoleto;
  begin
    Application.CreateForm(TFmMeuFrx, FmMeuFrx);
    frxBloq.Preview := FmMeufrx.PvVer;
    //
    FmMeufrx.PvVer.OutlineWidth := frxBloq.PreviewOptions.OutlineWidth;
    FmMeufrx.PvVer.Zoom         := frxBloq.PreviewOptions.Zoom;
    FmMeufrx.PvVer.ZoomMode     := frxBloq.PreviewOptions.ZoomMode;
    FmMeufrx.UpdateZoom;
    //
    FmMeufrx.ShowModal;
    FmMeufrx.Destroy;
  end;

begin
  Continua          := False;
  FCkZerado         := CkZerado;
  FTabLctA          := TabLctA;
  FTabLctB          := TabLctB;
  FTabLctD          := TabLctD;
  FQueryPrev        := QueryPrev;
  FQueryBoletos     := QueryBoletos;
  FQueryBoletosIts  := QueryBoletosIts;
  F_frxDsBoletos    := frxDsBoletos;
  F_frxDsBoletosIts := frxDsBoletosIts;
  F_frxDsPRI        := frxDsPRI;
  FQueryBloOpcoes   := QueryBloOpcoes;
  //
  Codigo  := FQueryPrev.FieldByName('Codigo').AsInteger;
  Periodo := FQueryPrev.FieldByName('Periodo').AsInteger;
  //
  frxRep := nil;
  prim   := False;
  FVBol  := False;
  //
  frxBloq.Clear;
  //
  ReabreTabelasBoleto(Codigo, CliInt, EntCliInt, Periodo, DataI, DataF);
  //
  case Quais of
    istAtual:
    begin
      case Como of
        ficMostra:
        begin
          ModelBloq  := FQueryBoletos.FieldByName('ModelBloq').AsInteger;
          Colunas    := FQueryBoletos.FieldByName('Colunas').AsInteger;
          BalAgrMens := FQueryBoletos.FieldByName('BalAgrMens').AsInteger;
          Compe      := FQueryBoletos.FieldByName('Compe').AsInteger;
          CNAB_Cfg   := FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
          ConfigBol  := FQueryBoletos.FieldByName('ConfigBol').AsInteger;
          //
          Continua := GeraBloq(ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg);
        end;
        ficExporta: frxExporta('Boletos', Quais, Arquivo, Filtro);
      end;
    end;
    istSelecionados:
    begin
      if Grade <> nil then
      begin
        if Grade.SelectedRows.Count > 1 then
        begin
          with Grade.DataSource.DataSet do
          for i:= 0 to Grade.SelectedRows.Count - 1 do
          begin
            //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
            GotoBookmark(Grade.SelectedRows.Items[i]);
            case Como of
              ficMostra:
              begin
                ModelBloq  := FQueryBoletos.FieldByName('ModelBloq').AsInteger;
                Colunas    := FQueryBoletos.FieldByName('Colunas').AsInteger;
                BalAgrMens := FQueryBoletos.FieldByName('BalAgrMens').AsInteger;
                Compe      := FQueryBoletos.FieldByName('Compe').AsInteger;
                CNAB_Cfg   := FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
                ConfigBol  := FQueryBoletos.FieldByName('ConfigBol').AsInteger;
                //
                Continua := GeraBloq(ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg);
              end;
              ficExporta: frxExporta('Boletos', Quais, Arquivo, Filtro);
            end;
          end;
        end else
        begin
          case Como of
            ficMostra:
            begin
              ModelBloq  := FQueryBoletos.FieldByName('ModelBloq').AsInteger;
              Colunas    := FQueryBoletos.FieldByName('Colunas').AsInteger;
              BalAgrMens := FQueryBoletos.FieldByName('BalAgrMens').AsInteger;
              Compe      := FQueryBoletos.FieldByName('Compe').AsInteger;
              CNAB_Cfg   := FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
              ConfigBol  := FQueryBoletos.FieldByName('ConfigBol').AsInteger;
              //
              Continua := GeraBloq(ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg);
            end;
            ficExporta: frxExporta('Boletos', Quais, Arquivo, Filtro);
          end;
        end;
      end else
        Geral.MB_Erro('Grade n�o definida!');
    end;
    istTodos:
    begin
      FQueryBoletos.First;
      while not FQueryBoletos.Eof do
      begin
        case Como of
          ficMostra:
          begin
            ModelBloq  := FQueryBoletos.FieldByName('ModelBloq').AsInteger;
            Colunas    := FQueryBoletos.FieldByName('Colunas').AsInteger;
            BalAgrMens := FQueryBoletos.FieldByName('BalAgrMens').AsInteger;
            Compe      := FQueryBoletos.FieldByName('Compe').AsInteger;
            CNAB_Cfg   := FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
            ConfigBol  := FQueryBoletos.FieldByName('ConfigBol').AsInteger;
            //
            Continua := GeraBloq(ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg);
          end;
          ficExporta: frxExporta('Boletos', Quais, Arquivo, Filtro);
        end;
        //
        FQueryBoletos.Next;
      end;
    end;
  end;
  if (Como = ficMostra) and (Continua = True) then
    ImprimeBoleto;
  //
  //Verso
  prim := False;
  frxBloq.Clear;
  case Quais of
    istAtual:
    begin
      if (FQueryPrev.FieldByName('BloqFV').AsInteger = 1) and (Como <> ficExporta) then
      begin
        ModelBloq  := -1;
        Colunas    := FQueryBoletos.FieldByName('Colunas').AsInteger;
        BalAgrMens := FQueryBoletos.FieldByName('BalAgrMens').AsInteger;
        Compe      := FQueryBoletos.FieldByName('Compe').AsInteger;
        CNAB_Cfg   := FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
        ConfigBol  := FQueryBoletos.FieldByName('ConfigBol').AsInteger;
        //
        FVBol := GeraBloq(ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg);
      end;
    end;
    istSelecionados:
    begin
      if Grade <> nil then
      begin
        if Grade.SelectedRows.Count > 1 then
        begin
          with Grade.DataSource.DataSet do
          for i:= 0 to Grade.SelectedRows.Count - 1 do
          begin
            //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
            GotoBookmark(Grade.SelectedRows.Items[i]);
            if (FQueryPrev.FieldByName('BloqFV').AsInteger = 1) and (Como <> ficExporta) then
            begin
              ModelBloq  := -1;
              Colunas    := FQueryBoletos.FieldByName('Colunas').AsInteger;
              BalAgrMens := FQueryBoletos.FieldByName('BalAgrMens').AsInteger;
              Compe      := FQueryBoletos.FieldByName('Compe').AsInteger;
              CNAB_Cfg   := FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
              ConfigBol  := FQueryBoletos.FieldByName('ConfigBol').AsInteger;
              //
              FVBol := GeraBloq(ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg);
            end;
          end;
        end else
        begin
          if (FQueryPrev.FieldByName('BloqFV').AsInteger = 1) and (Como <> ficExporta) then
          begin
            ModelBloq  := -1;
            Colunas    := FQueryBoletos.FieldByName('Colunas').AsInteger;
            BalAgrMens := FQueryBoletos.FieldByName('BalAgrMens').AsInteger;
            Compe      := FQueryBoletos.FieldByName('Compe').AsInteger;
            CNAB_Cfg   := FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
            ConfigBol  := FQueryBoletos.FieldByName('ConfigBol').AsInteger;
            //
            FVBol := GeraBloq(ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg);
          end;
        end;
      end else
        Geral.MB_Erro('Grade n�o definida!');
    end;
    istTodos:
    begin
      FQueryBoletos.First;
      while not FQueryBoletos.Eof do
      begin
        if (FQueryPrev.FieldByName('BloqFV').AsInteger = 1) and (Como <> ficExporta) then
        begin
          ModelBloq  := -1;
          Colunas    := FQueryBoletos.FieldByName('Colunas').AsInteger;
          BalAgrMens := FQueryBoletos.FieldByName('BalAgrMens').AsInteger;
          Compe      := FQueryBoletos.FieldByName('Compe').AsInteger;
          CNAB_Cfg   := FQueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
          ConfigBol  := FQueryBoletos.FieldByName('ConfigBol').AsInteger;
          //
          FVBol := GeraBloq(ModelBloq, ConfigBol, Colunas, BalAgrMens, Compe, CNAB_Cfg);
        end;
        //
        FQueryBoletos.Next;
      end;
    end;
  end;
  if (Como = ficMostra) and (FVBol = True) then
    ImprimeBoleto;
  //
  if frxRep = nil then
    Result := nil
  else
    Result := frxBloq;
end;

procedure TFmBloImp.QrCartNCalcFields(DataSet: TDataSet);
begin
  QrCartNANTERIOR.Value := QrCartNInicial.Value + QrCartNANT_CRED.Value - QrCartNANT_DEB.Value;
  QrCartNRECEITAS.Value := QrCartNATU_CRED.Value - QrCartNANT_CRED.Value;
  QrCartNDESPESAS.Value := -(QrCartNATU_DEB.Value - QrCartNANT_DEB.Value);
  QrCartNSALDOMES.Value := QrCartNRECEITAS.Value + QrCartNDESPESAS.Value;
  QrCartNFINAL.Value := QrCartNInicial.Value + QrCartNATU_CRED.Value
    - QrCartNATU_DEB.Value + QrCartNTRANSF.Value;
end;

procedure TFmBloImp.QrCartsCalcFields(DataSet: TDataSet);
begin
  QrCartsANTERIOR.Value := QrCartsInicial.Value + QrCartsANT_CRED.Value - QrCartsANT_DEB.Value;
  QrCartsRECEITAS.Value := QrCartsATU_CRED.Value - QrCartsANT_CRED.Value - QrCartsTRF_CRED.Value;
  QrCartsDESPESAS.Value := -(QrCartsATU_DEB.Value - QrCartsANT_DEB.Value - QrCartsTRF_DEBI.Value);
  QrCartsSALDOMES.Value := QrCartsRECEITAS.Value + QrCartsDESPESAS.Value;
  QrCartsFINAL.Value    := QrCartsInicial.Value + QrCartsATU_CRED.Value - QrCartsATU_DEB.Value;
end;

procedure TFmBloImp.QrCondCalcFields(DataSet: TDataSet);
begin
  QrCondDOCNUM_TXT.Value := Geral.FormataCNPJ_TT(QrCondDOCNUM.Value);
end;

procedure TFmBloImp.QrInquilinoCalcFields(DataSet: TDataSet);
  procedure Agregador(var Endereco: String; const Item, Prefixo, PosFixo: String);
  begin
    if Item <> '' then
    Endereco := Endereco + Prefixo + Item + PosFixo;
    //Result := True;
  end;
var
  LNR, LN2, Compl, Resp, Prop: String;
  TipoEnd: Integer;
begin
  Resp := DModG.ReCaptionTexto('Resp.');
  Prop := DModG.ReCaptionTexto('Propr.');
  //
  QrInquilinoTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrICTe1.Value);
  QrInquilinoFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrICFax.Value);
  QrInquilinoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrICCNPJ_CPF.Value);

  //
  ///////////////// P R E P A R A � � O   D O   E N D E R E � O ////////////////
  //
  TipoEnd := 0;
  case QrInquilinoBloqEndTip.Value of
    //WHEN 0 THEN (IF(imv.Usuario <> 0, cnd.Cliente, imv.Propriet) )
    0: if QrInquilinoUsuario.Value > 0 then TipoEnd := 1 else TipoEnd := 0;
    1: TipoEnd := 1;
    2: TipoEnd := 0;
    3: TipoEnd := 0; //
    4: TipoEnd := 2;
  end;
  case TipoEnd of
    0: Compl := QrICCOMPL.Value;
    1: Compl := QrInquilinoPrefixoUH.Value + ' ' + QrInquilinoUnidade.Value;
    else Compl := '';
  end;
  //
  if QrICNumero.Value = 0 then
    QrInquilinoNUMERO_TXT.Value :=  'S/N�'
  else
    QrInquilinoNUMERO_TXT.Value := FormatFloat('###,###,##0', QrICNumero.Value);
  //
  LNR := '';
  LN2 := '';
  if TipoEnd < 2 then
  begin
    //
    /////////////////////////// E N D E R E � O //////////////////////////////////
    //
    // Tipo Logradouro
    Agregador(LNR, QrICNOMELOGRAD.Value, '', ' ');
    // Rua
    Agregador(LNR, QrICRUA.Value, '', ', ');
    // N�mero
    Agregador(LNR, QrInquilinoNUMERO_TXT.Value, '', ' ' );
    // Complemento
    Agregador(LNR, Compl, '', ', ');
    // Bairro
    Agregador(LNR, QrICBairro.Value, '', ' - ');
    // Cidade e UF
    Agregador(LN2, QrICCidade.Value, '', ' - ' + QrICNOMEUF.Value);
    // CEP
    Agregador(LN2, Geral.FormataCEP_NT(QrICCEP.Value), '     CEP ', '');
    QrInquilinoLNR.Value := LNR;
    QrInquilinoLN2.Value := LN2;
    QrInquilinoE_ALL.Value := QrInquilinoLNR.Value + ' ' + QrInquilinoLN2.Value;
  end else begin
    QrInquilinoLNR.Value := QrInquilinoEnderLin1.Value;
    QrInquilinoLN2.Value := QrInquilinoEnderLin2.Value;
    QrInquilinoE_ALL.Value :=
      QrInquilinoEnderLin1.Value + sLineBreak + QrInquilinoEnderLin2.Value;
  end;
  ///////////////////// F I M   E N D E R E � O ////////////////////////////////
  //
  QrInquilinoECEP_TXT.Value := Geral.FormataCEP_NT(QrICCEP.Value);
  //
  if Trim(QrDonoUHNOMEPROPRIET.Value) <> '' then
     QrInquilinoPROPRI_E_MORADOR.Value := Prop + ': ' +
     QrDonoUHNOMEPROPRIET.Value + ' - ' + QrDonoUHNOMETIPO.Value + ' ' + QrDonoUHCNPJ_CPF_TXT.Value +
     '  ||  ' + Resp +  ': ' + QrInquilinoNO_USUARIO.Value  + ' - ' +
     QrInquilinoNOMETIPO_USUARIO. Value + ' ' +
     Geral.FormataCNPJ_TT(QrInquilinoCNPJ_CPF_USUARIO.Value)
  else begin
    case TipoEnd of
      0: // entidade pesquisada
      begin
        if QrInquilinoBloqEndTip.Value = 2 then
        begin
          QrInquilinoPROPRI_E_MORADOR.Value := Prop + ': ' +
          QrICNOMEENT.Value  + ' - ' +
          QrICNOMETIPO.Value + ' ' + QrInquilinoCNPJ_TXT.Value;
        end else if QrInquilinoBloqEndTip.Value = 0 then
        begin
          if QrInquilinoUsuario.Value = 0 then
          begin
            QrInquilinoPROPRI_E_MORADOR.Value := Prop + ': ' +
            QrICNOMEENT.Value  + ' - ' +
            QrICNOMETIPO.Value + ' ' + QrInquilinoCNPJ_TXT.Value;
          end else
          begin
            QrInquilinoPROPRI_E_MORADOR.Value := Resp + ': ' +
            QrICNOMEENT.Value  + ' - ' +
            QrICNOMETIPO.Value + ' ' + QrInquilinoCNPJ_TXT.Value;
          end;
        end else
        begin
          QrInquilinoPROPRI_E_MORADOR.Value := Resp + ': ' +
          QrICNOMEENT.Value  + ' - ' +
          QrICNOMETIPO.Value + ' ' + QrInquilinoCNPJ_TXT.Value;
        end;
      end;
      1: // do moraddor
      begin
        QrInquilinoPROPRI_E_MORADOR.Value := Resp + ': ' +
        QrInquilinoNO_USUARIO.Value  + ' - ' +
        QrInquilinoNOMETIPO_USUARIO.Value + ' ' +
        Geral.FormataCNPJ_TT(QrInquilinoCNPJ_CPF_USUARIO.Value);
      end;
      else //2: // ningu�m
      begin
        QrInquilinoPROPRI_E_MORADOR.Value := QrInquilinoEnderNome.Value;
      end;
    end;
  end;
end;

procedure TFmBloImp.QrMov3CalcFields(DataSet: TDataSet);
var
  SubPgto1: String;
begin
  if QrMov3SubPgto1.Value = 0 then
    SubPgto1 := ''
  else
    SubPgto1 := ' RECEB. ACORDO EXTRAJUDICIAL';

  QrMov3NOMECONTA_EXT.Value := QrMov3NOMECONTA.Value;
  if (QrMov3Mez.Value > 0) and (FBalAgrMens = 1) then
    QrMov3NOMECONTA_EXT.Value := QrMov3NOMECONTA_EXT.Value + ' - ' +
    dmkPF.MezToFDT(QrMov3Mez.Value, 0, 104) + SubPgto1;
end;

procedure TFmBloImp.QrMovBeforeClose(DataSet: TDataSet);
begin
  QrCjSdo.Close;
  QrCjMov.Close;
end;

procedure TFmBloImp.QrMovCalcFields(DataSet: TDataSet);
var
  SubPgto1: String;
begin
  if QrMovSubPgto1.Value = 0 then SubPgto1 := ''
    else SubPgto1 := ' RECEB. ACORDO EXTRAJUDICIAL';

  QrMovNOMECONTA_EXT.Value := QrMovNOMECONTA.Value;
  if QrMovMez.Value > 0 then QrMovNOMECONTA_EXT.Value :=
    QrMovNOMECONTA_EXT.Value + ' - ' + dmkPF.MezToFDT(QrMovMez.Value, 0, 104)
    + SubPgto1;
  //
  QrMovCJ_SALI.Value :=
    QrMovCJ_SALF.Value - QrMovCJ_CRED.Value + QrMovCJ_DEBI.Value;
  QrMovCJ_SALM.Value :=
    QrMovCJ_CRED.Value - QrMovCJ_DEBI.Value;
end;

procedure TFmBloImp.ReabreTabelasBoleto(Codigo, CliInt, EntCliInt,
  Periodo: Integer; var DataI, DataF: String);

  procedure GeraParteSQL(TabLct, Empresa, DataI, DataF: String);
  begin
    QrMov.SQL.Add('SELECT pla.Nome NOMEPLANO, cjt.Nome NOMECONJUNTO,');
    QrMov.SQL.Add('gru.Conjunto, gru.Nome NOMEGRUPO, sgr.Grupo,');
    QrMov.SQL.Add('sgr.Nome NOMESUBGRUPO, con.SubGrupo, con.Nome NOMECONTA,');
    QrMov.SQL.Add('lct.Genero, lct.Mez, SUM(lct.Credito - lct.Debito) Valor,');
    QrMov.SQL.Add('lct.SubPgto1, pla.OrdemLista OL_PLA,');
    QrMov.SQL.Add('cjt.OrdemLista OL_CJT, gru.OrdemLista OL_GRU,');
    QrMov.SQL.Add('sgr.OrdemLista OL_SGR, con.OrdemLista OL_CTA');
    QrMov.SQL.Add('');
    QrMov.SQL.Add('FROM ' + TabLct + ' lct');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lct.Genero');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.plano     pla ON pla.Codigo=cjt.Plano');
    QrMov.SQL.Add('');
    QrMov.SQL.Add('WHERE lct.CliInt=' + Empresa);
    QrMov.SQL.Add('AND car.Tipo < 2');
    QrMov.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
    QrMov.SQL.Add('GROUP BY lct.Genero, lct.Mez, lct.SubPgto1');
  end;

var
  DtIni, DtEncer, DtMorto: TDateTime;
  FldIni, Empresa: String;
  Mez: Integer;
begin
  DefineSaldoResumo;
  //
  FldIni := UFinanceiro.DefLctFldSdoIni(DtIni, DtEncer, DtMorto);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAptoModBloq, Dmod.MyDB, [
    'SELECT pmb.ModelBloq, pmb.ConfigBol, pmb.BalAgrMens, ',
    'pmb.Compe, cfb.Colunas, pmb.Apto ',
    'FROM prevmodbol pmb ',
    'LEFT JOIN configbol cfb ON cfb.Codigo=pmb.ConfigBol ',
    'WHERE pmb.Codigo=' + Geral.FF0(Codigo),
    '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrCond, Dmod.MyDB, [
    'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI, mun.Nome CIDADE, ',
    'IF(ent.Tipo=0, ent.EUF, ent.PUF) + 0.000 UF, ufs.Nome NOMEUF, con.*, ',
    'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCNUM ',
    'FROM cond con ',
    'LEFT JOIN entidades ent ON ent.Codigo=con.Cliente ',
    'LEFT JOIN ufs   ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) ',
    'WHERE con.Codigo=' + Geral.FF0(CliInt),
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPendT, Dmod.MyDB, [
    'SELECT SUM(lan.Pago-lan.Credito) SALDO ',
    'FROM ' + FTabLctA + ' lan ',
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
    'WHERE car.Tipo=2 ',
    'AND car.ForneceI=' + Geral.FF0(FQueryPrev.FieldByName('CondCli').AsInteger),
    'AND lan.Sit<2 ',
    'AND lan.Reparcel=0 ',
    'AND lan.Vencimento < SYSDATE() ',
    '']);
  //
  Empresa := Geral.FF0(QrCondCliente.Value);
  DataI := Geral.FDT(dmkPF.PrimeiroDiaDoPeriodo_Date(Periodo - 1 + QrCondPBB.Value), 1);
  DataF := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(Periodo - 1 + QrCondPBB.Value), 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCjSdo, Dmod.MyDB, [
    'SELECT Codigo, SdoAtu ',
    'FROM conjunsdo ',
    'WHERE Entidade=' + Geral.FF0(QrCondCliente.Value),
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCjMov, Dmod.MyDB, [
    'SELECT gru.Conjunto, SUM(lan.Credito) Credito, ',
    'SUM(lan.Debito) Debito, SUM(lan.Credito - lan.Debito) Valor ',
    'FROM ' + FTabLctA + ' lan ',
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
    'LEFT JOIN contas    con ON con.Codigo=lan.Genero ',
    'LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo ',
    'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo ',
    ' ',
    'WHERE lan.CliInt=' + Empresa,
    'AND car.Tipo < 2 ',
    'AND lan.Data BETWEEN "' + DataI + '" AND "' + DataF + '" ',
    'GROUP BY gru.Conjunto ',
    '']);
  //
  if QrCondPBB.Value = 0 then
    Mez := dmkPF.PeriodoToAnoMes(FQueryPrev.FieldByName('Periodo').AsInteger - 1)
  else
    Mez := dmkPF.PeriodoToAnoMes(FQueryPrev.FieldByName('Periodo').AsInteger);
  //
  DmodFin.AtualizaContasHistSdo3(QrCondCliente.Value, nil(*LaSub1*), nil, DModG.MezLastEncerr(), FTabLctA);
  DmodFin.SaldosDeContasControladas3(QrCondCliente.Value, Mez, QrSaldosNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSdoGru, DModG.MyPID_DB, [
    'SELECT CO_SGR Codigo, SdoAnt, ',
    'Credito SumCre, Debito SumDeb, Movim SdoFim, ',
    'Credito-Debito Valor ',
    'FROM saldosniv ',
    'WHERE Nivel=2 ',
    '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrSdoGru, DModG.MyPID_DB, [
    'SELECT CO_GRU Codigo, SdoAnt, ',
    'Credito SumCre, Debito SumDeb, Movim SdoFim, ',
    'Credito-Debito Valor ',
    'FROM saldosniv ',
    'WHERE Nivel=3 ',
    '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrSdoCjt, DModG.MyPID_DB, [
    'SELECT NO_Cjt NOME_CJT, CO_Cjt Codigo, SdoAnt, ',
    'Credito SumCre, Debito SumDeb, Movim SdoFim, ',
    'Credito-Debito Valor ',
    'FROM saldosniv ',
    'WHERE Nivel=4 ',
    '']);
  //
  QrMov.Close;
  QrMov.SQL.Clear;
  QrMov.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_RESUMO_1_;');
  QrMov.SQL.Add('CREATE TABLE _MOD_COND_RESUMO_1_');
  QrMov.SQL.Add('');
  {$IFDEF DEFINE_VARLCT}
    GeraParteSQL(FTabLctA, Empresa, DataI, DataF);
    QrMov.SQL.Add('UNION');
    GeraParteSQL(FTabLctB, Empresa, DataI, DataF);
    QrMov.SQL.Add('UNION');
    GeraParteSQL(FTabLctD, Empresa, DataI, DataF);
    QrMov.SQL.Add(';');
  {$ELSE}
    GeraParteSQL(FTabLctA, Empresa, DataI, DataF);
  {$ENDIF}
  QrMov.SQL.Add('SELECT NOMEPLANO, NOMECONJUNTO, Conjunto, NOMEGRUPO, Grupo,');
  QrMov.SQL.Add('NOMESUBGRUPO, SubGrupo, NOMECONTA, Genero, Mez, Valor, SubPgto1');
  QrMov.SQL.Add('FROM _MOD_COND_RESUMO_1_');
  QrMov.SQL.Add('ORDER BY OL_PLA, NOMEPLANO, OL_CJT, NOMECONJUNTO,');
  QrMov.SQL.Add('OL_GRU, NOMEGRUPO, OL_SGR, NOMESUBGRUPO,');
  QrMov.SQL.Add('OL_CTA, NOMECONTA, Mez');
  QrMov.SQL.Add(';');
  UMyMod.AbreQuery(QrMov, Dmod.MyDB);
end;

procedure TFmBloImp.ReopenEnderecoInquilino();
var
  Conta: Integer;
begin
  Conta := FQueryBoletos.FieldByName('Apto').AsInteger;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDonoUH, Dmod.MyDB, [
    'SELECT IF(cim.Usuario=0 OR cim.Usuario=cim.Propriet, "", ',
    '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NOMEPROPRIET, ',
    'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, ',
    'ELT(ent.Tipo+1, "CNPJ", "CPF") NOMETIPO ',
    'FROM condimov cim ',
    'LEFT JOIN entidades ent ON ent.Codigo=cim.Propriet ',
    'WHERE cim.Conta=' + Geral.FF0(Conta),
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrIC, Dmod.MyDB, [
    'SELECT en.Codigo, en.Tipo, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome    END NOMEENT, ',
    'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF     END CNPJ_CPF, ',
    'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG      END IE_RG, ',
    'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""         END NIRE_, ',
    'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua    END RUA, ',
    'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END + 0.000 NUMERO, ',
    'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL, ',
    'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIRRO, ',
    'IF(en.Tipo=0, IF(en.ECodMunici = 0, en.ECidade, emu.Nome), IF(en.PCodMunici = 0, en.PCidade, pmu.Nome)) CIDADE, ',
    'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOMELOGRAD, ',
    'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOMEUF, ',
    'IF(en.Tipo=0, IF(en.ECodiPais = 0, en.EPais, epa.Nome), IF(en.PCodiPais = 0, en.PPais, ppa.Nome)) Pais, ',
    'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END + 0.000 Lograd, ',
    'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END + 0.000 CEP, ',
    'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1, ',
    'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX, ',
    'ELT(en.Tipo+1, "CNPJ", "CPF") NOMETIPO ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN ufs ufc ON ufc.Codigo=en.CUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici emu ON emu.Codigo=en.ECodMunici ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici pmu ON pmu.Codigo=en.PCodMunici ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais epa ON epa.Codigo=en.ECodiPais ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais ppa ON ppa.Codigo=en.PCodiPais ',
    'WHERE en.Codigo = (SELECT ',
    'CASE imv.BloqEndTip ',
    '  WHEN 0 THEN (IF(imv.Usuario <> 0, cnd.Cliente, imv.Propriet) ) ',
    '  WHEN 1 THEN cnd.Cliente ',
    '  WHEN 2 THEN imv.Propriet ',
    '  WHEN 3 THEN BloqEndEnt ',
    '  WHEN 4 THEN 0 ',
    'END ',
    'FROM condimov imv ',
    'LEFT JOIN cond cnd ON cnd.Codigo=imv.Codigo ',
    'WHERE imv.Conta=' + Geral.FF0(Conta),
    ') ',
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrInquilino, Dmod.MyDB, [
    'SELECT imv.Unidade, imv.BloqEndTip, blc.PrefixoUH, ',
    'imv.Usuario, cnd.Cliente, imv.Propriet, ',
    'imv.EnderNome, imv.EnderLin1, imv.EnderLin2, ',
    'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF     END CNPJ_CPF_USUARIO, ',
    'ELT(en.Tipo+1, "CNPJ", "CPF") NOMETIPO_USUARIO, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome    END NO_USUARIO ',
    'FROM condimov imv ',
    'LEFT JOIN cond cnd ON cnd.Codigo=imv.Codigo ',
    'LEFT JOIN condbloco blc ON imv.Controle=blc.Controle ',
    'LEFT JOIN entidades en ON en.Codigo=imv.Usuario ',
    'WHERE imv.Conta=' + Geral.FF0(Conta),
    '']);
end;

function TFmBloImp.TraduzInstrucaoArrecadacao(Instrucao, Propriet,
  Unidade, Empresa, EmpresaCNPJ: String; FracaoIdeal: Double): String;

  procedure Troca(var Instr: String; const Variavel: String);
  var
    p, t1: Integer;
    Txt: String;
  begin
    p := pos(Variavel, Instr);
    t1 := Length(Variavel);
    if p > 0 then
    begin
      Delete(Instr, p, t1);
      Txt := '';
      if Variavel = '[PROPRIET]' then
        Txt := Propriet
      else if Variavel = '[UNIDADE]' then
        Txt := Unidade
      else if Variavel = '[VALFRAC]' then
        Txt := FormatFloat('0', FracaoIdeal)
      else if Variavel = '[EMPRNOME]' then
        Txt:= Empresa
      else if Variavel = '[EMPRCNPJ]' then
        Txt := EmpresaCNPJ;
      //
      Insert(Txt, Instr, p);
    end;
  end;

var
  Instr: String;
begin
  Instr := Instrucao;
  Troca(Instr, '[PROPRIET]');
  Troca(Instr, '[UNIDADE]');
  Troca(Instr, '[VALFRAC]');
  Troca(Instr, '[EMPRNOME]');
  Troca(Instr, '[EMPRCNPJ]');
  Result := Instr;
end;

function TFmBloImp.ObtemValorQrBoletosIts(Item, Tipo: Integer): String;
begin
  if FQueryBoletosIts.RecordCount >= Item then
  begin
    if FQueryBoletosIts.RecNo < Item then
      while FQueryBoletosIts.RecNo < Item do
        FQueryBoletosIts.Next;
    if FQueryBoletosIts.RecNo > Item then
      while FQueryBoletosIts.RecNo > Item do
        FQueryBoletosIts.Prior;
    case Tipo of
        0: Result := FQueryBoletosIts.FieldByName('TEXTO_IMP').AsString;
        1: Result := Geral.FFT(FQueryBoletosIts.FieldByName('VALOR').AsFloat, 2, siPositivo);
      else Result := '<?>';
    end;
  end else
    Result := ' ';
end;

procedure TFmBloImp.DefineSaldoResumo;
begin
  FResumo_SaldoAnt := 0;
  FResumo_Receitas := 0;
  FResumo_Despesas := 0;
  FResumo_SaldoMes := 0;
  FResumo_SaldoTrf := 0;
  //
  ReopenResumo();
  //
  QrCarts.First;
  while not QrCarts.Eof do
  begin
    FResumo_SaldoAnt := FResumo_SaldoAnt + QrCartsANTERIOR.Value;
    FResumo_Receitas := FResumo_Receitas + QrCartsRECEITAS.Value;
    FResumo_Despesas := FResumo_Despesas + QrCartsDESPESAS.Value;
    FResumo_SaldoMes := FResumo_SaldoMes + QrCartsSALDOMES.Value;
    FResumo_SaldoTrf := FResumo_SaldoTrf + QrCartsFINAL.Value;
    //
    QrCarts.Next;
  end;
  //
  QrCartN.First;
  while not QrCartN.Eof do
  begin
    FResumo_SaldoAnt := FResumo_SaldoAnt + QrCartNANTERIOR.Value;
    FResumo_Receitas := FResumo_Receitas + QrCartNRECEITAS.Value;
    FResumo_Despesas := FResumo_Despesas + QrCartNDESPESAS.Value;
    FResumo_SaldoMes := FResumo_SaldoMes + QrCartNSALDOMES.Value;
    FResumo_SaldoTrf := FResumo_SaldoTrf + QrCartNFINAL.Value;
    //
    QrCartN.Next;
  end;
end;

procedure TFmBloImp.ReopenPendU(Entidade, Apto: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPendU, Dmod.MyDB, [
    'SELECT SUM(lan.Pago-lan.Credito) SALDO ',
    'FROM ' + FTabLctA + ' lan ',
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
    'WHERE car.Tipo=2 ',
    'AND lan.Reparcel=0 ',
    'AND car.ForneceI=' + Geral.FF0(Entidade),
    'AND lan.Depto=' + Geral.FF0(Apto),
    'AND lan.Sit<2 ',
    'AND lan.Vencimento < SYSDATE() ',
    '']);
end;

function TFmBloImp.FormataPeriodo(Periodo, FmtType: Integer): String;
var
  DataI, DataF: TDateTime;
begin
  Result := '';
  DataI := dmkPF.PrimeiroDiaDoPeriodo_Date(Periodo);
  DataF := dmkPF.UltimoDiaDoPeriodo_Date(Periodo);
  case FmtType of
    //dd/mm/aa a dd/mm/aa
    0: Result := Geral.FDT(DataI,  3) + ' a ' + Geral.FDT(DataF,  3);
    //dd a dd/mm/aa
    1: Result := Geral.FDT(DataI, 16) + ' a ' + Geral.FDT(DataF,  3);
    //mmmm/aa
    2: Result := Geral.FDT(DataI, 17);
    //mmm/aa
    3: Result := Geral.FDT(DataI, 18);
    //mm/aa
    4: Result := Geral.FDT(DataI, 19);
    else Result := '???'
  end;
end;

procedure TFmBloImp.FormCreate(Sender: TObject);
begin
  QrCtasSdo.DataBase   := DModG.MyPID_DB;
  QrSdoGru.DataBase    := DModG.MyPID_DB;
  QrSdoGru.DataBase    := DModG.MyPID_DB;
  QrSaldosNiv.DataBase := DModG.MyPID_DB;
  //
  frxCondH2.ScriptText := frxCondH1.ScriptText;
  frxCondH3.ScriptText := frxCondH1.ScriptText;
  frxCondH4.ScriptText := frxCondH1.ScriptText;
  frxCondH5.ScriptText := frxCondH1.ScriptText;
  frxCondH6.ScriptText := frxCondH1.ScriptText;
  frxCondR3.ScriptText := frxCondR2.ScriptText;
  frxCondR4.ScriptText := frxCondR2.ScriptText;
  //frxCondR2.ScriptText := frxCondH1.ScriptText;
  frxCondR3.ScriptText := frxCondR2.ScriptText;
  frxCondR4.ScriptText := frxCondR2.ScriptText;
end;

procedure TFmBloImp.ReopenResumo();
var
  Ini, Fim: String;
  Entidade, CliInt: Integer;
  DtIni, DtEncer, DtMorto: TDateTime;
  Enti_TXT, FldIni, TabLctA, TabLctB, TabLctD, TabLctX: String;
begin
  Ini := Geral.FDT(dmkPF.PrimeiroDiaDoPeriodo_Date(
           FQueryPrev.FieldByName('Periodo').AsInteger - 1 + QrCondPSB.Value), 1);
  Fim := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(
           FQueryPrev.FieldByName('Periodo').AsInteger - 1 + QrCondPSB.Value), 1);
  //
  Entidade := FQueryPrev.FieldByName('CondCli').AsInteger;
  Enti_TXT := Geral.FF0(Entidade);
  CliInt   := FQueryPrev.FieldByName('Cond').AsInteger;
  //
  DtIni := Int(dmkPF.PrimeiroDiaDoPeriodo_Date(
             FQueryPrev.FieldByName('Periodo').AsInteger - 1 + QrCondPSB.Value));
  //
  DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
  //
  TabLctX := UFinanceiro.DefLctTab(DtIni, DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
  FldIni  := UFinanceiro.DefLctFldSdoIni(DtIni, DtEncer, DtMorto);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartsIni, Dmod.MyDB, [
    'SELECT SUM(lct.Credito) Credito, SUM(lct.Debito) Debito, ',
    'lct.Carteira ',
    'FROM ' + FTabLctA + ' lct ',
    'WHERE lct.Tipo < 2 ',
    'AND lct.Data < "' + Ini + '" ',
    'AND lct.Carteira in ( ',
    '  SELECT Codigo ',
    '  FROM carteiras ',
    '  WHERE ForneceI=' + Enti_TXT,
    '  AND Tipo < 2) ',
    'GROUP BY lct.Carteira ',
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartsFim, Dmod.MyDB, [
    'SELECT SUM(lct.Credito) Credito, SUM(lct.Debito) Debito, ',
    'lct.Carteira ',
    'FROM ' + FTabLctA + ' lct ',
    'WHERE lct.Tipo < 2 ',
    'AND lct.Data <= "' + Fim + '" ',
    'AND lct.Carteira in ( ',
    '  SELECT Codigo ',
    '  FROM carteiras ',
    '  WHERE ForneceI=' + Enti_TXT,
    '  AND Tipo < 2) ',
    'GROUP BY lct.Carteira ',
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartsTrf, Dmod.MyDB, [
    'SELECT SUM(lct.Credito) CRED, SUM(lct.Debito) DEB, ',
    'SUM(lct.Credito-lct.Debito) Valor, lct.Carteira ',
    'FROM ' + FTabLctA + ' lct ',
    'WHERE lct.Genero = -1 ',
    'AND (lct.Data BETWEEN "' + Ini + '" AND "' + Fim + '") ',
    'AND lct.Carteira in ( ',
    '  SELECT Codigo ',
    '  FROM carteiras ',
    '  WHERE ForneceI=' + Enti_TXT,
    '  AND Tipo < 2) ',
    'GROUP BY lct.Carteira ',
    '']);
  //
  // Reabrir somente ap�s Ini e Fim para Lookup
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarts, Dmod.MyDB, [
    'SELECT car.Codigo Carteira, car.Nome NOMECART, ' + FldIni + ' Inicial ',
    'FROM carteiras car ',
    'WHERE car.ForneceI=' + Geral.FF0(FQueryPrev.FieldByName('CondCli').AsInteger),
    'AND car.ForneceN=0 ',
    'AND car.Tipo < 2 ',
    '']);
  // Carteiras de terceiros (Cobradores)
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartN, Dmod.MyDB, [
    'SELECT Codigo Carteira, Nome NOMECART, Inicial ',
    'FROM carteiras ',
    'WHERE ForneceI=' + Geral.FF0(FQueryPrev.FieldByName('CondCli').AsInteger),
    'AND ForneceN=1 ',
    'AND Tipo < 2 ',
    '']);
end;

end.
