unit UnBloquetos;

interface

uses System.Variants, mySQLDbTables, UMySQLModule, dmkGeral, Module, ModuleGeral,
  dmkImage, Forms, Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids, DB,
  UnDmkProcFunc, ExtCtrls, dmkDBGrid, Math, Dialogs, UnDmkEnums,
  Classes, dmkDBGridZTO, frxDBSet, frxClass, UnInternalConsts, Vcl.StdCtrls;

type
  TUnBloquetos = class(TObject)
  private
  public
    function  ObtemDocumProtPakIts(ProtPakIts: Integer): Double;
    function  LetraModelosBloq(Item: Integer): String;
    function  TextoExplicativoItemBoleto(EhConsumo, Casas: Integer; MedAnt,
              MedAtu, Consumo, UnidFat: Double; UnidLei, UnidImp: String;
              GeraTyp, CasRat, NaoImpLei: Integer; GeraFat: Double): String;
    function  CalculaTotalPRI(TabelaPriA, TabelaPrvA: String; Periodo, Codigo,
              Controle: Integer): Boolean;
    function  RecalculaArrecadacoes(TabelaAriA, TabelaLctA: String; Codigo,
              CliInt: Integer; Gastos: Double): Boolean;
    function  VerificaSeLctEstaoNaTabLctA(TabLctA: String; Lancto: Integer;
                QueryAux: TmySQLQuery; var Msg: String): Boolean;
    function  VerificaSeLancamentoEstaPago(Lancto: Integer; QueryAux: TmySQLQuery;
              TabLctA: String; var Msg: String): Boolean;
    function  GetPercAptUni(Controle, ListaBaU, Conta: Integer; NomeConta: String): Double;
    function  GetPercAptAll(Controle, ListaBaU: Integer): Double;
    function  ObtemDiaVenctoDeApto(CliInt, Depto: Integer): Integer;
    function  AlteraVencimentoPreBol(CliInt, Periodo, Depto: Integer;
              Prebol: Boolean; Tipo: TSelType; Grade: TDBGrid; QueryBoletos,
              QueryBoletosIts, QueryUpd, QueryAux: TmySQLQuery; Database: TmySQLDatabase;
              Progress: TProgressBar; TabLctA: String): Boolean;
    function  VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts,
              QueryAux: TmySQLQuery; Database: TmySQLDatabase; TabLctA: String): Boolean;
    function  DesfazerBoletos(Selecao: TSelType; QueryUpd, QueryAux,
                QueryBoletos, QueryBoletosIts: TmySQLQuery;
                Database: TmySQLDatabase; Grade: TDBGrid; TabLctA, TabPrvA,
                TabAriA, TabCnsA: String; Codigo, EntCliInt, Periodo: Integer;
                Progress: TProgressBar): Boolean;
    function  VerificaSeHouveEmissaoPosterior(QueryAux: TmySQLQuery;
                Database: TmySQLDatabase; TabAriA, TabPrvA, TabCnsA: String;
                EntCliInt, Periodo: Integer): Boolean;
    function  VerificaSeAlgumItemDoBoletoEstaPago(QueryBoletosIts, QueryAux: TmySQLQuery;
                Database: TmySQLDatabase; TabLctA: String): Boolean;
    function  ImprimeBloqueto(CkZerado, Escolher: Boolean; TabLctA, TabLctB,
                TabLctD: String; CliInt, EntCliInt: Integer; QueryPrev,
                QueryBoletos, QueryBoletosIts, QueryBloOpcoes: TmySQLQuery; frxDsPRI,
                frxDsBoletos, frxDsBoletosIts: TfrxDBDataset; Quais: TselType;
                Como: TfrxImpComo; Arquivo: String; Filtro: TfrxCustomExportFilter;
                Grade: TDBGrid): TfrxReport;
    function  VerificaSeOpcoesForamConfiguradas(Query: TmySQLQuery): Boolean;
    function  EmiteBoletoAvulso(TabLctA: String; Grade: TdmkDBGrid;
                QrLcts: TmySQLQuery; EmiteLotProtocoloSeComRegistro: Boolean;
                FatNum: Double; Tipo, Config: Integer; var LoteProtoCR: Integer): Boolean;
    function  VerificaSeBoletoExiste(Controle: Integer): Boolean;
    function  AtualizaArrecadacao(SQLType: TSQLType; Codigo, ControleArre, Conta,
                BloArre, BloArreIts, Entidade, CartEmiss, Tipo, Config, NFSeSrvCad,
                Lancto, Avulso, ComoAdd, FatID, FatParcela: Integer;
                Vencto: String; Valor, FatNum, Boleto: Double; Texto: String;
                var Mensagem: String): Integer;
    function  ArrecadacoesVencidas_e_AVencer(Hoje: TDateTime;
                MostraForm: Boolean): Integer;
    procedure MostraBloGeren(Aba, FatID, Lancamento: Integer; FatNum:
              Double; PageControl: TPageControl; Pager: TWinControl);
    procedure PreencheModelosBloq(RG: TRadioGroup);
    procedure ReopenPRI(Query: TmySQLQuery; Codigo, Controle: Integer; Tabela: String);
    procedure ReopenQrCons(Query, QuerySum: TmySQLQuery; Tabela: String; CliInt,
              Periodo, Codigo: Integer);
    procedure ReopenCNS(Query: TmySQLQuery; CliInt, Periodo, Codigo,
              Controle: Integer; Tabela: String);
    procedure ReopenArre(Query, QuerySum: TmySQLQuery; Codigo, Apto,
              Propriet: Integer; Tabela: String);
    procedure ReopenArreBol(Query: TmySQLQuery; Codigo, Apto, Propriet: Integer;
              Boleto: Double; Tabela: String);
    procedure ReopenARI(Query: TmySQLQuery; Codigo, Controle, Apto,
              Propriet: Integer; Boleto: Double; Tabela: String);
    procedure ReopenBoletos(Query, QryArr, QryLei: TmySQLQuery; BOLAPTO: String;
              PreBol: Boolean; TabelaPrvA, TabelaAriA, TabelaCnsA: String;
              CliInt, Periodo, Codigo: Integer);
    procedure ReopenBoletosIts(Query: TmySQLQuery; TabelaAriA, TabelaCnsA: String;
              Boleto: Double; Codigo, Periodo, Apto, CNAB_Cfg: Integer);
    procedure ReopenPrevModBol(Query: TmySQLQuery; Codigo, Apto: Integer);
    procedure ReopenQrSumBol(Query: TmySQLQuery; TabelaAriA, TabelaCnsA: String;
              CliInt, Periodo, Codigo: Integer; PreBol: Boolean);
    procedure ReopenQrSumCP(Query: TmySQLQuery; TabelaCnsA: String; CliInt,
              Periodo, Codigo: Integer);
    procedure ReopenMU6PM(CliInt, LastP, Genero: Integer; TabelaLctA,
              TabelaLctB, TabelaLctD: String; QueryMU6PM, QueryMU6PT: TmySQLQuery);
    procedure ReopenMU6MM(CliInt, LastP, Genero: Integer; TabelaLctA,
              TabelaLctB, TabelaLctD: String; QueryMU6MM, QueryMU6MT: TmySQLQuery);
    procedure ExcluiLeituraAtual(Controle: Integer; Lancto, Boleto: Double;
              TabCnsA, TabLctA: String);
    procedure ReopenArreFut(QueryArreFutI: TmySQLQuery; CliInt, MesesAnt,
              Controle: Integer);
    procedure ReopenCNAB_CfgSel(Query: TmySQLQuery; Cliente: Integer);
    procedure ExcluiItesArrecadacao(TabAriA, TabLctA: String; Controle,
              Lancto: Integer; Boleto: Double);
    procedure ExcluiItemPreBloqueto(Tipo: TselType; TabAriA, TabCnsA: String;
              QueryBoletosIts: TmySQLQuery; Grade: TDBGrid);
    procedure ReopenProt1(QueryProt1: TmySQLQuery; AbaPro, AbaGerados, EntCliInt,
                Cond, Codigo: Integer; TabPrvA, TabAriA, TabCnsA: String);
    procedure ReopenQuery(QueryOpc, QueryProtocoBol: TmySQLQuery; TipoProtocolo,
                Protocolo, Codigo, Cond, EntCliInt, AbaGerados: Integer; TabPrvA,
                TabAriA, TabCnsA: String);
    procedure ReopenBloOpcoes(Query: TmySQLQuery);
    //Migra��es
    procedure AtualizaCondEmeios(DataBase: TMySQLDataBase; PB1: TProgressBar);
    procedure ConverteControleToBloOpcoes(DataBase: TMySQLDataBase);
    procedure ConverteCondImovProtToProEnPr(DataBase: TMySQLDataBase; PB1: TProgressBar);
    procedure ConverteCondBoletosParaCNABcfg(DataBase: TMySQLDataBase; PB1: TProgressBar);
    procedure AtualizaDadosProtocolosNosBoletos(DataBase: TMySQLDataBase; PB1: TProgressBar);
    procedure AtualizaArreFut(DataBase: TMySQLDataBase; PB1: TProgressBar);
  end;

var
  UBloquetos: TUnBloquetos;

implementation

uses
{$IfNDef SemProtocolo}
  Protocolo,
{$EndIf}
  DmkDAC_PF, UnBloqGerl, UCreate, UnFinanceiro, UnMyObjects, MyDBCheck,
  UnBloquetos_Jan, BloImp;

{ TUnBloquetos }

function TUnBloquetos.GetPercAptAll(Controle, ListaBaU: Integer): Double;
var
  Ctrl: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    if ListaBau <> 0 then
      Ctrl := ListaBaU
    else
      Ctrl := Controle;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(Percent) TotPerc ',
      'FROM arrebau ',
      'WHERE Controle=' + Geral.FF0(Ctrl),
      '']);
    Result := Qry.FieldByName('TotPerc').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnBloquetos.VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts,
  QueryAux: TmySQLQuery; Database: TmySQLDatabase; TabLctA: String): Boolean;
var
  Lcts: String;
begin
  //Verifica se todos os lan�amentos est�o na tabela TabLctA caso n�o, n�o atualiza nenhum
  try
    Lcts := '';
    //
    QueryBoletosIts.First;
    //
    while not QueryBoletosIts.Eof do
    begin
      if QueryBoletosIts.RecNo = QueryBoletosIts.RecordCount then
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger)
      else
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger) + ', ';
      //
      QueryBoletosIts.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
      'SELECT Documento ',
      'FROM ' + TabLctA,
      'WHERE Controle IN (' + Lcts + ')',
      '']);
    if QueryBoletosIts.RecordCount = QueryAux.RecordCount then
      Result := True
    else
      Result := False;
  except
    Result := False;
  end;
end;

function TUnBloquetos.AlteraVencimentoPreBol(CliInt, Periodo, Depto: Integer;
  Prebol: Boolean; Tipo: TSelType; Grade: TDBGrid; QueryBoletos, QueryBoletosIts,
  QueryUpd, QueryAux: TmySQLQuery; Database: TmySQLDatabase; Progress: TProgressBar;
  TabLctA: String): Boolean;

  function AtualizaVenctoBoletosIts(QueryBoletosIts,
    QueryAux, QueryUpd: TmySQLQuery; Database: TmySQLDatabase; DataSel: TDateTime;
    Prebol: Boolean; TabLctA: String): Boolean;
  var
    TabUpd, Vencto: String;
    Controle, Lancto: Integer;
    Continua: Boolean;
  begin
    try
      Result := True;
      //
      if (Lancto <> 0) and (Prebol = False) and (TabLctA <> '') then
        Continua := VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts, QueryAux,
                      Database, TabLctA)
      else
        Continua := True;
      //
      if Continua then
      begin
        QueryBoletosIts.First;
        //
        while not QueryBoletosIts.Eof do
        begin
          TabUpd   := QueryBoletosIts.FieldByName('TabelaOrig').AsString;
          Controle := QueryBoletosIts.FieldByName('Controle').AsInteger;
          Vencto   := Geral.FDT(DataSel, 1);
          Lancto   := QueryBoletosIts.FieldByName('Lancto').AsInteger;
          //
          Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabUpd, False,
                      ['Vencto'], ['Controle'], [Vencto], [Controle], True);
          //
          if (Lancto <> 0) and (Prebol = False) and (TabLctA <> '') then
          begin
            Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabLctA, False,
                        ['Vencimento'], ['Controle'], [Vencto], [Lancto], True);
          end;
          //
          QueryBoletosIts.Next;
        end;
      end else
      begin
        Geral.MB_Aviso('Lan�amento(s) financeiro(s) n�o foram localizados para o boleto n�mero ' +
          FloatToStr(QueryAux.FieldByName('Documento').AsFloat) + sLineBreak +
          'Altera��o de vencimento cancelada!');
        Result := False;
      end;
    except
      Result := False;
    end;
  end;

var
  I, MesValido, DiaVencto: Integer;
  MesTxt: String;
  DataDef, DataSel: TDateTime;
  ValidouMes: Boolean;
begin
  Result := False;
  //
  if (QueryBoletos.State <> dsInactive) and (QueryBoletos.RecordCount > 0) then
  begin
    DiaVencto := ObtemDiaVenctoDeApto(CliInt, Depto);
    DataDef   := dmkPF.UltimoDiaDoPeriodo_Date(Periodo) + DiaVencto;
    //
    if not DBCheck.ObtemData(DataDef, DataSel, VAR_DATA_MINIMA) then Exit;
    //
    ValidouMes := UBloqGerl.ValidaVencimentoPeriodo(nil, nil, Periodo, DataSel, MesValido);
    MesTxt     := dmkPF.VerificaMes(MesValido, False);
    //
    //if MyObjects.FIC(ValidouMes, nil, 'O vencimento deve ser correspondente ao m�s ' + MesTxt + '!') then Exit;
    if ValidouMes then
      if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    if Geral.MB_Pergunta('Confirma a altera��o do vencimento para a data ' +
      Geral.FDT(DataSel, 2) + '?') = ID_YES then
    begin
      try
        Progress.Position := 0;
        Grade.Enabled     := False;
        //
        QueryBoletos.DisableControls;
        QueryBoletosIts.DisableControls;
        //
        case Tipo of
          istAtual:
          begin
            Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                        QueryUpd, Database, DataSel, Prebol, TabLctA);
          end;
          istSelecionados:
          begin
            if Grade.SelectedRows.Count > 0 then
            begin
              with Grade.DataSource.DataSet do
              for I := 0 to Grade.SelectedRows.Count - 1 do
              begin
                //GotoBookmark(pointer(Grade.SelectedRows.Items[I]));
                GotoBookmark(Grade.SelectedRows.Items[I]);
                //
                Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                            QueryUpd, Database, DataSel, Prebol, TabLctA);
              end;
            end else
              Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                          QueryUpd, Database, DataSel, Prebol, TabLctA);
          end;
          istTodos:
          begin
            Progress.Max := QueryBoletos.RecordCount;
            //
            QueryBoletos.First;
            //
            while not QueryBoletos.Eof do
            begin
              Result := AtualizaVenctoBoletosIts(QueryBoletosIts, QueryAux,
                          QueryUpd, Database, DataSel, Prebol, TabLctA);
              //
              Progress.Position := Progress.Position + 1;
              Progress.Update;
              Application.ProcessMessages;
              //
              QueryBoletos.Next;
            end;
          end
          else
          begin
            Geral.MB_Aviso('Tipo de sele��o n�o implementada!');
            Exit;
          end;
        end;
      finally
        Progress.Position := 0;
        Grade.Enabled     := True;
        //
        QueryBoletos.EnableControls;
        QueryBoletosIts.EnableControls;
      end;
    end;
  end;
end;

function TUnBloquetos.EmiteBoletoAvulso(TabLctA: String; Grade: TdmkDBGrid;
  QrLcts: TmySQLQuery; EmiteLotProtocoloSeComRegistro: Boolean; FatNum: Double;
  Tipo, Config: Integer; var LoteProtoCR: Integer): Boolean;
begin
  //Compatibilidade
end;

procedure TUnBloquetos.ExcluiItemPreBloqueto(Tipo: TselType; TabAriA, TabCnsA: String;
  QueryBoletosIts: TmySQLQuery; Grade: TDBGrid);

  procedure ExcluiLeituraAtual;
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      DELETE_FROM  + TabCnsA,
      'WHERE Controle=' + Geral.FF0(QueryBoletosIts.FieldByName('Controle').AsInteger),
      '']);
  end;

  procedure ExcluiArrecadacaoAtual;
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      DELETE_FROM  + TabAriA,
      'WHERE Controle=' + Geral.FF0(QueryBoletosIts.FieldByName('Controle').AsInteger),
      '']);
  end;

var
  i: integer;
begin
  i := 0;
  if Tipo <> istTodos then
  begin
    case Tipo of
      istAtual: i := 1;
      istSelecionados:
      begin
        i := Grade.SelectedRows.Count;
        if i = 0 then i := 1;
      end;
    end;
    if i = 1 then
    begin
      if Geral.MensagemBox('Confirma a exclus�o do item de pr�-bloqueto selecionado?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        if QueryBoletosIts.FieldByName('Tipo').AsInteger = 0 then
          ExcluiArrecadacaoAtual
        else
          ExcluiLeituraAtual;
      end;
    end else begin
      if Geral.MB_Pergunta('Confirma a exclus�o dos ' +
        Geral.FF0(Grade.SelectedRows.Count) +
        ' itens de pr�-bloquetos selecionados?') = ID_YES then
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          GotoBookmark(Grade.SelectedRows.Items[i]);
          if QueryBoletosIts.FieldByName('Tipo').AsInteger = 0 then
            ExcluiArrecadacaoAtual
          else
            ExcluiLeituraAtual;
        end;
      end;
    end;
  end else
  begin
    QueryBoletosIts.First;
    while not QueryBoletosIts.Eof do
    begin
      if QueryBoletosIts.FieldByName('Tipo').AsInteger = 0 then
        ExcluiArrecadacaoAtual
      else
        ExcluiLeituraAtual;
      QueryBoletosIts.Next;
    end;
  end;
end;

procedure TUnBloquetos.ExcluiItesArrecadacao(TabAriA, TabLctA: String; Controle,
  Lancto: Integer; Boleto: Double);
var
  QrLocLct: TmySQLQuery;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    DELETE_FROM  + TabAriA,
    'WHERE Controle=' + Geral.FF0(Controle) + ' AND Boleto=0',
    '']);
  if (Lancto <> 0) and (Boleto <> 0) then
  begin
    QrLocLct := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QrLocLct, Dmod.MyDB, [
        'SELECT Data, Tipo, Carteira, Sub ',
        'FROM ' + TabLctA + ' ',
        'WHERE Controle=' + Geral.FF0(Lancto),
        'AND FatID=' + Geral.FF0(VAR_FATID_0600), //600 FATID da Arrecada��o
        '']);
      if (Lancto <> 0) and (Boleto <> 0) then
      begin
        if QrLocLct.RecordCount > 0 then
        begin
          UFinanceiro.ExcluiLct_Unico(TabLctA, Dmod.MyDB,
            QrLocLct.FieldByName('Data').AsDateTime,
            QrLocLct.FieldByName('Tipo').AsInteger,
            QrLocLct.FieldByName('Carteira').AsInteger,
            Lancto, QrLocLct.FieldByName('Sub').AsInteger,
            dmkPF.MotivDel_ValidaCodigo(306), False, False);
        end;
      end;
    finally
      QrLocLct.Free;
    end;
  end;
end;

function TUnBloquetos.GetPercAptUni(Controle, ListaBaU, Conta: Integer;
  NomeConta: String): Double;
var
  Ctrl: Integer;
  UH: String;
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    if ListaBau <> 0 then
      Ctrl := ListaBaU
    else
      Ctrl := Controle;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Percent ',
      'FROM arrebau ',
      'WHERE Controle=' + Geral.FF0(Ctrl),
      'AND Apto=' + Geral.FF0(Conta),
      '']);
    // Deve encontrar apenas um registro
    if Qry.RecordCount = 1 then
      Result := Qry.FieldByName('Percent').AsInteger
    else begin
      UH := DModG.ObtemNomeDepto(Conta);
      //
      if Qry.RecordCount = 0 then
        Geral.MB_Aviso('N�o h� item cadastrado para a unidade ' + UH +
          ' para a arrecada��o "' + NomeConta + '"! ')
      else
        Geral.MB_Aviso('H� mais de um item cadastrados para a unidade ' + UH +
          ' para a arrecada��o "' + NomeConta + '"! ');
    end;
  finally
    Qry.Free;
  end;
end;

function TUnBloquetos.ImprimeBloqueto(CkZerado, Escolher: Boolean; TabLctA,
  TabLctB, TabLctD: String; CliInt, EntCliInt: Integer; QueryPrev, QueryBoletos,
  QueryBoletosIts, QueryBloOpcoes: TmySQLQuery; frxDsPRI, frxDsBoletos,
  frxDsBoletosIts: TfrxDBDataset; Quais: TselType; Como: TfrxImpComo;
  Arquivo: String; Filtro: TfrxCustomExportFilter; Grade: TDBGrid): TfrxReport;
begin
  Result := nil;
  //
  Screen.Cursor := crHourGlass;
  try
    ReopenBloOpcoes(QueryBloOpcoes);
    //
    Application.CreateForm(TFmBloImp, FmBloImp);
    //
    Result := FmBloImp.ImprimeBloq(CkZerado, Escolher, TabLctA, TabLctB, TabLctD,
                CliInt, EntCliInt, QueryPrev, QueryBoletos, QueryBoletosIts,
                QueryBloOpcoes, frxDsPRI, frxDsBoletos, frxDsBoletosIts, Quais,
                Como, Arquivo, Filtro, Grade);
    //
    FmBloImp.Destroy;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnBloquetos.LetraModelosBloq(Item: Integer): String;
begin
  case Item of
     1: Result := 'A';
     2: Result := 'B';
     3: Result := 'C';
     4: Result := 'D';
     5: Result := 'E';
     6: Result := 'G';
     7: Result := 'G';
     8: Result := 'H';
     9: Result := 'R';
    10: Result := 'IB';
    11: Result := 'IR';
    else Result := '?';
  end;
end;

procedure TUnBloquetos.MostraBloGeren(Aba, FatID, Lancamento: Integer; FatNum:
Double; PageControl: TPageControl; Pager: TWinControl);
begin
  //Compatibilidade
end;

function TUnBloquetos.ObtemDiaVenctoDeApto(CliInt, Depto: Integer): Integer;
var
  Qry: TMySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ddVctEsp ',
      'FROM condimov ',
      'WHERE Conta=' + Geral.FF0(Depto),
      'AND ddVctEsp <> 0 ',
      '']);
    if Qry.RecordCount = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT DiaVencto ',
        'FROM cond ',
        'WHERE Codigo=' + Geral.FF0(CliInt),
        '']);
      if Qry.RecordCount > 0 then
        Result := Qry.FieldByName('DiaVencto').AsInteger
      else
        Result := 0;
    end else
      Result := Qry.FieldByName('ddVctEsp').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnBloquetos.ObtemDocumProtPakIts(ProtPakIts: Integer): Double;
begin
  //Compatibilidade
  Result := 0;
end;

procedure TUnBloquetos.ReopenCNAB_CfgSel(Query: TmySQLQuery; Cliente: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM CNAB_Cfg ',
    'WHERE Cedente=' + Geral.FF0(Cliente),
    '']);
end;

procedure TUnBloquetos.PreencheModelosBloq(RG: TRadioGroup);
begin
  RG.Items.Clear;
  {00} RG.Items.Add('? - Definir no momento da impress�o');
  {01} RG.Items.Add('A - 3 colunas fonte tamanho 7 sem rodap� (3 n�veis)');
  {02} RG.Items.Add('B - 3 colunas fonte tamanho 6 sem rodap� (3 n�veis)');
  {03} RG.Items.Add('C - 3 colunas fonte tamanho 7 com rodap� (3 n�veis)');
  {04} RG.Items.Add('D - 2 colunas fonte tamanho 6 com rodap� (3 n�veis)');
  {05} RG.Items.Add('E - Somente bloqueto (nenhum n�vel)');
  {06} RG.Items.Add('F - Extinto (se selecionado ser� usado o modelo G)');
  {07} RG.Items.Add('G - 3 colunas fonte tamanho 7 com rodap� (4 n�veis)');
  {08} RG.Items.Add('H - Colunas, fonte e tamanho vari�vel (4 n�veis) - C�digo de barras');
  {09} RG.Items.Add('R - Colunas, fonte e tamanho vari�vel (4 n�veis) - Recibo');
  {10} RG.Items.Add('IB - 1 coluna, fonte e tamanho vari�vel (1 n�vel)');
  {11} RG.Items.Add('IR - 1 coluna, fonte e tamanho vari�vel (1 n�vel) - Recibo');
end;

procedure TUnBloquetos.ReopenCNS(Query: TmySQLQuery; CliInt, Periodo, Codigo,
  Controle: Integer; Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT cdb.Descri Bloco, cdi.Unidade, cnp.Casas, cnp.UnidLei, ',
    'IF(cni.CNAB_Cfg<>0, cna.Nome, fat.Nome) NOME_Cfg, ',
    'cnp.UnidImp, cnp.UnidFat, cnp.CasRat, cni.* ',
    'FROM cons cns ',
    'LEFT JOIN ' + Tabela + '  cni ON cni.Codigo=cns.Codigo ',
    'LEFT JOIN consprc  cnp ON cnp.Codigo=cns.Codigo ',
    'LEFT JOIN condimov cdi ON cdi.Conta=cni.Apto ',
    'LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle ',
    'LEFT JOIN cnab_cfg  cna ON cna.Codigo=cni.CNAB_Cfg ',
    'LEFT JOIN faturcfg  fat ON fat.Codigo=cni.Fatur_Cfg ',
    'WHERE cni.Cond=' + Geral.FF0(CliInt),
    'AND cnp.Cond=' + Geral.FF0(CliInt),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    'AND cni.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY cdb.Ordem, cdi.Andar, cdi.Unidade ',
    '']);
  if Controle <> 0 then
    Query.Locate('Controle', Controle, []);
end;

procedure TUnBloquetos.ReopenMU6MM(CliInt, LastP, Genero: Integer; TabelaLctA,
  TabelaLctB, TabelaLctD: String; QueryMU6MM, QueryMU6MT: TmySQLQuery);

  procedure GeraParteSQL_MU6MM(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QueryMU6MM.SQL.Add('SELECT ((YEAR(lan.Data)-2000) * 100) + MONTH(lan.Data) MEZ,');
    QueryMU6MM.SQL.Add('SUM(lan.Debito) SUM_DEB');
    QueryMU6MM.SQL.Add('FROM ' + TabLct + ' lan');
    QueryMU6MM.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QueryMU6MM.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QueryMU6MM.SQL.Add('AND car.Tipo<>2');
    QueryMU6MM.SQL.Add('AND lan.Mez=0');
    QueryMU6MM.SQL.Add('AND lan.Debito>0');
    QueryMU6MM.SQL.Add('AND lan.Genero=' + GenTXT);
    QueryMU6MM.SQL.Add('AND ((YEAR(lan.Data)-2000) * 100) + ');
    QueryMU6MM.SQL.Add('MONTH(lan.Data) BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
    QueryMU6MM.SQL.Add('GROUP BY ((YEAR(lan.Data)-2000) * 100) + MONTH(lan.Data)');
  end;

  procedure GeraParteSQL_MU6MT(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QueryMU6MT.SQL.Add('SELECT SUM(lan.Debito) SUM_DEB');
    QueryMU6MT.SQL.Add('FROM ' + TabLct + ' lan');
    QueryMU6MT.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QueryMU6MT.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QueryMU6MT.SQL.Add('AND car.Tipo<>2');
    QueryMU6MT.SQL.Add('AND lan.Debito>0');
    QueryMU6MT.SQL.Add('AND lan.Mez=0');
    QueryMU6MT.SQL.Add('AND lan.Genero=' + GenTXT);
    QueryMU6MT.SQL.Add('AND ((YEAR(lan.Data)-2000) * 100) +');
    QueryMU6MT.SQL.Add('MONTH(lan.Data) BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
  end;

var
  MU6MM, MU6MT, Mez1, Mez6, CliTXT, GenTXT: String;
begin
  Mez1   := Geral.FF0(dmkPF.PeriodoToAnoMes(LastP-1));
  Mez6   := Geral.FF0(dmkPF.PeriodoToAnoMes(LastP-6));
  CliTXT := Geral.FF0(CliInt);
  GenTXT := Geral.FF0(Genero);
  //
  QueryMU6MM.Close;
  QueryMU6MM.Database := DModG.MyPID_DB;
  MU6MM               := UCriar.RecriaTempTableNovo(ntrttMU6MM, DmodG.QrUpdPID1, False);
  QueryMU6MM.SQL.Clear;
  //
  QueryMU6MM.SQL.Add('INSERT INTO ' + MU6MM);
  QueryMU6MM.SQL.Add('');
  GeraParteSQL_MU6MM(TabelaLctA, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MM.SQL.Add('UNION');
  GeraParteSQL_MU6MM(TabelaLctB, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MM.SQL.Add('UNION');
  GeraParteSQL_MU6MM(TabelaLctD, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MM.SQL.Add(';');
  QueryMU6MM.SQL.Add('');
  QueryMU6MM.SQL.Add('SELECT MEZ, SUM(SUM_DEB) SUM_DEB ');
  QueryMU6MM.SQL.Add('FROM ' + MU6MM);
  QueryMU6MM.SQL.Add('GROUP BY MEZ ');
  QueryMU6MM.SQL.Add('ORDER BY MEZ;');
  QueryMU6MM.SQL.Add('');
  UMyMod.AbreQuery(QueryMU6MM, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  //
  QueryMU6MT.Close;
  QueryMU6MT.Database := DModG.MyPID_DB;
  MU6MT               := UCriar.RecriaTempTableNovo(ntrttMU6MT, DmodG.QrUpdPID1, False);
  QueryMU6MT.SQL.Clear;
  //
  QueryMU6MT.SQL.Add('INSERT INTO ' + MU6MT);
  QueryMU6MT.SQL.Add('');
  GeraParteSQL_MU6MT(TabelaLctA, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MT.SQL.Add('UNION');
  GeraParteSQL_MU6MT(TabelaLctB, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MT.SQL.Add('UNION');
  GeraParteSQL_MU6MT(TabelaLctD, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6MT.SQL.Add(';');
  QueryMU6MT.SQL.Add('');
  QueryMU6MT.SQL.Add('SELECT SUM(SUM_DEB) SUM_DEB ');
  QueryMU6MT.SQL.Add('FROM ' + MU6MT + ';');
  QueryMU6MT.SQL.Add('');
  UMyMod.AbreQuery(QueryMU6MT, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
end;

procedure TUnBloquetos.ReopenMU6PM(CliInt, LastP, Genero: Integer; TabelaLctA,
  TabelaLctB, TabelaLctD: String; QueryMU6PM, QueryMU6PT: TmySQLQuery);

  procedure GeraParteSQL_MU6PM(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QueryMU6PM.SQL.Add('SELECT lan.Mez, SUM(lan.Debito) SUM_DEB');
    QueryMU6PM.SQL.Add('FROM ' + TabLct + ' lan');
    QueryMU6PM.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QueryMU6PM.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QueryMU6PM.SQL.Add('AND car.Tipo<>2');
    QueryMU6PM.SQL.Add('AND lan.Debito>0');
    QueryMU6PM.SQL.Add('AND lan.Genero=' + GenTXT);
    QueryMU6PM.SQL.Add('AND lan.Mez BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
    QueryMU6PM.SQL.Add('GROUP BY lan.Mez');
  end;

  procedure GeraParteSQL_MU6PT(TabLct, Mez1, Mez6, CliTXT, GenTXT: String);
  begin
    QueryMU6PT.SQL.Add('SELECT SUM(lan.Debito) SUM_DEB');
    QueryMU6PT.SQL.Add('FROM ' + TabLct + ' lan');
    QueryMU6PT.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QueryMU6PT.SQL.Add('WHERE car.ForneceI=' + CliTXT);
    QueryMU6PT.SQL.Add('AND car.Tipo<>2');
    QueryMU6PT.SQL.Add('AND lan.Debito>0');
    QueryMU6PT.SQL.Add('AND lan.Genero=' + GenTXT);
    QueryMU6PT.SQL.Add('AND lan.Mez BETWEEN "' + Mez6 + '" AND "' + Mez1 + '"');
  end;

var
  Mez1, Mez6, CliTXT, GenTxt: String;
begin
  Mez1   := Geral.FF0(dmkPF.PeriodoToAnoMes(LastP-1));
  Mez6   := Geral.FF0(dmkPF.PeriodoToAnoMes(LastP-6));
  CliTXT := Geral.FF0(CliInt);
  GenTXT := Geral.FF0(Genero);
  //
  QueryMU6PM.Close;
  QueryMU6PM.Database := DModG.MyPID_DB;
  QueryMU6PM.SQL.Clear;
  //
  QueryMU6PM.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_MU6PM_;');
  QueryMU6PM.SQL.Add('CREATE TABLE _MOD_COND_MU6PM_');
  QueryMU6PM.SQL.Add('');
  GeraParteSQL_MU6PM(TabelaLctA, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PM.SQL.Add('UNION');
  GeraParteSQL_MU6PM(TabelaLctB, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PM.SQL.Add('UNION');
  GeraParteSQL_MU6PM(TabelaLctD, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PM.SQL.Add(';');
  QueryMU6PM.SQL.Add('');
  QueryMU6PM.SQL.Add('SELECT Mez, SUM(SUM_DEB) SUM_DEB ');
  QueryMU6PM.SQL.Add('FROM _MOD_COND_MU6PM_');
  QueryMU6PM.SQL.Add('GROUP BY Mez');
  QueryMU6PM.SQL.Add('ORDER BY Mez DESC');
  QueryMU6PM.SQL.Add('');
  UMyMod.AbreQuery(QueryMU6PM, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  //
  //
  QueryMU6PT.Close;
  QueryMU6PT.Database := DModG.MyPID_DB;
  QueryMU6PT.SQL.Clear;
  //
  QueryMU6PT.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_MU6PT_;');
  QueryMU6PT.SQL.Add('CREATE TABLE _MOD_COND_MU6PT_');
  QueryMU6PT.SQL.Add('');
  GeraParteSQL_MU6PT(TabelaLctA, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PT.SQL.Add('UNION');
  GeraParteSQL_MU6PT(TabelaLctB, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PT.SQL.Add('UNION');
  GeraParteSQL_MU6PT(TabelaLctD, Mez1, Mez6, CliTXT, GenTXT);
  QueryMU6PT.SQL.Add(';');
  QueryMU6PT.SQL.Add('');
  QueryMU6PT.SQL.Add('SELECT SUM(SUM_DEB) SUM_DEB ');
  QueryMU6PT.SQL.Add('FROM _MOD_COND_MU6PT_;');
  QueryMU6PT.SQL.Add('');
  UMyMod.AbreQuery(QueryMU6PT, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
end;

procedure TUnBloquetos.ReopenPRI(Query: TmySQLQuery; Codigo, Controle: Integer;
  Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT pit.*, con.SubGrupo, ',
    'con.Codigo CODCONTA, sgo.Codigo CODSUBGRUPO, ',
    'con.Nome NOMECONTA, sgo.Nome NOMESUBGRUPO ',
    'FROM ' + Tabela + ' pit ',
    'LEFT JOIN contas con ON con.Codigo=pit.Conta ',
    'LEFT JOIN subgrupos sgo ON sgo.Codigo=con.SubGrupo ',
    'WHERE pit.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY NOMESUBGRUPO, NOMECONTA ',
    '']);
  if Controle <> 0 then
    Query.Locate('Controle', Controle, []);
end;

procedure TUnBloquetos.ReopenProt1(QueryProt1: TmySQLQuery; AbaPro, AbaGerados,
  EntCliInt, Cond, Codigo: Integer; TabPrvA, TabAriA, TabCnsA: String);
{$IfNDef SemProtocolo}
var
  ProtTipo: Integer;
begin
  case AbaPro of
    0: //Documentos
      ProtTipo := 1;
    1: //E-mail
      ProtTipo := 2;
    2: //Cobran�a com registro
      ProtTipo := 4;
    else
      ProtTipo := -1;
  end;
  //
  if AbaPro = 2 then //Cobran�a com registro
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QueryProt1, Dmod.MyDB, [
      'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
      'FROM ' + TabAriA + ' ari ',
      'LEFT JOIN ' + TabPrvA + ' pre ON pre.Codigo = ari.Codigo ',
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = ari.CNAB_cfg ',
      'LEFT JOIN protocolos pro ON pro.Codigo = cfg.ProtocolCR ',
      'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
      'WHERE pre.Codigo=' + Geral.FF0(Codigo),
      'AND pre.Cond=' + Geral.FF0(Cond),
      'AND pro.Tipo=' + Geral.FF0(ProtTipo),
      'AND pro.def_client=' + Geral.FF0(EntCliInt),
      'GROUP BY pro.Codigo ',
      ' ',
      'UNION ',
      ' ',
      'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
      'FROM ' + TabCnsA + ' cns ',
      'LEFT JOIN ' + TabPrvA + ' pre ON pre.Periodo = cns.Periodo ',
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = cns.CNAB_cfg ',
      'LEFT JOIN protocolos pro ON pro.Codigo = cfg.ProtocolCR ',
      'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
      'WHERE pre.Codigo=' + Geral.FF0(Codigo),
      'AND pre.Cond=' + Geral.FF0(Cond),
      'AND pro.Tipo=' + Geral.FF0(ProtTipo),
      'AND pro.def_client=' + Geral.FF0(EntCliInt),
      'GROUP BY pro.Codigo ',
      'ORDER BY Nome ',
      '']);
  end else
  begin
    if AbaGerados = 1 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QueryProt1, Dmod.MyDB, [
        'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
        'FROM ' + TabAriA + ' ari ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Codigo=ari.Codigo ',
        'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
        'LEFT JOIN protpakits ppi ON (ppi.Docum=ari.Boleto AND ppi.Depto=ari.Apto ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti AND ppi.ID_Cod4=ari.CNAB_Cfg) ',
        'LEFT JOIN protocolos pro ON pro.Codigo=ppi.Codigo ',
        'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND pro.Tipo=' + Geral.FF0(ProtTipo),
        'AND pro.def_client=' + Geral.FF0(EntCliInt),
        'GROUP BY ppi.Codigo ',
        ' ',
        'UNION ',
        ' ',
        'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
        'FROM ' + TabCnsA + ' cns ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Periodo=cns.Periodo ',
        'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
        'LEFT JOIN protpakits ppi ON (ppi.Docum=cns.Boleto AND ppi.Depto=cns.Apto ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti AND ppi.ID_Cod4=cns.CNAB_Cfg) ',
        'LEFT JOIN protocolos pro ON pro.Codigo=ppi.Codigo ',
        'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND pro.Tipo=' + Geral.FF0(ProtTipo),
        'AND pro.def_client=' + Geral.FF0(EntCliInt),
        'GROUP BY ppi.Codigo ',
        ' ',
        'ORDER BY Nome ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QueryProt1, Dmod.MyDB, [
        'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
        'FROM ' + TabAriA + ' ari ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Codigo = ari.Codigo ',
        'LEFT JOIN proenprit ppi ON ppi.Depto = ari.Apto ',
        'LEFT JOIN proenpr ppr ON ppr.Codigo = ppi.Codigo ',
        'LEFT JOIN protocolos pro ON pro.Codigo = ppr.Protocolo ',
        'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND pro.Tipo=' + Geral.FF0(ProtTipo),
        'AND pro.def_client=' + Geral.FF0(EntCliInt),
        'AND ppr.Finalidade = 0 ',
        'GROUP BY pro.Codigo ',
        ' ',
        'UNION ',
        ' ',
        'SELECT pro.Codigo, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
        'FROM ' + TabCnsA + ' cns ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Periodo = cns.Periodo ',
        'LEFT JOIN proenprit ppi ON ppi.Depto = cns.Apto ',
        'LEFT JOIN proenpr ppr ON ppr.Codigo = ppi.Codigo ',
        'LEFT JOIN protocolos pro ON pro.Codigo = ppr.Protocolo ',
        'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND pro.Tipo=' + Geral.FF0(ProtTipo),
        'AND pro.def_client=' + Geral.FF0(EntCliInt),
        'AND ppr.Finalidade = 0 ',
        'GROUP BY pro.Codigo ',
        ' ',
        'ORDER BY Nome ',
        '']);
    end;
  end;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TUnBloquetos.ReopenQrCons(Query, QuerySum: TmySQLQuery; Tabela: String;
  CliInt, Periodo, Codigo: Integer);
var
  Cond_Txt, Peri_Txt:String;
begin
  Cond_Txt := Geral.FF0(CliInt);
  Peri_Txt := Geral.FF0(Periodo);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QuerySum, Dmod.MyDB, [
    'SELECT SUM(Consumo) CONSUMO, SUM(Valor) VALOR ',
    'FROM ' + Tabela + ' cni ',
    'WHERE cni.Cond=' + Cond_Txt,
    'AND cni.Periodo=' + Peri_Txt,
    '']);
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT DISTINCT cns.Codigo, cns.Nome, cnp.Preco, ',
    'cnp.Casas, cnp.UnidLei, cnp.UnidImp, cnp.UnidFat, ',
    'cnp.DifCaren, cnp.Carencia ',
    'FROM consprc cnp ',
    'LEFT JOIN cons    cns ON cns.Codigo=cnp.Codigo ',
    'LEFT JOIN ' + Tabela + ' cni ON cni.Codigo=cns.Codigo ',
    'WHERE cni.Cond=' + Cond_Txt,
    'AND cnp.Cond=' + Cond_Txt,
    'AND cni.Periodo=' + Peri_Txt,
    '']);
  if Codigo <> 0 then
    Query.Locate('Codigo', Codigo, []);
  //
end;

function TUnBloquetos.TextoExplicativoItemBoleto(EhConsumo, Casas: Integer;
  MedAnt, MedAtu, Consumo, UnidFat: Double; UnidLei, UnidImp: String; GeraTyp,
  CasRat, NaoImpLei: Integer; GeraFat: Double): String;
var
  Leitura: String;
begin
  if EhConsumo = 0 then Result := '' else
  begin
    if GeraTyp = 0 then
      Result := ' (' + Geral.FFT(MedAnt, Casas, siNegativo) + ' ' +
      ' > ' + Geral.FFT(MedAtu, Casas, siNegativo) + ' ' +
      ' = ' + Geral.FFT(Consumo, Casas, siNegativo) + ' ' +UnidLei +
      ' ~ ' + Geral.FFT(Consumo * UnidFat, Casas, siNegativo) +
      ' ' + UnidImp + ')'
    else begin
      if NaoImpLei = 1 then Leitura := '' else Leitura := ' ~ ' +
        Geral.FFT(Consumo * UnidFat, Casas, siNegativo) + ' ' + UnidLei;
      Result := ' (' + UnidImp + ': ' + Geral.FFT(GeraFat, CasRat, siNegativo) +
      Leitura + ')';
    end;
  end;
end;

function TUnBloquetos.VerificaSeLctEstaoNaTabLctA(TabLctA: String;
  Lancto: Integer; QueryAux: TmySQLQuery; var Msg: String): Boolean;
begin
  //True  = Est� na tabela LctA
  //False = N�o est� na tabela LctA
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + TabLctA,
    'WHERE Controle=' + Geral.FF0(Lancto),
    '']);
  if QueryAux.RecordCount > 0 then
  begin
    Msg    := '';
    Result := True;
  end else
  begin
    Msg    := 'N�o foi poss�vel localizar o lan�amento financeiro!';
    Result := False;
  end;
end;

function TUnBloquetos.VerificaSeAlgumItemDoBoletoEstaPago(QueryBoletosIts,
  QueryAux: TmySQLQuery; Database: TmySQLDatabase; TabLctA: String): Boolean;
var
  Lcts: String;
begin
  //Verifica se todos os lan�amentos est�o em aberto
  //True  = Pago
  //False = Em aberto
  //
  try
    Lcts := '';
    //
    QueryBoletosIts.First;
    //
    while not QueryBoletosIts.Eof do
    begin
      if QueryBoletosIts.RecNo = QueryBoletosIts.RecordCount then
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger)
      else
        Lcts := Lcts + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger) + ', ';
      //
      QueryBoletosIts.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
      'SELECT * ',
      'FROM ' + TabLctA,
      'WHERE Controle IN (' + Lcts + ')',
      'AND Sit < 2 ',
      'AND Pago = 0 ',
      '']);
    if QueryAux.RecordCount > 0 then
    begin
      if QueryBoletosIts.RecordCount = QueryAux.RecordCount then
        Result := False
      else
        Result := True;
    end else
      Result := True;
  except
    Result := True;
  end;
end;

function TUnBloquetos.VerificaSeBoletoExiste(Controle: Integer): Boolean;
begin
  //Compatibilidade
end;

function TUnBloquetos.VerificaSeHouveEmissaoPosterior(QueryAux: TmySQLQuery;
  Database: TmySQLDatabase; TabAriA, TabPrvA, TabCnsA: String; EntCliInt,
  Periodo: Integer): Boolean;
begin
  //True  = Houve emiss�o posterior
  //False = N�o houve emiss�o posterior
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
    'SELECT MAX(cns.Boleto) Boleto ',
    'FROM ' + TabCnsA + ' cns ',
    'LEFT JOIN ' + TabPrvA + ' prv ON  prv.Periodo=cns.Periodo ',
    'LEFT JOIN enticliint eci ON eci.CodCliInt=prv.Cond ',
    'WHERE eci.CodEnti=' + Geral.FF0(EntCliInt),
    'AND cns.Periodo > ' + Geral.FF0(Periodo),
    ' ',
    'UNION ',
    ' ',
    'SELECT MAX(ari.Boleto) Boleto ',
    'FROM ' + TabAriA + ' ari ',
    'LEFT JOIN ' + TabPrvA + ' prv ON  prv.Codigo=ari.Codigo ',
    'LEFT JOIN enticliint eci ON eci.CodCliInt=prv.Cond ',
    'WHERE eci.CodEnti= ' + Geral.FF0(EntCliInt),
    'AND prv.Periodo > ' + Geral.FF0(Periodo),
    '',
    'ORDER BY Boleto DESC ',
    '']);
  //
  if QueryAux.FieldByName('Boleto').AsFloat > 0 then
    Result := True
  else
    Result := False;
end;

function TUnBloquetos.VerificaSeLancamentoEstaPago(Lancto: Integer;
  QueryAux: TmySQLQuery; TabLctA: String;
  var Msg: String): Boolean;
begin
  //True  = N�o possui pagamentos
  //False = Possui pagamentos
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabLctA,
    'WHERE Controle=' + Geral.FF0(Lancto),
    'AND Sit < 2 ',
    'AND Pago = 0 ',
    '']);
  if QueryAux.RecordCount > 0 then
  begin
    Msg    := 'O lan�amento financeiro possui pagamentos nele!';
    Result := False;
  end else
  begin
    Msg    := '';
    Result := True;
  end;
end;

procedure TUnBloquetos.ReopenArre(Query, QuerySum: TmySQLQuery; Codigo, Apto,
  Propriet: Integer; Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT cdi.Andar, ari.Propriet, ari.Apto, SUM(ari.Valor) Valor, ',
    'cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ',
    'ent.Nome END NOMEPROPRIET, ari.ArreBaC, ari.ArreBaI ',
    'FROM ' + Tabela + ' ari ',
    'LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet ',
    'LEFT JOIN condimov  cdi ON cdi.Conta=ari.Apto ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    'GROUP BY Propriet, Apto ',
    'ORDER BY cdi.Andar, Unidade, NOMEPROPRIET ',
    '']);
  if (Apto <> 0) and (Propriet <> 0) then
    Query.Locate('Apto;Propriet', VarArrayOf([Apto, Propriet]), []);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QuerySum, Dmod.MyDB, [
    'SELECT SUM(ari.Valor) Valor ',
    'FROM ' + Tabela + ' ari ',
    'LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet ',
    'LEFT JOIN condimov  cdi ON cdi.Conta=ari.Apto ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    '']);
end;

procedure TUnBloquetos.ReopenArreBol(Query: TmySQLQuery; Codigo, Apto,
  Propriet: Integer; Boleto: Double; Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT ari.Boleto ',
    'FROM ' + Tabela + ' ari ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    'AND ari.Apto=' + Geral.FF0(Apto),
    'AND ari.Propriet=' + Geral.FF0(Propriet),
    'GROUP BY ari.Propriet, ari.Apto, ari.Boleto ',
    'ORDER BY ari.Boleto ',
    '']);
  if Boleto <> 0 then
    Query.Locate('Boleto', Boleto, []);
end;

function TUnBloquetos.RecalculaArrecadacoes(TabelaAriA, TabelaLctA: String;
  Codigo, CliInt: Integer; Gastos: Double): Boolean;
var
  Qry, Qry2, QryUpd, QryUpd2: TmySQLQuery;
  Valor: Double;
begin
  Screen.Cursor := crHourGlass;
  //
  Result  := False;
  Qry     := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  Qry2    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QryUpd  := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QryUpd2 := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DISTINCT abi.Codigo, abi.Percent, ari.Lancto ',
      'FROM ' + TabelaAriA + ' ari',
      'LEFT JOIN arrebai abi ON abi.Codigo=ari.ArreBaC',
      'WHERE abi.Fator=1 ',
      'AND ari.Codigo=' + Geral.FF0(Codigo),
      '']);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
      'SELECT cdi.Conta, cdi.Unidade, cdi.Propriet, CASE WHEN ',
      'ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEPROPRIET, ',
      'FracaoIdeal, Moradores ',
      'FROM condimov cdi ',
      'LEFT JOIN entidades ent ON ent.Codigo=cdi.Propriet ',
      'WHERE cdi.SitImv=1 ',
      'AND cdi.Codigo=' + Geral.FF0(CliInt),
      '']);
    if (Qry.RecordCount > 0) and (Qry2.RecordCount > 0) then
    begin
      if Geral.MB_Pergunta('Houve altera��es de provis�es, ap�s ' +
        'adi��o de arrecada��es. Deseja atualizar as arrecada��es que s�o ' +
        'calculadas pelo valor das provis�es?' + sLineBreak +
        'ATEN��O: Este procedimento pode demorar alguns minutos!') = ID_YES then
      begin
        while not Qry.Eof do
        begin
          Valor := Int((Gastos * Qry.FieldByName('Percent').AsFloat / Qry2.RecordCount) + 0.005) / 100;
          //
          UMyMod.SQLInsUpd(QryUpd, stUpd, TabelaAriA, False,
            ['Valor'], ['Codigo', 'ArreBaC'],
            [Valor], [Codigo, Qry.FieldByName('Codigo').AsInteger], True);
          //
          UMyMod.SQLInsUpd(QryUpd2, stUpd, TabelaLctA, False,
            ['Credito'], ['Controle'], [Valor], [Qry.FieldByName('Lancto').AsInteger], True);
          //
          Qry.Next;
        end;
      end;
    end;
    Result := True;
  finally
    Qry.Free;
    Qry2.Free;
    QryUpd.Free;
    QryUpd2.Free;
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnBloquetos.ReopenARI(Query: TmySQLQuery; Codigo, Controle, Apto,
  Propriet: Integer; Boleto: Double; Tabela: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT ',
    {$IfNDef sNFSe}
    'nsc.Nome NFSeSrvCad_TXT, ',
    {$Else}
    '"" NFSeSrvCad_TXT, ',
    {$EndIf}
    'IF(ari.CNAB_Cfg<>0, cna.Nome, fat.Nome) NOME_Cfg, ',
    'ari.* ',
    'FROM ' + Tabela + ' ari ',
    {$IfNDef sNFSe}
    'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo = ari.NFSeSrvCad ',
    {$EndIf}
    'LEFT JOIN cnab_cfg  cna ON cna.Codigo=ari.CNAB_Cfg ',
    'LEFT JOIN faturcfg  fat ON fat.Codigo=ari.Fatur_Cfg ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    'AND ari.Apto=' + Geral.FF0(Apto),
    'AND ari.Propriet=' + Geral.FF0(Propriet),
    'AND ari.Boleto=' + Geral.FFI(Boleto),
    'ORDER BY Valor DESC ',
    '']);
  if Controle <> 0 then
    Query.Locate('Controle', Controle, []);
end;

procedure TUnBloquetos.ReopenBloOpcoes(Query: TmySQLQuery);
begin
  UMyMod.AbreQuery(Query, Dmod.MyDB);
end;

procedure TUnBloquetos.ReopenBoletos(Query, QryArr, QryLei: TmySQLQuery;
  BOLAPTO: String; PreBol: Boolean; TabelaPrvA, TabelaAriA, TabelaCnsA: String;
  CliInt, Periodo, Codigo: Integer);
var
  TipoBol: String;
begin
  if PreBol then
    TipoBol := '='
  else
    TipoBol := '<>';
  //
  // Arrecada��es
  if QryArr <> nil then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QryArr, Dmod.MyDB, [
      'SELECT ari.Boleto, ari.Apto, SUM(ari.Valor) Valor, ',
      'CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO ',
      'FROM ' + TabelaAriA + ' ari ',
      'WHERE ari.Codigo=' + Geral.FF0(Codigo),
      'GROUP BY ari.Boleto, ari.Apto ',
      '']);
  end;
  // Leituras
  if QryLei <> nil then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QryLei, Dmod.MyDB, [
      'SELECT cni.Boleto, cni.Apto, SUM(cni.Valor) Valor, ',
      'CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO ',
      'FROM ' + TabelaCnsA + ' cni ',
      'WHERE cni.Cond=' + Geral.FF0(CliInt),
      'AND cni.Periodo=' + Geral.FF0(Periodo),
      'GROUP BY cni.Boleto, cni.Apto ',
      '']);
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT prv.Codigo, eci.CodEnti Empresa, cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, ',
    'cdi.Juridico, ari.Boleto, ari.Apto, ',
    'ari.Propriet, ari.Vencto, cdi.Unidade, ',
    'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
    'ELSE ent.Nome END NOMEPROPRIET, ',
    'CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO, ',
    'cdb.IDExporta cdb_IDExpota, cdi.IDExporta cdi_IDExpota,',
    '0 KGT, 1 Ativo, ari.CNAB_Cfg, ari.Fatur_Cfg, cdi.Conta, ',
    'prv.ModelBloq, prv.ConfigBol, prv.Compe, prv.BalAgrMens, cfb.Colunas, ',
    'IF(ari.Vencto <= "1900-01-01", "", DATE_FORMAT(ari.Vencto , "%d/%m/%Y")) Vencto_TXT, ',
    '( ',
    'SELECT COUNT(DISTINCT ppi.Codigo) + 0.000 ',
    'FROM proenprit ppi ',
    'LEFT JOIN proenpr per ON per.Codigo = ppi.Codigo ',
    'WHERE ppi.Depto = ari.Apto ',
    'AND (per.Finalidade = 0 OR per.Finalidade IS NULL) ',
    ') Envios, "" USERNAME, "" PASSWORD, ', //Provis�rio
    'IF(ari.CNAB_Cfg<>0, cna.Nome, fcf.Nome) NOME_Cfg, ',
    'IF(ari.CNAB_Cfg<>0, 0, 1) + 0.000 Tipo, ',
    'IF(ari.CNAB_Cfg<>0, ari.CNAB_Cfg, ari.Fatur_Cfg) + 0.000 Config ',
    'FROM ' + TabelaAriA + ' ari',
    'LEFT JOIN ' + TabelaPrvA + ' prv ON prv.Codigo=ari.Codigo ',
    'LEFT JOIN enticliint eci ON eci.CodCliInt=prv.Cond ',
    'LEFT JOIN configbol cfb ON cfb.Codigo=prv.ConfigBol ',
    'LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto ',
    'LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle ',
    'LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet ',
    'LEFT JOIN cnab_cfg cna ON cna.Codigo = ari.CNAB_Cfg ',
    'LEFT JOIN faturcfg fcf ON fcf.Codigo = ari.Fatur_Cfg ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    'AND ari.Boleto ' + TipoBol + ' 0 ',
    'GROUP BY ari.Apto, Tipo, Config, ari.Boleto, ari.Vencto ',
    ' ',
    'UNION ',
    ' ',
    'SELECT prv.Codigo, eci.CodEnti Empresa, cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, ',
    'cdi.Juridico, cni.Boleto, cni.Apto, ',
    'cni.Propriet, cni.Vencto, cdi.Unidade, ',
    'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
    'ELSE ent.Nome END NOMEPROPRIET, ',
    'CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO, ',
    'cdb.IDExporta cdb_IDExpota, cdi.IDExporta cdi_IDExpota, ',
    '0 KGT, 1 Ativo, cni.CNAB_Cfg, cni.Fatur_Cfg, cdi.Conta, ',
    'prv.ModelBloq, prv.ConfigBol, prv.Compe, prv.BalAgrMens, cfb.Colunas, ',
    'IF(cni.Vencto <= "1900-01-01", "", DATE_FORMAT(cni.Vencto , "%d/%m/%Y")) Vencto_TXT, ',
    '( ',
    'SELECT COUNT(DISTINCT ppi.Codigo) + 0.000 ',
    'FROM proenprit ppi ',
    'LEFT JOIN proenpr per ON per.Codigo = ppi.Codigo ',
    'WHERE ppi.Depto = cni.Apto ',
    'AND (per.Finalidade = 0 OR per.Finalidade IS NULL) ',
    ') Envios, "" USERNAME, "" PASSWORD, ', //Provis�rio
    'IF(cni.CNAB_Cfg<>0, cna.Nome, fcf.Nome) NOME_Cfg, ',
    'IF(cni.CNAB_Cfg<>0, 0, 1) + 0.000 Tipo, ',
    'IF(cni.CNAB_Cfg<>0, cni.CNAB_Cfg, cni.Fatur_Cfg) + 0.000 Config ',
    'FROM ' + TabelaCnsA + ' cni ',
    'LEFT JOIN ' + TabelaPrvA + ' prv ON prv.Periodo=cni.Periodo ',
    'LEFT JOIN enticliint eci ON eci.CodCliInt=prv.Cond ',
    'LEFT JOIN configbol cfb ON cfb.Codigo=prv.ConfigBol ',
    'LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto ',
    'LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle ',
    'LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet ',
    'LEFT JOIN cnab_cfg cna ON cna.Codigo = cni.CNAB_Cfg ',
    'LEFT JOIN faturcfg fcf ON fcf.Codigo = cni.Fatur_Cfg ',
    'WHERE cni.Cond=' + Geral.FF0(CliInt),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    'AND cni.Boleto ' + TipoBol + ' 0 ',
    'GROUP BY cni.Apto, Tipo, Config, cni.Boleto, cni.Vencto ',
    ' ',
    'ORDER BY Ordem, Andar, Unidade, Boleto ',
    '']);
  if BOLAPTO <> '' then
    Query.Locate('BOLAPTO', BOLAPTO, [])
end;

procedure TUnBloquetos.ReopenBoletosIts(Query: TmySQLQuery; TabelaAriA,
  TabelaCnsA: String; Boleto: Double; Codigo, Periodo, Apto, CNAB_Cfg: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT ari.Texto TEXTO, ari.Valor VALOR, ari.Vencto, ',
    '0 Tipo, 0 MedAnt, 0 MedAtu, 0 Consumo, 0 Casas, ',
    '"" UnidLei, "" UnidImp, 1 UnidFat, Controle, Lancto, ',
    '0 GeraTyp, 0 GeraFat, 0 CasRat, 0 NaoImpLei, ',
    '"' + TabelaAriA + '" TabelaOrig, ari.Conta Genero ',
    'FROM ' + TabelaAriA + ' ari ',
    'WHERE ari.Boleto=' + Geral.FFI(Boleto),
    'AND ari.Apto=' + Geral.FF0(Apto),
    'AND ari.Codigo=' + Geral.FF0(Codigo),
    'AND ari.CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
    ' ',
    'UNION ',
    ' ',
    'SELECT cns.Nome TEXTO, cni.Valor VALOR, cni.Vencto, ',
    '1 Tipo, MedAnt, MedAtu, Consumo, Casas, ',
    'UnidLei, UnidImp, UnidFat, Controle, Lancto, ',
    'cni.GeraTyp, cni.GeraFat, cni.CasRat, cni.NaoImpLei, ',
    '"' + TabelaCnsA + '" TabelaOrig, cns.Genero ',
    'FROM cons cns ',
    'LEFT JOIN ' + TabelaCnsA + ' cni ON cni.Codigo=cns.Codigo ',
    'WHERE cni.Boleto=' + Geral.FFI(Boleto),
    'AND cni.Apto=' + Geral.FF0(Apto),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    'AND cni.CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
    ' ',
    'ORDER BY VALOR DESC ',
    '']);
end;

procedure TUnBloquetos.ReopenPrevModBol(Query: TmySQLQuery; Codigo, Apto: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT imv.Unidade, cfb.Nome NOME_CONFIGBOL, ',
    'pmb.Apto, pmb.ConfigBol, pmb.ModelBloq, ',
    'pmb.BalAgrMens, pmb.Compe, pmb.Codigo ',
    'FROM prevmodbol pmb ',
    'LEFT JOIN condimov imv ON imv.Conta=pmb.Apto ',
    'LEFT JOIN configbol cfb ON cfb.Codigo=pmb.ConfigBol ',
    'WHERE pmb.Codigo=' + Geral.FF0(Codigo),
    '']);
  if Apto <> 0 then
    Query.Locate('Apto', Apto, []);
end;

procedure TUnBloquetos.ReopenQrSumBol(Query: TmySQLQuery; TabelaAriA,
  TabelaCnsA: String; CliInt, Periodo, Codigo: Integer; PreBol: Boolean);
var
  TipoBol: String;
begin
  if PreBol then
    TipoBol := '='
  else
    TipoBol := '<>';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT SUM(Valor) VALOR ',
    'FROM ' + TabelaAriA + ' ari ',
    'LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto ',
    'LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet ',
    'WHERE ari.Codigo=' + Geral.FF0(Codigo),
    'AND ari.Boleto ' + TipoBol + ' 0 ',
    ' ',
    'UNION ',
    ' ',
    'SELECT SUM(Valor) VALOR ',
    'FROM ' + TabelaCnsA + ' cni ',
    'LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto ',
    'LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet ',
    'WHERE cni.Cond=' + Geral.FF0(CliInt),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    'AND cni.Boleto ' + TipoBol + ' 0 ',
    '']);
end;

procedure TUnBloquetos.ReopenQrSumCP(Query: TmySQLQuery; TabelaCnsA: String;
  CliInt, Periodo, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT SUM(Consumo) CONSUMO, SUM(Valor) VALOR ',
    'FROM ' + TabelaCnsA + ' cni ',
    'WHERE cni.Cond=' + Geral.FF0(CliInt),
    'AND cni.Codigo=' + Geral.FF0(Codigo),
    'AND cni.Periodo=' + Geral.FF0(Periodo),
    '']);
end;

procedure TUnBloquetos.ReopenQuery(QueryOpc, QueryProtocoBol: TmySQLQuery;
  TipoProtocolo, Protocolo, Codigo, Cond, EntCliInt, AbaGerados: Integer;
  TabPrvA, TabAriA, TabCnsA: String);
var
  TipoEmail: Integer;
  SQLCompl: String;
begin
  if TipoProtocolo = 2 then //E-mail
  begin
    ReopenBloOpcoes(QueryOpc);
    //
    TipoEmail := QueryOpc.FieldByName('EntiTipCto').AsInteger;
    //
    if MyObjects.FIC(TipoEmail = 0, nil, 'Tipo de email para envio de boleto n�o definido nas op��es de bloquetos!') then
    begin
      QueryProtocoBol.Close;
      Exit;
    end;
    //
    if AbaGerados = 1 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QueryProtocoBol, Dmod.MyDB, [
        'SELECT DISTINCT ari.Boleto, pre.Codigo, eci.CodEnti Empresa, imv.Propriet Entidade, ',
        'ari.Vencto, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
        'ptc.Codigo PROTOCOD, ppi.Controle ProtocoPak, ptc.Tipo TipoProt, imv.Unidade, ',
        'CONCAT_WS("-", ari.Boleto, IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet)) BOLENT, ',
        'SUM(ari.Valor) Valor, cna.Nome NOME_Cfg, ari.Fatur_Cfg, ari.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, enm.Conta + 0.000 EMeio_ID, enm.Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ari.Apto, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE <= "1900-01-01", "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD <= "1900-01-01", "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y %H:%i")) DataD_Txt, ',
        'cna.JurosPerc, cna.MultaPerc ',
        'FROM condimov imv ',
        'LEFT JOIN ' + TabAriA + ' ari ON ari.Apto=imv.Conta ',
        'LEFT JOIN entimail enm ON enm.Codigo=IF(imv.BloqEndEnt <> 0, imv.BloqEndEnt, (IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet))) ',
        'LEFT JOIN enticontat eco ON eco.Controle = enm.Controle ',
        'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
        'LEFT JOIN entidades ent ON ent.Codigo = ece.Codigo ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Codigo=ari.Codigo ',
        'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=' + Geral.FF0(Protocolo),
        'AND ppi.Docum=ari.Boleto AND ppi.Depto=ari.Apto ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti ',
        'AND ppi.ID_Cod3=enm.Conta AND ppi.ID_Cod4=ari.CNAB_Cfg) ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND ptc.def_client=' + Geral.FF0(EntCliInt),
        'AND enm.EntiTipCto=' + Geral.FF0(TipoEmail),
        'AND ari.Boleto > 0 ',
        'AND ppi.Conta <> 0 ',
        'AND ari.CNAB_Cfg <> 0 ',
        'GROUP BY ptc.Codigo, enm.Email, ari.CNAB_Cfg, ari.Boleto, ari.Apto, ari.Vencto ',
        ' ',
        'UNION ',
        ' ',
        'SELECT DISTINCT cns.Boleto, pre.Codigo, eci.CodEnti Empresa, imv.Propriet Entidade, ',
        'cns.Vencto, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
        'ptc.Codigo PROTOCOD, ppi.Controle ProtocoPak, ptc.Tipo TipoProt, imv.Unidade, ',
        'CONCAT_WS("-", cns.Boleto, IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet)) BOLENT, ',
        'SUM(cns.Valor) Valor, cna.Nome NOME_Cfg, cns.Fatur_Cfg, cns.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, enm.Conta + 0.000 EMeio_ID, enm.Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, cns.Apto, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, cns.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE <= "1900-01-01", "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD <= "1900-01-01", "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y %H:%i")) DataD_Txt, ',
        'cna.JurosPerc, cna.MultaPerc ',
        'FROM condimov imv ',
        'LEFT JOIN ' + TabCnsA + ' cns ON Cns.Apto=imv.Conta ',
        'LEFT JOIN entimail enm ON enm.Codigo=IF(imv.BloqEndEnt <> 0, imv.BloqEndEnt, (IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet))) ',
        'LEFT JOIN enticontat eco ON eco.Controle = enm.Controle ',
        'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
        'LEFT JOIN entidades ent ON ent.Codigo = ece.Codigo ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Periodo=cns.Periodo ',
        'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=cns.CNAB_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=' + Geral.FF0(Protocolo),
        'AND ppi.Docum=cns.Boleto AND ppi.Depto=cns.Apto ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti ',
        'AND ppi.ID_Cod3=enm.Conta AND ppi.ID_Cod4=cns.CNAB_Cfg) ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND ptc.def_client=' + Geral.FF0(EntCliInt),
        'AND enm.EntiTipCto=' + Geral.FF0(TipoEmail),
        'AND cns.Boleto > 0 ',
        'AND ppi.Conta <> 0 ',
        'AND cns.CNAB_Cfg <> 0 ',
        'GROUP BY ptc.Codigo, enm.Email, cns.CNAB_Cfg, cns.Boleto, cns.Apto, cns.Vencto ',
        'ORDER BY NOMEENT, Boleto, Email ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QueryProtocoBol, Dmod.MyDB, [
        'SELECT DISTINCT ari.Boleto, pre.Codigo, eci.CodEnti Empresa, imv.Propriet Entidade,  ',
        'ari.Vencto, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
        'ptc.Codigo PROTOCOD, ppi.Controle ProtocoPak, ptc.Tipo TipoProt, imv.Unidade, ',
        'CONCAT_WS("-", ari.Boleto, IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet)) BOLENT, ',
        'SUM(ari.Valor) Valor, cna.Nome NOME_Cfg, ari.Fatur_Cfg, ari.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, enm.Conta + 0.000 EMeio_ID, enm.Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ari.Apto, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE <= "1900-01-01", "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD <= "1900-01-01", "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y %H:%i")) DataD_Txt, ',
        'cna.JurosPerc, cna.MultaPerc ',
        'FROM condimov imv ',
        'LEFT JOIN ' + TabAriA + ' ari ON ari.Apto = imv.Conta ',
        'LEFT JOIN entimail enm ON enm.Codigo=IF(imv.BloqEndEnt <> 0, imv.BloqEndEnt, (IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet))) ',
        'LEFT JOIN enticontat eco ON eco.Controle = enm.Controle ',
        'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
        'LEFT JOIN entidades ent ON ent.Codigo = eco.Codigo ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Codigo=ari.Codigo ',
        'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
        'LEFT JOIN proenprit pep ON pep.Depto=ari.Apto ',
        'LEFT JOIN proenpr per ON per.Codigo=pep.Codigo ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=per.Protocolo ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=ptc.Codigo ',
        'AND ppi.Docum=ari.Boleto AND ppi.Depto=ari.Apto ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti ',
        'AND ppi.ID_Cod3=enm.Conta AND ppi.ID_Cod4=ari.CNAB_Cfg) ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND ptc.def_client=' + Geral.FF0(EntCliInt),
        'AND enm.EntiTipCto=' + Geral.FF0(TipoEmail),
        'AND ari.Boleto > 0 ',
        'AND ppi.Conta IS NULL ',
        'AND ari.CNAB_Cfg <> 0 ',
        'AND ptc.Codigo=' + Geral.FF0(Protocolo),
        'GROUP BY ptc.Codigo, enm.Email, ari.CNAB_Cfg, ari.Boleto, ari.Apto, ari.Vencto ',
        ' ',
        'UNION ',
        ' ',
        'SELECT DISTINCT cns.Boleto, pre.Codigo, eci.CodEnti Empresa, imv.Propriet Entidade, ',
        'cns.Vencto, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
        'ptc.Codigo PROTOCOD, ppi.Controle ProtocoPak, ptc.Tipo TipoProt, imv.Unidade, ',
        'CONCAT_WS("-", cns.Boleto, IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet)) BOLENT, ',
        'SUM(cns.Valor) Valor, cna.Nome NOME_Cfg, cns.Fatur_Cfg, cns.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, enm.Conta + 0.000 EMeio_ID, enm.Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, cns.Apto, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, cns.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE <= "1900-01-01", "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD <= "1900-01-01", "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y %H:%i")) DataD_Txt, ',
        'cna.JurosPerc, cna.MultaPerc ',
        'FROM condimov imv ',
        'LEFT JOIN ' + TabCnsA + ' cns ON cns.Apto = imv.Conta ',
        'LEFT JOIN entimail enm ON enm.Codigo=IF(imv.BloqEndEnt <> 0, imv.BloqEndEnt, (IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet))) ',
        'LEFT JOIN enticontat eco ON eco.Controle = enm.Controle ',
        'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
        'LEFT JOIN entidades ent ON ent.Codigo = eco.Codigo ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Periodo=cns.Periodo ',
        'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
        'LEFT JOIN proenprit pep ON pep.Depto=cns.Apto ',
        'LEFT JOIN proenpr per ON per.Codigo=pep.Codigo ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=per.Protocolo ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=cns.CNAB_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=ptc.Codigo ',
        'AND ppi.Docum=cns.Boleto AND ppi.Depto=cns.Apto ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti ',
        'AND ppi.ID_Cod3=enm.Conta AND ppi.ID_Cod4=cns.CNAB_Cfg) ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND ptc.def_client=' + Geral.FF0(EntCliInt),
        'AND enm.EntiTipCto=' + Geral.FF0(TipoEmail),
        'AND cns.Boleto > 0 ',
        'AND ppi.Conta IS NULL ',
        'AND cns.CNAB_Cfg <> 0 ',
        'AND ptc.Codigo=' + Geral.FF0(Protocolo),
        'GROUP BY ptc.Codigo, enm.Email, cns.CNAB_Cfg, cns.Boleto, cns.Apto, cns.Vencto ',
        'ORDER BY NOMEENT, Boleto, Email ',
        '']);
    end;
  end else
  if TipoProtocolo = 4 then //CR
  begin
    if AbaGerados = 1 then
      SQLCompl := 'AND ppi.Conta <> 0'
    else
      SQLCompl := 'AND ppi.Conta IS NULL';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QueryProtocoBol, Dmod.MyDB, [
      'SELECT DISTINCT ari.Boleto, pre.Codigo, eci.CodEnti Empresa, imv.Propriet Entidade, ',
      'ari.Vencto, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
      'ptc.Codigo PROTOCOD, ppi.Controle ProtocoPak, ptc.Tipo TipoProt, imv.Unidade, ',
      'CONCAT_WS("-", ari.Boleto, IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet)) BOLENT, ',
      'SUM(ari.Valor) Valor, cna.Nome NOME_Cfg, ari.Fatur_Cfg, ari.CNAB_Cfg, ',
      'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, 0 + 0.000 EMeio_ID, "" Email, ',
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ari.Apto, ',
      'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
      'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
      'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
      'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
      'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DataD_Txt, ',
      'cna.JurosPerc, cna.MultaPerc ',
      'FROM ' + TabAriA + ' ari ',
      'LEFT JOIN condimov imv ON imv.Conta=ari.Apto ',
      'LEFT JOIN ' + TabPrvA + ' pre ON pre.Codigo=ari.Codigo ',
      'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
      'LEFT JOIN entidades ent ON ent.Codigo=imv.Propriet ',
      'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
      'LEFT JOIN protocolos ptc ON ptc.Codigo=cna.ProtocolCR ',
      'LEFT JOIN protpakits ppi ON (ppi.Codigo=ptc.Codigo ',
      'AND ppi.Docum=ari.Boleto AND ppi.Depto=ari.Apto ',
      'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti AND ppi.ID_Cod4=ari.CNAB_Cfg) ',
      'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
      'WHERE pre.Codigo=' + Geral.FF0(Codigo),
      'AND pre.Cond=' + Geral.FF0(Cond),
      'AND ptc.def_client=' + Geral.FF0(EntCliInt),
      'AND ari.Boleto > 0 ',
      'AND ptc.Codigo=' + Geral.FF0(Protocolo),
      'AND ari.CNAB_Cfg <> 0 ',
      SQLCompl,
      'GROUP BY ari.CNAB_Cfg, ari.Boleto, ari.Apto, ari.Vencto ',
      ' ',
      'UNION ',
      ' ',
      'SELECT DISTINCT cns.Boleto, pre.Codigo, eci.CodEnti Empresa, imv.Propriet Entidade, ',
      'cns.Vencto, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
      'ptc.Codigo PROTOCOD, ppi.Controle ProtocoPak, ptc.Tipo TipoProt, imv.Unidade, ',
      'CONCAT_WS("-", cns.Boleto, IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet)) BOLENT, ',
      'SUM(cns.Valor) Valor, cna.Nome NOME_Cfg, cns.Fatur_Cfg, cns.CNAB_Cfg, ',
      'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, 0 + 0.000 EMeio_ID, "" Email, ',
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, cns.Apto, ',
      'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, cns.Controle Arreits, ',
      'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
      'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
      'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
      'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DataD_Txt, ',
      'cna.JurosPerc, cna.MultaPerc ',
      'FROM ' + TabCnsA + ' cns ',
      'LEFT JOIN condimov imv ON imv.Conta=cns.Apto ',
      'LEFT JOIN ' + TabPrvA + ' pre ON pre.Periodo=cns.Periodo ',
      'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
      'LEFT JOIN entidades ent ON ent.Codigo=imv.Propriet ',
      'LEFT JOIN cnab_cfg cna ON cna.Codigo=cns.CNAB_Cfg ',
      'LEFT JOIN protocolos ptc ON ptc.Codigo=cna.ProtocolCR ',
      'LEFT JOIN protpakits ppi ON (ppi.Codigo=ptc.Codigo ',
      'AND ppi.Docum=cns.Boleto AND ppi.Depto=cns.Apto ',
      'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti AND ppi.ID_Cod4=cns.CNAB_Cfg) ',
      'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
      'WHERE pre.Codigo=' + Geral.FF0(Codigo),
      'AND pre.Cond=' + Geral.FF0(Cond),
      'AND ptc.def_client=' + Geral.FF0(EntCliInt),
      'AND cns.Boleto > 0 ',
      'AND ptc.Codigo=' + Geral.FF0(Protocolo),
      'AND cns.CNAB_Cfg <> 0 ',
      'GROUP BY cns.CNAB_Cfg, cns.Boleto, cns.Apto, cns.Vencto ',
      'ORDER BY NOMEENT, Boleto ',
      '']);
  end else
  begin
    if AbaGerados = 1 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QueryProtocoBol, Dmod.MyDB, [
        'SELECT DISTINCT ari.Boleto, pre.Codigo, eci.CodEnti Empresa, imv.Propriet Entidade, ',
        'ari.Vencto, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
        'ptc.Codigo PROTOCOD, ppi.Controle ProtocoPak, ptc.Tipo TipoProt, imv.Unidade,  ',
        'CONCAT_WS("-", ari.Boleto, IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet)) BOLENT, ',
        'SUM(ari.Valor) Valor, ari.Fatur_Cfg, ari.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, 0 + 0.000 EMeio_ID, "" Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ari.Apto, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DataD_Txt, ',
        'IF(ari.CNAB_Cfg<>0, cna.Nome, fcf.Nome) NOME_Cfg, ',
        'IF(ari.CNAB_Cfg<>0, 0, 1) + 0.000 Tipo, ',
        'IF(ari.CNAB_Cfg<>0, ari.CNAB_Cfg, ari.Fatur_Cfg) + 0.000 Config, ',
        'IF(ari.CNAB_Cfg<>0, cna.JurosPerc, fcf.JurosPerc) JurosPerc, ',
        'IF(ari.CNAB_Cfg<>0, cna.MultaPerc, fcf.MultaPerc) MultaPerc ',
        'FROM ' + TabAriA + ' ari ',
        'LEFT JOIN condimov imv ON imv.Conta=ari.Apto ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Codigo=ari.Codigo ',
        'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
        'LEFT JOIN entidades ent ON ent.Codigo=imv.Propriet ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
        'LEFT JOIN faturcfg fcf ON fcf.Codigo=ari.Fatur_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=' + Geral.FF0(Protocolo),
        'AND ppi.Docum=ari.Boleto AND ppi.Depto=ari.Apto ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti AND ppi.ID_Cod4=ari.CNAB_Cfg) ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND ptc.def_client=' + Geral.FF0(EntCliInt),
        'AND ari.Boleto > 0 ',
        'AND ppi.Conta <> 0',
        'AND ari.CNAB_Cfg <> 0 ',
        'GROUP BY ptc.Codigo, Tipo, Config, ari.Boleto, ari.Apto, ari.Vencto ',
        ' ',
        'UNION ',
        ' ',
        'SELECT DISTINCT cns.Boleto, pre.Codigo, eci.CodEnti Empresa, imv.Propriet Entidade, ',
        'cns.Vencto, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
        'ptc.Codigo PROTOCOD, ppi.Controle ProtocoPak, ptc.Tipo TipoProt, imv.Unidade,  ',
        'CONCAT_WS("-", cns.Boleto, IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet)) BOLENT, ',
        'SUM(cns.Valor) Valor, cns.Fatur_Cfg, cns.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, 0 + 0.000 EMeio_ID, "" Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, cns.Apto, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, cns.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DataD_Txt, ',
        'IF(cns.CNAB_Cfg<>0, cna.Nome, fcf.Nome) NOME_Cfg, ',
        'IF(cns.CNAB_Cfg<>0, 0, 1) + 0.000 Tipo, ',
        'IF(cns.CNAB_Cfg<>0, cns.CNAB_Cfg, cns.Fatur_Cfg) + 0.000 Config, ',
        'IF(cns.CNAB_Cfg<>0, cna.JurosPerc, fcf.JurosPerc) JurosPerc, ',
        'IF(cns.CNAB_Cfg<>0, cna.MultaPerc, fcf.MultaPerc) MultaPerc ',
        'FROM ' + TabCnsA + ' cns ',
        'LEFT JOIN condimov imv ON imv.Conta=cns.Apto ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Periodo=cns.Periodo ',
        'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
        'LEFT JOIN entidades ent ON ent.Codigo=imv.Propriet ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=cns.CNAB_Cfg ',
        'LEFT JOIN faturcfg fcf ON fcf.Codigo=cns.Fatur_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=' + Geral.FF0(Protocolo),
        'AND ppi.Docum=cns.Boleto AND ppi.Depto=cns.Apto ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti AND ppi.ID_Cod4=cns.CNAB_Cfg) ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND ptc.def_client=' + Geral.FF0(EntCliInt),
        'AND cns.Boleto > 0 ',
        'AND ppi.Conta <> 0',
        'AND cns.CNAB_Cfg <> 0 ',
        'GROUP BY ptc.Codigo, Tipo, Config, cns.Boleto, cns.Apto, cns.Vencto ',
        'ORDER BY NOMEENT, Boleto ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QueryProtocoBol, Dmod.MyDB, [
        'SELECT DISTINCT ari.Boleto, pre.Codigo, eci.CodEnti Empresa, imv.Propriet Entidade, ',
        'ari.Vencto, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
        'ptc.Codigo PROTOCOD, ppi.Controle ProtocoPak, ptc.Tipo TipoProt, imv.Unidade,  ',
        'CONCAT_WS("-", ari.Boleto, IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet)) BOLENT, ',
        'SUM(ari.Valor) Valor, ari.Fatur_Cfg, ari.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, 0 + 0.000 EMeio_ID, "" Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ari.Apto, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, ari.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DataD_Txt, ',
        'IF(ari.CNAB_Cfg<>0, cna.Nome, fcf.Nome) NOME_Cfg, ',
        'IF(ari.CNAB_Cfg<>0, 0, 1) + 0.000 Tipo, ',
        'IF(ari.CNAB_Cfg<>0, ari.CNAB_Cfg, ari.Fatur_Cfg) + 0.000 Config, ',
        'IF(ari.CNAB_Cfg<>0, cna.JurosPerc, fcf.JurosPerc) JurosPerc, ',
        'IF(ari.CNAB_Cfg<>0, cna.MultaPerc, fcf.MultaPerc) MultaPerc ',
        'FROM ' + TabAriA + ' ari ',
        'LEFT JOIN condimov imv ON imv.Conta=ari.Apto ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Codigo=ari.Codigo ',
        'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
        'LEFT JOIN entidades ent ON ent.Codigo=imv.Propriet ',
        'LEFT JOIN proenprit pep ON pep.Depto=ari.Apto ',
        'LEFT JOIN proenpr per ON per.Codigo=pep.Codigo ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=per.Protocolo ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg ',
        'LEFT JOIN faturcfg fcf ON fcf.Codigo=ari.Fatur_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=ptc.Codigo ',
        'AND ppi.Docum=ari.Boleto AND ppi.Depto=ari.Apto ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti AND ppi.ID_Cod4=ari.CNAB_Cfg) ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND ptc.def_client=' + Geral.FF0(EntCliInt),
        'AND ari.Boleto > 0 ',
        'AND ptc.Codigo=' + Geral.FF0(Protocolo),
        'AND ppi.Conta IS NULL ',
        'GROUP BY ptc.Codigo, Tipo, Config, ari.Boleto, ari.Apto, ari.Vencto ',
        ' ',
        'UNION ',
        ' ',
        'SELECT DISTINCT cns.Boleto, pre.Codigo, eci.CodEnti Empresa, imv.Propriet Entidade, ',
        'cns.Vencto, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT, ',
        'ptc.Codigo PROTOCOD,  ppi.Controle ProtocoPak, ptc.Tipo TipoProt, imv.Unidade,  ',
        'CONCAT_WS("-", cns.Boleto, IF(imv.Usuario <> 0, imv.Usuario, imv.Propriet)) BOLENT, ',
        'SUM(cns.Valor) Valor, cns.Fatur_Cfg, cns.CNAB_Cfg, ',
        'ppi.Conta PROTOCOLO, ptc.Nome TAREFA, 0 + 0.000 EMeio_ID, "" Email, ',
        'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, cns.Apto, ',
        'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, cns.Controle Arreits, ',
        'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, ',
        'ppi.MultaVal MULTAVAL, pre.Codigo PREVCOD, pre.Periodo, ppi.DataD, ppi.DataE, ',
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DataD_Txt, ',
        'IF(cns.CNAB_Cfg<>0, cna.Nome, fcf.Nome) NOME_Cfg, ',
        'IF(cns.CNAB_Cfg<>0, 0, 1) + 0.000 Tipo, ',
        'IF(cns.CNAB_Cfg<>0, cns.CNAB_Cfg, cns.Fatur_Cfg) + 0.000 Config, ',
        'IF(cns.CNAB_Cfg<>0, cna.JurosPerc, fcf.JurosPerc) JurosPerc, ',
        'IF(cns.CNAB_Cfg<>0, cna.MultaPerc, fcf.MultaPerc) MultaPerc ',
        'FROM ' + TabCnsA + ' cns ',
        'LEFT JOIN condimov imv ON imv.Conta=cns.Apto ',
        'LEFT JOIN ' + TabPrvA + ' pre ON pre.Periodo=cns.Periodo ',
        'LEFT JOIN enticliint eci ON eci.CodCliInt=pre.Cond ',
        'LEFT JOIN entidades ent ON ent.Codigo=imv.Propriet ',
        'LEFT JOIN proenprit pep ON pep.Depto=cns.Apto ',
        'LEFT JOIN proenpr per ON per.Codigo=pep.Codigo ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=per.Protocolo ',
        'LEFT JOIN cnab_cfg cna ON cna.Codigo=cns.CNAB_Cfg ',
        'LEFT JOIN faturcfg fcf ON fcf.Codigo=cns.Fatur_Cfg ',
        'LEFT JOIN protpakits ppi ON (ppi.Codigo=ptc.Codigo ',
        'AND ppi.Docum=cns.Boleto AND ppi.Depto=cns.Apto ',
        'AND ppi.ID_Cod1=pre.Codigo AND ppi.ID_Cod2=eci.CodEnti AND ppi.ID_Cod4=cns.CNAB_Cfg) ',
        'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender ',
        'WHERE pre.Codigo=' + Geral.FF0(Codigo),
        'AND pre.Cond=' + Geral.FF0(Cond),
        'AND ptc.def_client=' + Geral.FF0(EntCliInt),
        'AND cns.Boleto > 0 ',
        'AND ptc.Codigo=' + Geral.FF0(Protocolo),
        'AND ppi.Conta IS NULL ',
        'GROUP BY ptc.Codigo, Tipo, Config, cns.Boleto, cns.Apto, cns.Vencto ',
        'ORDER BY NOMEENT, Boleto ',
        '']);
    end;
  end;
end;

function TUnBloquetos.CalculaTotalPRI(TabelaPriA, TabelaPrvA: String; Periodo,
  Codigo, Controle: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result        := False;
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(Valor) Total ',
      'FROM ' + TabelaPriA,
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabelaPrvA, False,
      ['Gastos'], ['Codigo'], [Qry.FieldByName('Total').AsFloat], [Codigo], True) then
    begin
      Result := True;
    end;
  finally
    Qry.Free;
    Screen.Cursor := crDefault;
  end;
end;

function TUnBloquetos.VerificaSeOpcoesForamConfiguradas(Query: TmySQLQuery): Boolean;
begin
  ReopenBloOpcoes(Query);
  //
  if Query.FieldByName('Configurado').AsInteger = 0 then
    Result := False
  else
    Result := True;
end;

function TUnBloquetos.DesfazerBoletos(Selecao: TSelType; QueryUpd, QueryAux,
  QueryBoletos, QueryBoletosIts: TmySQLQuery; Database: TmySQLDatabase;
  Grade: TDBGrid; TabLctA, TabPrvA, TabAriA, TabCnsA: String; Codigo, EntCliInt,
  Periodo: Integer; Progress: TProgressBar): Boolean;

  function DesfazerBoletoAtual(QueryBoletos, QueryBoletosIts, QueryAux,
    QueryUpd: TmySQLQuery; Database: TmySQLDatabase; TabLctA, TabAriA,
    TabCnsA: String): Boolean;
  var
    Data: TDateTime;
    TabelaOrig: String;
    MotivDel, Tipo, Carteira, Lancto, Sub, Controle, CNAB_Cfg, Apto: Integer;
    Boleto: Double;
  begin
    Result   := False;
    CNAB_Cfg := QueryBoletos.FieldByName('CNAB_Cfg').AsInteger;
    Apto     := QueryBoletos.FieldByName('Apto').AsInteger;
    Boleto   := QueryBoletos.FieldByName('Boleto').AsFloat;
    //
    if MyObjects.FIC(UnProtocolo.VerificaSeLoteCRFoiGerado(Dmod.MyDB, Codigo,
      EntCliInt, CNAB_Cfg, Apto, Boleto), nil, 'O boleto n�mero ' + Geral.FFI(Boleto) +
      ' n�o pode ser exclu�do!' + sLineBreak + 'Motivo: Foi gerado um arquivo remessa para ele!')
    then
      Exit;
    if MyObjects.FIC(UnProtocolo.VerificaSeProtocoloRetornou(Dmod.MyDB, Codigo,
      EntCliInt, CNAB_Cfg, Apto, Boleto), nil, 'O boleto n�mero ' + Geral.FFI(Boleto) +
      ' n�o pode ser exclu�do!' + sLineBreak + 'Motivo: O protocolo foi marcado com data de retorno!')
    then
      Exit;
    if not VerificaSeTodosLctsEstaoNaTabLctA(QueryBoletosIts,QueryAux, Database, TabLctA) then
    begin
      Geral.MB_Aviso('A��o abortada!' + sLineBreak +
        'Motivo: N�o foi poss�vel localizar todos os lan�amentos financeiros referentes ao boleto!');
      Exit;
    end;
    if VerificaSeAlgumItemDoBoletoEstaPago(QueryBoletosIts, QueryAux, Database, TabLctA) then
    begin
      Geral.MB_Aviso('A��o abortada!' + sLineBreak +
        'Motivo: O boleto j� possu� pagamentos lan�ados nele!');
      Exit;
    end;
    try
      QueryBoletosIts.First;
      //
      while not QueryBoletosIts.Eof do
      begin
        TabelaOrig := QueryBoletosIts.FieldByName('TabelaOrig').AsString;
        Controle   := QueryBoletosIts.FieldByName('Controle').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
          'SELECT Data, Tipo, Carteira, Controle, Sub, FatID',
          'FROM ' + TabLctA,
          'WHERE Controle=' + Geral.FF0(QueryBoletosIts.FieldByName('Lancto').AsInteger),
          '']);
        //Caso exista mais exista um lan�amento
        if QueryAux.RecordCount > 0 then
        begin
          QueryAux.First;
          while not QueryAux.Eof do
          begin
            if LowerCase(TabelaOrig) = LowerCase(TabAriA) then
              MotivDel := dmkPF.MotivDel_ValidaCodigo(306)
            else
              MotivDel := dmkPF.MotivDel_ValidaCodigo(307);
            //
            Data     := QueryAux.FieldByName('Data').AsDateTime;
            Tipo     := QueryAux.FieldByName('Tipo').AsInteger;
            Carteira := QueryAux.FieldByName('Carteira').AsInteger;
            Lancto   := QueryAux.FieldByName('Controle').AsInteger;
            Sub      := QueryAux.FieldByName('Sub').AsInteger;
            //
            try
              //Lan�amento financeiro
              UFinanceiro.ExcluiLct_Unico(TabLctA, Database, Data, Tipo, Carteira,
                Lancto, Sub, MotivDel, False);
            except
              ;
            end;
            //
            QueryAux.Next;
          end;
          try
            {$IfNDef SemProtocolo}
            UnProtocolo.ExcluiLoteProtocoloBoleto(Database, 0, Codigo, EntCliInt, Controle, TabelaOrig);
            {$EndIf}
            if LowerCase(TabelaOrig) = LowerCase(TabAriA) then
            begin
              //Arrecada��es
              Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabAriA, False,
                          ['Boleto', 'Lancto'], ['Controle'], [0, 0], [Controle], True);
            end else
            begin
              //Consumos
              Result := UMyMod.SQLInsUpd(QueryUpd, stUpd, TabCnsA, False,
                          ['Boleto', 'Lancto'], ['Controle'], [0, 0], [Controle], True);
            end;
          except
            ;
          end;
          //
          QueryBoletosIts.Next;
        end;
      end;
    finally
      ;
    end;
  end;

var
  I: Integer;
  Resposta: Integer;
begin
  if (QueryBoletos.RecordCount > 0) then
  begin
    if not VerificaSeHouveEmissaoPosterior(QueryAux, Database, TabAriA, TabPrvA,
      TabCnsA, EntCliInt, Periodo) then
    begin
      try
        QueryBoletos.DisableControls;
        QueryBoletosIts.DisableControls;
        //
        Grade.Enabled     := False;
        Progress.Position := 0;
        //
        case Selecao of
          istAtual:
            Resposta := Geral.MB_Pergunta('Tem certeza que deseja desfazer a gera��o do boleto atual?' +
                          sLineBreak + 'ATEN��O: Todos os lan�amentos financeiros atrelados ao boleto ser�o exclu�dos!');
          istSelecionados:
            Resposta := Geral.MB_Pergunta('Tem certeza que deseja desfazer a gera��o dos boletos selecionados?' +
                          sLineBreak + 'ATEN��O: Todos os lan�amentos financeiros atrelados ao boleto ser�o exclu�dos!');
          istTodos:
            Resposta := Geral.MB_Pergunta('Tem certeza que deseja desfazer a gera��o de todos boletos?' +
                          sLineBreak + 'ATEN��O: Todos os lan�amentos financeiros atrelados ao boleto ser�o exclu�dos!');
          else
          begin
            Geral.MB_Aviso('Tipo de sele��o n�o implementada!');
            Resposta := ID_NO;
          end;
        end;
        if Resposta = ID_YES then
        begin
          case Selecao of
            istAtual:
            begin
              Result := DesfazerBoletoAtual(QueryBoletos, QueryBoletosIts,
                        QueryAux, QueryUpd, Database, TabLctA, TabAriA, TabCnsA);
            end;
            istSelecionados:
            begin
              if Grade.SelectedRows.Count > 0 then
              begin
                Progress.Max := Grade.SelectedRows.Count;
                //
                with Grade.DataSource.DataSet do
                begin
                  for I := 0 to Grade.SelectedRows.Count - 1 do
                  begin
                    //GotoBookmark(pointer(Grade.SelectedRows.Items[I]));
                    GotoBookmark(Grade.SelectedRows.Items[I]);
                    //
                    Result := DesfazerBoletoAtual(QueryBoletos, QueryBoletosIts,
                              QueryAux, QueryUpd, Database, TabLctA, TabAriA, TabCnsA);
                    //
                    Progress.Position := Progress.Position + 1;
                    Progress.Update;
                    Application.ProcessMessages;
                  end;
                end;
              end else
                Result := DesfazerBoletoAtual(QueryBoletos, QueryBoletosIts,
                          QueryAux, QueryUpd, Database, TabLctA, TabAriA, TabCnsA);
            end;
            istTodos:
            begin
              Progress.Max := QueryBoletos.RecordCount;
              //
              QueryBoletos.First;
              //
              while not QueryBoletos.Eof do
              begin
                Result := DesfazerBoletoAtual(QueryBoletos, QueryBoletosIts,
                          QueryAux, QueryUpd, Database, TabLctA, TabAriA, TabCnsA);
                //
                Progress.Position := Progress.Position + 1;
                Progress.Update;
                Application.ProcessMessages;
                //
                QueryBoletos.Next;
              end;
            end;
          end;
        end else
          Result := False;
      finally
        QueryBoletos.EnableControls;
        QueryBoletosIts.EnableControls;
        //
        Progress.Position := 0;
        Grade.Enabled     := True;
      end;
    end else
    begin
      Geral.MB_Aviso('A��o cancelada! J� houve emiss�o posterior!');
      Result := False;
    end;
    if (Result = False) and (Resposta = ID_YES) then
      Geral.MB_Aviso('Falha ao desfazer boletos!');
  end else
    Result := True;
end;

procedure TUnBloquetos.ReopenArreFut(QueryArreFutI: TmySQLQuery; CliInt, MesesAnt,
  Controle: Integer);
var
  Loc, Peri: Integer;
begin
  Peri := Geral.Periodo2000(DModG.ObtemAgora());
  Loc  := Controle;
  //
  if Controle < 1 then
    if QueryArreFutI.State = dsBrowse then
      Loc := QueryArreFutI.FieldByName('Controle').AsInteger;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryArreFutI, Dmod.MyDB, [
    'SELECT IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPROP, ',
    'imv.Unidade UNIDADE, con.Nome NOMECONTA, ',
    {$IfNDef sNFSe}
    'nsc.Nome NFSeSrvCad_TXT, ',
    {$Else}
    '"" NFSeSrvCad_TXT, ',
    {$EndIf}
    'IF(arf.CNAB_Cfg<>0, cna.Nome, fat.Nome) NOME_Cfg, ',
    'arf.* ',
    'FROM arrefut arf ',
    {$IfNDef sNFSe}
    'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo = arf.NFSeSrvCad ',
    {$EndIf}
    'LEFT JOIN condimov  imv ON imv.Conta=arf.Depto ',
    'LEFT JOIN entidades prp ON prp.Codigo=imv.Propriet ',
    'LEFT JOIN contas    con ON con.Codigo=arf.Conta ',
    'LEFT JOIN cnab_cfg  cna ON cna.Codigo=arf.CNAB_Cfg ',
    'LEFT JOIN faturcfg  fat ON fat.Codigo=arf.Fatur_Cfg ',
    'WHERE arf.' + TAB_ARI + '=0 ',
    'AND arf.CliInt=' + Geral.FF0(CliInt),
    'AND arf.Periodo>=' + Geral.FF0(Peri - MesesAnt),
    'ORDER BY arf.Periodo DESC ',
    '']);
  //
  if Loc > 0 then
    QueryArreFutI.Locate('Controle', Loc, []);
end;

procedure TUnBloquetos.ExcluiLeituraAtual(Controle: Integer; Lancto, Boleto: Double;
  TabCnsA, TabLctA: String);
var
  QueryUpd, QrLocLct: TmySQLQuery;
begin
  QueryUpd := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  QrLocLct := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Dmod.MyDB, [
      DELETE_FROM  + TabCnsA,
      'WHERE Controle=' + Geral.FF0(Controle) + ' AND Boleto=0',
      '']);
    if (Lancto <> 0) and (Boleto = 0) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLocLct, Dmod.MyDB, [
        'SELECT Data, Tipo, Carteira, Sub ',
        'FROM ' + TabCnsA,
        'WHERE Controle=' + Geral.FFI(Lancto),
        'AND FatID=' + Geral.FF0(VAR_FATID_0601),
        '']);
      if QrLocLct.RecordCount > 0 then
      begin
        UFinanceiro.ExcluiLct_Unico(TabLctA, Dmod.MyDB,
          QrLocLct.FieldByName('Data').AsDateTime,
          QrLocLct.FieldByName('Tipo').AsInteger,
          QrLocLct.FieldByName('Carteira').AsInteger,
          Lancto, QrLocLct.FieldByName('Sub').AsInteger,
          dmkPF.MotivDel_ValidaCodigo(307), False, False);
      end;
    end;
  finally
    QueryUpd.Free;
    QrLocLct.Free;
  end;
end;

//Migra��es
procedure TUnBloquetos.ConverteControleToBloOpcoes(DataBase: TMySQLDataBase);
var
  Qry, QryUpd: TMySQLQuery;
  BLQ_MEsqAvisoV, BLQ_TopoAvisoV, BLQ_AltuAvisoV, BLQ_LargAvisoV,
  BLQ_MEsqDestin, BLQ_TopoDestin, BLQ_AltuDestin, BLQ_LargDestin: Integer;
begin
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM bloopcoes ',
      'WHERE AtualizOpc=1 ',
      '']);
    if Qry.RecordCount > 0 then
      Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT BLQ_MEsqAvisoV, BLQ_TopoAvisoV, ',
      'BLQ_AltuAvisoV, BLQ_LargAvisoV, ',
      'BLQ_MEsqDestin, BLQ_TopoDestin, ',
      'BLQ_AltuDestin, BLQ_LargDestin ',
      'FROM controle ',
      '']);
    BLQ_MEsqAvisoV := Qry.FieldByName('BLQ_MEsqAvisoV').AsInteger;
    BLQ_TopoAvisoV := Qry.FieldByName('BLQ_TopoAvisoV').AsInteger;
    BLQ_AltuAvisoV := Qry.FieldByName('BLQ_AltuAvisoV').AsInteger;
    BLQ_LargAvisoV := Qry.FieldByName('BLQ_LargAvisoV').AsInteger;
    BLQ_MEsqDestin := Qry.FieldByName('BLQ_MEsqDestin').AsInteger;
    BLQ_TopoDestin := Qry.FieldByName('BLQ_TopoDestin').AsInteger;
    BLQ_AltuDestin := Qry.FieldByName('BLQ_AltuDestin').AsInteger;
    BLQ_LargDestin := Qry.FieldByName('BLQ_LargDestin').AsInteger;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
      'UPDATE bloopcoes SET ',
      'BLQ_MEsqAvisoV=' + Geral.FF0(BLQ_MEsqAvisoV) + ', ',
      'BLQ_TopoAvisoV=' + Geral.FF0(BLQ_TopoAvisoV) + ', ',
      'BLQ_AltuAvisoV=' + Geral.FF0(BLQ_AltuAvisoV) + ', ',
      'BLQ_LargAvisoV=' + Geral.FF0(BLQ_LargAvisoV) + ', ',
      'BLQ_MEsqDestin=' + Geral.FF0(BLQ_MEsqDestin) + ', ',
      'BLQ_TopoDestin=' + Geral.FF0(BLQ_TopoDestin) + ', ',
      'BLQ_AltuDestin=' + Geral.FF0(BLQ_AltuDestin) + ', ',
      'BLQ_LargDestin=' + Geral.FF0(BLQ_LargDestin) + ' ',
      '']);
    UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
      'UPDATE bloopcoes SET AtualizOpc=1 ',
      '']);
  finally
    Qry.Free;
    QryUpd.Free;
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnBloquetos.ConverteCondImovProtToProEnPr(DataBase: TMySQLDataBase;
  PB1: TProgressBar);
var
  Qry, QryLoc, QryUpd: TMySQLQuery;
  Codigo, Controle, Depto, Protocolo: Integer;
begin
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryLoc        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM bloopcoes ',
      'WHERE AtualizOpc=1 ',
      '']);
    if Qry.RecordCount > 0 then
      Exit;
    //
    PB1.Position := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DISTINCT Protocolo Protocolo ',
      'FROM condimov ',
      'WHERE Protocolo <> 0 ',
      ' ',
      'UNION ',
      ' ',
      'SELECT DISTINCT Protocolo2 Protocolo ',
      'FROM condimov ',
      'WHERE Protocolo2 <> 0 ',
      ' ',
      'UNION ',
      ' ',
      'SELECT DISTINCT Protocolo3 Protocolo ',
      'FROM condimov ',
      'WHERE Protocolo3 <> 0 ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max      := Qry.RecordCount;
      //
      while not Qry.EOF do
      begin
        Protocolo := Qry.FieldByName('Protocolo').AsInteger;
        Codigo    := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'proenpr', 'proenpr', 'Codigo');
        //
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'proenpr', False,
          ['Protocolo', 'Finalidade'], ['Codigo'],
          [Protocolo, Integer(ptkBoleto)], [Codigo], True)
        then
          Exit;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        Qry.Next;
      end;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Conta, Protocolo Protocolo ',
        'FROM condimov ',
        'WHERE Protocolo <> 0 ',
        ' ',
        'UNION ',
        ' ',
        'SELECT Conta, Protocolo2 Protocolo ',
        'FROM condimov ',
        'WHERE Protocolo2 <> 0 ',
        ' ',
        'UNION ',
        ' ',
        'SELECT Conta, Protocolo3 Protocolo ',
        'FROM condimov ',
        'WHERE Protocolo3 <> 0 ',
        '']);
      UnDmkDAC_PF.AbreMySQLQuery0(QryLoc, Dmod.MyDB, [
        'SELECT Codigo, Protocolo ',
        'FROM proenpr ',
        '']);
      if (Qry.RecordCount > 0) and (QryLoc.RecordCount > 0) then
      begin
        PB1.Position := 0;
        PB1.Max      := Qry.RecordCount;
        //
        while not Qry.EOF do
        begin
          Depto     := Qry.FieldByName('Conta').AsInteger;
          Protocolo := Qry.FieldByName('Protocolo').AsInteger;
          //
          if QryLoc.Locate('Codigo', Protocolo, []) then
          begin
            Codigo   := QryLoc.FieldByName('Codigo').AsInteger;
            Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'proenprit', 'proenprit', 'Controle');
            //
            if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'proenprit', False,
              ['Depto', 'Codigo'], ['Controle'],
              [Depto, Codigo], [Controle], True)
            then
              Exit;
          end;
          //
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          //
          Qry.Next;
        end;
      end;
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
      'UPDATE bloopcoes SET AtzCondImvPro=1 ',
      '']);
  finally
    PB1.Position := 0;
    Qry.Free;
    QryLoc.Free;
    QryUpd.Free;
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnBloquetos.ConverteCondBoletosParaCNABcfg(
  DataBase: TMySQLDataBase; PB1: TProgressBar);
var
  Qry, QryLoc, QryUpd: TMySQLQuery;
  Cond, CNAB_Cfg, Aceite, Correio: Integer;
  TabAriA, TabCnsA: String;
  LastNosNum: Largeint;
begin
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryLoc        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    PB1.Position := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT con.*, ',
      'CONCAT("BOLETOS - ", IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NomeCfg ',
      'FROM cond con ',
      'LEFT JOIN entidades ent ON ent.Codigo = con.Cliente ',
      'WHERE MigrouPCNAB_Cfg=0 ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max      := Qry.RecordCount;
      //
      while not Qry.EOF do
      begin
        Cond := Qry.FieldByName('Codigo').AsInteger;
        //
        if Qry.FieldByName('UsaCNAB_Cfg').AsInteger = 0 then
        begin
          CNAB_Cfg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'CNAB_Cfg', 'CNAB_Cfg', 'Codigo');
          //
          if UpperCase(Qry.FieldByName('Aceite').AsString) = 'S' then
            Aceite := 1
          else
            Aceite := 0;
          //
          if UpperCase(Qry.FieldByName('Correio').AsString) = 'S' then
            Correio := 1
          else
            Correio := 0;
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QryLoc, Database, [
            'SELECT Bloqueto ',
            'FROM bancosblo ',
            'WHERE Codigo=' + Geral.FF0(Qry.FieldByName('Banco').AsInteger),
            'AND Entidade=' + Geral.FF0(Qry.FieldByName('Cliente').AsInteger),
            '']);
          LastNosNum := QryLoc.FieldByName('Bloqueto').AsLargeInt;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'CNAB_Cfg', False,
          [
            'Nome', 'CedBanco',
            'Cedente', 'SacadAvali',
            'CedAgencia', 'CedConta',
            'CedDAC_A', 'CedDAC_C',
            'CartNum', 'AceiteTit',
            'Correio', 'CodEmprBco',
            'JurosPerc', 'MultaPerc',
            'Texto01', 'Texto02',
            'Texto03', 'Texto04',
            'Texto05', 'Texto06',
            'Texto07', 'Texto08',
            'CNAB', 'LastNosNum', 'LocalPag',
            'EspecieVal', 'OperCodi',
            'IDCobranca', 'AgContaCed',
            'EspecieDoc', 'CedPosto',
            'CartEmiss', 'TipoCobranca',
            'EspecieTxt', 'CartTxt',
            'ModalCobr', 'CtaCooper',
            'VTCBBNITAR', 'CorresBco',
            'CorresAge', 'CorresCto',
            'NPrinBc', 'TermoAceite'
          ], ['Codigo'], [
            Qry.FieldByName('NomeCfg').AsString, Qry.FieldByName('Banco').AsInteger,
            Qry.FieldByName('Cedente').AsInteger, Qry.FieldByName('SacadAvali').AsInteger,
            Qry.FieldByName('Agencia').AsInteger, Qry.FieldByName('Conta').AsString,
            Qry.FieldByName('DVAgencia').AsString, Qry.FieldByName('DVConta').AsString,
            Qry.FieldByName('Carteira').AsString, Aceite,
            Correio, Qry.FieldByName('CodCedente').AsString,
            Qry.FieldByName('PercJuros').AsFloat, Qry.FieldByName('PercMulta').AsFloat,
            Qry.FieldByName('Instrucao1').AsString, Qry.FieldByName('Instrucao2').AsString,
            Qry.FieldByName('Instrucao3').AsString, Qry.FieldByName('Instrucao4').AsString,
            Qry.FieldByName('Instrucao5').AsString, Qry.FieldByName('Instrucao6').AsString,
            Qry.FieldByName('Instrucao7').AsString, Qry.FieldByName('Instrucao8').AsString,
            Qry.FieldByName('CNAB').AsInteger, LastNosNum, Qry.FieldByName('LocalPag').AsString,
            Qry.FieldByName('EspecieVal').AsString, Qry.FieldByName('OperCodi').AsString,
            Qry.FieldByName('IDCobranca').AsString, Qry.FieldByName('AgContaCed').AsString,
            Qry.FieldByName('EspecieDoc').AsString, Qry.FieldByName('Posto').AsInteger,
            Qry.FieldByName('CartEmiss').AsInteger, Qry.FieldByName('TipoCobranca').AsInteger,
            Qry.FieldByName('EspecieVal').AsString, Qry.FieldByName('CartTxt').AsString,
            Qry.FieldByName('ModalCobr').AsInteger, Qry.FieldByName('CtaCooper').AsString,
            Qry.FieldByName('VTCBBNITAR').AsFloat, Qry.FieldByName('CorresBco').AsInteger,
            Qry.FieldByName('CorresAge').AsInteger, Qry.FieldByName('CorresCto').AsString,
            Qry.FieldByName('NPrinBc').AsString, -1
          ], [CNAB_Cfg], True) then
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
              'UPDATE arrefut SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
              'WHERE Cond=' + Geral.FF0(Cond),
              '']);
            UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
              'UPDATE arrebai SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
              'WHERE Cond=' + Geral.FF0(Cond),
              '']);
            UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
              'UPDATE bloqparc SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
              'WHERE CodCliEsp=' + Geral.FF0(Cond),
              '']);
            UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
              'UPDATE consprc SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
              'WHERE Cond=' + Geral.FF0(Cond),
              '']);
            //
            TabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, Cond);

            if (TabAriA <> '') then
            begin
              UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
                'UPDATE ' + TabAriA + ' SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
                '']);
            end;

            TabCnsA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, Cond);

            if (TabCnsA <> '') then
            begin
              UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
                'UPDATE ' + TabCnsA + ' SET CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
                '']);
            end;
          end;
        end;
        UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
          'UPDATE cond SET MigrouPCNAB_Cfg=1 ',
          'WHERE Codigo=' + Geral.FF0(Cond),
          '']);
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        Qry.Next;
      end;
    end;
  finally
    PB1.Position := 0;
    Qry.Free;
    QryLoc.Free;
    QryUpd.Free;
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnBloquetos.AtualizaCondEmeios(DataBase: TMySQLDataBase; PB1: TProgressBar);
var
  Qry, Qry2, QryUpd: TMySQLQuery;
  EntiTipCto, Controle, Conta, Item, Entidade: Integer;
  Nome, EMeio: String;
begin
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  Qry2          := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM bloopcoes ',
      'WHERE AtzCondImvEme=1 ',
      '']);
    if Qry.RecordCount > 0 then
      Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM bloopcoes ',
      '']);
    EntiTipCto := Qry.FieldByName('EntiTipCto').AsInteger;
    //
    if EntiTipCto = 0 then
      Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
      'SELECT cem.Item, cem.Nome, cem.EMeio, ',
      'IF(BloqEndEnt <> 0, BloqEndEnt, (IF(Usuario <> 0, Usuario, Propriet))) Entidade ',
      'FROM condemeios cem ',
      'LEFT JOIN condimov imv ON imv.Conta = cem.Conta ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max      := Qry.RecordCount;
      //
      while not Qry.EOF do
      begin
        Item     := Qry.FieldByName('Item').AsInteger;
        Nome     := Qry.FieldByName('Nome').AsString;
        EMeio    := Qry.FieldByName('Emeio').AsString;
        Entidade := Qry.FieldByName('Entidade').AsInteger;
        //
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'enticontat', 'enticontat', 'Controle');
        //
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'enticontat', False,
          ['Nome', 'Codigo'], ['Controle'],
          [Nome, Entidade], [Controle], True)
        then
          Exit;
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'enticonent', False,
          ['Codigo'], ['Controle'],
          [Entidade], [Controle], True)
        then
          Exit;
        //
        Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'entimail', 'entimail', 'Conta');
        //
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entimail', False,
          ['Email', 'EntiTipCto', 'Controle', 'Codigo'], ['Conta'],
          [EMeio, EntiTipCto, Controle, Entidade], [Controle], True)
        then
          Exit;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        Qry.Next;
      end;
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, DataBase, [
      'UPDATE bloopcoes ',
      'SET AtzCondImvEme = 1 ',
      '']);
  finally
    PB1.Position := 0;
    //
    Qry.Free;
    Qry2.Free;
    QryUpd.Free;
    Screen.Cursor := crDefault;
  end;
end;

function TUnBloquetos.ArrecadacoesVencidas_e_AVencer(Hoje: TDateTime;
  MostraForm: Boolean): Integer;
begin
  Result := 0;
  //Compatibilidade
end;

function TUnBloquetos.AtualizaArrecadacao(SQLType: TSQLType; Codigo,
  ControleArre, Conta, BloArre, BloArreIts, Entidade, CartEmiss, Tipo, Config,
  NFSeSrvCad, Lancto, Avulso, ComoAdd, FatID, FatParcela: Integer; Vencto: String;
  Valor, FatNum, Boleto: Double; Texto: String; var Mensagem: String): Integer;
begin
  Result := 0;
  //Compatibilidade
end;

procedure TUnBloquetos.AtualizaArreFut(DataBase: TMySQLDataBase;
  PB1: TProgressBar);
var
  Controle, Cond, Apto: Integer;
  Qry, QryUpd: TMySQLQuery;
begin
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM bloopcoes ',
      'WHERE CorrigiuArreFut=1 ',
      '']);
    if Qry.RecordCount > 0 then
      Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
      'SELECT * ',
      'FROM arrefut ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max      := Qry.RecordCount;
      //
      while not Qry.EOF do
      begin
        Cond     := Qry.FieldByName('Cond').AsInteger;
        Apto     := Qry.FieldByName('Apto').AsInteger;
        Controle := Qry.FieldByName('Controle').AsInteger;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'arrefut', False,
          ['CliInt', 'Depto'], ['Controle'], [Cond, Apto], [Controle], True);
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.Processmessages;
        //
        Qry.Next;
      end;
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, DataBase, [
      'UPDATE bloopcoes ',
      'SET CorrigiuArreFut = 1 ',
      '']);
  finally
    Qry.Free;
    QryUpd.Free;
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnBloquetos.AtualizaDadosProtocolosNosBoletos(
  DataBase: TMySQLDataBase; PB1: TProgressBar);
var
  Qry, Qry3, QryUpd: TMySQLQuery;
  Conta, CNAB_Cfg, CodCliInt, Prev, Depto, CodEnti: Integer;
  TabAriA, TabCnsA, TabPrvA: String;
  Docum: Double;
begin
  Screen.Cursor := crHourGlass;
  Qry           := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  Qry3          := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM bloopcoes ',
      'WHERE CorrigiuProtAriCns=1 ',
      '']);
    if Qry.RecordCount > 0 then
      Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
      'SELECT its.ID_Cod1, its.ID_Cod2, eci.CodEnti, ',
      'eci.CodCliInt, its.Depto, its.Docum, its.Conta ',
      'FROM protpakits its ',
      'LEFT JOIN protocolos pro ON pro.Codigo = its.Codigo ',
      'LEFT JOIN enticliint eci ON eci.CodCliInt = its.ID_Cod2 ',
      'WHERE pro.Tipo IN (1,2,4) ',
      'AND its.ID_Cod1 <> 0 ',
      'AND its.ID_Cod2 <> 0 ',
      'AND its.Link_ID=' + Geral.FF0(VAR_TIPO_LINK_ID_01_GENERICO),
      'AND its.Depto <> 0 ',
      'AND its.Docum <> 0 ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      PB1.Position := 0;
      PB1.Max      := Qry.RecordCount;
      //
      while not Qry.EOF do
      begin
        CodCliInt := Qry.FieldByName('CodCliInt').AsInteger;
        TabAriA   := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CodCliInt);
        TabCnsA   := DModG.NomeTab(TMeuDB, ntCns, False, ttA, CodCliInt);
        TabPrvA   := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, CodCliInt);
        Prev      := Qry.FieldByName('ID_Cod1').AsInteger;
        Depto     := Qry.FieldByName('Depto').AsInteger;
        Docum     := Qry.FieldByName('Docum').AsFloat;
        Conta     := Qry.FieldByName('Conta').AsInteger;
        CodEnti   := Qry.FieldByName('CodEnti').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry3, Database, [
          'SELECT ari.CNAB_Cfg ',
          'FROM ' + TabAriA + ' ari ',
          'WHERE ari.Codigo=' + Geral.FF0(Prev),
          'AND ari.Apto=' + Geral.FF0(Depto),
          'AND ari.Boleto=' + Geral.FFI(Docum),
          ' ',
          'UNION ',
          ' ',
          'SELECT cns.CNAB_Cfg ',
          'FROM ' + TabCnsA + ' cns ',
          'LEFT JOIN ' + TabPrvA + ' prv ON prv.Periodo=cns.Periodo ',
          'WHERE prv.Codigo=' + Geral.FF0(Prev),
          'AND cns.Apto=' + Geral.FF0(Depto),
          'AND cns.Boleto=' + Geral.FFI(Docum),
          '']);
        CNAB_Cfg := Qry3.FieldByName('CNAB_Cfg').AsInteger;
        //
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False,
          ['ID_Cod1', 'ID_Cod2', 'ID_Cod4'], ['Conta'],
          [Prev, CodEnti, CNAB_Cfg], [Conta], True)
        then
          Exit;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        Qry.Next;
      end;
    end;
    UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, DataBase, [
      'UPDATE bloopcoes ',
      'SET CorrigiuProtAriCns = 1 ',
      '']);
  finally
    PB1.Position := 0;
    //
    Qry.Free;
    Qry3.Free;
    QryUpd.Free;
    Screen.Cursor := crDefault;
  end;
end;

end.
