object FmCondGerAvisos: TFmCondGerAvisos
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-012 :: Avisos no Bloqueto'
  ClientHeight = 664
  ClientWidth = 778
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 778
    Height = 502
    ActivePage = TabSheet3
    Align = alClient
    TabHeight = 25
    TabOrder = 0
    object TabSheet3: TTabSheet
      Caption = ' Bloqueto '
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object RGModelBloq: TRadioGroup
        Left = 0
        Top = 0
        Width = 770
        Height = 335
        Align = alClient
        Caption = ' Modelo de impress'#227'o do bloqueto: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'UBloqCond.PreencheModelosBloq')
        TabOrder = 0
        OnClick = RGModelBloqClick
      end
      object RGCompe: TRadioGroup
        Left = 0
        Top = 380
        Width = 770
        Height = 37
        Align = alBottom
        Caption = ' Ficha de compensa'#231#227'o (modelo E): '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '??'
          '1 (uma via)'
          '2 (duas vias)')
        TabOrder = 1
      end
      object GBConfig: TGroupBox
        Left = 0
        Top = 335
        Width = 770
        Height = 45
        Align = alBottom
        Caption = ' Configura'#231#227'o do modelo H, R, IB ou IR: '
        TabOrder = 2
        object SpeedButton2: TSpeedButton
          Left = 729
          Top = 18
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton2Click
        end
        object EdConfigBol: TdmkEditCB
          Left = 6
          Top = 18
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBConfigBol
          IgnoraDBLookupComboBox = False
        end
        object CBConfigBol: TdmkDBLookupComboBox
          Left = 65
          Top = 18
          Width = 660
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsConfigBol
          TabOrder = 1
          dmkEditCB = EdConfigBol
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 417
        Width = 770
        Height = 50
        Align = alBottom
        TabOrder = 3
        object CkBalAgrMens: TCheckBox
          Left = 6
          Top = 6
          Width = 407
          Height = 17
          Caption = 
            'Separar as somas de valores no balancete por compet'#234'ncia ao agru' +
            'par valores.'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object CkBloqFV: TCheckBox
          Left = 6
          Top = 25
          Width = 407
          Height = 17
          Caption = 'Boleto frente e verso'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Parte interna do bloqueto'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 770
        Height = 467
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label1: TLabel
          Left = 28
          Top = 44
          Width = 38
          Height = 13
          Caption = 'Linha 1:'
        end
        object Label2: TLabel
          Left = 28
          Top = 64
          Width = 38
          Height = 13
          Caption = 'Linha 2:'
        end
        object Label3: TLabel
          Left = 28
          Top = 84
          Width = 38
          Height = 13
          Caption = 'Linha 3:'
        end
        object Label4: TLabel
          Left = 28
          Top = 104
          Width = 38
          Height = 13
          Caption = 'Linha 4:'
        end
        object Label5: TLabel
          Left = 28
          Top = 124
          Width = 38
          Height = 13
          Caption = 'Linha 5:'
        end
        object Label6: TLabel
          Left = 28
          Top = 144
          Width = 38
          Height = 13
          Caption = 'Linha 6:'
        end
        object Label7: TLabel
          Left = 28
          Top = 224
          Width = 44
          Height = 13
          Caption = 'Linha 10:'
        end
        object Label8: TLabel
          Left = 28
          Top = 204
          Width = 38
          Height = 13
          Caption = 'Linha 9:'
        end
        object Label9: TLabel
          Left = 28
          Top = 184
          Width = 38
          Height = 13
          Caption = 'Linha 8:'
        end
        object Label10: TLabel
          Left = 28
          Top = 164
          Width = 38
          Height = 13
          Caption = 'Linha 7:'
        end
        object Label11: TLabel
          Left = 79
          Top = 24
          Width = 338
          Height = 13
          AutoSize = False
          Caption = '12345678901234567890123456789012345678901234567'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label12: TLabel
          Left = 79
          Top = 12
          Width = 338
          Height = 13
          AutoSize = False
          Caption = '         1         2         3         4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
        end
        object Label17: TLabel
          Left = 28
          Top = 244
          Width = 44
          Height = 13
          Caption = 'Linha 11:'
        end
        object Label18: TLabel
          Left = 28
          Top = 264
          Width = 44
          Height = 13
          Caption = 'Linha 12:'
        end
        object Label19: TLabel
          Left = 28
          Top = 284
          Width = 44
          Height = 13
          Caption = 'Linha 13:'
        end
        object Label20: TLabel
          Left = 28
          Top = 304
          Width = 44
          Height = 13
          Caption = 'Linha 14:'
        end
        object Label21: TLabel
          Left = 28
          Top = 324
          Width = 44
          Height = 13
          Caption = 'Linha 15:'
        end
        object Label22: TLabel
          Left = 28
          Top = 344
          Width = 44
          Height = 13
          Caption = 'Linha 16:'
        end
        object Label23: TLabel
          Left = 28
          Top = 364
          Width = 44
          Height = 13
          Caption = 'Linha 17:'
        end
        object Label24: TLabel
          Left = 28
          Top = 384
          Width = 44
          Height = 13
          Caption = 'Linha 18:'
        end
        object Label25: TLabel
          Left = 28
          Top = 404
          Width = 44
          Height = 13
          Caption = 'Linha 19:'
        end
        object Label26: TLabel
          Left = 28
          Top = 424
          Width = 44
          Height = 13
          Caption = 'Linha 20:'
        end
        object Edit1: TEdit
          Left = 76
          Top = 40
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 0
        end
        object Edit2: TEdit
          Left = 76
          Top = 60
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 1
        end
        object Edit3: TEdit
          Left = 76
          Top = 80
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 2
        end
        object Edit4: TEdit
          Left = 76
          Top = 100
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 3
        end
        object Edit5: TEdit
          Left = 76
          Top = 120
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 4
        end
        object Edit6: TEdit
          Left = 76
          Top = 140
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 5
        end
        object Edit7: TEdit
          Left = 76
          Top = 160
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 6
        end
        object Edit8: TEdit
          Left = 76
          Top = 180
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 7
        end
        object Edit9: TEdit
          Left = 76
          Top = 200
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 8
        end
        object Edit10: TEdit
          Left = 76
          Top = 220
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 9
        end
        object GroupBox1: TGroupBox
          Left = 420
          Top = 24
          Width = 341
          Height = 241
          Caption = ' Modelos de impress'#227'o e tamanho m'#225'ximo da linha (em carateres): '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = ' Modelos de impress'#227'o e tamanho'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          object Label13: TLabel
            Left = 16
            Top = 28
            Width = 210
            Height = 14
            Caption = 'Modelo A:       40 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label14: TLabel
            Left = 16
            Top = 44
            Width = 210
            Height = 14
            Caption = 'Modelo B:       47 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label15: TLabel
            Left = 16
            Top = 60
            Width = 210
            Height = 14
            Caption = 'Modelo C:       40 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label16: TLabel
            Left = 16
            Top = 76
            Width = 210
            Height = 14
            Caption = 'Modelo D:       40 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label27: TLabel
            Left = 16
            Top = 92
            Width = 210
            Height = 14
            Caption = 'Modelo E:       40 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label28: TLabel
            Left = 16
            Top = 108
            Width = 210
            Height = 14
            Caption = 'Modelo F:       40 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label29: TLabel
            Left = 16
            Top = 124
            Width = 210
            Height = 14
            Caption = 'Modelo G:       40 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label31: TLabel
            Left = 16
            Top = 188
            Width = 210
            Height = 14
            Caption = 'Modelo H4:      41 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label32: TLabel
            Left = 16
            Top = 204
            Width = 210
            Height = 14
            Caption = 'Modelo H5:      42 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label33: TLabel
            Left = 16
            Top = 220
            Width = 210
            Height = 14
            Caption = 'Modelo H6:      35 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label34: TLabel
            Left = 16
            Top = 140
            Width = 210
            Height = 14
            Caption = 'Modelo H1:      40 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label30: TLabel
            Left = 16
            Top = 156
            Width = 210
            Height = 14
            Caption = 'Modelo H2:      40 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object Label35: TLabel
            Left = 16
            Top = 172
            Width = 210
            Height = 14
            Caption = 'Modelo H3:      40 caracteres.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
        end
        object Edit11: TEdit
          Left = 76
          Top = 240
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 11
        end
        object Edit12: TEdit
          Left = 76
          Top = 260
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 12
        end
        object Edit13: TEdit
          Left = 76
          Top = 280
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 13
        end
        object Edit14: TEdit
          Left = 76
          Top = 300
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 14
        end
        object Edit15: TEdit
          Left = 76
          Top = 320
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 15
        end
        object Edit16: TEdit
          Left = 76
          Top = 340
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 16
        end
        object Edit17: TEdit
          Left = 76
          Top = 360
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 17
        end
        object Edit18: TEdit
          Left = 76
          Top = 380
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 18
        end
        object Edit19: TEdit
          Left = 76
          Top = 400
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 19
        end
        object Edit20: TEdit
          Left = 76
          Top = 420
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 20
        end
        object BitBtn1: TBitBtn
          Left = 416
          Top = 360
          Width = 209
          Height = 40
          Caption = '&Abaixa uma linha (todos)'
          NumGlyphs = 2
          TabOrder = 21
          OnClick = BitBtn1Click
        end
        object BitBtn2: TBitBtn
          Left = 416
          Top = 404
          Width = 209
          Height = 40
          Caption = '&Sobe uma linha (todos)'
          NumGlyphs = 2
          TabOrder = 22
          OnClick = BitBtn2Click
        end
        object Edit21: TEdit
          Left = 420
          Top = 332
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 23
          Visible = False
        end
        object Edit0: TEdit
          Left = 420
          Top = 312
          Width = 337
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          MaxLength = 47
          ParentFont = False
          TabOrder = 24
          Visible = False
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Parte externa do bloqueto (Verso)'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object MeAvisoVerso: TMemo
        Left = 0
        Top = 0
        Width = 770
        Height = 37
        Align = alTop
        TabOrder = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 778
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 730
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 682
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 233
        Height = 32
        Caption = 'Avisos no Bloqueto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 233
        Height = 32
        Caption = 'Avisos no Bloqueto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 233
        Height = 32
        Caption = 'Avisos no Bloqueto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 550
    Width = 778
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 774
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 594
    Width = 778
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 774
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 630
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DsConfigBol: TDataSource
    DataSet = QrConfigBol
    Left = 358
    Top = 232
  end
  object QrConfigBol: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Colunas'
      'FROM configbol')
    Left = 330
    Top = 232
    object QrConfigBolCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigBolNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrConfigBolColunas: TSmallintField
      FieldName = 'Colunas'
    end
  end
end
