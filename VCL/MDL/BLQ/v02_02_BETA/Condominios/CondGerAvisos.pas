unit CondGerAvisos;

interface

uses
  System.StrUtils, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkGeral, DB,
  mySQLDbTables, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage,
  UnDmkEnums;

type
  TFmCondGerAvisos = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    Edit10: TEdit;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    MeAvisoVerso: TMemo;
    Label17: TLabel;
    Edit11: TEdit;
    Label18: TLabel;
    Edit12: TEdit;
    Label19: TLabel;
    Edit13: TEdit;
    Edit14: TEdit;
    Label20: TLabel;
    Label21: TLabel;
    Edit15: TEdit;
    Edit16: TEdit;
    Label22: TLabel;
    Label23: TLabel;
    Edit17: TEdit;
    Edit18: TEdit;
    Label24: TLabel;
    Label25: TLabel;
    Edit19: TEdit;
    Edit20: TEdit;
    Label26: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit21: TEdit;
    Edit0: TEdit;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label30: TLabel;
    Label35: TLabel;
    TabSheet3: TTabSheet;
    DsConfigBol: TDataSource;
    QrConfigBol: TmySQLQuery;
    RGModelBloq: TRadioGroup;
    RGCompe: TRadioGroup;
    GBConfig: TGroupBox;
    EdConfigBol: TdmkEditCB;
    CBConfigBol: TdmkDBLookupComboBox;
    Panel3: TPanel;
    CkBalAgrMens: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    SpeedButton2: TSpeedButton;
    QrConfigBolColunas: TSmallintField;
    QrConfigBolCodigo: TIntegerField;
    QrConfigBolNome: TWideStringField;
    CkBloqFV: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RGModelBloqClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    procedure ConfiguraModelBloq(ModelBol: Integer);
    procedure MostraEdicao(ApenasSelModelBol: Boolean);
  public
    { Public declarations }
    FCodigo, FModelBloq, FConfigBol, FBalAgrMens, FColunas, FCompe: Integer;
    FTabela: String;
    FQueryPrev: TMySQLQuery;
    FApenasSelModelBol: Boolean;
  end;

  var
    FmCondGerAvisos: TFmCondGerAvisos;
  const
    CO_Modelos_E: String = 'E';
    CO_Modelos_H_R_IB_IR: Array[0..3] of String = ('H', 'R', 'IB', 'IR');
    CO_Modelos_IB_IR: Array[0..1] of String = ('IB', 'IR');

implementation

uses Module, UMySQLModule, Principal, UnInternalConsts, UnMyObjects, UnBloquetos,
  DmkDAC_PF, UnBloquetos_Jan;

{$R *.DFM}

procedure TFmCondGerAvisos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerAvisos.ConfiguraModelBloq(ModelBol: Integer);
var
  ModelBolTxt: String;
begin
  ModelBolTxt := UBloquetos.LetraModelosBloq(ModelBol);
  //
  if ModelBolTxt = CO_Modelos_E then
  begin
    GBConfig.Visible := False;
    RGCompe.Visible  := True;
  end else
  if AnsiMatchStr(ModelBolTxt, CO_Modelos_H_R_IB_IR) then
  begin
    GBConfig.Visible := True;
    RGCompe.Visible  := False;
  end else
  begin
    GBConfig.Visible := False;
    RGCompe.Visible  := False;
  end;
end;

procedure TFmCondGerAvisos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerAvisos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerAvisos.FormShow(Sender: TObject);
begin
  MostraEdicao(FApenasSelModelBol);
end;

procedure TFmCondGerAvisos.MostraEdicao(ApenasSelModelBol: Boolean);
begin
  PageControl1.ActivePageIndex := 0;
  GBConfig.Visible             := False;
  RGCompe.Visible              := False;
  //
  if ApenasSelModelBol = False then
  begin
    TabSheet1.TabVisible := True;
    TabSheet2.TabVisible := True;
    CkBloqFV.Visible     := True;
    //
    MeAvisoVerso.Text     := FQueryPrev.FieldByName('AvisoVerso').AsString;
    Edit1.Text            := FQueryPrev.FieldByName('Aviso01').AsString;
    Edit2.Text            := FQueryPrev.FieldByName('Aviso02').AsString;
    Edit3.Text            := FQueryPrev.FieldByName('Aviso03').AsString;
    Edit4.Text            := FQueryPrev.FieldByName('Aviso04').AsString;
    Edit5.Text            := FQueryPrev.FieldByName('Aviso05').AsString;
    Edit6.Text            := FQueryPrev.FieldByName('Aviso06').AsString;
    Edit7.Text            := FQueryPrev.FieldByName('Aviso07').AsString;
    Edit8.Text            := FQueryPrev.FieldByName('Aviso08').AsString;
    Edit9.Text            := FQueryPrev.FieldByName('Aviso09').AsString;
    Edit10.Text           := FQueryPrev.FieldByName('Aviso10').AsString;
    Edit11.Text           := FQueryPrev.FieldByName('Aviso11').AsString;
    Edit12.Text           := FQueryPrev.FieldByName('Aviso12').AsString;
    Edit13.Text           := FQueryPrev.FieldByName('Aviso13').AsString;
    Edit14.Text           := FQueryPrev.FieldByName('Aviso14').AsString;
    Edit15.Text           := FQueryPrev.FieldByName('Aviso15').AsString;
    Edit16.Text           := FQueryPrev.FieldByName('Aviso16').AsString;
    Edit17.Text           := FQueryPrev.FieldByName('Aviso17').AsString;
    Edit18.Text           := FQueryPrev.FieldByName('Aviso18').AsString;
    Edit19.Text           := FQueryPrev.FieldByName('Aviso19').AsString;
    Edit20.Text           := FQueryPrev.FieldByName('Aviso20').AsString;
    CkBalAgrMens.Checked  := Geral.IntToBool(FQueryPrev.FieldByName('BalAgrMens').AsInteger);
    CkBloqFV.Checked      := Geral.IntToBool(FQueryPrev.FieldByName('BloqFV').AsInteger);
    RGModelBloq.ItemIndex := FQueryPrev.FieldByName('ModelBloq').AsInteger;
    RGCompe.ItemIndex     := FQueryPrev.FieldByName('Compe').AsInteger;
    EdConfigBol.Text      := Geral.FF0(FQueryPrev.FieldByName('ConfigBol').AsInteger);
    CBConfigBol.KeyValue  := FQueryPrev.FieldByName('ConfigBol').AsInteger;
  end else
  begin
    TabSheet1.TabVisible := False;
    TabSheet2.TabVisible := False;
    CkBloqFV.Visible     := False;
    //
    RGModelBloq.ItemIndex := -1;
  end;
  ConfiguraModelBloq(RGModelBloq.ItemIndex);
end;

procedure TFmCondGerAvisos.RGModelBloqClick(Sender: TObject);
begin
  ConfiguraModelBloq(RGModelBloq.ItemIndex);
end;

procedure TFmCondGerAvisos.SpeedButton2Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UBloquetos_Jan.MostraConfigBol(EdConfigBol.ValueVariant);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.AbreQuery(QrConfigBol, DMod.MyDB);
    //
    EdConfigBol.ValueVariant := VAR_CADASTRO;
    CBConfigBol.KeyValue     := VAR_CADASTRO;
    EdConfigBol.SetFocus;
  end;
end;

procedure TFmCondGerAvisos.BitBtn1Click(Sender: TObject);
begin
  Edit21.Text := Edit20.Text;
  Edit20.Text := Edit19.Text;
  Edit19.Text := Edit18.Text;
  Edit18.Text := Edit17.Text;
  Edit17.Text := Edit16.Text;
  Edit16.Text := Edit15.Text;
  Edit15.Text := Edit14.Text;
  Edit14.Text := Edit13.Text;
  Edit13.Text := Edit12.Text;
  Edit12.Text := Edit11.Text;
  Edit11.Text := Edit10.Text;
  Edit10.Text := Edit9.Text;
  Edit9.Text  := Edit8.Text;
  Edit8.Text  := Edit7.Text;
  Edit7.Text  := Edit6.Text;
  Edit6.Text  := Edit5.Text;
  Edit5.Text  := Edit4.Text;
  Edit4.Text  := Edit3.Text;
  Edit3.Text  := Edit2.Text;
  Edit2.Text  := Edit1.Text;
  Edit1.Text  := Edit0.Text;
  Edit0.Text  := '';
end;

procedure TFmCondGerAvisos.BitBtn2Click(Sender: TObject);
begin
  Edit0.Text  := Edit1.Text;
  Edit1.Text  := Edit2.Text;
  Edit2.Text  := Edit3.Text;
  Edit3.Text  := Edit4.Text;
  Edit4.Text  := Edit5.Text;
  Edit5.Text  := Edit6.Text;
  Edit6.Text  := Edit7.Text;
  Edit7.Text  := Edit8.Text;
  Edit8.Text  := Edit9.Text;
  Edit9.Text  := Edit10.Text;
  Edit10.Text := Edit11.Text;
  Edit11.Text := Edit12.Text;
  Edit12.Text := Edit13.Text;
  Edit13.Text := Edit14.Text;
  Edit14.Text := Edit15.Text;
  Edit15.Text := Edit16.Text;
  Edit16.Text := Edit17.Text;
  Edit17.Text := Edit18.Text;
  Edit18.Text := Edit19.Text;
  Edit19.Text := Edit20.Text;
  Edit20.Text := Edit21.Text;
  Edit21.Text := '';
end;

procedure TFmCondGerAvisos.BtOKClick(Sender: TObject);
var
  Continua: Boolean;
  ModelBloq: Integer;
begin
  Continua  := False;
  ModelBloq := RGModelBloq.ItemIndex;
  //
  if FApenasSelModelBol = False then
  begin
    if MyObjects.FIC(FCodigo = 0, nil, 'Per�odo n�o informado!') then Exit;
    if MyObjects.FIC(FTabela = '', nil, 'Tabela n�o informada!') then Exit;
  end;
  //
  if UBloquetos.LetraModelosBloq(ModelBloq) = CO_Modelos_E then
  begin
    if MyObjects.FIC(RGCompe.ItemIndex < 1, nil, 'Informe a ficha de compensa��o!') then
      Exit
    else
      Continua := True;
  end else
  if AnsiMatchStr(UBloquetos.LetraModelosBloq(ModelBloq), CO_Modelos_H_R_IB_IR) then
  begin
    if MyObjects.FIC(EdConfigBol.ValueVariant = 0, EdConfigBol, 'Informe a configura��o do modelo!') then
      Exit;
    //
    if AnsiMatchStr(UBloquetos.LetraModelosBloq(ModelBloq), CO_Modelos_IB_IR) then
    begin
      if MyObjects.FIC(QrConfigBolColunas.Value <> 1, EdConfigBol,
        'O formato selecionado possui suporte para apenas uma coluna!' +
        sLineBreak + 'Escolha um modelo compat�vel!')
      then
        Exit;
    end;
    Continua := True;
  end else
    Continua := True;
  if Continua then
  begin
    if FApenasSelModelBol = False then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, FTabela, False, [
        'Aviso01', 'Aviso02', 'Aviso03',
        'Aviso04', 'Aviso05', 'Aviso06',
        'Aviso07', 'Aviso08', 'Aviso09',
        'Aviso10', 'AvisoVerso', 'Aviso11',
        'Aviso12', 'Aviso13', 'Aviso14',
        'Aviso15', 'Aviso16', 'Aviso17',
        'Aviso18', 'Aviso19', 'Aviso20',
        'ModelBloq', 'ConfigBol',
        'BalAgrMens', 'Compe',
        'BloqFV'
      ], ['Codigo'], [
        Edit1.Text, Edit2.Text, Edit3.Text,
        Edit4.Text, Edit5.Text, Edit6.Text,
        Edit7.Text, Edit8.Text, Edit9.Text,
        Edit10.Text, MeAvisoVerso.Text, Edit11.Text,
        Edit12.Text, Edit13.Text, Edit14.Text,
        Edit15.Text, Edit16.Text, Edit17.Text,
        Edit18.Text, Edit19.Text, Edit20.Text,
        RGModelBloq.ItemIndex, EdConfigBol.ValueVariant,
        Geral.BoolToInt(CkBalAgrMens.Checked), RGCompe.ItemIndex,
        Geral.BoolToInt(CkBloqFV.Checked)
      ], [FCodigo], True) then
      begin
        Close;
      end;
    end else
    begin
      FModelBloq  := RGModelBloq.ItemIndex;
      FConfigBol  := EdConfigBol.ValueVariant;
      FBalAgrMens := Geral.BoolToInt(CkBalAgrMens.Checked);
      FCompe      := RGCompe.ItemIndex;
      //
      if FConfigBol <> 0 then
        FColunas := QrConfigBolColunas.Value
      else
        FColunas := 0;
      //
      Close;
    end;
  end;
end;

procedure TFmCondGerAvisos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UBloquetos.PreencheModelosBloq(RGModelBloq);
  //
  FTabela            := '';
  FCodigo            := 0;
  FModelBloq         := 0;
  FConfigBol         := 0;
  FBalAgrMens        := 0;
  FColunas           := 0;
  FCompe             := 0;
  FApenasSelModelBol := False;
  //
  CBConfigBol.ListSource := DsConfigBol;
  //
  UnDmkDAC_PF.AbreQuery(QrConfigBol, Dmod.MyDB);
end;

end.
